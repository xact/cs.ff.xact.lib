//using System;
//using System.Collections;
//using System.Collections.Generic;
//using Omu.ValueInjecter;

//namespace XAct.ObjectMapping.Implementations
//{
//    public class TypeMapperFactory :ITypeMapperFactory
//    {
//        private static readonly IDictionary<Type, object> Mappers = new Dictionary<Type, object>();
//        private IValueInjecterConventionInjectionCache _conventionInjectionCache;

//        public TypeMapperFactory(IValueInjecterConventionInjectionCache conventionInjectionCache)
//        {
//            _conventionInjectionCache = conventionInjectionCache;
//        }



//        public ITypeMapper<TSource, TTarget> GetMapper<TSource, TTarget>(ITypeMap typeMap, string contextName)
//        {
//            IDictionary<string, IList<ConventionInjection>> result;

//            if (_conventionInjectionCache.TryGetValue(typeMap, out result))
//            {
//                if (contextName.IsNullOrEmpty())
//                {
//                    contextName = string.Empty;
//                }

//                IList<ConventionInjection> results2;
                
//                if (result.TryGetValue(contextName, out results2))
//                {
//                    return results2;
//                }
//            }

//            //if we have a specified TypeMapper for <TSource,Target> return it
//            if (Mappers.ContainsKey(typeof (ITypeMapper<TSource, TTarget>)))
//                return Mappers[typeof (ITypeMapper<TSource, TTarget>)] as ITypeMapper<TSource, TTarget>;

//            //if both Source and Target types are Enumerables return new EnumerableTypeMapper<TSource,TTarget>()
//            if (typeof (TSource).IsEnumerable() && typeof (TTarget).IsEnumerable())
//            {
//                return
//                    (ITypeMapper<TSource, TTarget>)
//                    Activator.CreateInstance(typeof (EnumerableTypeMapper<,>).MakeGenericType(typeof (TSource),
//                                                                                              typeof (TTarget)));
//            }

//            //return the default TypeMapper
//            return new TypeMapper<TSource, TTarget>();
//        }

//        public static void AddMapper<TS, TT>(ITypeMapper<TS, TT> o)
//        {
//            Mappers.Add(typeof (ITypeMapper<TS, TT>), o);
//        }

//        public static void ClearMappers()
//        {
//            Mappers.Clear();
//        }
//    }
//}