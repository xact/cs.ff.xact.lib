﻿
namespace XAct.ObjectMapping
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public abstract class ValueInjecterTypeMapperBase  : TypeMapperBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueInjecterTypeMapperBase" /> class.
        /// </summary>
        protected ValueInjecterTypeMapperBase(Type sourceType, Type targetType) : base(sourceType, targetType) { }

        /// <summary>
        /// Action executed before source is mapped to target.
        /// <remarks>
        /// </remarks>
        /// </summary>
        protected override void PreInternalMap(object source = null)
        {
            //Do nothing (with ValueInjector there is no Registration of the Mapper require before it can be used.
        }

        /// <summary>
        /// Map a source entity to a target entity
        /// </summary>
        /// <param name="source">The source to map</param>
        /// <param name="target">The target.</param>
        /// <returns>
        /// A existing (or new if target is null) instance of the target.
        /// </returns>
        /// <remarks>
        /// If you use a framework, use this method for setup or resolve mapping.
        /// <example>Automapper.Map{TSource,KTarget}</example>
        /// </remarks>
        protected abstract override object InternalMap(object source, object target = null);


    }
}
