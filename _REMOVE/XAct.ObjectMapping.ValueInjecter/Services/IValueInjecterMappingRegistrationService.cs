﻿namespace XAct.ObjectMapping
{
    using Omu.ValueInjecter;

    /// <summary>
    /// An Automapper based specialization of the 
    /// <see cref="IObjectMappingRegistrationService"/>
    /// contract.
    /// </summary>
    public interface IValueInjecterMappingRegistrationService : IObjectMappingRegistrationService, IHasXActLibService
    {




        /// <summary>
        /// Registers the convention against the specific type pair.
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="T2">The type of the 2.</typeparam>
        /// <param name="typeMap">The type map.</param>
        /// <param name="conventionInjection">The convention injection.</param>
        /// <param name="contextName">Name of the context.</param>
        void RegisterConvention<T1, T2>(ITypeMap<T1, T2> typeMap, ConventionInjection conventionInjection, string contextName = null);


    }
}