﻿namespace XAct.ObjectMapping
{
    using System.Collections.Generic;
    using Omu.ValueInjecter;

    /// <summary>
    /// 
    /// </summary>
    public interface IValueInjecterConventionInjectionCache : IDictionary<ITypeMap, IDictionary<string,IList<ConventionInjection>>>
    {
        
    }

}