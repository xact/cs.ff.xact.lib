﻿namespace XAct.ObjectMapping.Implementations
{
    using System;
    using System.Collections.Generic;
    using Omu.ValueInjecter;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An ValueInjecter based implementation of 
    /// <see cref="IObjectMappingRegistrationService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IObjectMappingRegistrationService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class ValueInjecterMappingRegistrationService : XActLibServiceBase, IValueInjecterMappingRegistrationService
    {
        private readonly IValueInjecterConventionInjectionCache _valueInjecterConventionInjectionCache;
        private readonly IObjectMappingServiceState _typeMapperCache;


        /// <summary>
        /// Tests this completeness of the registered maps.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Test()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ValueInjecterMappingRegistrationService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="typeMapperCache">The type mapper cache.</param>
        /// <param name="valueInjecterConventionInjectionCache">The value injecter convention injection cache.</param>
        public ValueInjecterMappingRegistrationService(ITracingService tracingService, IObjectMappingServiceState typeMapperCache, IValueInjecterConventionInjectionCache valueInjecterConventionInjectionCache)
            : base(tracingService)
        {
            _typeMapperCache = typeMapperCache;
            _valueInjecterConventionInjectionCache = valueInjecterConventionInjectionCache;
        }

        /// <summary>
        /// Registers the <see cref="ITypeMapper{TS,TT}" /> with the service
        /// so that it can later be used by <see cref="IObjectMappingService" />
        /// </summary>
        /// <param name="typeMapper">The type mapper.</param>
        /// <param name="contextName">Name of the context.</param>
        public void RegisterMapper(ITypeMapper typeMapper, string contextName=null)
        {
            typeMapper.ValidateIsNotDefault("typeMapper");

            _tracingService.Trace(TraceLevel.Verbose, "RegisteringMapper: {0}", typeMapper.ToString());


            //Add or replace:
            int index = typeMapper.GetHashCode();
            _typeMapperCache.RegisterMap(index, typeMapper, contextName);


            IInitializableTypeMapper typeMapper2 = (IInitializableTypeMapper)typeMapper;

            if (!typeMapper2.DeferInitialization)
            {
                //Add to underlying AutoMapper:
                typeMapper2.Initialize();
            }
        }


        /// <summary>
        /// Registers the convention.
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="T2">The type of the 2.</typeparam>
        /// <param name="typeMap">The type map.</param>
        /// <param name="conventionInjection">The convention injection.</param>
        /// <param name="contextName">Name of the condition.</param>
        public void RegisterConvention<T1, T2>(ITypeMap<T1, T2> typeMap, ConventionInjection conventionInjection, string contextName = null)
        {
            if (contextName.IsNullOrEmpty())
            {
                contextName = string.Empty;
            }

            IDictionary<string, IList<ConventionInjection>> conventions;

            if (!_valueInjecterConventionInjectionCache.TryGetValue(typeMap, out conventions))
            {
                conventions = new Dictionary<string, IList<ConventionInjection>>();
                _valueInjecterConventionInjectionCache[typeMap] = conventions;
            }

            if (!conventions.ContainsKey(contextName))
            {
                conventions[contextName] = new List<ConventionInjection>();
            }

            IList<ConventionInjection> conventions2 = conventions[contextName];

            if (!conventions2.Contains(conventionInjection))
            {
                conventions2.Add(conventionInjection);
            }
        }

    }
}
