﻿namespace XAct.ObjectMapping.Implementations
{
    using System;
    using System.Collections.Generic;
    using Omu.ValueInjecter;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IValueInjecterMappingService"/>,
    /// using <c>ValueInjecter</c>
    /// </summary>
    [DefaultBindingImplementation(typeof(IObjectMappingService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class ValueInjecterMappingService : IValueInjecterMappingService
    {
        private readonly IObjectMappingServiceState _typeMapperCache;
        private readonly IValueInjecterConventionInjectionCache _valueInjecterConventionInjectionCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueInjecterMappingService" /> class.
        /// </summary>
        /// <param name="valueInjecterConventionInjectionCache">The value injecter convention injection cache.</param>
        /// <param name="typeMapperCache">The type mapper cache.</param>
        public ValueInjecterMappingService(IValueInjecterConventionInjectionCache valueInjecterConventionInjectionCache,
                                           IObjectMappingServiceState typeMapperCache)
        {
            _valueInjecterConventionInjectionCache = valueInjecterConventionInjectionCache;
            _typeMapperCache = typeMapperCache;
        }


        /// <summary>
        /// Tests this completeness of the registered maps.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Test()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Maps the specified t1 type to the given t2 type.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="contextName">Name of the condition.</param>
        /// <param name="strict">if set to <c>true</c> [strict].</param>
        /// <returns></returns>
        public TTarget Map<TSource, TTarget>(TSource source, string contextName = null, bool strict = true)
            where TSource : class
            where TTarget : class, new()
        {

            return Map(typeof (TSource), typeof (TTarget), source, null, contextName) as TTarget;
        }







        /// <summary>
        /// Maps the specified t1 type to the given t2 type.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="contextName">Name of the condition.</param>
        /// <param name="strict">if set to <c>true</c> [strict].</param>
        /// <returns></returns>
        public TTarget Map<TSource, TTarget>(TSource source, TTarget target, string contextName = null, bool strict=true)
            where TSource : class
            where TTarget : class
        {

            return Map(typeof(TSource), typeof(TTarget), source, target, contextName) as TTarget;
        }


        /// <summary>
        /// Maps the specified source.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="contextName">Name of the context.</param>
        /// <param name="strict">if set to <c>true</c> [strict].</param>
        /// <returns></returns>
        /// <internal>
        /// IMPORTANT: Must keep this signature in order to handle
        /// scenarios such as:
        ///   <code>
        ///   <![CDATA[
        /// return XAct.DependencyResolver.Current.GetInstance<IValueInjecterMappingService>().Map(c.SourceProp.Type, c.TargetProp.Type, c.Source.Value, c.Target.Value);
        /// ]]>
        ///   </code>
        /// when invoked from <see cref="MapperInjectionConventionInjection" />
        ///   </internal>
        public object Map(Type sourceType, Type targetType, object source, object target, string contextName = null, bool strict = true)
        {
            if (target == null)
            {
                target = Activator.CreateInstance(targetType);
            }

            ITypeMapper typeMapper;

            if (!_typeMapperCache.TryGetMapper(sourceType,targetType, out typeMapper, contextName))
            {
                typeMapper = new DefaultValueInjecterTypeMapper(sourceType,targetType);
            }

            typeMapper.Map(source, target);

            return target;
        }







        /// <summary>
        /// Gets the mapping between the two given types.
        /// </summary>
        /// <param name="t1">The t1.</param>
        /// <param name="t2">The t2.</param>
        /// <param name="contextName">Name of the condition.</param>
        /// <param name="strict">if set to <c>true</c> [strict].</param>
        /// <returns></returns>
        private IEnumerable<ConventionInjection> GetConventions(Type t1, Type t2, string contextName = null, bool strict = true)
        {
            if (contextName.IsNullOrEmpty())
            {
                contextName = string.Empty;
            }

            //We build a unique TypeMap, 
            //to get the unique Id which is unique per pair:
            ITypeMap typeMap = new TypeMap(t1, t2);

            IDictionary<string, IList<ConventionInjection>> conventions;


            if (!_valueInjecterConventionInjectionCache.TryGetValue(typeMap, out conventions))
            {
                return null;
            }
            IList<ConventionInjection>  namedConventions;

            if (!conventions.TryGetValue(contextName, out namedConventions))
            {
                return null;
            }

            return namedConventions;
        }





    }
}
