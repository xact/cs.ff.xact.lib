namespace XAct.ObjectMapping
{
    using System;
    using Omu.ValueInjecter;

    /// <summary>
    /// A ConventionInjection mapping for mapping 
    /// Sa,
    /// <para>
    /// <example>
    /// <code>
    /// eg: int Age = int? Age;
    /// </code>
    /// </example>
    /// </para>
    /// </summary>
    public class NormalToNullablesConventionInjection : ConventionInjection
    {
        private readonly string _prefix;
        private readonly string _suffix;

        /// <summary>
        /// Initializes a new instance of the <see cref="NormalToNullablesConventionInjection"/> class.
        /// </summary>
        public NormalToNullablesConventionInjection()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NormalToNullablesConventionInjection"/> class.
        /// </summary>
        /// <param name="targetPrefix">The target prefix.</param>
        /// <param name="targetSuffix">The target suffix.</param>
        public NormalToNullablesConventionInjection(string targetPrefix = null, string targetSuffix = null)
        {
            if (targetPrefix == null)
            {
                targetPrefix = string.Empty;
            }
            if (targetSuffix == null)
            {
                targetSuffix = string.Empty;
            }
            _prefix = targetPrefix;
            _suffix = targetSuffix;
        }

        /// <summary>
        /// Matches the specified c.
        /// </summary>
        /// <param name="c">The ValueInjecter ConvetionInfo</param>
        /// <returns></returns>
        protected override bool Match(ConventionInfo c)
        {
            //Same name, and 
            //source property is same type as target property (even if nullable).
            string targetName = _prefix + c.SourceProp.Name + _suffix;
            return 
                   c.TargetProp.Name == targetName &&
                   c.SourceProp.Type == Nullable.GetUnderlyingType(c.TargetProp.Type);
        }
    }

}