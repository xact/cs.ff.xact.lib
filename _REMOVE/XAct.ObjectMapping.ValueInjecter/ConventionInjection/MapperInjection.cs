namespace XAct.ObjectMapping
{
    using Omu.ValueInjecter;

    /// <summary>
    /// </summary>
    public class MapperInjectionConventionInjection : ConventionInjection
    {
        /// <summary>
        /// Matches the specified scnario .
        /// </summary>
        /// <param name="c">The ValueInjecter ConvetionInfo</param>
        /// <returns></returns>
        protected override bool Match(ConventionInfo c)
        {
            //Same name

            return c.SourceProp.Name == c.TargetProp.Name &&
                   !c.SourceProp.Type.IsValueType && c.SourceProp.Type != typeof(string) &&
                   !c.SourceProp.Type.IsGenericType && !c.TargetProp.Type.IsGenericType
                   ||
                   c.SourceProp.Type.IsEnumerable() &&
                   c.TargetProp.Type.IsEnumerable();
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns></returns>
        protected override object SetValue(ConventionInfo c)
        {
            if (c.SourceProp.Value == null) return null;

            return XAct.DependencyResolver.Current.GetInstance<IValueInjecterMappingService>().Map(c.SourceProp.Type, c.TargetProp.Type, c.Source.Value, c.Target.Value);



            //return Mapper.Map(c.SourceProp.Value, c.TargetProp.Value, c.SourceProp.Type, c.TargetProp.Type);
        }
    }
}