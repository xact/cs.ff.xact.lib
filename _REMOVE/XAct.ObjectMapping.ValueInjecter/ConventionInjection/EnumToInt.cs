namespace XAct.ObjectMapping.Conventions
{
    using System;
    using Omu.ValueInjecter;

    /// <summary>
    /// A ConventionInjection mapping for mapping 
    /// Enum value to Integers.
    /// <para>
    /// Example, maps A to B:
    /// <code>
    /// <![CDATA[
    /// class A {Gender Gender {get;set;}}
    /// calss B {int Gender {get;set;}
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Note: consider EnumToRawInt ConventionInjection
    /// </para>
    /// </summary>
    public class EnumToIntConventionInjection : ConventionInjection
    {
        private readonly string _prefix;

        private readonly string _suffix;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumToIntConventionInjection"/> class.
        /// </summary>
        public EnumToIntConventionInjection()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumToIntConventionInjection"/> class.
        /// </summary>
        /// <param name="targetPrefix">The optional target prefix.</param>
        /// <param name="targetSuffix">The optional target suffix (eg: "Raw").</param>
        public EnumToIntConventionInjection(string targetPrefix = null, string targetSuffix = null)
        {
            if (targetPrefix == null)
            {
                targetPrefix = string.Empty;
            }
            if (targetSuffix == null)
            {
                targetSuffix = string.Empty;
            }
            _prefix = targetPrefix;
            _suffix = targetSuffix;
        }

        /// <summary>
        /// Matches the specified c.
        /// </summary>
        /// <param name="c">The ValueInjecter ConvetionInfo</param>
        /// <returns></returns>
        protected override bool Match(ConventionInfo c)
        {

            //Same name:
            //enum src => int target:
            string targetName = _prefix + c.SourceProp.Name + _suffix;
            return
                   c.TargetProp.Name == targetName &&
                   c.SourceProp.Type.IsSubclassOf(typeof(Enum)) && c.TargetProp.Type == typeof(int);
        }
    }
}