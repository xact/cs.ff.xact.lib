namespace XAct.ObjectMapping
{
    using System;
    using Omu.ValueInjecter;

    /// <summary>
    /// A ConventionInjection mapping for mapping 
    /// Nulable source properties to target properties.
    /// <para>
    /// <example>
    /// <code>
    /// eg: int? Age = int Age;
    /// </code>
    /// </example>
    /// </para>
    /// </summary>
    public class NullablesToNormalConventionInjection : ConventionInjection
    {

        private readonly string _prefix;
        private readonly string _suffix;

        /// <summary>
        /// Initializes a new instance of the <see cref="NormalToNullablesConventionInjection"/> class.
        /// </summary>
        public NullablesToNormalConventionInjection()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NormalToNullablesConventionInjection"/> class.
        /// </summary>
        /// <param name="targetPrefix">The target prefix.</param>
        /// <param name="targetSuffix">The target suffix.</param>
        public NullablesToNormalConventionInjection(string targetPrefix = null, string targetSuffix = null)
        {
            if (targetPrefix == null)
            {
                targetPrefix = string.Empty;
            }
            if (targetSuffix == null)
            {
                targetSuffix = string.Empty;
            }
            _prefix = targetPrefix;
            _suffix = targetSuffix;
        }

        /// <summary>
        /// Matches the specified c.
        /// </summary>
        /// <param name="c">The ValueInjecter ConvetionInfo</param>
        /// <returns></returns>
        protected override bool Match(ConventionInfo c)
        {
            //Same name, and 
            //source nullable property type is same type as target property.
            string targetName = _prefix + c.SourceProp.Name + _suffix;
            return
                   c.TargetProp.Name == targetName &&
                   Nullable.GetUnderlyingType(c.SourceProp.Type) == c.TargetProp.Type;
        }
    }
}