namespace XAct.ObjectMapping
{
    using System;
    using Omu.ValueInjecter;

    /// <summary>
    /// A ConventionInjection mapping for mapping 
    /// integers to Enums.
    /// </summary>
    public class IntToEnumConventionInjection : ConventionInjection
    {
        private readonly string _prefix;
        private readonly string _suffix;

        /// <summary>
        /// Initializes a new instance of the <see cref="IntToEnumConventionInjection"/> class.
        /// </summary>
        public IntToEnumConventionInjection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IntToEnumConventionInjection"/> class.
        /// </summary>
        /// <param name="targetPrefix">The target prefix.</param>
        /// <param name="targetSuffix">The target suffix.</param>
        public IntToEnumConventionInjection(string targetPrefix = null, string targetSuffix = null)
        {
            if (targetPrefix == null)
            {
                targetPrefix = string.Empty;
            }
            if (targetSuffix == null)
            {
                targetSuffix = string.Empty;
            }
            _prefix = targetPrefix;
            _suffix = targetSuffix;
        }



        /// <summary>
        /// Matches the the scenario when the source is an integer, and the target an enum.
        /// </summary>
        /// <param name="c">The ValueInjecter ConvetionInfo</param>
        /// <returns></returns>
        protected override bool Match(ConventionInfo c)
        {

            //Same name:
            //int src => enum target:
            string targetName = _prefix + c.SourceProp.Name + _suffix;
            return
                   c.TargetProp.Name == targetName &&
                   c.SourceProp.Type == typeof(int) && c.TargetProp.Type.IsSubclassOf(typeof(Enum));
        }
    }
}