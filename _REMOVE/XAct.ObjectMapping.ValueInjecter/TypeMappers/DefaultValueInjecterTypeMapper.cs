﻿namespace XAct.ObjectMapping
{
    using System;
    using Omu.ValueInjecter;
    using XAct.ObjectMapping.Conventions;

    /// <summary>
    /// 
    /// </summary>
    public class DefaultValueInjecterTypeMapper : ValueInjecterTypeMapperBase
    {

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="DefaultValueInjecterTypeMapper" /> class.
        /// </summary>
        public DefaultValueInjecterTypeMapper(Type sourceType, Type targetType)
            : base(sourceType, targetType)
        {

        }



        /// <summary>
        /// Map a source entity to a target entity
        /// </summary>
        /// <param name="source">The source to map</param>
        /// <param name="target">The target.</param>
        /// <returns>
        /// A existing (or new if target is null) instance of the target.
        /// </returns>
        /// <remarks>
        /// If you use a framework, use this method for setup or resolve mapping.
        /// <example>Automapper.Map{TSource,KTarget}</example>
        /// </remarks>
        protected override object InternalMap(object source, object target = null)
        {
            if (target == null)
            {
                target = Activator.CreateInstance(base.TargetType);
            }

            target.InjectFrom(source)
                .InjectFrom<NullablesToNormalConventionInjection>(source)
                .InjectFrom<NormalToNullablesConventionInjection>(source)
                .InjectFrom<IntToEnumConventionInjection>(source)
                .InjectFrom<EnumToIntConventionInjection>(source)
                .InjectFrom<MapperInjectionConventionInjection>(source);// apply mapper.map for Foo, Bar, IEnumerable<Foo> etc.

            return target;
        }
    }
}
