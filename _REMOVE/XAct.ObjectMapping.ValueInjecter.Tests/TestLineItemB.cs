namespace XAct.ObjectMapping.ValueInjecter.Tests
{
    public class TestLineItemB
    {
        public int Id { get; set; }
        public int TestInvoiceBId {get;set;}
        public TestInvoiceB Invoice {get;set;}
        public string Description { get; set; }
        public int Count { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal Amount { get { return Count*ItemAmount; } set {}}
    }
}