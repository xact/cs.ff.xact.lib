namespace XAct.ObjectMapping.ValueInjecter.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class TestInvoiceB
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public ICollection<TestLineItemB> LineItems { get; private set; }
        
        public TestInvoiceB()
        {
            LineItems = new Collection<TestLineItemB>();
        }
    }
}
