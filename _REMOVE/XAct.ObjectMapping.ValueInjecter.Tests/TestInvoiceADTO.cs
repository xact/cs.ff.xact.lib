namespace XAct.ObjectMapping.ValueInjecter.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class TestInvoiceADTO 
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }
        public ICollection<TestLineItemADTO> LineItems { get { return _LineItems; } private set { _LineItems = value; } }
        private ICollection<TestLineItemADTO> _LineItems = new Collection<TestLineItemADTO>();
    }
}