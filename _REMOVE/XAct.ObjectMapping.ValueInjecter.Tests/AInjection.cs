﻿
namespace XAct.ObjectMapping.ValueInjecter.Tests
{
    using Omu.ValueInjecter;

    public class AInjection : ConventionInjection 
    {
        protected override bool Match(ConventionInfo c)
        {
            if (c.Source.Type.IsEnumerable() && c.Target.Type.IsEnumerable())
            {
                return true;
            }
            return false;
        }
    }
}
