//namespace XAct.ObjectMapping.ValueInjecter.Tests
//{
//    using System;
//    using System.Linq;
//    using NUnit.Framework;
//    using Omu.ValueInjecter;
//    using XAct.Bootstrapper.Tests;
//    using XAct.ObjectMapping.Implementations;
//    using XAct.Tests;

//    /// <summary>
//    ///   NUNit Test Fixture.
//    /// </summary>
//    [TestFixture]
//    public class MappingServiceTests
//    {

//        protected IObjectMappingService MappingService
//        {
//            get
//            {
//                return XAct.DependencyResolver.Current.GetInstance<IObjectMappingService>();
//            }
//        }
//        /// <summary>
//        ///   Sets up to do before any tests 
//        ///   within this test fixture are run.
//        /// </summary>
//        [TestFixtureSetUp]
//        public void TestFixtureSetUp()
//        {
//            //run once before any tests in this testfixture have been run...
//            //...setup vars common to all the upcoming tests...

//                        Singleton<IocContext>.Instance.ResetIoC();

//        }

//        /// <summary>
//        ///   Tear down after all tests in this fixture.
//        /// </summary>
//        [TestFixtureTearDown]
//        public void TestFixtureTearDown()
//        {
            
//        }


//        [Test]
//        public void Can_Get_IObjectMappingServiceState()
//        {
//            var serviceState = XAct.DependencyResolver.Current.GetInstance<IObjectMappingServiceState>();
//            Assert.IsNotNull(serviceState);
//        }

//        [Test]
//        public void Can_Get_IValueInjecterConventionInjectionCache()
//        {
//            var serviceState = XAct.DependencyResolver.Current.GetInstance<IValueInjecterConventionInjectionCache>();
//            Assert.IsNotNull(serviceState);
            
//        }

//        [Test]
//        public void Cage_Get_IValueInjecterMappingService()
//        {
//            var service = XAct.DependencyResolver.Current.GetInstance<IValueInjecterMappingService>();

//            Assert.IsNotNull(service);
//        }

//        [Test]
//        public void Can_Get_IObjectMappingService()
//        {
//            var service = XAct.DependencyResolver.Current.GetInstance<IObjectMappingService>();
//            Assert.IsNotNull(service);
//        }

//        [Test]
//        public void Can_Get_IObjectMappingService_Of_ExpectedType()
//        {
//            var service = XAct.DependencyResolver.Current.GetInstance<IObjectMappingService>();
//            Assert.AreEqual(typeof(ValueInjecterMappingService),  service.GetType());
//        }




//        /// <summary>
//        ///   An Example Test.
//        /// </summary>
//        [Test]
//        public void UnitTest01_MapTestInvoiceA_To_TestinvoiceADTO_Done_Directly()
//        {
//            TestInvoiceA source = CreateTestInvoiceA();

//            TestInvoiceA target = new TestInvoiceA();
            
//            target.InjectFrom(source);

//            Assert.AreEqual(101,target.Id);
//            Assert.IsTrue(target.DateCreated != DateTime.MinValue);

//        }


//        [Test]
//        public void UnitTest01_MapTestInvoiceA_To_TestinvoiceADTO_Done_Using_Service()
//        {
//            TestInvoiceA source = CreateTestInvoiceA();

//            TestInvoiceA target = new TestInvoiceA();

//            this.MappingService.Map(source, target);

//            Assert.AreEqual(101, target.Id);
//            Assert.IsTrue(target.DateCreated != DateTime.MinValue);

//        }


//        [Test]
//        public void UnitTest01_MapTestInvoiceA_To_TestinvoiceADTO_Done_Directly_Including_ChildElements()
//        {
//            TestInvoiceA source = CreateTestInvoiceA();

//            TestInvoiceA target = new TestInvoiceA();

//            target.InjectFrom(source);

//            Assert.IsTrue(target.LineItems.Count > 0);

//        }


//        //[Test]
//        //public void UnitTest02B()
//        //{

//        //}

//        ///// <summary>
//        /////   An Example Test.
//        ///// </summary>
//        //[Test]
//        //public void UnitTest02()
//        //{
//        //    var invoice = CreateTestInvoiceB();

//        //    TestInvoiceBMapper x = new TestInvoiceBMapper();

//        //    TestInvoiceBDTO y = x.Map(invoice);

//        //    Assert.IsTrue(y.SomeLineItems.Count > 0);

//        //    TestInvoiceBMapper x2 = new TestInvoiceBMapper();

//        //    TestInvoiceBDTO y2 = x2.Map(invoice);

//        //    Assert.IsTrue(y2.SomeLineItems.Count > 0);



//        //}


//        //[Test]
//        //public void UnitTest04()
//        //{
//        //    MappingRegistrationService mappingRegistrationService = 
//        //        new MappingRegistrationService(new DefaultTracingService(new TraceSwitchService(new TraceSwitchCache(), new TraceSwitchServiceConfiguration())),new MappingRegistrationCache() );

            
//        //    MappingService mappingService 
//        //        = new MappingService(new DefaultTracingService(new TraceSwitchService(new TraceSwitchCache(), new TraceSwitchServiceConfiguration())), mappingRegistrationService);

//        //    mappingRegistrationService.RegisterMapper(new TestInvoiceAMapper());

//        //    //mappingRegistrationService.RegisterMapper(new TestInvoiceBMapper());

//        //    TestInvoiceA invoice = CreateTestInvoiceA();



//        //    TestInvoiceADTO dto = mappingService.Map<TestInvoiceA, TestInvoiceADTO>(invoice);

//        //    Assert.IsTrue(dto!=null);
//        //    Assert.IsTrue(dto.LineItems.Count>0);
//        //}


//        //[Test]
//        //public void UnitTest05()
//        //{
//        //    IMappingRegistrationService mappingRegistrationService =
//        //        DependencyResolver.Current.GetInstance<IMappingRegistrationService>();


//        //    IMappingService mappingService = DependencyResolver.Current.GetInstance<IMappingService>();

//        //    mappingRegistrationService.RegisterMapper(new TestInvoiceAMapper());
//        //    //mappingRegistrationService.RegisterMapper(new TestInvoiceBMapper());

//        //    TestInvoiceA invoice = CreateTestInvoiceA();


//        //    TestInvoiceADTO dto = mappingService.Map<TestInvoiceA, TestInvoiceADTO>(invoice);

//        //    Assert.IsTrue(dto != null);
//        //    Assert.IsTrue(dto.LineItems.Count > 0);
//        //}




//        private static TestInvoiceA CreateTestInvoiceA()
//        {
//            TestInvoiceA invoice = new TestInvoiceA();
//            invoice.Id = 101;
//            invoice.DateCreated = DateTime.Now;
//            invoice.LineItems.Add(new TestLineItemA { Id = 202, TestInvoiceAId = 101, Description = "Nails", Count = 30, ItemAmount = 0.15m });

//            invoice.LineItems.First().Invoice = invoice;

//            return invoice;
//        }
//        private static TestInvoiceB CreateTestInvoiceB()
//        {
//            TestInvoiceB invoice = new TestInvoiceB();

//            invoice.Id = 101;
//            invoice.DateCreated = DateTime.Now;
//            invoice.LineItems.Add(
//                new TestLineItemB { Id = 202, TestInvoiceBId = 101, Description = "Nails", Count = 30, ItemAmount = 0.15m });
//            invoice.LineItems.First().Invoice = invoice;
//            return invoice;
//        }

//    }


//}


