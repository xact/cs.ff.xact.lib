namespace XAct.ObjectMapping.ValueInjecter.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class TestInvoiceA
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public ICollection<TestLineItemA> LineItems { get; private set; }
        
        public TestInvoiceA()
        {
            LineItems = new Collection<TestLineItemA>();
        }
    }
}
