namespace XAct.ObjectMapping.ValueInjecter.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class TestInvoiceBDTO 
    {
        public int Id { get; private set; }
        public DateTime DateCreated { get; set; }
        public ICollection<TestLineItemBDTO> SomeLineItems { get { return _SomeLineItems; } private set { _SomeLineItems = value;}}
        private ICollection<TestLineItemBDTO> _SomeLineItems = new Collection<TestLineItemBDTO>();
    }
}