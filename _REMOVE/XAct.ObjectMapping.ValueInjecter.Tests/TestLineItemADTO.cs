namespace XAct.ObjectMapping.ValueInjecter.Tests
{
    public class TestLineItemADTO
    {
        public int Id { get; set; }
        public int TestInvoiceAId { get; set; }
        public TestInvoiceA Invoice { get; set; }
        public string Description { get; set; }
        public int Count { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal Amount { get;set;}
    }
}