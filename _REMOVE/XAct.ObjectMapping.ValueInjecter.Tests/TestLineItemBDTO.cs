namespace XAct.ObjectMapping.ValueInjecter.Tests
{
    public class TestLineItemBDTO
    {
        public int Id { get; private set; }
        public int TestInvoiceBId { get; private set; }
        public TestInvoiceB Invoice { get; set; }
        public string XDescription { get; set; }
        public int Count { get; set; }
        public decimal ItemBmount { get; set; }
        public decimal Amount { get;set;}
    }
}