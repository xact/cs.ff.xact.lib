//namespace XAct.Notifications
//{
//    using System;
//    using XAct.Messages;

//    /// <summary>
//    /// 
//    /// </summary>
//    public interface INotificationRepositoryService: IHasXActLibService
//    {

//        /// <summary>
//        /// Gets the single.
//        /// </summary>
//        /// <param name="id">The identifier.</param>
//        /// <returns></returns>
//        Notification GetSingle(Guid id);


//        /// <summary>
//        /// Gets the by filder.
//        /// </summary>
//        /// <param name="applicationIdentifier">The application identifier.</param>
//        /// <param name="applicationTennantId">The application tennant identifier.</param>
//        /// <param name="uniqueUserIdentifier">The unique user identifier.</param>
//        /// <param name="readState">State of the read.</param>
//        /// <param name="pagedQuerySpecification">The paged query specification.</param>
//        /// <returns></returns>
//        Notification[] GetByFilter(string applicationIdentifier, Guid applicationTennantId, string uniqueUserIdentifier, ReadState readState,
//                                   IPagedQuerySpecification pagedQuerySpecification);

//    }
//}