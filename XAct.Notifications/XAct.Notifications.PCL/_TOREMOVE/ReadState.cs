//namespace XAct.Notifications
//{
//    /// <summary>
//    /// Define the state of the Message (whether it has been read or not).
//    /// </summary>
//    public enum ReadState
//    {
//        /// <summary>
//        /// An error state.
//        /// <para>
//        /// Value = 0
//        /// </para>
//        /// </summary>
//        Undefined = 0,
//        /// <summary>
//        /// Retrieve messages that have been not been read.
//        /// <para>
//        /// Value = 1
//        /// </para>
//        /// </summary>
//        Unread = 1,
//        /// <summary>
//        /// Retrieve messages that have been read.
//        /// <para>
//        /// Value = 2
//        /// </para>
//        /// </summary>
//        Read = 2,
//        /// <summary>
//        /// Retrieve messages both read and unread.
//        /// <para>
//        /// Value = 3
//        /// </para>
//        /// </summary>
//        Both = 3
//    }
//}