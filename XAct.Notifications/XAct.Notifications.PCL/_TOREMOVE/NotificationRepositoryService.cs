//namespace XAct.Notifications
//{
//    using System;
//    using System.Linq;
//    using XAct.Domain.Repositories;
//    using XAct.Messages;
//    using XAct.Services;

//    /// <summary>
//    /// An XAct.Notifications.Persistence.EF class.
//    /// </summary>
//    public class NotificationRepositoryService : INotificationRepositoryService
//    {
//        private readonly IRepositoryService _repositoryService;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="NotificationRepositoryService"/> class.
//        /// </summary>
//        public NotificationRepositoryService(IRepositoryService repositoryService) 
//        {
//            _repositoryService = repositoryService;
//        }

//        /// <summary>
//        /// Gets the single.
//        /// </summary>
//        /// <param name="id">The identifier.</param>
//        /// <returns></returns>
//        public Notification GetSingle(Guid id)
//        {

//            return
//                _repositoryService.GetSingle<Notification>(
//                    n => n.Id == id);
//        }

//        /// <summary>
//        /// Gets the by filder.
//        /// </summary>
//        /// <param name="applicationIdentifier">The application identifier.</param>
//        /// <param name="applicationTennantId">The application tennant identifier.</param>
//        /// <param name="uniqueUserIdentifier">The unique user identifier.</param>
//        /// <param name="readState">State of the read.</param>
//        /// <param name="pagedQuerySpecification">The paged query specification.</param>
//        /// <returns></returns>
//        public Notification[] GetByFilter(string applicationIdentifier, Guid applicationTennantId, string uniqueUserIdentifier, ReadState readState, IPagedQuerySpecification pagedQuerySpecification)
//        {

//            return _repositoryService.GetByFilter<Notification>(
//                n => (
//                         (n.ApplicationTennantId == applicationTennantId) && 
//                         (n.UserIdentifier == uniqueUserIdentifier) &&
//                         (n.Read == (readState == ReadState.Read || readState == ReadState.Both) ? true : false)
//                     ),
//                null,
//                //Note we are using just the paging specification (no filtering, no sorting)
//                pagedQuerySpecification,
//                q => q.OrderByDescending(d => d.CreatedOnUtc)
//                ).ToArray();
//        }


//        /// <summary>
//        /// Determines whether the entity has ever been saved before.
//        /// <para>
//        /// This method is required, in order to be invoked by <c>PersistOnCommit</c>
//        /// </para>
//        /// </summary>
//        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
//        /// <returns>
//        /// <c>true</c> if [is entity new] [the specified aggregate root entity]; otherwise, <c>false</c>.
//        /// </returns>
//        public bool IsEntityNew(Notification aggregateRootEntity)
//        {
//            return  aggregateRootEntity.Id == Guid.Empty;
//        }
//    }
//}