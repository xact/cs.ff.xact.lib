// ReSharper disable CheckNamespace
namespace XAct.Notifications
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Typeof Notification
    /// </summary>
    public enum NotificationType
    {
        /// <summary>
        /// An Error state.
        /// <para>
        /// Value = 0
        /// </para>
        /// </summary>
        Undefined=0,
        
        ///// <summary>
        ///// 
        ///// <para>
        ///// Value = 1
        ///// </para>
        ///// </summary>
        //Welcome=1,
        
        
        ///// <summary>
        ///// 
        ///// <para>
        ///// Value = 2
        ///// </para>
        ///// </summary>
        //Tip=2,

        /// <summary>
        /// 
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        Info=3,

        /// <summary>
        /// An alert notification, of things that happened.
        /// <para>
        /// Value = 4
        /// </para>
        /// </summary>
        Alert=4,

        /// <summary>
        /// An alert notification, expecting an action to be taken in response.
        /// <para>
        /// Value = 5
        /// </para>
        /// </summary>
        AlertAction = 5,
    }
}