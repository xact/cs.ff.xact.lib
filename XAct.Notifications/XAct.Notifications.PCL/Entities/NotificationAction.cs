namespace XAct.Notifications
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Resources;

    /// <summary>
    /// An optional Action that can be attached to a <see cref="Notification"/> (that is Sticky).
    /// </summary>
    [DataContract]
    public class NotificationAction : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasResourceFilter, IHasText, IHasDescription, IHasAction, IHasNotificationFK
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the FK to the <see cref="Notification"/>
        /// to which this action is associated.
        /// </summary>
        /// <value>
        /// The notification fk.
        /// </value>
        [DataMember]
        public virtual Guid NotificationFK { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// Gets or sets a flag hinting to the UX system
        ///  whether the action is to displayed as a primary action.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is primary]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool IsPrimaryAction { get; set; }


        /// <summary>
        /// Gets or sets the Filter for the Resource in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        [DataMember]
        public virtual string ResourceFilter { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// <para>
        /// If ResourceFilter is not null, this is a ResourceKey for a resource available using <see cref="IResourceService"/>
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Text { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }

        /// <summary>
        /// A hint to the UX system as to what to do if the Action is selected.
        /// <para>
        /// In a MVC app this can be an url.
        /// In a SPA, this might the url to a WebAPI invocation - but the UX service will know what do with the WebAPI response.
        /// Or it could be a combination of Url + Hint as to a UX action to undertake after the WebAPI invocation.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string ActionHint { get; set; }


                /// <summary>
        /// Initializes a new instance of the <see cref="NotificationReadBy"/> class.
        /// </summary>
        public NotificationAction()
        {
            this.GenerateDistributedId();
        }

    }
}