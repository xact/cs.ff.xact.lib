namespace XAct.Notifications
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class NotificationRecipient :
        IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp,
        IHasUserIdentifier,
        IHasNotificationFK
    {
        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasId{TId}"/>.
        /// </para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal><para>8/9/2011: Sky</para></internal>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        /// <value>The user id.</value>
        [DataMember]
        public virtual Guid NotificationFK { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        /// <value>The user id.</value>
        [DataMember]
        public virtual string UserIdentifier { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationRecipient"/> class.
        /// </summary>
        public NotificationRecipient()
        {
            this.GenerateDistributedId();
        }
    }
}