namespace XAct.Notifications
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasAction 
    {
        /// <summary>
        /// A hint to the UX system as to what to do if the Action is selected.
        /// <para>
        /// In a MVC app this can be an url. 
        /// In a SPA, this might the url to a WebAPI invocation - but the UX service will know what do with the WebAPI response.
        /// Or it could be a combination of Url + Hint as to a UX action to undertake after the WebAPI invocation.
        /// </para>
        /// </summary>
        string ActionHint { get; set; }
    }
}