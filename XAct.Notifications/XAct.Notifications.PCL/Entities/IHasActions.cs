namespace XAct.Notifications
{
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public interface IHasActionsReadOnly
    {
        /// <summary>
        /// An optional list of actions.
        /// </summary>
        ICollection<NotificationAction> Actions { get; } 
    }
}