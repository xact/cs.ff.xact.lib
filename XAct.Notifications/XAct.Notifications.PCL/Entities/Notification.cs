namespace XAct.Notifications
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;


    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Notification :
        IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp,
        //IHasUserIdentifier, 
        IHasApplicationTennantId, 
        IHasSubjectAndBody, 
        IHasImportance,
        IHasRenderingImageHints,
        IHasDateTimeCreatedOnUtc,
        IHasActionsReadOnly,
        IHasNullableStartDateTimeEndDateTimeUtc
        
    {


        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasId{TId}"/>.
        /// </para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal><para>8/9/2011: Sky</para></internal>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }



        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        /// <value></value>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        /// </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }


        
        /// <summary>
        /// Gets or sets the type of the Notification message.
        /// </summary>
        /// <value>The type.</value>
        [DataMember]
        public virtual NotificationType Type { get; set; }

        /// <summary>
        /// Gets or sets the importance of the message.
        /// </summary>
        /// <value>The importance.</value>
        [DataMember]
        public virtual Importance Importance { get; set; }


        ///// <summary>
        ///// Gets or sets a value indicating whether this <see cref="Notification"/> has been read.
        ///// </summary>
        ///// <value><c>true</c> if read; otherwise, <c>false</c>.</value>
        //[DataMember]
        //public virtual bool IsRead { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Notification
        /// remains open after it is displayed for a default time (eg 5 seconds)
        /// </summary>
        [DataMember]
        public virtual bool IsSticky { get; set; }

        /// <summary>
        /// Gets or sets the start date time UTC.
        /// </summary>
        [DataMember]
        public virtual DateTime? StartDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets the end date time UTC.
        /// </summary>
        [DataMember]
        public virtual DateTime? EndDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets the message subject.
        /// </summary>
        /// <value>The subject.</value>
        [DataMember]
        public virtual string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body.
        /// </summary>
        /// <value>The body.</value>
        [DataMember]
        public virtual string Body { get; set; }




        /// <summary>
        /// Gets a hint as to the icons to render.
        /// </summary>
        [DataMember]
        public virtual string RenderingImageHints { get; set; }



        /// <summary>
        /// Gets the list of recipients this notification is intended for.
        /// </summary>
        [DataMember]
        /*NO:virtual*/
        public ICollection<NotificationRecipient>  Recipients
        {
            get { return _recipients ?? (_recipients = new Collection<NotificationRecipient>()); }
        }
        [DataMember]
        private ICollection<NotificationRecipient> _recipients;

        
        /// <summary>
        /// Gets the collection of users who have read this notification..
        /// </summary>
        /// <value>
        /// The read by.
        /// </value>
        [DataMember]
        /*NO:virtual*/
        public ICollection<NotificationReadBy>  ReadBy
        {
            get { return _readBy ?? (_readBy = new Collection<NotificationReadBy>()); }
        }
        [DataMember]
        private ICollection<NotificationReadBy> _readBy;

        /// <summary>
        /// An optional list of actions.
        /// </summary>
        public virtual ICollection<NotificationAction> Actions
        {
            get { return _actions??(_actions = new Collection<NotificationAction>()); }
        }
        [DataMember]
        private ICollection<NotificationAction> _actions;

        /// <summary>
        /// Initializes a new instance of the <see cref="Notification"/> class.
        /// </summary>
        public Notification()
        {
            this.GenerateDistributedId();
        }

    }
}