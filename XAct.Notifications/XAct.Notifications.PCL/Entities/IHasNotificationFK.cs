namespace XAct.Notifications
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface IHasNotificationFK
    {
        /// <summary>
        /// Gets or sets the FK to the <see cref="Notification"/>.
        /// </summary>
        Guid NotificationFK { get; set; }
    }
}