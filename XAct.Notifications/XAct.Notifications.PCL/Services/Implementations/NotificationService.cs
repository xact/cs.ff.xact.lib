namespace XAct.Notifications
{
    using System;
    using System.Linq;
    using XAct.Data;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="INotificationService"/>
    /// to show Notification messages to a user.
    /// </summary>
    public class NotificationService : INotificationService
    {
        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly IDateTimeService _dateTimeService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IPrincipalService _principalService;
        private readonly IRepositoryService _repositoryService;
        private readonly INotificationServiceConfiguration _notificationServiceConfiguration;


        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public INotificationServiceConfiguration Configuration
        {
            get { return _notificationServiceConfiguration; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="notificationServiceConfiguration">The notification service configuration.</param>
        public NotificationService(ITracingService tracingService, 
            IEnvironmentService environmentService, 
            IDateTimeService dateTimeService,
            IApplicationTennantService applicationTennantService,
            IPrincipalService principalService,
            IRepositoryService repositoryService,
            INotificationServiceConfiguration notificationServiceConfiguration
            )
        {
            _tracingService = tracingService;
            _environmentService = environmentService;
            _dateTimeService = dateTimeService;
            _applicationTennantService = applicationTennantService;
            _principalService = principalService;
            _repositoryService = repositoryService;
            _notificationServiceConfiguration = notificationServiceConfiguration;
        }

        /// <summary>
        /// Gets a singel notification message.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Notification GetNotification(Guid id)
        {
            var applicationTennantId = _applicationTennantService.Get();

            return _repositoryService.GetSingle<Notification>(x=>x.Id == id && x.ApplicationTennantId == applicationTennantId);
            
        }


        /// <summary>
        /// Gets a page of messages to display to the user.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        public Notification[] GetNotifications(PagedQuerySpecification pagedQuerySpecification)
        {
            if (pagedQuerySpecification == null)
            {
                pagedQuerySpecification = new PagedQuerySpecification(0, 20);
            }

            string uniqueUserIdentifier =
                _principalService.Principal.Identity.Name;


            var applicationTennantId = _applicationTennantService.Get();

            var nowUtc = _dateTimeService.NowUTC;

            string[] identifiers = new string[] { "*", uniqueUserIdentifier };

            Notification[] results =
                _repositoryService.GetByFilter<Notification>(
                    x => (
                             (x.ApplicationTennantId == applicationTennantId)
                             && (x.Recipients.Any(c => identifiers.Contains(c.UserIdentifier)))
                             && (x.ReadBy.All(c => c.UserIdentifier != uniqueUserIdentifier))
                             && (!x.StartDateTimeUtc.HasValue || x.StartDateTimeUtc.Value <= nowUtc)
                             && (!x.EndDateTimeUtc.HasValue || x.EndDateTimeUtc.Value >= nowUtc)
                         ),
                    null,
                    //Note we are using just the paging specification (no filtering, no sorting)
                    pagedQuerySpecification,
                    q => q.OrderByDescending(d => d.CreatedOnUtc)
                    ).ToArray();


            return results;
        }


        /// <summary>
        /// Marks the notification as un read.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="forAll">if set to <c>true</c> [for all].</param>
        public void MarkNotificationAsRead(Guid id, string userIdentifier = null, bool forAll = false)
        {
            if (userIdentifier == null)
            {
                userIdentifier = forAll ? "*" : _principalService.CurrentIdentityIdentifier;
            }
            _repositoryService.AddOnCommit(new NotificationReadBy{NotificationFK = id,UserIdentifier = userIdentifier});
        }

        /// <summary>
        /// Marks the notification as unread, so that it can be redisplayed.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="forAll">if set to <c>true</c> [for all].</param>
        public void MarkNotificationAsUnRead(Guid id, string userIdentifier = null, bool forAll = false)
        {
            if (userIdentifier == null)
            {
                userIdentifier = _principalService.CurrentIdentityIdentifier;
            }
            if (forAll)
            {
                _repositoryService.DeleteOnCommit<NotificationReadBy>(
                    x => x.NotificationFK == id);
            }
            else
            {
                _repositoryService.DeleteOnCommit<NotificationReadBy>(
                    x => x.NotificationFK == id && x.UserIdentifier == userIdentifier);
            }
        }

        /// <summary>
        /// Creates a new Notification message.
        /// <para>
        /// The <see cref="Notification.Recipients" /> array defines
        /// whom will receive the notification.
        /// <para>
        /// If no <see cref="Notification.Recipients" /> are defined,
        /// a single <see cref="NotificationRecipient" /> will be added
        /// to the collection. If the <paramref name="forAll"/> value is
        /// false, the value of the <see cref="NotificationRecipient"/>
        /// will be the current User, and if it is <c>true</c> it will be
        /// "*", making the <see cref="Notification" /> for all.
        /// </para>
        /// </para>
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <param name="forAll">if set to <c>true</c> [for all].</param>
        public void AddNotification(Notification notification, bool forAll = false)
        {

            if (notification.ApplicationTennantId.IsDefault())
            {
                notification.ApplicationTennantId = _applicationTennantService.Get();
            }

            notification.CreatedOnUtc = _dateTimeService.NowUTC;

            if (!notification.StartDateTimeUtc.HasValue)
            {
                notification.StartDateTimeUtc = _dateTimeService.NowUTC;
            }

            if (!notification.EndDateTimeUtc.HasValue)
            {
                notification.EndDateTimeUtc =
                    notification.StartDateTimeUtc.Value.Add(_notificationServiceConfiguration.DefaultDisplayTimeSpan);
            }


            if (notification.Recipients.Count == 0)
            {
                var notificationRecipient = new NotificationRecipient();

                var userIdentifier = forAll ? "*" : _principalService.CurrentIdentityIdentifier;

                notificationRecipient.UserIdentifier = userIdentifier;

                notification.Recipients.Add(notificationRecipient);
            }

            _repositoryService.AddOnCommit<Notification>(notification);

        }

        /// <summary>
        /// Adds the notification.
        /// </summary>
        /// <param name="isSticky">if set to <c>true</c> [is sticky].</param>
        /// <param name="text">The text.</param>
        /// <param name="description">The description.</param>
        /// <param name="importance">The importance.</param>
        /// <param name="startDateTimeUtc">The start date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="forAll">if set to <c>true</c> [for all].</param>
        public void AddNotification(bool isSticky, string text, string description = null, Importance importance = Importance.Normal, DateTime? startDateTimeUtc = null,
                                    DateTime? endDateTimeUtc = null, Guid? applicationTennantId = null,
                                    bool forAll = false)
        {

            if (!applicationTennantId.HasValue)
            {
                applicationTennantId = _applicationTennantService.Get();
            }


            Notification notification = new Notification
                {
                    ApplicationTennantId = applicationTennantId.Value,
                    IsSticky = isSticky,
                    Importance = importance,
                    Subject = text,
                    Body = description,
                    StartDateTimeUtc = startDateTimeUtc,
                    EndDateTimeUtc = endDateTimeUtc,
                };

            AddNotification(notification, forAll);

        
        }
    }
}