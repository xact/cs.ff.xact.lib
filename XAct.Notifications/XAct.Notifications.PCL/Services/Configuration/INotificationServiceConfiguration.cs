﻿namespace XAct.Notifications
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface INotificationServiceConfiguration: IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// The TimeSpan to show messages.
        /// </summary>
        TimeSpan DefaultDisplayTimeSpan { get; }

    }
}