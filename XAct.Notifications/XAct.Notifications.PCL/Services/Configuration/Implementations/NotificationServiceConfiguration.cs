﻿namespace XAct.Notifications.Services.Configuration.Implementations
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationServiceConfiguration : INotificationServiceConfiguration
    {

        /// <summary>
        /// The TimeSpan to show messages.
        /// </summary>
        public TimeSpan DefaultDisplayTimeSpan { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationServiceConfiguration"/> class.
        /// </summary>
        public NotificationServiceConfiguration()
        {
            DefaultDisplayTimeSpan = TimeSpan.FromSeconds(5);
        }
    }
}