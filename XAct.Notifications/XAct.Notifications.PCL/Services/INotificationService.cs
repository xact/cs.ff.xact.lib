﻿namespace XAct.Notifications
{
    using System;
    using XAct.Data;
    using XAct.Events;
    using XAct.Messages;


    /// <summary>
    /// Contract for a service to manage Notifications to a User.
    /// <para>
    /// * Event Notifications
    ///     * eg: VERBOSE: Email was sent.
    /// * Status Notifications
    ///     * eg: INFO: "Wifi is available"
    ///     * eg: WARN: "Cert is about to expire"
    /// There are several types of Notifications:
    /// * Notifications that are timely
    ///     * eg: INFO: "Appointment at 1.00pm (in 17 minutes)"
    /// * Notifications that are updatable
    ///     * eg: INFO: "Appointment at 1.00pm (in 16 minutes)"
    /// * Notifications per View
    ///     * eg: of importance to SPA development, when one Cancels out of a blocked form (form is still in mem, just blocked).
    /// </para>
    /// </summary>
    public interface INotificationService : IHasXActLibService
    {

        //See: IEventAggregatorService
        
        
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        INotificationServiceConfiguration Configuration { get; }

        
        
        

        /// <summary>
        /// Gets a singel notification message.
        /// <para>
        /// Tip: Use when a user clicks an entry in the list returned
        /// by <see cref="GetNotifications" />.
        /// </para>
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Notification GetNotification(Guid id );

        /// <summary>
        /// Gets a page of messages to display to the user.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        Notification[] GetNotifications(PagedQuerySpecification pagedQuerySpecification);



        /// <summary>
        /// Marks the given notification as read.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="forAll">if set to <c>true</c> [for all].</param>
        void MarkNotificationAsRead(Guid id, string userIdentifier = null, bool forAll = false);


        /// <summary>
        /// Marks the notification as unread, so that it can be redisplayed.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="forAll">if set to <c>true</c> [for all].</param>
        void MarkNotificationAsUnRead(Guid id, string userIdentifier = null, bool forAll=false);

        /// <summary>
        /// Creates a new Notification message.
        /// <para>
        /// The <see cref="Notification.Recipients" /> array defines
        /// whom will receive the notification.
        /// <para>
        /// If no <see cref="Notification.Recipients" /> are defined,
        /// a single <see cref="NotificationRecipient" /> will be added
        /// to the collection. If the <paramref name="forAll"/> value is
        /// false, the value of the <see cref="NotificationRecipient"/>
        /// will be the current User, and if it is <c>true</c> it will be
        /// "*", making the <see cref="Notification" /> for all.
        /// </para>
        /// </para>
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <param name="forAll">if set to <c>true</c> [for all].</param>
        void AddNotification(Notification notification, bool forAll=false);

        /// <summary>
        /// Adds the notification.
        /// </summary>
        /// <param name="isSticky">if set to <c>true</c> [is sticky].</param>
        /// <param name="text">The text.</param>
        /// <param name="description">The description.</param>
        /// <param name="importance">The importance.</param>
        /// <param name="startDateTimeUtc">The start date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="forAll">if set to <c>true</c> [for all].</param>
        void AddNotification(bool isSticky, string text, string description = null,
                             Importance importance = Importance.Normal, DateTime? startDateTimeUtc = null,
                             DateTime? endDateTimeUtc = null, Guid? applicationTennantId = null,
                             bool forAll = false);



    }
}
