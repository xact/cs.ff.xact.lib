namespace XAct.Notifications.Tests
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class NotificationServiceUnitTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_NotificationService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();
            Assert.IsNotNull(service);
        }


        [Test]
        public void Can_Get_NotificationService_Of_Expected_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();
            Assert.AreEqual(typeof (NotificationService), service.GetType());

        }

        [Test]
        public void Can_Get_Notification_By_Id()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var notification = service.GetNotification(1.ToGuid());

            Assert.IsNotNull(notification);

        }

        [Test]
        public void Can_Not_Get_Notification_By_Id_If_Different_ApplicationHostId()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var notification = service.GetNotification(2.ToGuid());

            Assert.IsNull(notification);

        }

        [Test]
        public void Can_Not_Get_Notification_If_Read()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();

            var hold = principalService.Principal;

            try
            {
                principalService.Principal = new ClaimsPrincipal(new GenericIdentity("ssigal"));

                var notification = service.GetNotification(3.ToGuid());

                Assert.IsNotNull(notification);
            }
            finally
            {
                principalService.Principal = hold;
            }
        }




        [Test]
        public void Can_Get_Notifications_For_All()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var notifications = service.GetNotifications(new PagedQuerySpecification());

            Assert.IsNotNull(notifications);

            //Should be returning Id = 1, skip 2, have 3 as current username is ""
            Assert.IsTrue(notifications.Length > 0);
            Assert.IsTrue(notifications.Contains(x=>x.Id==1.ToGuid()),"1 should be there");
            Assert.IsTrue(!notifications.Contains(x => x.Id == 2.ToGuid()),"2 should not");
            Assert.IsTrue(notifications.Contains(x => x.Id == 3.ToGuid()), "3 should not");
        }

        [Test]
        public void Can_Get_Notifications_For_All_2()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();

            var hold = principalService.Principal;

            try
            {

                principalService.Principal = new ClaimsPrincipal(new GenericIdentity("ssigal"));

                var notifications = service.GetNotifications(new PagedQuerySpecification());

            Assert.IsNotNull(notifications);

            //Should be returning Id = 1, skip 2, skip 3
            Assert.IsTrue(notifications.Length > 0);
            Assert.IsTrue(notifications.Contains(x => x.Id == 1.ToGuid()), "1 should be there");
            Assert.IsTrue(!notifications.Contains(x => x.Id == 2.ToGuid()), "2 should not");
            Assert.IsTrue(!notifications.Contains(x => x.Id == 3.ToGuid()), "3 should not");
            }
            finally
            {
                principalService.Principal = hold;
            }
        }

        [Test]
        public void Can_Get_Notifications_For_All_2_Using_EndDates()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();

            var hold = principalService.Principal;

            try
            {

                principalService.Principal = new ClaimsPrincipal(new GenericIdentity("ssigal"));

                var notifications = service.GetNotifications(new PagedQuerySpecification());

                Assert.IsNotNull(notifications);

                //Should be returning Id = 1, skip 2, skip 3, have 4, but skip 5
                Assert.IsTrue(notifications.Length > 0);
                Assert.IsTrue(notifications.Contains(x => x.Id == 1.ToGuid()), "1 should be there");
                Assert.IsTrue(!notifications.Contains(x => x.Id == 2.ToGuid()), "2 should not");
                Assert.IsTrue(!notifications.Contains(x => x.Id == 3.ToGuid()), "3 should not");
                Assert.IsTrue(notifications.Contains(x => x.Id == 4.ToGuid()), "4 should be there");
                Assert.IsTrue(!notifications.Contains(x => x.Id == 5.ToGuid()), "5 should not");
            }
            finally
            {
                principalService.Principal = hold;
            }
        }


        [Test]
        public void Can_Get_Notifications_For_All_2_Using_StartDate()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();

            var hold = principalService.Principal;

            try
            {

                principalService.Principal = new ClaimsPrincipal(new GenericIdentity("ssigal"));

                var notifications = service.GetNotifications(new PagedQuerySpecification());

                Assert.IsNotNull(notifications);

                //Should be returning Id = 1, skip 2, skip 3, have 4, but skip 5
                Assert.IsTrue(notifications.Length > 0);
                Assert.IsTrue(notifications.Contains(x => x.Id == 4.ToGuid()), "6 should be there");
                Assert.IsTrue(!notifications.Contains(x => x.Id == 5.ToGuid()), "7 should not");
            }
            finally
            {
                principalService.Principal = hold;
            }
        }

        [Test]
        public void Can_Get_Notifications_That_Are_Directed_To_A_UserA()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();

            var hold = principalService.Principal;

            try
            {

                principalService.Principal = new ClaimsPrincipal(new GenericIdentity("UserA"));

                var notifications = service.GetNotifications(new PagedQuerySpecification());

                Assert.IsNotNull(notifications);
                Assert.IsTrue(notifications.Contains(x => x.Id == 11.ToGuid()), "11 should be there");
                Assert.IsTrue(notifications.Contains(x => x.Id == 13.ToGuid()), "13 should be there");
                Assert.IsTrue(!notifications.Contains(x => x.Id == 12.ToGuid()), "11 should be there");
                Assert.IsTrue(!notifications.Contains(x => x.Id == 14.ToGuid()), "13 should be there");
            }
            finally
            {
                principalService.Principal = hold;
            }
        }

        [Test]
        public void Can_Get_Notifications_That_Are_Directed_To_A_UserB()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();

            var hold = principalService.Principal;

            try
            {

                principalService.Principal = new ClaimsPrincipal(new GenericIdentity("UserB"));

                var notifications = service.GetNotifications(new PagedQuerySpecification());

                Assert.IsNotNull(notifications);
                Assert.IsTrue(!notifications.Contains(x => x.Id == 11.ToGuid()), "11 should be there");
                Assert.IsTrue(!notifications.Contains(x => x.Id == 13.ToGuid()), "13 should be there");
                Assert.IsTrue(notifications.Contains(x => x.Id == 12.ToGuid()), "11 should be there");
                Assert.IsTrue(notifications.Contains(x => x.Id == 14.ToGuid()), "13 should be there");
            }
            finally
            {
                principalService.Principal = hold;
            }
        }



        [Test]
        public void Can_Save_A_New_Notification()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();
            var repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            var hold = principalService.Principal;

            try
            {

                var notification = new Notification { Subject = "Hi1", Body = "boo" };

                service.AddNotification(notification);

                repositoryService.GetContext().Commit();

            }
            finally
            {
                principalService.Principal = hold;
            }
        }


        [Test]
        public void Can_Save_A_New_Notification_And_Retrieve_It()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();
            var repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            var hold = principalService.Principal;

            try
            {
                var notification = new Notification {Subject = "Hi2", Body = "boo"};

                service.AddNotification(notification);

                repositoryService.GetContext().Commit();

                var notificationCopy = service.GetNotification(notification.Id);

                Assert.IsNotNull(notificationCopy);


            }
            finally
            {
                principalService.Principal = hold;
            }
        }



        [Test]
        public void Can_Save_A_New_Notification_And_Retrieve_It_In_List()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INotificationService>();

            var principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();
            var repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            var hold = principalService.Principal;

            try
            {
                var notification = new Notification { Subject = "Hi3", Body = "boo" };

                service.AddNotification(notification);

                repositoryService.GetContext().Commit();

                var notifications = service.GetNotifications(new PagedQuerySpecification());


                Assert.IsTrue(notifications.Length>0);
                Assert.IsTrue(notifications.Any(x=>x.Id == notification.Id));

            }
            finally
            {
                principalService.Principal = hold;
            }
        }




    }
}


