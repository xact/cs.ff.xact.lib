﻿namespace XAct.Notifications.Tests.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Notifications.Initialization.DbContextSeeders;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationRecipientDbContextSeeder : UnitTestXActLibDbContextSeederBase<NotificationRecipient>,
                                                        INotificationRecipientDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationRecipientDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="notificationDbContextSeeder">The notification database context seeder.</param>
        public NotificationRecipientDbContextSeeder(ITracingService tracingService,
                                                    INotificationDbContextSeeder notificationDbContextSeeder)
            : base(tracingService, notificationDbContextSeeder)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            this.SeedInternalHelper(dbContext,true,x=>x.Id);
        }

    

    public override void CreateEntities()
        {
            this.InternalEntities = new List<NotificationRecipient>();

            for (int i = 1; i < 8; i++)
            {
                this.InternalEntities.Add(new NotificationRecipient
                    {
                        Id = i.ToGuid(), 
                        NotificationFK = i.ToGuid(), 
                        UserIdentifier = "*"
                    });
            }

            this.InternalEntities.Add(new NotificationRecipient { Id = 11.ToGuid(), NotificationFK = 11.ToGuid(), UserIdentifier = "UserA"});
            this.InternalEntities.Add(new NotificationRecipient { Id = 12.ToGuid(), NotificationFK = 12.ToGuid(), UserIdentifier = "UserB" });
            this.InternalEntities.Add(new NotificationRecipient { Id = 13.ToGuid(), NotificationFK = 13.ToGuid(), UserIdentifier = "UserA" });
            this.InternalEntities.Add(new NotificationRecipient { Id = 14.ToGuid(), NotificationFK = 14.ToGuid(), UserIdentifier = "UserB" });

        }
    }
}