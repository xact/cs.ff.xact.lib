﻿namespace XAct.Notifications.Tests.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Notifications.Initialization.DbContextSeeders;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationActionDbContextSeeder : UnitTestXActLibDbContextSeederBase<NotificationAction>, INotificationActionDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationActionDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="notificationRecipientDbContextSeeder">The notification recipient database context seeder.</param>
        public NotificationActionDbContextSeeder(ITracingService tracingService, INotificationRecipientDbContextSeeder notificationRecipientDbContextSeeder)
            : base(tracingService, notificationRecipientDbContextSeeder)
        {
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}