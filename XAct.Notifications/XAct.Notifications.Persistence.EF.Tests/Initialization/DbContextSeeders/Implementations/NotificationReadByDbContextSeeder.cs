﻿namespace XAct.Notifications.Tests.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Notifications.Initialization.DbContextSeeders;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationReadByDbContextSeeder : UnitTestXActLibDbContextSeederBase<NotificationReadBy>, INotificationReadByDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationReadByDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="notificationRecipientDbContextSeeder">The notification database context seeder.</param>
        public NotificationReadByDbContextSeeder(ITracingService tracingService, INotificationRecipientDbContextSeeder notificationRecipientDbContextSeeder)
            : base(tracingService, notificationRecipientDbContextSeeder)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true, x=>x.Id);
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<NotificationReadBy>();
            //3 was read by me:
            this.InternalEntities.Add(new NotificationReadBy { Id = 1.ToGuid(), NotificationFK = 3.ToGuid(), UserIdentifier = "ssigal" });

        }
    }
}