﻿namespace XAct.Notifications.Tests.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Notifications;
    using XAct.Notifications.Initialization.DbContextSeeders;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationDbContextSeeder : UnitTestXActLibDbContextSeederBase<Notification>, INotificationDbContextSeeder
    {
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IDateTimeService _dateTimeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        public NotificationDbContextSeeder(ITracingService tracingService, IApplicationTennantService applicationTennantService, IDateTimeService dateTimeService)
            : base(tracingService)
        {
            _applicationTennantService = applicationTennantService;
            _dateTimeService = dateTimeService;
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true, x=>x.Id);
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {

            this.InternalEntities = new List<Notification>();

            var applicationTennantId = _applicationTennantService.Get();

            //var dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            //This apptennat, read by noone
            this.InternalEntities.Add(new Notification {Id = 1.ToGuid(), Subject = "Sub1", Body = "Body1",ApplicationTennantId = applicationTennantId ,CreatedOnUtc = _dateTimeService.NowUTC});

            //Different apptennat
            this.InternalEntities.Add(new Notification { Id = 2.ToGuid(), Subject = "Sub2", Body = "Body", ApplicationTennantId = 2.ToGuid(), CreatedOnUtc = _dateTimeService.NowUTC });

            //THis apptennat, but read by ssigal
            this.InternalEntities.Add(new Notification { Id = 3.ToGuid(), Subject = "Sub3", Body = "Body", ApplicationTennantId = applicationTennantId, CreatedOnUtc = _dateTimeService.NowUTC });


            //This apptennat, read by noone, closing in the future:
            this.InternalEntities.Add(new Notification { Id = 4.ToGuid(), Subject = "Sub1", Body = "Body1", ApplicationTennantId = applicationTennantId, EndDateTimeUtc = _dateTimeService.NowUTC.AddDays(1), CreatedOnUtc = _dateTimeService.NowUTC });

            //This apptennat, read by noone, closing in the past:
            this.InternalEntities.Add(new Notification { Id = 5.ToGuid(), Subject = "Sub1", Body = "Body1", ApplicationTennantId = applicationTennantId, EndDateTimeUtc = _dateTimeService.NowUTC.AddDays(-1), CreatedOnUtc = _dateTimeService.NowUTC });

            //This apptennat, read by noone, closing in the future:
            this.InternalEntities.Add(new Notification { Id = 6.ToGuid(), Subject = "Sub1", Body = "Body1", ApplicationTennantId = applicationTennantId, EndDateTimeUtc = _dateTimeService.NowUTC.AddDays(1), StartDateTimeUtc = _dateTimeService.NowUTC.AddHours(1), CreatedOnUtc = _dateTimeService.NowUTC });

            //This apptennat, read by noone, closing in the future:
            this.InternalEntities.Add(new Notification { Id = 7.ToGuid(), Subject = "Sub1", Body = "Body1", ApplicationTennantId = applicationTennantId, EndDateTimeUtc = _dateTimeService.NowUTC.AddDays(1), StartDateTimeUtc = _dateTimeService.NowUTC.AddHours(1), CreatedOnUtc = _dateTimeService.NowUTC });



            //Now for some directed ones:
            this.InternalEntities.Add(new Notification { Id = 11.ToGuid(), Subject = "Sub11", Body = "Body1", ApplicationTennantId = applicationTennantId, CreatedOnUtc = _dateTimeService.NowUTC });
            this.InternalEntities.Add(new Notification { Id = 12.ToGuid(), Subject = "Sub12", Body = "Body1", ApplicationTennantId = applicationTennantId, CreatedOnUtc = _dateTimeService.NowUTC });
            this.InternalEntities.Add(new Notification { Id = 13.ToGuid(), Subject = "Sub13", Body = "Body1", ApplicationTennantId = applicationTennantId, CreatedOnUtc = _dateTimeService.NowUTC });
            this.InternalEntities.Add(new Notification { Id = 14.ToGuid(), Subject = "Sub14", Body = "Body1", ApplicationTennantId = applicationTennantId, CreatedOnUtc = _dateTimeService.NowUTC });



        }
    }
}
