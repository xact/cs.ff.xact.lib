namespace XAct.Notifications.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Notifications;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationModelPersistenceMap : EntityTypeConfiguration<Notification>,
                                                   INotificationModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationModelPersistenceMap"/> class.
        /// </summary>
        public NotificationModelPersistenceMap()
        {

            this.ToXActLibTable("Notification");


            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;


            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);


            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this
                .Property(m => m.ApplicationTennantId)
                .DefineRequiredApplicationTennantId(colOrder++);

            //this
            //    .Property(m => m.UserIdentifier)
            //    .IsOptional()
            //    .HasColumnOrder(colOrder++)
            //    ;

            this
                .Property(m => m.Type)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Importance)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.IsSticky)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            //this
            //    .Property(m => m.IsRead)
            //    .IsRequired()
            //    .HasColumnOrder(colOrder++)
            //    ;

            this
                .Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);

            this
                .Property(m => m.StartDateTimeUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.EndDateTimeUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;


            this
                .Property(m => m.RenderingImageHints)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            
            this
                .Property(m => m.Subject)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Body)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;




//Relationships:
            this.HasMany(x => x.Recipients)
                .WithRequired()
                .HasForeignKey(x => x.NotificationFK)
                ;

            this.HasMany(x => x.ReadBy)
                .WithOptional()
                .HasForeignKey(x => x.NotificationFK)
                ;

            this.HasMany(x => x.Actions)
                .WithOptional()
                .HasForeignKey(x => x.NotificationFK)
                ;

        }

    }
}