namespace XAct.Notifications.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Notifications;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationReadByModelPersistenceMap : EntityTypeConfiguration<NotificationReadBy>, INotificationReadByModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationModelPersistenceMap"/> class.
        /// </summary>
        public NotificationReadByModelPersistenceMap()
        {

            this.ToXActLibTable("NotificationReadBy");


            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;


            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this
                .Property(m => m.NotificationFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.UserIdentifier)
                .DefineRequired64CharUserIdentifier(colOrder++);
        }
    }
}