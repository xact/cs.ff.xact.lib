namespace XAct.Notifications.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Notifications;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationActionModelPersistenceMap : EntityTypeConfiguration<NotificationAction>,
                                                         INotificationActionModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationModelPersistenceMap"/> class.
        /// </summary>
        public NotificationActionModelPersistenceMap()
        {

            this.ToXActLibTable("NotificationAction");

            

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;


            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this
                .Property(m => m.NotificationFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.ResourceFilter)
                .DefineOptional256CharResourceFilter(colOrder++)
                ;
            
            this
                .Property(m => m.Text)
                .DefineOptional1024CharText(colOrder++)
                ;

            this
                .Property(m => m.ActionHint)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;
        }
    }
}