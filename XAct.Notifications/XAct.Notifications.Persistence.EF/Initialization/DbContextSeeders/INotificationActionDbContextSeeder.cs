namespace XAct.Notifications.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Notifications;

    /// <summary>
    /// 
    /// </summary>
    public interface INotificationActionDbContextSeeder : IHasXActLibDbContextSeeder<NotificationAction>
    {


    }
}