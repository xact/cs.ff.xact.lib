namespace XAct.Notifications.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Notifications;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Notification}" />
    /// to seed the Resource tables with default data.
    /// </summary>
    public interface INotificationDbContextSeeder : IHasXActLibDbContextSeeder<Notification>
    {


    }
}