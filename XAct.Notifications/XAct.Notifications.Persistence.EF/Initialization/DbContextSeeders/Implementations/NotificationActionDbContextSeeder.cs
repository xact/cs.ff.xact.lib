namespace XAct.Notifications.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Notifications;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationActionDbContextSeeder : XActLibDbContextSeederBase<NotificationAction>,
                                                     INotificationActionDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationActionDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="notificationReadByDbContextSeeder">The notification read by database context seeder.</param>
        public NotificationActionDbContextSeeder(ITracingService tracingService,
                                                 INotificationReadByDbContextSeeder notificationReadByDbContextSeeder)
            : base(tracingService, notificationReadByDbContextSeeder)
        {
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}