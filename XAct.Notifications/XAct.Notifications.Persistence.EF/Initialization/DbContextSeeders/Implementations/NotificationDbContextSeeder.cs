namespace XAct.Notifications.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Notifications;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationDbContextSeeder : XActLibDbContextSeederBase<Notification>, INotificationDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public NotificationDbContextSeeder(ITracingService tracingService) : base(tracingService)
        {
        }



        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }

    }
}