namespace XAct.Notifications.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Notifications;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationReadByDbContextSeeder : XActLibDbContextSeederBase<NotificationReadBy>, INotificationReadByDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationReadByDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="notificationRecipientDbContextSeeder">The notification recipient database context seeder.</param>
        public NotificationReadByDbContextSeeder(ITracingService tracingService, INotificationRecipientDbContextSeeder notificationRecipientDbContextSeeder)
            : base(tracingService, notificationRecipientDbContextSeeder)
        {
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}