namespace XAct.Notifications.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Notifications;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationRecipientDbContextSeeder : XActLibDbContextSeederBase<NotificationRecipient>,
                                                        INotificationRecipientDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationReadByDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="notificationDbContextSeeder">The notification database context seeder.</param>
        public NotificationRecipientDbContextSeeder(ITracingService tracingService,
                                                    INotificationDbContextSeeder notificationDbContextSeeder)
            : base(tracingService, notificationDbContextSeeder)
        {
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }

    }
}