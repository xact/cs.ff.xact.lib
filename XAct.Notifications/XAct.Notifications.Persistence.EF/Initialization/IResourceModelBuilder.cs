namespace XAct.Notifications.Initialization
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// Contract for the <see cref="IHasXActLibDbModelBuilder"/>
    /// specific to setting up XActLib Resources capabilities.
    /// </summary>
    public interface INotificationsDbModelBuilder : IHasXActLibDbModelBuilder
    {

    }
}