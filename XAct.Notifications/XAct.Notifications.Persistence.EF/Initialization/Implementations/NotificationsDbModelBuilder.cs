namespace XAct.Notifications.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Notifications;
    using XAct.Notifications.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// 
    /// </summary>
    public class NotificationsDbModelBuilder : INotificationsDbModelBuilder
    {

        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationsDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public NotificationsDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;
            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add(
                (EntityTypeConfiguration<Notification>)
                XAct.DependencyResolver.Current.GetInstance<INotificationModelPersistenceMap>());


            modelBuilder.Configurations.Add(
                (EntityTypeConfiguration<NotificationRecipient>)
                XAct.DependencyResolver.Current.GetInstance<INotificationRecipientModelPersistenceMap>());

            modelBuilder.Configurations.Add(
                (EntityTypeConfiguration<NotificationReadBy>)
                XAct.DependencyResolver.Current.GetInstance<INotificationReadByModelPersistenceMap>());

            modelBuilder.Configurations.Add(
                (EntityTypeConfiguration<NotificationAction>)
                XAct.DependencyResolver.Current.GetInstance<INotificationActionModelPersistenceMap>());


        }

    }
}