namespace XAct.Identity
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Messages;

    /// <summary>
    /// Gets the authority backing the claim of identity of the entity.
    /// </summary>
    [DataContract]
    public class IdentityClaimAuthority : ReferenceDataBase<Guid>
    {

    }
}