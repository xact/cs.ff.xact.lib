﻿namespace XAct.Identity.Entities.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum SuffixTitle
    {
        /// <summary>
        /// The undefined
        /// </summary>
        Undefined=0,
        /// <summary>
        /// The custom
        /// </summary>
        Custom=1,
        /// <summary>
        /// The esq
        /// </summary>
        Esq=2,
    }
}