﻿namespace XAct.Identity.Entities.Enums
{
    /// <summary>
    /// Enumeration of common name prefixes
    /// </summary>
    public enum PrefixTitle
    {
        /// <summary>
        /// The undefined
        /// </summary>
        Undefined=0,
        /// <summary>
        /// The custom
        /// </summary>
        Custom = 1,
        /// <summary>
        /// The mr
        /// </summary>
        Mr = 2,
        /// <summary>
        /// The MRS
        /// </summary>
        Mrs=3,
        /// <summary>
        /// The miss
        /// </summary>
        Miss=4,
        /// <summary>
        /// The dr
        /// </summary>
        Dr=5,
        /// <summary>
        /// The rev
        /// </summary>
        Rev=6
    }
}