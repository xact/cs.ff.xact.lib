﻿

//namespace XAct.Identity.Entities.Enums
//{
//    using XAct.Identity.Entities.Location.Postal;

//    /// <summary>
//    /// Enumeration of common <see cref="FlexiblePostalAddressItem"/>
//    /// elements.
//    /// </summary>
//    public enum AddressElementType
//    {
//        /// <summary>
//        /// The undefined
//        /// </summary>
//        Undefined=0,
//        /// <summary>
//        /// The room
//        /// </summary>
//        Room=1,
//        /// <summary>
//        /// The apt
//        /// </summary>
//        Apt=2,
//        /// <summary>
//        /// The building
//        /// </summary>
//        Building=3,
//        /// <summary>
//        /// The street
//        /// </summary>
//        Street=4,
//        /// <summary>
//        /// The street2
//        /// </summary>
//        Street2=5,
//        /// <summary>
//        /// The street3
//        /// </summary>
//        Street3=6,
//        /// <summary>
//        /// The village
//        /// </summary>
//        Village=8,
//        /// <summary>
//        /// The town
//        /// </summary>
//        Town=9,
//        /// <summary>
//        /// The city
//        /// </summary>
//        City=10,
//        /// <summary>
//        /// The district
//        /// </summary>
//        District=11,
//        /// <summary>
//        /// The region
//        /// </summary>
//        Region=12,
//        /// <summary>
//        /// The po code
//        /// </summary>
//        POCode=13,
//        /// <summary>
//        /// The country
//        /// </summary>
//        Country=14,
//        /// <summary>
//        /// The custom
//        /// </summary>
//        Custom=15
//    }
//}
