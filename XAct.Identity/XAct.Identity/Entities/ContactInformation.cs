namespace XAct.Identity
{
    /// <summary>
    /// 
    /// </summary>
    public class ContactInformation: IHasId<int>
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

    }
}