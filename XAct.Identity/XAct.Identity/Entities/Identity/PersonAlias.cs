namespace XAct.Identity
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class PersonAlias : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasNames, IHasPreferred 
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// A flag indicating whether the alias is the preferred 
        /// means of identifying the user.
        /// </summary>
        [DataMember]
        public virtual bool Preferred { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DataMember]
        public virtual string Title { get; set; }


        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [DataMember]
        public virtual string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the second, third, fourth FirstNames.
        /// </summary>
        [DataMember]
        public virtual string MoreNames { get; set; }

        
        /// <summary>
        /// Gets or sets the name of the person's surname.
        /// </summary>
        [DataMember]
        public virtual string SurName { get; set; }

    }
}