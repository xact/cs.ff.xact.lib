namespace XAct.Identity.Entities.Location.Postal
{
    /// <summary>
    /// Contract for an Address, which has a POCode.
    /// <para>Probably no longer relevant.</para>
    /// </summary>
    public interface IHasPOCode
    {
        /// <summary>
        /// Gets or sets the PO code (Zip in the US, etc.).
        /// </summary>
        /// <value>
        /// The po code.
        /// </value>
        string POCode { get; set; }
    }
}