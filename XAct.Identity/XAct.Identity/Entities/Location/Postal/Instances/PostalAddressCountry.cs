//namespace XAct.Identity.Entities.Location.Postal
//{
//    using System;
//    using System.Runtime.Serialization;
//    using XAct.Messages;

//    /// <summary>
//    /// An enumeration of countries.
//    /// </summary>
//    /// <para>
//    /// Note that in some scenarios, such as embargo's, one can disable countries.
//    /// </para>
//    [DataContract]
//    public class PostalAddressCountry : ReferenceDataBase<Guid>
//    {


//        /// <summary>
//        /// Initializes a new instance of the <see cref="PostalAddressCountry"/> class.
//        /// </summary>
//        public PostalAddressCountry()
//        {
//            this.GenerateDistributedId();
//        }
//    }
//}