//namespace XAct.Identity.Entities.Location.Postal
//{
//    using System.Runtime.Serialization;
//    using XAct.Messages;

//    /// <summary>
//    /// A Postal Address has n PostalAddressItems. 
//    /// Each PostalAddressItem has a Type (Street,Street2,City,Country)
//    /// </summary>
//    [DataContract]
//    public class FlexiblePostalAddressItemType : ReferenceDataBase<Entities.Enums.AddressElementType>
//    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="FlexiblePostalAddressItemType"/> class.
//        /// </summary>
//        public FlexiblePostalAddressItemType()
//        {
//        }
//    }
//}