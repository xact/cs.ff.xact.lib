//namespace XAct.Identity.Entities.Location.Postal
//{
//    using System;
//    using System.Runtime.Serialization;
//    using XAct.Identity.Entities.Enums;

//    /// <summary>
//    /// A single line within an address. 
//    /// Room, Apt, Building, Street, Street2, Street3, Village, Town, City, District, Region, POCode, Country, ...?
//    /// </summary>
//    [DataContract]
//    public class FlexiblePostalAddressItem : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
//    {

//        /// <summary>
//        /// Gets or sets the identifier.
//        /// </summary>
//        /// <value>
//        /// The identifier.
//        /// </value>
//        [DataMember]
//        public virtual Guid Id { get; set; }

//        /// <summary>
//        /// Gets or sets the datastore concurrency check timestamp.
//        /// <para>
//        /// Note that this is filled in when persisted in the db -- 
//        /// so it's usable to determine whether to generate the 
//        /// Guid <c>Id</c>.
//        ///  </para>
//        /// </summary>
//        [DataMember]
//        public virtual byte[] Timestamp { get; set; }

//        /// <summary>
//        /// The order in which to display the line items
//        /// to make a coherent Addres.
//        /// </summary>
//        [DataMember]
//        public virtual int Order { get; set; }
        
//        /// <summary>
//        /// The Type of the Address line item
//        /// (Room, Apt, Building, Street, Street2, Street3, Village, Town, City, District, Region, POCode, Country, ...)
//        /// </summary>
//        [DataMember]
//        public virtual int AddressLineItemTypeFK { get; set; }

//        /// <summary>
//        /// The Type of the Address line item
//        /// (Room, Apt, Building, Street, Street2, Street3, Village, Town, City, District, Region, POCode, Country, ...)
//        /// </summary>
//        [DataMember]
//        public virtual FlexiblePostalAddressItemType AddressLineItemType { get; set; }

//        /// <summary>
//        /// The custom title to use if the AddressLineItemType is set to Custom.
//        /// </summary>
//        [DataMember]
//        public virtual string CustomKey { get; set; }


//        /// <summary>
//        /// The string value of the line.
//        /// </summary>
//        [DataMember]
//        public virtual string Value { get; set; }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="FlexiblePostalAddressItem"/> class.
//        /// </summary>
//        public FlexiblePostalAddressItem()
//        {
//            this.GenerateDistributedId();
//        }
//    }
//}