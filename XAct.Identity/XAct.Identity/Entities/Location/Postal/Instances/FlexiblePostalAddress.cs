//namespace XAct.Identity.Entities.Location.Postal
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Collections.ObjectModel;
//    using System.Runtime.Serialization;

//    /// <summary>
//    /// <para>
//    /// The basis of an Order's Shipping or Billing address.
//    /// </para>
//    /// <para>
//    /// IMPORTANT:
//    /// </para>
//    /// <para>
//    /// Whereas a Postal Address is *mostly* the same from one country 
//    /// to another, one cannot very well say that PostalAddress always
//    /// has Zip, State, Country. 
//    /// </para>
//    /// <para>
//    /// To allow for flexibility in the number and types of rows, this
//    /// is a collection of *ordered* <see cref="FlexiblePostalAddressItem"/>s.
//    /// that make up an Address.
//    /// </para>
//    /// <para>
//    /// Note that PostalCountry is not connected by FK, but used as a lookup to populat the UI.
//    /// The value is stored as a string, in order to allow users to edit it as they please
//    /// (maybe preferring to use 'Espana' rather than 'Spain').
//    /// </para>
//    /// </summary>
//    [DataContract]
//    public class FlexiblePostalAddress : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
//    {

//        /// <summary>
//        /// Gets or sets the identifier.
//        /// </summary>
//        /// <value>
//        /// The identifier.
//        /// </value>
//        [DataMember]
//        public virtual Guid Id { get; set; }

//        /// <summary>
//        /// Gets or sets the datastore concurrency check timestamp.
//        /// <para>
//        /// Note that this is filled in when persisted in the db -- 
//        /// so it's usable to determine whether to generate the 
//        /// Guid <c>Id</c>.
//        ///  </para>
//        /// </summary>
//        [DataMember]
//        public virtual byte[] Timestamp { get; set; }

//        /// <summary>
//        /// Gets or sets the date the address is valid from.
//        /// (people move -- no use using old addresses in lists
//        /// of possible shipping/billing addresses).
//        /// </summary>
//        /// <value>
//        /// The valid from.
//        /// </value>
//        [DataMember]
//        public virtual DateTime? ValidFrom { get; set; }

//        /// <summary>
//        /// Gets or sets the date the address is valid till
//        /// (people move -- no use using old addresses in lists
//        /// of possible shipping/billing addresses).
//        /// </summary>
//        /// <value>
//        /// The valid from.
//        /// </value>
//        [DataMember]
//        public virtual DateTime? ValidTill { get; set; }

//        ///// <summary>
//        ///// ForeignKey to Type of Address (Billing,Shipping,etc.)
//        ///// </summary>
//        //    /// <internal>
//        //    /// I'm not convinced this is adds any value.
//        //    /// An address's use depends on the consumer,
//        //    /// not the address itself.
//        //    /// </internal>
//        //public virtual AddressType AddressTypeFK { get; set; }
//        //    /// <internal>
//        //    /// I'm not convinced this is adds any value.
//        //    /// An address's use depends on the consumer,
//        //    /// not the address itself.
//        //    /// </internal>
//        //public virtual AddressTypeRecord AddressType { get; set; }


//        /// <summary>
//        /// Collection of (ordered) lines within the Address
//        /// (street,street2,village,city,POCode, etc.)
//        /// </summary>
//        public virtual ICollection<FlexiblePostalAddressItem> AddressLines
//        {
//            get
//            {
//                return _addressLines ?? (_addressLines = new Collection<FlexiblePostalAddressItem>());
//            }
//        }
//        [DataMember]
//        private virtual ICollection<FlexiblePostalAddressItem> _addressLines;


//        /// <summary>
//        /// Postals the address element record.
//        /// </summary>
//        public FlexiblePostalAddress()
//        {
//            this.GenerateDistributedId();
//        }


//    }
//}