﻿
namespace XAct.Identity.Entities.Location.Postal
{

    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A fixed postal address.
    /// </summary>
    [DataContract]
    public class PostalAddress : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
    {


        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// Gets or sets the name to put on the address.
        /// <para>
        /// Important: the name used on an address does not
        /// automatically match with the name of the owner of an address.
        /// An example would be, for a corporation, the Name on the address 
        /// could be Reception (whereas the name of the company would be something 
        /// like XYZ Inc.)
        /// </para>
        /// <para>
        /// Another common case where the address name may be different is on a Billing Address
        /// where the name is used for Verification purposes (eg: J SMITH), whereas the full
        /// Customer's full name is John Wilbert Smith.
        /// </para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public virtual string Name { get; set; }



        /// <summary>
        /// Gets or sets the email address associated to this Address.
        /// <para>
        /// In the case of a Billing Address, the Email is used for Verification, 
        /// and in the case of a Shipping Address, the email is used to alert that
        /// physical delivery is imminent, attempted, failed.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Email { get; set; }


        /// <summary>
        /// Gets or sets the phone number associated to this Address.
        /// <para>
        /// In the case of a Billing Address, the number is used for Verification, 
        /// and in the case of a Shipping Address, the number is used to alert that
        /// physical delivery is imminent, attempted, failed.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Phone { get; set; }


        /// <summary>
        /// Gets or sets the phone number associated to this Address.
        /// <para>
        /// In the case of a Billing Address, the number is used for Verification, 
        /// and in the case of a Shipping Address, the number is used to alert that
        /// physical delivery is imminent, attempted, failed.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string PhoneExt { get; set; }


        /// <summary>
        /// Gets or sets the phone number associated to this Address.
        /// <para>
        /// In the case of a Billing Address, this is ignored.
        /// </para>
        /// <para>
        /// In the case of a Shipping Address, this can be used to
        /// provide additional information ('Leave on Doorstep', etc.).
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string DeliveryInstructions { get; set; }

        /// <summary>
        /// Gets or sets the mandatory street address.
        /// <para>
        /// Can also be the specs for a PO Box.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string StreetLine1 { get; set; }

        /// <summary>
        /// Gets or sets optional additional street information.
        /// </summary>
        [DataMember]
        public virtual string StreetLine2 { get; set; }

        /// <summary>
        /// Gets or sets optional information about neighbourhood
        /// (eg: 'Soho').
        /// </summary>
        [DataMember]
        public virtual string Neighbourhood { get; set; }

        /// <summary>
        /// Gets or sets the name of the village, town, or city.
        /// </summary>
        [DataMember]
        public virtual string City { get; set; }

        /// <summary>
        /// Gets or sets the optional name of the region ('State' in the US).
        /// <para>
        /// eg: North Island.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Region { get; set; }

        /// <summary>
        /// Gets or sets the postal code (Zip in the US).
        /// </summary>
        [DataMember]
        public virtual string PostalCode { get; set; }


        /// <summary>
        /// Gets or sets the name of the Country.
        /// </summary>
        [DataMember]
        public virtual string Country { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="PostalAddress"/> class.
        /// </summary>
        public PostalAddress()
        {
            this.GenerateDistributedId();
        }

    }
}
