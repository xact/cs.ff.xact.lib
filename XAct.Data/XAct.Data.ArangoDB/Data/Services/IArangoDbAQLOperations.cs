namespace XAct.Data.Services
{
    using System.Collections.Generic;
    using Arango.Client;

    /// <summary>
    /// Contract for the operations that use AQL to query the database.
    /// </summary>
    public interface IArangoDbAQLOperations
    {
        /// <summary>
        /// Peforms an AQL Query and returns the result list.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        List<Document> QueryList(string statement, params KeyValue<string, object>[] paramenters);

        /// <summary>
        /// Peforms an AQL Query and returns the result list.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        List<TModel> QueryList<TModel>(string statement, params KeyValue<string, object>[] paramenters)
            where TModel : class, new();

        /// <summary>
        /// Peforms an AQL Query and returns a s ingle document.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        Document QuerySingle(string statement, params KeyValue<string, object>[] paramenters);

        /// <summary>
        /// Peforms an AQL Query and returns a s ingle document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        TModel QuerySingle<TModel>(string statement, params KeyValue<string, object>[] paramenters)
            where TModel : class, new();

    }
}