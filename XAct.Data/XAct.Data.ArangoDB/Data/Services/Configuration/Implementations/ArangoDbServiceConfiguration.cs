namespace XAct.Data.Services.Configuration.Implementations
{
    using System.Collections.Generic;

    /// <summary>
    /// An implementation of the <see cref="IArangoDbServiceConfiguration"/>
    /// </summary>
    public class ArangoDbServiceConfiguration : IArangoDbServiceConfiguration
    {
        /// <summary>
        /// Gets information required to successfully ping a connection's collection.
        /// </summary>
        public ArangoDbPingConfigurationInfo PingInfo
        {
            get { return _pingInfo; }
            private set { _pingInfo = value; }
        }
        private ArangoDbPingConfigurationInfo _pingInfo = new ArangoDbPingConfigurationInfo();

        /// <summary>
        /// Gets the connection infos.
        /// </summary>
        public List<ArangoDbConnectionInfo> ConnectionInfos { get { return _connectionInfos; }  }
        private readonly List<ArangoDbConnectionInfo> _connectionInfos = new List<ArangoDbConnectionInfo>();
    }
}