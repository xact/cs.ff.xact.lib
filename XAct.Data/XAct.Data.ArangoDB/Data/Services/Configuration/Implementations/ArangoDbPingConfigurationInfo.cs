namespace XAct.Data.Services.Configuration.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public class ArangoDbPingConfigurationInfo
    {
        /// <summary>
        /// Gets or sets the database alias.
        /// </summary>
        /// <value>
        /// The database alias.
        /// </value>
        public string DatabaseAlias { get; set; }

        /// <summary>
        /// Gets or sets the name of the collection.
        /// </summary>
        /// <value>
        /// The name of the collection.
        /// </value>
        public string CollectionName { get; set; }
    }
}