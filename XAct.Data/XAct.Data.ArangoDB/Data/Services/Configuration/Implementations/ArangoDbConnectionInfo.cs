namespace XAct.Data.Services.Configuration.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public class ArangoDbConnectionInfo
    {

        /// <summary>
        /// If set to true, this <see cref="Alias"/>
        /// is chosen during initialization 
        /// to be the basis of setting up the Service's Context.
        /// </summary>
        public bool IsDefaultContext { get; set; }


        /// <summary>
        /// Gets or sets the name of the host (eg: localhost).
        /// </summary>
        /// <value>
        /// The name of the host.
        /// </value>
        public string HostName { get; set; }

        /// <summary>
        /// Gets or sets the port (eg: 8529).
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port { get; set; }


        /// <summary>
        /// Gets or sets whether to use Https
        /// </summary>
        public bool IsSecure { get; set; }

        /// <summary>
        /// Gets or sets the name of the database (eg: myDatabase).
        /// </summary>
        /// <value>
        /// The name of the database.
        /// </value>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the alias (eg: myDatabaseAlias).
        /// </summary>
        /// <value>
        /// The alias.
        /// </value>
        public string Alias { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArangoDbConnectionInfo"/> class.
        /// </summary>
        public ArangoDbConnectionInfo()
        {
            Alias = "localhost";

            HostName = "localhost";
            Port = 8529;
            IsSecure = false;

            //DatabaseName = null;

            //UserName = "root";
            //Password = null;
        }

    }
}