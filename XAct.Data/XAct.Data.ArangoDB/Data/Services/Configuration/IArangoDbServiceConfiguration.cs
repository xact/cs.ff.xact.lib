namespace XAct.Data.Services.Configuration
{
    using System.Collections.Generic;
    using XAct.Data.Services.Configuration.Implementations;

    /// <summary>
    /// A contract for a singleton Configuration object for the
    /// <see cref="IArangoDbService"/>
    /// </summary>
    public interface IArangoDbServiceConfiguration :IHasXActLibServiceConfiguration
    {

        /// Gets information required to successfully ping a connection's collection.
        ArangoDbPingConfigurationInfo PingInfo { get; }

        /// <summary>
        /// Array of Connection Infos required by Service during Service initialization
        /// in order to subsequently create connections as needed.
        /// </summary>
        List<ArangoDbConnectionInfo> ConnectionInfos { get; }


        
    }
}