﻿namespace XAct.Data.Services
{
    /// <summary>
    /// Contract for creating, retrieving, deleting Graphs.
    /// </summary>
    public interface IArangoDbGraphDefinitionOperations
    {
        /// <summary>
        /// Gets a list of existing Graphs in the current Database.
        /// </summary>
        /// <returns></returns>
        string[] GetGraphNames();

        /// <summary>
        /// Determins if a graph of that name exists in the current database.
        /// </summary>
        /// <param name="graphName"></param>
        /// <returns></returns>
        bool EnsureGraphExists(string graphName);

        /// <summary>
        /// Creates a new  Graph in the current database.
        /// </summary>
        /// <param name="graphName">Name of the graph.</param>
        /// <param name="edgeCollectionName">Name of the edge collection.</param>
        /// <param name="sourceVertexCollectionName">Name of the source vertex collection.</param>
        /// <param name="targetVertexCollectionName">Name of the target vertex collection.</param>
        /// <returns></returns>
        bool CreateGraph(string graphName, string edgeCollectionName, string sourceVertexCollectionName,
                         string targetVertexCollectionName);

        /// <summary>
        /// Deletes the specified graph if it exists in the current database.
        /// </summary>
        /// <param name="graphName"></param>
        /// <returns></returns>
        bool DeleteGraph(string graphName);
    }
}