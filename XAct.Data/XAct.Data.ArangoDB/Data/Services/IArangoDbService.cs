namespace XAct.Data.Services
{
    using XAct;
    using XAct.Data.Services.Configuration;

#pragma warning disable 1591
    /// <summary>
    /// Contract for a service to manage Documents
    /// stored within ArangoDb.
    /// <para>
    /// Excludes Graph and Edge functionality.
    /// </para>
    /// </summary>
    public interface IArangoDbService :         
        IArangoDbCollectionOperations, 
        IArangoDbDocumentOperations, 
        IArangoDbAQLOperations,
        IHasXActLibService
#pragma warning restore 1591
    {

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        IArangoDbContext Context { get; }

        /// <summary>
        /// Gets the singleton shared configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        IArangoDbServiceConfiguration Configuration { get; }

        /// <summary>
        /// Test whether Client is able to connect to Server.
        /// </summary>
        /// <returns></returns>
        bool Ping();

    }
}
