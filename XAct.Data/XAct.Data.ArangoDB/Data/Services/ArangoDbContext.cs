namespace XAct.Data.Services
{
    using System.Collections.Generic;
    using System;
    using Arango.Client;
    using System.Linq;
    using XAct.Data.Models;
    using XAct.Data.Models.Implementations;

#pragma warning disable 1591
#pragma warning restore 1591

    /// <summary>
    /// 
    /// </summary>
    public class ArangoDbContext : IArangoDbContext
    {
        /// <summary>
        /// Gets the Context alias.
        /// </summary>
        /// <value>
        /// The alias.
        /// </value>
        public string Alias { get; private set; }

        private ArangoDatabase _db;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArangoDbContext"/> class.
        /// </summary>
        /// <param name="alias">The* alias.</param>
        public ArangoDbContext(string alias)
        {
            Alias = alias;
            _db = new ArangoDatabase(alias);

        }

        /// <summary>
        /// Verifies if the Document Collection the exists.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        /// <returns></returns>
        public bool CollectionExists(string collectionName)
        {
            ArangoCollection collection = _db.Collection.Get(collectionName);
            return (collection != null);
        }

        /// <summary>
        /// Creates the Document collection.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="isEdge">if set to <c>true</c> [is edge].</param>
        public void CreateCollection(string collectionName, bool isEdge)
        {
            ArangoCollection collection = _db.Collection.Get(collectionName);

            if (collection == null)
            {

                // create collection object
                collection = new ArangoCollection();
                collection.Name = collectionName;
                collection.Type = isEdge ? ArangoCollectionType.Edge : ArangoCollectionType.Document;

                _db.Collection.Create(collection);
            }
        }

        /// <summary>
        /// Clears the Document collection of all Documents.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        public void ClearCollection(string collectionName)
        {
            _db.Collection.Clear(collectionName);
        }

        /// <summary>
        /// Deletes the specified Document collection.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        public void DeleteCollection(string collectionName)
        {
            _db.Collection.Delete(collectionName);
        }




        /// <summary>
        /// Documents the exists.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="documentId">The document identifier.</param>
        /// <returns></returns>
        public bool DocumentExists<TModel>(string documentId)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            return _db.Document.Exists(documentId);
        }


        ///// <summary>
        ///// Create or Update a Document.
        ///// <code>
        ///// <![CDATA[
        ///// //Create a document as an array, or map it to a Type:
        ///// document = new Document("{someProp:\"foo/123\"}");
        ///// //or
        ///// docuemnt = new Document()
        /////        .String("foo", "foo string value 1")
        /////        .Int("bar", 12345);
        ///// ]]>
        ///// </code>
        ///// </summary>
        ///// <param name="collectionName">Name of the collection.</param>
        ///// <param name="document">The document.</param>
        //public void PersistDocument(string collectionName, Document document)
        //{

        //    string id = document.String("_id");

        //    if (id.IsNullOrEmpty())
        //    {
        //        _db.Document.Create(collectionName, document);
        //    }
        //    else
        //    {
        //        _db.Document.Update(document);
        //    }
        //}

        /// <summary>
        /// Create or Update a <typeparamref name="TModel" /> based Document.
        /// <code>
        /// <![CDATA[
        /// document = new Document("{someProp:\"foo/123\"}");
        /// docuemnt = new Document()
        /// .String("foo", "foo string value 1")
        /// .Int("bar", 12345);
        /// ]]>
        /// </code>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="model">The Document model.</param>
        public void PersistDocument<TModel>(string collectionName, TModel model)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            if (model._id.IsNullOrEmpty())
            {
                bool waitForSync = true;
                _db.Document.Create(collectionName, model, waitForSync);
            }
            else
            {
                _db.Document.Update(model);
            }
        }

        //public Document GetDocument(string id)
        //{
        //    Document result = _db.Document.Get(id);
        //    return result;

        //}

        /// <summary>
        /// Gets a <typeparamref name="TModel" /> based Document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public TModel GetDocument<TModel>(string id)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            TModel result = _db.Document.Get<TModel>(id);
            return result;
        }

        /// <summary>
        /// Deletes the <typeparamref name="TModel" /> based Document.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="id">The identifier.</param>
        public void DeleteDocument<TModel>(string id)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            _db.Document.Delete(id);
        }

        /// <summary>
        /// Deletes the document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="model">The model.</param>
        public void DeleteDocument<TModel>(TModel model)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            _db.Document.Delete(model._id);
        }


        //public Document CreateEdge(string collectionName, string sourceId, string targetId)
        //{
        //    Document result = _db.Edge.Create(collectionName, sourceId, targetId);
        //    return result;
        //}


        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public TEdgeModel CreateEdgeDocument<TEdgeModel>(string collectionName, IArangoDbRecordHandle source, IArangoDbRecordHandle target)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            TEdgeModel edgeDocument = new TEdgeModel {_from = source._id, _to = target._id};

            bool waitForSync = true;

            _db.Edge.Create<TEdgeModel>(collectionName, edgeDocument, waitForSync);

            return edgeDocument;

        }


        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        /// <returns></returns>
        public TEdgeModel CreateEdgeDocument<TEdgeModel>(string collectionName, string sourceId, string targetId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            TEdgeModel edgeDocument = new TEdgeModel {_from = sourceId, _to = targetId};


            bool waitForSync = true;
            _db.Edge.Create<TEdgeModel>(collectionName, edgeDocument, waitForSync);

            return edgeDocument;
        }

        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeDocument">The edge document.</param>
        public void CreateEdgeDocument<TEdgeModel>(string collectionName, TEdgeModel edgeDocument)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            bool waitForSync = true;
            _db.Edge.Create<TEdgeModel>(collectionName, edgeDocument, waitForSync);
        }

        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeId">The edge identifier.</param>
        /// <returns></returns>
        public TEdgeModel GetEdgeDocument<TEdgeModel>(string collectionName, string edgeId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            TEdgeModel result = _db.Edge.Get<TEdgeModel>(edgeId);

            return result;
        }

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeId">The edge identifier.</param>
        public void DeleteEdgeDocument<TEdgeModel>(string collectionName, string edgeId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            _db.Edge.Delete(edgeId);
        }

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void DeleteEdgeDocument<TEdgeModel>(string collectionName, IArangoDbRecordHandle sourceId, IArangoDbRecordHandle targetId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            throw new NotImplementedException();

            //TEdgeModel result = null;

            //_db.Edge.Delete(result._id);
        }

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void DeleteEdgeDocument<TEdgeModel>(string collectionName, string sourceId, string targetId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            throw new NotImplementedException();
            //TEdgeModel result = null;

            //_db.Edge.Delete(result._id);
        }



        /// <summary>
        /// Gets the out vertices.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="edgeCollection"></param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TEdgeModel[] GetOutEdgeDocuments<TModel, TEdgeModel>(string edgeCollection, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {

            List<Document> outEdges = _db.Edge.Get(edgeCollection, source._id, ArangoEdgeDirection.Out);
            return outEdges.Select(x => x.ToObject<TEdgeModel>()).ToArray();

//            var query = _db.Query.Aql(
//                @"FOR p IN PATHS(SomeVertex, SomeEdge,'outbound')
//   FILTER 
//   p.source._id == @@sourceId 
//   && 
//   LENGTH(p.edges) == 1
//   RETURN p.vertices[*]._id").AddParameter("sourceId", source._id);

//            Document doc;
//            doc.ToObject<>()
//            return query.ToList<TEdgeModel>().ToArray();
        }


        /// <summary>
        /// Peforms an AQL Query and returns the result list.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        public List<Document> QueryList(string statement, params KeyValue<string, object>[] paramenters)
        {
            var query = _db.Query.Aql(statement);
            foreach (var parameter in paramenters)
            {
                query.AddParameter(parameter.Key, parameter.Value);
            }
            List<Document> results = query.ToList();
            return results;

        }

        /// <summary>
        /// Peforms an AQL Query and returns the result list.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        public List<TModel> QueryList<TModel>(string statement, params KeyValue<string, object>[] paramenters)
            where TModel : class, new()
        {
            var query = _db.Query.Aql(statement);
            foreach (var parameter in paramenters)
            {
                query.AddParameter(parameter.Key, parameter.Value);
            }
            var tmp = query.ToList();

            List<TModel> r = new List<TModel>();
            foreach (var t in tmp)
            {
                var tt = t.ToObject<TModel>();
                r.Add(tt);
            }

            var results = tmp.Select(x => x.ToObject<TModel>()).ToList();
            return results;
        }

        /// <summary>
        /// Peforms an AQL Query and returns a s ingle document.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        public Document QuerySingle(string statement, params KeyValue<string, object>[] paramenters)
        {
            var query = _db.Query.Aql(statement);
            foreach (var parameter in paramenters)
            {
                query.AddParameter(parameter.Key, parameter.Value);
            }
            Document results = query.ToObject();
            return results;

        }

        /// <summary>
        /// Peforms an AQL Query and returns a s ingle document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        public TModel QuerySingle<TModel>(string statement, params KeyValue<string, object>[] paramenters)
            where TModel : class, new()
        {
            var query = _db.Query.Aql(statement);
            foreach (var parameter in paramenters)
            {
                query.AddParameter(parameter.Key, parameter.Value);
            }
            TModel results = query.ToObject<TModel>();

            return results;
        }

        /// <summary>
        /// Gets the in vertices.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="edgeCollection"></param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TEdgeModel[] GetInEdgeDocuments<TModel, TEdgeModel>(string edgeCollection, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            List<Document> inEdges = _db.Edge.Get(edgeCollection, source._id, ArangoEdgeDirection.Out);
            return inEdges.Select(x => x.ToObject<TEdgeModel>()).ToArray();
        }




        /// <summary>
        /// Gets the in documents.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="edgeCollection">The edge collection.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public TModel[] GetInDocuments<TModel, TEdgeModel>(string edgeCollection, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {


            //   List<Document> inEdges = _db.Edge.Get(edgeCollection, source._id, ArangoEdgeDirection.Out);
            //inEdges.ForEach(x=>x.getV);
            //   GetInEdges<TEdgeModel>()

            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the out documents.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="graphName">The edge collection.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TModel[] GetOutDocuments<TModel, TEdgeModel>(string graphName, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {

            //See: https://docs.arangodb.com/Aql/GraphOperations.html

            int maxDepth = 1;

            string statement = @"
FOR e in GRAPH_NEIGHBORS
    (
        @graphName, 
        @sourceId,
        {
            direction:'outbound',
           minDepth: 1,
           maxDepth: @maxDepth
            }
    )
RETURN e";

#pragma warning disable 168
            var results =
#pragma warning restore 168
            
            this.QueryList<NeighborResponseItem<TModel, TEdgeModel>>(
                                statement,
                               new KeyValue<string, object>() {Key = "graphName", Value = graphName},
                               new KeyValue<string, object>() {Key = "sourceId", Value = source._id},
                               new KeyValue<string, object>() {Key = "maxDepth", Value = maxDepth}
                );
            
            return results.Select(x=>x.vertex).ToArray();
        }



    }


}

    
