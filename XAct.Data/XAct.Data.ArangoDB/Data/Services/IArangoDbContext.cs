namespace XAct.Data.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IArangoDbContext :         
        IArangoDbCollectionOperations, 
        IArangoDbDocumentOperations, 
        IArangoDbAQLOperations    ,
        IArangoDbEdgeDocumentOperations
    {
        
    }
}