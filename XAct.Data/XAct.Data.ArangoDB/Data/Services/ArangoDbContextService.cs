﻿namespace XAct.Data.Services
{
    using System;
    using XAct.Data.Services.Configuration.Implementations;

    /// <summary>
    /// 
    /// </summary>
    public class ArangoDbContextService : IArangoDbContextService
    {
        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public IArangoDbContext Context
        {
            get { return _context ?? (_context = new ArangoDbContext(_contextInfo.Alias)); }
        }
        [ThreadStatic]
        static IArangoDbContext _context;//

        /// <summary>
        /// </summary>
        public ArangoDbConnectionInfo ContextInfo { get { return _contextInfo; } set { _contextInfo = value; } }
        [ThreadStatic]
        static ArangoDbConnectionInfo _contextInfo ;//

    }
}