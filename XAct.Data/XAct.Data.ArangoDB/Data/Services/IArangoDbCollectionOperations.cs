namespace XAct.Data.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IArangoDbCollectionOperations
    {
        /// <summary>
        /// Verifies if the Document Collection the exists.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        /// <returns></returns>
        bool CollectionExists(string collectionName);

        /// <summary>
        /// Creates the Document collection.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="isEdge">if set to <c>true</c> [is edge].</param>
        void CreateCollection(string collectionName, bool isEdge);
        /// <summary>
        /// Clears the Document collection of all Documents.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        void ClearCollection(string collectionName);


        /// <summary>
        /// Deletes the specified Document collection.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        void DeleteCollection(string collectionName);

    }
}