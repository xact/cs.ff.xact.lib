﻿namespace XAct.Data.Services
{
    using XAct.Data.Services.Configuration.Implementations;

    /// <summary>
    /// 
    /// </summary>
    public interface IArangoDbContextService :IHasXActLibService
    {

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        IArangoDbContext Context { get; }

        /// <summary>
        /// 
        /// </summary>
        ArangoDbConnectionInfo ContextInfo { get; set; }


    }
}
