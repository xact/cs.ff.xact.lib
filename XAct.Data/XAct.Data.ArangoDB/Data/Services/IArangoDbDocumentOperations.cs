namespace XAct.Data.Services
{
    using XAct.Data.Models;

    /// <summary>
    /// 
    /// </summary>
    public interface IArangoDbDocumentOperations
    {


        ///// <summary>
        ///// Create or Update a <typeparamref name="TModel" /> based Document.
        ///// <code>
        ///// <![CDATA[
        ///// //Create a document as an array, or map it to a Type:
        ///// document = new Document("{someProp:\"foo/123\"}");
        ///// //or
        ///// docuemnt = new Document()
        /////        .String("foo", "foo string value 1")
        /////        .Int("bar", 12345);
        ///// ]]>
        ///// </code>
        ///// </summary>
        ///// <param name="collectionName">Name of the collection.</param>
        ///// <param name="document">The document.</param>
        //void PersistDocument(string collectionName, Document document);


        /// <summary>
        /// Create or Update a <typeparamref name="TModel" /> based Document.
        /// <code>
        /// <![CDATA[
        /// //Create a document as an array, or map it to a Type:
        /// document = new Document("{someProp:\"foo/123\"}");
        /// //or
        /// docuemnt = new Document()
        ///        .String("foo", "foo string value 1")
        ///        .Int("bar", 12345);
        /// ]]>
        /// </code>
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="model">The Document model.</param>
        void PersistDocument<TModel>(string collectionName, TModel model)
            where TModel : class, IArangoDbRecordHandle, new();

        //Document GetDocument(string id);


        /// <summary>
        /// Determines whether a <typeparamref name="TModel" /> based Document exists.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        bool DocumentExists<TModel>(string id)
            where TModel : class, IArangoDbRecordHandle, new();



        /// <summary>
        /// Gets a <typeparamref name="TModel" /> based Document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        TModel GetDocument<TModel>(string id)
            where TModel : class, IArangoDbRecordHandle, new();

        /// <summary>
        /// Deletes the <typeparamref name="TModel" /> based Document.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteDocument<TModel>(string id)
            where TModel : class, IArangoDbRecordHandle, new();

        /// <summary>
        /// Deletes the <typeparamref name="TModel" /> based Document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="id">The identifier.</param>
        void DeleteDocument<TModel>(TModel id)
            where TModel : class, IArangoDbRecordHandle, new();




    }
}