namespace XAct.Data.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Arango.Client;
    using XAct.Data.Models;
    using XAct.Data.Services.Configuration;
    using XAct.Data.Services.Configuration.Implementations;

    /// <summary>
    /// An implementation of the <see cref="IArangoDbService"/>
    /// </summary>
    public class ArangoDbService : IArangoDbService
    {
        private readonly IArangoDbContextService _adangoDbContextService;

        /// <summary>
        /// Gets the singleton shared configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public IArangoDbServiceConfiguration Configuration { get; private set; }




        /// <summary>
        /// Prevents a default instance of the <see cref="ArangoDbService" /> class from being created.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="adangoDbContextService">The adango database context service.</param>
        public ArangoDbService(IArangoDbServiceConfiguration configuration, IArangoDbContextService adangoDbContextService)
        {
            _adangoDbContextService = adangoDbContextService;
            Configuration = configuration;
            InitializeConnectionInfos();

        }


        /// <summary>
        /// Test whether Client is able to connect to Server.
        /// </summary>
        /// <returns></returns>
        public bool Ping()
        {

            var db = new ArangoDatabase(Configuration.PingInfo.DatabaseAlias);

            var collection = db.Collection.Get(Configuration.PingInfo.CollectionName);

#pragma warning disable 168
            var check = collection.Status;
#pragma warning restore 168

            return true;
        }



        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public IArangoDbContext Context
        {
            get
            {
                return  _adangoDbContextService.Context;
            }
        }











        /// <summary>
        /// Sets the context.
        /// </summary>
        /// <param name="ContextInfo">The alias context.</param>
        public void SetContext(ArangoDbConnectionInfo ContextInfo)
        {
            _adangoDbContextService.ContextInfo = ContextInfo;
        }


        /// <summary>
        /// Verifies if the Document Collection the exists.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        /// <returns></returns>
        public bool CollectionExists(string collectionName)
        {
            var result = this.Context.CollectionExists(collectionName);
            return result;
        }

        /// <summary>
        /// Creates the Document collection.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="isEdge">if set to <c>true</c> [is edge].</param>
        public void CreateCollection(string collectionName, bool isEdge)
        {
            this.Context.CreateCollection(collectionName, isEdge);
        }

        /// <summary>
        /// Clears the Document collection of all Documents.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        public void ClearCollection(string collectionName)
        {
            this.Context.ClearCollection(collectionName);
        }

        /// <summary>
        /// Deletes the specified Document collection.
        /// </summary>
        /// <param name="collectionName">Name of the collection.</param>
        public void DeleteCollection(string collectionName)
        {
            this.Context.DeleteCollection(collectionName);
        }















        /// <summary>
        /// Documents the exists.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="documentId">The document identifier.</param>
        /// <returns></returns>
        public bool DocumentExists<TModel>(string documentId)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            var result = this.Context.DocumentExists<TModel>(documentId);
            return result;
        }

        /// <summary>
        /// Create or Update a Document.
        /// <code>
        /// <![CDATA[
        /// //Create a document as an array, or map it to a Type:
        /// document = new Document("{someProp:\"foo/123\"}");
        /// //or
        /// docuemnt = new Document()
        /// .String("foo", "foo string value 1")
        /// .Int("bar", 12345);
        /// ]]>
        /// </code>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="model">The Document model.</param>
        public void PersistDocument<TModel>(string collectionName, TModel model)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            this.Context.PersistDocument(collectionName, model);
        }







        /// <summary>
        /// Gets a <typeparamref name="TModel" /> based Document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public TModel GetDocument<TModel>(string id)
    where TModel : class, IArangoDbRecordHandle, new()
        {
            var result = this.Context.GetDocument<TModel>(id);
            return result;
        }




        /// <summary>
        /// Deletes the <typeparamref name="TModel" /> based Document.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        public void DeleteDocument<TModel>(string id)
    where TModel : class, IArangoDbRecordHandle, new()
        {
            this.Context.DeleteDocument<TModel>(id);
        }

        /// <summary>
        /// Deletes the <typeparamref name="TModel" /> based Document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="id">The identifier.</param>
        public void DeleteDocument<TModel>(TModel id)
    where TModel : class, IArangoDbRecordHandle, new()
        {
            this.Context.DeleteDocument<TModel>(id);
        }






        #region Query 
        /// <summary>
        /// Peforms an AQL Query and returns the result list.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        public List<Document> QueryList(string statement, params KeyValue<string, object>[] paramenters)
        {
            var results = this.Context.QueryList(statement, paramenters);
            return results;

        }

        /// <summary>
        /// Peforms an AQL Query and returns the result list.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        public List<TModel> QueryList<TModel>(string statement, params KeyValue<string, object>[] paramenters)
            where TModel : class, new()
        {
            var results = this.Context.QueryList<TModel>(statement, paramenters);
            return results;
        }

        /// <summary>
        /// Peforms an AQL Query and returns a s ingle document.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        public Document QuerySingle(string statement, params KeyValue<string, object>[] paramenters)
        {
            var result = this.Context.QuerySingle(statement, paramenters);
            return result;

        }

        /// <summary>
        /// Peforms an AQL Query and returns a s ingle document.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <param name="paramenters">The paramenters.</param>
        /// <returns></returns>
        public TModel QuerySingle<TModel>(string statement, params KeyValue<string, object>[] paramenters)
            where TModel : class, new()
        {
            var result = this.Context.QuerySingle<TModel>(statement, paramenters);
            return result;
        }


        #endregion





        #region Private
        void InitializeConnectionInfos()
        {
            var x = Configuration.ConnectionInfos.LastOrDefault();
            foreach (var c in Configuration.ConnectionInfos)
            {
                CreateConnection(c);
                if (c.IsDefaultContext)
                {
                    x =c;
                }
            }
            if (x != null)
            {
                this.SetContext(x);
            }
        }

        void CreateConnection(ArangoDbConnectionInfo connectionInfo)
        {
            Arango.Client.ArangoClient.AddConnection(connectionInfo.HostName,
                connectionInfo.Port,
                connectionInfo.IsSecure,
                connectionInfo.DatabaseName,
                connectionInfo.Alias,
                connectionInfo.UserName,
                connectionInfo.Password);

        }
        #endregion

    }
}
    