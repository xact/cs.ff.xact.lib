﻿namespace XAct.Data.Models
{
    using Arango.Client;
    using XAct.Data.Models.Implementations;

    /// <summary>
    /// Contract for ArangoDB Document entities.
    /// <see cref="RecordHandleBase"/>
    /// </summary>
    public interface IArangoDbRecordHandle
    {

        //IMPORTANT:
        //Unfortunately cant associate Attribute to Interface, so this won't work:
        //has to be on a Class, not an inteface  :-(
        //See RecordHandleBase
        //[ArangoProperty(Identity = true)]



        /// <summary>
        /// Gets or sets the _id (an ArangoDB Document specific property).
        /// </summary>
        [ArangoProperty(Identity = true)]
        string _id { get; set; }

        /// <summary>
        /// Gets or sets the _rev (an ArangoDB Document specific property)
        /// </summary>
        [ArangoProperty(Revision = true)]
        string _rev { get; set; }


        /// <summary>
        /// Gets or sets the _key (an ArangoDB Document specific property).
        /// </summary>
        [ArangoProperty(Key = true)]
        string _key { get; set; }
    }
}