﻿namespace XAct.Data.Models.Implementations
{
#pragma warning disable 1591
    public abstract class ApiResponseBase
    {
        public bool error { get; set; }
        public int code { get; set; }
    }
#pragma warning restore 1591
}