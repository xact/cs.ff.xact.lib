﻿namespace XAct.Data.Models.Implementations
{
    using System.Runtime.Serialization;
    using Arango.Client;

    /// <summary>
    /// Abstract base for ArangodDB Document entities.
    /// </summary>
    [DataContract]
    public abstract class RecordHandleBase : IArangoDbRecordHandle
    {

        /// <summary>
        /// Gets or sets the _id (an ArangoDB Document specific property).
        /// </summary>
        [ArangoProperty(Identity = true)]
        [DataMember]
        public virtual string _id { get; set; }

        /// <summary>
        /// Gets or sets the _rev (an ArangoDB Document specific property)
        /// </summary>
        [ArangoProperty(Revision = true)]
        [DataMember]
        public virtual string _rev { get; set; }

        /// <summary>
        /// Gets or sets the _key (an ArangoDB Document specific property).
        /// </summary>
        [ArangoProperty(Key = true)]
        [DataMember]
        public virtual string _key { get; set; }
    }
}