﻿namespace XAct.Data.Models.Implementations
{
    using System.Collections.Generic;
    using Arango.Client;

#pragma warning disable 1591

    public class NeighborResponsePathSet
        {
            public List<Document> vertices
            {
                get { return _vertices ?? (_vertices = new List<Document>()); }
                set { _vertices = value; }
            }

            private List<Document> _vertices;

            public List<Document> edges
            {
                get { return _edges ?? (_edges = new List<Document>()); }
                set { _edges = value; }
            }

            private List<Document> _edges;

        }




    public class NeighborResponsePathSet<TModel, TEdgeModel>
        where TModel : class, IArangoDbRecordHandle, new()
        where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
    {

        public NeighborResponsePathSet()
        {
            
        } 
        public NeighborResponsePathSet(NeighborResponsePathSet unTyped)
        {
            foreach (var x in unTyped.vertices)
            {
                vertices.Add(x.ToObject<TModel>());
            }
            foreach (var x in unTyped.edges)
            {
                edges.Add(x.ToObject<TEdgeModel>());
            }
        }

        public List<TModel> vertices
        {
            get { return _vertices ?? (_vertices = new List<TModel>()); }
            set { _vertices = value; }
        }

        private List<TModel> _vertices;

        public List<TEdgeModel> edges
        {
            get { return _edges ?? (_edges = new List<TEdgeModel>()); }
            set { _edges = value; }
        }

        private List<TEdgeModel> _edges;
    }
#pragma warning restore 1591
}