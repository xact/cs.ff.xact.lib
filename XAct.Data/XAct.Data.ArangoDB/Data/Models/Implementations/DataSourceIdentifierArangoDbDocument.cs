﻿namespace XAct.Data.Models.Implementations
{
    using System;
    using System.Runtime.Serialization;
    using Arango.Client;

    /// <summary>
    /// An example DurangoDb Document,
    /// demonstrating being a subclass of <see cref="RecordHandleBase"/>
    /// </summary>
    [DataContract]
    public  class DefaultDataSourceIdentifierArangoDbDocument : RecordHandleBase, IHasDataSourceIdentifier
    {

        /// <summary>
        /// A unique identifier (FK) for the <c>IDataSource</c>
        /// (eg: the XXX Table in YYY Db in ZZZ ConnectionString)
        /// <para>Member defined in<see cref="IHasDataSourceIdentifier" /></para>
        /// </summary>
        /// <remarks>
        /// The Guid (rather than a shorter Int) allows developers to develop offline,
        /// rather than having to first reserve a DataSource Id, centrally.
        /// </remarks>
        [DataMember]
        [ArangoProperty(Serializable=false, Deserializable=false)]
        public Guid DataSourceId { get; set; }


        /// <summary>
        /// Gets or sets the _serializable data source identifier.
        /// </summary>
        [DataMember]
        [ArangoProperty(Alias="DataSourceId", Serializable=true)]
        public string _serializableDataSourceId
        {
            get { return DataSourceId.ToString(); }
            set { DataSourceId = Guid.Parse(value); }
        }

        /// <summary>
        /// The unique identifiers for the data object in the source XXX table.
        /// <para>
        /// It is up to the Connector to know how to deserialize the value
        /// into a string, guid, etc. depending on the Connector's knowledge
        /// of the source catalog.
        /// </para>
        /// <para>Member defined in<see cref="IHasDataSourceIdentifier" /></para>
        /// </summary>
        /// <remarks>
        /// I agonized a long time on whether this should be a dictionary,
        /// but
        /// a) makes it hard to impossible to use ORMs effectively,
        /// b) saves time serialize/deserialize as the most common use is in the serialized state.
        /// </remarks>
        [DataMember]
        public virtual string DataSourceSerializedIdentities { get; set; }
    }
}