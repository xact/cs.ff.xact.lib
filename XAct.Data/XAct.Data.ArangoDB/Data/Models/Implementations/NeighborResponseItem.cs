﻿#define CantDoIt
namespace XAct.Data.Models.Implementations
{
#pragma warning disable 1591
        public class NeighborResponseItem<TModel, TEdgeModel>
        where TModel : class, IArangoDbRecordHandle, new()
        where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
    {
        public TModel vertex { get; set; }


        public NeighborResponsePathSet<TModel,TEdgeModel> TypedPath
        {
            get
            {
#if CantDoIt
                return _typedPath ?? (_typedPath = new NeighborResponsePathSet<TModel, TEdgeModel>(_path));
#else
                return _path;
#endif
            }
        }
#pragma warning disable 169
        private NeighborResponsePathSet<TModel, TEdgeModel> _typedPath;
#pragma warning restore 169


#if CantDoIt
        public NeighborResponsePathSet path
        {
            get { return _path; }
            set { _path = value; }
        }
        private NeighborResponsePathSet _path = new NeighborResponsePathSet();
#else

        public NeighborResponsePathSet<TModel, TEdgeModel> path
        {
            get { return _path ?? (_path = new NeighborResponsePathSet<TModel, TEdgeModel>()); }
            set { _path = value; }
        }
        private NeighborResponsePathSet<TModel, TEdgeModel> _path = new NeighborResponsePathSet<TModel, TEdgeModel>(); 
#endif

        public string startVertex { get; set; }


    }
#pragma warning restore 1591
}
