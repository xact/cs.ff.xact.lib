﻿namespace XAct.Data.Models.Implementations
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class DefaultDataSourceIdentifiedSummary : DefaultDataSourceIdentifierArangoDbDocument,
                                                      IHasTextAndDescription, IHasDateTimeModifiedOnUtc,
                                                      IHasRenderingHints
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        [DataMember]
        public string Text { get; set; }

        /// <summary>
        /// Gets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets information as to the known status of the Node.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [DataMember]
        public string Status { get; set; }

        /// <summary>
        /// DateTimeUtc last summarized.
        /// </summary>
        [DataMember]
        public DateTime? LastModifiedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the rendering image hints.
        /// <para>
        /// Images could be for controls tucked away, or simply rendered beside/instead of labels.
        /// </para>
        /// </summary>
        [DataMember]
        public string RenderingImageHints { get; set; }

        /// <summary>
        /// Gets or sets hints that can be used by a
        /// View rendering mechanism to group Settings together.
        /// </summary>
        [DataMember]
        public string RenderGroupingHints { get; set; }

        /// <summary>
        /// Gets or sets hints that can be used by a
        /// View rendering mechanism to determine the rendering order the within a Group.
        /// </summary>
        [DataMember]
        public int RenderOrderHint { get; set; }

    }
}