﻿namespace XAct.Data.Models.Implementations
{
    using System.Runtime.Serialization;
    using Arango.Client;

    /// <summary>
    /// Abstract base for ArangodDB Edge Document entities.
    /// </summary>
    [DataContract]
    public abstract class EdgeRecordHandleBase : RecordHandleBase, IArangoDbEdgeRecordHandle
    {
        /// <summary>
        /// Gets or sets the identity of the Source document (an ArangoDB Document specific property).
        /// </summary>
        [ArangoProperty(From = true)]
        [DataMember]
        public virtual string _from { get; set; }

        /// <summary>
        /// Gets or sets the identity of the Source document (an ArangoDB Document specific property).
        /// </summary>
        [ArangoProperty(To = true)]
        [DataMember]
        public virtual string _to  { get; set; }
    }
}