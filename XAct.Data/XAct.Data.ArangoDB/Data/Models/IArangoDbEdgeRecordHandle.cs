﻿

namespace XAct.Data.Models
{
    using XAct.Data.Models.Implementations;

    /// <summary>
    /// Contract for ArangoDB Edge Document entities.
    /// <see cref="EdgeRecordHandleBase"/>
    /// </summary>
    public interface IArangoDbEdgeRecordHandle : IArangoDbRecordHandle
    {

        //IMPORTANT:
        //Unfortunately cant associate Attribute to Interface, so this won't work:
        //has to be on a Class, not an inteface  :-(
        //See RecordHandleBase
        //[ArangoProperty(Identity = true)]


        /// <summary>
        /// Gets or sets the identity of the Source document (an ArangoDB Document specific property).
        /// </summary>
        //[ArangoProperty(From = true)]
        string _from { get; set; }

        /// <summary>
        /// Gets or sets the identity of the Source document (an ArangoDB Document specific property).
        /// </summary>
        // [ArangoProperty(To = true)]
        string _to { get; set; }
    }
}
