﻿namespace XAct.Data.Documents.PersistenceMaps
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Data.Documents.Models;

    public class AltNamePersistenceMap : EntityTypeConfiguration<AltName>
    {
        public AltNamePersistenceMap()
        {
            this.ToXActLibTable("AltName");

            this.HasKey(x => x.Id);
            this.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}