﻿namespace XAct.Data.Documents.PersistenceMaps
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Data.Documents.Models;

    public class StudentPersistenceMap : EntityTypeConfiguration<Student>
    {

        public StudentPersistenceMap()
        {
            this.ToXActLibTable("Student");

            this.HasKey(x => x.Id);
            this.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            this.HasMany(x => x.AltNames)
                .WithRequired()
                .HasForeignKey(x => x.StudentId);

        }


    }
}