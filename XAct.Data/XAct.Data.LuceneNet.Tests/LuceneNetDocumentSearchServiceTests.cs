
namespace XAct.Data.Documents.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Lucene.Net.Index;
    using Lucene.Net.Search;
    using Lucene.Net.Util;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Data.Documents.Models;
    using XAct.Data.Search;
    using XAct.Domain.Repositories;
    using XAct.Messages;
    using XAct.Spikes.CodeFirst.Data.Search.Entities;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class LuceneNetDocumentSearchServiceTests
    {
        private const string CatalogName = "Students";

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            var configuration =
                XAct.DependencyResolver.Current.GetInstance<ILuceneDocumentSearchManagementServiceConfiguration>();


            configuration.IndexRootDirectory =  Path.Combine(System.IO.Path.GetTempPath(),"Lucene.NET\\Data\\");

            //Creation of a new index, and indexing all records:
            InitialCreationOfIndex();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Can_Get_IDocumentSearchManagementService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();

            Assert.IsNotNull(service);
        }

        [Test]
        public void Can_Get_IDocumentSearchManagementService_Of_ExpectedType()
        {
            var service = XAct.DependencyResolver.Current.GetInstance< IDocumentService>();

            Assert.AreEqual(typeof(LuceneNetDocumentService), service.GetType());
        }

        [Test]
        public void Database_Is_Seeded()
        {
#pragma warning disable 168
            var repoService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();


            //Ensure we are up to date:
            repoService.GetContext().Commit();
            //Count:
            var r = repoService.Count<Student>();
            Assert.IsTrue(r > 0, "Have some student records");
        }


        [Test]
        public void Can_Find_Indexed_Records_With_Exact_Match()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();

            var criteria = "FamilyName:Kleinman";
            var results = RetrieveMatchedDbRecords(service.Search(CatalogName, criteria, "FamilyName", "Id"));

            Assert.IsNotNull(results, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Assert.IsTrue(results.Length>0, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Console.WriteLine("'{0}' ==> '{1}'", criteria, results.First().FamilyName);
        }

        [Test]
        public void Can_Find_Indexed_Records_With_Exact_Prefix()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();

            var criteria = "FamilyName:Klein*";
            var results = RetrieveMatchedDbRecords(service.Search(CatalogName, criteria, "FamilyName", "Id"));

            Assert.IsNotNull(results, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Assert.IsTrue(results.Length>0, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Console.WriteLine("'{0}' ==> '{1}'", criteria, results.First().FamilyName);
        }

        [Test]
        public void Can_Find_Indexed_Records_With_Exact_Prefix_And_Fuzzy_Suffix()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();

            var criteria = "FamilyName:Klein~";
            var results = RetrieveMatchedDbRecords(service.Search(CatalogName, criteria, "FamilyName", "Id"));

            Assert.IsNotNull(results, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Assert.IsTrue(results.Length > 0, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Console.WriteLine("'{0}' ==> '{1}'", criteria, results.First().FamilyName);
        }

        [Test]
        public void Can_Find_Indexed_Records_With_Phonetic_Prefix_Fuzzzy_Suffix()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();

            var criteria = "FamilyName:Kleyn~";
            var results = RetrieveMatchedDbRecords(service.Search(CatalogName, criteria, "FamilyName", "Id"));

            Assert.IsNotNull(results, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Assert.IsTrue(results.Length>0, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Console.WriteLine("'{0}' ==> '{1}'", criteria, results.First().FamilyName);
        }


        [Test]
        public void Can_Find_Indexed_Records_With_Phonetic_Near_Prefix_Fuzzzy_Suffix()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();

            var criteria = "FamilyName:Klen~";
            var results = RetrieveMatchedDbRecords(service.Search(CatalogName, criteria, "FamilyName", "Id"));

            Assert.IsNotNull(results, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Assert.IsTrue(results.Length>0, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Console.WriteLine("'{0}' ==> '{1}'", criteria, results.First().FamilyName);
        }
            
        [Test]
        public void Can_Find_Indexed_Records_With_Phonetic_Near_FullName_And_Fuzzzy_Suffix()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();

            var criteria = "FamilyName:Kleynman~";
            var results = RetrieveMatchedDbRecords(service.Search(CatalogName, criteria, "FamilyName", "Id"));

            Assert.IsNotNull(results, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Assert.IsTrue(results.Length>0, "Not found with '{0}'".FormatStringInvariantCulture(criteria));
            Console.WriteLine("'{0}' ==> '{1}'", criteria, results.First().FamilyName);
        }


        [Test]
        public void Can_Find_Indexed_Other_Records()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();

            var resultsC1 = RetrieveMatchedDbRecords(service.Search(CatalogName, "FamilyName:Sylvestre~", "FamilyName",
                                                "Id"));
            var resultsC2 = RetrieveMatchedDbRecords(service.Search(CatalogName, "FamilyName:Sylva~", "FamilyName",
                                                "Id"));

            Assert.IsNotNull(resultsC1);
            Assert.IsNotNull(resultsC2);

        }






        [Test]
        public void Ensure_Service_Is_Consistent_Even_If_Same_Record_Is_Added_Multiple_Times()
        {
            var repoService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();


            //Count:
            var r = repoService.Count<Student>();

            //Does doing it multuple times mess it up? YES:
            var allStudents = repoService.GetByFilter<Student>(x => true, null, new PagedQuerySpecification(0, int.MaxValue));
            service.Persist(CatalogName, allStudents);
            service.Persist(CatalogName, allStudents);
            service.Persist(CatalogName, allStudents);

            SeedMoreStudents();
            allStudents = repoService.GetByFilter<Student>(x => true, null, new PagedQuerySpecification(0, int.MaxValue));
            service.Persist(CatalogName, allStudents);
            service.Persist(CatalogName, allStudents);
            service.Persist(CatalogName, allStudents);

            AddSPecificStudents();
            allStudents = repoService.GetByFilter<Student>(x => true, null, new PagedQuerySpecification(0, int.MaxValue));
            service.Persist(CatalogName, allStudents);


            var criteria = "FamilyName:Kleinman";
            var results = RetrieveMatchedDbRecords(service.Search(CatalogName, criteria, "FamilyName", "Id"));
            Assert.IsNotNull(results, "Not found with '{0}'".FormatStringInvariantCulture(criteria));

        }




        [Test]
        public void Using_LuceneNet_Query_Search()
        {
#pragma warning disable 168
            var service = XAct.DependencyResolver.Current.GetInstance<ILuceneNetDocumentService>();


            Lucene.Net.Analysis.Analyzer analyzer =
                new Lucene.Net.Analysis.Standard.StandardAnalyzer
                    (Lucene.Net.Util.Version.LUCENE_30);


            Lucene.Net.Search.TermQuery termQuery = new TermQuery(new Term("Val", "3"));

                    //public ScoredDocumentResult[] Search(string indexName, Query query, params string[] hitFieldNamesToReturn)

            var uxy = service.Search(CatalogName, termQuery, "Id");


            Lucene.Net.Search.TermQuery termQuery2 = new TermQuery(new Term("Val", NumericUtils.IntToPrefixCoded(3)));

            var uxy2x = service.Search(CatalogName, termQuery, "Id");



            var q2 = NumericRangeQuery.NewLongRange("Val", 3, 3, true, true);

            var uxy2 = service.Search(CatalogName, termQuery, "Id");





            var resultsE1 = RetrieveMatchedDbRecords(service.Search(CatalogName,
                                                "Val:3",
                                                "FamilyName",
                                                "Id"));


            var resultsD1 = RetrieveMatchedDbRecords(service.Search(CatalogName,
                                                "FamilyName:Smith~ AND GivenName1:Steve~",
                                                "FamilyName",
                                                "Id"));

            var x = new DateTime(1968, 1, 1).Date.Ticks/TimeSpan.TicksPerMillisecond;
            var resultsD2 = RetrieveMatchedDbRecords(service.Search(CatalogName,
                                                "Dob:" + x,
                                                "FamilyName",
                                                "Id"));
            var resultsD2b = RetrieveMatchedDbRecords(service.Search(CatalogName,
                                                 "Dob:[62072438400 TO 62072438400]",
                                                 "FamilyName",
                                                 "Id"));
            var resultsD3 = RetrieveMatchedDbRecords(service.Search(CatalogName,
                                                "GivenName1:Steve~ AND Dob:62072438400~",
                                                "FamilyName",
                                                "Id"));
            // AND Dob:19690101~

            var resultsF1 = RetrieveMatchedDbRecords(service.Search(CatalogName,
                                                "Id:[1 TO 10]",
                                                "FamilyName",
                                                "Id"));




#pragma warning restore 168
        }
































        ///
        Student[] RetrieveMatchedDbRecords(ScoredDocumentResult[] scoredDocumentResults)
        {
            return scoredDocumentResults.Select(RetrieveMatchedDbRecord).ToArray();
        }


        Student RetrieveMatchedDbRecord(ScoredDocumentResult response)
        {
            var repoService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            var id = response.FieldValues[0].Value.ToInt32();

            return repoService.GetSingle<Student>(x => x.Id == id);
            
        }



        private void InitialCreationOfIndex()
        {
            var repoService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            var service = XAct.DependencyResolver.Current.GetInstance<IDocumentService>();


            //Count:
            var r = repoService.Count<Student>();

            //Does doing it multuple times mess it up? YES:
            var allStudents = repoService.GetByFilter<Student>(x => true, null, new PagedQuerySpecification(0, int.MaxValue));

            //Notice 'false' that creates new Catalog:
            service.Persist(CatalogName, allStudents,false,true);
        }




        private void SeedMoreStudents()
        {
            var repoService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();


            string[] names = new string[]
                {
                    "Lucretia Mccullen",
                    "Rochell Harkleroad",
                    "Delinda Shelby",
                    "Audria Bettinger",
                    "Deann Sanroman",
                    "Ola Mcgough",
                    "Trudy Prue",
                    "Rana Rooks",
                    "Deanne Margerum",
                    "Leeann Jarrells",
                    "Ramonita Hawkins",
                    "Opal Banda",
                    "Reva Lebo",
                    "Ira Carbone",
                    "Demetrice Sylvestre",
                    "Bulah Plyler",
                    "Georgia Hix",
                    "Pasty Revilla",
                    "Jacinda Hane",
                    "Marietta Newsome",
                    "Berenice Antrim",
                    "Carl Ketner",
                    "Jewel Chittenden",
                    "Dudley Kear",
                    "Shizuko Hardie",
                    "Earnest Smith",
                    "Laure Reardon",
                    "Darcy Taber",
                    "Susan Kemmer",
                    "Felipa Cipriano",
                    "Pamila Hanlin",
                    "Kelsi Jain",
                    "Asha Guillermo",
                    "Lucinda Ming",
                    "Jackeline Courtright",
                    "Lilia Rost",
                    "Charita Guebert",
                    "Lawana Delara",
                    "Bobbie Mckillop",
                    "Sunny Gish",
                    "Cameron Woodmansee",
                    "Christopher Schnell",
                    "Ivan Appleton",
                    "Duncan Bourbeau",
                    "Bruce Gehringer",
                    "Suzann Opperman",
                    "Bertram Worsley",
                    "Lia Weiland",
                    "Katharine Lightner",
                    "Magdalena Gearing"
                };


            foreach (string name in names)
            {
                var parts = name.Split(' ');

                repoService.AddOrUpdate(
                    x => x.FamilyName,
                    new Student {GivenName1 = parts.First(), FamilyName = parts.Last()}
                    );
            }
            repoService.GetContext().Commit();
        }



        private static void AddSPecificStudents()
        {

            var repoService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            string[] names = new string[]
                {
                    "Stev Smith",
                    "Steve Smith",
                    "Steve Smith",
                    "Steve Smith",
                    "Steve Smyth",
                    "Steve"
                };

            DateTime dob = new DateTime(1968,1,1);


            foreach (string name in names)
            {
                var parts = name.Split(' ');

                repoService.AddOrUpdate(x => x.FamilyName,
                                    new Student {GivenName1 = parts.First(), FamilyName = parts.Last(), Dob=dob});
                //dob = dob.AddYears(1);
            }

            repoService.GetContext().Commit();
        }

    }
}


