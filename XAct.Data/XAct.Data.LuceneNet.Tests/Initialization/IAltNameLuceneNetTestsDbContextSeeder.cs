﻿namespace XAct.Data.Documents.Initialization
{
    using XAct.Data.DataSources;
    using XAct.Data.Documents.Models;
    using XAct.Data.EF.CodeFirst;

    public interface IAltNameLuceneNetTestsDbContextSeeder : IHasXActLibDbContextSeeder<AltName>
    {

    }
}