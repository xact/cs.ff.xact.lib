﻿namespace XAct.Data.Documents.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface ILuceneNETTestsDbModelBuilder : IHasXActLibDbModelBuilder
    {
    }
}
