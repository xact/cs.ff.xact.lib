﻿namespace XAct.Data.Documents.Initialization.Implementations
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.Entity;
    using XAct.Data.Documents.Models;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;


    public class StudentLuceneNetTestsDbContextSeeder : XActLibDbContextSeederBase<Student>, 
        IStudentLuceneNetTestsDbContextSeeder, 
        IHasMediumBindingPriority
    {


        public StudentLuceneNetTestsDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {

        }

        public override void CreateEntities()
            //where TDbContext : DbContext
        {
            this.InternalEntities = new List<Student>();

            foreach (string name in NamesList.Names)
            {
                var parts = name.Split(' ');
                InternalEntities.Add(new Student
                    {
                        GivenName1 = parts.First(),
                        FamilyName = parts.Last(),
                        Val = 3
                    });
            }


        }
    }
}
