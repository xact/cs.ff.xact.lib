﻿namespace XAct.Data.Documents.Initialization.Implementations
{
    using System.Data.Entity;
    using XAct.Data.Documents.PersistenceMaps;
    using XAct.Diagnostics;

    public class LuceneNETTestsDbModelBuilder : ILuceneNETTestsDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LuceneNETTestsDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public LuceneNETTestsDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new StudentPersistenceMap());
            modelBuilder.Configurations.Add(new AltNamePersistenceMap());

        }
    }
}