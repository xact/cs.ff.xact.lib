﻿namespace XAct.Data.Documents.Initialization.Implementations
{
    internal static class NamesList
    {
        public static string[] Names
        {
            get
            {

                return new string[]
                    {
                        "Dionne Mangano",
                        "Kathy Dahmen",
                        "Mozell Guillermo",
                        "Georgeanna Slama",
                        "Kelle Corral",
                        "Kim Peralto",
                        "Malcolm Kleinman",
                        "Danelle Fiala",
                        "Malcolm Kleynmann",
                        "Flor Winborne",
                        "Enda Speelman",
                        "Melda Leitch",
                        "Kirstie Dauer",
                        "Preston Willetts",
                        "Dwain Shead",
                        "Kandis Adelson",
                        "Rebbeca Hosking",
                        "Almeta Hook",
                        "Nakisha Digirolamo",
                        "Elene Millett",
                        "Melaine Bosco",
                        "Phyliss Huot",
                        "Vince Willett",
                        "Saundra Lebleu",
                        "Zena Gonzalez",
                        "Gabrielle Anding",
                        "Manuel Foronda",
                        "Juliann Dano",
                        "Russel Blakeley",
                        "Boris Cagney",
                        "Donnell Seaman",
                        "Fredericka Rieth",
                        "Shira Stecher",
                        "Suzanne Lipford",
                        "Lynsey Kruse",
                        "Glendora Crigger",
                        "Ricarda Ocegueda",
                        "Lydia Voss",
                        "Kimiko Durgin",
                        "Aurora Lenz",
                        "Tari Starnes",
                        "Saran Unruh",
                        "Raeann Moeckel",
                        "Ilda Lamacchia",
                        "Juli Shand",
                        "Deshawn Steve",
                        "Magda Ensign",
                        "Georgiann Eaves",
                        "Ming Aiello",
                        "Donald Germany",
                        "Krista Harpe"
                    };
            }

        }

    }
}