﻿namespace XAct.Data.Documents.Initialization.Implementations
{
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Data.Documents.Models;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    public class AltNameLuceneNETTestsDbContextSeeder : XActLibDbContextSeederBase<AltName>,
                                                        IAltNameLuceneNetTestsDbContextSeeder,
                                                        IHasMediumBindingPriority
    {
        private readonly IStudentLuceneNetTestsDbContextSeeder _studentLuceneNetTestsDbContextSeeder;

        public AltNameLuceneNETTestsDbContextSeeder(IStudentLuceneNetTestsDbContextSeeder
                                                        studentLuceneNetTestsDbContextSeeder)
            : base(studentLuceneNetTestsDbContextSeeder)
        {
            _studentLuceneNetTestsDbContextSeeder = studentLuceneNetTestsDbContextSeeder;
        }


        public override void CreateEntities()
        {
            this.InternalEntities = new List<AltName>();

            foreach (string name in NamesList.Names)
            {
                var parts = name.Split(' ');
                var gn = parts[0];
                var fn = parts.Last();

                var student = _studentLuceneNetTestsDbContextSeeder.Entities.SingleOrDefault(x => x.GivenName1 == gn && x.FamilyName == fn);

                this.InternalEntities.Add(new AltName{StudentId = student.Id, GivenName1=student.GivenName1+"y" , FamilyName = student.FamilyName + "y"});
                this.InternalEntities.Add(new AltName {StudentId = student.Id,  GivenName1 = student.GivenName1 + "o", FamilyName = student.FamilyName + "o" });
            }
        
        }
    }
}