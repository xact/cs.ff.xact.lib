﻿namespace XAct.Data.Documents.Initialization
{
    using XAct.Data.Documents.Models;
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    public interface IStudentLuceneNetTestsDbContextSeeder : IHasXActLibDbContextSeeder<Student>
    {

    }
}