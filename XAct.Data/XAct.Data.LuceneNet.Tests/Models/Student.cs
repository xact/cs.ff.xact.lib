﻿namespace XAct.Data.Documents.Models
{
    using System;
    using System.Collections.ObjectModel;
    using XAct.Data.Search;

    public class Student : IHasXActLibEntity, IHasNames
    {
        [DocumentField(Stored.Yes, Indexed.Untokenized, FieldType.Undefined,true)]
        public int Id { get; set; }

        //We want to search through this, but don't need it persisted:
        [DocumentField(Stored.No, Indexed.Tokenized)]
        public string FamilyName { get; set; }

        [DocumentField(Stored.No, Indexed.Tokenized)]
        public string GivenName1 { get; set; }

        [DocumentField(Stored.No, Indexed.Tokenized)]
        public string GivenName2 { get; set; }

        [DocumentField(Stored.No, Indexed.Tokenized)]
        public string GivenName3 { get; set; }

        //
        [DocumentField(Stored.Yes, Indexed.Untokenized)]
        public DateTime? Dob { get; set; }


        [DocumentField(Stored.Yes, Indexed.Untokenized)]
        public int Val { get; set; }


        public Collection<AltName> AltNames { get { return _altNames ?? (_altNames = new Collection<AltName>()); } }

        //[LuceneFieldAttribute()]
        private Collection<AltName> _altNames;
    }
}