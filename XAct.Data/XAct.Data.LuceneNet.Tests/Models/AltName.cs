﻿namespace XAct.Data.Documents.Models
{
    public class AltName: IHasXActLibEntity
    {
        public int Id { get; set; }

        public int StudentId { get; set; }
        public Student Student { get; set; }

        public string FamilyName { get; set; }

        public string GivenName1 { get; set; }
        public string GivenName2 { get; set; }
        public string GivenName3 { get; set; }

    }
}