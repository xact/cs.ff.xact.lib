﻿namespace XAct.Data.Documents.Models
{
    public interface IHasNames 
    {
        string FamilyName { get; set; }

        string GivenName1 { get; set; }
        string GivenName2 { get; set; }
        string GivenName3 { get; set; }
    }
}