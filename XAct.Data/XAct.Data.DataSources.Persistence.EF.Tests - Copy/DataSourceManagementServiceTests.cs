namespace XAct.Data.Tests
{
    using System;
    using System.Data.Entity.Validation;
    using NUnit.Framework;
    using XAct.Data.DataSources;
    using XAct.Domain.Repositories;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DataSourceManagementServiceTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {

            try
            {
                InitializeUnitTestDbIoCContextAttribute.BeforeTest();
            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void DataSourceManagementServiceIsAvailable()
        {
            IDataSourcesManagementService dataSourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourcesManagementService>();


            Assert.IsTrue(dataSourceManagementService != null);
        }


        [Test]
        public void DataSourceManagementServiceIsOfExpectedService()
        {
            IDataSourcesManagementService dataSourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourcesManagementService>();


            Assert.AreEqual(typeof(DataSourcesManagementService), dataSourceManagementService.GetType());
        }



        [Test]
        public void CanRetrieveDataSourceByName()
        {
            IDataSourcesManagementService dataSourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourcesManagementService>();

            DataSource dataSource = dataSourceManagementService.GetByKey("Contacts");

            Assert.IsNotNull(dataSource);
            Assert.AreEqual("Contacts", dataSource.Name);
        }


        [Test]
        public void CanRetrieveDataSourceById()
        {
            IDataSourcesManagementService dataSourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourcesManagementService>();

            DataSource tmp = dataSourceManagementService.GetByKey("Contacts");


            DataSource dataSource = dataSourceManagementService.GetById(tmp.Id);
            
            Assert.IsNotNull(dataSource);
            Assert.AreEqual("Contacts", dataSource.Name);
        }

        [Test]
        public void CanCreateADataSource()
        {
            IDataSourcesManagementService dataSourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourcesManagementService>();

            DataSource dataSource = new DataSource
                {
                    IsRemote = false,
                    Name="TASKS",
                    DataStoreTableName = "Tasks",
                    DataStoreSerializedIdentifierNames = "Id"

                };
            dataSource.Validate();

            dataSourceManagementService.PersistOnCommit(dataSource);
            IUnitOfWorkService unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
            unitOfWorkService.GetCurrent().Commit();
            dataSource = dataSourceManagementService.GetByKey("TASKS");

            Assert.IsNotNull(dataSource);
            Assert.AreEqual("TASKS", dataSource.Name);
        }


        [Test]
        [ExpectedException]
        public void CanCreateADataSource2()
        {

            DataSource dataSource = new DataSource
            {
                IsRemote = true,
                Name = "APPT",
                DataStoreTableName = "Appointments",
                DataStoreSerializedIdentifierNames = "Id"
            };

            //Will throw Exception:
            dataSource.Validate();
        }
    }
}


