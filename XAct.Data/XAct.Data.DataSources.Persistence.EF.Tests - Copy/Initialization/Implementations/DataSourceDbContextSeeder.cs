﻿namespace XAct.Data.DataSources.Initialization.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.DataSources.Initialization.DbContextSeeders;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    public class DataSourceDbContextSeeder : XActLibDbContextSeederBase<DataSource>, IDataSourceDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public DataSourceDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _environmentService = environmentService;
        }

        /// <summary>
        /// Seeds the specified database context.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            //Create and Cache the entities:
            CreateEntities();
            //Once created, consider:
            SeedInternalHelper(dbContext,true, x=>x.Id);
        }

        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {

            this.InternalEntities = new List<DataSource>();

            DataSource ds;

            ds = new DataSource
                {
                    Name = "Contacts",
                    DataStoreCatalogName = "MyDb",
                    DataStoreTableName = "Contacts",
                    ConnectionMetaData = "[]",
                    ConnectionString = "Foo...",
                    ConnectorClassification = "Db",
                    ConnectionProviderType = "SqlServer",
                    DataStoreSerializedIdentifierNames = "Id"

                };
            ds.Validate();
            this.InternalEntities.Add(ds);



            ds = new DataSource
                {
                    Name = "Addresses",
                    DataStoreCatalogName = "MyDb",
                    DataStoreTableName = "Addresses",
                    ConnectionMetaData = "[]",
                    ConnectionString = "Foo...",
                    ConnectorClassification = "Db",
                    ConnectionProviderType = "SqlServer",
                    DataStoreSerializedIdentifierNames = "Id"
                };
            ds.Validate();
            this.InternalEntities.Add(ds);

        }
    }
}
