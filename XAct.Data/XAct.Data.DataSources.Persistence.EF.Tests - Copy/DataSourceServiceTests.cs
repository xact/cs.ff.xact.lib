namespace XAct.Data.Tests
{
    using System;
    using System.Data.Entity.Validation;
    using NUnit.Framework;
    using XAct.Data.DataSources;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DataSourceServiceTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {

            try
            {
                InitializeUnitTestDbIoCContextAttribute.BeforeTest();
            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void DataSourceServiceIsAvailable()
        {
            IDataSourceService dataSourceService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourceService>();


            Assert.IsTrue(dataSourceService != null);
        }


        [Test]
        public void DataSourceServiceIsOfExpectedService()
        {
            IDataSourceService dataSourceService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourceService>();


            Assert.AreEqual(typeof(DataSourceService), dataSourceService.GetType());
        }



        [Test]
        public void CanRetrieveDataSourceByName()
        {
            IDataSourceService dataSourceService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourceService>();

            DataSource dataSource = dataSourceService.GetByText("Contacts");

            Assert.IsNotNull(dataSource);
            Assert.AreEqual("Contacts", dataSource.Name);
        }


        [Test]
        public void CanRetrieveDataSourceById()
        {
            IDataSourceService dataSourceService =
                XAct.DependencyResolver.Current.GetInstance<IDataSourceService>();

            DataSource tmp = dataSourceService.GetByText("Contacts");


            DataSource dataSource = dataSourceService.GetById(tmp.Id);
            
            Assert.IsNotNull(dataSource);
            Assert.AreEqual("Contacts", dataSource.Name);
        }


    }
}


