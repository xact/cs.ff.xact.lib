﻿namespace XAct.Data.DataSources.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Data.DataSources;
    using XAct.Data.DataSources.Initialization.ModelPersistenceMaps;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class DataSourcesDbModelBuilder : IDataSourcesDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourcesDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public DataSourcesDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<DataSource>)XAct.DependencyResolver.Current.GetInstance<IDataSourceModelPersistenceMap>());

        }
    }
}