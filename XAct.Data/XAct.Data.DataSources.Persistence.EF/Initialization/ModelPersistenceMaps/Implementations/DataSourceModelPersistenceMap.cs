﻿namespace XAct.Data.DataSources.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Data.DataSources;

    /// <summary>
    /// 
    /// </summary>
    public class DataSourceModelPersistenceMap : EntityTypeConfiguration<DataSource>, IDataSourceModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceModelPersistenceMap"/> class.
        /// </summary>
        public DataSourceModelPersistenceMap()
        {

            this.ToXActLibTable("DataSource");

            this
                .HasKey(h => h.Id);


            int colOrder = 0;

            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);
            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;

            //Db, Facebook, etc.
            this
                .Property(m => m.ConnectorClassification)
                .IsOptional()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                .IsOptional()
                ;

            //SqlServerProvider, etc.
            this
                .Property(m => m.ConnectionProviderType)
                .IsOptional()
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++)
                .IsOptional()
                ;

            this
                .Property(m => m.ConnectionString)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                .IsOptional()
                ;

            this
                .Property(m => m.DataStoreCatalogName)
                .IsOptional()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                .IsOptional()
                ;

            this
                .Property(m => m.DataStoreTableName)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.DataStoreSerializedIdentifierNames)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.ConnectionMetaData)
                .IsOptional()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                .IsOptional()
                ;
            this
                .Property(m => m.DisplayMetaData)
                .IsOptional()
                .HasMaxLength(64)
// ReSharper disable RedundantAssignment
                .HasColumnOrder(colOrder++)
// ReSharper restore RedundantAssignment
                .IsOptional()
                ;
        }
    }
}
