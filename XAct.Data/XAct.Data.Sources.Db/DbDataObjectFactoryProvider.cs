using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;

using System.Reflection;


namespace XAct.Data {

  /// <summary>
  /// represents a collection of IDataObjectFactory instances.
  /// </summary>
  /// <remarks>
  /// <para>
  /// There will be instantiated and provided as a singleton collection
  /// by the the <see cref="P:DataObjectFactoryManager.Instance"/> property.
  /// </para>
  /// </remarks>
  public class DbDataObjectFactoryProvider : DataObjectFactoryProviderBase {






    /// <summary>
    /// Dictionary of DbDataObjectFactoryDelegate instances. 
    /// Note that a Factory may be registered under one or more names.
    /// </summary>
#if (CE) || (PocketPC) || (pocketPC) || (WindowsCE)
    private static Dictionary<string, ProxyDbDataObjectFactoryDelegate> _Factories = new Dictionary<string, ProxyDbDataObjectFactoryDelegate>(StringComparer.InvariantCultureIgnoreCase);
#else
    private static Dictionary<string, DbDataObjectFactoryDelegate> _Factories = new Dictionary<string, DbDataObjectFactoryDelegate>(StringComparer.InvariantCultureIgnoreCase);
#endif


    #region Properties - Indexer
    /// <summary>
    /// Gets the <see cref="T:XAct.Data.IDataObjectFactory"/> with the specified name (Case-Insensitive).
    /// </summary>
    /// <exception cref="System.Collections.Generic.KeyNotFoundException">An exception is raised if the key is not found.</exception>
    /// <value></value>
    public new IDataObjectFactory this[string name] {
      get {
        return DataObjectFactoryByName[name];
      }
    }
    /// <summary>
    /// Gets the <see cref="T:XAct.Data.IDataObjectFactory"/> with the specified Id.
    /// </summary>
    /// <exception cref="System.Collections.Generic.KeyNotFoundException">An exception is raised if the key is not found.</exception>
    /// <value></value>
    public new IDataObjectFactory this[int id] {
      get {
        return DataObjectFactoryById[id];
      }
    }
    #endregion


		#region Properties
		/// <summary>
		/// Gets the install script prefix to use when searching for embedded/file db scripts.
		/// </summary>
		/// <value>The install script prefix.</value>
		virtual protected string InstallScriptsPrefix {
			get {
				return _InstallScriptsPrefix;
			}
		}
		private string _InstallScriptsPrefix;

		/// <summary>
		/// Gets or sets the param prefix
		/// (default is '@').
		/// </summary>
		/// <value>The param prefix.</value>
		virtual public string ParamPrefix {
			get {
				return _ParamPrefix;
			}
		}
		private string _ParamPrefix;

		/// <summary>
		/// Gets the format code for Guid.ToString() operations.
		/// (default is 'D').
		/// </summary>
		/// <value>The param prefix.</value>
		virtual public string GuidFormatCode {
			get {
				return _GuidFormatCode;
			}
		}
		private string _GuidFormatCode;

		#endregion


    #region Implementation of ProviderBase
    /// <summary>
    /// Gets the friendly name used to refer to the provider during configuration.
    /// </summary>
    /// <value></value>
    /// <returns>The friendly name used to refer to the provider during configuration.</returns>
    public override string Name {
      get {
        return string.IsNullOrEmpty(base.Name) ? "DbDataObjectFactoryProvider" : base.Name;
      }
    }
    #endregion

    #region Properties
    /// <summary>
    /// Gets the Connection string/settings to the DB
    /// that contains the DataFactory settings.
    /// </summary>
    /// <value>The connection settings.</value>
    public ConnectionStringSettings ConnectionSettings {
      get {
        return _ConnectionSettings;
      }
    }
    private ConnectionStringSettings _ConnectionSettings;


    /// <summary>
    /// Gets the common (configurable) db settings that this provider uses.
    /// </summary>
    /// <value>The db settings.</value>
    public DbDataObjectFactoryProviderSettings DbSettings {
      get {
        return _DbSettings;
      }
    }
    private DbDataObjectFactoryProviderSettings _DbSettings;


    /// <summary>
    /// SQL statement to get all DataObjectFactory records.
    /// </summary>
    /// <value>The SQL to get all DataObjectFactory records.</value>
    protected string sqlToGetAllDataFactoryCollection {
      get {
        if (String.IsNullOrEmpty(_sqlToGetAllDataFactoryCollection)) {
          _sqlToGetAllDataFactoryCollection = 
            this.FormatSql(
            "SELECT "+
            " DS.{dataObjectFactoryId}, DS.{dataObjectFactoryName}, DS.{dataObjectFactoryProviderName},"+
            " DS.{dataObjectFactoryConnectionString},"+
            " DS.{dataObjectFactoryTableName}, DS.{dataObjectFactoryKeyColumn}"+
            " FROM" + 
            " {dataObjectFactoryTable} AS DS");
        }
        return _sqlToGetAllDataFactoryCollection;
      }
    }
    private string _sqlToGetAllDataFactoryCollection;

    /// <summary>
    /// Gets the Application's base path.
    /// </summary>
    /// <value>The app dir.</value>
    protected static string AppDir {
      get {
        return XAct.Core.Utils.Env.AppDir;
      }
    }
    #endregion





    
    #region Private Methods

    /// <summary>
    /// Retrieve and instanciate the DbDataObjectFactoryProvider from DB DataFactory table.
    /// </summary>
    public override void InitSources() {

      lock (this) {
        //DO FACTORY:
        //Get the current assembly:
        Assembly assembly = typeof(DbDataObjectFactoryProvider).Assembly;

        
        //And register all Data Factories within it.
        //In other words, register the 'DbDataObjectFactory.CreateFactory' method
        //under both '' (default), and 'db':

        RegisterDataFactories(assembly);



        //Open connection to DataObjectFactory database:
        using (IDbConnection connection = CreateConnection()) {
          using (IDbCommand command = connection.CreateCommand()) {

            command.CommandText = sqlToGetAllDataFactoryCollection;


            //record example:
            //(ID, Name, TableName, KeyColumn)
            //VALUES (1, "Contacts", "Contacts", "ID")

            using (IDataReader reader = command.ExecuteReader()) {
              while (reader.Read()) {
                //Get Provider Name from record (eg: 'db'):
                string dataObjectFactoryProvider=
                  (reader.IsDBNull((int)DbDataObjectFactoryRecordColumnIds.Provider))?
                  String.Empty:
                  reader.GetString((int)DbDataObjectFactoryRecordColumnIds.Provider);

                //Get Factories that are registered as being initialized from IDataRecords:
                //DbDataObjectFactoryDelegate dataObjectFactoryDelegate = 
                  //DbDataObjectFactoryFactories.GetFactory(dataObjectFactoryProvider);


                
                if (!_Factories.ContainsKey(dataObjectFactoryProvider)) {
                  throw new Exception(String.Format("No DataObjectFactory of type '{0}' registered.", dataObjectFactoryProvider));
                }

                IDataObjectFactory ds;

#if (CE) || (PocketPC) || (pocketPC) || (WindowsCE)
                ProxyDbDataObjectFactoryDelegate proxyDelegate = _Factories[dataObjectFactoryProvider];
                ds = proxyDelegate.Invoke(reader); 
#else
                DbDataObjectFactoryDelegate dataObjectFactoryDelegate = _Factories[dataObjectFactoryProvider];

                ds = dataObjectFactoryDelegate(reader);
#endif
                relayEvents(ds);

                this.DataObjectFactoryById.Add(ds.Id, ds);
                this.DataObjectFactoryByName.Add(ds.Name, ds);

#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
  //PC:
                Trace.TraceInformation("DataObjectFactory '{0}' (#{1}) added.", ds.Name, ds.Id);
#endif
              }
            }
          }
        }

#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
  //PC:
        Trace.TraceInformation("{0} DbDataObjectFactoryProvider Loaded", Count);
#endif
      }//endof:lock
    }


    /// <summary>
    /// Creates the connection to the local DB.
    /// Note: The connection is not opened.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Intended for use in a <c>using</c> statement so that the
    /// connection go back to pool after commands execution.
    /// (according to msdn this is a cheap operation)
    /// this is better that having one connection open
    /// because of thread safety reasons (entre autres).
    /// </para>
    /// </remarks>
    /// <returns></returns>
    private IDbConnection CreateConnection() {
      return DbHelpers.CreateConnectionII(_ConnectionSettings);
    }



    #endregion


    #region Public Static Methods - Registration

    /// <summary>
    /// Registers all DbDataObjectFactoryDelegate methods in the given assembly.
    /// </summary>
    /// <param name="assembly">The assembly.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the passed argument is null.</exception>
    public static void RegisterDataFactories(Assembly assembly) {
      if (assembly == null) {
        throw new System.ArgumentNullException("assembly");
      }
      //Get all types in this assembly:
      Type[] types = assembly.GetTypes();

      //Loop through each type:
      foreach (Type type in types) {

        //Loop through each method of each Type:
        foreach (MethodInfo methodInfo in type.GetMethods()) {

          //In order to find methods decorated with 'DbDataObjectFactoryDelegateAttribute':
          foreach (DbDataObjectFactoryDelegateAttribute attribute in methodInfo.GetCustomAttributes(typeof(DbDataObjectFactoryDelegateAttribute), false)) {

            Type delegateType = typeof(DbDataObjectFactoryDelegate);
            //Found one:
            //Register it as an dataObjectFactory creator:

            foreach (string registrationName in attribute.RegistrationNames) {

#if (CE) || (PocketPC) || (pocketPC) || (WindowsCE)
              ProxyDbDataObjectFactoryDelegate proxyDelegate = 
                new ProxyDbDataObjectFactoryDelegate(methodInfo, null);
              RegisterDataObjectFactory(
                registrationName,
                proxyDelegate);
#else
              //The NET 2.0 way:
                DbDataObjectFactoryDelegate myDelegate = 
              (DbDataObjectFactoryDelegate)Delegate.CreateDelegate(delegateType, methodInfo);

              RegisterDataObjectFactory(
                registrationName, 
                myDelegate
                  );
#endif
            }
          }
        }
      }
    }



    /// <summary>
    /// register the DbDataObjectFactoryDelegate Data Source factory by its identifier (dataObjectFactoryName)
    /// </summary>
    /// <param name="dataObjectFactoryName">Name to register the DbDataObjectFactoryDelegate under.</param>
    /// <param name="dataObjectFactoryDelegate">The DbDataObjectFactoryDelegate to register.</param>
    /// <returns>The number of DbDataObjectFactoryDelegate instances currently registered.</returns>
    /// <exception cref="ArgumentException">An exception is raised if a DataObjectFactory by that name has already been registered.</exception>
    public static int RegisterDataObjectFactory(string dataObjectFactoryName, object dataObjectFactoryDelegate) {
      if (dataObjectFactoryName == null) {
        //BUT do not trigger error is '':
        throw new System.ArgumentNullException("dataObjectFactoryName");
      }
      if (dataObjectFactoryDelegate == null) {
        throw new System.ArgumentNullException("dataObjectFactoryDelegate");
      }

      if (_Factories.ContainsKey(dataObjectFactoryName)) {
        throw new ArgumentException(
          string.Format(
          "A dataObjectFactoryDelegate by that name ('{0}') has already been registered.",
          dataObjectFactoryName));
      }

#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
  //PC:
      Trace.TraceInformation("registering DataObjectFactory factory for '{0}'", dataObjectFactoryName);
#endif


#if (CE) || (PocketPC) || (pocketPC) || (WindowsCE)
      _Factories[dataObjectFactoryName] = (ProxyDbDataObjectFactoryDelegate)dataObjectFactoryDelegate;
#else
      _Factories[dataObjectFactoryName] = (DbDataObjectFactoryDelegate)dataObjectFactoryDelegate;
#endif
      return _Factories.Count;
    }

    /*
    /// <summary>
    /// Registers the DataObjectFactory factory (or dataObjectFactoryDelegate) MethodInfo identified by
    /// the dataObjectFactoryName identifier.
    /// </summary>
    /// <param name="dataObjectFactoryName">Name of the factory.</param>
    /// <param name="methodInfo">The methodInfo.</param>
    public static void RegisterDataObjectFactory(string dataObjectFactoryName, MethodInfo methodInfo) {

      if (dataObjectFactoryName == null) {
        //BUT do not trigger error is '':
        throw new System.ArgumentNullException("dataObjectFactoryName");
      }
      if (methodInfo == null) {
        throw new System.ArgumentNullException("methodInfo");
      }

      // Debug.Print("register ({0} : {1})", dataObjectFactoryName, methodInfo);

      RegisterDataObjectFactory(dataObjectFactoryName,
                      delegate(IDataRecord record) {
                        Object[] param = new Object[1];
                        param[0] = record;
                        //Debug.Print("calling DataObjectFactory dataObjectFactoryDelegate : " + methodInfo);
                        return (IDataObjectFactory)methodInfo.Invoke(null, param);
                      });
    }
    */


    #endregion








  }//Class:End
}//Namespace:End