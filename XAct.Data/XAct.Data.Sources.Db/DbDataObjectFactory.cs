using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Text;
using System.Xml;

using XAct.Data;
using XAct.Data.Support;
using XAct.Core.Utils;

namespace XAct.Data {

  /// <summary>
  /// Generic Sql DBMS-based implementation of IDataObjectFactory.
  /// </summary>
  /// <remarks>
  /// <para>
  /// Retrieves and stores a dataObject in an underlying DB table.
  /// </para>
  /// </remarks>
  public class DbDataObjectFactory : IDataObjectFactory {

    //--------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------
    #region Public STATIC Method - Factory Registration
    /// <summary>
    /// Static method implementation of <see cref="T:DbDataObjectFactory"/> delegate.
    /// </summary>
    /// <param name="record"></param>
    /// <returns></returns>
    [DbDataObjectFactoryDelegate("", "db")]
    public static IDataObjectFactory Create(IDataRecord record) {
      if (record == null) {
        throw new System.ArgumentNullException("record");
      }
      return new DbDataObjectFactory(record);
    }
    #endregion
    //--------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------


    #region Events Raised
    /// <summary>
    /// Cancelable event raised before a new dataObject is about to be created.
    /// </summary>
    public event EventHandler<CancelDataObjectEventArgs> DataObjectCreating;
    /// <summary>
    /// Event raised after a new dataObject has been created.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> DataObjectCreated;

    /// <summary>
    /// Cancelable event raised before a dataObject is about to be updated.
    /// </summary>
    public event EventHandler<CancelDataObjectEventArgs> DataObjectUpdating;
    /// <summary>
    /// Event raised after a dataObject has been updated.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> DataObjectUpdated;

    /// <summary>
    /// Cancelable event raised before a dataObject is about to be deleted.
    /// </summary>
    public event EventHandler<CancelDataObjectEventArgs> DataObjectDeleting;
    /// <summary>
    /// Event raised after a dataObject has been deleted.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> DataObjectDeleted;

    #endregion

    #region Fields
    Dictionary<string, string> _SQL = new Dictionary<string, string>();
    #endregion

    #region Properties - Implementation of IDataObjectFactory

    /// <summary>
    /// Id of this DataObjectFactory (must be unique).
    /// </summary>
    /// <value>The Id.</value>
    public int Id {
      get {
        return _Id;
      }
    }
    private int _Id;

    /// <summary>
    /// The name of this DataObjectFactory (must be unique).
    /// eg: 'Contact'.
    /// </summary>
    /// <value>The name.</value>
    public string Name {
      get {
        return _Name;
      }
    }
    private string _Name;


    /// <summary>
    /// Gets the schema that describes the elements in this DataObjectFactory.
    /// </summary>
    /// <value>The schema.</value>
    public DataObjectSchema Schema {
      get {
        return _Schema;
      }
    }
    private DataObjectSchema _Schema;

    #endregion

    #region Protected Properties 
    /// <summary>
    /// Gets the provider that manages this DataObjectFactory.
    /// </summary>
    /// <value>The provider.</value>
    protected DbDataObjectFactoryProvider Provider{
      get {
        return _Provider;
      }
    }
    private DbDataObjectFactoryProvider _Provider;

    /// <summary>
    /// Gets the db settings specific to this DataObjectFactory.
    /// </summary>
		/// <remarks>
		/// <para>
		/// Set by Constructor.
		/// </para>
		/// </remarks>
    /// <value>The db settings.</value>
    protected DbDataObjectFactoryProviderSettings DbSettings {
      get {
        return _DbSettings;
      }
    }
    private DbDataObjectFactoryProviderSettings _DbSettings;
    #endregion


    #region Protected Properties 
    


    /// <summary>
    /// Gets a concatenation of Record field names, with no prefix.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Used by <see cref="CreateCommand_DataObjectRecordCreate"/>.
    /// </para>
    /// </remarks>
    protected string RecordFields {
      get {
        if (String.IsNullOrEmpty(_RecordFields)) {

          StringBuilder fields = new StringBuilder();

          foreach (DataObjectPropertySchema propertySchema in Schema.Properties) {
            fields.Append(string.Format(" {0},", propertySchema.Db.Name));
          }
          //Remove last comma:
          if (fields.Length > 0) {
            fields.Remove(fields.Length - 1, 1);
          }
          //No Need to FormatSql as we are talking about real db column names
          //not {} markers:
          _RecordFields = fields.ToString();
        }
        return _RecordFields;
      }
    }
    private string _RecordFields = null;




    /// <summary>
    /// Get a concatenation of all the Records Fields, prefixed with a DBMS ParamPrefix (ie: '@').
    /// </summary>
    /// <remarks>
    /// <para>
    /// Used by <see cref="CreateCommand_DataObjectRecordCreate"/>.
    /// </para>
    /// </remarks>
    protected string RecordFieldsParams {
      get {
        if (String.IsNullOrEmpty(_RecordFieldsParams)) {
          StringBuilder fields = new StringBuilder();
          foreach (DataObjectPropertySchema propertySchema in Schema.Properties) {
            fields.Append(string.Format(" @{0},", propertySchema.Name));
          }
          //Remove last comma:
          if (fields.Length > 0) {
            fields.Remove(fields.Length - 1, 1);
          }
          //Do not FormatSql yet (or it will be repeated later):
          _RecordFieldsParams = fields.ToString();
        }
        return _RecordFieldsParams;
      }
    }
    private string _RecordFieldsParams = null;


    /*
    /// <summary>
    /// Gets a concatenation of MetaData field names, prefixed with M.
    /// </summary>
    /// <value>The M meta fields.</value>
    protected string MMetaFields {
      get {
        if (String.IsNullOrEmpty(_MMetaFields)) {
          //Format it once now:
          _MMetaFields = FormatSql("M.{MetaDataDateCreated}, M.{MetaDataDateEdited}, M.{metaDataSyncStatus}");
        }
        return _MMetaFields;
      }
    }
    private string _MMetaFields = null;
    */

    /*
    /// <summary>
    /// Gets a LEFT JOIN concatenation of MMetaFields and RRecordFields names.
    /// (Note that it is not an INNER JOIN...!)
    /// </summary>
    protected string MetaDataAndRecordTableNames {
      get {
        if (String.IsNullOrEmpty(_MetaDataAndRecordTableNames)) {
          //INNER JOIN
          _MetaDataAndRecordTableNames = FormatSql(
            "{0} AS R" +
            " LEFT JOIN {MetaDataTable} AS M" +
            " ON (M.{MetaDataDataObjectFactoryId} = {1}" +
            " AND" +
            " M.{MetaDataRecordId} = R.{2})",
            _TableName, _Id, _KeyColumn);
        }
        return _MetaDataAndRecordTableNames;
      }
    }
    private string _MetaDataAndRecordTableNames = null;
    */

    #endregion

    #region Properties - Custom

    /// <summary>
    /// Name of the .NET Db provider used to communicate
    /// with the database where data record are stored.
    /// </summary>
    /// <remarks>
    /// <para>
    /// <c>null</c> if the table is in the same database as the graph database.
    /// </para>
    /// </remarks>
    /// <value>The name of the provider.</value>
    public string ProviderName {
      get {
        return _ProviderName;
      }
    }
    private string _ProviderName;


    /// <summary>
    /// Connection string used to open the database where data records are kept.
    /// </summary>
    /// <remarks>
    /// <para>
    /// <c>null</c> if the table is in the same database as the graph database.
    /// </para>
    /// </remarks>
    /// <value>The connection string.</value>
    /*
    public string ConnectionString {
      get {
        return _ConnectionString;
      }
    }
     */
    private string _ConnectionString;



    /// <summary>
    /// Gets the name of the table where the dataobjects are stored.
    /// </summary>
    /// <value>The name of the table.</value>
    public string TableName {
      get {
        return _TableName;
      }
    }
    private string _TableName;

    /// <summary>
    /// Gets the name of the column in TableName where the dataObject ids are stored.
    /// </summary>
    /// <value>The key column.</value>
    public string KeyColumn {
      get {
        return _KeyColumn;
      }
    }
    private string _KeyColumn;

    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DbDataObjectFactory"/> class
    /// from a Db record.
    /// </summary>
    /// <param name="record">The record.</param>
    public DbDataObjectFactory(IDataRecord record) {


      _Provider =  (DbDataObjectFactoryProvider) DataObjectFactoryManager.Instance;
      _DbSettings = _Provider.DbSettings;

      //Check Args:
      if (record == null) {
        throw new System.Exception("record");
      }

      //Extract and fill local fields:
      _Id = record.GetInt32((int)DbDataObjectFactoryRecordColumnIds.Id);
      _Name = record.GetString((int)DbDataObjectFactoryRecordColumnIds.Name);


      //If no specific connection string provided in db record
      //fall back to same DbProvider used by provider itself
			if (!(record.IsDBNull((int)DbDataObjectFactoryRecordColumnIds.Provider))) {
				_ProviderName =
					record.GetString((int)DbDataObjectFactoryRecordColumnIds.Provider);
			}
			if (string.IsNullOrEmpty(_ProviderName)) {
				_ProviderName = ((DbDataObjectFactoryProvider)DataObjectFactoryManager.Instance).ConnectionSettings.ProviderName;

			}
      

      //If no specific connection string provided in db record
      //fall back to same connection string used by provider itself
      //(in other words, the actual data table is in the 
      //same db as the DataObjectFactory table):
        if (!(record.IsDBNull((int)DbDataObjectFactoryRecordColumnIds.ConnectionSettings))){
      _ConnectionString =
        record.GetString((int)DbDataObjectFactoryRecordColumnIds.ConnectionSettings);
				};

				if (string.IsNullOrEmpty(_ConnectionString)) {
					_ConnectionString = 
					((DbDataObjectFactoryProvider)DataObjectFactoryManager.Instance).ConnectionSettings.ConnectionString;
				}

      _TableName = record.GetString((int)DbDataObjectFactoryRecordColumnIds.TableName);
      _KeyColumn = record.GetString((int)DbDataObjectFactoryRecordColumnIds.KeyColumn);


      _Schema = SchemaManager.DataObjectSchemas[_Name];
    }

    #endregion

    #region Method Overrides

    /// <summary>
    /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
    /// </returns>
    public override string ToString() {
      return String.Format("<DbDataObjectFactory: Id={0}, name={1}, provider={2}, connection={3}, table={4}, key={5}>",
                           _Id, _Name, _ProviderName, _ConnectionString, _TableName, _KeyColumn);
    }

    #endregion

    #region IDataObjectFactory implementation


    /// <summary>
    /// Create a new dataObject (in memory only).
    /// The dataObject should be saved with <see cref="Flush"/> method
    /// to be permanently stored on DataObjectFactory storage
    /// </summary>
    /// <returns>A new empty dataObject</returns>
    public object Create() {
      
      object dataObject = new DataObject(this);//OK

      return dataObject;
    }


    /*
    /// <summary>
    /// Create a new dataObject (in memory only) with the
    /// specified Id.
    /// The dataObject should be saved with <see cref="M:StoreNewDataObject"/> method
    /// to be permanently stored on DataObjectFactory storage
    /// </summary>
    /// <param name="recordId">The record id.</param>
    /// <returns>A new empty dataObject</returns>
    protected object NewDataObject(string recordId) {
      object dataObject =  new DataObject(recordId, this);

			//Mark it as a new record:
      trackingVars.MergeStatus(XPDOTracker.SyncStatus.CreationPending);
      return dataObject;
    }
    */

    /// <summary>
    /// Loads the dataObject in mem from an Xml description.
    /// </summary>
    /// <param name="node">The xml node.</param>
    /// <returns></returns>
    public object Load(XmlNode node) {
      if (node == null) {
        throw new System.ArgumentNullException("node");
      }
      XmlNodeReader reader = new XmlNodeReader(node);
      return Load(reader);
    }

    /// <summary>
    /// Loads the dataObject in mem from an Xml description.
    /// </summary>
    /// <param name="reader">The reader.</param>
    /// <returns></returns>
    public object Load(XmlReader reader) {
      if (reader == null) {
        throw new System.ArgumentNullException("reader");
      }
      DataObject dataObject = new DataObject(this);
			XPDOTracker.XPDOTrackingVars trackingVars = XPDOTracker.Get(dataObject);
      //trackingVars.SyncStatus = XPDOTracker.SyncStatus.Clean;

      dataObject.ReadXml(reader);

      return dataObject;
    }



    /// <summary>
    /// Merges the given status into the <see cref="XPDOTracker.SyncStatus"/>
    /// of the dataObject's <see cref="XPDOTracker.XPDOTrackingVars"/>.
    /// <br/>
    /// For example, if the current flag was Clear, and Update is merged, it will be Update.
    /// <br/>
    /// If the current flag already contains Update, and Update is merged, no change will occur.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Choices to manipulate the status could be:
    /// <code>
    /// <![CDATA[
    /// Set: _SyncStatus = SyncStatus.UpdatePending;
    /// ]]>
    /// </code>
    /// <code>
    /// <![CDATA[
    /// Clear all but the given one, but only if it is already set: 
    /// _SyncStatus &= SyncStatus.UpdatePending;
    /// ]]>
    /// </code>
    /// <code>
    /// <![CDATA[
    /// Merge: _SyncStatus |= SyncStatus.UpdatePending;
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// One then tests for a flag being set by:
    /// <code>
    /// <![CDATA[
    /// if (_SyncStatus & SyncStatus.UpdatePending) != 0) {
    ///   //Update required...
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <param name="dataObject"></param>
    /// <param name="syncStatus"></param>
    public XPDOTracker.SyncStatus MergeStatus(object dataObject, XPDOTracker.SyncStatus syncStatus) {
      return XPDOTracker.MergeStatus(dataObject, syncStatus);
    }



    /// <summary>
    /// Persists changes to DataObject to underlying dataStore.
    /// <para>
    /// <c>Creates</c>, <c>Updates</c>, or <c>Deletes</c> depending on associated 
    /// <see cref="XPDOTracker.XPDOTrackingVars.SyncStatus"/>.
    /// </para>
    /// </summary>
    /// <param name="dataObject">The DataObject to persist.</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">An exception is raised if the dataObject is null.</exception>
    /// <exception cref="ArgumentException">An exception is raised if the dataObject's DataObjectFactory is not this IDataObjectFactory.</exception>
    /// <exception cref="ArgumentException">An exception is raised if the dataObject is not being tracked.</exception>
    public bool Flush(object dataObject) {
      //Check Args:
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }
      //----------------------------------------
      XPDOTracker.XPDOTrackingVars trackingVars =
        XPDOTracker.Get(dataObject);
      //----------------------------------------
      object dataObjectId = this.Schema.PrimaryKey.GetValue(dataObject);
      //----------------------------------------
      XPDOTracker.SyncStatus syncStatus = trackingVars.SyncStatus;
      //----------------------------------------

      bool result;
      result = false;
      if ((syncStatus & XPDOTracker.SyncStatus.LocalPending) == 0) {
        //DataObject has already been persisted to local storage
        //so there is nothing to do:
        result = true;
        return result;
      }


        if ((syncStatus & XPDOTracker.SyncStatus.LocalDeletionPending) != 0) {
          //The dataObject was marked for local deleting...
          //IMPORTANT:
          //This supercedes any LocalCreatePending or LocalUpdatePending flags that may 
          //have been set as well.
          if ((syncStatus & XPDOTracker.SyncStatus.RemoteDeletionPending) != 0) {
            
            //...and is marked for remote deletion as well, 
            //so we can't delete it yet (or we would lose this information upon local shutdown).
            //ie: this is a serious deletion (not just a local cleanup)...
            //So mark the DataObject *record* as needing to be deleted from remote db.
            //(does not delete from local storage yet):

            result = storageDataObjectRecordMarkForDeletion(dataObjectId);
            //This means that the dataObject record .Deleted flag is 1, and therefore filters
            //it out from GetDataObject requests etc...
          }
          else {
            //This is a local delete only (ie a local release of storage resources).
            //And does not have to worry about remote deletion being completed...
            //So just delete locally and we're done:
            result = storageDataObjectRecordDelete(dataObject);
          }


        }
        else if ((syncStatus & XPDOTracker.SyncStatus.LocalCreationPending) != 0) {
          //This dataObject was created in memory, but 
          //was not persisted to local storage yet...
          //So create the new record:
          result = storageDataObjectRecordCreate(dataObject);
          //IMPORTANT:
          //Note that Create persists the same information 
          //as Update, so there is no reason to worry about the Update afterwards...
        }
        else {
          //This dataObject has been persisted to local storage at some point
          //in the past, (ie was not marked as LocalCreationPending),
          //but changes have been made since then...
          //Update the record in local storage:
          Debug.Assert((syncStatus & XPDOTracker.SyncStatus.LocalUpdatePending) != 0);
          result = storageDataObjectRecordUpdate(dataObject);
        }

        //IMPORTANT:
        //Note that 'storageDataObjectRecordCreate' and 'storageDataObjectRecordUpdate'
        //only *sent* flags that had RemotePending still up, but
        //both methods had not modified the actual values of the DataObject.
        //This was so that if they failed, one had nothing to rollback here.
        //On the other hand, one must not forget to update the flags
        //in the DataObject to match what was written to the Db....
        //So....
        //Wrap up by updating the in-memory DataObject SyncStatus, leaving only 
        //the RemotePending flag up if already up (which it will be):
        trackingVars.SyncStatus &= XPDOTracker.SyncStatus.RemotePending;

        return result;
      }//~flush








    /// <summary>
      /// Deletes the dataObject from this <see cref="IDataObjectFactory"/>'s underlying storage.
    /// <para>
    /// NOTE: Does not delete the object from memory -- end programmer has to do that next.
    /// </para>
    /// <para>
    /// Sets the DataObject's <see cref="XPDOTracker.XPDOTrackingVars"/>'s
      /// <see cref="XPDOTracker.XPDOTrackingVars.SyncStatus"/> property 
      /// to <see cref="XPDOTracker.SyncStatus.DeletionPending"/>,
      /// then invokes <see cref="Flush"/>.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Removes the dataObject record (found by its Id) from the underlying data table.
    /// </para>
    /// </remarks>
    /// <param name="dataObject">The dataObject.</param>
    /// <returns></returns>
    /// <remarks>
    /// </remarks>
    /// <exception cref="ArgumentNullException">An exception is raised if the dataObject is null.</exception>
    /// <exception cref="Exception">An exception is raised if the dataObject's DataObjectFactory is not this IDataObjectFactory.</exception>
    public bool Delete(object dataObject) {
      XPDOTracker.Get(dataObject).MergeStatus(XPDOTracker.SyncStatus.DeletionPending);
      return Flush(dataObject);
    }




    /// <summary>
    /// Gets the id of all DataObjects available from this DataObjectFactory
    /// </summary>
    /// <returns>
    /// An enumeration of DataObject Ids (zero or more sized).
    /// </returns>
    public IEnumerable<string> GetDataObjectsIds() {
      return GetDataObjectsIds(new DbDataObjectEmptyFilter(this));
    }


    /// <summary>
    /// Gets the collection of id of the DataObjects that match the given
    /// filter.
    /// </summary>
    /// <param name="filter">The dataObject filter.</param>
    /// <returns>
    /// An enumeration of DataObject Ids (zero or more sized).
    /// </returns>
    public IEnumerable<string> GetDataObjectsIds(IDataObjectFilter filter) {

      //Check Args:
      _CheckDataFilterType(filter);

      List<string> objectsIds = new List<string>();

      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_GetDataObjectIds(connection)) {

          command.CommandText +=
            ((filter == null) || (filter.Empty))
            ?
            string.Empty :
            String.Format(" WHERE ({0})", ((IDbDataObjectFilter)filter).ToSqlWhereClause());



          IDataReader reader = command.ExecuteReader();
          //WARNING: TODO: Can we get past the string?????
          while (reader.Read()) {
						yield return reader.GetString(0);
          }
        }
      }
    }




    /// <summary>
    /// Gets all the DataObjects from this <see cref="IDataObjectFactory"/>.
    /// </summary>
    /// <returns>A <see cref="DataObjectCollection"/>.</returns>
    public DataObjectCollection GetDataObjects() {
      return GetDataObjects(new DbDataObjectEmptyFilter(this));
    }

    /// <summary>
    /// Returns the dataObject associated with the given 
    /// dataObject Id (Not DataObject Id).
    /// </summary>
    /// <param name="dataObjectId">The Id of the dataObject (Not DataObject Id).</param>
    /// <returns></returns>
    /// <remarks>
    /// No error is raised if a dataObject is not found that matches the Id given.
    /// </remarks>
    /// <exception cref="ArgumentNullException">Error raised if the dataObjectId is null or Empty.</exception>
    /// <internal>
    /// TODO: Make ID Non-Guid specific.
    /// </internal>
    public object Get(string dataObjectId) {
      //Check Args:
      if (dataObjectId == null) {
        throw new ArgumentNullException("dataObjectId");
      }
      object dataObject;


      DbDataObjectIdFilter filter = new DbDataObjectIdFilter(this, new object[] { dataObjectId });



      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_GetDataObjectsEx(connection,false)) {

          //-------------------------------------------------------
          //Correct the CommandText in order to add the WHERE and ORDER BY clauses:
          command.CommandText += " WHERE " + filter.ToSqlWhereClause();
          //-------------------------------------------------------

          using (IDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow)) {
            if (reader.Read() == false) {
              return null;
            }

            dataObject = new DataObject(reader, this);
						//XPDOTracker.XPDOTrackingVars trackingVars = XPDOTracker.Get(dataObject);
						//----------------------------------------

          }//~reader
        }//~command
      }//~connection
      return dataObject;
    }


    /// <summary>
    /// Returns a <see cref="DataObjectCollection"/> of 
    /// DataObjects matching the filtering criteria, 
    /// from local DB, and which are not already present in mem 
    /// (i.e. their Id is not in the inMemRecordsIds list)
    /// </summary>
    /// <param name="filter">The filter.</param>
    /// <returns></returns>
    public DataObjectCollection GetDataObjects(IDataObjectFilter filter) {

      //Check Args:
      _CheckDataFilterType(filter);

      
      DataObjectCollection dataObjects = new DataObjectCollection();

      using (IDbConnection connection = CreateConnection()) {

        using (IDbCommand command = CreateCommand_GetDataObjectsEx(connection,true)) {

          //-------------------------------------------------------
          //build WHERE clause:
          string whereClause =            
            ((filter == null) || (filter.Empty))
            ?
            string.Empty :
            String.Format(" WHERE ({0}) ", ((IDbDataObjectFilter)filter).ToSqlWhereClause());


          List<string> orderByColumnNames = new List<string>();
          //-------------------------------------------------------


          //-------------------------------------------------------
          //build ORDER BY clause:
          foreach (DataObjectPropertySchema orderByPropertyInfo in Schema.NaturalSortProperties) {
            RelationshipSchema parentRelationshipSchema;
            string parentTableAlias;
            string parentDataFielAlias;
            GenerateAliasInfo(orderByPropertyInfo, out parentRelationshipSchema, out parentTableAlias, out parentDataFielAlias);
            string orderByColumnName = orderByPropertyInfo.Db.Name;
            /*
            string orderByColumnName = 
              (orderByPropertyInfo!= null)?
              parentDataFielAlias
              :
              orderByPropertyInfo.Db.Name;
            */
            orderByColumnNames.Add(orderByColumnName);
          }

          string orderByClause =
            (orderByColumnNames.Count == 0) 
            ?
            string.Empty :
            string.Format(" ORDER BY {0}", string.Join(", ",orderByColumnNames.ToArray()));
          //-------------------------------------------------------
          //Correct the CommandText in order to add the WHERE and ORDER BY clauses:
          command.CommandText += whereClause + orderByClause;
          //-------------------------------------------------------
					string guidFormatCode = this.Provider.GuidFormatCode;
					//-------------------------------------------------------
					using (IDataReader reader = command.ExecuteReader()) {
          while (reader.Read()) {

            DataObject dataObject = new DataObject(reader, this);
            
						//System.AppDomain.CurrentDomain.DomainUnload
						//System.AppDomain.CurrentDomain.ProcessExit
 
            //Add to result dictionary:
						string dataObjectId = ((Guid)this.Schema.GetPrimaryKeyValue(dataObject)).ToString(guidFormatCode);
            dataObjects.Add(dataObjectId, dataObject);
          }//~read
          }//reader
          //-------------------------------------------------------
        }//~command
      }//~connection

      /*

      //We need to parse through the WHERE clauses
      //watching out for alias structures.
      //Condition can looks like:
      //Category_FK = 3
      //(Category_FK > 4) AND (Category_FK < 10)
      //LastName LIKE 'JOHN%'

      //What we will want instead are:
      //
      //sqlSelectedAliased_Reffed

      string pattern;
      string with;



      //string sqlOrderBy;
      string sOrderBy = string.Empty;
      for (int i = 0; i < orderByColumnNames.Count; i++) {
        string orderByColumnName = orderByColumnNames[i];
        //We need to strip out the leader main name: 
        pattern = "(^|\\s+)(" + mainFieldName + "\\.)";
        with = "$1" + "";
        orderByColumnName = System.Text.RegularExpressions.Regex.Replace(orderByColumnName, pattern, with, System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        //put it back:
        orderByColumnNames[i] = orderByColumnName;

        //What about other names:
        RelationshipSchema[] parentRelationships =
          SchemaManager.Relationships.GetParentRelationships(this.Schema);

        foreach (RelationshipSchema parentRelationship in parentRelationships) {
          string parentTableName = parentRelationship.ParentSchema.DataStoreName;

          pattern = "(^|\\s+)(" + parentTableName + ")(\\.)";
          with = "$1" + parentAliasedTableName + "$3";
          orderByColumnName =
            System.Text.RegularExpressions.Regex.Replace(
            orderByColumnName,
            pattern,
            with,
            System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        }
      }



      //Final step: Is there any ORDER BY parts?
      //Get started
      sqlOrderBy.AddRange(orderBy);
      if (sqlOrderBy.Count > 0) {

        for (int i = 0; i < sqlOrderBy.Count; i++) {
          //We had to do this part by hand watching each var as it goes by because
          //if the OrderBy fieldname was passed in the 'table.field' syntax, 
          //rather than 'fieldonly', we can assume the programmer knows what he 
          //is doing -- but otherwise we have to add a tablename since we all 
          //aliased -- we assume he means the main db table...
          //but don't forget that it is aliased...
          if (((string)sqlOrderBy[i]).IndexOf(".") == -1) {
            sqlOrderBy[i] = dataObjectSchema.DataStoreName + "." + sqlOrderBy[i];
          }
        }
      }

       */

      return dataObjects;
    }

    #endregion

    #region Protected - Raise Events

    /// <summary>
    /// Raises the <see cref="E:DataObjectCreating"/> event.
    /// </summary>
    /// <param name="e">The <see cref="XAct.Data.CancelDataObjectEventArgs"/> instance containing the event data.</param>
    protected virtual void OnDataObjectCreating(CancelDataObjectEventArgs e) {
      if (DataObjectCreating != null) {
        DataObjectCreating(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:DataObjectCreated"/> event.
    /// </summary>
    /// <param name="e">The <see cref="XAct.Data.DataObjectEventArgs"/> instance containing the event data.</param>
    protected virtual void OnDataObjectCreated(DataObjectEventArgs e) {
      if (DataObjectCreated != null) {
        DataObjectCreated(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:DataObjectUpdating"/> event.
    /// </summary>
    /// <param name="e">The <see cref="XAct.Data.CancelDataObjectEventArgs"/> instance containing the event data.</param>
    protected virtual void OnDataObjectUpdating(CancelDataObjectEventArgs e) {
      if (DataObjectUpdating != null) {
        DataObjectUpdating(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:DataObjectUpdated"/> event.
    /// </summary>
    /// <param name="e">The <see cref="XAct.Data.DataObjectEventArgs"/> instance containing the event data.</param>
    protected virtual void OnDataObjectUpdated(DataObjectEventArgs e) {
      if (DataObjectUpdated != null) {
        DataObjectUpdated(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:DataObjectDeleting"/> event.
    /// </summary>
    /// <param name="e">The <see cref="XAct.Data.CancelDataObjectEventArgs"/> instance containing the event data.</param>
    protected virtual void OnDataObjectDeleting(CancelDataObjectEventArgs e) {
      if (DataObjectDeleting != null) {
        DataObjectDeleting(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:DataObjectDeleted"/> event.
    /// </summary>
    /// <param name="e">The <see cref="XAct.Data.DataObjectEventArgs"/> instance containing the event data.</param>
    protected virtual void OnDataObjectDeleted(DataObjectEventArgs e) {
      if (DataObjectDeleted != null) {
        DataObjectDeleted(this, e);
      }
    }

    #endregion


    #region Public Methods - Data Filters

    /// <summary>
    /// Creates an empty dataObject filter for this DataObjectFactory
    /// which match with every DataObjects.
    /// </summary>
    /// <returns>An empty dataObject filter.</returns>
    public IDataObjectFilter CreateEmptyFilter() {
      return new DbDataObjectEmptyFilter(this);
    }

    /// <summary>
    /// Creates a new dataObject filter for this DataObjectFactory
    /// which match with DataObjects whose one of the specified fields
    /// contains the specified string request.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <param name="fieldNames">The field names.</param>
    /// <returns>A new dataObject filter.</returns>
    public IDataObjectFilter CreateStringFilter(string request, IEnumerable<string> fieldNames) {
      return new DbDataObjectStringFilter(this, request, fieldNames);
    }

    #endregion

    #region Protected Methods - Build and Run Commands



    /// <summary>
    /// Creates the dataObject record in the underlying storage.
    /// </summary>
    /// <param name="dataObject"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">An exception is raised if the dataObject is null.</exception>
    /// <exception cref="ArgumentException">An exception is raised if the dataObject's Source is not 'this'.</exception>
    protected virtual bool storageDataObjectRecordCreate(object dataObject) {
      //Check Args:
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }
      //----------------------------------------
      XPDOTracker.XPDOTrackingVars trackingVars =
        XPDOTracker.Get(dataObject);
      //----------------------------------------
      object dataObjectId = this.Schema.PrimaryKey.GetValue(dataObject);
      //----------------------------------------

      //Raise Event before proceeding:
      CancelDataObjectEventArgs e = new CancelDataObjectEventArgs(dataObject, this);
      OnDataObjectCreating(e);
      if (e.Cancel) {
        //Cancelled, so get out now:
        return false;
      }

      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {

          //----------------------------------------
          //UPDATE METATABLE FIRST:
          using (IDbCommand command = CreateCommand_MetaDataRecordCreate(connection, dataObjectId, trackingVars)) {
            command.Transaction = transaction;
            command.ExecuteNonQuery();
          }

          //----------------------------------------
          //UPDATE DATATABLE:
          using (IDbCommand command = CreateCommand_DataObjectRecordCreate(connection, dataObject)) {
            command.Transaction = transaction;
            command.ExecuteNonQuery();
          }
          //----------------------------------------


          transaction.Commit();
        }
      }

      OnDataObjectCreated(new DataObjectEventArgs(dataObject, this));

      //Return that we got this far:
      return true;
    }

    /// <summary>
    /// Updates the dataObject record in the underlying storage.
    /// </summary>
    /// <param name="dataObject">The dataObject.</param>
    /// <returns></returns>
    /// <internal>
    /// TODO: think of making this method asynchronous.
    /// </internal>
    /// <exception cref="ArgumentNullException">An exception is raised if the dataObject is null.</exception>
    /// <exception cref="ArgumentException">An exception is raised if the dataObject's Source is not 'this'.</exception>
    protected virtual bool storageDataObjectRecordUpdate(object dataObject) {
      //Check Args:
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }
      //----------------------------------------
      XPDOTracker.XPDOTrackingVars trackingVars =
        XPDOTracker.Get(dataObject);
      //----------------------------------------
      object dataObjectId = this.Schema.PrimaryKey.GetValue(dataObject);
      //----------------------------------------

      //Raise cancellable Event prior to action: 
      CancelDataObjectEventArgs e = new CancelDataObjectEventArgs(dataObject, this);
      OnDataObjectUpdating(e);
      if (e.Cancel) {
        return false;
      }

      //----------------------------------------
      trackingVars.DateEdited = DateTime.Now;


      //----------------------------------------
      using (IDbConnection connection = CreateConnection()) {
        //----------------------------------------
        using (IDbTransaction transaction = connection.BeginTransaction()) {
          //----------------------------------------
          //UPDATE METATABLE FIRST:
          if (!trackingVars.TrackingTableRecordFound) {
            using (IDbCommand command = CreateCommand_MetaDataRecordCreate(connection, dataObjectId, trackingVars)) {
              command.Transaction = transaction;
              command.ExecuteNonQuery();
              //And mark that we have done it!
              trackingVars.TrackingTableRecordFound = true;
            }
          }
          using (IDbCommand command = CreateCommand_MetaDataRecordUpdate(connection, dataObjectId, trackingVars)) {
            command.Transaction = transaction;
            command.ExecuteNonQuery();
          }
          //----------------------------------------
          //UPDATE DATATABLE:
          using (IDbCommand command = CreateCommand_DataObjectRecordUpdate(connection, dataObject)) {
            command.Transaction = transaction;
            command.ExecuteNonQuery();
          }
          //----------------------------------------
          transaction.Commit();
        }
      }

      OnDataObjectUpdated(new DataObjectEventArgs(dataObject, this));
      return true;
    }




    /// <summary>
    /// Mark the DataObject's record for deletion from the local storage.
    /// <para>
    /// IMPORTANT: that this is not a Delete, but a setting of the record's Deleted flag to true.
    /// </para>
    /// <para>
    /// NOTE: 
    /// The argument is for the ID's of the DataObject -- *not* the DataObject's themselves.
    /// </para>
    /// </summary>
    /// <param name="dataObjectIds">The Collection of DataObject's </param>
    /// <exception cref="ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
    protected virtual bool storageDataObjectRecordMarkForDeletion(params object[] dataObjectIds) {
      //Check Args:
      if (dataObjectIds == null) {
        throw new System.ArgumentNullException("dataObjectIds");
      }
      if (dataObjectIds.Length == 0) {
        //Get out early: 
        return true;
      }

      return storageDataObjectRecordDelete(dataObjectIds);
      /*
      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_DataObjectRecordMarkForDeletion(connection, dataObjects)) {
          command.ExecuteNonQuery();
        }
      }
       */


    }

    /// <summary>
    /// Delete the DataObject's record from the local storage.
    /// <para>
    /// </para>
    /// </summary>
    /// <param name="dataObject">The the DataObject.</param>
    /// <exception cref="ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
    protected virtual bool storageDataObjectRecordDelete(object dataObject) {
      //Check Args:
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }
      //----------------------------------------
      XPDOTracker.XPDOTrackingVars trackingVars =
        XPDOTracker.Get(dataObject);
      //----------------------------------------
      object dataObjectId = this.Schema.PrimaryKey.GetValue(dataObject);
      //----------------------------------------


      //Raise Event before:
      CancelDataObjectEventArgs e = new CancelDataObjectEventArgs(dataObject, this);
      OnDataObjectDeleting(e);
      if (e.Cancel) {
        return false;
      }


      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {
          //----------------------------------------
          //UPDATE DATATABLE:
          using (IDbCommand command = CreateCommand_DataObjectRecordDelete(connection, dataObject)) {
            command.Transaction = transaction;
            command.ExecuteNonQuery();
          }
          //----------------------------------------
          //UPDATE METATABLE:
          using (IDbCommand command = CreateCommand_MetaDataRecordDelete(connection, dataObjectId, trackingVars)) {
            command.Transaction = transaction;
            command.ExecuteNonQuery();
          }
          //----------------------------------------

          transaction.Commit();
        }
      }

      OnDataObjectDeleted(new DataObjectEventArgs(dataObject, this));
      return true;
    }

    #endregion

    #region Protected Methods - CreateCommands
    /// <summary>
    /// Creates an <see cref="IDbCommand"/> that retrieves DataObjects 
    /// </summary>
    /// <param name="connection">The connection to use in order to build the IDbCommand</param>
    /// <returns></returns>
    protected IDbCommand CreateCommand_GetDataObjectIds(IDbConnection connection) {
      IDbCommand command = connection.CreateCommand();

      if (!_SQL.ContainsKey("CreateCommand_GetDataObjectIds")) {
        _SQL["CreateCommand_GetDataObjectIds"] =
                    FormatSql(
                      "SELECT R.{0}" +
                      " FROM {1} AS R",
                      _KeyColumn,
                      _TableName);
      }


      command.CommandText = _SQL["CreateCommand_GetDataObjectIds"];
      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Generates the SQL statement from according to the qConditions given and (optionally) ordered.
    /// </summary>
    /// <param name="connection">The connection to use in order to build the IDbCommand</param>
    /// <param name="retrieveExtended">Flag to retrieve Parent table Data column values that match this Child table's FK values.</param>
    /// <returns></returns>
    /// <remarks>
    /// <para>
    /// This static method only creates the Statement -- it does not run it.
    /// </para>
    /// <para>
    /// The Extended flag extends the SQL generated to getting Extended ('derived') values as well.
    /// To understand what we mean by this, let's consider the following example:
    /// </para>
    /// <code>
    /// class Contact {
    ///		public System.Guid GUID {get {return _GUID;}set{_GUID=value;}
    ///		private System.Guid _GUID=System.Guid.Empty;
    ///		public string NameFirst {get {return _NameFirst;}set {_NameFirst=value;}
    ///		private string _NameFirst=string.Empty;
    ///		public string NameLast {get {return _NameLast;}set {_NameLast=value;}
    ///		private string _NameLast=string.Empty;
    ///		public System.Guid CategoryID {get {return _CategoryID;}set {_CategoryID=value;}
    ///		private System.Guid _CategoryID=string.Empty;
    ///		//And finally...
    ///		public string Category {get {return _Category;}set {_Category=value;}
    ///		private string _Category=string.Empty;
    /// }
    /// </code>
    /// <para>
    /// in a database that has two tables:
    /// </para>
    /// <code>
    ///		Contact [GUID|NameFirst|NameLast|ContactID]
    ///		ContactType [GUID|Description]
    /// </code>
    /// <para>
    /// The above really hase only 4 fields in the database (GUID, NameFirst, NameLast, CategoryID).
    /// </para>
    /// <para>
    /// The fourth is not actually in the database -- its what we call a 'derived' property: it is the ContactType:Description that matches
    /// the ContactType:GUID that _CategoryID points to.
    /// </para>
    /// <para>
    /// When the Extended flag is false, the SQL generated, it is of the garden variety type:
    /// </para>
    /// <code>
    /// <![CDATA[
    /// SELECT * FROM Contact WHERE GUID = 'xxxx-.....' ;
    /// ]]>
    /// </code>
    /// <para>
    /// This fills the first four params -- in other words, <c>CategoryID</c>, but not <c>Category</c>. 
    /// </para>
    /// <para>
    /// This is fine for most scenarios, as it is as fast as the DBMS can get. 
    /// But it is not enough to automatically make viewing of the record easy to generate. 
    /// What is needed in such cases is a join statement to fetch the 
    /// record's values -- PLUS its foreign information.
    /// </para>
    /// <code>
    /// <![CDATA[
    /// SELECT 
    ///		D.*, 
    ///		ContactCategory_FK_TBL.Name as ContactCategory_FK_FD 
    ///		Director_FK_TBL.Name as Director_FK_FD 
    ///		Manager_FK_TBL.Name as Manager_FK_FD 
    ///	FROM 
    ///		Contact as D
    /// INNER JOIN 
    ///		ContactType AS ContactCategory_FK_TBL 
    ///     ON (ContactCategory_FK = ContactCategory_FK_TBL.ID) ,
    ///   Employees AS Director_FK_TBL
    ///     ON (Director_FK = Directory_FK_TBL.ID) ,
    ///   Employees AS Manager_FK_TBL
    ///     ON (Manager_FK = Manager_FK_TBL.ID)
    ///	WHERE 
    ///		GUID = 'xxxx-....'
    /// ]]>
    /// </code>
    /// <para>
    /// As you can see this will provide 5 values -- 4 from the original table (Contact) and 1
    /// from the ContactType table -- and therefore fills all values, even the derived property, 
    /// at once.
    /// </para>
    /// </remarks>
    /// <internal>
    /// <para>
    /// If conditions is null, fix:
    /// Let's talk about those conditions for a sec:
    /// This function is called most often to get a single record.
    /// in other words, the conditions passed are something like:
    /// "GUID = "xxxx-.....\""
    /// If this method were being called to load a collection, the 
    /// conditions might look like:
    /// "D.ContactID = \"13\" AND D.ManagerID=\"18\""
    /// More omplicated would be
    /// "TBL_Contact_FK.Name = \"John\"";
    /// </para>
    /// </internal>
    protected virtual IDbCommand CreateCommand_GetDataObjectsEx(IDbConnection connection, bool retrieveExtended) {

      //Build command that we will be returning:
      IDbCommand command = connection.CreateCommand();


        if (!_SQL.ContainsKey("CreateCommand_GetDataObjects_Extended")) {


          //List of columns that belong to this Business Object.
          //Format is: 'FieldDbName'.
          List<string> sqlSelectedColumns_Main = new List<string>();

          //List of referenced Data columns in Parent Tables.
          //Format is: 'ParentTableNameAlias.ParentDataFieldAlias'.
          List<string> sqlSelectedAliased_Meta = new List<string>();

          //List of referenced Data columns in Parent Tables.
          //Format is: 'ParentTableNameAlias.ParentDataFieldAlias'.
          List<string> sqlSelectedAliased_Reffed = new List<string>();

          //List of complete JOIN statements of
          //'ParentTableName AS ParentTableAlias ON 
          List<string> sqlJoinStatements_Meta = new List<string>();
          //List of complete JOIN statements of
          //'ParentTableName AS ParentTableAlias ON 
          List<string> sqlJoinStatements_Reffed = new List<string>();


          List<string> sqlSelectedColumns_MetaMain = new List<string>();
          List<string> sqlSelectedColumns_MetaMainReffed = new List<string>();



          //Right...we now go through internal schema:
          foreach (DataObjectPropertySchema propertySchema in this.Schema.Properties) {

            string mainFieldName = propertySchema.Db.Name;

            //Add this property to send off:
            sqlSelectedColumns_Main.Add("R." + mainFieldName);


            //string aliasedFieldName;

            RelationshipSchema parentRelationshipSchema;
            string parentTableName;
            string parentTableAlias;
            string parentIdFieldName;
            string parentDataFieldName;
            string parentDataFieldAlias;

            if (GenerateAliasInfo(
              propertySchema,
              out parentRelationshipSchema,
              out parentTableAlias,
              out parentDataFieldAlias
              )
              ) {
              parentTableName = parentRelationshipSchema.ParentSchema.DataStoreName;
              parentIdFieldName = parentRelationshipSchema.ParentKeyPropertySchema.Db.Name; 
              parentDataFieldName = parentRelationshipSchema.ParentDataPropertySchema.Db.Name;

              //Make SELECT fragment ('aliasedTableName.parentFieldName as aliasesFieldName'):
              sqlSelectedAliased_Reffed.Add(
                string.Format(
                "{0}.{1} AS {2}",
                parentTableAlias,
                parentDataFieldName,
                parentDataFieldAlias));


              //Make INNER JOIN statement:
              //"INNER JOIN Y AS Y1 ON a = Y1.b" 
              sqlJoinStatements_Reffed.Add(
                string.Format(
                " INNER JOIN {0} AS {1} ON R.{2}={3}.{4}",
                parentTableName, //Y
                parentTableAlias, //Y1
                mainFieldName,  //a
                parentTableAlias, //Y1
                parentIdFieldName)); //b //CHECK!!!

            }

          }//~loop:propertySchemas


          sqlSelectedAliased_Meta.Add("M.{MetaDataDateCreated} AS META_DateCreated");
          sqlSelectedAliased_Meta.Add("M.{MetaDataDateEdited} AS META_DateEdited");
          sqlSelectedAliased_Meta.Add("M.{MetaDataSyncStatus} AS META_SyncStatus");

          sqlJoinStatements_Meta.Add(
            FormatSql(
            " LEFT JOIN {MetaDataTable} AS M"+
            " ON"+
            " (M.{MetaDataDataObjectFactoryId}={0})"+
            " AND"+
            " (M.{MetaDataRecordId}=R.{1})",
                _Id, //Y1
                _KeyColumn)); //b //CHECK!!!


          //SELECT R.ID, R.FirstName, R.LastName, R.Email, R.Phone, 
          //M.{MetaDataSyncStatus} AS META_SyncStatus, 
            //M.{MetaDataCreated} AS META_DateCreated, 
              //M.{MetaDataCreated} AS META_DateEdited 
          //FROM Contacts AS R  LEFT JOIN MetaData AS M ON (M.DataSourceId=1) AND (M.RecordId=D.ID)

          sqlSelectedColumns_MetaMain.AddRange(sqlSelectedAliased_Meta);
          sqlSelectedColumns_MetaMain.AddRange(sqlSelectedColumns_Main);


          //Combine the Main column names with the extended column names
          //with the main fields first:
          sqlSelectedColumns_MetaMainReffed.AddRange(sqlSelectedColumns_MetaMain); //"a,b,c" from main table
          sqlSelectedColumns_MetaMainReffed.AddRange(sqlSelectedAliased_Reffed); //"Y1.a"

          //Combine all together:
          //Which gives:
          //SELECT a,b,c, Y1.a FROM X INNER JOIN Y AS Y1 ON X.a = Y1.b 

          
          _SQL["CreateCommand_GetDataObjects_Simple"] =
          FormatSql(
            "SELECT {0} FROM {1} AS R {2}",
            FormatSql(string.Join(", ", sqlSelectedColumns_MetaMain.ToArray())),
            this.Schema.DataStoreName,
            FormatSql(string.Join("", sqlJoinStatements_Meta.ToArray()))
            );
  

          _SQL["CreateCommand_GetDataObjects_Extended"] =
          FormatSql(
            "SELECT {0} FROM {1} AS R {2}{3}",
            FormatSql(string.Join(", ", sqlSelectedColumns_MetaMainReffed.ToArray())),
            this.Schema.DataStoreName,
            FormatSql(string.Join("", sqlJoinStatements_Meta.ToArray())),
            FormatSql(string.Join("", sqlJoinStatements_Reffed.ToArray()))
            );
          

        }//~_SQL["CreateCommand_GetDataObjects_Extended"]

      command.CommandText = (!retrieveExtended)?
        _SQL["CreateCommand_GetDataObjects_Simple"]
        :
        _SQL["CreateCommand_GetDataObjects_Extended"];


      //((sqlOrderBy.Count > 0) ? " ORDER BY " + string.Join(", ", sqlOrderBy) : string.Empty)

      //You won't believe this...but we are done:
      SqlTrace.TraceCommand(command);
      return command;
    }



    
    /// <summary>
    /// Creates an <see cref="IDbCommand"/> for creating a new DataObject.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="M:storageDataObjectRecordCreate"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="dataObject"></param>
    /// <returns>An <see cref="IDbCommand"/> with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObject argument is null.</exception>
    protected virtual IDbCommand CreateCommand_DataObjectRecordCreate(IDbConnection connection, object dataObject) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }

      IDbCommand command = connection.CreateCommand();

      if (!_SQL.ContainsKey("CreateCommand_DataObjectRecordCreate")) {
        _SQL["CreateCommand_DataObjectRecordCreate"] =
          FormatSql(
          "INSERT INTO {0}" +
          " ({1})" +
          "VALUES" +
          " ({2})",
          _TableName,
          this.RecordFields,
          this.RecordFieldsParams);
      }


      //Create Data Row (eg: Contact/Task/whatever):
      command.CommandText = _SQL["CreateCommand_DataObjectRecordCreate"];

      //Now wire up parameters:
      foreach (DataObjectPropertySchema propertySchema in Schema.Properties) {
        //For CREATE statements, we include the Primary Key (unlike for UPDATEs):
        object fieldVal = propertySchema.GetValue(dataObject);
        DbHelpers.AttachParam(command, propertySchema.Name, fieldVal);
      }

      SqlTrace.TraceCommand(command);
      return command;
    }





    /// <summary>
    /// Creates an <see cref="IDbCommand"/> for updating a single existing DataObject.
    /// 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="M:CreateCommand_DataObjectRecordCreate"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="dataObject"></param>
    /// <returns>An <see cref="IDbCommand"/> with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObject argument is empty.</exception>
    protected virtual IDbCommand CreateCommand_DataObjectRecordUpdate(IDbConnection connection, object dataObject) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }

      object dataObjectId = this.Schema.PrimaryKey.GetValue(dataObject);


      DataObjectPropertySchemaCollection propertySchemas = this.Schema.Properties;

      if (!_SQL.ContainsKey("CreateCommand_UpdateDataObjectRecord")) {
        StringBuilder sb = new StringBuilder();
        sb.Append(string.Format("UPDATE {0} SET ", _TableName));

        int count = 0;
        foreach (DataObjectPropertySchema propertySchema in propertySchemas) {
          //We never update the primary key (its used as a WHERE): 
          if (propertySchema == this.Schema.PrimaryKey) {
            continue;
          }
          //Append the record:
          sb.Append(string.Format(" {0} = @{1},", propertySchema.Db.Name, propertySchema.Name));
          ++count;
        }
        //Remove last comma:
        if (count > 0) {
          sb.Remove(sb.Length - 1, 1);
        }
        //Finish up with a WHERE:
        sb.Append(string.Format(" WHERE ({0} = @recordId)", _KeyColumn));

        _SQL["CreateCommand_UpdateDataObjectRecord"]
          = FormatSql(sb.ToString());
      }

      // and update data
      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_UpdateDataObjectRecord"];

      //Have SQL, now add parameters:
      foreach (DataObjectPropertySchema propertySchema in propertySchemas) {
        if (propertySchema == this.Schema.PrimaryKey) {
          continue;
        }
        //Param names have to match what was set in sqlUpdateDataRecordTemplate,
        //(which was the param name):

        object fieldVal = propertySchema.GetValue(dataObject);

        DbHelpers.AttachParam(command, propertySchema.Name, fieldVal);
      }

      DbHelpers.AttachParam(command, "recordId", dataObjectId);

      SqlTrace.TraceCommand(command);
      return command;
    }





    /// <summary>
    /// Creates an <see cref="IDbCommand"/> to delete an DataObject record.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageDataObjectRecordDelete"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="dataObject">The dataObject.</param>
    /// <returns>An <see cref="IDbCommand"/> with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjects argument is null.</exception>
    protected virtual IDbCommand CreateCommand_DataObjectRecordDelete(IDbConnection connection, object dataObject) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }

      object dataObjectId = this.Schema.PrimaryKey.GetValue(dataObject);

      string primaryKeyDbColumnName = _KeyColumn;//Schema.PrimaryKey.Db.Name;

      if (!_SQL.ContainsKey("CreateCommand_DataObjectRecordDelete")) {
        _SQL["CreateCommand_DataObjectRecordDelete"] =
            FormatSql(
            "DELETE FROM {0}" +
            " WHERE" +
            " {1}=@recordId",
            _TableName,
            primaryKeyDbColumnName
            );
      }


      IDbCommand command = connection.CreateCommand();
      command.CommandText = _SQL["CreateCommand_DataObjectRecordDelete"];
      DbHelpers.AttachParam(command, primaryKeyDbColumnName, dataObjectId);

      SqlTrace.TraceCommand(command);
      return command;
    }




    #endregion

    #region Protected Methods - CreateCommands - MetaData Table

    /// <summary>
    /// Creates an <see cref="IDbCommand"/> for creating an new MetaData Record for a single DataObject.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="M:CreateCommand_VertexCreateRecord"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="dataObjectId">The data object id.</param>
    /// <param name="trackingVars">The tracking vars.</param>
    /// <returns>An <see cref="IDbCommand"/> with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectId argument is null or empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection trackingVars is null.</exception>
    protected virtual IDbCommand CreateCommand_MetaDataRecordCreate(IDbConnection connection, object dataObjectId, XPDOTracker.XPDOTrackingVars trackingVars) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObjectId == null) {
        throw new System.ArgumentNullException("dataObjectId");
      }
      if (trackingVars == null) {
        throw new System.ArgumentNullException("trackingVars");
      }

      if (!_SQL.ContainsKey("CreateCommand_MetaDataRecordCreate")) {
        _SQL["CreateCommand_MetaDataRecordCreate"] =
              FormatSql(
            "INSERT INTO {MetaDataTable}" +
            " (" +
            " {MetaDataDataObjectFactoryId}," +
            " {MetaDataRecordId}," +
            " {MetaDataDateCreated}," +
            " {MetaDataDateEdited}," +
            " {MetaDataSyncStatus}," +
            " {MetaDataDeleted})" +
            " VALUES" +
            " (" +
            " @metaDataDataObjectFactoryId," +
            " @metaDataRecordId," +
            " @metaDataDateCreated," +
            " @metaDataDateEdited," +
            " @metaDataSyncStatus," +
            " @metaDataDeleted" +
            ")");
      }
      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_MetaDataRecordCreate"];


      DbHelpers.AttachParam(command, "metaDataDataObjectFactoryId", _Id);
      DbHelpers.AttachParam(command, "metaDataRecordId", dataObjectId);

      DbHelpers.AttachParam(command, "metaDataDateCreated", trackingVars.DateCreated);
      DbHelpers.AttachParam(command, "metaDataDateEdited", trackingVars.DateEdited);

      //IMPORTANT:
      //The write happens in two steps:
      //First we write the value to the DbParam leaving only the Remote flags up.
      //If it *FAILS*, the object has not been modified, so need to save/rollback the Vertex values as well.
      //If it succeeds, we then have to do the changes to the in-mem flags in a secondary step:
      DbHelpers.AttachParam(command, "metaDataSyncStatus", (int)(trackingVars.SyncStatus & XPDOTracker.SyncStatus.RemotePending));

      DbHelpers.AttachParam(command, "metaDataDeleted", false);

      SqlTrace.TraceCommand(command);
      return command;
    }





    /// <summary>
    /// Creates an <see cref="IDbCommand"/> for updating an existing MetaData Record of a single DataObject.
    /// </summary>
    /// <param name="connection">The connection.</param>
    /// <param name="dataObjectId">The data object id.</param>
    /// <param name="trackingVars">The tracking vars.</param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectId argument is null or empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection trackingVars is null.</exception>
    protected virtual IDbCommand CreateCommand_MetaDataRecordUpdate(IDbConnection connection, object dataObjectId, XPDOTracker.XPDOTrackingVars trackingVars) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObjectId == null) {
        throw new System.ArgumentNullException("dataObjectId");
      }
      if (trackingVars == null) {
        throw new System.ArgumentNullException("trackingVars");
      }

      if (!_SQL.ContainsKey("CreateCommand_UpdateMetaDataRecord")) {
        _SQL["CreateCommand_UpdateMetaDataRecord"] =
            FormatSql(
            "UPDATE {metaDataTable}" +
            " SET" +
            " {metaDataDateEdited}=@metaDataDateEdited," +
            " {metaDataSyncStatus}=@metaDataSyncStatus" +
            " WHERE " +
              " (" +
              " {MetaDataDataObjectFactoryId}=@metaDataDataObjectFactoryId" +
              " AND" +
              " {metaDataRecordId}=@metaDataRecordId" + 
              " )" +
              ""
            );
      }
      IDbCommand command = connection.CreateCommand();


      command.CommandText = _SQL["CreateCommand_UpdateMetaDataRecord"];
      DbHelpers.AttachParam(command, "metaDataDateEdited", trackingVars.DateEdited);
      //IMPORTANT:
      //The write happens in two steps:
      //First we write the value to the DbParam leaving only the Remote flags up.
      //If it *FAILS*, the object has not been modified, so need to save/rollback the MetaData values as well.
      //If it succeeds, we then have to do the changes to the in-mem flags in a secondary step:
      DbHelpers.AttachParam(command, "metaDataSyncStatus", (int)(trackingVars.SyncStatus & XPDOTracker.SyncStatus.RemotePending));

      //WHERE Condition:
      DbHelpers.AttachParam(command, "metaDataDataObjectFactoryId", _Id);
        //Save the RecordId as a String representation of a Guid:
        //TODO: This could be improved if we update CreateParam method to allow for defining type:
      DbHelpers.AttachParam(command, "metaDataRecordId", dataObjectId);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an <see cref="IDbCommand"/> to mark edge records for deletion.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageDataObjectRecordMarkForDeletion"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="dataObjectId">The data object id.</param>
    /// <param name="trackingVars">The tracking vars.</param>
    /// <returns>An <see cref="IDbCommand"/> with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectId argument is null or empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection trackingVars is null.</exception>
    protected virtual IDbCommand CreateCommand_MetaDataRecordMarkForDeletion(IDbConnection connection, object dataObjectId, XPDOTracker.XPDOTrackingVars trackingVars) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObjectId == null) {
        throw new System.ArgumentNullException("dataObject");
      }
      if (trackingVars == null) {
        throw new System.ArgumentNullException("trackingVars");
      }


      if (!_SQL.ContainsKey("CreateCommand_MetaDataRecordMarkForDeletion")) {
        _SQL["CreateCommand_MetaDataRecordMarkForDeletion"] =
            FormatSql(
            "UPDATE {metaDataTable}" +
            " SET" +
            " {metaDataDeleted}=@metaDataDeleted," + //1
            " {metaDataSyncStatus}=@metaDataSyncStatus" +
            " WHERE" +
            " (" +
            " {MetaDataDataObjectFactoryId}=@metaDataDataObjectFactoryId" +
            " AND" +
            " {metaDataRecordId}=@metaDataRecordId" + 
            " )"
            );
      }
      //WARNING: This will only update records that have bloody metatags...
      //MIGHT have to first scan to find missings, and create them, to keep consistency?

      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_MetaDataRecordMarkForDeletion"];



      DbHelpers.AttachParam(command, "metaDataDeleted", true);
      //IMPORTANT:
      //The write happens in two steps:
      //First we write the value to the DbParam leaving only the Remote flags up.
      //If it *FAILS*, the object has not been modified, so need to save/rollback the DataObject values as well.
      //If it succeeds, we then have to do the changes to the flags in a secondary step:

      //SyncStatus.RemoteDeletionPending;

      //HACK:
      //Do to batching, and therefore no longer being able to address each edge
      //individually, I can no longer do:
      //DbHelpers.AttachParam(command, "edgeSync", (int)(((ICachedElement)edge).SyncStatus & SyncStatus.RemotePending));
      //But we know that the dataObject is marked to be deleted...which preceeds
      //all other operations...so, we can just go ahead and strip out any other
      //values that were there before:
      DbHelpers.AttachParam(command, "metaDataSyncStatus", XPDOTracker.SyncStatus.RemotePending);

      //WHERE Conditions:
      DbHelpers.AttachParam(command, "metaDataDataObjectFactoryId", _Id);
      DbHelpers.AttachParam(command, "metaDataRecordId", dataObjectId);

      SqlTrace.TraceCommand(command);
      return command;
    }

    
    /// <summary>
    /// Creates an <see cref="IDbCommand"/> to delete MetaData table records
    /// for records belonging to this DataObjectFactory, and with
    /// the specified 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageDataObjectRecordDelete"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="dataObjectId">The data object id.</param>
    /// <param name="trackingVars">The tracking vars.</param>
    /// <returns>An <see cref="IDbCommand"/> with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectId argument is null or empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection trackingVars is null.</exception>
    protected virtual IDbCommand CreateCommand_MetaDataRecordDelete(IDbConnection connection, object dataObjectId, XPDOTracker.XPDOTrackingVars trackingVars) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObjectId == null) {
        throw new System.ArgumentNullException("dataObjectId");
      }
      if (trackingVars == null) {
        throw new System.ArgumentNullException("trackingVars");
      }


      if (!_SQL.ContainsKey("CreateCommand_MetaDataRecordDelete")) {
        _SQL["CreateCommand_MetaDataRecordDelete"] =
            FormatSql(
            "DELETE FROM {metaDataTable}" +
            " (" +
            " {MetaDataDataObjectFactoryId}=@metaDataDataObjectFactoryId" +
            " AND" +
            " {metaDataRecordId}=@metaDataRecordId" +
            " )"
            );
      }


      IDbCommand command = connection.CreateCommand();


      command.CommandText = _SQL["CreateCommand_MetaDataRecordDelete"];

      //WHERE Conditions:
      DbHelpers.AttachParam(command, "metaDataDataObjectFactoryId", _Id);
      DbHelpers.AttachParam(command, "metaDataRecordId", dataObjectId);

      SqlTrace.TraceCommand(command);
      return command;
    }





    /// <summary>
    /// Creates an <see cref="IDbCommand"/> for creating 
    /// a table to persist a DataObject.
    /// <para>
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// This method can create generic portable SQL -- but 
    /// it cannot update the table if the schema changes.
    /// </para>
    /// <para>
    /// For this reason, this method is considered 
    /// unreliable, and currently unused.
    /// </para>
    /// <para>
    /// It works by scanning each 
    /// <see cref="DataObjectSchema.Properties"/> 
    /// in this DbDataObjectFactory's 
    /// <see cref="DataObjectPropertySchema"/> 
    /// and creates the SQL string fragment to 
    /// describe it from the various properties 
    /// of the 
    /// <see cref="DataObjectSchema.Properties"/> 
    /// (eg: DataObjectPropertySchema.Db.Name, 
    /// DataObjectPropertySchema.Db.Type, 
    /// DataObjectPropertySchema.Db.IsKey, DataObjectPropertySchema.Db.IsPrimaryInfo, etc.)
    /// </para>
    /// <para>
    /// The resulting string fragment of a class like this:
    /// <code>
    /// <![CDATA[
    /// class MyClass {
    ///		public System.Guid GUID {get return _GUID;}set{_GUID=value;}}
    ///		private System.Guid _GUID;
    ///		
    ///		public string NameFirst {get {return _NameFirst;}set{_NameFirst =value;}}
    ///		private string _NameFirst;
    ///		
    ///		public string NameLast {get {return _NameFirst;}set{_NameFirst =value;}}
    ///		private string _NameLast;
    ///		
    ///		//Public fields will NOT be used:
    ///		public string OtherField = string.Empty;
    /// } 
    /// ]]>
    /// </code>
    /// might produce, combined with this classes' 
    /// <see cref="DataObjectSchema.DataStoreName"/>, 
    /// something like:
    /// <code>
    /// <![CDATA[
		/// "CREATE MyFieldName (GUID NVARCHAR(32) PRIMARY KEY, NameFirst varchar 50, NameLast varchar 50)"
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection to use in order to build the IDbCommand</param>
    /// <returns>An <see cref="IDbCommand"/> pre-loaded with <c>CommandText</c> set.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectSchema argument is null.</exception>
    protected virtual IDbCommand CreateCommand_CreateTable(IDbConnection connection) {
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }


      IDbCommand command = connection.CreateCommand();

      List<string> sqlFields = new List<string>();


      //A very lucky break is that GetProperties() brings the fields back in the order
      //they were defined...not alphabetical or any other stupid order...whew!
      bool primaryKeyColumnFound = false;
      foreach (DataObjectPropertySchema propertySchema in Schema.Properties) {

        string columnName;
        string columnType;
        string columnLength;
        string columnIsKey;


        //Get name of Column:
        columnName = propertySchema.Db.Name;

        //Determine the type of column to save as:
        Type propertyType = propertySchema.Db.DataType;

        if (propertyType == typeof(System.String)) {
          columnType = (propertySchema.Db.Size == 0) ? "text" : "varchar";
        }
        else if (propertyType == typeof(System.Guid)) {
					columnType = "NVARCHAR";// "uniqueidentifier";
        }
        else if (propertyType == typeof(System.DateTime)) {
          columnType = "DATETIME";
        }
        else if (propertyType == typeof(System.Double)) {
          columnType = "FLOAT";
        }
        else if (propertyType == typeof(System.Boolean)) {
          columnType = "INTEGER";
          //Can't do this if read/only: propertySchema.Db.Size = 1;
        }
        else if (propertyType.ToString().Substring(0, 10) == "System.Int") {
          columnType = "INTEGER";
        }
        else {
          throw new System.Exception(string.Format("Unhandled Type '{0}'", propertyType.ToString()));
        }

        //Handle FieldLength:
        if (propertyType == typeof(System.Guid)) {
          //Can't do this if read/only: propertySchema.Db.Size = 1;
          propertySchema.Db.Size = 16;
        }
        columnLength = (propertySchema.Db.Size > 0 ? "(" + propertySchema.Db.Size + ")" : string.Empty);

        //Handle FieldKey:
        columnIsKey = (propertySchema.Db.IsKey == true) ? "KEY" : string.Empty;
        if (propertySchema.Db.IsPrimaryKey == true) {
          primaryKeyColumnFound = true;
          columnIsKey = "PRIMARY " + columnIsKey;
        }

        //Put it all together:
        string columnSQL = string.Format("{0} {1}{2}{3}",
          columnName,
          columnType,
          columnLength,
          columnIsKey);
        sqlFields.Add(columnSQL);
      }
      if (primaryKeyColumnFound == false) {
        throw new System.Exception("No PrimaryKey defined. Cannot continue.");
      }

      command.CommandText = string.Format("CREATE {0} ({1})", Schema.DataStoreName, string.Join(", ", sqlFields.ToArray()));

      return command;
    }

    #endregion

    #region Protected Methods - Basics (CreateConnection,_CheckDataObjectType,_CheckDataFilterType)
    /// <summary>
    /// Creates a db connection to the db where dataobjects are stored.
    /// Note: The connection is not opened.
    /// </summary>
    /// <returns></returns>
    protected IDbConnection CreateConnection() {
      return DbHelpers.CreateConnection(_ProviderName, _ConnectionString);
    }

    /// <summary>
    /// Verify that passed object is not null, and is of compatible type.
    /// </summary>
    DataObject _CheckDataObjectType(object dataObject) {
      //Check Args:
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }


      //Type the argument:
      DataObject typedDataObject = dataObject as DataObject;
      if (typedDataObject == null) {
        throw new System.ArgumentException("DataObject is not of Type DataObject.");
      }

      XPDOTracker.XPDOTrackingVars trackingVars = XPDOTracker.Get(dataObject);
      if (trackingVars.Schema != this.Schema) {
        throw new System.ArgumentException("dataObject's DataStore is not 'this'.");
      }

      /*
      if (typedDataObject.DataObjectFactory != this) {
        throw new System.ArgumentException("dataObject's DataStore is not 'this'.");
      }
       */

      return typedDataObject;
    }


    /// <summary>
    /// Verify that passed filter is not null, and is of compatible type.
    /// </summary>
    /// <param name="filter"></param>
    protected void _CheckDataFilterType(IDataObjectFilter filter) {
      if (filter == null) {
        throw new System.ArgumentNullException("filter");
      }
      IDbDataObjectFilter dbFilter = filter as IDbDataObjectFilter;
      if (dbFilter == null) {
        throw new System.ArgumentException("IDataObjectFilter is not of Type IDbDataObjectFilter.");
      }

      if (dbFilter.DataObjectFactory != this) {
        throw new ArgumentException("DataObject filter is incompatible with this DataObjectFactory", "filter");
      }
    }

    /// <summary>
    /// Expand the sql statements, replacing any variable terms.
    /// <para>
    /// Note: Uses String.Format *after* expanding custom tags.
    /// </para>
    /// </summary>
		/// <internal>
		/// This is wrapper around <see cref="M:Support.DbHelpers.FormatStringEx"/>
		/// </internal>
		/// <param name="sql"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    protected string FormatSql(string sql, params object[] args) {
			return DbHelpers.FormatStringEx(sql, _DbSettings.SqlKeyWords, _Provider.ParamPrefix, args);
    }
    #endregion

    /// <summary>
    /// TODO:
    /// </summary>
    /// <param name="propertySchema"></param>
    /// <param name="parentRelationshipSchema"></param>
    /// <param name="parentTableAlias"></param>
    /// <param name="parentDataFieldAlias"></param>
    /// <returns></returns>
    public static bool GenerateAliasInfo(
      DataObjectPropertySchema propertySchema, 
      out RelationshipSchema parentRelationshipSchema,
      out string parentTableAlias,
      out string parentDataFieldAlias) {

      parentRelationshipSchema = propertySchema.ParentRelationship;

      if (parentRelationshipSchema == null) {
        //Set the 'out' statements before exiting:
        parentTableAlias = null;
        parentDataFieldAlias = null;
        return false;
      }

      string mainFieldName = propertySchema.Db.Name;
      parentTableAlias = mainFieldName + "_TBL";
      parentDataFieldAlias = mainFieldName + "_DF";


      //We want this one:
      string parentTableName = parentRelationshipSchema.ParentSchema.DataStoreName;
      string parentTableKeyFieldName = parentRelationshipSchema.ParentKeyPropertySchema.Db.Name;
      string parentTableDataFieldName = parentRelationshipSchema.ParentDataPropertySchema.Db.Name;




      //IMPORTANT:
      //It is not enough to just alias the table.
      //We have to alias the field as well as it is possible that more than one 
      //column in the primary table points to the same lookup table,
      //in the form of two JOINs.
      //an example would be a Project table, that has refs to an Employees table in
      //both a ProjectManager column, and ProjectContactPerson column.

      return true;
    }
  }//Class:End
}//Namespace:End
