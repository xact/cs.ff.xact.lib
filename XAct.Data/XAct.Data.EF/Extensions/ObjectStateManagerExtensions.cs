﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Linq;

    /// <summary>
    /// Extensions to the EF <see cref="ObjectStateManager"/>
    /// </summary>
    public static class ObjectStateManagerExtensions
    {

        /// <summary>
        /// Gets the object state entries.
        /// </summary>
        /// <param name="objectStateManager">The object state manager.</param>
        /// <param name="excludeDetached">if set to <c>true</c> [exclude detached].</param>
        /// <param name="includeUnchanged">if set to <c>true</c> [include unchanged].</param>
        /// <returns></returns>
        /// <internal>src: Pg 601</internal>
        public static IEnumerable<ObjectStateEntry> GetObjectStateEntries(this ObjectStateManager objectStateManager,bool excludeDetached=true, bool includeUnchanged=true)
        {
            EntityState entityState = EntityState.Added | EntityState.Deleted | EntityState.Modified;

            if (!excludeDetached)
            {
                entityState = EntityState.Detached;
            }

            if (includeUnchanged)
            {
                entityState |= EntityState.Unchanged;
            }
            return
                objectStateManager.GetObjectStateEntries(entityState);
        }


        /// <summary>
        /// Gets the object state entries.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="objectStateManager">The object state manager.</param>
        /// <param name="entityState">State of the entity.</param>
        /// <returns></returns>
        /// <internal>src: Pg 601</internal>
        public static IEnumerable<ObjectStateEntry> GetObjectStateEntries<TEntity>(this ObjectStateManager objectStateManager, EntityState entityState)
        {
            return objectStateManager.GetObjectStateEntries(entityState).Where(entry => entry.Entity is TEntity);
        }


        /// <summary>
        /// Gets the object state E ntries.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="objectStateManager">The object state manager.</param>
        /// <returns></returns>
        /// <internal>src: Pg 601</internal>
        public static IEnumerable<ObjectStateEntry> GetObjectStateEntries<TEntity>(this ObjectStateManager objectStateManager)
        {
            return objectStateManager.GetObjectStateEntries().Where(entry => entry.Entity is TEntity);
        }


    }
}
