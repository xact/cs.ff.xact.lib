﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct
{
    /// <summary>
    /// Extensions to a the  
    /// IDatabaseInitializer of a specific TContext
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public static class DbContextUtils<TContext>
        //Note how the Generic Type is defined on the class
        //so that _initializedLock is not shared across more than a specific
        //TContext
        where TContext : DbContext
    {
        static object _InitializeLock = new object();

        static bool _InitializeLoaded = false;

        /// <summary>
        /// Method to allow running a DatabaseInitializer exactly once
        /// </summary>
        /// <param name="initializer">A Database Initializer to run</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        /// <returns></returns>
        public static bool SetInitializer(IDatabaseInitializer<TContext> initializer, bool force)
        {
            if (force)
            {
                _InitializeLoaded = false;
            }

            if (_InitializeLoaded)
            {
                return false;
            }

            // watch race condition
            lock (_InitializeLock)
            {
                // are we sure?
                if (_InitializeLoaded)
                {
                    return false;
                }

                _InitializeLoaded = true;

                // force Initializer to load only once
                System.Data.Entity.Database.SetInitializer<TContext>(initializer);

                return true;
            }
        }

    }
}
