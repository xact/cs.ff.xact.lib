﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Data.Entity.Core.Objects;
    using System.Linq.Expressions;
    using System.Reflection;

// ReSharper restore CheckNamespace
#endif


    /// <summary>
    /// 
    /// </summary>
    public static class ObjectQueryExtensions
    {
        /// <summary>
        /// Allows Typed references to Include.
        /// </summary>
        /// <remarks>
        /// <para>
        /// <code>
        /// <![CDATA[
        /// Organizations.Include(o => o.Assets).Where(o => o.Id == id).Single()
        /// instead of
        /// Organizations.Include("Assets").Where(o => o.Id == id).Single()
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="objectQuery"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        /// <internal>
        /// Src: http://bit.ly/elNvA3
        /// </internal>
        public static ObjectQuery<T> Include<T, TProperty>(this ObjectQuery<T> objectQuery, Expression<Func<T, TProperty>> selector)
        {

            MemberExpression memberExpr = selector.Body as MemberExpression;

            if (memberExpr != null)
            {
                return objectQuery.Include(memberExpr.Member.Name);
            }

            throw new ArgumentException("The expression must be a MemberExpression", "selector");
        }



        /// <summary>
        /// Allows Typed references to EF Include.
        /// <para>
        /// Note that this version allows for Property Paths (ie, with dots '.')
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="exp">The exp.</param>
        /// <returns></returns>
        /// <internal>
        /// Src: http://bit.ly/yPCLmq
        /// </internal>
            public static ObjectQuery<T> Include<T>(this ObjectQuery<T> query, Expression<Func<T, object>> exp)
            {
#pragma warning disable 168
                Expression body = exp.Body;
#pragma warning restore 168
                MemberExpression memberExpression = (MemberExpression)exp.Body;
                string path = GetIncludePath(memberExpression);
                return query.Include(path);
            }

            private static string GetIncludePath(MemberExpression memberExpression)
            {
                string path = "";
                if (memberExpression.Expression is MemberExpression)
                {
                    path = GetIncludePath((MemberExpression)memberExpression.Expression) + ".";
                }
                PropertyInfo propertyInfo = (PropertyInfo)memberExpression.Member;
                return path + propertyInfo.Name;
            }






        ////http://bit.ly/yQ1ep3
            //public static IEnumerable<string> WriteGeneratedSql<T>(ObjectQuery<T> query)
            //{
            //    IServiceProvider isp = (IServiceProvider)EntityProviderFactory.Instance;

            //    DbProviderServices mps = (DbProviderServices)isp.GetService(typeof(DbProviderServices));

            //    EntityCommandDefinition definition = (EntityCommandDefinition)mps.CreateCommandDefinition(query.Context.Connection, query.CreateCommandTree());


            //    foreach (string commandText in definition.MappedCommands)
            //    {

            //        yield return commandText;
            //    }

            //}
    }


#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
