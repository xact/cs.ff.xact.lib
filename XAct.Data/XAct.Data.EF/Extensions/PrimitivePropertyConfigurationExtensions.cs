﻿namespace XAct
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration.Configuration;

    /// <summary>
    /// 
    /// </summary>
    public static class             PrimitivePropertyConfigurationExtensions

    {

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredGuidId(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(colOrder);
            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredGuidFK(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasColumnOrder(colOrder);
            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredIntId(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .HasColumnOrder(colOrder);
            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// <para>
        /// IMPORTANT:As it's optional, only really used for Audit Records.
        /// </para>
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredIntIdNonIdentity(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsOptional()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(colOrder);
            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptionalParentFK(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsOptional()
                .HasColumnOrder(colOrder++);
            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredTimestamp(this BinaryPropertyConfiguration x, int colOrder)
        {
            x
                //NO:.IsRequired()
                .IsRowVersion()
                .IsConcurrencyToken()
                .HasColumnOrder(colOrder);
            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredOrder(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasColumnOrder(colOrder);

            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredEnabled(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasColumnOrder(colOrder);

            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredApplicationTennantId(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasColumnOrder(colOrder);

            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired64Char(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder);

            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional64Char(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsOptional()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder);

            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional64CharDeletedBy(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineOptional64Char(colOrder);

            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired1024CharText(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder);

            return x;
        }
        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired4000Char(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasMaxLength(4000)
                .HasColumnOrder(colOrder);

            return x;
        }

        
        
        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired64CharUserIdentifier(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineRequired64Char(colOrder);

            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired64CharKey(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineRequired64Char(colOrder);

            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// <para>
        /// For displayable *mutable* Names (eg: HelpCategory), but that are *not* Keys.
        /// See <see cref="DefineOptional256CharResourceFilter"/>
        /// </para>
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired64CharName(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineRequired64Char(colOrder);

            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired64CharCreatedBy(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineRequired64Char(colOrder);

            return x;
        }




        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired64CharCode(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineRequired64Char(colOrder);

            return x;
        }
        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired64CharLastModifiedBy(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineRequired64Char(colOrder);

            return x;
        }



        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredSerializationMethod(this PrimitivePropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasColumnOrder(colOrder);

            return x;
        }



        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired1024CharSerializationValueType(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder);

            return x;
        }


        
        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional4000CharSerializationValue(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsOptional()
                .HasMaxLength(4000)
                .HasColumnOrder(colOrder);

            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptionalMaxLengthCharSerializationValue(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsOptional()
                .IsMaxLength()
                .HasColumnOrder(colOrder);

            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptionalMaxLengthCharText(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsOptional()
                .IsMaxLength()
                .HasColumnOrder(colOrder);

            return x;
        }

        
        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional4000CharDescription(this StringPropertyConfiguration x, int colOrder)
        {
            x.IsOptional()
             .HasMaxLength(4000)
             .HasColumnOrder(colOrder);

            return x;
        }

        

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional256CharTag(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .HasMaxLength(256)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            return x;
        }

        ///// <summary>
        ///// Defines the property in a common way.
        ///// </summary>
        //public static PrimitivePropertyConfiguration DefineOptionalCreatedOnUtc(this DateTimePropertyConfiguration x, int colOrder)
        //{
        //    x.IsOptional()
        //     .HasColumnOrder(colOrder);
        //}

                    /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredCreatedOnUtc(this DateTimePropertyConfiguration x, int colOrder)
        {
            x
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            return x;
        }


       
        
        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequiredLastModifiedOnUtc(this DateTimePropertyConfiguration x, int colOrder)
        {
            x.IsRequired()
             .HasColumnOrder(colOrder);

            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptionalDeletedOnUtc(this DateTimePropertyConfiguration x, int colOrder)
        {
            x.IsOptional()
             .HasColumnOrder(colOrder);

            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional256CharResourceFilter(this StringPropertyConfiguration x, int colOrder)
        {
    //It's required so that filtering can be done at the database side, without having to resort to more complex
    //sql case "if null/or empty string" 
            x.
                IsOptional()
             .HasMaxLength(256)
             .HasColumnOrder(colOrder++);

            return x;
        }


        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional256CharFilter(this StringPropertyConfiguration x, int colOrder)
        {
            x
                .IsOptional()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++);

            return x;
        }
        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired26CharFilter(this StringPropertyConfiguration x, int colOrder)
        {
            //TODO: Not all sure if Required is the right thing here.
            x
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++);

            return x;
        }
        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineRequired1024CharSubject(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineRequired1024CharText(colOrder);

            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional1024CharSubject(this StringPropertyConfiguration x, int colOrder)
        {
            x.DefineOptional1024CharText(colOrder);

            return x;
        } 

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional1024CharText(this StringPropertyConfiguration x, int colOrder)
        {
            x.IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder);

            return x;
        }

        /// <summary>
        /// Defines the property in a common way.
        /// </summary>
        public static PrimitivePropertyConfiguration DefineOptional4000CharText(this StringPropertyConfiguration x, int colOrder)
        {
            x.IsOptional()
                .HasMaxLength(4000)
                .HasColumnOrder(colOrder);

            return x;
        }

    }
}