using System.Data.Entity.ModelConfiguration.Configuration;
using XAct.Library.Settings;

namespace XAct
{
    /// <summary>
    /// Extensions to <see cref="ManyToManyAssociationMappingConfiguration"/>
    /// instances.
    /// </summary>
    public static class ManyToManyAssociationMappingConfigurationExtensions
    {
        /// <summary>
        /// Maps the entity to a table, using
        /// <see cref="XAct.Library.Settings.Db.DefaultXActLibSchema"/>
        /// </summary>
        /// <param name="manyToManyAssociationMappingConfiguration">The many to many association mapping configuration.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="schemaName">Name of the schema.</param>
        public static void ToXActLibTable(
            this ManyToManyAssociationMappingConfiguration manyToManyAssociationMappingConfiguration, string tableName,
            string schemaName = null)
        {

            if (schemaName.IsNullOrEmpty())
            {
                schemaName = XAct.Library.Settings.Db.DefaultXActLibSchema;
            }
            if (schemaName.IsNullOrEmpty())
            {
                manyToManyAssociationMappingConfiguration
                    .ToTable(
                        tableName:"{0}{1}".FormatStringInvariantCulture(Db.DefaultXActLibDbTablePrefix, tableName));

            }
            else
            {
                manyToManyAssociationMappingConfiguration
                    .ToTable(
                        tableName: "{0}{1}".FormatStringInvariantCulture(Db.DefaultXActLibDbTablePrefix, tableName),
                        schemaName: schemaName
                    );
            }
        }

    }
}