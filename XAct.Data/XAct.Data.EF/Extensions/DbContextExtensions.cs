﻿using System.Data.Entity.ModelConfiguration;
using System.IO;
using XAct.Diagnostics;
using XAct.Environment;
using XAct.IO;

namespace XAct
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Core.Metadata.Edm;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Validation;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Reflection;
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Library.Settings;

    /// <summary>
    /// Extensions to the <see cref="DbContext"/>
    /// </summary>
    public static class DbContextExtensions
    {

        private static IDbContextSeederService DbContextSeedingService
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<IDbContextSeederService>();
                
            }
        }


        /// <summary>
        /// Casts the given DbContext into the underlying EF Object Context,
        /// from which you can access more methods.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        /// <returns></returns>
        public static ObjectContext GetObjectContext(this DbContext dbContext)
        {
            ObjectContext objectContext = ((IObjectContextAdapter) dbContext).ObjectContext;
            return objectContext;
        }


        /// <summary>
        /// Sets the MergeOptions for a specific entity.
        /// </summary>
        /// <internal>
        /// See: http://msmvps.com/blogs/kevinmcneish/archive/2010/02/16/setting-entity-framework-mergeoptions-what-works-what-doesn-t.aspx
        /// see: http://social.msdn.microsoft.com/Forums/sa/adonetefx/thread/790b9c35-ed3e-4ab2-9b7c-6986614c530a
        /// </internal>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbContext">The db context.</param>
        /// <param name="mergeOption">The merge option.</param>
        public static void SetMergeOptions<T>(this DbContext dbContext, MergeOption mergeOption)
            where T : class
        {
            dbContext.GetObjectContext().CreateObjectSet<T>().MergeOption = mergeOption;
        }

        /// <summary>
        /// Deletes the given object, using <see cref="GetObjectContext"/>
        /// to get to the underlying <see cref="ObjectContext"/>
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        /// <param name="entity">The entity.</param>
        public static void DeleteObject(this DbContext dbContext, object entity)
        {
            dbContext.GetObjectContext().DeleteObject(entity);
        }


        /// <summary>
        /// Returns a ist of the Names of the entity's Key fields.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns></returns>
        public static IEnumerable<string> KeysFor(this DbContext context, Type entityType)
        {
            Contract.Requires(context != null);
            Contract.Requires(entityType != null);

            entityType = ObjectContext.GetObjectType(entityType);

            MetadataWorkspace metadataWorkspace =
                ((IObjectContextAdapter) context).ObjectContext.MetadataWorkspace;
            ObjectItemCollection objectItemCollection =
                (ObjectItemCollection) metadataWorkspace.GetItemCollection(DataSpace.OSpace);

            EntityType ospaceType = metadataWorkspace
                .GetItems<EntityType>(DataSpace.OSpace)
                .SingleOrDefault(t => objectItemCollection.GetClrType(t) == entityType);

            if (ospaceType == null)
            {
                throw new ArgumentException(
                    string.Format(
                        "The type '{0}' is not mapped as an entity type.",
                        entityType.Name),
                    "entityType");
            }

            return ospaceType.KeyMembers.Select(k => k.Name);
        }


        /// <summary>
        /// Makes a DbContext entities DateTime properties all Utc in one fell swoop.
        /// <para>
        /// Usage:
        /// </para>
        /// <para>
        /// <code>
        /// <![CDATA[
        /// public AppDbContext()
        /// {
        /// //That's it!
        /// this.ReadAllDateTimeValuesAsUtc();
        /// }
        /// 
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// See: http://stackoverflow.com/a/11683020 and http://blog.3d-logic.com/2012/04/08/entity-framework-and-datetime-of-unspecified-kind/
        /// </para>
        /// </summary>
        /// <param name="context">The context.</param>
        public static void ReadAllDateTimeValuesAsUtc(this DbContext context)
        {
            ((IObjectContextAdapter) context).ObjectContext.ObjectMaterialized += ReadAllDateTimeValuesAsUtc;
        }

        private static void ReadAllDateTimeValuesAsUtc(object sender, ObjectMaterializedEventArgs e)
        {
            //Extract all DateTime properties of the object type
            List<PropertyInfo> properties = e.Entity.GetType().GetProperties()
                              .Where(property => property.PropertyType == typeof (DateTime) ||
                                                 property.PropertyType == typeof (DateTime?)).ToList();
            //Set all DaetTimeKinds to Utc
            properties.ForEach(property => SpecifyUtcKind(property, e.Entity));
        }

        private static void SpecifyUtcKind(PropertyInfo property, object value)
        {
            //Get the datetime value
            object datetime = property.GetValue(value, null);

            //set DateTimeKind to Utc
            if (property.PropertyType == typeof (DateTime))
            {
                datetime = DateTime.SpecifyKind((DateTime) datetime, DateTimeKind.Utc);
            }
            else if (property.PropertyType == typeof (DateTime?))
            {
                DateTime? nullable = (DateTime?) datetime;
                if (!nullable.HasValue) return;
                datetime = (DateTime?) DateTime.SpecifyKind(nullable.Value, DateTimeKind.Utc);
            }
            else
            {
                return;
            }

            //And set the Utc DateTime value
            property.SetValue(value, datetime, null);
        }


        /// <summary>
        /// Builds the models automatically,
        /// by searching for implementaions of 
        /// <c>EntityTypeConfiguration&lt;&gt;</c>,
        /// that pass an optional <paramref name="filter"/>
        /// <para>
        ///  If searchDomain is false, searches only the DbContext's assembly, plus whatever 
        ///  assemblies are specifically listed.
        /// </para>
        /// </summary>
        /// <typeparam name="TEntityTypeConfiguration">The type of the entity type configuration.</typeparam>
        /// <param name="dbContext">The database context.</param>
        /// <param name="modelBuilder">The model builder.</param>
        /// <param name="invokeModelBuildingWhenFound">if set to <c>true</c> [invoke model building when found].</param>
        /// <param name="searchDomain">if set to <c>true</c> [search domain].</param>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static Type[] AutoBuildModels<TEntityTypeConfiguration>(this DbContext dbContext,
            DbModelBuilder modelBuilder,
            bool invokeModelBuildingWhenFound = true, 
            bool searchDomain = false, 
            Assembly[] assemblies = null,
            Func<Type, bool> filter = null)
        {

            return dbContext.AutoBuildModels(
                modelBuilder, 
                invokeModelBuildingWhenFound, 
                searchDomain, 
                assemblies, 
                typeof(TEntityTypeConfiguration),
                filter);
        }

        /// <summary>
        /// Builds the models automatically,
        /// by searching for implementaions of 
        /// <c>EntityTypeConfiguration&lt;&gt;</c>,
        /// that pass an optional <paramref name="filter"/>
        /// <para>
        ///  If searchDomain is false, searches only the DbContext's assembly, plus whatever 
        ///  assemblies are specifically listed.
        /// </para>
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        /// <param name="modelBuilder">The model builder.</param>
        /// <param name="invokeModelBuildingWhenFound">if set to <c>true</c> [invoke model building when found].</param>
        /// <param name="searchDomain">if set to <c>true</c> [search domain].</param>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="searchForEentityTypeConfigurationType">Type of the entity type configuration.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static Type[] AutoBuildModels(
            this DbContext dbContext, 
            DbModelBuilder modelBuilder,
            bool invokeModelBuildingWhenFound = true, 
            bool searchDomain = false, 
            Assembly[] assemblies = null, 
            Type searchForEentityTypeConfigurationType = null,
            Func<Type,bool> filter=null )
        {
            List<string> errors = new List<string>();
            if (searchForEentityTypeConfigurationType == null)
            {
                searchForEentityTypeConfigurationType = typeof(EntityTypeConfiguration<>);
            }
            //
            var allAssemblies = dbContext.xyz(searchDomain, assemblies);

            var entityTypeConfigurationTypes =
                allAssemblies.GetAllTypesImplementingOpenGenericType(searchForEentityTypeConfigurationType);


            if (filter != null)
            {
                entityTypeConfigurationTypes = entityTypeConfigurationTypes.Where(filter).ToArray();
            }

            if (invokeModelBuildingWhenFound)
            {
                //Do not use linq here:
                foreach (var entityTypeConfigurationType in entityTypeConfigurationTypes)
                {
                    try
                    {
                        InstantiateEntityTypeConfigurationType(modelBuilder, entityTypeConfigurationType);
                    }
                    catch (System.Exception e)
                    {
                        errors.Add(e.Message);
                        errors.Add(e.StackTrace);
                        errors.Add(string.Empty);
                        errors.Add(string.Empty);

                        //throw;
                    }
                }

            }
            if (errors.Count > 0)
            {
                DumpToBaseAppFile(errors.ToArray(), "XActLib_DbContext_AutoBuildModels.txt");
            }
            return entityTypeConfigurationTypes;

        }

        private static void InstantiateEntityTypeConfigurationType(DbModelBuilder modelBuilder, Type entityTypeConfigurationType)
        {
            //Determine what Foo actually is:
            //Type entityType = entityTYpeConfigurationType.BaseType.GetGenericArguments().First();

            //Type entityTypeConfigurationType2 = typeof (EntityTypeConfiguration<>).MakeGenericType(entityType);

            //Make an instance of EntityTypeConfiguration<Foo>
            //Huge risk: It's a generalisation of EntityTypeConfiguration<>, 
            //and normally should not need any injection. ever...but what if it did?
            //Hence why I am using the enhanced activator:
            //Which in turn, uses IoC... which means it needs to be alaready available.
            //So, to hell with it. TUrn it off:
            //object entityTypeConfiguration = XAct.DependencyResolver.Current.GetInstance(entityTYpeConfigurationType);
            object entityTypeConfigurationInstance = System.Activator.CreateInstance(entityTypeConfigurationType);

            //modelBuilder.Configurations.Add((EntityTypeConfiguration<ScheduledTask>)XAct.DependencyResolver.Current.GetInstance<IScheduledTaskModelPersistenceMap>());
            //Type genericListType = typeof(Bar<>).MakeGenericType(a[0])
            dynamic foo = entityTypeConfigurationInstance;
            modelBuilder.Configurations.Add(foo);

            //MethodInfo tMethod1 = modelBuilder.Configurations.GetType().GetMethod("Add",  new[] { entityTypeConfigurationType2 });
            //MethodInfo tMethod2 = modelBuilder.Configurations.GetType().GetMethod("Add", BindingFlags.InvokeMethod, null, new[] { entityTypeConfigurationType2 }, null);
            //modelBuilder.Configurations.InvokeMethod("Add", new[] { entityTypeConfigurationType2 }, new[] { entityTypeConfiguration });
        }


        /// <summary>
        /// Builds the models automatically.
        /// </summary>
        /// <typeparam name="TDbModelBuilder">The type of the database model builder.</typeparam>
        /// <param name="dbContext">The database context.</param>
        /// <param name="modelBuilder">The model builder.</param>
        /// <param name="invokeModelBuildingWhenFound">if set to <c>true</c> [invoke model building when found].</param>
        /// <param name="searchDomain">if set to <c>true</c> [search domain].</param>
        /// <param name="assemblies">The assemblies.</param>
        [Obsolete]
        public static Type[] AutoBuildModels_Legacy<TDbModelBuilder>(this DbContext dbContext, DbModelBuilder modelBuilder, bool invokeModelBuildingWhenFound = true, bool searchDomain = false, Assembly[] assemblies = null)
            where TDbModelBuilder : IDbModelBuilder
        {
            //NOTE:
            /* WHERE WE WANT TO GO NEXT IS DIRECTLY BYPASS THE MODELBUILDER AND
            void Main()
            {
            IFoo i = new Foo();
            var t = i.GetType().BaseType;
            var a = t.GetGenericArguments();
            Type genericListType = typeof(Bar<>).MakeGenericType(a[0]);
            var r = System.Activator.CreateInstance(genericListType);
            r.Dump();
            }
            // Define other methods and classes here
            public class C {}
            public interface IFoo {}
            public class Foo : Bar<C>, IFoo {}
            public class Bar<T> {}             
             * BUT IT WILL NEED GetTypesImplementingType TO BE ABLE TO HANDLE OPEN TYPES (eg: EntityTypeConfiguration<>)
            */


            //Get assemblies directly referenced from Assembly containing dbContext:

            Type[] modelBuilderTypesFound = dbContext.X<TDbModelBuilder>(searchDomain, assemblies);

            if (invokeModelBuildingWhenFound)
            {
                modelBuilderTypesFound.ForEach(x =>
                {
                    TDbModelBuilder dbModelBuilder = ((TDbModelBuilder) XAct.DependencyResolver.Current.GetInstance(x));
                    dbModelBuilder.OnModelCreating(modelBuilder);


                });
            }
            return modelBuilderTypesFound;
        }




        /// <summary>
        /// Seeds the specified <see cref="DbContext"/>
        /// using <see cref="IDbContextSeederService"/>
        /// to scan for implementations of 
        /// <see cref="IDbContextSeeder{TEntity}"/>
        /// </summary>
        /// <typeparam name="TDbContextSeeder">The type of the <see cref="IDbContextSeeder"/> based context seeder.</typeparam>
        /// <param name="dbContext">The database context.</param>
        /// <param name="searchDomain">if set to <c>true</c> [search domain].</param>
        /// <param name="assemblies">The assemblies.</param>
        /// <returns></returns>
        public static Type[] AutoSeed<TDbContextSeeder>(this DbContext dbContext, bool searchDomain = false, Assembly[] assemblies = null)
            where TDbContextSeeder : IDbContextSeeder
        {
            try
            {
                return DbContextSeedingService.Seed<TDbContextSeeder>(dbContext, searchDomain,assemblies);
            }
            catch (Exception e)
            {
                Debug(e.Message);
                throw;
            }
        }


        private static Type[] X<T>(this DbContext dbContext, bool searchDomain = false, Assembly[] assemblies = null)
        {
            Type[] modelBuilderTypesFound;

            Assembly[] allAssemblies = dbContext.xyz(searchDomain,assemblies);
            modelBuilderTypesFound = allAssemblies.GetTypesImplementingType(typeof (T), true);
            return modelBuilderTypesFound;
        }

        private static Assembly[] xyz(this DbContext dbContext, bool searchDomain = false, Assembly[] assemblies = null)
        {
            if (searchDomain)
            {
                return AppDomain.CurrentDomain.GetAssemblies();

            }
                var assemblyNames = dbContext.GetType().Assembly.GetReferencedAssemblies();

                return assemblyNames.Select(System.Reflection.Assembly.Load).ToArray();
        }


        /// <summary>
        /// Commit conditionally.
        /// Used during Seeding to minimize the number of trips to the Db.
        /// </summary>
        /// <typeparam name="TContext">The type of the context.</typeparam>
        /// <param name="dbContext">The database context.</param>
        /// <param name="seedingCommitLevel">The seeding commit level.</param>
        public static void ConditionallyCommit<TContext>(this TContext dbContext, SeedingCommitLevel seedingCommitLevel)
            where TContext : DbContext
        {
            //You want it to commit if default is less than given arg value:
            if (seedingCommitLevel > XAct.Library.Settings.Db.CommitRegularlyDuringSeeding)
            {
                //ie, if given "Debug only", and lib is set to regularly, then it doesn't do it.
                //ie if given "Regualrly and value lib default is Regularly, then it commits.
                return;
            }

            try
            {
                dbContext.SaveChanges();
            }
                // ReSharper disable RedundantCatchClause
#pragma warning disable 168
            catch (DbEntityValidationException e)
#pragma warning restore 168
            {
                throw;
            }
#pragma warning disable 168
            catch (DbUpdateException e)
#pragma warning restore 168
            {
                throw;
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                throw;
            }
            // ReSharper restore RedundantCatchClause
        }

        /// <summary>
        /// Potentiallies the commit.
        /// Used during Seeding to minimize the number of trips to the Db.
        /// </summary>
        /// <typeparam name="TContext">The type of the context.</typeparam>
        /// <param name="dbContext">The database context.</param>
        /// <param name="seedingCommitLevel">The seeding commit level.</param>
        public static void PotentiallyCommit<TContext>(this TContext dbContext, SeedingCommitLevel seedingCommitLevel)
            where TContext : DbContext
        {
            if (XAct.Library.Settings.Db.CommitRegularlyDuringSeeding >= seedingCommitLevel)
            {
                try
                {
                    dbContext.SaveChanges();
                }
// ReSharper disable RedundantCatchClause
#pragma warning disable 168
                catch (DbUpdateException e)
#pragma warning restore 168
                {
                    throw;
                }
// ReSharper restore RedundantCatchClause
            }
            else
            {
                return;
            }
        }



        /// <summary>
        /// Gets the changed enties.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        /// <param name="entityState">State of the entity.</param>
        /// <returns></returns>
        public static IEnumerable<DbEntityEntry> GetEntityEntries(this DbContext dbContext, EntityState entityState= (EntityState.Added | EntityState.Modified | EntityState.Deleted))
        {
            return dbContext.ChangeTracker.Entries()
                     .Where(x => x.State == entityState);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void Debug(this string message)
        {
#if DEBUG
            System.IO.File.AppendAllText("C:\\tmp\\debug.txt",
                "{0}: {1}.{2}{3}"
                    .FormatStringInvariantCulture(
                        DateTime.Now.ToString("hh:mm:ss.fff tt"),
                        System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                        message,
                        System.Environment.NewLine));
#endif
        }


        //Note the non-reliance on IoC
        private static void DumpToBaseAppFile(string[] contents, string fileName)
        {
            try
            {
                //Remove Bin:
                string fullFilePath = AppDomainExtensions.BaseDir + "\\" + fileName;

                //Going to use this shortcut approach (rather than XActLib)
                //as I don't want to have to start IoC:
                System.IO.File.WriteAllLines(fullFilePath, contents);
            }
                // ReSharper disable once UnusedVariable
            catch 
            {
                //absorb;
            }
        }

        /// <summary>
        /// Determines whether the context's ChangeTracker has any entries that are Added, Modified, or Deleted.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static bool IsDirty(this DbContext context)
        {
            return context.ChangeTracker
                          .Entries()
                          .Any(IsDirty);
        }

        /// <summary>
        /// Determines whether the given entry has any entries that are Added, Modified, or Deleted.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <returns></returns>
        public static bool IsDirty(this DbEntityEntry entry)
        {
            return entry.State == EntityState.Added
                   || entry.State == EntityState.Modified
                   || entry.State == EntityState.Deleted;
        }




    }
}

