﻿
// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.Data.Entity.Core.Objects;

    /// <summary>
    /// Extensions to the <see cref="ObjectStateEntry"/>
    /// elements that keep track of Original of Entities.
    /// </summary>
    public static class ObjectStateEntryExtensions
    {
        //public void X(this ObjectStateEntry objectStateEntry)
        //{
        //    //objectStateEntry.OriginalValues[];
        //    //objectStateEntry.CurrentValues[];

        //    //Accept Current values are Original values.
        //    //Sets state to unmodified.
        //    //objectStateEntry.AcceptChanges();
        //}
    }
}
