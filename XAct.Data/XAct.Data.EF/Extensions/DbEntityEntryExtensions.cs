﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct
{
    using System.Data.Entity.Infrastructure;

    /// <summary>
    /// Extension methods to the <see cref="DbEntityEntry"/> object.
    /// </summary>
    public static class DbEntityEntryExtensions
    {

        /// <summary>
        /// Gets the entity property's original (Typed) value, prior to being updated.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entry">The entry.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static TValue GetOriginalValue<TValue, TEntity>(this DbEntityEntry<TEntity> entry, string propertyName) where TEntity : class
        {
            var result = entry.OriginalValues[propertyName];
            if (result == null) return default(TValue);

            if (!(result is TValue))
            {
                throw new Exception(string.Format("Property {0} is of type {1} and cannot be cast to {2}", propertyName, result.GetType(), typeof(TValue)));
            }

            return (TValue)result;
        }

        /// <summary>
        /// Gets the entity property's current Typed value, after being edited.
        /// <para>
        /// May still be equal to the Original value.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entry">The entry.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static TValue GetCurrentValue<TValue, TEntity>(this DbEntityEntry<TEntity> entry, string propertyName) where TEntity : class
        {
            var result = entry.CurrentValues[propertyName];
            if (result == null) return default(TValue);

            if (!(result is TValue))
            {
                throw new Exception(string.Format("Property {0} is of type {1} and cannot be cast to {2}", propertyName, result.GetType(), typeof(TValue)));
            }

            return (TValue)result;
        }

        /// <summary>
        /// Determines whether any of the specified properties have been edited on the entity (Current != Original value).
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entry">The entry.</param>
        /// <param name="propertyNames">Name of the property.</param>
        /// <returns></returns>
        public static bool IsValueUpdated<TValue, TEntity>(this DbEntityEntry<TEntity> entry, params string[] propertyNames) where TEntity : class
        {
            var result = propertyNames
                .Any(p => !entry.GetCurrentValue<TValue, TEntity>(p).EqualsSafely(entry.GetOriginalValue<TValue, TEntity>(p)));

            return result;
        }




        /// <summary>
        /// Gets the entity property's original value, prior to being updated.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public static object GetOriginalValue(this DbEntityEntry entry, string propertyName)
        {
            return entry.OriginalValues[propertyName];
        }

        /// <summary>
        /// Gets the entity property's current value, after being edited.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public static object GetUpdatedValue(this DbEntityEntry entry, string propertyName)
        {
            return entry.CurrentValues[propertyName];
        }

        /// <summary>
        /// Determines whether the entity's properties have been edited (Current != Original value).
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <param name="propertyNames">Name of the property.</param>
        /// <returns></returns>
        public static bool IsValueUpdated(this DbEntityEntry entry, params string[] propertyNames)
        {
            var result = propertyNames.Any(p => !entry.GetUpdatedValue(p).Equals(entry.GetOriginalValue(p)));
            return result;
        }

    }
}
