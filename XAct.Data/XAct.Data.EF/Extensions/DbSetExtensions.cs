﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Data.Entity;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// Extensions to the EF4.2 CodeFirst DbSet
    /// </summary>
    public static class DbSetExtensions
    {
        /// <summary>
        /// Examples the specified dbset.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbset">The dbset.</param>
        public static void Example<T>(DbSet<T> dbset) 
            where T:class
        {

        }
    }


#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif

