﻿

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Data.Entity.Core.Objects;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// 
    /// <para>
    /// Attention: in a correctly uncoupled application, there
    /// should be little reason to be working against a vendor
    /// specific ObjectContext.
    /// </para>
    /// <para>
    /// Use <see cref="IHasGetContext"/> if at all possible.
    /// </para>
    /// </summary>
    /// 
    public static class ObjectContextExtensions
    {
        /// <summary>
        /// TODO: Document
        /// </summary>
        /// <param name="objectContext">The object context.</param>
        public static void ExampleExtensionMethods(this ObjectContext objectContext)
        {
            
            //do nothing.
        }
    }


#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
