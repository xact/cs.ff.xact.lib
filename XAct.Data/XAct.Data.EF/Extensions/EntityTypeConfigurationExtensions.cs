using System;
using System.Data.Entity.ModelConfiguration;
using XAct;
using XAct.Library.Settings;

namespace XAct
{
    /// <summary>
    /// Extesnsion to <see cref="EntityTypeConfiguration{T}"/> instances.
    /// </summary>
    public static class EntityTypeConfigurationExtensions
    {
        /// <summary>
        /// Maps the entity to a table, using
        /// <see cref="XAct.Library.Settings.Db.DefaultXActLibSchema"/>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="entityTypeConfiguration">The entity type configuration.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="schemaName">Name of the schema.</param>
        public static void ToXActLibTable<TModel>(this EntityTypeConfiguration<TModel> entityTypeConfiguration, string tableName, string schemaName=null)
            where TModel : class
        {
            if (!typeof(TModel).IsSubClassOfEx(typeof(IHasXActLibEntity)))
            {
                throw new ArgumentException("ToXActLibTable was invoked with a Type ('{0}') that does not implement {1}".FormatStringInvariantCulture(typeof(TModel).Name,typeof(IHasXActLibEntity).Name));
            }

            if (tableName.IsNullOrEmpty())
            {
                tableName = typeof(TModel).Name;
            }

            if (schemaName.IsNullOrEmpty())
            {
                schemaName = XAct.Library.Settings.Db.DefaultXActLibSchema;
            }
            if (schemaName.IsNullOrEmpty())
            {
                entityTypeConfiguration
                    .ToTable("{0}{1}".FormatStringInvariantCulture(Db.DefaultXActLibDbTablePrefix, tableName));

            }
            else
            {
                entityTypeConfiguration
                    .ToTable("{0}{1}".FormatStringInvariantCulture(Db.DefaultXActLibDbTablePrefix, tableName),
                        schemaName: schemaName
                    );
            }
        }
    }
}