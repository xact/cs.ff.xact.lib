﻿using System;
using System.Reflection;
using XAct.Diagnostics;

namespace XAct.Data.EF.CodeFirst
{
    using System.Data.Entity;

    /// <summary>
    /// Contract for a service that scans for
    /// implementations of
    /// <see cref="IHasXActLibDbContextSeeder{TEntity}"/>
    /// and seeds a specific <see cref="DbContext"/>
    /// </summary>
    public interface IDbContextSeederService : IHasXActLibService
    {


        /// <summary>
        /// Seeds the DbContext, by searching for implementations of 
        /// <see cref="XAct.Data.EF.CodeFirst"/>.
        /// </summary>
        /// <typeparam name="TDbContextSeeder">The type of the database context seeder.</typeparam>
        /// <param name="dbContext">The database context.</param>
        /// <param name="searchDomain">if set to <c>true</c> [search domain].</param>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <returns></returns>
        Type[] Seed<TDbContextSeeder>(DbContext dbContext, bool searchDomain = false,
            Assembly[] assemblies = null, ITracingService tracingService = null)
            where TDbContextSeeder : IDbContextSeeder;

    }
}