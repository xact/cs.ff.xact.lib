﻿using System.Reflection;

namespace XAct.Data.EF.CodeFirst.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Validation;
    using System.Linq;
    using XAct;
    using XAct.Commands;
    using XAct.Data.EF.CodeFirst.Seeders.Implementations;
    using XAct.Diagnostics;
    using XAct.Initialization;
    using XAct.Library.Settings;
    using XAct.Services;


    /// <summary>
    /// An implementation of <see cref="IDbContextSeederService"/>
    /// to scan for implementations of
    /// <see cref="XAct.Data.EF.CodeFirst.IDbContextSeeder"/>
    /// and seeds a specific <see cref="DbContext"/>
    /// </summary>
    public class DbContextSeederService : XActLibServiceBase, IDbContextSeederService
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextSeederService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public DbContextSeederService(ITracingService tracingService)
            : base(tracingService)
        {
        }


        /// <summary>
        /// Seeds the specified database by
        /// scanning for implementations of <see cref="XAct.Data.EF.CodeFirst.IDbContextSeeder" />
        /// </summary>
        /// <typeparam name="TDbContextSeeder">The type of the database context seeder.</typeparam>
        /// <param name="dbContext">The database context.</param>
        /// <param name="searchDomain">if set to <c>true</c> [search domain].</param>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <returns></returns>
        public Type[] Seed<TDbContextSeeder>(DbContext dbContext, bool searchDomain = false, Assembly[] assemblies = null, ITracingService tracingService = null)
             where TDbContextSeeder : IDbContextSeeder
        {

            //Should be safe to invoke it here (NOT IN CONSTRUCTOR)

            //Technically, this stuff is straight forward.
            Type[] seederTypes = X<TDbContextSeeder>(dbContext, searchDomain, assemblies);

            try
            {
                Type lastOne;
                foreach (var seederType in seederTypes)
                {
                    lastOne = seederType;
                    try
                    {
                        var seeder = seederType.ActivateEx<IDbContextSeeder>();
                        seeder.Seed(dbContext);
                    }
                    catch (System.Exception e)
                    {
                        _tracingService.TraceException(XAct.Diagnostics.TraceLevel.Error, e, "Seeding failed for {0}!",
                            seederType);
                        throw;
                    }
                }
            }
            catch (System.Exception e)
            {
                _tracingService.TraceException(XAct.Diagnostics.TraceLevel.Error, e, "Seeding failed!");
                throw;
            }

            return seederTypes;
        }

        private static Type[] X<T>(DbContext dbContext, bool searchDomain = false, Assembly[] assemblies = null)
        {
            Type[] modelBuilderTypesFound;

            Assembly[] allAssemblies = xyz(dbContext, searchDomain, assemblies);
            modelBuilderTypesFound = allAssemblies.GetTypesImplementingType(typeof(T), true);
            return modelBuilderTypesFound;
        }


        private static Assembly[] xyz(DbContext dbContext, bool searchDomain = false, Assembly[] assemblies = null)
        {
            if (searchDomain)
            {
                return AppDomain.CurrentDomain.GetAssemblies();

            }
            var assemblyNames = dbContext.GetType().Assembly.GetReferencedAssemblies();

            List<Assembly> results = new List<Assembly>();
            results.Add(dbContext.GetType().Assembly);
            results.Add(assemblyNames.Select(System.Reflection.Assembly.Load));
            
            return results.ToArray();
        }


        private ExecutableRunnerActionResult LogTheTimeTaken1()
        {
            ExecutableRunnerActionResult executableRunnerActionResult1 =
                new XAct.Commands.ExecutableRunnerActionResult();

            executableRunnerActionResult1.StepType = this.GetType();
            executableRunnerActionResult1.Step = null;
            executableRunnerActionResult1.IndentLevel = HasExecutableActionRunner.IndentLevel + 1;

            XAct.Commands.HasExecutableActionRunner.AddToGlobalSummary(executableRunnerActionResult1);

            return executableRunnerActionResult1;
        }

        private static ExecutableRunnerActionResult LogTheTimeTaken2(Type t)
        {
            ExecutableRunnerActionResult executableRunnerActionResult2 =
                new XAct.Commands.ExecutableRunnerActionResult();

            executableRunnerActionResult2.StepType = t;
            executableRunnerActionResult2.Step = null;
            executableRunnerActionResult2.IndentLevel = HasExecutableActionRunner.IndentLevel + 2;

            XAct.Commands.HasExecutableActionRunner.AddToGlobalSummary(executableRunnerActionResult2);

            return executableRunnerActionResult2;
        }

    }
}
