﻿namespace XAct.Data.EF.CodeFirst
{
    /// <summary>
    /// Contract for the implementation of a DbModelBuilder.
    /// </summary>
    /// <internal>
    /// Look at how it was used in XAct.Resources.Persistence
    /// </internal>
    public interface IHasXActLibDbModelBuilder : IDbModelBuilder
    {
    }
}