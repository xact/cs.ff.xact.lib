﻿namespace XAct.Data.EF.CodeFirst
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for the implementation of a recursive <see cref="IDbContextSeeder"/>
    /// of type <typeparamref name="TEntity"/>
    /// <para>
    /// Note: Implemented by <see cref="IHasXActLibDbContextSeeder{TEntity}"/>, which is used to differentiate
    /// library seeders from end-user seeders.
    /// </para>
    /// <para>
    /// See <see cref="IUpdateDbContextSeeder{TEntity}"/>
    /// </para>
    /// </summary>
    /// <internal>
    /// Look at how it was used in XAct.Resources.Persistence
    /// </internal>
    public interface IDbContextSeeder<TEntity> : IDbContextSeeder
        where TEntity:class
    {
        /// <summary>
        /// The number of records created by the seeder.
        /// <para>
        /// The reason it is <c>public</c> is as follows:
        /// </para>
        /// <para>
        /// If StudentSeeder creates 3 records, 
        /// and SeederSeeder is a dependency of SeederDetailsSeeder,
        /// when SeederDetailsSeeder has finished invoking SeederSeeder first,
        /// it can find out the number of records created by SeederSeeder
        /// and match it (one Details record per student).
        /// </para>
        /// <para>
        /// The reason it's not simply <c>Entities.Count</c>
        /// is that for a base Seeder this would be correct, but
        /// for derived seeders, they may want to change the count.
        /// </para>
        /// <para>
        /// <code>
        /// <![CDATA[
        /// class DefaultStudentSeeder ... {
        ///    public int EntityCount {get 10;}
        ///    public MakeEntities {for (int i=0;i<EntityCount;i++){...}
        /// }
        /// class AnotherStudentSeeder : DefaultStudentSeeder ... {
        ///    public int EntityCount {get 3;}
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        int EntityCount { get; }

        /// <summary>
        /// A cached collection of the Entities created the first time
        /// <see cref="IDbContextSeeder.Seed"/> is invoked,
        /// and returned for all subsequent times.
        /// <para>
        /// The cache is made public in order to UnitTest the seeder --
        /// before running subsequent tests relying on the seeder.
        /// </para>
        /// </summary>
        IEnumerable<TEntity> Entities { get; }





        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        void CreateEntities();

    }
}
