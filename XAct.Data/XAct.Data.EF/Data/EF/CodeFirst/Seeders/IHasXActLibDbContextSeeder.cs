﻿namespace XAct.Data.EF.CodeFirst
{
    /// <summary>
    /// Contract for an XActLib specific implementation of a Seeder.
    /// <para>
    /// Adds no functionality -- just helps identify which seeders
    /// belong to the library, and which do not.
    /// </para>
    /// </summary>
    /// <internal>
    /// Look at how it was used in XAct.Resources.Persistence
    /// </internal>
    public interface IHasXActLibDbContextSeeder<TEntity> : IDbContextSeeder<TEntity>, IHasXActLibDbContextSeeder
        where TEntity:class 
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IHasXActLibDbContextSeeder :  IDbContextSeeder
    {
        
    }
}