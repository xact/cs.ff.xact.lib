﻿namespace XAct.Data.EF.CodeFirst
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics;

    /// <summary>
    /// An abstract base implementation of 
    /// <see cref="IDbContextSeeder"/>
    /// specialized for Unit Testing (it's binding 
    /// priority is already set to <see cref="IHasMediumBindingPriority"/> 
    /// in order to override the default Seeding solution (which 
    /// usually creates no records).
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public abstract class UnitTestXActLibDbContextSeederBase<TEntity> : XActLibDbContextSeederBase<TEntity>, IHasUnitTestXActLibDbContextSeeder<TEntity>
        where TEntity:class 
    {
        /// <summary>
        /// </summary>
        protected readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTestXActLibDbContextSeederBase{TEntity}"/> class.
        /// </summary>
        protected UnitTestXActLibDbContextSeederBase() : base()
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTestXActLibDbContextSeederBase{TEntity}"/> class.
        /// </summary>
        protected UnitTestXActLibDbContextSeederBase(ITracingService tracingService, params IDbContextSeeder[] seeders)
            : base(seeders)
        {
            _tracingService = tracingService;
        }


        ///// <summary>
        ///// Initializes a new instance of the <see cref="UnitTestXActLibDbContextSeederBase{TEntity}"/> class.
        ///// </summary>
        ///// <param name="tracingService">The tracing service.</param>
        ///// <param name="seedersTypes">The seeders types.</param>
        //protected UnitTestXActLibDbContextSeederBase(Type[] seedersTypes, ITracingService tracingService)
        //    : base(tracingService, seedersTypes)
        //{
        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTestXActLibDbContextSeederBase{TEntity}"/> class.
        /// </summary>
        /// <param name="seeders">The seeders.</param>
        protected UnitTestXActLibDbContextSeederBase(params IDbContextSeeder[] seeders) : 
            base(seeders)
        {
        }
    }
}