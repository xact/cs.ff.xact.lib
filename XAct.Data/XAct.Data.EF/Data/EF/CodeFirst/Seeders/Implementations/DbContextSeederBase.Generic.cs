﻿using System;
using System.Collections.Generic;

namespace XAct.Data.EF.CodeFirst.Seeders.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using XAct.Library.Settings;


    /// <summary>
    /// Static cache of the RecursiveSeeders invoked
    /// since the last time the cache was cleared.
    /// <para>
    /// Note: Refactored out of <see cref="DbContextSeederBase{TEntity}"/>
    /// in order for the method to be invokable
    /// even if the seeder Type is not known.
    /// </para>
    /// </summary>
    public static class DbContextSeederContext
    {
        #region Static
        /// <summary>
        /// Static cache of the <see cref="Type"/> of 
        /// <see cref="IDbContextSeeder"/>s
        /// already processed, and therfore can be skipped if invoked again.
        /// </summary>
        public static IList<Type> StaticDbContextSeederTypeCache { get { return _staticDbContextSeederTypeCacheTypeCache ?? (_staticDbContextSeederTypeCacheTypeCache = new List<Type>()); } }
        // ReSharper disable StaticFieldInGenericType
        /// <summary></summary>
        private static IList<Type> _staticDbContextSeederTypeCacheTypeCache;
        // ReSharper restore StaticFieldInGenericType

        /// <summary>
        /// Clear's the static cache of previously run seeders.
        /// <para>
        /// IMPORTANT: Generally only invoked only within Test Suites 
        /// (in an application, there should be no need to clear the cache).
        /// </para>
        /// </summary>
        public static void ClearCache()
        {
            if (_staticDbContextSeederTypeCacheTypeCache != null)
            {
                _staticDbContextSeederTypeCacheTypeCache.Clear();
            }
        }
        #endregion

    }

    /// <summary>
    /// An abstract base class implementation of the 
    /// <see cref="IDbContextSeeder{TEntity}"/> contract.
    /// <para>
    /// Use this base class to implement seeders in your application.
    /// </para>
    /// <para>
    /// A usage example:
    /// <code>
    /// <![CDATA[
    /// //Implement a contract
    /// public interface ISomeAppDbContextSeeder : IDbContextSeeder<SomeEntity> {}
    /// 
    /// //Implement a class of the same name, minus the 'I', and implementing 
    /// //this base class.
    /// public class SomeAppDbContextSeeder : DbContextSeederBase<SomeEntity>, ISomeAppDbContextSeeder {
    ///   //Implement the default constructors:
    ///   public SomeAppDbContextSeeder():base(){}
    ///   public SomeAppDbContextSeeder(IEnumerable<Type> IDbContextSeederBaseDerivedTypes):base(IDbContextSeederBaseDerivedTypes){}
    ///   public SomeAppDbContextSeeder(IDbContextSeeder[] seeders):base(seeders){}
    ///   //Implement SeedInternal:
    ///   public override void SeedInternal(){
    ///      var set = dbContext.Set<TEntity>(); 
    ///      set.InsertOrUpdate(new SomeEntity{FirstName=..., LastName=..., etc});
    ///      dbContext.Commit();
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public abstract class DbContextSeederBase<TEntity> : IDbContextSeeder<TEntity>
        where TEntity : class
    {




        /// <summary>
        /// The object's type name. Use for logging or caching purposes.
        /// </summary>
        // ReSharper disable InconsistentNaming
        protected readonly string _typeName;
        // ReSharper restore InconsistentNaming

        /// <summary>
        /// The <see cref="IDbContextSeeder"/>s -- if any -- 
        /// that <see cref="Seed"/> will invoke
        /// prior to implementing the custom logic 
        /// with <see cref="SeedInternal"/>
        /// </summary>
        // ReSharper disable InconsistentNaming
        protected readonly IDbContextSeeder[] _dbContextSeeders;
        // ReSharper restore InconsistentNaming

        
        /// <summary>
        /// Gets the entity count.
        /// </summary>
        /// <value>
        /// The entity count.
        /// </value>
        public virtual int EntityCount { get { return InternalEntities.Count(); } }

        /// <summary>
        /// A cached collection of the Entities created the first time
        /// <see cref="IDbContextSeeder.Seed"/> is invoked,
        /// and returned for all subsequent times.
        /// <para>
        /// The cache is made public in order to UnitTest the seeder --
        /// before running subsequent tests relying on the seeder.
        /// </para>
        /// </summary>
        public IEnumerable<TEntity> Entities
        {
            get { return InternalEntities; }
        }

        /// <summary>
        /// Gets or sets the internal entities.
        /// </summary>
        /// <value>
        /// The internal entities.
        /// </value>
        protected List<TEntity> InternalEntities
        {
            get { return _internalEntities ?? (_internalEntities = new List<TEntity>()); }
            set { _internalEntities = value; }
        }
        private List<TEntity> _internalEntities;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextSeederBase{TEntity}"/> class.
        /// </summary>
        // ReSharper disable PublicConstructorInAbstractClass
        public DbContextSeederBase()
        // ReSharper restore PublicConstructorInAbstractClass
        {
            _typeName = this.GetType().Name;

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextSeederBase{TEntity}"/> class.
        /// </summary>
        /// <param name="dbContextSeeders">The seeders.</param>
// ReSharper disable PublicConstructorInAbstractClass
        public DbContextSeederBase(params IDbContextSeeder[] dbContextSeeders)
// ReSharper restore PublicConstructorInAbstractClass
        {
            _typeName = this.GetType().Name;
            _dbContextSeeders = dbContextSeeders;
        }

        private static int StaticSanityCounter;
        /// <summary>
        /// Seeds the specified db based on the DbSets properties found in the dbContext.
        /// <para>
        /// Important: An exception will be raies if the DbContext does not have a DbSet property for the entities trying to be build.
        /// </para>
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        public void Seed(DbContext dbContext)
        {
            StaticSanityCounter++;

            if (StaticSanityCounter > 500)
            {
                Debug.Assert(false,"Seeder maybe in a loop.");
            }

            if (DbContextSeederContext.StaticDbContextSeederTypeCache.Contains(this.GetType()))
            {
                //This IDbContextSeeder has already been invoked, 
                //so skip it.
                return;
            }

            //It's the first time this seeder has been invoked.

            //But Invoke any required seeders first:
            InvokePreviousSeeders(dbContext);

            //There's a very small chance the previous seeders in turn
            //invoked this seeder...(not sure if that would actually cause 
            //a stack overflow, but still)...
            if (DbContextSeederContext.StaticDbContextSeederTypeCache.Contains(this.GetType()))
            {
                return;
            }

            //We've done all the previous seeders,
            //Lets do this seeding proper, invoking
            //the abstract method:
            SeedInternal(dbContext);

            //Wrap up by saving a reference to this seeder's type:
            DbContextSeederContext.StaticDbContextSeederTypeCache.AddIfNotAlreadyAdded(this.GetType());
        }

        /// <summary>
        /// Invokes the previous seeders.
        /// </summary>
        /// <typeparam name="TContext">The type of the context.</typeparam>
        /// <param name="dbContext">The database context.</param>
        protected void InvokePreviousSeeders<TContext>(TContext dbContext) where TContext : DbContext
        {
//If an dependency Seeders need invoking first, 
            //Invoke those seeders first:
            if (_dbContextSeeders != null)
            {
                foreach (IDbContextSeeder seeder in _dbContextSeeders)
                {
#if needsDbContextSeederBase
                    //Most seeders will be a subclass of this abstract base class.
                    //Note that the target seeder, if it is an implementation
                    //of this abstract base class would have checked and 
                    //come to the same conclusion. 
                    //But we can't be assolutely sure they did subclass from this abstract
                    //so we should check it ourselves if need be:
                    DbContextSeederBase dbContextSeederBase = seeder as DbContextSeederBase;
               
                    if (dbContextSeederBase == null)
                    {
                        if (StaticDbContextSeederTypeCache.Contains(seeder.GetType()))
                        {
                            //Already run:
                            continue;
                        }
                    }
#endif
                    //The seeder was a subclass of this abstract -- let the target (ie, this) 
                    //method throw it out if it has already been processed.
                    seeder.Seed(dbContext);
                }
            }
        }

        /// <summary>
        /// Invoked from <see cref="IDbContextSeeder.Seed"/>
        /// <para>
        /// Use this method, rather than <see cref="IDbContextSeeder.Seed"/>
        /// to implement your custom seeding logic, 
        /// as <see cref="IDbContextSeeder.Seed"/> is already implemented -- within
        /// <see cref="DbContextSeederBase{TEntity}"/> --
        /// using recursion to invoke other seeders first.
        /// </para>
        /// <para>
        /// Many times the implementation is no more than:
        /// <code>
        /// <![CDATA[
        /// public override void SeedInternal<SomeEntity>(DbContext dbContext){
        ///   SeedInternalHelper(dbContext, false,null, SeedingCommitLevel.Always);
        ///   //or
        ///   //SeedInternalHelper(dbContext, true, x=>x.Id, SeedingCommitLevel.Always);
        /// }
        /// //And implement:
        /// public override void CreateEntities(){
        ///   this.Entities.Clear();
        ///   this.Entities.Add(new SomeEntity{FirstName="Joe",LastName="Smith"});
        ///   ...
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="dbContext"></param>
        public abstract void SeedInternal(DbContext dbContext);
            


        /// <summary>
        /// A helper method that can be used by an implementation of 
        /// <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to first invoke <see cref="CreateEntities"/> (populating
        /// <see cref="Entities"/> up to the value of <see cref="EntityCount"/>)
        /// then for each record in <see cref="Entities"/>
        /// populate the database.
        /// <para>
        /// This is often done within test seeders and simple scenarios.
        /// </para>
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="addOrUpdate"></param>
        /// <param name="addOrUpdateComparison"></param>
        /// <param name="seedingCommitLevel"></param>
        /// <returns></returns>
        protected virtual void SeedInternalHelper(DbContext dbContext, bool addOrUpdate=false, Expression<Func<TEntity,object>> addOrUpdateComparison=null, SeedingCommitLevel seedingCommitLevel =SeedingCommitLevel.Always)
        {

            // Create first the records entities
            CreateEntities();

            try
            {
                dbContext.ConditionallyCommit(SeedingCommitLevel.Always);
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException e)
            {
                Trace.TraceError(e.ToLogString("Failed during prepration for Seeding of " + this.GetType().Name));

                throw;
            }

            // They are not yet commited, so now loop and commit them:
            DbSet<TEntity> set = dbContext.Set<TEntity>();

            int counter = 0;
            int iMax = this.Entities.Count();


            int pageSize = Math.Min((iMax-1),50);

            for(int i=0;i<iMax;i++)
            {
                var entity = this.InternalEntities[i];

                counter++;
                //Choose whether to add, or addUpdate
                if (addOrUpdate)
                {
                    if (addOrUpdateComparison == null)
                    {
                        set.AddOrUpdate(entity);
                    }
                    else
                    {
                        set.AddOrUpdate(addOrUpdateComparison, entity);
                    }
                }
                else
                {
                    set.Add(entity);
                }

                if (counter >= pageSize)
                {
                    try
                    {
                        dbContext.ConditionallyCommit(SeedingCommitLevel.Always);
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        if (pageSize == 1)
                        {
                            //we already cut down before, so just throw this time:
                            throw;
                        }
                        var validationErrors = e.EntityValidationErrors.ToArray();
                        var json = validationErrors.ToJSON(Encoding.UTF8);
                        Trace.TraceError("* Entry causing validation issues: " + validationErrors[0].Entry.Entity.GetType().Name + ":" + json);
                        Rewind(ref pageSize, e, entity, ref i);
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException e)
                    {

                        if (pageSize == 1)
                        {
                            //we already cut down before, so just throw this time:
                            throw;
                        }
                        for (int ix = 0; ix < Math.Min(3, e.Entries.Count()); ix++)
                        {
                            var entry = e.Entries.ElementAt(ix);
                            var json = (entry != null) ? entity.ToJSON(Encoding.UTF8) : string.Empty;
                            Trace.TraceError("* Entry causing update issues: " + entry.Entity.GetType().Name + ":" + json);
                        }
                        Rewind(ref pageSize, e, entity, ref i);

                        //And try again:
                    }
                }
            }

        }

        private void Rewind(ref int pageSize, Exception e, TEntity entity, ref int i)
        {

            Trace.TraceError(e.ToLogString("Failed during Seeding of: " + this.GetType().Name));

            //Rollback to last safe position:
            //Check:
            //30 records:
            //0-...< 30
            //Page size=29
            //29-29=0...ok
            i = i - pageSize;

            //Cut down to smaller size:
            pageSize = 1;

        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="Entities"/> is still empty.
        /// </para>
        /// </summary>
        public abstract void CreateEntities();


#if unittesting

        Nice concept, lets figure out where we can put it 
        other than this base class, as it would burden all the normal 
        seeders -- not just the unit test ones.

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public virtual void CreateEntities()
        {
            List<TEntity> result = new List<TEntity>();
            for (int i = 0; i < this.EntityCount; i++)
            {
                result.Add(CreateEntity(i));
            }
        }

        /// <summary>
        /// Abstract base method to create a single Entity.
        /// <para>
        /// Invoked <see cref="EntityCount"/> times by <see cref="CreateEntities"/>
        /// </para>
        /// </summary>
        /// <param name="index"></param>
        public abstract TEntity CreateEntity(int index);
#endif

    }

}
