﻿namespace XAct.Data.EF.CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst.Seeders.Implementations;
    using XAct.Diagnostics;


    /// <summary>
    /// The a specialized abstrct base class 
    /// implementations of the 
    /// <see cref="IDbContextSeeder{TEntity}"/> contract
    /// for creating XActLib seeders.
    /// <para>
    /// This implementation is to be used just for within 
    /// XActLib -- to help distinguish from application
    /// Seeders, which should implement
    /// <see cref="DbContextSeederBase{TEntity}"/>, or
    /// <see cref="IDbContextSeeder{TEntity}"/>
    /// </para>
    /// <para>
    /// Applications should use <see cref="DbContextSeederBase{TEntity}"/>
    /// </para>
    /// </summary>
    public abstract class XActLibDbContextSeederBase<TEntity> : DbContextSeederBase<TEntity>
        where TEntity :class 
    {




        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextSeederBase{TEntity}" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="seeders">The seeders.</param>
        protected XActLibDbContextSeederBase(ITracingService tracingService, params IDbContextSeeder[] seeders)
            : base(seeders)
        {
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="DbContextSeederBase{TEntity}" /> class.
        /// </summary>
        /// <param name="seeders">The seeders.</param>
        protected XActLibDbContextSeederBase(params IDbContextSeeder[] seeders)
            : base(seeders)
        {
        }


        /// <summary>
        /// Invoked from <see cref="IDbContextSeeder.Seed"/>
        /// <para>
        /// Use this method, rather than <see cref="IDbContextSeeder.Seed"/>
        /// to implement your custom seeding logic, 
        /// as <see cref="IDbContextSeeder.Seed"/> is already implemented -- within
        /// <see cref="DbContextSeederBase{TEntity}"/> --
        /// using recursion to invoke other seeders first.
        /// </para>
        /// <para>
        /// Many times the implementation is no more than:
        /// <code>
        /// <![CDATA[
        /// public override void SeedInternal<SomeEntity>(DbContext dbContext){
        ///   SeedInternalHelper(dbContext, false,null, SeedingCommitLevel.Always);
        ///   //or
        ///   //SeedInternalHelper(dbContext, true, x=>x.Id, SeedingCommitLevel.Always);
        /// }
        /// //And implement:
        /// public override void CreateEntities(){
        ///   this.Entities.Clear();
        ///   this.Entities.Add(new SomeEntity{FirstName="Joe",LastName="Smith"});
        ///   ...
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="dbContext"></param>
        public override void SeedInternal(DbContext dbContext) 
        {
            
            SeedInternalHelper(dbContext, false);

        }

    }
}