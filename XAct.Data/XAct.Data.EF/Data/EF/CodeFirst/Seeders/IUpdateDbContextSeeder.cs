﻿namespace XAct.Data.EF.CodeFirst
{
    /// <summary>
    /// IDbContextSeeder for use for Udpates, to be run after
    /// all the base seeders have run.
    /// <para>
    /// See <see cref="IDbContextSeeder{TEntity}"/>
    /// </para>
    /// </summary>
    public interface IUpdateDbContextSeeder<TEntity> : IDbContextSeeder
    {

    }
}