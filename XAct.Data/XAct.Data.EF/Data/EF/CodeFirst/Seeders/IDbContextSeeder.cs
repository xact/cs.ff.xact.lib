﻿namespace XAct.Data.EF.CodeFirst
{
    using System.Data.Entity;

    /// <summary>
    /// Base contract for DbContext seeders.
    /// </summary>
    public interface IDbContextSeeder : IHasSingletonBindingScope, IHasLowBindingPriority
    {



            
        /// <summary>
        /// Seeds the specified db based on the DbSets properties found in the dbContext.
        /// <para>
        /// Seed in turn invokes <see cref="SeedInternal"/>
        /// to handle recursive seeding of any dependencies first.
        /// </para>
        /// <para>
        /// Method is Invoked during application initialization.</para>
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        void Seed(DbContext dbContext);




        /// <summary>
        /// Invoked from <see cref="IDbContextSeeder.Seed"/>
        /// <para>
        /// Use this method, rather than use <see cref="IDbContextSeeder.Seed"/>
        /// to seed the database, as <see cref="IDbContextSeeder.Seed"/>
        /// is implemented using recursion.
        /// </para>
        /// </summary>
        /// <param name="dbContext"></param>
        void SeedInternal(DbContext dbContext);





    }
}