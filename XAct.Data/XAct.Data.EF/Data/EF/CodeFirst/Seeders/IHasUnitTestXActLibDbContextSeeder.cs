﻿namespace XAct.Data.EF.CodeFirst
{
    /// <summary>
    /// A specialization of <see cref="IHasXActLibDbContextSeeder{TEntity}"/>
    /// to ensure that Unit Test seeders have a higher binding priority (<see cref="IHasMediumBindingPriority"/>)
    /// than the default case (<see cref="IHasLowBindingPriority"/>).
    /// <para>
    /// Example usage:
    /// <code>
    /// <![CDATA[
    /// public void UnitTestSomEntityCaseDbContextSeeder : UnitTestXActLibDbContextSeederBase<SomeEntity> {
    /// ...
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IHasUnitTestXActLibDbContextSeeder<TEntity> : IDbContextSeeder<TEntity>, IHasMediumBindingPriority
        where TEntity : class
    {
        
    }
}