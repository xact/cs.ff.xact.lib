﻿namespace XAct.Data.EF.CodeFirst
{
    using System.Data.Entity;

    /// <summary>
    /// A Null (doesn't do anything) implementation of the 
    /// <see cref="IDatabaseInitializer{TContext}"/> for
    /// EF CodeFirst Scenarios:
    /// contract.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage Example:
    /// <code>
    /// <![CDATA[
    /// //Somewhere *early*, eg, in the Bootstrapper:
    /// #if DEBUG
    /// //Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<TestContext>());
    /// #else
    /// //With no strategy, it won't check to see whether EdmMetaData table is there or not.
    /// //So either of the following:
    /// //Database.SetInitializer<TestContext>(null);
    /// //Database.SetInitializer(new NullInitializer<TestContext>());
    /// endif
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// See also
    /// <see cref="System.Data.Entity.CreateDatabaseIfNotExists{T}"/>
    /// <see cref="System.Data.Entity.DropCreateDatabaseIfModelChanges{T}"/>
    /// <see cref="System.Data.Entity.DropCreateDatabaseAlways{T}"/>
    /// (default is <see cref="System.Data.Entity.DropCreateDatabaseIfModelChanges{T}"/>).
    /// </para>
    /// </remarks>
    /// <typeparam name="TDbContext">The type of the db context.</typeparam>
    /// <internal>
    /// See: http://bit.ly/rMlV8K for more...
    /// </internal>
    public class NullDatabaseInitializer<TDbContext> : IDatabaseInitializer<TDbContext>
                where TDbContext : DbContext
    {

        /// <summary>
        /// Executes the strategy to initialize the database for the given context.
        /// </summary>
        /// <param name="context">The context.</param>
        public void InitializeDatabase(TDbContext context)
        {
            //bool exists = context.Database.Exists();

            //bool compatible = context.Database.CompatibleWithModel(false);
            
            //Just in case:
            this.Seed(context);
        }

        /// <summary>
        /// Seeds the database of the given database.
        /// </summary>
        /// <param name="context">The context.</param>
        protected virtual void Seed(TDbContext context) { }

    }
}
