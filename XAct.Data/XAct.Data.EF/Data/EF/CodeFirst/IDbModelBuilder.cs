﻿namespace XAct.Data.EF.CodeFirst
{
    using System.Data.Entity;

    /// <summary>
    /// Contract for the implementation of a DbModelBuilder.
    /// </summary>
    /// <internal>
    /// Look at how it was used in XAct.Resources.Persistence
    /// </internal>
    public interface IDbModelBuilder : IHasSingletonBindingScope, IHasLowBindingPriority
    {
        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        void OnModelCreating(DbModelBuilder modelBuilder);
    
    }
}
