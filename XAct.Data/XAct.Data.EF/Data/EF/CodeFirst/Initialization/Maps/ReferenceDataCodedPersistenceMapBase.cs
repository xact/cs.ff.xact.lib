﻿namespace XAct.Data
{
    using System.Data.Entity.ModelConfiguration;


        /// <summary>
        /// Base class for creating a PersistenceMap
        /// for EF datastorage of Keyed Reference Data (more suitable for Immutable Reference data).
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TId">The type of the identifier.</typeparam>
        /// <internal>
        /// See:
        /// <see cref="ReferenceDataPersistenceMapBase{T,TId}"/>
        /// <see cref="ApplicationTennantSpecificReferenceDataPersistenceMapBase{T,TId}"/>
        /// </internal>
        public abstract class ReferenceDataCodedPersistenceMapBase<TEntity,TId> : EntityTypeConfiguration<TEntity>
        where TId : struct
        where TEntity : class, IHasCodedReferenceDataReadOnly<TId>, IHasTimestamp
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceDataCodedPersistenceMapBase{TEntity,TId}"/> class.
        /// </summary>
        /// <param name="tableNameSuffix">The table name suffix.</param>
        protected ReferenceDataCodedPersistenceMapBase(string tableNameSuffix)
        {

            this.ToXActLibTable(tableNameSuffix);


            //The properties expression 'h => new <>f__AnonymousType0`2(TargetMachineId = h.MachineId, Id = h.Id)' is not valid. 
            //The expression should represent a property: C#: 't => t.MyProperty'  VB.Net: 'Function(t) t.MyProperty'. When specifying multiple properties use an anonymous type: C#: 't => new { t.MyProperty1, t.MyProperty2 }'  VB.Net: 'Function(t) New With { t.MyProperty1, t.MyProperty2 }'.
            this
                .HasKey(h => h.Id);



            int colOrder = 0;
#pragma warning disable 168
            //int indexMember = 1; //Indexs of db's are 1 based.
#pragma warning restore 168

            this
                .Property(m => m.Id)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++)
                ;
            this
                .Property(x=>x.Code)
                .DefineRequired64CharCode(colOrder++)
                ;
            this
                .Property(m => m.Text)
                .DefineOptional1024CharText(colOrder++)
                ;
            this
                .Property(m => m.ResourceFilter)
                .DefineOptional256CharResourceFilter(colOrder++)
                ;
            this
                .Property(m => m.Filter)
                .DefineRequired26CharFilter(colOrder++)
                ;


            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;
        }
    }
}
