﻿namespace XAct.Data
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;


    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    /// <internal>
    /// See:
    /// <see cref="ReferenceDataCodedPersistenceMapBase{T,TId}"/>
    /// <see cref="ApplicationTennantSpecificReferenceDataPersistenceMapBase{T,TId}"/>
    /// </internal>
    public abstract class ReferenceDataPersistenceMapBase<T, TId> : EntityTypeConfiguration<T>
        where TId : struct
        where T : class, IHasReferenceData<TId>, IHasTimestamp, new()
    {
        private readonly DatabaseGeneratedOption _idDatabaseGeneratedOption;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceDataPersistenceMapBase{T,TId}" /> class.
        /// </summary>
        /// <param name="tableNameSuffix">The table name suffix.</param>
        /// <param name="idDatabaseGeneratedOption">The identifier database generated option.</param>
        protected ReferenceDataPersistenceMapBase(string tableNameSuffix, DatabaseGeneratedOption idDatabaseGeneratedOption)
        {
            _idDatabaseGeneratedOption = idDatabaseGeneratedOption;

            this.ToXActLibTable(tableNameSuffix);


            //The properties expression 'h => new <>f__AnonymousType0`2(TargetMachineId = h.MachineId, Id = h.Id)' is not valid. 
            //The expression should represent a property: C#: 't => t.MyProperty'  VB.Net: 'Function(t) t.MyProperty'. When specifying multiple properties use an anonymous type: C#: 't => new { t.MyProperty1, t.MyProperty2 }'  VB.Net: 'Function(t) New With { t.MyProperty1, t.MyProperty2 }'.
            this
                .HasKey(h => h.Id);



            int colOrder = 0;

            this
                .Property(m => m.Id)
                .HasDatabaseGeneratedOption(_idDatabaseGeneratedOption)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);
            this
                .Property(m => m.Text)
                .DefineOptional1024CharText(colOrder++)
                ;

            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;
            this
                .Property(m => m.ResourceFilter)
                .DefineOptional256CharResourceFilter(colOrder++)
                ;
            //this
            //    .Property(m => m.ResourceKey)
            //    .IsOptional()
            //    .HasMaxLength(256)
            //    .HasColumnOrder(colOrder++)
            //    ;
            //this
            //    .Property(m => m.DescriptionResourceKey)
            //    .IsOptional()
            //    .HasMaxLength(256)
            //    .HasColumnOrder(colOrder++)
            //    ;
            this

                .Property(m => m.Filter)
                .DefineOptional256CharFilter(colOrder++);




        }
    }
}
