﻿namespace XAct.Data.Updates.Services.Initialization.Maps
{
    /// <summary>
    /// Contract for a DB Map of <see cref="DataStoreUpdateLog"/>
    /// </summary>
    public interface IDataStoreUpdateLogModelPersistenceMap : IHasXActLibModelPersistenceMap
    {

    }
}