﻿namespace XAct.Data.Updates.Services.Initialization.Maps.Implementations
{
    using System.Data.Entity.ModelConfiguration;


    /// <summary>
    /// 
    /// </summary>
    public class DataStoreUpdateLogModelPersistenceMap : EntityTypeConfiguration<DataStoreUpdateLog>, IDataStoreUpdateLogModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DataStoreUpdateLogModelPersistenceMap"/> class.
        /// </summary>
        public DataStoreUpdateLogModelPersistenceMap()
        {

            this.ToXActLibTable("DataStoreUpdateLog");


            this
                .HasKey(h => h.Id);


            int colOrder = 0;

            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);
            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);
            this
                .Property(m => m.EnvironmentIdentifier)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++)
                ;
        }
    }
}