﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using XAct.Diagnostics;
using XAct.Services;

namespace XAct.Data.Updates.Services.Initialization.Implementations
{
    using XAct.Data.Updates.Services.Initialization.Maps;

    /// <summary>
    /// 
    /// </summary>
    public class DataStoreUpdateLogDbModelBuilder : IDataStoreUpdateLogDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataStoreUpdateLogDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public DataStoreUpdateLogDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add(
                (EntityTypeConfiguration<DataStoreUpdateLog>)
                XAct.DependencyResolver.Current.GetInstance<IDataStoreUpdateLogModelPersistenceMap>());

        }
    }
}