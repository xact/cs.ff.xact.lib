﻿namespace XAct
{
    using System;
    using System.Data;
    using System.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public static class DbCommandExtensions
    {

        /// <summary>
        ///   Creates a Parameter ready for any SQL-92 database.
        /// </summary>
        /// <param name = "command">The command to attach the parameter to.</param>
        /// <param name = "paramName">Name of the param.</param>
        /// <param name = "value">The value.</param>
        /// <returns>The IDbDataParameter that was created.</returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the command argument is null.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the paramName argument is null or empty.
        /// </exception>
        public static IDbDataParameter CreateParam(
            this IDbCommand command,
            string paramName,
            object value)
        {
            command.ValidateIsNotDefault("command");
            if (paramName.IsNullOrEmpty())
            {
                throw new ArgumentNullException("paramName");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            IDbDataParameter param;
            param = command.CreateParameter();
            param.Direction = ParameterDirection.Input;
            param.ParameterName = paramName;

            if ((value == null) || (value == DBNull.Value))
            {
                param.Value = DBNull.Value;
                return param;
            }

            //This fails on SqlServerCE:
            //param.Value = value;

            //first look for a type specialized param construction
            Type type = value.GetType();

            if (type.IsEnum)
            {
                param.DbType = DbType.Int32;
                param.Value = value;
            }
            else if (type == typeof(DateTime))
            {
                param.DbType = DbType.DateTime;
                param.Value = value;
                //SqlServer's Min date is 1/1/1753:so will fail if you
                //insert a 0001/01/01...
                //http://database.ittoolbox.com/pub/TP012003.HTML

                if (command.GetType().FullName.Contains("SqlServer"))
                {
                    if (DateTime.MinValue.CompareTo(value) == 0)
                    {
                        param.Value = DBNull.Value;
                    }
                }
            }
            else if (type == typeof(Guid))
            {
                //param.DbType = DbType.Guid;
                //BUGFIX: Convert to String because 
                //some IDbDataParameter (Sqlite...) garble 
                //the output (maybe converting binary to string?).

                param.DbType = DbType.String;
                //BUGFIX: Actually...that causes it to fail on SqlServerCE
                //So maybe back to D Format...
                if (command.GetType().FullName.Contains("SqlServer"))
                {
                    param.Value = ((Guid)value).ToString("D"); //OK
                }
                else
                {
                    param.Value = ((Guid)value).ToString("N"); //OK
                }

                //Which messes up 32 chars and now needs 36 chars...
                //xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx 
                //but luckily not yet 38 of:
                //{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx} 
            }
            else if (type == typeof(bool))
            {
                //param.Value = ((bool)value) ? 1 : 0;
                param.DbType = DbType.Boolean;
                param.Value = value;
            }
            else if (type == typeof(int))
            {
                param.DbType = DbType.Int32;
                param.Value = value;
            }
            else if (type == typeof(Enum))
            {
                param.DbType = DbType.Int32;
                param.Value = (int)value;
            }
            else if (type == typeof(String))
            {
                param.DbType = DbType.String;
                param.Value = value;
            }
            else
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                Trace.TraceWarning(
                    "CreateParam: unknown type = {0} (try converting to string)",
                    type);
#endif
                param.DbType = DbType.String;
                param.Value = Convert.ToString(value);
            }
            return param;
        }

    }
}
