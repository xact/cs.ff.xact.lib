﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Configuration package for an instance of
    /// <see cref="CodeFirstMigrationsStatusServiceConnector"/>
    /// </summary>
    public class CodeFirstMigrationsStatusServiceConnectorConfiguration :IHasTransientBindingScope
    {

        /// <summary>
        /// Gets or sets information aboutthe database connection.
        /// <para>
        /// Default is 'System.Data.SqlClient' but can change to other common providers such as 'System.Data.SqlServerCe.4.0'
        /// </para>
        /// </summary>
        /// <value>
        /// The database connection information.
        /// </value>
        public DbConnectionInfo DbConnectionInfo { get; private  set; }


        /// <summary>
        /// The <see cref="Type"/> of the application's <see cref="DbMigrationsConfiguration"/>
        /// </summary>
        public Type DbMigrationsConfigurationType { get; private set; }

        
        /// <summary>
        /// Sets the database migrations configuration.
        /// </summary>
        /// <typeparam name="TDbContextConfiguration">The type of the database context configuration.</typeparam>
        public void Configure<TDbContextConfiguration>(string connectionString, string providerInvariantName = "System.Data.SqlClient")
            where TDbContextConfiguration : DbMigrationsConfiguration
        {
            DbMigrationsConfigurationType = typeof(TDbContextConfiguration);
            DbConnectionInfo = new DbConnectionInfo(connectionString, providerInvariantName);
        }
    }
}