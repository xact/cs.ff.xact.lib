﻿
namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Linq;
    using System.Data.Entity.Migrations;
    using XAct;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Resources;


    /// <summary>
    /// A StatusService connector that reports on CodeFirst migrations.
    /// </summary>
    public class CodeFirstMigrationsStatusServiceConnector: XActLibStatusServiceConnectorBase
    {

        /// <summary>
        /// The configuration package.
        /// </summary>
        public CodeFirstMigrationsStatusServiceConnectorConfiguration Configuration { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeFirstMigrationsStatusServiceConnector"/> class.
        /// </summary>
        /// <param name="codeFirstMigrationsFeedServiceConnectorConfiguration">The code first migrations feed service connector configuration.</param>
        public CodeFirstMigrationsStatusServiceConnector(CodeFirstMigrationsStatusServiceConnectorConfiguration codeFirstMigrationsFeedServiceConnectorConfiguration)
            : base()
        {

            Name = "CodeFirstMigrations";

            Configuration = codeFirstMigrationsFeedServiceConnectorConfiguration;
        }

        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null)
        {
            //Using the base helper ensures it gets the right Title/Description
            //in the user's culture:
            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }

        private void MakeTable(StatusResponseTable table, object arguments)
        {
            DbMigrationsConfiguration dbMigrationsConfiguration =
                System.Activator.CreateInstance(Configuration.DbMigrationsConfigurationType)
                as DbMigrationsConfiguration;


            dbMigrationsConfiguration.TargetDatabase = Configuration.DbConnectionInfo;

            DbMigrator dbMigrator = new DbMigrator(dbMigrationsConfiguration);

            //Get the migrations defined in code:
            var localMigrations  = dbMigrator.GetLocalMigrations();
            var dbMigrations = dbMigrator.GetLocalMigrations();
            var pendingMigrations = dbMigrator.GetPendingMigrations();



            //Make headers:
            table.Headers.Add("Migration");
            table.Headers.Add("Status");


            if (pendingMigrations.Any())
            {
                foreach (var pending in pendingMigrations)
                {
                    StatusResponseTableRow row = new StatusResponseTableRow();
                    row.Cells.Add(pending);
                    row.Cells.Add("Pending");
                    row.Status = ResultStatus.Warn;
                    table.Rows.Add(row);
                }
            }

            if (localMigrations.Any())
            {
                StatusResponseTableRow row = new StatusResponseTableRow();
                row.Cells.Add("#{0}:{1}".FormatStringInvariantCulture( localMigrations.Count()+1, localMigrations.Last()));
                row.Cells.Add("In Code");
                row.Status = ResultStatus.Success;
                table.Rows.Add(row);
            }
            if (dbMigrations.Any())
            {
                StatusResponseTableRow row = new StatusResponseTableRow();
                row.Cells.Add("#{0}:{1}".FormatStringInvariantCulture(dbMigrations.Count() + 1, localMigrations.First()));
                row.Cells.Add("In Db");
                row.Status = ResultStatus.Success;
                table.Rows.Add(row);
            }

        }

    }
}
