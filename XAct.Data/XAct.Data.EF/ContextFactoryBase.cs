﻿//namespace XAct.Domain.Repositories
//{
//    using System.Data.Entity;

//    /// <summary>
//    /// Abstract base for implementations of 
//    /// <see cref="IContextFactory"/>
//    /// </summary>
//    public abstract class ContextFactoryBase<TDbContext> : IContextFactory
//        where TDbContext : DbContext
//    {
//        /// <summary>
//        /// Creates the DbContext.
//        /// </summary>
//        /// <typeparam name="TContext">The type of the context.</typeparam>
//        /// <param name="tag">The tag.</param>
//        /// <returns></returns>
//        public virtual IContext Create<TContext>(string tag = null)
//            where TContext : IContext
//        {
//            DbContext dbContext = (DbContext)System.Activator.CreateInstance(typeof (TDbContext));

//            IContext result = new EntityDbContext(dbContext);
//            return result;
//        }
//    }
//}
