﻿namespace XAct
{
    using System.Data.Entity.ModelConfiguration.Configuration;

    /// <summary>
    /// 
    /// </summary>
    public static class MergeOptionExtensions
    {
        /// <summary>
        /// Maps an Entity speicific MergeOption
        /// to the library's cross platform equivalent.
        /// </summary>
        /// <param name="mergeOption">The merge option.</param>
        /// <returns></returns>
        public static System.Data.Entity.Core.Objects.MergeOption MapTo(this XAct.Domain.Repositories.MergeOption mergeOption)
        {
            return (System.Data.Entity.Core.Objects.MergeOption) (int) mergeOption;
        }
    }
}
