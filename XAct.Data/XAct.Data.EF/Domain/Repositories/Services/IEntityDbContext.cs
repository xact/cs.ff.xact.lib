﻿// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Specialization of the <see cref="IContext"/>
    /// contract for an Entity Framework DbContext.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The specialization adds no functionality -- 
    /// but it is intended to differentiate the type of Entity
    /// </para>
    /// <para>
    /// Implemented by <see cref="EntityDbContext"/>
    /// as follows:
    /// <code>
    /// <![CDATA[
    /// public class EntityDbContext : IEntityDbContext {
    ///   private readonly DbContext _dbContext;
    ///   
    ///   public EntityDbContext(DbContext objectContext) {
    ///     if (objectContext.IsNull()) { throw new ArgumentNullException("objectContext"); }
    ///     _dbContext = objectContext;
    ///   }
    ///   
    ///   public int Commit(CommitType commitType = CommitType.Default) {
    ///     int result;
    ///   
    ///     IObjectContextAdapter objectContextAdapter = _dbContext;
    ///     ObjectContext objectContext = objectContextAdapter.ObjectContext;
    ///     switch (commitType) {
    ///       case CommitType.AcceptAllChanges:
    ///         //result = _dbContext.SaveChanges();
    ///         result = objectContext.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
    ///         break;
    ///       case CommitType.DetectAndHoldChangesTillComplete:
    ///         result = objectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave);
    ///         break;
    ///       case CommitType.HoldChangesTillComplete:
    ///         result = objectContext.SaveChanges(SaveOptions.None);
    ///         break;
    ///       case CommitType.CompleteChanges:
    ///         objectContext.AcceptAllChanges();
    ///         result = 0;
    ///         break;
    ///       default:
    ///         throw new ArgumentOutOfRangeException("commitType");
    ///       }
    ///     return result;
    ///   }
    ///   public void Dispose() {_dbContext.Dispose();}
    ///   object IObjectWrapper<object>.InnerObject {get { return _dbContext;}}
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public interface IEntityDbContext : IUnitOfWork {}
}