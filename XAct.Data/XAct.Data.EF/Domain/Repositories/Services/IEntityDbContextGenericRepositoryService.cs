namespace XAct.Domain.Repositories
{
    /// <summary>
    /// Contract for an EF specific implementation of the GenericRepository pattern,
    /// using EF's DbConect/DbSet mechanism, introduced in EF 4.2.
    /// </summary>
    public interface IEntityDbContextGenericRepositoryService : IRepositoryService, IHasXActLibService
    {
        
    }
}