// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories.Services
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Performance.Services;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IEntityDbContextGenericRepositoryService"/>
    /// contract for an EF specific implementation of the GenericRepository pattern,
    /// using EF's DbConect/DbSet mechanism, introduced in EF 4.2.
    /// </summary>
    /// <remarks>
    /// <para>
    /// As a reminder, a Repository has only DbSet -- for the Aggregagate Entity that it manages.
    /// If there is a cross relationship (for example, Vertices and Edges in a Graph, it's better 
    /// to have two Repositories, linked by a common Context, instead of one Repository,
    /// with two DbSets.
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(IRepositoryService), BindingLifetimeType.SingletonPerWebRequestScope, Priority.Low/*OK:Secondary Binding*/)]
    public class EntityDbContextGenericRepositoryService : XActLibServiceBase, IEntityDbContextGenericRepositoryService, IHasInnerItemReadOnly
    {
        private readonly IDistributedIdService _distributedIdService;
        private readonly IPerformanceCounterService _performanceCounterService;
        private readonly IContextStatePerformanceCounterService _contextStatePerformanceCounterService;
        private readonly IUnitOfWorkService _unitOfWorkService;




        /// <summary>
        /// Gets the current <see cref="IUnitOfWork"/> from the underlying 
        /// <see cref="IUnitOfWorkService"/>.
        /// <para>
        /// Use that to <c>Execute</c> it's internal <see cref="IContext"/>.
        /// </para>
        /// <para>
        /// Note that I am not especially happy having the Repository (Domain) have a reference 
        /// to an infrastructural context -- but it's just easier not having to inject two
        /// different contracts into every location the contract is needed.
        /// </para>
        /// </summary>
        public IUnitOfWork GetContext(string contextKey=null)
        {
                IUnitOfWork unitOfWork =  _unitOfWorkService.GetCurrent(contextKey);

                return unitOfWork;
        }

        /// <summary>
        /// AVOID: Gets the db context.
        /// </summary>
        /// <returns></returns>
        protected DbContext GetDbContext ()
        {
            return this.DbContext;
        }

		/// <summary>
		/// Returns the internal DbContext.
		/// Avoid unless you really have to as it creates tight 
		/// coupling to the technology, rather than remaining
		/// abstract to the concept of Repository, in general.
		/// </summary>
		/// <typeparam name="TDbContext">The type of the db context.</typeparam>
		/// <returns></returns>
		public TDbContext GetInnerItem<TDbContext>()
		{
			var result = this.DbContext.ConvertTo<TDbContext>();
            return result;
        }

        /// <summary>
        /// The EF CodeFirst <see cref="DbContext"/>
        /// </summary>
        /// <remarks>
        /// <para>
        /// The property is protected, in order to not 
        /// drag Assembly dependencies to higher layers.
        /// </para>
        /// </remarks>
        /// <summary>
        /// The EF <see cref="DbContext"/>
        /// </summary>
        /// <remarks>
        /// <para>
        /// The property is PRIVATE, in order to not 
        /// drag Assembly dependencies to higher layers.
        /// </para>
        /// </remarks>
        private DbContext DbContext
        {
            get
            {
                //If not defined by constructor, use the service to get the
                //current UoW, that matches this type of Repository:
                IUnitOfWork unitOfWork = _unitOfWorkService.GetCurrent();

                if (unitOfWork == null)
                {
                    Debug.Assert(unitOfWork != null,
                        "EntityDbContextGeneicRepositoryService.DbContext is null. Did you init the DB???",string.Empty);
                }

                DbContext dbContext = unitOfWork.GetInnerItem<DbContext>();
	            
                return dbContext;
            }
        }






        /// <summary>
        /// Gets the EF CodeFirst DbSet specific to this aggregate root.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The property is protected, in order to not 
        /// drag dependencies to higher layers.
        /// </para>
        /// </remarks>
        protected DbSet<TModel> GetDbSet<TModel>()
            where TModel : class
        {
            //Get the DbContext specific to this Repository type:
            DbContext dbContext = this.DbContext;

            //Try to find in the cache an DbSet specific to this DbContext:
            DbSet<TModel> result = dbContext.Set<TModel>();

            return result;

        }






        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EntityDbContextGenericRepositoryService" /> class.
        /// <para>
        /// This constructor is used for more robust applications
        /// that can contain more than one UoW...hence the need for
        /// a <see cref="IUnitOfWorkService" />
        /// </para>
        /// <para>
        /// The constructor is for using this Repository as a Service: the ObjectContext and ObjectSet
        /// will be selected dynamically (using the <see cref="IUnitOfWorkService" />)
        /// per method invocation.
        /// </para>
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="distributedIdService">The distributed identifier service.</param>
        /// <param name="performanceCounterService">The performance counter service.</param>
        /// <param name="contextStatePerformanceCounterService">The context state performance counter service.</param>
        /// <param name="unitOfWorkManagementService">The unit of work management service.</param>
        public EntityDbContextGenericRepositoryService(ITracingService tracingService, IDistributedIdService distributedIdService, IPerformanceCounterService performanceCounterService, IContextStatePerformanceCounterService contextStatePerformanceCounterService,  IUnitOfWorkService unitOfWorkManagementService):base(tracingService)
        {
            unitOfWorkManagementService.ValidateIsNotDefault("unitOfWorkManagementService");
            _distributedIdService = distributedIdService;
            _performanceCounterService = performanceCounterService;
            _contextStatePerformanceCounterService = contextStatePerformanceCounterService;
            _unitOfWorkService = unitOfWorkManagementService;
        }






        private void IncrementOperationCounter(bool deferredOperation)
        {

            string tag = (deferredOperation)
                             ? XAct.Constants.PerformanceCounters.RepositoryServiceQueriesDeferred
                             : XAct.Constants.PerformanceCounters.RepositoryServiceQueriesImmediate;
            _contextStatePerformanceCounterService.IncrementContextOperationCounter(tag, 1);
            _contextStatePerformanceCounterService.IncrementContextOperationCounter(XAct.Constants.PerformanceCounters.RepositoryServiceQueriesAll, 1);


            _performanceCounterService.Offset("XActLib", tag, null, amount: 1, raiseExceptionIfNotFound: false);
            _performanceCounterService.Offset("XActLib", XAct.Constants.PerformanceCounters.RepositoryServiceQueriesAll, null, amount: 1, raiseExceptionIfNotFound: false);
        }

        /// <summary>
        /// Determines if any elements with the specified condition exists.
        /// (faster than Count).
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="filter"></param>
        /// <returns></returns>
        public bool Exists<TModel>(Expression<Func<TModel, bool>> filter = null)
            where TModel : class
        {
            IncrementOperationCounter(false);

            bool result = filter == null ? this.GetDbSet<TModel>().Any() : this.GetDbSet<TModel>().Any(filter);

            return result;
        }

        /// <summary>
        /// Get the total of objects that meet the criteria.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// IRepository<Example> _exampleRepository = ...
        /// int howManyExpensiveThings =
        ///   _exampleRepository.Count(ex=>ex.Price > 10000);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <internal>
        /// Generally implemented without using <see cref="GetByFilter{TModel}"/>
        /// as not all Providers will create the same SQL.
        /// </internal>
        public virtual int Count<TModel>(Expression<Func<TModel, bool>> filter=null)
            where TModel : class
        {
            IncrementOperationCounter(false);
            
            int result = filter == null? this.GetDbSet<TModel>().Count(): this.GetDbSet<TModel>().Count(filter);

            return result;
        }

        /// <summary>
        /// Gets whether the object(s) exists in the datastore.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// <remarks>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// IRepository<Example> _exampleRepository = ...
        /// bool hasExpensiveStuff =
        ///   _exampleRepository.Contains(ex=>ex.Price > 10000);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// </remarks>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        public virtual bool Contains<TModel>(Expression<Func<TModel, bool>> filter)
            where TModel : class
        {
            IncrementOperationCounter(false);

            bool result = this.GetDbSet<TModel>().Count(filter) > 0;
            return result;
        }



        //[Obsolete("Doesn't give much value. Far better to use the other GetSingle")]
        //public TModel GetSingle(params object[] keys)
        //{
        //    return this.GetDbSet<TModel>().Find(keys);
        //}

        /// <summary>
        /// Find a single entity, based on the given predicate filter.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// 	</para>
        /// </summary>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <param name="includeSpecification">The include.</param>
        /// <param name="mergeOptions">The merge options.</param>
        /// <returns></returns>
        /// <remarks>
        /// Invoked as follows:
        /// <code>
        /// 		<![CDATA[
        /// _exampleRepository
        /// .GetByFilter(
        /// contact => contact.Id >= 3,
        /// new IncludeSpecification("Addresses"));
        /// ]]>
        /// 	</code>
        /// </remarks>
        public virtual TModel GetSingle<TModel>(Expression<Func<TModel, bool>> filter, 
            IIncludeSpecification includeSpecification = null,
            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined
            )
            where TModel : class
        {
            IncrementOperationCounter(false);

            IQueryable<TModel> query = this.GetDbSet<TModel>();

            //Add the includes if any 
            //Note that each provider determines the format
            //in this case we are usign the string values of the properties
            //to include. Another ORM, it might be Types, etc.
            if (includeSpecification != null && includeSpecification.Includes.Length > 0)
            {
// ReSharper disable UseMethodIsInstanceOfType
// ReSharper disable UseIsOperator.1
                if (typeof(IIncludeSpecification<TModel>).IsAssignableFrom(includeSpecification.GetType()))
// ReSharper restore UseIsOperator.1
// ReSharper restore UseMethodIsInstanceOfType
                // ReSharper restore UseIsOperator.1
                // ReSharper restore UseMethodIsInstanceOfType
                {
                    Expression<Func<TModel, object>>[] typedIncludes =
                        ((Expression<Func<TModel, object>>[])includeSpecification.Includes);

                    query = typedIncludes.Aggregate(
                        query,
                        (current, include) => current.Include(include));
                }
                else
                {

                    // ReSharper disable LoopCanBeConvertedToQuery
                    foreach (object includeProperty in includeSpecification.Includes)
                        // ReSharper restore LoopCanBeConvertedToQuery
                    {
                        // ReSharper disable UseMethodIsInstanceOfType
                        // ReSharper disable UseIsOperator.1
                        if (
                            typeof (Expression<Func<TModel, object>>).IsAssignableFrom(
                                includeProperty.GetType()))
                            // ReSharper restore UseIsOperator.1
                            // ReSharper restore UseMethodIsInstanceOfType
                        {
                            Expression<Func<TModel, object>> expression =
                                includeProperty as Expression<Func<TModel, object>>;
                            query = query.Include(expression);
                            //expression.Compile().Invoke(e).Dump();
                            //typedOptions =  (Expression<Func<E,object>>[]) x.Includes;
                        }
                        else
                        {
                            query = query.Include((string) includeProperty);
                        }
                    }
                }
            }


            //Dealing with merge:
            //http://blogs.msdn.com/b/dsimmons/archive/2010/01/12/ef-merge-options-and-compiled-queries.aspx
            if (mergeOptions != XAct.Domain.Repositories.MergeOption.Undefined)
            {
                ((ObjectQuery) query).MergeOption = mergeOptions.MapTo();
            }


            return query.FirstOrDefault(filter);
        }




        /// <summary>
        /// Returns an <see cref="IQueryable"/> of all entities
        /// in the datastore that match the given filter,
        /// using the given paging specs.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// 	</para>
        /// 	<para>IMPORTANT: Note the Syntax for the OrderBy (q =&gt; q.OrderBy(d =&gt; d.Name)), it is not just a property reference (and therefore allows for Field + Direction + Chaining).</para>
        /// 	<para>
        /// Invoke as follows:
        /// <code>
        /// 			<![CDATA[
        /// var departmentsQuery =
        /// _exampleRepository
        /// .GetByFilter(
        /// contact => contact.Age >= 18
        /// paging: new PagedQuerySpecification(20,0),
        /// orderBy: q => q.OrderBy(d => d.Name).ThenBy(d=>d.Order),
        /// new IncludeSpecification("Addresses"));
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </summary>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <param name="includeSpecification">The (optional) list of child Entities (Properties) to include in this query to avoid the later cost of Lazy loading.</param>
        /// <param name="pagedQuerySpecifications">The (optional) paged query specs.</param>
        /// <param name="orderBy">The (optional) order by.</param>
        /// <param name="mergeOptions">The merge options.</param>
        /// <returns></returns>
        public virtual IQueryable<TModel> GetByFilter<TModel>(
            Expression<Func<TModel, bool>> filter,
            IIncludeSpecification includeSpecification = null,
            IPagedQuerySpecification pagedQuerySpecifications = null,
            Func<IQueryable<TModel>, IOrderedQueryable<TModel>> orderBy = null,
            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined
            )
            where TModel : class
        {
            //NO: IncrementImmediateOperationCounter();

            IQueryable<TModel> queryableEntities = filter != null ? this.GetDbSet<TModel>().Where(filter) : this.GetDbSet<TModel>();

            return InternalGetByFilter(queryableEntities, includeSpecification, pagedQuerySpecifications, orderBy,mergeOptions);
        }




        /// <summary>
        /// Gets the lookups.
        /// </summary>
        /// <typeparam name="TLookupEntity">The type of the lookup entity.</typeparam>
        /// <param name="filter">The filter.</param>
        /// <param name="includeSpecification">The include specification.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="mergeOptions">The merge options.</param>
        /// <returns></returns>
        protected virtual IQueryable<TLookupEntity> GetLookups<TLookupEntity>(
            Expression<Func<TLookupEntity, bool>> filter = null,
            IIncludeSpecification includeSpecification = null,
            Func<IQueryable<TLookupEntity>, IOrderedQueryable<TLookupEntity>> orderBy = null,
            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined)
            where TLookupEntity : class
        {
            IQueryable<TLookupEntity> queryableEntities = filter != null
                                                              ? this.DbContext.Set<TLookupEntity>().Where(filter)
                                                              : this.DbContext.Set<TLookupEntity>();

            //NO: IncrementOperationCounter();
            
            return InternalGetByFilter(queryableEntities, includeSpecification, null, orderBy, mergeOptions);
        }



        /// <summary>
        /// The internal GetByFilter
        /// <para>
        /// WARNING: Somewhat dangerous.
        /// </para>
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="queryableEntities">The queryable entities.</param>
        /// <param name="includeSpecification">The include specification.</param>
        /// <param name="pagedQuerySpecifications">The paged query specifications.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="mergeOptions">The merge options.</param>
        /// <returns></returns>
        protected virtual IQueryable<TEntity> InternalGetByFilter<TEntity>(
            IQueryable<TEntity> queryableEntities,
            IIncludeSpecification includeSpecification = null,
            IPagedQuerySpecification pagedQuerySpecifications = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined
            )
            where TEntity : class
        {

            IncrementOperationCounter(false);

            if (pagedQuerySpecifications != null && pagedQuerySpecifications.RetrieveTotalRecordsInSource)
            {
                //Note that this will cause a second sql statement to be executed...
                //Rather poor show MS...
                pagedQuerySpecifications.TotalRecordsInSource = queryableEntities.Count();
            }


            //Add the includes if any 
            //Note that each provider determines the format
            //in this case we are usign the string values of the properties
            //to include. Another ORM, it might be Types, etc.
            if (includeSpecification != null && includeSpecification.Includes.Length > 0)
            {

// ReSharper disable UseMethodIsInstanceOfType
// ReSharper disable UseIsOperator.1
                if (typeof(IIncludeSpecification<TEntity>).IsAssignableFrom(includeSpecification.GetType()))
                // ReSharper restore UseIsOperator.1
                // ReSharper restore UseMethodIsInstanceOfType
                {
                    Expression<Func<TEntity,object>>[] typedIncludes =
                        ((Expression<Func<TEntity, object>>[])includeSpecification.Includes);

                    queryableEntities = typedIncludes.Aggregate(
                        queryableEntities, 
                        (current, include) => current.Include(include));
                }
                else
                {

                    // ReSharper disable LoopCanBeConvertedToQuery
                    foreach (object includeProperty in includeSpecification.Includes)
                        // ReSharper restore LoopCanBeConvertedToQuery
                    {
                        // ReSharper disable UseMethodIsInstanceOfType
                        // ReSharper disable UseIsOperator.1
                        if (
                            typeof (Expression<Func<TEntity, object>>).IsAssignableFrom(
                                includeProperty.GetType()))
                            // ReSharper restore UseIsOperator.1
                            // ReSharper restore UseMethodIsInstanceOfType
                        {
                            Expression<Func<TEntity, object>> expression =
                                includeProperty as Expression<Func<TEntity, object>>;
                            queryableEntities = queryableEntities.Include(expression);
                            //expression.Compile().Invoke(e).Dump();
                            //typedOptions =  (Expression<Func<E,object>>[]) x.Includes;
                        }
                        else
                        {
                            queryableEntities = queryableEntities.Include((string) includeProperty);
                        }
                    }
                }
            }

            if (orderBy != null)
            {
                queryableEntities = orderBy(queryableEntities);
            }

            if (pagedQuerySpecifications != null)
            {
                //if (pagedQuerySpecifications.PageIndex == 0)
                //{
                //    throw new ArgumentOutOfRangeException("pagedQuerySpecifications.PageIndex cannot be less than 1.");
                //}
                //WHAT THE FUCK WAS I THIKING?!? int skipCount = (pagedQuerySpecifications.PageIndex - 1) * pagedQuerySpecifications.PageSize;
                int skipCount = pagedQuerySpecifications.PageIndex * pagedQuerySpecifications.PageSize;

                queryableEntities = skipCount == 0
                               ? queryableEntities.Take(pagedQuerySpecifications.PageSize)
                               : queryableEntities.Skip(skipCount).Take(pagedQuerySpecifications.PageSize);
            }


            //Dealing with merge:
            //http://blogs.msdn.com/b/dsimmons/archive/2010/01/12/ef-merge-options-and-compiled-queries.aspx
            if (mergeOptions != XAct.Domain.Repositories.MergeOption.Undefined)
            {
                ((ObjectQuery) queryableEntities).MergeOption = mergeOptions.MapTo();
            }

            return queryableEntities;

        }






        /// <summary>
        /// Persists the given entity.
        /// <para>
        /// The implementation invokes <paramref name="isEntityNew"/>
        /// to determine whether to invoke
        /// <see cref="AddOnCommit{TModel}"/> (for new Aggregate Root Entities)
        /// or <see cref="UpdateOnCommit{TModel}"/>
        /// 	</para>
        /// 	<para>
        /// Defined in <see cref="IRepositoryService"/>
        /// 	</para>
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <param name="model">The aggregate root.</param>
        /// <param name="isEntityNew">The is entity new.</param>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when
        /// the UnitOfWork Commits the underlying ORM Context.
        /// </remarks>
        public virtual void PersistOnCommit<TModel>(TModel model, Func<TModel,bool> isEntityNew)
            where TModel : class
        {
            //NO: IncrementDeferredOperationCounter();

            if (isEntityNew(model))
            {
                AddOnCommit(model);
            }
            else
            {
                UpdateOnCommit(model);
            }

        }


        /// <summary>
        /// Adds or updates a model which matches the given criteria.
        /// Used only for seeding.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="identifierExpression">The identifier expression.</param>
        /// <param name="models">The models.</param>
        public virtual void AddOrUpdate<TModel>(Expression<Func<TModel, object>> identifierExpression, params TModel[] models)
            where TModel : class
        {
            this.DbContext.Set<TModel>().AddOrUpdate( identifierExpression, models);
        }


        /// <summary>
        /// Persists the on commit.
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <typeparam name="TId">The type of the identifier.</typeparam>
        /// <param name="model">The aggregate root entity.</param>
        /// <param name="generateDistributeIdentityIfRequired">if set to <c>true</c> [generate distribute identity if required].</param>
        public virtual void PersistOnCommit<TModel,TId>(TModel model, bool generateDistributeIdentityIfRequired=false)
            where TModel : class, IHasId<TId>
        {
            //NO: IncrementDeferredOperationCounter();
            
            if (IsNew<TModel, TId>(model, generateDistributeIdentityIfRequired))
            {
                AddOnCommit(model);
            }
            else
            {
                UpdateOnCommit(model);
            }

        }

        /// <summary>
        ///   Adds the given entity to the datastore,
        ///   when the active 
        ///   <see cref = "IUnitOfWork" />
        ///   is Committed.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <param name = "model"></param>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// </remarks>
        public virtual void AddOnCommit<TModel>(TModel model)
            where TModel : class
        {

            IncrementOperationCounter(true);

            this.GetDbSet<TModel>().Add(model);

            //return ForceCommit(forceCommitNow);
        }


        /// <summary>
        /// Adds the given model to the datastore,
        /// when the active
        /// <see cref="IUnitOfWork" />
        /// is Committed.
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService" />
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <typeparam name="TId">The type of the identifier.</typeparam>
        /// <param name="model">The aggregate root entity.</param>
        /// <param name="generateDistributeIdentityIfRequired">if set to <c>true</c>, generates a distribute identity (guid) if required.</param>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects
        /// or displays the results.
        /// In other words, the Controller get's DI'ed
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page.
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        public virtual void AddOnCommit<TModel, TId>(TModel model, bool generateDistributeIdentityIfRequired = true)
            where TModel : class, IHasId<TId>
        {
            IncrementOperationCounter(true);

            //Either way, we give it a value:
            IsNew<TModel, TId>(model, generateDistributeIdentityIfRequired);
            
            AddOnCommit(model);

            this.GetDbSet<TModel>().Add(model);

            //return ForceCommit(forceCommitNow);
        }



        

        /// <summary>
        /// Attaches untracked -- but already saved at some point --
        /// entities.the on commit.
        /// <para>
        /// NOTE: When possible (not all ORMs can pull it off) prefer
        /// <see cref="UpdateOnCommit{TModel}"/> to <see cref="AttachOnCommit{TModel}"/>
        /// as higher levels should not have to keep track of what has
        /// been added or not.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// </remarks>
        /// <param name="model">The aggregate root entity.</param>
        /// <returns></returns>
        public void AttachOnCommit<TModel>(TModel model)
            where TModel : class
        {
            IncrementOperationCounter(true);

            this.GetDbSet<TModel>().Attach(model);
            
            this.DbContext.Entry(model).State = EntityState.Modified;

            //return ForceCommit(forceCommitNow);
        }

        /// <summary>
        /// Detaches the specified aggregate root entity.
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <param name="model">The aggregate root entity.</param>
        public void Detach<TModel>(TModel model)
            where TModel : class
        {
            //NO:IncrementOperationCounter();

            this.DbContext.Entry(model).State = EntityState.Detached;
        }

        /// <summary>
        /// Update object changes and save to database.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <param name="model">Specified the object to save.</param>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// </remarks>
        public virtual void UpdateOnCommit<TModel>(TModel model)
            where TModel : class
        {
            IncrementOperationCounter(true);

            DbEntityEntry<TModel> dbEntityEntry = this.DbContext.Entry(model);

            if ( ((int)dbEntityEntry.State).BitIsSet((int)EntityState.Detached))
            {
                this.GetDbSet<TModel>().Attach(model);
            }
            //Is this really needed?
            if (((int)dbEntityEntry.State).BitIsNotSet((int)EntityState.Modified))
            {

                dbEntityEntry.State |= EntityState.Modified;
            }

            //return ForceCommit(forceCommitNow);

        }

        /// <summary>
        /// Deleting without retrieveing Element.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TId"></typeparam>
        /// <param name="id"></param>
        public virtual void DeleteOnCommit<TModel, TId>(TId id)
            where TModel:class, IHasId<TId>,new()
        {
            TModel model = System.Activator.CreateInstance<TModel>();
            model.Id = id;

            
            DbEntityEntry<TModel> entityEntry = this.DbContext.Entry(model);

            entityEntry.State = EntityState.Deleted;
        

        }

        /// <summary>
        ///   Removes the given entity from the datastore
        ///   when the active 
        ///   <see cref = "IUnitOfWork" />
        ///   is Committed.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// </remarks>
        /// <param name = "model"></param>
        /// <returns>The number of Elements changed in the current commit (may not be exact match to number of deleted).</returns>
        public virtual void DeleteOnCommit<TModel>(TModel model)
            where TModel : class
        {

            IncrementOperationCounter(true);

            //Revert values:
            try
            {
                DbEntityEntry<TModel> entityEntry = this.DbContext.Entry(model);
                //We reset original values so that Auditing (see AuditableChangesStrategy)
                //records Info *before* changes...
                entityEntry.CurrentValues.SetValues(entityEntry.OriginalValues);
            }
            catch
            {
                
            }


            this.GetDbSet<TModel>().Remove(model);

            //return ForceCommit(forceCommitNow);
        }

        /// <summary>
        /// Removes entities from the datastore
        /// that match the given filter,
        /// when the active
        /// <see cref="IUnitOfWork"/>
        /// is Committed.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// </remarks>
        /// <returns>The number of Elements changed in the current commit (may not be exact match to number of deleted).</returns>
        public virtual void DeleteOnCommit<TModel>(Expression<Func<TModel, bool>> filter)
            where TModel : class
        {
            IncrementOperationCounter(true);
            
            // ReSharper disable RedundantArgumentDefaultValue
            IQueryable<TModel> aggregateRootEntities = this.GetByFilter(filter,null,null);
// ReSharper restore RedundantArgumentDefaultValue

            foreach (TModel entity in aggregateRootEntities)
            {
                this.GetDbSet<TModel>().Remove(entity);
            }


            DbContext dbContext = this.DbContext;
            DbSet<TModel> dbSet = dbContext.Set<TModel>();

            foreach (TModel entity in aggregateRootEntities)
            {
                DbEntityEntry<TModel> entityEntry = dbContext.Entry(entity);
                EntityState state = entityEntry.State;
                if (((int)state).BitIsSet((int)EntityState.Detached))
                {
                    dbSet.Attach(entity);
                }

                dbContext.Set<TModel>().Remove(entity);
                try
                {
                    //We reset original values so that Auditing (see AuditableChangesStrategy)
                    //records Info *before* changes...
                    entityEntry.CurrentValues.SetValues(entityEntry.OriginalValues);
                }
                catch
                {

                }
            }

            
            //dbContext.SaveChanges();//Not sure why we are doing this now...


        }


        /// <summary>
        /// Executes the sql statement.
        /// <para>
        /// As per http://stackoverflow.com/questions/4873607/how-to-use-dbcontext-database-sqlquerytelementsql-params-with-stored-proced
        /// If a Stored Proc, don't forget to prefix with EXEC.
        /// </para>
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="argumentsPackage">The arguments package.</param>
        /// <param name="outArgumentsPackage">The out arguments package.</param>
        /// <returns></returns>
        /// <remarks>
        /// Usage is as follows:
        /// <para>
        /// 		<code>
        /// 			<![CDATA[
        /// context.Database.SqlQuery<EntityType>(
        /// "EXEC ProcName @param1, @param2",
        /// new SqlParameter("param1", param1),
        /// new SqlParameter("param2", param2));
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </remarks>
        protected virtual IEnumerable<TEntity> ExecuteSqlQuery<TEntity>(string sqlStatement, object argumentsPackage = null, object outArgumentsPackage = null)
        {
            IncrementOperationCounter(false);

            IDbDataParameter[] p2 = ConvertObjectToParameters(argumentsPackage, outArgumentsPackage);

            IEnumerable<TEntity> result = ExecuteSqlQuery<TEntity>(sqlStatement, p2);

            //Now take variables returned and use to pass values back, via out object:
            foreach (IDbDataParameter parameter in p2)
            {
                if ((parameter.Direction == ParameterDirection.Output || parameter.Direction == ParameterDirection.InputOutput))
                {
                    outArgumentsPackage.SetMemberValue(parameter.ParameterName,parameter.Value);
                }
            }

            return result;
        }

        /// <summary>
        /// Executes the sql statement.
        /// <para>
        /// As per http://stackoverflow.com/questions/4873607/how-to-use-dbcontext-database-sqlquerytelementsql-params-with-stored-proced
        /// If a Stored Proc, don't forget to prefix with EXEC.
        /// </para>
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <remarks>
        /// Usage is as follows:
        /// <para>
        /// 		<code>
        /// 			<![CDATA[
        /// context.Database.SqlQuery<EntityType>(
        /// "EXEC ProcName @param1, @param2",
        /// new SqlParameter("param1", param1),
        /// new SqlParameter("param2", param2));
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </remarks>
        protected virtual IEnumerable<TEntity> ExecuteSqlQuery<TEntity>(string sqlStatement, params IDbDataParameter[] parameters)
        {
            IncrementOperationCounter(false);

            //Multiple results:
            //Redo: http://romiller.com/2012/08/15/code-first-stored-procedures-with-multiple-results/

            IEnumerable<TEntity> result = (parameters != null)
                                   ? this.DbContext.Database.SqlQuery<TEntity>(sqlStatement, parameters)
                                   : this.DbContext.Database.SqlQuery<TEntity>(sqlStatement);


            return result;
        }


        /// <summary>
        /// Executes the SQL command (DELETE, UPDATE, etc).
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="argumentsPackage">The arguments package.</param>
        /// <param name="outArgumentsPackage">The out arguments package.</param>
        /// <returns></returns>
        protected virtual int ExecuteSqlCommand<TEntity>(string sqlStatement, object argumentsPackage = null, object outArgumentsPackage = null)
        {
            IncrementOperationCounter(false);
            
            //Multiple results:
            //Redo: http://romiller.com/2012/08/15/code-first-stored-procedures-with-multiple-results/

            IDbDataParameter[] p2 = ConvertObjectToParameters(argumentsPackage, outArgumentsPackage);

            int rowsAffected = (p2 != null)
                                   ? this.DbContext.Database.ExecuteSqlCommand(sqlStatement, p2)
                                   : this.DbContext.Database.ExecuteSqlCommand(sqlStatement);

            //Now take variables returned and use to pass values back, via out object:
            foreach (IDbDataParameter parameter in p2)
            {
                if ((parameter.Direction == ParameterDirection.Output || parameter.Direction == ParameterDirection.InputOutput))
                {
                    outArgumentsPackage.SetMemberValue(parameter.ParameterName,parameter.Value);
                }
            }

            return rowsAffected;
        }


        /// <summary>
        /// Executes the SQL command.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sqlStatement">The SQL statement.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        protected virtual int ExecuteSqlCommand<TEntity>(string sqlStatement, params IDbDataParameter[] parameters)
        {
            IncrementOperationCounter(false);


            int rowsAffected = (parameters != null)
                                   ? this.DbContext.Database.ExecuteSqlCommand(sqlStatement, parameters)
                                   : this.DbContext.Database.ExecuteSqlCommand(sqlStatement);

            return rowsAffected;
        }
        
        
        private IDbDataParameter[] ConvertObjectToParameters(object argumentsPackage = null, object argumentsOutPackage = null)
        {

            IList<IDbDataParameter> parameters2 = new List<IDbDataParameter>();

            IDbCommand command = this.DbContext.Database.Connection.CreateCommand();

            if (argumentsPackage == null)
            {
                IDictionary<string, object> prps = argumentsPackage.GetObjectPropertyValues();

                foreach (KeyValuePair<string, object> o in prps)
                {
                    IDbDataParameter param = command.CreateParam(o.Key, o.Value);
                    parameters2.Add(param);
                }
            }
            if (argumentsOutPackage == null)
            {
                IDictionary<string, object> prps = argumentsOutPackage.GetObjectPropertyValues();
                foreach (KeyValuePair<string, object> o in prps)
                {
                    IDbDataParameter param = command.CreateParam(o.Key, o.Value);
                    param.Direction = ParameterDirection.Output;
                    parameters2.Add(param);
                }
            }

            return parameters2.ToArray();
        }


        /// <summary>
        /// Determines whether the specified entity is attached, and within the dbcontext.
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <param name="model">The aggregate root entity.</param>
        /// <returns></returns>
        public virtual bool IsAttached<TModel>(TModel model)
            where TModel : class
        {
            DbEntityEntry<TModel> dbEntityEntry = this.DbContext.Entry(model);
            {
                return !((int) dbEntityEntry.State).BitIsSet((int) EntityState.Detached);
            }
        }



        /// <summary>
        /// Determines whether [is entity new] [the specified model].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TId">The type of the identifier.</typeparam>
        /// <param name="model">The model.</param>
        /// <param name="generateIdentity">if set to <c>true</c> [generate identity].</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException">Havn't chosen an appropriate sequential int/long number generator. See SnowFlake.</exception>
        public bool IsNew<T, TId>(T model, bool generateIdentity = false)
            where T : IHasId<TId>
        {
            TId id = model.Id;

            //Timestamp is still the best guarantee that record was persisted.
            IHasTimestamp timestampped = model as IHasTimestamp;
            if (timestampped!=null)
            {

                //Has Guid, and timestamp, means its new if timetamp is still null:
                //and we don't set it either way:
                return (timestampped.Timestamp == null) ? true : false;
            }

            //Then, ensuring the Id is not 0 or Guid.Empty
            if (!object.Equals(id, default(TId)))
            {
                //it has a value:
                return false;
            }

            if (!generateIdentity)
            {

                //Value is 0 or Guid.EMpty.

                //Therefore it's definately new
                //-- but we've been asked to 
                //not set it
                return true;
            }


            //No value -- and we have to generate one.
            if (model.Id is Guid)
            {
                model.Id = _distributedIdService.NewGuid().ConvertTo<TId>();
            }
            else
            {
                throw new NotSupportedException(
                    "Haven't chosen an appropriate sequential int/long number generator. See SnowFlake.");
            }
            return true;
        }



        ///// <summary>
        ///// Determines whether the entity has ever been saved before.
        ///// <para>
        ///// Intended to be invoked by <c>PersistOnCommit</c>
        ///// </para>
        ///// </summary>
        ///// <param name="model">The aggregate root entity.</param>
        ///// <returns>
        /////   <c>true</c> if [is entity new] [the specified aggregate root entity]; otherwise, <c>false</c>.
        ///// </returns>
        //public abstract bool IsEntityNew<TModel>(TModel model)
        //    where TModel : class;


        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        public void Dispose()
        {
        }


        ///// <summary>
        ///// Helper method to force or not at this time.
        ///// </summary>
        ///// <param name="forceCommitNow">if set to <c>true</c> [force commit now].</param>
        ///// <returns></returns>
        //private int ForceCommit(bool forceCommitNow)
        //{
        //    if ((forceCommitNow) || (!_alwaysCommit))
        //    {
        //        return this.DbContext.SaveChanges();
        //    }
        //    return 0;
        //}



	}
} 
    
