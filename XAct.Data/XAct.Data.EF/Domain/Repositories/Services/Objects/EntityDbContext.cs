﻿// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories
// ReSharper restore CheckNamespace
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Diagnostics;
    using System.Linq;
    using XAct;
    using XAct.Diagnostics.Performance.Services;

    /// <summary>
    /// An implementation of the <see cref="IContext"/>
    /// in order to abstract away the EF specific context
    /// so that refering to it doesn't drag a reference to it
    /// all over the application's code.
    /// </summary>
    public class EntityDbContext : IEntityDbContext
    {
        private readonly DbContext _dbContext;


        private IContextStatePerformanceCounterService ContextStatePerformanceCounterService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IContextStatePerformanceCounterService>(); }
        }

        /// <summary>
        /// Gets the <see cref="IUnitOfWorkCommitPreProcessorService"/>
        /// (if any is available).
        /// </summary>
        /// <value>
        /// The unit of work commit preprocessor service.
        /// </value>
        public IUnitOfWorkCommitPreProcessorService UnitOfWorkCommitPreprocessorService
        {
            get
            {
                return _unitOfWorkCommitPreprocessorService ??
                       (_unitOfWorkCommitPreprocessorService =
                        XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorService>(false));
            }
        }
        private IUnitOfWorkCommitPreProcessorService _unitOfWorkCommitPreprocessorService;



        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "EntityDbContext" /> class.
        /// </summary>
        /// <param name = "dbContext">The object context.</param>
        public EntityDbContext(DbContext dbContext)
        {
            if (dbContext.IsNull()){throw new ArgumentNullException("dbContext");}

            _dbContext = dbContext;

            ((IObjectContextAdapter)dbContext).ObjectContext.SavingChanges +=ObjectContext_SavingChanges;
        }

        private void ObjectContext_SavingChanges(object sender, EventArgs e)
        {
            if (UnitOfWorkCommitPreprocessorService != null)
            {
                UnitOfWorkCommitPreprocessorService.Process(this);
            }
        }

        /// <summary>
        /// Commit pending changes to the
        /// DataStore.
        /// </summary>
        /// <param name="commitType"></param>
        /// <returns>
        /// The number of objects written to the underlying database.
        /// </returns>
        public int Commit(CommitType commitType = CommitType.Default)
        {
            int result;

            //This is where one updates records before saving.

            //var changeSet = _dbContext.ChangeTracker.Entries<User>();

            IObjectContextAdapter objectContextAdapter = _dbContext;

            ObjectContext objectContext = objectContextAdapter.ObjectContext;

            var changedEntities = _dbContext.GetEntityEntries().ToList();

            int changed = changedEntities.Count();
            int added = changedEntities.Count(x => x.State == EntityState.Added);
            int modified = changedEntities.Count(x => x.State == EntityState.Modified);
            int deleted = changedEntities.Count(x => x.State == EntityState.Deleted);


            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                switch (commitType)
                {
                    case CommitType.AcceptAllChanges:
                        result = objectContext.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                        break;
                    case CommitType.DetectAndHoldChangesTillComplete:
                        result = objectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave);
                        break;
                    case CommitType.HoldChangesTillComplete:
                        result = objectContext.SaveChanges(SaveOptions.None);
                        break;
                    case CommitType.CompleteChanges:
                        objectContext.AcceptAllChanges();
                        result = 0;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("commitType");
                }

                stopwatch.Stop();

                ContextStatePerformanceCounterService.IncrementContextOperationCounter(XAct.Constants.PerformanceCounters.RepositoryServiceQueriesCommitEntitiesChanged, changed);
                ContextStatePerformanceCounterService.IncrementContextOperationCounter(XAct.Constants.PerformanceCounters.RepositoryServiceQueriesCommitEntitiesAdded, added);
                ContextStatePerformanceCounterService.IncrementContextOperationCounter(XAct.Constants.PerformanceCounters.RepositoryServiceQueriesCommitEntitiesUpdated, modified);
                ContextStatePerformanceCounterService.IncrementContextOperationCounter(XAct.Constants.PerformanceCounters.RepositoryServiceQueriesCommitEntitiesDeleted, deleted);
            }
            finally
            {
                if (stopwatch.IsRunning)
                {
                    stopwatch.Stop();
                }

                ContextStatePerformanceCounterService.IncrementContextOperationDuration(XAct.Constants.PerformanceCounters.RepositoryServiceQueriesCommitDuration, stopwatch.Elapsed);
            }


            return result;
        }

        /// <summary>
        ///   Performs application-defined tasks associated with 
        ///   freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _dbContext.Dispose();
        }


        ///// <summary>
        ///// Gets the DbSet for the given entity.
        ///// <para>
        ///// Note: Intended to be implemented Explicitly,to not drag around dependencies.
        ///// </para>
        ///// 	<para>
        ///// Defined in <see cref="IEntityDbContext"/>
        ///// 	</para>
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <returns></returns>
        //DbSet<T> IEntityDbContext.CreateDbSet<T>() 
        //{
        //    return _dbContext.Set<T>();
        //}

        /// <summary>
        /// Gets the inner object (the EF CodeFirst DbContext).
        /// <para>
        /// This property is used by the EF based Repository
        /// to extract the vendor specific ObjectContext 
        /// from this Vendor agnostic wrapper.
        /// </para>
        /// </summary>
        /// <value>
        /// The inner object.
        /// </value>
        T IHasInnerItemReadOnly.GetInnerItem<T>()
        {
            //DO NOT DO THIS AS WE DO NOT WANT TO CONVERT IT, JUST TYPE IT:
            //(Plus does not work, as does not implement IConvertible):
        	//T result = _dbContext.ConvertTo<T>();
            //return result;

            return (T)(object)_dbContext;
            
        }

    }
}
