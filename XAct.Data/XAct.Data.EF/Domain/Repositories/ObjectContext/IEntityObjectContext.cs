﻿// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Provides an Entity Framework 4 (EF4) EntityContext
    /// </summary>
    public interface IEntityObjectContext : IContext
    {
        ///// <summary>
        ///// <para>
        ///// Note: Intended to be implemented Explicitly.
        ///// </para>
        ///// <para>
        ///// Defined in <see cref="IEntityObjectContext"/>
        ///// </para>
        ///// </summary>
        ///// <typeparam name = "T"></typeparam>
        ///// <returns></returns>
        //ObjectSet<T> CreateObjectSet<T>() where T : class;


    }
}