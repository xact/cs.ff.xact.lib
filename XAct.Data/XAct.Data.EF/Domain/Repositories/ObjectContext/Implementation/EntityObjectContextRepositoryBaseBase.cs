//// ReSharper disable CheckNamespace
//namespace XAct.Domain.Repositories
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Data.Entity;
//    using System.Data.Entity.Core.Objects;
//    using System.Diagnostics;
//    using System.Linq;
//    using System.Linq.Expressions;
//    using XAct.Messages;

//    /// <summary>
//    /// An abstract base class for an EF specific Repository,
//    /// using EF's ObjectContext/ObjectSet mechanism.
//    /// <para>
//    /// This is an internal class, not intended to be used directly.
//    /// </para>
//    /// <para>
//    /// Use <see cref="EntityObjectContextRepositoryBase{TAggregateRootEntity}"/>
//    /// </para>
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Without any public Constructors, this class is intended to be 
//    /// used only as the base class for 
//    /// <see cref="EntityObjectContextRepositoryBase{TAggregateRootEntity}"/>
//    /// </para>
//    /// </remarks>
//    /// <typeparam name = "TAggregateRootEntity"></typeparam>
//    public abstract class EntityObjectContextRepositoryBaseBase<TAggregateRootEntity> : IRepositoryService<TAggregateRootEntity>
//        where TAggregateRootEntity : class
//    {

//        /// <summary>
//        /// EF EntityName ("ContainerName.EntityName")
//        /// </summary>
//        protected string EntityName;

//        private readonly IUnitOfWorkManagementService _unitOfWorkManagementService;


//        /// <summary>
//        /// Gets the current <see cref="IUnitOfWork"/> from the underlying 
//        /// <see cref="IUnitOfWorkManagementService"/>.
//        /// <para>
//        /// Use that to <c>Execute</c> it's internal <see cref="IContext"/>.
//        /// </para>
//        /// <para>
//        /// Note that I am not especially happy having the Repository (Domain) have a reference 
//        /// to an infrastructural context -- but it's just easier not having to inject two
//        /// different contracts into every location the contract is needed.
//        /// </para>
//        /// </summary>
//        public IUnitOfWork Context
//        {
//            get
//            {

//                IUnitOfWork unitOfWork =
//                    _unitOfWorkManagementService.GetCurrent<IEntityDbContext>();

//                return unitOfWork;
//            }
//        }

//        /// <summary>
//        /// The EF <see cref="ObjectContext"/>
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// The property is PRIVATE, in order to not 
//        /// drag Assembly dependencies to higher layers.
//        /// </para>
//        /// </remarks>
//        /// <internal>
//        /// Return arg can be <see cref="ObjectContext"/>
//        /// rather than interface: we're in privacy of this class...
//        /// </internal>
//        private ObjectContext ObjectContext
//        {
//            get
//            {
//                //Always check first if a the UnitOfWork was 
//                //set via the Constructor first:
//                if (_objectContext != null)
//                {
//                    return _objectContext;
//                }

//                //If not defined by constructor, use the service to get the
//                //current UoW, that matches this type of Repository:
//                IUnitOfWork unitOfWork = _unitOfWorkManagementService.GetCurrent<IEntityObjectContext>();

//                //From which the ObjectContext can be retrieved:
//                IHasInnerItemReadOnly objectWrapper = (IHasInnerItemReadOnly)unitOfWork;

//                IEntityObjectContext abstractObjectContext =
//                    (IEntityObjectContext) objectWrapper.GetInnerItem<IEntityObjectContext>();

//                //From that abstract wrapper of wrappers, 
//                //we can extract the vendor specific object object:
//                ObjectContext objectContext = abstractObjectContext.GetInnerItem<ObjectContext>();

//                return objectContext;

//            }
//        }
//        //Note: Property may not be set -- depends on Constructor used
//        private readonly ObjectContext _objectContext;



//        /// <summary>
//        /// Gets the object set.
//        /// </summary>
//        protected ObjectSet<TAggregateRootEntity> ObjectSet
//        {
//            get
//            {
//                //Always check first if a the UnitOfWork was 
//                //set via the Constructor first:
//                if (_objectSet != null)
//                {
//                    return _objectSet;
//                }
                
//                //ObjectContext returns the 'Current' ObjectContext.
//                //It won't change often, I don't think it's expensive
//                //to create a new ObjectSet, and I don't think its important
//                //that we always work with the same ObjectSet within the same ObjectContext...
//                //but still...

//                //Get the ObjectContext specific to this Repository type:
//                ObjectContext objectContext = this.ObjectContext;
                
//                //Try to find in the cache an ObjectSet specific to this ObjectContext:
//                ObjectSet<TAggregateRootEntity> result;

//                //Look in internal Request specific dictionary.
//                //Could even have used IContextService...maybe. Hum. Don't think so.
//                if (_cache.TryGetValue(objectContext, out result))
//                {
//                    return result;
//                }

//                //Ok...Never been created before for this this Context.
//                //Create a new one, and cache it against the current objectContext:
//                _cache.Add(
//                        new KeyValuePair<ObjectContext, ObjectSet<TAggregateRootEntity>>
//                            (objectContext, result = objectContext.CreateObjectSet<TAggregateRootEntity>()));
                
                
//                return result;
//            }
//        }
//        //Note: Property may not be set -- depends on Constructor used
//        private readonly ObjectSet<TAggregateRootEntity> _objectSet;

//        //If not set by constructor, ie, using IUnitOfWorkManagementService
//        //to return current UoW, then the DbSet will have to be linked to
//        //DbContext that the UoW represents.
//        private readonly IDictionary<ObjectContext, ObjectSet<TAggregateRootEntity>> _cache =
//            new Dictionary<ObjectContext, ObjectSet<TAggregateRootEntity>>();

        
//        #region Constructors
//        /// <summary>
//        /// Initializes a new instance of the 
//        /// <see cref="EntityObjectContextRepositoryBaseBase&lt;TAggregateRootEntity&gt;"/> class.
//        /// <para>
//        /// This constructor is used only for simple apps, when
//        /// only one UoW/DbContext will be used throughout the lifespan of the Page.
//        /// </para>
//        /// <para>
//        /// This constructor sets both the internal ObjectContext, and ObjectSet,
//        /// so has no need of the <see cref="IUnitOfWorkManagementService"/>
//        /// required by the other Constructor overload.
//        /// </para>
//        /// </summary>
//        /// <param name="objectContext">The object context.</param>
//        protected EntityObjectContextRepositoryBaseBase(IEntityObjectContext objectContext)
//        {
//            _objectContext = objectContext.GetInnerItem<ObjectContext>();

//            _objectContext.ValidateIsNotNull("objectContext");

//            //Extract from the passed wrapper, the internal
//            //EF specific class:

//            //Note that use 
//            //Covariance is not supported for something like:
//            //IObjectSet<TAggregateRootEntity> _ObjectSet = 
//            //    (IObjectSet<TAggregateRootEntity>)objectContext.CreateObjectSet<TAggregateRootEntity>();
//            // objectContext.CreateObjectSet<TAggregateRootEntity>();
//            _objectSet = _objectContext.CreateObjectSet<TAggregateRootEntity>();
//        }

//        /// <summary>
//        /// Initializes a new instance of the 
//        /// <see cref="EntityObjectContextRepositoryBaseBase&lt;TAggregateRootEntity&gt;"/> class.
//        /// <para>
//        /// This constructor is used for more robust applications
//        /// that can contain more than one UoW...hence the need for 
//        /// a <see cref="IUnitOfWorkManagementService"/>
//        /// </para>
//        /// <para>
//        /// The constructor is for using this Repository as a Service: the ObjectContext and ObjectSet
//        /// will be selected dynamically (using the <see cref="IUnitOfWorkManagementService"/>)
//        ///  per method invocation.
//        /// </para>
//        /// </summary>
//        protected EntityObjectContextRepositoryBaseBase(IUnitOfWorkManagementService unitOfWorkManagementService)
//        {
//            unitOfWorkManagementService.ValidateIsNotNull("unitOfWorkManagementService");
//            _unitOfWorkManagementService = unitOfWorkManagementService;
            
//            EntityName = GetEntityName();

//        }
//        #endregion



//        /// <summary>
//        /// Get the total of objects that meet the criteria.
//        /// </summary>
//        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
//        /// <internal>
//        /// Generally implemented without using <see cref="GetByFilter"/>
//        /// as not all Providers will create the same SQL.
//        /// </internal>
//        public virtual int Count(Expression<Func<TAggregateRootEntity, bool>> filter=null)
//        {
//            return filter==null?this.ObjectSet.Count() : this.ObjectSet.Count(filter);
//        }


//        /// <summary>
//        /// Gets whether the object(s) exists in the datastore.
//        /// </summary>
//        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
//        public bool Contains(Expression<Func<TAggregateRootEntity, bool>> filter)
//        {
//            //Can't do this as easily as one can with a CodeFirst Repo:
//            //return this.ObjectSet.Contains(filter);

//            return ((IRepositoryService<TAggregateRootEntity>)this).GetByFilter(filter,null,null).Take(1).Count() > 0;
//        }



//        //public IQueryable<TAggregateRootEntity> GetAll(IPagedQuerySpecs pagedQuerySpecifications)
//        //{
//        //    int skipCount = pagedQuerySpecifications.PageIndex * pagedQuerySpecifications.PageSize;

//        //    IQueryable<TAggregateRootEntity> resetSet = this.ObjectSet/*.AsQueryable()*/;

//        //    resetSet = skipCount == 0 ? resetSet.Take(pagedQuerySpecifications.PageSize) : resetSet.Skip(skipCount).Take(pagedQuerySpecifications.PageSize);

//        //    pagedQuerySpecifications.SetTotalRecordsFound(resetSet.Count());

//        //    return resetSet.AsQueryable();
//        //}





//        /// <summary>
//        /// Find a single entity, based on the given predicate filter.
//        /// <para>
//        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
//        /// 	</para>
//        /// </summary>
//        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
//        /// <param name="includeSpecification">The include specification.</param>
//        /// <param name="mergeOptions">The merge options.</param>
//        /// <returns></returns>
//        /// <remarks>
//        /// Invoked as follows:
//        /// <code>
//        /// 		<![CDATA[
//        /// _exampleRepository
//        /// .GetByFilter(
//        /// contact => contact.Id >= 3,
//        /// new IncludeSpecification("Addresses"));
//        /// ]]>
//        /// 	</code>
//        /// </remarks>
//        public TAggregateRootEntity GetSingle(Expression<Func<TAggregateRootEntity, bool>> filter, 
//            IIncludeSpecification includeSpecification = null,
//            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined
//            )
//        {
//            //Get the ObjectSet out of the property:
//            IQueryable<TAggregateRootEntity> resetSet = this.ObjectSet;

//            //Add the includes if any 
//            //Note that each provider determines the format
//            //in this case we are usign the string values of the properties
//            //to include. Another ORM, it might be Types, etc.
//            if (includeSpecification != null && includeSpecification.Includes.Length > 0)
//            {
//                // ReSharper disable LoopCanBeConvertedToQuery
//                foreach (object includeProperty in includeSpecification.Includes)
//                // ReSharper restore LoopCanBeConvertedToQuery
//                {
//                    resetSet = resetSet.Include((string)includeProperty);
//                }
//            }

//            //Dealing with merge:
//            //http://blogs.msdn.com/b/dsimmons/archive/2010/01/12/ef-merge-options-and-compiled-queries.aspx
//            if (mergeOptions != MergeOption.Undefined)
//            {
//                ((ObjectQuery) resetSet).MergeOption = mergeOptions.MapTo();
//            }
//            return resetSet.FirstOrDefault(filter);
//        }


//        /// <summary>
//        /// IMPORTANT: Note the Syntax for the OrderBy, it is not just a property reference (and therefore allows for Field + Direction + Chaining).
//        /// </summary>
//        /// <param name="filter">The filter.</param>
//        /// <param name="includeSpecification">The include specification.</param>
//        /// <param name="pagedQuerySpecifications">The paged query specifications.</param>
//        /// <param name="orderBy">The order by.</param>
//        /// <param name="mergeOptions">The merge options.</param>
//        /// <returns></returns>
//        /// <remarks>
//        /// Invoke as follows:
//        /// <code>
//        /// 		<![CDATA[
//        /// var departmentsQuery =
//        /// _exampleRepository
//        /// .GetByFilter(
//        /// contact => contact.Age >= 18
//        /// paging: new PagedQuerySpecification(20,0),
//        /// orderBy: q => q.OrderBy(d => d.Name),
//        /// include: new IncludeSpecification{"Addresses"});
//        /// ]]>
//        /// 	</code>
//        /// </remarks>
//        public virtual IQueryable<TAggregateRootEntity> GetByFilter(
//            Expression<Func<TAggregateRootEntity, bool>> filter,
//            IIncludeSpecification includeSpecification,
//            IPagedQuerySpecification pagedQuerySpecifications=null,
//            Func<IQueryable<TAggregateRootEntity>, IOrderedQueryable<TAggregateRootEntity>> orderBy = null,
//            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined
//            )
//        {

            
//            IQueryable<TAggregateRootEntity> queryableEntities = filter != null 
//                ? 
//                this.ObjectSet.Where(filter) 
//                :
//                this.ObjectSet;

//            if (pagedQuerySpecifications!=null && pagedQuerySpecifications.RetrieveTotalRecordsInSource)
//            {
//                //Note that this will cause a second sql statement to be executed...
//                pagedQuerySpecifications.TotalRecordsInSource = (queryableEntities.Count());
//            }


//            if (includeSpecification != null && includeSpecification.Includes.Length>0)
//            {
//// ReSharper disable LoopCanBeConvertedToQuery
//                foreach (object includeProperty in includeSpecification.Includes)
//// ReSharper restore LoopCanBeConvertedToQuery
//                {
//                    queryableEntities = queryableEntities.Include((string)includeProperty);
//                }
//            }

//            //The method 'Skip' is only supported for sorted input in LINQ to Entities. The method 'OrderBy' must be called before the method 'Skip'.

//            if (orderBy != null)
//            {
//                queryableEntities = orderBy(queryableEntities);
//            }


//            if (pagedQuerySpecifications!=null)
//            {
//                //WHAT THE FUCK WAS I THINKING?!? int skipCount = (pagedQuerySpecifications.PageIndex - 1) * pagedQuerySpecifications.PageSize;
//                int skipCount = pagedQuerySpecifications.PageIndex * pagedQuerySpecifications.PageSize;
            
//                queryableEntities = skipCount == 0
//                               ? queryableEntities.Take(pagedQuerySpecifications.PageSize)
//                               : queryableEntities.Skip(skipCount).Take(pagedQuerySpecifications.PageSize);
//            }


//            //Dealing with merge:
//            //http://blogs.msdn.com/b/dsimmons/archive/2010/01/12/ef-merge-options-and-compiled-queries.aspx
//            if (mergeOptions != MergeOption.Undefined)
//            {
//                ((ObjectQuery)queryableEntities).MergeOption = mergeOptions.MapTo();
//            }


//            return queryableEntities;

//        }



//        /// <summary>
//        /// Persists the given entity.
//        /// <para>
//        /// The implementation invokes <see cref="IsEntityNew"/>
//        /// to determine whether to invoke 
//        /// <see cref="AddOnCommit"/> (for new Aggregate Root Entities)
//        /// or <see cref="UpdateOnCommit"/>
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// There is no need to return the number of records committed,
//        /// as it won't be happening just then -- only when 
//        /// the UnitOfWork Commits the underlying ORM Context.
//        /// </remarks>
//        /// <param name="aggregateRootEntity">The aggregate root.</param>
//        /// <returns></returns>
//        public virtual void PersistOnCommit(TAggregateRootEntity aggregateRootEntity)
//        {
//            if (IsEntityNew(aggregateRootEntity))
//            {
//                AddOnCommit(aggregateRootEntity);
//            }
//            else
//            {
//                UpdateOnCommit(aggregateRootEntity);
//            }

//        }



//        /// <summary>
//        ///   Adds the given aggregateRootEntity to the datastore,
//        ///   when the active 
//        ///   <see cref = "IUnitOfWork" />
//        ///   is Committed.
//        /// <para>
//        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// There is no need to return the number of records committed,
//        /// as it won't be happening just then -- only when 
//        /// the UnitOfWork Commits the underlying ORM Context.
//        /// </remarks>
//        /// <param name = "aggregateRootEntity"></param>
//        public void AddOnCommit(TAggregateRootEntity aggregateRootEntity)
//        {
//            this.ObjectSet.AddObject(aggregateRootEntity);

//            //return ForceCommit(forceCommitNow);
//        }



//        /// <summary>
//        /// Attaches untracked -- but already saved at some point --
//        /// entities.
//        /// <para>
//        /// NOTE: When possible (not all ORMs can pull it off) prefer
//        /// <see cref="UpdateOnCommit"/> to <see cref="AttachOnCommit"/>
//        /// as higher levels should not have to keep track of what has
//        /// been added or not.
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// There is no need to return the number of records committed,
//        /// as it won't be happening just then -- only when 
//        /// the UnitOfWork Commits the underlying ORM Context.
//        /// </remarks>
//        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
//        /// <returns></returns>
//        public void AttachOnCommit(TAggregateRootEntity aggregateRootEntity)
//        {
//            this.ObjectSet.Attach(aggregateRootEntity);

//            //return ForceCommit(forceCommitNow);
//        }

//        /// <summary>
//        /// Update object changes and save to database.
//        /// </summary>
//        /// <param name="aggregateRootEntity">Specified the object to save.</param>
//        /// <remarks>
//        /// There is no need to return the number of records committed,
//        /// as it won't be happening just then -- only when 
//        /// the UnitOfWork Commits the underlying ORM Context.
//        /// </remarks>
//        public virtual void UpdateOnCommit(TAggregateRootEntity aggregateRootEntity)
//        {
//            ObjectStateEntry objectStateEntry;
//            if (!this.ObjectContext.ObjectStateManager.TryGetObjectStateEntry(aggregateRootEntity, out objectStateEntry))
//            {
//                this.ObjectSet.Attach(aggregateRootEntity);
//                objectStateEntry = this.ObjectContext.ObjectStateManager.GetObjectStateEntry(aggregateRootEntity);

//            }else
//            {
//              if (((int)objectStateEntry.State).BitIsSet((int)EntityState.Detached))
//              {
//                  this.ObjectSet.Attach(aggregateRootEntity);
//              }  
//            }
//            //Is this really needed?
//            if (((int)objectStateEntry.State).BitIsNotSet((int)EntityState.Modified))
//            {
//                Debug.Assert(true);
//                objectStateEntry.SetModified();
//                //Or this way:
//                //this.ObjectContext.ObjectStateManager.ChangeObjectState(aggregateRootEntity, EntityState.Modified);
//            }

//            //See: http://bit.ly/vVCecC
//            //this.ObjectContext.ApplyCurrentValues(this.EntityName, aggregateRootEntity);
            
//            //return ForceCommit(forceCommitNow);
//        }


//        /// <summary>
//        /// Removes the given aggregateRootEntity from the datastore
//        /// when the active
//        /// <see cref="IUnitOfWork"/>
//        /// is Committed.
//        /// <para>
//        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
//        /// 	</para>
//        /// </summary>
//        /// <remarks>
//        /// There is no need to return the number of records committed,
//        /// as it won't be happening just then -- only when 
//        /// the UnitOfWork Commits the underlying ORM Context.
//        /// </remarks>
//        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
//        /// <returns></returns>
//        public void DeleteOnCommit(TAggregateRootEntity aggregateRootEntity)
//        {
//            ObjectStateEntry objectStateEntry;
//            if (!this.ObjectContext.ObjectStateManager.TryGetObjectStateEntry(aggregateRootEntity, out objectStateEntry))
//            {
//                this.ObjectSet.Attach(aggregateRootEntity);
//                objectStateEntry = this.ObjectContext.ObjectStateManager.GetObjectStateEntry(aggregateRootEntity);

//            }

//            //Now that we have an objectStateEntry...are we detached?

//            if (((int)objectStateEntry.State).BitIsSet((int)EntityState.Detached))
//            {
//                this.ObjectSet.Attach(aggregateRootEntity);
//            }


//            //Now that we are attached....delete it:
//            this.ObjectSet.DeleteObject(aggregateRootEntity);

//            //return ForceCommit(forceCommitNow);
//        }



//        /// <summary>
//        /// Removes entities from the datastore
//        /// that match the given filter,
//        /// when the active
//        /// <see cref="IUnitOfWork"/>
//        /// is Committed.
//        /// </summary>
//        /// <remarks>
//        /// There is no need to return the number of records committed,
//        /// as it won't be happening just then -- only when 
//        /// the UnitOfWork Commits the underlying ORM Context.
//        /// </remarks>
//        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
//        /// <returns>The number of Elements changed in the current commit (may not be exact match to number of deleted).</returns>
//        public virtual void DeleteOnCommit(Expression<Func<TAggregateRootEntity, bool>> filter)
//        {
//            IQueryable<TAggregateRootEntity> aggregateRootEntities = 
//                ((IRepositoryService<TAggregateRootEntity>)this).GetByFilter(filter,null,null);

//            foreach (TAggregateRootEntity entity in aggregateRootEntities)
//            {
//                this.ObjectSet.DeleteObject(entity);
//            }

//            //return ForceCommit(forceCommitNow);
//        }



//        /// <summary>
//        /// Determines whether the entity has ever been saved before.
//        /// </summary>
//        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
//        /// <returns>
//        ///   <c>true</c> if [is entity new] [the specified aggregate root entity]; otherwise, <c>false</c>.
//        /// </returns>
//        public abstract bool IsEntityNew(TAggregateRootEntity aggregateRootEntity);


//        ///// <summary>
//        ///// Helper method to force or not at this time.
//        ///// </summary>
//        ///// <param name="forceCommitNow">if set to <c>true</c> [force commit now].</param>
//        ///// <returns></returns>
//        //private int ForceCommit(bool forceCommitNow)
//        //{

//        //    if ((forceCommitNow) || (!_alwaysCommit))
//        //    {
//        //        return this.ObjectContext.SaveChanges();
//        //    }
//        //    return 0;
//        //}


//        private string GetEntityName()
//        {
//            ObjectSet<TAggregateRootEntity> objectSet = this.ObjectSet /*as ObjectSet<TAggregateRootEntity>*/;

//            return "{0}.{1}".FormatStringInvariantCulture(objectSet.EntitySet.EntityContainer, objectSet.EntitySet.Name);
//        }


//        //private bool HasChangesPending()
//        //{
//        //    // get the entities that have been inserted or modified
//        //    return (this.ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified | EntityState.Detached).Count()>0);
//        //}


//    }
//}