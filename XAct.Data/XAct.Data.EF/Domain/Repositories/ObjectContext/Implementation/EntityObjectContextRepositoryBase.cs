﻿
//// ReSharper disable CheckNamespace
//namespace XAct.Domain.Repositories
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// Abstract base class for Repositories 
//    /// intialized using 
//    /// an EF ObjectContext
//    /// entity.
//    /// </summary>
//    /// <typeparam name="TAggregateRootEntity"></typeparam>
//    public abstract class EntityObjectContextRepositoryBase<TAggregateRootEntity> : EntityObjectContextRepositoryBaseBase<TAggregateRootEntity>
//        where TAggregateRootEntity : class
//    {
//        /// <summary>
//        /// Initializes a new instance of the
//        /// <see cref="EntityObjectContextRepositoryBase&lt;TAggregateRootEntity&gt;"/>
//        /// class.
//        /// <para>
//        /// This constructor sets the internal ObjectContext and ObjectSet, tightly coupling
//        /// this Repository instance to a single Context/UoW.
//        /// </para>
//        /// </summary>
//        /// <param name="objectContext">The object context.</param>
//        protected EntityObjectContextRepositoryBase(
//            IEntityObjectContext objectContext) : base(objectContext)
//        {

//        }

//        /// <summary>
//        /// Determines whether the entity has ever been saved before.
//        /// <para>
//        /// This method is required, in order to be invoked by <c>PersistOnCommit</c>
//        /// </para>
//        /// </summary>
//        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
//        /// <returns>
//        ///   <c>true</c> if [is entity new] [the specified aggregate root entity]; otherwise, <c>false</c>.
//        /// </returns>
//        public override abstract bool IsEntityNew(TAggregateRootEntity aggregateRootEntity);
        

//    }
//}