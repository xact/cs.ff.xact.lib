﻿//// ReSharper disable CheckNamespace
//namespace XAct.Domain.Repositories
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Data.Entity.Core.Objects;

//    /// <summary>
//    ///   Abstraction of the Entity Framework 4 ObjectContext object
//    ///   so that upper layers can instantiate Repositories without
//    /// having a direct reference to a vendor product (ie, EF).
//    /// <para>
//    /// Note that the <c>ObjectContext</c> is generally superceded by 
//    /// <see cref="EntityDbContext"/>
//    /// </para>
//    /// </summary>
//    /// <remarks>
//    ///   <para>
//    ///     Usage (when not using an DependencyInjectionContainer) would be:
//    ///     <code>
//    ///       <![CDATA[
//    /// //Create EF specific ObjectContext:
//    /// using (ObjectContext origObjectContext = new XAct_App()){
//    ///   //Wrap in order to work with portable wrapper:
//    ///   using (IObjectContext objectContext = new EntityObjectContext(origObjectContext)){
//    ///     //Use it to define a new UoW:
//    ///     using (IUnitOfWork uow = new UnitOfWork(abstractObjectContext)){
//    ///       using (IRepository repository = new ExampleRepository(objectContext)){
//    ///         ...
//    ///         repository.MakeChanges(...);
//    ///         ...
//    ///         uow.Commit();
//    ///       }
//    ///     }
//    ///   }
//    /// }
//    /// ]]>
//    ///     </code>
//    ///   </para>
//    /// </remarks>
//    /// <internal>
//    ///   <para>
//    ///     http://elegantcode.com/2009/12/15/entity-framework-ef4-generic-repository-and-unit-of-work-prototype/comment-page-1/
//    ///   </para>
//    /// </internal>
//    public class EntityObjectContext : IEntityObjectContext 
//    {

//        private readonly ObjectContext _objectContext;



//        #region Constructors

//        /// <summary>
//        ///   Initializes a new instance of the 
//        ///   <see cref = "EntityObjectContext" /> class.
//        /// </summary>
//        /// <param name = "objectContext">The object context.</param>
//        /// <internal>
//        /// Unfortunately, ObjectContext doesn't have an interface to use.
//        /// </internal>
//        public EntityObjectContext(ObjectContext objectContext)
//        {
//        if (objectContext.IsNull()){throw new ArgumentNullException("objectContext");}

//            _objectContext = objectContext;
//            //_objectContext.SavingChanges += ObjectContext_SavingChanges;

//        }

//        #endregion


//        ///// <summary>
//        ///// Creates an EF4 ObjectSet.
//        ///// <para>
//        ///// Defined in <see cref="IEntityObjectContext"/>
//        ///// </para>
//        ///// </summary>
//        ///// <typeparam name="T"></typeparam>
//        ///// <returns></returns>
//        //ObjectSet<T> IEntityObjectContext.CreateObjectSet<T>()
//        //{
//        //    return _objectContext.CreateObjectSet<T>();
//        //}



//        /// <summary>
//        ///   Performs application-defined tasks associated with 
//        ///   freeing, releasing, or resetting unmanaged resources.
//        /// <para>
//        /// Defined in <see cref="IDisposable"/>.
//        /// </para>
//        /// </summary>
//        public void Dispose()
//        {
//            _objectContext.Dispose();
//        }

//        /// <summary>
//        /// Commit pending changes to the
//        /// DataStore.
//        /// <para>
//        /// Defined in <see cref="IContext"/>.
//        /// </para>
//        /// </summary>
//        /// <param name="commitType"></param>
//        /// <returns>
//        /// The number of objects written to the underlying database.
//        /// </returns>
//        public int Commit(CommitType commitType)
//        {

//            //Note: SaveChanges() is the same as: 
//            //SaveChanges(SaveOptions.DetectChangesBeforeSave | SaveOptions.AcceptAllChangesAfterSave);
//            int result;
//            switch (commitType)
//            {
//                case CommitType.AcceptAllChanges:
//                    result =_objectContext.SaveChanges();
//                    break;
//                case CommitType.DetectAndHoldChangesTillComplete:
//                    result = _objectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave);
//                    break;
//                case CommitType.HoldChangesTillComplete:
//                    result = _objectContext.SaveChanges(SaveOptions.None);
//                    break;
//                case CommitType.CompleteChanges:
//                    _objectContext.AcceptAllChanges();
//                    result = 0;
//                    break;
//                    default:
//                throw new ArgumentOutOfRangeException("commitType");
//            }

//            //As recommended by MSDN: http://bit.ly/uQlipa
//            //If in a transactionScope:
//            //using (TransactionScope transactionScope = new TransactionScope())
//            //{
//            //    try
//            //    {
//                    //  // Save changes pessimistically. This means that changes 
//                    // must be accepted manually once the transaction succeeds.
//                    //context1.SaveChanges(SaveOptions.DetectChangesBeforeSave);
//                    //context2.SaveChanges(SaveOptions.DetectChangesBeforeSave);

//                    // Mark the transaction as complete.
//                    //transaction.Complete();

//                    //When everything complete:
//                    //_context1.AcceptAllChanges();
//                    //context2.AcceptAllChanges();
//                //}catch
//                //{
                    
//                //}
//            //}

//            return result;
//        }



//        /// <summary>
//        /// Gets or sets the inner object (the EF ObjectContext).
//        /// <para>
//        /// This property is used by the EF based Repository
//        /// to extract the vendor specific ObjectContext 
//        /// from this Vendor agnostic wrapper.
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Defined in <see cref="IHasInnerItem{T}"/>
//        /// </para>
//        /// </remarks>
//        /// <value>
//        /// The inner object.
//        /// </value>
//        T IHasInnerItemReadOnly.GetInnerItem<T>()
//        {
//                return _objectContext.ConvertTo<T>();
//        }
//    }
//}