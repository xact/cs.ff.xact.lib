﻿
//// ReSharper disable CheckNamespace
//namespace XAct.Domain.Repositories
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// Abstract base class for Repository Services 
//    /// intialized using 
//    /// an EF ObjectContext
//    /// entity.
//    /// </summary>
//    /// <typeparam name="TAggregateRootEntity"></typeparam>
//    public abstract class EntityObjectContextRepositoryServiceBase<TAggregateRootEntity> : EntityObjectContextRepositoryBaseBase<TAggregateRootEntity>
//        where TAggregateRootEntity : class
//    {
//        /// <summary>
//        /// Initializes a new instance of the
//        /// <see cref="EntityObjectContextRepositoryBase&lt;TAggregateRootEntity&gt;"/>
//        /// class.
//        /// </summary>
//        protected EntityObjectContextRepositoryServiceBase(IUnitOfWorkManagementService unitOfWorkManagementService) 
//            : base(unitOfWorkManagementService)
//        {

//        }

//        /// <summary>
//        /// Determines whether the entity has ever been saved before.
//        /// <para>
//        /// This method is required, in order to be invoked by <c>PersistOnCommit</c>
//        /// </para>
//        /// </summary>
//        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
//        /// <returns>
//        ///   <c>true</c> if [is entity new] [the specified aggregate root entity]; otherwise, <c>false</c>.
//        /// </returns>
//        public override abstract bool IsEntityNew(TAggregateRootEntity aggregateRootEntity);
        

//    }
//}