namespace XAct.Domain.Repositories
{
    using System;
    using System.Data.Entity;
    using XAct.Environment;

    /// <summary>
    /// 
    /// </summary>
    public class HasDateTimeCreatedOnUnitOfWorkPreCommitProcessingStrategy : UnitOfWorkPreCommitProcessingStrategyBase<IHasDateTimeCreatedOnUtc>
    {
        /// <summary>
        /// Initializes a new instance of the
        ///  <see cref="HasDateTimeCreatedOnUnitOfWorkPreCommitProcessingStrategy"/> class.
        /// </summary>
        public HasDateTimeCreatedOnUnitOfWorkPreCommitProcessingStrategy(IDateTimeService dateTimeService, IPrincipalService principalService) : 
            base(
            dateTimeService,
            principalService,
            EntityState.Added)
        {
        }

        /// <summary>
        /// Invoked by <see cref="UnitOfWorkPreCommitProcessingStrategyBase{IHasDateTimeModifiedOn}.Process(IUnitOfWork)"/>
        /// before iterating through all the known Entities.
        /// <para>
        /// This is a good place to set up common variables (eg: EnvironmentService.Now, etc)
        /// so that the same variable is shared across Entities in the same batch.
        /// </para>
        /// </summary>
        protected override void BeginProcessingInternal()
        {
        }


        /// <summary>
        /// Process the entity before Persisting changes.
        /// </summary>
        protected override void ProcessInternal(IHasDateTimeCreatedOnUtc entity)
        {

            if ((entity.CreatedOnUtc == null) || (entity.CreatedOnUtc == DateTime.MinValue))
            {
                entity.CreatedOnUtc = _nowUtc;

                //May also be IAuditable:
                IHasDateTimeCreatedBy auditable = entity as IHasDateTimeCreatedBy;

                if (auditable != null)
                {
                    if (auditable.CreatedBy.IsNullOrEmpty())
                    {
                        auditable.CreatedBy = _currentUser;
                    }
                }
            }

        }
    }
}