namespace XAct.Domain.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using XAct.Environment;

    /// <summary>
    /// Abstract base class for a strategy to be applied when persisting changes.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class UnitOfWorkPreCommitProcessingStrategyBase<T>: IUnitOfWorkPreCommitProcessingStrategy
        where T:class
    {

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected DbContext _dbContext;
        /// <summary>
        /// 
        /// </summary>
        protected DbChangeTracker _dbChangeTracker;

        /// <summary>
        /// The interface the <see cref="DbEntityEntry.Entity"/> must match before the Strategy is applied.
        /// </summary>
        /// <value></value>
        public Type InterfaceType { get { return _interfaceType; } }
// ReSharper disable InconsistentNaming
        /// <summary>
        /// The interface the <see cref="DbEntityEntry.Entity"/> must match before the Strategy is applied.
        /// </summary>
        protected /*readonly*/ Type _interfaceType;
// ReSharper restore InconsistentNaming
        
        /// <summary>
        /// The <see cref="EntityState"/> the <see cref="DbEntityEntry.State"/> must match before the Strategy is applied.
        /// </summary>
        /// <value></value>
        public EntityState[] States { get { return _states; } }
// ReSharper disable InconsistentNaming
        /// <summary>
        /// The _date time service
        /// </summary>
        protected readonly IDateTimeService _dateTimeService;
        /// <summary>
        /// The _principal service
        /// </summary>
        protected readonly IPrincipalService _principalService;

        /// <summary>
        /// The <see cref="EntityState"/> the <see cref="DbEntityEntry.State"/> must match before the Strategy is applied.
        /// </summary>
        protected /*readonly*/ EntityState[] _states;
// ReSharper restore InconsistentNaming

        /// <summary>
        /// The _now UTC
        /// </summary>
        protected DateTime _nowUtc;
        /// <summary>
        /// The _current user
        /// </summary>
        protected string _currentUser;


        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkPreCommitProcessingStrategyBase&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="principalService"></param>
        /// <param name="states">The states.</param>
        /// <param name="dateTimeService"></param>
        protected UnitOfWorkPreCommitProcessingStrategyBase(
            IDateTimeService dateTimeService,
            IPrincipalService principalService,
            params EntityState[] states)
        {
            Enabled = true;
            _interfaceType = typeof(T);
            _dateTimeService = dateTimeService;
            _principalService = principalService;
            _states = states;
        }


        /// <summary>
        /// Processes the specified db change tracker,
        /// iterating through all the Entities in it,
        /// applying <see cref="IUnitOfWorkPreCommitProcessingStrategy"/> strategies that match.
        /// </summary>
        public virtual void Process(IUnitOfWork unitOfWork)
        {

            _nowUtc = _dateTimeService.NowUTC;

            _currentUser = _principalService.Principal.Identity.Name;
 

            this.Enabled = true;

            EntityDbContext entityDbContext = (EntityDbContext) unitOfWork;
            _dbContext = ((IHasInnerItemReadOnly) entityDbContext).GetInnerItem<DbContext>();
            _dbChangeTracker = _dbContext.ChangeTracker;

            
            
            BeginProcessingInternal();

            //Iterate through entities
            //Could only find a way to interate through all entities
            //so loop through them first (there won't be many in most cases):
            foreach (DbEntityEntry dbEntityEntry in _dbChangeTracker.Entries())
            {
                //Get the Entity Type (History, Vertex, etc.)
                //In order to find out if any interfaces the entity impleemnts
                //match cached save strategies:
                Type entityType = dbEntityEntry.Entity.GetType();

                if (!this.InterfaceType.IsAssignableFrom(entityType))
                {
                    //Go to next entity:
                    continue;
                }
                //Have an entity that matches interface, so ok to process:
                Process(dbEntityEntry);
            }
        }


        /// <summary>
        /// Apply the strategy to the given <see cref="DbEntityEntry"/>.
        /// <para>
        /// 	</para>
        /// </summary>
        /// <param name="dbEntityEntry">The <see cref="DbEntityEntry"/>.</param>
        protected virtual void Process(DbEntityEntry dbEntityEntry)
        {
            //Does the entity's state match any of the states the subclass constructor set?
            bool found = false;

            foreach (EntityState dbEntityEntry2 in States)
            {
                if (((int)dbEntityEntry.State).BitIsSet((int)dbEntityEntry2))
                {
                    found=true;
                    break;
                }
            }

            //Doesn't account for Flag nature of EntityState:
            //if (! States.Contains(dbEntityEntry.State))
            if (found==false)
            {
                return;
            }

            //Set the MachineId:
            T entity = dbEntityEntry.Entity as T;
            if (entity == default(T))
            {
                return;
            }


            ProcessInternal(entity);

        }


        /// <summary>
        /// Gets the state of the entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        protected EntityState GetEntityState(T entity)
        {
          return _dbContext.Entry(entity).State;
            
        }

        /// <summary>
        /// Determines whether [is entity state set] [the specified entity].
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entityState">State of the entity.</param>
        /// <returns></returns>
        protected bool IsEntityStateSet(T entity, EntityState entityState)
        {
            return ((int)GetEntityState(entity)).BitIsSet((int)entityState);
        }


        /// <summary>
        /// Invoked by <see cref="Process(IUnitOfWork)"/>
        /// before iterating through all the known Entities.
        /// <para>
        /// This is a good place to set up common variables (eg: EnvironmentService.Now, etc)
        /// so that the same variable is shared across Entities in the same batch.
        /// </para>
        /// </summary>
        protected virtual void BeginProcessingInternal()
        {
            
        }

        /// <summary>
        /// Process the entity before Persisting changes.
        /// <para>
        /// An example could be something like:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// protected override void ProcessInternal(IHasDistributedIdentity entity) {
        ///   IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
        ///   entity.MachineId = environmentService.DeviceUniqueId;
        /// }
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// </summary>
        /// <param name="entity">The entity.</param>
        protected abstract void ProcessInternal(T entity);




    }
}