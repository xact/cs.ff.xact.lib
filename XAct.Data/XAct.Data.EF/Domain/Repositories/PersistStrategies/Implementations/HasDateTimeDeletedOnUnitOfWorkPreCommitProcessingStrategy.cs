//namespace XAct.Domain.Repositories
//{
//    using System;
//    using System.Data.Entity;
//    using System.Data.Entity.Infrastructure;
//    using XAct.Environment;

//    /// <summary>
//    /// 
//    /// </summary>
//    public class HasDateTimeDeletedOnUnitOfWorkPreCommitProcessingStrategy : UnitOfWorkPreCommitProcessingStrategyBase<IHasDateTimeDeletedOnUtc>
//    {

        
//        /// <summary>
//        /// Initializes a new instance of the
//        ///  <see cref="HasDateTimeCreatedOnUnitOfWorkPreCommitProcessingStrategy"/> class.
//        /// </summary>
//        public HasDateTimeDeletedOnUnitOfWorkPreCommitProcessingStrategy(IDateTimeService dateTimeService,IPrincipalService principalService) :
//            base(
//            dateTimeService,
//            principalService,
//            EntityState.Deleted)
//        {
//        }



//        /// <summary>
//        /// Invoked by <see cref="UnitOfWorkPreCommitProcessingStrategyBase{IHasDateTimeModifiedOn}.Process(IUnitOfWork)"/>
//        /// before iterating through all the known Entities.
//        /// <para>
//        /// This is a good place to set up common variables (eg: EnvironmentService.Now, etc)
//        /// so that the same variable is shared across Entities in the same batch.
//        /// </para>
//        /// </summary>
//        protected override void BeginProcessingInternal()
//        {
//        }



//        /// <summary>
//        /// Processes the internal.
//        /// </summary>
//        /// <param name="entity">The entity.</param>
//        protected override void ProcessInternal(IHasDateTimeDeletedOnUtc entity)
//        {

//            if (!(entity is IHasIsAuditModel))
//            {
//                if ((entity.DeletedOnUtc == null) || (entity.DeletedOnUtc == DateTime.MinValue))
//                {

//                    entity.DeletedOnUtc = _nowUtc;

//                    //May also be IAuditable:
//                    IHasDateTimeDeletedBy auditable = entity as IHasDateTimeDeletedBy;

//                    if (auditable != null)
//                    {
//                        if (auditable.DeletedBy.IsNullOrEmpty())
//                        {
//                            auditable.DeletedBy = _currentUser;
//                        }
//                    }
//                }
//            }
//        }
//    }
//}