namespace XAct.Domain.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Reflection;
    using XAct.Environment;

    /// <summary>
    /// 
    /// </summary>
    public class AuditableChangesStrategy : UnitOfWorkPreCommitProcessingStrategyBase<object>
    {
        private readonly IDistributedIdService _distributedIdService;

        /// <summary>
        /// Initializes a new instance of the
        ///  <see cref="HasDateTimeCreatedOnUnitOfWorkPreCommitProcessingStrategy"/> class.
        /// </summary>
        public AuditableChangesStrategy(IDateTimeService dateTimeService, IPrincipalService principalService, IDistributedIdService distributedIdService) :
            base(
            dateTimeService,
            principalService,
           //NOTE: If the Id is integer autoset by Id, the Id will not be set by the time the 
            //Audit record is copying it....so it will have an Id of 0.
            //Since I strongly suggest (in this world of Mobile and Azure) that you use Guids instead,
            //this should not be an issue.
            EntityState.Added,
            EntityState.Modified,
            EntityState.Deleted)
        {
            _distributedIdService = distributedIdService;
        }


        /// <summary>
        /// Invoked by <see cref="UnitOfWorkPreCommitProcessingStrategyBase{IHasDateTimeModifiedOn}.Process(IUnitOfWork)"/>
        /// before iterating through all the known Entities.
        /// <para>
        /// This is a good place to set up common variables (eg: EnvironmentService.Now, etc)
        /// so that the same variable is shared across Entities in the same batch.
        /// </para>
        /// </summary>
        protected override void BeginProcessingInternal()
        {
        }



        /// <summary>
        /// Processes the internal.
        /// </summary>
        /// <param name="entity">The entity.</param>
        protected override void ProcessInternal(object entity)
        {


            AuditableAttribute auditableAttribute =
                entity.GetType().GetAttribute<AuditableAttribute>();

            if (auditableAttribute == null)
            {
                return;
            }


            //if (IsEntityStateSet(entity,EntityState.Deleted))
            //{
            //Have to roll it back to only deal with values as it was in pre-final update (but aborted) changeds
            //var entry = _dbContext.Entry(entity);

            //Hack: due to System.InvalidOperationException : CurrentValues cannot be used for entities in the Deleted state.
            //entry.State = EntityState.Unchanged;
            //entry.CurrentValues.SetValues(entry.OriginalValues);
            //entry.State = EntityState.Deleted;

            //Hack #2: Not sure why, but when State is set to deleted, OriginalValues is wiped out, and set to CurrentValues.
            //So we now take care of this in EntityDbContextGenericRepositoryService.

            //}

            Type type = auditableAttribute.Type;
            object auditRecord = XAct.DependencyResolver.Current.GetInstance(type);



            MapScalarValues(entity, auditRecord);


            
            //The Audit record may have auditing column.
            //The Audit record may have additional attributes, to describe audit action:
            SetOperationType(entity, auditRecord);
            SetDeletedDate(entity, auditRecord);


            _dbContext.Entry(auditRecord).State = EntityState.Added;
        }

        private void MapScalarValues(object entity, object auditRecord)
        {
            //NO: because it expects source and target to have exactly same propertyInfo:
            //entity.MapPropertyValues(auditRecord, true);

            Type targetType = auditRecord.GetType();

            foreach (PropertyInfo targetPropertyInfo in targetType.GetProperties())
            {
                if ((!targetPropertyInfo.PropertyType.IsValueType) &&
                    (targetPropertyInfo.PropertyType != typeof (string)))
                {
                    continue;
                }


                var sourceValue = GetSourceValue(entity, targetPropertyInfo);

                auditRecord.SetMemberValue(
                    targetPropertyInfo.Name, 
                    sourceValue.ConvertTo(targetPropertyInfo.PropertyType)
                    );
            }
        }

        private object GetSourceValue(object entity, 
                                      PropertyInfo targetPropertyInfo)
        {
            AuditablePropertyAttribute auditablePropertyAttribute =
                targetPropertyInfo.GetAttribute<AuditablePropertyAttribute>();


            object sourceValue;
            if (auditablePropertyAttribute != null)
            {
                string sourcePropertyDotPath = auditablePropertyAttribute.SourcePropertyDotPath;

                sourceValue = Fish(entity, sourcePropertyDotPath);

                if (sourceValue == null)
                {
                    return targetPropertyInfo.PropertyType.GetDefault();
                }

                //Have a value:
                //Are there any options?
                //If so, expect source to be an int, or an Enum:
                string[] options = auditablePropertyAttribute.Options;
                if (options != null && options.Length > 0)
                {
                    if (sourceValue.GetType().IsEnum)
                    {
                        //Ensure it's an int (otherwise it will try to go from Enum to String ('Female') then to int...
                        sourceValue = (int)sourceValue;
                    }

                    int position;
                    if (int.TryParse(sourceValue.ToString(), out position))
                    {
                        if (position > -1 && position < options.Length)
                        {
                            sourceValue = options[position];
                        }
                    }
                }
                else
                {
                    //Not an Options (enum/int).
                    //Maybe it's an Enum
                    if (sourceValue.GetType().IsEnum)
                    {
                        sourceValue = sourceValue.ToString();
                    }
                }
                return sourceValue;
            }

            //No Attribute:
            //Is there a source 
            Type sourceType = entity.GetType();

            PropertyInfo sourcePropertyInfo = sourceType
                .GetProperty(targetPropertyInfo.Name,
                             BindingFlags.Instance | BindingFlags.Public |
                             BindingFlags.IgnoreCase);

            if (sourcePropertyInfo == null)
            {
                return targetPropertyInfo.PropertyType.GetDefault();
            }


            sourceValue = entity.GetMemberValue(sourcePropertyInfo.Name);

            return sourceValue.ConvertTo(targetPropertyInfo.PropertyType);

        }

        object Fish(object target, string sourcePropertyDotPath)
        {
            if (sourcePropertyDotPath.IsNullOrEmpty())
            {
                return null;
            }
            foreach (string p2 in sourcePropertyDotPath.Trim().Split('.'))
            {
                if (target == null)
                {
                    return "[" + p2 + " = null]";
                }
                target = target.GetType().GetProperty(p2, BindingFlags.Instance | BindingFlags.Public).GetValue(target);
            }
            return target;
        }


        private void SetOperationType(object entity, object auditRecord)
        {
            EntityState entityState = GetEntityState(entity);

            IHasIsAuditModel e = auditRecord as IHasIsAuditModel;

            if (e != null)
            {
                e.AuditId = _distributedIdService.NewGuid();

                switch (entityState)
                {
                    case EntityState.Added:
                        e.Action = "C";
                        break;
                    case EntityState.Modified:
                        e.Action = "U";
                        break;
                    case EntityState.Deleted:
                        e.Action = "D";
                        break;
                    default:
                        break;
                }

                e.AuditActionOn = _nowUtc;
                e.AuditActionBy = _currentUser;
            }
        }

        private void SetDeletedDate(object entity, object auditRecord)
        {
//May also be IAuditable:



            var x = auditRecord as IHasDateTimeDeletedBy;

            if (x !=null)
            {
                if ((x.DeletedOnUtc == null) || (x.DeletedOnUtc == DateTime.MinValue))
                {

                    x.DeletedOnUtc = _nowUtc;

                    //May also be IAuditable:
                    IHasDateTimeDeletedBy auditable = auditRecord as IHasDateTimeDeletedBy;

                    if (auditable != null)
                    {
                        if (auditable.DeletedBy.IsNullOrEmpty())
                        {
                            auditable.DeletedBy = _currentUser;
                        }
                    }
                }
            }
        }
    }
}