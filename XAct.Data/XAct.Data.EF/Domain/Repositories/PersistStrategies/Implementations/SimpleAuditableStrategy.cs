﻿namespace XAct.Domain.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Reflection;
    using XAct.Environment;

    /// <summary>
    /// 
    /// </summary>
    public class SimpleAuditableStrategy : UnitOfWorkPreCommitProcessingStrategyBase<object>
    {
        private readonly IDistributedIdService _distributedIdService;

        /// <summary>
        /// Initializes a new instance of the
        ///  <see cref="HasDateTimeCreatedOnUnitOfWorkPreCommitProcessingStrategy"/> class.
        /// </summary>
        public SimpleAuditableStrategy(IDateTimeService dateTimeService, IPrincipalService principalService, IDistributedIdService distributedIdService) :
            base(
            dateTimeService,
            principalService,
                //NOTE: If the Id is integer autoset by Id, the Id will not be set by the time the 
                //Audit record is copying it....so it will have an Id of 0.
                //Since I strongly suggest (in this world of Mobile and Azure) that you use Guids instead,
                //this should not be an issue.
            EntityState.Added,
            EntityState.Modified,
            EntityState.Deleted)
        {
            _distributedIdService = distributedIdService;
        }


        /// <summary>
        /// Invoked by <see cref="UnitOfWorkPreCommitProcessingStrategyBase{IHasDateTimeModifiedOn}.Process(IUnitOfWork)"/>
        /// before iterating through all the known Entities.
        /// <para>
        /// This is a good place to set up common variables (eg: EnvironmentService.Now, etc)
        /// so that the same variable is shared across Entities in the same batch.
        /// </para>
        /// </summary>
        protected override void BeginProcessingInternal()
        {
        }



        /// <summary>
        /// Processes the internal.
        /// </summary>
        /// <param name="entity">The entity.</param>
        protected override void ProcessInternal(object entity)
        {



            AuditableAttribute auditableAttribute =
                entity.GetType().GetAttribute<AuditableAttribute>();

            if (auditableAttribute == null)
            {
                return;
            }

            Type type = auditableAttribute.Type;
            object auditRecord = XAct.DependencyResolver.Current.GetInstance(type);

            SimpleAuditableAttribute a2 = auditableAttribute as SimpleAuditableAttribute;
            string sourceIdentifier = (a2 != null) ? a2.SourceIdentifier : entity.GetType().Name;

            SetOperationType(entity, auditRecord, sourceIdentifier);
            
            _dbContext.Entry(auditRecord).State = EntityState.Added;
        }

        private void SetOperationType(object entity, object auditRecord, string sourceIdentifier)
        {
            EntityState entityState = GetEntityState(entity);

            IHasIsAuditModel e = auditRecord as IHasIsAuditModel;

            if (e == null)
            {
                return;
            }

            e.AuditId = _distributedIdService.NewGuid();

            switch (entityState)
            {
                case EntityState.Added:
                    e.Action = "C";
                    break;
                case EntityState.Modified:
                    e.Action = "U";
                    break;
                case EntityState.Deleted:
                    e.Action = "D";
                    break;
                default:
                    break;
            }

            e.AuditActionOn = _nowUtc;
            e.AuditActionBy = _currentUser;


            IHasIsSimpleAuditModel e2 = auditRecord as IHasIsSimpleAuditModel;

            if (e2 == null)
            {
                return;
            }
            e2.Source = sourceIdentifier;

        }

    }
}