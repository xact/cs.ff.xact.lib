namespace XAct.Domain.Repositories.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class UnitOfWorkCommitPreProcessorServiceState : List<IUnitOfWorkPreCommitProcessingStrategy>, IUnitOfWorkCommitPreProcessorServiceState 
    {

    }
}