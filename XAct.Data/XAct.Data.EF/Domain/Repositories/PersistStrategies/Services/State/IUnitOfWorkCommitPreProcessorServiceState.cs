namespace XAct.Domain.Repositories
{
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public interface IUnitOfWorkCommitPreProcessorServiceState : IList<IUnitOfWorkPreCommitProcessingStrategy>, IHasXActLibServiceState
    {
        
    }
}