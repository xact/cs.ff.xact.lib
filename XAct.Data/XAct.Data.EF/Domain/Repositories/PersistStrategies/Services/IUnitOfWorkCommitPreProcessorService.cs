namespace XAct.Domain.Repositories
{

    /// <summary>
    /// 
    /// </summary>
    public interface IUnitOfWorkCommitPreProcessorService : IHasXActLibService
    {


        /// <summary>
        /// Gets the shared list of registered IUnitOfWorkCommitPreProcessorServiceState.
        /// <para>
        /// Add to this list during the application's 
        /// initialization sequence.
        /// </para>
        /// <para>
        /// See <see cref="HasDateTimeCreatedOnUnitOfWorkPreCommitProcessingStrategy"/>,
        /// <see cref="HasDateTimeModifiedOnUnitOfWorkPreCommitProcessingStrategy"/>,
        /// <see cref="AuditableChangesStrategy"/>.
        /// </para>
        /// </summary>
        IUnitOfWorkCommitPreProcessorServiceState PreProcessors { get; }

        /// <summary>
        /// Processes all the Entities in the current DbChangeTracker
        /// applying appropriate <see cref="IUnitOfWorkPreCommitProcessingStrategy" />.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        void Process(IUnitOfWork unitOfWork);
    }
}