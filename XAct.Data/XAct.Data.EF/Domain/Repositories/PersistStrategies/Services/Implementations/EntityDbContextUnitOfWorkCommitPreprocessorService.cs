﻿namespace XAct.Domain.Repositories
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IUnitOfWorkCommitPreProcessorService), BindingLifetimeType.Undefined, Priority.Low /*OK: An Override Priority*/)]
    public class EntityDbContextUnitOfWorkCommitPreprocessorService : XActLibServiceBase, IEntityDbContextUnitOfWorkCommitPreprocessorService
    {

        /// <summary>
        /// Gets the shared list of registered IUnitOfWorkCommitPreProcessorServiceState.
        /// </summary>
        public IUnitOfWorkCommitPreProcessorServiceState PreProcessors
        {
            get { return _persistChangesStrategyCache; }
        }

        private readonly IUnitOfWorkCommitPreProcessorServiceState _persistChangesStrategyCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityDbContextUnitOfWorkCommitPreprocessorService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="persistChangesStrategyCache">The persist changes strategy cache.</param>
        public EntityDbContextUnitOfWorkCommitPreprocessorService(ITracingService tracingService,
                                                                   IUnitOfWorkCommitPreProcessorServiceState
                                                                       persistChangesStrategyCache):base(tracingService)
        {
            _persistChangesStrategyCache = persistChangesStrategyCache;
        }



        /// <summary>
        /// Processes all the Entities in the current DbChangeTracker
        /// applying appropriate <see cref="IUnitOfWorkPreCommitProcessingStrategy" />.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        public void Process(IUnitOfWork unitOfWork)
        {

            IUnitOfWorkCommitPreProcessorServiceState cache =
                DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorServiceState>();

            foreach (IUnitOfWorkPreCommitProcessingStrategy strategyThatMatchStateOfEntity in cache)
            {
                if (!strategyThatMatchStateOfEntity.Enabled)
                {
                    continue;
                }
                strategyThatMatchStateOfEntity.Process(unitOfWork);
            }
        }
    }
}