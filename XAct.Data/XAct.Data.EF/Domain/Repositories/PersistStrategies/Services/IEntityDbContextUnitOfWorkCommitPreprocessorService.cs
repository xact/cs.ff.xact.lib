namespace XAct.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEntityDbContextUnitOfWorkCommitPreprocessorService : IUnitOfWorkCommitPreProcessorService, IHasXActLibService
    {
    
    }
}