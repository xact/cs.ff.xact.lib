namespace XAct.Domain.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    /// <summary>
    /// 
    /// </summary>
    public interface IUnitOfWorkPreCommitProcessingStrategy : IHasEnabled
    {

        
        /// <summary>
        /// The interface the <see cref="DbEntityEntry.Entity"/> must match before the Strategy is applied.
        /// </summary>
        Type InterfaceType { get;  }

        /// <summary>
        /// The <see cref="EntityState"/> the <see cref="DbEntityEntry.State"/> must match before the Strategy is applied.
        /// </summary>
        EntityState[] States { get; }


        /// <summary>
        /// Processes the specified db change tracker,
        /// iterating through all the Entities in it, 
        /// applying <see cref="IUnitOfWorkPreCommitProcessingStrategy"/> strategies that match.
        /// </summary>
        void Process(IUnitOfWork unitOfWork);
        
        ///// <summary>
        ///// Apply the strategy to the given <see cref="DbEntityEntry"/>.
        ///// <para>
        ///// </para>
        ///// </summary>
        ///// <param name="dbEntityEntry">The <see cref="DbEntityEntry"/>.</param>
        //void Process(DbEntityEntry dbEntityEntry);
    }
}