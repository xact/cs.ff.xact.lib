﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Resources;


    /// <summary>
    /// An implementation of <see cref="IStatusServiceConnector"/>
    /// that returns the values of DbAccessibility on the Server.
    /// </summary>
    public class DbAccessibilityStatusServiceConnector : XActLibStatusServiceConnectorBase
    {
        /// <summary>
        /// The configuration package of this Connector.
        /// </summary>
        public DbAccessibilityStatusServiceConnectorConfiguration Configuration { get; private set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="DbAccessibilityStatusServiceConnector" /> class.
        /// </summary>
        /// <param name="connectorConfiguration">The connector configuration.</param>
        public DbAccessibilityStatusServiceConnector(DbAccessibilityStatusServiceConnectorConfiguration connectorConfiguration)
            :base()
        {
            Name = "DbAccessibility";
            Configuration = connectorConfiguration;
        }

        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null)
        {

            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }


        private void MakeTable(StatusResponseTable table, object arguments)
        {

            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:
            List<string> connectionStringNames = 
                arguments == null
                    ? Configuration.ConnectionStringName
                    : ProcessArguments<string>(arguments, (o) => o as string);


            //Create Headers whether there are rows or not:
            table.Headers.Add("ConnectionString Name");
            table.Headers.Add("Result");

            //Make rows:
            if (connectionStringNames == null)
            {
                return;
            }

            foreach (string connectionStringName in connectionStringNames)
            {
                table.Rows.Add(MakeTableRow(connectionStringName));

            }
        }

        private StatusResponseTableRow MakeTableRow(string connectionStringName)
        {
            StatusResponseTableRow row = new StatusResponseTableRow();


            //Cell #1:
            row.Cells.Add(connectionStringName);


            string result;
            try
            {
#pragma warning disable 168
                var connection = XAct.Data.DbHelpers.CreateConnection(connectionStringName);
#pragma warning restore 168
                result = "OK";
                row.Status = ResultStatus.Success;
            }
#pragma warning disable 168
            catch (System.Exception e)
#pragma warning restore 168
            {
                result = "Err";
                row.Status = ResultStatus.Fail;
            }



            //Cell #2:
            row.Cells.Add(result);


            return row;
        }
    }
}
