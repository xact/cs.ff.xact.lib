﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System.Collections.Generic;

    /// <summary>
    /// Configuration object for an instane of
    /// <see cref="DbAccessibilityStatusServiceConnector"/>
    /// </summary>
    public class DbAccessibilityStatusServiceConnectorConfiguration : IHasTransientBindingScope
    {
        /// <summary>
        /// Gets the name of the connection string.
        /// </summary>
        /// <value>
        /// The name of the connection string.
        /// </value>
        public List<string> ConnectionStringName
        {
            get { return _connectionStringsName ?? (_connectionStringsName = new List<string>()); }
        }

        private List<string> _connectionStringsName;
    }
}