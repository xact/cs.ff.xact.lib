﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using XAct.Data;

#endif

/// <summary>
    ///   Extension Methods for the ConnectionStringSettings
    /// </summary>
// ReSharper disable CheckNamespace
    public static class ConnectionStringSettingsExtensionMethods
// ReSharper restore CheckNamespace
    {
        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;

        /// <summary>
        ///   Gets the DbProviderFactory specified 
        ///   in the <see cref = "ConnectionStringSettings" />.
        /// </summary>
        /// <param name = "connectionSettings"></param>
        /// <returns></returns>
        public static DbProviderFactory GetFactory(this
                                                       ConnectionStringSettings connectionSettings)
        {
            //return DbProviderFactories.GetFactory(
            //   connectionSettings.ProviderName);

            DbProviderFactory factory;
            try
            {
                factory = DbProviderFactories.GetFactory(
                    connectionSettings.ProviderName);
            }
            catch (ArgumentException e)
            {
                ////First, write the critical/error error:
                //if (XSourceSwitch.ShouldTrace(
                //    TraceEventType.Error))
                //{
                //    Trace.WriteLine(E.Message);
                //}
                ////Secondly, write the verbose explanation of the error:
                //if (XSourceSwitch.ShouldTrace(
                //    TraceEventType.Verbose))
                //{
                //    Trace.WriteLine(E.StackTrace);
                //}


                throw new ArgumentException(
                    string.Format(
                        _invariantCulture,
                        "::DbProviderFactories.GetFactory('{1}') failed.{0}" +
                        "Make sure it is installed in the" +
                        " config file as follows:{0}" +
                        "<system.data>{0}" +
                        "  <DbProviderFactories>{0}" +
                        "    <add invariant=\"{1}\" name=\"...\"" +
                        " description=\"...\" type=\"...\"/>{0}" +
                        "  <DbProviderFactories>{0}" +
                        "<system.data>{0}" +
                        "",
                        System.Environment.NewLine,
                        connectionSettings.ProviderName),
                    e);
            }
            return factory;
        }


        /// <summary>
        ///   Creates a new open DB connection using the specified 
        ///   <see cref = "ConnectionStringSettings" />.
        ///   <para>
        ///     Opens the connection before returning it.
        ///   </para>
        ///   <para>
        ///     Note: the reason it is called <c>CreateConnectionII</c>
        ///     and is not an overload of <c>CreateConnection</c>
        ///     is explained in the <c>Remarks</c>.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     The reason this method is called <c>CreateConnectionII</c>,
        ///     rather than being an overload of <c>CreateConnection</c> is
        ///     because it would cause any assembly that does access 
        ///     <c>CreateConnection</c>, even if using only <c>string</c> arguments,
        ///     and not a <see cref = "ConnectionStringSettings" /> argument,
        ///     to have to reference the <c>System.Configuration.dll</c>.
        ///   </para>
        ///   <para>
        ///     The error would be:
        ///     <code>
        ///       The type 'System.Configuration.ConnectionStringSettings' is 
        ///       defined in an assembly that is not referenced. 
        ///       You must add a reference to assembly 
        ///       'System.Configuration, Version=2.0.0.0, Culture=neutral, 
        ///       PublicKeyToken=b03f5f7f11d50a3a'.	
        ///     </code>
        ///   </para>
        ///   <para>
        ///     So between two evils -- havig unclear nomenclature, and
        ///     causing unnecessary references, I prefer the first option.
        ///   </para>
        /// </remarks>
        /// <param name = "connectionStringSetting">The conn settings.</param>
        /// <returns>A new open conection.</returns>
        public static IDbConnection CreateConnection(
            this ConnectionStringSettings connectionStringSetting)
        {
            connectionStringSetting.ValidateIsNotDefault("connectionStringSetting");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            return DbHelpers.CreateConnection(
                connectionStringSetting.ProviderName,
                connectionStringSetting.ConnectionString);
        }

        /// <summary>
        ///   Helper Method to expand the path part of a 
        ///   <see cref = "ConnectionStringSettings.ConnectionString" /> property
        ///   replacing the '|DATADIRECTORY|' macro with 
        ///   the application's designated Data directory.
        ///   <para>
        ///     If not rooted, prepends the path with the <c>AppDir</c>.
        ///   </para>
        /// </summary>
        /// <param name = "connectionSettings">
        ///   The connectionSettings object.
        /// </param>
        /// <returns>
        ///   The modified ConnectionStringSetting.
        /// </returns>
        public static ConnectionStringSettings ConfigureConnectionSettings(
            this ConnectionStringSettings connectionSettings)
        {
            connectionSettings.ValidateIsNotDefault("connectionSettings");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            string connStr = connectionSettings.ConnectionString;
            connStr = DbHelpers.ExpandDataDirectoryMacro(connStr, false);

            connectionSettings =
                new ConnectionStringSettings(
                    connectionSettings.Name,
                    connStr,
                    connectionSettings.ProviderName
                    );

            return connectionSettings;
        }
    }

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
