﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Data;

#endif

//Note: ExtensionMethods are common to Data -- hence no unique NS.

    /// <summary>
    ///   Extension Methods to the <see cref = "IDataRecord" /> object.
    /// </summary>
// ReSharper disable CheckNamespace
    public static class IDataRecordExtensions
// ReSharper restore CheckNamespace
    {
        #region Static Helper Methods - Db - Extract Record Values

        /// <summary>
        ///   Extracts the typed record value.
        /// </summary>
        /// <param name = "record">The record.</param>
        /// <param name = "columnName">Name of the col.</param>
        /// <returns></returns>
        public static T ExtractRecordValue<T>(
            this IDataRecord record,
            string columnName)
        {
            Type t = typeof (T);
            T defaultValue = (T)
                             (t.IsValueType
                                  ? Activator.CreateInstance(t)
                                  : null);
            int colIndex = record.GetOrdinal(columnName);
            return ExtractRecordValue(record, colIndex, defaultValue);
        }

        /// <summary>
        ///   Extracts the typed record value.
        /// </summary>
        /// <param name = "record">The record.</param>
        /// <param name = "columnName">Name of the col.</param>
        /// <param name = "defaultValue">The default value.</param>
        /// <returns></returns>
        public static T ExtractRecordValue<T>(
            this IDataRecord record,
            string columnName,
            T defaultValue)
        {
            int colIndex = record.GetOrdinal(columnName);

            return ExtractRecordValue(record, colIndex, defaultValue);
        }


        /// <summary>
        ///   Extracts the typed record value.
        /// </summary>
        /// <param name = "record">The record.</param>
        /// <param name = "colIndex">Index of the col.</param>
        /// <returns></returns>
        public static T ExtractRecordValue<T>(
            this IDataRecord record,
            int colIndex)
        {
            Type t = typeof (T);

            T defaultValue =
                (T)
                (t.IsValueType
                     ? Activator.CreateInstance(t)
                     : null);

            return ExtractRecordValue(record, colIndex, defaultValue);
        }

        /// <summary>
        ///   Extracts the typed record value.
        /// </summary>
        /// <param name = "record">The record.</param>
        /// <param name = "colIndex">Index of the col.</param>
        /// <param name = "defaultValue">The default value.</param>
        /// <returns></returns>
        public static T ExtractRecordValue<T>(
            this IDataRecord record, int colIndex, T defaultValue)
        {
            record.ValidateIsNotDefault("record");
            if (defaultValue.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("defaultValue");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            //Bool
            //Guid
            //Int
            //String

            object rawValue = record.GetValue(colIndex);

            return (T) Convert.ChangeType(rawValue, typeof (T), null);
        }

        #endregion
    }

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
