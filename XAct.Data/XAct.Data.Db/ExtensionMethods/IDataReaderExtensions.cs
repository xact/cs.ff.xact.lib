﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;

#endif

/// <summary>
    ///   Extension Methods to the IDataReader Interface.
    /// </summary>
// ReSharper disable CheckNamespace
    public static class IDataReaderExtensions
// ReSharper restore CheckNamespace
    {
        /// <summary>
        ///   Converts a SqlDataReader to a DataSet
        ///   <returns>DataSet filled with the contents of the reader.</returns>
        /// </summary>
        /// <param name = 'dataReader'>SqlDataReader to convert.</param>
        /// <param name = 'qOneRowOnly'>Just do one row?</param>
        /// <remarks>
        ///   <para>
        ///     Currently not used anywhere in this assembly.
        ///   </para>
        ///   <para>
        ///     TODO: This method is not yet using NextResult() correctly.
        ///   </para>
        /// </remarks>
        [SuppressMessage("Microsoft.Security", "CA2000", Justification = "dataSet is the return value.")]
        public static DataSet ToDataSet(IDataReader dataReader, bool qOneRowOnly)
        {
            DataSet dataSet = new DataSet();
            do
            {
                dataSet.Tables.Add(dataReader.ToDataTable(qOneRowOnly));
            } while (dataReader.NextResult());

            return dataSet;
        }

//Method:End


        /// <summary>
        ///   Converts a DataReader to a DataTable.
        /// </summary>
        /// <param name = "dataReader">DataReader to scan.</param>
        /// <param name = "qOneRowOnly">One row only?</param>
        /// <returns>DataTable</returns>
        /// <remarks>
        ///   <para>
        ///     TODO: This method is not yet using NextResult() correctly.
        ///   </para>
        /// </remarks>
        //[SuppressMessage("Microsoft.Security", "CA2000", Justification = "dataTable is the return value.")]
        public static DataTable ToDataTable(this IDataReader dataReader, bool qOneRowOnly)
        {
            DataTable dataTable = new DataTable();

            using (DataTable schemaTable = dataReader.GetSchemaTable())
            {
                if (schemaTable != null)
                {
                    // A query returning records was executed

                    for (int i = 0; i < schemaTable.Rows.Count; i++)
                    {
                        DataRow dataRow = schemaTable.Rows[i];
                        // Create a column name that is unique in the data table
                        string columnName = (string) dataRow["ColumnName"]; //+ "<C" + i + "/>";
                        // Add the column definition to the data table
                        DataColumn column = new DataColumn(columnName, (Type) dataRow["DataType"]);
                        dataTable.Columns.Add(column);
                    }
                    // Fill the data table we just created
                    while (dataReader.Read())
                    {
                        DataRow dataRow = dataTable.NewRow();

                        for (int i = 0; i < dataReader.FieldCount; i++)
                        {
                            dataRow[i] = dataReader.GetValue(i);
                        }

                        dataTable.Rows.Add(dataRow);
                        if (qOneRowOnly)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    // No records were returned

                    DataColumn column = new DataColumn("RowsAffected");
                    dataTable.Columns.Add(column);
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = dataReader.RecordsAffected;
                    dataTable.Rows.Add(dataRow);
                }

                dataReader.Close(); //DB
            }
            return dataTable;
        }


        /// <summary>
        ///   Creates an Xml compliant string representation of the current row of a DataReader.
        /// </summary>
        /// <param name = "dataReader">DataReader to Read</param>
        /// <param name = "xResult">returned string result</param>
        /// <param name = "useFullFieldLabels">Make XML tags as FieldName -- or the full TableName.FieldName.</param>
        /// <param name = "recordTagName">Tag to use for record tags (eg: 'record')</param>
        /// <param name = "closeXmlRecordWhenFinished">Leave open for more appending -- close if record has no children to add later.</param>
        /// <returns>
        ///   bool to indicate success.
        /// </returns>
        /// <remarks>
        ///   <para>
        ///     Currently not used anywhere in this assembly.
        ///   </para>
        /// </remarks>
        public static bool ToXmlFragment(this IDataRecord dataReader, ref string xResult, bool useFullFieldLabels,
                                         string recordTagName, bool closeXmlRecordWhenFinished)
        {
            bool tResult = false;

            xResult = string.Empty;

            if (dataReader != null)
            {
                xResult = "<" + recordTagName + ">";
                int tFieldCount = dataReader.FieldCount;

                for (int i = 0; i < tFieldCount; i++)
                {
                    string tFieldName = dataReader.GetName(i);
                    string tValue = dataReader.GetValue(i).ToString();
                    if (tValue.IsNullOrEmpty())
                    {
                        xResult += "<" + tFieldName + "/>";
                    }
                    else
                    {
                        xResult += "<" + tFieldName + ">" + tValue + "</" + tFieldName + ">";
                    }
                }
                if (closeXmlRecordWhenFinished)
                {
                    xResult += "</" + recordTagName + ">";
                }
                tResult = true;
            }
            return tResult;
        }


        /*
/// <summary>
/// Converts one DataReader record to an XmlNode.
/// </summary>
/// <param name="qDataReader">The DataReader to read.</param>
/// <param name="oRecordNode">The XmlNode to fill.</param>
/// <param name="qUseFullFieldName">Make XML tags as FieldName -- or the full TableName.FieldName.</param>
/// <returns>bool to indicate success.</returns>
/// <remarks>
/// <para>
/// Called repetitively by <see cref="DataReader_To_XmlRecordSet"/>.
/// </para>
/// </remarks>
/// <inhouse>
/// Called repetitively by DataReader_To_XmlRecordSet.
/// </inhouse>
public static bool DataReaderRecord_To_XmlNode(System.Data.IDataReader qDataReader, ref XmlNode oRecordNode, bool qUseFullFieldName) {
    if (qDataReader == null) {
        throw(new System.Exception("DataReader is Empty."));
    }

    string tFieldName;
    //Loop through all the columns in the the DataReader:
                
    foreach (System.Data.DataColumn tCol in (System.Data.DataColumnCollection)qDataReader) {
        tFieldName = (!qUseFullFieldName)?tCol.ColumnName:(tCol.Table.TableName + "." + tCol.ColumnName);
        XmlNode oField = oRecordNode.OwnerDocument.CreateElement(tFieldName);
        oField.InnerText = qDataReader[tCol.ColumnName].ToString();
        oRecordNode.AppendChild(oField);
    }
    return true;
}
*/


        /*
            /// <summary>
            /// Converts a whole DataReader to an XmlNode
            /// </summary>
            /// <param name="xDR">DataReader to read</param>
            /// <param name="xRecordSetNode">Node that will contain the other nodes.</param>
            /// <param name="qUseFullFieldLabels">Make an Xml tags as FieldName -- or the full TableName.FieldName tag.</param>
            /// <returns>
            /// A boolean to indicate success.
            /// </returns>
            /// <remarks>
            /// TODO: Study this and improve.
            /// </remarks>
            /// <inhouse>
            /// TODO: Check this: Written for, but currently not used by, XGraph class.
            /// </inhouse>
            public static bool DataReader_To_XmlRecordSet(System.Data.IDataReader xDR, ref XmlNode xRecordSetNode, bool qUseFullFieldLabels) {
                if ((xRecordSetNode == null) || (xDR == null)) {throw (new System.ArgumentNullException());}
                //If Records Found, loop through each one
                while (xDR.Read()) {
                    XmlNode oRecordNode = xRecordSetNode.OwnerDocument.CreateElement("record");
                    xRecordSetNode.AppendChild(oRecordNode);
                    DataReaderRecord_To_XmlNode(xDR,ref oRecordNode,true);
                }//End:While/Do
                return true;
            }
            */
    }

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
