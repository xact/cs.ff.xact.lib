﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
namespace XAct {
    using System.Configuration;
    using System.Data;
    using System.Data.Common;

#endif

/// <summary>
    ///   Extension Methods for the DbProviderFactory Type.
    /// </summary>
// ReSharper disable CheckNamespace
    public static class DbProviderFactoryExtensions
// ReSharper restore CheckNamespace
    {
        /// <summary>
        ///   Creates a new open DB connection using the specified
        ///   <see cref = "DbProviderFactory" /> and 
        ///   <see cref = "ConnectionStringSettings" />.
        /// </summary>
        /// <param name = "dbFactory">The db factory.</param>
        /// <param name = "connectionSettings">The connection settings.</param>
        /// <returns>A new open connection.</returns>
        public static IDbConnection CreateConnection(
            this DbProviderFactory dbFactory,
            ConnectionStringSettings connectionSettings)
        {
            // Construct and open a connection.
            IDbConnection connection = dbFactory.CreateConnection();

            connection.ConnectionString = connectionSettings.ConnectionString;

            connection.Open();

            return connection;
        }


        /// <summary>
        ///   Initializes a new connection to the DBMS.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     To be called by Connection {get;} only -- not by methods.
        ///   </para>
        ///   <para>
        ///     TODO: Really not convinced this is an 
        ///     //optimized way of handling concurrent users/threads.
        ///     TODO: TO DISCUSS.
        ///   </para>
        /// </remarks>
        public static IDbConnection InitConnection(
            this DbProviderFactory dbFactory,
            ConnectionStringSettings connectionSettings,
            IDbConnection connection)
        {
            // Create a new connection if it has never been created before:
            if (connection == null)
            {
                // Construct and open a connection.
                connection = dbFactory.CreateConnection();
                connection.ConnectionString =
                    connectionSettings.ConnectionString;
            }
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
            return connection;
        }
    }

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
