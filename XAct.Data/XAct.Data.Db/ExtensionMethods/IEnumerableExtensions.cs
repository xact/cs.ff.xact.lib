﻿
#if (!REMOVENS4EXTENSIONS) 
  //See: http://bit.ly/snE0xY
  namespace XAct {
      using System;
      using System.Collections;
      using System.Globalization;
      using System.Text;

#endif

    /// <summary>
    ///   Extension methods for IEnumerable objects in the Data Tier.
    /// </summary>
// ReSharper disable CheckNamespace
    public static class IEnumerableExtensions
// ReSharper restore CheckNamespace
    {
        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;

        ///<summary>
        ///  Convert the given list of values to an SQL 'IN' ready string.
        ///  <para>
        ///    The output is a string that will look like 
        ///    <c>"1,2,3"</c> or <c>"'A','B','C'"</c> 
        ///    or even <c>"('A','B','C')"</c>
        ///  </para>
        ///  <para>
        ///    Warning: Does not (yet?) escape inner single quotes...
        ///  </para>
        ///</summary>
        ///<param name = "list">The list of elements to convert.</param>
        ///<param name = "optionalFormatCode">
        ///  The optional formatCode to use when 
        ///  converting each element to a string.
        ///</param>
        ///<param name = "optionalTableAliasPrefix">
        ///  An optional table prefix (eg: 'R' or 'R.'). 
        ///  Note that the DOT separator is added if not included.
        ///</param>
        ///<param name = "useSinqleQuotes">
        ///  Wrap each element in a single quote.
        ///</param>
        ///<param name = "useLowerCase">
        ///  drop each element value to lowercase.
        ///</param>
        ///<param name = "addBrackets">
        ///  Wrap the whole list in brackets before returning.
        ///</param>
        ///<returns></returns>
        public static string ConvertListToInClause(
            this IEnumerable list,
            string optionalFormatCode,
            string optionalTableAliasPrefix,
            bool useSinqleQuotes, bool useLowerCase, bool addBrackets)
        {
            //Get out early if nothing to do:
            if (list == null)
            {
                return string.Empty;
            }
            //Right...in some cases, the list refers to 
            //column names, which may or may not have table alias prefixes:
            bool usingATableAlias = false;
            if (!string.IsNullOrEmpty(optionalTableAliasPrefix))
            {
                usingATableAlias = true;
                //If he wants a "R." before each element in the list:
                if (!optionalTableAliasPrefix.EndsWith(".", true, _invariantCulture))
                {
                    optionalTableAliasPrefix =
                        optionalTableAliasPrefix + ".";
                }
            }

            //bool useFormatting = String.IsNullOrEmpty(optionalFormatCode);

            if ((useSinqleQuotes) && (usingATableAlias))
            {
                throw new FormatException(
                    "Cannot have an IN clause that uses a table alias which is quoted.");
            }

            //Right...ready to roll:
            StringBuilder result = new StringBuilder();

            //Loop through args:
            foreach (object listItem in list)
            {
                //Stick a comma between args:
                result.Append(",");
                //Quote if asked for:
                if (useSinqleQuotes)
                {
                    result.Append("'");
                }
                if (usingATableAlias)
                {
                    result.Append(optionalTableAliasPrefix);
                }
                //Convert the value to a string:
                string val = (listItem is IFormattable)
                                 ? ((IFormattable) listItem).ToString(
                                     optionalFormatCode,
                                     CultureInfo.CurrentCulture)
                                 : listItem.ToString();

                //Lowercase if asked for (sometimes this is expected for Guid's)
                result.Append((!useLowerCase) ? val : val.ToLower(_invariantCulture));
                //Quote if asked for:
                if (useSinqleQuotes)
                {
                    result.Append("'");
                }
            }

            //Remove first comma:
            if (result.Length != 0)
            {
                result.Remove(0, 1);
            }

            //Bracket the whole thing:
            if ((addBrackets) && (result.Length > 0))
            {
                //we have args so bracket them:
                result.Insert(0, "(");
                result.Append(")");
            }
            //Convert to a string:
            return result.ToString();
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
