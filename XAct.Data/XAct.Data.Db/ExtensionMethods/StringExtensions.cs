﻿
#if (!REMOVENS4EXTENSIONS) 
  //See: http://bit.ly/snE0xY
  namespace XAct {
      using System;
      using System.Globalization;

#endif

    /// <summary>
    ///   Extension Methods to the String object, 
    /// specific to the Data Tier/Layer
    /// </summary>
    /// <internal>
    ///   <para>
    ///     As the methods are specific to the Data layer, they are not included in XAct.Core's 
    ///     StringExtensionMethods.
    ///   </para>
    /// </internal>
// ReSharper disable CheckNamespace
    public static class StringExtensions
// ReSharper restore CheckNamespace
    {
        /// <summary>
        ///   Convert string from portable SQL Date/Time format to DateTime object.
        /// </summary>
        /// <param name = "stringContainingDateTime">string reprentation of Date/Time.</param>
        /// <returns>DateTime object</returns>
        public static DateTime StrToDateTime_DB(this string stringContainingDateTime)
        {
            string[] tFormats = new[]
                                    {
                                        "yyyy-M-d H:m:s",
                                        "yy-M-d H:m:s",
                                        "yyyy-M-d"
                                    };
            try
            {
                DateTime tResult =
                    DateTime.ParseExact(stringContainingDateTime, tFormats, DateTimeFormatInfo.InvariantInfo,
                                        DateTimeStyles.None);
                return tResult;
            }
            catch (Exception exception)
            {
                throw new FormatException(
                    "Could not parse given date: '{0}'.".FormatStringCurrentUICulture(stringContainingDateTime),
                    exception);
            }
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
