﻿
#if (!REMOVENS4EXTENSIONS) 
  //See: http://bit.ly/snE0xY
  namespace XAct {
      using System;
      using System.Data;
      using System.Diagnostics;
      using System.Diagnostics.CodeAnalysis;

#endif

//Note: ExtensionMethods are common to Data -- hence no unique NS.

    /// <summary>
    ///   Extension Methods to the <see cref = "IDbCommand" /> object.
    /// </summary>
    [SuppressMessage("Microsoft.Security", "CA1709", Justification = "Db is perfectly ok.")]
// ReSharper disable CheckNamespace
    public static class IDbCommandExtensions
// ReSharper restore CheckNamespace
    {
        #region Static Helper Methods - Db - IDbParameter Creation

        /// <summary>
        ///   Creates a new IDbParameter and attaches it to the command.
        /// </summary>
        /// <param name = "command">The command.</param>
        /// <param name = "paramName">Name of the param.</param>
        /// <param name = "paramValue">The param value.</param>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the command argument is null.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the paramName argument is null or empty.
        /// </exception>
        public static void AttachParam(
            this IDbCommand command,
            string paramName,
            object paramValue)
        {
            if (paramName.IsNullOrEmpty())
            {
                throw new ArgumentNullException("paramName");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            IDbDataParameter param =
                CreateParam(
                    command,
                    paramName,
                    paramValue);

            command.Parameters.Add(param);
        }


        /// <summary>
        ///   Creates a Parameter ready for any SQL-92 database.
        /// </summary>
        /// <param name = "command">The command to attach the parameter to.</param>
        /// <param name = "paramName">Name of the param.</param>
        /// <param name = "value">The value.</param>
        /// <returns>The IDbDataParameter that was created.</returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the command argument is null.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the paramName argument is null or empty.
        /// </exception>
        public static IDbDataParameter CreateParam(
            this IDbCommand command,
            string paramName,
            object value)
        {
            command.ValidateIsNotDefault("command");
            if (paramName.IsNullOrEmpty())
            {
                throw new ArgumentNullException("paramName");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            IDbDataParameter param;
            param = command.CreateParameter();
            param.Direction = ParameterDirection.Input;
            param.ParameterName = paramName;

            if ((value == null) || (value == DBNull.Value))
            {
                param.Value = DBNull.Value;
                return param;
            }

            //This fails on SqlServerCE:
            //param.Value = value;

            //first look for a type specialized param construction
            Type type = value.GetType();

            if (type.IsEnum)
            {
                param.DbType = DbType.Int32;
                param.Value = value;
            }
            else if (type == typeof (DateTime))
            {
                param.DbType = DbType.DateTime;
                param.Value = value;
                //SqlServer's Min date is 1/1/1753:so will fail if you
                //insert a 0001/01/01...
                //http://database.ittoolbox.com/pub/TP012003.HTML

                if (command.GetType().FullName.Contains("SqlServer"))
                {
                    if (DateTime.MinValue.CompareTo(value) == 0)
                    {
                        param.Value = DBNull.Value;
                    }
                }
            }
            else if (type == typeof (Guid))
            {
                //param.DbType = DbType.Guid;
                //BUGFIX: Convert to String because 
                //some IDbDataParameter (Sqlite...) garble 
                //the output (maybe converting binary to string?).

                param.DbType = DbType.String;
                //BUGFIX: Actually...that causes it to fail on SqlServerCE
                //So maybe back to D Format...
                if (command.GetType().FullName.Contains("SqlServer"))
                {
                    param.Value = ((Guid) value).ToString("D"); //OK
                }
                else
                {
                    param.Value = ((Guid) value).ToString("N"); //OK
                }

                //Which messes up 32 chars and now needs 36 chars...
                //xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx 
                //but luckily not yet 38 of:
                //{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx} 
            }
            else if (type == typeof (bool))
            {
                //param.Value = ((bool)value) ? 1 : 0;
                param.DbType = DbType.Boolean;
                param.Value = value;
            }
            else if (type == typeof (int))
            {
                param.DbType = DbType.Int32;
                param.Value = value;
            }
            else if (type == typeof (Enum))
            {
                param.DbType = DbType.Int32;
                param.Value = (int) value;
            }
            else if (type == typeof (String))
            {
                param.DbType = DbType.String;
                param.Value = value;
            }
            else
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                Trace.TraceWarning(
                    "CreateParam: unknown type = {0} (try converting to string)",
                    type);
#endif
                param.DbType = DbType.String;
                param.Value = Convert.ToString(value);
            }
            return param;
        }

        #endregion

        /// <summary>
        /// Use with caution.
        /// <para>
        /// Same as using SetCommandText -- but without Code Validation throwing up an error.
        /// </para>
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="commandText"></param>
        [SuppressMessage("Microsoft.Security", "CA2100", Justification = "Command is a stored proc")]
        public static void SetCommandTextWithoutCA2010Warning(this IDbCommand dbCommand, string commandText)
        {
            dbCommand.CommandText = commandText;
        }

        #region Static Helper Methods - Db - Execute Scalar

        #region Static Helper Methods - Db - Execute Scalar

        /// <summary>
        ///   Executes a scalar request, and returns the scalar value.
        ///   <para>
        ///     Note: now also available as an Extension of <see cref = "IDbCommand" />.
        ///   </para>
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "command"></param>
        /// <returns></returns>
        public static T ExecuteScalar<T>(this IDbCommand command)
        {
            Type t = typeof (T);

            T defaultValue =
                (T)
                (t.IsValueType
                     ? Activator.CreateInstance(t)
                     : null);

            return ExecuteScalar(command, defaultValue);
        }

        /// <summary>
        ///   Executes a scalar request, and returns the scalar value.
        ///   <para>
        ///     Note: now also available as an Extension of <see cref = "IDbCommand" />.
        ///   </para>
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "command"></param>
        /// <param name = "defaultValue"></param>
        /// <returns></returns>
        public static T ExecuteScalar<T>(this IDbCommand command, T defaultValue)
        {
            defaultValue.ValidateIsNotDefault("defaultValue");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            object rawValue = command.ExecuteScalar();

            return (
                       (rawValue != null)
                       &&
                       (rawValue != DBNull.Value)
                   )
                       ? (T) Convert.ChangeType(
                           rawValue,
                           typeof (T), null)
                       : defaultValue;
        }

        #endregion

        #endregion
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
