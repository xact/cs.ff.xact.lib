//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Data;
//using System.Data.Common;

//namespace XAct.Data.Utils {


//  /// <summary>
//  /// Collection of static methods to aid syncing databases.
//  /// </summary>
//    public class SyncHelpers {




//        /// <summary>
//        /// 
//        /// </summary>
//        /// <internal>
//        /// <para>
//        /// According to: http://msdn2.microsoft.com/en-us/library/ms171933.aspx
//        /// Updating Two Related Tables in a Dataset with a Tableadapter
//        /// When updating related tables in a dataset, it is important to 
//        /// update in the proper sequence in order to reduce the 
//        /// chance of violating referential integrity constraints. 
//        /// The order of command execution will also follow the 
//        /// indices of the DataRowCollection in the dataset. 
//        /// To prevent data integrity errors from being raised, 
//        /// the best practice is to update the database in the following sequence:
//        /// Child table: delete records.
//        /// Parent table: insert, update, and delete records.
//        /// Child table: insert and update records.
//        /// </para>
//        /// <para>
//        /// In other words, we have to:
//        /// DELETE: LineItem, LineItemType, Invoice, InvoiceType, Contact, ContactType...etc.
//        /// INSERT: ContactType, Contact, InvoiceType, Invoice, LineItemType, LineItem...etc.
//        /// Notice how the order is reversed.
//        /// </para>
//        /// </internal>
//        /// <param name="clientChanges"></param>
//        /// <returns></returns>
//        public static DataSet SyncTable(DataSet clientChanges) {
//            //Check Args:
//            if (clientChanges == null) {
//                throw new System.ArgumentNullException("clientChanges");
//            }


//            //Declare vars:
//            string tableName;
//            string idColumnName;
//            DataTable dataTable;
//            DataTable toProcess;

//            ArrayList keysList;
//            ArrayList valsList;

//            string[] tKeys;
//            string[] tVals;

//            IDbCommand deleteCommand;
//            IDbCommand insertCommand;
//            IDbCommand updateCommand;

//            System.Data.IDbDataParameter param;
//            DbDataAdapter adapter;

//            System.Data.Common.DbProviderFactory factory = null;

//            //Get the order in which to process the files:
//            DataObjectSchema[] safeDataObjectOrder = GetSafeSyncOrderOfTables();


//            //*****************************
//            //PASS 1: DELETE CHILDREN FIRST
//            //*****************************
//            //Loop through the tables registered with SchemaManager
//            //as it should be the most complete:
//            foreach (DataObjectSchema dataObjectSchema in safeDataObjectOrder) {
//                //Get the name of the table:
//                tableName = dataObjectSchema.DataStoreName;

//                //If the dataset does not contain such a table, move on to the next:
//                if (!clientChanges.Tables.Contains(tableName)) {
//                    continue;
//                }


//                //Table exists -- we're going to process it.
//                //To process it we will need to know the id column:
//                idColumnName = dataObjectSchema.PrimaryKey.Name;

//                //Get the dataTable we will be importing:
//                dataTable = clientChanges.Tables[tableName];

//                //Check to see whether programmer took the time to be clean:
//                if (dataTable.GetChanges(DataRowState.Unchanged).Rows.Count > 0) {
//                    throw new System.Exception("::It is an error to transmit to the server a DataSet containnting a table that has 1 or more rows with state=Unchanged.");
//                }

//                //Get the deleted records from the table:
//                toProcess = dataTable.GetChanges(DataRowState.Deleted);

//                //Create an adapter:
//                adapter = factory.CreateDataAdapter();
        

//                //Create a Delete command:
//                deleteCommand = factory.CreateCommand();

//                //Fill it with SQL:
//                deleteCommand.CommandText = string.Format(
//                    "DELETE FROM {1} WHERE {2} {3} {0}",
//                    "?",
//                    tableName,
//                    idColumnName,
//                    "="
//                    );

//                //And add the one ID param:
//                param = deleteCommand.CreateParameter();

//                //We want the data to come from the column that has same name 
//                //as primary key property:
//                param.ParameterName = idColumnName;
//                param.SourceColumn = idColumnName;
//                deleteCommand.Parameters.Add(param);

//                //Command is ready to fly now...
//                adapter.Update(toProcess);
//            }



//            //*****************************
//            //PASS 2: INSERT, UPDATE PARENTS NEXT
//            //*****************************
//            //Loop through the tables registered with SchemaManager
//            //as it should be the most complete:
//            foreach (DataObjectSchema dataObjectSchema in safeDataObjectOrder) {

//                //Get the name of the table:
//                tableName = dataObjectSchema.DataStoreName;


//                //If the dataset does not contain such a table, move on to the next:
//                if (!clientChanges.Tables.Contains(tableName)) {
//                    continue;
//                }


//                //Table exists -- we're going to process it.
//                //To process it we will need to know the id column:
//                idColumnName = dataObjectSchema.PrimaryKey.Name;

//                //Get the dataTable we will be importing:
//                dataTable = clientChanges.Tables[tableName];

//                //Check to see whether programmer took the time to be clean:
//                if (dataTable.GetChanges(DataRowState.Unchanged).Rows.Count > 0) {
//                    throw new System.Exception("::It is an error to transmit to the server a DataSet containnting a table that has 1 or more rows with state=Unchanged.");
//                }

//                //----------
//                //Create the Commands command:
//                insertCommand = factory.CreateCommand();
//                updateCommand = factory.CreateCommand();
//                //----------
//                //Build SQL fragments:
//                keysList = new ArrayList();
//                valsList = new ArrayList();

//                tKeys = null;
//                tVals = null;

//                foreach (System.Data.DataColumn column in dataTable.Columns) {
//                    keysList.Add(column.ColumnName);
//                    valsList.Add("{0}");
//                }

//                foreach (DataObjectPropertySchema propertySchema in dataObjectSchema.Properties) {
//                    keysList.Add(propertySchema.Db.Name);
//                    valsList.Add("{0}");
//                }
//                tKeys = (string[])keysList.ToArray(typeof(string));
//                tVals = (string[])valsList.ToArray(typeof(string));

//                string sqlKeys = string.Join(", ", tKeys);
//                string sqlVals = string.Join(", ", tVals);

//                string sqlUpdate = " SET ";
//                foreach (string key in tKeys) {
//                    sqlUpdate += "(" + key + "=" + "{0}" + ")";
//                }
//                //----------
//                //Attach sql to commands:
//                insertCommand.CommandText = string.Format(
//                    "INSERT INTO {1} ({2}) VALUES ({3})",
//                    "?",
//                    tableName,
//                    sqlKeys,
//                    sqlVals
//                    );

//                updateCommand.CommandText = string.Format(
//                    "UPDATE {1} SET {2}",
//                    "?",
//                    tableName,
//                    sqlUpdate
//                    );
//                //----------
//                //Loop through the properties of this table's schema:
//                //Attaching parameters to the commands, in order to match
//                //sql placeholders:
//                foreach (DataObjectPropertySchema propertySchema in dataObjectSchema.Properties) {
//                    //For each, create a param:
//                    param = insertCommand.CreateParameter();
//                    //Name and associate it to the source column:
//                    param.ParameterName = propertySchema.Db.Name;
//                    param.SourceColumn = propertySchema.Db.Name;
//                    insertCommand.Parameters.Add(param);

//                    param = updateCommand.CreateParameter();
//                    //Name and associate it to the source column:
//                    param.ParameterName = propertySchema.Db.Name;
//                    param.SourceColumn = propertySchema.Db.Name;
//                    updateCommand.Parameters.Add(param);
//                }
//                //----------
//                //Create an adapter for Inserts:
//                adapter = factory.CreateDataAdapter();

//                //Get the deleted records from the table:
//                toProcess = dataTable.GetChanges(DataRowState.Added);

//                //Connect the commands to the adapter:
//                adapter.InsertCommand = (DbCommand)insertCommand;

//                //....
//                adapter.ContinueUpdateOnError = true;

//                //Go!
//                adapter.Update(toProcess);

//                //Done with Updates, now with Modified:
//                //Create an adapter:
//                adapter = factory.CreateDataAdapter();

//                //Get the Updated records from the table:
//                toProcess = dataTable.GetChanges(DataRowState.Modified);

//                //Connect the commands to the adapter:
//                adapter.InsertCommand = (DbCommand)insertCommand;

//                //....
//                adapter.ContinueUpdateOnError = true;

//                //Go!
//                adapter.Update(toProcess);
//            }

//            return null;

//        }



//    /// <summary>
//    /// Gets the safe sync order of tables names.
//    /// </summary>
//    /// <returns></returns>
//        public static DataObjectSchema[] GetSafeSyncOrderOfTables() {
//            DataObjectSchema[] list = SchemaManager.DataObjectSchemas.ToArray();
//            return GetSafeSyncOrderOfTables(list);
//        }


//    /// <summary>
//    /// Gets the safe sync order of tables names.
//    /// </summary>
//    /// <param name="tableNames">The names of the tables to sync.</param>
//    /// <returns></returns>
//        public static DataObjectSchema[] GetSafeSyncOrderOfTables(string[] tableNames) {
//            //Check Args:
//            if (tableNames == null) {
//                throw new System.ArgumentNullException("tableNames");
//            }

//            //Build an array of DataObjectSchema objects derived from the passed tableNames:
//            System.Collections.ArrayList tmp = new System.Collections.ArrayList();
//            foreach (string tableName in tableNames) {
//                DataObjectSchema schema = SchemaManager.DataObjectSchemas.GetByDataStoreName(tableName);
//                if (tmp == null) {
//                    throw new System.Exception("::Relationship Schema not found.");
//                }
//                tmp.Add(schema);
//            }

//            DataObjectSchema[] dataObjectSchemas = (DataObjectSchema[])tmp.ToArray(typeof(DataObjectSchema));

//            //First pass:
//            dataObjectSchemas = GetSafeSyncOrderOfTables(dataObjectSchemas);
//            //Second pass:
//            dataObjectSchemas = GetSafeSyncOrderOfTables(dataObjectSchemas);

//            return dataObjectSchemas;
//        }


//    /// <summary>
//    /// Gets the safe sync order of tables names.
//    /// </summary>
//    /// <param name="dataObjectSchemas">The <c>dataObject</c> schemas.</param>
//    /// <returns></returns>
//        public static DataObjectSchema[] GetSafeSyncOrderOfTables(DataObjectSchema[] dataObjectSchemas) {
//            //Check Args:
//            if (dataObjectSchemas == null) {
//                throw new System.ArgumentNullException("dataObjectSchemas");
//            }

//            System.Collections.ArrayList buffer = new System.Collections.ArrayList();


//            int insertedAt = 0;

//            foreach (DataObjectSchema dataObjectSchema in dataObjectSchemas) {
//                //Loop through already placed results:

//                //Reset flag every time:
//                bool inserted = false;

//                //Get lookup tables that this table is dependant on:
//        DataObjectSchema[] dependants = dataObjectSchema.GetParentSchemas();

//                if (dependants != null) {
//                    //Work from left to right so that new 
//                    //entry is inserted BEFORE first 'child/dependancy' found:
//                    for (int i = 0; i < buffer.Count; i++) {

//                        //We want to ch
//                        DataObjectSchema alreadyAdded = (DataObjectSchema)buffer[i];

//                        //Reset contained flag:
//                        bool contained = false;

//                        //Loop through list of dependants:
//                        for (int j = 0; j < dependants.Length; j++) {
//                            //We want one:
//                            if (dependants[j] == alreadyAdded) {
//                                //Get out:
//                                contained = true;
//                                break;
//                            }
//                        }

//                        if (contained) {
//                            //Insert the object BEFORE the one that was found:
//                            buffer.Insert(i, dataObjectSchema);
//                            inserted = true;
//                            insertedAt = i;
//                            break;
//                        }//End:contained
//                    }//End:i
//                }//End:dependants != null

//                //If we are at the end, and it hasn't been inserted yet,
//                //append it to the end of the buffer array:
//                if (!inserted) {
//                    buffer.Add(dataObjectSchema);
//                }

//                //Moving right along...

//            }//End foreach

//            //INPUT: ContactType, Invoice, Contact, ListItemType, ListItem
//            //1) [ContactType]
//            //2) ContactType, [Invoice]
//            //3) ConctactType, [Contact], Invoice
//            //4) ContactType, Contact, Invoice, [ListItemType]
//            //5) ContactType, Contact, Invoice, [ListItem], ListItemType


//            //INPUT: Invoice, ListItem, Contact, ListItemType, ContactType
//            //[Invoice]
//            //Invoice, [ListItem]
//            //[Contact], Invoice, ListItem
//            //Contact, Invoice, ListItemType, ListItem
//            //ContactType, Contact, Invoice, ListItemType,ListItem


//            //ContactType, ListItem, Project, Contact, Invoice, ProjectType, ListItemType
//            //[ConntactType]
//            //ContactType, [ListItem]
//            //ContactType, [Project], ListItem
//            //ContactType, Project, [Contact], ListItem
//            //ContactType, Project, Contact, [Invoice], ListItem
//            //ContactType, [ProjectType], Project, Contact, Invoice, ListItem
//            //ContactType, ProjectType, Project, Contact, Invoice, ListItemType, ListItem


//            return (DataObjectSchema[])buffer.ToArray(typeof(DataObjectSchema));


//        }


//  }
//}
