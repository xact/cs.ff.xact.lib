namespace XAct.Data
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using XAct.Environment;

    /// <summary>
    ///   A static class of helpful static methods used in applications accessing Databases.
    /// </summary>
    public static class DbHelpers
    {
        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;


        static IEnvironmentService EnvironmentService
        {
            get { return DependencyResolver.Current.GetInstance<IEnvironmentService>(); }
        }


        #region IDbConnection creation

        /// <summary>
        ///   Gets the DbProviderFactory specified 
        ///   by the named 
        ///   <see cref = "ConnectionStringSettings" />.
        /// </summary>
        /// <param name = "nameOfConfigConnectionSetting">
        ///   The name of config connection setting.
        /// </param>
        /// <returns></returns>
        /// <internal>
        /// There is no Interface backing <see cref="DbProviderFactory"/>
        /// </internal>
        public static DbProviderFactory GetFactory(
            string nameOfConfigConnectionSetting)
        {
            ConnectionStringSettings connectionSettings =
                ConfigurationManager.ConnectionStrings[
                    nameOfConfigConnectionSetting
                    ].ConfigureConnectionSettings();

            //return DbProviderFactories.GetFactory(connectionSettings.ProviderName);

            DbProviderFactory factory;
            try
            {
                factory = DbProviderFactories.GetFactory(
                    connectionSettings.ProviderName);
            }
            catch (ArgumentException exception)
            {
                throw new ArgumentException(
                    string.Format(
                        _invariantCulture,
                        "::DbProviderFactories.GetFactory('{1}') failed.{0}" +
                        "Make sure it is installed in the" +
                        " config file as follows:{0}" +
                        "<system.data>{0}" +
                        "  <DbProviderFactories>{0}" +
                        "    <add invariant=\"{1}\" name=\"...\"" +
                        " description=\"...\" type=\"...\"/>{0}" +
                        "  <DbProviderFactories>{0}" +
                        "<system.data>{0}" +
                        "",
                        System.Environment.NewLine,
                        connectionSettings.ProviderName),
                    exception);
            }
            return factory;
        }


        /// <summary>
        ///   Creates a new open DB connection 
        ///   using the specified ConnectionString settings.
        ///   <para>
        ///     Opens the connection before returning it.
        ///   </para>
        /// </summary>
        /// <param name = "nameOfConfigConnectionSetting">
        ///   The ConfigConnectionSetting name.
        /// </param>
        /// <returns>A new open conection.</returns>
        public static IDbConnection CreateConnection(
            string nameOfConfigConnectionSetting)
        {
            if (nameOfConfigConnectionSetting.IsNullOrEmpty())
            {
                throw new ArgumentNullException("nameOfConfigConnectionSetting");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            ConnectionStringSettings connectionSettings =
                ConfigurationManager.ConnectionStrings[
                    nameOfConfigConnectionSetting
                    ].ConfigureConnectionSettings();

            if (connectionSettings == null)
            {
                throw new ArgumentException(
                    string.Format(
                        _invariantCulture,
                        "::Could not find ConnectionSetting called '{0}'.",
                        nameOfConfigConnectionSetting));
            }

            return connectionSettings.CreateConnection();
        }


        /// <summary>
        ///   Creates a new open DB connection using the specified
        ///   providerName and connectionString.
        ///   <para>
        ///     Opens the connection before returning it.
        ///   </para>
        /// </summary>
        /// <param name = "providerName">Name of the provider.</param>
        /// <param name = "connectionString">The connection string.</param>
        /// <returns>A new open connection.</returns>
        public static IDbConnection CreateConnection(
            string providerName,
            string connectionString)
        {
            //Check Args:
            if (providerName.IsNullOrEmpty())
            {
                throw new ArgumentNullException("providerName");
            }
            if (connectionString.IsNullOrEmpty())
            {
                throw new ArgumentNullException("connectionString");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            // assume that if invalid providerName, 
            //it throws something (not specified in msdn doc)

            //return DbProviderFactories.GetFactory(
            // connectionSettings.ProviderName);

            DbProviderFactory factory;
            try
            {
                factory = DbProviderFactories.GetFactory(providerName);
            }
            catch (ArgumentException exception)
            {
                throw new ArgumentException(
                    string.Format(
                        _invariantCulture,
                        "::DbProviderFactories.GetFactory('{1}') failed.{0}" +
                        "Make sure it is installed" +
                        " in the config file as follows:{0}" +
                        "<system.data>{0}" +
                        "  <DbProviderFactories>{0}" +
                        "    <add invariant=\"{1}\" name=\"...\"" +
                        " description=\"...\" type=\"...\"/>{0}" +
                        "  <DbProviderFactories>{0}" +
                        "<system.data>{0}" +
                        "".FormatStringInvariantCulture(System.Environment.NewLine, providerName),
                        exception));
            }
            if (factory == null)
            {
                throw new ConfigurationErrorsException(
                    string.Format(
                        _invariantCulture,
                        "DbProviderFactory not found called '{0}'.", providerName)
                    );
            }
            IDbConnection connection = factory.CreateConnection();

            connection.ConnectionString = connectionString;

            connection.Open();

            return connection;
        }

        #endregion

        #region ConnectionString / DataDirectory / Rooted paths.

        /// <summary>
        ///   Helper Method to expand the path part of a
        ///   <see cref = "ConnectionStringSettings.ConnectionString" /> property
        ///   replacing the '|DATADIRECTORY|' macro with the 
        ///   application's designated Data directory.
        ///   <para>
        ///     If not rooted, prepends the path with the <c>AppDir</c>.
        ///   </para>
        /// </summary>
        /// <param name = "connectionStringSettingsName">
        ///   Name of the connection settings.</param>
        /// <returns>
        ///   The modified ConnectionStringSetting.</returns>
        public static ConnectionStringSettings ConfigureConnectionSettings(
            string connectionStringSettingsName)
        {
            if (connectionStringSettingsName.IsNullOrEmpty())
            {
                throw new ArgumentNullException("connectionStringSettingsName");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            ConnectionStringSettings connectionStringSetting =
                ConfigurationManager.ConnectionStrings[
                    connectionStringSettingsName
                    ];

            if (connectionStringSetting == null)
            {
                throw new ArgumentException(
                    string.Format(
                        _invariantCulture,
                        "Missing connection string : '{0}'",
                        connectionStringSettingsName));
            }

            return connectionStringSetting.ConfigureConnectionSettings();
        }






        /// <summary>
        ///   Replace '|DataDirectory|' in the given path, 
        ///   with the path to the application's designated Data directory.
        ///   <para>
        ///     If not rooted, prepends the path with the <c>AppDir</c>.
        ///   </para>
        /// </summary>
        /// <param name = "tmpPath">The path to parse.</param>
        /// <returns>The expanded path.</returns>
        public static string ExpandDataDirectoryMacro(string tmpPath)
        {
            return ExpandDataDirectoryMacro(tmpPath, true);
        }

        /// <summary>
        ///   Replace '|DataDirectory|' in the given path, 
        ///   with the path to the application's designated Data directory.
        ///   <para>
        ///     If not rooted, can be prepended with the <c>AppDir</c>.
        ///   </para>
        /// </summary>
        /// <param name = "tmpPath">The path to parse.</param>
        /// <param name = "ensureRooted">Whether or not to ensure the string is rooted.</param>
        /// <returns>The expanded path.</returns>
        public static string ExpandDataDirectoryMacro(string tmpPath, bool ensureRooted)
        {
            if (string.IsNullOrEmpty(tmpPath))
            {
                return string.Empty;
            }
            //CASE INSENSITIVE REPLACEMENT OF |DATADIRECTORY| MACRO:
            const string dataDirectoryPattern = "\\|DataDirectory\\|";
            if (Regex.IsMatch(
                tmpPath,
                dataDirectoryPattern,
                RegexOptions.IgnoreCase))
            {
                tmpPath =
                    Regex.Replace(
                        tmpPath,
                        dataDirectoryPattern,
                        DataDirectoryPath + "\\",
                        RegexOptions.IgnoreCase);
            }
            return (ensureRooted) ? EnvironmentService.MapPath(tmpPath) : tmpPath;
        }


        private static string DataDirectoryPath 
        {
            get
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                object dataDirectoryObject =
                    AppDomain.CurrentDomain.GetData("DataDirectory");

                string dataDirectory =
                    (dataDirectoryObject != null)
                        ? dataDirectoryObject + @"\"
#pragma warning disable 612,618
                        : EnvironmentService.ApplicationBasePath + @"\Data\";
#pragma warning restore 612,618
#else
        string dataDirectory = AppDir + @"\Data\";
#endif

                return dataDirectory;
            }
        }


        #endregion

        #region CE Extensions

#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//PC: Not required
#else

    /// <summary>
    /// CE solution for getting EntryAssembly.
    /// </summary>
    /// <returns>The name of the assembly</returns>
    protected static System.Reflection.Assembly GetEntryAssembly() {
        //CE:
      System.Text.StringBuilder sb = 
      new System.Text.StringBuilder(
         0x100 
        * 
        Marshal.SystemDefaultCharSize);
      int returnedSize = GetModuleFileName(IntPtr.Zero, sb, 0xff);
      if (returnedSize <= 0) {
        return null;
      }
      if (returnedSize > 0xff) {
        throw new System.Exception(
        "Returned Assembly name longer than MAX_PATH chars."
        );
      }
      return System.Reflection.Assembly.LoadFrom(sb.ToString());
    }

    public static class API
    {
    [DllImport("coredll.dll", SetLastError = true)]
    private static extern int GetModuleFileName(
      IntPtr hModule, 
      System.Text.StringBuilder lpFilename, 
      int nSize);
    }
#endif

        #endregion
    }


}


