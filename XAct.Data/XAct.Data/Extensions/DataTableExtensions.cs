﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Text;

#endif

/// <summary>
    ///   Extension Methods to the DataTable Type.
    /// </summary>
    public static class DataTableExtensions
    {
        /// <summary>
        ///   Creates an ArrayList of the values of one column in a DataTable.
        /// </summary>
        /// <param name = "dataTable">DataTable to scan.</param>
        /// <param name = "columnName">Name of Column.</param>
        /// <returns>An ArrayList of Column values.</returns>
        /// <remarks>
        ///   <para>
        ///     Could be useful to create DropDownLists, etc.
        ///   </para>
        ///   <para>
        ///     Currently not used anywhere in this assembly.
        ///   </para>
        /// </remarks>
        public static IList<T> ToArrayList<T>(this DataTable dataTable, string columnName)
        {
            IList<T> results = new List<T>();

            if (dataTable != null)
            {
                foreach (DataRow tRow in dataTable.Rows)
                {
                    results.Add((T) tRow[columnName]);
                }
            }
            return results;
        }


        /// <summary>
        /// Serializes the DataTable to a humanly readable string output.
        /// <para>
        /// Not brilliant...
        /// </para>
        /// </summary>
        /// <param name="table">The table.</param>
        /// <param name="sepChar">The sep char.</param>
        /// <returns></returns>
        public static string DataTableToString(this DataTable table, string sepChar="\t")
        {
            StringBuilder builder = new StringBuilder();
            foreach (DataColumn column in table.Columns)
            {
                builder.Append(column.Caption + sepChar);
            }
            builder.Append(System.Environment.NewLine);

            foreach (DataRow row in table.Rows)
            {
                foreach (DataColumn column in table.Columns)
                {
                    builder.Append(row[column] + sepChar);
                }
                builder.Append(System.Environment.NewLine);
            }
            return builder.ToString();
        }



        /// <summary>
        /// Converts the DataTable to CSV.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="sepChar">The sep char.</param>
        /// <returns></returns>
        public static string ToCsv(this DataTable dataTable, string sepChar=",")
        {
            MemoryStream ms = new MemoryStream();
            StreamWriter sw = new StreamWriter(ms);
            int iColCount = dataTable.Columns.Count;
            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dataTable.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(sepChar);
                }
            }
            sw.Write(sw.NewLine);

            foreach (DataRow dr in dataTable.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }

                    if (i < iColCount - 1)
                    {
                        sw.Write(sepChar);
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
            return Encoding.UTF8.GetString(ms.ToArray());
        }
    }

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
