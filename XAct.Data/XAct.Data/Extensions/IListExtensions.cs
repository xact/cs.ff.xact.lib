﻿namespace XAct
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;

    /// <summary>
    /// Extensions to IList suitable for the the Data Layer
    /// (where one has access to DataTable)
    /// </summary>
    public static class IListExtensions
    {

        /// <summary>
        /// Converts a List into a DataTable.
        /// </summary>
        /// <para>
        /// Given 
        /// <code>
        /// <![CDATA[
        /// IList list = new List<Persons>{{First="John",Last="Smith"}};
        /// ]]>
        /// </code>
        /// the result will be a a datatable, with 
        /// Column headers matching the property names.
        /// 
        /// </para>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }

            object[] values = new object[props.Count];

            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
    }
}
