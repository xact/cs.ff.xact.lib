namespace XAct
{
    using System;
    using Lucene.Net.Documents;
    using XAct.Data.Search;

    /// <summary>
    /// Extensions to the <see cref="Stored"/> enum
    /// </summary>
    public static class StoredExtensions
    {
        /// <summary>
        /// Maps the lib specific enum to a Lucene.NET specific enum.
        /// </summary>
        /// <param name="indexed">The indexed.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public static Field.Store MapTo(this Stored indexed)
        {
            switch (indexed)
            {
                case Stored.No:
                    return Field.Store.NO;
                case Stored.Yes:
                    return Field.Store.YES;
                default:
                    throw new ArgumentOutOfRangeException();
                    //break;
            }
        }
    }
}