namespace XAct
{
    using System;
    using Lucene.Net.Documents;
    using XAct.Data.Search;

    /// <summary>
    /// Extensions to <see cref="Indexed"/> enums.
    /// </summary>
    public static class IndexedExtensions
    {
        /// <summary>
        /// Maps the lib specific enum to a Lucene.NET specific enum.
        /// </summary>
        /// <param name="indexed">The indexed.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public static Field.Index MapTo(this Indexed indexed)
        {
            switch (indexed)
            {
                case Indexed.No:
                    return Field.Index.NO;
                case Indexed.Untokenized:
                    return Field.Index.NOT_ANALYZED;
                    //case Indexed.Untokenized:
                    //    return Field.Index.NOT_ANALYZED_NO_NORMS;
                case Indexed.Tokenized:
                    return Field.Index.ANALYZED;
                    //case Indexed.Tokened:
                    //    return Field.Index.ANALYZED_NO_NORMS;
                default:
                    throw new ArgumentOutOfRangeException();
                    //break;
            }
        }
    }
}