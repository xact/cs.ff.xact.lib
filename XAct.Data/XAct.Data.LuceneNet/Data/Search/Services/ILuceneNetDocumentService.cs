﻿namespace XAct.Data.Search
{
    using Lucene.Net.Analysis;
    using Lucene.Net.Search;
    using XAct.Spikes.CodeFirst.Data.Search.Entities;

    /// <summary>
    /// A Lucene.NET specific contract of 
    /// the <see cref="IDocumentService"/> contract.
    /// </summary>
    public interface ILuceneNetDocumentService : IDocumentService, IHasXActLibService
    {

        /// <summary>
        /// Gets the shared singleton configuration object used to initiate
        /// an instance of this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        ILuceneDocumentSearchManagementServiceConfiguration Configuration { get; }


        /// <summary>
        /// Searches the specified indexed collection.
        /// </summary>
        /// <param name="indexName">Name of the index.</param>
        /// <param name="query">The query.</param>
        /// <param name="hitFieldNamesToReturn">The hit field names to return.</param>
        /// <returns></returns>
        ScoredDocumentResult[] Search(string indexName, Query query, params string[] hitFieldNamesToReturn);



    }
}