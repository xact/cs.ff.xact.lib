﻿
namespace XAct.Data.Search
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using Lucene.Net.Analysis.Standard;
    using Lucene.Net.Documents;
    using Lucene.Net.Index;
    using Lucene.Net.QueryParsers;
    using Lucene.Net.Search;
    using XAct.Services;
    using XAct.Spikes.CodeFirst.Data.Search;
    using XAct.Spikes.CodeFirst.Data.Search.Entities;

    /// <summary>
    /// An implementation of the Lucene.NET specific implementation of the
    /// <see cref="ILuceneNetDocumentService" /> contract.
    /// </summary>
    [DefaultBindingImplementation(typeof(IDocumentService), BindingLifetimeType.Undefined, Priority.Low)]
    public class LuceneNetDocumentService : ILuceneNetDocumentService
    {
        /// <summary>
        /// Gets the configuration used for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public ILuceneDocumentSearchManagementServiceConfiguration Configuration
        {
            get { return _configuration; }
        }
        private readonly ILuceneDocumentSearchManagementServiceConfiguration _configuration;


        /// <summary>
        /// Initializes a new instance of the <see cref="LuceneNetDocumentService"/> class.
        /// </summary>
        /// <param name="luceneDocumentSearchManagementServiceConfiguration">The lucene document search management service configuration.</param>
        public LuceneNetDocumentService(
            ILuceneDocumentSearchManagementServiceConfiguration luceneDocumentSearchManagementServiceConfiguration)
        {
            _configuration = luceneDocumentSearchManagementServiceConfiguration;
        }

        /// <summary>
        /// Persist the given Models as Documents in the named Document store.
        /// </summary>
        /// <typeparam name="TModel">The type of the entity.</typeparam>
        /// <param name="indexedDocumentStoreName">Name of the index.</param>
        /// <param name="models">The models.</param>
        public void Persist<TModel>(string indexedDocumentStoreName, params TModel[] models)
        {
            Persist(indexedDocumentStoreName, models, appendToExistingIndex: true, createIfNonExistent: true);
        }

        /// <summary>
        /// Creates the lucene.net index with person details
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="indexedDocumentStoreName"></param>
        /// <param name="models"></param>
        /// <param name="appendToExistingIndex"></param>
        /// <param name="createIfNonExistent"></param>
        /// <exception cref="System.Exception">Configuration.IndexRootDirectory has not been configued yet.</exception>
        public void Persist<TModel>(string indexedDocumentStoreName, IEnumerable<TModel> models, bool appendToExistingIndex = true, bool createIfNonExistent=true)
        {
            if (_configuration.IndexRootDirectory.IsNullOrEmpty())
            {
                throw new Exception("Configuration.IndexRootDirectory has not been configued yet.");
            }

            var luceneDirectory = CreateDirectory<TModel>(indexedDocumentStoreName);


            var standardAnalyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);

            //                        System.IO.TextReader someText = ....;
            //TokenStream phonetics = new PhoneticFilter(new LowerCaseFilter(new WhitespaceTokenizer(someText )));
            IndexWriter indexWriter;
            try
            {
                indexWriter = new IndexWriter(luceneDirectory, standardAnalyzer, !appendToExistingIndex,
                                              IndexWriter.MaxFieldLength.UNLIMITED);
            }
            catch (System.IO.FileNotFoundException)
            {
                //Was asked to open an existing index, but didn't find one
                //so creating a new one if am allowed.
                if (createIfNonExistent)
                {
                    indexWriter = new IndexWriter(luceneDirectory, standardAnalyzer, true,
                                                  IndexWriter.MaxFieldLength.UNLIMITED);
                }
                else
                {
                    throw;
                }
            }
            catch
            {
                //whatever other error (eg Lucene.Net.Store.NoSuchDirectoryException)
                throw;
            }

            //AHA: http://stackoverflow.com/questions/24948750/implement-smart-search-fuzzy-string-comparison?lq=1



            indexWriter.SetRAMBufferSizeMB(10.0);


            //indexWriter.SetUseCompoundFile(false);
            //indexWriter.SetMaxMergeDocs(10000);
            //indexWriter.SetMergeFactor(100);

            foreach (TModel entity in models)
            {

                List<Term> storedNotIndexed = new List<Term>();
                
                //Create a new Document object
                //that we will populate with Fields
                //from the scanned entity's properties (those that are marked with DocumentFieldAttribute.Stored)
                var document = MapToDocument(entity, ref storedNotIndexed);

                if ((appendToExistingIndex) && (storedNotIndexed.Count > 0))
                {
                    //We're appending to an existing store.
                    //Which means the document may already be there.
                    //Which means we have to delete it first.

                    //when the document was being generated, it turns out that
                    //it had several fields marked as the source of Indexes.
                    //These fields are being stored, but not indexed.

                    indexWriter.DeleteDocuments(storedNotIndexed.ToArray());
                    indexWriter.Commit();
                }

                // Write the Document to the catalog
                indexWriter.AddDocument(document);
            }

            // Close the writer
            indexWriter.Dispose();
        }



        //public void DeleteRecord(string indexedDocumentStoreName, string key, string value)
        //{
        //    //Delete an old one:
        //    //indexReader.DeleteDocuments(new Term("patient_id", patientID));
        //    //Specify the index file location where the indexes are to be stored
        //    string indexFileLocation = Path.Combine(_configuration.IndexRootDirectory, indexedDocumentStoreName);


        //    Lucene.Net.Store.Directory luceneDirectory = Lucene.Net.Store.FSDirectory.Open(indexFileLocation);

            
        //         var indexWriter = new IndexWriter(luceneDirectory, standardAnalyzer, !appendToExistingIndex,
        //                                      IndexWriter.MaxFieldLength.UNLIMITED);
           
        //    indexWriter.DeleteDocuments();

        //    Lucene.Net.Index.IndexModifier modifier = new Lucene.Net.Index.IndexModifier("c:\\folder_where_lucene_index_lives", analyzer, false);
          
        //}



        /// <summary>
        /// </summary>
        /// <param name="indexedDocumentStoreName"></param>
        /// <param name="searchString"></param>
        /// <param name="defaultFieldName"></param>
        /// <param name="hitFieldNamesToReturn"></param>
        /// <returns></returns>
        public ScoredDocumentResult[] Search(string indexedDocumentStoreName, string searchString, string defaultFieldName,
                                             params string[] hitFieldNamesToReturn)
        {
            defaultFieldName.ValidateIsNotNullOrEmpty("defaultFieldName");

            Lucene.Net.Analysis.Analyzer analyzer =
                new Lucene.Net.Analysis.Standard.StandardAnalyzer
                    (Lucene.Net.Util.Version.LUCENE_30);

            //Create a parser, giving the default field (eg: 'FamilyName') that will be searched if
            //term is not prefixed with fieldname 
            //eg: in the following case Buck will be searched in the default document Field name: 
            //"buck" AND title:"foo bar" AND body:"quick fox"
            QueryParser queryParser = new QueryParserEx(Lucene.Net.Util.Version.LUCENE_30, defaultFieldName, analyzer);

            //Parse the string:
            //title:"foo bar" AND body:"quick fox" OR body:"quik~ fox" OR body:"quic* fox"
            Lucene.Net.Search.Query query = queryParser.Parse(searchString);

            return Search(indexedDocumentStoreName, query, hitFieldNamesToReturn);
        }


        /// <summary>
        /// Searches the specified indexed collection.
        /// </summary>
        /// <param name="indexedDocumentStoreName">Name of the index.</param>
        /// <param name="query">The query.</param>
        /// <param name="hitFieldNamesToReturn">The hit field names to return.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Configuration.IndexRootDirectory has not been configued yet.</exception>
        public ScoredDocumentResult[] Search(string indexedDocumentStoreName, Query query, params string[] hitFieldNamesToReturn)
        {
            if (_configuration.IndexRootDirectory.IsNullOrEmpty())
            {
                throw new Exception("Configuration.IndexRootDirectory has not been configued yet.");
            }
            indexedDocumentStoreName.ValidateIsNotNullOrEmpty("indexedDocumentStoreName");
            hitFieldNamesToReturn.ValidateIsNotDefault("hitFieldNamesToReturn");

            //Specify the index file location where the indexes are to be stored
            string indexFileLocation = Path.Combine(_configuration.IndexRootDirectory, indexedDocumentStoreName); 


            Lucene.Net.Store.Directory luceneDirectory = Lucene.Net.Store.FSDirectory.Open(indexFileLocation);


            IndexSearcher indexSearcher = new IndexSearcher(luceneDirectory);

            //See this first
            //http://stackoverflow.com/questions/20710503/is-it-best-to-use-a-lucene-keywordanalyzer-to-index-text-for-an-auto-suggest-tex

            //SEe: http://www.thebestcsharpprogrammerintheworld.com/blogs/How-to-create-and-search-a-Lucene-Net-index-in-4-simple-steps-using-c-sharp-Step-4.aspx

            //SEE: http://stackoverflow.com/questions/468405/how-to-incorporate-multiple-fields-in-queryparser


            //Lucene.Net.Search.


            //Get top 10 records:
            TopScoreDocCollector topScoreDocCollector = TopScoreDocCollector.Create(10, true);


            //Search away:
            indexSearcher.Search(query, topScoreDocCollector);


            // Making a boolean query for searching and get the searched hits
            List<ScoredDocumentResult> results = new List<ScoredDocumentResult>();

            foreach (var scoreDoc in topScoreDocCollector.TopDocs(0, 10).ScoreDocs)
            {
                ScoredDocumentResult resultItem = new ScoredDocumentResult();

                //Get the whole doc:
                Lucene.Net.Documents.Document doc = indexSearcher.Doc(scoreDoc.Doc);


                //Save the score:
                resultItem.Score = scoreDoc.Score;
                //Iterate through Fields we were asked to return:

                List<ScoredDocumentFieldResult> fields = new List<ScoredDocumentFieldResult>();

// ReSharper disable LoopCanBeConvertedToQuery
                foreach (string fieldName in hitFieldNamesToReturn)
// ReSharper restore LoopCanBeConvertedToQuery
                {
                    string fieldValue = doc.Get(fieldName);
                    fields.Add(new ScoredDocumentFieldResult{Key= fieldName,Value=fieldValue});
                }

                resultItem.FieldValues = fields.ToArray();

                results.Add(resultItem);
            }

            indexSearcher.Dispose();

            return results.ToArray();
        }


        /// <summary>
        /// </summary>
        /// <param name="indexedDocumentStoreName"></param>
        /// <param name="keyFieldName"></param>
        /// <param name="keyFieldValue"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Delete(string indexedDocumentStoreName, string keyFieldName, string keyFieldValue)
        {
            throw new NotImplementedException();
        }




















        private static Document MapToDocument<TModel>(TModel entity, ref List<Term> StoredNotIndexed)
        {
            Document result = new Document();

            foreach (
                PropertyInfo propertyInfo in
                    entity.GetType().GetPropertiesWithAttribute(typeof(DocumentFieldAttribute)))
            {
                //Get the decorative attribute:
                DocumentFieldAttribute documentFieldAttribute =
                    (DocumentFieldAttribute)propertyInfo.GetAttribute(typeof(DocumentFieldAttribute));


                //Determine the Document Field name from the Attribute.FieldName -- or if not given
                //fall back to the name of the scanned entity's property:
                var documentFieldName = documentFieldAttribute.FieldName ?? propertyInfo.Name;

                //Get it's scanned entitie's typed value:
                object value = propertyInfo.GetValue(entity);

                //Convert the value to a string, as the Field type is pretty simplistic in that regard:
                //string stringValue;

                //The type of the field, which we can use to guess/autodetermine the document
                //field type.
                Type type = propertyInfo.PropertyType;
                //Some, like DateTime, need some hinting to better choose how to store the data:
                FieldType fieldType = documentFieldAttribute.FieldType;

#pragma warning disable 219
                AbstractField field;
#pragma warning restore 219
                NumericField numericField;
                Field stringField;

                bool index = (documentFieldAttribute.Indexed != Indexed.No);


                if (type == typeof(int))
                {
                    var typedValue = (int)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            4,
                                                            documentFieldAttribute.Stored.MapTo(), index);

                    numericField.SetIntValue(typedValue);
                    result.Add(numericField);
                }
                else if (type == typeof(int?))
                {
                    var typedValue = (int?)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            4,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    numericField.SetIntValue(typedValue.HasValue ? typedValue.Value : 0);
                    result.Add(numericField);
                }
                else if (type == typeof(long))
                {
                    var typedValue = (long)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            8,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    numericField.SetLongValue(typedValue);
                    result.Add(numericField);
                }
                else if (type == typeof(long?))
                {
                    var typedValue = (long?)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            8,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    numericField.SetLongValue(typedValue.HasValue ? typedValue.Value : 0);
                    result.Add(numericField);
                }
                else if (type == typeof(float))
                {
                    var typedValue = (float)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            4,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    numericField.SetFloatValue(typedValue);
                    result.Add(numericField);
                }
                else if (type == typeof(float?))
                {
                    var typedValue = (long?)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            4,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    numericField.SetFloatValue(typedValue.HasValue ? typedValue.Value : 0);
                    result.Add(numericField);
                }
                else if (type == typeof(double))
                {
                    var typedValue = (double)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            8,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    numericField.SetDoubleValue(typedValue);
                    result.Add(numericField);
                }
                else if (type == typeof(double?))
                {
                    var typedValue = (double?)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            8,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    numericField.SetDoubleValue(typedValue.HasValue ? typedValue.Value : 0);
                    result.Add(numericField);
                }
                else if (type == typeof(DateTime))
                {
                    var typedValue = (DateTime)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            4,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    long milliseconds;
                    if (fieldType == FieldType.Date)
                    {
                        milliseconds = (typedValue.Date.Ticks / TimeSpan.TicksPerMillisecond);
                    }
                    else if (fieldType == FieldType.Time)
                    {
                        milliseconds = (typedValue.TimeOfDay.Ticks / TimeSpan.TicksPerMillisecond);
                    }
                    else
                    {
                        milliseconds = (typedValue.Ticks / TimeSpan.TicksPerMillisecond);
                    }
                    numericField.SetLongValue(milliseconds);
                    result.Add(numericField);
                }
                else if (type == typeof(DateTime?))
                {
                    var typedValue = (DateTime?)value;
                    field = numericField = new NumericField(documentFieldName,
                                                            4,
                                                            documentFieldAttribute.Stored.MapTo(), index);
                    long milliseconds;
                    if (fieldType == FieldType.Date)
                    {
                        milliseconds = typedValue.HasValue ? (typedValue.Value.Date.Ticks / TimeSpan.TicksPerMillisecond) : 0;
                    }
                    else if (fieldType == FieldType.Time)
                    {
                        milliseconds = typedValue.HasValue ? (typedValue.Value.TimeOfDay.Ticks / TimeSpan.TicksPerMillisecond) : 0;
                    }
                    else
                    {
                        milliseconds = typedValue.HasValue ? (typedValue.Value.Ticks / TimeSpan.TicksPerMillisecond) : 0;
                    }
                    numericField.SetLongValue(milliseconds);
                    result.Add(numericField);
                }
                else
                {
                    field = stringField = new Field(
                                              documentFieldName,
                                              (value != null) ? value.ToString() : string.Empty,
                                              documentFieldAttribute.Stored.MapTo(),
                                              documentFieldAttribute.Indexed.MapTo());
                    result.Add(stringField);
                }


                if (documentFieldAttribute.IsKey)
                {
                    StoredNotIndexed.Add(new Term(documentFieldName, value.ToString()));
                }
            } //~loop
            return result;
        }

        private Lucene.Net.Store.Directory CreateDirectory<TModel>(string indexedDocumentStoreName)
        {
            //Specify the index file location where the indexes are to be stored
            string indexFileLocation = Path.Combine(_configuration.IndexRootDirectory, indexedDocumentStoreName);

            //@"D:\tmp\Lucene.Net\Data\" + indexedDocumentStoreName;

            Lucene.Net.Store.Directory luceneDirectory = Lucene.Net.Store.FSDirectory.Open(indexFileLocation);
            return luceneDirectory;
        }



    }
}
