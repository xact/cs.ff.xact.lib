namespace XAct.Data.Search
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILuceneDocumentSearchManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the index root directory.
        /// </summary>
        /// <value>
        /// The index root directory.
        /// </value>
        string IndexRootDirectory { get; set; }

    }
}