namespace XAct.Data.Search
{
    using XAct.Services;

    /// <summary>
    /// A Lucene.NET specific implementation of the 
    /// <see cref="ILuceneDocumentSearchManagementServiceConfiguration"/>
    /// </summary>
    public class LuceneDocumentSearchManagementServiceConfiguration : ILuceneDocumentSearchManagementServiceConfiguration
    {
        /// <summary>
        /// The directory where indexes are created by this service.
        /// <para>
        /// Example: 'c:\Data\DocumentIndexes\'
        /// </para>
        /// </summary>
        public string IndexRootDirectory { get; set; }
    }
}