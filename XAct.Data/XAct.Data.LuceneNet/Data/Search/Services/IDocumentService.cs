namespace XAct.Data.Search
{
    using System.Collections.Generic;
    using XAct.Spikes.CodeFirst.Data.Search.Entities;

    /// <summary>
    /// A contract for a service to store Documents and retrieve them later.
    /// </summary>
    public interface IDocumentService : IHasXActLibService
    {
        /// <summary>
        /// Persist the given Models as Documents in the named Document store.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="indexedDocumentStoreName"></param>
        /// <param name="models"></param>
        /// <param name="appendToExistingIndex">Append to existing index, or start a new one.</param>
        /// <param name="createIfNonExistent">Create a new Document if non-existent in the Document store.</param>
        void Persist<TModel>(string indexedDocumentStoreName, IEnumerable<TModel> models, bool appendToExistingIndex = true, bool createIfNonExistent = true);


        /// <summary>
        /// Persist the given Models as Documents in the named Document store.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="indexedDocumentStoreName"></param>
        /// <param name="models"></param>
        void Persist<TModel>(string indexedDocumentStoreName, params TModel[] models);


        /// <summary>
        /// Search the named Document store for a Document matching the given <paramref name="searchString"/>.
        /// </summary>
        /// <param name="indexedDocumentStoreName"></param>
        /// <param name="searchString"></param>
        /// <param name="defaultFieldName"></param>
        /// <param name="hitFieldNamesToReturn"></param>
        /// <returns></returns>
        ScoredDocumentResult[] Search(string indexedDocumentStoreName, string searchString, string defaultFieldName, params string[] hitFieldNamesToReturn);


        /// <summary>
        /// Delete the Document with a <paramref name="keyFieldName"/> with a value matching the <paramref name="keyFieldValue"/>.
        /// </summary>
        /// <param name="indexedDocumentStoreName"></param>
        /// <param name="keyFieldName"></param>
        /// <param name="keyFieldValue"></param>
        void Delete(string indexedDocumentStoreName, string keyFieldName, string keyFieldValue);
    }
}