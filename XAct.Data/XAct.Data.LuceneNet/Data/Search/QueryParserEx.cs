﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Spikes.CodeFirst.Data.Search
{
    using Lucene.Net.Analysis;
    using Lucene.Net.Documents;
    using Lucene.Net.QueryParsers;
    using Lucene.Net.Search;
    using Lucene.Net.Util;

    /// <summary>
    /// An example extension to the default <see cref="QueryParser"/>
    /// </summary>
    public class QueryParserEx : QueryParser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParserEx"/> class.
        /// </summary>
        /// <param name="matchVersion">The match version.</param>
        /// <param name="f">The f.</param>
        /// <param name="a">A.</param>
        public QueryParserEx(Version matchVersion, string f, Analyzer a)
            : base(matchVersion, f, a)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParserEx"/> class.
        /// </summary>
        /// <param name="stream"></param>
        protected internal QueryParserEx(ICharStream stream)
            : base(stream)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParserEx"/> class.
        /// </summary>
        /// <param name="tm"></param>
        protected QueryParserEx(QueryParserTokenManager tm)
            : base(tm)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="queryText"></param>
        /// <returns></returns>
        protected override Query GetFieldQuery(string field, string queryText)
        {
            return base.GetFieldQuery(field, queryText);
        }

        /// <summary>
        /// Base implementation delegates to {@link #GetFieldQuery(String,String)}.
        /// This method may be overridden, for example, to return
        /// a SpanNearQuery instead of a PhraseQuery.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="queryText"></param>
        /// <param name="slop"></param>
        /// <returns></returns>
        protected override Query GetFieldQuery(string field, string queryText, int slop)
        {
            return base.GetFieldQuery(field, queryText, slop);
        }

        /// <summary>
        /// Factory method for generating a query (similar to
        /// {@link #getWildcardQuery}). Called when parser parses
        /// an input term token that has the fuzzy suffix (~) appended.
        /// </summary>
        /// <param name="field">Name of the field query will use.</param>
        /// <param name="termStr">Term token to use for building term for the query</param>
        /// <param name="minSimilarity"></param>
        /// <returns>
        /// Resulting {@link Query} built for the term
        /// </returns>
        protected override Query GetFuzzyQuery(string field, string termStr, float minSimilarity)
        {
            if (string.Equals(field, "Dob"))
            {
                
                return base.GetFuzzyQuery(field, termStr, minSimilarity);
            }
            return base.GetFuzzyQuery(field, termStr, minSimilarity);
        }

        /// <summary>
        /// Factory method for generating a query (similar to
        /// {@link #getWildcardQuery}). Called when parser parses an input term
        /// token that uses prefix notation; that is, contains a single '*' wildcard
        /// character as its last character. Since this is a special case
        /// of generic wildcard term, and such a query can be optimized easily,
        /// this usually results in a different query object.
        /// <p />
        /// Depending on settings, a prefix term may be lower-cased
        /// automatically. It will not go through the default Analyzer,
        /// however, since normal Analyzers are unlikely to work properly
        /// with wildcard templates.
        /// <p />
        /// Can be overridden by extending classes, to provide custom handling for
        /// wild card queries, which may be necessary due to missing analyzer calls.
        /// </summary>
        /// <param name="field">Name of the field query will use.</param>
        /// <param name="termStr">Term token to use for building term for the query
        /// (<b>without</b> trailing '*' character!)</param>
        /// <returns>
        /// Resulting {@link Query} built for the term
        /// </returns>
        protected override Query GetPrefixQuery(string field, string termStr)
        {
            return base.GetPrefixQuery(field, termStr);
        }

        /// <summary>
        /// Factory method for generating query, given a set of clauses.
        /// By default creates a boolean query composed of clauses passed in.
        /// Can be overridden by extending classes, to modify query being
        /// returned.
        /// </summary>
        /// <param name="clauses">List that contains {@link BooleanClause} instances
        /// to join.</param>
        /// <returns>
        /// Resulting {@link Query} object.
        /// </returns>
        protected override Query GetBooleanQuery(IList<BooleanClause> clauses)
        {
            return base.GetBooleanQuery(clauses);
        }

        /// <summary>
        /// Factory method for generating query, given a set of clauses.
        /// By default creates a boolean query composed of clauses passed in.
        /// Can be overridden by extending classes, to modify query being
        /// returned.
        /// </summary>
        /// <param name="clauses">List that contains {@link BooleanClause} instances
        /// to join.</param>
        /// <param name="disableCoord">true if coord scoring should be disabled.</param>
        /// <returns>
        /// Resulting {@link Query} object.
        /// </returns>
        protected override Query GetBooleanQuery(IList<BooleanClause> clauses, bool disableCoord)
        {
            return base.GetBooleanQuery(clauses, disableCoord);
        }

        /// <summary>
        /// Factory method for generating a query. Called when parser
        /// parses an input term token that contains one or more wildcard
        /// characters (? and *), but is not a prefix term token (one
        /// that has just a single * character at the end)
        /// <p />
        /// Depending on settings, prefix term may be lower-cased
        /// automatically. It will not go through the default Analyzer,
        /// however, since normal Analyzers are unlikely to work properly
        /// with wildcard templates.
        /// <p />
        /// Can be overridden by extending classes, to provide custom handling for
        /// wildcard queries, which may be necessary due to missing analyzer calls.
        /// </summary>
        /// <param name="field">Name of the field query will use.</param>
        /// <param name="termStr">Term token that contains one or more wild card
        /// characters (? or *), but is not simple prefix term</param>
        /// <returns>
        /// Resulting {@link Query} built for the term
        /// </returns>
        protected override Query GetWildcardQuery(string field, string termStr)
        {
            return base.GetWildcardQuery(field, termStr);
        }


        /// <summary>
        /// </summary>
        /// <param name="field"></param>
        /// <param name="part1"></param>
        /// <param name="part2"></param>
        /// <param name="inclusive"></param>
        /// <returns></returns>
        protected override Query GetRangeQuery(string field, string part1, string part2, bool inclusive)
        {

            if (string.Equals(field, "Dob"))
            {
                long? min = long.Parse(part1);
                long? max = long.Parse(part1);

                return NumericRangeQuery.NewLongRange(field, min, max, true, true);


            }

            return (TermRangeQuery)base.GetRangeQuery(field, part1, part2, inclusive);

        }


    }




}