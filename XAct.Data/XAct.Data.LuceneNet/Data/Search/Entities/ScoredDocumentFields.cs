﻿using System.Collections.Generic;

namespace XAct.Spikes.CodeFirst.Data.Search.Entities
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Result entity containing 
    /// </summary>
    [DataContract]
    public class ScoredDocumentResult
    {
        /// <summary>
        /// The Score given to the found document.
        /// <para>
        /// Note that the score is *not* normalized: http://wiki.apache.org/lucene-java/ScoresAsPercentages
        /// </para>
        /// </summary>
        [DataMember]
        public double Score { get; set; }

        /// <summary>
        /// The fields of the document found.
        /// <para>
        /// In many cases, the number of fields returned are small (just an Id)
        /// and are then used to do a direct db hit.
        /// </para>
        /// </summary>
        [DataMember]
        public ScoredDocumentFieldResult[] FieldValues { get; set; }
    }
}
