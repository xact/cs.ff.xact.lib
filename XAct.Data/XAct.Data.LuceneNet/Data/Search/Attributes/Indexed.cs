﻿namespace XAct.Data.Search
{
    /// <summary>
    /// An enumeration of the options of whether
    /// to Store the indexed data.
    /// <para>
    /// For an explanation, see <see cref="DocumentFieldAttribute.Indexed"/>.
    /// </para>
    /// </summary>
    public enum Indexed
    {
        /// <summary>
        /// No value has been given. This is an error state.
        /// <para>
        /// Value = 0.
        /// </para>
        /// </summary>
        Undefined=0,
        /// <summary>
        /// The data will not be Indexed, presumably just stored (eg: an 'Id' column)
        /// </summary>
        No=1,
        /// <summary>
        /// The data will be Indexed in one chunk (ie, the whole sentence, the whole word).
        /// </summary>
        Untokenized=2,
        /// <summary>
        /// The data will be tokenized (depends on the tokenizer used) into words, or even smaller tokens, then those will indexed.
        /// </summary>
        Tokenized=3
    }

    /// <summary>
    /// The Document FieldType.
    /// </summary>
    public enum FieldType
    {
        /// <summary>
        /// The field type has not been defined (an error state)
        /// </summary>
        Undefined=0,

        /// <summary>
        /// The field is a s string
        /// </summary>
        String=1,
        
        /// <summary>
        /// The field is numerical.
        /// </summary>
        Numerical=2,

        /// <summary>
        /// The field contains Date (note: stored as a Number, HHmmssfff)
        /// </summary>
        Time = 3,

        /// <summary>
        /// The field contains Date (note: stored as a Number, yyyyMMdd)
        /// </summary>
        Date=3,

        /// <summary>
        /// The field contains DateTime (note: stored as a Number yyyyMMddHHmmssfff)
        /// </summary>
        DateTime = 4,

    }
}