﻿namespace XAct.Data.Search
{
    /// <summary>
    /// An enumeration of the options of whether
    /// to Store the indexed data.
    /// <para>
    /// For an explanation, see <see cref="DocumentFieldAttribute.Stored"/>.
    /// </para>
    /// </summary>
    public enum Stored
    {
        /// <summary>
        /// Whether the field value should be stored or not has not been defined (an error state).
        /// </summary>
        Unspecified=0,

        /// <summary>
        /// The raw/original value should not be stored  (although it can be used for indexing purposes)
        /// via the value of <see cref="Indexed"/>.
        /// </summary>
        No=1,

        /// <summary>
        /// The raw/original value should be stored in order to be retrieved later.
        /// </summary>
        Yes=2, 
    }
}