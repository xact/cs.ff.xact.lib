﻿namespace XAct.Data.Search
{
    using System;

    /// <summary>
    /// Attribute applied to Model properties, in order to describe how to persist the property
    /// as a document Attribute/value.
    /// <code>
    /// <![CDATA[
    /// public class Student :IHasNames
    /// {
    ///     [DocumentFieldAttribute(Stored.Yes, Indexed.Untokenized, FieldType.Undefined,true)]
    ///     public int Id { get; set; }
    ///     [DocumentFieldAttribute(Stored.No, Indexed.Tokenized)]
    ///     public string FamilyName { get; set; }
    ///     [DocumentFieldAttribute(Stored.No, Indexed.Tokenized)]
    ///     public string GivenName1 { get; set; }
    ///     [DocumentFieldAttribute(Stored.No, Indexed.Tokenized)]
    ///     public string GivenName2 { get; set; }
    ///     [DocumentFieldAttribute(Stored.No, Indexed.Tokenized)]
    ///     public string GivenName3 { get; set; }
    ///     [DocumentFieldAttribute(Stored.Yes, Indexed.Untokenized)]
    ///     public DateTime? Dob { get; set; }
    ///     [DocumentFieldAttribute(Stored.Yes, Indexed.Untokenized)]
    ///     public int Val { get; set; }
    ///     public Collection<AltName> AltNames { get { return _altNames ?? (_altNames = new Collection<AltName>()); } }
    ///     //[LuceneFieldAttribute()]
    ///     private Collection<AltName> _altNames;
    /// }
    /// ]]>
    /// </code>
    /// It's important to understand that it's not common to apply these attributes
    /// to an application model
    /// directly -- usually the properties are transfered from an app model (eg: Student)
    /// to Document type (StudentDocument) which is a Projection of one or more app models (eg: Student + StudentAltName)
    /// whose properties are appropriately decorated
    /// with this attribute. It is the StudentDocument that is persisted, indexed, and therefore retrieved.
    /// From the StudentDocument, the Id is used to get back to the original Student object.
    /// </summary>
    public class DocumentFieldAttribute : System.Attribute
    { 
        /// <summary>
        /// The name of the persisted Document Field.
        /// By default, the Field name will be the same as
        /// the property this Attribute decorates -- unless 
        /// specifically set here.
        /// </summary>
        public string FieldName { get; private set; }


        /// <summary>
        /// This field is the record key to search against
        /// when updating/deleting a document in the document 
        /// store.
        /// </summary>
        public bool IsKey { get; private set; }


        /// <summary>
        /// Gets or sets whether the Field is used as a Search criteria.
        /// <para>
        /// A common example of when to not index a field is a Db record's Id -- 
        /// it needs to be Stored (in order to be retrieved), but doesn't need indexing,
        /// as one would query the Db directly if one had the Id.
        /// </para>
        /// <para>
        /// Alternatly, one does Index a column one is searching against (eg: FamilyName)
        /// -- but it often doesn't need to be <see cref="Stored"/>, as we're generally
        /// after just the <see cref="Stored"/> Id Field, which is subsequently used to retrieve
        /// the Db record via a call to the DataStore.
        /// </para>
        /// </summary>
        public Indexed Indexed { get; private set; }

        /// <summary>
        /// Gets or sets whether the field should be stored (or just <see cref="Indexed"/>)
        /// </summary>
        public Stored Stored { get; private set; }


        /// <summary>
        /// Gets the type of the field (string, bool, etc).
        /// </summary>
        /// <value>
        /// The type of the field.
        /// </value>
        public FieldType FieldType { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentFieldAttribute" /> class.
        /// </summary>
        /// <param name="fieldName">The alias.</param>
        /// <param name="stored">The stored.</param>
        /// <param name="indexed">The indexed.</param>
        /// <param name="fieldType">Type of the field.</param>
        /// <param name="isKey">if set to <c>true</c> [is key].</param>
        public DocumentFieldAttribute(string fieldName, Stored stored, Indexed indexed, FieldType fieldType = FieldType.Undefined, bool isKey = false)
            : this(stored, indexed, fieldType, isKey)
        {
            FieldName = fieldName;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentFieldAttribute" /> class.
        /// </summary>
        /// <param name="stored">The stored.</param>
        /// <param name="indexed">The indexed.</param>
        /// <param name="fieldType">Type of the field.</param>
        /// <param name="isKey">if set to <c>true</c> [is key].</param>
        /// <exception cref="System.ArgumentException">Can't mark a field as IsKey, if not Indexed.</exception>
        public DocumentFieldAttribute(Stored stored, Indexed indexed, FieldType fieldType=FieldType.Undefined, bool isKey=false)
        {
            Stored = stored;
            Indexed = indexed;
            FieldType = fieldType;
            IsKey = isKey;

            if ((IsKey) && (indexed == Indexed.No))
            {
                throw new ArgumentException("Can't mark a field as IsKey, if not Indexed.");
            }
        }
    }
}