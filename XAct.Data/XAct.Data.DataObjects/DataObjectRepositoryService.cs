﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using XAct.Diagnostics;

namespace XAct.Data.Entities
{
    public class DataObjectFactoryService
    {

#region Services
        private readonly ITracingService _tracingService;
#endregion

        #region Fields
        private readonly Dictionary<string, IDataObjectRepository> _dataObjectFactoryByName = 
            new Dictionary<string, IDataObjectRepository>(StringComparer.InvariantCultureIgnoreCase);

        private readonly Dictionary<Guid, IDataObjectRepository> _dataObjectFactoryById = 
            new Dictionary<Guid, IDataObjectRepository>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="dataObjectFactoryService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public DataObjectFactoryService(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }
        #endregion

        public void Register(IDataObjectRepository dataObjectFactory)
        {
            _dataObjectFactoryByName.Add(dataObjectFactory.Name, dataObjectFactory);
            _dataObjectFactoryById.Add(dataObjectFactory.Id, dataObjectFactory);

        }

        /// <summary>
        /// Gets the <see cref="XAct.Data.IDataObjectRepository"/> with the specified name (Case-Insensitive).
        /// </summary>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">An exception is raised if the key is not found.</exception>
        /// <value></value>
        public IDataObjectRepository this[string name]
        {
            get
            {
                return _dataObjectFactoryByName[name];
            }
        }

        /// <summary>
        /// Gets the <see cref="XAct.Data.IDataObjectRepository"/> with the specified Id.
        /// </summary>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">An exception is raised if the key is not found.</exception>
        /// <value></value>
        public IDataObjectRepository this[Guid id]
        {
            get
            {
                return _dataObjectFactoryById[id];
            }
        }

        #region Properties (Protected)




        #endregion


        #region Properties (Implementation of IDataObjectRepositoryCollection)

        /// <summary>
        /// Gets the number of dataObjectFactory elements in the collection.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get
            {
                Debug.Assert(_dataObjectFactoryByName.Count == _dataObjectFactoryById.Count);

                return _dataObjectFactoryById.Count;
            }
        }

        /// <summary>
        /// Determines whether the dataObjectFactory with specified typename exists.
        /// </summary>
        /// <param name="typeName">The name of the Type.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified typename]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(string typeName)
        {
            return _dataObjectFactoryByName.ContainsKey(typeName);
        }

        /// <summary>
        /// Determines whether the dataObjectFactory with the specified Id exists
        /// </summary>
        /// <param name="id">The Id.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified Id]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(Guid id)
        {
            return _dataObjectFactoryById.ContainsKey(id);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns an enumerator that iterates through the DbdataObjectFactoryProvider
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
        /// </returns>
        IEnumerable<IDataObjectRepository> GetAll()
        {
            return _dataObjectFactoryById.Values;
        }

        #endregion

    }
}
