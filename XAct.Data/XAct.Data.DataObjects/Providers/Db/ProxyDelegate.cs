#if (CE) || (PocketPC) || (pocketPC) || (WindowsCE)

using System;
using System.Data;
using System.Reflection;

namespace XAct.Data {

  #region Protected Methods - Delegates
  //http://www.codeproject.com/netcf/UsingMyXamlCF.asp
  public class ProxyDbDataObjectFactoryDelegate {
    object _Instance;
    MethodInfo _MethodInfo;
    object[] _MethodParams = new Object[1];

    public IDataObjectFactory Invoke(IDataRecord record) {
      _MethodParams[0] = record;
      return (IDataObjectFactory)_MethodInfo.Invoke(_Instance, _MethodParams);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:ProxyDbDataObjectFactoryDelegate"/> class.
    /// </summary>
    /// <param name="methodInfo">The method info.</param>
    /// <param name="eventTarget">The event target.</param>
    /// <param name="eventObject">The event object.</param>
    public ProxyDbDataObjectFactoryDelegate(MethodInfo methodInfo, object eventTarget) {
      _MethodInfo = methodInfo;
      _Instance = eventTarget;
    }
  }
  #endregion
}
#endif