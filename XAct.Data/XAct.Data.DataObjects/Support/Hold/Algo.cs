using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Generics {
  /// <summary>
  /// Class of Static Delegates and Methods for 
  /// working with IEnumerable collections.
  /// </summary>
  /// <remarks>
  /// </remarks>
  /// <internal>
  /// See: http://blog.dotnetwiki.org/PermaLink,guid,c773e395-d715-4313-90c2-3c34d1102b2f.aspx
  /// </internal>
  internal static class CollectionAlgorythms {

    #region Delegates

    /// <summary>
    /// Delegate signature for an action function (void return) taking no parameter.
    /// </summary>
    public delegate void Action();

    /// <summary>
    /// Delegate signature for an action function (void return) taking a parameter of generic type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="a"></param>
    public delegate void Action<T>(T a);

    /// <summary>
    /// Delegate signature for a Predicate (taking a T parameter and returning bool).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="a"></param>
    /// <returns></returns>
    public delegate bool Predicate<T>(T a);

    /// <summary>
    /// Delegate signature for a function taking a generic T parameter
    /// and returning a generic R parameter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="R"></typeparam>
    /// <param name="a"></param>
    /// <returns></returns>
    public delegate R Function<T, R>(T a);
    #endregion


    #region Methods
    /// <summary>
    /// Apply a function to each item in a collection.
    /// </summary>
    /// <remarks>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// List<int> result = new List<int>();
    /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    ///   ForEach<int>( myList, 
    ///                 delegate(int id) {
    ///                                   if (id>10){result.Add(++id);}}
    ///               );
    ///   foreach(int i in result){
    ///      Trace.WriteLine(i);
    ///   }
    /// ]]>
    /// This results in the following list:
    /// <c>{21,31,41,51,61,71,81,91,101}</c>.
    /// </code>
    /// </para>
    /// <para>
    /// Note that this method is very similar to <see cref="M:Filter"/>
    /// except that it takes only an <see cref="Action"/>, rather than a <c>Predicate</c>.
    /// </para>
    /// <para>
    /// Note that unlike <see cref="M:Map"/>, no collection is returned.
    /// </para>
    /// </remarks>
    /// <typeparam name="T">The Type of the collection items.</typeparam>
    /// <param name="collection">The collection to iterate through.</param>
    /// <param name="actionToApply">The function to apply to each element of the collection.</param>
    public static void ForEach<T>(IEnumerable<T> collection, Action<T> actionToApply) {
      foreach (T item in collection) {
        actionToApply(item);
      }
    }


    /// <summary>
    /// Retuen a new collection corresponding to the result of applying a function (f)
    /// to each item in the given collection.
    /// </summary>
    /// <remarks>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// MemberInfo[] members = this.GetType().GetMembers();
    /// foreach(string name in Map<MemberInfo,string>(
    ///                           members,
    ///                           delegate(MemberInfo memberInfo){return memberInfo.Name;}
    ///                           );
    ///                           ){
    ///               Trace.WriteLine(name);
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Note that the result collection will always be the same size as the input collection.
    /// </para>
    /// </remarks>
    /// <typeparam name="T">The Type of the collection items.</typeparam>
    /// <typeparam name="R">The Type of the return value of the function to apply.</typeparam>
    /// <param name="collection">The collection of elements to iterate through.</param>
    /// <param name="functionToApply">The function to apply to each item &lt;Type, ReturnType&gt;.</param>
    /// <returns></returns>
    public static IEnumerable<R> Map<T, R>(IEnumerable<T> collection, Function<T, R> functionToApply) {
      foreach (T item in collection) {
        yield return functionToApply(item);
      }
    }


    /// <summary>
    /// Filters the collection according to the given predicate.
    /// return only the items whose match with predicateToApply
    /// </summary>
    /// <remarks>
    /// <para>
    /// The following sample demonstrates the filtering of a collection
    /// for values by a function that checks for the values being 
    /// in a specified range:
    /// <code>
    /// <![CDATA[
    /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    /// IEnumerable<int> resultList = 
    ///   CollectionAlgorythms.Filter<int>(
    ///       myList,
    ///       delegate(int id) {return ((id > 10) && (id <100));}
    ///       );
    /// foreach (int i in resultList){
    ///   //Do Something...
    /// }
    /// ]]>
    /// </code>
    /// As the results are <c>yield</c>ed, one can write the same thing a 
    /// little more compactly as follows:
    /// <code>
    /// <![CDATA[
    /// foreach (int i in CollectionAlgorythms.Filter<int>(
    ///           myList,
    ///           delegate(int id) {return ((id > 10) && (id <100));}
    ///         ){
    ///   //Do Something...
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Note that the result collection size will always be &lt;= size as the input collection
    /// (the result will depend on the number of input items that pass the predicate).
    /// </para>
    /// </remarks>
    /// <typeparam name="T">The Type of the collection items.</typeparam>
    /// <param name="collection">The collection of elements to iterate through.</param>
    /// <param name="predicateToApply">The predicate method to test each collection item with.</param>
    /// <returns>The items that passed the predicate.</returns>
    public static IEnumerable<T> Filter<T>(IEnumerable<T> collection, Predicate<T> predicateToApply) {
      foreach (T item in collection) {
        if (predicateToApply(item)) {
          yield return item;
        }
      }
    }


    /// <summary>
    /// Determines whether the collection contains an item 
    /// which matches with the given Predicate.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The following simple sample demonstrates its use
    /// to see if a given list contains numbers within a given range:
    /// <code>
    /// <![CDATA[
    /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    /// bool found = Contains<int>(
    ///                           myList,
    ///                           delegate(int i){return ((i>10)&&(i<100));}
    ///                           );
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <typeparam name="T">The Type of the collection items.</typeparam>
    /// <param name="collection">The collection of elements to iterate through.</param>
    /// <param name="predicateToApply">The predicateToApply.</param>
    /// <returns>
    /// 	<c>true</c> if [contains] [the specified collection]; otherwise, <c>false</c>.
    /// </returns>
    public static bool Contains<T>(IEnumerable<T> collection, Predicate<T> predicateToApply) {
      foreach (T item in collection) {
        if (predicateToApply(item)) {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Finds an item in the collection which matches the specified predicate.
    /// Returns true if an item is matched, false otherwise.
    /// </summary>
    /// <remarks>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    /// int foundItem=-1;
    /// bool found = Contains<int>(
    ///                           myList,
    ///                           delegate(int i){return ((i>10)&&(i<100));}
    ///                           ref foundItem);
    /// ]]>
    /// </code>
    /// The result will be <c>20</c>.
    /// </para>
    /// </remarks>
    /// <typeparam name="T">The Type of the collection items.</typeparam>
    /// <param name="collection">The collection of elements to iterate through.</param>
    /// <param name="predicateToApply">The predicateToApply.</param>
    /// <param name="matchedItem">The matchedItem.</param>
    /// <returns></returns>
    public static bool Find<T>(IEnumerable<T> collection, Predicate<T> predicateToApply, ref T matchedItem) {
      foreach (T item in collection) {
        if (predicateToApply(item)) {
          matchedItem = item;
          return true;
        }
      }
      return false;
    }


    /// <summary>
    /// Convert a collection to a CSV string
    /// </summary>
    /// <remarks>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    /// ]]>
    /// </code>
    /// The result will be: <c>"{1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100}"</c>
    /// </para>
    /// <para>
    /// Note that the result is wrapped in '{' and '}'.
    /// </para>
    /// </remarks>
    /// <typeparam name="T">The Type of the collection items.</typeparam>
    /// <param name="collection">The collection of elements to iterate through.</param>
    /// <returns></returns>
    public static string CollectionToString<T>(IEnumerable<T> collection) {
      StringBuilder sb = new StringBuilder();
      sb.Append("{");
      foreach (T item in collection) {
        sb.Append(string.Format(" {0},", item.ToString()));
      }
      sb.Remove(sb.Length - 1, 1); // remove ending ','
      sb.Append("}");
      return sb.ToString();
    }
    #endregion

  } //Class:End
}//Namespace:End
