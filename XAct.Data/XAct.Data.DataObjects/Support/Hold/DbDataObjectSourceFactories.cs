/*

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Reflection;

namespace XAct.Data {


  /// <summary>
  /// Static class which manages Registered 
  /// DataObjectFactory Factories.
  /// </summary>
  public static class DbDataObjectFactoryFactories {


    #region Fields

    /// <summary>
    /// Dictionary of DbDataObjectFactoryFactory instances. 
    /// Note that a Factory may be registered under one or more names.
    /// </summary>
    private static Dictionary<string, DbDataObjectFactoryFactory> _Factories = new Dictionary<string, DbDataObjectFactoryFactory>(StringComparer.InvariantCultureIgnoreCase);

    #endregion


    #region Constructors

    /// <summary>
    /// Initializes the <see cref="DbDataObjectFactoryFactories"/> class.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Iterates through all classes of the current assembly 
    /// in order to find the DataObjectFactory _Factories
    /// (i.e. methods tagged with DataSourceFactoryAttribute).
    /// </para>
    /// </remarks>
    /// <internal>
    /// TODO: create a mechanism which permit to iterate classes on other assemblies
    /// (loaded dynamically) and register their DbDataObjectFactoryProvider
    /// (this should be easy).
    /// </internal>
    static DbDataObjectFactoryFactories() {
      //Trace.WriteLine("DbDataObjectFactoryFactories static constructor");
      //Get the current assembly:
      Assembly assembly = Assembly.GetAssembly(typeof(DbDataObjectFactoryFactories));
      //And register all Data Factories within it.
      //In other words, register the 'DbDataObjectFactory.createInstance' method
      //under both '' (default), and 'db':
      RegisterDataSourceFactories(assembly);
    }
    #endregion

    #region Public Static Methods
    /// <summary>
    /// Get the DataObjectFactory factory associate with the given identifier (Case-Insenstive).
    /// </summary>
    /// <param name="dataSourceFactoryName">Name of dataSourceFactory</param>
    /// <returns>Returns the DbDataObjectFactoryFactory, or null if none exists.</returns>
    public static DbDataObjectFactoryFactory GetFactory(string dataSourceFactoryName) {
      if (!_Factories.ContainsKey(dataSourceFactoryName)) {
        return null;
      }
      return _Factories[dataSourceFactoryName];
    }
    #endregion


    #region Public Static Methods - Registration

    /// <summary>
    /// Registers all DbDataObjectFactoryFactory methods in the given assembly.
    /// </summary>
    /// <param name="assembly">The assembly.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the passed argument is null.</exception>
    public static void RegisterDataSourceFactories(Assembly assembly) {
      if (assembly == null) {
        throw new System.ArgumentNullException("assembly");
      }
      //Get all types in this assembly:
      Type[] types = assembly.GetTypes();

      //Loop through each type:
      foreach (Type type in types) {

        //Loop through each method of each Type:
        foreach (MethodInfo methodInfo in type.GetMethods()) {

          //In order to find methods decorated with 'DataSourceFactoryAttribute':
          foreach (DbDataSourceFactoryAttribute attribute in methodInfo.GetCustomAttributes(typeof(DbDataSourceFactoryAttribute), false)) {

            //Found one:
            //Register it as an dataObjectFactory creator:

            foreach (string factoryName in attribute.RegistrationNames) {
              RegisterDataSourceFactory(factoryName, (DbDataObjectFactoryFactory)Delegate.CreateDelegate(typeof(DbDataObjectFactoryFactory), methodInfo));
            }
          }
        }
      }
    }

    /// <summary>
    /// Registers the DataObjectFactory factory (or dataSourceFactory) MethodInfo identified by
    /// the dataSourceFactoryName identifier.
    /// </summary>
    /// <param name="dataSourceFactoryName">Name of the factory.</param>
    /// <param name="methodInfo">The methodInfo.</param>
    public static void RegisterDataSourceFactory(string dataSourceFactoryName, MethodInfo methodInfo) {

      if (dataSourceFactoryName == null) {
        //BUT do not trigger error is '':
        throw new System.ArgumentNullException("dataSourceFactoryName");
      }
      if (methodInfo == null) {
        throw new System.ArgumentNullException("methodInfo");
      }

      // Debug.Print("register ({0} : {1})", dataSourceFactoryName, methodInfo);

      RegisterDataSourceFactory(dataSourceFactoryName,
                      delegate(IDataRecord record) {

                        Object[] param = new Object[1];
                        param[0] = record;
                        //Debug.Print("calling DataObjectFactory dataSourceFactory : " + methodInfo);
                        return (IDataObjectFactory)methodInfo.Invoke(null, param);
                      });
    }


    /// <summary>
    /// register the DbDataObjectFactoryFactory Data Source factory by its identifier (dataSourceFactoryName)
    /// </summary>
    /// <param name="dataSourceFactoryName">Name to register the DbDataObjectFactoryFactory under.</param>
    /// <param name="dataSourceFactory">The DbDataObjectFactoryFactory to register.</param>
    /// <returns>The number of DbDataObjectFactoryFactory instances currently registered.</returns>
    /// <exception cref="ArgumentException">An exception is raised if a DataObjectFactory by that name has already been registered.</exception>
    public static int RegisterDataSourceFactory(string dataSourceFactoryName, DbDataObjectFactoryFactory dataSourceFactory) {
      if (dataSourceFactoryName == null) {
        //BUT do not trigger error is '':
        throw new System.ArgumentNullException("dataSourceFactoryName");
      }
      if (dataSourceFactory == null) {
        throw new System.ArgumentNullException("dataSourceFactory");
      }

      if (_Factories.ContainsKey(dataSourceFactoryName)) {
        throw new ArgumentException(
          string.Format(
          "A dataSourceFactory by that name ('{0}') has already been registered.",
          dataSourceFactoryName));
      }

      Trace.TraceInformation("registering DataObjectFactory factory for '{0}'", dataSourceFactoryName);
      _Factories[dataSourceFactoryName] = dataSourceFactory;
      return _Factories.Count;
    }

    #endregion
  }//Class:End
}//Namespace:End
*/