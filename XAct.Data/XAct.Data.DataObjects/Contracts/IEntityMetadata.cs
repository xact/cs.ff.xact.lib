﻿using System.Collections.Generic;

namespace XAct.Data.Entities
{
    /// <summary>
    /// The contract for Metadata that describes entities.
    /// </summary>
    /// <remarks>
    /// I'd like to think that DataAnnotations (introduced in .NET 3.5)
    /// could do it all...but they can't.
    /// <para>
    /// For one, they have to work with concrete classes. 
    /// An object such as a <see cref="IDataObject"/>
    /// would not be possible without some kind of schema/metadata
    /// to validate its properties.
    /// </para>
    /// </remarks>
    public interface IEntityMetadata 
    {
        /// <summary>
        /// Unique Name of Entity (eg: AApp.AModule.Contact)
        /// <para>
        /// The Name needs to include Namespace to avoid conflict among 3rd parties.
        /// </para>
        /// <para>
        /// Without it being unique, we would have to fallback to using a Guid Id, 
        /// which is a lot less intuitive.
        /// </para>
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Enumerable list of the <see cref="IEntityPropertyMetadata"/>
        /// describing the entity's properties.
        /// </summary>
        IEnumerable<IEntityPropertyMetadata> Properties { get; }

        /// <summary>
        /// Registers the property metadata.
        /// <para>
        /// Used to build up the inner dictionary (eg from an xml file 
        /// or other, describing the entity), 
        /// before the inner dictionary is made into a readonly one.
        /// </para>
        /// </summary>
        /// <param name="propertyMetadata">The property metadata.</param>
        void AddPropertyMetadata(IEntityPropertyMetadata propertyMetadata);
        
        /// <summary>
        /// Get a Property's Metadata.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="throwExceptionIfNotFound"></param>
        /// <returns></returns>
        IEntityPropertyMetadata GetPropertyMetadataByName(string propertyName, bool throwExceptionIfNotFound =true);
    }
}
