﻿using System;

namespace XAct.Data.Entities
{
    /// <summary>
    /// A contract for an extenion of
    /// <see cref="IEntityPropertyMetadata"/>
    /// in order to describe an entity's property
    /// suitable for storage purposes.
    /// </summary>
    /// <remarks>
    /// Intendede to be implemented alongside <see cref="IIsReadOnly"/>
    /// </remarks>
    public interface IEntityPropertyStorageMetadata
    {
        bool IsIndex { get; set; }
        bool IsIdentity { get; set; } 
        Type StorageDataType {get;set;}
    }
}