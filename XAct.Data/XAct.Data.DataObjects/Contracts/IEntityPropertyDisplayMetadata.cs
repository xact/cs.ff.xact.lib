﻿namespace XAct.Data.Entities
{
    /// <summary>
    /// A contract for an extenion of
    /// <see cref="IEntityPropertyMetadata"/>
    /// in order to describe an entity's property
    /// suitable for display purposes.
    /// </summary>
    /// <remarks>
    /// Intendede to be implemented alongside <see cref="IIsReadOnly"/>
    /// </remarks>
    public interface IEntityPropertyDisplayMetadata
    {
        /// <summary>An optional Resource class Type name.</summary>
        string Resource { get; set; }
        /// <summary>The text -- or if Resource is specified, the Key to the resource text -- to use for a Form Label.</summary>
        string Label { get; set; }
        /// <summary>The watermark -- or if Resource is specified, the Key to the resource text -- to use for a Form Input field Watermark.</summary>
        string Hint { get; set; }
        /// <summary>The description -- or if Resource is specified, the Key to the resource text -- to use for a Form Input field Description.</summary>
        string Description { get; set; }
        /// <summary>A boolean flag indicating whether the form is required or not.</summary>
        bool Required { get; set; }
    }
}
