
using System.Xml.Serialization;

namespace XAct.Data.Entities {
  
  /// <summary>
  /// Interface for DataObjects whose properties can be accessed 
  /// via this[get;set;]
  /// </summary>
  public interface IDataObject : IXmlSerializable {

    /// <summary>
      /// Gets the IDataObjectMetadata that describes this dataObject.
      /// <para>
      /// Implement explictly.
      /// </para>
    /// </summary>
    /// <value>The schema.</value>
    IEntityMetadata Metadata { get; }

    /// <summary>
    /// Gets or sets the <see cref="System.Object"/> value
    /// associated with the specified field name.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <value>The value.</value>
    object this[string fieldName] { get; set; }

  }//Class:End
}//Namespace:End

