﻿

using System;

namespace XAct.Data.Entities
{
    /// <summary>
    /// A contract to describe an entity's properties.
    /// </summary>
    /// <internal>
    /// The notion of building a metadata schema description
    /// of an entity  became necessary -- rather than using 
    /// Attributes and DataAnnotations, for example -- to 
    /// handle <see cref="IDataObject"/>, which does not have
    /// properties -- but has 
    /// </internal>
    public interface IEntityPropertyMetadata
    {
        /// <remarks>
        /// Intendede to be implemented alongside <see cref="IIsReadOnly"/>
        /// </remarks>

        /// <summary>
        /// The Name of the DataObject's Property.
        /// </summary>
        string Name { get; }


        Type Type { get; }

        /// <summary>
        /// Storage specific details about the object to store.
        /// </summary>
        IEntityPropertyStorageMetadata Storage { get; }


        IEntityPropertyDisplayMetadata Display { get; }
    }
}
