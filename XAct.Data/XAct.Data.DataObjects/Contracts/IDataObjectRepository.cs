using System;
using System.Xml;
using XAct.Data.Entities;


namespace XAct.Data {



  /// <summary>
  /// Interface that a single DataObjectRepository must implement.
	/// <para>
	/// A DataObjectRepository creates, updates, and deletes some kind of DataObject.
	/// </para>
  /// </summary>
  /// <internal>
  /// For now this is minimal
  /// we should probably enhance it with
  /// methods as CanCreate|CanUpdate etc. for exemple
  /// </internal>
  public interface IDataObjectRepository: IRepository<IDataObject> {

   


    #region Properties

    /// <summary>
    /// Id of this DataObjectRepository (must be unique).
    /// Eg: 1=Contacts, 2=Notes, etc.
    /// </summary>
    /// <value>The Id.</value>
    Guid Id { get; }

    /// <summary>
    /// The name of this DataObjectRepository (must be unique).
    /// Eg: 'Contacts'.
    /// </summary>
    string Name { get; }


    /// <summary>
    /// Gets the <see cref="IEntityMetadata"/> associated 
    /// with this DataObjectRepository.
    /// </summary>
    /// <value>The schema.</value>
    IEntityMetadata DataObjectMetadata { get; }
    #endregion


    /// <summary>
    /// Create a new dataObject instance (in memory only).
    /// Note that this is only an *in-memory* instance only: to persist it, 
    /// <see cref="M:StoreNewDataObject"/> must be called.
    /// </summary>
    /// <remarks>
    /// <para>
    /// </para>
    /// </remarks>
    /// <returns>A new empty dataObject</returns>
    object Create();

      /// <summary>
    /// Loads the dataObject in mem from an Xml description.
    /// </summary>
    /// <param name="node">The node.</param>
    /// <returns></returns>
    object Load(XmlNode node);

    /// <summary>
    /// Loads the dataObject in mem from an Xml description.
    /// </summary>
    /// <param name="reader">The reader.</param>
    /// <returns></returns>
    object Load(XmlReader reader);



  
  }//Class:End
}//Namespace:End
