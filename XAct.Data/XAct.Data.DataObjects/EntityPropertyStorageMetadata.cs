﻿using System;

namespace XAct.Data.Entities
{
    public class EntityPropertyStorageMetadata :IEntityPropertyStorageMetadata, IIsReadOnly
    {


        /// <summary>
        /// Gets a value indicating whether the properties of this instance are read only.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is read only; otherwise, <c>false</c>.
        /// </value>
        public bool IsReadOnly
        {
            set
            {
                if ((value == false) && (_isReadOnly))
                {
                    throw new ArgumentException("Once set, cannot reset object back to IsReadOnly=false.");
                }
                _isReadOnly = value;
            }
            get
            {
                return _isReadOnly;
            }
        }
        private bool _isReadOnly;



        public bool IsIndex 
        {
            get
            {
                return _IsIndex;
            }
            set
            {
                CheckReadOnlyStatus();
                _isIndex = value;
            }
        }
        private bool _isIndex;

        public bool IsIdentity
        {
            get
            {
                return _isIdentity;
            }
            set
            {
                CheckReadOnlyStatus();
                _isIdentity = value;
            }
        }
        private bool _isIdentity;

        public Type StorageDataType
        {
            get
            {
                return _storageDataType;
            }
            set
            {
                CheckReadOnlyStatus();
                _storageDataType = value;
            }
        }
        protected Type _storageDataType;

        private void CheckReadOnlyStatus()
        {
            if (_isReadOnly) { throw new ArgumentException("IsReadOnly is set to true."); }
        }
    }
}
