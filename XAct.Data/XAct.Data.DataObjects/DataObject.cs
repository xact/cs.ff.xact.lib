using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using XAct.Collections;


namespace XAct.Data.Entities {

  /// <summary>
  /// DataObject represents an object.
  /// <para>
  /// Basically it is a Dictionary&lt;string, object&gt;
  /// where the string key (i.e. column name for a db record)
  /// are known in the backing <see cref="IEntityMetadata"/>, 
  /// as well as the type of
  /// the associated object.
    /// </para>
    /// </summary>
  /// <remarks>
  /// <para>
  /// Notice that it has no reference back to its vertex.
  /// </para>
  /// </remarks>
  public partial class DataObject : IDataObject {


      private readonly IDataObjectRepository _dataObjectRepository;
      private readonly DirtyDictionary<string, object> _propertyValues =
        new DirtyDictionary<string, object>(); //StringComparer.InvariantCultureIgnoreCase

    #region Implementation of IDataObject

      /// <summary>
    /// Gets or sets the value (object)
    /// associated with the specified field (string).
    /// </summary>
    /// <internal>
    /// TODO: do some type checking using related schema.
    /// </internal>
    /// <value></value>
    public object this[string fieldName] {
      get {
        return _propertyValues[fieldName];
      }
      set {
        //Get the propertyschema by that name:
        IEntityPropertyMetadata dataObjectPropertyMetadata = this.Metadata.Properties[fieldName];
        
          if (dataObjectPropertyMetadata == null) {
          throw new System.ArgumentException(
            string.Format(
            "Cannot set Property '{0}' if no underlying PropertyType can be found.",
            fieldName));
        }

        /*
        if ((value != null) && (value.GetType() != dataObjectPropertyMetadata.Type)){
          throw new System.ArgumentException(
            string.Format(
            "::Cannot set Property '{0}', of Type '{1}', with value of Type '{2}'",
            fieldName,
            dataObjectPropertyMetadata.Type,
            value.GetType()));
        }
         */
        //This allows us to know the Type that the property should be in...
        //So if we are receiving a String...convert to eg, a Guid:
        _propertyValues[fieldName] = Types.ConvertTo(value, dataObjectPropertyMetadata.Type);
      }
    }


    /// <summary>
    /// Gets the schema that describes this DataObject.
    /// </summary>
    /// <value>The schema.</value>
    public IEntityMetadata Metadata {
      get {
        return _dataObjectRepository.DataObjectMetadata;
      }
    }

    /// <summary>
    /// Gets whether any value in the dictionary has been updated.
    /// Use <see cref="ResetDirty"/> to reset the flag to <c>false</c>.
    /// </summary>
    public bool IsDirty {
      get {
        return _propertyValues.IsDirty;
      }
    }

    #endregion


    #region Constructors
    /// <summary>
    /// Create a new empty dataObject for the specified dataObjectRepository.
		/// <para>
		/// Marked as <c>SyncStatus.CreationPending</c>.
		/// </para>
    /// </summary>
    /// <param name="dataObjectRepository">The dataObjectRepository.</param>
    /// <exception cref="ArgumentNullException">An exception is raised if the dataObjectRepository is null.</exception>
    public DataObject(IDataObjectRepository dataObjectRepository) {

        //Check Args:
      if (dataObjectRepository == null) {
        throw new System.ArgumentNullException("dataObjectRepository");
      }

      //Initialize Properties:
      _dataObjectRepository = dataObjectRepository;


      //Build and Clear Fields:
      foreach (IEntityPropertyMetadata dataObjectPropertyMetadata in Metadata.Properties) {
        _propertyValues[dataObjectPropertyMetadata.Name] = null;
      }

			XPDOTracker.XPDOTrackingVars trackingVars =
				XPDOTracker.Register(this, this.Metadata, true);

			trackingVars.MergeStatus(XPDOTracker.SyncStatus.CreationPending);

    }

    #endregion



    #region Public methods
    /// <summary>
    /// Resets the <see cref="IsDirty"/> flag on all items in the Fields DirtyDictionary.
    /// <para>
    /// A potential use of this method is after instantiating a Business Object
    /// and rehydrating it from a Db record. Reseting the flag states that
    /// nothing has changed since last write to Db.
    /// </para>
    /// </summary>
    public void ResetDirty() {
      _propertyValues.ResetDirty();
    }


    /// <summary>
    /// This property is reserved,
    /// apply the <see cref="T:System.Xml.Serialization.SchemaXmlProviderAttribute"></see> to the
    /// class instead.
    /// </summary>
    /// <returns>
    /// An <see cref="T:System.Xml.Schema.XmlSchema"></see> that describes the XML representation of the object that is produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)"></see> method and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)"></see> method.
    /// </returns>
    public XmlSchema GetSchema() {
      return null;
    }


    /// <summary>
    /// Return a Xml representation of this dataobject.
    /// </summary>
    /// <returns>a string which contains xml.</returns>
    public string ToXml() {
      StringBuilder sb = new StringBuilder();
      using (XmlWriter writer = XmlWriter.Create(sb)) {
        WriteXml(writer);
      }
      return sb.ToString();
    }


    /// <summary>
    /// Converts an object into its XML representation.
    /// </summary>
    /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"></see> stream to which the object is serialized.</param>
    public void WriteXml(XmlWriter writer) {

      writer.WriteStartElement("dataObject");
      writer.WriteAttributeString("type", dataObjectRepository.Name);
			writer.WriteAttributeString("id", Types.ConvertTo<object,string>(Metadata.GetPrimaryKeyValue(this)));

      //OLIVIER: Is this essential?
      //writer.WriteAttributeString("created", DateCreated.ToString());
      //writer.WriteAttributeString("edited", DateEdited.ToString());

      foreach (IEntityPropertyMetadata dataObjectPropertyMetadata in Metadata.Properties) {

        writer.WriteStartElement("field");
        writer.WriteAttributeString("name", dataObjectPropertyMetadata.Name);
        writer.WriteAttributeString("type", dataObjectPropertyMetadata.Type.ToString());

        RelationshipSchema parentRelationship = dataObjectPropertyMetadata.ParentRelationship;
        if (parentRelationship != null) {
          writer.WriteAttributeString("fkInfo",
            string.Format(
            "{0}:{1}:{2}",
            parentRelationship.ParentSchema.DataStoreName,
            parentRelationship.ParentKeyPropertySchema.Db.Name,
            parentRelationship.ParentDataPropertySchema.Db.Name));
        }

        writer.WriteStartElement("value");
        object val = this[dataObjectPropertyMetadata.Name];

        if (val != null) {
          XmlSerializer valSerializer = new XmlSerializer(val.GetType());
          valSerializer.Serialize(writer, val);
        }
        else {
          writer.WriteAttributeString("empty", "true");
        }
        writer.WriteEndElement(); // value
        writer.WriteEndElement(); //field
      }

      writer.WriteEndElement();//dataObject
    }


    /// <summary>
    /// Generates an object from its XML representation.
    /// </summary>
    /// <param name="reader">The <see cref="T:System.Xml.XmlReader"></see> stream from which the object is deserialized.</param>
    public void ReadXml(XmlReader reader) {
      System.Diagnostics.Debug.Assert(true, "this isn't clear! (when to call reader.Read???)");

      reader.MoveToContent();

      _propertyValues.Clear();

      string dataObjectRepositoryName = reader.GetAttribute("type");
      if (dataObjectRepositoryName == null) {
        throw new XmlException(String.Format("DataObject.ReadXml() : 'type' attribute is missing ({0} element)",
          reader.LocalName));
      }
      _dataObjectRepository = dataObjectRepositoryManager.Instance[dataObjectRepositoryName];
      //_Id = reader.GetAttribute("id");

      //OLIVIER: Is this essential?
      //_DateCreated = DateTime.Parse(reader.GetAttribute("created"));
      //_DateEdited = DateTime.Parse(reader.GetAttribute("edited"));

      reader.Read();
      while (reader.NodeType != XmlNodeType.EndElement) {

        if (!reader.IsStartElement("field")) {
          throw new XmlException("dataobjec readXML : malformed XML data");
        }

        string name = reader.GetAttribute("name");
        if (String.IsNullOrEmpty(name)) {
          throw new XmlException("DataObject:readXML: 'name' attribute is missing");
        }

        IEntityPropertyMetadata dataObjectPropertyMetadata = Metadata.Properties[name];
        Type valueType = dataObjectPropertyMetadata.Type;
        XmlSerializer valueSerializer = new XmlSerializer(valueType);

        reader.ReadToDescendant("value");
        string empty = reader.GetAttribute("empty");
        if (empty != null || empty == "true") {
          reader.Read();
          _propertyValues[name] = null;
        }
        else {
          reader.ReadStartElement();
          object value = valueSerializer.Deserialize(reader);
          _propertyValues[name] = value;
          reader.ReadEndElement(); // value???
        }

        reader.ReadEndElement();//field

        reader.MoveToContent();
      }
      reader.ReadEndElement();
    }

    #endregion



    #region Method Overrides

    /// <summary>
    /// Returns a <see cref="T:System.String"></see> that represents
    /// the current <see cref="T:System.Object"></see>.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
    /// </returns>
    public override string ToString() {

      StringBuilder txt = new StringBuilder();
      txt.Append(string.Format("{{{0}: ", this.dataObjectRepository.Name));
      foreach (KeyValuePair<string, object> P in _propertyValues) {
        txt.Append(string.Format("{0}={1}, ", P.Key, P.Value));
      }
      txt.Append("}");
      return txt.ToString();
    }

    #endregion




  }//Class:End
}//Namespace:End

