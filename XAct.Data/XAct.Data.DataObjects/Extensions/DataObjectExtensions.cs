﻿
using System;
using System.Data;
using XAct.Data;
using XAct.Data.Entities;

public static class DataObjectExtensions
{
    public static IEntityPropertyMetadata GetPrimaryKeyPropertyMetadata(this DataObject dataObject)
    {
        return null;
    }

    public static string GetPrimaryKeySerializedValue(this DataObject dataObject)
    {
        return null;
    }

    /// <summary>
    /// Create a new DataRecord from a DB row.
    /// </summary>
    /// <param name="dataObject">The data object.</param>
    /// <param name="dataRecord">The dataRecord.</param>
    /// <param name="dataObjectRepository">The SRC.</param>
    /// <param name="offset">The offset.</param>
    /// <exception cref="ArgumentNullException">An exception is raised if the dataRecord is null.</exception>
    ///   
    /// <exception cref="ArgumentNullException">An exception is raised if the dataObjectRepository is null.</exception>
    public static void DataObject(this IDataObject dataObject, IDataRecord dataRecord, IDataObjectRepository dataObjectRepository, int offset)
    {
        //Check Args:
        if (dataRecord == null)
        {
            throw new System.ArgumentNullException("dataRecord");
        }

        if (dataObjectRepository == null)
        {
            throw new System.ArgumentNullException("dataObjectRepository");
        }

        //Save to local fields:
        _dataObjectRepository = dataObjectRepository;

        //NB: offset is the column index of the record:
        //so increment, *after* you have picked up value:

        XPDOTracker.XPDOTrackingVars trackingVars =
          XPDOTracker.Register(this, this.Schema, true);

        bool hasId = false;

        //The first fields in the record are coming from the JOIN
        //to the meta table:






        foreach (IEntityPropertyMetadata dataObjectPropertyMetadata in dataObject.Metadata.Properties)
        {
            //Debug.Print("processing with {0}", dataObjectPropertyMetadata);
            //Get Val from DB:
            object val = dataRecord.GetValue(offset);

            dataObject[dataObjectPropertyMetadata.Name] = val.ConvertTo(dataObjectPropertyMetadata.Type);

            hasId |= dataObjectPropertyMetadata.IsKey();


            /*
            if (dataObjectPropertyMetadata.Equals(Schema.PrimaryKey)) { // got an ID
                if (dataObjectPropertyMetadata.Type == typeof(Guid)) {
                    //BUGFIX: Convert to String because some IDbDataParameter (Sqlite...) garble the output (maybe converting binary to string?).
                    _Id = ((Guid)_Fields[dataObjectPropertyMetadata.Name]).ToString(_GuidFormatCode);//OK
                }
                else {
                    _Id = (string)_Fields[dataObjectPropertyMetadata.Name];
                }
                hasId = true;
            }
             */
            ++offset;
        }


        if (!hasId)
        {
            throw new Exception(String.Format("DataRecord '{0}': no ID", dataObject.Repository.Name));
        }

        trackingVars.TrackingTableRecordFound = true; //!dataRecord.IsDBNull(offset);


        //For all the fields that are not defined in the schema proper,
        //but are in this db record, such as FK values, add at the end:
        for (int i = offset; i < dataRecord.FieldCount; i++)
        {
            string columnName = dataRecord.GetName(i);

            dataObject[columnName] = dataRecord[i];
        }

    }
}



