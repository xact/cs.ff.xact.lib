﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using XAct.Data.Entities;

/// <summary>
    /// Extensions to the Object
    /// </summary>
    public static class ObjectExtensions
    {

        private static IEntityPropertyMetadata SerializePropertyDisplayParameters<TDataObject>(this TDataObject dataObject, PropertyInfo propertyInfo, bool embedValues, bool embedAnyParentEntityValuesInDropDowns)
        {
            IEntityPropertyMetadata dataObjectPropertyMetadata = new DataObjectPropertyMetadata();

        }

        /// <summary>
        /// Serialize some Entity's Property.
        /// </summary>
        /// <typeparam name="TDataObject">The type of the data object.</typeparam>
        /// <param name="dataObject">The data object.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="propertyInfo">The property info.</param>
        /// <param name="embedValues">if set to <c>true</c> [embed values].</param>
        /// <param name="embedAnyParentEntityValuesInDropDowns">if set to <c>true</c> [embed any parent entity values in drop downs].</param>
        private static void SerializePropertyDisplayParameters<TDataObject>(this TDataObject dataObject, Stream stream, PropertyInfo propertyInfo, bool embedValues, bool embedAnyParentEntityValuesInDropDowns)
        {


            using (XmlWriter xmlWriter = XmlWriter.Create(stream))
            {
                //What type of control should we use to display the control?
                SerializeFormControlHints<TDataObject>(dataObject, xmlWriter, propertyInfo);
                //What about the label? Watermark? Description?
                SerializeFormDisplayHints<TDataObject>(dataObject, xmlWriter, propertyInfo);
                //Some validation...
                SerializeFormControlValidation<TDataObject>(dataObject, xmlWriter, propertyInfo);

                //Ah...If it is a FK, we need to embed some info about the Parent Entity.
                //This would allow JS or other solution to go get values.
                FKSummary fkSummary;
                SerializeFormControlFKInfo<TDataObject>(dataObject, xmlWriter, propertyInfo, out fkSummary);
                //We embed the value in its own tag, always, so that complex properties can be 
                //handled the same way as simple ints, etc:
                SerializeFormControlValue<TDataObject>(dataObject, xmlWriter, propertyInfo, (fkSummary!=null));

                //So...at this point, we have:
                // <dataObject type="" id="">
                //    <field name="" type=""
                //         label="" hidden="" enabled="" inputType="" ....>
                //      <value>myVal</value>
                //    </field>
                //    ...
                //  </dataObject>

                //There is one exception to this...
                //When the thing is a drop down, it could look like:
                //
                //So...at this point, we have:
                // <dataObject type="" id="">
                //    <field name="" type=""
                //         label="" hidden="" enabled="" inputType="" ....>
                //      <value>1</value>
                //      <options>
                //        <option></option>
                //      </options>
                //    </field>
                //    ...
                //  </dataObject>
                //


                SerializParentRelationshipValues<TDataObject>(dataObject, xmlWriter, propertyInfo, fkSummary,null, embedAnyParentEntityValuesInDropDowns);
            }









        }

        /// <summary>
        /// </summary>
        /// <remarks>
        /// When serializing Property values to embed as xml, they can be formatted first.
        /// </remarks>
        /// <param name="propertyValue"></param>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        private static string FormatPropertyValueAsString(this object propertyValue, PropertyInfo propertyInfo)
        {

            DisplayFormatAttribute displayFormatAttribute = propertyInfo.GetAttribute<DisplayFormatAttribute>();

            if (displayFormatAttribute == null)
            {
                return propertyValue.ConvertTo<string>();
            }

            if ((displayFormatAttribute.ConvertEmptyStringToNull) && (propertyValue is string) && ((string)propertyValue).IsNullOrEmpty())
            {
                propertyValue = null;
            }

            if ((propertyValue == null) && (!string.IsNullOrEmpty(displayFormatAttribute.NullDisplayText)))
            {
                return displayFormatAttribute.NullDisplayText;
            }

            if (string.IsNullOrEmpty(displayFormatAttribute.DataFormatString))
            {
                return propertyValue.ConvertTo<string>();
            }

            return string.Format(displayFormatAttribute.DataFormatString, propertyValue);

        }

        private static TAttributeType GetMetaDataAttribute<TDataObject, TAttributeType>(this TDataObject dataObject, PropertyInfo dataObjectPropertyInfo) where TAttributeType : Attribute
        {
            PropertyInfo metaDataTypePropertyInfo;

            Type metaDataClassType = dataObject.GetMetaDataClass();

            if (metaDataClassType != null)
            {
                metaDataTypePropertyInfo =
                    metaDataClassType.GetProperty(dataObjectPropertyInfo.Name,
                                             BindingFlags.Instance |
                                             BindingFlags.Public |
                                             BindingFlags.IgnoreCase);

                if (metaDataTypePropertyInfo != null)
                {
                    TAttributeType result = metaDataTypePropertyInfo.GetAttribute<TAttributeType>(true);
                    if (result != null)
                    {
                        return result;
                    }
                }
            }

            //Fall back to looking for attributes on base class definition:
            return dataObjectPropertyInfo.GetAttribute<TAttributeType>(true);
        }

        private static Type GetMetaDataClass<TDataObject>(this TDataObject dataObject)
        {
            Type dataObjectType = typeof(TDataObject);

            MetadataTypeAttribute metadataTypeAttribute = dataObjectType.GetAttribute<MetadataTypeAttribute>();

            Type metaDataType = (metadataTypeAttribute != null) ? metadataTypeAttribute.MetadataClassType : null;

            return metaDataType;
        }

        private static void SerializeFormDisplayHints<TDataObject>(this TDataObject dataObject, XmlWriter xmlWriter, PropertyInfo propertyInfo)
        {
            DisplayAttribute displayAttribute = GetMetaDataAttribute<TDataObject, DisplayAttribute>(dataObject, propertyInfo);

            //Use the dispaly
            xmlWriter.WriteAttributeString("label", (displayAttribute != null) ? displayAttribute.Name : propertyInfo.Name);

        }

        private static void SerializeFormControlHints<TDataObject>(this TDataObject dataObject, XmlWriter xmlWriter, PropertyInfo propertyInfo)
        {

            DataTypeAttribute dataTypeAttribute = GetMetaDataAttribute<TDataObject, DataTypeAttribute>(dataObject, propertyInfo);
            DataType dataType;
            string inputType;
            if (dataTypeAttribute != null)
            {
                dataType = dataTypeAttribute.DataType;
                inputType = (dataType == DataType.Custom) ? dataTypeAttribute.CustomDataType : dataType.ToString();
            }
            else
            {
                UIHintAttribute uiHintAttribute = GetMetaDataAttribute<TDataObject, UIHintAttribute>(dataObject, propertyInfo);
                inputType = uiHintAttribute != null ? uiHintAttribute.UIHint : DataType.Text.ToString();
            }


            if (string.Compare(inputType, "hidden", true) == 0)
            {
                xmlWriter.WriteAttributeString("hidden", true.ToString());
                inputType = DataType.Text.ToString();
            }
            else
            {
                //Only disable things if not hidden
                EditableAttribute editableAttribute = GetMetaDataAttribute<TDataObject, EditableAttribute>(dataObject, propertyInfo);
                if ((editableAttribute != null) && (editableAttribute.AllowEdit == false))
                {
                    xmlWriter.WriteAttributeString("enabled", "false");
                }
            }

            xmlWriter.WriteAttributeString("inputType", inputType);
        }




        private static void SerializeFormControlValidation<TDataObject>(this TDataObject dataObject, XmlWriter xmlWriter, PropertyInfo propertyInfo)
        {
            ////Write attributes that the OK button will check before allowing click:
            //XmlWriter.WriteAttributeString("validationRequired", System.Convert.ToString(dataObjectPropertyMetadata.Form.ValidationRequired));


            //System.ComponentModel.DataAnnotations.RequiredAttribute;
            RequiredAttribute requiredAttribute = GetMetaDataAttribute<TDataObject, RequiredAttribute>(dataObject, propertyInfo);
            if (requiredAttribute != null)
            {
                xmlWriter.WriteAttributeString("required", true.ToString());
            }

            RegularExpressionAttribute regularExpressionAttribute = GetMetaDataAttribute<TDataObject, RegularExpressionAttribute>(dataObject, propertyInfo);

            if (regularExpressionAttribute != null)
            {
                xmlWriter.WriteAttributeString("validationRegex", regularExpressionAttribute.Pattern);
            }

            StringLengthAttribute stringLengthAttribute = GetMetaDataAttribute<TDataObject, StringLengthAttribute>(dataObject, propertyInfo);

            if (stringLengthAttribute != null)
            {
                xmlWriter.WriteAttributeString("validationMinLength", stringLengthAttribute.MinimumLength.ToString());
                xmlWriter.WriteAttributeString("validationMaxLength", stringLengthAttribute.MaximumLength.ToString());
            }

            RangeAttribute rangeAttribute = GetMetaDataAttribute<TDataObject, RangeAttribute>(dataObject, propertyInfo);

            if (rangeAttribute != null)
            {
                xmlWriter.WriteAttributeString("minimumValue", rangeAttribute.Minimum.ToString());
                xmlWriter.WriteAttributeString("maximumValue", rangeAttribute.Maximum.ToString());

            }

        }

        private class FKSummary
        {
            public Type ParentType;
            public string KeyColumn;
            public string DisplayColumn;
            public string SortColumn;
            public bool SortDescending;

        }
        private static void SerializeFormControlFKInfo<TDataObject>(this TDataObject dataObject, XmlWriter xmlWriter, PropertyInfo propertyInfo, out FKSummary fkSummary)
        {

            AssociationAttribute associationAttribute = propertyInfo.GetAttribute<AssociationAttribute>();

            if (associationAttribute == null)
            {
                fkSummary = null;
                return;
            }

            if (associationAttribute.IsForeignKey)
            {
                fkSummary = null;
                return;
            }

            fkSummary = new FKSummary();

            //Cool. What is the Type of the Foreign Class/Table?
            fkSummary.ParentType = propertyInfo.PropertyType;
            fkSummary.KeyColumn = associationAttribute.OtherKey;

            DisplayColumnAttribute displayColumnAttribute =
                fkSummary.ParentType.GetAttribute<DisplayColumnAttribute>();

            if (displayColumnAttribute == null)
            {
                throw new Exception();
            }

            //Foreign Primary Display COlumn is:
            fkSummary.DisplayColumn = displayColumnAttribute.DisplayColumn;
            fkSummary.SortColumn = displayColumnAttribute.SortColumn;
            fkSummary.SortDescending = displayColumnAttribute.SortDescending;

            //*********************************************
            //TODO
            //What's missing is 
            //a) Multiple SortColumns
            //b) Filter..(to select a subset of a Parent table)
            //*********************************************


            xmlWriter.WriteAttributeString("fkInfo",
                                           string.Format(
                                               "{0}:{1}:{2}:{3}:{4}",
                                               fkSummary.ParentType,
                                               fkSummary.KeyColumn,
                                               fkSummary.DisplayColumn,
                                               fkSummary.SortColumn,
                                               fkSummary.SortDescending));

            //xmlWriter.WriteAttributeString("fkKey", rawValue.ConvertTo<string>());
        }




        private static void SerializeFormControlValue<TDataObject>(this TDataObject dataObject, XmlWriter xmlWriter, PropertyInfo propertyInfo, bool useRawRatherThanFormatted)
        {
            //May come back null:
            Type dataObjectType = dataObject.GetMetaDataClass();

            //Get Raw value up front
            object rawValue = dataObject.GetValue(propertyInfo.Name);

            //And the formatted equiv:
            string stringFormattedValue = rawValue.FormatPropertyValueAsString(propertyInfo);


            if (stringFormattedValue != null)
            {
                //rawValue 
                xmlWriter.WriteStartElement("value");
                xmlWriter.WriteValue(useRawRatherThanFormatted ? rawValue.ConvertTo<string>() : stringFormattedValue);
                xmlWriter.WriteEndElement(); //~value
            }

        }
        /// <summary>
        /// Embeds in the serialized values options that the value can be:
        /// </summary>
        /// <typeparam name="TDataObject">The type of the data object.</typeparam>
        /// <param name="dataObject">The data object.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <param name="propertyInfo">The property info.</param>
        /// <param name="fkSummary">The fk summary.</param>
        /// <param name="rawValue">The raw value.</param>
        /// <param name="embedAnyParentEntityValuesInDropDowns">if set to <c>true</c> [embed any parent entity values in drop downs].</param>
        /// <remarks>
        ///   <para>
        /// Let me explain something here...
        /// In WinForms, you build the form, bind the data, then set the value.
        /// The binding operation calls back to the data layer, and works.
        /// In WebForms, on client side, you are suppossed to embed OPTION tags.
        /// And that misled me for a second -- hence this method.
        ///   </para>
        ///   <para>
        /// But its really not needed at all. In fact, the Xslt's transformation
        /// of the Xml is NOT Html. It is Xml used by a WinForm Form Engine.
        ///   </para>
        ///   <para>
        /// What that means is that in web environment, this xml is then used by a
        /// form engine to build the form on the server side,
        /// where it DOES have
        /// access to the relationship attributes, so it too can bind...
        /// Only then does ASP.NET spit out correct HTML.
        ///   </para>
        ///   <para>
        /// In other words, all this was a red herring...
        ///   </para>
        /// </remarks>
        private static void SerializParentRelationshipValues<TDataObject>(this TDataObject dataObject, XmlWriter xmlWriter, PropertyInfo propertyInfo, FKSummary fkSummary, object rawValue, bool embedAnyParentEntityValuesInDropDowns)
        {

            if ((!embedAnyParentEntityValuesInDropDowns) || (fkSummary == null))
            {
                return;
            }
            Dictionary<object, object> nestedValues = null;

            /*
Events.SchemaFKValueOptionsEventArgs e = new Events.SchemaFKValueOptionsEventArgs(primaryKey, parentRelationship);

if (!OnRaiseFKEvent(e)) {
  if (!SchemaManager.RaiseFKEvent(this, e)) {
    //throw new System.Exception();
  }
}
if (!e.Handled) {
  //throw new System.Exception();
}
nestedValues = e.ReturnedValues;
 */

            if (nestedValues != null)
            {
                xmlWriter.WriteStartElement("options");
                //Nest the values:
                foreach (KeyValuePair<object, object> pair in nestedValues)
                {
                    xmlWriter.WriteStartElement("option");
                    xmlWriter.WriteAttributeString("value", pair.Key.ToString());
                    xmlWriter.WriteAttributeString("text", pair.Value.ToString());
                    if (pair.Key == rawValue)
                    {
                        xmlWriter.WriteAttributeString("selected", "True");
                    }
                    xmlWriter.WriteEndElement(); //~option tag 
                }
                xmlWriter.WriteEndElement(); //~options tag 


            }
        }
    }
