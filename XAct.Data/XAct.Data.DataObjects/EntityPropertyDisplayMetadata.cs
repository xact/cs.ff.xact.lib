﻿using System;


namespace XAct.Data.Entities
{
    public class EntityPropertyDisplayMetadata : IEntityPropertyDisplayMetadata, IIsReadOnly
    {
        /// <summary>
        /// Gets a value indicating whether the properties of this instance are read only.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is read only; otherwise, <c>false</c>.
        /// </value>
        public bool IsReadOnly
        {
            set
            {
                if ((value == false) && (_isReadOnly))
                {
                    throw new ArgumentException("Once set, cannot reset object back to IsReadOnly=false.");
                }
                _isReadOnly = value;
            }
            get
            {
                return _isReadOnly;
            }
        }
        private bool _isReadOnly;

        public string Resource
        {
            get { return _resource; }
            set
            {
                if (IsReadOnly) { throw new ArgumentException("IsReadOnly is set to true."); }
                _resource = value;
            }
        }
        private string _resource;

        /// <summary>
        /// The text -- or if Resource is specified, the Key to the resource text -- to use for a Form Label.
        /// </summary>
        public string Label
        {
            get { return _label; }
            set
            {
                CheckReadOnlyStatus();
                _label = value;
            }
        }


        private string _label;


        public string Hint
        {
            get { return _hint; }
            set
            {
                CheckReadOnlyStatus();
                _hint = value;
            }
        }
        private string _hint;


        public string Description
        {
            get { return _description; }
            set
            {
                CheckReadOnlyStatus();
                _description = value;
            }
        }
        private string _description;


        public bool Required
        {
            get { return _required; }
            set
            {
                CheckReadOnlyStatus();
                _required = value;
            }
        }
        private bool _required;



        private void CheckReadOnlyStatus()
        {
            if (_isReadOnly) { throw new ArgumentException("IsReadOnly is set to true."); }
        }
    }
}
