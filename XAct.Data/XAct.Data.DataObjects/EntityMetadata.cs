﻿using System;
using System.Collections.Generic;
using System.Linq;
using XAct.Collections;

namespace XAct.Data.Entities
{
    /// <summary>
    /// An implementation of the <see cref="IEntityMetadata"/> contract.
    /// </summary>
    public class EntityMetadata : IEntityMetadata, IIsReadOnly
    {

    /// <summary>
    /// Gets a value indicating whether the properties of this instance are read only.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance is read only; otherwise, <c>false</c>.
    /// </value>
    public bool  IsReadOnly
    {
        set
        {
            if ((value == false) && (_isReadOnly))
            {
                throw new ArgumentException("Once set, cannot reset object back to IsReadOnly=false.");
            }
            _isReadOnly = value;
            foreach (var y in _properties.Select(x => ((object) x) as IIsReadOnly))
            {
                y.IsReadOnly = value;
            }
            _properties.IsReadOnly = value;
        }
        get
        {
            return _isReadOnly;
        }
    }
    private bool _isReadOnly;



    /// <summary>
    /// Unique Name of Entity (eg: AApp.AModule.Contact)
    /// <para>
    /// The Name needs to include Namespace to avoid conflict among 3rd parties.
    /// </para>
    /// 	<para>
    /// Without it being unique, we would have to fallback to using a Guid Id,
    /// which is a lot less intuitive.
    /// </para>
    /// </summary>
        public string Name 
        {
            get
            {
                return _name;
            }
            set
            {
                CheckReadOnlyStatus();
                _name = value;
            }
        }
        private string _name;



        /// <summary>
        /// Enumerable list of the <see cref="IEntityPropertyMetadata"/>
        /// describing the entity's properties.
        /// </summary>
        public IEnumerable<IEntityPropertyMetadata> Properties
        {
            get { return _properties.Values; }
        }
        private readonly ReadOnlyDictionary<string, IEntityPropertyMetadata> _properties
            = new ReadOnlyDictionary<string, IEntityPropertyMetadata>();



        private void CheckReadOnlyStatus()
        {
            if (_isReadOnly) { throw new ArgumentException("IsReadOnly is set to true."); }
        }


        /// <summary>
        /// Registers the property metadata.
        /// <para>
        /// Used to build up the inner dictionary (eg from an xml file
        /// or other, describing the entity),
        /// before the inner dictionary is made into a readonly one.
        /// </para>
        /// </summary>
        /// <param name="propertyMetadata">The property metadata.</param>
        public void AddPropertyMetadata(IEntityPropertyMetadata propertyMetadata)
        {
            CheckReadOnlyStatus();

            _properties.Add(propertyMetadata.Name, propertyMetadata);
        }

        /// <summary>
        /// Get a Property's Metadata.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="throwExceptionIfNotFound"></param>
        /// <returns></returns>
        public IEntityPropertyMetadata GetPropertyMetadataByName(string propertyName, bool throwExceptionIfNotFound = true)
        {
            IEntityPropertyMetadata entityPropertyMetadata;

            if ((!_properties.TryGetValue(propertyName,out entityPropertyMetadata))&& throwExceptionIfNotFound)
            {
                throw new ArgumentOutOfRangeException("propertyName");
            }
            return entityPropertyMetadata;
        }
    }
}
