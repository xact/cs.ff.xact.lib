using XAct.Data.Entities;
using System.Collections.ObjectModel;

namespace XAct.Data {

  /// <summary>
  /// Dictionary of dataObject instances, 
  /// indexed by the dataObject's Serialized Identifier.
  /// IMPORTANT: the Id is in string format to allow for int, Guid, and 
  /// (in a user defined format) multiField keys).
  /// </summary>
  public class DataObjectCollection : KeyedCollection<string,DataObject>{

      protected override string GetKeyForItem(DataObject dataObject)
      {
          return dataObject.GetPrimaryKeySerializedValue();
      }
  }//Class:End
}//Namespace:End
