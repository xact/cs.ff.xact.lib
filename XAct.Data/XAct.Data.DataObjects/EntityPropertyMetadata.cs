﻿using System;

namespace XAct.Data.Entities
{
    public class EntityPropertyMetadata : IEntityPropertyMetadata , IIsReadOnly
    {

    /// <summary>
    /// Gets a value indicating whether the properties of this instance are read only.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance is read only; otherwise, <c>false</c>.
    /// </value>
    public bool  IsReadOnly
    {
        set
        {
            if ((value == false) && (_isReadOnly))
            {
                throw new ArgumentException("Once set, cannot reset object back to IsReadOnly=false.");
            }
            _isReadOnly = value;

        }
        get
        {
            return _isReadOnly;
        }
    }
    private bool _isReadOnly;


        
        public string Name 
        {
            get
            {
                return _name;
            }
            set
            {
                CheckReadOnlyStatus();
                if (IsReadOnly) { throw new ArgumentException("IsReadOnly is set to true."); }
                _name = value;
            }
        }
        private string _name;

        public IEntityPropertyStorageMetadata Storage
        {
            get
            {
                return _storage;
            }
        }

        private readonly IEntityPropertyStorageMetadata _storage; 

        public IEntityPropertyDisplayMetadata Display
        {
            get
            {
                return _display;
            }
        }


        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="EntityPropertyMetadata"/> class.
        /// </summary>
        public EntityPropertyMetadata ()
        {
            _storage = new EntityPropertyStorageMetadata();
            _display = new EntityPropertyDisplayMetadata();
        }

        
        private void CheckReadOnlyStatus()
        {
            if (_isReadOnly) { throw new ArgumentException("IsReadOnly is set to true."); }
        }


        public Type Type
        {
            get { throw new NotImplementedException(); }
        }
    }
}
