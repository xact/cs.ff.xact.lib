echo ReplaceEx Company (COMPANYNAME) (eg: 'XAct Software Solutions, Inc.'):
..\_BIN\XAct.Apps4Console.ReplaceEx\ReplaceEx.EXE -v:Verbose -s "XAct Software Solutions, Inc." -k:".svn,_svn"
echo ReplaceEx CopyrightYear (COPYRIGHTYEAR) (eg: '1997'):
..\_BIN\XAct.Apps4Console.ReplaceEx\ReplaceEx.EXE -v:Verbose -s "2011" -k:".svn,_svn"
echo ReplaceEx CopyrightYear (PRODUCTNAME) (eg: 'XAct Library'):
..\_BIN\XAct.Apps4Console.ReplaceEx\ReplaceEx.EXE -v:Verbose -s "XActLib" -k:".svn,_svn"
echo ReplaceEx AssemblyName (ASSEMBLYNAME) (eg: 'XAct.Data'):
..\_BIN\XAct.Apps4Console.ReplaceEx\ReplaceEx.EXE -v:Verbose -s "XAct.Data.Schemas" -k:".svn,_svn"
echo ReplaceEx Description (ASSEMBLYDESCRIPTION) (eg: 'An XAct Library Assembly'):
..\_BIN\XAct.Apps4Console.ReplaceEx\ReplaceEx.EXE -v:Verbose -s "." -k:".svn,_svn"
echo ReplaceEx Namespace (NAMESPACENAME) (eg: 'XAct.Data'):
..\_BIN\XAct.Apps4Console.ReplaceEx\ReplaceEx.EXE -v:Verbose -s "XAct.Data.Schemas" -k:".svn,_svn"
echo ReplaceEx 1stClass (SOMECLASSNAME) (eg: 'AClass'):
..\_BIN\XAct.Apps4Console.ReplaceEx\ReplaceEx.EXE -v:Verbose -s "SOMECLASSNAME" -k:".svn,_svn"


