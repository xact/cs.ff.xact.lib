﻿namespace XAct.Tests
{
    using System;
    using XAct.Domain;
    using XAct.Domain.Repositories;

    public class AuditedSourceAuditRecord : AuditedSourceBase, IHasXActLibEntity, IHasIsAuditModel
    {
        [AuditableProperty(SourcePropertyDotPath = "Gender.Text")]
        public string GenderText { get; set; }

        [AuditableProperty(SourcePropertyDotPath = "GenderFK",Options=new string[]{"Undefined","M","F","Unknown"})]
        public string GenderTextFromOptions { get; set; }



        //NOTE:
        //Has variables from AuditedSourceBase
        //But also incorporates properties from IHasIsAuditModel

        #region IHasIsAuditModel
        /// <summary>
        /// Gets or sets the Id of the audit record (not the Id of the record being Audited).
        /// </summary>
        /// <value>
        /// The audit identifier.
        /// </value>
        public Guid AuditId { get; set; }

        /// <summary>   
        /// The Action that was done to the record ("C"reate, "U"pdate, "D"elete).
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// Gets or sets the audit action on.
        /// </summary>
        /// <value>
        /// The audit action on.
        /// </value>
        public DateTime AuditActionOn { get; set; }
        /// <summary>
        /// Gets or sets the audit action by.
        /// </summary>
        /// <value>
        /// The audit action by.
        /// </value>
        public string AuditActionBy { get; set; }

        #endregion

    }
}