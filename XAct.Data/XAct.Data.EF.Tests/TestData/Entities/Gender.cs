﻿namespace XAct.Tests
{
    public enum Gender
    {
        Unknown=0,
        Male=1,
        Female=2,
        Undecided=3
    }
}