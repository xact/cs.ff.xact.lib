﻿

namespace XAct.Tests
{
    using XAct.Domain;

    [Auditable(Type = typeof(AuditedSourceAuditRecord))]
    public class AuditedSource : AuditedSourceBase, IHasXActLibEntity // IHasIntId, IHasNameAndDescription, IHasDateTimeCreatedBy, IHasDateTimeModifiedBy, IHasDateTimeDeletedBy
    {
        //All variables are coming up from AuditedSourceBase


    }
}