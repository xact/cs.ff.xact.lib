﻿

namespace XAct.Tests
{
    public class ReferenceData_Gender : IHasXActLibEntity, IHasId<int>
    {
        public int Id { get; set; }

        public string Text { get; set; }
    }
}