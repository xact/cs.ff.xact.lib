﻿

namespace XAct.Tests
{

    public class NonAuditedSource: IHasXActLibEntity, IHasIntId, IHasNameAndDescription
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
