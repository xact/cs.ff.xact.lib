﻿namespace XAct.Tests
{
    using System;

    /// <summary>
    /// Common base to both <see cref="AuditedSource"/> and <see cref="AuditedSourceAuditRecord"/>
    /// </summary>
    public abstract class AuditedSourceBase : IHasIntId, IHasNameAndDescription, IHasDateTimeCreatedBy, IHasDateTimeModifiedBy, IHasDateTimeDeletedBy
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
        public virtual string Description { get; set; }


        public virtual int GenderFK { get; set; }
        public virtual ReferenceData_Gender Gender { get; set; }

        public virtual Gender Gender2FK { get; set; }
        public virtual ReferenceData_Gender Gender2 { get; set; }

        public virtual DateTime? CreatedOnUtc { get; set; }
        public virtual string CreatedBy { get; set; }

        public virtual DateTime? LastModifiedOnUtc { get; set; }
        public virtual string LastModifiedBy { get; set; }

        public virtual DateTime? DeletedOnUtc { get; set; }
        public virtual string DeletedBy { get; set; }
    }
}