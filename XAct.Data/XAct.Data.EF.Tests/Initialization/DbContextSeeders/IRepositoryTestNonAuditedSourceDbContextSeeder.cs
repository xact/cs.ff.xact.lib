﻿namespace XAct.Tests
{
    using XAct.Data.EF.CodeFirst;

    public interface IRepositoryTestNonAuditedSourceDbContextSeeder : IHasXActLibDbContextSeeder<NonAuditedSource>
    {

    }
}