﻿namespace XAct.Tests
{
    using XAct.Data.EF.CodeFirst;

    public interface IRepositoryTestReferenceData_GenderDbContextSeeder : IHasXActLibDbContextSeeder<ReferenceData_Gender>
    {

    }
}