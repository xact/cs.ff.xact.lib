﻿namespace XAct.Tests
{
    using XAct.Data.EF.CodeFirst;

    public interface IRepositoryTestAuditedSourceDbContextSeeder : IHasXActLibDbContextSeeder<AuditedSource>
    {

    }
}