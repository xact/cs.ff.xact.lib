﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Services;

    /// <summary>
    /// An implementation of 
    /// <see cref="IRepositoryTestNonAuditedSourceDbContextSeeder"/>
    /// <para>
    /// Important:
    /// during XActLib development, called by initializer setup by bootstrapper.
    /// </para>
    /// <para>
    /// Important:
    /// Called by <c>IViewModeDbInitializer</c>
    /// when it is setup by the bootsrapper.
    /// </para>
    /// </summary>
    public class RepositoryTestAuditedSourceDbContextSeeder : UnitTestXActLibDbContextSeederBase<AuditedSource>, 
        IRepositoryTestAuditedSourceDbContextSeeder, IHasMediumBindingPriority
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryTestAuditedSourceDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public RepositoryTestAuditedSourceDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {

        }

        /// <summary>
        /// Seeds the specified db context.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        public override void SeedInternal(DbContext dbContext) 
        {

            this.SeedInternalHelper(dbContext,true,x=>x.Id);



        }

        public override void CreateEntities()
        {
                this.InternalEntities = new List<AuditedSource>();

                this.InternalEntities.Add(new AuditedSource
                                      {
                                          Id = 1,
                                          Name = "Demo1",
                                          Description = "Description1",
                                          CreatedOnUtc = DateTime.UtcNow,
                                          LastModifiedOnUtc = DateTime.UtcNow,
                                          CreatedBy = "Foo",
                                          LastModifiedBy = "Foo",
                                          Gender2FK = Gender.Male,
                                          GenderFK = 1,
                                      });
                this.InternalEntities.Add(new AuditedSource
                {
                                          Id = 2,
                                          Name = "Demo2",
                                          Description = "Description2",
                                          CreatedOnUtc = DateTime.UtcNow,
                                          LastModifiedOnUtc = DateTime.UtcNow,
                                          CreatedBy = "Foo",
                                          LastModifiedBy = "Foo",
                                          Gender2FK = Gender.Female,
                                          GenderFK = 2
                                      });
                this.InternalEntities.Add(new AuditedSource
                {
                                          Id = 3,
                                          Name = "Demo3",
                                          Description = "Description3",
                                          CreatedOnUtc = DateTime.UtcNow,
                                          LastModifiedOnUtc = DateTime.UtcNow,
                                          LastModifiedBy = "Foo",
                                          CreatedBy = "Foo",
                                          Gender2FK = Gender.Unknown,
                                          GenderFK = 3
                                      });


        }


    }
}