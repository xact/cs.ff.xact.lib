﻿namespace XAct.Tests
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    public class RepositoryTestReferenceData_GenderDbContextSeeder : UnitTestXActLibDbContextSeederBase<ReferenceData_Gender>,
                                                                     IRepositoryTestReferenceData_GenderDbContextSeeder, IHasMediumBindingPriority
    {
        public RepositoryTestReferenceData_GenderDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.Id);
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<ReferenceData_Gender>();

            this.InternalEntities.Add(new ReferenceData_Gender { Id = 1, Text = "Male" });
            this.InternalEntities.Add(new ReferenceData_Gender { Id = 2, Text = "Female" });
            this.InternalEntities.Add(new ReferenceData_Gender { Id = 3, Text = "OnTheFence" });

        }
    }
}