﻿namespace XAct.Tests
{
    using System.Collections.Generic;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    public class RepositoryTestNonAuditedSourceDbContextSeeder : UnitTestXActLibDbContextSeederBase<NonAuditedSource>,
                                                                 IRepositoryTestNonAuditedSourceDbContextSeeder, IHasMediumBindingPriority
    {

        public RepositoryTestNonAuditedSourceDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }


        public override void CreateEntities()
        {

            this.InternalEntities = new List<NonAuditedSource>
                {
                    new NonAuditedSource {Id = 1, Name = "Demo1", Description = "Description1"},
                    new NonAuditedSource {Id = 2, Name = "Demo2", Description = "Description2"},
                    new NonAuditedSource {Id = 3, Name = "Demo3", Description = "Description3"}
                };

        }
    }
}