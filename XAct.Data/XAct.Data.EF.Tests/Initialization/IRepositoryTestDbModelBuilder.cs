﻿namespace XAct.Tests
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface IRepositoryTestDbModelBuilder : IHasXActLibDbModelBuilder
    {

    }
}