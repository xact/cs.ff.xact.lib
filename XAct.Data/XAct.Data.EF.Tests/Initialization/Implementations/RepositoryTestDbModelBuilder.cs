﻿namespace XAct.Tests
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// IMPORTANT: Only for testing is this ModuleBuilder in the Test Suite -- in
    /// every other assembly, the Builder is in the library assembly.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class RepositoryTestDbModelBuilder : IRepositoryTestDbModelBuilder, IHasMediumBindingPriority
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryTestDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public RepositoryTestDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<ReferenceData_Gender>)XAct.DependencyResolver.Current.GetInstance<IReferenceDataGenderPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<NonAuditedSource>)XAct.DependencyResolver.Current.GetInstance<INonAuditedSourceModelPersistenceMap>());

            modelBuilder.Configurations.Add((EntityTypeConfiguration<AuditedSource>)XAct.DependencyResolver.Current.GetInstance<IAuditedSourceModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<AuditedSourceAuditRecord>)XAct.DependencyResolver.Current.GetInstance<IAuditedSourceAuditRecordModelPersistenceMap>());



        }
    }
}


