﻿namespace XAct.Tests
{
    using System.Data.Entity.ModelConfiguration;

    public class NonAuditedSourceModelPersistenceMap : EntityTypeConfiguration<NonAuditedSource>, INonAuditedSourceModelPersistenceMap, IHasMediumBindingPriority
    {

        public NonAuditedSourceModelPersistenceMap()
        {
            this.ToXActLibTable("DemoSource");

            this.HasKey(m => m.Id);

            int colIndex = 0;

            this.Property(m => m.Id)
                .DefineRequiredIntId(colIndex++);
                ;

            this.Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(64) //Name
                .HasColumnOrder(colIndex++)
                ;

            this.Property(m => m.Description)
                .DefineOptional4000CharDescription(colIndex++)
                ;
        }
    }
}