﻿namespace XAct.Tests
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Services;


    public class ReferenceDataGenderPersistenceMap : EntityTypeConfiguration<ReferenceData_Gender>,
                                        IReferenceDataGenderPersistenceMap
    {
        public ReferenceDataGenderPersistenceMap()
        {
            this.ToXActLibTable("DemoReferenceDataGender");

            this.HasKey(m => m.Id);

            int colIndex = 0;

            this.Property(m => m.Id)
                .IsRequired()
                .HasColumnOrder(colIndex++)
                ;

            this.Property(m => m.Text)
                .IsRequired()
                .HasMaxLength(64) //Name
                .HasColumnOrder(colIndex++)
                ;
        }

    }
}