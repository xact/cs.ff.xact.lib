﻿namespace XAct.Tests
{
    using System;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Services;

    public class AuditedSourceAuditRecordModelPersistenceMap : EntityTypeConfiguration<AuditedSourceAuditRecord>, IAuditedSourceAuditRecordModelPersistenceMap, IHasMediumBindingPriority
    {

        public AuditedSourceAuditRecordModelPersistenceMap()
        {
            this.ToXActLibTable("AuditedSourceAudit");



            this.HasKey(m => m.AuditId);

            int colOrder = 0;

            AddAuditingPropertyMaps(ref colOrder);

            //---------------

            this.Property(m => m.Id)
                .DefineRequiredIntIdNonIdentity(colOrder++);

            this.Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(64) //Name
                .HasColumnOrder(colOrder++)
                ;

            this.Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;

            this.Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);

            this.Property(m => m.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++);

            this.Property(m => m.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++);

            this.Property(m => m.LastModifiedBy)
                .DefineRequired64CharLastModifiedBy(colOrder++);

            this.Property(m => m.DeletedOnUtc)
                .DefineOptionalDeletedOnUtc(colOrder++);
            this.Property(m => m.DeletedBy)
                .DefineOptional64CharDeletedBy(colOrder++);

        }

        private int AddAuditingPropertyMaps(ref int colIndex)
        {
            this.Property(m => m.AuditId)
                .IsRequired()
                .HasColumnOrder(colIndex++)
                ;

            this.Property(m => m.Action)
                .IsOptional()
                .HasColumnOrder(colIndex++)
                ;

            this.Property(m => m.AuditActionOn)
                .IsRequired()
                .HasColumnOrder(colIndex++)
                ;

            this.Property(m => m.AuditActionBy)
                .IsRequired()
                .HasColumnOrder(colIndex++)
                ;
            return colIndex;
        }
    }
}