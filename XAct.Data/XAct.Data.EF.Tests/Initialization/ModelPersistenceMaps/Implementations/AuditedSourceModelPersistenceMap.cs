﻿namespace XAct.Tests
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Services;

    public class AuditedSourceModelPersistenceMap : EntityTypeConfiguration<AuditedSource>, IAuditedSourceModelPersistenceMap, IHasMediumBindingPriority
    {

        public AuditedSourceModelPersistenceMap()
        {
            this.ToXActLibTable("AuditedSource");

            this.HasKey(m => m.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredIntId(colOrder++)
                ;

            this.Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(64) //Name
                .HasColumnOrder(colOrder++)
                ;

            this.Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;

            this.Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);
            this.Property(m => m.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++);
            this.Property(m => m.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++);
            this.Property(m => m.LastModifiedBy)
                .DefineRequired64CharLastModifiedBy(colOrder++);
            this.Property(m => m.DeletedOnUtc)
                .DefineOptionalDeletedOnUtc(colOrder++)
                ;
            this.Property(m => m.DeletedBy)
                .DefineOptional64CharDeletedBy(colOrder++)
                ;

        }
    }
}