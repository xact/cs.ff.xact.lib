﻿namespace XAct.Data.EF.Tests
{
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;

    public class RepositoryTestService : XActLibServiceBase, IRepositoryTestService
    {
        private readonly IRepositoryService _repositoryService;

        public RepositoryTestService(ITracingService tracingService, IRepositoryService repositoryService):base(tracingService)
        {
            _repositoryService = repositoryService;
        }
    }
}
