﻿using NUnit.Framework;
using XAct.Domain.Repositories;
using XAct.Domain.Repositories.Services;
using XAct.Settings;
using XAct.Tests;

namespace XAct.Data.EF.Tests.Tests
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Security.Principal;
    using System.Threading;
    using XAct.Environment;

    [TestFixture]
    public class UnitOfWorkPostCommitServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();


            XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>().Configuration.Initialize();





        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        private static void AttachPreprocessors(IUnitOfWorkCommitPreProcessorService preProcessorService)
        {
            if (preProcessorService.PreProcessors.Count == 0)
            {
                preProcessorService.PreProcessors.Add(
                    XAct.DependencyResolver.Current
                        .GetInstance<HasDateTimeCreatedOnUnitOfWorkPreCommitProcessingStrategy>());
                preProcessorService.PreProcessors.Add(
                    XAct.DependencyResolver.Current
                        .GetInstance<HasDateTimeModifiedOnUnitOfWorkPreCommitProcessingStrategy>());
                //preProcessorService.PreProcessors.Add(
                //    XAct.DependencyResolver.Current
                //        .GetInstance<HasDateTimeDeletedOnUnitOfWorkPreCommitProcessingStrategy>());

                //Register last
                preProcessorService.PreProcessors.Add(
                    XAct.DependencyResolver.Current.GetInstance<AuditableChangesStrategy>());


            }
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        public void CanGetIRepositoryService()
        {
            //ARRANGE:
            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ACT:
            int count = repositoryService.Count<NonAuditedSource>();

            //ASSERT:
            Assert.IsTrue(count > 0);
        }


        [Test]
        public void CanGetIRepositoryServiceOfExpectedTYpe()
        {
            //ARRANGE:
            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ASSERT:
            Assert.AreEqual(typeof (EntityDbContextGenericRepositoryService), repositoryService.GetType());
        }

        [Test]
        public void CanGetUoWPreprocessorService()
        {


            //ARRANGE:
            IUnitOfWorkCommitPreProcessorService preProcessorService =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorService>();

            //ASSERT:
            Assert.AreEqual(typeof (EntityDbContextUnitOfWorkCommitPreprocessorService), preProcessorService.GetType());
        }

        [Test]
        public void CanRegisterePreProcessorsInService()
        {

            //ARRANGE:
            IUnitOfWorkCommitPreProcessorService preProcessorService =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorService>();


            AttachPreprocessors(preProcessorService);

            Assert.IsFalse(preProcessorService.PreProcessors.Count == 0);
        }





        [Test]
        public void GetDemoEntries()
        {
            //ARRANGE:
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ACT:
            AuditedSource[] result = repository.GetByFilter<AuditedSource>(x => x.Id > 0).ToArray();

            //ASSERT:
            Console.WriteLine(result.Count());
            Assert.IsTrue(result.Count() >= 3, "Not More than 3");

        }




        [Test]
        public void UpdateFirstEntry()
        {
            //ARRANGE:
            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            IUnitOfWorkCommitPreProcessorService preProcessorService =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorService>();

            AttachPreprocessors(preProcessorService);

            //ACT:
            AuditedSource[] entities = repositoryService.GetByFilter<AuditedSource>(x => x.Id > 0).ToArray();


            var firstEntity = entities.First();

            firstEntity.Description = "Description changed on " + DateTime.Now;


            var countBefore = repositoryService.Count<AuditedSourceAuditRecord>();
            repositoryService.GetContext().Commit();
            var countAfter = repositoryService.Count<AuditedSourceAuditRecord>();

            var auditRecord =
                repositoryService.GetByFilter<AuditedSourceAuditRecord>(x => x.Id == firstEntity.Id, null, null,
                                                                        o => o.OrderByDescending(x => x.AuditActionOn))
                                 .FirstOrDefault();

            //ASSERT:

            Assert.IsTrue(true);
            Assert.IsTrue(countAfter > countBefore, "Should have had some Audit records created.");
            Assert.AreEqual("U", auditRecord.Action);
        }



        [Test]
        public void UpdateSecondEntry()
        {
            //ARRANGE:
            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            IUnitOfWorkCommitPreProcessorService preProcessorService =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorService>();

            AttachPreprocessors(preProcessorService);


            var holdPrincipal = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal;
            try
            {

                //ACT:
                AuditedSource[] entities = repositoryService.GetByFilter<AuditedSource>(x => x.Id > 0).ToArray();


                var secondEntity = entities[1];

                secondEntity.Description = "Description changed on " + DateTime.Now;

                XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal =
                    new GenericPrincipal(new GenericIdentity("Bar"), null);

                var countBefore = repositoryService.Count<AuditedSourceAuditRecord>();
                repositoryService.GetContext().Commit();
                var countAfter = repositoryService.Count<AuditedSourceAuditRecord>();

                var auditRecord =
                    repositoryService.GetByFilter<AuditedSourceAuditRecord>(x => x.Id == secondEntity.Id, null, null,
                                                                            o =>
                                                                            o.OrderByDescending(x => x.AuditActionOn))
                                     .FirstOrDefault();

                //ASSERT:

                Assert.IsTrue(true);
                Assert.IsTrue(countAfter > countBefore, "Should have had some Audit records created.");
                Assert.AreEqual("U", auditRecord.Action);
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal = holdPrincipal;
            }
        }


        [Test]
        public void UpdateSecondEntryMultipleTimes()
        {
            //ARRANGE:
            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            IUnitOfWorkCommitPreProcessorService preProcessorService =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorService>();

            AttachPreprocessors(preProcessorService);

            var holdPrincipal = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal;
            try
            {
                //ACT:
                AuditedSource[] entities = repositoryService.GetByFilter<AuditedSource>(x => x.Id > 0).ToArray();
                AuditedSourceAuditRecord auditRecord = null;


                int countBefore = 0;
                int countAfter = 0;
                for (int i = 0; i < 5; i++)
                {
                    XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal =
                        new GenericPrincipal(new GenericIdentity("Bar" + i), null);

                    var secondEntity = entities[1];
                    secondEntity.Description = "Description " + i + " changed on " + DateTime.Now;


                    countBefore = repositoryService.Count<AuditedSourceAuditRecord>();
                    repositoryService.GetContext().Commit();
                    countAfter = repositoryService.Count<AuditedSourceAuditRecord>();

                    auditRecord =
                        repositoryService.GetByFilter<AuditedSourceAuditRecord>(x => x.Id == secondEntity.Id, null, null,
                                                                                o =>
                                                                                o.OrderByDescending(x => x.AuditActionOn))
                                         .FirstOrDefault();

                    Thread.Sleep(100);
                }


                //ASSERT:

                Assert.IsTrue(true);
                Assert.IsTrue(countAfter > countBefore, "Should have had some Audit records created.");
                Assert.AreEqual("U", auditRecord.Action);
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal = holdPrincipal;
            }
        }


        [Test]
        public void DeleteThirdEntry()
        {
            //ARRANGE:
            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            IUnitOfWorkCommitPreProcessorService preProcessorService =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorService>();

            AttachPreprocessors(preProcessorService);

            var holdPrincipal = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal;
            try
            {
                //ACT:
                AuditedSource[] result = repositoryService.GetByFilter<AuditedSource>(x => x.Id > 0).ToArray();

                Assert.IsTrue(result.Length>2,"Should Have 3 results" + result.Length);
                var entity = result[2];
                
                //var shuoldBeOriginal = entity.Description;

                //DbContext dbContext =
                //  ((IHasInnerItemReadOnly) repositoryService.Context).GetInnerItem<DbContext>();

                //string x1 = dbContext.Entry(entity).CurrentValues["Description"] as string;

                entity.Description = "Description changed on " + DateTime.Now;

                //string y1 = dbContext.Entry(entity).CurrentValues["Description"] as string;

                var countBefore = repositoryService.Count<AuditedSourceAuditRecord>();

                repositoryService.DeleteOnCommit<AuditedSource>(entity);

                //string z1 = dbContext.Entry(entity).CurrentValues["Description"] as string;

                XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal =
                    new GenericPrincipal(new GenericIdentity("Bar"), null);

                repositoryService.GetContext().Commit();
                var countAfter = repositoryService.Count<AuditedSourceAuditRecord>();

                var auditRecords =
                    repositoryService.GetByFilter<AuditedSourceAuditRecord>(x => x.Id == entity.Id, null, null,
                                                                            o =>
                                                                            o.OrderByDescending(x => x.AuditActionOn));
               Assert.IsTrue(auditRecords.Count()>0);
      
                        var auditRecord = auditRecords.FirstOrDefault();

                //ASSERT:

                Assert.IsNotNull(auditRecord,"AuditRecord should exist.");
                Assert.IsTrue(countAfter > countBefore, "Should have had some Audit records created.");
                Assert.AreEqual("D", auditRecord.Action);
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal = holdPrincipal;
            }
        }

        [Test]
        public void AddFourthEntry()
        {
            //ARRANGE:
            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            IUnitOfWorkCommitPreProcessorService preProcessorService =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkCommitPreProcessorService>();

            AttachPreprocessors(preProcessorService);

            var holdPrincipal = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal;
            try
            {
                //ACT:

                XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal =
                    new GenericPrincipal(new GenericIdentity("Bar"), null);

                var entity = new AuditedSource
                    {
                        Name = "Foo.New",
                        Description = "Description changed on " + DateTime.Now
                    };


                var countBefore = repositoryService.Count<AuditedSourceAuditRecord>();

                repositoryService.PersistOnCommit<AuditedSource>(entity, x => x.Id == 0);


                repositoryService.GetContext().Commit();
                var countAfter = repositoryService.Count<AuditedSourceAuditRecord>();

                //This will fail because Source was not commmitted at point in time
                //when Audit record was created.
                //Therefore it still has ID=0, and that is what ended up in Audit record.
                //So could not be matched by ID...only other criteria.
#pragma warning disable 168
                var auditRecord =
                    repositoryService.GetByFilter<AuditedSourceAuditRecord>(x => x.Id == entity.Id, null, null,
                                                                            o =>
                                                                            o.OrderByDescending(x => x.AuditActionOn))
                                     .FirstOrDefault();
#pragma warning restore 168

                //ASSERT:

                Assert.IsTrue(true);
                Assert.IsTrue(countAfter > countBefore, "Should have had some Audit records created.");

                //Since I strongly suggest (in this world of Mobile and Azure) that you use Guids instead,
                //this should not be an issue.
                //Assert.IsNotNull(auditRecord);
                //WILL NOT BE TRUE: Assert.AreEqual("C", auditRecord.Action);
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IPrincipalService>().Principal = holdPrincipal;
            }
        }








    }
}

