namespace XAct.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Domain.Repositories.Services;
    using XAct.Settings;

    [TestFixture]
    public class RepositoryServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();


            XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>().Configuration.Initialize();
        }


        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_RepositoryService()
        {
            //ARRANGE:
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ACT:

            //ASSERT:
            Assert.IsNotNull(repository);
        }


        [Test]
        public void Can_Get_RepositoryService_Of_ExpectedTYpe()
        {
            //ARRANGE:
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ASSERT:
            Assert.AreEqual(typeof(EntityDbContextGenericRepositoryService),repository.GetType());
        }


        [Test]
        public void Count_DemoEntries()
        {
            //ARRANGE:
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ACT:
            int result = repository.Count<NonAuditedSource>();
            
            //ASSERT:
            Assert.IsTrue(result>0);

        }


        [Test]
        public void Get_A_Single_DemoEntry_By_Id()
        {
            //ARRANGE:
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ACT:
            NonAuditedSource result = repository.GetSingle<NonAuditedSource>(x => x.Id == 1);
            
            //ASSERT:
            Assert.IsNotNull(result);

        }

        [Test]
        public void Get_All_DemoEntries_Using_GetByFilter()
        {
            //ARRANGE:
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ACT:
            NonAuditedSource[] result = repository.GetByFilter<NonAuditedSource>(x => x.Id > 0).ToArray();

            //ASSERT:
            Assert.IsTrue(result.Count() > 1);

        }




        [Test]
        public void Once_Marked_For_Persistence_Latest_Value_Set_Prior_To_Commit_Is_Persisted()
        {
            //ARRANGE:
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            //ACT:
            NonAuditedSource repositoryTest = new NonAuditedSource
                {
                    Name = "Foobly",
                    Description = "Descr...."
                };

            repository.PersistOnCommit<NonAuditedSource,int>(repositoryTest,false);



            repository.GetContext().Commit();
            var id = repositoryTest.Id;
            Console.WriteLine("Id:" + repositoryTest.Id);

            foreach (var x in repository.GetByFilter<NonAuditedSource>(x => true))
            {
                Console.WriteLine(x.Id + ":" + x.Name);
            }

            var retrieved = repository.GetSingle<NonAuditedSource>(x=>x.Id==id);
            Assert.IsNotNull(retrieved,"First retrival");

            retrieved.Description = "New1";
            repository.PersistOnCommit<NonAuditedSource, int>(retrieved, false);
            retrieved.Description = "New2";
            retrieved.Description = "New3";

            repository.GetContext().Commit();

            
            var retrieved2 = repository.GetSingle<NonAuditedSource>(x => x.Id == id);

            //ASSERT:
            Assert.IsNotNull(retrieved2,"second retrieval");
            Assert.AreEqual("New3",retrieved2.Description,"Should be last value given before Commit()");

        }



    }


}


