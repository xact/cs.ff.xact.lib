using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("XAct.Data.Db.Tests")]
[assembly: AssemblyDescription("Unit Tests for an assembly.")]
#if DEBUG

[assembly: AssemblyConfiguration("DEBUG")]
#else 
[assembly: AssemblyConfiguration("")]
#endif

[assembly: AssemblyCompany("XAct Sofware Solutions, Inc.")]
[assembly: AssemblyProduct("XActLib")]
[assembly: AssemblyCopyright("Copyright © XAct Software Solutions, Inc. 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("a5474fb2-26d2-47b5-ae14-2280171bc865")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]