namespace XAct.Data.Documents.Services
{
    using XAct;

#pragma warning disable 1591
    public interface ISomeService :IHasXActLibService 
#pragma warning restore 1591
    {
#pragma warning disable 1591
        SomeServiceEntity Foo();
#pragma warning restore 1591
    }
}
