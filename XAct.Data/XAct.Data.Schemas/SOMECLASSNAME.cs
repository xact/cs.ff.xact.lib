using System;
using System.ComponentModel.DataAnnotations;

namespace XAct.Data.Schemas
{
    /// <summary>
    ///   An XAct.Data.Schemas class.
    /// </summary>
    //[MetadataType(typeof(SomeTaskEntityMetadata))]
    public class SomeTaskEntity
    {
        /// <summary/>
        public int Id { get; set; }
        /// <summary/>
        public DateTime? Start { get; set; }
        /// <summary/>
        public DateTime? End { get; set; }
        /// <summary/>
        public string Subject { get; set; }
        /// <summary/>
        public string Body { get; set; }
    }

      // + DataObjectPropertySchema instance
  //   - DataObjectPropertySchemaDataStoreInfo instance
  //   - DataObjectPropertySchemaDisplayListInfo instance
  //   - DataObjectPropertySchemaDisplayFormInfo instance

    //PARENT/TOP
    //DataObjectPropertySchema[] NaturalSortProperties (done by scanning properties for SortOrder)
    //strin[] PropertyNames (done by scanning properties)
    //string[] DataStorePropertyNames 

    //GENERAL:
    //Name
    //Type
    //ViaIndexer (is property accessed via this{get;set;})
    //Readonly
    //IsSearchable (if yes, offer autocomplete, etc.)
    //StringFormatCode

    //DISPLAY
    //Attributes to figure out are:
    //Hidden
    //Editable
    //Label => Display.Name
    //Required =>
    //ValidationMinLength
    //ValidationMaxLength
    //ValidationRegex
    //ValidationMinValue
    //ValidationMaxValue

    //DATA:
    
/*
 * * string Name (ColumnName)
 * bool ReadOnly (Can be modified further)
 * Type DataType
 * int DataStore Length
 * bool IsPrimaryKey
 *  bool IsKey
 *  bool CanBeNull
 *  bool IsDbGenerated (see Id...etc)
 *  bool IsDiscriminator (no idea)
 *  bool IsVersion (is the timestamp or version column)
 *  bool Expression (is a ComputedColumn)
 *  
 *  /

    public class SomeTaskEntityMetadata
    {
        // see: http://bit.ly/r2sLsl then http://bit.ly/qb18mv
        [UIHint("Hidden")]
        [Editable(false)]
        public int Id { get; set; }

        public DateTime? Start { get; set; }
     
        [Range(typeof(DateTime),"1900-01-01","2100-01-01",ErrorMessage = "xxx")]
        public DateTime? End { get; set; }

        [Display(
            ResourceType = typeof(XAct.Data.Schemas.Properties.Resources),
            Name = "TaskSubjectName",
            Order = -9,
            Prompt = "TaskSubjectPrompt",
            Description = "TaskSubjectDescription",
            GroupName = "Tab2")]
        [Required]
        [StringLength(1024, MinimumLength=4)]
        public string Subject { get; set; }

        
        [Display(
            Name = "TaskBodyName",
            Order = -9,
            Prompt = "TaskBodyPrompt",
            Description = "TaskBodyDescription", 
            GroupName = "Tab2")]
        public string Body { get; set; }

        [RegularExpression("dddd", ErrorMessage = "errMsg", ErrorMessageResourceName = "errMsgRsourceName",ErrorMessageResourceType = typeof(XAct.Data.Schemas.Properties.Resources))]
        public string TaskCode { get; set; }
 * 
 */
    }
