﻿namespace XAct {

    using XAct.Data.DataSources;

    /// <summary>
    /// Extension methods to <see cref="DataSource"/> entities.
    /// </summary>
    public static class DataSourceExtensions
    {

        /// <summary>
        /// Validates the specified <see cref="DataSource"/> properties prior to persisting.
        /// </summary>
        /// <param name="dataSource">The data source.</param>
        public static void Validate(this DataSource dataSource)
        {
            dataSource.Name.ValidateIsNotNullOrEmpty("dataSource.Name");
            dataSource.DataStoreTableName.ValidateIsNotNullOrEmpty("dataSource.DataStoreCatalogName");
            dataSource.DataStoreSerializedIdentifierNames.ValidateIsNotNullOrEmpty("dataSource.DataStoreSerializedIdentifierNames");

            if (dataSource.IsRemote)
            {
                dataSource.ConnectorClassification.ValidateIsNotNullOrEmpty("dataSource.ConnectorTypeName");
                dataSource.ConnectionProviderType.ValidateIsNotNullOrEmpty("dataSource.ConnectionProviderType");

                dataSource.ConnectionString.ValidateIsNotNullOrEmpty("dataSource.ConnectionString");
                dataSource.ConnectionMetaData.ValidateIsNotNullOrEmpty("dataSource.ConnectionMetaData");

                dataSource.DataStoreCatalogName.ValidateIsNotNullOrEmpty("dataSource.DataStoreCatalogName");

                //Required either way:
                dataSource.DataStoreTableName.ValidateIsNotNullOrEmpty("dataSource.DataStoreTableName");
                //Required either way:
                dataSource.DataStoreSerializedIdentifierNames.ValidateIsNotNullOrEmpty("dataSource.SerializedIdentifierNames");

                dataSource.DisplayMetaData.ValidateIsNotNullOrEmpty("dataSource.DisplayMetaData");

            }

        }
    }
}
