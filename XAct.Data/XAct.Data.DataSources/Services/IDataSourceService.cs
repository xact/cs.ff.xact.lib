namespace XAct.Data.DataSources
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface IDataSourceService : IHasXActLibService
    {

        /// <summary>
        /// Gets the specified <see cref="DataSource"/>.
        /// </summary>
        /// <returns></returns>
        DataSource GetById(Guid id);

        /// <summary>
        /// Gets the specified <see cref="DataSource"/>.
        /// </summary>
        /// <returns></returns>
        DataSource GetByText(string name);
    }
}