namespace XAct.Data.DataSources
{
    using System;
    using System.Linq.Expressions;
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="IDataSourcesManagementService" />
    /// contract for a service to manage <see cref="DataSource" />
    /// elements.
    /// </summary>
    public class DataSourcesManagementService : DistributedGuidIdRepositoryServiceBase<DataSource>,  IDataSourcesManagementService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourcesManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public DataSourcesManagementService(ITracingService tracingService, IRepositoryService repositoryService) : base(tracingService, repositoryService)
        {
        }



        /// <summary>
        /// Persists the new or updated <see cref="DataSource" />.
        /// </summary>
        /// <param name="dataSource">The data source.</param>
        public new virtual void PersistOnCommit(DataSource dataSource)
        {
            dataSource.Validate();
            base.PersistOnCommit(dataSource);
        }



        /// <summary>
        /// Gets the specified DataSource using its unique key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public DataSource GetByKey(string key)
        {
            return _repositoryService.GetSingle<DataSource>(x => x.Name == key);
        }

    }
}