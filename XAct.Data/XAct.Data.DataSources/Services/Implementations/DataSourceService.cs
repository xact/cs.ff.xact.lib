namespace XAct.Data.DataSources
{
    using System;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class DataSourceService : IDataSourceService
    {
        private readonly IDataSourcesManagementService _dataSourceManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceService"/> class.
        /// </summary>
        /// <param name="dataSourceManagementService">The data source management service.</param>
        public DataSourceService(IDataSourcesManagementService dataSourceManagementService)
        {
            _dataSourceManagementService = dataSourceManagementService;
        }

        /// <summary>
        /// Gets the specified <see cref="DataSource"/>
        /// </summary>
        /// <returns></returns>
        public DataSource GetById(Guid id)
        {
            return _dataSourceManagementService.GetById(id);
        }

        /// <summary>
        /// Gets the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public DataSource GetByText(string name)
        {
            return _dataSourceManagementService.GetByKey(name);
        }
    }
}