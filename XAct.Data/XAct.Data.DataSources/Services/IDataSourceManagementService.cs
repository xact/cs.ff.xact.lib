namespace XAct.Data.DataSources
{
    using System;
    using XAct.Domain.Repositories;

    /// <summary>
    /// Contract for a service to manage <see cref="DataSource" /> elements.
    /// </summary>
    public interface IDataSourcesManagementService : ISimpleRepository<DataSource,Guid>, IHasXActLibService
    {

        /// <summary>
        /// Gets the specified DataSource using its unique key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        DataSource GetByKey(string key);
    }
}