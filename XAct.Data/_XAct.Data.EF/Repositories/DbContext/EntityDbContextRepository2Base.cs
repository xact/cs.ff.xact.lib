﻿using System;
using System.Data.Entity;

namespace XAct.Data.Repositories
{
    /// <summary>
    /// Abstract base class for Repositories 
    /// intialized using 
    /// an EF 4.2+ CodeFirst DbContext
    /// entity.
    /// <para>
    /// Compared to the simpler 
    /// <see cref="EntityDbContextRepositoryBase{TAggregateRootEntity}"/>
    /// </para>
    /// <para>
    /// The difference betweeen the two is that this one expects an
    /// <see cref="EntityDbContext"/> (a non Vendor-specific wrapper of
    /// a Vendor specific <see cref="DbContext"/>).
    /// </para>
    /// </summary>
    /// <typeparam name="TAggregateRootEntity">The type of the aggregate root entity.</typeparam>
    public abstract class EntityDbContextRepository2Base<TAggregateRootEntity> : EntityDbContextRepositoryBaseBase<TAggregateRootEntity>
        where TAggregateRootEntity : class
    {

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="EntityDbContextRepository2Base&lt;TAggregateRootEntity&gt;"/> class.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        protected EntityDbContextRepository2Base(IEntityDbContext dbContext)
        {
            if (dbContext == null){throw new ArgumentNullException("dbContext");}

            this.DbContext = dbContext.InnerObject as DbContext;
            
            if (this.DbContext==null)
            {
                throw new ArgumentNullException("Given EntityDbContext did not contain a valid EF CodeFirst DbContext.");
            }
            
            this.DbSet = this.DbContext.Set<TAggregateRootEntity>();

            //_alwaysCommit = true;
        }

        /// <summary>
        /// Determines whether the entity has ever been saved before.
        /// <para>
        /// Intended to be invoked by <c>PersistOnCommit</c>
        /// </para>
        /// </summary>
        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
        /// <returns>
        ///   <c>true</c> if [is entity new] [the specified aggregate root entity]; otherwise, <c>false</c>.
        /// </returns>
        public override abstract bool IsEntityNew(TAggregateRootEntity aggregateRootEntity);
    }
}