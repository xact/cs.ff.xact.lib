using NUnit.Framework;
using XAct.Tests;

namespace XAct.Data.Tests
{
    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UnitTests1
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            IoCBootStrapper.Initialize();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test(Description = "")]
        public void UnitTest01()
        {
            //SOMECLASSNAME ctrl = new SOMECLASSNAME();
            Assert.IsTrue(true);
        }
    }


}


