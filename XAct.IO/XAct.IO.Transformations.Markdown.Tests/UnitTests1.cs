using NUnit.Framework;
using XAct.Tests;

namespace XAct.IO.Transformations.Tests
{
    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UnitTests1
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            IoCBootStrapper.Initialize();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test(Description = "")]
        public void UnitTest01()
        {
            SOMECLASSNAME ctrl = new SOMECLASSNAME();
            Assert.IsTrue(true);
        }
    }


}


