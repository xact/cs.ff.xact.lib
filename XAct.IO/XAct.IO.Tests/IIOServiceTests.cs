namespace TODO.Tests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using XAct;
    using XAct.IO;
    using XAct.Tests;

    /// <summary>
    /// NUNit Tests for TODO
    /// </summary>
    [TestFixture]
    public class IsolatedStorageIOServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }







        //TEST:FAILING (HANGS)
        [Test]
        [Ignore]
        public void ReadAncIsolatedStorageFile()
        {
            string lineText = "Test";

            IIsolatedStorageIOService ioService = DependencyResolver.Current.GetInstance<IIsolatedStorageIOService>();

            //Should have got back IsolatedStorage service:
            using (Stream stream = ioService.FileOpenWriteAsync("\\Test.txt").WaitAndGetResult())
            {
                using (StreamWriter streamWriter = stream.CreateStreamWriter())
                {
                    streamWriter.WriteLine(lineText + ": " + DateTime.Now);
                }
            }

            using (Stream stream = ioService.FileOpenReadAsync("$:\\Test.txt").WaitAndGetResult())
            {
                using (StreamReader streamWriter = stream.CreateStreamReader())
                {
                    string result = streamWriter.ReadLine();

                    Assert.AreEqual(result.Substring(0, lineText.Length), lineText);
                }
            }


            Assert.IsTrue(true);
        }

    }
}