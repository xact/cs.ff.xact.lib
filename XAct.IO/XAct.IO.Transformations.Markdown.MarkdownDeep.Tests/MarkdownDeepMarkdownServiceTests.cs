namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.IO;
    using XAct.IO.Transformations;
    using XAct.IO.Transformations.Implementations;
    using XAct.IO.Transformations.Services.Implementations;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class MarkdownDeepMarkdownServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                                    Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        [Test]
        public void CanGetMarkdownDeepMarkdownService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IMarkdownDeepMarkdownService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }


        [Test]
        public void CanGetMarkdownDeepMarkodwnServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IMarkdownDeepMarkdownService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(MarkdownDeepMarkdownService), service.GetType());
        }




        [Test]
        public void TransformMarkdownToDefaultHTML()
        {
            string result = DependencyResolver.Current.GetInstance<IMarkdownDeepMarkdownService>().Transform(
                @"#A Header#

Followed by

* One Point
* Two Points
   * And an indented point

FInally a hard line

-----

",Format.Default);


            Assert.IsTrue(result.StartsWith("<h1"));
        }




        [Test]
        public void WithLins()
        {
            string result = DependencyResolver.Current.GetInstance<IMarkdownDeepMarkdownService>().Transform(
                @"#A Header#

Followed by

* One Point
* Two Points
   * And an indented point

FInally a hard line


-----

* FAIL:{{LocalLink.htm}}
* FAIL: [[LocalLink.htm]]
* OK: [The Title](LocalLink.htm)
* FAIL: {LocalLink.htm}
* OK: ![The Title](LocalLink.png)
",Format.Default);


            Assert.IsTrue(result.StartsWith("<h1"));
        }


        [Test]
        public void AsLabel1()
        {
            string result = DependencyResolver.Current.GetInstance<IMarkdownDeepMarkdownService>().Transform(
                "First Name:", Format.Html);

            Assert.IsTrue(result.StartsWith("First"));
        }



        [Test]
        public void AsLabel1D()
        {
            string result = DependencyResolver.Current.GetInstance<IMarkdownDeepMarkdownService>().Transform(
                "Long label with nothing folding it and no need for html anywhere:", Format.Html);

            Assert.IsTrue(result.StartsWith("Long"));
        }


        [Test]
        public void AsLabel2()
        {
            string result = DependencyResolver.Current.GetInstance<IMarkdownDeepMarkdownService>().Transform(
                "**First** Name", Format.Html);

                Assert.IsTrue(result.StartsWith("<strong>"));
        }

        [Test]
        public void AsLabel3()
        {
            string result = DependencyResolver.Current.GetInstance<IMarkdownDeepMarkdownService>().Transform(
                @"**First** Name  
(*And some info about it*)", Format.Html);

            Assert.IsTrue(result.StartsWith("<strong>"));
        }



    }


}


