namespace XAct.IO.Transformations
{
    /// <summary>
    /// A specialization of the <see cref="IMarkdownService"/>
    /// to provide Markdown transformation services, using 
    /// the MarkdownSharp library.
    /// </summary>
    public interface IMarkdownSharpMarkdownService : IMarkdownService, IHasXActLibService
    {
        
    }
}