namespace XAct.IO.Transformations.Services.Implementations
{
    using System;
    using MarkdownSharp;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    ///   An XAct.IO.Transformations.Markdown.MarkdownSharp class.
    /// </summary>
    [DefaultBindingImplementation(typeof(IMarkdownService), BindingLifetimeType.Undefined, Priority.Normal /*OK: An Override Priority*/)]
    public class MarkdownSharpMarkdownService : IMarkdownSharpMarkdownService
    {
        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;

        private readonly MarkdownSharp.Markdown _markdownProcessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="MarkdownSharpMarkdownService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public MarkdownSharpMarkdownService(ITracingService tracingService, IEnvironmentService environmentService)
        {
            _markdownProcessor = new Markdown();

            //Note: not many options provided with the Options property...
            
            _tracingService = tracingService;
            _environmentService = environmentService;
        }


        /// <summary>
        /// Transforms the specified markdown text to html output.
        /// </summary>
        /// <param name="markdownText">The markdown text.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        public string Transform(string markdownText, Format format = Format.Auto)
        {
            // Create and setup Markdown translator
            switch (format)
            {
                case Format.Undefined:
                    throw new IndexOutOfRangeException("Format");
                case Format.Auto:
                    //When working within the context of a website
                    //return Web HTML.
                    if (_environmentService.HttpContext != null)
                    {
                        return _markdownProcessor.Transform(markdownText);
                    }
                    else
                    {
                        //We are not in a web context, so output should be made ready 
                        //for console output.
                        return markdownText; //TODO: Find a way to convert Markdown to Console
                        //It was suggested to convert to html, and then from there
                        //get InnerText, using: http://stackoverflow.com/questions/1349023/how-can-i-strip-html-from-text-in-net

                    }
                case Format.Console:
                    throw new NotImplementedException("Not Yet: only to HTML for now.");
                case Format.PDF:
                    throw new NotImplementedException("Not Yet: only to HTML for now.");
                case Format.InputFormat:
                    return markdownText;
                case Format.Default:
                case Format.Html:
                default:
                    return _markdownProcessor.Transform(markdownText);
            }

        }
    }
}