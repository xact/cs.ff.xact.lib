namespace XAct.IO.FS.Tests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using XAct;
    using XAct.IO;
    using XAct.IO.Implementations;
    using XAct.Tests;


    /// <summary>
    /// NUNit Tests for TODO
    /// </summary>
    [TestFixture]
    public class IFSIOServiceDirectoryTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }




        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }







        [Test]
        public void CanGetIFSIOService()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();
            Assert.IsNotNull(ioService);
        }


        [Test]
        public void CanGetIFSIOServiceOfExpectedType()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();
            Assert.AreEqual(typeof(FSIOService), ioService.GetType());
        }


        [Test]
        public void DirectoryExistsWithSystemVariables()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("%userprofile%", false).Result);
        }


        [Test]
        public void DirectoryExistsAsyncWithSystemVariables()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("%userprofile%", false).WaitAndGetResult());
        }



        [Test]
        public void DirectoryExists()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("c:\\windows", false).Result);
        }


        [Test]
        public void DirectoryExistsAsync()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("c:\\windows", false).WaitAndGetResult());
        }



        [Test]
        public void DirectoryDoesNotExists()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsFalse(ioService.DirectoryExistsAsync("c:\\doesnotexist", false).Result);
        }



        [Test]
        public void DirectoryDoesNotExistsAsync()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsFalse(ioService.DirectoryExistsAsync("c:\\doesnotexist", false).WaitAndGetResult());
        }


        [Test]
        public void DirectoryExistsUsingSystemVariables()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("%temp%").Result);

        }


        [Test]
        public void DirectoryExistsUsingSystemVariablesAsync()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("%temp%").WaitAndGetResult());

        }



        [Test]
        public void DirectoryCreateAndExists()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            const string dirPath = "%temp%\\serviceTest";

            if (!ioService.DirectoryExistsAsync(dirPath).Result)
            {
                ioService.DirectoryCreateAsync(dirPath).Wait();
            }
            Assert.IsTrue(ioService.DirectoryExistsAsync(dirPath).Result, "Test1 Failed.");

            if (ioService.DirectoryExistsAsync(dirPath).Result)
            {
                ioService.DirectoryDeleteAsync(dirPath).Wait();
            }
            Assert.IsFalse(ioService.DirectoryExistsAsync(dirPath).Result, "Test2 Failed");
        }


        [Test]
        public void DirectoryCreateAndExistsAsync()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            const string dirPath = "%temp%\\serviceTest";

            if (!ioService.DirectoryExistsAsync(dirPath).WaitAndGetResult())
            {
                ioService.DirectoryCreateAsync(dirPath).Wait();
            }
            Assert.IsTrue(ioService.DirectoryExistsAsync(dirPath).WaitAndGetResult(), "Test1 Failed.");

            if (ioService.DirectoryExistsAsync(dirPath).WaitAndGetResult())
            {
                ioService.DirectoryDeleteAsync(dirPath).Wait();
            }
            Assert.IsFalse(ioService.DirectoryExistsAsync(dirPath).WaitAndGetResult(), "Test2 Failed");
        }


        [Test]
        public void GetDirectoryFileNamesReturnsMoreThanOne()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            const string dirPath = "%userprofile%";
            //const string filePath = "%userprofile%\\unittest_check9.txt";

#pragma warning disable 168
            string check = Path.GetDirectoryName(dirPath);
            string check2 = Path.GetFullPath(dirPath);
#pragma warning restore 168
            string[] fileNames = ioService.GetDirectoryFileNamesAsync(dirPath).Result;

            Assert.IsTrue(fileNames.Length > 0);
        }



        [Test]
        public void GetDirectoryFileNamesReturnsMoreThanOneAsync()
        {

            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            const string dirPath = "%userprofile%";
            //const string filePath = "%userprofile%\\unittest_check10.txt";

#pragma warning disable 168
            string check = Path.GetDirectoryName(dirPath);
            string check2 = Path.GetFullPath(dirPath);
#pragma warning restore 168
            string[] fileNames = ioService.GetDirectoryFileNamesAsync(dirPath).WaitAndGetResult();

            Assert.IsTrue(fileNames.Length > 0);
        }




  
    }

}