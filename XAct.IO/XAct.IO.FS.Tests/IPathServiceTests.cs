﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.IO;
    using XAct.IO.Services.Implementations;
    using XAct.Tests;

    [TestFixture]
    public class IPathServiceTests
    {

        private string[] _paths;
    
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

            _paths = new[]
                {
                    @"c:\wada\wada2\wada3\foo.bar",
                    @"c:\wada\wada2\foo.bar",
                    @"c:\wada\foo.bar",
                    @"c:\foo.bar",
                };

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void CanGetIPathService()
        {
            //ARRANGE:
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ASSERT
            Assert.IsNotNull(pathService);
        }


        [Test]
        public void CanGetIPathServiceOfExpectedType()
        {
            //ARRANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ASSERT
            Assert.AreEqual(typeof(DefaultPathService), pathService.GetType());
        }


        [Test]
        public void GetFileName()
        {
            //ARRANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ASSERT/ACT
            _paths.ForEach(p => Assert.AreEqual("foo.bar", pathService.GetFileName(p)));
        }

        [Test]
        public void GetFileExtension()
        {
            //ARRANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ASSERT/ACT
            _paths.ForEach(p => Assert.AreEqual(".bar", pathService.GetExtension(p)));
        }
        [Test]
        public void GetFileNameWithoutExtension()
        {
            //ARRANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ASSERT/ACT
            _paths.ForEach(p => Assert.AreEqual("foo", pathService.GetFileNameWithoutExtension(p)));
        }
        [Test]
        public void GetDirectoryName()
        {
            //ARRANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ASSERT/ACT
            _paths.ForEach(p => Assert.AreEqual(p.Length - 8, pathService.GetDirectoryName(p).Length));
        }
        [Test]
        public void Combine()
        {
            //ARANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ASERT/ACT
            Assert.AreEqual(@"c:\foo\foo\bar.foo", pathService.Combine(@"c:\foo\foo","bar.foo"));
            Assert.AreEqual(@"c:\foo\foo\bar.foo",pathService.Combine(@"c:\foo\foo\","bar.foo"));
        }


        [Test]
        public void HasExtension()
        {
            //ARRANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ASSERT/ACT
            Assert.IsTrue(pathService.HasExtension(@"c:\foo\foo\bar.foo"));
            Assert.IsFalse(pathService.HasExtension(@"c:\foo\foo\bar"));

        }


        [Test]
        public void GetDirectoryNameWithoutSlash()
        {
            //ARRANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ACT
            string path = @"c:\Dir1\Dir2\Dir3";

            //ASSERT/ACT
            Assert.AreEqual("Dir3", pathService.GetFileName(path));

        }
        [Test]
        public void GetDirectoryNameWithSlash()
        {
            //ARRANGE
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //ACT
            string path = @"c:\Dir1\Dir2\Dir3";

            //ASSERT/ACT
            Assert.AreEqual("Dir3", pathService.GetFileName(path));

        }

    }
}
