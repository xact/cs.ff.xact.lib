namespace XAct.IO.FS.Tests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using XAct;
    using XAct.IO;
    using XAct.IO.Implementations;
    using XAct.Tests;


    /// <summary>
    /// NUNit Tests for TODO
    /// </summary>
    [TestFixture]
    public class IIOServiceDirectoryTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }



        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }







        [Test]
        public void CanGetIIOService()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();
            Assert.IsNotNull(ioService);
        }


        [Test]
        public void CanGetIIOServiceOfExpectedType()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();
            Assert.AreEqual(typeof(FSIOService), ioService.GetType());
        }


        [Test]
        public void DirectoryExistsWithSystemVariables()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("%userprofile%", false).Result);
        }


        [Test]
        public void DirectoryExists()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("c:\\windows", false).Result);
        }


        [Test]
        public void DirectoryDoesNotExists()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            Assert.IsFalse(ioService.DirectoryExistsAsync("c:\\doesnotexist", false).Result);
        }

        [Test]
        public void DirectoryExistsUsingSystemVariables()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            Assert.IsTrue(ioService.DirectoryExistsAsync("%temp%").Result);

        }

        [Test]
        public void DirectoryCreateAndExists()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string dirPath = "%temp%\\serviceTest";

            if (!ioService.DirectoryExistsAsync(dirPath).WaitAndGetResult())
            {
                ioService.DirectoryCreateAsync(dirPath).Wait();
            }
            Assert.IsTrue(ioService.DirectoryExistsAsync(dirPath).Result, "Test1 Failed.");

            if (ioService.DirectoryExistsAsync(dirPath).WaitAndGetResult())
            {
                ioService.DirectoryDeleteAsync(dirPath).Wait();
            }
            Assert.IsFalse(ioService.DirectoryExistsAsync(dirPath).Result, "Test2 Failed");
        }

        [Test]
        public void GetDirectoryFileNamesReturnsMoreThanOne()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string dirPath = "%userprofile%";
            //const string filePath = "%userprofile%\\unittest_check11.txt";

#pragma warning disable 168
            string check = Path.GetDirectoryName(dirPath);
            string check2 = Path.GetFullPath(dirPath);
#pragma warning restore 168
            string[] fileNames = ioService.GetDirectoryFileNamesAsync(dirPath).Result;

            Assert.IsTrue(fileNames.Length > 0);
        }




        static void EnsureFileDoesNotExist(string filePath)
        {
            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            if (ioService.FileExistsAsync(filePath).WaitAndGetResult())
            {
                ioService.FileDeleteAsync(filePath).Wait();
            }

        }


    }

}