﻿namespace XAct.IO.FS.Tests
{
    using System;
    using System.IO;
    using System.Linq;
    using NUnit.Framework;
    using XAct;
    using XAct.IO;
    using XAct.IO.Implementations;
    using XAct.Tests;

    /// <summary>
    /// NUNit Tests for TODO
    /// </summary>
    [TestFixture]
    public class IIOServiceFileTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIIOService()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();
            Assert.IsNotNull(ioService);
        }


        [Test]
        public void CanGetIIOServiceOfExpectedType()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();
            Assert.AreEqual(typeof(FSIOService), ioService.GetType());
        }


        [Test]
        public void FileExistsWorksWithNonExistentFiles()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            Assert.IsFalse(ioService.FileExistsAsync("c:\\NonExistentFile.txt").Result);
        }
        [Test]
        public void FileExistsWorksWithNonExistentFilesAsync()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            Assert.IsFalse(ioService.FileExistsAsync("c:\\NonExistentFile.txt").WaitAndGetResult());
        }

        [Test]
        public void FileExistsWorksWithExistentFiles()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string filePath = "%userprofile%\\unittest_check_A12.txt";

            EnsureFileDoesNotExist(filePath);

            Assert.IsFalse(ioService.FileExistsAsync(filePath).Result);
        }
        [Test]
        public void FileExistsWorksWithExistentFilesAsync()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string filePath = "%userprofile%\\unittest_check_A13.txt";

            EnsureFileDoesNotExist(filePath);

            Assert.IsFalse(ioService.FileExistsAsync(filePath).WaitAndGetResult());
        }
        //[Test]
        //public async void FileExistsWorksWithExistentFilesAsync2()
        //{

        //    IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

        //    const string filePath = "%userprofile%\\unittest_check_A14.txt";
            //EnsureFileDoesNotExist(filePath);

        //    Assert.IsFalse(await ioService.FileExistsAsync(filePath));
        //}


        /* NOT GAURANTEED TO COMPLETE IN TIME
        [Test]
        public void FileCreateWorksWithExistentFiles()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string filePath = "%userprofile%\\unittest_check_A16.txt";

            //EnsureFileDoesNotExist(filePath);
         * 
            Assert.IsFalse(ioService.FileExistsAsync(filePath).Result);
            ioService.FileOpenWriteAsync(filePath).Result.AppendAllText("test");

            Assert.IsTrue(ioService.FileExistsAsync(filePath).Result,"Check#1");

            ioService.FileDeleteAsync(filePath);

            Assert.IsFalse(ioService.FileExistsAsync(filePath).Result, "Check#2");
        }
        */

        [Test]
        public void FileCreateWorksWithExistentFilesAsync()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string filePath = "%userprofile%\\unittest_check_A17.txt";

            EnsureFileDoesNotExist(filePath);

            Assert.IsFalse(ioService.FileExistsAsync(filePath).Result);
            
            ioService.FileOpenWriteAsync(filePath).Result.AppendAllText("test");

            Assert.IsTrue(ioService.FileExistsAsync(filePath).WaitAndGetResult());

            ioService.FileDeleteAsync(filePath).Wait();

            Assert.IsFalse(ioService.FileExistsAsync(filePath).WaitAndGetResult());
        }



        [Test]
        public void FileCreateWorksAndSoDoesFileOpenRead()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string filePath = "%userprofile%\\unittest_check_A18.txt";

            EnsureFileDoesNotExist(filePath);

            Assert.IsFalse(ioService.FileExistsAsync(filePath).Result);
            ioService.FileOpenWriteAsync(filePath).WaitAndGetResult().AppendAllText("test");

            Assert.IsTrue(ioService.FileExistsAsync(filePath).WaitAndGetResult(), "Check #1");

            string check = ioService.FileOpenReadAsync(filePath).WaitAndGetResult().ReadToEnd();

            Assert.IsTrue(check.StartsWith("test"), "Check #2");

            ioService.FileDeleteAsync(filePath).Wait();

            Assert.IsFalse(ioService.FileExistsAsync(filePath).WaitAndGetResult(), "Check #3");
        }



        [Test]
        public void FileCreateWorksAndSoDoesFileOpenReadAsync()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string filePath = "%userprofile%\\unittest_check_A19.txt";

            EnsureFileDoesNotExist(filePath);
            
            Assert.IsFalse(ioService.FileExistsAsync(filePath).WaitAndGetResult());
            ioService.FileOpenWriteAsync(filePath).WaitAndGetResult().AppendAllText("test");

            Assert.IsTrue(ioService.FileExistsAsync(filePath).WaitAndGetResult(), "Check #1");

            string check = ioService.FileOpenReadAsync(filePath).WaitAndGetResult().ReadToEnd();

            Assert.IsTrue(check.StartsWith("test"), "Check #2");

            ioService.FileDeleteAsync(filePath).Wait();

            Assert.IsFalse(ioService.FileExistsAsync(filePath).WaitAndGetResult(), "Check #3");
        }



        /* NOT GAURANTEED TO COMPLETE IN TIME
        [Test]
        public void FileCreateWorksAndSoDoesFileReadWrittingSeveralLines()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string filePath = "%userprofile%\\unittest_check_A20.txt";

                        EnsureFileDoesNotExist(filePath);
Assert.IsFalse(ioService.FileExistsAsync(filePath).Result, "Check#1");
            string[] lines = new string[] { "test", "line2", "line3" };

            ioService.FileOpenWriteAsync(filePath).Result.AppendAllText(lines);

            Assert.IsTrue(ioService.FileExistsAsync(filePath).Result, "Check#2");

            string check = ioService.FileOpenReadAsync(filePath).Result.ReadToEnd();

            Assert.IsTrue(check.StartsWith("test"), "Check#3");


            string[] check2 = ioService.FileOpenReadAsync(filePath).Result.ReadLines().ToArray();

            Assert.AreEqual(3, check2.Length);
            Assert.IsTrue(check2[0].StartsWith("test"));

            ioService.FileDeleteAsync(filePath);

            Assert.IsFalse(ioService.FileExistsAsync(filePath).Result);
        }
         */



        [Test]
        public void FileCreateWorksAndSoDoesFileReadWrittingSeveralLinesAsync()
        {

            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            const string filePath = "%userprofile%\\unittest_check_A21.txt";

            EnsureFileDoesNotExist(filePath);
            Assert.IsFalse(ioService.FileExistsAsync(filePath).WaitAndGetResult());
            string[] lines = new string[] { "test", "line2", "line3" };

            ioService.FileOpenWriteAsync(filePath).WaitAndGetResult().AppendAllText(lines);

            Assert.IsTrue(ioService.FileExistsAsync(filePath).WaitAndGetResult());

            string check = ioService.FileOpenReadAsync(filePath).WaitAndGetResult().ReadToEnd();

            Assert.IsTrue(check.StartsWith("test"));


            string[] check2 = ioService.FileOpenReadAsync(filePath).WaitAndGetResult().ReadLines().ToArray();

            Assert.AreEqual(3, check2.Length);
            Assert.IsTrue(check2[0].StartsWith("test"));

            ioService.FileDeleteAsync(filePath).Wait();

            Assert.IsFalse(ioService.FileExistsAsync(filePath).WaitAndGetResult());
        }




        /// <summary>
        /// TODO: Describe Test.
        /// </summary>
        [Test]
        public void UnitTestWriteReadFromSameFile()
        {
            string lineText = "Test";


            IIOService ioService = DependencyResolver.Current.GetInstance<IIOService>();

            //Should have got back IsolatedStorage service:
            using (Stream stream = ioService.FileOpenWriteAsync("%userprofile%\\Test.txt").Result)
            {
                using (StreamWriter streamWriter = stream.CreateStreamWriter())
                {
                    streamWriter.WriteLine(lineText + ": " + DateTime.Now);
                }
            }

            using (Stream stream = ioService.FileOpenReadAsync("%userprofile%\\Test.txt").Result)
            {
                using (StreamReader streamWriter = stream.CreateStreamReader())
                {
                    string result = streamWriter.ReadLine();

                    Assert.AreEqual(result.Substring(0, lineText.Length), lineText);
                }
            }


            Assert.IsTrue(true);
        }

        /// <summary>
        /// TODO: Describe Test.
        /// </summary>
        [Test]
        public void UnitTestWriteReadFromSameFileAsync()
        {
            string lineText = "Test";


            IIOService ioService = DependencyResolver.Current.GetInstance<IIOService>();

            //Should have got back IsolatedStorage service:
            using (Stream stream = ioService.FileOpenWriteAsync("%userprofile%\\Test.txt").WaitAndGetResult())
            {
                using (StreamWriter streamWriter = stream.CreateStreamWriter())
                {
                    streamWriter.WriteLine(lineText + ": " + DateTime.Now);
                }
            }

            using (Stream stream = ioService.FileOpenReadAsync("%userprofile%\\Test.txt").WaitAndGetResult())
            {
                using (StreamReader streamWriter = stream.CreateStreamReader())
                {
                    string result = streamWriter.ReadLine();

                    Assert.AreEqual(result.Substring(0, lineText.Length), lineText);
                }
            }


            Assert.IsTrue(true);
        }


        static void EnsureFileDoesNotExist(string filePath)
        {
            IIOService ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

            if (ioService.FileExistsAsync(filePath).WaitAndGetResult())
            {
                ioService.FileDeleteAsync(filePath).Wait();
            }

        }
    }
}