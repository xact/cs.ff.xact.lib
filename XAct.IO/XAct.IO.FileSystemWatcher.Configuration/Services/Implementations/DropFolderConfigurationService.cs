namespace XAct.IO.Configuration.Implementations
{
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class DropFolderConfigurationService : IDropFolderConfigurationService
    {
        /// <summary>
        /// Gets the DropFolder settings from the underlying persistence media (ie, app.config, or other).
        /// </summary>
        /// <returns></returns>
        public XAct.IO.DropFolderSpecifications GetSettings()
        {
            object o = System.Configuration.ConfigurationManager.GetSection("dropFolderSettings");
            DropFolderSettingsSection io = o as DropFolderSettingsSection;
            if (io ==null)
            {
                return null;
            }


            DropFolderSpecifications result = io.Map();


            return result;

        }

    }
}