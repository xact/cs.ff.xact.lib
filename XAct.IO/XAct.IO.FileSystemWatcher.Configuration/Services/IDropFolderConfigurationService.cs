﻿
// ReSharper disable CheckNamespace

namespace XAct.IO.Configuration
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDropFolderConfigurationService: IHasXActLibService
    {
        /// <summary>
        /// Gets the DropFolder settings from the underlying persistence media (ie, app.config, or other).
        /// </summary>
        /// <returns></returns>
        XAct.IO.DropFolderSpecifications GetSettings();
    }
}
