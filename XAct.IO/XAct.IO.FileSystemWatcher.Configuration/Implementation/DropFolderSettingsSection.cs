﻿// ReSharper disable CheckNamespace
namespace XAct.IO.Configuration
// ReSharper restore CheckNamespace
{
    using System.Configuration;

    /// <summary>
    /// The configuration section used to configure
    /// the <see cref="IDropFolderService"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// An example of usage:
    /// <code>
    /// <![CDATA[
    ///   <configSections>
    ///     <section name ="dropFolderSettings" type="XAct.IO.Configruation.DropFolderSection, XAct.IO.FileSystemWatcher.Configuration"/>
    /// </configSections>
    /// ...
    /// <dropFolderSettings>
    ///   <dropFolders>
    ///     <dropFolder Directory="..." >
    ///       <successStrategy enabled="true" delay="0,0,1" directory="..."/>
    ///       <errorStrategy enabled="true" directory="..."/>
    ///     </dropFolder>
    ///     <dropFolder Directory="..."/>
    ///   </dropFolders>
    ///   <successStrategy enabled="true" delay="0,0,1" directory="..."/>
    ///   <errorStrategy enabled="true" directory="..."/>
    /// </dropFolderSettings>
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    [ConfigurationCollection(typeof(DropFolderDirectorySpecificationElementCollection))]
    public class DropFolderSettingsSection : ConfigurationSection
    {

        //protected override ConfigurationPropertyCollection Properties { get { return _properties; } }
        //private static ConfigurationPropertyCollection _properties = new ConfigurationPropertyCollection();

        //private static ConfigurationProperty _dropFoldersDef = new ConfigurationProperty("settings", typeof(DropFolderDirectorySpecificationElementCollection));
        //private static ConfigurationProperty _archiveDelayInDaysDef = new ConfigurationProperty("archiveSettings", typeof(ArchiveSettings));
        //private static ConfigurationProperty _archiveFolderDef = new ConfigurationProperty("archiveFolder", typeof(FolderSettings));
        //private static ConfigurationProperty _errorFolderDef = new ConfigurationProperty("errorFolder", typeof(FolderSettings));

        //public DropFolderSection()
        //{

        //    _properties.Add(_dropFoldersDef);
        //    _properties.Add(_archiveDelayInDaysDef);
        //    _properties.Add(_archiveFolderDef);
        //    _properties.Add(_errorFolderDef);
        //}


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DropFolderSettingsSection"/> is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        [ConfigurationProperty("enabled", IsRequired = false, DefaultValue = true)]
        public bool Enabled
        {
            get { return (bool)this["enabled"]; }
            set { this["enabled"] = value; }
        }



        /// <summary>
        /// Gets the collection of drop folder specifications.
        /// </summary>
        /// <value>The drop folders.</value>
        [ConfigurationProperty("dropFolders", IsRequired = false)]
        public DropFolderDirectorySpecificationElementCollection DropFolders
        {
            get { return this["dropFolders"] as DropFolderDirectorySpecificationElementCollection; }
        }


        /// <summary>
        /// Gets or sets the optional success stategy specifications.
        /// </summary>
        /// <value>The error strategy.</value>
        [ConfigurationProperty("successStrategy", IsRequired = false)]
        public DropFolderStrategySpecificationElement SuccessStrategy
        {
            get { 
                DropFolderStrategySpecificationElement o = this["successStrategy"] as DropFolderStrategySpecificationElement; 
                if (o == null)
                {
                    this["successStrategy"] = o = new DropFolderStrategySpecificationElement();
                }
                return o;
            }
            set { this["successStrategy"] = value; }
        }


        /// <summary>
        /// Gets or sets the optional error strategy specifications.
        /// </summary>
        /// <value>The error strategy.</value>
        [ConfigurationProperty("errorStrategy", IsRequired = false)]
        public DropFolderStrategySpecificationElement ErrorStategy
        {
            get
            {
                DropFolderStrategySpecificationElement o = this["errorStrategy"] as DropFolderStrategySpecificationElement;
                if (o == null)
                {
                    this["errorStrategy"] = o = new DropFolderStrategySpecificationElement();
                }
                return o;
            }
            set { this["errorStrategy"] = value; }
        }
    }
}
