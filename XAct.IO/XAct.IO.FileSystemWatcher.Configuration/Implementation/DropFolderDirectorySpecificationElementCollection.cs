﻿// ReSharper disable CheckNamespace
namespace XAct.IO.Configuration
// ReSharper restore CheckNamespace
{
    using System.Configuration;

    /// <summary>
    /// The collection of <see cref="DropFolderDirectorySpecificationElement"/>s
    /// nested within <see cref="DropFolderSettingsSection"/>
    /// </summary>
    [ConfigurationCollection(typeof(DropFolderDirectorySpecificationElement), CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class DropFolderDirectorySpecificationElementCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Gets the collection of properties.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The <see cref="T:System.Configuration.ConfigurationPropertyCollection"/> of properties for the element.
        /// </returns>
        protected override ConfigurationPropertyCollection Properties { get { return _properties; } }
// ReSharper disable InconsistentNaming
        private static readonly ConfigurationPropertyCollection _properties = new ConfigurationPropertyCollection();
// ReSharper restore InconsistentNaming


        /// <summary>
        /// Static constructor to initializes the 
        /// <see cref="DropFolderDirectorySpecificationElementCollection"/> class.
        /// </summary>
        static DropFolderDirectorySpecificationElementCollection()
        {

        }

        ///// <summary>
        ///// Initializes a new instance of the <see cref="DropFolderDirectorySpecificationCollection"/> class.
        ///// </summary>
        //public DropFolderDirectorySpecificationCollection()
        //{
        //}

        /// <summary>
        /// Gets the type of the <see cref="T:System.Configuration.ConfigurationElementCollection"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The <see cref="T:System.Configuration.ConfigurationElementCollectionType"/> of this collection.
        /// </returns>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        //More overrrides, this time so that it knows what to create each time
        //it encounters an Add child element:  
        /// <summary>
        /// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new DropFolderDirectorySpecificationElement();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to return the key for.</param>
        /// <returns>
        /// An <see cref="T:System.Object"/> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            string name = ((DropFolderDirectorySpecificationElement)element).Name;
            return name;
        }



        /// <summary>
        /// Gets or sets the <see cref="XAct.IO.Configuration.DropFolderDirectorySpecificationElement"/> at the specified index.
        /// </summary>
        /// <value></value>
        public DropFolderDirectorySpecificationElement this[int index]
        {
            get { return (DropFolderDirectorySpecificationElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);
                BaseAdd(index, value);
            }
        }

        /// <summary>
        /// Gets the <see cref="XAct.IO.Configuration.DropFolderDirectorySpecificationElement"/> with the specified name.
        /// </summary>
        /// <value></value>
        public new DropFolderDirectorySpecificationElement this[string name]
        {
            //Using Key found by above GetElementKey:
            get { return (DropFolderDirectorySpecificationElement)base.BaseGet(name); }

        }

        /// <summary>
        /// Gets the name used to identify this collection of elements in the configuration file when overridden in a derived class.
        /// </summary>
        /// <value></value>
        protected override string ElementName
        {
            get { return "dropFolder"; }
        }


    }
}
