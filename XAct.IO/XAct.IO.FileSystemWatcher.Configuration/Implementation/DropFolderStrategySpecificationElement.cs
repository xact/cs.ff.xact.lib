﻿// ReSharper disable CheckNamespace
namespace XAct.IO.Configuration
// ReSharper restore CheckNamespace
{
    using System;
    using System.Configuration;

    /// <summary>
    /// A <see cref="ConfigurationElement"/> to hold the settings required 
    /// for a <see cref="IDropFolderConfigurationService"/> to build a 
    /// <see cref="DropFolderResultStrategySpecification "/> as part of the
    /// returned <see cref="IDropFolderSpecifications"/>
    /// </summary>
    public class DropFolderStrategySpecificationElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets a value indicating whether this strategy is enabled.
        /// <para>
        /// Default value is <c>true</c>.
        /// </para>
        /// </summary>
        [ConfigurationProperty("enabled", IsRequired = false, DefaultValue = true)]
        public bool Enabled
        {
            get { return (bool)this["enabled"]; }
            set { this["enabled"] = value; }
        }

        /// <summary>
        /// Gets or sets the directory to move the error files to.
        /// <para>
        /// Default is null.
        /// </para>
        /// <para>
        /// If null, defaults to the <c>Directory</c> value of <see cref="DropFolderSettingsSection.ErrorStategy"/>
        /// </para>
        /// </summary>
        /// <value>The directory.</value>
        [ConfigurationProperty("directory", IsRequired = false, DefaultValue = null)]
        public string Directory
        {
            get { return (string)this["directory"]; }
            set { this["directory"] = value; }
        }

        /// <summary>
        /// Gets or sets the delay before files the <see cref="Action"/> (Archived or Deleted)
        /// if <see cref="Enabled"/> = true
        /// is taken place.
        /// </summary>
        /// <value>The delay.</value>
        [ConfigurationProperty("delay", IsRequired = false, DefaultValue = null)]
        public TimeSpan Delay
        {
            get
            {
                object o = this["delay"];
                return (TimeSpan)(o ?? new TimeSpan(14, 0, 0, 0));
            }
            set { this["delay"] = value; }
        }


        /// <summary>
        /// Gets or sets the archiving strategy to use on files in <see cref="Directory"/>
        /// once the <see cref="Delay"/> has passed.
        /// <para>
        /// Default value is <c>Move</c>
        /// </para>
        /// </summary>
        /// <value>The action.</value>
        [ConfigurationProperty("archiveAction", IsRequired = false, DefaultValue = XAct.IO.ArchivingAction.NoAction)]
        public XAct.IO.ArchivingAction ArchiveAction
        {
            get { return (XAct.IO.ArchivingAction)this["archiveAction"]; }
            set { this["archiveAction"] = value; }
        }


        /// <summary>
        /// If <see cref="ArchiveAction"/> is set to <c>Move</c>,
        /// moves the file to this directory after the <see cref="Delay"/>.
        /// </summary>
        [ConfigurationProperty("archiveTo", IsRequired = false, DefaultValue = null)]
        public string ArchiveMoveToDirectory
        {
            get { return (string)this["archiveTo"]; }
            set { this["archiveTo"] = value; }
        }

    }

}
