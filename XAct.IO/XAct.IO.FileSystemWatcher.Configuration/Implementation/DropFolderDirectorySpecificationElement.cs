﻿// ReSharper disable CheckNamespace
namespace XAct.IO.Configuration
// ReSharper restore CheckNamespace
{
    using System.Configuration;
    using XAct.Environment;

    /// <summary>
    /// The configuration element used to describe a single
    /// drop folder.
    /// <para>
    /// These setings are used by the IDropFolderConfigurationService 
    /// to create a <see cref="DropFolderDirectorySpecification"/>
    /// </para>
    /// </summary>
    public class DropFolderDirectorySpecificationElement : ConfigurationElement
    {

        /// <summary>
        /// Gets or sets a value indicating whether the dropFolder watcher should be enabled or not.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        [ConfigurationProperty("enabled", IsRequired = false, DefaultValue = true)]
        public bool Enabled
        {
            get { return (bool)this["enabled"]; }
            set { this["enabled"] = value; }
        }


        /// <summary>
        /// Gets or sets the *required* unique name for the drop folder specification, 
        /// used as the key within the .
        /// </summary>
        /// <value>The name.</value>
        [ConfigurationProperty("name", IsRequired = false,DefaultValue = null)]
        public string Name
        {
            get
            {
                
                string o = (string)this["name"];

                if (o.IsNullOrEmpty())
                {
                    IDateTimeService dt = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
                    IEnvironmentService e = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>();
                    o = "TMP_{0}_{1}".FormatStringInvariantCulture(dt.NowUTC.Ticks, e.Random.Random(0, 1000));
                    this["name"] = o;
                }

                return o;

            }
            set { this["name"] = value; }
        }

        /// <summary>
        /// Gets or sets an optional description.
        /// </summary>
        /// <value>The description.</value>
        [ConfigurationProperty("description", IsRequired = false, DefaultValue = null)]
        public string Description
        {
            get { return (string)this["description"]; }
            set { this["description"] = value; }
        }

        /// <summary>
        /// Gets or sets an optional string tag/texto that can be used for identification or other purpose.
        /// </summary>
        /// <value>The tag.</value>
        [ConfigurationProperty("tag", IsRequired = false, DefaultValue = null)]
        public string Tag
        {
            get { return (string)this["tag"]; }
            set { this["tag"] = value; }
        }
        /// <summary>
        /// Gets or sets the directory to watch.
        /// </summary>
        /// <value>The directory.</value>
        [ConfigurationProperty("directory", IsRequired = true, DefaultValue = null)]
        public string Directory
        {
            get { return (string)this["directory"]; }
            set { this["directory"] = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the dropFolder watcher should be enabled or not
        /// for child folders.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        [ConfigurationProperty("includeSubDirectories", IsRequired = false, DefaultValue = false)]
        public bool IncludeSubDirectories
        {
            get { return (bool)this["includeSubDirectories"]; }
            set { this["includeSubDirectories"] = value; }
        }


        /// <summary>
        /// Gets or sets the optional success strategy specifications.
        /// </summary>
        /// <value>The error strategy.</value>
        [ConfigurationProperty("successStrategy", IsRequired = false, DefaultValue = null)]
        public DropFolderStrategySpecificationElement SuccessStrategy
        {
            get
            {
                DropFolderStrategySpecificationElement o = this["successStrategy"] as DropFolderStrategySpecificationElement; 
                if (o == null)
                {
                    this["successStrategy"] = o = new DropFolderStrategySpecificationElement();
                }
                return o;
            }
            set { this["successStrategy"] = value; }
        }

        /// <summary>
        /// Gets or sets the optional error strategy specifications.
        /// </summary>
        /// <value>The error strategy.</value>
        [ConfigurationProperty("errorStrategy",IsRequired=false,DefaultValue=null)]
        public DropFolderStrategySpecificationElement ErrorStrategy
        {
            get
            {
                DropFolderStrategySpecificationElement o = this["errorStrategy"] as DropFolderStrategySpecificationElement;
                if (o == null)
                {
                    this["errorStrategy"] = o = new DropFolderStrategySpecificationElement();
                }
                return o;
            }
            set { this["errorStrategy"] = value; }
        }

    }
}
