// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.IO;
    using XAct.IO;
    using XAct.IO.Configuration;

    /// <summary>
    /// 
    /// </summary>
    public static class MiscExtensions
    {
        /// <summary>
        /// Maps the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static DropFolderSpecifications Map(this DropFolderSettingsSection element)
        {

            DropFolderSpecifications result = new DropFolderSpecifications();

            List<DropFolderDirectorySpecification> r = new List<DropFolderDirectorySpecification>();
            foreach(DropFolderDirectorySpecificationElement el in element.DropFolders)
            {
                r.Add(el.Map());
            }
            result.Directories = r.ToArray();

            result.Enabled = element.Enabled;

            result.ErrorStrategy = element.ErrorStategy.Map();
            result.SuccessStrategy = element.SuccessStrategy.Map();

            return result;
        }

        /// <summary>
        /// Maps the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static DropFolderResultStrategySpecification Map(this DropFolderStrategySpecificationElement element)
        {
            DropFolderResultStrategySpecification result = new DropFolderResultStrategySpecification();

            result.ArchiveAction = element.ArchiveAction;
            if (!element.ArchiveMoveToDirectory.IsNullOrEmpty())
            {
                result.ArchiveMoveTo = element.ArchiveMoveToDirectory;
            }
            if (!element.Directory.IsNullOrEmpty())
            {
                result.Directory = element.Directory;
            }
            result.Enabled = element.Enabled;

            return result;
        }

        /// <summary>
        /// Maps the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static DropFolderDirectorySpecification Map(this DropFolderDirectorySpecificationElement element)
        {
            DropFolderDirectorySpecification result = new DropFolderDirectorySpecification();

            if (!element.Directory.IsNullOrEmpty())
            {
                result.Directory = element.Directory;
            }
            result.Enabled = element.Enabled;
            result.ErrorStrategy = element.ErrorStrategy.Map();
            result.IncludeSubDirectories = element.IncludeSubDirectories;
            result.Name = element.Name;
            result.SuccessStrategy = element.SuccessStrategy.Map();
            result.Tag = element.Tag;

            return result;
        }
    }
}