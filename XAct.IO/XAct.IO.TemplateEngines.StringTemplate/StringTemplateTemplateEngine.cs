namespace XAct.IO.TemplateEngines
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using XAct.Services;

    /// <summary>
    ///   An XAct.IO.TemplateEngines.StringTemplate class.
    /// </summary>
    [DefaultBindingImplementation(typeof(ITemplateRenderingService), BindingLifetimeType.Undefined, Priority.Low /*OK: Don't have a preference between technologies*/)]
    public class StringTemplateTemplateEngine : IStringTemplateTemplateEngine 
    {


        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public ITemplateRenderingServiceConfiguration Configuration
        {
            get;
            private set;
        }
        //Notes:
        //       StringTemplateGroup group = new StringTemplateGroup("myGroup", @"c:\temp");

            //StringTemplate query = group.GetInstanceOf("body");

            //query.SetAttribute("someKey", "SomeValue");
            //query.SetAttribute("someFlag", true);
            //query.SetAttribute("aList","A|B|C|D|E".Split('|'));

            //Console.WriteLine(query.ToString());


        /// <summary>
        /// Initializes a new instance of the <see cref="StringTemplateTemplateEngine"/> class.
        /// </summary>
        /// <param name="serviceSettings">The service settings.</param>
        public StringTemplateTemplateEngine(ITemplateRenderingServiceConfiguration serviceSettings)
        {
            Configuration = serviceSettings;
        }

        #region ITemplateRenderingService Members

        /// <summary>
        /// Compiles the source string template to the the output string.
        /// </summary>
        /// <param name="templateText">The template text.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateString(string templateText, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Compiles the source template string to the given output stream.
        /// </summary>
        /// <param name="outputStream">The stream.</param>
        /// <param name="templateText">The template text.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateString(Stream outputStream, string templateText, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Compiles the source template file to the target string.
        /// </summary>
        /// <param name="templateFileName">Name of the template file.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateFile(string templateFileName, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Compiles the source template file to the given output stream.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="templateFileName">Name of the template file.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateFile(Stream outputStream, string templateFileName, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Compiles the source template text stream to the target string.
        /// </summary>
        /// <param name="textReader">The text reader.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateStream(TextReader textReader, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Compiles the source template text stream to the target output stream.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="textReader">The text reader.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateStream(Stream outputStream, TextReader textReader, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}