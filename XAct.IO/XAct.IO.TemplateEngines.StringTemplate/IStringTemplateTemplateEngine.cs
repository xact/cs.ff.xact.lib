﻿

namespace XAct.IO.TemplateEngines
{
    /// <summary>
    /// A Contract for a StringTemplates based implementation of 
    /// <see cref="ITemplateRenderingService"/>
    /// </summary>
    public interface IStringTemplateTemplateEngine : ITemplateRenderingService, IHasXActLibService
    {
    }
}
