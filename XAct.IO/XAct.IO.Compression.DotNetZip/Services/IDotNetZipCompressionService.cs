﻿
namespace XAct.IO.Compression
{
    /// <summary>
    /// A specialization of the <see cref="ICompressionService"/>
    /// to provide the service when using the 
    /// <c>DotNetZip</c> library.
    /// </summary>
    /// <remarks>
    /// <para>
    /// For background, you may be interested in: http://bit.ly/AmjQIH
    /// </para>
    /// </remarks>
    public interface IDotNetZipCompressionService : ICompressionService, IHasXActLibService
    {
    }
}
