﻿namespace XAct.IO.Compression.Services.Implementations

{
    using System.IO;
    using Ionic.Zip;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IDotNetZipCompressionService"/>
    /// to provide a Compression service that uses a
    /// <c>DotNetZip</c> library.
    /// </summary>
    /// <remarks>
    /// <para>
    /// For background, you may be interested in: http://bit.ly/AmjQIH
    /// </para>
    /// </remarks>
    /// <internal>
    /// Further reading:
    ///   <![CDATA[
    /// http://dotnetzip.codeplex.com/wikipage?title=CS-Examples&referringTitle=Examples
    /// ]]>
    ///   </internal>
    [DefaultBindingImplementation(typeof(ICompressionService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class DotNetZipCompressionService : IDotNetZipCompressionService
    {
        

        /// <summary>
        /// Initializes a new instance of the <see cref="DotNetZipCompressionService"/> class.
        /// </summary>
        public DotNetZipCompressionService()
        {
        }

        /// <summary>
        /// Adds all descendent files within <paramref name="directoryFullName"/> to the Zip file.
        /// </summary>
        /// <param name="outputZipFileFullName">The zip file path.</param>
        /// <param name="directoryFullName">The directory info.</param>
        public void AddDirectoryToZip(string outputZipFileFullName, string directoryFullName)
        {

// ReSharper disable SuggestUseVarKeywordEvident
            using (ZipFile zip = new ZipFile())
// ReSharper restore SuggestUseVarKeywordEvident
            {

                zip.AddDirectory(directoryFullName.ToString(),"\\");

                //string[] files = _ioService.GetDirectoryFiles(directory.ToString(), null, SearchOption.AllDirectories);


                //foreach (string filePath in files)
                //{

                //    FileInfo relativeFileName =
                //        new FileInfo(filePath).RelativeTo(relativeToDirectory, false);

                //    zip.AddFile(Path.GetFileName(relativeFileName.ToString()), Path.GetDirectoryName(relativeFileName.ToString()));
                //}

                //zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                
                //zip.MaxOutputSegmentSize = 2 * 1024 * 1024;   // 2mb

                zip.Save(outputZipFileFullName);
            }
        }

        /// <summary>
        /// Adds the specified file to the Zip file.
        /// <para>
        /// If <paramref name="relativeToDirectoryFullName"/> = null, defaults to <c>file.DirectoryInfo</c>
        /// 	</para>
        /// </summary>
        /// <param name="outputZipFileFullName">The zip file path.</param>
        /// <param name="fileFullName">The new file path.</param>
        /// <param name="relativeToDirectoryFullName">The relative to directory.</param>
        public void AddFileToZip(string outputZipFileFullName, string fileFullName, string relativeToDirectoryFullName = null)
        {



            using (ZipFile zip = new ZipFile())
            {
                //zip.AddDirectory(directory.ToString(), "\\");

                if (relativeToDirectoryFullName == null)
                {
                    zip.AddFile(fileFullName,"\\");
                }
                else
                {
                    FileInfo relativeFileInfo = new FileInfo(fileFullName).RelativeTo(new DirectoryInfo(relativeToDirectoryFullName), false);

                    // "\B\B.txt"

                    //string s = relativeFileInfo.Directory.ToString();

                    //if (s.EndsWith("\\"))
                    //{
                    //    s = s.Substring(0, s.Length - 1);
                    //}

                    //string[] sParts = s.Split('\\');


                    string folderPath = Path.GetDirectoryName(relativeFileInfo.ToString()) /*+ "\\"*/ ;

                    zip.AddFile(fileFullName.ToString(), folderPath);
                }

                zip.Save(outputZipFileFullName);

            }

        }

        /// <summary>
        /// Adds all descendent files within <paramref name="directoryFullName"/> to the Zip file.
        /// </summary>
        /// <param name="outputZipFileStream">The zip file stream.</param>
        /// <param name="directoryFullName">The directory of files to add.</param>
        public void AddDirectoryToZip(Stream outputZipFileStream, string directoryFullName)
        {
            using (ZipFile zip = new ZipFile())
            {

                zip.AddDirectory(directoryFullName.ToString(), "\\");

                zip.Save(outputZipFileStream);
            }
        }

        /// <summary>
        /// Adds the file to the zip file.
        /// <para>
        /// If <paramref name="relativeToDirectoryFullName"/> = null, defaults to <c>file.DirectoryInfo</c>
        /// 	</para>
        /// </summary>
        /// <param name="outputZipFileStream">The zip file stream.</param>
        /// <param name="fileFullName">The file to add.</param>
        /// <param name="relativeToDirectoryFullName"></param>
        public void AddFileToZip(Stream outputZipFileStream, string fileFullName, string relativeToDirectoryFullName = null)
        {
            using (ZipFile zip = new ZipFile())
            {
                //zip.AddDirectory(directory.ToString(), "\\");

                if (relativeToDirectoryFullName == null)
                {
                    zip.AddFile(fileFullName.ToString(), "\\");
                }
                else
                {
                    FileInfo relativeFileInfo = new FileInfo(fileFullName).RelativeTo(new DirectoryInfo(relativeToDirectoryFullName), false);
                    // "\B\B.txt"

                    //string s = relativeFileInfo.Directory.ToString();

                    //if (s.EndsWith("\\"))
                    //{
                    //    s = s.Substring(0, s.Length - 1);
                    //}

                    //string[] sParts = s.Split('\\');


                    string folderPath = Path.GetDirectoryName(relativeFileInfo.ToString()) /*+ "\\"*/ ;

                    zip.AddFile(fileFullName.ToString(), folderPath);
                }

                zip.Save(outputZipFileStream);

            }
        }
    }
}
