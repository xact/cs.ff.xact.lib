namespace XAct.IO.Tests
{
    using System;
    using System.IO;

    //Support class used to record events that were raised
    public class FileEvent
    {
        public DateTime Time { get; private set; }
        public string Name { get; private set; }
        public string FullPath { get; private set; }
        public WatcherChangeTypes ChangeTypes { get; private set; }

        public FileEvent(FileSystemEventArgs e)
        {
            Time = DateTime.Now;
            ChangeTypes = e.ChangeType;
            FullPath = e.FullPath;
            Name = e.Name;
        }
        public override string ToString()
        {
            string s = "{0}: {1}: {2}: {3}".FormatStringCurrentCulture(Time, ChangeTypes.ToString(),
                                                                       Name, FullPath);
            return s;
        }
    }
}