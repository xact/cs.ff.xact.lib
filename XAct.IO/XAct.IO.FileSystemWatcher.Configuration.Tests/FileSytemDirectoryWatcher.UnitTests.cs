namespace XAct.IO.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.IO.Configuration;
    using XAct.IO.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DirectoryWatcherServiceUnitTests
    {
        private IDirectoryWatcher _fileSystemDirectoryWatcherInstance;
        private IEnvironmentService _environmentService;
        private List<FileEvent> _eventsCaptured;


        private string _dirPath;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Bootstrap all services found by reflection:
                        Singleton<IocContext>.Instance.ResetIoC();


            //FSIOService fsioService = null;
            //FSIOService o = fsioService;

            //Create a list to capture events as they are raised
            //for tests to see if events raised in what order:
            _eventsCaptured = new List<FileEvent>();


            _environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //Use a subfolder within bin/debug:
            _dirPath = _environmentService.MapPath("WatchMe");

            //For good measure delete any tmp files we used in previous tests:
            DeleteTmpFiles();

            //Need the 
            IDirectoryWatcherService fileSystemWatcherService =
                DependencyResolver.Current.GetInstance<IDirectoryWatcherService>();

            DirectoryWatcherConfiguration config = new DirectoryWatcherConfiguration { Path = _dirPath };

            //Pick up Created -- and Dleted.
            config.WatcherChangeTypes = WatcherChangeTypes.All;

            _fileSystemDirectoryWatcherInstance =
                fileSystemWatcherService.Register(config);

            _fileSystemDirectoryWatcherInstance.Event += _fileSystemDirectoryWatcherInstance_Event;
            //XAct.IO.FileSystemDirectoryWatcherInstanceConfiguration 

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        void _fileSystemDirectoryWatcherInstance_Event(object sender, FileSystemEventArgs e)
        {
            _eventsCaptured.Add(new FileEvent(e));


        }


        /// <summary>
        /// Helper method to cleanup old test tmp files.
        /// </summary>
        private void DeleteTmpFiles()
        {
            string[] paths = Directory.GetFiles(_dirPath, "test*.txt");
            foreach (string path2 in paths)
            {
                File.Delete(path2);
            }
        }


        /// <summary>
        /// Helper method for unit tests to create test output files.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void MakeNewFile(string filePath)
        {
            using (Stream stream = File.Create(filePath))
            {
                //bool b1 = stream.CanWrite;

                stream.AppendAllText("X", true);
                //bool b2 = stream.CanWrite;
            }
        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_SectionIsParseable()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();


            object o = System.Configuration.ConfigurationManager.GetSection("dropFolderSettings");

            DropFolderSettingsSection io = o as DropFolderSettingsSection;

            Assert.IsNotNull(io);
        }



        [Test]
        public void Test_SectionIsParseable_EnsureMoreThanOneDropFoldersDefined()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();


            object o = System.Configuration.ConfigurationManager.GetSection("dropFolderSettings");

            DropFolderSettingsSection io = o as DropFolderSettingsSection;
            Assert.IsNotNull(io);

            Console.WriteLine(io.DropFolders.Count);
            Assert.IsTrue(io.DropFolders.Count > 3);
        }


        [Test]
        public void Test_SectionIsParseable_EnsureDropFolderEnabledAttributeWorkds()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();


            object o = System.Configuration.ConfigurationManager.GetSection("dropFolderSettings");

            DropFolderSettingsSection io = o as DropFolderSettingsSection;
            Assert.IsNotNull(io);

            

            Assert.IsTrue(io.DropFolders["China"].Enabled);
            Assert.IsFalse(io.DropFolders["Japan"].Enabled);
        }

    


        [Test]
        public void Test_SectionIsParseable_EnsureGeneralStrategyEnabledAttributeWorkds()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();


            object o = System.Configuration.ConfigurationManager.GetSection("dropFolderSettings");

            DropFolderSettingsSection io = o as DropFolderSettingsSection;
            Assert.IsNotNull(io);



            Assert.IsTrue(io.SuccessStrategy.Enabled);
            Assert.IsFalse(io.ErrorStategy.Enabled);
        }

        [Test]
        public void Test_SectionIsParseable_EnsureStrateyActionIsParsable()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();


            object o = System.Configuration.ConfigurationManager.GetSection("dropFolderSettings");

            DropFolderSettingsSection io = o as DropFolderSettingsSection;
            Assert.IsNotNull(io);



            Assert.IsTrue(io.SuccessStrategy.ArchiveAction == ArchivingAction.Move);
            Assert.IsTrue(io.ErrorStategy.ArchiveAction == ArchivingAction.NoAction);
        }

        [Test]
        public void Test_SectionIsParseable_EnsureStrategyDirectoryIsParsable()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();


            object o = System.Configuration.ConfigurationManager.GetSection("dropFolderSettings");

            DropFolderSettingsSection io = o as DropFolderSettingsSection;
            Assert.IsNotNull(io);



            Assert.IsNotNullOrEmpty(io.SuccessStrategy.Directory);
            Assert.IsNotNullOrEmpty(io.ErrorStategy.Directory);
            Assert.IsNotNullOrEmpty(io.DropFolders[0].Directory);
        }
        [Test]
        public void Test_SectionIsParseable_EnsureStrategyArchvieToDirectoryIsParsable()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();


            object o = System.Configuration.ConfigurationManager.GetSection("dropFolderSettings");

            DropFolderSettingsSection io = o as DropFolderSettingsSection;
            Assert.IsNotNull(io);



            Assert.IsNotNullOrEmpty(io.SuccessStrategy.ArchiveMoveToDirectory);
            Assert.IsNullOrEmpty(io.ErrorStategy.ArchiveMoveToDirectory);
            Assert.IsNullOrEmpty(io.DropFolders[0].SuccessStrategy.ArchiveMoveToDirectory);
            Assert.IsNullOrEmpty(io.DropFolders[0].ErrorStrategy.ArchiveMoveToDirectory);
        }


        [Test]
        public void Test_SectionIsParseable8()
        {
            IDropFolderConfigurationService dropFolderConfigurationService = XAct.DependencyResolver.Current.GetInstance<IDropFolderConfigurationService>();
           DropFolderSpecifications io =  dropFolderConfigurationService.GetSettings();


           Assert.IsTrue(io.SuccessStrategy.ArchiveAction == ArchivingAction.Move);
        }

    }


}


