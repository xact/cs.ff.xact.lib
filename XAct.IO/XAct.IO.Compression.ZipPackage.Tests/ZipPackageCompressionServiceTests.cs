namespace XAct.IO.Compression.Tests
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.IO.Compression.Implementations;
    using XAct.Services;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class ZipPackageCompressionServiceTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();


        }


        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetICompressionService()
        {
            ICompressionService zipService =
                DependencyResolver.Current.GetInstance<ICompressionService>();
            
            Assert.IsNotNull(zipService);
        }

        [Test]
        public void CanGetICompressionServiceOfExpectedType()
        {
            ICompressionService zipService =
                DependencyResolver.Current.GetInstance<ICompressionService>();

            Assert.AreEqual(typeof(ZipPackageCompressionService), zipService.GetType());
        }

        [Test]
        public void ZipUpAFolder()
        {

            IEnvironmentService environmentService = 
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            
            ICompressionService zipService =
                DependencyResolver.Current.GetInstance<ICompressionService>();

            string directoryFullName = environmentService.MapPath("FA");
            string outZipFullName = environmentService.MapPath(@"..\..\GoBabyGo.zip");

            if (File.Exists(outZipFullName))
            {
                File.Delete(outZipFullName);
            }
            Assert.IsFalse(File.Exists(directoryFullName),"File is not deleted. Can't start test");




            zipService.AddDirectoryToZip(outZipFullName, directoryFullName);


            
            Assert.IsTrue(File.Exists(outZipFullName));

            Assert.IsTrue(new FileInfo(outZipFullName).Length>0);


        }



                [Test]
        public void AddSingleFile01()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string fileFullName = environmentService.MapPath("FA\\a.txt");

            string outZipFullName = environmentService.MapPath(@"..\..\GoBabyGo2c.zip");

#pragma warning disable 168
            string directoryPath = environmentService.MapPath("FA\\");
#pragma warning restore 168

                    //zipService.AddDirectoryToZip();
            zipService.AddFileToZip(outZipFullName, fileFullName, null);

            Assert.IsTrue(true);
        }


        [Test]
        public void AddSingleFile02()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string fileFullName = environmentService.MapPath("FA\\FB\\b.txt");

            string outZipFullName = environmentService.MapPath(@"..\..\GoBabyGo2d.zip");

            zipService.AddFileToZip(outZipFullName, fileFullName, environmentService.MapPath("FA\\"));

            Assert.IsTrue(true);
        }

        [Test]
        public void AddSingleFile02b()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string fileFullName = environmentService.MapPath("FA\\FB\\b.txt");

            string outZip = environmentService.MapPath(@"..\..\GoBabyGo2d2.zip");

            zipService.AddFileToZip(outZip, fileFullName, environmentService.MapPath("FA"));

            Assert.IsTrue(true);
        }


        [Test]
        public void AddSingleFile03()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string fileFullName = environmentService.MapPath("FA\\FB\\b.txt");

            string outZipFullName = environmentService.MapPath(@"..\..\GoBabyGo2e.zip");

            zipService.AddFileToZip(outZipFullName, fileFullName, environmentService.MapPath("FA\\FB\\"));

            Assert.IsTrue(true);
        }

        [Test]
        public void AddSingleFile04()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string path = environmentService.MapPath("FA\\FB\\b.txt");

            string outZip = environmentService.MapPath(@"..\..\GoBabyGo2f.zip");

            zipService.AddFileToZip(outZip, path, environmentService.MapPath("FA\\FB"));

            Assert.IsTrue(true);
        }

    }





}


