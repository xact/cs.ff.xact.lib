namespace XAct.IO.Transformations.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class MarkdownSharpMarkdownServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest01()
        {
            string result = DependencyResolver.Current.GetInstance<IMarkdownService>().Transform(
                @"#A Header#

Followed by

* One Point
* Two Points
   * And an indented point

FInally a hard line

-----

",Format.Default);


            Assert.IsTrue(result.StartsWith("<h1"));
        }


        [Test]
        public void TransformMarkdownToDefaultHTML()
        {
            string result = DependencyResolver.Current.GetInstance<IMarkdownService>().Transform(
                @"#A Header#

Followed by

* One Point
* Two Points
   * And an indented point

FInally a hard line


-----

* FAIL:{{LocalLink.htm}}
* FAIL: [[LocalLink.htm]]
* OK: [The Title](LocalLink.htm)
* FAIL: {LocalLink.htm}
* OK: ![The Title](LocalLink.png)
", Format.Default);


            Assert.IsTrue(result.StartsWith("<h1"));
        }

    }
}


