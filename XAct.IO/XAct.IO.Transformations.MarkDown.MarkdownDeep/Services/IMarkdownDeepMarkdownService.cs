namespace XAct.IO.Transformations
{
    /// <summary>
    /// A specialization of the <see cref="IMarkdownService"/>
    /// to provide Markdown transformation services, using 
    /// the MarkdownDeep library.
    /// </summary>
    public interface IMarkdownDeepMarkdownService : IMarkdownService, IHasXActLibService
    {

    }
}