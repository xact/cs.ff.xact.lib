namespace XAct.IO.Transformations.Services.Implementations
{
    using System;
    using System.Text.RegularExpressions;
    using MarkdownDeep;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    ///   An XAct.IO.Transformations.MarkDown.MarkdownDeep class.
    /// </summary>
    [DefaultBindingImplementation(typeof(IMarkdownService), BindingLifetimeType.Undefined, Priority.Normal /*OK: An Override Priority*/)]
    public class MarkdownDeepMarkdownService : IMarkdownDeepMarkdownService
    {
        private readonly Markdown _markdownProcessor;
        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly bool _removeP;
        /// <summary>
        /// Initializes a new instance of the <see cref="MarkdownDeepMarkdownService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public MarkdownDeepMarkdownService(ITracingService tracingService,IEnvironmentService environmentService)
        {


            _tracingService = tracingService;
            _environmentService = environmentService;
            _markdownProcessor = new Markdown();
            //For API Settings: http://www.toptensoftware.com/markdowndeep/api
            _markdownProcessor.SafeMode = true;
            _markdownProcessor.ExtraMode = true;
            
            _markdownProcessor.FormatCodeBlock = FormatCodePrettyPrint;

            _removeP = true;

        }
        //private static Regex rxExtractLanguage = new Regex("^({{(.+)}}[\r\n])", RegexOptions.Compiled);
        private static readonly Regex rxExtractLanguage = new Regex("^(#!(.+)[\r\n])", RegexOptions.Compiled);
        private string FormatCodePrettyPrint(MarkdownDeep.Markdown m, string code)
        {
            // Try to extract the language from the first line    
            Match match = rxExtractLanguage.Match(code);
            string language = null;
            if (match.Success)
            {
                // Save the language        
                Group g = (Group) match.Groups[2];
                language = g.ToString().Trim();
                // Remove the first line        
                code = code.Substring(match.Groups[1].Length);
            }
            // If not specified, look for a link definition called "default_syntax" and  
            // grab the language from its title 
            if (language == null)
            {
                LinkDefinition d = m.GetLinkDefinition("default_syntax");
                if (d != null)
                {
                    language = d.title;
                }
            }
            // Common replacements    
            switch (language)
            {
                case "C#":
                case "c#":
                    language = "csharp";
                    break;
                case "C++":
                    language = "cpp";
                    break;
                default:
                    break;
            }

            // Wrap code in pre/code tags and add PrettyPrint attributes if necessary    
            if (string.IsNullOrEmpty(language))
            {
                return string.Format("<pre><code>{0}</code></pre>\n", code);
            }
            return string.Format("<pre class=\"prettyprint lang-{0}\"><code>{1}</code></pre>\n",
                                 language.ToLowerInvariant(),
                                 code);
        }


        /// <summary>
        /// Transforms the specified markdown text to html output.
        /// </summary>
        /// <param name="markdownText">The markdown text.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        public string Transform(string markdownText, Format format = Format.Auto)
        {
            // Create and setup Markdown translator
            switch (format)
            {
                case Format.Undefined:
                    throw new IndexOutOfRangeException("Format");
                case Format.Auto:
                    if (_environmentService.HttpContext != null)
                    {
                        return _markdownProcessor.Transform(markdownText);
                    }
                    //We are not in a web context, so output should be made ready 
                    //for console output.
                    return markdownText; //TODO: Find a way to convert Markdown to Console
                    //It was suggested to convert to html, and then from there
                    //get InnerText, using: http://stackoverflow.com/questions/1349023/how-can-i-strip-html-from-text-in-net
                case Format.Console:
                    throw new NotImplementedException("Not Yet: only to HTML for now.");
                case Format.PDF:
                    throw new NotImplementedException("Not Yet: only to HTML for now.");
                case Format.InputFormat:
                    return markdownText;
                case Format.Default:
                case Format.Html:
                default:
                    string result = _markdownProcessor.Transform(markdownText);
                    if (_removeP)
                    {
                        if (result.StartsWith("<p>"))
                        {
                            //Remove leading <p>
                            result = result.Remove(0, 3);
                            //Remove trailing </p>\n
                            result = result.Substring(0, result.Length - 5);
                        }
                    }
                    return result;
            }
        }

    }
}