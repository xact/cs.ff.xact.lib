namespace XAct.IO.Compression.Tests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.IO.Compression.Implementations;
    using XAct.IO.Compression.Services.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DotNetZipCompressionServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                        Singleton<IocContext>.Instance.ResetIoC();

        }


        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetICompressionService()
        {
            ICompressionService zipService =
                DependencyResolver.Current.GetInstance<ICompressionService>();

            Assert.IsNotNull(zipService);
        }


        [Test]
        public void CanGetICompressionServiceOfExpectedType()
        {
            ICompressionService zipService =
                DependencyResolver.Current.GetInstance<ICompressionService>();

            Assert.AreEqual(typeof(DotNetZipCompressionService), zipService.GetType());
        }



        [Test]
        public void CanZipUpAFolder()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string path = environmentService.MapPath("FA");

            string outZip = environmentService.MapPath(@"..\..\GoBabyGo2b.zip");

            zipService.AddDirectoryToZip(outZip, path);

            Assert.IsTrue(true);
        }

        [Test]
        public void AddSingleFile01()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string path = environmentService.MapPath("FA\\a.txt");

            string outZip = environmentService.MapPath(@"..\..\GoBabyGo2c.zip");

            zipService.AddFileToZip(outZip, path, null);

            Assert.IsTrue(true);
        }


        [Test]
        public void AddSingleFile02()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string path = environmentService.MapPath("FA\\FB\\b.txt");

            string outZip = environmentService.MapPath(@"..\..\GoBabyGo2d.zip");

            zipService.AddFileToZip(outZip, path, environmentService.MapPath("FA\\"));

            Assert.IsTrue(true);
        }

        [Test]
        public void AddSingleFile02b()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string path = environmentService.MapPath("FA\\FB\\b.txt");

            string outZip = environmentService.MapPath(@"..\..\GoBabyGo2d2.zip");

            zipService.AddFileToZip(outZip, path, environmentService.MapPath("FA"));

            Assert.IsTrue(true);
        }


        [Test]
        public void AddSingleFile03()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string path = environmentService.MapPath("FA\\FB\\b.txt");

            string outZip = environmentService.MapPath(@"..\..\GoBabyGo2e.zip");

            zipService.AddFileToZip(outZip, path, environmentService.MapPath("FA\\FB\\"));

            Assert.IsTrue(true);
        }

        [Test]
        public void AddSingleFile04()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            ICompressionService zipService = DependencyResolver.Current.GetInstance<ICompressionService>();

            string path = environmentService.MapPath("FA\\FB\\b.txt");

            string outZip = environmentService.MapPath(@"..\..\GoBabyGo2f.zip");

            zipService.AddFileToZip(outZip, path, environmentService.MapPath("FA\\FB"));

            Assert.IsTrue(true);
        }

    }


}


