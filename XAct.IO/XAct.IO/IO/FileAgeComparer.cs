﻿namespace XAct.IO
{
    using System;
    using System.Collections;
    using System.IO;

    /// <summary>
    /// Compare FileInfo objects (or strings which are the names of files) by datetime.
    /// </summary>
    public class FileAgeComparer : IComparer
    {
        private static class Constants
        {
            public static readonly string NotAKnownConstant = "Not a valid comparison ({0})";

            public static readonly string ComparisonObjectsMustBeOfSameType = "ErrMsg_ComparisonObjectsMustBeOfSameType	Comparisons objects ({0}, and {1}) must be of the same type.";
        }

        private readonly FileDateType _compareByWhatFileDateTime = FileDateType.Created;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FileAgeComparer"/> class.
        /// <para>
        /// Default behavior is to compare using 
        /// <see cref="FileDateType.Created"/>
        /// </para>
        /// </summary>
        public FileAgeComparer()
        {
        }

        /// <summary>
        /// ctor, compares files using specified criteria
        /// </summary>
        /// <param name="comparison"></param>
        public FileAgeComparer(FileDateType comparison)
        {
            _compareByWhatFileDateTime = comparison;
        }

        #endregion

        #region IComparer Members

        /// <summary>
        /// Compare two FileInfo objects and/or file-name strings
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <returns></returns>
        public int Compare(object x, object y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null)
            {
                return -1;
            }
            if (y == null)
            {
                return +1;
            }

            FileInfo file1 = null;
            FileInfo file2 = null;

            if (x is string && y is string)
            {
                file1 = new FileInfo(x as string);
                file2 = new FileInfo(y as string);
            }
            else if (x is FileInfo && y is FileInfo)
            {
                file1 = x as FileInfo;
                file2 = y as FileInfo;
            }
            else
            {
                throw new ArgumentException(
                    Constants.ComparisonObjectsMustBeOfSameType.FormatStringExceptionCulture(
                        x.GetType(),
                        y.GetType()
                        ));
            }

            switch (_compareByWhatFileDateTime)
            {
                case FileDateType.Created:
                    return file1.CreationTime.CompareTo(file2.CreationTime);
                case FileDateType.LastAccessed:
                    return file1.LastAccessTime.CompareTo(file2.LastAccessTime);
                case FileDateType.LastModified:
                    return file1.LastWriteTime.CompareTo(file2.LastWriteTime);
                default:
                    throw new ArgumentException(Constants.NotAKnownConstant);
            }

            #endregion
        }
    }
}