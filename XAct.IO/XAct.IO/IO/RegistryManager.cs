#undef INCLUDE_EXAMPLES

namespace XAct.IO
{
    using System;
    using System.Diagnostics;
    using Microsoft.Win32;

    /// <summary>
    ///   Collection of often used static methods for working with the Registry.
    /// </summary>
    /// <remarks>
    ///   Good Sources: http://www.csharphelp.com/archives2/archive430.html
    ///   <br />
    ///   http://www.csharphelp.com/archives/archive79.html
    /// </remarks>
    public class RegistryManager
    {
        /// <summary>
        ///   ToDo
        /// </summary>
        /// <param name = "qBaseKey"></param>
        /// <param name = "qKey"></param>
        /// <returns></returns>
        public static string KeyGetAsString(string qBaseKey, string qKey)
        {
            RegistryKey tResult;
            tResult = KeyGet(qBaseKey, qKey);
            if (tResult != null)
            {
                return tResult.GetValue(qKey).ToString();
            }
            return "";
        }

        /// <summary>
        ///   ToDO
        /// </summary>
        /// <param name = "baseKey"></param>
        /// <param name = "subKey"></param>
        /// <returns></returns>
        public static RegistryKey KeyGet(string baseKey,
                                         string subKey)
        {
            RegistryKey key;

            /* --- Resolve (create or open) Base Key --- */
            key = Registry.LocalMachine.OpenSubKey(baseKey, true);

            if (key == null)
            {
                key = Registry.LocalMachine.CreateSubKey(baseKey);
            }
            else
            {
                Trace.WriteLine("Base key resolved");
            }

            /* --- Resolve (create or open) Sub Key --- */
            key = Registry.LocalMachine.CreateSubKey(baseKey +
                                                     "\\" + subKey);
            if (key == null)
            {
                throw (new Exception("Error creating SubKey"));
            }

            Trace.WriteLine("Subkey resolved");

            return key;
        }

        /// <summary>
        ///   Set Single Registry Value
        /// </summary>
        /// <param name = "key"></param>
        /// <param name = "valueName"></param>
        /// <param name = "valueValue"></param>
        /// <remarks>
        ///   Storing and retrieving Registry values is quite simple. Listing 2 illustrates the syntax for writing a value to the Registry using the SetValue method of the RegistryKey class. There is no separate method for creating a value. If the value already exists, then SetValue will update it. If it does not already exist, it will automatically be created.
        /// </remarks>
        public static void KeySet(RegistryKey key, string valueName, object valueValue)
        {
            // In production code, you would validate the arguments
            key.SetValue(valueName, valueValue);
        }
    }

//End Class
}

//End NameSpace