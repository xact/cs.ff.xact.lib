#undef INCLUDE_EXAMPLES

namespace XAct.IO
{
    using System;
    using System.IO;

    /// <summary>
    ///   Generally static functions for manipulating FileNames/Paths, etc.
    /// </summary>
    public class FileName
    {
/*
function ShowFileVersion(pathspec){
  var fso, s = "";
  fso = new ActiveXObject("Scripting.FileSystemObject");
  s += fso.GetFileVersion(pathspec);
  if (s == String.Empty)
    s = "No version information available.";
  return(s);
}
 
*/



        /// <summary>
        ///   Adds slash to the end of paths.
        /// </summary>
        /// <param name = "fileName"></param>
        /// <returns></returns>
        public static string Normalize(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }

            string trimmedFileName = fileName.Trim();

            if (string.IsNullOrEmpty(trimmedFileName))
            {
                return trimmedFileName;
            }

            if (Path.GetExtension(fileName) == String.Empty)
            {
                if (fileName != String.Empty)
                {
                    string lastChar = fileName.Substring(fileName.Length - 1, 1);

                    if ((lastChar != "\\") && (lastChar != "/"))
                    {
                        trimmedFileName += "\\";
                    }
                }
            }
            return trimmedFileName;
        }


        /// <summary>
        ///   Returns Extension of File given, including "."
        /// </summary>
        /// <remarks>
        ///   If No Extension found, returns empty string.
        /// </remarks>
        /// <param name = "fileName"></param>
        /// <param name = "withDot">AddDot</param>
        /// <returns></returns>
        public static string Ext(string fileName, bool withDot)
        {
            fileName.ValidateIsNotNullOrEmpty("fileName");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            return withDot
                       ? fileName.Substring(fileName.LastIndexOf("."))
                       : fileName.Substring(fileName.LastIndexOf(".") + 1);
        }


        /// <summary>
        ///   Changes Extension of FileName (TheFile.Dat -> TheFile.Tmp)
        /// </summary>
        /// <param name = "fileName"></param>
        /// <param name = "qExt"></param>
        /// <returns></returns>
        public static string ChangeExt(string fileName, string qExt)
        {
            fileName.ValidateIsNotNullOrEmpty("fileName");
            qExt.ValidateIsNotNullOrEmpty("qExt");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            int tPos = fileName.LastIndexOf(".");
            string tExt = qExt.Substring(qExt.LastIndexOf("."));
            if (tPos > -1)
            {
                //Make sure the ext has no "." before you tack it on
                return fileName.Substring(0, tPos) + tExt;
            }
            return fileName + "." + tExt;
        }


    }
}