﻿
// ReSharper disable CheckNamespace
namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using XAct.IO.TemplateEngines;

    /// <summary>
    /// Interface for a template drive output system.
    /// <para>
    /// Implemented by <c>NVelocityTemplateService</c>
    /// in <c>XAct.IO.TemplateEngines.NVelocity</c>,
    /// in <c>XAct.IO.TemplateEngines.RazorHost</c>, and
    /// in <c>XAct.IO.TemplateEngines.StringTemplate</c>
    /// </para>
    /// </summary>
    public interface ITemplateRenderingService : IHasXActLibService
    {

        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        ITemplateRenderingServiceConfiguration Configuration { get; }

        /// <summary>
        /// Compiles the source string template to the the output string.
        /// </summary>
        /// <param name="templateText">The template text.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        string CompileTemplateString(string templateText,
                               IEnumerable<Assembly> optionalReferencedAssemblies = null,
                               object optionalContext = null,
                               bool avoidContextPrefixIfPossible=true);

        /// <summary>
        /// Compiles the source template string to the given output stream.
        /// </summary>
        /// <param name="outputStream">The stream.</param>
        /// <param name="templateText">The template text.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        long CompileTemplateString(Stream outputStream, string templateText,
                               IEnumerable<Assembly> optionalReferencedAssemblies = null,
                               object optionalContext = null,
                               bool avoidContextPrefixIfPossible=true);
        
        
        /// <summary>
        /// Compiles the source template file to the target string.
        /// </summary>
        /// <param name="templateFileName">Name of the template file.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        string CompileTemplateFile(string templateFileName,
                               IEnumerable<Assembly> optionalReferencedAssemblies = null,
                               object optionalContext = null,
                               bool avoidContextPrefixIfPossible=true);


        /// <summary>
        /// Compiles the source template file to the given output stream.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="templateFileName">Name of the template file.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        long CompileTemplateFile(Stream outputStream, string templateFileName,
                       IEnumerable<Assembly> optionalReferencedAssemblies = null,
                       object optionalContext = null,
            bool avoidContextPrefixIfPossible=true);

        /// <summary>
        /// Compiles the source template text stream to the target string.
        /// </summary>
        /// <param name="textReader">The text reader.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        string CompileTemplateStream(TextReader textReader,
                                     IEnumerable<Assembly>
                                         optionalReferencedAssemblies = null, object optionalContext = null,bool avoidContextPrefixIfPossible=true);

        /// <summary>
        /// Compiles the source template text stream to the target output stream.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="textReader">The text reader.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        long CompileTemplateStream(Stream outputStream, TextReader textReader,
                                     IEnumerable<Assembly>
                                         optionalReferencedAssemblies = null, object optionalContext = null,bool avoidContextPrefixIfPossible=true);

    }
}
