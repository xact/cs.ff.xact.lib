﻿namespace XAct.IO.TemplateEngines
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITemplateRenderingServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether to use Model/Context.
        /// container when setting barialbes.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [embed variables directly when possible]; otherwise, <c>false</c>.
        /// </value>
        bool EmbedVariablesDirectlyWhenPossible { get; set; }
    }
}
