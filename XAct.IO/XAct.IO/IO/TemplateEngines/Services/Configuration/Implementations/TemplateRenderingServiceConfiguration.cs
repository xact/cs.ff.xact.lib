﻿namespace XAct.IO.TemplateEngines.Implementations
{
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class TemplateRenderingServiceConfiguration : ITemplateRenderingServiceConfiguration ,IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether to use Model/Context.
        /// container when setting barialbes.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [embed variables directly when possible]; otherwise, <c>false</c>.
        /// </value>
        public bool EmbedVariablesDirectlyWhenPossible { get; set; }
    }
}