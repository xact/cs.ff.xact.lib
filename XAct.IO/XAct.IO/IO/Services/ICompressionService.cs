// ReSharper disable CheckNamespace
namespace XAct.IO.Compression
// ReSharper restore CheckNamespace
{
    using System.IO;

    /// <summary>
    /// The contract for a service to create and expand Zip and other Compression files.
    /// <para>
    /// <see cref="ICompressionService"/> is specialized further by
    /// <c>IZipPackageCompressionService</c> and <c>IDotNetZipCompressionService</c>
    /// in <c>XAct.Compression.ZipPackage</c> and <c>XAct.Compression.DotNetZip</c>
    /// </para>
    /// </summary>
    public interface ICompressionService : IHasXActLibService
    {
        /// <summary>
        /// Adds all descendent files within <paramref name="directoryFullName"/> to the Zip file.
        /// </summary>
        /// <param name="outputZipFileFullName">The zip file path.</param>
        /// <param name="directoryFullName">The directory info.</param>
        void AddDirectoryToZip(string outputZipFileFullName, string directoryFullName);

        /// <summary>
        /// Adds the specified file to the Zip file.
        /// <para>
        /// If <paramref name="relativeToDirectoryFullName"/> = null, defaults to <c>file.DirectoryInfo</c>
        /// </para>
        /// </summary>
        /// <param name="outputZipFileFullName">The zip file path.</param>
        /// <param name="fileFullName">The new file path.</param>
        /// <param name="relativeToDirectoryFullName">The relative to directory.</param>
        void AddFileToZip(string outputZipFileFullName, string fileFullName, string relativeToDirectoryFullName = null);

        /// <summary>
        /// Adds all descendent files within <paramref name="directoryFullName"/> to the Zip file.
        /// </summary>
        /// <param name="outputZipFileStream">The zip file stream.</param>
        /// <param name="directoryFullName">The directory of files to add.</param>
        void AddDirectoryToZip(Stream outputZipFileStream, string directoryFullName);

        /// <summary>
        /// Adds the file to the zip file.
        /// <para>
        /// If <paramref name="relativeToDirectoryFullName"/> = null, defaults to <c>file.DirectoryInfo</c>
        /// </para>
        /// </summary>
        /// <param name="outputZipFileStream">The zip file stream.</param>
        /// <param name="fileFullName">The file to add.</param>
        /// <param name="relativeToDirectoryFullName"></param>
        void AddFileToZip(Stream outputZipFileStream, string fileFullName, string relativeToDirectoryFullName = null);

        ///// <summary>
        ///// Adds a file to the specified zip file.
        ///// </summary>
        ///// <param name="outputZipFileStream">The zip file stream.</param>
        ///// <param name="filePathStream">The file to add.</param>
        ///// <param name="zipRelativeUri">The zip relative URI.</param>
        //void AddFileToZip(Stream outputZipFileStream, Stream filePathStream, Uri zipRelativeUri);
    }

}