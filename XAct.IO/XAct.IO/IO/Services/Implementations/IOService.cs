﻿//namespace XAct.IO.Implementations
//{
//    using System;
//    using System.IO;
//    using System.Threading.Tasks;
//    using XAct.Diagnostics;
//    using XAct.Enums;
//    using XAct.Services;

//    /// <summary>
//    /// Implementation of <see cref="IIOService" />
//    /// that in turn invokes <see cref="IIsolatedStorageIOService" />
//    /// or <see cref="IFSIOService" />, depending on whether the path
//    /// starts with <see cref="IsolatedStoragePrefix" />.
//    /// </summary>
//    [DefaultBindingImplementation(typeof(IIOService))]
//    public class IOService : XActLibServiceBase,  IIOService
//    {

//        /// <summary>
//        /// Gets the name of the Directory Accessibility test file.
//        /// </summary>
//        /// <value>
//        /// The name of the access test file.
//        /// </value>
//        public string DirectoryAccessTestFileName
//        {
//            get
//            {
//                return _accessTestFileName;
//            }
//        }
//        private const string _accessTestFileName = "_integrationTest.txt";

//        /// <summary>
//        /// The prefix used to indicate the path is relative to
//        /// IsolatedStorage (it will either be "$\..." or "$/..."
//        /// </summary>
//        public const string IsolatedStoragePrefix = "$";

//        private readonly string _typeName;
//        private readonly ITracingService _tracingService;
//        private readonly IIsolatedStorageIOService _isolatedStorageService;

//        /// <summary>
//        /// Gets a value indicating whether [prefer isolated storage].
//        /// </summary>
//        /// <value>
//        /// <c>true</c> if [prefer isolated storage]; otherwise, <c>false</c>.
//        /// </value>
//        public bool PreferIsolatedStorage
//        {
//            get { 
//                return XAct.Library.Settings.IO.PreferIsolatedStorage;
//            }
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="IOService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        /// <param name="isolatedStorageService">The isolated storage service.</param>
//        public IOService(ITracingService tracingService, IIsolatedStorageIOService isolatedStorageService)
//        {
//             _typeName  = this.GetType().Name;

//            tracingService.ValidateIsNotDefault("tracingService");
//            isolatedStorageService.ValidateIsNotDefault("isolatedStorageService");
//            _tracingService = tracingService;
//            _isolatedStorageService = isolatedStorageService;
//        }

//        private IFSIOService FSService
//        {
//            get
//            {
//                return _fsService ??
//                       (_fsService = DependencyResolver.Current.GetInstance<IFSIOService>());
//            }
//        }
//        private IFSIOService _fsService;


//        /// <summary>
//        /// Opens the specified resource with the given settings.
//        /// </summary>
//        /// <param name="uri">The URI.</param>
//        /// <param name="fileMode">The file mode.</param>
//        /// <param name="fileAccess">The file access.</param>
//        /// <param name="fileShare">The file share.</param>
//        /// <returns></returns>
//        public async Task<Stream> FileOpenAsync(string uri, XAct.IO.FileMode fileMode, XAct.IO.FileAccess fileAccess, XAct.IO.FileShare fileShare)
//        {

//            uri.ValidateIsNotNullOrEmpty("uri");

//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}({2},{3},{4},{5})", _typeName, "Open", uri, fileMode, fileAccess, fileShare);

//            return (IsUriForIsolatedStorage(uri))
//                       ? await _isolatedStorageService.FileOpenAsync(StripIsolatedStoragePrefix(uri), fileMode, fileAccess, fileShare)
//                       : await this.FSService.FileOpenAsync(uri, fileMode, fileAccess, fileShare);
//        }

//        /// <summary>
//        /// Opens the named resource and returns a readonly stream.
//        /// </summary>
//        /// <param name="uri">The full uri to the resource.</param>
//        /// <returns></returns>
//// ReSharper disable RedundantNameQualifier
//        public async Task<Stream> FileOpenReadAsync(string uri)
//// ReSharper restore RedundantNameQualifier
//        {
//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}({2})", _typeName, "OpenRead", uri);

//            uri.ValidateIsNotNullOrEmpty("uri");

//            return (IsUriForIsolatedStorage(uri))
//                       ? await _isolatedStorageService.FileOpenReadAsync(StripIsolatedStoragePrefix(uri))
//                       : await this.FSService.FileOpenReadAsync(uri);
//        }

//        /// <summary>
//        /// Opens the named resource and returns a write only stream.
//        /// <para>
//        /// Note that by default Windows will happily open a stream to replace an existing file,
//        /// so check first whether a file exists.
//        /// </para>
//        /// </summary>
//        /// <param name="uri">The uri.</param>
//        /// <param name="replaceAnyIfFound">if set to <c>true</c> replace any if file already exists.</param>
//        /// <param name="append">if set to <c>true</c> opens the stream for appending.</param>
//        /// <returns></returns>
//// ReSharper disable RedundantNameQualifier
//#pragma warning disable 1998
//        public async Task<Stream> FileOpenWriteAsync(string uri, bool replaceAnyIfFound = true, bool append = false)
//#pragma warning restore 1998
//// ReSharper restore RedundantNameQualifier
//        {
//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}({2},replaceAnyIfFound={3},append={4})", _typeName, "OpenWrite", uri,replaceAnyIfFound,append);

//            uri.ValidateIsNotNullOrEmpty("uri");

//            return (IsUriForIsolatedStorage(uri))
//                       ? _isolatedStorageService.FileOpenWriteAsync(StripIsolatedStoragePrefix(uri), replaceAnyIfFound, append).Result
//                       : this.FSService.FileOpenWriteAsync(uri, replaceAnyIfFound, append).Result;
//        }

//        /// <summary>
//        /// Opens the named resource and returns a <see cref="StreamReader"/>.
//        /// </summary>
//        /// <param name="uri">The uri.</param>
//        /// <returns>
//        /// A <see cref="StreamReader"/>.
//        /// </returns>
//// ReSharper disable RedundantNameQualifier
//        [Obsolete]
//        public async Task<StreamReader> FileOpenTextAsync(string uri)
//            // ReSharper restore RedundantNameQualifier
//        {
//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}({2})", _typeName, "OpenText", uri);

//            uri.ValidateIsNotNullOrEmpty("uri");

//            return (IsUriForIsolatedStorage(uri))
//#pragma warning disable 612,618
//                       ? await _isolatedStorageService.FileOpenTextAsync(StripIsolatedStoragePrefix(uri))
//#pragma warning restore 612,618
//#pragma warning disable 612,618
//                       : await this.FSService.FileOpenTextAsync(uri);
//#pragma warning restore 612,618
//        }

//        /// <summary>
//        /// Replaces the destination, with the source,
//        /// after moving the destination to a Backup position.
//        /// </summary>
//        /// <param name="sourceUri">Name of the source file.</param>
//        /// <param name="destinationUri">Name of the destination file.</param>
//        /// <param name="destinationBackupUri">Name of the destination backup file.</param>
//        public async Task FileReplaceAsync(string sourceUri, string destinationUri, string destinationBackupUri = null)
//        {
//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}({2},{3},{4})", _typeName, "ReplaceFile", sourceUri, destinationUri, destinationBackupUri);

            
//            sourceUri.ValidateIsNotNullOrEmpty("sourceUri");

//            if (IsUriForIsolatedStorage(sourceUri))
//            {
//               await _isolatedStorageService.FileReplaceAsync(StripIsolatedStoragePrefix(sourceUri), destinationUri,
//                                                    destinationBackupUri);
//            }
//            else
//            {
//                await this.FSService.FileReplaceAsync(sourceUri, destinationUri, destinationBackupUri);
//            }
//        }

//        /// <summary>
//        /// Copies the specified source resource to the target URI.
//        /// </summary>
//        /// <param name="sourceUri">The source URI.</param>
//        /// <param name="destinationUri">The destination URI.</param>
//        /// <param name="overwriteAllowed">if set to <c>true</c> [overwrite] any existing file.</param>
//        /// <internal>5/8/2011: Sky</internal>
//        public async Task FileCopyAsync(string sourceUri, string destinationUri, bool overwriteAllowed = false)
//        {
//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}({2},{3},{4})", _typeName, "CopyFile", sourceUri,destinationUri,overwriteAllowed);

//            sourceUri.ValidateIsNotNullOrEmpty("sourceUri");

//            if (IsUriForIsolatedStorage(sourceUri))
//            {
//                await _isolatedStorageService.FileCopyAsync(StripIsolatedStoragePrefix(sourceUri), destinationUri,
//                                                    overwriteAllowed);
//            }
//            else
//            {
//                await this.FSService.FileCopyAsync(sourceUri, destinationUri, overwriteAllowed);
//            }
//        }

//        /// <summary>
//        /// Creates a new resource at the specified location, returning a stream.
//        /// </summary>
//        /// <param name="uri">The uri.</param>
//        /// <returns></returns>
//// ReSharper disable RedundantNameQualifier
//        [Obsolete]
//        public async Task<Stream> FileCreateAsync(string uri)
//// ReSharper restore RedundantNameQualifier
//        {
//            uri.ValidateIsNotNullOrEmpty("uri");

//            return (IsUriForIsolatedStorage(uri))
//#pragma warning disable 612,618
//                       ? await _isolatedStorageService.FileCreateAsync(StripIsolatedStoragePrefix(uri))
//#pragma warning restore 612,618
//#pragma warning disable 612,618
//                       : await this.FSService.FileCreateAsync(uri);
//#pragma warning restore 612,618
//        }

//        /// <summary>
//        /// Deletes the specified URI.
//        /// </summary>
//        /// <param name="uri">The URI.</param>
//        /// <internal>5/8/2011: Sky</internal>
//        public async Task FileDeleteAsync(string uri)
//        {
//            uri.ValidateIsNotNullOrEmpty("uri");

//            if (IsUriForIsolatedStorage(uri))
//            {
//                await _isolatedStorageService.FileDeleteAsync(StripIsolatedStoragePrefix(uri));
//            }
//            else
//            {
//                await this.FSService.FileDeleteAsync(uri);
//            }
//        }

//        /// <summary>
//        /// Determines if the specified resource exists.
//        /// </summary>
//        /// <param name="uri">The URI.</param>
//        /// <returns></returns>
//        /// <internal>5/8/2011: Sky</internal>
//        public async Task<bool> FileExistsAsync(string uri)
//        {
//            uri.ValidateIsNotNullOrEmpty("uri");

//            return (IsUriForIsolatedStorage(uri))
//                       ? await _isolatedStorageService.FileExistsAsync(StripIsolatedStoragePrefix(uri))
//                       : await this.FSService.FileExistsAsync(uri);
//        }

//        /// <summary>
//        /// Moves the specified resource.
//        /// <para>
//        /// Note that there must be no file in the destinationUri.
//        /// If there is, it should be deleted first,
//        /// using <see cref="FileDeleteAsync"/>
//        /// 	</para>
//        /// </summary>
//        /// <param name="sourceUri">Name of the source file.</param>
//        /// <param name="destinationUri">Name of the destination file.</param>
//        /// <param name="overWriteAllowed">if set to <c>true</c> [over write allowed].</param>
//        /// <internal>5/8/2011: Sky</internal>
//        public async Task FileMoveAsync(string sourceUri, string destinationUri, bool overWriteAllowed = false)
//        {
//            sourceUri.ValidateIsNotNullOrEmpty("sourceUri");

//            if (IsUriForIsolatedStorage(sourceUri))
//            {
//                await _isolatedStorageService.FileMoveAsync(StripIsolatedStoragePrefix(sourceUri), destinationUri, overWriteAllowed);
//            }
//            else
//            {
//                await this.FSService.FileMoveAsync(sourceUri, destinationUri, overWriteAllowed);
//            }
//        }

//        /// <summary>
//        /// Appends the given string to the specified resource.
//        /// </summary>
//        /// <param name="uri">The URI.</param>
//        /// <param name="contents">The contents.</param>
//        /// <internal>5/8/2011: Sky</internal>
//        [Obsolete]
//        public async Task FileAppendAllTextAsync(string uri, string contents)
//        {
//            uri.ValidateIsNotNullOrEmpty("uri");

//            if (IsUriForIsolatedStorage(uri))
//            {
//#pragma warning disable 612,618
//                await _isolatedStorageService.FileAppendAllTextAsync(StripIsolatedStoragePrefix(uri), contents);
//#pragma warning restore 612,618
//            }
//            else
//            {
//#pragma warning disable 612,618
//                await this.FSService.FileAppendAllTextAsync(uri, contents);
//#pragma warning restore 612,618
//            }
//        }

//        /// <summary>
//        /// A wrapping of Directory.GetFiles
//        /// <para>
//        /// At present, don't think this deserves its own service. We'll see.
//        /// </para>
//        /// </summary>
//        /// <param name="directoryName">Name of the directory.</param>
//        /// <param name="searchPattern">The search pattern.</param>
//        /// <param name="hierarchicalOperationOption">The hierarchical operation option.</param>
//        /// <returns></returns>
//        public async Task<string[]> GetDirectoryFilesAsync(string directoryName, string searchPattern = null, XAct.HierarchicalOperationOption hierarchicalOperationOption = HierarchicalOperationOption.TopOnly)
//        {
//            directoryName.ValidateIsNotNullOrEmpty("directoryName");

//            return (IsUriForIsolatedStorage(directoryName))
//                       ? await _isolatedStorageService.GetDirectoryFileNamesAsync(StripIsolatedStoragePrefix(directoryName), searchPattern, hierarchicalOperationOption)
//                       : await this.FSService.GetDirectoryFileNamesAsync(directoryName, searchPattern, hierarchicalOperationOption);
//        }

//        /// <summary>
//        /// Creates the directory.
//        /// </summary>
//        /// <param name="directoryPath">The directory path.</param>
//        /// <returns></returns>
//// ReSharper disable RedundantNameQualifier
//        public async Task DirectoryCreateAsync(string directoryPath)
//// ReSharper restore RedundantNameQualifier
//        {
//            directoryPath.ValidateIsNotNullOrEmpty("directoryPath");

//            if (IsUriForIsolatedStorage(directoryPath))
//            {

//                await _isolatedStorageService.DirectoryCreateAsync(StripIsolatedStoragePrefix(directoryPath));
//            }
//            else
//            {
//                await this.FSService.DirectoryCreateAsync(directoryPath);
//            }
//        }


//        /// <summary>
//        /// Determines if the directory exists.
//        /// </summary>
//        /// <param name="directoryPath">The directory path.</param>
//        /// <param name="createIfMissing">if set to <c>true</c> [create if missing].</param>
//        /// <returns></returns>
//        public async Task<bool> DirectoryExistsAsync(string directoryPath, bool createIfMissing=false)
//        {
//            directoryPath.ValidateIsNotNullOrEmpty("directoryPath");

//            return (IsUriForIsolatedStorage(directoryPath))
//                       ? await _isolatedStorageService.DirectoryExistsAsync(StripIsolatedStoragePrefix(directoryPath), createIfMissing)
//                       : await this.FSService.DirectoryExistsAsync(directoryPath, createIfMissing);
            
//        }

//        /// <summary>
//        /// Directories the delete.
//        /// </summary>
//        /// <param name="directoryPath">The directory path.</param>
//        public async Task DirectoryDeleteAsync(string directoryPath)
//        {
//            if (IsUriForIsolatedStorage(directoryPath))
//            {
//                await _isolatedStorageService.DirectoryDeleteAsync(directoryPath);
//                return;
//            }

//            await this.FSService.DirectoryDeleteAsync(directoryPath);
//        }

//        /// <summary>
//        /// Determines whether the given Uri starts with the IsolatedStorage indicator.
//        /// </summary>
//        /// <param name="path">The path.</param>
//        /// <returns>
//        ///   <c>true</c> if [is URI for isolated storage] [the specified path]; otherwise, <c>false</c>.
//        /// </returns>
//        public bool IsUriForIsolatedStorage(string path)
//        {

//            bool result = path.StartsWith(IsolatedStoragePrefix + ":\\");

//                //System.Diagnostics.Debug.Assert(
//                //    !(result == false  && this.PreferIsolatedStorage),
//                //    "PreferIsolatedStorage=true, yet requesting: {0}".FormatStringInvariantCulture(path));

//            return result;

//            ////if path starts with : $:\\, use isolated storage:
//            //return (
//            //    //NO:$(path.StartsWith(IsolatedStoragePrefix + "\\")) 
//            //    //NO: | 
//            //    ////NO: (path.StartsWith(IsolatedStoragePrefix + "/"))
//            //    ////NO: |
//            //    (path.StartsWith(IsolatedStoragePrefix + ":\\"))
//            //    );
//        }

//        private static string StripIsolatedStoragePrefix(string path)
//        {
//            //As we know this is for $:\\ we can strip off the "$:\" part and leave it
//            //for the IsolatedStorageService to deal with:
//            if (path.Substring(1,1)==":")
//            {
//                return path.Substring(IsolatedStoragePrefix.Length + 2);
//            }
//            return path.Substring(IsolatedStoragePrefix.Length + 1);
//        }



//        /// <summary>
//        /// Gets the portable information about the file.
//        /// </summary>
//        /// <param name="fileFullName">Full name of the file.</param>
//        /// <returns></returns>
//        /// <exception cref="System.NotImplementedException"></exception>
//        public async Task<IFileInfo> GetFileInfoAsync(string fileFullName)
//        {
//            return  await XAct.Threading.TaskConstants<IFileInfo>.NotImplemented;
//        }



//        /// <summary>
//        /// Sets the directory's dates.
//        /// </summary>
//        /// <param name="fileFullName">Full name of the directory.</param>
//        /// <param name="type">The type.</param>
//        /// <param name="dateTimeUtc">The date time UTC.</param>
//        public async Task FileSetDateTimeAsync(string fileFullName, AuditableEvent type, DateTime dateTimeUtc)
//        {
//            if (IsUriForIsolatedStorage(fileFullName))
//            {
//                await _isolatedStorageService.FileSetDateTimeAsync(StripIsolatedStoragePrefix(fileFullName), type, dateTimeUtc);
//            }
//            else
//            {
//                await this.FSService.FileSetDateTimeAsync(fileFullName, type, dateTimeUtc);
//            }


//        }

//        /// <summary>
//        /// Sets the Directorys dates.
//        /// </summary>
//        /// <param name="directoryFullName">Full name of the directory.</param>
//        /// <param name="type">The type.</param>
//        /// <param name="dateTimeUtc">The date time UTC.</param>
//        public async Task DirectorySetDateTimeAsync(string directoryFullName, AuditableEvent type, DateTime dateTimeUtc)
//        {
//            if (IsUriForIsolatedStorage(directoryFullName))
//            {
//                await _isolatedStorageService.FileSetDateTimeAsync(StripIsolatedStoragePrefix(directoryFullName), type, dateTimeUtc);
//            }
//            else
//            {
//                await this.FSService.FileSetDateTimeAsync(directoryFullName, type, dateTimeUtc);
//            }


//        }


//        /// <summary>
//        /// Gets the directory file names.
//        /// </summary>
//        /// <param name="directoryFullName">Full name of the directory.</param>
//        /// <param name="searchPattern">The search pattern.</param>
//        /// <param name="searchOption">The search option.</param>
//        /// <returns></returns>
//        public async Task<string[]> GetDirectoryFileNamesAsync(string directoryFullName, string searchPattern = null, HierarchicalOperationOption searchOption = HierarchicalOperationOption.TopOnly)
//        {
//            if (IsUriForIsolatedStorage(directoryFullName))
//            {
//                return await _isolatedStorageService.GetDirectoryFileNamesAsync(StripIsolatedStoragePrefix(directoryFullName), searchPattern, searchOption);
//            }
//            else
//            {
//                return await FSService.GetDirectoryFileNamesAsync(directoryFullName, searchPattern, searchOption);
//            }
//        }


//        /// <summary>
//        /// Checks the Directory's accessibility.
//        /// </summary>
//        /// <param name="directoryFullName">Full name of the directory.</param>
//        /// <param name="tryForceCreateDirectory">if set to <c>true</c> [try force create directory].</param>
//        public async Task<DirectoryAccessibilityCheckResults> DirectoryCheckAccessibilityAsync(
//            string directoryFullName,
//            bool tryForceCreateDirectory)
//        {
//            if (IsUriForIsolatedStorage(directoryFullName))
//            {
//                return await _isolatedStorageService.DirectoryCheckAccessibilityAsync(
//                    StripIsolatedStoragePrefix(directoryFullName),
//                    tryForceCreateDirectory);
                
//            }
//            return await FSService.DirectoryCheckAccessibilityAsync(
//                directoryFullName,
//                tryForceCreateDirectory);

//        }


//        /// <summary>
//        /// Files the check accessibility.
//        /// </summary>
//        /// <param name="fileFullName">Full name of the file.</param>
//        /// <param name="desiredFileAccess">The desired file access.</param>
//        /// <param name="desiredFileShare">The desired file share.</param>
//        /// <returns></returns>
//        public async Task<bool> FileCheckAccessibilityAsync(string fileFullName, 
//            IO.FileAccess desiredFileAccess = IO.FileAccess.Read, IO.FileShare desiredFileShare = IO.FileShare.Read)
//        {
//            return IsUriForIsolatedStorage(fileFullName)
//                ? await _isolatedStorageService.FileCheckAccessibilityAsync(StripIsolatedStoragePrefix(fileFullName), desiredFileAccess, desiredFileShare)
//                : await FSService.FileCheckAccessibilityAsync(fileFullName, desiredFileAccess, desiredFileShare);
//        }

//        ///// <summary>
//        ///// Files the check accessibility.
//        ///// </summary>
//        ///// <param name="directoryFullName">Full name of the directory.</param>
//        ///// <param name="tryForceCreateDirectory">if set to <c>true</c> [try force create directory].</param>
//        //public async Task FileCheckAccessibility(
//        //    string directoryFullName,
//        //    bool tryForceCreateDirectory)
//        //{
//        //    if (IsUriForIsolatedStorage(directoryFullName))
//        //    {
//        //        return await _isolatedStorageService.FileCheckAccessibility(
//        //            StripIsolatedStoragePrefix(directoryFullName),
//        //            tryForceCreateDirectory);
//        //    }
//        //    return await FSService.FileCheckAccessibility(
//        //        directoryFullName,
//        //        tryForceCreateDirectory);

//        //}



//        /// <summary>
//        /// Gets the files's dates.
//        /// </summary>
//        /// <param name="fileFullName">Full name of the directory.</param>
//        /// <param name="type">The type.</param>
//        /// <returns></returns>
//        public async Task<DateTime> FileGetDateTimeAsync(string fileFullName, AuditableEvent type)
//        {
//            return IsUriForIsolatedStorage(fileFullName)
//                ? await _isolatedStorageService.FileGetDateTimeAsync(StripIsolatedStoragePrefix(fileFullName), type)
//                : await FSService.FileGetDateTimeAsync(fileFullName, type);
//        }

//        /// <summary>
//        /// Gets the directory's dates.
//        /// </summary>
//        /// <param name="directoryFullName">Full name of the directory.</param>
//        /// <param name="type">The type.</param>
//        /// <returns></returns>
//        public async Task<DateTime> DirectoryGetDateTimeAsync(string directoryFullName, AuditableEvent type)
//        {
//            return IsUriForIsolatedStorage(directoryFullName)
//                ? await _isolatedStorageService.DirectoryGetDateTimeAsync(StripIsolatedStoragePrefix(directoryFullName), type)
//                : await FSService.DirectoryGetDateTimeAsync(directoryFullName, type);
//        }
//    }
//}
