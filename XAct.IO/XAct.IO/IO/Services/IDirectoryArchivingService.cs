﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using XAct.Diagnostics;
//using XAct.Services;

//namespace XAct.IO
//{

//    public enum ArchivingFileInfoDateSource
//    {
//        Unspecified = 0,
//        Created = 1,
//        LastAccessed = 2,
//        LastWritten = 3,

//    }

//    public enum ArchivingAction
//    {
//        Nothing,
//        Move,
//        Delete,
//    }

//    public interface IDirectoryArchivingService: IHasXActLibServiceDefinition
//    {
//        IDirectoryArchiver Register(IDirectoryArchivingConfiguration configuration);
//    }

//    public interface IDirectoryArchivingConfiguration
//    {
//        bool Enabled { get; set; }
//        string Name { get; set; }
//        DirectoryInfo Path { get; set; }
//        TimeSpan Delay { get; set; }
//        ArchivingAction Action { get; set; }
//        ArchivingFileInfoDateSource FileInfoDateSource { get; set; }
//        DirectoryInfo MoveToDirectory { get; set; }
//        TimeSpan CheckInterval { get; set; }
//    }

//    public interface IDirectoryArchiverCache : IDictionary<string, IDirectoryArchiver>
//    {

//    }

//    public interface IDirectoryArchiver
//    {
//        bool Enabled { get; set; }
//        string Name { get; }
//        DirectoryInfo Path { get; }
//        TimeSpan Delay { get; }
//        ArchivingAction Action { get; }
//        ArchivingFileInfoDateSource FileInfoDateSource { get; }
//        DirectoryInfo MoveToDirectory { get; }
//        TimeSpan CheckInterval { get; }
//    }


//In Services\State\Implementations
//    [DefaultBindingImplementation(typeof(IDirectoryArchiverCache), BindingLifetimeType.SingletonScope,Priority.Low)]
//    public class DirectoryArchiverSharedCache : Dictionary<string, IDirectoryArchiver>, IDirectoryArchiverCache
//    {

//    }


//    [DefaultBindingImplementation(typeof(IDirectoryArchivingService))]
//    public class DirectoryArchivingService : XActLibServiceBase,  IDirectoryArchivingService
//    {
//        private readonly ITracingService _tracingService;
//        private readonly IDirectoryArchiverCache _DirectoryArchiverCache;

//        public DirectoryArchivingService(ITracingService tracingService, IDirectoryArchiverCache DirectoryArchiverCache)
//        {
//            _tracingService = tracingService;
//            _DirectoryArchiverCache = DirectoryArchiverCache;
//        }

//        public IDirectoryArchiver Register(IDirectoryArchivingConfiguration configuration)
//        {
//            IDirectoryArchiver archiver = new DirectoryArchiver(_tracingService, configuration);

//            _DirectoryArchiverCache.Add(archiver.Name, archiver);

//            return archiver;
//        }
//    }


//    public class DirectoryArchivingConfiguration : IDirectoryArchivingConfiguration
//    {

//        public bool Enabled { get; set; }
//        public string Name { get; set; }
//        public DirectoryInfo Path { get; set; }
//        public TimeSpan Delay { get; set; }
//        public ArchivingAction Action { get; set; }
//        public ArchivingFileInfoDateSource FileInfoDateSource { get; set; }
//        public DirectoryInfo MoveToDirectory { get; set; }

//        public TimeSpan CheckInterval
//        {
//            get { return _checkInterval; }
//            set { _checkInterval = (value > TimeSpan.FromMinutes(1)) ? value : TimeSpan.FromMinutes(1); }
//        }

//        private TimeSpan _checkInterval;

//        public DirectoryArchivingConfiguration()
//        {
//            Enabled = true;
//            CheckInterval = TimeSpan.FromHours(1);
//            Action = ArchivingAction.Nothing;
//            Delay = new TimeSpan(30, 0, 0, 0);
//            FileInfoDateSource = ArchivingFileInfoDateSource.Created;
//            MoveToDirectory = null;
//        }
//    }


//    //NO: [DefaultBindingImplementation(typeof(IDirectoryArchivingConfiguration),BindingLifetimeType.TransientScope)]
//    public class DirectoryArchiver : IDirectoryArchiver
//    {
//        private System.Timers.Timer _timer;

//        private readonly ITracingService _tracingService;

//        public bool Enabled
//        {
//            get { return _timer.Enabled; }
//            set { _timer.Enabled = value; }
//        }

//        public string Name { get; private set; }

//        public DirectoryInfo Path { get; private set; }

//        public TimeSpan Delay { get; private set; }

//        public ArchivingAction Action { get; private set; }

//        public ArchivingFileInfoDateSource FileInfoDateSource { get; private set; }

//        public DirectoryInfo MoveToDirectory { get; private set; }

//        public TimeSpan CheckInterval { get; private set; }



//        public DirectoryArchiver(ITracingService tracingService,
//                                 IDirectoryArchivingConfiguration DirectoryArchivingConfiguration)
//        {
//            _tracingService = tracingService;

//            _timer = new System.Timers.Timer();


//            //After timer is created (as enabled and another prop talks to timer)
//            this.Action = DirectoryArchivingConfiguration.Action;
//            this.CheckInterval = DirectoryArchivingConfiguration.CheckInterval;
//            this.Delay = DirectoryArchivingConfiguration.Delay;
//            this.Enabled = DirectoryArchivingConfiguration.Enabled;
//            this.FileInfoDateSource = DirectoryArchivingConfiguration.FileInfoDateSource;
//            this.MoveToDirectory = DirectoryArchivingConfiguration.MoveToDirectory;
//            this.Name = DirectoryArchivingConfiguration.Name;
//            this.Path = DirectoryArchivingConfiguration.Path;


//            _timer.Interval = this.CheckInterval.TotalMilliseconds;
//            _timer.Elapsed += new System.Timers.ElapsedEventHandler(_timer_Elapsed);


//            _timer_Elapsed(null, null);
//        }

//        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
//        {
//            if (this.Delay == new TimeSpan(0, 0, 0))
//            {

//                _tracingService.Trace(TraceLevel.Info, "DirectoryArchiver for {0} has a Delay value that is not set.",
//                                      this.Name);
//                return;
//            }
//            if (!this.Path.Exists)
//            {
//                _tracingService.Trace(TraceLevel.Info, "DirectoryArchiver for {0} has a Path that does not exist.",
//                                      this.Name);
//                return;
//            }


//            foreach (FileInfo fileInfo in Path.GetFiles())
//            {
//                DateTime checkDate = GetDateFromFileInfo(fileInfo);

//                TimeSpan fileAge = DateTime.UtcNow - checkDate;

//                if (fileAge < Delay)
//                {
//                    //Not stale enough.
//                    continue;
//                }

//                try
//                {
//                    switch (Action)
//                    {
//                        case ArchivingAction.Delete:
//                            _tracingService.Trace(TraceLevel.Info, "DirectoryArchiver for {0}: Deleting {1}.",
//                                                  this.Name, fileInfo.Name);
//                            fileInfo.Delete();
//                            break;
//                        case ArchivingAction.Move:
//                            string destFileName = System.IO.Path.Combine(this.MoveToDirectory.ToString(), fileInfo.Name);
//                            _tracingService.Trace(TraceLevel.Info, "DirectoryArchiver for {0}: Moving {1} to {2}.",
//                                                  this.Name, fileInfo.Name, destFileName);
//                            fileInfo.MoveTo(destFileName);
//                            break;
//                        case ArchivingAction.Nothing:
//                        default:
//                            break;
//                    }
//                }
//                catch (System.Exception err)
//                {
//                    _tracingService.TraceException(TraceLevel.Error, err,
//                                                   "Error occurred in DirectoryArchiving '{0}' implementing '{1}' on  '{1}'.",
//                                                   this.Name, Action, fileInfo.Name);
//                }

//            }
//        }

//        private DateTime GetDateFromFileInfo(FileInfo fileInfo)
//        {
//            DateTime checkDate;
//            switch (this.FileInfoDateSource)
//            {
//                case ArchivingFileInfoDateSource.LastWritten:
//                    checkDate = fileInfo.LastWriteTimeUtc;
//                    break;
//                case ArchivingFileInfoDateSource.LastAccessed:
//                    checkDate = fileInfo.LastAccessTimeUtc;
//                    break;
//                default:
//                    checkDate = fileInfo.CreationTimeUtc;
//                    break;
//            }
//            return checkDate;
//        }

//    }
//}

