﻿namespace XAct.Diagnostics.Status.Connectors
{
    using XAct.Diagnostics.Status.Connectors.Implementations;

    /// <summary>
    /// Factory for a <see cref="FileSizeStatusServiceConnector"/>
    /// </summary>
    public class FileSizeStatusServiceConnectorFactory
    {
        /// <summary>
        /// Creates a <see cref="FileSizeStatusServiceConnector"/>.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        /// <param name="performDeleteCheck">if set to <c>true</c> [perform delete check].</param>
        /// <param name="directoryInfos">The directories.</param>
        /// <returns></returns>
        public static FileSizeStatusServiceConnector Create(string name,
                                                            string title, string description,
                                                            bool performDeleteCheck = true,
                                                            params FileSizeStatusServiceConnectorConfigurationItem[]
                                                                directoryInfos)
        {
            var statusServiceConnector =
                XAct.DependencyResolver.Current.GetInstance<FileSizeStatusServiceConnector>();

            statusServiceConnector.ConfigureBasicInformation(name, title, description);


            statusServiceConnector.Configuration.Files.Add(directoryInfos);


            statusServiceConnector.Name =
                name;

            return statusServiceConnector;
        }


    }
}