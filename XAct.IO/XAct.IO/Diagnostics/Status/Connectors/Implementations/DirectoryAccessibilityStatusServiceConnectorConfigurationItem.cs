﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public class DirectoryAccessibilityStatusServiceConnectorConfigurationItem : IHasTitle
    {
        /// <summary>
        /// Gets or sets the title.
        /// <para>
        /// Member defined in the <see cref="IHasTitle" /> contract.
        /// </para>
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The name of the directory to check
        /// </summary>
        public string DirectoryPath { get; set; }

        /// <summary>
        /// If not found, force the creation of the directory?
        /// </summary>
        public bool TryForcingDirectoryCreationIfDirectoryDoesNotExist { get; set; }

        /// <summary>
        /// Perform a read check?
        /// </summary>
        public bool PerformReadCheck { get; set; }

        /// <summary>
        /// Perform a write check?
        /// </summary>
        public bool PerformWriteCheck { get; set; }

        /// <summary>
        /// Perform a delete check?
        /// </summary>
        public bool PerformDeleteCheck { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryAccessibilityStatusServiceConnectorConfigurationItem"/> class.
        /// </summary>
        public DirectoryAccessibilityStatusServiceConnectorConfigurationItem(string title, string directoryPath,
                                                                             bool performReadCheck = true,
                                                                             bool performWriteCheck = true,
                                                                             bool performDeleteCheck = true,
                                                                             bool
                                                                                 tryForcingDirectoryCreationIfDirectoryDoesNotExist
                                                                                 = false)
        {
            Title = title;
            DirectoryPath = directoryPath;
            PerformReadCheck = performReadCheck;
            PerformWriteCheck = performWriteCheck;
            PerformDeleteCheck = performDeleteCheck;
            TryForcingDirectoryCreationIfDirectoryDoesNotExist = tryForcingDirectoryCreationIfDirectoryDoesNotExist;
        }

    }
}