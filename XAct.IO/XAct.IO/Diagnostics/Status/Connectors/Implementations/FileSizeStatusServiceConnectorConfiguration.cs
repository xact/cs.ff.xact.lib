namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System.Collections.Generic;

    /// <summary>
    /// Configuration object for an instance of 
    /// <see cref="FileSizeStatusServiceConnector"/>
    /// </summary>
    public class FileSizeStatusServiceConnectorConfiguration : IHasTransientBindingScope
    {
        /// <summary>
        /// Gets the files to test size of.
        /// </summary>
        /// <value>
        /// The files.
        /// </value>
        public List<FileSizeStatusServiceConnectorConfigurationItem> Files
        {
            get { return _files ?? (_files = new List<FileSizeStatusServiceConnectorConfigurationItem>()); }
        }

        private List<FileSizeStatusServiceConnectorConfigurationItem> _files;
    }
}