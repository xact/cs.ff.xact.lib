﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Environment;
    using XAct.IO;
    using XAct.Resources;



    /// <summary>
    /// Connector to retrieve information regarding accessibility of Directories
    /// </summary>
    public class FileSizeStatusServiceConnector : XActLibStatusServiceConnectorBase
    {

        private readonly IEnvironmentService _environmentService;
        private readonly IFSIOService _fsioService;
        private readonly IPathService _pathService;
        
        /// <summary>
        /// The configuration package of this Connector.
        /// </summary>
        public FileSizeStatusServiceConnectorConfiguration Configuration { get; private set; }


        /// <summary>
        /// Files the read write access connector.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="fsioService">The fsio service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fileSizeStatusServiceConnectorConfiguration">The file size status feed service connector configuration object.</param>
        public FileSizeStatusServiceConnector(IEnvironmentService environmentService,
                                                  IFSIOService fsioService,
                                                  IPathService pathService,
            FileSizeStatusServiceConnectorConfiguration fileSizeStatusServiceConnectorConfiguration)
            :
                base()
        {
            Name = "FileSize";

            _environmentService = environmentService;
            _fsioService = fsioService;
            _pathService = pathService;
            Configuration = fileSizeStatusServiceConnectorConfiguration;
        }





        /// <summary>
        /// Gets the <see cref="StatusResponse" />
        /// containing Data and/or Metrics.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null)
        {

            //Using the base helper ensures it gets the right Title/Description
            //in the user's culture:
            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }


        private void MakeTable(StatusResponseTable table, object arguments)
        {
            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:
            List<FileSizeStatusServiceConnectorConfigurationItem> filesToTest =
                arguments == null
                    ? Configuration.Files
                    : ProcessArguments<FileSizeStatusServiceConnectorConfigurationItem>(arguments,
                                                              (s) => new FileSizeStatusServiceConnectorConfigurationItem
                                                                  (
                                                                      title:s,
                                                                      filePath:s,
                                                                      warningBytesSize:long.MaxValue,
                                                                      errorBytesSize:long.MaxValue
                                                                  ));


            //Create Headers whether there are rows or not:
            table.Headers.Add("FileName");
            table.Headers.Add("Size");

            //Make rows:
            if (filesToTest == null)
            {
                return;
            }

            foreach (FileSizeStatusServiceConnectorConfigurationItem givenFileName in filesToTest)
            {
                table.Rows.Add(MakeTableRow(givenFileName));

            }
        }




        private StatusResponseTableRow MakeTableRow(FileSizeStatusServiceConnectorConfigurationItem givenFileName)
        {
            var row = new StatusResponseTableRow();


            givenFileName.FilePath = _environmentService.MapPath(givenFileName.FilePath);


            //Cell #1:
            row.Cells.Add(givenFileName.Title);


            //Cell #2:

            var fs = new FileInfo(givenFileName.FilePath);

            string message;
            
            if (fs.Exists)
            {
                long fileSize = fs.Length;
                message = fileSize.ToHumanReadableFileSizeString();

                row.Status =
                    (fileSize < givenFileName.WarningBytesSize)
                        ? ResultStatus.Success
                        : (fileSize < givenFileName.ErrorBytesSize)
                              ? ResultStatus.Warn
                              : ResultStatus.Fail;
            }
            else
            {
                message = "?";
                row.Status = ResultStatus.Undetermined;
            }

            row.Cells.Add(message);
            return row;
        }
    }
}