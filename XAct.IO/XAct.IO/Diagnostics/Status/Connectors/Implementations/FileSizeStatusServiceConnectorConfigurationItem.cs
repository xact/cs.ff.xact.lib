namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public class FileSizeStatusServiceConnectorConfigurationItem: IHasTitle
    {
        /// <summary>
        /// The displayed Title (use this to show the filename, wtihout path, or obfuscate the actual name of the file).
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The name of the file to check
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// The File size (in Bytes) at which to change the status to Warning.
        /// </summary>
        public long WarningBytesSize { get; set; }

        /// <summary>
        /// The Max file size to allow -- the file size (in Bytes) at which to change the status to Fail/Error.
        /// </summary>
        public long ErrorBytesSize { get; set; }


        ///// <summary>
        ///// Initializes a new instance of the <see cref="FileNameAndSizeLimits"/> class.
        ///// </summary>
        //public FileNameAndSizeLimits()
        //{
        //    WarningBytesSize = long.MaxValue;
        //    ErrorBytesSize = long.MaxValue;
        //}
        /// <summary>
        /// Initializes a new instance of the <see cref="FileSizeStatusServiceConnectorConfigurationItem"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="filePath">The path.</param>
        /// <param name="warningBytesSize">Size of the warning byte.</param>
        /// <param name="errorBytesSize">Size of the error byte.</param>
        public FileSizeStatusServiceConnectorConfigurationItem(string title, string filePath, long warningBytesSize, long errorBytesSize)
        {
            Title = title;
            FilePath = filePath;
            if (title.IsNullOrEmpty())
            {
                title = filePath;
            }
            WarningBytesSize = warningBytesSize;
            ErrorBytesSize = errorBytesSize;
        }
    }
}