﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Environment;
    using XAct.IO;
    using XAct.IO.Implementations;
    using XAct.Resources;



    /// <summary>
    /// An implementation 
    /// Connector to retrieve information regarding 
    /// accessibility of Directories
    /// </summary>
    public class DirectorySizeStatusServiceConnector : XActLibStatusServiceConnectorBase
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IPathService _pathService;
        private readonly IFSIOService _fsioService;

        /// <summary>
        /// The configuration package of this Connector.
        /// </summary>
        public DirectorySizeStatusServiceConnectorConfiguration Configuration { get; private set; }

        /// <summary>
        /// Files the read write access connector.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fsioService">The fsio service.</param>
        /// <param name="directoryAccessibilityStatusServiceConnectorConfiguration">The directory accessibility status feed service connector configuration.</param>
        public DirectorySizeStatusServiceConnector(IEnvironmentService environmentService,
            IPathService pathService,IFSIOService fsioService,
                                                                DirectorySizeStatusServiceConnectorConfiguration
                                                                    directoryAccessibilityStatusServiceConnectorConfiguration)
            : base()
        {

            Name = "DirectorySize";

            _environmentService = environmentService;
            _pathService = pathService;
            _fsioService = fsioService;
            Configuration =
                directoryAccessibilityStatusServiceConnectorConfiguration;
        }



        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null)
        {

            //Using the base helper ensures it gets the right Title/Description
            //in the user's culture:
            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }


        private void MakeTable(StatusResponseTable table, object arguments)
        {

            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:
            List<DirectorySizeStatusServiceConnectorConfigurationItem> directoriesToTest
                = arguments ==
                  null
                      ? Configuration
                            .Directories
                      : ProcessArguments
                            <DirectorySizeStatusServiceConnectorConfigurationItem>(
                                arguments,
                                (s) =>
                                new DirectorySizeStatusServiceConnectorConfigurationItem
                                    (
                                        title:s,
                                        directoryPath:s,
                                        warningBytesSize:1024*1024*10,
                                        errorBytesSize:1024*1024*100
                                    ));



            //Create Headers:
            table.Headers.Add("Directory");
            table.Headers.Add("r/w/d");


            //Make rows:
            if (directoriesToTest != null)
            {
                return;
            }
            foreach (
                DirectorySizeStatusServiceConnectorConfigurationItem directory in directoriesToTest)
            {
                table.Rows.Add(MakeTableRow(directory));
            }
        }

        private StatusResponseTableRow MakeTableRow(
            DirectorySizeStatusServiceConnectorConfigurationItem directoryInformation)
        {
            StatusResponseTableRow row = new StatusResponseTableRow();

            var dir = _environmentService.MapPath(directoryInformation.DirectoryPath);

            var d = new System.IO.DirectoryInfo(dir);

            //Cell #1:
            row.Cells.Add( directoryInformation.Title);



            //Cell #2:
            string message;

            if (d.Exists)
            {
                long fileSize = d.GetFiles().Sum(x => x.Length);

                message = fileSize.ToHumanReadableFileSizeString();

                row.Status =
                    (fileSize > directoryInformation.WarningBytesSize)
                        ? ResultStatus.Success
                        : (fileSize < directoryInformation.ErrorBytesSize)
                              ? ResultStatus.Warn
                              : ResultStatus.Fail;
            }
            else
            {
                message = "?";
                row.Status = ResultStatus.Undetermined;
            }



            row.Cells.Add(message);

            return row;
        }
    }
}