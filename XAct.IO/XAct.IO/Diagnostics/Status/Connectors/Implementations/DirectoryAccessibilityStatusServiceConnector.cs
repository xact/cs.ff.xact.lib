﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Environment;
    using XAct.IO;
    using XAct.IO.Implementations;
    using XAct.Resources;



    /// <summary>
    /// An implementation 
    /// Connector to retrieve information regarding 
    /// accessibility of Directories
    /// </summary>
    public class DirectoryAccessibilityStatusServiceConnector : XActLibStatusServiceConnectorBase
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IPathService _pathService;
        private readonly IFSIOService _fsioService;

        /// <summary>
        /// The configuration package of this Connector.
        /// </summary>
        public DirectoryAccessibilityStatusServiceConnectorConfiguration Configuration { get; private set; }

        /// <summary>
        /// Files the read write access connector.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fsioService">The fsio service.</param>
        /// <param name="directoryAccessibilityStatusServiceConnectorConfiguration">The directory accessibility status feed service connector configuration.</param>
        public DirectoryAccessibilityStatusServiceConnector(IEnvironmentService environmentService,
            IPathService pathService,IFSIOService fsioService,
                                                                DirectoryAccessibilityStatusServiceConnectorConfiguration
                                                                    directoryAccessibilityStatusServiceConnectorConfiguration)
            : base()
        {

            Name = "DirectoryAccessibility";

            _environmentService = environmentService;
            _pathService = pathService;
            _fsioService = fsioService;
            Configuration =
                directoryAccessibilityStatusServiceConnectorConfiguration;
        }



        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null)
        {

            //Using the base helper ensures it gets the right Title/Description
            //in the user's culture:
            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }


        private void MakeTable(StatusResponseTable table, object arguments)
        {

            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:
            List<DirectoryAccessibilityStatusServiceConnectorConfigurationItem> directoriesToTest
                = arguments ==
                  null
                      ? Configuration
                            .Directories
                      : ProcessArguments
                            <DirectoryAccessibilityStatusServiceConnectorConfigurationItem>(
                                arguments,
                                (s) =>
                                new DirectoryAccessibilityStatusServiceConnectorConfigurationItem
                                    (
                                        title:s,
                                        directoryPath:s
                                    ));



            //Create Headers:
            table.Headers.Add("Directory");
            table.Headers.Add("r/w/d");


            //Make rows:
            if (directoriesToTest != null)
            {
                return;
            }
            foreach (
                DirectoryAccessibilityStatusServiceConnectorConfigurationItem directory in directoriesToTest)
            {
                table.Rows.Add(MakeTableRow(directory));
            }
        }

        private StatusResponseTableRow MakeTableRow(
            DirectoryAccessibilityStatusServiceConnectorConfigurationItem directoryInformation)
        {
            StatusResponseTableRow row = new StatusResponseTableRow();


            DirectoryAccessibilityCheckResults directoryResult =
                _fsioService.DirectoryCheckAccessibilityAsync(
                    directoryInformation.DirectoryPath,
                    directoryInformation.TryForcingDirectoryCreationIfDirectoryDoesNotExist)
                            .WaitAndGetResult();


            //Cell #1:
            row.Cells.Add( directoryInformation.Title);



            //Cell #2:
            string message = string.Empty;

            message += "r:";
            message += (directoryInformation.PerformReadCheck)
                           ? ((directoryResult.CanRead == ResultStatus.Success) ? "y" : "n")
                           : "?";


            message += ", w:";
            message += (directoryInformation.PerformWriteCheck)
                           ? ((directoryResult.CanWrite == ResultStatus.Success) ? "y" : "n")
                           : "?";

            message += ", d:";
            message += (directoryInformation.PerformDeleteCheck)
                           ? ((directoryResult.CanDelete == ResultStatus.Success) ? "y" : "n")
                           : "?";


            row.Cells.Add(message);

            return row;
        }
    }
}