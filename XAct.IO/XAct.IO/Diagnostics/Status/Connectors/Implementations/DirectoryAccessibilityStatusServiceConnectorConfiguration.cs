﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System.Collections.Generic;

    /// <summary>
    /// A configuration object for 
    /// an instance of 
    /// <see cref="DirectoryAccessibilityStatusServiceConnector"/>
    /// </summary>
    public class DirectoryAccessibilityStatusServiceConnectorConfiguration : IHasTransientBindingScope
    {

        /// <summary>
        /// List of directories to check.
        /// </summary>
        public List<DirectoryAccessibilityStatusServiceConnectorConfigurationItem> Directories { get { return _directories ?? (_directories = new List<DirectoryAccessibilityStatusServiceConnectorConfigurationItem>()); } }
        private List<DirectoryAccessibilityStatusServiceConnectorConfigurationItem> _directories;

    }
}