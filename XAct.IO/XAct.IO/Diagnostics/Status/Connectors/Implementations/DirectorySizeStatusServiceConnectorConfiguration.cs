﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System.Collections.Generic;

    /// <summary>
    /// A configuration object for 
    /// an instance of 
    /// <see cref="DirectoryAccessibilityStatusServiceConnector"/>
    /// </summary>
    public class DirectorySizeStatusServiceConnectorConfiguration : IHasTransientBindingScope
    {

        /// <summary>
        /// List of directories to check.
        /// </summary>
        public List<DirectorySizeStatusServiceConnectorConfigurationItem> Directories { get { return _directories ?? (_directories = new List<DirectorySizeStatusServiceConnectorConfigurationItem>()); } }
        private List<DirectorySizeStatusServiceConnectorConfigurationItem> _directories;

    }
}