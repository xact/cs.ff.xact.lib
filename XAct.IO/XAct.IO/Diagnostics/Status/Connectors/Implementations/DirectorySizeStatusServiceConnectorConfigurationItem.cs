﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public class DirectorySizeStatusServiceConnectorConfigurationItem :IHasTitle
    {

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// The name of the directory to check
        /// </summary>
        public string DirectoryPath { get; set; }


        /// <summary>
        /// The File size (in Bytes) at which to change the status to Warning.
        /// </summary>
        public long WarningBytesSize { get; set; }

        /// <summary>
        /// The Max file size to allow -- the file size (in Bytes) at which to change the status to Fail/Error.
        /// </summary>
        public long ErrorBytesSize { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryAccessibilityStatusServiceConnectorConfigurationItem"/> class.
        /// </summary>
        public DirectorySizeStatusServiceConnectorConfigurationItem(string title, string directoryPath, long warningBytesSize, long errorBytesSize)
        {
            DirectoryPath = directoryPath;
            Title = title;
            if (Title.IsNullOrEmpty())
            {
                title = directoryPath;
            }
            WarningBytesSize = warningBytesSize;
            ErrorBytesSize = errorBytesSize;
        }

    }
}