﻿
namespace XAct.Diagnostics.Status.Connectors
{
    using XAct.Diagnostics.Status.Connectors.Implementations;

    /// <summary>
    /// A Factory for creating <see cref="DirectoryAccessibilityStatusServiceConnector"/> instances.
    /// </summary>
    public class DirectoryAccessibilityStatusServiceConnectorFactory
    {

        /// <summary>
        /// Creates a <see cref="DirectoryAccessibilityStatusServiceConnector"/>.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        /// <param name="performDeleteCheck">if set to <c>true</c> [perform delete check].</param>
        /// <param name="directoryInfos">The directories.</param>
        /// <returns></returns>
        public static DirectoryAccessibilityStatusServiceConnector Create(string name, string title, string description,
                                                                          bool performDeleteCheck = true,
                                                                          params
                                                                              DirectoryAccessibilityStatusServiceConnectorConfigurationItem
                                                                              [] directoryInfos)
        {
            var statusServiceConnector =
                XAct.DependencyResolver.Current.GetInstance<DirectoryAccessibilityStatusServiceConnector>();

            statusServiceConnector.ConfigureBasicInformation(name, title, description);

            statusServiceConnector.Configuration.Directories.Add(directoryInfos);

            statusServiceConnector.Name =
                name;

            return statusServiceConnector;
        }
    }
}
