﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.IO;
    using XAct.IO;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    /// Static Extensions methods to the DirectoryInfo object.
    /// </summary>
    public static partial class DirectoryInfoExtensions
    {


        //private static IFSIOService FsIoService
        //{
        //    get
        //    {
        //        return DependencyResolver.Current.GetInstance<IFSIOService>();
        //    }
        //}




        /// <summary>
        /// Cleanup directory, given a pattern such as "*.log"
        /// </summary>
        /// <param name="directoryInfo">Directory path</param>
        /// <param name="pattern">Filename pattern, such as "*.log"</param>
        /// <param name="deleteFilesWithCreatedDateOlderThanThisManyDays">Number of days after which files will be removed</param>
        public static void CleanupDirectoryByFileCreationDate(DirectoryInfo directoryInfo, string pattern,
                                                              int deleteFilesWithCreatedDateOlderThanThisManyDays)
        {
            foreach (FileInfo fileInfo in directoryInfo.GetFiles(pattern))
            {
                TimeSpan age = DateTime.Now.Subtract(fileInfo.CreationTime);
                if (age.Days <= deleteFilesWithCreatedDateOlderThanThisManyDays)
                {
                    continue;
                }
                fileInfo.Delete();
            }
        }


    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
