﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.Diagnostics.Contracts;
    using System.IO;
    using XAct.IO;

    /// <summary>
    /// Extensions to the Stream object, that need a reference
    /// to <see cref="IIOService"/> for writting to Isolated Storage
    /// or HD.
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        /// Writes Resource stream to File
        /// </summary>
        /// <param name="resourceStream">The resource stream.</param>
        /// <param name="iioService">The iio service.</param>
        /// <param name="absolutePath">The absolute path.</param>
        /// <param name="overwriteOk">Ok to overwrite what ever is already there.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// An exception is raised if <paramref name="resourceStream"/> is null.
        ///   </exception>
        ///   
        /// <exception cref="System.ArgumentNullException">
        /// An exception is raised if <paramref name="absolutePath"/> is null.
        ///   </exception>
        ///   
        /// <exception cref="System.ArgumentException">
        /// An exception is raised if <paramref name="absolutePath"/> is
        /// not an absolute Physical path.
        ///   </exception>
        ///   
        /// <internal>
        /// Used by Resource handling.
        ///   </internal>
        public static void WriteStreamToFile(
            this Stream resourceStream,
            IIOService iioService,
            string absolutePath,
            bool overwriteOk)
        {
            //Check Args:
            resourceStream.ValidateIsNotDefault("resourceStream");
            absolutePath.ValidateIsNotNullOrEmpty("absolutePath");
            string absolutePathDirectory = Path.GetDirectoryName(absolutePath);
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            //Ensure directory exists
            iioService.DirectoryCreateAsync(absolutePathDirectory);

            //try writting to it:
// ReSharper disable RedundantArgumentDefaultValue
            using (Stream fileStream = iioService.FileOpenWriteAsync(absolutePath, overwriteOk, false).Result)
// ReSharper restore RedundantArgumentDefaultValue
            {
                fileStream.CopyStreamFrom(resourceStream);
            }
        }

    }
}

