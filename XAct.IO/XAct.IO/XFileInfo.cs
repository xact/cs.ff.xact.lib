﻿namespace XAct.IO
{
    using System;

    /// <summary>
    ///   Class for Information about a file.
    /// </summary>
    public class XFileInfo
    {
        /// <summary>
        /// Returns the age of the file.
        /// </summary>
        /// <param name="fileCreationTime">The file creation time.</param>
        /// <returns></returns>
        public TimeSpan FileAge(long fileCreationTime)
        {
            //Copy of Example from MSDN:
            DateTime now = DateTime.Now;

            DateTime fCreationTime =
                DateTime.FromFileTime(fileCreationTime);
            TimeSpan fileAge = now.Subtract(fCreationTime);
            return fileAge;
        }

    }
}