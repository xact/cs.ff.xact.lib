namespace XAct.IO.TemplateEngines.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.IO.TemplateEngines.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class NVelocityServiceTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {

            Singleton<IocContext>.Instance.ResetIoC();

        }


        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetITempalteRenderingService()
        {
            ITemplateRenderingService service = XAct.DependencyResolver.Current.GetInstance<ITemplateRenderingService>();

            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetITemplateRenderingServiceOfExpectedType()
        {
            ITemplateRenderingService service = XAct.DependencyResolver.Current.GetInstance<ITemplateRenderingService>();

            Assert.AreEqual(typeof(NVelocityTemplateService),service.GetType());
        }


        //TEST:FAILING *
        [Test]
        public void CompileATemplateFile()
        {

            INVelocityTemplateService nVelocityTemplateService =
                DependencyResolver.Current.GetInstance<INVelocityTemplateService>();


            var viewModel = new
                {
                    headerText = "AHEADER!!!",
                    subHeader="foo bar foo...",
                    name = "John", date = DateTime.Now, 
                    dict=new Dictionary<string,string> {{"AKey","AValue"},{"BKey","BValue"}}
                };

            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            string path = environmentService.MapPath(@"Templates\NV\body.nv");
            Trace.WriteLine("path:" + path);
            Trace.WriteLine("found:" + File.Exists(path));

            string results = nVelocityTemplateService.CompileTemplateFile(path, null, viewModel,false);

            Assert.IsTrue(results.Contains("John"),"Should contain 'John' but was" + results);

            Trace.WriteLine("Results:" + results);

            //NVelocity.
            //string templateDir = @"c:\Temp\NVelocity\Templates\";
            //string templateName = "SimpleTemplate.vm";

            //INVelocityEngine fileEngine =
            //    NVelocityEngineFactory.CreateNVelocityFileEngine(templateDir, true);
            // NVelocityFileEngine(templateDirectory, cacheTemplate);
            //IDictionary context = new Hashtable();

            //context.Add("name", TextBox1.Text);
            //context.Add("surname", TextBox2.Text);
            //context.Add("templateType", "file");
            //context.Add("date", DateTime.Now.ToString("D"));

            //LabelMergedFile.Text = fileEngine.Process(context, templateName);

        }



        [Test]
        public void CompileATemplateString()
        {
            INVelocityTemplateService nVelocityTemplateService =
                DependencyResolver.Current.GetInstance<INVelocityTemplateService>();

            //Missing 'Context.' prefix:
           string output = nVelocityTemplateService.CompileTemplateString("A $replaceme test", null, new {replaceme="quick"},false);
            Assert.AreEqual("A $replaceme test", output);
            Assert.AreNotEqual("A quick test", output);
        }



        [Test]
        public void CompileATemplateStringB()
        {
            INVelocityTemplateService nVelocityTemplateService =
                DependencyResolver.Current.GetInstance<INVelocityTemplateService>();


            string output = nVelocityTemplateService.CompileTemplateString("A $Context.replaceme test", null, new { replaceme = "quick" },false);
            Assert.AreEqual("A quick test", output);
        }


        [Test]
        public void CompileATemplateStringC()
        {
            INVelocityTemplateService nVelocityTemplateService =
                DependencyResolver.Current.GetInstance<INVelocityTemplateService>();


            string output = nVelocityTemplateService.CompileTemplateString("A $replaceme test", null, new { replaceme = "quick" },true);
            Assert.AreEqual("A quick test", output);
        }




        public void CompileATemplateStringD()
        {
            INVelocityTemplateService nVelocityTemplateService =
                DependencyResolver.Current.GetInstance<INVelocityTemplateService>();

            //Missing 'Context.' prefix:
            Dictionary<string,object> data = new Dictionary<string, object>();
            data["replaceme"] = "quick";
            string output = nVelocityTemplateService.CompileTemplateString("A $replaceme test", null, data, false);
            Assert.AreEqual("A $replaceme test", output);
            Assert.AreNotEqual("A quick test", output);
        }



        [Test]
        public void CompileATemplateStringE()
        {
            INVelocityTemplateService nVelocityTemplateService =
                DependencyResolver.Current.GetInstance<INVelocityTemplateService>();


            Dictionary<string, object> data = new Dictionary<string, object>();
            data["replaceme"] = "quick";
            string output = nVelocityTemplateService.CompileTemplateString("A $Context.replaceme test", null, data, false);
            Assert.AreEqual("A quick test", output);
        }


        [Test]
        public void CompileATemplateStringF()
        {
            INVelocityTemplateService nVelocityTemplateService =
                DependencyResolver.Current.GetInstance<INVelocityTemplateService>();


            Dictionary<string, object> data = new Dictionary<string, object>();
            data["replaceme"] = "quick";
            string output = nVelocityTemplateService.CompileTemplateString("A $replaceme test", null, data, true);
            Assert.AreEqual("A quick test", output);
        }

    
    
    
    }
}

