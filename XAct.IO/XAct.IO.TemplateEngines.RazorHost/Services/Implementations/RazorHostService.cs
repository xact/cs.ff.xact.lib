namespace XAct.IO.TemplateEngines.Implementations 
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using RazorHosting;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of 
    /// <see cref="IRazorHostService"/>,
    /// which defines a contract for a 
    /// RazorHost based implementation of 
    /// <see cref="XAct.IO.ITemplateRenderingService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(ITemplateRenderingService), BindingLifetimeType.Undefined, Priority.Low /*OK: Don't have a preference between technologies*/)]
    public class RazorHostService : XActLibServiceBase, IRazorHostService
    {
        private readonly IIOService _ioService;



        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public ITemplateRenderingServiceConfiguration Configuration
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RazorHostService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ioService">The specific io service to use.</param>
        /// <param name="serviceSettings">The service settings.</param>
        public RazorHostService(ITracingService tracingService, IIOService ioService, ITemplateRenderingServiceConfiguration serviceSettings):base(tracingService)
        {
            _ioService = ioService;
            Configuration = serviceSettings;
        }
        /// <summary>
        /// Cached instance of RazorHost
        /// </summary>
        RazorEngine<RazorTemplateBase> Host
        {
            get
            {
                if (_host == null)
                {

                    _host = RazorEngineFactory<RazorTemplateBase>.CreateRazorHostInAppDomain();

                    if (_host == null)
                    {
                        throw new Exception("Cannot instantiate RazorHost");
                    }
                }
                return _host;
            }
            set
            {
                if ((value == null) && (_host != null))
                {
                    RazorEngineFactory<RazorTemplateBase>.UnloadRazorHostInAppDomain();
                }
                _host = value;
            }
        }
        private RazorEngine<RazorTemplateBase> _host;



        /// <summary>
        /// Compiles the string template.
        /// </summary>
        /// <param name="templateText">The template text.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateString(string templateText, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            string[] assemblyNames = (optionalReferencedAssemblies != null) ? (optionalReferencedAssemblies.Select(a => Path.GetFileName(a.Location)).ToArray()) : null;

            var result = this.Host.RenderTemplate(templateText, assemblyNames, optionalContext);
            return result;
        }

        /// <summary>
        /// Compiles the template file.
        /// </summary>
        /// <param name="templateFileName">Name of the template file.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateFile(string templateFileName, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            using (StreamReader reader = new StreamReader(_ioService.FileOpenReadAsync(templateFileName).WaitAndGetResult(),true))
            {
                var result = this.CompileTemplateStream(reader, optionalReferencedAssemblies, optionalContext);
                return result;
            }
        }


        /// <summary>
        /// Compiles the template text stream.
        /// </summary>
        /// <param name="textReader">The text reader.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateStream(TextReader textReader, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            string[] assemblyNames = (optionalReferencedAssemblies != null) ? (optionalReferencedAssemblies.Select(a => a.FullName).ToArray()) : null;

            var result = this.Host.RenderTemplate(textReader,
                assemblyNames,
                optionalContext);
            return result;

        }

        #region ITemplateRenderingService Members


        /// <summary>
        /// Compiles the source template string to the given output stream.
        /// </summary>
        /// <param name="outputStream">The stream.</param>
        /// <param name="templateText">The template text.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateString(Stream outputStream, string templateText, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Compiles the source template file to the given output stream.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="templateFileName">Name of the template file.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateFile(Stream outputStream, string templateFileName, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Compiles the source template text stream to the target output stream.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="textReader">The text reader.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateStream(Stream outputStream, TextReader textReader, IEnumerable<Assembly> optionalReferencedAssemblies = null, object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}