namespace XAct.IO.TemplateEngines
{
    /// <summary>
    /// A RazorHost based implementation of
    /// <see cref="XAct.IO.ITemplateRenderingService"/>
    /// </summary>
    public interface IRazorHostService : ITemplateRenderingService, IHasXActLibService
    {
    }
}
