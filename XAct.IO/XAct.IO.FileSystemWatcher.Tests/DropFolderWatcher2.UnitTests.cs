namespace XAct.IO.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.Tests;


    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DropFolderService2UnitTests
    {
        private IDropFolderWatcher _dropFolderWatcherInstance;
        private IEnvironmentService _environmentService;
        private IDropFolderService _fileSystemWatcherService;
        private List<FileEvent> _eventsCaptured;


        private string _dirPath;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Bootstrap all services found by reflection:
                        Singleton<IocContext>.Instance.ResetIoC();


            //FSIOService fsioService = null;
            //FSIOService o = fsioService;

            //Create a list to capture events as they are raised
            //for tests to see if events raised in what order:
            _eventsCaptured = new List<FileEvent>();


            _environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            _fileSystemWatcherService =
    DependencyResolver.Current.GetInstance<IDropFolderService>();

            //Use a subfolder within bin/debug:
            _dirPath = _environmentService.MapPath("WatchMe");

            //For good measure delete any tmp files we used in previous tests:
            DeleteTmpFiles();



            BuildUpConfigAndWatcher();
            //XAct.IO.FileSystemDropFolderWatcherInstanceConfiguration 

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        private void BuildUpConfigAndWatcher()
        {
            //Need the 

            DropFolderSpecifications config = new DropFolderSpecifications();
            config.SuccessStrategy.Enabled = false;
            config.ErrorStrategy.Enabled = false;
             
            List<IDropFolderDirectorySpecification> dropFolderDirectorySpecifications
                = new List<IDropFolderDirectorySpecification>();

            IDropFolderDirectorySpecification dropFolderDirectorySpecification = new DropFolderDirectorySpecification();

            //Path of folder to watch 'WatchMe'
            dropFolderDirectorySpecification.Directory = _dirPath;
            dropFolderDirectorySpecification.IncludeSubDirectories = false;

            //Success folder: 'WatchMe/Success'
            dropFolderDirectorySpecification.SuccessStrategy.Directory = Path.Combine(_dirPath, "Success");
            dropFolderDirectorySpecification.SuccessStrategy.ArchiveAction = ArchivingAction.Move;
            dropFolderDirectorySpecification.SuccessStrategy.ArchiveCheckInterval = new TimeSpan(0, 0, 0, 0, 100);
            dropFolderDirectorySpecification.SuccessStrategy.ArchiveAction = ArchivingAction.Delete;
            //Error folder: 'WatchMe/Error'
            dropFolderDirectorySpecification.ErrorStrategy.Directory = Path.Combine(_dirPath, "Error");
            //dropFolderDirectorySpecification.ErrorStrategy.ArchiveAction = ArchivingAction.Undefined;

            dropFolderDirectorySpecifications.Add(dropFolderDirectorySpecification);


            //List of files to watch:
            config.Directories = dropFolderDirectorySpecifications.ToArray();

            config.SuccessStrategy.Enabled = true;
            config.SuccessStrategy.ArchiveAction = ArchivingAction.Delete;
            config.SuccessStrategy.Directory = Path.Combine(_dirPath, "Success2");
            
            _dropFolderWatcherInstance =
                _fileSystemWatcherService.Register(config);

            _dropFolderWatcherInstance.NewFile += _dropFolderWatcherInstance_Event;
        }




        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }


        void _dropFolderWatcherInstance_Event(object sender, DropFolderEventArgs args)
        {
            if (args.FullPath.EndsWith("_placeholder.txt"))
            {
                //Ignore the UnitTest file as it will +1 all counters...not what we want.
                return;
            }
            //Does file exist?
            _eventsCaptured.Add(new FileEvent(args));

            if (args.FullPath.Contains("causeAnException"))
            {
                //No exception should cause it to show up in Success.
                args.Exception = new Exception("Something....");
            }

        }


        /// <summary>
        /// Helper method to cleanup old test tmp files.
        /// </summary>
        private void DeleteTmpFiles(string path=null)
        {
            if (path == null)
            {
                path = _dirPath;
            }

            string[] paths = Directory.GetFiles(_dirPath, "*test*.txt",SearchOption.AllDirectories);

            foreach (string path2 in paths)
            {
                File.Delete(path2);
            }
        }


        /// <summary>
        /// Helper method for unit tests to create test output files.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void MakeNewFile(string filePath)
        {
            using (Stream stream = File.Create(filePath))
            {
                //bool b1 = stream.CanWrite;

                stream.AppendAllText("X", true);
                //bool b2 = stream.CanWrite;
            }
        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_CreateNewFile()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            
            //bool enabled=_dropFolderWatcherInstance.Enabled;

            string filePath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].Directory , "test_01.txt");

            MakeNewFile(filePath);

            //ThreadPool has a delay before spinning up...not sure what it is...
            Thread.Sleep(1100);

            //There should one recorded event, of type New/Created:
            Assert.AreEqual(1,_eventsCaptured.Count);
            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_CreateNewFileAndArchive()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_dropFolderWatcherInstance.Enabled;

            string filePath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].Directory, "test_02.txt");

            string successPath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].SuccessStrategy.Directory.ToString(), "test_02.txt");
            Assert.IsFalse(File.Exists(successPath));

            MakeNewFile(filePath);

            //ThreadPool has a delay before spinning up...not sure what it is...
            Thread.Sleep(1100);


            //Now let's see it moved to Success Folder:
            Assert.IsTrue(File.Exists(successPath));

        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_CreateNewFileAndArchiveAsError()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_dropFolderWatcherInstance.Enabled;

            string filePath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].Directory.ToString(), "test_causeAnException_02.txt");

            MakeNewFile(filePath);

            //ThreadPool has a delay before spinning up...not sure what it is...
            Thread.Sleep(1100);

            Thread.Sleep(1000);


            //Assert.AreEqual(_eventsCaptured.Count, 1);
            //Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);


            //Now let's see it moved to Success Folder:
            string successPath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].ErrorStrategy.Directory.ToString(), "test_causeAnException_02.txt");

            Assert.IsTrue(File.Exists(successPath));

        }




        [Test]
        public void Test_CreateManyNewFileAndCleanupOldFiles()
        {
            DeleteTmpFiles();

            try
            {
                DeleteTmpFiles(_dirPath + "\\Success");
                DeleteTmpFiles(_dirPath + "\\Error");
            }catch
            {
                //Files not created yet?
            }


            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            
            _eventsCaptured.Clear();

            //bool enabled=_dropFolderWatcherInstance.Enabled;
            //5 new files:
            for (int i = 1; i < 6; i++)
            {
                string filePath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].Directory.ToString(),
                                               "test_{0}.txt".FormatStringInvariantCulture(i.Pad(2)));

                MakeNewFile(filePath);
            }



            //ThreadPool has a delay before spinning up...not sure what it is...
            Thread.Sleep(1100);

            Thread.Sleep(1000);



            //Now let's see it moved to Success Folder:
            string successPath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].SuccessStrategy.Directory.ToString(), "test_02.txt");

            Assert.IsTrue(File.Exists(successPath));



            //Update Times:
            for (int i = 3; i < 6; i++)
            {
                string filePath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].SuccessStrategy.Directory.ToString(),
                                               "test_{0}.txt".FormatStringInvariantCulture(i.Pad(2)));


                DateTime newDate = DateTime.UtcNow.AddDays(-15);
                new FileInfo(filePath).SetLastWriteTime(newDate);

            }

            Assert.AreEqual(5, _eventsCaptured.Count);
            //Wait for archiver to kick in...
            Thread.Sleep(1000);

            Thread.Sleep(1000);


        }






        [Test]
        public void Test_CreateManyNewFileAndCleanupOldFilesSomeAreLocked()
        {
            DeleteTmpFiles();
            try
            {
                DeleteTmpFiles(_dirPath + "\\Success");
                DeleteTmpFiles(_dirPath + "\\Error");
            }
            catch
            {
                //Files not created yet?
            }


            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_dropFolderWatcherInstance.Enabled;

            for (int i = 1; i < 6; i++)
            {
                string filePath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].Directory.ToString(),
                                               "test_{0}.txt".FormatStringInvariantCulture(i.Pad(2)));

                MakeNewFile(filePath);
            }


            //Simulate access by others:
            List<Stream> lockedFiles = new List<Stream>();
            try
            {
                for (int i = 2; i < 5; i++)
                {
                    //lock up some files:

                    string filePath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].Directory.ToString(),
                                                   "test_{0}.txt".FormatStringInvariantCulture(i.Pad(2)));


                    lockedFiles.Add(new FileInfo(filePath).Open(FileMode.Append, FileAccess.Write, FileShare.None));

                }

                //ThreadPool has a delay before spinning up...not sure what it is...
                Thread.Sleep(1100);

                Thread.Sleep(1000);



                //Now let's see it moved to Success Folder:
                string successPath =
                    Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].SuccessStrategy.Directory.ToString(),
                                 "test_02.txt");

                Assert.IsTrue(File.Exists(successPath));

                //Update Times:
                for (int i = 3; i < 6; i++)
                {
                    string filePath =
                        Path.Combine(
                            _dropFolderWatcherInstance.Specs.Directories[0].SuccessStrategy.Directory.ToString(),
                            "test_{0}.txt".FormatStringInvariantCulture(i.Pad(2)));


                    DateTime newDate = DateTime.UtcNow.AddDays(-15);
                    new FileInfo(filePath).SetLastWriteTime(newDate);

                }

                Assert.AreEqual(_eventsCaptured.Count, 5);
                //Wait for archiver to kick in...
                Thread.Sleep(1000);

                Thread.Sleep(1000);

            }catch
            {
                
            }
            finally
            {
                foreach(Stream s in lockedFiles)
                {
                    s.Close();
                }
            }

        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_CreateNewFileAndArchiveMissingInfo()
        {
            
            _fileSystemWatcherService.Unregister(_dropFolderWatcherInstance);


            DeleteTmpFiles();

            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_dropFolderWatcherInstance.Enabled;

            string filePath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].Directory.ToString(), "test_02.txt");

            MakeNewFile(filePath);

            //ThreadPool has a delay before spinning up...not sure what it is...
            Thread.Sleep(1100);

            Thread.Sleep(1000);



            //Assert.AreEqual(_eventsCaptured.Count, 1);
            //Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);


            //Now let's see it moved to Success Folder:
            string successPath = Path.Combine(_dropFolderWatcherInstance.Specs.Directories[0].SuccessStrategy.Directory.ToString(), "test_02.txt");

            Assert.IsTrue(File.Exists(successPath));

        }


    }


}


