namespace XAct.IO.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Environment;
    using XAct.IO.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class PollingDirectoryWatcherUnitTests
    {
        private IDirectoryWatcher _fileSystemDirectoryWatcherInstance;
        private IEnvironmentService _environmentService;
        private List<FileEvent> _eventsCaptured;


        private string _dirPath;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Bootstrap all services found by reflection:
                        Singleton<IocContext>.Instance.ResetIoC();


            //FSIOService fsioService = null;
            //FSIOService o = fsioService;

            //Create a list to capture events as they are raised
            //for tests to see if events raised in what order:
            _eventsCaptured = new List<FileEvent>();


            _environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //Use a subfolder within bin/debug:
            _dirPath = _environmentService.MapPath("WatchMe");

            //For good measure delete any tmp files we used in previous tests:
            DeleteTmpFiles();

            //Need the 
            IDirectoryWatcherService fileSystemWatcherService =
                DependencyResolver.Current.GetInstance<IDirectoryWatcherService>();

            //Before adding a Watcher for these tests, 
            //clear out the old ones:
            fileSystemWatcherService.Clear();


            _fileSystemDirectoryWatcherInstance =
                fileSystemWatcherService.Register(
                    new DirectoryWatcherConfiguration
                        {
                            Path = _dirPath, 
                            Method=  FileSystemWatcherMethod.Polling,
                            IncludeSubDirectories=false
                        });
            ((IPollingDirectoryWatcher) _fileSystemDirectoryWatcherInstance).Interval 
                = new TimeSpan(0, 0, 0, 0,100);

            _fileSystemDirectoryWatcherInstance.Event += _fileSystemDirectoryWatcherInstance_Event;
            //XAct.IO.FileSystemDirectoryWatcherInstanceConfiguration 

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_CreateNewFile()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            Assert.AreEqual(0, _eventsCaptured.Count, "Events captured should be 0, as we just cleared it.");

            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path, "test.txt");

            ((IPollingDirectoryWatcher)_fileSystemDirectoryWatcherInstance).ResetTimer();

            MakeNewFile(filePath);

            Thread.Sleep(500);

            //It's possible that a poll already happened, so it could be 1, could be >1.
            Assert.AreEqual(1, _eventsCaptured.Count);
            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);
        }

        [Test]
        public void Test_CreateMany()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            
            Assert.AreEqual(0, _eventsCaptured.Count, "Events captured should be 0, as we just cleared it.");


            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;
            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path, "test{0}.txt");





            ((IPollingDirectoryWatcher)_fileSystemDirectoryWatcherInstance).ResetTimer();

            for (int i = 0; i < 50; i++)
            {
                MakeNewFile(filePath.FormatStringInvariantCulture(i));
                Thread.Sleep(10);
            }
            //As timer is 100, the following is plenty of time:
            Thread.Sleep(500);



            Assert.AreEqual(50, _eventsCaptured.Count);
            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);
        }






        [Test]
        public void CreateAndDeleteMany()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            //Make the first:
            Assert.AreEqual(0, _eventsCaptured.Count,"Events captured shoule be 0.");

            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path, "test.txt");


            ((IPollingDirectoryWatcher)_fileSystemDirectoryWatcherInstance).ResetTimer();

            for (int i = 0; i < 50; i++)
            {
                MakeNewFile(filePath);
            }

            //As poling interval is 100, plenty of time.
            Thread.Sleep(1000);


            //Deleted, Then Created
            Console.Write("Number of events are:" + _eventsCaptured.Count);
            Assert.IsTrue(_eventsCaptured.Count>=1, "Events Captured should be only 1 as all files are same name.");
            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured[0].ChangeTypes);

        }






//==============================================================================================










        /// <summary>
        /// Event handler for events captured by Poller/Watcher:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _fileSystemDirectoryWatcherInstance_Event(object sender, FileSystemEventArgs e)
        {
            _eventsCaptured.Add(new FileEvent(e));


        }


        /// <summary>
        /// Helper method to cleanup old test tmp files.
        /// </summary>
        private void DeleteTmpFiles()
        {
            string[] paths = Directory.GetFiles(_dirPath, "test*.txt",SearchOption.AllDirectories);
            foreach (string path2 in paths)
            {
                File.Delete(path2);
            }
        }


        /// <summary>
        /// Helper method for unit tests to create test output files.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void MakeNewFile(string filePath)
        {
            using (Stream stream = File.Create(filePath))
            {
                //bool b1 = stream.CanWrite;

                stream.AppendAllText("X", true);
                //bool b2 = stream.CanWrite;
            }
        }





    }


}


