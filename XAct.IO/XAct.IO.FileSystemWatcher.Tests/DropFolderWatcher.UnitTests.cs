namespace XAct.IO.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DropFolderUnitTests
    {
        private IDropFolderWatcher _dropFolderWatcher;
        private IEnvironmentService _environmentService;
        private List<FileEvent> _eventsCaptured;


        private string _dirPath;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Bootstrap all services found by reflection:
                        Singleton<IocContext>.Instance.ResetIoC();


            //FSIOService fsioService = null;
            //FSIOService o = fsioService;

            //Create a list to capture events as they are raised
            //for tests to see if events raised in what order:
            _eventsCaptured = new List<FileEvent>();


            _environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //Use a subfolder within bin/debug:
            _dirPath = _environmentService.MapPath("DropFolder");

            //For good measure delete any tmp files we used in previous tests:
            DeleteTmpFiles();

            //Need the 
            IDropFolderService dropFolderService =
                DependencyResolver.Current.GetInstance<IDropFolderService>();


            

            DropFolderSpecifications dropFolderSpecifications =
                new DropFolderSpecifications();

            List<IDropFolderDirectorySpecification> dropFolderDirectorySpecifications = new List<IDropFolderDirectorySpecification>();

            DropFolderDirectorySpecification dropFolderSpecs = new DropFolderDirectorySpecification
                                      {
                                          Name = "A1",
                                          Enabled = true,
                                          Directory = Path.Combine(_dirPath, "China"),
                                          IncludeSubDirectories = false,
                                          
                                          Tag = "Blah",
                                          SuccessStrategy = new DropFolderResultStrategySpecification
                                                                {
                                                                    Enabled = true,
                                                                    ArchiveAction = ArchivingAction.Move,
                                                                    ArchiveMoveTo = Path.Combine(_dirPath, @"China\Success")
                                                                },
                                          ErrorStrategy = new DropFolderResultStrategySpecification
                                                              {
                                                                  Enabled = true,
                                                                  ArchiveAction = ArchivingAction.Move,
                                                                  ArchiveMoveTo = Path.Combine(_dirPath, @"China\Error")
                                                              }
                                      };

            
            dropFolderDirectorySpecifications.Add(dropFolderSpecs);



            dropFolderSpecifications.Directories = dropFolderDirectorySpecifications.ToArray();



               _dropFolderWatcher = dropFolderService.Register(dropFolderSpecifications);

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        void _fileSystemDirectoryWatcherInstance_Event(object sender, FileSystemEventArgs e)
        {
            _eventsCaptured.Add(new FileEvent(e));


        }


        /// <summary>
        /// Helper method to cleanup old test tmp files.
        /// </summary>
        private void DeleteTmpFiles(string dirPath=null)
        {
            if (dirPath.IsNullOrEmpty())
            {
                dirPath = _dirPath;
            }
            string[] paths = Directory.GetFiles(dirPath, "test*.txt");
            foreach (string path2 in paths)
            {
                File.Delete(path2);
            }

        }


        /// <summary>
        /// Helper method for unit tests to create test output files.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private FileInfo MakeNewFile(string filePath)
        {
            using (Stream stream = File.Create(filePath))
            {
                //bool b1 = stream.CanWrite;

                stream.AppendAllText("X", true);
                //bool b2 = stream.CanWrite;
            }
            return new FileInfo(filePath);
        }


        [Test]
        public void Test_CreateFile()
        {
            //China:
            string folderPath = _dropFolderWatcher.Specs.Directories[0].Directory;
            string filePath = Path.Combine(folderPath, "test_01.txt");

            DeleteTmpFiles(folderPath);
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();


            MakeNewFile(filePath);
            Assert.IsTrue(File.Exists(filePath));
        }


        [Test]
        public void Test_LeaveFilesAlone()
        {
            //China:
            string folderPath = _dropFolderWatcher.Specs.Directories[0].Directory;
            string filePath = Path.Combine(folderPath, "test_02.txt");

            DeleteTmpFiles(folderPath);
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            Assert.AreEqual(0,_eventsCaptured.Count,"Events captured is not zero size.");

            MakeNewFile(filePath);
            Assert.IsTrue(File.Exists(filePath), "File was not created.");

            Thread.Sleep(1000);

            Assert.IsTrue(File.Exists(filePath),"File should still be there. But it isn't.");
        }



        /*[Test]
        public void Test_ClearOutOldFiles()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            //China:
            string filePath = Path.Combine(_dropFolderWatcher.Specs.Directories[0].ToString(), "test_01.txt");

            MakeNewFile(filePath);

            Thread.Sleep(1000);

            File.SetLastWriteTimeUtc(filePath, DateTime.UtcNow.Subtract(TimeSpan.FromDays(13.98)));
            Thread.Sleep(1100);

            Assert.IsTrue(File.Exists(filePath));



        }

        [Test]
        public void Test_ClearOutOldFiles2()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;
            //China:
            string filePath = Path.Combine(_dropFolderWatcher.Specs.Directories[0].ToString(), "test_01.txt");

            MakeNewFile(filePath);
            DateTime cutoff = DateTime.UtcNow.Subtract(TimeSpan.FromDays(14));
            File.SetLastWriteTimeUtc(filePath, cutoff);

            //Now that we've moved date back, we have to wait for next scan job:
            Thread.Sleep(1000);





            Assert.IsFalse(File.Exists(filePath));


        }

        [Test]
        public void Test_ClearOutOldFiles3()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            Console.WriteLine("Utc:" + DateTime.UtcNow);
            Console.WriteLine("Utc:" + XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().NowUTC);

            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;
            //China:
            string filePath = Path.Combine(_dropFolderWatcher.Specs.Directories[0].ToString(), "test_01.txt");

            FileInfo fileInfo = MakeNewFile(filePath);
            fileInfo.MoveTo(fileInfo.FullName + ".tmp");

            new FileInfo(filePath + ".tmp").MoveTo(filePath);

            fileInfo = new FileInfo(filePath);
            fileInfo.Refresh();
            Console.WriteLine("A1: " + fileInfo.LastWriteTimeUtc);
            Console.WriteLine("A2: " + fileInfo.GetFileAge());

            File.SetLastWriteTimeUtc(filePath, DateTime.UtcNow.Subtract(new TimeSpan(13, 23, 59, 58)));
            Console.WriteLine("B1: " + fileInfo.LastWriteTimeUtc);
            Console.WriteLine("B2: " + fileInfo.GetFileAge());
            Thread.Sleep(1200);
            Assert.IsTrue(File.Exists(filePath), "Test1");


            //Now that we've moved date back, we have to wait for next scan job:
            Thread.Sleep(1200);
            Console.WriteLine("C1: " + fileInfo.LastWriteTimeUtc);
            Console.WriteLine("C2: " + fileInfo.GetFileAge());
            Assert.IsFalse(File.Exists(filePath), "Test2");


        }

        */
    }


}


