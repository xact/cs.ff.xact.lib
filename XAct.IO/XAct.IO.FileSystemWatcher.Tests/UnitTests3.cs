namespace XAct.IO.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.IO.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class UnitTests3
    {
        private IDirectoryWatcher _fileSystemDirectoryWatcherInstance;
        private IEnvironmentService _environmentService;
        private List<FileEvent> _eventsCaptured;


        private string _dirPath;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Bootstrap all services found by reflection:
                        Singleton<IocContext>.Instance.ResetIoC();


            //FSIOService fsioService = null;
            //FSIOService o = fsioService;

            //Create a list to capture events as they are raised
            //for tests to see if events raised in what order:
            _eventsCaptured = new List<FileEvent>();


            _environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //Use a subfolder within bin/debug:
            _dirPath = _environmentService.MapPath("WatchMe");

            //For good measure delete any tmp files we used in previous tests:
            DeleteTmpFiles();

            //Need the 
            IDirectoryWatcherService fileSystemWatcherService =
                DependencyResolver.Current.GetInstance<IDirectoryWatcherService>();

            //Before adding a Watcher for these tests, 
            //clear out the old ones:
            fileSystemWatcherService.Clear();

            _fileSystemDirectoryWatcherInstance =
                fileSystemWatcherService.Register(new DirectoryWatcherConfiguration { Path = _dirPath, Method=  FileSystemWatcherMethod.Polling,IncludeSubDirectories=false });

            ((IPollingDirectoryWatcher) _fileSystemDirectoryWatcherInstance).Interval = new TimeSpan(0, 0, 0, 0,100);

            _fileSystemDirectoryWatcherInstance.Event += _fileSystemDirectoryWatcherInstance_Event;
            //XAct.IO.FileSystemDirectoryWatcherInstanceConfiguration 

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        void _fileSystemDirectoryWatcherInstance_Event(object sender, FileSystemEventArgs e)
        {
            _eventsCaptured.Add(new FileEvent(e));


        }


        /// <summary>
        /// Helper method to cleanup old test tmp files.
        /// </summary>
        private void DeleteTmpFiles()
        {
            string[] paths = Directory.GetFiles(_dirPath, "test*.txt");
            foreach (string path2 in paths)
            {
                File.Delete(path2);
            }
        }


        /// <summary>
        /// Helper method for unit tests to create test output files.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void MakeNewFile(string filePath)
        {
            using (Stream stream = File.Create(filePath))
            {
                //bool b1 = stream.CanWrite;

                stream.AppendAllText("X", true);
                //bool b2 = stream.CanWrite;
            }
        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_CreateNewFile()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            
            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path , "test.txt");

            MakeNewFile(filePath);

            Thread.Sleep(500);

            IIOService ioService = DependencyResolver.Current.GetInstance<IFSIOService>();
#pragma warning disable 168
            using (Stream streamOut = ioService.FileOpenWriteAsync(filePath,true,true).WaitAndGetResult())
            {
                try
                {
                    using (Stream streamIn = ioService.FileOpenAsync(filePath,XAct.IO.FileMode.Open,XAct.IO.FileAccess.Read,XAct.IO.FileShare.ReadWrite).WaitAndGetResult())
                    {
                        //will throw an exception
                    }
                }
                catch (System.Exception e)
                {
                    //Err message will be:
                    //The process cannot access the file 'C:\DATA\HG\cs.ff.xact.lib\XAct.IO\XAct.IO.FileSystemWatcher.NUnit.Tests\bin\Debug\WatchMe\test.txt' because it is being used by another process.
                    string msg = e.Message;
                }
            }
#pragma warning restore 168


            Assert.AreEqual(_eventsCaptured.Count, 1);
            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);
        }


    }


}


