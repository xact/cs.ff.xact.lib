namespace XAct.IO.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.IO.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DirectoryWatcherServiceUnitTests
    {
        private IDirectoryWatcher _fileSystemDirectoryWatcherInstance;
        private IEnvironmentService _environmentService;
        private List<FileEvent> _eventsCaptured;


        private string _dirPath;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Bootstrap all services found by reflection:
                        Singleton<IocContext>.Instance.ResetIoC();


            //FSIOService fsioService = null;
            //FSIOService o = fsioService;

            //Create a list to capture events as they are raised
            //for tests to see if events raised in what order:
            _eventsCaptured = new List<FileEvent>();


            _environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //Use a subfolder within bin/debug:
            _dirPath = _environmentService.MapPath("WatchMe");

            //For good measure delete any tmp files we used in previous tests:
            DeleteTmpFiles();



            //Need the 
            IDirectoryWatcherService fileSystemWatcherService =
                DependencyResolver.Current.GetInstance<IDirectoryWatcherService>();


            //Before adding a Watcher for these tests, 
            //clear out the old ones:
            fileSystemWatcherService.Clear();

            DirectoryWatcherConfiguration config = new DirectoryWatcherConfiguration { Path = _dirPath };

            //Pick up Created -- and Dleted.
            config.WatcherChangeTypes = WatcherChangeTypes.All;


            
            _fileSystemDirectoryWatcherInstance =
                fileSystemWatcherService.Register(config);

            _fileSystemDirectoryWatcherInstance.Event += _fileSystemDirectoryWatcherInstance_Event;
            //XAct.IO.FileSystemDirectoryWatcherInstanceConfiguration 

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        void _fileSystemDirectoryWatcherInstance_Event(object sender, FileSystemEventArgs e)
        {
            _eventsCaptured.Add(new FileEvent(e));


        }


        /// <summary>
        /// Helper method to cleanup old test tmp files.
        /// </summary>
        private void DeleteTmpFiles()
        {
            string[] paths = Directory.GetFiles(_dirPath, "test*.txt");
            foreach (string path2 in paths)
            {
                File.Delete(path2);
            }
        }


        /// <summary>
        /// Helper method for unit tests to create test output files.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void MakeNewFile(string filePath)
        {
            using (Stream stream = File.Create(filePath))
            {
                //bool b1 = stream.CanWrite;

                stream.AppendAllText("X", true);
                //bool b2 = stream.CanWrite;
            }
        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_CreateNewFile()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            
            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path.ToString() , "test_01.txt");

            MakeNewFile(filePath);

            Thread.Sleep(500);



            Assert.AreEqual(_eventsCaptured.Count, 1);
            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);
        }

        [Test]
        public void Test_CreateMany()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;
            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path.ToString(), "test{0}.txt");


            for (int i = 0; i < 50; i++)
            {
                MakeNewFile(filePath.FormatStringInvariantCulture(i));
                Thread.Sleep(10);
            }

            Thread.Sleep(500);



            Assert.AreEqual(50, _eventsCaptured.Count);
            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);
        }


        [Test]
        public void MovingAFile()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;
            
            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path.ToString(), "test.txt");
            string newPath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path.ToString(), "test2.txt");


            //Make the first:
            MakeNewFile(filePath);

            Thread.Sleep(10);


            _eventsCaptured.Clear();


            //Move it:
            File.Move(filePath, newPath);
            
            Thread.Sleep(100);


            //Deleted, Then Created
            Assert.AreEqual(1,_eventsCaptured.Count);
            Assert.AreEqual(WatcherChangeTypes.Renamed, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);

        }



        [Test]
        public void MovingMany()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path.ToString(), "test.txt");
            string newPath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path.ToString(), "test2.txt");


            //Make the first:
            MakeNewFile(filePath);

            Thread.Sleep(10);


            _eventsCaptured.Clear();


            for (int i = 0; i < 50; i++)
            {
                File.Move(filePath, newPath);
                File.Move(newPath, filePath);
            }

            Thread.Sleep(100);


            //Deleted, Then Created
            Assert.AreEqual(100, _eventsCaptured.Count);
            Assert.AreEqual(WatcherChangeTypes.Renamed, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);

        }



        [Test]
        public void DeleteOne()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path.ToString(), "test.txt");


            //Make the first:
            MakeNewFile(filePath);

            Thread.Sleep(10);

            Assert.AreEqual(1, _eventsCaptured.Count,"T1");

            _eventsCaptured.Clear();

            File.Delete(filePath);

 
            Thread.Sleep(100);


            //Deleted, Then Created
            Assert.AreEqual(1, _eventsCaptured.Count,"T2");
            Assert.AreEqual(WatcherChangeTypes.Deleted, _eventsCaptured.FirstOrDefaultEx().ChangeTypes);

        }


        [Test]
        public void CreateAndDeleteMany()
        {
            DeleteTmpFiles();
            Thread.Sleep(10);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            string filePath = Path.Combine(_fileSystemDirectoryWatcherInstance.Path.ToString(), "test.txt");

            _eventsCaptured.Clear();

            //Make the first:

            Thread.Sleep(10);



            for (int i = 0; i < 50; i++)
            {
                MakeNewFile(filePath);
                File.Delete(filePath);
                Thread.Sleep(1);
            }


            Thread.Sleep(100);


            //Deleted, Then Created
            Assert.AreEqual(100, _eventsCaptured.Count);
            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured[0].ChangeTypes);
            Assert.AreEqual(WatcherChangeTypes.Deleted, _eventsCaptured[1].ChangeTypes);

            Assert.AreEqual(WatcherChangeTypes.Created, _eventsCaptured[98].ChangeTypes);
            Assert.AreEqual(WatcherChangeTypes.Deleted, _eventsCaptured[99].ChangeTypes);
        }







    }


}


