namespace XAct.IO.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.IO.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DirectoryArvhiverUnitTests
    {
        private IDirectoryArchiver _fileSystemDirectoryArchiverInstance;
        private IEnvironmentService _environmentService;
        private List<FileEvent> _eventsCaptured;


        private string _dirPath;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Bootstrap all services found by reflection:
                        Singleton<IocContext>.Instance.ResetIoC();


            //FSIOService fsioService = null;
            //FSIOService o = fsioService;

            //Create a list to capture events as they are raised
            //for tests to see if events raised in what order:
            _eventsCaptured = new List<FileEvent>();


            _environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //Use a subfolder within bin/debug:
            _dirPath = _environmentService.MapPath("WatchMe");

            //For good measure delete any tmp files we used in previous tests:
            DeleteTmpFiles();

            //Need the 
            IDirectoryArchivingService directoryArchivingService =
                DependencyResolver.Current.GetInstance<IDirectoryArchivingService>();


            _fileSystemDirectoryArchiverInstance =
                directoryArchivingService.Register(
                new DirectoryArchiverConfiguration { 
                    Directory = _dirPath, 
                    ArchiveAction = ArchivingAction.Delete,
                    ArchiveCheckInterval = new TimeSpan(0,0,0,0,100),
                    ArchiveDelay = new TimeSpan(14,0,0,0)

                    });

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        void _fileSystemDirectoryWatcherInstance_Event(object sender, FileSystemEventArgs e)
        {
            _eventsCaptured.Add(new FileEvent(e));


        }


        /// <summary>
        /// Helper method to cleanup old test tmp files.
        /// </summary>
        private void DeleteTmpFiles()
        {
            string[] paths = Directory.GetFiles(_dirPath, "test*.txt");
            foreach (string path2 in paths)
            {
                File.Delete(path2);
            }

        }


        /// <summary>
        /// Helper method for unit tests to create test output files.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private FileInfo MakeNewFile(string filePath)
        {
            using (Stream stream = File.Create(filePath))
            {
                //bool b1 = stream.CanWrite;

                stream.AppendAllText("X", true);
                //bool b2 = stream.CanWrite;
            }
            return new FileInfo(filePath);
        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Test_LeaveFilesAlone()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();
            
            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            string filePath = Path.Combine(_fileSystemDirectoryArchiverInstance.Directory.ToString() , "test_01.txt");

            MakeNewFile(filePath);

            Thread.Sleep(1000);

            Assert.IsTrue(File.Exists(filePath));
        }

        [Test]
        public void Test_ClearOutOldFiles()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;

            string filePath = Path.Combine(_fileSystemDirectoryArchiverInstance.Directory.ToString(), "test_02.txt");

            MakeNewFile(filePath);

            Thread.Sleep(1000);

            File.SetLastWriteTimeUtc(filePath, DateTime.UtcNow.Subtract(TimeSpan.FromDays(13.98)));
            Thread.Sleep(1100);

            Assert.IsTrue(File.Exists(filePath));



        }

        [Test]
        public void Test_ClearOutOldFiles2()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;
            string filePath = Path.Combine(_fileSystemDirectoryArchiverInstance.Directory.ToString(), "test_03.txt");

            MakeNewFile(filePath);
            DateTime cutoff = DateTime.UtcNow.Subtract(TimeSpan.FromDays(14));
            File.SetLastWriteTimeUtc(filePath, cutoff);

            //Now that we've moved date back, we have to wait for next scan job:
            Thread.Sleep(1000);





            Assert.IsFalse(File.Exists(filePath));


        }

        [Test]
        public void Test_ClearOutOldFiles3()
        {
            DeleteTmpFiles();
            Thread.Sleep(150);//Need time to finish deleting before clearing events.
            _eventsCaptured.Clear();

            Console.WriteLine("Utc:" + DateTime.UtcNow);
            Console.WriteLine("Utc:" + XAct.DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC);

            //bool enabled=_fileSystemDirectoryWatcherInstance.Enabled;
            string filePath = Path.Combine(_fileSystemDirectoryArchiverInstance.Directory.ToString(), "test_04.txt");
            FileInfo fileInfo = MakeNewFile(filePath);
            fileInfo.MoveTo(fileInfo.FullName + ".tmp");

            new FileInfo(filePath + ".tmp").MoveTo(filePath);

            fileInfo = new FileInfo(filePath);
            fileInfo.Refresh();
            Console.WriteLine("A1: " + fileInfo.LastWriteTimeUtc);
            Console.WriteLine("A2: " + fileInfo.GetFileAge());

            File.SetLastWriteTimeUtc(filePath, DateTime.UtcNow.Subtract(new TimeSpan(13, 23, 59, 58)));
            Console.WriteLine("B1: " + fileInfo.LastWriteTimeUtc);
            Console.WriteLine("B2: " + fileInfo.GetFileAge());
            Thread.Sleep(1200);
            Assert.IsTrue(File.Exists(filePath), "Test1");


            //Now that we've moved date back, we have to wait for next scan job:
            Thread.Sleep(1200);
            Console.WriteLine("C1: " + fileInfo.LastWriteTimeUtc);
            Console.WriteLine("C2: " + fileInfo.GetFileAge());
            Assert.IsFalse(File.Exists(filePath), "Test2");


        }


    }


}


