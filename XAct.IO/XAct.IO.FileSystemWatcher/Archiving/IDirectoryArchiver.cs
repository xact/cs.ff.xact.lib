// ReSharper disable CheckNamespace
namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using System;
    
    /// <summary>
    /// Contract for an object created by <see cref="IDirectoryArchivingService"/>
    /// from a <see cref="IDirectoryArchiverConfiguration"/>
    /// </summary>
    public interface IDirectoryArchiver: IHasEnabled, IHasNameReadOnly
    {
        ///// <summary>
        ///// Gets or sets a value indicating whether this <see cref="IDirectoryArchiver"/> is enabled.
        ///// <para>
        ///// Default is <c>true</c>
        ///// </para>
        ///// </summary>
        ///// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        //bool Enabled { get; set; }

        ///// <summary>
        ///// Gets the name.
        ///// <para>
        ///// The name will be used as the key within the <see cref="IDirectoryArchiverCache"/>
        ///// </para>
        ///// </summary>
        ///// <value>The name.</value>
        //string Name { get; }

        /// <summary>
        /// Gets the path of the file to watch.
        /// <para>
        /// Default is <c>null</c>
        /// </para>
        /// </summary>
        /// <value>The path.</value>
        string Directory { get; }

        /// <summary>
        /// Gets or sets the interval in between checks.
        /// <para>
        /// The default is <c>30</c> minutes.
        /// </para>
        /// </summary>
        /// <value>The check interval.</value>
        TimeSpan CheckInterval { get; }

        /// <summary>
        /// Gets or sets how old a file has to be before it is archived.
        /// <para>
        /// The default is <c>14</c> days.
        /// </para>
        /// </summary>
        /// <value>The delay.</value>
        TimeSpan ArchiveDelay { get; }
        
        /// <summary>
        /// Gets or sets the archiving action to take (Move, Delete, etc).
        /// <para>
        /// The default is <c>NoAction</c>
        /// </para>
        /// </summary>
        /// <value>The action.</value>
        ArchivingAction ArchiveAction { get; }
        /// <summary>
        /// Gets or sets the file attribute to use (FileCreated, LastAccessed, LastWritten)
        /// when determining if the <see cref="ArchiveDelay"/> has passed.
        /// <para>
        /// The default is FileCreationDate.
        /// </para>
        /// </summary>
        /// <value>The file info date source.</value>
        FileDateType FileInfoDateType { get; }

        /// <summary>
        /// Gets or sets the directory to move the file to (if the <see cref="ArchiveAction"/> is Move).
        /// <para>
        /// The default is null.
        /// </para>
        /// </summary>
        /// <value>The move to directory.</value>
        string ArchviveToDirectory { get; }
    }
}