namespace XAct.IO.Implementations
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IDirectoryArchivingService"/>
    /// to provide a service to register folder watchers that will
    /// periodically scan for files that can be archived.
    /// </summary>
    public class DirectoryArchivingService : IDirectoryArchivingService
    {
        private readonly XAct.Diagnostics.ITracingService _tracingService;
        private readonly IIOService _ioService;
        private readonly IDirectoryArchivingServiceState _DirectoryArchiverCache;


        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryArchivingService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ioService">The io service.</param>
        /// <param name="directoryArchiverCache">The directory archiver cache.</param>
        public DirectoryArchivingService(
            ITracingService tracingService,
            IIOService ioService,
            IDirectoryArchivingServiceState directoryArchiverCache)
        {
            _tracingService = tracingService;
            _ioService = ioService;
            _DirectoryArchiverCache = directoryArchiverCache;
        }

        /// <summary>
        /// Registers a new Directory watcher, that
        /// will periodically scan for old files to archive.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        public IDirectoryArchiver Register(IDirectoryArchiverConfiguration configuration)
        {
            //Create a new instance of folder watcher:
            IDirectoryArchiver archiver = new DirectoryArchiver(_tracingService,_ioService, configuration);

            //cache for later:
            _DirectoryArchiverCache.Add(archiver.Name, archiver);

            //return for further handling as required:
            return archiver;
        }

    }

}