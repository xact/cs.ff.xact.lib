namespace XAct.IO
{

    /// <summary>
    /// A service to manage the automatic archiving of files in a folder.
    /// </summary>
    public interface IDirectoryArchivingService : IHasXActLibService
    {
        /// <summary>
        /// Registers a new Directory watcher, that 
        /// will periodically scan for old files to archive.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        IDirectoryArchiver Register(IDirectoryArchiverConfiguration configuration);
    }
}