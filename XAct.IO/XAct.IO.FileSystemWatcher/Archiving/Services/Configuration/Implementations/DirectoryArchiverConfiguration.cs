// ReSharper disable CheckNamespace
namespace XAct.IO.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IDirectoryArchiverConfiguration"/>
    /// contract to provide an argument package from which 
    /// the <see cref="IDirectoryArchivingService"/> can build
    /// an implementation of <see cref="IDirectoryArchiver"/>.
    /// </summary>
    [DefaultBindingImplementation(typeof(IDirectoryArchiverConfiguration), BindingLifetimeType.TransientScope, Priority.Low)]
    public class DirectoryArchiverConfiguration : IDirectoryArchiverConfiguration
    {

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IDirectoryArchiverConfiguration"/> is enabled.
        /// <para>
        /// The default is <c>true</c> (although <see cref="ArchiveAction"/> is NoAction)
        /// 	</para>
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the unique name of this <see cref="IDirectoryArchiver"/>.
        /// <para>
        /// The name will be used as the key within the <see cref="IDirectoryArchivingServiceState"/>
        /// </para>
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the path to periodically scan for archiveable files.
        /// <para>
        /// The default is <c>null</c>
        /// 	</para>
        /// </summary>
        /// <value>The path.</value>
        public string Directory { get; set; }

        /// <summary>
        /// Gets or sets how old a file has to be before it is archived.
        /// <para>
        /// The default is <c>14</c> days.
        /// </para>
        /// </summary>
        /// <value>The delay.</value>
        public TimeSpan ArchiveDelay { get; set; }

        /// <summary>
        /// Gets or sets the archiving action to take (Move, Delete, etc).
        /// <para>
        /// The default is <c>NoAction</c>
        /// 	</para>
        /// </summary>
        /// <value>The action.</value>
        public ArchivingAction ArchiveAction { get; set; }

        /// <summary>
        /// Gets or sets the file attribute to use (FileCreated, LastAccessed, LastWritten)
        /// when determining if the <see cref="ArchiveDelay"/> has passed.
        /// <para>
        /// The default is FileCreationDate.
        /// </para>
        /// </summary>
        /// <value>The file info date source.</value>
        public FileDateType FileInfoDateType { get; set; }

        /// <summary>
        /// Gets or sets the directory to move the file to (if the <see cref="ArchiveAction"/> is Move).
        /// <para>
        /// The default is null.
        /// </para>
        /// </summary>
        /// <value>The move to directory.</value>
        public string ArchiveMoveToDirectory { get; set; }

        /// <summary>
        /// Gets or sets the interval in between checks.
        /// <para>
        /// The default is <c>15</c> minutes.
        /// </para>
        /// <para>
        /// Minimum is 1 minute.
        /// </para>
        /// </summary>
        /// <value>The check interval.</value>
        public TimeSpan ArchiveCheckInterval { get { return _checkInterval; } set { _checkInterval = (value > TimeSpan.FromSeconds(100)) ? value : TimeSpan.FromMilliseconds(100); } }

        /// <summary>
        /// 
        /// </summary>
        private TimeSpan _checkInterval;






        /// <summary>
        /// Initializes the specified archive check interval.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="archiveCheckInterval">The archive check interval.</param>
        /// <param name="directoryFullName">The directory.</param>
        /// <param name="archiveDelay">The archive delay.</param>
        /// <param name="archiveAction">The archive action.</param>
        /// <param name="archiveMoveToDirectory">The archive move to directory.</param>
        /// <param name="fileInfoDateType">Type of the file info date.</param>
        public void Initialize(bool enabled, TimeSpan archiveCheckInterval, string directoryFullName, 
            TimeSpan archiveDelay, ArchivingAction archiveAction, string archiveMoveToDirectory, FileDateType fileInfoDateType)
        {
            this.Enabled = enabled;
            this.ArchiveCheckInterval = archiveCheckInterval;
            this.Directory = directoryFullName;
            this.ArchiveDelay = archiveDelay;
            this.ArchiveAction = archiveAction;

            this.ArchiveMoveToDirectory = archiveMoveToDirectory;
            this.FileInfoDateType = fileInfoDateType;

        }



        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryArchiverConfiguration"/> class.
        /// </summary>
        public DirectoryArchiverConfiguration()
        {
            // ReSharper disable RedundantNameQualifier
            Name = "Unspecified_{0}_{1}_{2}".FormatStringInvariantCulture(
                DateTime.Now.Ticks,XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random.Next(0,1000),
                XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random.Next(0,1000));
            // ReSharper restore RedundantNameQualifier

            Enabled = true;
            ArchiveCheckInterval = TimeSpan.FromMinutes(15);
            ArchiveAction = ArchivingAction.NoAction;
            ArchiveDelay = new TimeSpan(14, 0, 0, 0);
            Directory = null;
            FileInfoDateType = FileDateType.Created;
            ArchiveMoveToDirectory = null;
        }
    }
}