// ReSharper disable CheckNamespace
namespace XAct.IO

// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// The contract for the configation to use when creating 
    /// an instance derived from <see cref="IDirectoryArchiver"/>.
    /// </summary>
    public interface IDirectoryArchiverConfiguration: IHasEnabled, IHasName
    {
        ///// <summary>
        ///// Gets or sets a value indicating whether this <see cref="IDirectoryArchiverConfiguration"/> is enabled.
        ///// <para>
        ///// The default is <c>true</c> (although Action is NoAction)
        ///// </para>
        ///// </summary>
        ///// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        //bool Enabled { get; set; }

        ///// <summary>
        ///// Gets or sets the unique name.
        ///// <para>
        ///// The name 
        ///// </para>
        ///// </summary>
        ///// <value>The name.</value>
        //string Name { get; set; }

        /// <summary>
        /// Gets or sets the interval in between checks.
        /// <para>
        /// The default is <c>30</c> minutes.
        /// </para>
        /// <para>
        /// Minimum is <c>15</c> minutes.
        /// </para>
        /// </summary>
        /// <value>The check interval.</value>
        TimeSpan ArchiveCheckInterval { get; }

        /// <summary>
        /// Gets or sets the path to periodically scan for archiveable files.
        /// <para>
        /// The default is <c>null</c>
        /// </para>
        /// </summary>
        /// <value>The path.</value>
        string Directory { get;  }

        /// <summary>
        /// Gets or sets how old a file has to be before it is archived.
        /// <para>
        /// The default is <c>14</c> days.
        /// </para>
        /// </summary>
        /// <value>The delay.</value>
        TimeSpan ArchiveDelay { get;  }

        /// <summary>
        /// Gets or sets the archiving action to take (Move, Delete, etc).
        /// <para>
        /// The default is <c>NoAction</c>
        /// </para>
        /// </summary>
        /// <value>The action.</value>
        ArchivingAction ArchiveAction { get; }


        /// <summary>
        /// Gets or sets the directory to move the file to (if the <see cref="ArchiveAction"/> is Move).
        /// <para>
        /// The default is null.
        /// </para>
        /// </summary>
        /// <value>The move to directory.</value>
        string ArchiveMoveToDirectory { get; }

        /// <summary>
        /// Gets or sets the file attribute to use (FileCreated, LastAccessed, LastWritten)
        /// when determining if the <see cref="ArchiveDelay"/> has passed.
        /// <para>
        /// The default is FileCreationDate.
        /// </para>
        /// </summary>
        /// <value>The file info date source.</value>
        FileDateType FileInfoDateType { get; }


        /// <summary>
        /// Initializes the specified archive check interval.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="archiveCheckInterval">The archive check interval.</param>
        /// <param name="directoryFullName">The directory.</param>
        /// <param name="archiveDelay">The archive delay.</param>
        /// <param name="archiveAction">The archive action.</param>
        /// <param name="archiveMoveToDirectory">The archive move to directory.</param>
        /// <param name="fileInfoDateType">Type of the file info date.</param>
        void Initialize(bool enabled, TimeSpan archiveCheckInterval, string directoryFullName, TimeSpan archiveDelay,
                        ArchivingAction archiveAction, string archiveMoveToDirectory,
                        FileDateType fileInfoDateType);
    }
}