namespace XAct.IO
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a singleton cache of the instantiates of <see cref="IDirectoryArchiver"/>
    /// registered with <see cref="IDirectoryArchivingService"/>
    /// </summary>
    public interface IDirectoryArchivingServiceState : IDictionary<string, IDirectoryArchiver>, IHasXActLibServiceState
    {

    }
}