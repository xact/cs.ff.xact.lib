namespace XAct.IO.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// An singleton implementation of <see cref="IDirectoryArchivingServiceState"/>
    /// backing the <see cref="IDirectoryArchivingService"/>.
    /// </summary>
    public class DirectoryArchivingServiceState : Dictionary<string, IDirectoryArchiver>, IDirectoryArchivingServiceState
    {

    }
}