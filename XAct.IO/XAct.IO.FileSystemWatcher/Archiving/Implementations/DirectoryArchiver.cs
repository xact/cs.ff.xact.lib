﻿// ReSharper disable CheckNamespace
namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using System;
    using System.IO;
    using XAct.Diagnostics;

    //NO: [DefaultBindingImplementation(typeof(IDirectoryArchivingConfiguration),BindingLifetimeType.TransientScope)]
    /// <summary>
    /// Inmplementation of <see cref="IDirectoryArchiver"/>,
    /// created by <see cref="IDirectoryArchivingService"/>
    /// from values provided using <see cref="IDirectoryArchiverConfiguration"/>
    /// </summary>
    public class DirectoryArchiver : IDirectoryArchiver
    {
        private readonly System.Timers.Timer _timer;

        private readonly ITracingService _tracingService;
        private readonly IIOService _ioService;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DirectoryArchiver"/> is enabled.
        /// <para>
        /// Default is <c>Enabled</c> (although <see cref="ArchiveAction"/> is NoAction).
        /// </para>
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled { get { return _timer.Enabled; } set { _timer.Enabled = value; } }

        /// <summary>
        /// Gets or sets the unique name of this <see cref="IDirectoryArchiver"/>.
        /// <para>
        /// The name will be used as the key within the <see cref="IDirectoryArchivingServiceState"/>
        /// </para>
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; private set; }


        /// <summary>
        /// Gets the path of the file to watch.
        /// <para>
        /// Default is <c>null</c>
        /// 	</para>
        /// </summary>
        /// <value>The path.</value>
        public string Directory { get; private set; }

        /// <summary>
        /// Gets or sets how old a file has to be before it is archived.
        /// <para>
        /// The default is <c>14</c> days.
        /// </para>
        /// </summary>
        /// <value>The delay.</value>
        public TimeSpan ArchiveDelay { get; private set; }

        /// <summary>
        /// Gets or sets the archiving action to take (Move, Delete, etc).
        /// <para>
        /// The default is <c>NoAction</c>
        /// 	</para>
        /// </summary>
        /// <value>The action.</value>
        public ArchivingAction ArchiveAction { get; private set; }

        /// <summary>
        /// Gets or sets the file attribute to use (FileCreated, LastAccessed, LastWritten)
        /// when determining if the <see cref="ArchiveDelay"/> has passed.
        /// <para>
        /// The default is FileCreationDate.
        /// </para>
        /// </summary>
        /// <value>The file info date source.</value>
        public FileDateType FileInfoDateType { get; private set; }

        /// <summary>
        /// Gets or sets the directory to move the file to (if the <see cref="ArchiveAction"/> is Move).
        /// <para>
        /// The default is null.
        /// </para>
        /// </summary>
        /// <value>The move to directory.</value>
        public string ArchviveToDirectory { get; private set; }


        /// <summary>
        /// Gets or sets the interval in between checks.
        /// <para>
        /// The default is <c>30</c> minutes.
        /// </para>
        /// </summary>
        /// <value>The check interval.</value>
        public TimeSpan CheckInterval { get; private set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryArchiver" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="directoryArchivingConfiguration">The directory archiving configuration.</param>
        /// <param name="ioService">The io service.</param>
        public DirectoryArchiver(ITracingService tracingService, IIOService ioService, IDirectoryArchiverConfiguration directoryArchivingConfiguration)
        {
            _tracingService = tracingService;
            _ioService = ioService;

            _timer = new System.Timers.Timer();


            //After timer is created (as enabled and another prop talks to timer)
            // ReSharper disable RedundantThisQualifier
            this.ArchiveAction = directoryArchivingConfiguration.ArchiveAction;
            this.CheckInterval = directoryArchivingConfiguration.ArchiveCheckInterval;
            this.ArchiveDelay = directoryArchivingConfiguration.ArchiveDelay;
            this.Enabled = directoryArchivingConfiguration.Enabled;
            this.FileInfoDateType = directoryArchivingConfiguration.FileInfoDateType;
            this.ArchviveToDirectory = directoryArchivingConfiguration.ArchiveMoveToDirectory;
            this.Name = directoryArchivingConfiguration.Name;
            this.Directory = directoryArchivingConfiguration.Directory;


            _timer.Interval = this.CheckInterval.TotalMilliseconds;
// ReSharper disable RedundantDelegateCreation
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsed);
// ReSharper restore RedundantDelegateCreation


            TimerElapsed(null, null);
            // ReSharper restore RedundantThisQualifier
        }

        private void TimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (this.Directory == null)
            {
                _tracingService.Trace(TraceLevel.Verbose, "DirectoryArchiver for {0} has no Path defined.", this.Name);
                return;
            }
            if (!_ioService.DirectoryExistsAsync(this.Directory,true).WaitAndGetResult())
            {
                _tracingService.Trace(TraceLevel.Verbose, "DirectoryArchiver for {0} has a Path that does not exist.", this.Name);
                return;
            }
            if (this.ArchiveDelay == new TimeSpan(0, 0, 0))
            {
                _tracingService.Trace(TraceLevel.Verbose, "DirectoryArchiver for {0} has a Delay value that is not set.", this.Name);
                return;
            }


            foreach (string fileFullName in _ioService.GetDirectoryFileNamesAsync(this.Directory).WaitAndGetResult())
            {
                 FileInfo fileInfo = new FileInfo(fileFullName);

                TimeSpan fileAge = fileInfo.GetFileAge();

                if (fileAge < ArchiveDelay)
                {
                    //Not stale enough.
                    continue;
                }

                try
                {
                    switch (ArchiveAction)
                    {
                        case ArchivingAction.Delete:
                            _tracingService.Trace(TraceLevel.Verbose, "DirectoryArchiver for {0}: Deleting {1}.",
                                                  this.Name, fileInfo.Name);
                            fileInfo.Delete();
                            break;
                        case ArchivingAction.Move:
// ReSharper disable RedundantNameQualifier
                            string destFileName = System.IO.Path.Combine(this.ArchviveToDirectory.ToString(), fileInfo.Name);
// ReSharper restore RedundantNameQualifier
                            _tracingService.Trace(TraceLevel.Verbose, "DirectoryArchiver for {0}: Moving {1} to {2}.",
                                                  this.Name, fileInfo.Name, destFileName);
                            fileInfo.MoveTo(destFileName);
                            break;
                    }
                }
// ReSharper disable RedundantNameQualifier
                catch (System.Exception err)
// ReSharper restore RedundantNameQualifier
                {
                    _tracingService.TraceException(TraceLevel.Error, err, "Error occurred in DirectoryArchiving '{0}' implementing '{1}' on  '{1}'.",
                                          this.Name, ArchiveAction, fileInfo.Name);
                }

            }
        }


    }
}
