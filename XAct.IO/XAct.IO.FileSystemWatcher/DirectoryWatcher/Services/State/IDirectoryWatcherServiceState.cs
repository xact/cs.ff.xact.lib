﻿namespace XAct.IO
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a cache of <see cref="IDirectoryWatcher"/>
    /// insantiated by <see cref="IDirectoryWatcherService"/>
    /// </summary>
    public interface IDirectoryWatcherServiceState : IList<IDirectoryWatcher>, IHasXActLibServiceState
    {
    }
}
