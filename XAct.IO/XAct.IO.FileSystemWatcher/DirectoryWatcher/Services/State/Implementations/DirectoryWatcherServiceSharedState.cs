namespace XAct.IO.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IDirectoryWatcherServiceState"/>
    /// to keep track of <see cref="IDirectoryWatcher"/> objects
    /// created by an implementation of <see cref="IDirectoryWatcherService"/>.
    /// </summary>
    public class DirectoryWatcherServiceState : List<IDirectoryWatcher>, IDirectoryWatcherServiceState
    {
    }
}