﻿namespace XAct.IO
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for setting up
    /// <see cref="FileSystemDirectoryWatcher"/>
    /// to watch for file changes in a directory.
    /// </summary>
    public interface IDirectoryWatcherService : IHasXActLibService
    {

        /// <summary>
        /// Gets the collection of watchers.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        IEnumerable<IDirectoryWatcher> State { get; }


        /// <summary>
        /// Disables and clears all watchers.
        /// </summary>
        void Clear();

        /// <summary>
        /// Sets up a <see cref="FileSystemDirectoryWatcher"/> 
        /// to monitor a directory or file according to the specs
        /// given in <paramref name="watcherInstanceConfiguration"/>
        /// </summary>
        /// <param name="watcherInstanceConfiguration">The watcher instance configuration.</param>
        /// <returns></returns>
        IDirectoryWatcher Register(IDirectoryWatcherConfiguration watcherInstanceConfiguration);

        /// <summary>
        /// Removes the specified <see cref="IDirectoryWatcher"/> from the <see cref="IDirectoryWatcherServiceState"/>
        /// after Disabling it.
        /// </summary>
        /// <param name="fileSystemDirectoryWatcherInstance">The file system directory watcher instance.</param>
        void Remove(IDirectoryWatcher fileSystemDirectoryWatcherInstance);

    }
}