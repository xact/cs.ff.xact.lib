namespace XAct.IO
{
    /// <summary>
    /// Type of polling mechanism to use.
    /// </summary>
    public enum FileSystemWatcherMethod
    {
        /// <summary>
        /// No method selected.
        /// <para>
        /// This is an error state.
        /// </para>
        /// <para>
        /// Value = 0.
        /// </para>
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Use the Windows specific event based 
        /// FileSystemWatcher mechanism.
        /// <para>
        /// Note that although more efficient and less traffic heavy
        /// this limits both listener and server to being Windows servers.
        /// </para>
        /// <para>
        /// Value = 1.
        /// </para>
        /// </summary>
        FileSystemWatcher =1,

        /// <summary>
        /// Use a periodic polling mechanism to spot changes.
        /// <para>
        /// Less performant, but more polyvalent than using
        /// <c>FileSystemWatcher.</c>
        /// </para>
        /// <para>
        /// Value = 1.
        /// </para>
        /// </summary>
        Polling =2,

        /// <summary>
        /// Hybrid using Watcher, and Polling to compensate for potential errors.
        /// </summary>
        Hybrid=3,
    }
}