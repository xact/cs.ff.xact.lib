﻿//using System;


// ReSharper disable CheckNamespace
namespace XAct.IO.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IDirectoryWatcherService"/>
    /// </summary>
    public class DirectoryWatcherService :IDirectoryWatcherService
    {
        private readonly ITracingService _tracingService;
        private readonly IIOService _ioService;
        private readonly IDirectoryWatcherServiceState _fileSystemDirectoryWatcherState;

        /// <summary>
        /// List of file watchers:
        /// </summary>
        public IEnumerable<IDirectoryWatcher> State { get { return _fileSystemDirectoryWatcherState; } }

/// <summary>
/// Initializes a new instance of the <see cref="DirectoryWatcherService" /> class.
/// </summary>
/// <param name="tracingService">The tracing service.</param>
/// <param name="ioService">The io service.</param>
/// <param name="fileSystemDirectoryWatcherState">The file system directory watcher cache.</param>
        public DirectoryWatcherService(ITracingService tracingService, IIOService ioService,  IDirectoryWatcherServiceState fileSystemDirectoryWatcherState)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            ioService.ValidateIsNotDefault("ioService");

            _tracingService = tracingService;
            _ioService = ioService;
            _fileSystemDirectoryWatcherState = fileSystemDirectoryWatcherState;
        }

        /// <summary>
        /// Disables and clears all registered FileSystemDirectoryWatchers.
        /// </summary>
        /// <returns></returns>
        public void Clear()
        {
            foreach (var x in this._fileSystemDirectoryWatcherState)
            {
                x.Enabled = false;
                x.Dispose();
            }
            _fileSystemDirectoryWatcherState.Clear();
        }


        /// <summary>
        /// Sets up a <see cref="FileSystemDirectoryWatcher"/>
        /// to monitor a directory or file according to the specs
        /// given in <paramref name="watcherInstanceConfiguration"/>
        /// </summary>
        /// <param name="watcherInstanceConfiguration">The watcher instance configuration.</param>
        /// <returns></returns>
        public IDirectoryWatcher  Register(IDirectoryWatcherConfiguration watcherInstanceConfiguration)
        {
            _tracingService.QuickTrace("Setting up a FileSystemDirectoryWatcherInstance to watch '{0}'".FormatStringCurrentCulture(watcherInstanceConfiguration.Path));

            IDirectoryWatcher result;

            switch (watcherInstanceConfiguration.Method)
            {
                case (FileSystemWatcherMethod.Polling):
                    result = new  PollingDirectoryWatcher(_tracingService, _ioService, watcherInstanceConfiguration);
                    break;
                case (FileSystemWatcherMethod.FileSystemWatcher):
                    result = new FileSystemDirectoryWatcher(_tracingService, _ioService, watcherInstanceConfiguration);
                    break;
                case (FileSystemWatcherMethod.Hybrid):
                    result = new HybridDirectoryWatcher(_tracingService, _ioService, watcherInstanceConfiguration);
                    break;
                default:
                    throw new IndexOutOfRangeException("watcherInstanceConfiguration.Method is underfined.");
            }

            lock (this)
            {
                _fileSystemDirectoryWatcherState.Add(result);
            }




            return result;
        }

        /// <summary>
        /// Removes the specified <see cref="IDirectoryWatcher"/> from the <see cref="IDirectoryWatcherServiceState"/>
        /// after Disabling it.
        /// </summary>
        /// <param name="fileSystemDirectoryWatcherInstance">The file system directory watcher instance.</param>
        public void Remove(IDirectoryWatcher fileSystemDirectoryWatcherInstance)
        {
                fileSystemDirectoryWatcherInstance.Enabled = false;
                _fileSystemDirectoryWatcherState.Remove(fileSystemDirectoryWatcherInstance);
        }




    }



}