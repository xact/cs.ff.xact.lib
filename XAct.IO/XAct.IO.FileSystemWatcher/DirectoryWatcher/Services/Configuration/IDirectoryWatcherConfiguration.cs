namespace XAct.IO
{
    using System;
    using System.IO;

    /// <summary>
    /// 
    /// </summary>
    public interface IDirectoryWatcherConfiguration : IHasEnabled, IHasName,IHasTag
    {

        /// <summary>
        /// Gets or sets the method to use to spot changes.
        /// <para>
        /// Default is: <see cref="FileSystemWatcherMethod.FileSystemWatcher"/> 
        /// </para>
        /// </summary>
        /// <value>The method.</value>
        FileSystemWatcherMethod Method { get; set; }

        /// <summary>
        /// Gets or sets the polling interval (applied only if <see cref="Method"/> is set to Polling).
        /// </summary>
        /// <value>The polling interval.</value>
        TimeSpan PollingInterval { get; set; }


        /// <summary>
        /// The folder/directory path to be watched.
        /// </summary>
        string Path { get; set; }

        /// <summary>
        /// The filename filter to be watched for (eg: '*.doc').
        /// </summary>
        string Filter { get; set; }

        /// <summary>
        /// An optional list files to be excluded 
        /// beyond what was <see cref="Filter"/>ed out
        /// (eg: '*.tmp.doc')
        /// </summary>
        string[] Excluded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether 
        /// to include changes within sub directories.
        /// <para>
        /// Default is true.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include sub directories]; otherwise, <c>false</c>.
        /// </value>
        bool IncludeSubDirectories { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to
        /// fire for Directory change events as well.
        /// Most times you will be looking for File events only.
        /// <para>
        /// Default is false.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [fire directory change events as well]; otherwise, <c>false</c>.
        /// </value>
        bool FireDirectoryChangeEventsAsWell { get; set; }

        /// <summary>
        /// Gets or sets the change events to watch for.
        /// (FileName, DirectoryName, Attributes,Size, LastWrite,LastAccess, CreationTime, Security)
        /// <para>
        /// Default is <c>NotifyFilters.LastWrite | NotifyFilters.FileName</c>
        /// </para>
        /// <para>
        /// Note: Ignored when <see cref="Method"/> is Polling.
        /// </para>
        /// </summary>
        /// <value>
        /// The change filters.
        /// </value>
        NotifyFilters ChangeFilters { get; set; }



        /// <summary>
        /// Gets or sets the watcher events to watch for.
        /// (Created, Deleted, Changed, Renamed,All)
        /// <para>
        /// Default is <see cref="System.IO.WatcherChangeTypes.Created"/>
        /// (in order to produce less chatter, and not overflow the 64K buffer. A known issue with FileSystemWatcher).
        /// </para>
        /// </summary>
        /// <value>
        /// The watcher change types.
        /// </value>
        WatcherChangeTypes WatcherChangeTypes { get; set; }

    }
}