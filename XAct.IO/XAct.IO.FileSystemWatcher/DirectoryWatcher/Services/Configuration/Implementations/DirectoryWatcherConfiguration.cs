﻿// ReSharper disable CheckNamespace
namespace XAct.IO.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.IO;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// COnfiguration settings required
    /// by <see cref="IDirectoryWatcherService"/>
    /// to Factory up a <see cref="FileSystemDirectoryWatcher"/>
    /// in order to watch a file drop folder (or other).
    /// </summary>
    /// <remarks>
    /// <para>
    /// This could be taken a bit further by:
    /// http://www.codeproject.com/Articles/58740/FileSystemWatcher-Pure-Chaos-Part-1-of-2
    /// </para>
    /// <para>
    /// But that's a lot of watchers just for change events...
    /// </para>
    /// <para>
    /// Note that the polling mechanism he is using is just to ensure the
    /// FileWatcher doesn't fail. 
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(IDirectoryWatcherConfiguration), BindingLifetimeType.TransientScope, Priority.Low)]
    public class DirectoryWatcherConfiguration: IDirectoryWatcherConfiguration
    {
        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal>8/13/2011: Sky</internal>
        public string Name { get; set; }


        /// <summary>
        /// Gets an optional tag of the object.
        /// <para>Can be used to attach any type of text information to the watcher.</para>
        /// <para>Member defined in<see cref="XAct.IHasTag"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal>8/13/2011: Sky</internal>
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the method to use to spot changes.
        /// <para>
        /// Default is: <see cref="FileSystemWatcherMethod.FileSystemWatcher"/> 
        /// </para>
        /// </summary>
        /// <value>The method.</value>
        public FileSystemWatcherMethod Method { get; set; }


        /// <summary>
        /// Gets or sets the polling interval (applied only if <see cref="Method"/> is set to Polling).
        /// </summary>
        /// <value>The polling interval.</value>
        public TimeSpan PollingInterval { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the
        /// scanning is active or not.
        /// <para>
        /// Default is <c>true</c>.
        /// </para>
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled { get; set; }

        /// <summary>
        /// The folder/directory path to be watched.
        /// </summary>
        /// <value></value>
        public string Path { get; set; }

        /// <summary>
        /// The filename filter to be watched for (eg: '*.doc').
        /// </summary>
        /// <value></value>
        public string Filter { get; set; }

        /// <summary>
        /// An optional list files to be excluded
        /// beyond what was <see cref="Filter"/>ed out
        /// (eg: '*.tmp.doc')
        /// </summary>
        /// <value></value>
        public string[] Excluded { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether
        /// to include changes within sub directories.
        /// <para>
        /// Default is true.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include sub directories]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeSubDirectories { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether to
        /// fire for Directory change events as well.
        /// Most times you will be looking for File events only.
        /// <para>
        /// Default is false.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [fire directory change events as well]; otherwise, <c>false</c>.
        /// </value>
        public bool FireDirectoryChangeEventsAsWell { get; set; }


        /// <summary>
        /// Gets or sets the change events to watch for.
        /// (FileName, DirectoryName, Attributes,Size, LastWrite,LastAccess, CreationTime, Security)
        /// <para>
        /// Default is <c>NotifyFilters.LastWrite | NotifyFilters.FileName</c>
        /// </para>
        /// <para>
        /// Note: Ignored when <see cref="Method"/> is Polling.
        /// </para>
        /// </summary>
        /// <value>
        /// The change filters.
        /// </value>
        public NotifyFilters ChangeFilters { get; set; }



        /// <summary>
        /// Gets or sets the watcher events to watch for.
        /// (Created, Deleted, Changed, Renamed,All)
        /// <para>
        /// Default is <see cref="System.IO.WatcherChangeTypes.Created"/>
        /// (in order to produce less chatter, and not overflow the 64K buffer. A known issue with FileSystemWatcher).
        /// </para>
        /// </summary>
        /// <value>
        /// The watcher change types.
        /// </value>
        public WatcherChangeTypes WatcherChangeTypes { get; set; }




        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryWatcherConfiguration"/> class.
        /// </summary>
        public DirectoryWatcherConfiguration()
        {
            PollingInterval = new TimeSpan(0,0,1,0);


            Name = "Unspecified_{0}_{1}_{2}".FormatStringInvariantCulture(
    System.DateTime.Now.Ticks, XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random.Next(0, 1000),
    XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random.Next(0, 1000));

            //Set defaults
            Method = FileSystemWatcherMethod.FileSystemWatcher;

            WatcherChangeTypes = WatcherChangeTypes.Created;// WatcherChangeTypes.All;

            //Want to know the following attributes:
            ChangeFilters = NotifyFilters.LastWrite | NotifyFilters.FileName;

            //NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.DirectoryName | NotifyFilters.FileName;
            Enabled = true;

            
            FireDirectoryChangeEventsAsWell = false;
            
            IncludeSubDirectories = true;
        }

    }
}