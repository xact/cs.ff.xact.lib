namespace XAct.IO
{
    using System;
    using System.IO;

    /// <summary>
    /// Contract for a instance to watch a directory.
    /// </summary>
    public interface IDirectoryWatcher: IHasEnabled , IHasName, IHasTag, IDisposable
    {

        

        /// <summary>
        /// Occurs when a File is Created.
        /// </summary>
        event ErrorEventHandler Error;

        /// <summary>
        /// Occurs when a File is Created,Changed,Renamed, or Deleted.
        /// <para>
        /// Always raised after <see cref="FileSystemDirectoryWatcher.Created"/>,
        /// <see cref="FileSystemDirectoryWatcher.Changed"/>,
        /// <see cref="FileSystemDirectoryWatcher.Renamed"/>,
        /// <see cref="FileSystemDirectoryWatcher.Deleted"/>.
        /// </para>
        /// </summary>
        event FileSystemEventHandler Event;

        /// <summary>
        /// Occurs when a File is Created.
        /// <para>
        /// In the case of a Move in the same directory (ie, a Rename)
        /// it fires after the <see cref="FileSystemDirectoryWatcher.Deleted"/>.
        /// </para>
        /// <para>
        /// In the case of a Copy, just <see cref="FileSystemDirectoryWatcher.Created"/> is fired.
        /// </para>
        /// </summary>
        event FileSystemEventHandler Created;

        /// <summary>
        /// Occurs when a File is Changed.
        /// </summary>
        event FileSystemEventHandler Changed;

        /// <summary>
        /// Occurs when a File is Renamed.
        /// <para>
        /// Occurs after both <see cref="FileSystemDirectoryWatcher.Deleted"/> and <see cref="FileSystemDirectoryWatcher.Created"/> are Fired.
        /// </para>
        /// </summary>
        event RenamedEventHandler Renamed;

        /// <summary>
        /// Occurs when a File is Deleted.
        /// <para>
        /// In the case of a Move in the same dir (ie a Rename)
        /// it fires before the <see cref="FileSystemDirectoryWatcher.Created"/>
        /// </para>
        /// </summary>
        event FileSystemEventHandler Deleted;

        /// <summary>
        /// Gets or sets the watcher events to watch for.
        /// </summary>
        /// <value>
        /// The watcher change types.
        /// </value>
        WatcherChangeTypes WatcherChangeTypes { get; set; }

        /// <summary>
        /// Gets the events that raise the <see cref="Changed"/> event.
        /// </summary>
        NotifyFilters ChangeFilters { get; }

        /// <summary>
        /// Gets a value indicating whether [include sub directories].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include sub directories]; otherwise, <c>false</c>.
        /// </value>
        bool IncludeSubDirectories { get; }

        /// <summary>
        /// Gets or sets a value indicating whether to
        /// fire for Directory change events as well.
        /// Most times you will be looking for File events only.
        /// <para>
        /// If you do turn this on, you will have to check whether
        /// events come from Files or Dirs as follows:
        /// <code>
        /// <![CDATA[
        ///    FileAttributes attr = File.GetAttributes(e.FullPath);
        ///    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
        ///     {...} 
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [fire directory change events as well]; otherwise, <c>false</c>.
        /// </value>
        bool FireDirectoryChangeEventsAsWell { get; }

        /// <summary>
        /// Gets the path to the folder or file.
        /// </summary>
        string Path { get; }

        /// <summary>
        /// Gets the text filter of files to watch.
        /// </summary>
        string Filter { get; }

        /// <summary>
        /// Gets a list of excluded files.
        /// </summary>
        string[] Excluded { get; }

        ///// <summary>
        ///// Gets or sets a value indicating whether this 
        ///// <see cref="FileSystemDirectoryWatcherInstance"/> is enabled.
        ///// </summary>
        ///// <value>
        /////   <c>true</c> if enabled; otherwise, <c>false</c>.
        ///// </value>
        //bool Enabled { get; set; }

        ///// <summary>
        ///// Handles Created, Changed, Renamed (but not Deleted)
        /////  Events of the FileSystemWatcher control.
        ///// </summary>
        ///// <param name="source">The source of the event.</param>
        ///// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        //void FileSystemWatcherEvent(object source, FileSystemEventArgs e);
    }
}