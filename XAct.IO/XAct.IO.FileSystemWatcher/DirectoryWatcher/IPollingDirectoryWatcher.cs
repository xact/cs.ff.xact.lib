namespace XAct.IO
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface IPollingDirectoryWatcher : IDirectoryWatcher
    {
        /// <summary>
        /// Gets or sets the polling interval.
        /// </summary>
        /// <value>The interval.</value>
        TimeSpan Interval { get; set; }

        /// <summary>
        /// Resets the timer. Only useful for test purposes.
        /// </summary>
        void ResetTimer();

    }
}