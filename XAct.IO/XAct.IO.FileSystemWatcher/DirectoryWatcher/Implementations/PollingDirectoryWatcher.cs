﻿namespace XAct.IO
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Timers;
    using XAct.Diagnostics;
    using XAct.Services;

    //public class FileInfoKeyedCollection :System.Collections.ObjectModel.KeyedCollection<string, FileInfo>
    //{

    //    /// <summary>
    //    /// When implemented in a derived class, extracts the key from the specified element.
    //    /// </summary>
    //    /// <returns>
    //    /// The key for the specified element.
    //    /// </returns>
    //    /// <param name="item">The element from which to extract the key.</param>
    //    protected override string GetKeyForItem(FileInfo item)
    //    {
    //        return item.FullName;
    //    }


    //}

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof (IPollingDirectoryWatcher), BindingLifetimeType.TransientScope, Priority.Low)]
    public class PollingDirectoryWatcher : IPollingDirectoryWatcher
    {



        //private bool _Initialized = false;
        private readonly System.Timers.Timer _timer;
        private readonly ITracingService _tracingService;
        private IIOService _ioService;

        //private FileInfoKeyedCollection _previousChecks = new FileInfoKeyedCollection();
        private IList<string> _previousChecks = new List<string>();

        //private string[] _lastTimeChecked = new string[0];


        //flag to prevent race condition.
        private bool _polling = false;

        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal>8/13/2011: Sky</internal>
        public string Name { get; set; }


        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal>8/13/2011: Sky</internal>
        public string Tag { get; set; }


        /// <summary>
        /// Resets the timer. Only useful for test purposes.
        /// </summary>
        public void ResetTimer()
        {
            _timer.Stop();
            _timer.Interval = _timer.Interval;
            _timer.Start();

        }


        /// <summary>
        /// Gets or sets the polling interval.
        /// </summary>
        /// <value>The interval.</value>
        public TimeSpan Interval
        {
            get { return _interval; }
            set
            {
                _interval = value;

                _timer.Interval = value.TotalMilliseconds;
                _timer.Enabled = false;
                _timer.Enabled = this.Enabled;

            }
        }

        private TimeSpan _interval;

        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// <see cref="FileSystemDirectoryWatcher"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;
                _timer.Enabled = value;
                //Fix:
                if (value)
                {
                    TimerElapsed(this, null);
                }
            }
        }

        private bool _enabled;

        /// <summary>
        /// Occurs when a File is Created.
        /// </summary>
        public event ErrorEventHandler Error;

        /// <summary>
        /// Occurs when a File is Created,Changed,Renamed, or Deleted.
        /// <para>
        /// Always raised after <see cref="FileSystemDirectoryWatcher.Created"/>,
        /// <see cref="FileSystemDirectoryWatcher.Changed"/>,
        /// <see cref="FileSystemDirectoryWatcher.Renamed"/>,
        /// <see cref="FileSystemDirectoryWatcher.Deleted"/>.
        /// </para>
        /// </summary>
        public event FileSystemEventHandler Event;

        /// <summary>
        /// Occurs when a File is Created.
        /// <para>
        /// In the case of a Move in the same directory (ie, a Rename)
        /// it fires after the <see cref="FileSystemDirectoryWatcher.Deleted"/>.
        /// </para>
        /// 	<para>
        /// In the case of a Copy, just <see cref="FileSystemDirectoryWatcher.Created"/> is fired.
        /// </para>
        /// </summary>
        public event FileSystemEventHandler Created;

        /// <summary>
        /// Occurs when a File is Changed.
        /// </summary>
        public event FileSystemEventHandler Changed;

        /// <summary>
        /// Occurs when a File is Renamed.
        /// <para>
        /// Occurs after both <see cref="FileSystemDirectoryWatcher.Deleted"/> and <see cref="FileSystemDirectoryWatcher.Created"/> are Fired.
        /// </para>
        /// </summary>
        public event RenamedEventHandler Renamed;

        /// <summary>
        /// Occurs when a File is Deleted.
        /// <para>
        /// In the case of a Move in the same dir (ie a Rename)
        /// it fires before the <see cref="FileSystemDirectoryWatcher.Created"/>
        /// 	</para>
        /// </summary>
        public event FileSystemEventHandler Deleted;

        /// <summary>
        /// Gets or sets the watcher events to watch for.
        /// </summary>
        /// <value>
        /// The watcher change types.
        /// </value>
        public WatcherChangeTypes WatcherChangeTypes { get; set; }


        /// <summary>
        /// Gets the events that raise the <see cref="IDirectoryWatcher.Changed"/> event.
        /// </summary>
        public NotifyFilters ChangeFilters { get; private set; }


        /// <summary>
        /// Gets a value indicating whether [include sub directories].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include sub directories]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeSubDirectories { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether to
        /// fire for Directory change events as well.
        /// Most times you will be looking for File events only.
        /// <para>
        /// If you do turn this on, you will have to check whether
        /// events come from Files or Dirs as follows:
        /// <code>
        /// <![CDATA[
        ///    FileAttributes attr = File.GetAttributes(e.FullPath);
        ///    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
        ///     {...} 
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [fire directory change events as well]; otherwise, <c>false</c>.
        /// </value>
        public bool FireDirectoryChangeEventsAsWell { get; private set; }


        /// <summary>
        /// Gets the path to the folder or file.
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// Gets the text filter of files to watch.
        /// </summary>
        public string Filter { get; private set; }

        /// <summary>
        /// Gets a list of excluded files.
        /// </summary>
        public string[] Excluded { get; private set; }




        /// <summary>
        /// Initializes a new instance of the <see cref="PollingDirectoryWatcher"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ioService">The io service.</param>
        /// <param name="fileSystemWatcherInstanceConfiguration">The file system watcher instance configuration.</param>
        public PollingDirectoryWatcher(ITracingService tracingService, IIOService ioService,
                                       IDirectoryWatcherConfiguration fileSystemWatcherInstanceConfiguration
            )
        {
            _timer = new System.Timers.Timer();

            ioService.ValidateIsNotDefault("ioService");
            fileSystemWatcherInstanceConfiguration.ValidateIsNotDefault("fileSystemWatcherInstanceConfiguration");

            //Note: it's intentional that I'm not using Path.Exists...
            fileSystemWatcherInstanceConfiguration.Path.ToString().ValidateIsNotNullOrEmpty("Path");

            //Save ref to services needed:
            _tracingService = tracingService;
            _ioService = ioService;

            //Although we are not using Poling to get File directory and compare,
            //there is a known issue with FileWatcher that if the network connection
            //is lost the Filewatcher stops. The way around it is to 
            //create a new FileWatcher with the same settings (which is why we transfered
            //the vars here, where they may be used again to create the new FileWatcher, 
            //in case the config object gets deleted.

            //Start a timer on a rapid clip to check whether 
            //the target dir is accessible.
            //Default interval = 1 minute.
            this.Interval = new TimeSpan(0, 1, 0);

            _timer.Elapsed += TimerElapsed;




            //Transfer settings from config path to local vars,
            //as we are going to let go/discard config settings:

            //Enabled will be called, which if true will fire first event.
            TransferConfigArgsToLocalArgs(fileSystemWatcherInstanceConfiguration);


        }


        /// <summary>
        /// Handles Buffer errors raised by the internal
        /// <see cref="FileSystemWatcher"/>.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.IO.ErrorEventArgs"/> instance containing the event data.</param>
        protected void FileSystemWatcherError(object sender, ErrorEventArgs e)
        {
            if (Error != null)
            {
                Error.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handles the Created event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherGeneralEvent(object sender, FileSystemEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Event != null)
            {
                Event.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handles the Created event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherCreated(object sender, FileSystemEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Created != null)
            {
                Created.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handles the Changed event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherUpdated(object sender, FileSystemEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Changed != null)
            {
                Changed.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handles the Deleted event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherDeleted(object sender, FileSystemEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Deleted != null)
            {
                Deleted.Invoke(this, e);
            }
        }


        /// <summary>
        /// Handles the Renamed event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherRenamed(object sender, RenamedEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Renamed != null)
            {
                Renamed.Invoke(this, e);
            }
        }


        /// <summary>
        /// Transfers the config args received in constructor to local args.
        /// </summary>
        /// <param name="fileSystemWatcherInstanceConfiguration">The file system watcher instance configuration.</param>
        private void TransferConfigArgsToLocalArgs(
            IDirectoryWatcherConfiguration fileSystemWatcherInstanceConfiguration)
        {
            this.Name = fileSystemWatcherInstanceConfiguration.Name;
            this.Tag = fileSystemWatcherInstanceConfiguration.Tag;
            this.Path = fileSystemWatcherInstanceConfiguration.Path;
            this.Excluded = fileSystemWatcherInstanceConfiguration.Excluded;

            //Take the hit one time: convert Exclude List (wildcards) to Regex patterns
            //as windows doesn't have wildcard compare built in, but does have 
            //regex:
            if (this.Excluded != null)
            {
                for (int num = 0; num < this.Excluded.Length; num++)
                {
                    this.Excluded[num] = this.Excluded[num].WildcardPatternToRegexPattern();
                }
            }

            this.WatcherChangeTypes = fileSystemWatcherInstanceConfiguration.WatcherChangeTypes;

            this.ChangeFilters = fileSystemWatcherInstanceConfiguration.ChangeFilters;

            this.Filter = fileSystemWatcherInstanceConfiguration.Filter;

            this.IncludeSubDirectories = fileSystemWatcherInstanceConfiguration.IncludeSubDirectories;

            this.FireDirectoryChangeEventsAsWell =
                fileSystemWatcherInstanceConfiguration.FireDirectoryChangeEventsAsWell;

            //Note how -- once the FileWatcher gets built -- this will set both the Timer and watcher:
            this.Enabled = fileSystemWatcherInstanceConfiguration.Enabled;
        }


        /// <summary>
        /// Event to handle timer used to check whether we can still see target dir.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (this.Enabled == false)
            {
                return;
            }


            if (_polling)
            {
                return;
            }


            _polling = true;

            if (Directory.Exists(this.Path.ToString()))
            {

                //Returns full paths:


                //Filter cannot be null (error)
                //and empty string matches nothing.
                string filter = string.IsNullOrEmpty(this.Filter) ? "*" : this.Filter;

                //Get FileInfo (as it has date info that FilePath does not...or is that fetched over the wire anyway?)

                List<string> newPaths = _ioService.GetDirectoryFileNamesAsync(
                    this.Path, filter,
                    (this.IncludeSubDirectories
                         ? HierarchicalOperationOption.Recursive
                         : HierarchicalOperationOption.TopOnly)).WaitAndGetResult().ToList();





                //Strip out any files excluded with the excluded filter.
                if (this.Excluded != null)
                {
                    List<string> toRemoves = new List<string>();

                    foreach (string newFileFullName in newPaths)
                    {
                        foreach (string excluded in this.Excluded)
                        {
                            if (newFileFullName.CompareWildcard(excluded, false))
                            {
                                toRemoves.Add(newFileFullName);
                            }
                        }
                    }
                    foreach (string toRemove in toRemoves)
                    {
                        newPaths.Remove(toRemove);
                    }
                }

                // Returns items missing from list1 found in list2.
                List<string> noLongerThereSoShouldRaiseRemovedEvents = newPaths.Missing(_previousChecks, fi => fi);


                // Returns items missing from list1 found in list2.
                //Extract *new* files only.
                List<string> newFilesSoShouldRaiseCreatedEvents = _previousChecks.Missing(newPaths, m => m).ToList();

                //Save for next check:
                this._previousChecks = newPaths;



                //Raise events for all *new* files.
                foreach (string newFile in noLongerThereSoShouldRaiseRemovedEvents)
                {
                    FileSystemEventArgs fileSystemEventArgs =
                        new FileSystemEventArgs(WatcherChangeTypes.Deleted, this.Path.ToString(),
                                                System.IO.Path.GetFileName(newFile));

                    this.FileSystemWatcherGeneralEvent(this, fileSystemEventArgs);
                    this.FileSystemWatcherDeleted(this, fileSystemEventArgs);
                }


                //Raise events for all *new* files.
                foreach (string newFile in newFilesSoShouldRaiseCreatedEvents)
                {
                    FileSystemEventArgs fileSystemEventArgs =
                        new FileSystemEventArgs(WatcherChangeTypes.Created, this.Path.ToString(),
                                                System.IO.Path.GetFileName(newFile));

                    this.FileSystemWatcherGeneralEvent(this, fileSystemEventArgs);
                    this.FileSystemWatcherCreated(this, fileSystemEventArgs);
                }
            }
            _polling = false;
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            _timer.Stop();


            _timer.Dispose();

        }

    }


}
