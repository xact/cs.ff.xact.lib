// ReSharper disable CheckNamespace
namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Timers;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IHybridDirectoryWatcher), BindingLifetimeType.TransientScope, Priority.Low)]
    public class HybridDirectoryWatcher : IHybridDirectoryWatcher, IHasInnerItemReadOnly
    {
        private bool _initialized;
// ReSharper disable NotAccessedField.Local
        private readonly ITracingService _tracingService;
// ReSharper restore NotAccessedField.Local
// ReSharper disable NotAccessedField.Local
        private readonly IIOService _ioService;
// ReSharper restore NotAccessedField.Local


        private class CacheInfo
        {
            public FileAttributes Attributes { get; private set; }
            public string Path { get; private set; }

            public CacheInfo(string path, FileAttributes attributes)
            {
                Path = path;
                Attributes = attributes;
            }
        }

        private readonly List<CacheInfo> _cache = new List<CacheInfo>();

        /// <summary>
        /// Timer used to poll whether target directory is still 
        /// accessible. If not, destroys current watcher, and builds
        /// another when directory rebecomes available (this is a solution
        /// to work around a known bug of <see cref="FileSystemWatcher"/>
        /// where if the target dir becomes unaccessible (network failure)
        /// FileSystemWatcher doesn't restart when the folder becomes available.
        /// </summary>
        private readonly System.Timers.Timer _targetStillAccessibleTimer;

        #region Events Raised (Error,Event, Created,Changed,Renamed,Deleted)

        /// <summary>
        /// Occurs when a File is Created.
        /// </summary>
        public event ErrorEventHandler Error;

        /// <summary>
        /// Occurs when a File is Created,Changed,Renamed, or Deleted.
        /// <para>
        /// Always raised after <see cref="Created"/>,
        /// <see cref="Changed"/>,
        /// <see cref="Renamed"/>,
        /// <see cref="Deleted"/>.
        /// </para>
        /// </summary>
        public event FileSystemEventHandler Event;

        /// <summary>
        /// Occurs when a File is Created.
        /// <para>
        /// In the case of a Move in the same directory (ie, a Rename)
        /// it fires after the <see cref="Deleted"/>.
        /// </para>
        /// <para>
        /// In the case of a Copy, just <see cref="Created"/> is fired.
        /// </para>
        /// </summary>
        public event FileSystemEventHandler Created;

        /// <summary>
        /// Occurs when a File is Changed.
        /// </summary>
        public event FileSystemEventHandler Changed;

        /// <summary>
        /// Occurs when a File is Renamed.
        /// <para>
        /// Occurs after both <see cref="Deleted"/> and <see cref="Created"/> are Fired.
        /// </para>
        /// </summary>
        public event RenamedEventHandler Renamed;

        /// <summary>
        /// Occurs when a File is Deleted.
        /// <para>
        /// In the case of a Move in the same dir (ie a Rename)
        /// it fires before the <see cref="Created"/>
        /// </para>
        /// </summary>
        public event FileSystemEventHandler Deleted;
        #endregion



        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public string Name { get; set; }

        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal>8/13/2011: Sky</internal>
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the watcher events to watch for.
        /// </summary>
        /// <value>
        /// The watcher change types.
        /// </value>
        public WatcherChangeTypes WatcherChangeTypes { get; set; }

        /// <summary>
        /// Gets the events that raise the <see cref="Changed"/> event.
        /// </summary>
        public NotifyFilters ChangeFilters { get; private set; }

        /// <summary>
        /// Gets a value indicating whether [include sub directories].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include sub directories]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeSubDirectories { get; private set; }


        /// <summary>
        /// Gets or sets a value indicating whether to
        /// fire for Directory change events as well.
        /// Most times you will be looking for File events only.
        /// <para>
        /// If you do turn this on, you will have to check whether
        /// events come from Files or Dirs as follows:
        /// <code>
        /// <![CDATA[
        ///    FileAttributes attr = File.GetAttributes(e.FullPath);
        ///    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
        ///     {...} 
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [fire directory change events as well]; otherwise, <c>false</c>.
        /// </value>
        public bool FireDirectoryChangeEventsAsWell { get; private set; }

        /// <summary>
        /// Gets the path to the folder or file.
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// Gets the text filter of files to watch.
        /// </summary>
        public string Filter { get; private set; }

        /// <summary>
        /// Gets a list of excluded files.
        /// </summary>
        public string[] Excluded { get; private set; }


        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// <see cref="FileSystemDirectoryWatcher"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;
                if (FileSystemWatcher != null)
                {
                    FileSystemWatcher.EnableRaisingEvents = _enabled;
                }
                _targetStillAccessibleTimer.Enabled = value;
                //Fix:
                if (value)
                {
                    TimerElapsed(this, null);
                }
            }
        }
        private bool _enabled;



        /// <summary>
        /// Gets the wrapped <c>FileSystemWatcher</c>.
        /// </summary>
        /// <value>The inner object.</value>

        T IHasInnerItemReadOnly.GetInnerItem<T>()
        {
            return FileSystemWatcher.ConvertTo<T>();
        }

        /// <summary>
        /// Gets the wrapped <see cref="FileSystemWatcher"/>.
        /// </summary>
        protected FileSystemWatcher FileSystemWatcher { get; private set; }



        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="FileSystemDirectoryWatcher"/> class
        /// in order to watch for changes in a drop folder
        /// (or other similar scenario).
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ioService">The io service.</param>
        /// <param name="fileSystemWatcherInstanceConfiguration">The file system watcher instance configuration.</param>
        public HybridDirectoryWatcher(
            ITracingService tracingService, IIOService ioService,
            IDirectoryWatcherConfiguration fileSystemWatcherInstanceConfiguration)
        {

            ioService.ValidateIsNotDefault("ioService");
            fileSystemWatcherInstanceConfiguration.ValidateIsNotDefault("fileSystemWatcherInstanceConfiguration");

            //Note: it's intentional that I'm avoiding using Path.Exists...
            fileSystemWatcherInstanceConfiguration.Path.ToString().ValidateIsNotNullOrEmpty("Path");

            //Save ref to services needed:
            _tracingService = tracingService;
            _ioService = ioService;

            //Although we are not using Poling to get File directory and compare,
            //there is a known issue with FileWatcher that if the network connection
            //is lost the Filewatcher stops. The way around it is to 
            //create a new FileWatcher with the same settings (which is why we transfered
            //the vars here, where they may be used again to create the new FileWatcher, 
            //in case the config object gets deleted.

            //Start a timer on a rapid clip to check whether 
            //the target dir is accessible.
            _targetStillAccessibleTimer = new System.Timers.Timer(1000);
            _targetStillAccessibleTimer.Elapsed += TimerElapsed;


            //Transfer settings from config path to local vars,
            //as we are going to let go/discard config settings:

            //Enabled will be called, which if true will fire first event.
            TransferConfigArgsToLocalArgs(fileSystemWatcherInstanceConfiguration);

            _initialized = true;

        }









        /// <summary>
        /// Handles Created, Changed, Renamed (but not Deleted)
        ///  Events of the FileSystemWatcher control.
        /// <para>
        /// </para>
        /// </summary>
        /// <internal>
        /// IMPORTANT: This is the main processor of events raised
        /// by the FileSystemWatcher.
        /// </internal>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        private void FileSystemWatcherEvent(object source, FileSystemEventArgs e)
        {



            string fileName = e.Name;
#pragma warning disable 168
            string fileFullPath = e.FullPath;
#pragma warning restore 168

            if (!_initialized)
            {
                //we don't want to raise events the first time.
                _initialized = true;
                //Actually, maybe we do... for faxes dropped while the service was turned off...
                //return;
            }



            if (IsAnExcludableDirectory(fileFullPath))
            {
                return;
            }


            //if ((Excluded!=null)&&(Excluded.Any(i => String.Compare(fileName, i, true) == 0)))
            //{
            //    return;
            //}

            if (Excluded != null)
            {
                try
                {
                    if (Excluded.Any(pattern => fileName.CompareWildcard(pattern,false)))
                    {
                        return;
                    }
                }
                catch (Exception ex)
                {
                    throw new ArgumentException("Exception raised when trying to parse given WildCard", ex);
                }
            }

            //Note:
            //Common file system operations might raise more than one event. 
            //For example, when a file is moved from one directory to another, 
            //several OnChanged and some OnCreated and OnDeleted events might be raised. 
            //Moving a file is a complex operation that consists of multiple simple operations, 
            //therefore raising multiple events. 
            //Likewise, some applications (for example, anti-virus software) might cause additional 
            //file system events that are detected by FileSystemWatcher.

            switch (e.ChangeType)
            {

                case WatcherChangeTypes.Created:
                    if (((int) this.WatcherChangeTypes).BitIsSet(((int) WatcherChangeTypes.Created)))
                    {
                        FileSystemWatcherCreated(source, e);
                        FileSystemWatcherGeneralEvent(source, e);
                    }
                    break;
                case WatcherChangeTypes.Changed:
                    //TO Do if create Event for file is fired then Change Event must be avoided-To Be implemented in Ver 2
                    //TO DO Bypass Multiple Change event - fired after rename To Be implemented in Ver 2
                    if (((int) this.WatcherChangeTypes).BitIsSet(((int) WatcherChangeTypes.Changed)))
                    {
                        FileSystemWatcherUpdated(source, e);
                        FileSystemWatcherGeneralEvent(source, e);
                    }
                    break;
                case WatcherChangeTypes.Deleted:
                    if (((int) this.WatcherChangeTypes).BitIsSet(((int) WatcherChangeTypes.Deleted)))
                    {
                        FileSystemWatcherDeleted(source, e);
                        FileSystemWatcherGeneralEvent(source, e);
                    }
                    break;
            }
        }



















        /// <summary>
        /// Transfers the config args received in constructor to local args.
        /// </summary>
        /// <param name="fileSystemWatcherInstanceConfiguration">The file system watcher instance configuration.</param>
        private void TransferConfigArgsToLocalArgs(
            IDirectoryWatcherConfiguration fileSystemWatcherInstanceConfiguration)
        {
            this.Name = fileSystemWatcherInstanceConfiguration.Name;
            this.Tag = fileSystemWatcherInstanceConfiguration.Tag;

            this.Path = fileSystemWatcherInstanceConfiguration.Path;
            this.Excluded = fileSystemWatcherInstanceConfiguration.Excluded;

            //Take the hit one time: convert Exclude List (wildcards) to Regex patterns
            //as windows doesn't have wildcard compare built in, but does have 
            //regex:
            if (this.Excluded != null)
            {
                for (int num = 0; num < this.Excluded.Length; num++)
                {
                    this.Excluded[num] = this.Excluded[num].WildcardPatternToRegexPattern();
                }
            }

            this.WatcherChangeTypes = fileSystemWatcherInstanceConfiguration.WatcherChangeTypes;

            this.ChangeFilters = fileSystemWatcherInstanceConfiguration.ChangeFilters;

            this.Filter = fileSystemWatcherInstanceConfiguration.Filter;

            this.IncludeSubDirectories = fileSystemWatcherInstanceConfiguration.IncludeSubDirectories;

            this.FireDirectoryChangeEventsAsWell =
                fileSystemWatcherInstanceConfiguration.FireDirectoryChangeEventsAsWell;


            //Note how -- once the FileWatcher gets built -- this will set both the Timer and watcher:
            this.Enabled = fileSystemWatcherInstanceConfiguration.Enabled;
        }


        /// <summary>
        /// Event to handle timer used to check whether we can still see target dir.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            //Check if target path is accessible.
            if (_ioService.DirectoryExistsAsync(this.Path,false).WaitAndGetResult())
            {
                //If it is, we can have a fileSystemWatcher look at it.

                //It already exists:
                if (FileSystemWatcher == null)
                {
                    //Doesn't exist (either first time, or it was inaccessible for a while, and it was deleted at some point).

                    //Have to recreate a new one:
                    CreateFileSystemWatcher();

                    //Now that connection is re-established, we can 
                    //slow timer back down to a slower regular check:
                    _targetStillAccessibleTimer.Interval = 60000;
                }
// ReSharper disable RedundantIfElseBlock
                else
                {
                    //FielSystemWatcher already exists.
                    //Do nothing.
                }
// ReSharper restore RedundantIfElseBlock

                //As FileSystemWatcher exists -- either from before, or newly created --
                //just get out:
                return;
            }

            //Directory not accessible!
            if (FileSystemWatcher == null)
            {
                //But associated  FSW already killed...
                //waiting for the Directory to be accessivle before
                //building again:
                return;
            }

            //Path not available, but it's associated 
            //watcher exists, and not yet killed
            //... so we murder now:

            //Remove Handlers:
            DettachEventHandlers();
            //Kill it:
            FileSystemWatcher.Dispose();
            //Start scanning for folder coming back online:
            _targetStillAccessibleTimer.Interval = 1000;
        }













        /// <summary>
        /// Creates the file system watcher.
        /// <para>
        /// Invoked by Constructor, and then 
        /// only invoked if Directory became inaccessible, 
        /// and had to delete previous FSW.
        /// (this is because once a folder becomes inaccessible, FSW
        /// doesn't recover).
        /// </para>
        /// </summary>
        private void CreateFileSystemWatcher()
        {
            //Create new FileSystemWatcher with values saved earlier
            //via TransferConfigArgsToLocalArgs method:
            FileSystemWatcher = new FileSystemWatcher(this.Path.ToString())
                                    {
                                        IncludeSubdirectories = this.IncludeSubDirectories,
                                        NotifyFilter = this.ChangeFilters,
                                        Filter = this.Filter
                                    };

            //Attach event handlers:
            AttachEventHandlers();

            //Finally, turn on:
            FileSystemWatcher.EnableRaisingEvents = this.Enabled;
        }


        /// <summary>
        /// Attaches the event handlers.
        /// <para>
        /// Invoked by <see cref="CreateFileSystemWatcher"/> only.
        /// </para>
        /// </summary>
        private void AttachEventHandlers()
        {
            FileSystemWatcher.Error += FileSystemWatcherError;

            FileSystemWatcher.Created += FileSystemWatcherEvent;
            FileSystemWatcher.Changed += FileSystemWatcherEvent;
            //Note how all event handlers are going to the same event
            //handler, except the Rename one, as that fires off 
            //message in a different sequence,that needs more 
            //analysis:
            FileSystemWatcher.Renamed += FileSystemWatcherRenamed;
            FileSystemWatcher.Deleted += FileSystemWatcherEvent;
        }

        /// <summary>
        /// <para>
        /// Invoked by <see cref="TimerElapsed"/> if target
        /// path is no longer available, and have to destroy
        /// FileSystemWatcher.
        /// </para>
        /// Detaches event handlers prior to Disposing FSW's whose 
        /// directory have become inaccessible.
        /// </summary>
        private void DettachEventHandlers()
        {
            FileSystemWatcher.Error -= FileSystemWatcherError;

            FileSystemWatcher.Created -= FileSystemWatcherEvent;
            FileSystemWatcher.Changed -= FileSystemWatcherEvent;
            FileSystemWatcher.Renamed -= FileSystemWatcherRenamed;
            FileSystemWatcher.Deleted -= FileSystemWatcherEvent;
        }












        /// <summary>
        /// Determines whether event is for a filename that is excluded, and can be 
        /// ignored.
        /// <para>
        /// Invoked by <see cref="FileSystemWatcherEvent"/>.
        /// </para>
        /// </summary>
        /// <param name="fileFullPath">The file full path.</param>
        /// <returns>
        /// 	<c>true</c> if [is an excludable directory] [the specified file full path]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsAnExcludableDirectory(string fileFullPath)
        {

            if (this.FireDirectoryChangeEventsAsWell)
            {
                //As we are listening to directories,
                //we won't be excluding this path even if it is a directory:
                return false;
            }

            //See if directory still exists on HD (could be deleted by now):
            if (Directory.Exists(fileFullPath))
            {
                //Still on hard drive, so safe to ask for its attributes:

                // get the file attributes for file or directory
                FileAttributes fileAttributes = File.GetAttributes(fileFullPath);

                //Save the fileName + attributes in  a log/cache. We'll need it.
                _cache.Add(new CacheInfo(fileFullPath, fileAttributes));

                //If getting too big, prune the list a bit:
                if (_cache.Count > 400)
                {
                    while (_cache.Count > 200)
                    {
                        _cache.RemoveAt(0);
                    }
                }

                //99.99% sure it's a directory, but check anyways.
                //Check whether the file path is that of a directory.
                //Do that by comparing it's attributes:
                if ((fileAttributes & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    //The Path was for a Directory,
                    //and we currently don't care about directories:
                    return true;
                }
            }
            else
            {

                //File has been deleted or moved. So we can't get attributes
                //from a file that no longer exists on hard-drive.
                //So search in the log/cache to find out what it *was*:

                for (int i = _cache.Count - 1; i > 0; i--)
                {
                    if (_cache[i].Path == fileFullPath)
                    {
                        if ((_cache[i].Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                        {
                            //The Path was for a Directory,
                            //and we currently don't care about directories:
                            return true;
                        }
                    }
                }
            }
            //Wasn't a directory. So can't exclude this event:
            return false;
        }


        /// <summary>
        /// Handles Buffer errors raised by the internal
        /// <see cref="FileSystemWatcher"/>.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.IO.ErrorEventArgs"/> instance containing the event data.</param>
        protected void FileSystemWatcherError(object sender, ErrorEventArgs e)
        {
            if (Error != null)
            {
                Error.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handles the Created event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherGeneralEvent(object sender, FileSystemEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Event != null)
            {
                Event.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handles the Created event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherCreated(object sender, FileSystemEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Created != null)
            {
                Created.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handles the Changed event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherUpdated(object sender, FileSystemEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Changed != null)
            {
                Changed.Invoke(this, e);
            }
        }

        /// <summary>
        /// Handles the Deleted event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        // ReSharper disable UnusedParameter.Local
        protected void FileSystemWatcherDeleted(object sender, FileSystemEventArgs e)
            // ReSharper restore UnusedParameter.Local
        {
            //Raise event to anybody listening:
            if (Deleted != null)
            {
                Deleted.Invoke(this, e);
            }
        }



        /// <summary>
        /// Handles the Renamed event of the <see cref="FileSystemWatcher"/>  control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.IO.RenamedEventArgs"/> instance containing the event data.</param>
        protected void FileSystemWatcherRenamed(object sender, RenamedEventArgs e)
        {

            //Check whether Event is of type Rename.

            if (((int) this.WatcherChangeTypes).BitIsNotSet(((int) WatcherChangeTypes.Renamed)))
            {
                //Was not. So don't raise an event:
                return;
            }


            //Are we talking about a directory that we don't care about?
            string fileFullPath = e.FullPath;
            if (IsAnExcludableDirectory(fileFullPath))
            {
                //Event is for a directory.
                return;
            }


            //Seems it was a raisable event, so raise it:
            if (Renamed != null)
            {
                Renamed.Invoke(this, e);
            }

            //Don't have to as RenamedEventArgs is a specialization of FileSystemEventArgs;
            //new FileSystemEventArgs(e.ChangeType, e.FullPath, e.Name);
            //And best not to, so that received can clas it back to RenamedEventArgs if need be...

            FileSystemWatcherGeneralEvent(sender, e);

        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
                public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


                /// <summary>
                /// Releases unmanaged and - optionally - managed resources.
                /// </summary>
                /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {


            _targetStillAccessibleTimer.Stop();

            FileSystemWatcher.Dispose();
            _targetStillAccessibleTimer.Dispose();
        }


    }
}
