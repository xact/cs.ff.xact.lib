﻿namespace XAct.IO
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a global cache of <see cref="IDropFolderWatcher"/>
    /// instances instantiated by <see cref="IDropFolderService"/>
    /// </summary>
    public interface IDropFolderWatcherServiceCache : IList<IDropFolderWatcher> { }
}