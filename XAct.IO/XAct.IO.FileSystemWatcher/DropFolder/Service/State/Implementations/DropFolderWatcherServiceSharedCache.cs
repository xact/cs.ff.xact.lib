﻿namespace XAct.IO.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IDropFolderWatcherServiceCache"/>
    /// to provide a singleton cache backing <see cref="IDropFolderService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IDropFolderWatcherServiceCache), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class DropFolderWatcherServiceSharedCache : List<IDropFolderWatcher>, IDropFolderWatcherServiceCache
    {

    }
}