// ReSharper disable CheckNamespace
namespace XAct.IO.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="IDropFolderService"/> to create and register
    /// instances of <see cref="IDropFolderWatcher"/>, built according
    /// to specs defined in <see cref="IDropFolderSpecifications"/>
    /// </summary>
    public class DropFolderService : IDropFolderService
    {
        private readonly ITracingService _tracingService;
// ReSharper disable NotAccessedField.Local
        private readonly IEnvironmentService _environmentService;
// ReSharper restore NotAccessedField.Local
        private readonly IIOService _ioService;
        private readonly IDirectoryArchivingService _directoryArchivingService;
        private readonly IDropFolderWatcherServiceCache _dropFolderWatcherCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="DropFolderService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="ioService">The io service.</param>
        /// <param name="directoryArchivingService">The directory archiving service.</param>
        /// <param name="dropFolderWatcherCache">The file drop watcher cache.</param>
        public DropFolderService (ITracingService tracingService, IEnvironmentService environmentService, IIOService ioService, 
            IDirectoryArchivingService directoryArchivingService, 
            IDropFolderWatcherServiceCache dropFolderWatcherCache)
        {
            _tracingService = tracingService;
            _environmentService = environmentService;
            _ioService = ioService;
            _directoryArchivingService = directoryArchivingService;
            _dropFolderWatcherCache = dropFolderWatcherCache;
        }

        /// <summary>
        /// Creates <see cref="IDropFolderWatcher"/> instance according to the 
        /// given <paramref name="dropFolderSpecifications"/>,
        /// registers it in the global <see cref="IDropFolderWatcherServiceCache"/>,
        /// and returns it for the invoker to attach event handlers to.
        /// </summary>
        /// <param name="dropFolderSpecifications">The file drop specifications.</param>
        /// <returns>A instance of <see cref="IDropFolderWatcher"/>.</returns>
        public IDropFolderWatcher Register(IDropFolderSpecifications dropFolderSpecifications)
        {
            //The first part is to make the DropFolderWatcher:
            IDropFolderWatcher dropFolderWatcher = DependencyResolver.Current.GetInstance<IDropFolderWatcher>();
            //And initialize it with the directory to watch:
            dropFolderWatcher.Initialize(dropFolderSpecifications);





            //Once that's done, we have to also register archivers for each one of its Success and Failure folders:
                  
            //Now register the Archivers:
            //--------------------
            //Success Archivers:
            IDirectoryArchiverConfiguration directoryArchiverConfiguration;

            //Have to create a new one everytime, as the name needs to be different:
            directoryArchiverConfiguration = DependencyResolver.Current.GetInstance<IDirectoryArchiverConfiguration>();
            //Get the config, create a new Config:
            dropFolderSpecifications.SuccessStrategy.Map(directoryArchiverConfiguration);
            //And register that archiver:
            _directoryArchivingService.Register(directoryArchiverConfiguration);
            //--------------------
            //Do same for Error Strategy:
            directoryArchiverConfiguration
                = DependencyResolver.Current.GetInstance<IDirectoryArchiverConfiguration>();

            //Have to create a new one everytime, as the name needs to be different:
            directoryArchiverConfiguration = DependencyResolver.Current.GetInstance<IDirectoryArchiverConfiguration>();
            //Get the config, create a new Config:
            dropFolderSpecifications.ErrorStrategy.Map(directoryArchiverConfiguration);


            //And register that archiver:
            _directoryArchivingService.Register(directoryArchiverConfiguration);
            //--------------------
            foreach (IDropFolderDirectorySpecification i in dropFolderSpecifications.Directories)
            {
            //Have to create a new one everytime, as the name needs to be different:
            directoryArchiverConfiguration = DependencyResolver.Current.GetInstance<IDirectoryArchiverConfiguration>();
                i.SuccessStrategy.Map(directoryArchiverConfiguration);
                //And register that archiver:
                _directoryArchivingService.Register(directoryArchiverConfiguration);


            //Have to create a new one everytime, as the name needs to be different:
            directoryArchiverConfiguration = DependencyResolver.Current.GetInstance<IDirectoryArchiverConfiguration>();
                i.ErrorStrategy.Map(directoryArchiverConfiguration);
                //And register that archiver:
                _directoryArchivingService.Register(directoryArchiverConfiguration);

            }





            return dropFolderWatcher;

        }

        /// <summary>
        /// Removes the given <paramref name="dropFolderWatcher"/> from the internal
        /// <see cref="IDropFolderWatcherServiceCache"/>.
        /// </summary>
        /// <param name="dropFolderWatcher">The file drop watcher.</param>
        public void Unregister(IDropFolderWatcher dropFolderWatcher)
        {
            _dropFolderWatcherCache.Remove(dropFolderWatcher);
        }

        /// <summary>
        /// Don't use.
        /// </summary>
        public void Clear()
        {

            _dropFolderWatcherCache.Clear();
        }


        /// <summary>
        /// Validates the specified file drop specifications (checking whether directories exist, are accessible, etc.)
        /// </summary>
        /// <param name="dropFolderSpecifications">The file drop specifications.</param>
        /// <param name="errorMessages">The error messages.</param>
        /// <returns></returns>
        public bool Validate(IDropFolderSpecifications dropFolderSpecifications, out string[] errorMessages)
        {

            bool result = true;
// ReSharper disable JoinDeclarationAndInitializer
            bool check;
// ReSharper restore JoinDeclarationAndInitializer
            List<string> validationErrorMessages = new List<string>();

            //Ensure Overall strategies work first:
            check = EnsureOverallSuccessStrategyFolderExists(dropFolderSpecifications, ref validationErrorMessages);
            result &= check;

            check = EnsureOverallErrorStrategyFolderExists(dropFolderSpecifications, ref validationErrorMessages);
            result &= check;

            //Ensure drop folder strategies work as well:
            check = EnsureDropFoldersExist(dropFolderSpecifications, ref validationErrorMessages);
            result &= check;

            errorMessages = validationErrorMessages.ToArray();

            return result;
        }



        #region Private Validation 

        private bool EnsureOverallSuccessStrategyFolderExists(IDropFolderSpecifications dropFolderSpecifications, ref List<string> validationErrorMessages)
        {
            const string title = "Base SuccessStrategy";
            IDropFolderResultStrategySpecification folderStrategy = dropFolderSpecifications.SuccessStrategy;
            return EnsureStrategyFolderIsCoherent(title, folderStrategy, ref validationErrorMessages);
        }

        private bool EnsureOverallErrorStrategyFolderExists(IDropFolderSpecifications dropFolderSpecifications, ref List<string> validationErrorMessages)
        {
            const string title = "Base ErrorStrategy";
            IDropFolderResultStrategySpecification folderStrategy = dropFolderSpecifications.ErrorStrategy;
            return EnsureStrategyFolderIsCoherent(title, folderStrategy, ref validationErrorMessages);
        }


        private bool EnsureDropFoldersExist(IDropFolderSpecifications dropFolderSpecifications, ref List<string> validationErrorMessages)
        {
            bool result = true;
            foreach (IDropFolderDirectorySpecification dropFolderDirectorySpecification in dropFolderSpecifications.Directories)
            {
                result &= EnsureDropFolderExist(dropFolderDirectorySpecification,ref validationErrorMessages);
            }
            return result;
        }

        private bool EnsureDropFolderExist(IDropFolderDirectorySpecification dropFolderDirectorySpecification, ref List<string> validationErrorMessages)
        {
            bool result = true;
// ReSharper disable JoinDeclarationAndInitializer
            bool check;
// ReSharper restore JoinDeclarationAndInitializer
            string title = "'{0}' DropFolder".FormatStringInvariantCulture(dropFolderDirectorySpecification.Name);

            check = dropFolderDirectorySpecification.Enabled;
            if (!check)
            {
                //Not enabled, but still coherent:
                return true;
            }

            check = EnsureDirectoryIsAccessible(title, dropFolderDirectorySpecification.Directory, true,
                                                ref validationErrorMessages);
            result &= check;
            if (check)
            {
                //If core folder is failing, we don't analyse the rest?
                //Think we should anyway, to get better error logging:
                //continue;
            }


            check = EnsureStrategyFolderIsCoherent(title + " SuccessStrategy",
                                                   dropFolderDirectorySpecification.SuccessStrategy,
                                                   ref validationErrorMessages);
            result &= check;

            check = EnsureStrategyFolderIsCoherent(title + " ErrorStrategy", dropFolderDirectorySpecification.ErrorStrategy,
                                                    ref validationErrorMessages);
            result &= check;

            return result;
        }


        private bool EnsureStrategyFolderIsCoherent(string title, IDropFolderResultStrategySpecification folderStrategy, ref List<string> validationErrorMessages)
        {

            bool result = true;
// ReSharper disable JoinDeclarationAndInitializer
            bool check;
// ReSharper restore JoinDeclarationAndInitializer
            if (!folderStrategy.Enabled)
            {
                _tracingService.Trace(TraceLevel.Verbose,
                                      "{0} is Disabled. Directory will not be verfied for existence.", title);

                //Not enabled, but still coherent:
                return true;
            }

            check = EnsureDirectoryIsAccessible(title, folderStrategy.Directory, true, ref validationErrorMessages);
            result &= check;
            if (!check)
            {
                //Strategy directory -- if enabled, has to exist:
                //If it does not exist, do nothing at this stage...errors will be written to log.
            }

            //Ok to check even if directory is not there:
            check = EnsureDirectoryIsAccessible(title, folderStrategy.ArchiveMoveTo, false, ref validationErrorMessages);
            result &= check;
            if (!check)
            {
                //It does not have to exist, as at run time, may drop down to parent one...
                //If it does not exist, do nothing at this stage...errors will be written to log.
            }

            return result;
        }


        private bool EnsureDirectoryIsAccessible(string directoryTitle, string directoryInfo, bool mustExist, ref List<string> validationErrorMessages)
        {
// ReSharper disable JoinDeclarationAndInitializer
            bool result;
// ReSharper restore JoinDeclarationAndInitializer

            if ((directoryInfo == null)||(directoryInfo.IsNullOrEmpty()))
            {
                if (mustExist)
                {
                    string msg =
                        "{0} Directory is not defined, but is required.".
                            FormatStringInvariantCulture(directoryTitle,
                                                         directoryInfo);

                    _tracingService.Trace(TraceLevel.Warning, msg);

                    validationErrorMessages.Add(msg);
                }
                return false;
            }

            //Starting out hopeful:
            result = true;

            DirectoryAccessibilityCheckResults results =
                _ioService.DirectoryCheckAccessibilityAsync(directoryInfo, true).WaitAndGetResult();

            if (!results.Exists)
            {

                string msg =
                    "{0} Directory ('{1}') does not exist, and could not be created on the fly.".
                        FormatStringInvariantCulture(directoryTitle,
                                                     directoryInfo);


                validationErrorMessages.Add(msg);
                //Not 100% sure an exception is returned in all cases:
                if (results.Exception == null) { results.Exception = new Exception(); }
                _tracingService.TraceException(TraceLevel.Verbose, results.Exception, msg);
                
                //If directory is not there, not much more we can meaningfully test:
                return false;
            }

            //Directory does exist, so check access:

            if (results.CanRead != ResultStatus.Success)
            {
                result = false;

                string msg = "'{0}' Directory ('{1}') exists -- but was unable to read from it.".FormatStringInvariantCulture(
                    directoryTitle,
                    directoryInfo);
                    

                validationErrorMessages.Add(msg);
                //Not 100% sure an exception is returned in all cases:
                if (results.Exception == null) { results.Exception = new Exception(); }
                _tracingService.TraceException(TraceLevel.Verbose, results.Exception, msg);

            }



            if (results.CanWrite != ResultStatus.Success)
            {
                result = false;

                string msg = "'{0}' Directory ('{1}') exists -- but was unable to write to it.".FormatStringInvariantCulture(
                    directoryTitle,
                    directoryInfo);
                    

                validationErrorMessages.Add(msg);
                //Not 100% sure an exception is returned in all cases:
                if (results.Exception == null) { results.Exception = new Exception(); }
                _tracingService.TraceException(TraceLevel.Verbose, results.Exception, msg);

            }

            if (results.CanWrite != ResultStatus.Success)
            {
                result = false;

                string msg = "'{0}' Directory ('{1}') exists -- but was unable to delete/move from it.".FormatStringInvariantCulture(
                    directoryTitle,
                    directoryInfo);
                    

                validationErrorMessages.Add(msg);
                //Not 100% sure an exception is returned in all cases:
                if (results.Exception == null) { results.Exception = new Exception(); }
                _tracingService.TraceException(TraceLevel.Verbose, results.Exception, msg);

            }

            return result;
        }

        #endregion
    }
}