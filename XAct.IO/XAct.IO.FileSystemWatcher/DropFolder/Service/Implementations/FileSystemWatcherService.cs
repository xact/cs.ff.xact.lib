﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using XAct.Diagnostics;
//using XAct.Services;

//namespace XAct.IO.Implementations
//{



//    /// <summary>
//    /// 
//    /// </summary>
//    /// <internal>
//    /// Protect upper layers from vendor rot by using a service to wrap the vendor service.
//    /// </internal>
//    [DefaultBindingImplementation(typeof(IFileSystemWatcherService))]
//    public class FileSystemWatcherService : IFileSystemWatcherService
//    {
//        private readonly ITracingService _tracingService;
//        private VENDOR.FileSystemWatcherService _fileSystemWatcherService;


//        /// <summary>
//        /// Initializes a new instance of the <see cref="FileSystemWatcherService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        /// <param name="fileSystemWatcherService">The file system watcher service.</param>
//        public FileSystemWatcherService(ITracingService tracingService,
//                                        VENDOR.FileSystemWatcherService fileSystemWatcherService)
//        {
//            _tracingService = tracingService;
//            _fileSystemWatcherService = fileSystemWatcherService;
//        }

//        // Summary:
//        //     Sets up a XAct.IO.FileSystemDirectoryWatcherInstance to monitor a directory
//        //     or file according to the specs given in watcherInstanceConfiguration
//        //
//        // Parameters:
//        //   watcherInstanceConfiguration:
//        //     The watcher instance configuration.
//        public IFileSystemDirectoryWatcherInstance Watch(FileWatchConfiguration fileWatchConfiguration)
//        {
//            VENDOR.IFileSystemDirectoryWatcherConfiguration fileSystemDirectoryWatcherInstanceConfiguration =
//                fileWatchConfiguration.MapTo();


//            VENDOR.IFileSystemDirectoryWatcher fileSystemDirectoryWatcherInstance =
//              _fileSystemWatcherService.Register(fileSystemDirectoryWatcherInstanceConfiguration);

//            //wrap the vendor object before returning it in order to protect upper layers:
//            FileSystemDirectoryWatcherInstance result = new FileSystemDirectoryWatcherInstance(fileSystemDirectoryWatcherInstance);

//            return result;
//        }

//    }
//}


