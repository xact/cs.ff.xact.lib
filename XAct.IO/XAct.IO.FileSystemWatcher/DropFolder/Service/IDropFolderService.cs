﻿// ReSharper disable CheckNamespace
namespace XAct.IO
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for a service to create, register, and manage a 
    /// <see cref="IDropFolderWatcher"/> instance.
    /// </summary>
    public interface IDropFolderService : IHasXActLibService
    {
        //bool Validate(IDropFolderSpecifications dropFolderSpecifications);

        /// <summary>
        /// Creates <see cref="IDropFolderWatcher"/> instance according to the 
        /// given <paramref name="dropFolderSpecifications"/>,
        /// registers it in the global <see cref="IDropFolderWatcherServiceCache"/>,
        /// and returns it for the invoke to attach event handlers to.
        /// </summary>
        /// <param name="dropFolderSpecifications">The file drop specifications.</param>
        /// <returns>A instance of <see cref="IDropFolderWatcher"/>.</returns>
        IDropFolderWatcher Register(IDropFolderSpecifications dropFolderSpecifications);


        /// <summary>
        /// Removes the given <paramref name="dropFolderWatcher"/> from the internal
        /// <see cref="IDropFolderWatcherServiceCache"/>.
        /// </summary>
        /// <param name="dropFolderWatcher">The file drop watcher.</param>
        void Unregister(IDropFolderWatcher dropFolderWatcher);



        /// <summary>
        /// Unregister all.
        /// </summary>
        void Clear();

        /// <summary>
        /// Validates the specified file drop specifications.
        /// </summary>
        /// <param name="dropFolderSpecifications">The file drop specifications.</param>
        /// <param name="errorMessages">The error messages.</param>
        /// <returns></returns>
        bool Validate(IDropFolderSpecifications dropFolderSpecifications, out string[] errorMessages);

    }
}
