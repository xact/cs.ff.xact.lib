namespace XAct
{
    using XAct.IO;

    /// <summary>
    /// Extension Methods
    /// </summary>
    public static class XTensions
    {
        /// <summary>
        /// Maps the config settings to the target object.
        /// </summary>
        /// <param name="dropFolderSpecifications">The drop folder specifications.</param>
        /// <param name="directoryArchiverConfiguration">The directory archiver configuration.</param>
        public static void Map(this IDropFolderResultStrategySpecification dropFolderSpecifications, IDirectoryArchiverConfiguration directoryArchiverConfiguration)
        {
            directoryArchiverConfiguration.Initialize( 
                dropFolderSpecifications.Enabled,
                dropFolderSpecifications.ArchiveCheckInterval,
                dropFolderSpecifications.Directory,
                dropFolderSpecifications.ArchiveDelay,
                dropFolderSpecifications.ArchiveAction,
                dropFolderSpecifications.ArchiveMoveTo,
                dropFolderSpecifications.ArchiveFileDateType);
        }
    }
}