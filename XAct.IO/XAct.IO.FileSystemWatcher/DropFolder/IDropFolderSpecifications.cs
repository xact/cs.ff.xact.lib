// ReSharper disable CheckNamespace

namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for the specification of a a <see cref="DropFolderWatcher"/> instance.
    /// <para>
    /// Used to pass to <see cref="IDropFolderService"/> in order for it
    /// to create, register, and return a <see cref="IDropFolderWatcher"/>
    /// </para>
    /// </summary>
    public interface IDropFolderSpecifications
    {
        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>
        /// The default state is enabled.
        /// </para>
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        bool Enabled { get; }



        /// <summary>
        /// Gets or sets the delay between receiving the event, and raising
        /// it for custom handling (can be used to handle closing down of
        /// incoming threads (eg: from scanner) that may not have finished writing).
        /// <para>
        /// Default value is <c>100ms</c>.
        /// </para>
        /// </summary>
        /// <value>
        /// The delay.
        /// </value>
        TimeSpan EventDelay { get; set; }



        /// <summary>
        /// Gets or sets the amount of time to wait before 
        /// giving up getting exclusive access to the file.
        /// </summary>
        /// <value>
        /// The event timeout.
        /// </value>
        TimeSpan EventTimeout { get; set; }



        /// <summary>
        /// Gets or sets the collection of drop folder directories to watch.
        /// </summary>
        /// <value>The directories.</value>
        IDropFolderDirectorySpecification[] Directories { get; set; }

        /// <summary>
        /// Gets or sets the (optional) folder to archive documents to.
        /// <para>
        /// The path can be local or remote.
        /// <code>
        /// "\\serverXYZ\archive\"
        /// </code>
        /// </para>
        /// <para>
        /// The default state is disabled, as there is no default directory.
        /// </para>
        /// </summary>
        /// <value>The archive folder.</value>
        IDropFolderResultStrategySpecification SuccessStrategy { get; }

        /// <summary>
        /// Gets or sets the (optional) error folder to move documents (when possible)
        /// to when there was an error.
        /// <para>
        /// The path can be local or remote.
        /// <code>
        /// "\\serverXYZ\sharedFolder\error\"
        /// </code>
        /// </para>
        /// <para>
        /// The default state is disabled, as there is no default directory.
        /// </para>
        /// </summary>
        /// <value>The error folder.</value>
        IDropFolderResultStrategySpecification ErrorStrategy { get; }
    }
}