namespace XAct.IO
{
    using System;

    /// <summary>
    /// Contract for DropFolderWatcher instances created by the
    /// <see cref="IDropFolderService"/> in order to watch
    /// the files specified using a <see cref="DropFolderSpecifications"/>
    /// </summary>
    public interface IDropFolderWatcher
    {

        /// <summary>
        /// Occurs when [new file].
        /// </summary>
        event EventHandler<DropFolderEventArgs> NewFile;

        /// <summary>
        /// Gets the specs defined when <see cref="Initialize"/> 
        /// was invoked.
        /// </summary>
        /// <value>
        /// The specs.
        /// </value>
        IDropFolderSpecifications Specs { get; }
    

    /// <summary>
        /// Initializes the <see cref="IDropFolderWatcher"/>
        /// using the specs defined in the given <see cref="IDropFolderSpecifications"/>
        /// </summary>
        /// <param name="dropFolderSpecifications">The file drop specifications.</param>
        void Initialize(IDropFolderSpecifications dropFolderSpecifications);


    }
}