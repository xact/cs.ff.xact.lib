﻿/// <summary>
/// The action to take when the Archiving Strategy's delay is up.
/// </summary>
public enum FileDropArchivingAction
{
    /// <summary>
    /// The action is undefined. This is an error state.
    /// <para>
    /// Value=0
    /// </para>
    /// </summary>
    Undefined = 0,


    /// <summary>
    /// Files are left to accumulate in the folder.
    /// <para>
    /// Value=2
    /// </para>
    /// </summary>
    NoAction=2,

    /// <summary>
    /// Files are archived to another directory after a delay.
    /// <para>
    /// Value=3
    /// </para>
    /// </summary>
    Move=3,

    /// <summary>
    /// Files are deleted from the folder after a delay.
    /// <para>
    /// Value=4
    /// </para>
    /// </summary>
    Delete=4,

}
