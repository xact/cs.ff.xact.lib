﻿
namespace XAct.IO
{
    /// <summary>
    /// A flag indicating whether to implement an archive or error strategy or not.
    /// </summary>
    public enum DropFolderStrategyState
    {
        /// <summary>
        /// No value was set (this is an error state).
        /// <value>Value=0</value>
        /// </summary>
        Unspecified=0,
        /// <summary>
        /// Inherit the state from a lower configuration.
        /// <value>Value=1</value>
        /// </summary>
        Inherit = 1,
        /// <summary>
        /// Enabled
        /// </summary>
        Enabled = 2,
        /// <summary>
        /// Disabled.
        /// <value>Value=1</value>
        /// </summary>
        Disabled = 3,

    //    /// <summary>
    //    /// HACK for parsing Enums.
    //    /// </summary>
    //    True = 2,
    //    /// <summary>
    //    /// HACK for parsing Enums.
    //    /// </summary>
    //    False = 3,
    //    /// <summary>
    //    /// HACK for parsing Enums.
    //    /// </summary>
    //    @true = 2,
    ///// <summary>
    ///// HACK for parsing Enums.
    ///// </summary>
    //@false=3

    }
}
