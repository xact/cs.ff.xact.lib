﻿// ReSharper disable CheckNamespace
namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using XAct.Environment;

    /// <summary>
    /// Element created by a Configuration Service of some kind,
    /// as a element within the <see cref="DropFolderSpecifications.Directories"/>
    /// collection.
    /// </summary>
    /// <remarks>
    /// <para>
    /// If using a config file to specify settings, the values
    /// will come from parsing something similar to the following:
    /// <![CDATA[
    /// <dropFolderSettings>
    ///   <dropFolders>
    ///     <dropFolder Directory="...">
    ///       <successStrategy enabled="Enabled" delay="0,0,1" directory="..."/>
    ///       <errorStrategy enabled="Enabled" directory="..."/>
    ///     </dropFolder>
    ///     <dropFolder Directory="..."/>
    ///   </dropFolders>
    ///   <successStrategy enabled="Enabled" delay="0,0,1" directory="..."/>
    ///   <errorStrategy enabled="Enabled" directory="..."/>
    /// </dropFolderSettings>
    /// ]]>
    /// </para>
    /// </remarks>
    public class DropFolderDirectorySpecification : IDropFolderDirectorySpecification
    {
        /// <summary>
        /// Gets the unique name of the specification within it's collection.
        /// <para>Member defined in<see cref="T:XAct.IHasName"/></para>
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>
        /// Default is <c>true</c>
        /// </para>
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets the custom Tag of the object.
        /// <para>Member defined in<see cref="T:XAct.IHasTag"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal>8/13/2011: Sky</internal>
        public string Tag { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether to include events from sub directories.
        /// <para>
        /// CAUTION:
        /// Default value is <c>False</c> so that if Archive and Error folders are 
        /// nested (per Directory specification), there isn't a recursive loop.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include sub directories]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeSubDirectories { get; set; }

        /// <summary>
        /// Gets or sets the path of the folder to watch.
        /// <para>
        /// The path can be local or remote.
        /// <code>
        /// "\\serverXYZ\sharedFolder\"
        /// </code>
        /// </para>
        /// </summary>
        /// <value>The path to the folder to watch.</value>
        public string Directory { get; set; }

        /// <summary>
        /// Gets or sets settings as to where to archive dropped files,
        /// as well when to remove them.
        /// </summary>
        /// <remarks>
        /// <remarks>
        /// <para>
        /// If using a config file to specify settings, the values
        /// will come from parsing something similar to the following:
        /// <![CDATA[
        /// <dropFolderSettings>
        ///   <dropFolders>
        ///     <dropFolder Directory="...">
        ///       <successStrategy Enabled="Enabled" delay="0,0,1" directory="..."/>
        ///       <errorStrategy Enabled="Enabled" directory="..."/>
        ///     </dropFolder>
        ///     <dropFolder Directory="..."/>
        ///   </dropFolders>
        ///   <successStrategy enabled="true" delay="0,0,1" directory="..."/>
        ///   <errorStrategy enabled="true" directory="..."/>
        /// </dropFolderSettings>
        /// ]]>
        /// </para>
        /// </remarks>
        /// </remarks>
        /// <value>The archive specs.</value>
        public IDropFolderResultStrategySpecification SuccessStrategy { get; set; }

        /// <summary>
        /// Gets or sets the (optional) error folder to move documents (when possible)
        /// to when there was an error.
        /// <para>
        /// The path can be local or remote.
        /// <code>
        /// "\\serverXYZ\sharedFolder\error\"
        /// </code>
        /// </para>
        /// </summary>
        /// <value>The error folder.</value>
        public IDropFolderResultStrategySpecification ErrorStrategy { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="DropFolderDirectorySpecification"/> class.
        /// </summary>
        public DropFolderDirectorySpecification()
        {
            Enabled = true;

            Name = "Unspecified_{0}_{1}_{2}".FormatStringInvariantCulture(
    System.DateTime.Now.Ticks, XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random.Next(0, 1000),
    XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random.Next(0, 1000));


            IncludeSubDirectories = false;
            
            SuccessStrategy = new DropFolderResultStrategySpecification();
            ErrorStrategy = new DropFolderResultStrategySpecification();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "'{0}' [Name:{1}] [Path:{2}]".FormatStringInvariantCulture(Name, Enabled, Directory);
        }
    }
}
