﻿// ReSharper disable CheckNamespace
namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IDropFolderWatcher"/>
    /// contract for DropFolderWatcher instances created by the
    /// <see cref="IDropFolderService"/>.
    /// </summary>
    [DefaultBindingImplementation(typeof(IDropFolderWatcher), BindingLifetimeType.TransientScope, Priority.Low)]
    public class DropFolderWatcher : IDropFolderWatcher
    {
        private readonly ITracingService _tracingService;
        private readonly IDateTimeService _dateTimeService;

        private readonly IEnvironmentService _environmentService;
        private readonly IIOService _ioService;
        private readonly IDirectoryWatcherService _fileSystemWatcherService;

        //Used to validate itself:


        /// <summary>
        /// Occurs when a new file has been created in the drop folder.
        /// </summary>
        public event EventHandler<DropFolderEventArgs> NewFile;

        private readonly IDictionary<IDirectoryWatcher, IDropFolderDirectorySpecification> _fileListenersCache =
            new Dictionary<IDirectoryWatcher, IDropFolderDirectorySpecification>();


        /// <summary>
        /// The specifications that define the collections of folders this 
        /// watcher is responsible for watching, the archiving strategy,
        /// and error strategy.
        /// <para>
        /// Set using the <see cref="Initialize"/> method.
        /// </para>
        /// </summary>
        public IDropFolderSpecifications Specs
        {
            get { return _dropFolderSpecifications; }
        }

        private IDropFolderSpecifications _dropFolderSpecifications;

        /// <summary>
        /// Initializes a new instance of the <see cref="DropFolderWatcher"/> class.
        /// </summary>
        public DropFolderWatcher(ITracingService tracingService, 
            IDateTimeService dateTimeService,
            IEnvironmentService environmentService, 
            IIOService ioService,
            IDirectoryWatcherService fileSystemWatcherService)
        {
            _tracingService = tracingService;
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
            _ioService = ioService;
            _fileSystemWatcherService = fileSystemWatcherService;
        }


        /// <summary>
        /// Initializes the <see cref="IDropFolderWatcher"/>
        /// using the specs defined in the given <see cref="IDropFolderSpecifications"/>
        /// </summary>
        /// <param name="dropFolderSpecifications">The file drop specifications.</param>
        public void Initialize(IDropFolderSpecifications dropFolderSpecifications)
        {
            //Save the specs:
            _dropFolderSpecifications = dropFolderSpecifications;

            CreateAndRegisterWatchersForEachDropFolderSpec();

            //Finally, now that config and setup has been completed, we can 
            //move on to scanning for 'stale' records 
            //that might have be left there when the service was turned off and on again:
            foreach (IDirectoryWatcher fileSystemDirectoryWatcher in _fileListenersCache.Keys)
            {
                //Each file found in the target dirs wil call FileSystemWatcher_FileCreated:

                ScanForPreExistingFiles(fileSystemDirectoryWatcher);
            }
        }



        private void CreateAndRegisterWatchersForEachDropFolderSpec()
        {


            foreach (
                DropFolderDirectorySpecification dropFolderDirectorySpecification in _dropFolderSpecifications.Directories)
            {
                try
                {
                    IDirectoryWatcher fileSystemDirectoryWatcherInstance =
                        CreateSingleFileSystemDirectoryWatcherInstance(dropFolderDirectorySpecification);

                    //But it's not enough to keep handl on just object -- we need to relate it back to the original 
                    //specs, so that event handlers can use the specs:
                    _fileListenersCache[fileSystemDirectoryWatcherInstance] = dropFolderDirectorySpecification;


//                    //Ensure that each one has a Success and Error Directory. 
//                    //If it doesn't get information from 
//                    IDirectoryInfo directoryInfo = dropFolderDirectorySpecification.SuccessStrategy.Directory;

//                    if ((directoryInfo == null) || (string.IsNullOrEmpty(directoryInfo.FullName)))
//                    {
//                        //If no per-listener directory, 
//                        //fall back to overall directory
//                        dropFolderDirectorySpecification.SuccessStrategy.Directory
//// ReSharper disable RedundantAssignment
//                            = directoryInfo
//                            = _dropFolderSpecifications.SuccessStrategy.Directory;
//// ReSharper restore RedundantAssignment
//                    }




                }
                catch (Exception e)
                {
                    _tracingService.TraceException(TraceLevel.Warning, e, "Failed creating Watcher for '{0}' (Path:{1})",
                                                   dropFolderDirectorySpecification.Name,
                                                   dropFolderDirectorySpecification.Directory);

                }
            }
        }

        private IDirectoryWatcher CreateSingleFileSystemDirectoryWatcherInstance(
            DropFolderDirectorySpecification dropFolderDirectorySpecification)
        {

            IDirectoryWatcher fileSystemDirectoryWatcherInstance =
                CreateFileSystemWatcher(dropFolderDirectorySpecification);


            //Once we have a fileSystemDirectoryWatcher, we can attach an event handler to it:

            fileSystemDirectoryWatcherInstance.Created += FileSystemWatcherFileCreated;

            return fileSystemDirectoryWatcherInstance;

        }





        private void ScanForPreExistingFiles(IDirectoryWatcher fileSystemDirectoryWatcherInstance)
        {
            if (_ioService.DirectoryExistsAsync(fileSystemDirectoryWatcherInstance.Path,false).WaitAndGetResult())
            {
                string filter = !string.IsNullOrEmpty(fileSystemDirectoryWatcherInstance.Filter)
                                    ? fileSystemDirectoryWatcherInstance.Filter
                                    : "*";

                HierarchicalOperationOption searchOption = fileSystemDirectoryWatcherInstance.IncludeSubDirectories
                                                ? HierarchicalOperationOption.Recursive
                                                : HierarchicalOperationOption.TopOnly;

                foreach (string fileFullName in _ioService.GetDirectoryFileNamesAsync(fileSystemDirectoryWatcherInstance.Path, filter, searchOption).WaitAndGetResult())
                {
                    bool excluded = false;
                    string[] excludedFilters = fileSystemDirectoryWatcherInstance.Excluded;

                    if (excludedFilters != null)
                    {
                        if (excludedFilters.Any(excludePattern => Path.GetFileName(fileFullName).CompareWildcard(excludePattern, false)))
                        {
                            excluded = true;
                        }
                    }

                    if (!excluded)
                    {
                        FileSystemWatcherFileCreated(fileSystemDirectoryWatcherInstance,
// ReSharper disable AssignNullToNotNullAttribute
                                       new FileSystemEventArgs(WatcherChangeTypes.Created, Path.GetDirectoryName(fileFullName),
// ReSharper restore AssignNullToNotNullAttribute
                                                               Path.GetFileName(fileFullName)));
                    }
                }
            }
        }




        /// <summary>
        /// Create a <see cref="IDirectoryWatcher"/> instance
        /// configured in the way a file drop would need it (ie, just the Create event, really, and no subdirectories).
        /// </summary>
        /// <param name="dropFolderDirectorySpecification"></param>
        /// <returns></returns>
        private IDirectoryWatcher CreateFileSystemWatcher(
            DropFolderDirectorySpecification dropFolderDirectorySpecification)
        {


            //Create a configuration package for this directory
            //that specifies that we want to watch for creates only really:
            IDirectoryWatcherConfiguration fileWatchConfiguration = 
                DependencyResolver.Current.GetInstance<IDirectoryWatcherConfiguration>();

            fileWatchConfiguration.Path = dropFolderDirectorySpecification.Directory;
            fileWatchConfiguration.IncludeSubDirectories = false;
            fileWatchConfiguration.WatcherChangeTypes = WatcherChangeTypes.Created | WatcherChangeTypes.Renamed |
                                                        WatcherChangeTypes.Deleted;

            IDirectoryWatcher result = _fileSystemWatcherService.Register(fileWatchConfiguration);

            result.Name = dropFolderDirectorySpecification.Tag;

            result.Tag = dropFolderDirectorySpecification.Tag;

            return result;
        }




        
        /// <summary>
        /// Event handler for the filewatcher:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void FileSystemWatcherFileCreated(object sender, FileSystemEventArgs eventArgs)
        {

            //Queue it up in a thread, 
            //that will then delay it before processing it.
            //allowing time for hd to calm down, complete writting, etc.

            ThreadPool.QueueUserWorkItem(
// ReSharper disable RedundantDelegateCreation
                new WaitCallback(WaitCallbackCustomHandler),
// ReSharper restore RedundantDelegateCreation
                new WaitCallbackCustomeState{Watcher = sender as IDirectoryWatcher, EventArgs = eventArgs}
                );
        }




        /// <summary>
        /// Called when a new file is found in the drop folder.
        /// </summary>
        /// <param name="a">The args.</param>
        protected void WaitCallbackCustomHandler(object a)
        {
            try
            {
                //Unpack the known args from the given object:
// ReSharper disable SuggestUseVarKeywordEvident
                WaitCallbackCustomeState args = a as WaitCallbackCustomeState;
// ReSharper restore SuggestUseVarKeywordEvident

                //We're wrapping the call to ensure no Exceptions leak through...
                WaitCallbackCustomHandler(args);

            }catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Error, e,"Failed processing dropped file.");
            }
        }

        /// <summary>
        /// Typed callback
        /// </summary>
        /// <param name="args">The args.</param>
        protected void WaitCallbackCustomHandler(WaitCallbackCustomeState args)
        {

        System.Diagnostics.Debug.Assert(args!=null);

            DelayEvent();

            //We're going to rename file atomically to lock it:
            string originalFilePath = args.EventArgs.FullPath;

            string dirPath = Path.GetDirectoryName(originalFilePath);
            DirectoryInfo processingFolderPath = new DirectoryInfo(Path.Combine(dirPath, "Processing"));
            
            string lockedFilePath;
            bool usingLockFolder = processingFolderPath.ExistsOrCreate();
            if (usingLockFolder)
            {
                lockedFilePath = Path.Combine(processingFolderPath.FullName, Path.GetFileName(originalFilePath));
            }
            else
            {

                lockedFilePath =
                    new FileInfo(originalFilePath).PrefixFileName(
                        "_locked_{0}__".FormatStringInvariantCulture(
                            _environmentService.MachineName.MakeSafeForForFilename().Replace("-", "")));
            }


            bool haveExclusiveAccess = GetExclusiveAccess(originalFilePath, ref lockedFilePath);


            //---------------------------------------------
            if (!haveExclusiveAccess)
            {
                //Nothing we can do at this point.
                //As we don't have exclusive access, can't move it to Error folder either.
                _tracingService.Trace(TraceLevel.Verbose, "Don't have exclusive access. Moving on to next file.");

                return;
            }
            //---------------------------------------------
            //We have exclusive access, so can raise event for custom 
            //handler
            IDirectoryWatcher fileSystemDirectoryWatcher = args.Watcher;
            IDropFolderDirectorySpecification dropFolderDirectorySpecification =
                    _fileListenersCache[fileSystemDirectoryWatcher];

            //---------------------------------------------
// ReSharper disable SuggestUseVarKeywordEvident
            DropFolderEventArgs e = new DropFolderEventArgs(args.EventArgs);
// ReSharper restore SuggestUseVarKeywordEvident
            OnNewFile(fileSystemDirectoryWatcher, e);
            //If there was no handler, there will be no Exception
            //---------------------------------------------
            //Depending on whether there were errors call one or the other strategy:

            //But to make things easier, rename things back to how they were:
            string path = lockedFilePath;

            if (e.Exception==null)
            {
                UseTheStrategy(dropFolderDirectorySpecification.SuccessStrategy,
                               _dropFolderSpecifications.SuccessStrategy, path, usingLockFolder, originalFilePath);


            }else
            {
                UseTheStrategy(dropFolderDirectorySpecification.ErrorStrategy,
                               _dropFolderSpecifications.ErrorStrategy, path, usingLockFolder, originalFilePath);
            }
        }

        private bool GetExclusiveAccess(string originalFilePath, ref string lockedFilePath)
        {

            bool haveExclusiveAccess = false;

            int counter = 0;
// ReSharper disable SuggestUseVarKeywordEvident
            int timeout = (int)this.Specs.EventTimeout.TotalMilliseconds;

            int counterMax = (int)(timeout/100d);
// ReSharper restore SuggestUseVarKeywordEvident

            while ((counter < counterMax) && !haveExclusiveAccess)
            {
                try
                {
                    File.Move(originalFilePath, lockedFilePath);
                    haveExclusiveAccess = true;
                    break;
                }
                catch (UnauthorizedAccessException uae)
                {
                    //Never going to work if no access:
                    _tracingService.TraceException(TraceLevel.Error, uae,
                                                   "Cannot process folders in {0} if not given enough rights.", originalFilePath);
                    break;
                }
                catch (FileNotFoundException)
                {
                    //Going to guess that other thread already got it.
                    _tracingService.Trace(TraceLevel.Verbose,
                                          "File Not Found ({0}): appears another process already picked up file.",
                                          originalFilePath);
                    //Never going to exist.
                    break;
                }
                catch (IOException ioe)
                {
                    _tracingService.TraceException(TraceLevel.Verbose,ioe,
                                          "Error..figuring it out....",
                                          originalFilePath);

                    //One reason is Cannot create file if target already exists...
                    if (File.Exists(lockedFilePath))
                    {
                        _tracingService.Trace(TraceLevel.Verbose,"Was because target existed",originalFilePath);
                        lockedFilePath = new FileInfo(lockedFilePath).PrefixFileName("_conflict_{0:yyyyMMddHHmm_ffff}_".FormatStringInvariantCulture(_dateTimeService.NowUTC));
                        counter++;
                        //Loop back now without waiting:
                        continue;
                    }


                    //something else. Maybe other process has it open?
                    if (!File.Exists(originalFilePath))
                    {

                        //File doesn't exist...guess it's same condition as above?
                        _tracingService.Trace(TraceLevel.Verbose,
                                              "File Not Found ({0}): appears another process already picked up file.",
                                              originalFilePath);
                        break;
                    }
                    _tracingService.Trace(TraceLevel.Verbose, "Exists...but don't have access to it yet. Let's try again in a bit", originalFilePath);

                    //it exists, but don't have access. Let's try it again...
                    //NO BREAK:
                }
                catch (Exception ge)
                {
                    //If file is there, we're never going to get this sorted out:
                    _tracingService.TraceException(TraceLevel.Error, ge,
                                                   "Cannot process file ({0}), due to unknown reasons.", originalFilePath);
                    break;
                }

                Thread.Sleep(100);

                //Increment counter before we try again:
                counter++;
            }
            return haveExclusiveAccess;
        }

        private void DelayEvent()
        {
//---------------------------------------------
            //We are in another thread, so safe to Sleep:
            //to wait for folder to calm down:
            if (this.Specs.EventDelay != new TimeSpan(0))
            {
                Thread.Sleep((int) this.Specs.EventDelay.TotalMilliseconds);
            }
            //---------------------------------------------
        }


        private void OnNewFile(IDirectoryWatcher fileSystemDirectoryWatcher, DropFolderEventArgs e)
        {
            //If we have an eventhandler, raise it:
            if (NewFile == null)
            {
                //hasErrors = false;
            }
            else
            {
                //raise event:
                NewFile(fileSystemDirectoryWatcher, e);
            }
        }

        private void UseTheStrategy(IDropFolderResultStrategySpecification strategySpecification, IDropFolderResultStrategySpecification fallbackStrategySpecification, string srcPath, bool usingLockFolder, string originalFilePath)
        {

            //From instance, get specs:
            if (!strategySpecification.Enabled)
            {
                //The strategy is turned off, so we won't be moving it.
                return;
            }


            //The goal here is to move the file from the drop folder to the current strategy's folder.

            string directoryFullName = strategySpecification.Directory;

            if ((directoryFullName == null) || (string.IsNullOrEmpty(directoryFullName)))
            {
                if (fallbackStrategySpecification == null)
                {
                    //we are at the top, but no path...error
                    return;
                }

                //As this directory is of no use, see if the base on is better?
                //Note that we know this one is enabled, but the base one could be disabled.
                //That's still valid...(it would not move here, nor move down there, each
                //for different reasons).
                UseTheStrategy(fallbackStrategySpecification, null, srcPath, usingLockFolder, originalFilePath);
                //Get out:
                return;
            }

            // ReSharper disable AssignNullToNotNullAttribute


            string destPath = Path.Combine(directoryFullName, Path.GetFileName(originalFilePath));
            // ReSharper restore AssignNullToNotNullAttribute

            //There is no reason this should fail at this point:
            bool success = false;
            bool DIREXISTS = _ioService.DirectoryExistsAsync(directoryFullName, true).WaitAndGetResult();
            
            if (DIREXISTS)
            {
                success = new FileInfo(srcPath).SafeMove(destPath, 3, 100, true);
            }
            if (success==false)
            {
                throw new Exception("Failed moving file.");
            }

        }


        #region Nested Classes
        /// <summary>
        /// Arg package for the thread:
        /// </summary>
        public class WaitCallbackCustomeState
        {
            /// <summary>
            /// Gets or sets the watcher.
            /// </summary>
            /// <value>The watcher.</value>
            public IDirectoryWatcher Watcher { get; set; }
            /// <summary>
            /// Gets or sets the event args.
            /// </summary>
            /// <value>The event args.</value>
            public FileSystemEventArgs EventArgs { get; set; }
        }
        #endregion

    }
}