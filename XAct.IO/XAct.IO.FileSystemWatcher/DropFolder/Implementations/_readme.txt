﻿## Configuration Package ##
A custom DropFolderConfigurationservice is required to parse the config file to create a DropFolderSpecifications.
The DropFolderSpecifications contains:
* colleciton of DropFolderDirectorySpecifiction
   * each one with a nested: 
      * DropFolderDirectoryArchiveSpecification
      * DropFolderDirectoryErrorSpecification
In addition, each DropFolderSpecification has a fallback
* DropFolderArchiveSpecification
* DropFolderErrorSpecification

## Service ## 
The above DropFolderSpecifications is passed to a DropFolderService.Register method, which 
creates a DropFolderWatcher instance, registers a reference to it in the global FielDropWatcherCache
and returns it.
Once returned, the invoking application can attach events to the instance, without dealing with inner workings
of archiving, or error handling, just return Failed=true in even raised, so that Listener moves it according
to Archiving or Error strategy.


## DropFolder Instance ##
The DropFolderWatcher maintains its watcher stretegies, only raising events that an application can take advantage of.

