﻿// ReSharper disable CheckNamespace
namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using System;
    
    /// <summary>
    /// A common absract base class for Success/Error strategies.
    /// </summary>
    /// <remarks>
    /// If using a config file to specify settings, the values
    /// will come from parsing something similar to the following:
    /// <![CDATA[
    /// <dropFolderSettings>
    /// <dropFolders>
    /// <dropFolder Directory="...">
    /// <successStrategy directory="...\Sales\Success" archiveAction="Delete" archiveDelay="14,0,0,0"  />
    /// <errorStrategy  directory="...\Sales\Error" archiveAction="Move" archiveDelay="14,0,0,0"  archiveDirectory="...\Common\Error" />
    /// </dropFolder>
    /// <dropFolder Directory="..."/>
    /// </dropFolders>
    /// <successStrategy directory="...\Common\Success" archiveAction="Delete" archiveDelay="14,0,0,0" />
    /// <errorStrategy  directory="...\Common\Error" archiveAction="Delete" archiveDelay="14,0,0,0"   />
    /// </dropFolderSettings>
    /// ]]>
    /// </remarks>
    public class DropFolderResultStrategySpecification : IDropFolderResultStrategySpecification
    {

        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// <see cref="DropFolderResultStrategySpecification"/> is enabled.
        /// <para>Default is <c>true</c> (although default <see cref="Action"/>
        /// is NoAction).
        /// </para>
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the directory to move the files to.
        /// <para>Default is null.</para>
        /// </summary>
        /// <value>The directory.</value>
        public string Directory { get; set; }


        /// <summary>
        /// Gets or sets the delay before files the <see cref="ArchiveAction"/> (Move or Delete)
        /// is taken.
        /// <para>
        /// Default is <c>15</c> minutes.
        /// </para>
        /// </summary>
        /// <value>The delay.</value>
        public TimeSpan ArchiveCheckInterval { get; set; }

        /// <summary>
        /// Gets or sets the delay before files the <see cref="ArchiveAction"/> (Move or Delete)
        /// is taken.
        /// <para>
        /// Default is 14 days.
        /// </para>
        /// </summary>
        /// <value>The delay.</value>
        public TimeSpan ArchiveDelay { get; set; }

        /// <summary>
        /// Gets or sets the archiving strategy to use on the associated drop folder.
        /// <para>
        /// Default value is <c>NoAction</c>.
        /// </para>
        /// </summary>
        /// <value>The action.</value>
        public ArchivingAction ArchiveAction { get; set; }
        
        /// <summary>
        /// If <see cref="ArchiveAction"/> is set to <c>Move</c>,
        /// moves the file to this directory
        /// after the <see cref="ArchiveDelay"/>
        /// <para>
        /// Default value = null
        /// </para>
        /// </summary>
        public string ArchiveMoveTo { get; set; }

        /// <summary>
        /// Gets the type of the file date.
        /// </summary>
        /// <value>
        /// The type of the file date.
        /// </value>
        public FileDateType ArchiveFileDateType { get; set; }


        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="DropFolderResultStrategySpecification"/> class.
        /// </summary>
        public DropFolderResultStrategySpecification()
        {
            Enabled = true;
            Directory = null;
            ArchiveCheckInterval = TimeSpan.FromMinutes(15);
            ArchiveDelay = new TimeSpan(14, 0, 0, 0);
            ArchiveAction = ArchivingAction.NoAction;
            ArchiveMoveTo = null;
            ArchiveFileDateType=FileDateType.LastModified;
        }

    }
}
