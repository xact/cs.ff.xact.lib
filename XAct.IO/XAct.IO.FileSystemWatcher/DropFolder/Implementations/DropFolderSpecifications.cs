﻿
// ReSharper disable CheckNamespace

namespace XAct.IO
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Specifications needed by the <see cref="IDropFolderService"/>
    /// to watch and process a set of directories for new files.
    /// </summary>
    /// <remarks>
    /// <para>
    /// If using a config file to specify settings, the values
    /// will come from parsing something similar to the following:
    /// <![CDATA[
    /// <dropFolderSettings>
    ///   <dropFolders>
    ///     <dropFolder Directory="...">
    ///       <successStrategy enabled="true" delay="0,0,1" directory="..."/>
    ///       <errorStrategy enabled="true" directory="..."/>
    ///     </dropFolder>
    ///     <dropFolder Directory="..."/>
    ///   </dropFolders>
    ///   <successStrategy enabled="true" delay="0,0,1" directory="..."/>
    ///   <errorStrategy enabled="true" directory="..."/>
    /// </dropFolderSettings>
    /// ]]>
    /// </para>
    /// </remarks>
    public class DropFolderSpecifications : IHasEnabledReadOnly, IDropFolderSpecifications, IHasTag
    {

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>
        /// The default state is enabled.
        /// </para>
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled { get; set; }



        /// <summary>
        /// Gets or sets the delay between receiving the event, and raising
        /// it for custom handling (can be used to handle closing down of
        /// incoming threads (eg: from scanner) that may not have finished writing).
        /// <para>
        /// Default value is <c>100ms</c>.
        /// </para>
        /// </summary>
        /// <value>
        /// The delay.
        /// </value>
        public TimeSpan EventDelay { get; set; }

        /// <summary>
        /// Gets or sets the amount of time to wait before
        /// giving up getting exclusive access to the file.
        /// </summary>
        /// <value>
        /// The event timeout.
        /// </value>
        public TimeSpan EventTimeout { get; set; }


        /// <summary>
        /// Gets the tag of the object. Useful to distinguish between processes
        /// in a High Availability scenario, when two processes are looking at the same directory.
        /// </summary>
        /// <internal>8/13/2011: Sky</internal>
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the collection of drop folder directories to watch.
        /// </summary>
        /// <value>The directories.</value>
        public IDropFolderDirectorySpecification[] Directories { get; set; }

        /// <summary>
        /// Gets or sets the (optional) success folder to move documents to (when possible)
        /// upon success.
        /// <para>
        /// The path can be local or remote.
        /// <code>
        /// "\\serverXYZ\sharedFolder\success\"
        /// </code>
        /// </para>
        /// </summary>
        /// <value>The success folder.</value>
        public IDropFolderResultStrategySpecification SuccessStrategy { get; set; }


        /// <summary>
        /// Gets or sets the (optional) error folder to move documents (when possible)
        /// to when there was an error.
        /// <para>
        /// The path can be local or remote.
        /// <code>
        /// "\\serverXYZ\sharedFolder\error\"
        /// </code>
        /// </para>
        /// </summary>
        /// <value>The error folder.</value>
        public IDropFolderResultStrategySpecification ErrorStrategy { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DropFolderSpecifications"/> class.
        /// </summary>
        public DropFolderSpecifications()
        {
            Enabled = true;

            EventDelay = new TimeSpan(0,0,0,0,0);
            EventTimeout = TimeSpan.FromSeconds(30);

            SuccessStrategy = new DropFolderResultStrategySpecification();
            ErrorStrategy = new DropFolderResultStrategySpecification();
        }

    }
}