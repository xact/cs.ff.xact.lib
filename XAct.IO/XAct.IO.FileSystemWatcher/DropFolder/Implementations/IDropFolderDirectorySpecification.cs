namespace XAct.IO
{
    
    /// <summary>
    /// 
    /// </summary>
    public interface IDropFolderDirectorySpecification: IHasNameReadOnly, IHasEnabledReadOnly, IHasTagReadOnly
    {
        /// <summary>
        /// Gets or sets a value indicating whether to include events from sub directories.
        /// <para>
        /// CAUTION:
        /// Default value is <c>False</c> so that if Archive and Error folders are 
        /// nested (per Directory specification), there isn't a recursive loop.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include sub directories]; otherwise, <c>false</c>.
        /// </value>
        bool IncludeSubDirectories { get; set; }

        /// <summary>
        /// Gets or sets the path of the folder to watch.
        /// <para>
        /// The path can be local or remote.
        /// <code>
        /// "\\serverXYZ\sharedFolder\"
        /// </code>
        /// </para>
        /// </summary>
        /// <value>The path to the folder to watch.</value>
        string Directory { get; set; }

        /// <summary>
        /// Gets or sets settings as to where to archive dropped files,
        /// as well when to remove them.
        /// </summary>
        /// <remarks>
        /// <remarks>
        /// <para>
        /// If using a config file to specify settings, the values
        /// will come from parsing something similar to the following:
        /// <![CDATA[
        /// <dropFolderSettings>
        ///   <dropFolders>
        ///     <dropFolder Directory="...">
        ///       <successStrategy Enabled="Enabled" delay="0,0,1" directory="..."/>
        ///       <errorStrategy Enabled="Enabled" directory="..."/>
        ///     </dropFolder>
        ///     <dropFolder Directory="..."/>
        ///   </dropFolders>
        ///   <successStrategy enabled="true" delay="0,0,1" directory="..."/>
        ///   <errorStrategy enabled="true" directory="..."/>
        /// </dropFolderSettings>
        /// ]]>
        /// </para>
        /// </remarks>
        /// </remarks>
        /// <value>The archive specs.</value>
        IDropFolderResultStrategySpecification SuccessStrategy { get; set; }

        /// <summary>
        /// Gets or sets the (optional) error folder to move documents (when possible)
        /// to when there was an error.
        /// <para>
        /// The path can be local or remote.
        /// <code>
        /// "\\serverXYZ\sharedFolder\error\"
        /// </code>
        /// </para>
        /// </summary>
        /// <value>The error folder.</value>
        IDropFolderResultStrategySpecification ErrorStrategy { get; set; }
    }
}