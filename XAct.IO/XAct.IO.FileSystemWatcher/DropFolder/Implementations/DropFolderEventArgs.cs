namespace XAct.IO
{
    using System;
    using System.IO;

    /// <summary>
    /// Argument package for the event raised by the 
    /// <see cref="IDropFolderWatcher"/> when a new file is ready to be processed.
    /// </summary>
    public class DropFolderEventArgs : FileSystemEventArgs, IHasException
    {

        /// <summary>
        /// Gets or sets a value indicating whether the event handler encountered errors.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has errors; otherwise, <c>false</c>.
        /// </value>
        public Exception Exception { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="DropFolderEventArgs"/> class.
        /// </summary>
        /// <param name="dropFolderEventArgs">The <see cref="System.IO.FileSystemEventArgs"/> instance containing the event data.</param>
        public DropFolderEventArgs(FileSystemEventArgs dropFolderEventArgs)
            : base(dropFolderEventArgs.ChangeType, Path.GetDirectoryName(dropFolderEventArgs.FullPath), dropFolderEventArgs.Name)
        {
        }
    }
}