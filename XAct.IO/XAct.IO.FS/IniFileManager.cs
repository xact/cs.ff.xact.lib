#undef INCLUDE_EXAMPLES

namespace XAct.IO
{
    using System;
    using System.Collections.Specialized;
    using System.Runtime.InteropServices;
    using System.Text;

    ///<summary>
    ///  Class to handle INI file I/O
    ///</summary>
    ///<summary>
    ///  Constructor
    ///</summary>
    ///<remarks>
    ///  <para>
    ///    I've built this class too many times in too many languages...this time, the code was found for free at: http://archive.devx.com/dotnet/discussions/040902/cominterop.asp, thank you!...Although he does end with:
    ///    this code is meant for example use only. You should add error-trapping and checking (see the DllImportAttribute.SetLastError field in the documentation, and the Marshal.GetLastWin32Error method in the documentation for more information.) 
    ///  </para>
    ///  <para>
    ///    ToDo: 
    ///    Extend to use XML or INI, according to article:
    ///    http://www.devx.com/dotnet/Article/7008/0/page/1
    ///  </para>
    ///</remarks>
    public class IniFileManager
    {
        /// <summary>
        ///   Wrapper for API Call
        /// </summary>
        [DllImport("KERNEL32.DLL",
            EntryPoint = "GetPrivateProfileString")]
        protected internal static extern int
            GetPrivateProfileString(string lpAppName,
                                    string lpKeyName, string lpDefault,
                                    StringBuilder lpReturnedString, int nSize,
                                    string lpFileName);

        /// <summary>
        ///   Wrapper for API Call
        /// </summary>
        [DllImport("KERNEL32.DLL")]
        protected internal static extern int
            GetPrivateProfileInt(string lpAppName,
                                 string lpKeyName, int iDefault,
                                 string lpFileName);

        /// <summary>
        ///   Wrapper for API Call
        /// </summary>
        [DllImport("KERNEL32.DLL",
            EntryPoint = "WritePrivateProfileString")]
        protected internal static extern bool
            WritePrivateProfileString(string lpAppName,
                                      string lpKeyName, string lpString,
                                      string lpFileName);

        /// <summary>
        ///   Wrapper for API Call
        /// </summary>
        [DllImport("KERNEL32.DLL",
            EntryPoint = "GetPrivateProfileSection")]
        protected internal static extern int
            GetPrivateProfileSection(string lpAppName,
                                     byte[] lpReturnedString, int nSize,
                                     string lpFileName);

        /// <summary>
        ///   Wrapper for API Call
        /// </summary>
        [DllImport("KERNEL32.DLL",
            EntryPoint = "WritePrivateProfileSection")]
        protected internal static extern bool
            WritePrivateProfileSection(string lpAppName,
                                       byte[] data, string lpFileName);


        /// <summary>
        ///   Wrapper for API Call
        /// </summary>
        [DllImport("KERNEL32.DLL",
            EntryPoint = "GetPrivateProfileSectionNames")]
        protected internal static extern int
            GetPrivateProfileSectionNames(
            byte[] lpReturnedString,
            int nSize, string lpFileName);


        /// <summary>
        ///   Returns Value from INI file.
        /// </summary>
        /// <param name = "fileName"></param>
        /// <param name = "section"></param>
        /// <param name = "key"></param>
        /// <returns></returns>
        public static String GetIniValue(String fileName,
                                         String section, String key)
        {
            StringBuilder buffer = new StringBuilder(256);
            string sDefault = "";
            if (GetPrivateProfileString(section, key, sDefault,
                                        buffer, buffer.Capacity, fileName) != 0)
            {
                return buffer.ToString();
            }
            return null;
        }


        /// <summary>
        ///   Writes string to an INI file.
        /// </summary>
        /// <param name = "fileName"></param>
        /// <param name = "section"></param>
        /// <param name = "key"></param>
        /// <param name = "sValue"></param>
        /// <returns></returns>
        public static bool WriteIniValue(String fileName,
                                         String section, String key, String sValue)
        {
            return WritePrivateProfileString(section, key,
                                             sValue, fileName);
        }


        /// <summary>
        ///   Get Int value from an INI File.
        /// </summary>
        /// <param name = "fileName"></param>
        /// <param name = "section"></param>
        /// <param name = "key"></param>
        /// <returns></returns>
        public static int GetIniInt(String fileName,
                                    String section, String key)
        {
            int iDefault = -1;
            var result = GetPrivateProfileInt(section, key, iDefault, fileName);
            return result;
        }


        /// <summary>
        ///   Gets List of Values from an INI section.
        /// </summary>
        /// <param name = "fileName"></param>
        /// <param name = "section"></param>
        /// <returns></returns>
        public static StringCollection GetIniSection(String fileName, String section)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            StringCollection items = new StringCollection();
            byte[] buffer = new byte[32768];
            int bufLen;
            bufLen = GetPrivateProfileSection(section, buffer,
                                              buffer.GetUpperBound(0), fileName);
            if (bufLen > 0)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bufLen; i++)
                {
                    if (buffer[i] != 0)
                    {
                        sb.Append((char) buffer[i]);
                    }
                    else
                    {
                        if (sb.Length > 0)
                        {
                            items.Add(sb.ToString());
                            sb = new StringBuilder();
                        }
                    }
                }
            }
            return items;
        }


        /// <summary>
        ///   Writes a List to INI Section
        /// </summary>
        /// <param name = "fileName"></param>
        /// <param name = "section"></param>
        /// <param name = "items"></param>
        /// <returns></returns>
        public static bool WriteIniSection(string fileName, string
                                                                section, StringCollection items)
        {
            byte[] b = new byte[32768];
            int j = 0;
            foreach (string s in items)
            {
                //ASCII is not available on PCL, so  switching to UTF8:
                Encoding.UTF8.GetBytes(s, 0, s.Length, b, j);
                j += s.Length;
                b[j] = 0;
                j += 1;
            }
            b[j] = 0;
            return WritePrivateProfileSection(section, b, fileName);
        }


        /// <summary>
        ///   Get name of all sections in INI File.
        /// </summary>
        /// <param name = "fileName"></param>
        /// <returns></returns>
        public static StringCollection GetIniSectionNames(
            String fileName)
        {
            StringCollection sections = new StringCollection();
            byte[] buffer = new byte[32768];
            int bufLen;
            bufLen = GetPrivateProfileSectionNames(buffer,
                                                   buffer.GetUpperBound(0), fileName);
            if (bufLen > 0)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bufLen; i++)
                {
                    if (buffer[i] != 0)
                    {
                        sb.Append((char) buffer[i]);
                    }
                    else
                    {
                        if (sb.Length > 0)
                        {
                            sections.Add(sb.ToString());
                            sb = new StringBuilder();
                        }
                    }
                }
            }
            return sections;
        }
    }
}