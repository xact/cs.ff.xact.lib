﻿
// ReSharper disable CheckNamespace
namespace XAct.IO.Compression
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// The contract for a CompressionService that uses
    /// the Framework's built in Zip Package technology.
    /// <para>
    /// Note that a Zip Package is not really a proper
    /// Zip service -- it's more like a poor man' solution,
    /// as it's built into the Framework
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// For background, you may be interested in: http://bit.ly/AmjQIH
    /// </para>
    /// </remarks>
    public interface IZipPackageCompressionService : ICompressionService, IHasXActLibService 
    {
    }
}
