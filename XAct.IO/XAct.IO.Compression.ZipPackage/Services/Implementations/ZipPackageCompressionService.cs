namespace XAct.IO.Compression.Implementations
{
    using System;
    using System.IO;
    using System.IO.Packaging;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="ICompressionService"/>
    /// to create Zip Packages (Compression + Structure)
    /// </summary>
    /// <remarks>
    /// <para>
    /// For background, you may be interested in: http://bit.ly/AmjQIH
    /// </para>
    /// </remarks>
    /// <internal>
    /// To finish it, read this: http://www.gutgames.com/post/Creating-a-Zip-File-in-C.aspx
    /// </internal>
    [DefaultBindingImplementation(typeof(ICompressionService),BindingLifetimeType.Undefined,Priority.Low /*OK:Secondary Binding*/)]
    public class ZipPackageCompressionService : IZipPackageCompressionService
    {

        private readonly ITracingService _tracingService;
        private readonly IIOService _ioService;
        private readonly IPathService _pathService;

        //static void Main(string[] args)
        //{
        //    AddFileToZip("Output.zip", @"C:\Windows\Notepad.exe");
        //    AddFileToZip("Output.zip", @"C:\Windows\System32\Calc.exe");
        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="ZipPackageCompressionService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ioService">The io service.</param>
        /// <param name="pathService">The path service.</param>
        public ZipPackageCompressionService (ITracingService tracingService, IIOService ioService, IPathService pathService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            ioService.ValidateIsNotDefault("ioService");

            _tracingService = tracingService;
            _ioService = ioService;
            _pathService = pathService;

            _tracingService.Trace(TraceLevel.Verbose, "ZipPackageServier.Constructor();");
        }


     //   public void AddFileToZip(string outputZipFilePath, string file)
     //   {
     //using (Stream outputZipFileStream = new FileStream(outputZipFilePath,  FileMode.OpenOrCreate,FileAccess.ReadWrite)
                                           
     //                                  }
        
     //   using (Stream stream = _ioService.OpenWrite(outputZipFilePath,false,true){
     //       {
     //       }

     //   }


        /// <summary>
        /// Adds all descendent files within <paramref name="directoryFullName"/> to the Zip file.
        /// </summary>
        /// <param name="outputZipFileFullName">The zip file path.</param>
        /// <param name="directoryFullName">The directory info.</param>
        public void AddDirectoryToZip(string outputZipFileFullName, string directoryFullName)
        {
            _tracingService.Trace(TraceLevel.Verbose,"ZipPackageService.AddDirectoryToZip({0},{1})".FormatStringCurrentCulture(outputZipFileFullName.ToString(),directoryFullName.ToString()));

            using (
                Stream outputZipFileStream = 
                    _ioService.FileOpenAsync(
                    outputZipFileFullName,
                    XAct.IO.FileMode.OpenOrCreate,
                    XAct.IO.FileAccess.ReadWrite,
                    XAct.IO.FileShare.None).WaitAndGetResult())
            {
                AddDirectoryToZip(outputZipFileStream, directoryFullName);
            }
        }


        /// <summary>
        /// Adds the specified file to the Zip file.
        /// <para>
        /// If <paramref name="relativeToDirectory"/> = null, defaults to <c>file.DirectoryInfo</c>
        /// 	</para>
        /// </summary>
        /// <param name="outputZipFileFullName">The zip file path.</param>
        /// <param name="fileFullName">The new file path.</param>
        /// <param name="relativeToDirectory">The relative to directory.</param>
        public void AddFileToZip(string outputZipFileFullName, string fileFullName, string relativeToDirectory = null)
        {
            if (relativeToDirectory== null){relativeToDirectory = _pathService.GetDirectoryName(outputZipFileFullName);}

            _tracingService.Trace(TraceLevel.Verbose, "ZipPackageService.AddFileToZip({0},{1},{2})".FormatStringCurrentCulture(outputZipFileFullName.ToString(), fileFullName.ToString(), relativeToDirectory.ToString()));

            using (
                Stream outputZipFileStream = 
                    _ioService.FileOpenAsync(
                    outputZipFileFullName,
                    XAct.IO.FileMode.OpenOrCreate,
                    XAct.IO.FileAccess.ReadWrite,
                    XAct.IO.FileShare.None).WaitAndGetResult())
            {
                AddFileToZip(outputZipFileStream, fileFullName, relativeToDirectory);
            }
        }


        /// <summary>
        /// Adds all descendent files within <paramref name="directoryFullName"/> to the Zip file.
        /// </summary>
        /// <param name="outputZipFileStream">The zip file stream.</param>
        /// <param name="directoryFullName">The directory of files to add.</param>
public void AddDirectoryToZip(Stream outputZipFileStream, string directoryFullName)
        {



            string[] fileFullNames = _ioService.GetDirectoryFileNamesAsync(directoryFullName, null, HierarchicalOperationOption.Recursive).WaitAndGetResult();

            foreach ( string fileFullName in fileFullNames)
            {
                this.AddFileToZip(outputZipFileStream, fileFullName, directoryFullName);
            }
        }



/// <summary>
/// Adds the file to the zip file.
/// <para>
/// If <paramref name="relativeToDirectoryFullName"/> = null, defaults to <c>file.DirectoryInfo</c>
/// 	</para>
/// </summary>
/// <param name="outputZipFileStream">The zip file stream.</param>
/// <param name="fileFullName">The file to add.</param>
/// <param name="relativeToDirectoryFullName">The relative to. If null, defaults to DirectoryInfo of file.</param>
        public void AddFileToZip(Stream outputZipFileStream, string fileFullName, string relativeToDirectoryFullName=null)
        {
            if (relativeToDirectoryFullName == null)
            {
                relativeToDirectoryFullName = _pathService.GetDirectoryName(fileFullName);
            }

            using (FileStream filePathStream = new FileStream(fileFullName, FileMode.Open, FileAccess.Read))
            {
                //From the source file, 
                //Get the 'Zip-relative' Uri where we will place the file within the zip:
                string zipRelativeFilePath = "." + new FileInfo(fileFullName).RelativeTo(new DirectoryInfo(relativeToDirectoryFullName), false);// Path.GetFileName(file);

                Uri zipRelativeUri = PackUriHelper.CreatePartUri(new Uri(zipRelativeFilePath, UriKind.Relative));

                AddFileToZip(outputZipFileStream, filePathStream, zipRelativeUri);
            }
        }

        /// <summary>
        /// Adds a file to the specified zip file.
        /// </summary>
        /// <param name="outputZipFileStream">The zip file stream.</param>
        /// <param name="filePathStream">The file to add.</param>
        /// <param name="zipRelativeUri">The zip relative URI.</param>
        private void AddFileToZip(Stream outputZipFileStream, Stream filePathStream, Uri zipRelativeUri)
        {
            using (Package zip = Package.Open(outputZipFileStream, FileMode.OpenOrCreate))
            {
                //As we are adding to an existing zip, the file
                //might already be there...and needs to be cleared out first:
                if (zip.PartExists(zipRelativeUri))
                {
                    zip.DeletePart(zipRelativeUri);
                }

                PackagePart part = zip.CreatePart(zipRelativeUri, "", CompressionOption.Normal);
                
                using (Stream dest = part.GetStream())
                {
                    dest.CopyStreamFrom(filePathStream);
                }

                zip.PackageProperties.ContentType = null;
            }

        }



    }
}