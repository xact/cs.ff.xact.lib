﻿namespace XAct.IO.TemplateEngines.Implementations
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using Commons.Collections;
    using NVelocity;
    using NVelocity.App;
    using NVelocity.Runtime;
    using XAct.Diagnostics;
    using XAct.Services;
//using System.Linq;

    /// <summary>
    /// An implementation of <see cref="ITemplateRenderingService"/>
    /// using NVelocity
    /// </summary>
    /// <remarks>
    /// <para>
    /// Reference: http://velocity.apache.org/engine/releases/velocity-1.5/user-guide.html
    /// </para>
    /// <para>
    /// Excellent post: http://mark-dot-net.blogspot.com/2009/05/creating-html-using-nvelocity.html
    /// </para>
    /// For an intro to the syntax: http://www.auctivacommerce.com/help/entry.aspx?id=An-introduction-to-NVelocity-for-custom-email-template-design
    /// </remarks>
    [DefaultBindingImplementation(typeof(ITemplateRenderingService), BindingLifetimeType.Undefined, Priority.Low /*OK: Don't have a preference between technologies*/)]
    public class NVelocityTemplateService : XActLibServiceBase, INVelocityTemplateService
    {

        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public ITemplateRenderingServiceConfiguration Configuration
        {
            get;
            private set;
        }
        private string _templateDirectory;

        private VelocityEngine _velocityEngine;
        //private VelocityContext _velocityContext;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="NVelocityTemplateService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="serviceSettings">The service settings.</param>
        public NVelocityTemplateService(ITracingService tracingService, ITemplateRenderingServiceConfiguration serviceSettings):base(tracingService)
        {
            _velocityEngine = new VelocityEngine();
            _velocityEngine.Init(new ExtendedProperties());

            Configuration = serviceSettings;
        }

        #region ITemplateRenderingService Members

        /// <summary>
        /// Compiles the source string template to the the output string.
        /// </summary>
        /// <param name="templateText">The template text.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateString(string templateText,
                                            IEnumerable<Assembly> optionalReferencedAssemblies = null,
                                            object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            if (templateText.IsNullOrEmpty()) { throw new ArgumentNullException("templateText"); }

            using (TextWriter outputStreamTextWriter = new StringWriter())
            {
                NVelocity.Context.IContext context = CreateNVelocityContext(optionalContext, avoidContextPrefixIfPossible);

                _velocityEngine.Evaluate(context, outputStreamTextWriter, "string", templateText);
                
                return outputStreamTextWriter.ToString();
            }
        }

        /// <summary>
        /// Compiles the source template string to the given output stream.
        /// </summary>
        /// <param name="outputStream">The stream.</param>
        /// <param name="templateText">The template text.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateString(Stream outputStream, string templateText,
                                           IEnumerable<Assembly> optionalReferencedAssemblies = null,
                                           object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            outputStream.ValidateIsNotDefault("outputStream");
            if (templateText.IsNullOrEmpty()) { throw new ArgumentNullException("templateText"); }

            long startPosition = outputStream.Position;

            using (TextWriter outputStreamTextWriter = new StreamWriter(outputStream))
            {
                NVelocity.Context.IContext context = CreateNVelocityContext(optionalContext, avoidContextPrefixIfPossible);
                _velocityEngine.Evaluate(context, outputStreamTextWriter, "string", templateText);
                return outputStream.Position - startPosition;
            }
        }



        /// <summary>
        /// Compiles the source template text stream to the target string.
        /// </summary>
        /// <param name="textReader">The text reader.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateStream(TextReader textReader,
                                    IEnumerable<Assembly> optionalReferencedAssemblies = null,
                                    object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            textReader.ValidateIsNotDefault("textReader");

            using (TextWriter outputStreamTextWriter = new StringWriter())
            {
                NVelocity.Context.IContext context = CreateNVelocityContext(optionalContext, avoidContextPrefixIfPossible);
                _velocityEngine.Evaluate(context, outputStreamTextWriter, "string", textReader);
                return outputStreamTextWriter.ToString();
            }
        }


        /// <summary>
        /// Compiles the source template text stream to the target output stream.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="textReader">The text reader.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateStream(Stream outputStream, TextReader textReader,
                                    IEnumerable<Assembly> optionalReferencedAssemblies = null,
                                    object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            outputStream.ValidateIsNotDefault("outputStream");
            textReader.ValidateIsNotDefault("textReader");

            long startPosition = outputStream.Position;
            using (TextWriter outputStreamTextWriter = new StreamWriter(outputStream))
            {
                NVelocity.Context.IContext context = CreateNVelocityContext(optionalContext, avoidContextPrefixIfPossible);
                _velocityEngine.Evaluate(context, outputStreamTextWriter, "string", textReader);
                return outputStream.Position - startPosition;
            }
        }

        /// <summary>
        /// Compiles the source template file to the target string.
        /// <para>
        /// Note that in NVelocity, the template variables are preceded with the word $Model:
        /// <code>
        /// 			<![CDATA[
        /// Hello $Model.PlanelVar!
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </summary>
        /// <param name="templateFileName">Name of the template file.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns></returns>
        public string CompileTemplateFile(string templateFileName,
                                          IEnumerable<Assembly> optionalReferencedAssemblies = null,
                                          object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            if (templateFileName.IsNullOrEmpty()){throw new ArgumentNullException("templateFileName");}

            InitEngine(templateFileName);

            using (TextWriter outputStreamTextWriter = new StringWriter())
            {
                string fileName = Path.GetFileName(templateFileName);
                //Note that there is no way that I can pass this a stream 
                //instead of a fileName....
                Template template = _velocityEngine.GetTemplate(fileName);

                NVelocity.Context.IContext context = CreateNVelocityContext(optionalContext, avoidContextPrefixIfPossible);
                
                template.Merge(context, outputStreamTextWriter);

                return outputStreamTextWriter.ToString();
            }
            //catch (ResourceNotFoundException rnf)
            //catch (ParseErrorException pe)
        }

        /// <summary>
        /// Compiles the source template file to the given output stream.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="templateFileName">Name of the template file.</param>
        /// <param name="optionalReferencedAssemblies">The optional referenced assemblies.</param>
        /// <param name="optionalContext">The optional context.</param>
        /// <param name="avoidContextPrefixIfPossible">if set to <c>true</c> [avoid context prefix if possible].</param>
        /// <returns>
        /// The number of bytes written to the stream
        /// </returns>
        public long CompileTemplateFile(Stream outputStream, string templateFileName,
                                          IEnumerable<Assembly> optionalReferencedAssemblies = null,
                                          object optionalContext = null, bool avoidContextPrefixIfPossible = true)
        {
            outputStream.ValidateIsNotDefault("outputStream");
            if (templateFileName.IsNullOrEmpty()) { throw new ArgumentNullException("templateFileName"); }

            InitEngine(templateFileName);

            long startPos = outputStream.Position;
            
            using (TextWriter outputStreamTextWriter = new StreamWriter(outputStream))
            {
                Template template = _velocityEngine.GetTemplate(templateFileName);

                NVelocity.Context.IContext context = CreateNVelocityContext(optionalContext, avoidContextPrefixIfPossible);
                
                template.Merge(context, outputStreamTextWriter);
                
                return outputStream.Position - startPos;
            }
        }




        #endregion

        #region Private

        void InitEngine(string templateFileName)
        {
            string templateDirectory = Path.GetDirectoryName(templateFileName);
            if (string.Compare(templateDirectory, _templateDirectory, true) == 0)
            {
                return;
            }
            _velocityEngine = new VelocityEngine();
            PrepareEngineForTemplates(_velocityEngine, templateDirectory, true);
            _templateDirectory = templateDirectory;

        }

// ReSharper disable UnusedMember.Local
        private VelocityEngine MakeFileEngine(string templateDirectory)
// ReSharper restore UnusedMember.Local
        {
            VelocityEngine velocityEngine = new VelocityEngine();
            PrepareEngineForTemplates(velocityEngine, templateDirectory, true);

            return velocityEngine;
        }

        private static void PrepareEngineForTemplates(VelocityEngine velocityEngine, string templateDirectory, bool cacheTemplate)
        {
            //templateDirectory = templateDirectory.Replace("\\", "/");
            ExtendedProperties extendedProperties = new ExtendedProperties();
            extendedProperties.SetProperty(RuntimeConstants.RESOURCE_LOADER, "file");
            if (Path.IsPathRooted(templateDirectory))
            {
                extendedProperties.SetProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH,
                                               new ArrayList(new [] {".", templateDirectory}));
            }
            extendedProperties.SetProperty("assembly.resource.loader.cache", cacheTemplate.ToString().ToLower());
            //extendedProperties.AddProperty("file.resource.loader.path", templateDirectory);
            velocityEngine.Init(extendedProperties);
        }


// ReSharper disable UnusedMember.Local
        private NVelocity.Context.IContext CreateNVelocityContext(object context, bool avoidContextPrefixIfPossible)
// ReSharper restore UnusedMember.Local
        {


            Hashtable hashtable=null;
            Type t = context.GetType();

            if (avoidContextPrefixIfPossible)
            {
                if (t.IsAnonymous())
                {
                    Dictionary<string, object> dic = context.GetObjectPropertyValues();
                    context = dic;
                    t = context.GetType();
                }

                if (typeof (IDictionary).IsAssignableFrom(t))
                {
                    hashtable = new Hashtable(((IDictionary) context));
                }
            }

            if (hashtable == null)
            {
                //was not dictionary-able...so has to be embedded within hashtable:
                hashtable = new Hashtable();
                hashtable["Context"] = context;
            }

            NVelocity.Context.IContext result = new VelocityContext(hashtable);
            return result;
        }






        #endregion
    }

}
