namespace XAct.DirectoryServices.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.DirectoryServices.Configuration;
    using XAct.Settings;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IDirectoryContextSettingsManagementServiceTests
    {
        private UnitTestConfiguration Config 
        { 
            get { return _config ?? (_config = new UnitTestConfiguration()); }
        }

        private UnitTestConfiguration _config;

        
        bool RunADTests
        {
            get
            {
                if (
                    XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>()
                        .Get<string>("RunADTests")
                        .ToBool())
                {
                    try
                    {
                        string domain = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain().Name;
                        //Can be returning CORP, whereas we want to test with CORP.INT
                        //return domain.Contains(Config.Domain,StringComparison.OrdinalIgnoreCase);
                        return true;
                    }
                    catch
                    {
                        //will throw an exception if not hooked up.
                    }
                }
                return false;
            }
        }

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetIDirectoryServiceContextConfigurationFromService()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

           IDirectoryServiceContextConfiguration directoryServiceContextSettings =
               directoryContextSettingsManagementService.Current;

            
            Assert.IsTrue(directoryServiceContextSettings is IActiveDirectoryServiceContextConfiguration);
        }




        //TEST:FAILING
        [Test]
        public void CanSetIDirectoryServiceContextConfigurationDomainProperty()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;

            directoryServiceContextSettings.Domain = Config.Domain;

            IDirectoryServiceContextConfiguration directoryServiceContextSettings2 =
                directoryContextSettingsManagementService.Current;

            Assert.IsTrue(object.Equals(Config.Domain, directoryServiceContextSettings2.Domain));
        }


        [Test]
        public void CanGetIActiveDirectoryPrincipalContextManagerService()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            
            IActiveDirectoryPrincipalContextManagerService activeDirectoryPrincipalContextManagerService =
                XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryPrincipalContextManagerService>();


            Assert.IsTrue(activeDirectoryPrincipalContextManagerService!=null);
        }



        [Test]
        public void UnitTest05()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;

            directoryServiceContextSettings.Domain = Config.Domain;
            directoryServiceContextSettings.DefaultOU = "DC=" + Config.Domain + "," + "DC=local";
            directoryServiceContextSettings.ServiceUser = Config.Domain + "\\" + Config.UserName;
            directoryServiceContextSettings.ServicePassword = Config.Password;

            IDirectoryServiceContextConfiguration directoryServiceContextSettings2 =
                directoryContextSettingsManagementService.Current;




            Assert.IsTrue(object.Equals(Config.Domain, directoryServiceContextSettings2.Domain));
        }

    }


}


