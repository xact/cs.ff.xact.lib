﻿
namespace XAct.DirectoryServices.Tests
{
    using XAct.Settings;

    public class UnitTestConfiguration
    {
        public string Domain { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string OU1 { get; set; }

        public string GRP0 { get; set; }
        public string GRP1 { get; set; }
        public string GRP2 { get; set; }
        public string GRP3 { get; set; }

        public UnitTestConfiguration()
        {
            IHostSettingsService  hostSettingsService =
                XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();

            //eg: CORP,SkyS,Pwd
            string[] stuff = hostSettingsService.Get<string>("AppDomainTestDataLogin").Split(new []{';',',','|'});
            
            if (stuff.Length >= 3)
            {
                Domain = stuff[0];
                UserName = stuff[1];
                Password = stuff[2];
                
            }
            string[] stuff2 = hostSettingsService.Get<string>("AppDomainTestDataOUs").Split(new[] { ';', ',', '|' });
            if (stuff2.Length >= 1)
            {
                OU1 = stuff2[0];
            }

            string[] stuff3 = hostSettingsService.Get<string>("AppDomainTestDataGroups").Split(new[] { ';', ',', '|' });
            if (stuff3.Length >= 4)
            {
                GRP0 = stuff3[0];
                GRP1 = stuff3[1]; 
                GRP2 = stuff3[2];
                GRP3 = stuff3[3];
            }
        }
    }
}
