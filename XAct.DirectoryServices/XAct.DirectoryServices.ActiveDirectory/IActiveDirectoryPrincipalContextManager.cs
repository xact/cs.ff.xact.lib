﻿namespace XAct.DirectoryServices
{
    using System;
    using System.DirectoryServices.AccountManagement;
    using XAct.Diagnostics;
    using XAct.DirectoryServices.Configuration;
    using XAct.Services;
    using XAct.State;


    /// <summary>
    /// Implementation of <see cref="IActiveDirectoryPrincipalContextManagerService"/>
    /// to provide a service to manage ActiveDirectory 
    /// <see cref="PrincipalContext"/> instances on a per thread basis.
    /// </summary>
    public class ActiveDirectoryPrincipalContextManagerService : 
            WebThreadSpecificStackableContextManagementServiceBase<PrincipalContext>, IActiveDirectoryPrincipalContextManagerService
    {
        private readonly IDirectoryContextSettingsManagementService _directoryContextSettingsManagementService;


        /// <summary>
        /// Gets the current thread specific settings used to create new Connections/Contexts.
        /// </summary>
        /// <value>The settings.</value>
        public IDirectoryServiceContextConfiguration Settings
        {
            get
            {
                return _directoryContextSettingsManagementService.Current;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActiveDirectoryPrincipalContextManagerService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="stateService">The state service.</param>
        /// <param name="directoryContextSettingsManagementService"> </param>
        public ActiveDirectoryPrincipalContextManagerService(ITracingService tracingService, IContextStateService stateService,IDirectoryContextSettingsManagementService directoryContextSettingsManagementService) 
            : base(tracingService, stateService)
        {
            _directoryContextSettingsManagementService = directoryContextSettingsManagementService;

            base.IsMutable = false;
        }




        /// <summary>
        /// Invoked by <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.Create"/> to creates the source instance.
        /// Before <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.Create"/> calls <see cref="CloneSourceInstanceValuesToNewThreadInstance"/>
        /// if <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.IsMutable"/>=<c>true</c>.
        /// </summary>
        /// <returns></returns>
        protected override PrincipalContext CreateNewSourceInstance()
        {
            //return base.CreateSourceInstance();

            //Get current settings...should probably be same as they always have:
            //Ensure they are AD specific:
            IDirectoryServiceContextConfiguration baseSettings = 
                _directoryContextSettingsManagementService.Current;

            IActiveDirectoryServiceContextConfiguration settings = 
                baseSettings as IActiveDirectoryServiceContextConfiguration;

            ContextType contextType;

            if (settings != null)
            {
                contextType = settings.ContextType;
            }
            else
            {

            try
                {
                    contextType = (ContextType)Enum.Parse(typeof(ContextType), baseSettings.ContextTypeName);
                }
                catch
                {
                    throw new Exception("Could not convert IDirectoryServiceContextSettings to IActiveDirectoryServiceContextSettings and IDirectoryServiceContextSettings.contextType is unparsable to AD's ContentType.");
                }

            }



            PrincipalContext principalContext 
                    = new PrincipalContext(contextType, settings.Domain,settings.DefaultOU, settings.ServiceUser, settings.ServicePassword);

            return principalContext;

        }


        /// <summary>
        /// Clones the source instance values to the new thread specific instance.
        /// </summary>
        /// <param name="srcContext">The SRC context.</param>
        /// <returns></returns>
        protected override PrincipalContext CloneSourceInstanceValuesToNewThreadInstance(PrincipalContext srcContext)
        {
            //Should have a need to do this as never will get hit with with IsMutable=false;
            return srcContext;
        }

    }
}
