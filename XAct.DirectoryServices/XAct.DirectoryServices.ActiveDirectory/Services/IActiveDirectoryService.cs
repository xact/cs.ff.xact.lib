
// ReSharper disable CheckNamespace
namespace XAct.DirectoryServices
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.DirectoryServices.AccountManagement;

    /// <summary>
    /// Contract for a service to manage AD Group and User Principals.
    /// </summary>
    public interface IActiveDirectoryService : IHasXActLibService
    {


        /// <summary>
        /// Gets the current <see cref="IDirectoryServiceContextConfiguration"/>
        /// used to create the <see cref="GetCurrentContext"/>
        /// </summary>
        /// <value>The current context settings.</value>
        IDirectoryServiceContextConfiguration CurrentContextSettings { get; }

        /// <summary>
        /// Gets the current <see cref="PrincipalContext"/>.
        /// </summary>
        /// <value>The current context.</value>
        PrincipalContext GetCurrentContext(string ou = null);


        /// <summary>
        /// Checks if the User Account is Expired
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true if Expired</returns>
        bool IsUserExpired(string userName);

        /// <summary>
        /// Checks if user exists on AD
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true if username Exists</returns>
        bool IsUserExisting(string userName);

        /// <summary>
        /// Gets a certain user on Active Directory
        /// <para>
        /// Compares against all of  SamAccountName, Name, UserPrincipalName, DistinguishedName, Sid, Guid
        /// </para>
        /// 	<para>
        /// Therefore it is possible to match more than one -- causing an exception.
        /// </para>
        /// </summary>
        /// <param name="userName">The username to get</param>
        /// <param name="ou">The ou.</param>
        /// <returns>Returns the UserPrincipal Object</returns>
        UserPrincipal GetUser(string userName, string ou = null);


        /// <summary>
        /// Searches for users that match the given filter.
        /// <example>
        /// <code>
        /// <![CDATA[
        /// //Search from base:
        /// var result = SearchForUsers(filter);
        /// //Search from within an OU
        /// var result = SearchForUsers(filter,"OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local");
        /// ]]>
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="userPrincipalAsFilter">The user principal as filter.</param>
        /// <param name="ou">The optional container ou.</param>
        /// <returns></returns>
        UserPrincipal[] SearchForUsers(UserPrincipal userPrincipalAsFilter, string ou=null);


        /// <summary>
        /// Checks if user account is locked
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true of Account is locked</returns>
        bool IsUserLocked(string userName);

        /// <summary>
        /// Force expire password of a user
        /// </summary>
        /// <param name="userName">The username to expire the password</param>
        void ExpireUserPassword(string userName);

        /// <summary>
        /// Enables a disabled user account
        /// </summary>
        /// <param name="userName">The username to enable</param>
        void EnableUserAccount(string userName);

        /// <summary>
        /// Force disabling of a user account
        /// </summary>
        /// <param name="userName">The username to disable</param>
        void DisableUserAccount(string userName);

        /// <summary>
        /// Changes the user's password question and answer.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="newChallengeQuestion">The new challenge question.</param>
        /// <param name="newChallengeAnswer">The new challenge answer.</param>
        /// <internal>5/13/2011: Sky</internal>
        void ChangeUserChallengeQuestionAndAnswer(string userName, string newChallengeQuestion,
                                                  string newChallengeAnswer);

        /// <summary>
        /// Resets password to an automatically generated password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="answerToChallengeQuestion"></param>
        string ResetUserPassword(string userName, string answerToChallengeQuestion);

        /// <summary>
        /// Changes the user's password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <internal>5/13/2011: Sky</internal>
        void ChangeUserPassword(string userName, string oldPassword, string newPassword);

        /// <summary>
        /// Validates that the member/user's name/pwd combination is in the datastore.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <internal>5/13/2011: Sky</internal>
        bool ValidateUser(string userName, string password);

        /// <summary>
        /// Unlocks a the member/user who has locked up their account
        /// (probably by entering too many wrong passwords
        /// within a given time).
        /// </summary>
        /// <param name="userName"></param>
        void UnlockUser(string userName);



        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userPassword">The user password.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="ou">The OU in which to create the user.</param>
        void CreateUser(string userName, string userPassword, string emailAddress, bool enabled, string ou = null);

        /// <summary>
        /// Deletes the specified member/user.
        /// </summary>
        /// <param name="userName">Name of the member/user.</param>
        /// <param name="ou">The ou.</param>
        /// <internal>5/13/2011: Sky</internal>
        void DeleteUser(string userName, string ou = null);



        /// <summary>
        /// Gets a list of the users group memberships
        /// </summary>
        /// <param name="userName">The user you want to get the group memberships</param>
        /// <param name="ou">The ou.</param>
        /// <returns>
        /// Returns an arraylist of group memberships
        /// </returns>
        GroupPrincipal[] GetUserGroups(string userName, string ou = null);


        /// <summary>
        /// Gets a list of the users authorization groups
        /// </summary>
        /// <param name="userName">The user you want to get authorization groups</param>
        /// <param name="ou">The ou.</param>
        /// <returns>
        /// Returns an arraylist of group authorization memberships
        /// </returns>
        GroupPrincipal[] GetUserAuthorizationGroups(string userName, string ou = null);




        /// <summary>
        /// Gets the specified <see cref="GroupPrincipal"/>.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="ou">The OU in which to create the Group.</param>
        /// <returns></returns>
        GroupPrincipal GetGroup(string groupName, string ou=null);



        /// <summary>
        /// Creates a new group in Active Directory
        /// </summary>
        /// <param name="groupName">The name of the new group</param>
        /// <param name="groupDescription">The description of the new group</param>
        /// <param name="groupScope">The scope of the new group</param>
        /// <param name="isSecurityGroup">True is you want this group
        /// to be a security group, false if you want this as a distribution group</param>
        /// <param name="ou">The OU in which to create the Group.</param>
        /// <returns>Returns the GroupPrincipal object</returns>
        GroupPrincipal CreateNewGroup(string groupName,
                                             string groupDescription, GroupScope groupScope, bool isSecurityGroup,
                                             string ou = null);



        /// <summary>
        /// Searches for groups that match the given filter.
        /// <example>
        /// <code>
        /// <![CDATA[
        /// //Search from base:
        /// var result = SearchForGroups(filter);
        /// //Search from within an OU
        /// var result = SearchForGroups(filter,"OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local");
        /// ]]>
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="groupPrincipalAsFilter">The group principal as filter.</param>
        /// <param name="ou">The optional container ou.</param>
        /// <returns></returns>
        GroupPrincipal[] SearchForGroups(GroupPrincipal groupPrincipalAsFilter, string ou=null);

        /// <summary>
        /// Gets group, with a colleciton of <see cref="UserPrincipalEx"/> (which is more descriptive
        /// than what you would get using group.Members).
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="ou">The ou.</param>
        /// <returns></returns>
        KeyValuePair<GroupPrincipal, UserPrincipalEx[]> GetGroupEx(string groupName, string ou = null);


        /// <summary>
        /// Checks if user is a member of a given group
        /// </summary>
        /// <param name="userName">The user you want to validate</param>
        /// <param name="groupName">The group you want to check the 
        /// membership of the user</param>
        /// <returns>Returns true if user is a group member</returns>
        bool IsUserGroupMember(string userName, string groupName);



        /// <summary>
        /// Adds the user for a given group
        /// </summary>
        /// <param name="userName">The user you want to add to a group</param>
        /// <param name="groupName">The group you want the user to be added in</param>
        /// <returns>Returns true if successful</returns>
        void AddUserToGroup(string userName, string groupName);


        /// <summary>
        /// Adds the user for a given group
        /// </summary>
        /// <param name="userName">The user you want to add to a group</param>
        /// <param name="groupName">The group you want the user to be added in</param>
        /// <returns>Returns true if successful</returns>
        void RemoveUserFromGroup(string userName, string groupName);



        /// <summary>
        /// Gets a dictionary of Groups to <see cref="UserPrincipalEx"/>
        /// which has a few more properties than the normal <see cref="UserPrincipal"/>.
        /// </summary>
        /// <param name="groupFilter">The group filter.</param>
        /// <param name="excludeGroupFilter">The exclude group filter.</param>
        /// <param name="ou">The optional container ou.</param>
        /// <returns></returns>
        Dictionary<GroupPrincipal, UserPrincipalEx[]> GetUserPrincipalExByGroup(string groupFilter, string excludeGroupFilter=null, string ou = null);

    }

}