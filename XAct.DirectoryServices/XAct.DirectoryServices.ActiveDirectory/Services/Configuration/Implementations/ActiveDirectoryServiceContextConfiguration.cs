﻿namespace XAct.DirectoryServices.Configuration.Implementations
{
    using System;
    using System.DirectoryServices.AccountManagement;
    using XAct.DirectoryServices.Implementations;
    using XAct.Services;

    /// <summary>
    /// An AD specific specialization of <see cref="DirectoryServiceContextConfiguration"/>
    /// </summary>
    /// <internal>
    /// </internal>
    [DefaultBindingImplementation(typeof(IDirectoryServiceContextConfiguration), BindingLifetimeType.SingletonScope, Priority.Low /*OK:SecondaryBinding*/)]
    public class ActiveDirectoryServiceContextConfiguration : DirectoryServiceContextConfiguration, IActiveDirectoryServiceContextConfiguration 
    {

        /// <summary>
        /// A directory system specific descriptor of the Domain Type (local machine, or corp AD)
        /// <para>
        /// For Ad, this is the String representation of a
        /// System.DirectoryServices.AccountManagement.ContextType value.
        /// (Machine=0,Domain=1,ApplicationDomain=2)
        /// </para>
        /// </summary>
        /// <value></value>
        public override string ContextTypeName
        {
            get
            {
                return ContextType.ToString();
            }
            set
            {
                this.ContextType = (ContextType) Enum.Parse(typeof(ContextType), value);
            }
        }

        /// <summary>
        /// The type of Domain we are connecting to (local machine, or corp AD)
        /// </summary>
        /// <internal>
        /// ContextType is a AD specific property that I don't want dragged down 
        /// into XAct.DirectoryServices
        /// </internal>
        public ContextType ContextType { get; set; }


        ///// <summary>
        ///// Initializes a new instance of the <see cref="ActiveDirectoryServiceContextSettings"/> class.
        ///// </summary>
        //public ActiveDirectoryServiceContextSettings():base()
        //{
            
        //}


        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public override object Clone()
        {
            IActiveDirectoryServiceContextConfiguration result = (IActiveDirectoryServiceContextConfiguration) base.Clone() ;

            //Set the one setting not available lower down:
            result.ContextType = this.ContextType;

            return result;
        }
    }
}
