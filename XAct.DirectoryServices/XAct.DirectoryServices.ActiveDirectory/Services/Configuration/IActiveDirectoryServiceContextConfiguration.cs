
namespace XAct.DirectoryServices.Configuration
{
    using System.DirectoryServices.AccountManagement;

    /// <summary>
    /// An AD specific specialization of <see cref="IDirectoryServiceContextConfiguration"/>
    /// </summary>
    public interface IActiveDirectoryServiceContextConfiguration : IDirectoryServiceContextConfiguration
    {

        /// <summary>
        /// The type of Domain we are connecting to (local machine, or corp AD)
        /// <para>
        /// System.DirectoryServices.AccountManagement.ContextType value
        /// (Machine=0,Domain=1,ApplicationDomain=2)
        /// </para>
        /// </summary>
        /// <internal>
        /// ContextType is a AD specific property that I don't want dragged down 
        /// into XAct.DirectoryServices
        /// </internal>
        ContextType ContextType { get; set;}
        
    }
}