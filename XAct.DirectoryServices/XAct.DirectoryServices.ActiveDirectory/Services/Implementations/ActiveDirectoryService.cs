﻿namespace XAct.DirectoryServices.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.DirectoryServices.AccountManagement;
    using System.Web.Security;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Security;
    using XAct.Services;

    /// <summary>
    /// A service to manage AD Group and User Principals.
    /// </summary>
    public class ActiveDirectoryService : IActiveDirectoryService
    {
        private static string _traceName;
        private readonly StrongPasswordGenerator _strongPasswordGenerator;

        private readonly ITracingService _tracingService;
        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;
        private readonly IActiveDirectoryPrincipalContextManagerService _activeDirectoryPrincipalContextManagerService;


        /// <summary>
        /// Retrieves from <see cref="IDirectoryContextSettingsManagementService"/>
        /// the current <see cref="IDirectoryServiceContextConfiguration"/>.
        /// </summary>
        /// <value>The current context settings.</value>
        public IDirectoryServiceContextConfiguration CurrentContextSettings
        {
            get { return _activeDirectoryPrincipalContextManagerService.Settings; }
        }



        /// <summary>
        /// Gets the current <see cref="PrincipalContext"/>.
        /// </summary>
        /// <param name="ou"></param>
        /// <returns></returns>
        /// <value>The current context.</value>
        public PrincipalContext GetCurrentContext(string ou=null)
        {

            PrincipalContext current = _activeDirectoryPrincipalContextManagerService.Current;
         
            //If there is nothing specific mentioned, return default, whatever it is:
            if (string.IsNullOrEmpty(ou))
            {
                return current;
            }

            //Same as last?
            if ( ou == current.Container)
            {
                return current;
            }

            PrincipalContext  result =_activeDirectoryPrincipalContextManagerService.Current;

            //_activeDirectoryPrincipalContextManagerService.Push(result);
            
            return result;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ActiveDirectoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="activeDirectoryPrincipalContextManagerService">The active directory principal context manager service.</param>
        public ActiveDirectoryService(
            ITracingService tracingService,
            IDateTimeService dateTimeService,
            IEnvironmentService environmentService, 
            IActiveDirectoryPrincipalContextManagerService activeDirectoryPrincipalContextManagerService)
        {
            _traceName = this.GetType().Name;
        _strongPasswordGenerator = new StrongPasswordGenerator();

            _tracingService = tracingService;
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
            _activeDirectoryPrincipalContextManagerService = activeDirectoryPrincipalContextManagerService;
        }

        #region User Account Methods
        /// <summary>
        /// Checks if the User Account is Expired
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true if Expired</returns>
        public bool IsUserExpired(string userName)
        {
            using (UserPrincipal userPrincipal = GetUser(userName))
            {
                return ((userPrincipal.AccountExpirationDate == null)
                        ||
                        (userPrincipal.AccountExpirationDate.Value < _dateTimeService.NowUTC));
            }
        }

        /// <summary>
        /// Checks if user exists on AD
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true if username Exists</returns>
        public bool IsUserExisting(string userName)
        {
            try
            {
                bool result = GetUser(userName) != null;

                return result;
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning,e,"{0}.IsUserExisting({1}): an exception occurred. ",_traceName,userName);
                throw;
            }
        }


        /// <summary>
        /// Gets a certain user on Active Directory
        /// <para>
        /// Compares against all of  SamAccountName, Name, UserPrincipalName, DistinguishedName, Sid, Guid
        /// </para>
        /// 	<para>
        /// Therefore it is possible to match more than one -- causing an exception.
        /// </para>
        /// </summary>
        /// <param name="userName">The username to get</param>
        /// <param name="ou">The ou.</param>
        /// <returns>Returns the UserPrincipal Object</returns>
        public UserPrincipal GetUser(string userName, string ou = null)
        {
            try
            {
                UserPrincipal userPrincipal = this.GetCurrentContext(ou).GetUser(userName);

                return userPrincipal;
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, "{0}.GetUser({1}): an exception occurred. ", _traceName, userName);
                throw;
            }
        }


        /// <summary>
        /// Searches for users that match the given filter.
        /// <example>
        /// <code>
        /// <![CDATA[
        /// //Search from base:
        /// var result = SearchForUsers(filter);
        /// //Search from within an OU
        /// var result = SearchForUsers(filter,"OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local");
        /// ]]>
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="userPrincipalAsFilter">The user principal as filter.</param>
        /// <param name="ou">The optional container ou.</param>
        /// <returns></returns>
        public UserPrincipal[] SearchForUsers(UserPrincipal userPrincipalAsFilter, string ou = null)
        {
            PrincipalContext principalContext = this.GetCurrentContext(ou);
            return principalContext.SearchForUsers(userPrincipalAsFilter);
        }



        /// <summary>
        /// Checks if user account is locked
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true of Account is locked</returns>
        public bool IsUserLocked(string userName)
        {
            try
            {
                using (UserPrincipal userPrincipal = GetUser(userName))
                {
                    return userPrincipal.IsAccountLockedOut();
                }
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, "{0}.IsUserLocked({1}): an exception occurred. ",
                                               _traceName, userName);
                throw;
            }
        }


        /// <summary>
        /// Gets a list of the users group memberships
        /// </summary>
        /// <param name="userName">The user you want to get the group memberships</param>
        /// <param name="ou"></param>
        /// <returns>
        /// Returns an arraylist of group memberships
        /// </returns>
        public GroupPrincipal[] GetUserGroups(string userName, string ou = null)
        {
            try
            {
                using (UserPrincipal userPrincipal = this.GetCurrentContext(ou).GetUser(userName))
                {

                    //HACK:Overload is hard to get to as both sigs are same:
                    GroupPrincipal[] result = UserPrincipalExtensions.GetGroups(userPrincipal);

                    return result;
                }
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, "{0}.GetUserGroups({1}): an exception occurred. ",
                                               _traceName, userName);
                throw;
            }

        }

        /// <summary>
        /// Gets a list of the users authorization groups
        /// </summary>
        /// <param name="userName">The user you want to get authorization groups</param>
        /// <param name="ou">The ou.</param>
        /// <returns>
        /// Returns an arraylist of group authorization memberships
        /// </returns>
        public GroupPrincipal[] GetUserAuthorizationGroups(string userName, string ou = null)
        {
            try {
            UserPrincipal userPrincipal = this.GetCurrentContext(ou).GetUser(userName);
            //HACK:Overload is hard to get to as both sigs are same:
            return UserPrincipalExtensions.GetAuthorizationGroups(userPrincipal);
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, "{0}.GetUserAuthorizationGroups({1}): an exception occurred. ", _traceName, userName);
                throw;
            }
        }




        /// <summary>
        /// Enables a disabled user account
        /// </summary>
        /// <param name="userName">The username to enable</param>
        public void EnableUserAccount(string userName)
        {
            try {
            this.GetCurrentContext().EnableUserAccount(userName);
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, "{0}.EnableUserAccount({1}): an exception occurred. ", _traceName, userName);
                throw;
            }
        }

        /// <summary>
        /// Force disabling of a user account
        /// </summary>
        /// <param name="userName">The username to disable</param>
        public void DisableUserAccount(string userName)
        {
            try {
            this.GetCurrentContext().DisableUserAccount(userName);
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, "{0}.DisableUserAccount({1}): an exception occurred. ", _traceName, userName);
                throw;
            }
        }

        /// <summary>
        /// Force expire password of a user
        /// </summary>
        /// <param name="userName">The username to expire the password</param>
        public void ExpireUserPassword(string userName)
        {
            try {
            this.GetCurrentContext().ExpireUserPassword(userName);
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, "{0}.ExpireUserPassword({1}): an exception occurred. ", _traceName, userName);
                throw;
            }
        }




        /// <summary>
        /// Changes the user's password question and answer.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="newChallengeQuestion">The new challenge question.</param>
        /// <param name="newChallengeAnswer">The new challenge answer.</param>
        /// <internal>5/13/2011: Sky</internal>
        public void ChangeUserChallengeQuestionAndAnswer(string userName, string newChallengeQuestion, string newChallengeAnswer)
        {

            throw new NotImplementedException();
        }

        /// <summary>
        /// Resets password to an automatically generated password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="answerToChallengeQuestion"></param>
        /// <internal>
        /// Should invoke <see cref="GeneratePassword"/>.
        ///   </internal>
        public string ResetUserPassword(string userName, string answerToChallengeQuestion)
        {
            try
            {
               UserPrincipal userPrincipal= this.GetCurrentContext().GetUser(userName);
                string generatedPassword = GeneratePassword();
                userPrincipal.ChangePassword(userName, generatedPassword);

            _tracingService.Trace(TraceLevel.Info, "User {0} pasword reset.", userName);
                return generatedPassword;
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, "{0}.ResetUserPassword({1}): an exception occurred. ", _traceName, userName);
                throw;
            }

        }

        /// <summary>
        /// Changes the user's password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <internal>5/13/2011: Sky</internal>
        public void ChangeUserPassword(string userName, string oldPassword, string newPassword)
        {
            try
            {

                bool result = this.GetCurrentContext().ChangePassword(userName, oldPassword, newPassword);

                if (!result)
                {
                    throw new Exception("Old password was not correct.");
                }
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.ChangeUserPassword({1}): an exception occurred. ", _traceName,
                                               userName);
                throw;
            }
        }

        /// <summary>
        /// Validates that the member/user's name/pwd combination is in the datastore.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <internal>5/13/2011: Sky</internal>
        public bool ValidateUser(string userName, string password)
        {
            try {
            bool result = this.GetCurrentContext().ValidateCredentials(userName, password);

            _tracingService.Trace(TraceLevel.Verbose, "User {0} IsValidate:{1}", userName, result);

            return result;
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.ValidateUser({1}): an exception occurred. ", _traceName,
                                               userName);
                throw;
            }
        }


        /// <summary>
        /// Unlocks a the member/user who has locked up their account
        /// (probably by entering too many wrong passwords
        /// within a given time).
        /// </summary>
        /// <param name="userName"></param>
        public void UnlockUser(string userName)
        {
            try {
            this.GetCurrentContext().EnableUserAccount(userName);

            _tracingService.Trace(TraceLevel.Info, "User {0} unlocked.", userName);
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.UnlockUser({1}): an exception occurred. ", _traceName,
                                               userName);
                throw;
            }
        }



        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userPassword">The user password.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="ou">The OU.</param>
        public void CreateUser(string userName, string userPassword, string emailAddress, bool enabled, string ou=null)
        {
            try {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.CreateUser({1},)", _traceName, userName);

            PrincipalContext principalContext = this.GetCurrentContext(ou);

            principalContext.CreateUser(userName, userPassword, emailAddress, enabled /*Enabled or not*/);

            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.CreatekUser({1}): an exception occurred. ", _traceName,
                                               userName);
                throw;
            }
        }

        /// <summary>
        /// Deletes the specified member/user.
        /// </summary>
        /// <param name="userName">Name of the member/user.</param>
        /// <param name="ou">The ou.</param>
        /// <internal>5/13/2011: Sky</internal>
        public void DeleteUser(string userName, string ou = null)
        {
            try {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.DeleteUser({1})",_traceName, userName);
            this.GetCurrentContext(ou).DeleteUser(userName);

            _tracingService.Trace(TraceLevel.Info, "User {0} deleted.", userName);
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.DeleteUser({1}): an exception occurred. ", _traceName,
                                               userName);
                throw;
            }

        }
        #endregion

        #region Group Methods

        /// <summary>
        /// Gets a group on Active Directory
        /// </summary>
        /// <param name="groupName">The group to get</param>
        /// <param name="ou">The OU in which to create the Group.</param>
        /// <returns>Returns the GroupPrincipal Object</returns>
        public GroupPrincipal GetGroup(string groupName,string ou=null)
        {
            try {
            GroupPrincipal groupPricipal = this.GetCurrentContext(ou).GetGroup(groupName);

            return groupPricipal;
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.GetGroup({1}): an exception occurred. ", _traceName,
                                               groupName);
                throw;
            }
        }

        /// <summary>
        /// Gets group, with a colleciton of <see cref="UserPrincipalEx"/> (which is more descriptive
        /// than what you would get using group.Members).
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="ou">The ou.</param>
        /// <returns></returns>
        public KeyValuePair<GroupPrincipal,UserPrincipalEx[]> GetGroupEx(string groupName, string ou=null)
        {
            var result = GetUserPrincipalExByGroup(groupName, ou).FirstOrDefaultEx();
            return result;
        }


        /// <summary>
        /// Searches for groups that match the given filter.
        /// <para>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// //Search from base:
        /// var result = SearchForGroups(filter);
        /// //Search from within an OU
        /// var result = SearchForGroups(filter,"OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local");
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// </summary>
        /// <param name="groupPrincipalAsFilter">The group principal as filter.</param>
        /// <param name="ou">The optional container ou.</param>
        /// <returns></returns>
        public GroupPrincipal[] SearchForGroups(GroupPrincipal groupPrincipalAsFilter, string ou =null)
        {
            PrincipalContext principalContext = this.GetCurrentContext(ou);
            return principalContext.SearchForGroups(groupPrincipalAsFilter);
        }


        /// <summary>
        /// Creates a new group in Active Directory
        /// </summary>
        /// <param name="groupName">The name of the new group</param>
        /// <param name="groupDescription">The description of the new group</param>
        /// <param name="groupScope">The scope of the new group</param>
        /// <param name="isSecurityGroup">True is you want this group
        /// to be a security group, false if you want this as a distribution group</param>
        /// <param name="ou">The OU.</param>
        /// <returns>Returns the GroupPrincipal object</returns>
        public GroupPrincipal CreateNewGroup(string groupName,
           string groupDescription, GroupScope groupScope, bool isSecurityGroup, string ou=null)
        {
            try {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.CreateNewGroup(Name:{1},Description:{2},Scope:{3},IsSecurityGroup:{4},Container:{5})",
                _traceName,groupName,groupDescription,groupScope,isSecurityGroup,ou);

                PrincipalContext principalContext = this.GetCurrentContext(ou);

            GroupPrincipal groupPrincipal =
                principalContext.CreateGroup(groupName, groupDescription, groupScope, isSecurityGroup);

            _tracingService.Trace(TraceLevel.Info, "Group {0} created.", groupName);

            return groupPrincipal;
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.CreateNewGroup({1}): an exception occurred. ", _traceName,
                                               groupName);
                throw;
            }

        }



        /// <summary>
        /// Checks if user is a member of a given group
        /// </summary>
        /// <param name="userName">The user you want to validate</param>
        /// <param name="groupName">The group you want to check the 
        /// membership of the user</param>
        /// <returns>Returns true if user is a group member</returns>
        public bool IsUserGroupMember(string userName, string groupName)
        {
            try
            {
                UserPrincipal userPrincipal = GetUser(userName);
                //Note that I'm pretty sure it's not hitting the dangerous
                //Members collection...ie, ok performance:
                return userPrincipal.IsMemberOf(groupName);
            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.IsUserGroupMember({1}): an exception occurred. ", _traceName,
                                               groupName);
                throw;
            }

        }



        /// <summary>
        /// Adds the user for a given group
        /// </summary>
        /// <param name="userName">The user you want to add to a group</param>
        /// <param name="groupName">The group you want the user to be added in</param>
        /// <returns>Returns true if successful</returns>
        public void AddUserToGroup(string userName, string groupName)
        {
            try
            {

                GroupPrincipal groupPrincipal = GetGroup(groupName);


                groupPrincipal.AddUser(userName);


            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.AddUserToGroup({1}): an exception occurred. ", _traceName,
                                               groupName);
                throw;
            }
        }

        /// <summary>
        /// Removes user from a given group
        /// </summary>
        /// <param name="userName">The user you want to remove from a group</param>
        /// <param name="groupName">The group you want the user to be removed from</param>
        /// <returns>Returns true if successful</returns>
        public void RemoveUserFromGroup(string userName, string groupName)
        {
            try
            {
                GroupPrincipal groupPrincipal = GetGroup(groupName);

                groupPrincipal.RemoveUser(userName);

            }
            catch (Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e,
                                               "{0}.AddUserToGroup({1}): an exception occurred. ", _traceName,
                                               groupName);
                throw;
            }
        }


        /// <summary>
        /// Gets a dictionary of Groups to <see cref="UserPrincipalEx"/>
        /// which has a few more properties than the normal <see cref="UserPrincipal"/>.
        /// </summary>
        /// <param name="groupFilter">The group filter.</param>
        /// <param name="excludeGroupFilter">The exclude group filter.</param>
        /// <param name="ou">The optional container ou.</param>
        /// <returns></returns>
        public Dictionary<GroupPrincipal, UserPrincipalEx[]> GetUserPrincipalExByGroup(string groupFilter, string excludeGroupFilter = null, string ou = null)
        {

            Dictionary<GroupPrincipal, UserPrincipalEx[]> results2 = new Dictionary<GroupPrincipal, UserPrincipalEx[]>();

            PrincipalContext principalContext = this.GetCurrentContext(ou);

            GroupPrincipal groupPrincipal =  new GroupPrincipal(principalContext);
            groupPrincipal.Name = groupFilter;

            GroupPrincipal[] results = this.SearchForGroups(groupPrincipal);



            //int count = 0;
            foreach (GroupPrincipal groupResult in results)
            {

                if (!excludeGroupFilter.IsNullOrEmpty())
                {
                    if (groupResult.Name.CompareWildcard(excludeGroupFilter,false))
                    {
                        continue;
                    }
                }

                List<UserPrincipalEx> r = new List<UserPrincipalEx>();

                UserPrincipalEx filter2 = new UserPrincipalEx(principalContext);
                //filter2.MemberOf = groupResult.DistinguishedName;

                filter2.AdvancedSearchFilter.MemberOf(groupResult);

                PrincipalSearcher principalSearcher2 = new PrincipalSearcher();
                principalSearcher2.QueryFilter = filter2;

                UserPrincipalEx[] pr = groupResult.GetMemberUserPrincipalEx();

                foreach (Principal p in pr)
                {
                    UserPrincipalEx up = p as UserPrincipalEx;
                    r.Add(up);
                }
                //count += g.Members.Count
                results2.Add(groupResult, r.ToArray());
            }

            return results2;

        }

        #endregion


        #region Private 
        /// <summary>
        /// Generates the new password.
        /// </summary>
        /// <returns></returns>
        /// <internal>5/13/2011: Sky</internal>
        public string GeneratePassword()
        {
            //MembershipProvider is defined in System.Web.Security,
            //and of no use in client apps.
            MembershipProvider membershipProvider = Membership.Provider;

            return _strongPasswordGenerator.GeneratePassword(
                (membershipProvider != null) ? membershipProvider.MinRequiredPasswordLength : 6,
                Math.Max((membershipProvider != null) ? membershipProvider.MinRequiredPasswordLength : 8, 8),
                (((membershipProvider != null) ? membershipProvider.MinRequiredNonAlphanumericCharacters : 1) > 0),
                null);

            //_MembershipProvider.MinRequiredNonAlphanumericCharacters;
            //_MembershipProvider.MinRequiredPasswordLength;
            //_MembershipProvider.PasswordStrengthRegularExpression;        
        }
        #endregion
    }
}
