﻿//using System;
//using System.Collections.Generic;
//using System.DirectoryServices.AccountManagement;
//using System.Linq;
//using System.Text;
//using XAct.Diagnostics;
//using XAct.Environment;

//namespace XAct.DirectoryServices.Implementations
//{
//    public class ActiveDirectoryAuthenticationSelfManagementService :XAct.Security.IAuthenticationSelfManagementService
//    {
//        private bool _initialized;
//        private ActiveDirectoryServicesSettings _activeDirectoryServicesSettings;


//        private readonly ITracingService _tracingService;
//        private readonly IEnvironmentService _environmentService;

//        /// <summary>
//        /// Initializes a new instance of the 
//        /// <see cref="ActiveDirectoryAuthenticationSelfManagementService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        /// <param name="environmentService">The environment service.</param>
//        public ActiveDirectoryAuthenticationSelfManagementService(ITracingService tracingService, IEnvironmentService environmentService)
//        {
//            tracingService.ValidateIsNotDefault("tracingService");
//            environmentService.ValidateIsNotDefault("environmentService");

//            _tracingService = tracingService;
//            _environmentService = environmentService;
//        }

//        /// <summary>
//        /// Initializes a new instance of the 
//        /// <see cref="ActiveDirectoryAuthenticationSelfManagementService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        /// <param name="activeDirectoryServicesSettings">The active directory services settings.</param>
//        public ActiveDirectoryAuthenticationSelfManagementService(ITracingService tracingService, ActiveDirectoryServicesSettings activeDirectoryServicesSettings):this(tracingService,environmentService)
//        {
//            _activeDirectoryServicesSettings = activeDirectoryServicesSettings;
//        }

//        /// <summary>
//        /// Changes the currently logged in user's password.
//        /// </summary>
//        /// <param name="oldPassword">The old password.</param>
//        /// <param name="newPassword">The new password.</param>
//        /// <internal>5/13/2011: Sky</internal>
//        public void ChangePassword(string oldPassword, string newPassword)
//        {

//            if (ValidateMember(userName, oldPassword))
//            {
//                UserPrincipleAction(userName, up => up.SetPassword(oldPassword));
//            }

//        }

//        /// <summary>
//        /// Changes the password question and answer.
//        /// </summary>
//        /// <param name="password">The password.</param>
//        /// <param name="newChallengeQuestion">The new challenge question.</param>
//        /// <param name="newChallengeAnswer">The new challenge answer.</param>
//        /// <internal>5/13/2011: Sky</internal>
//        public void ChangePasswordQuestionAndAnswer(string password, string newChallengeQuestion, string newChallengeAnswer)
//        {
//            throw new NotImplementedException();
//        }

//        #region Helper Methods
        
//        private void VerifyInitialized()
//        {
//            if (!_initialized)
//            {
//                throw new Exception("IAuthenticationManagementService.Initialize() must be invoked before IAuthenticationManagementService's methods can be invoked.");
//            }
//        }


//        /// <summary>
//        /// Gets the principal context
//        /// </summary>
//        /// <returns>
//        /// Returns the PrincipalContext object
//        /// </returns>
//        private PrincipalContext GetPrincipalContext()
//        {
//            var result = GetPrincipalContext(_activeDirectoryServicesSettings.DefaultOU);
//            return result;
//        }

//        /// <summary>
//        /// Gets the principal context on specified OU
//        /// </summary>
//        /// <param name="organisationUnit">The OU you want your Principal Context to run on</param>
//        /// <returns>Returns the PrincipalContext object</returns>
//        private PrincipalContext GetPrincipalContext(string organisationUnit)
//        {

//            var result = GetPrincipalContext(
//                _activeDirectoryServicesSettings.Domain,
//                organisationUnit,
//                _activeDirectoryServicesSettings.ServiceUserName,
//                _activeDirectoryServicesSettings.ServiceUserPassword);
//            return result;
//        }

//        /// <summary>
//        /// Gets the principal context.
//        /// </summary>
//        /// <param name="domainName">Name of the domain.</param>
//        /// <param name="ou">The ou.</param>
//        /// <param name="serviceUser">The service user.</param>
//        /// <param name="serviceUserPassword">The service user password.</param>
//        /// <returns></returns>
//        private PrincipalContext GetPrincipalContext(string domainName, string ou, string serviceUser, string serviceUserPassword)
//        {

//            return new PrincipalContext(
//                   ContextType.Domain,
//                   domainName,
//                   ou,
//                   ContextOptions.SimpleBind,
//                   serviceUser,
//                   serviceUserPassword);
//        }

//        /// <summary>
//        /// Users the principle action.
//        /// </summary>
//        /// <param name="userName">Name of the user.</param>
//        /// <param name="action">The action.</param>
//        private void UserPrincipleAction(string userName, Action<UserPrincipal> action)
//        {
//            VerifyInitialized();

//            using (UserPrincipal userPrincipal = GetUser(userName))
//            {
//                userPrincipal.Action(action);

//                userPrincipal.Save();
//            }
//        }

//        #endregion

//    }
//}
