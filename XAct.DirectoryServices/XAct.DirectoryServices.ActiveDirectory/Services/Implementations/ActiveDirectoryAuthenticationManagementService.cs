namespace XAct.DirectoryServices.Implementations
{
    using System;
    using System.DirectoryServices.AccountManagement;
    using System.Web.Security;
    using XAct.Diagnostics;
    using XAct.Security;
    using XAct.Security.Authentication;
    using XAct.Services;

    /// <summary>
    /// AD based implementation of <see cref="IAuthenticationManagementService"/>.
    /// </summary>
    [DefaultBindingImplementation(typeof(IAuthenticationManagementService), BindingLifetimeType.Undefined, Priority.Low /*OK: No preferrence between technologies */)]
    public class ActiveDirectoryAuthenticationManagementService : IActiveDirectoryAuthenticationManagementService
    {
        //Generate one time so that it's internal Random is generated only once (more random).
        private readonly StrongPasswordGenerator _strongPasswordGenerator = new StrongPasswordGenerator();


        private readonly ITracingService _tracingService;
        private readonly IActiveDirectoryService _activeDirectoryService;
        private readonly IUserMembershipInfoManagementServiceConfiguration _authenticationManagementServiceConfiguration;


        PrincipalContext CurrentContext
        {
            get { return null; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActiveDirectoryAuthenticationManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="activeDirectoryService">The active directory service.</param>
        /// <param name="authenticationManagementServiceConfiguration">The authentication management service configuration.</param>
        public ActiveDirectoryAuthenticationManagementService(ITracingService tracingService,
            IActiveDirectoryService activeDirectoryService,
            IUserMembershipInfoManagementServiceConfiguration authenticationManagementServiceConfiguration
            )
        {
            tracingService.ValidateIsNotDefault("tracingService");

            _tracingService = tracingService;
            _activeDirectoryService = activeDirectoryService;
            _authenticationManagementServiceConfiguration = authenticationManagementServiceConfiguration;
        }





        /// <summary>
        /// Generates the new password.
        /// </summary>
        /// <returns></returns>
        /// <internal>5/13/2011: Sky</internal>
        public string GeneratePassword()
        {
                        //MembershipProvider is defined in System.Web.Security,
            //and of no use in client apps.

            return _strongPasswordGenerator.GeneratePassword(_authenticationManagementServiceConfiguration.MinRequiredPasswordLength,
                Math.Max(_authenticationManagementServiceConfiguration.MinRequiredPasswordLength, 8),
                _authenticationManagementServiceConfiguration.MinRequiredNonAlphanumericCharacters>0?true:false,
                null);

            //_MembershipProvider.MinRequiredNonAlphanumericCharacters;
            //_MembershipProvider.MinRequiredPasswordLength;
            //_MembershipProvider.PasswordStrengthRegularExpression;        
        }

        /// <summary>
        /// Changes the password question and answer.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="newChallengeQuestion">The new challenge question.</param>
        /// <param name="newChallengeAnswer">The new challenge answer.</param>
        /// <internal>5/13/2011: Sky</internal>
        public void ChangeChallengeQuestionAndAnswer(string userName, string newChallengeQuestion, string newChallengeAnswer)
        {
            _activeDirectoryService.ChangeUserChallengeQuestionAndAnswer(userName, newChallengeQuestion,newChallengeAnswer);
        }

        /// <summary>
        /// Resets password to an automatically generated password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="answerToChallengeQuestion"></param>
        /// <internal>
        /// Should invoke <see cref="GeneratePassword"/>.
        ///   </internal>
        public void ResetPassword(string userName, string answerToChallengeQuestion)
        {
            //TODO:Challenge 
            _activeDirectoryService.ResetUserPassword(userName, GeneratePassword());

            _tracingService.Trace(TraceLevel.Info, "User {0} Password Reset.", userName);
        }

        /// <summary>
        /// Changes the user's password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <internal>5/13/2011: Sky</internal>
        public void ChangePassword(string userName, string oldPassword, string newPassword)
        {

            _activeDirectoryService.ChangeUserPassword(userName,oldPassword,newPassword);

            _tracingService.Trace(TraceLevel.Info, "User {0} Password Reset.", userName);
        }

        /// <summary>
        /// Validates that the member/user's name/pwd combination is in the datastore.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <internal>5/13/2011: Sky</internal>
        public bool ValidateMember(string userName, string password)
        {
            bool result = _activeDirectoryService.ValidateUser(userName,password);

            _tracingService.Trace(TraceLevel.Info, "User {0} IsValidate:{1}", userName, result);

            return result;
        }

        /// <summary>
        /// Unlocks a the member/user who has locked up their account
        /// (probably by entering too many wrong passwords
        /// within a given time).
        /// </summary>
        /// <param name="userName"></param>
        public void UnlockMember(string userName)
        {
            _activeDirectoryService.UnlockUser(userName);

            _tracingService.Trace(TraceLevel.Info, "User {0} unlocked.", userName);
        }

        /// <summary>
        /// Creates the member.
        /// </summary>
        /// <typeparam name="TId">The type of the id.</typeparam>
        /// <param name="identityInfo">The identity info.</param>
        public void CreateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
        {
            _activeDirectoryService.CreateUser(identityInfo.Name, identityInfo.Password,identityInfo.EmailAddress, true /*Enabled or not*/);

            _tracingService.Trace(TraceLevel.Info, "User {0} created.", identityInfo.Name);

        }

        /// <summary>
        /// Updates the member.
        /// </summary>
        /// <typeparam name="TId">The type of the id.</typeparam>
        /// <param name="identityInfo">The identity info.</param>
        public void UpdateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
        {

            using (UserPrincipal userPrincipal = this.CurrentContext.GetUser(identityInfo.Name))
            {
                bool changesMade = false;
// ReSharper disable StringCompareIsCultureSpecific.1
                if ((identityInfo.EmailAddress != null) &&(System.String.Compare(userPrincipal.EmailAddress, identityInfo.EmailAddress)==0))
// ReSharper restore StringCompareIsCultureSpecific.1
                {
                    userPrincipal.EmailAddress = identityInfo.EmailAddress;
                    changesMade = true;
                }
// ReSharper disable StringCompareIsCultureSpecific.1
                if ((identityInfo.Name != null) && (string.Compare(userPrincipal.UserPrincipalName, identityInfo.Name) == 0))
// ReSharper restore StringCompareIsCultureSpecific.1
                {
                    userPrincipal.UserPrincipalName = identityInfo.Name;
                    changesMade = true;
                }
                if (changesMade)
                {
                    userPrincipal.Save();
                }
            }
            _tracingService.Trace(TraceLevel.Info, "User {0} updated.", identityInfo.Name);
        }


        /// <summary>
        /// Deletes the specified member/user.
        /// </summary>
        /// <param name="userName">Name of the member/user.</param>
        /// <internal>5/13/2011: Sky</internal>
        public void DeleteMember(string userName)
        {
            _activeDirectoryService.DeleteUser(userName);

            _tracingService.Trace(TraceLevel.Info, "User {0} deleted.", userName);

        }

        



    }
}