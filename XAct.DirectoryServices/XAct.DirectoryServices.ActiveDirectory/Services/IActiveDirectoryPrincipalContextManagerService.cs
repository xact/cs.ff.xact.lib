
namespace XAct.DirectoryServices
{
    using System.DirectoryServices.AccountManagement;

    /// <summary>
    /// Contract for a service to manage ActiveDirectory 
    /// <see cref="PrincipalContext"/> instances on a per thread basis.
    /// </summary>
    public interface IActiveDirectoryPrincipalContextManagerService : IWebThreadSpecificStackableContextManagementServiceBase<PrincipalContext>,  IHasXActLibService
    {
        /// <summary>
        /// Gets the current thread specific settings used to create new Connections/Contexts.
        /// </summary>
        /// <value>The settings.</value>
        IDirectoryServiceContextConfiguration Settings { get; }

    }
}