﻿// ReSharper disable CheckNamespace
namespace XAct.DirectoryServices
// ReSharper restore CheckNamespace
{
    using XAct.Security.Authentication;

    /// <summary>
    /// A specialization of <see cref="IAuthenticationManagementService"/>
    /// for managing AD Users (not groups).
    /// </summary>
    public interface IActiveDirectoryAuthenticationManagementService : IAuthenticationManagementService, IHasXActLibService
    {
    }
}