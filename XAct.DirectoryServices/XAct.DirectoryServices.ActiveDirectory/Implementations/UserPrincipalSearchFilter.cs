﻿namespace XAct.DirectoryServices
{
    using System.DirectoryServices.AccountManagement;

    /// <summary>
    /// An Advanced Filter for <see cref="UserPrincipal"/>
    /// </summary>
    /// <internal>
    /// src: http://stackoverflow.com/questions/10042673/getting-members-of-an-ad-group-where-result-type-is-a-custom-derived-userprincip
    /// </internal>
    public class UserPrincipalSearchFilter : AdvancedFilters
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserPrincipalSearchFilter"/> class.
        /// </summary>
        /// <param name="p"></param>
        public UserPrincipalSearchFilter(Principal p) : base(p)
        {
        }

        /// <summary>
        /// Members the of.
        /// </summary>
        /// <param name="group">The group.</param>
        public void MemberOf(GroupPrincipal group)
        {
            this.AdvancedFilterSet("memberof", group.DistinguishedName, typeof (string), MatchType.Equals);
        }
    }
}
