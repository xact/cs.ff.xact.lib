﻿namespace XAct.DirectoryServices
{
    using System.Collections.Generic;
    using System.DirectoryServices.AccountManagement;

    /// <summary>
    /// An extension of the <see cref="UserPrincipal"/>, 
    /// that includes the "Manages" property.
    /// </summary>
    /// <internal>
    /// http://stackoverflow.com/questions/11298811/how-do-i-set-the-manager-attribute-on-the-userprincipal-object-in-active-directo
    /// </internal>
[DirectoryRdnPrefix("CN")]
[DirectoryObjectClass("Person")]
public class UserPrincipalEx : UserPrincipal
{

    private const string DS_PROPERTY_MEMBER = "member";
    private const string DS_PROPERTY_DISPLAY_NAME = "displayName";
    private const string DS_PROPERTY_DN_NAME = "dn";
    private const string DS_PROPERTY_EMAIL = "mail";
    private const string DS_PROPERTY_GIVEN_NAME = "givenName";
    private const string DS_PROPERTY_SN = "sn";
    private const string DS_PROPERTY_MANAGES = "manager";
    private const string DS_PROPERTY_ACCOUNT_NAME = "sAMAccountName";
    private const string DS_PROPERTY_PRINCIPLE_NAME = "userPrincipalName";


    //http://stackoverflow.com/questions/10042673/getting-members-of-an-ad-group-where-result-type-is-a-custom-derived-userprincip
    private UserPrincipalSearchFilter searchFilter;

    /// <summary>
    /// Initializes a new instance of the <see cref="UserPrincipalEx"/> class.
    /// </summary>
    /// <param name="context">The context.</param>
        public UserPrincipalEx(PrincipalContext context):base(context)
        {
            
        }


    /// <summary>
    /// Initializes a new instance of the <see cref="UserPrincipalEx"/> class.
    /// </summary>
    /// <param name="context">The <see cref="T:System.DirectoryServices.AccountManagement.PrincipalContext"/> that specifies the server or domain against which operations are performed.</param>
    /// <param name="samAccountName">The SAM account name for this user principal.</param>
    /// <param name="password">The password for this account.</param>
    /// <param name="enabled">A Boolean value that specifies whether the account is enabled.</param>
        public UserPrincipalEx(PrincipalContext context,
                               string samAccountName,
                               string password,
                               bool enabled)
            : base(context, samAccountName, password, enabled)
        {
            //base.DisplayName
            //base.DistinguishedName
            //base.EmailAddress
            //base.GivenName

            //base.SamAccountName
            //base.UserPrincipalName
            
        }


        /// <summary>
        /// Gets or sets the manager.
        /// </summary>
        /// <value>The manager.</value>
        [DirectoryProperty("manager")]
        public string Manager
        {
            get
            {
                if (ExtensionGet("manager").Length != 1)
                    return string.Empty;

                return (string) ExtensionGet("manager")[0];
            }
            set { ExtensionSet("manager", value); }
        }



        /// <summary>
        /// Gets or sets the member.
        /// </summary>
        /// <value>The member.</value>
        [DirectoryProperty("member")]
        public string[] MemberOf
        {
            get
            {
                List<string> results = new List<string>();
                foreach (object member in ExtensionGet("member"))
                {
                    results.Add(member as string);

                }
                return results.ToArray();
            }
            set { ExtensionSet("manager", value); }
        }





        /// <summary>
        /// (Overloaded) Returns a user principal object that matches the specified identity value.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="identityValue">The identity value.</param>
        /// <returns></returns>
        public new static UserPrincipalEx FindByIdentity(PrincipalContext context, string identityValue)
        {
            return (UserPrincipalEx) FindByIdentityWithType(context, typeof (UserPrincipalEx), identityValue);
        }

        /// <summary>
        /// (Overloaded) Returns a user principal object that matches the specified identity type, and value
        /// . This version of the Overload:System.DirectoryServices.AccountManagement.UserPrincipal.FindByIdentity method determines the format of the identity value.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="identityType">Type of the identity.</param>
        /// <param name="identityValue">The identity value.</param>
        /// <returns></returns>
        public new static UserPrincipalEx FindByIdentity(PrincipalContext context, IdentityType identityType,
                                                         string identityValue)
        {
            return
                (UserPrincipalEx) FindByIdentityWithType(context, typeof (UserPrincipalEx), identityType, identityValue);
        }


        /// <summary>
        /// Returns an <c>System.DirectoryServices.AccountManagement.AdvancedSearchFilter</c> object, 
        /// for use with Query by Example to set read-only properties before passing the object to the <see cref="T:System.DirectoryServices.AccountManagement.PrincipalSearcher"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// An <c>System.DirectoryServices.AccountManagement.AdvancedSearchFilter</c> object.
        /// </returns>
        /// <internal>
        ///     http://stackoverflow.com/questions/10042673/getting-members-of-an-ad-group-where-result-type-is-a-custom-derived-userprincip
        /// </internal>
        public new UserPrincipalSearchFilter AdvancedSearchFilter
        {
            get
            {
                if (null == searchFilter)
                    searchFilter = new UserPrincipalSearchFilter(this);

                return searchFilter;
            }
        }

    }
}
