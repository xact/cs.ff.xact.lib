﻿namespace XAct.DirectoryServices
{
    using System.DirectoryServices.AccountManagement;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// See: http://www.codeproject.com/Articles/118122/How-to-use-AD-Attributes-not-represented-in-UserPr
    /// </internal>
    [DirectoryObjectClass("group")]
    [DirectoryRdnPrefix("CN")]
    public class GroupPrincipalEx : GroupPrincipal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupPrincipalEx"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public GroupPrincipalEx(PrincipalContext context) : base(context)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupPrincipalEx"/> class.
        /// </summary>
        /// <param name="context">The <see cref="T:System.DirectoryServices.AccountManagement.PrincipalContext"/> that specifies the server or domain against which operations are performed.</param>
        /// <param name="samAccountName">The SAM account name for this principal.</param>
        public GroupPrincipalEx(PrincipalContext context, string samAccountName)
            : base(context, samAccountName)
        {
        }

        /// <summary>
        /// Gets or sets the w WW home page.
        /// </summary>
        /// <value>The w WW home page.</value>
        [DirectoryProperty("wWWHomePage")]
        public string wWWHomePage
        {
            get
            {
                if (ExtensionGet("wWWHomePage").Length != 1)
                    return null;

                return (string) ExtensionGet("wWWHomePage")[0];

            }
            set { this.ExtensionSet("wWWHomePage", value); }
        }


        /// <summary>
        /// Gets or sets the email address of the group
        /// </summary>
        /// <value>The email address.</value>
        /// <internal>
        /// Src: http://anyrest.wordpress.com/tag/active-directory/
        /// </internal>
        [DirectoryProperty("mail")]
        public string EmailAddress
        {
            get
            {
                if (ExtensionGet("mail").Length != 1)
                    return null;

                return (string)ExtensionGet("mail")[0];

            }
            set { this.ExtensionSet("mail", value); }
        }
    }
}
