﻿namespace XAct
{
    using System.DirectoryServices.AccountManagement;
    using XAct.DirectoryServices;

    /// <summary>
    /// Misc
    /// </summary>
    public static class MappingExtensions
    {
        /// <summary>
        /// Maps to AD specific User to a portable <see cref="DirectoryUser"/>
        /// to break the dependency on Windows systems.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <returns></returns>
        public static DirectoryUser MapTo(this UserPrincipal  userPrincipal)
        {
            DirectoryUser directoryUser = new DirectoryUser();
            directoryUser.AccountExpirationDate = userPrincipal.AccountExpirationDate;
            directoryUser.Properties["AccountLockoutTime"] =  userPrincipal.AccountLockoutTime;
            directoryUser.Properties["AllowReversiblePasswordEncryption"]  = userPrincipal.AllowReversiblePasswordEncryption;
            directoryUser.Email =  userPrincipal.EmailAddress;

            directoryUser.Properties["IsAccountLockedOut"] = userPrincipal.IsAccountLockedOut();
            //userPrincipal.BadLogonCount
            //userPrincipal.DelegationPermitted
            //userPrincipal.Description 
            //userPrincipal.DisplayName
            //userPrincipal.DistinguishedName
            //userPrincipal.HomeDirectory
            //userPrincipal.GivenName
            //userPrincipal.
            return directoryUser;
        }
    }
}
