﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.DirectoryServices;
    using System.DirectoryServices.AccountManagement;
    using System.Linq;
    using XAct.Diagnostics;

    /// <summary>
    /// Extensions to the <see cref="UserPrincipal"/>
    /// </summary>
    public static class UserPrincipalExtensions
    {
        /// <summary>
        /// Perform the specified Action delegate on the specified AD
        /// <see cref="UserPrincipal"/>.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <param name="action">The action.</param>
        /// <param name="saveImmediately">if set to <c>true</c> [save immediately].</param>
        public static void Action(this UserPrincipal userPrincipal, Action<UserPrincipal> action,
                                  bool saveImmediately = true)
        {
            action.Invoke(userPrincipal);
            if (saveImmediately)
            {
                userPrincipal.Save();
            }
        }


        /// <summary>
        /// Gets the groups this User is a member of.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <returns></returns>
        public static GroupPrincipal[] GetGroups(this UserPrincipal userPrincipal)
        {
            List<GroupPrincipal> results = new List<GroupPrincipal>();

            PrincipalSearchResult<Principal> principalSearchResult = userPrincipal.GetGroups();

            foreach (Principal principal in principalSearchResult)
            {
                GroupPrincipal groupPrincipal = principal as GroupPrincipal;
                results.Add(groupPrincipal);
            }

            return results.ToArray();
        }

        /// <summary>
        /// Gets the names of the groups the user is a member of.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <returns></returns>
        public static string[] GetGroupNames(this UserPrincipal userPrincipal)
        {
            string[] results = userPrincipal.GetGroups().Select(g => g.Name).ToArray();
            return results;
        }



        /// <summary>
        /// Gets the Authorization groups this User is a member of.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <returns></returns>
        public static GroupPrincipal[] GetAuthorizationGroups(this UserPrincipal userPrincipal)
        {
            List<GroupPrincipal> results = new List<GroupPrincipal>();

            PrincipalSearchResult<Principal> principalSearchResult = userPrincipal.GetAuthorizationGroups();

            foreach (Principal principal in principalSearchResult)
            {
                GroupPrincipal groupPrincipal = principal as GroupPrincipal;
                results.Add(groupPrincipal);
            }


            return results.ToArray();
        }

        /// <summary>
        /// Gets the names of the Authorization groups the user is a member of.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <returns></returns>
        public static string[] GetAuthorizationGroupNames(this UserPrincipal userPrincipal)
        {
            string[] results = userPrincipal.GetAuthorizationGroups().Select(g => g.Name).ToArray();

            return results;
        }



        /// <summary>
        /// Determines whether the user is a member of the specified group.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <returns>
        /// 	<c>true</c> if [is member of] [the specified user principal]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsMemberOf(this UserPrincipal userPrincipal, string groupName)
        {
            GroupPrincipal groupPrincipal = userPrincipal.Context.GetGroup(groupName);
            //Note that it's not hitting the dangerous lazyloading Members collection
            //but is probably doing someing like this under the hood:
            //group.Invoke("IsMember", new object[] { deUser.Path }); 
 

            return userPrincipal.IsMemberOf(groupPrincipal);
        }

        /// <summary>
        /// Determines whether the user is a member of the specified group.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <returns>
        /// 	<c>true</c> if [is member of] [the specified user principal]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsMemberOf(this UserPrincipal userPrincipal, GroupPrincipal groupPrincipal)
        {

            if (userPrincipal == null || groupPrincipal == null)
            {
                return false;
            }
            //Members will contain Users and Groups
            return groupPrincipal.Members.Contains(userPrincipal);

        }




        /// <summary>
        /// Sets the password.
        /// </summary>
        /// <param name="userPrincipal">The user principal.</param>
        /// <param name="newPassword">The new password.</param>
        public static void SetPassword(this UserPrincipal userPrincipal, string newPassword)
        {
            try
            {
                userPrincipal.SetPassword(newPassword);
                XAct.Shortcuts.Trace(TraceLevel.Info,"New Password set for {0}",userPrincipal);

            }
            catch (System.Exception e)
            {
                XAct.Shortcuts.TraceException(TraceLevel.Warning, e,
                                              "An exception was raised when attempting to change the password for '{0}'",
                                              userPrincipal);
                throw;
            }
        }



        /// <summary>
        /// Gets the underlying directory entry.
        /// </summary>
        /// <param name="userPrincipal">The group principal.</param>
        /// <returns></returns>
        public static DirectoryEntry GetUnderlyingDirectoryEntry(this UserPrincipal userPrincipal)
        {

            return userPrincipal.GetUnderlyingObject() as DirectoryEntry;
        }

    }

}