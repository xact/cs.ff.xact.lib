﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.DirectoryServices.AccountManagement;
    using System.Linq;
    using XAct.DirectoryServices;

    /// <summary>
    /// Extension methods to the <see cref="PrincipalContext"/>
    /// </summary>
    public static class PrincipalContextExtensions
    {


        #region Group Actions


        /// <summary>
        /// Returns a list of all Groups.
        /// <para>
        /// WARNING: Obviously very dangerous.
        /// </para>
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <returns></returns>
        public static GroupPrincipal[] ListGroups(this PrincipalContext principalContext)
        {

            //Create a query:
            GroupPrincipal groupPrincipal = new GroupPrincipal(principalContext);

            groupPrincipal.Name = "*";

            return principalContext.SearchForGroups(groupPrincipal);
        }

        /// <summary>
        /// Returns a list of all Users.
        /// <para>
        /// WARNING: Obviously very dangerous.
        /// </para>
        /// </summary>
        public static UserPrincipal[] ListUsers(this PrincipalContext principalContext)
        {
            UserPrincipal userPrincipal = new UserPrincipal(principalContext);
            userPrincipal.Name = "*";

            return principalContext.SearchForUsers(userPrincipal);
        }


        /// <summary>
        /// Searches for groups that have properties matching the given <see cref="GroupPrincipal"/>.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="groupPrincipalAsFilter">The group principal as filter.</param>
        /// <returns></returns>
        public static GroupPrincipal[] SearchForGroups(this PrincipalContext principalContext,
                                                       GroupPrincipal groupPrincipalAsFilter)
        {
            List<GroupPrincipal> results = new List<GroupPrincipal>();
            try
            {
                //Note that the group has context already assigned to it:
                PrincipalContext context = groupPrincipalAsFilter.Context;
                Debug.Assert(context == principalContext);

                
                PrincipalSearcher principalSearcher = new PrincipalSearcher();

                //Give the groupPricipal containing properties to match:
                principalSearcher.QueryFilter = groupPrincipalAsFilter;

                PrincipalSearchResult<Principal> principalSearchResult = principalSearcher.FindAll();

                foreach (Principal principal in principalSearchResult)
                {
                    GroupPrincipal groupPrincipal = principal as GroupPrincipal;
                    Debug.Assert(groupPrincipal != null);
                    results.Add(groupPrincipal);
                }
                XAct.Shortcuts.Trace(
                    Diagnostics.TraceLevel.Verbose,  
                    "SearchForGroups: returning {0} groups matching given criteria.", 
                    results.Count);
                return results.ToArray();
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "SearchForGroups: failed returning groups matching given criteria.");
                throw;
            }
        }

        /// <summary>
        /// Gets the group that matches the specified name.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <returns></returns>
        public static GroupPrincipal GetGroup(this PrincipalContext principalContext, string groupName)
        {
            try
            {
                GroupPrincipal groupPrincipal = GroupPrincipal.FindByIdentity(principalContext, groupName);

                XAct.Shortcuts.Trace(
                    Diagnostics.TraceLevel.Verbose ,
                    "GetGroup: group matching given criteria '{0}': {1}", groupName,
                    groupPrincipal == null ? null : groupPrincipal.Name);

                return groupPrincipal;
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "GetGroup: failed returning user matching given criteria.");
                throw;
            }
        }

        /// <summary>
        /// Gets the group that matches the specified name.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="identityType">Type of the identity.</param>
        /// <returns></returns>
        public static GroupPrincipal GetGroup(this PrincipalContext principalContext, string groupName,
                                              IdentityType identityType)
        {
            try
            {
                GroupPrincipal groupPrincipal = GroupPrincipal.FindByIdentity(principalContext, identityType, groupName);

                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Verbose, 
                    "GetGroup: group matching given criteria: {0}",
                                     groupPrincipal == null ? null : groupPrincipal.Name);

                return groupPrincipal;
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "GetUser: failed returning user matching given criteria.");
                throw;
            }
        }


        /// <summary>
        /// Gets the groups with email addresses.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <returns></returns>
        public static GroupPrincipalEx[] GetGroupsWithEmailAddresses(this PrincipalContext principalContext)
        {





            PrincipalSearcher oPrincipalSearcher = new PrincipalSearcher();
            GroupPrincipalEx oGroupPrincipal = new GroupPrincipalEx(principalContext);

            //Get Anything with @ that indicates an email, I'm just lazy you can filter it better
            oGroupPrincipal.EmailAddress = "*@*";

            oPrincipalSearcher.QueryFilter = oGroupPrincipal;
            GroupPrincipalEx[] results = oPrincipalSearcher.FindAll().OfType<GroupPrincipalEx>().ToArray();


            return results;
        }

        /// <summary>
        /// Creates the A group.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="groupName">Name of the new group.</param>
        /// <param name="groupDescription">The new group's description.</param>
        /// <param name="groupScope">The new group's scope (Local, Global, Universal).</param>
        /// <param name="isSecurityGroup">if set to <c>true</c> [is security group].</param>
        /// <returns></returns>
        public static GroupPrincipal CreateGroup(this PrincipalContext principalContext, string groupName,
                                                 string groupDescription, GroupScope groupScope, bool isSecurityGroup)
        {
            try
            {
                GroupPrincipal groupPrincipal = new GroupPrincipal(principalContext, groupName);

                groupPrincipal.Description = groupDescription;
                groupPrincipal.GroupScope = groupScope;
                groupPrincipal.IsSecurityGroup = isSecurityGroup;

                groupPrincipal.Save();

                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Verbose, "GetGroups: group matching given criteria: {0}",
                                     principalContext == null ? null : principalContext.Name);

                return groupPrincipal;
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "CreateGroup: failed returning groups matching given criteria.");
                throw;

            }
        }



        #endregion



        #region User Account Methods

        /// <summary>
        /// Searches for users that have properties matching the given <see cref="UserPrincipal"/>.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userPrincipalAsFilter">The user principal as filter.</param>
        /// <returns></returns>
        public static UserPrincipal[] SearchForUsers(this PrincipalContext principalContext,
                                                     UserPrincipal userPrincipalAsFilter)
        {
            try
            {
                //Note that the group has context already assigned to it:
                PrincipalContext context = userPrincipalAsFilter.Context;
                Debug.Assert(context == principalContext);

                PrincipalSearcher principalSearcher = new PrincipalSearcher();

                //Give the userPricipal containing properties to match:
                principalSearcher.QueryFilter = userPrincipalAsFilter;

                PrincipalSearchResult<Principal> principalSearchResult = principalSearcher.FindAll();

                return principalSearchResult.OfType<UserPrincipal>().ToArray();
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "SearchForUsers: failed returning users matching given criteria.");
                throw;
            }
        }

        /// <summary>
        /// Gets the user that matches the specified name.
        /// <para>
        /// Compares against all of  SamAccountName, Name, UserPrincipalName, DistinguishedName, Sid, Guid
        /// </para>
        /// <para>
        /// Therefore it is possible to match more than one -- causing an exception.
        /// </para>
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">Name of the User.</param>
        /// <returns></returns>
        public static UserPrincipal GetUser(this PrincipalContext principalContext, string userName)
        {
            try
            {
                UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, userName);
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Verbose, "GetUser: user matching given criteria '{0}': {1}", userName,
                                     userPrincipal == null ? null : userPrincipal.Name);
                return userPrincipal;
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "GetUser: failed returning user matching given criteria.");
                throw;
            }


        }


        /// <summary>
        /// Gets the user that matches the specified name.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">Name of the group.</param>
        /// <param name="identityType">Type of the identity property to compare against.</param>
        /// <returns></returns>
        public static UserPrincipal GetUser(this PrincipalContext principalContext, string userName,
                                            IdentityType identityType)
        {
            try
            {
                //return UserPrincipal.FindByIdentity(principalContext, groupName);
                UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, identityType, userName);
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Verbose, "GetUser: user matching given criteria '{0}': {1}", userName,
                                     userPrincipal == null ? null : userPrincipal.Name);
                return userPrincipal;
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "GetUser: failed returning user matching given criteria.");
                throw;
            }

        }





        /// <summary>
        /// Enables a disabled user account
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">The username to enable</param>
        public static void EnableUserAccount(this PrincipalContext principalContext, string userName)
        {
            try
            {
                UserPrincipal userPrincipal = principalContext.GetUser(userName);
                userPrincipal.Action(u => u.Enabled = true);
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User Account  {0} Enabled.", userPrincipal.Name);

            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e, "User Account Enabling failed for '{0}'", userName);
                throw;
            }
        }


        /// <summary>
        /// Force disabling of a user account
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">The username to disable</param>
        public static void DisableUserAccount(this PrincipalContext principalContext, string userName)
        {
            try
            {
                UserPrincipal userPrincipal = principalContext.GetUser(userName);
                userPrincipal.Action(u => u.Enabled = false);
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User Account  {0} Disabled.", userPrincipal.Name);
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e, "User Account Disabling failed for '{0}'", userName);
                throw;
            }

        }

        /// <summary>
        /// Force expiry of user's password
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">The username to expire the password</param>
        public static void ExpireUserPassword(this PrincipalContext principalContext, string userName)
        {
            try
            {
                UserPrincipal userPrincipal = principalContext.GetUser(userName);
                userPrincipal.Action(u => u.ExpirePasswordNow());
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User Account {0} Password set to Expired.", userPrincipal.Name);
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e, "Forcing expiry of user account '{0}' failed.",
                                              userName);
                throw;
            }
        }


        /// <summary>
        /// Changes the user's password.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="newPassword">The new password.</param>
        public static void ChangePassword(this PrincipalContext principalContext, string userName, string newPassword)
        {
            try
            {
                UserPrincipal userPrincipal = principalContext.GetUser(userName);
                userPrincipal.Action(u => u.SetPassword(newPassword));
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User Account {0} Password set.", userPrincipal.Name);
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "Setting new password of user account '{0}' failed.", userName);
                throw;
            }
        }

        /// <summary>
        /// Changes the user's password if the old one is correct.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        public static bool ChangePassword(this PrincipalContext principalContext, string userName, string oldPassword,
                                          string newPassword)
        {
            try
            {
                UserPrincipal userPrincipal = principalContext.GetUser(userName);
                if (!principalContext.ValidateCredentials(userPrincipal.Name, oldPassword))
                {
                    XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info,
                                         "User Account {0} Password not reset (old password was incorrect).",
                                         userPrincipal.Name);
                    return false;
                }
                userPrincipal.Action(u => u.SetPassword(newPassword));
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User Account {0} Password set.", userPrincipal.Name);
                return true;
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e,
                                              "Setting new password of user account '{0}' failed.", userName);
                throw;
            }
        }


        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">The name.</param>
        /// <param name="userPassword">The password.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        public static UserPrincipal CreateUser(this PrincipalContext principalContext, string userName,

                                               string userPassword, string emailAddress = null, bool enabled = true)
        {

            try
            {
                UserPrincipal userPrincipal =
                    new UserPrincipal
                        (principalContext, userName, userPassword, enabled);

                userPrincipal.UserPrincipalName = userName;
                //userPrincipal.GivenName = string.Empty;
                //userPrincipal.Surname = string.Empty;
                if (!string.IsNullOrEmpty(emailAddress))
                {
                    userPrincipal.EmailAddress = emailAddress;
                }

                userPrincipal.Save();

                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User {0} created.", userName);

                return userPrincipal;
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e, "Creating of User '{0}' failed.", userName);
                throw;
            }
        }


        /// <summary>
        /// Deletes the specified user.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">Name of the user.</param>
        public static void DeleteUser(this PrincipalContext principalContext, string userName)
        {
            try
            {
                UserPrincipal userPrincipal = principalContext.GetUser(userName);
                //userPrincipal.Action(u => u.Delete());
                userPrincipal.Delete();
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User Account {0} Deleted.", userName);
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e, "Deleting user account '{0}' failed.", userName);
                throw;
            }

        }

        /// <summary>
        /// Deletes the specified user.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userPrincipal">The user principal.</param>
        public static void DeleteUser(this PrincipalContext principalContext, UserPrincipal userPrincipal)
        {
            try
            {
                userPrincipal.Action(u => u.Delete());
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User Account {0} Deleted.", userPrincipal.Name);
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e, "Deleting user account '{0}' failed.",
                                              userPrincipal.Name);
                throw;
            }

        }



        /// <summary>
        /// Validates the user name/password.
        /// </summary>
        /// <param name="principalContext">The principal context.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userPassword">The user password.</param>
        /// <returns></returns>
        public static bool ValidateUser(this PrincipalContext principalContext, string userName, string userPassword)
        {
            try
            {
                bool result = principalContext.ValidateCredentials(userName, userPassword);
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "User Account {0} IsValid:{1}", userName, result);
                return result;
            }
            catch (Exception e)
            {
                XAct.Shortcuts.TraceException(Diagnostics.TraceLevel.Warning, e, "Validating user account '{0}' failed.", userName);
                throw;
            }
        }



        

        #endregion

    }
}
