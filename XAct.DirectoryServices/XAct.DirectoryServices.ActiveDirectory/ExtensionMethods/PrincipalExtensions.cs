﻿namespace XAct
{
    using System.Collections.Generic;
    using System.DirectoryServices.AccountManagement;
    using System.Linq;

    /// <summary>
    /// Extensions to the <see cref="Principal"/> object.
    /// </summary>
    public static class PrincipalExtensions
    {
        /// <summary>
        /// returns from the given principals only those that are Users.
        /// </summary>
        /// <param name="principals">The principals.</param>
        /// <returns></returns>
        public static IEnumerable<UserPrincipal> OnlyUsers(this IEnumerable<Principal> principals)
        {
            return principals.OfType<UserPrincipal>();
            //return principals.Where(p => { return typeof(UserPrincipal).IsAssignableFrom(p.GetType()); }).OfType<UserPrincipal>();
        }
        /// <summary>
        /// returns from the given principals only those that are Groups.
        /// </summary>
        /// <param name="principals">The principals.</param>
        /// <returns></returns>
        public static IEnumerable<GroupPrincipal> OnlyGroups(this IEnumerable<Principal> principals)
        {
            return principals.OfType<GroupPrincipal>();
            //return principals.Where(p => { return typeof(GroupPrincipal).IsAssignableFrom(p.GetType()); }).OfType<GroupPrincipal>();
        }
    }
}
