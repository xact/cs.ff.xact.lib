﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Diagnostics;
    using System.DirectoryServices;
    using System.DirectoryServices.AccountManagement;
    using System.Linq;
    using XAct.DirectoryServices;

    /// <summary>
    /// Extensions to the <see cref="GroupPrincipal"/> object.
    /// </summary>
    public static class GroupPrincipalExtensions
    {
        /// <summary>
        /// Groups the principle action.
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <param name="action">The action.</param>
        /// <param name="saveImmediately">if set to <c>true</c> [save immediately].</param>
        public static void Action(this GroupPrincipal groupPrincipal, Action<GroupPrincipal> action,
                                  bool saveImmediately = true)
        {
            action.Invoke(groupPrincipal);
            if (saveImmediately)
            {
                groupPrincipal.Save();
            }

        }



        /// <summary>
        /// Determines whether the specified group has a user of the given name.
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <param name="userName">Name of the user.</param>
        /// <returns>
        /// 	<c>true</c> if the specified group principal has user; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasUser(this GroupPrincipal groupPrincipal, string userName)
        {
            //Use the groupPrincipal to get the current Context:
            UserPrincipal userPrincipal = groupPrincipal.Context.GetUser(userName);

            //Uses extension method:
            return groupPrincipal.HasUser(userPrincipal);
        }

        /// <summary>
        /// Determines whether the specified group has the given user
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <param name="userPrincipal">The user principal.</param>
        /// <returns>
        /// 	<c>true</c> if the specified group principal has user; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasUser(this GroupPrincipal groupPrincipal, UserPrincipal userPrincipal)
        {
            userPrincipal.ValidateIsNotDefault("userPrincipal");

            return userPrincipal.IsMemberOf(groupPrincipal);
        }

        /// <summary>
        /// Removes the specified <see cref="UserPrincipal"/>
        /// from the specified <see cref="GroupPrincipal"/>
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <param name="userName">Name of the user.</param>
        public static void AddUser(this GroupPrincipal groupPrincipal, string userName)
        {

            //Use the groupPrincipal to get the current Context:
            UserPrincipal userPrincipal = groupPrincipal.Context.GetUser(userName);

            //Uses extension method:
            groupPrincipal.AddUser(userPrincipal);

        }


        /// <summary>
        /// Adds the specified <see cref="UserPrincipal"/>
        /// to the specified <see cref="GroupPrincipal"/>
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <param name="userPrincipal">The user principal.</param>
        public static void AddUser(this GroupPrincipal groupPrincipal, UserPrincipal userPrincipal)
        {
            userPrincipal.ValidateIsNotDefault("userPrincipal");


            try
            {
                if (groupPrincipal.HasUser(userPrincipal))
                {
                    //Already the case.
                    return;
                }

                //DANGER: http://davybrion.com/blog/2008/05/how-a-simple-foreach-statement-can-waste-an-afternoon/
                //Group is lazy loaded...which can bring back a lot.
                //groupPrincipal.Users.Remove(userPrincipal);
                //groupPrincipal.Save();

                //As per http://stackoverflow.com/questions/1860772/groupprincipal-members-remove-doesnt-work-with-a-large-ad-group
                //the better appraoch is:
                DirectoryEntry directoryEntry = groupPrincipal.GetUnderlyingObject() as DirectoryEntry;
                DirectoryEntry userEntry = userPrincipal.GetUnderlyingObject() as DirectoryEntry;

                Debug.Assert(directoryEntry != null);
                Debug.Assert(userEntry != null);

                //This will execute the operation now:
                directoryEntry.Invoke("Add", new object[] {userEntry.Path});


                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "AD Directory User '{1}' added to group '{0}'.",
                                     groupPrincipal.Name, userPrincipal.Name);
            }
            catch
            {
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Error, "Failed removing AD Directory User '{1}' to group '{0}'.",
                                     groupPrincipal.Name, userPrincipal.Name);
                throw;
            }


        }


        /// <summary>
        /// Removes the specified <see cref="UserPrincipal"/>
        /// from the specified <see cref="GroupPrincipal"/>
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <param name="userName">Name of the user.</param>
        public static void RemoveUser(this GroupPrincipal groupPrincipal, string userName)
        {

            //Use the groupPrincipal to get the current Context:
            UserPrincipal userPrincipal = groupPrincipal.Context.GetUser(userName);

            //Uses extension method:
            groupPrincipal.RemoveUser(userPrincipal);

        }

        /// <summary>
        /// Removes the specified <see cref="UserPrincipal"/>
        /// from the specified <see cref="GroupPrincipal"/>
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <param name="userPrincipal">The user principal.</param>
        public static void RemoveUser(this GroupPrincipal groupPrincipal, UserPrincipal userPrincipal)
        {
            userPrincipal.ValidateIsNotDefault("userPrincipal");


            try
            {
                if (!groupPrincipal.HasUser(userPrincipal))
                {
                    //Already the case.
                    return;
                }

                //DANGER: http://davybrion.com/blog/2008/05/how-a-simple-foreach-statement-can-waste-an-afternoon/
                //Group is lazy loaded...which can bring back a lot.
                //groupPrincipal.Users.Remove(userPrincipal);
                //groupPrincipal.Save();

                //As per http://stackoverflow.com/questions/1860772/groupprincipal-members-remove-doesnt-work-with-a-large-ad-group
                //the better appraoch is:
                DirectoryEntry directoryEntry = groupPrincipal.GetUnderlyingObject() as DirectoryEntry;
                DirectoryEntry userEntry = userPrincipal.GetUnderlyingObject() as DirectoryEntry;


                Debug.Assert(directoryEntry != null);
                Debug.Assert(userEntry != null);

                //This will execute the operation now:
                directoryEntry.Invoke("Remove", new object[] {userEntry.Path});


                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "AD Directory User '{1}' removed from group '{0}'.",
                                     groupPrincipal.Name, userPrincipal.Name);
            }
            catch
            {
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Error, "Failed removing AD Directory User '{1}' from group '{0}'.",
                                     groupPrincipal.Name, userPrincipal.Name);
                throw;
            }


        }


        /// <summary>
        /// Deletes the specified <see cref="GroupPrincipal"/>.
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        public static void Delete(this GroupPrincipal groupPrincipal)
        {

            try
            {
                //Delete the current Group
                groupPrincipal.Delete();
                groupPrincipal.Save();
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Info, "AD Directory Group '{0}' removed.", groupPrincipal.Name);
            }
            catch
            {
                XAct.Shortcuts.Trace(Diagnostics.TraceLevel.Error, "Failed removing AD Directory Group '{0}'.", groupPrincipal.Name);
                throw;
            }
        }


        /// <summary>
        /// Gets the underlying directory entry.
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <returns></returns>
        public static DirectoryEntry GetUnderlyingDirectoryEntry(this GroupPrincipal groupPrincipal)
        {

            return groupPrincipal.GetUnderlyingObject() as DirectoryEntry;
        }

        /// <summary>
        /// Gets UserPrincipal Members. replacement of default Members property in that 
        /// it returns a <see cref="UserPrincipalEx"/> that has more properties.
        /// <para>
        /// Note that <c>GroupPrincipal.GetMembers</c> with recursive off, can return users and groups.
        /// When recursive true, returns only leafnodes (ie users) but recursively.
        /// </para>
        /// </summary>
        /// <param name="groupPrincipal">The group principal.</param>
        /// <returns></returns>
        public static UserPrincipalEx[] GetMemberUserPrincipalEx(this GroupPrincipal groupPrincipal)
        {
            // we need a User that has a more powerfule AdvancedSearchFilter
            UserPrincipalEx userPrincipalEx = new UserPrincipalEx(groupPrincipal.Context);
            userPrincipalEx.AdvancedSearchFilter.MemberOf(groupPrincipal);


            PrincipalSearcher principalSearcher = new PrincipalSearcher();
            principalSearcher.QueryFilter = userPrincipalEx;

            PrincipalSearchResult<Principal> principalSearchResult = principalSearcher.FindAll();
            
            UserPrincipalEx[] results = principalSearchResult.OfType<UserPrincipalEx>().ToArray();

            return results;
        }






    }
}
