// ReSharper disable CheckNamespace
namespace XAct.DirectoryServices
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    ///   An XAct.DirectoryServices class.
    /// </summary>
    public class DirectoryObject : XAct.IHasInnerItemReadOnly
    {
        /// <summary>
        /// Gets or sets the type of the object.
        /// </summary>
        /// <value>The type.</value>
        public DirectoryObjectType Type { get; protected set; }

        /// <summary>
        /// Gets or sets the parent 
        /// <see cref="DirectoryContainerObject"/>
        /// if set.
        /// </summary>
        /// <value>The parent.</value>
        public DirectoryContainerObject Parent { get; set; }


        /// <summary>
        /// Gets or sets the unique directory path for the object.
        /// <para>
        /// Format: CN=Sky Sigal,OU=Domain Users,DC=jqdev,DC=local
        /// </para>
        /// </summary>
        /// <value>The directory path.</value>
        public string DistinguishedName
        {
            get
            {
                return TryGetProperty<string>("DistinguishedName");
            } set { _properties["DistinguishedName"] = value; }
        }

        /// <summary>
        /// Gets a dictionary of the retrieved values for the given object.
        /// </summary>
        /// <value>The values.</value>
        public Dictionary<string, object> Properties { get { return _properties ?? (_properties = new Dictionary<string, object>()); } }
        private Dictionary<string, object> _properties;



        /// <summary>
        /// Tries to get the typed property from within <see cref="Properties"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        protected T TryGetProperty<T>(string tag)
        {
            object r;

            var result =
                this.Properties.TryGetValue(tag, out r)
                       ? r.ConvertTo<T>()
                       : default(T);
            return result;
        }


        /// <summary>
        /// Gets the inner item.
        /// </summary>
        T IHasInnerItemReadOnly.GetInnerItem<T>()
        {
            return _innerItem.ConvertTo<T>(); 
        }

        /// <summary>
        /// 
        /// </summary>
        public object _innerItem;
        
    }
}