namespace XAct.DirectoryServices
{
    /// <summary>
    /// A directory service Group.
    /// </summary>
    public class DirectoryGroup : DirectoryContainerObject
    {
        /// <summary>
        /// Gets or sets the directory's name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return TryGetProperty<string>("Name"); }
            set { this.Properties["Name"] = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryUser"/> class.
        /// </summary>
        public DirectoryGroup ()
        {
            this.Type = DirectoryObjectType.Group;
        }

    }
}