namespace XAct.DirectoryServices
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// 
    /// </summary>
    public abstract class DirectoryContainerObject :DirectoryObject
    {
        /// <summary>
        /// Gets the nested child directory objects (groups or users).
        /// </summary>
        /// <value>The directory objects.</value>
        public Collection<DirectoryObject> Objects { get { return _directoryObjects ?? (_directoryObjects = new Collection<DirectoryObject>()); } }
        private Collection<DirectoryObject> _directoryObjects;
    }
}