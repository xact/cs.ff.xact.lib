﻿namespace XAct.DirectoryServices
{
    using System;
    using System.Text;
    using XAct.Environment;


    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Src: KP
    /// </internal>
    public class DirectoryUser : DirectoryObject
    {


        /// <summary>
        /// Gets or sets the (nullable) account expiration date.
        /// </summary>
        /// <value>The account expiration date.</value>
        public DateTime? AccountExpirationDate
        {
            get { return TryGetProperty<DateTime?>("AccountExpirationDate"); }
            set { this.Properties["AccountExpirationDate"] = value; }
        }


        /// <summary>
        /// Gets the account lockout time.
        /// </summary>
        /// <value>The account lockout time.</value>
        public DateTime? AccountLockoutTime
        {
            get { return TryGetProperty<DateTime?>("AccountLockoutTime"); }
        }

        /// <summary>
        /// Gets a value indicating whether [allow reversible password encryption].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [allow reversible password encryption]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowReversiblePasswordEncryption
        {
            get { return TryGetProperty<bool>("AllowReversiblePasswordEncryption"); }
        }
        /// <summary>
        /// Gets or sets the display name.
        /// <para>
        /// As the DisplayName is often not set in AD, it has to be reconstructed
        /// from First, Last, falling back to the <see cref="SamAccountName"/>.
        /// </para>
        /// </summary>
        /// <value>The display name.</value>
        public string DisplayName
        {
            get
            {

                string displayName;

                //displayName = TryGetProperty<string>("DisplayName");
                //if (!string.IsNullOrEmpty(displayName))
                //{
                //    return displayName;
                //}

                displayName = GetDisplayNameFromLastAndGiven();
                if (!string.IsNullOrEmpty(displayName))
                {
                    return displayName;
                }

                displayName = SamAccountName;
                return displayName;
            }
            set { this.Properties["DisplayName"] = value; }
        }


        /// <summary>
        /// Gets or sets the email address of the user.
        /// </summary>
        /// <value>The email.</value>
        public string Email
        {
            get { return TryGetProperty<string>("Email"); }
            set { this.Properties["Email"] = value; }
        }


        /// <summary>
        /// Gets or sets the first name (ie, GivenName).
        /// <para>
        /// NOTE: This is the GivenName.
        /// </para>
        /// <para>
        /// WARNING: Often empty.
        /// </para>
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName
        {
            get { return TryGetProperty<string>("FirstName"); }
            set { this.Properties["FirstName"] = value; }
        }

        /// <summary>
        /// Gets or sets the name of the user.
        /// <para>
        /// WARNING: The FQName uses the pre-2000 login format of CORP\UserName,
        /// not the longer CORP.local\UserName.
        /// </para>
        /// </summary>
        /// <value>The name of the user.</value>
        public string FQName
        {
            get
            {
                string fqName;

                fqName = TryGetProperty<string>("FQN");

                if (!string.IsNullOrEmpty(fqName))
                {
                    return fqName;
                }


                // The 'short' domain name is not available from the AD query. 
                // The call will be bound against the same AD controller as the one that the search occurs against.
                string shortDomainName = DependencyResolver.Current.GetInstance<IEnvironmentService>().DomainName;

                fqName = string.Format("{0}\\{1}", shortDomainName, SamAccountName);

                if (!string.IsNullOrEmpty(fqName))
                {
                    return fqName;
                }

                
                fqName = GetFromPrincipal();

                if (!string.IsNullOrEmpty(fqName))
                {
                    return fqName;
                }

                fqName = GetFQNFromAccountName();

                return fqName;
            }
            set { this.Properties["FQName"] = value; }
        }





        /// <summary>
        /// Gets or sets the last name 
        /// <para>
        /// NOTE: This is the SurName.
        /// </para>
        /// <para>
        /// WARNING: Often empty.
        /// </para>
        /// </summary>
        /// <value>The first name.</value>
        public string LastName
        {
            get { return TryGetProperty<string>("LastName"); }
            set { this.Properties["LastName"] = value; }
        }

        /// <summary>
        /// Gets or sets the Manager's DistinguishedName (if it exists).
        /// <para>
        /// WARNING: Often null.
        /// </para>
        /// </summary>
        /// <value>The manager path.</value>
        public string ManagerDistinguishedName
        {
            get { return TryGetProperty<string>("ManagerDistinguishedName"); }
            set { this.Properties["ManagerDistinguishedName"] = value; }
        }

        /// <summary>
        /// Gets or sets a list of the SamAccounts this user manages.
        /// </summary>
        /// <value>The manages.</value>
        public string Manages
        {
            get { return TryGetProperty<string>("Manages"); }
            set { this.Properties["Manages"] = value; }
        }

        /// <summary>
        /// Gets or sets the name of the user.
        /// <para>
        /// WARNING: Often null.
        /// </para>
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return TryGetProperty<string>("Name"); }
            set { this.Properties["Name"] = value; }
        }

        /// <summary>
        /// Gets or sets the principalname.
        /// <para>
        /// Format: name@corp.local
        /// </para>
        /// </summary>
        /// <value>The name of the principal.</value>
        public string PrincipalName
        {
            get { return TryGetProperty<string>("PrincipleName"); }
            set { this.Properties["PrincipleName"] = value; }
        }



        /// <summary>
        /// Gets or sets the Sam account name for this entity (this is the username, without prefix or suffix).
        /// </summary>
        /// <value>The name of the sam account.</value>
        public string SamAccountName
        {
            get { return TryGetProperty<string>("SamAccountName"); }
            set { this.Properties["SamAccountName"] = value; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryUser"/> class.
        /// </summary>
        public DirectoryUser()
        {
            this.Type = DirectoryObjectType.User;
        }

        //------------------------------------------
        //FROM ADUser:






        private string GetDisplayNameFromLastAndGiven()
        {
            StringBuilder stringBuilder = new StringBuilder();

            //Last, First

            if (!string.IsNullOrEmpty(LastName))
            {
                stringBuilder.Append(LastName);
            }

            if (!string.IsNullOrEmpty(FirstName))
            {
                if (stringBuilder.Length > 0)
                {
                    stringBuilder.Append(", ");
                }
                stringBuilder.Append(FirstName);
            }

            if (stringBuilder.Length > 0)
            {
                return stringBuilder.ToString();
            }

            return null;
        }

        private string GetFQNFromAccountName()
        {
            string fqName = string.Empty;
            if (!string.IsNullOrEmpty(SamAccountName)
                && !string.IsNullOrEmpty(this.DistinguishedName))
            {
                StringBuilder sb = new StringBuilder();

                string[] pathParts = this.DistinguishedName.Split(new char[] { ',' });
                foreach (string pathPart in pathParts)
                {
                    if (pathPart.StartsWith("DC", StringComparison.InvariantCultureIgnoreCase))
                    {
                        string[] dcParts = pathPart.Split(new char[] { '=' });
                        if (dcParts.Length == 2)
                        {
                            sb.AppendFormat("{0}.", dcParts[1]);
                        }
                    }
                }
                fqName = string.Format("{0}\\{1}", sb.ToString().Substring(0, sb.Length - 1), SamAccountName);
            }
            return fqName;
        }

        private string GetFromPrincipal()
        {
            string fqName = string.Empty;
            if (!string.IsNullOrEmpty(PrincipalName))
            {
                string[] principalParts = PrincipalName.Split(new char[] { '@' });
                if (principalParts.Length == 2)
                {
                    fqName = string.Format("{0}\\{1}", principalParts[1], principalParts[0]);
                }
            }
            return fqName;
        }


        //------------------------------------------














        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return FQName.Equals(obj.ToString());
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return FQName.GetHashCode();
        }





    }
}