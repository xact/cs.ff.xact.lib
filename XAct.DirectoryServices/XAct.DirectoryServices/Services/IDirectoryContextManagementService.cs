﻿namespace XAct.DirectoryServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDirectoryContextSettingsManagementService : IWebThreadSpecificStackableContextManagementServiceBase<IDirectoryServiceContextConfiguration>, IHasXActLibService
    {
    }
}
