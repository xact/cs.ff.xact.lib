namespace XAct.DirectoryServices.Implementations
{
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IDirectoryServiceContextConfiguration"/>
    /// </summary>
    /// <internal>
    /// Priority is low so that if  <c>IActiveDirecoryServiceContextSettings</c>
    /// is in play, that one takes over.
    /// </internal>
    public class DirectoryServiceContextConfiguration : IDirectoryServiceContextConfiguration
    {


        /// <summary>
        /// A directory system specific descriptor of the Domain Type (local machine, or corp AD)
        /// <para>
        /// For Ad, this is the String representation of a
        /// System.DirectoryServices.AccountManagement.ContextType value.
        /// (Machine=0,Domain=1,ApplicationDomain=2)
        /// </para>
        /// </summary>
        /// <value></value>
        public virtual string ContextTypeName { get; set; }


        /// <summary>
        /// Gets or sets the service user.
        /// <para>
        /// Example: "JQDEV\Someone"
        /// </para>
        /// </summary>
        /// <value>The service user.</value>
        public string ServiceUser { get; set; } ////@"ServiceUser";

        /// <summary>
        /// Gets or sets the service user's password.
        /// <para>
        /// Example: "JustKidding!"
        /// </para>
        /// </summary>
        /// <value>The service password.</value>
        public string ServicePassword { get; set; }  //"ServicePassword";


        /// <summary>
        /// Gets or sets the current domain.
        /// <para>
        /// Example: "JQDEV"
        /// </para>
        /// </summary>
        /// <value>The domain.</value>
        public string Domain { get; set; }//= "test.com";

        /// <summary>
        /// Gets or sets the default root OU.
        /// <para>
        /// Example:
        /// <example>
        /// 			<code>
        /// 				<![CDATA[
        /// "DC=mycompany,DC=local"
        /// ]]>
        /// 			</code>
        /// 		</example>
        /// 	</para>
        /// </summary>
        /// <value>The default root OU.</value>
        public string RootOU { get; set; } //"DC=test,DC=com";

        /// <summary>
        /// Gets or sets the default OU for the app/department.
        /// <para>
        /// If not set, defaults to <see cref="RootOU"/>
        /// 	</para>
        /// 	<para>
        /// Example:
        /// <example>
        /// 			<code>
        /// 				<![CDATA[
        /// "OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local"
        /// ]]>
        /// 			</code>
        /// 		</example>
        /// 	</para>
        /// </summary>
        /// <value>The default OU.</value>

        public string DefaultOU
        {
            get { return (!string.IsNullOrEmpty(_defaultOU)) ? _defaultOU : RootOU; }
            set { _defaultOU = value; }
        } //"OU=Test Users,OU=Test,DC=test,DC=com";
        private string _defaultOU;




        /// <summary>
        /// Gets or sets the path to the OU in which to create new Groups
        /// (defaults to <see cref="DefaultOU"/> if not specifically set).
        /// <para>
        /// Example:
        /// <example>
        /// 			<code>
        /// 				<![CDATA[
        /// "OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local"
        /// ]]>
        /// 			</code>
        /// 		</example>
        /// 	</para>
        /// </summary>
        /// <value>The default new group OU.</value>
        public string DefaultNewGroupOU
        {
            get { return (!string.IsNullOrEmpty(_defaultNewGroupOU)) ? _defaultNewGroupOU : DefaultOU; }
            set { _defaultNewGroupOU = value; }
        }
        private string _defaultNewGroupOU;

        /// <summary>
        /// Gets or sets the path to the OU in which to create new Users
        /// (defaults to <see cref="DefaultOU"/> if not specifically set).
        /// <para>
        /// Example:
        /// <example>
        /// 			<code>
        /// 				<![CDATA[
        /// "OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local"
        /// ]]>
        /// 			</code>
        /// 		</example>
        /// 	</para>
        /// </summary>
        /// <value>The default new user OU.</value>
        public string DefaultNewUserOU
        {
            get { return (!string.IsNullOrEmpty(_defaultNewUserOU)) ? _defaultNewUserOU : DefaultOU; }
            set { _defaultNewUserOU = value; }
        }
        private string _defaultNewUserOU;







        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public virtual object Clone()
        {
            //Allows for enheritence:
            IDirectoryServiceContextConfiguration result = (IDirectoryServiceContextConfiguration)System.Activator.CreateInstance(this.GetType());

            result.ContextTypeName = this.ContextTypeName;
            result.Domain = this.Domain;
            result.ServiceUser = this.ServiceUser;
            result.ServicePassword = this.ServicePassword;
            result.RootOU = this.RootOU;
            result.DefaultOU = this.DefaultOU;
            result.DefaultNewGroupOU = this.DefaultNewGroupOU;
            result.DefaultNewUserOU = DefaultNewUserOU;

            return result;
        }

    }
}