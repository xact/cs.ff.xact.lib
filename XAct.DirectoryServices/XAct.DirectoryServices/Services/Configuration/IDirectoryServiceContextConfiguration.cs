namespace XAct.DirectoryServices
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface IDirectoryServiceContextConfiguration : IHasXActLibServiceConfiguration, ICloneable
    {

        /// <summary>
        /// A directory system specific descriptor of the Domain Type (local machine, or corp AD)
        /// <para>
        /// For Ad, this is the String representation of a
        /// System.DirectoryServices.AccountManagement.ContextType value.
        /// (Machine=0,Domain=1,ApplicationDomain=2)
        /// </para>
        /// </summary>
        /// <value></value>
        string ContextTypeName { get; set; }

        /// <summary>
        /// Gets or sets the current domain.
        /// <para>
        /// Example: "JQDEV"
        /// </para>
        /// </summary>
        /// <value>The domain.</value>
        string Domain { get; set; }//= "test.com";

        /// <summary>
        /// Gets or sets the service user.
        /// <para>
        /// Example: "JQDEV\Someone"
        /// </para>
        /// </summary>
        /// <value>The service user.</value>
        string ServiceUser { get; set; } ////@"ServiceUser";

        /// <summary>
        /// Gets or sets the service user's password.
        /// <para>
        /// Example: "JustKidding!"
        /// </para>
        /// </summary>
        /// <value>The service password.</value>
        string ServicePassword { get; set; }  //"ServicePassword";

        /// <summary>
        /// Gets or sets the default root OU.
        /// <para>
        /// Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// "DC=mycompany,DC=local"
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// </summary>
        /// <value>The default root OU.</value>
        string RootOU { get; set; } //"DC=test,DC=com";


        /// <summary>
        /// Gets or sets the default OU for the app/department.
        /// <para>
        /// If not set, defaults to <see cref="RootOU"/>
        /// </para>
        /// <para>
        /// Example:
        /// <![CDATA[
        /// "OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local"
        /// ]]>
        /// </para>
        /// </summary>
        /// <value>The default OU.</value>
        string DefaultOU { get; set; } //"OU=Test Users,OU=Test,DC=test,DC=com";


        /// <summary>
        /// Gets or sets the path to the OU in which to create new Groups
        /// (defaults to <see cref="DefaultOU"/> if not specifically set).
        /// <para>
        /// Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// "OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local"
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// </summary>
        /// <value>The default new group OU.</value>
        string DefaultNewGroupOU { get; set; }

        /// <summary>
        /// Gets or sets the path to the OU in which to create new Users
        /// (defaults to <see cref="DefaultOU"/> if not specifically set).
        /// <para>
        /// Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// "OU=Marketing,OU=Operations,OU=Applications,DC=mycompany,DC=local"
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// </summary>
        /// <value>The default new user OU.</value>
        string DefaultNewUserOU { get; set; }
    }
}