namespace XAct.DirectoryServices.Implementations
{
    using XAct.Diagnostics;
    using XAct.Services;
    using XAct.State;

    /// <summary>
    /// 
    /// </summary>
    public class DirectoryContextSettingsManagementService : WebThreadSpecificStackableContextManagementServiceBase<IDirectoryServiceContextConfiguration>, IDirectoryContextSettingsManagementService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryContextSettingsManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="contextStateService">The context state service.</param>
        public DirectoryContextSettingsManagementService(ITracingService tracingService, IContextStateService contextStateService):
            base(tracingService, contextStateService)
        {
        }


        ///// <summary>
        ///// Invoked by <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.Create"/> to creates the source instance
        ///// before <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.Create"/> calls <see cref="CloneSourceInstanceValuesToNewThreadInstance"/>
        ///// if <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.IsMutable"/>=<c>true</c>.
        ///// </summary>
        ///// <internal>
        ///// Implementors can use <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.CreateNewSourceInstanceHelper"/> to 
        ///// use the ServiceLocator to create the instance.
        ///// </internal>
        ///// <returns></returns>
        //protected override IDirectoryServiceContextConfiguration CreateNewSourceInstance()
        //{
        //    return CreateNewSourceInstanceHelper();
        //}

        ///// <summary>
        ///// Clones the source instance values to the new thread specific instance.
        ///// </summary>
        ///// <internal>
        ///// <para>
        ///// Implementators can use <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.CloneSourceInstanceValuesToNewThreadInstanceHelper"/> if they don't have a 
        ///// more custom solution.
        ///// </para>
        ///// </internal>
        ///// <param name="srcContext">The SRC context.</param>
        ///// <returns></returns>
        //protected override IDirectoryServiceContextConfiguration CloneSourceInstanceValuesToNewThreadInstance(IDirectoryServiceContextConfiguration srcContext)
        //{
        //    return CloneSourceInstanceValuesToNewThreadInstanceHelper(srcContext);
        //}
    }
}