namespace XAct.DirectoryServices.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.DirectoryServices.AccountManagement;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using XAct.DirectoryServices.Configuration;
    using XAct.Settings;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class DirectoryContextSettingsManagementServiceTests
    {
        private UnitTestConfiguration Config
        {
            get { return _config ?? (
                _config = new UnitTestConfiguration());
            }
        }

        private static UnitTestConfiguration _config;

        bool RunADTests
        {
            get
            {
                if (
                    XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>()
                        .Get<string>("RunADTests")
                        .ToBool())
                {
                    try
                    {
                        string domain = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain().Name;
                        //Can be returning CORP, whereas we want to test with CORP.INT
                        //return domain.Contains(Config.Domain,StringComparison.OrdinalIgnoreCase);
                        return true;
                    }
                    catch
                    {
                        //will throw an exception if not hooked up.
                    }
                }
                return false;
            }
        }

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        private void SETDOMAINSETTINGS(IDirectoryServiceContextConfiguration directoryServiceContextSettings)
        {
            directoryServiceContextSettings.Domain = Config.Domain;
            directoryServiceContextSettings.DefaultOU = "DC={0},DC=local".FormatStringInvariantCulture(Config.Domain);
            directoryServiceContextSettings.ServiceUser = Config.Domain +"\\" + Config.UserName;
            directoryServiceContextSettings.ServicePassword = Config.Password;
        }

        private void SETDOMAINSETTINGS_NO_OU(IDirectoryServiceContextConfiguration directoryServiceContextSettings)
        {
            SETDOMAINSETTINGS(directoryServiceContextSettings);
            directoryServiceContextSettings.DefaultOU = null;
        }

        private void SETDOMAINSETTINGS_FULL_OU(
            IDirectoryServiceContextConfiguration directoryServiceContextSettings)
        {
            SETDOMAINSETTINGS(directoryServiceContextSettings);
            directoryServiceContextSettings.DefaultOU = "OU="+ Config.OU1+",OU=Domain Users,DC="+Config.Domain +",DC=local";
        }

        //TEST:FAILING
        [Test]
        public void UnitTest00()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }
            PrincipalContext context =
                new PrincipalContext(ContextType.Domain, Config.Domain, "DC="+Config.Domain+",DC=local");

            GroupPrincipal g = GroupPrincipal.FindByIdentity(context, Config.GRP0);


            Assert.IsNotNull(g);

        }


//        /// <summary>
//        /// Units the test000.
//        /// </summary>
//        [Test]
//        public void UnitTest000()
//        {
//            PrincipalContext context =
        //new PrincipalContext(ContextType.Domain, Config.Domain, "DC="+Config.Domain+",DC=local");

//            GroupPrincipal g = GroupPrincipal.FindByIdentity(context, "t1");
//            //returns direct users and groups:
//            g.Members.Select(p => p.Name);
//            //Returns same thing as g.Members:
//            g.GetMembers().Select(p => p.Name);
//            //returns only users, but recursive. No groups:
//            g.GetMembers(true).Select(p => p.Name);

//        }


        [Test]
        public void CanGetIDirectoryContextSettingsManagementService()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            
            Assert.IsNotNull(directoryContextSettingsManagementService);
        }

        [Test]
        public void CanGetDirectoryManagementServiceContextConfiguration()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;


            Assert.IsNotNull(directoryServiceContextSettings);
        }

        [Test]
        public void CanGetADDirectoryManagementServiceContextConfiguration()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;


            Assert.IsTrue(directoryServiceContextSettings is IActiveDirectoryServiceContextConfiguration);
        }

        [Test]
        public void CanSetConfigurationDomainName()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;

            directoryServiceContextSettings.Domain = Config.Domain;

            //Pick it back up:
            IDirectoryServiceContextConfiguration directoryServiceContextSettings2 =
                directoryContextSettingsManagementService.Current;


            Assert.AreEqual(Config.Domain, directoryServiceContextSettings2.Domain);
        }




        [Test]
        public void CanGetIActiveDirectoryPrincipalContextManagerService()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }


            IActiveDirectoryPrincipalContextManagerService activeDirectoryPrincipalContextManagerService =
                XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryPrincipalContextManagerService>();


            Assert.IsNotNull(activeDirectoryPrincipalContextManagerService);
        }



        [Test]
        public void CanSetDirectoryServiceContextConfigurationSettings()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();


            IDirectoryServiceContextConfiguration directoryServiceContextConfiguration =
                directoryContextSettingsManagementService.Current;

            SETDOMAINSETTINGS(directoryServiceContextConfiguration);

            //IDirectoryServiceContextConfiguration directoryServiceContextSettings2 =
            //    directoryContextSettingsManagementService.Current;

            


            Assert.IsNotNull(directoryContextSettingsManagementService.Current);
        }


        [Test]
        public void UnitTest06()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;

            SETDOMAINSETTINGS(directoryServiceContextSettings);



            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();

            //bool isValid = activeDirectoryService.ValidateUser(Config.Domain +"\\" + Config.UserName, Config.Password);

            Assert.IsTrue(activeDirectoryService!=null);
        }

        //TEST:FAILING
        [Test]
        public void UnitTest07()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;


            SETDOMAINSETTINGS(directoryServiceContextSettings);


            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();

            bool isValid = activeDirectoryService.ValidateUser(Config.Domain+"\\"+Config.UserName, Config.Password);

            Assert.IsTrue(isValid);
        }


        //TEST:FAILING
        [Test]
        public void UnitTest08()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS(directoryServiceContextSettings);


            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();

            if (activeDirectoryService.GetUser("XActLibTest") == null)
            {
                activeDirectoryService.CreateUser("XActLibTest", Config.Password, "test@jqdev.local", false);

            }

            UserPrincipal userPrincipal = activeDirectoryService.GetUser("XActLibTest");

            Assert.IsTrue(userPrincipal!=null);
            Assert.IsTrue(userPrincipal.Name == "XActLibTest");

            activeDirectoryService.DeleteUser("XActLibTest");

            try
            {
                userPrincipal = activeDirectoryService.GetUser("XActLibTest");
                Assert.IsTrue(userPrincipal == null);
            }
            catch
            {
                Assert.IsTrue(true);
                
            }

        }


        //TEST:FAILING
        [Test]
        public void UnitTest09()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS(directoryServiceContextSettings);



            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();
            UserPrincipal filter = new UserPrincipal(activeDirectoryService.GetCurrentContext());
            filter.Name = "Sky*";
            UserPrincipal[] results = activeDirectoryService.SearchForUsers(filter);

            Assert.IsTrue(results.Length>0);

        }

        //TEST:FAILING
        [Test]
        public void UnitTest10()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;


            SETDOMAINSETTINGS(directoryServiceContextSettings);

            

            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();
            GroupPrincipal filter = new GroupPrincipal(activeDirectoryService.GetCurrentContext());
            filter.Name = Config.GRP1;
            GroupPrincipal[] results = activeDirectoryService.SearchForGroups(filter);

            Assert.IsTrue(results.Length > 0);

        }


        //TEST:FAILING
        [Test]
        public void UnitTest11()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;


            SETDOMAINSETTINGS(directoryServiceContextSettings);



            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();
            GroupPrincipal filter = new GroupPrincipal(activeDirectoryService.GetCurrentContext());
            filter.Name = Config.GRP1;
            GroupPrincipal[] results = activeDirectoryService.SearchForGroups(filter);

            DateTime start = DateTime.Now;
            int count = 0;
            foreach(GroupPrincipal groupResult in results)
            {
                count += groupResult.Members.Count;
            }
            TimeSpan elapsed = DateTime.Now.Subtract(start);

            Assert.IsTrue(results.Length > 0);
            Assert.IsTrue(count > 0);

           Debug.WriteLine(elapsed.TotalMilliseconds);

        }



        //TEST:FAILING
        [Test]
        public void UnitTest12()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS(directoryServiceContextSettings);



            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();
            GroupPrincipal filter = new GroupPrincipal(activeDirectoryService.GetCurrentContext());
            filter.Name = Config.GRP1;
            GroupPrincipal[] results = activeDirectoryService.SearchForGroups(filter);

            DateTime start = DateTime.Now;
            int count = 0;
            Parallel.ForEach(results, g => count+= g.Members.Count);


            TimeSpan elapsed = DateTime.Now.Subtract(start);

            Assert.IsTrue(results.Length > 0);
            Assert.IsTrue(count > 0);

           Debug.WriteLine(elapsed.TotalMilliseconds);

        }



        //TEST:FAILING
        [Test]
        public void UnitTest13()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS(directoryServiceContextSettings);


            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();

            UserPrincipalEx userPrincipalEx  =new UserPrincipalEx(activeDirectoryService.GetCurrentContext());
            //------------------------------------------------------
            //Damn. Eddie was right...can't search by Member Name....
            //------------------------------------------------------
            userPrincipalEx.MemberOf = new[] {Config.GRP1};

            UserPrincipal[] results = activeDirectoryService.SearchForUsers(userPrincipalEx);




            Assert.IsTrue(results.Length > 0);



        }

        //TEST:FAILING
        [Test]
        public void UnitTest14()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS(directoryServiceContextSettings);



            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();
            GroupPrincipal filter = new GroupPrincipal(activeDirectoryService.GetCurrentContext());
            filter.Name = Config.GRP1;
            GroupPrincipal[] results = activeDirectoryService.SearchForGroups(filter);

            DateTime start = DateTime.Now;

            Dictionary<GroupPrincipal, UserPrincipalEx[] > results2 = new Dictionary<GroupPrincipal, UserPrincipalEx[]>();


            //int count = 0;
            foreach(GroupPrincipal groupResult in results)
            {

                List<UserPrincipalEx> r = new List<UserPrincipalEx>();
                        UserPrincipalEx filter2 = new UserPrincipalEx(activeDirectoryService.GetCurrentContext());
                        //filter2.MemberOf = groupResult.DistinguishedName;

                filter2.AdvancedSearchFilter.MemberOf(groupResult);

                        PrincipalSearcher principalSearcher2 = new PrincipalSearcher();
                principalSearcher2.QueryFilter = filter2;

                PrincipalSearchResult<Principal> pr = principalSearcher2.FindAll();
                
                foreach(Principal p in pr)
                        {
                            UserPrincipalEx up = p as UserPrincipalEx;
                            r.Add(up);
                        }
                        //count += g.Members.Count
                results2.Add(groupResult,r.ToArray());
            }
            
                    


            TimeSpan elapsed = DateTime.Now.Subtract(start);


            Debug.WriteLine(elapsed.TotalMilliseconds);

        }




        //TEST:FAILING
        [Test]
        public void UnitTest15()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS(directoryServiceContextSettings);

            
            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();
            GroupPrincipal filter = new GroupPrincipal(activeDirectoryService.GetCurrentContext());
            filter.Name = Config.GRP1;
            GroupPrincipal[] results = activeDirectoryService.SearchForGroups(filter);

            DateTime start = DateTime.Now;

            Dictionary<GroupPrincipal, UserPrincipalEx[]> results2 = new Dictionary<GroupPrincipal, UserPrincipalEx[]>();





            //int count = 0;
            foreach (GroupPrincipal groupResult in results)
            {

                
                List<UserPrincipalEx> r = new List<UserPrincipalEx>();

                UserPrincipalEx filter2 = new UserPrincipalEx(activeDirectoryService.GetCurrentContext());
                //filter2.MemberOf = groupResult.DistinguishedName;

                filter2.AdvancedSearchFilter.MemberOf(groupResult);

                PrincipalSearcher principalSearcher2 = new PrincipalSearcher();
                principalSearcher2.QueryFilter = filter2;

                UserPrincipalEx[] pr = groupResult.GetMemberUserPrincipalEx();

                foreach (Principal p in pr)
                {
                    UserPrincipalEx up = p as UserPrincipalEx;
                    r.Add(up);
                }
                //count += g.Members.Count
                results2.Add(groupResult, r.ToArray());
            }




            TimeSpan elapsed = DateTime.Now.Subtract(start);


            Debug.WriteLine(elapsed.TotalMilliseconds);

        }


        //TEST:FAILING
        [Test]
        public void UnitTest16()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS(directoryServiceContextSettings);


            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();

            Dictionary<GroupPrincipal,UserPrincipalEx[]> results = 
                activeDirectoryService
                .GetUserPrincipalExByGroup(Config.GRP1,Config.GRP2 );


            Assert.IsTrue(results.Keys.Count>0);


        }


        //TEST:FAILING
        [Test]
        public void UnitTest17()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS(directoryServiceContextSettings);

            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();

            Dictionary<GroupPrincipal, UserPrincipalEx[]> results = activeDirectoryService.GetUserPrincipalExByGroup(Config.GRP1, Config.GRP3);


            Assert.IsTrue(results.Keys.Count > 0);


        }

        //TEST:FAILING
        [Test]
        public void UnitTest18()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS_FULL_OU(directoryServiceContextSettings);


            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();

            Dictionary<GroupPrincipal, UserPrincipalEx[]> results = activeDirectoryService.GetUserPrincipalExByGroup(Config.GRP1, Config.GRP3);


            Assert.IsTrue(results.Keys.Count > 0);


        }



        //TEST:FAILING
        [Test]
        public void UnitTest19()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS_NO_OU(directoryServiceContextSettings);


            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();

            Dictionary<GroupPrincipal, UserPrincipalEx[]> results = activeDirectoryService.GetUserPrincipalExByGroup("APP_APOS*", Config.GRP3);


            Assert.IsTrue(results.Keys.Count > 0);


        }



        //TEST:FAILING
        [Test]
        public void UnitTest20()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;


            SETDOMAINSETTINGS_NO_OU(directoryServiceContextSettings);

            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();
            //
            Dictionary<GroupPrincipal, UserPrincipalEx[]> results = activeDirectoryService.GetUserPrincipalExByGroup(Config.Domain + "\\APP_APOS_CN_PB_ASSC", null,null);


            Assert.IsTrue(results.Keys.Count > 0);


        }


        //TEST:FAILING
        [Test]
        public void UnitTest21()
        {
            if (!RunADTests)
            {
                Assert.Ignore("AD is not available. Or Config not correct on this server. Ignoring test.");
                return;
            }

            IDirectoryContextSettingsManagementService directoryContextSettingsManagementService =
                XAct.DependencyResolver.Current.GetInstance<IDirectoryContextSettingsManagementService>();

            IDirectoryServiceContextConfiguration directoryServiceContextSettings =
                directoryContextSettingsManagementService.Current;
            ((IActiveDirectoryServiceContextConfiguration)directoryServiceContextSettings).ContextType = ContextType.Domain;

            SETDOMAINSETTINGS_NO_OU(directoryServiceContextSettings);


            IActiveDirectoryService activeDirectoryService = XAct.DependencyResolver.Current.GetInstance<IActiveDirectoryService>();
            //
            Dictionary<GroupPrincipal, UserPrincipalEx[]> results = activeDirectoryService.GetUserPrincipalExByGroup("APP_APOS_CN_PB_ASSC", null, null);


            Assert.IsTrue(results.Keys.Count > 0);


        }








        
    }


}


