namespace XAct.Settings
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class SettingHistory: IHasUserIdentifier, IHasKey, IHasDateTimeModifiedBy, IHasNote
    {



        /// <summary>"UserId"</summary>
        public string UserIdentifier { get; set; }


        /// <summary>"Key"</summary>
        public string Key { get; set; }
        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc"/>.</para>
        /// 	<para>
        /// See also <see cref="IHasAuditability"/>.
        /// </para>
        /// 	<para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        /// <value></value>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        /// </internal>
        public DateTime? LastModifiedOnUtc { get; set; }


        /// <summary>
        /// Gets or sets who last Modified the setting.
        /// </summary>
        /// <value>The Modified by.</value>
        public string LastModifiedBy { get; set; }


        /// <summary>
        /// Gets or sets the note regarding last update (ie which group locked it, etc.)
        /// </summary>
        /// <value>
        /// The note.
        /// </value>
        public string Note { get; set; }



    }
}