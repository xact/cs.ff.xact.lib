﻿//namespace XAct.Settings
//{
//    using System;
//    using System.Runtime.Serialization;

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <internal>
//    /// <para>
//    /// The key is the ApplicationId + UserId + Key
//    /// </para>
//    /// Requires distributedIdentity as first entry may be created
//    /// on client (until then, using default values).
//    /// </internal>

//    public class Setting: IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp,
//        //IHasUserIdentifier, 
//        IHasKey, IHasSerializedTypeValueAndMethod, IHasDateTimeModifiedBy, IHasNote
//    {

//        /// <summary>
//        /// Gets or sets the datastore identifier.
//        /// </summary>
//        /// <value>
//        /// The identifier.
//        /// </value>
//        public virtual Guid Id { get; set; }

//        /// <summary>
//        /// Gets or sets the datastore concurrency check timestamp.
//        /// <para>
//        /// Note that this is filled in when persisted in the db -- 
//        /// so it's usable to determine whether to generate the 
//        /// Guid <c>Id</c>.
//        ///  </para>
//        /// </summary>
//        [DataMember]
//        public virtual byte[] Timestamp { get; set; }


//        ///// <summary>"UserId"</summary>
//        //public virtual string UserIdentifier { get; set; }


//        /// <summary>"Key"</summary>
//        public virtual string Key {get; set; }


//        /// <summary>"Locked"</summary>
//        public const string Locked = "Locked";

//        /// <summary>
//        /// Gets or sets the serialization method.
//        /// </summary>
//        /// <value>The serialization method.</value>
//        public virtual SerializationMethod SerializationMethod { get; set; }

//        /// <summary>
//        /// Gets or sets the Full Qualififed Assembly name (FQAN) 
//        /// of the Type of the 
//        /// <see cref="IHasSerializedTypeValueAndMethod.SerializedValue"/>.
//        /// </summary>
//        /// <value>The type.</value>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        public virtual string SerializedValueType { get; set; }

//        /// <summary>
//        /// Gets or sets the serialized value.
//        /// </summary>
//        /// <value>The value.</value>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        public virtual string SerializedValue { get; set; }

//        /// <summary>
//        /// Gets the date this entity was last modified, expressed in UTC.
//        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackability"/>.</para>
//        /// 	<para>
//        /// See also <see cref="IHasAuditability"/>.
//        /// </para>
//        /// 	<para>
//        /// Required: Must be set prior to being saved.
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal>
//        /// There are many arguments for wanting variables of a certain
//        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
//        /// and it's very very tempting...but in the long run, I can't see
//        /// it being right decision to go against the whole nomenclature of C#
//        /// just to give better Intellisense in VS...
//        /// </internal>
//        public virtual DateTime? LastModifiedOn { get; set; }


//        /// <summary>
//        /// Gets or sets who last Modified the setting.
//        /// </summary>
//        /// <value>The Modified by.</value>
//        public virtual string LastModifiedBy { get; set; }


//        /// <summary>
//        /// Gets or sets the note regarding last update (ie which group locked it, etc.)
//        /// </summary>
//        /// <value>
//        /// The note.
//        /// </value>
//        public virtual string Note { get; set; }
//    }
//}
