﻿//using System;
//using System.Diagnostics;
//using System.IO;
//using System.Linq;
//using System.Timers;
//using System.Xml;
//using XAct.Diagnostics;
//using XAct.Environment;
//using XAct.IO;
//using XAct.Services;

//namespace XAct.Settings
//{
//    /// <summary>
//    /// An implementation of <see cref="IProfileSettingsService"/>
//    /// that works with the <c>IsolatedStorage</c>
//    /// </summary>
//    /// <internal>
//    /// Not using XDocument as not available on all platforms...
//    /// </internal>
//    /// <internal><para>8/16/2011: Sky</para></internal>
//    [DefaultBindingImplementation(typeof(IIOProfileSettingsService))]
//    public class IOProfileSettingsService : ProfileSettingsServiceBase
//    {
//        //Dollar sign is used to denote IsolatedStorage Location:
//        private const string defaultPath = @"$:\\Setttings\UserSetttings.xml";
//        private const int DEFAULT_AUTOSAVE_INTERVAL = 5000;

//        private readonly IEnvironmentService _environmentService;
//        private readonly IIOService _ioService;
//        private XmlDocument _xmlDocument;
//        private readonly System.Timers.Timer _timer;

//        private XmlElement _rootNode;
//        private XmlElement _settingsGroupNode;
//        private XmlElement _userNode;

//        /// <summary>
//        /// Gets or sets the XML storage path.
//        /// </summary>
//        /// <value>The XML storage path.</value>
//        public string XmlStoragePath
//        {
//            get { return _xmlStoragePath; }
//            set
//            {
//                if (string.IsNullOrEmpty(value))
//                {
//                    value = defaultPath;
//                }
//                if (value == _xmlStoragePath)
//                {
//                    return;
//                }
//                //Save existing:
//                SaveDocument();
//                //Start new path:
//                _xmlStoragePath = value;

//                //Clear and reload:
//                _xmlDocument = null;

//                LoadXmlDocument();
//            }
//        }
//        private string _xmlStoragePath = defaultPath;


//        #region Constructors

//        /// <summary>
//        /// Initializes a new instance of the <see cref="IOProfileSettingsService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The logging service.</param>
//        /// <param name="environmentService">The environment service.</param>
//        /// <param name="ioService">The io service.</param>
//        /// <internal>8/16/2011: Sky</internal>
//        public IOProfileSettingsService(ITracingService tracingService, IEnvironmentService environmentService, IIOService ioService)
//            : base(tracingService)
//        {
//            _environmentService = environmentService;
//            _ioService = ioService;
//            _timer = new Timer();
//            _timer.Interval = DEFAULT_AUTOSAVE_INTERVAL;
//            _timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
//            //IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication();


//            LoadXmlDocument();
//        }

//        void _timer_Elapsed(object sender, ElapsedEventArgs e)
//        {
//            SaveDocument();
//        }

//        #endregion


//        #region Method Overrides

//        /// <summary>
//        /// Gets the current IIdentity's settings.
//        /// </summary>
//        /// <returns></returns>
//        /// <internal>8/16/2011: Sky</internal>
//        public override Profile GetProfile()
//        {
//            throw new NotImplementedException();
//        }

//        /// <summary>
//        /// Saves the current IIdentity's settings.
//        /// </summary>
//        /// <internal>8/16/2011: Sky</internal>
//        public override void SaveProfile()
//        {
//            throw new NotImplementedException();
//        }

//        /// <summary>
//        /// Gets the ids of modules in app that user is allowed to use.
//        /// </summary>
//        /// <typeparam name="TIdType">The type of the id type.</typeparam>
//        /// <returns></returns>
//        /// <internal>8/16/2011: Sky</internal>
//        /// <internal>
//        /// In a multi-enterprise portal system, there could be hundreds
//        /// of 3rdPartyModules - hopefully thousands.
//        /// <para>
//        /// In a single enterprise app, there might even by many tens of modules
//        /// that have been purchased for the app.
//        /// </para>
//        /// 	<para>
//        /// Hence, we dont' want to recurse through settings for each and
//        /// every module - we just want the Modules loaded into
//        /// this application instance, that the user has access rights to
//        /// with the current Roles the user is a member of.
//        /// </para>
//        /// </internal>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        protected override TIdType[] GetIdsOfOnlyTheModulesRegisteredForThisApp<TIdType>()
//        {
//            return null;
//        }


//        /// <summary>
//        /// Gets the default settings.
//        /// </summary>
//        /// <param name="moduleId">The module id.</param>
//        /// <returns></returns>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        protected override ProfileModuleSettingsGroup GetDefaultSettings<TIdType>(TIdType moduleId)
//        {
//            return null;
//        }

//        #endregion

//        #region Private Methods
//        private void LoadXmlDocument()
//        {
//            if (_xmlDocument != null)
//            {
//                return;
//            }
////Open a stream and load up xmldocument:
//            this.TracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Ensureing the Directory for {2} exists.", _typeName, "OpenSettingsXmlDocument", this.XmlStoragePath);
//            _ioService.CreateDirectory(Path.GetDirectoryName(this.XmlStoragePath));


//            bool exists = _ioService.FileExists(this.XmlStoragePath);
//            this.TracingService.Trace(TraceLevel.Verbose, "{0}.{1}::File exists:{2}.", _typeName, "OpenSettingsXmlDocument", exists);


//            this.TracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Opening xml document:{2}.", _typeName, "OpenSettingsXmlDocument", this.XmlStoragePath);

//            using (
//                Stream stream = _ioService.Open(this.XmlStoragePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read)
//                )
//            {
//                _xmlDocument.Load(stream);
//            }
//        }
//        private void SaveDocument()
//        {
//            if (_xmlDocument == null)
//            {
//                return;
//            }
//            using (
//                Stream stream = _ioService.Open(this.XmlStoragePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read)
//                )
//            {
//                _xmlDocument.Save(stream);
//            }
//        }

//        private XmlElement GetRootElement()
//        {
//            //structure of xml document is :
//            //root/Settings/{moduleName}
//            _settingsGroupNode =
//                _xmlDocument.DocumentElement.GetChildElement("allSettings", true);
//        }

//        private XmlElement GetUserElement()
//        {
//            //structure of xml document is :
//            //root/Settings/{moduleName}
//            _settingsGroupNode =
//                _xmlDocument.DocumentElement.GetChildElement("users", true);
//        }


//        private static XmlElement GetSettingsElement()
//        {
//            //structure of xml document is :
//            //root/Settings/{moduleName}
//            _settingsGroupNode =
//                _xmlDocument.DocumentElement.GetChildElement(Constants.DataStoreAttributeNames.Settings.TableName, true);

//        }

//        private static XmlElement GetUserElement()
//        {
//            XmlElement settingsGroupNode =
//                _xmlDocument.DocumentElement.GetChildElement(Constants.DataStoreAttributeNames.Settings.TableName, true);
//        }



//        private ProfileModuleSettingsGroup LoadVariables(string moduleName, Guid userId)
//        {
//            ProfileSettingsCollection settingsCollection=null;

//            //Clear the settings dictionary:
//            settingsCollection.Clear();

//            moduleName += ".";

//            Profile profile;

//            //structure of xml document is :
//            //root/Settings/{moduleName}
//            XmlElement settingsGroupNode =
//                _xmlDocument.DocumentElement.GetChildElement(Constants.DataStoreAttributeNames.Settings.TableName, true);

//            LoadModuleElement(settingsGroupNode, moduleName, ref settingsCollection);
//        }

//        private void LoadModuleElement(Profile profile, XmlElement settingsGroupNode, string moduleName)
//        {
//            //Get or create the Module:
//            ProfileModuleSettingsGroup profileModuleSettingsGroup = profile.GetModule(moduleName);

//            //Within the module, get the root collection:
//            ProfileSettingsCollection settingsCollection = ((IProfileSettingCollectionAccessor)profileModuleSettingsGroup).SettingsCollection;

//            //Now that we have the destination collection,
//            //get the source element:

//            //Get or create the Modules node (/settings/modules/):
//            XmlElement modulesGroupNode = settingsGroupNode.GetChildElement("modules", true);
            
//            //Loop through child elements and load the properties:
//            foreach (XmlElement moduleXmlElement in  modulesGroupNode.OfType<XmlElement>())
//            {
//                LoadModuleVariables(settingsCollection, moduleXmlElement);
//            }

//            XmlElement moduleGroupName = moduleName.GetChildElement(moduleName, true);

//            //XmlElement userIdNode =
//            //    GetChildElement(settingsGroupNode, userId.ToString(), true);
//            ProfileModuleSettingsGroup moduleSettingsGroup = new ProfileModuleSettingsGroup();
//        }


//        private void LoadModuleVariables(ProfileSettingsCollection settingsCollection, XmlElement moduleXmlElement)
//        {
//            //DO NOT CLEAR as a More than one nested modules are using the same base Collection settingsCollection.Clear();

//            foreach (XmlElement settingXmlElement in moduleXmlElement.OfType<XmlElement>()/*.Where(n => n.Name.StartsWith(moduleName))*/)
//            {
//                ProfileSetting  setting = MapTo(settingXmlElement);

//                settingsCollection.Add(setting);
//            }
//        }

//        private static ProfileSetting MapTo(XmlElement settingXmlElement)
//        {
////Each Element will contain: name, type, value, devaultValue, isEditable values:

//            string settingName = settingXmlElement.Name;

//            string settingValueTypeName = settingXmlElement.GetAttributeValue(Constants.DataStoreAttributeNames.Settings.Type,
//                                                                              string.Empty);
//            System.Type settingType = System.Type.GetType(settingValueTypeName);

//            //Convert the incoming string to the typed Value:
//            string settingserializedValue = settingXmlElement.InnerText;
//            object settingValue = Convert.ChangeType(settingserializedValue, settingType);
//            object defaultValue = settingValue;

//            LockState lockState = settingXmlElement.GetAttributeValue(Constants.DataStoreAttributeNames.Settings.Locked,
//                                                                      LockState.Unlocked);
//            bool isEditable = (lockState == LockState.Unlocked);

//            //Now that we have all elements, we can create 
//            ProfileSetting setting = new ProfileSetting(settingName, settingType, settingValue, defaultValue, () => isEditable);
//            return setting;
//        }

//        #endregion
//    }
//}
