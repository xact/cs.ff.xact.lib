﻿//using System;
//using System.Collections.Generic;
//using System.IO.IsolatedStorage;
//using System.Linq;
//using System.Xml;
//using XAct.Diagnostics;
//using XAct.Services;

//namespace XAct.Settings
//{
//    /// <summary>
//    /// An implementation of <see cref="ISettingsService"/>
//    /// that works with the <c>IsolatedStorage</c>
//    /// </summary>
//    /// <internal><para>8/16/2011: Sky</para></internal>
//    [DefaultService(typeof(ISettingsService), ServiceLifetimeType.SingletonPerThread)]
//    public class ProfileIsolatedStorageSettingsService : SettingsServiceBase
//    {


//        #region Constructors
//        /// <summary>
//        /// Initializes a new instance of the <see cref="IsolatedStorageSettingsService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The logging service.</param>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        public IsolatedStorageSettingsService(ILoggingService tracingService):base (tracingService)
//        {
//           IsolatedStorageFile isolatedStorageFile = IsolatedStorageFile.GetUserStoreForApplication();

//        }
//        #endregion


//        #region Method Overrides
//        /// <summary>
//        /// Gets the ids of modules in app that user is allowed to use.
//        /// </summary>
//        /// <typeparam name="TIdType">The type of the id type.</typeparam>
//        /// <returns></returns>
//        /// <internal>8/16/2011: Sky</internal>
//        /// <internal>
//        /// In a multi-enterprise portal system, there could be hundreds
//        /// of 3rdPartyModules - hopefully thousands.
//        /// <para>
//        /// In a single enterprise, there might even by many tens of modules
//        /// that have been purchased for the app.
//        /// </para>
//        /// 	<para>
//        /// Hence, we dont' want to recurse through settings for each and
//        /// every module - we just want the Modules loaded into
//        /// this application instance, that the user has access rights to
//        /// with the current Roles the user is a member of.
//        /// </para>
//        /// </internal>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        protected override TIdType[] GetIdsOfOnlyTheModulesRegisteredForThisApp<TIdType>()
//        {
//            return null;
//        }


//        /// <summary>
//        /// Gets the default settings.
//        /// </summary>
//        /// <param name="moduleId">The module id.</param>
//        /// <returns></returns>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        protected override ModuleSettingsGroup GetDefaultSettings<TIdType>(TIdType moduleId)
//        {
//            return null;
//        }

//        #endregion

//        #region Private Methods


//        private static ModuleSettingsGroup LoadVariables(string moduleName, XmlDocument xmlDocument, Guid userId)
//        {
//            //Clear the settings dictionary:
//            settingsCollection.Clear();

//            moduleName += ".";

//            XmlElement settingsGroupNode = xmlDocument.DocumentElement.GetChildElement(Constants.Settings.TableName, true);
//            XmlElement modulesGroupName = settingsGroupNode.GetChildElement("modules", true);


//            foreach(XmlElement in moduleXmlElement in modulesGroupNode.OfType<XmlElement>())
//            {
//                LoadModuleVariables(XmlElement moduleXmlElement, SettingsCollection settingsCollection);
//            }
//            XmlElement moduleGroupName = moduleName.GetChildElement(moduleName, true);

//            //XmlElement userIdNode =
//            //    GetChildElement(settingsGroupNode, userId.ToString(), true);
//            ModuleSettingsGroup moduleSettingsGroup = new ModuleSettingsGroup();
//        }

//        void LoadModuleVariables(XmlElement moduleXmlElement, SettingsCollection settingsCollection)
//        {
//            //DO NOT CLEAR as a More than one nested modules are using the same base Collection settingsCollection.Clear();

//            foreach (XmlElement settingXmlElement in moduleXmlElement.OfType<XmlElement>()/*.Where(n => n.Name.StartsWith(moduleName))*/)
//            {
//                string settingName = settingXmlElement.Name;

//                string settingValueTypeName = settingXmlElement.GetAttributeValue(Constants.DataStoreAttributeNames.Settings.Type, string.Empty);
//                System.Type settingType = System.Type.GetType(settingValueTypeName);

//                //Convert the incoming string to the typed Value:
//                string settingserializedValue = settingXmlElement.InnerText;
//                object settingValue = Convert.ChangeType(settingserializedValue, settingType);
//                object defaultValue = settingValue;

//                LockState lockState = settingXmlElement.GetAttributeValue(Constants.DataStoreAttributeNames.Settings.Locked, LockState.Unlocked);
//                bool isEditable = (lockState == LockState.Unlocked);
//                Setting setting = new Setting(settingName, settingType, settingValue, defaultValue, () => isEditable);

//                settingsCollection.Add(setting);
//            }

//        }

//        #endregion
//    }
//}
