namespace XAct.Settings
{
    /// <summary>
    /// A specialization of the <see cref="IProfileSettingsService"/>
    /// that persists settings in an xml document, locally.
    /// </summary>
    public interface IIOProfileSettingsService: IProfileSettingsService, IHasXActLibService
    {
        
    }
}