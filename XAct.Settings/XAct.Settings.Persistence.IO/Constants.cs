namespace XAct.Settings
{
    /// <summary>Constants</summary>
    public static class Constants
    {
        #region Nested type: DataStoreAttributeNames

        /// <summary>FieldName</summary>
        public static class DataStoreAttributeNames
        {
            #region Nested type: Applications

            /// <summary>
            /// Class of Static Column Names 
            /// </summary>
            public static class Applications
            {
                /// <summary>FieldName</summary>
                public const string TableName = "Applications";

                /// <summary>FieldName</summary>
                public const string Id = "Id";

                /// <summary>FieldName</summary>
                public const string Name = "Name";

                /// <summary>FieldName</summary>
                public const string NameLowered = "NameLowered";
            }

            #endregion

            #region Nested type: RolesToOwners

            /// <summary>
            /// Names of the DataStore Column names
            /// keeping track of the
            /// Role 1-* User 
            /// relationships
            /// </summary>
            public static class RolesToOwners
            {
                /// <summary>FieldName</summary>
                public static string RoleName = "RoleName";

                /// <summary>FieldName</summary>
                public static string UserId = "UserId";
            }

            #endregion

            #region Nested type: Settings

            /// <summary>
            /// Class of Static Column Names 
            /// </summary>
            public static class Settings
            {
                /// <summary>"Settings"</summary>
                public const string TableName = "Settings";


                /// <summary>"ApplicationId"</summary>
                public const string ApplicationId = "ApplicationId";

                /// <summary>"UserId"</summary>
                public const string UserId = "UserId";

                /// <summary>"Type" (Type of Setting Value)</summary>
                public const string Type = "Type";

                /// <summary>"Key"</summary>
                public const string Key = "Key";

                /// <summary>"Value"</summary>
                public const string Value = "Value";

                /// <summary>"Locked"</summary>
                public const string Locked = "Locked";
            }



            #endregion
        }

        #endregion
    }
}