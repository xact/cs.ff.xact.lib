﻿//namespace XAct.Settings
//{
//    using System;

//    /// <summary>
//    /// 
//    /// </summary>
//    public interface IAppHostSettingsBuilder
//    {
//        /// <summary>
//        /// Gets or sets the type of the Settings
//        /// package to create. 
//        /// <para>
//        /// The default is <see cref="Settings"/>
//        /// but should be replaced during the 
//        /// application's bootstrap sequence 
//        /// by your app's 
//        /// specific Settings package.
//        /// </para>
//        /// <para>
//        /// <code>
//        /// <![CDATA[
//        /// XAct.Shortcuts.GetInstance<IAppHostSettingsBuilder>().SetttingsType = typeof(
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The type of the settings.
//        /// </value>
//        void SetSettingType<TSettings>()
//            where TSettings : Settings;

//        /// <summary>
//        /// Gets the type of the <see cref="Settings"/>
//        /// created by <see cref="IApplicationSettingsService"/>.
//        /// <para>
//        /// Default is <see cref="Settings"/>.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The type of the settings.
//        /// </value>
//        Type SettingsType { get; } 

//        /// <summary>
//        /// Creates this instance.
//        /// </summary>
//        /// <returns></returns>
//        Settings Create();
//    }
//}