﻿//namespace XAct.Settings
//{
//    using System;
//    using XAct.Services;

//    /// <summary>
//    /// 
//    /// </summary>
//    [DefaultBindingImplementation(typeof(IAppHostSettingsBuilder))]
//    public class AppHostSettingsBuilder : IAppHostSettingsBuilder 
//    {
//        /// <summary>
//        /// Gets or sets the type of the setting.
//        /// </summary>
//        /// <value>
//        /// The type of the setting.
//        /// </value>
//        public Type SettingType { get; set; }

//        /// <summary>
//        /// Creates this instance.
//        /// </summary>
//        /// <returns></returns>
//        public Settings Create()
//        {
//            return Activator.CreateInstance(SettingType) as Settings;
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="AppHostSettingsBuilder" /> class.
//        /// </summary>
//        public AppHostSettingsBuilder()
//        {
//            SettingType = typeof (Settings);
//        }

//        /// <summary>
//        /// Gets or sets the type of the Settings
//        /// package to create.
//        /// <para>
//        /// The default is <see cref="Settings" />
//        /// but should be replaced during the
//        /// application's bootstrap sequence
//        /// by your app's
//        /// specific Settings package.
//        /// </para>
//        /// <para>
//        /// <code>
//        /// <![CDATA[
//        /// XAct.Shortcuts.GetInstance<IAppHostSettingsBuilder>().SetttingsType = typeof(
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TSettings"></typeparam>
//        /// <value>
//        /// The type of the settings.
//        ///   </value>
//        public void SetSettingType<TSettings>() where TSettings : Settings
//        {
//            SettingType = typeof (TSettings);
//        }

//        /// <summary>
//        /// Gets the type of the <see cref="Settings" />
//        /// created by <see cref="IApplicationSettingsService" />.
//        /// <para>
//        /// Default is <see cref="Settings" />.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The type of the settings.
//        /// </value>
//        public Type SettingsType { get; private set; }
//    }
//}