﻿//namespace XAct.Settings
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public interface IHasInitializer
//    {
//        /// <summary>
//        /// Initializes the specified settings.
//        /// </summary>
//        /// <param name="settings">The settings.</param>
//        /// <param name="deferInitialization">if set to <c>true</c> defers Initialization and locking till later.</param>
//        void Initialize(Settings settings, bool deferInitialization = false);
//    }
//}