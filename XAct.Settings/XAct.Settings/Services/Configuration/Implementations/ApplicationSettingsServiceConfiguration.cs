// ReSharper disable CheckNamespace
namespace XAct.Settings.Configuration.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Environment;
    using XAct.Services;
    using XAct.Settings.Implementations.Configuration;


    /// <summary>
    /// A singleton configuration package for 
    /// an implementation of 
    /// <see cref="IApplcationSettingsService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IRepositoryApplicationSettingsServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.Low)]
    [DefaultBindingImplementation(typeof(IApplicationSettingsServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.VeryLow)]
    public class ApplicationSettingsServiceConfiguration : IRepositoryApplicationSettingsServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether to audit changes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [audit changes]; otherwise, <c>false</c>.
        /// </value>
        public bool AuditChanges { get; set; }

        /// <summary>
        /// By default auditing of settings changes
        /// is done with Utc time.
        /// <para>
        /// Override if you really must (never recommended).
        /// </para>
        /// </summary>
        /// <value>
        /// <c>true</c> if [persist using local time]; otherwise, <c>false</c>.
        /// </value>
        public bool PersistUsingLocalTime { get; set; }


        public string EnvironmentIdentifier { get; set; }
        public string ZoneOrTierIdentifier { get; set; }
        public string HostIdentifier { get; set; }
        public Guid TennantIdentifier { get; set; }

        public Type SettingsType { get; private set; }

        /// <summary>
        /// Function to process
        /// a <see cref="SerializedSetting.IsUnlockedInformation" />
        /// and return a <c>bool</c> result.
        /// </summary>
        public Func<string, bool> IsUnlockedCallback { get; set; }

        /// <summary>
        /// Function to process
        /// a <see cref="SerializedSetting.IsReadableAuthorisationInformation" />
        /// and return a <c>bool</c> result.
        /// </summary>
        public Func<string, bool> IsReadAuthorizedCallback { get; set; }

        /// <summary>
        /// Function to process
        /// a <see cref="SerializedSetting.IsWritableAuthorisationInformation" />
        /// and return a <c>bool</c> result.
        /// </summary>
        public Func<string, bool> IsWriteAuthorizedCallback { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettingsServiceConfiguration"/> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        public ApplicationSettingsServiceConfiguration(IEnvironmentService environmentService)
        {
            EnvironmentIdentifier = environmentService.EnvironmentName;
            ZoneOrTierIdentifier = null;
            HostIdentifier = environmentService.MachineName;

            SettingsType = typeof (ApplicationSettings);

            AuditChanges = true;
            IsUnlockedCallback = (s) => true;
            IsReadAuthorizedCallback = (s) => true;
            IsWriteAuthorizedCallback = (s) => true;
        }

        public TSettings Build<TSettings>()
            where TSettings : Settings
        {
            return (TSettings) System.Activator.CreateInstance(SettingsType);
        }


        /// <summary>
        /// Sets the type of the <see cref="Settings" />
        /// object to be Instantiated when <see cref="Build{TSettings}" />
        /// is invoked.
        /// <para>
        /// If not set, the default action is to return
        /// a <see cref="Settings" /> (not a subclass).
        /// </para>
        /// <para>
        /// If invoked, this method should be invoked
        /// early, during the Application Bootstrap
        /// sequence, then left as is.
        /// </para>
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <param name="appSetting">The application setting.</param>
        /// <param name="zoneOrTier">The zone or tier.</param>
        /// <param name="isUnlockedCallback">The is unlocked callback.</param>
        /// <param name="isReadAuthorizedCallback">The is read authorized callback.</param>
        /// <param name="isWriteAuthorizedCallback">The is write authorized callback.</param>
        /// <param name="auditChanges">if set to <c>true</c> [audit changes].</param>
        public void Initialize<TSettings>(
            TSettings appSetting,
            string zoneOrTier=null,
            Func<string, bool> isUnlockedCallback=null,
            Func<string, bool> isReadAuthorizedCallback=null,
            Func<string, bool> isWriteAuthorizedCallback=null,
            bool auditChanges = true
            )
            where TSettings : Settings
        {
            SettingsType = typeof (TSettings);
            
            ZoneOrTierIdentifier = zoneOrTier;

            IsUnlockedCallback = isUnlockedCallback ?? ((s) => true);
            IsReadAuthorizedCallback = isReadAuthorizedCallback ?? ((s) => true);
            IsWriteAuthorizedCallback = isWriteAuthorizedCallback ?? ((s) => true);

            AuditChanges = auditChanges;

        }



    }
}
