namespace XAct.Settings.Implementations.Configuration
{
    using System;

    /// <internal>Has IHasSingletonScope</internal>
    public interface IRepositoryApplicationSettingsServiceConfiguration : IApplicationSettingsServiceConfiguration, IHasXActLibServiceConfiguration
    {
        //Type SettingsType { get;  }

        ///// <summary>
        ///// Function to process
        ///// a <see cref="SerializedSetting.IsUnlockedInformation" />
        ///// and return a <c>bool</c> result.
        ///// </summary>
        //Func<string, bool> IsUnlockedCallback { get; set; }

        ///// <summary>
        ///// Function to process
        ///// a <see cref="SerializedSetting.IsReadableAuthorisationInformation" />
        ///// and return a <c>bool</c> result.
        ///// </summary>
        //Func<string, bool> IsReadAuthorizedCallback { get; set; }

        ///// <summary>
        ///// Function to process
        ///// a <see cref="SerializedSetting.IsWritableAuthorisationInformation" />
        ///// and return a <c>bool</c> result.
        ///// </summary>
        //Func<string, bool> IsWriteAuthorizedCallback { get; set; }


        //Settings Build();

    }
}