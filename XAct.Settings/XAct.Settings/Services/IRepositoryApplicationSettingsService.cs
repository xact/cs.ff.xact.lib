// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for a Repository based implementation of
    /// <see cref="IApplicationSettingsService"/>
    /// </summary>
    public interface IRepositoryApplicationSettingsService : IApplicationSettingsService, IHasXActLibService
    {
        
    }
}