// ReSharper disable CheckNamespace
namespace XAct.Settings.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using XAct;
    using XAct.Diagnostics;
    using XAct.Domain;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;
    using XAct.Settings.Implementations.Configuration;
    using XAct.State;

    /// <summary>
    /// Implementation of the <see cref="IApplicationSettingsService"/>
    /// contract to persist application settings using a repository.
    /// </summary>

    [DefaultBindingImplementation(typeof(IApplicationSettingsService), BindingLifetimeType.Undefined, Priority.Low /*OK: For now*/)]
    public class RepositoryApplicationSettingsService : XActLibServiceBase, IRepositoryApplicationSettingsService
    {

        // ReSharper disable InconsistentNaming
        public const string C_STATE_KEY = "AppHostSettings";
        // ReSharper restore InconsistentNaming

        private readonly IDateTimeService _dateTimeService;
        private readonly IRepositoryService _repositoryService;
        private readonly IApplicationStateService _applicationStateService;
        private readonly IPrincipalService _principalService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryApplicationSettingsServiceConfiguration _applicationSettingsServiceConfiguration;


        DateTime Now
        {
            get
            {
                return _applicationSettingsServiceConfiguration.PersistUsingLocalTime ? 
#pragma warning disable 612,618
                    _dateTimeService.Now
#pragma warning restore 612,618
                    : _dateTimeService.NowUTC; }
        }

        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        XAct.Settings.IApplicationSettingsServiceConfiguration IApplicationSettingsService.Configuration
        {
            get { return _applicationSettingsServiceConfiguration; }
        }

        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IRepositoryApplicationSettingsServiceConfiguration Configuration
        {
            get { return _applicationSettingsServiceConfiguration; }
        }


        /// <summary>
        /// Retrieves the <see cref="Settings" /> (or a subclass of it)
        /// of the specified <see cref="Type" />.
        /// </summary>
        /// <returns></returns>
        public ApplicationSettings Current
        {
            get
            {
                var result = GetCurrentSettings<ApplicationSettings>();
                return result;
            }
        }


        /// <summary>
        /// Retrieves the current <see cref="Settings" />
        /// package.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <returns></returns>
        public TSettings GetCurrentSettings<TSettings>()
            where TSettings : ApplicationSettings
        {
            //If already retrieved, no need to go looking in app cache.
            if (_applicationSettings != null)
            {
                return (TSettings) _applicationSettings;
            }

            //Get it from Application Cache (as oppossed to Session/Request):
            Dictionary<string, object> dictionary = _applicationStateService.GetXActLibStateDictionary();

            //Create new if need be:
            if (!dictionary.ContainsKey(C_STATE_KEY))
            {
                dictionary[C_STATE_KEY] = _applicationSettingsServiceConfiguration.Build<TSettings>();
            }
            _applicationSettings = dictionary[C_STATE_KEY] as TSettings;

            Debug.Assert(_applicationSettings != null);

            //We embed into the Settings object 
            //information about the current app, and tier.
            _applicationSettings.ContextIdentifier =
                new ApplicationSettingScope(
                    _applicationSettingsServiceConfiguration.EnvironmentIdentifier,
                    _applicationSettingsServiceConfiguration.ZoneOrTierIdentifier,
                    _applicationSettingsServiceConfiguration.HostIdentifier,
                    _applicationSettingsServiceConfiguration.TennantIdentifier
                    );
            Initialize(
                _applicationSettings
                );

            return (TSettings) _applicationSettings;
        }

        private ApplicationSettings _applicationSettings;



        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryApplicationSettingsService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="applicationStateService">The application state service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="applicationSettingsServiceConfiguration">The application settings service configuration.</param>
        public RepositoryApplicationSettingsService(
            ITracingService tracingService,
            IDateTimeService dateTimeService,
            IApplicationStateService applicationStateService,
            IPrincipalService principalService,
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService,
            IRepositoryApplicationSettingsServiceConfiguration applicationSettingsServiceConfiguration)
            : base(tracingService)
        {
            _typeName = this.GetType().Name;

            _dateTimeService = dateTimeService;
            _applicationStateService = applicationStateService;
            _principalService = principalService;
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
            _applicationSettingsServiceConfiguration = applicationSettingsServiceConfiguration;


            //Consider: Alt: http://www.codeproject.com/Articles/475498/Easier-NET-settings-management

        }

        /// <summary>
        /// Initializes the settings.
        /// Invoked from the <see cref="Settings" /> property
        /// the first time it is invoked by each SubClass.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public TSettings Initialize<TSettings>(TSettings settings)
            where TSettings : ApplicationSettings
        {
            var contextIdentifier = settings.ContextIdentifier;

            contextIdentifier.ValidateIsNotDefault("settings",
                                                "settings.ContextIdentifier must be set before invoking Initilialize<TSetting>");

            //Use repository to get Settings from the Db backed EF repository:
            SerializedApplicationSetting[] serializedSettings =
                RetrieveSerializedSettingsFromDataStore(contextIdentifier).ToArray();

            //Use helper method to load SerializedSettings into given Setting object:
            settings.Load(
                serializedSettings,
                true /*settings are already ordered*/,
                true /*include editing information*/,
                _applicationSettingsServiceConfiguration.IsUnlockedCallback,
                _applicationSettingsServiceConfiguration.IsReadAuthorizedCallback,
                _applicationSettingsServiceConfiguration.IsWriteAuthorizedCallback
                );

            return settings;
        }

        /// <summary>
        /// Persists the current <see cref="Settings" />.
        /// <para>
        /// Any new <see cref="Setting"/> elements in the 
        /// <see cref="Current"/>
        /// <see cref="Settings"/> will be persisted using
        /// ZoneIdentifier, and HostIdentifier
        /// information retrieved from 
        /// <see cref="ApplicationSettings.ContextIdentifier"/>
        /// </para>
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Persist()
        {
            Persist(Current);
        }

        /// <summary>
        /// Persists the specified <see cref="Settings"/> object.
        /// <para>
        /// Any new <see cref="Setting"/> elements in 
        /// <paramref name="applicationSettingsToPersist"/> will be persisted using
        /// ZoneIdentifier, and HostIdentifier
        /// information retrieved from 
        /// <see cref="Settings.ContextIdentifier"/>
        /// </para>
        /// </summary>
        public void Persist(ApplicationSettings applicationSettingsToPersist, IDictionary<string, object> grabBag = null)
        {

            //An issue is knowing which settings are New, Updates, or Deletes
            //We start by flattening all the settings from the various modules, etc.
            //into one single list, that is easier to iterate:
            List<Setting> allApplicationSettingsToPersist = 
                SettingsExtensions.GetAllSettings(applicationSettingsToPersist);

            //It's cheaper to do a check for changes in memory than hitting db.
            if (allApplicationSettingsToPersist.All(s => s.ModelState == OfflineModelState.UnchangedExisting))
            {
                //Get out as nothing added New, nothing Updated, nothing Deleted:
                return;
            }


            IApplicationSettingScope applicationSettingScope = applicationSettingsToPersist.ContextIdentifier;


            //Retrieve from Db Retrieve settings from db to compare 
            //current settings (disconnected) settings against.
            List<SerializedApplicationSetting> existingPersistedSerializedSettings =
                RetrieveSerializedSettingsFromDataStore(applicationSettingScope);


            //Now use the current settings collection to update the settings currently in the database.
            UpdateDatabaseSettings(existingPersistedSerializedSettings, applicationSettingsToPersist, allApplicationSettingsToPersist, grabBag);

            //That's good for the database. But there's a small chance that the db has settings
            //that were added after the current settings were retrieved.
            //so we need to retrieve them.
            _repositoryService.GetContext().Commit(CommitType.Default);

            

            applicationSettingsToPersist.ClearAllSettings();


            //Have to Commit before we pick back up, else the retrieved array
            //will not contain newly added entries...

            existingPersistedSerializedSettings =
                RetrieveSerializedSettingsFromDataStore(applicationSettingScope);

            applicationSettingsToPersist.Load(
                existingPersistedSerializedSettings.ToArray(),
                true /*settings are already ordered*/,
                true /*include editing information*/,
                _applicationSettingsServiceConfiguration.IsUnlockedCallback,
                _applicationSettingsServiceConfiguration.IsReadAuthorizedCallback,
                _applicationSettingsServiceConfiguration.IsWriteAuthorizedCallback
                );

        }








        private void UpdateDatabaseSettings(List<SerializedApplicationSetting> existingPersistedSerializedSettings,
            ApplicationSettings applicationSettingsToPersist,
            List<Setting> allApplicationSettingsToPersist = null, IDictionary<string, object> grabBag = null)
        {


            //Update the retrieved set with settings:
            Dictionary<Guid, string> oldSettings = new Dictionary<Guid, string>();

            // ReSharper disable RedundantTypeArgumentsOfMethod
            existingPersistedSerializedSettings.UpdateFrom<SerializedApplicationSetting, Setting>(
                // ReSharper restore RedundantTypeArgumentsOfMethod
                allApplicationSettingsToPersist, 
                
                //Our comparison between new and old, by which we find out if new (no Guid), deleted (missing), or equal
                o => o.Id, //.CompositeKey()

                //Convert incoming Settings to SerializedSettings
                s => s.SerializeAsSerializedSetting(applicationSettingsToPersist),

                //Define the callback that is used to update the original collection item
                //from the new returning collection item:
                //Returns true if changes made:
                (ss1, ss2) =>
                    {
                        if (object.Equals(ss1.SerializedValue, ss2.SerializedValue))
                        {
                            return false;
                        }
                        //Hack to save settings to pass to audit
                        oldSettings[ss2.Id] = ss2.SerializedValue;

                        //Has scope changed?
                        //SetScope(ss2);

                        ss2.SerializedValue = ss1.SerializedValue;


                        return true;
                    },
                //softDeletePredicate:
                s => 
                    s.ModelState == OfflineModelState.Deleted,
                //PreCreate Callback:
                ss =>
                    {
                        //Scope will have been set to Settings:
                        //SetScope(ss);

                        ss.Enabled = true;
                        ss.CreatedOnUtc = Now;
                        ss.LastModifiedOnUtc = ss.CreatedOnUtc;
                        _repositoryService.PersistOnCommit<SerializedApplicationSetting,Guid>(ss,true);

                        if (_applicationSettingsServiceConfiguration.AuditChanges)
                        {
                            //Now Audit it:
                            var applicationSettingAudit = CreateAudit(ss, null, grabBag);
                            _repositoryService.PersistOnCommit<ApplicationSettingAudit, Guid>(applicationSettingAudit,
                                                                                              true);
                        }
                    },
                //PreUpdate Callback:
                ss =>
                    {

                        ss.LastModifiedOnUtc = Now;
                        _repositoryService.UpdateOnCommit(ss);

                        if (_applicationSettingsServiceConfiguration.AuditChanges)
                        {
                            //Now Audit it:
                            string previousSerializedValue = oldSettings[ss.Id];
                            var applicationSettingAudit = CreateAudit(ss, previousSerializedValue, grabBag);
                            _repositoryService.PersistOnCommit<ApplicationSettingAudit, Guid>(applicationSettingAudit,
                                                                                              true);
                        }

                    },
                //PreDelete Callback:
                ss => 
                    _repositoryService.DeleteOnCommit(ss));
        }

        void SetScope(SerializedApplicationSetting serializedApplicationSetting)
        {
            int scope = serializedApplicationSetting.Scope;

            if (scope == 0)
            {
                //Do nothing.
                return;
            }

            if (scope > 0)
            {
                serializedApplicationSetting.TennantIdentifier = Guid.Empty;
            }
            if (scope > 1)
            {
                serializedApplicationSetting.HostIdentifier = null;
            }
            if (scope > 2)
            {
                serializedApplicationSetting.ZoneOrTierIdentifier = null;
            }
            if (scope > 3)
            {
                serializedApplicationSetting.EnvironmentIdentifier = null;
            }

        }

        private ApplicationSettingAudit CreateAudit(SerializedApplicationSetting serializedApplicationSetting,
                                                    string previousSerializedValue, IDictionary<string,object> grabBag )
        {
            ApplicationSettingAudit applicationSettingAudit = new ApplicationSettingAudit();



            applicationSettingAudit.ApplicationTennantId = _applicationTennantService.Get();
            applicationSettingAudit.SettingFK = serializedApplicationSetting.Id;
            applicationSettingAudit.Key = serializedApplicationSetting.Key;

            applicationSettingAudit.SerializationMethod = serializedApplicationSetting.SerializationMethod;
            applicationSettingAudit.SerializedValueType = serializedApplicationSetting.SerializedValueType;
            applicationSettingAudit.SerializedValue = serializedApplicationSetting.SerializedValue;

            applicationSettingAudit.SerializedPreviousValue = previousSerializedValue;

            applicationSettingAudit.CreatedBy = _principalService.CurrentIdentityIdentifier ?? "anon";
            applicationSettingAudit.CreatedOnUtc = Now;

            if (grabBag != null)
            {
                object tmp;
                if (grabBag.TryGetValue("CreatedByOrganisation", out tmp))
                {
                    string s = tmp as string;
                    applicationSettingAudit.CreatedByOrganisation = s;
                }
                if (grabBag.TryGetValue("CreatedBy", out tmp))
                {
                    string s = tmp as string;
                    applicationSettingAudit.CreatedBy = s;
                }
                if (grabBag.TryGetValue("Tag", out tmp))
                {
                    string s = tmp as string;
                    applicationSettingAudit.Tag = s;
                }
            }

            return applicationSettingAudit;
        }



        private List<SerializedApplicationSetting> RetrieveSerializedSettingsFromDataStore(
            IApplicationSettingScope settingsScope)
        {
            settingsScope.ValidateIsNotDefault("settingsSerializationInfo");
            //Get entries that are enabled, for this or all applications (blank application name):
            //We'll go through 
            var results = _repositoryService.GetByFilter<SerializedApplicationSetting>(x =>
                                                                                       (
                                                                                           (x.Enabled)
                                                                                           &&
                                                                                           (new[]
                                                                                               {
                                                                                                   string.Empty,
                                                                                                   settingsScope
                                                                                               .EnvironmentIdentifier
                                                                                               }
                                                                                               .Contains(
                                                                                                   x
                                                                                                       .EnvironmentIdentifier))
                                                                                           &&
                                                                                           (new[]
                                                                                               {
                                                                                                   string.Empty,
                                                                                                   settingsScope
                                                                                               .ZoneOrTierIdentifier
                                                                                               }
                                                                                               .Contains(
                                                                                                   x
                                                                                                       .ZoneOrTierIdentifier))
                                                                                           &&
                                                                                           (new[]
                                                                                               {
                                                                                                   string.Empty,
                                                                                                   settingsScope
                                                                                               .HostIdentifier
                                                                                               }
                                                                                               .Contains(
                                                                                                   x.HostIdentifier))
                                                                                       ),
                                                                                       null, //no includes necessary
                                                                                       null, //no paging necessary
                                                                                       q =>
                                                                                       q.OrderBy(x => x.Key)
                                                                                        .ThenByDescending(
                                                                                            x => x.EnvironmentIdentifier)
                                                                                        .ThenByDescending(
                                                                                            x => x.ZoneOrTierIdentifier)
                                                                                        .ThenByDescending(
                                                                                            x => x.HostIdentifier)

                //Want for all applications first, then overwrite with our own.
                );
            
            var x1 = results.DistinctBy(x => x.Key).ToList();
            var x2 = results.ToArray();
            
            //Returning list rather than Array as
            //in Persist stage, items can be added.


            return x1;
        }
    }
}