using System;
using System.Collections.Generic;
using NConfig;
using XAct.Diagnostics;
using XAct.Environment;


namespace XAct.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public static class NConfigInitializer
    {
        private static object _lock = new Object();
        private static string _nConfigFilePath = "Config\\MachineSpecificSettings\\app.config";


        /// <summary>
        /// Gets or sets the use NConfig to merge 
        /// Machine specific settings in from 
        /// <see cref="NConfigFilePath"/>.
        /// <para>
        /// Default is <c>True</c>.
        /// </para>
        /// </summary>
        public static bool? EnableNConfig { get; set; }// = true;

        /// <summary>
        /// Gets a value indicating whether [static initialized].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [static initialized]; otherwise, <c>false</c>.
        /// </value>
        public static bool StaticInitialized { get; private set; }

        /// <summary>
        /// The Relative path to look for NConfig files to merge into AppSettings.
        /// <para>
        /// The default value is <c>"Config\\MachineSpecificSettings\\app.config"</c>
        /// </para>
        /// </summary>
        public static string NConfigFilePath
        {
            get { return _nConfigFilePath; }
            set { _nConfigFilePath = value; }
        }


        /// <summary>
        /// Gets the full paths to templates used to find NConfig override files.
        /// </summary>
        /// <value>
        /// The n configuration paths searched.
        /// </value>
        public static string[] NConfigPathsSearched { get; set; }

        /// <summary>
        /// Initializes NConfig as per the settings.
        /// </summary>
        /// <param name="vDir">The v dir.</param>
        public static void Initialize(string vDir="VDir")
        {
            if (StaticInitialized)
            {
                return;
            }

            lock (_lock)
            {
                if (StaticInitialized)
                {
                    return;
                }

                if (!EnableNConfig.HasValue)
                {
                    var x =
                        (System.Configuration.ConfigurationManager.AppSettings[XAct.Library.Constants.AppSettingKeys.EnableNConfig] ?? string.Empty).ToBool();
                    if (x == false)
                    {
                        EnableNConfig = false;
                    }
                }

                if (EnableNConfig.HasValue && !EnableNConfig.Value)
                {
                    StaticInitialized = true;
                    return;
                }

                //Local Path:
                //NConfigurator.UsingFiles("..\\..\\Config\\Custom.config", "..\\..\\Config\\Connections.config").SetAsSystemDefault();
                //Also works in Solution sharing across projects:

                //When running NConfig within a Website, it has to account for the fact that in one 
                //it might be running under a Virtual Directory, and in another, it's the website itself.
                //Also, the VDir can differ -- eg: one deployment package may release it to VDirDemo, another VDirBranch123, etc.
                string appSettingVDirValue = string.Empty;
                if (!vDir.IsNullOrEmpty())
                {
                    appSettingVDirValue = System.Configuration.ConfigurationManager.AppSettings[vDir] ?? string.Empty;
                }
                //Look in VDir/VDirDemo/VDirBranch123:
                string[] potentialVirtualDirectories = appSettingVDirValue.Split(new[] { '|', ',', ';' }
                    ,StringSplitOptions.RemoveEmptyEntries
                    );
                if (potentialVirtualDirectories.Length == 0)
                {
                    //Ensure at least one, for the loop below:
                    potentialVirtualDirectories = new[] {""};
                }
                List<string> possibleRelativeDirectoryPaths = new List<string>();
                List<string> possibleResolvedDirectoryPaths = new List<string>();
#pragma warning disable 168
                //Worth knowing:...
                //string hostAlias = NConfigurator.Settings.HostAlias;
#pragma warning restore 168

                foreach (string possibleVirtualDirectory in potentialVirtualDirectories)
                {
                    //gives back something like Config\\MachineSpecificSettings\\app.config:
                    string possibleRelativePaths = (possibleVirtualDirectory.IsNullOrEmpty() ? string.Empty : possibleVirtualDirectory + "\\") + NConfigFilePath;
                    possibleRelativeDirectoryPaths.Add(possibleRelativePaths);

                    //Note that in an MSTest unit test, the output will reflect somethng similar to:
                    //C:\Users\SkyS\AppData\Local\Temp\TestResults\skys_SKYSW8LT 2014-03-02 19_15_21\Out\Config\MachineSpecificSettings\app.config

                    //string fullVirtualDirectoryFileName = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().MapPath(possibleVirtualDirectoryFragment);
                    //possibleResolvedDirectoryPaths.Add(fullVirtualDirectoryFileName);

                }
                try
                {
                    //Save a list so we know where we looked:
                    NConfigPathsSearched = possibleResolvedDirectoryPaths.ToArray();

                    //Pas just the filepath fragments (eg: "VDir\Configs\MachineSPecific\"):
                    NConfigurator.UsingFiles(possibleRelativeDirectoryPaths.ToArray()).SetAsSystemDefault();
                }
                catch (System.Exception e)
                {
                    System.Diagnostics.Trace.Write("Could not handle NConfig references.");
                    System.Diagnostics.Trace.Write(e.Message);
                    System.Diagnostics.Trace.Write(e.StackTrace);
                    
                    //Avoid using ServiceLocator this early
                    //ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();
                    //tracingService.TraceException(TraceLevel.Error, e, "Could not handle NConfig references.");
                }


                StaticInitialized = true;
            }
        }

        
    }
}