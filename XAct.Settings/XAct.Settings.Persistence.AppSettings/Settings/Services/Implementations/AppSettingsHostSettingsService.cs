﻿// ReSharper disable CheckNamespace
namespace XAct.Settings.Implementations
// ReSharper restore CheckNamespace
{
    using XAct.Services;
    using XAct.Settings;


    /// <summary>
    /// An implementation of the 
    /// <see cref="IAppSettingsHostSettingsService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IHostSettingsService),BindingLifetimeType.Undefined,Priority.Low /*OK:Secondary Binding*/)]
    public class AppSettingsHostSettingsService : IAppSettingsHostSettingsService
    {


        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IHostSettingsServiceConfiguration IHostSettingsService.Configuration
        {
            get { return _appSettingsHostSettingsServiceConfiguration; }
        }


        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IAppSettingsHostSettingsServiceConfiguration Configuration
        {
            get { return _appSettingsHostSettingsServiceConfiguration; }
        }

        private readonly IAppSettingsHostSettingsServiceConfiguration _appSettingsHostSettingsServiceConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppSettingsHostSettingsService"/> class.
        /// </summary>
        /// <param name="appSettingsHostSettingsServiceConfiguration">The application settings host settings service configuration.</param>
        public AppSettingsHostSettingsService(IAppSettingsHostSettingsServiceConfiguration appSettingsHostSettingsServiceConfiguration)
        {
            _appSettingsHostSettingsServiceConfiguration = appSettingsHostSettingsServiceConfiguration;
            _appSettingsHostSettingsServiceConfiguration.Initialize();
        }

        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key, bool throwExceptionOnConversionException = true)
        {

            var result = Get(key, default(TValue), throwExceptionOnConversionException);
            return result;
        }

        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// <para>
        /// If the result is null, <paramref name="defaultValue"/>.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value if no value was found.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>

        public TValue Get<TValue>(string key, TValue defaultValue, bool throwExceptionOnConversionException = true)
        {
            string tmp = System.Configuration.ConfigurationManager.AppSettings.Get(key);

            //If there was a value, it would have come back as a string. Even an empty one.
            if (tmp == null) // && (!ConfigurationManager.AppSettings.AllKeys.Any(s => (s == key)))
            {
                return defaultValue;
            }

            if (throwExceptionOnConversionException)
            {
                tmp.ConvertTo<TValue>();
            }
            try
            {
                return tmp.ConvertTo<TValue>();
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}