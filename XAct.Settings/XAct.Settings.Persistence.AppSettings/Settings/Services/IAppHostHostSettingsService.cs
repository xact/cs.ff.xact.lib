﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{

    /// <summary>
    /// Contract for a service to retrieve HostSettings (which are different from ApplicationSettings) from 
    /// the applications web.config file.
    /// <para>
    /// Use sparingly. Prefer using <see cref="IApplicationSettingsService"/>
    /// whereever feasible.
    /// </para>
    /// </summary>
    public interface IAppSettingsHostSettingsService : IHostSettingsService, IHasXActLibService
    {
        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        new IAppSettingsHostSettingsServiceConfiguration Configuration { get; }
    }
}