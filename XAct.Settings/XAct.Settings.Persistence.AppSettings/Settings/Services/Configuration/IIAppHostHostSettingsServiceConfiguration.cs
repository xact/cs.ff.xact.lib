﻿namespace XAct.Settings
{
    /// <summary>
    /// Contract for a singleton initialization package for
    /// instances of <see cref="IAppSettingsHostSettingsService"/>.
    /// </summary>
    public interface IAppSettingsHostSettingsServiceConfiguration :  IHostSettingsServiceConfiguration , IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets or sets the use NConfig to merge 
        /// Machine specific settings in from 
        /// <see cref="NConfigFilePath"/>.
        /// <para>
        /// Default is <c>True</c>.
        /// </para>
        /// </summary>
        bool? EnableNConfig { get; set; }

        /// <summary>
        /// The Relative path to look for NConfig files to merge into AppSettings.
        /// <para>
        /// The default value is <c>"Config\\MachineSpecificSettings\\app.config"</c>
        /// </para>
        /// </summary>
         string NConfigFilePath { get; set; }


         /// <summary>
         /// Gets or sets the v dir application setting key.
         /// The default setting is <c>VDir</c>
         /// </summary>
         string VDirAppSettingKey { get; set; }


 
    }
}