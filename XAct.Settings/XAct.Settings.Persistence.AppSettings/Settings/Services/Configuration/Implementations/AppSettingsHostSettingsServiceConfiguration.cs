﻿namespace XAct.Settings.Implementations
{
    using XAct.Services;

    /// <summary>
    /// Implementation of
    /// <see cref="IAppSettingsHostSettingsServiceConfiguration" />
    /// </summary>
    [DefaultBindingImplementation(typeof(IHostSettingsServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.Low /*OK:Secondary Binding*/)]
    public class AppSettingsHostSettingsServiceConfiguration : IAppSettingsHostSettingsServiceConfiguration, IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets or sets the use NConfig to merge 
        /// Machine specific settings in from 
        /// <see cref="NConfigFilePath"/>.
        /// <para>
        /// Default is <c>True</c>.
        /// </para>
        /// </summary>
        public bool? EnableNConfig 
        {
            get { return NConfigInitializer.EnableNConfig; }
            set { NConfigInitializer.EnableNConfig = value; }
        }


        /// <summary>
        /// The Relative path to look for NConfig files to merge into AppSettings.
        /// <para>
        /// The default value is <c>"Config\\MachineSpecificSettings\\app.config"</c>
        /// </para>
        /// </summary>
        public string NConfigFilePath
        {
            get { return NConfigInitializer.NConfigFilePath; }
            set { NConfigInitializer.NConfigFilePath = value; }
        }



        /// <summary>
        /// Gets the full paths to templates used to find NConfig override files.
        /// </summary>
        /// <value>
        /// The n configuration paths searched.
        /// </value>
        public string[] NConfigPathsSearched
        {
            get { return NConfigInitializer.NConfigPathsSearched; }
        }


        /// <summary>
        /// Gets or sets the v dir application setting key.
        /// The default setting is <c>VDir</c>
        /// </summary>
        public string VDirAppSettingKey
        {
            get { return _VDirAppSettingKey; }
            set { _VDirAppSettingKey = value; }
        }
        /// <summary>
        /// Gets or sets the v dir application setting key.
        /// The default setting is <c>VDir</c>
        /// </summary>
        public string _VDirAppSettingKey = "VDir";



        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get { return NConfigInitializer.StaticInitialized; } }




        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {

            NConfigInitializer.Initialize(this.VDirAppSettingKey);
        }
    }
}