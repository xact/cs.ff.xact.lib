﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public class AppSettingsStatusServiceConnectorConfigurationItem
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string Key { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppSettingsStatusServiceConnectorConfigurationItem"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="key">The key.</param>
        public AppSettingsStatusServiceConnectorConfigurationItem(string title, string key)
        {
            Title = title;
            Key = key;
        }
    }
}