﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System.Collections.Generic;

    /// <summary>
    /// Configuration package for
    /// an implementation of
    /// <see cref="AppSettingsStatusServiceConnector"/>
    /// </summary>
    public class AppSettingsStatusServiceConnectorConfiguration : IHasTransientBindingScope
    {
        /// <summary>
        /// Gets the HostSettings (AppSettings) keys to report on.
        /// </summary>
        /// <value>
        /// The keys.
        /// </value>
        public List<AppSettingsStatusServiceConnectorConfigurationItem> AppSettingDefinitions
        {
            get { return _keys ?? (_keys = new List<AppSettingsStatusServiceConnectorConfigurationItem>()); }
        }

        private List<AppSettingsStatusServiceConnectorConfigurationItem> _keys;
    }
}