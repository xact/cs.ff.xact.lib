﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Resources;


    /// <summary>
    /// An implementation of <see cref="IStatusServiceConnector"/>
    /// that returns the values of AppSettings on the Server.
    /// </summary>
    public class AppSettingsStatusServiceConnector : XActLibStatusServiceConnectorBase
    {
        /// <summary>
        /// Gets or sets the configuration package for this Connector.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public AppSettingsStatusServiceConnectorConfiguration Configuration { get; private set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="AppSettingsStatusServiceConnector" /> class.
        /// </summary>
        /// <param name="connectorConfiguration">The connector configuration.</param>
        public AppSettingsStatusServiceConnector(AppSettingsStatusServiceConnectorConfiguration connectorConfiguration)
            :base()
        {
            Name = "AppSettings";
            Configuration = connectorConfiguration;
        }

        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null)
        {
            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }


        private void MakeTable(StatusResponseTable table, object arguments)
        {

            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:
            List<AppSettingsStatusServiceConnectorConfigurationItem> appSettingsToRetrieve =
                Configuration.AppSettingDefinitions;


            //Create Headers whether there are rows or not:
            table.Headers.Add("Key");
            table.Headers.Add("Value");

            //Make rows:
            if (appSettingsToRetrieve == null)
            {
                return;
            }

            foreach (AppSettingsStatusServiceConnectorConfigurationItem appSettingsDefinition in appSettingsToRetrieve)
            {
                table.Rows.Add(MakeTableRow(appSettingsDefinition));

            }
        }

        private StatusResponseTableRow MakeTableRow(AppSettingsStatusServiceConnectorConfigurationItem appSettingsStatusServiceConnectorValueDefinition)
        {
            StatusResponseTableRow row = new StatusResponseTableRow();

            //Get the value
            //Note: no exception raised if not found:
            string value = ConfigurationManager.AppSettings.Get(appSettingsStatusServiceConnectorValueDefinition.Key);

            //Cell #1:

            row.Cells.Add(appSettingsStatusServiceConnectorValueDefinition.Title);

            //Cell #2:
            if (value == null)
            {
                value = "[NULL]";
            }

            row.Cells.Add(value);

            //Either way, it's ok
            row.Status = ResultStatus.Success;

            return row;
        }
    }
}
