﻿namespace XAct.Diagnostics.Status.Connectors
{
    using XAct.Diagnostics.Status.Connectors.Implementations;

    /// <summary>
    /// Factory for <see cref="AppSettingsStatusServiceConnector"/> instances.
    /// </summary>
    public class AppSettingsStatusServiceConnectorFactory
    {
        /// <summary>
        /// Creates a <see cref="AppSettingsStatusServiceConnector"/>
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        /// <param name="appSettingInfos">The application settings to display values of.</param>
        /// <returns></returns>
        public static AppSettingsStatusServiceConnector Create(string name, string title, string description, params AppSettingsStatusServiceConnectorConfigurationItem[] appSettingInfos)
        {
            var statusServiceConnector =
                XAct.DependencyResolver.Current.GetInstance<AppSettingsStatusServiceConnector>();

            statusServiceConnector.ConfigureBasicInformation(name, title, description);

            statusServiceConnector.Configuration.AppSettingDefinitions.Add(appSettingInfos);

            return statusServiceConnector;
        }

    }
}
