﻿//namespace XAct.Settings.Services
//{
//    /// <summary>
//    /// Contract for an <c>AppSettings</c>
//    /// based implemention of <see cref="IApplicationSettingsService"/>
//    /// <para>
//    /// Due to the unportable nature of AppSettings
//    /// (only available on Desktop and WebServer apps 
//    /// where Full Framework is required -- 
//    /// not available in other .NET CF, Ag, PCL, XBox, RT, Windows Store)
//    /// avoid using it, and prefer the use of
//    /// <c>IRepositoryApplicationSettingsService</c>
//    /// for your apps.
//    /// </para>
//    /// </summary>
//    public interface IAppSettingsApplicationSettingsService : IApplicationSettingsService, IHasXActLibServiceDefinition
//    {

//    }
//}
