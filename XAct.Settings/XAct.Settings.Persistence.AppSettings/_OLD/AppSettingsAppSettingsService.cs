﻿//// ReSharper disable CheckNamespace
//namespace XAct.Settings.Implementations
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Configuration;
//    using XAct.Diagnostics;
//    using XAct.Domain;
//    using XAct.Services;
//    using XAct.State;

//    /// <summary>
//    /// An AppSettings based implementation of <see cref="IAppSettingsAppSettingsService"/>
//    /// </summary>
//    [DefaultBindingImplementation(typeof(IApplicationSettingsService), BindingLifetimeType.Undefined, Priority.Low)]
//    [DefaultBindingImplementation(typeof(IHostSettingsService), BindingLifetimeType.Undefined, Priority.Low)]
//    public class AppSettingsAppSettingsService : ApplicationSettingsServiceBase, IAppSettingsAppSettingsService, IHostSettingsService
//    {


//        /// <summary>
//        /// The unique name for this implementation of <see cref="ApplicationSettingsServiceBase"/>.
//        /// A SettingsService provider should only save the settings that are attributable to 
//        /// this provider, passing of persitence of other settings to the relevant SettingsService 
//        /// (eg: EFSettingsService).
//        /// </summary>
//        protected string C_UniqueSrcTag = "AppSettingsSettingsService";

//        /// <summary>
//        /// Initializes a new instance of the AppSettingsAppSettingsService class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        /// <param name="applicationStateService">The application state service.</param>
//        /// <param name="appHostSettingsBuilder">The app host settings builder.</param>
//        public AppSettingsAppSettingsService(ITracingService tracingService, IApplicationStateService applicationStateService, IAppHostSettingsBuilder appHostSettingsBuilder) : 
//            base(tracingService,applicationStateService, appHostSettingsBuilder, null)
//        {
//        }


//        /// <summary>
//        /// Initializes the settings.
//        /// Invoked from the <see cref="Settings"/> property
//        /// the first time it is invoked by each SubClass.
//        /// </summary>
//        /// <param name="settings">The settings.</param>
//        protected override void Initialize(Settings settings)
//        {
//            //We have to remove the lock applied by ApplicationSettings:
//            if (settings.Initialized)
//            {
//                ((ISettingsGroupInitializer) settings).Initialize(false, false);
//            }

//            //Convert old NameValueCollection to Dictionary:
//            Dictionary<string, string> dict = ConfigurationManager.AppSettings.ToDictionary();


//            //Use Extension Method to Set variables from Dictionary:
//            settings.SetValues(dict, C_UniqueSrcTag);

//            //Lock settings against further change. This time, it's ok.
//// ReSharper disable RedundantArgumentDefaultValue
//            settings.Initialize(true);
//// ReSharper restore RedundantArgumentDefaultValue
//        }


//        ///// <summary>
//        ///// Retrieves this Settings of the specified Type.
//        ///// </summary>
//        ///// <typeparam name="TSettings">The type of the settings.</typeparam>
//        ///// <param name="contextName">Name of the context.</param>
//        ///// <returns></returns>
//        //public TSettings Retrieve<TSettings>(string contextName = null)
//        //    where TSettings : Settings;



//        /// <summary>
//        /// Persists the <see cref="Settings"/> object.
//        /// </summary>
//        /// <exception cref="System.NotImplementedException"></exception>
//        public override void Persist()
//        {
            
//            System.Configuration.Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

//            //It would be good if we could Type it more precisely,
//            //but it's not needed, as all typed AppSettings are just
//            //subclassed wrappers to the underlying internasls of the core Settings object:
//            Settings settings = this.Retrieve<Settings>();

//            //Loop through Each (Vendor/Module) , looping through its Settings:
//// ReSharper disable RedundantThisQualifier

//            foreach (ModuleSettingGroup moduleSettingsGroup in ((IModuleSettingsGroupAccessor)settings).ModuleSettingsGroupCollection)
//// ReSharper restore RedundantThisQualifier
//            {
//                //Persist Module's Setings:
//                PersistModuleSettings(configuration, moduleSettingsGroup);
//            }
//            //Finally do same with base Settings:
//// ReSharper disable RedundantThisQualifier
//            PersistModuleSettings(configuration, settings);
//// ReSharper restore RedundantThisQualifier


//            configuration.Save(ConfigurationSaveMode.Minimal);

            
//        }

//        private void PersistModuleSettings(System.Configuration.Configuration configuration, ModuleSettingGroup moduleSettingsGroup)
//        {
//            foreach (Setting setting in ((IProfileSettingCollectionAccessor) moduleSettingsGroup).SettingsCollection)
//            {
//                if (setting.Src != C_UniqueSrcTag)
//                {
//                    //Persist only the Settings marked with "APP" or whatever this setting is:
//                    continue;
//                }

//                if (setting.ModelState == OfflineModelState.Unchanged)
//                {
//                    //If it hasn't changed, don't bother saving it:
//                    continue;
//                }

//                if (!setting.IsReadable)
//                {
//                    //If it's not readable, don't see how I'm going to get the new value:
//                    continue;
//                }

//                //It would be good if we could Type it more precisely,
//                //but it's not needed, as all typed AppSettings are just
//                //subclassed wrappers to the underlying internasls of the core Settings object:
//                Settings settings = this.Retrieve<Settings>();
                
//                //setting name will be of format 'mykey' or 'mygroup/mykey'.
//                string settingName = setting.Name;
//                //note that it doesn't have module name on it, so if we are in a module
//                //prepend it to look like 'mymodule:mykey' or 'mymodule:mygroup/mykey'
//// ReSharper disable RedundantThisQualifier
//                if (moduleSettingsGroup != settings)
//// ReSharper restore RedundantThisQualifier
//                {
//                    settingName = moduleSettingsGroup.Name + ":" + settingName;
//                }

//                string typeFullName = setting.ValueType.FullName;
//                string keyNameWithType = "[{0}]{1}".FormatStringInvariantCulture(typeFullName, settingName);
//                string keyNameWithTypeAlt = "[{0}]{1}".FormatStringInvariantCulture(setting.ValueType.Name, settingName);

//                //We hope to serialize it as String, but may run into issues.
//                SerializationMethod serializationMethod = SerializationMethod.String;
//                string str = setting.ValueType.Serialize(setting.Value, ref serializationMethod);
//                //...note that it may now be marked as SerializionMethod.Xml or binary...whatever worked first.

//                //Look for it in the existing collection...should be there, under one of the formats:
//                if (configuration.AppSettings.Settings.AllKeys.Contains(keyNameWithType, x => x))
//                {
//                    configuration.AppSettings.Settings[keyNameWithType].Value = str;
                    
//                }
//                else if (configuration.AppSettings.Settings.AllKeys.Contains(keyNameWithTypeAlt, x => x))
//                {
//                    configuration.AppSettings.Settings[keyNameWithTypeAlt].Value = str;
//                }
//                else if (configuration.AppSettings.Settings.AllKeys.Contains(settingName, x => x))
//                {
//                    configuration.AppSettings.Settings[settingName].Value = str;
//                }
//                else
//                {
//                    throw new ArgumentOutOfRangeException("Could not find {0} in appSettings under any expected key format.".FormatStringInvariantCulture(settingName));
//                }
//            }
//        }
//    }

//}
