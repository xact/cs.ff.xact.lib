namespace XAct.Settings.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for the <see cref="IHasXActLibDbModelBuilder"/>
    /// specific to setting up XActLib HostSettings capabilities.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S01_PreInitialization)]
    public interface IEFSettingsDbModelBuilder : IHasXActLibDbModelBuilder
    {

    }
}