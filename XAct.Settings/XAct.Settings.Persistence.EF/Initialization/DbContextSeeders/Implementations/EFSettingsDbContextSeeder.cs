﻿namespace XAct.Settings.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="IEFSettingsDbContextSeeder"/> contract
    /// to seed the HostSettings tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class EFSettingsDbContextSeeder : XActLibDbContextSeederBase<SerializedApplicationSetting>, IEFSettingsDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EFSettingsDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public EFSettingsDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}
