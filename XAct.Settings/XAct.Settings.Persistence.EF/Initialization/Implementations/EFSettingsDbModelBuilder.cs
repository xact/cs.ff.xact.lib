﻿// ReSharper disable CheckNamespace

namespace XAct.Settings.Initialization.Implementations
// ReSharper restore CheckNamespace
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Settings.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// Implementation of <see cref="IEFSettingsDbModelBuilder"/>
    /// in order to create HostSettings capabilities in a CodeFirst managed Db.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class EFSettingsDbModelBuilder : IEFSettingsDbModelBuilder
    {
        private readonly ITracingService _tracingService;
        private readonly string _typeName;

        /// <summary>
        /// Initializes a new instance of the <see cref="EFSettingsDbModelBuilder"/> class.
        /// </summary>
        public EFSettingsDbModelBuilder(ITracingService tracingService)
        {
            _tracingService = tracingService;
            _typeName = this.GetType().Name;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<SerializedApplicationSetting>)XAct.DependencyResolver.Current.GetInstance<ISerializedApplicationSettingModelPersistenceMap>());
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing: ISerializedApplicationSettingMap");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<ApplicationSettingAudit>)XAct.DependencyResolver.Current.GetInstance<IApplicationSettingAuditModelPersistenceMap>());
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing: IApplicationSettingAuditMap");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<SerializedSettingRenderingInformation>)XAct.DependencyResolver.Current.GetInstance<ISerializedSettingRenderingInformationModelPersistenceMap>());
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing: ISerializedSettingRenderingInformationMap");

        }
    }
}

