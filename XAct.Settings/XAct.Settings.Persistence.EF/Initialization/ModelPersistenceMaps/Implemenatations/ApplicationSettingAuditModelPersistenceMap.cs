﻿namespace XAct.Settings.Initialization.ModelPersistenceMaps.Implemenatations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class ApplicationSettingAuditModelPersistenceMap : EntityTypeConfiguration<ApplicationSettingAudit>, IApplicationSettingAuditModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettingAuditModelPersistenceMap"/> class.
        /// </summary>
        public ApplicationSettingAuditModelPersistenceMap()
        {
            this.ToXActLibTable("ApplicationSettingsAudit");
            


            //Use a Composite Key
            this
                .HasKey(m => m.Id);

            int colOrder = 0;
            int indexMember = 1; //Indexs of db's are 1 based.

            this.Property(x => x.ApplicationTennantId)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id", indexMember++) { IsUnique = true }))
                        .DefineRequiredApplicationTennantId(colOrder++)
                ;

            this.Property(x => x.Id)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id", indexMember++) {IsUnique = true}))
                .DefineRequiredGuidId(colOrder++)
                ;


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);




            this.Property(x => x.SettingFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(x => x.Key)
                .IsRequired()
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++)
                //.HasColumnAnnotation(
                //    "Index",
                //    new IndexAnnotation(
                //        new IndexAttribute("IX_XXX", indexMember++) { IsUnique = false }))
                ;
            this.Property(x => x.SerializationMethod)
                .DefineRequiredSerializationMethod(colOrder++);
                ;

            this.Property(x => x.SerializedValueType)
                .DefineRequired1024CharSerializationValueType(colOrder++);

            this.Property(x => x.SerializedPreviousValue)
                .IsOptional()
                .HasMaxLength(2048)
                .HasColumnOrder(colOrder++)
                ;
            this.Property(x => x.SerializedValue)
                .DefineOptional4000CharSerializationValue(colOrder++);


            this.Property(x => x.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++)
                ;

            this.Property(x => x.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++)
                //.HasColumnAnnotation(
                //    "Index",
                //    new IndexAnnotation(
                //        new IndexAttribute("IX_XXX", indexMember++) { IsUnique = false }))
                ;

            this.Property(x => x.CreatedByOrganisation)
                .IsOptional()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                //.HasColumnAnnotation(
                //    "Index",
                //    new IndexAnnotation(
                //        new IndexAttribute("IX_XXX", indexMember++) { IsUnique = false }))
                ;
            this.Property(x => x.Tag)
                .DefineOptional256CharTag(colOrder++)
                ;
        }
    }
}