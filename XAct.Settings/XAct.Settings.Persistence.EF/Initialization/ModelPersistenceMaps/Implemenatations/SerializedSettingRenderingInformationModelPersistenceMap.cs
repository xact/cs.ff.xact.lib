﻿namespace XAct.Settings.Initialization.ModelPersistenceMaps.Implemenatations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class SerializedSettingRenderingInformationModelPersistenceMap : EntityTypeConfiguration<SerializedSettingRenderingInformation>, ISerializedSettingRenderingInformationModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializedSettingRenderingInformationModelPersistenceMap"/> class.
        /// </summary>
        public SerializedSettingRenderingInformationModelPersistenceMap()
        {
            this.ToXActLibTable("SettingsRenderingInformation");


            //Use a Composite Key
            this
                .HasKey(m => m.Id);


            int colOrder = 0;
            //var indexMember = 1; //Index is 1 based.

            this
                .Property(m => m.Key)
                .IsRequired()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                //.HasColumnAnnotation(
                //"Index",
                //new IndexAnnotation(
                //    new IndexAttribute("IX_EnvAppZoneHostKeyUser", indexMember++) { IsUnique = true }))
                ;

            this
                .Property(m => m.RenderHintsIdentifier)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.RenderGroupingHints)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.RenderOrderHint)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            

            this
                .Property(m => m.RenderingImageHints)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            
            this
                .Property(m => m.RenderLabelHints)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            
            this
                .Property(m => m.RenderViewControlHints)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            
            this
                .Property(m => m.RenderEditControlHints)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.RenderEditValidationHints)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

        }
    }
}