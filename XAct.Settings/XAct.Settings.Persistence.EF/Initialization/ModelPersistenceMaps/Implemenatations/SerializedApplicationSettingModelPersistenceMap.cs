﻿namespace XAct.Settings.Initialization.ModelPersistenceMaps.Implemenatations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    /// <summary>
    /// EF Map to describe how to persist 
    /// <see cref="SerializedApplicationSetting"/>
    /// </summary>
    public class SerializedApplicationSettingModelPersistenceMap : EntityTypeConfiguration<SerializedApplicationSetting>, ISerializedApplicationSettingModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializedApplicationSettingModelPersistenceMap"/> class.
        /// </summary>
        public SerializedApplicationSettingModelPersistenceMap()
        {
            this.ToXActLibTable("ApplicationSettings");

            

            int colOrder = 0;
            int indexMember = 1; //Indexs of db's are 1 based.

            //Use a Composite Key
            this
                .HasKey(m => m.Id);
                
                //.HasKey(x =>
                //        new
                //            {
                //                x.EnvironmentIdentifier,
                //                x.ZoneOrTierIdentifier,
                //                x.HostIdentifier,
                //                x.Key
                //            });


            //We do *NOT* persist Scope.
            this.Ignore(x => x.Scope);

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++)
                ;

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++)
                ;


            this.Property(x => x.EnvironmentIdentifier)
                .IsRequired()
                .HasMaxLength(16)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_EnvAppZoneHostTennantKey", indexMember++) {IsUnique = true}))
                ;

            this.Property(x => x.ZoneOrTierIdentifier)
                .IsRequired()
                .HasMaxLength(16)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_EnvAppZoneHostTennantKey", indexMember++) {IsUnique = true}))
                ;
            this.Property(x => x.HostIdentifier)
                .IsRequired()
                .HasMaxLength(16)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_EnvAppZoneHostTennantKey", indexMember++) {IsUnique = true}))
                ;

            this.Property(x => x.TennantIdentifier)
                .IsRequired()
                //.HasMaxLength(32)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_EnvAppZoneHostTennantKey", indexMember++) {IsUnique = true}))
                ;

            this.Property(x => x.Key)
                .IsRequired()
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_EnvAppZoneHostTennantKey", indexMember++) {IsUnique = true}))
                ;

            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++)
                ;

            this.Property(x => x.SerializationMethod)
                .DefineRequiredSerializationMethod(colOrder++);

            this.Property(x => x.SerializedValueType)
                .DefineRequired1024CharSerializationValueType(colOrder++);

            this.Property(x => x.SerializedValue)
                .DefineOptional4000CharSerializationValue(colOrder++);
                

            this.Property(x => x.SerializedDefaultValue)
                .IsOptional()
                //Was MaxLength...but realized (after EF's AddOrUpdate balked at it)
                //that if I've got something that big in a Setting, something is wrong.
                .HasMaxLength(2048)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.IsUnlockedInformation)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;
            
            this.Property(x => x.IsReadableAuthorisationInformation)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.IsWritableAuthorisationInformation)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++)
                ;

            this.Property(x => x.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++)
                ;

            this.Property(x => x.Tag)
                .DefineOptional256CharTag(colOrder++)
                ;

            this.Property(x => x.Metadata)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;

        }

    }
}