﻿namespace XAct.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public class HostSetting : SerializedObject,IHasEnabled
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HostSetting"/> class.
        /// </summary>
        HostSetting():base(){}

        /// <summary>
        /// Gets or sets a value indicating whether this setting is enabled or not.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled { get; set; }
    }
}
