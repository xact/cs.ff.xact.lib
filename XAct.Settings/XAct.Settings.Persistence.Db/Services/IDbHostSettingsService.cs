namespace XAct.Settings
{
    /// <summary>
    /// A specialization of the <see cref="IApplicationSettingsService"/>
    /// to retrieve and cache settings retrieved from a Db, 
    /// falling back to AppHost settings if not found.
    /// </summary>
    public interface IDbHostSettingsService :IApplicationSettingsService, IHasXActLibService
    {

    }
}