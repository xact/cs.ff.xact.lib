using System;
using System.Collections.Generic;
using XAct.Caching;
using XAct.Diagnostics;
using XAct.Services;

namespace XAct.Settings
{
    /// <summary>
    /// An implementation of <see cref="IHostSettingsService"/>
    /// to retrieve and cache settings retrieved from a Db, 
    /// falling back to AppHost settings if not found.
    /// </summary>
    //[DefaultBindingImplementation(typeof(IDbHostSettingsService), Priority=Priority.Normal)] //Supercede over 
    public abstract class DbHostSettingsServiceBase : IDbHostSettingsService
    {
        private readonly ITracingService _tracingService;
        private readonly ICachingService _cachingService;
        private readonly IHDHostSettingsService _hdHostSettingsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbHostSettingsServiceBase"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="cachingService">The caching service.</param>
        /// <param name="hdHostSettingsService">The hd host settings service.</param>
        protected DbHostSettingsServiceBase(ITracingService tracingService, ICachingService cachingService, IHDHostSettingsService hdHostSettingsService)
        {
            _tracingService = tracingService;
            _cachingService = cachingService;
            _hdHostSettingsService = hdHostSettingsService;
        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key, bool throwExceptionOnConversionException = true)
        {
            Dictionary<string, object> cachedValues = GetCachedSettings;


            if (cachedValues.ContainsKey(key))
            {
                if (throwExceptionOnConversionException)
                {
                    return cachedValues[key].ConvertTo<TValue>();
                }else
                {
                  try
                  {
                    return cachedValues[key].ConvertTo<TValue>();

                  }catch
                  {
                   //fall back to apphost   
                  }
                }
            }

            return _hdHostSettingsService.Get<TValue>(key, throwExceptionOnConversionException);
        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key, TValue defaultValue, bool throwExceptionOnConversionException = true)
        {
            Dictionary<string, object> cachedValues = GetCachedSettings;


            if (cachedValues.ContainsKey(key))
            {
                if (throwExceptionOnConversionException)
                {
                    return cachedValues[key].ConvertTo<TValue>();
                }else
                {
                  try
                  {
                    return cachedValues[key].ConvertTo<TValue>();

                  }catch
                  {
                   //fall back to apphost   
                  }
                }
            }

            return _hdHostSettingsService.Get<TValue>(key, defaultValue, throwExceptionOnConversionException);
        }

        /// <summary>
        /// Gets the cached settings.
        /// 
        /// </summary>
        /// <returns></returns>
        protected Dictionary<string, object> GetCachedSettings 
        {
            get
            {
                Dictionary<string, object> cachedResults;

                TimeSpan timespan = _hdHostSettingsService.Get<TimeSpan>("HostSettingsCacheTimeSpan",
                                                                         TimeSpan.FromMinutes(1), false);

                _cachingService.TryGet(this.GetType().FullName, out cachedResults, RetrieveSettings, timespan, true);

                return cachedResults;
            }
        }

        /// <summary>
        /// Retrieves the settings.
        /// </summary>
        /// <returns></returns>
        protected abstract Dictionary<string, object> RetrieveSettings();


        /// <summary>
        /// Retrieves this instance.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Settings Retrieve()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Persists this instance.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Persist()
        {
            throw new NotImplementedException();
        }
    }
}