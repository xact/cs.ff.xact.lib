namespace XAct.Settings.AppSettings.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Settings.Implementations;
    using XAct.Settings;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class HostSettingsServiceTests
    { 
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIHostSettingsService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IHostSettingsService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }


        [Test]
        public void CanGetIHostSettingsServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IHostSettingsService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(AppSettingsHostSettingsService), service.GetType());
        }



        [Test]
        public void EnsureCanGetAppSettingsValue()
        {
            IHostSettingsService hostSettingsService = XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();

            string check = hostSettingsService.Get<string>("HostSettingsTest");
            Assert.AreEqual("ok", check);
        }




        [Test]
        public void EnsureCanGetAppSettingsValueEvenIfNotSet()
        {
            IHostSettingsService hostSettingsService = XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();

            int check = hostSettingsService.Get<int>("HostSettingsTestInt");
            Assert.AreEqual(3, check);
            
        }


        [Test]
        public void CanGetToConfigurationSettings()
        {
            IHostSettingsService hostSettingsService = XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();

            IAppSettingsHostSettingsServiceConfiguration config =
                hostSettingsService.Configuration as IAppSettingsHostSettingsServiceConfiguration;

            Assert.IsNotNull("VDir", config.VDirAppSettingKey);
        }


        [Test]
        public void GetSettingValueOverriddenByNConfigValue()
        {
            IHostSettingsService hostSettingsService = XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();

            string check = hostSettingsService.Get<string>("HostSettingsTest3");

            Assert.IsNotNull(check,"Not Empty");

            //It doesn't happen as the Bootstrap app.config crushes the local NConfig app.config 

            Assert.AreEqual("Overridden...", check, "Value is not overridden: {0}".FormatStringInvariantCulture(check));
        }


        [Test]
        public void GetSettingValueOverriddenByNConfigValue2()
        {
            IHostSettingsService hostSettingsService = XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();

            string check = hostSettingsService.Get<string>("HostSettingsTestNConfig");
            Assert.IsNotNull(check, "Not Empty");
            //Value comes from Bootstrap nconfig file -- not the local one. Surprising. 
            Assert.AreEqual("Overridden...", check, "Value is not overridden: {0}".FormatStringInvariantCulture(check));
        }


    }


}


