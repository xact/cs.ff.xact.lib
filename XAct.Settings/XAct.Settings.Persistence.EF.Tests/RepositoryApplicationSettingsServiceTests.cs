// ReSharper disable CheckNamespace
namespace XAct.Tests
// ReSharper restore CheckNamespace
{
    using System.Diagnostics;
    using Implementations;
    using XAct.Settings;
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Implementations;
    using XAct.Domain.Repositories;
    using XAct.Settings.Implementations;
    using XAct.Settings.Implementations.Configuration;

    /*
     * The seed data is:
     * 
ApplicationName		Host	Key		Enabled	Order	Ser.Method	SerializedValue	SerializedValueType
							Key1	1		0		1			Val1A	System.String
							KeyB1	1		0		1			Val:Unspecific	System.String
							KeyB2	1		0		1			Val:Unspecific	System.String
							KeyB3	1		0		1			Val:Unspecific	System.String
							KeyB4	1		0		1			Val:Unspecific	System.String
							KeyB5	1		0		1			Val:Unspecific	System.String
					STNSI06	Key1	1		0		1			Val1B	System.String
					STNSI06	KeyB1	1		0		1			Val:MachineSpecific	System.String
QTAgent32_40.exe			Key1	1		0		1			Val1C	System.String
QTAgent32_40.exe			Key2	1		0		1			Val2C	System.String
QTAgent32_40.exe			Key3	1		0		1			Val3C	System.String
QTAgent32_40.exe			KeyB2	1		0		1			Common value overridden by Application Name.	System.String
QTAgent32_40.exe			KeyB3	1		0		1			Common value overridden by Application Name.	System.String
QTAgent32_40.exe	STNSI06	Key1	1		0		1			Val1D	System.String
QTAgent32_40.exe	STNSI06	KeyB3	1		0		1			Common value overridden by Application and Machine Name.	System.String

     */




    [TestFixture]
    public class RepositoryApplicationSettingsServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
#pragma warning disable 168
            //Force reference, so it gets comied to unit test's bin directory.
            RepositoryApplicationSettingsService x;
#pragma warning disable 219
            TraceSwitchServiceState y;
#pragma warning restore 219
// ReSharper disable RedundantAssignment
            y = null;
// ReSharper restore RedundantAssignment
#pragma warning restore 168

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            SetupApplicationSettingsServiceConfiguration();
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }




        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanAccessTracingService()
        {
            ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();
            
            Assert.IsNotNull(tracingService);
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanAccessTheDb()
        {
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
#pragma warning disable 168
            int count = repository.Count<SerializedApplicationSetting>();
#pragma warning restore 168

            Assert.IsTrue(count>0);
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void TheAppSettingsIsActuallyTheEFBasedSetting()
        {

            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            bool righService = service is RepositoryApplicationSettingsService;

            Assert.IsTrue(righService);
        }



        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTheCurrentContext()
        {


            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            Settings settings = service.Current;

            Assert.IsNotNull(settings);
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetSettingsCount()
        {

            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            List<Setting> settings = SettingsExtensions.GetAllSettings(service.Current);


            List<string> check = new List<string>();
            foreach (Setting setting in settings)
            {
                string line = "{0}={1}".FormatStringInvariantCulture(setting.Name, setting.Value);
                check.Add(line);
                Console.WriteLine(line);
            }
            Assert.IsTrue(settings.Count>9);
        }








        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetOneOfTheSeededValuesCorrectValue()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();
            string o = service.Current.GetSettingValue<string>("KeyDEF");
            Assert.AreEqual("ValDEF:EnvSpecific", o);
        }

        

        

        

        
        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetEnvSpecificOverride()
        {

            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            /*
ApplicationName		Host	Key		Enabled	Order	Ser.Method	SerializedValue	SerializedValueType
							KeyB1	1		0		1			Val:Unspecific	System.String
					STNSI06	KeyB1	1		0		1			Val:MachineSpecific	System.String
              */
            string o = service.Current.GetSettingValue<string>("KeyB1");

            Assert.AreEqual("Val:EnvSpecific", o);
        }



        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetAMacineSpecificOverride()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();



            /*
            QTAgent32_40.exe			Key2	1		0		1			Val2C	System.String
             */

            string o = service.Current.GetSettingValue<string>("KeyB4");

            Assert.AreEqual("Val:MachSpecific", o);
        }

        










        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetServiceConfiguration()
        {
            //ARRANGE:
            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            //ACT:

            //ASSERT:
            var config = service.Configuration;
            Assert.IsNotNull(config);
        }


        [Test]
        [InitializeUnitTestDbIoCContext(true)]
        public void SetModValues()
        {
            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            IRepositoryApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(
                configuration.EnvironmentIdentifier, 
                null, 
                null, 
                Guid.Empty);

            service.Initialize(settings);


            var module = settings["Mod1"];
            Assert.IsNotNull(module, "Module should exists.");
        }

        [Test]

        [InitializeUnitTestDbIoCContext(true)]
        public void SetModValuesAndRetrieve()
        {
            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            IRepositoryApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(configuration.EnvironmentIdentifier, null, null, Guid.Empty);

            service.Initialize(settings);


            var module = settings["Mod1"];
            Assert.IsNotNull(module,"Module should exists.");

            string o = module.GetSettingValue<string>("KeyD1");

            Assert.IsTrue(o.StartsWith("Val:EnvSpecific"),"String found");
        }




        [Test]
        [InitializeUnitTestDbIoCContext(true)]
        public void GetModSubMod()
        {
            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            IRepositoryApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(
                configuration.EnvironmentIdentifier, 
                null, null, 
                Guid.Empty);

            service.Initialize(settings);


            var module = settings["Mod1"]["Mod2"];
            Assert.IsNotNull(module, "Module should exists.");
        }

        [Test]
        [InitializeUnitTestDbIoCContext(true)]
        public void GetModSubModValueWithTab()
        {
            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            IRepositoryApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(
                configuration.EnvironmentIdentifier, null, null, Guid.Empty);

            service.Initialize(settings);


            var module = settings["Mod1"]["Mod2"];
            var o = module.GetSettingValue<string>("KeyE1");
            Assert.AreEqual("Foo:\tX",o);
        }

        


        [Test]
        [InitializeUnitTestDbIoCContext(true)]
        public void GetModSubModValueWithEscapedTab()
        {
            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            IRepositoryApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = 
                new ApplicationSettingScope(
                    configuration.EnvironmentIdentifier, 
                    null, 
                    null, 
                    Guid.Empty);

            service.Initialize(settings);


            var module = settings["Mod1"]["Mod2"];
            var o = module.GetSettingValue<string>("KeyE2");
            Assert.AreEqual("Foo:\\tX",o);
        }


        [Test]
        [InitializeUnitTestDbIoCContext(true)]
        public void GetDefaultSettingIfValueNotSet()
        {
            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            IRepositoryApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(configuration.EnvironmentIdentifier, null, null, Guid.Empty);

            service.Initialize(settings);


            var module = settings["Mod1"]["Mod2"];

            string o;
            module.TryGetSettingValue<string>("NonExistentSetting", out o,"a default value");

            Assert.AreEqual("a default value",o);
        }

        [Test]
        [InitializeUnitTestDbIoCContext(true)]
        public void GetDefaultSettingUnlessValueIsSet()
        {
            IRepositoryApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsService>();

            IRepositoryApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(configuration.EnvironmentIdentifier, null, null, Guid.Empty);

            service.Initialize(settings);


            var module = settings["Mod1"]["Mod2"];

            string o;
            module.TryGetSettingValue<string>("KeyE2", out o, "a default value");

            Assert.AreNotEqual("a default value", o);
            Assert.AreEqual("Foo:\\tX", o);
        }





        private static void SetupApplicationSettingsServiceConfiguration()
        {
            //Get singleton configuration package:
            IRepositoryApplicationSettingsServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IRepositoryApplicationSettingsServiceConfiguration>();


            configuration.Initialize((ApplicationSettings)null, null, null, null);

        }



        
    }


}


