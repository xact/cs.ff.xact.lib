﻿// ReSharper disable CheckNamespace
namespace XAct.Tests
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using Implementations;
    using XAct.Settings;
    using NUnit.Framework;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Implementations;
    using XAct.Domain.Repositories;
    using XAct.Settings.Implementations;


    [TestFixture]
    public class ApplicationSettingsService_PersistanceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
#pragma warning disable 168
            //Force reference, so it gets comied to unit test's bin directory.
            RepositoryApplicationSettingsService x;
#pragma warning disable 219
            TraceSwitchServiceState y;
#pragma warning restore 219
            // ReSharper disable RedundantAssignment
            y = null;
            // ReSharper restore RedundantAssignment
#pragma warning restore 168

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            //DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>().FactoryDelegate =
            //    () => new EntityDbContext(new UnitTestDbContext());

            //Initialize the db using the common initializer passing it one entity from this class
            //so that it can perform a query (therefore create db as necessary):
            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<SerializedSetting>();


            SetupApplicationSettingsServiceConfiguration();

        }


        private static void SetupApplicationSettingsServiceConfiguration()
        {
            //Get singleton configuration package:
            IApplicationSettingsServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();


            configuration.Initialize((ApplicationSettings) null, null, null, null);

        }





        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanAccessTracingService()
        {
            ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();

            Assert.IsNotNull(tracingService);
        }




        [Test]
        public void MakeChangesToCurrentSettingAndPersist()
        {


            IApplicationSettingsService service =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();
            ApplicationSettings settings = service.Current;
            Assert.IsNotNull(settings);

            string val;
            val = service.Current.GetSettingValue<string>("KeyB1");

            Assert.AreEqual("Val:EnvSpecific", val,"KeyB1 should have a value - not:" + val);

            service.Current.SetSettingValue<string>("KeyB2", "Updated @*@*@*@*@*@*@*@*@*@*");
            val = service.Current.GetSettingValue<string>("KeyB2");
            Assert.IsTrue(val.StartsWith("Updated"),"New value for KeyB2 should have started with 'Updated'.");

            //
            service.Current.AddSetting<int>("SomeNumber", 12345,
                                            new SettingEditingMetadata(
                                                //service.Current.ContextIdentifier, 
                                                0)
                );
            
            //THis is a check...we are Committing, but since we didn't invoke Persist first, 
            //there should be no updates, nor additions:
            XAct.DependencyResolver.Current.GetInstance<IRepositoryService>().GetContext().Commit(CommitType.Default);


            var grabBag = new Dictionary<string, object>();
            grabBag["Tag"] = "foo";
            grabBag["CreatedByOrganisation"] = "bar";
            //NOW...we persist...
            //And new setting should have an id...
            service.Persist(settings, grabBag);



            System.Threading.Thread.Sleep(100);

            Setting setting;
            settings.TryGetSetting("SomeNumber", out setting);

            var before = setting.Id;
            Assert.IsTrue(setting.Id != Guid.Empty, "setting.Id should not be Guid.Empty as not retrieved.");
            XAct.DependencyResolver.Current.GetInstance<IRepositoryService>().GetContext().Commit(CommitType.Default);

            Assert.IsTrue(setting.Id != Guid.Empty, "setting.Id should not be be Guid.Empty after Commit finished.");
            Assert.IsNotNull(setting.Value, "SomeNumber should have value of 12345.");

            Assert.AreEqual(setting.Id,before,"Ids should still be the same.");

            //And now we persist:
            //...which should trigger one update, one add:
            XAct.DependencyResolver.Current.GetInstance<IRepositoryService>().GetContext().Commit(CommitType.Default);

            settings.TryGetSetting("SomeNumber", out setting);

            Assert.IsTrue(setting.Id != Guid.Empty, "setting.Id - retrieved - should not be Guid.Empty");
            Assert.IsNotNull(setting.Value, "SomeNumber - retrieved - should have value of 12345.");


            Assert.AreEqual(setting.Id, before, "Ids should still be the same (2).");
            
            //Cool....one more time:


            Setting settingX;
            settings.TryGetSetting("KeyB2", out settingX);
            settingX.Value ="Updated #*#*#*#*#*#*#*#*#*#*";
            System.Threading.Thread.Sleep(100);




            //Yeah...but don't forget to commit ;-)
            settings.TryGetSetting("SomeNumber", out setting);
            Assert.IsTrue(setting.Id != Guid.Empty, "SomeNumber.Id should not be Guid.Empty...");
            Assert.IsNotNull(setting.Value,"SomeNumber.Value should not be null...should still be 12345");


            //Cool....one more time, but this time with no auditing

            //Changed:
            bool hold = service.Configuration.AuditChanges;
            service.Configuration.AuditChanges = false;
            service.Persist(settings, grabBag);

            XAct.DependencyResolver.Current.GetInstance<IRepositoryService>().GetContext().Commit(CommitType.Default);


            //Changed:
            service.Configuration.AuditChanges = hold;

        }





        [Test]
        public void EnsureMountedSettingsHasGuid()
        {



            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();
            ApplicationSettings settings = service.Current;
            Assert.IsNotNull(settings);

            Setting setting;
            service.Current.TryGetSetting("KeyB3", out setting);


            Assert.IsTrue(setting.Id!=Guid.Empty);
        }


    }
}
