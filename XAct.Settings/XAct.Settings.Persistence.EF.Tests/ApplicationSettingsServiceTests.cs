// ReSharper disable CheckNamespace
namespace XAct.Tests
// ReSharper restore CheckNamespace
{
    using System.Diagnostics;
    using Implementations;
    using XAct.Settings;
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Implementations;
    using XAct.Domain.Repositories;
    using XAct.Settings.Implementations;





    /// <summary>
    /// General tests, using ApplicationSettingsService,
    /// which in turn wraps RepositoryApplicationSettingsService
    /// </summary>
    [TestFixture]
    public class ApplicationSettingsServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
#pragma warning disable 168
            //Force reference, so it gets comied to unit test's bin directory.
            RepositoryApplicationSettingsService x;
#pragma warning disable 219
            TraceSwitchServiceState y;
#pragma warning restore 219
// ReSharper disable RedundantAssignment
            y = null;
// ReSharper restore RedundantAssignment
#pragma warning restore 168

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            SetupApplicationSettingsServiceConfiguration();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanAccessTheDb()
        {
            IRepositoryService repository = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
#pragma warning disable 168
            int count = repository.Count<SerializedApplicationSetting>();
#pragma warning restore 168

            Assert.IsTrue(count>0);
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void TheAppSettingsIsActuallyTheEFBasedSetting()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            bool righService = service is RepositoryApplicationSettingsService;

            Assert.IsTrue(righService);
        }



        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTheCurrentContext()
        {


            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Settings settings = service.Current;

            Assert.IsNotNull(settings);
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetSettingsCount()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            List<Setting> settings = SettingsExtensions.GetAllSettings(service.Current);


            List<string> check = new List<string>();
            foreach (Setting setting in settings)
            {
                string line = "{0}={1}".FormatStringInvariantCulture(setting.Name, setting.Value);
                check.Add(line);
                Console.WriteLine(line);
            }

            Assert.IsTrue(settings.Count>9);
        }






        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetOneOfTheSeededValuesCorrectValue()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();



            /*
            QTAgent32_40.exe			Key2	1		0		1			Val2C	System.String
             */

            string o = service.Current.GetSettingValue<string>("KeyDEF");

            Assert.AreEqual("ValDEF:EnvSpecific", o);
        }

        

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_EnvSpecific()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);
            string o = service.Current.GetSettingValue<string>("KeyB1");

            Assert.AreEqual("Val:EnvSpecific", o);
        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_ZoneSpecific_ButWrongZone()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB2");

            Assert.AreEqual("Val:UnSpecific", o); //And not 'BackTierSpecific'
        }



        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_EnvBackTierSpecific_ButNoZone()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB3");

            Assert.AreEqual("Val:UnSpecific", o); //And not 'EnvBackTierSpecific'
        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_EnvBackTierSpecific_ButCorrectZone()
        {


            //Change Zone:
            IApplicationSettingsServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();
            configuration.ZoneOrTierIdentifier = "BackTier";

            Trace.WriteLine("Env:" + configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + configuration.HostIdentifier);

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();
            string o = service.Current.GetSettingValue<string>("KeyB3");

            Assert.AreEqual("Val:EnvBackTierSpecific", o);

        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_MachineSpecific()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB4");

            Assert.AreEqual("Val:MachSpecific", o); //And not 'EnvBackTierSpecific'
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_ZoneMachineSpecific_ButWrongZone()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB5");

            Assert.AreEqual("Val:UnSpecific", o); //And not 'BackTierMachSpecific'

        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_ZoneMachineSpecific_ButRightZone()
        {



            //Change Zone:
            IApplicationSettingsServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();
            configuration.ZoneOrTierIdentifier = "BackTier";




            IApplicationSettingsService service =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB5");

            Assert.AreEqual("Val:BackTierMachSpecific", o);

        }




        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_EnvMachineSpecific()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB6");

            Assert.AreEqual("Val:EnvMachSpecific", o); 
        }



        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_EnvZoneMachineSpecific_ButWrongZone()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB7");

            Assert.AreEqual("Val:UnSpecific", o);//And not EnvBackTierMachSpecific
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_EnvZoneMachineSpecific_ButCorrectZone()
        {

            //Change Zone:
            IApplicationSettingsServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();
            configuration.ZoneOrTierIdentifier = "BackTier";

            IApplicationSettingsService service =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB7");

            Assert.AreEqual("Val:EnvBackTierMachSpecific", o); //And not EnvBackTierMachSpecific
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_EnvZoneMachineSpecific_ButFrontZone()
        {

            //Change Zone:
            IApplicationSettingsServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();
            configuration.ZoneOrTierIdentifier = "FrontTier";

            IApplicationSettingsService service =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);

            string o = service.Current.GetSettingValue<string>("KeyB7");

            Assert.AreEqual("Val:EnvBackTierMachSpecific", o); //And not EnvBackTierMachSpecific
        }




        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetOverride_InitializeASettingAsIfItIsOnBackTier()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            IApplicationSettingsServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(
                configuration.EnvironmentIdentifier,
                "BackTier",
                null,
                configuration.TennantIdentifier);

            service.Initialize(settings);

            string o = settings.GetSettingValue<string>("KeyC1");

            Console.WriteLine("{0}={1}".FormatStringCurrentCulture("KeyC1", o));

            Assert.IsTrue(o.Contains("BackTier"));
        }




















        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void RealWorld_CheckParsingOfInitMethod()
        {

            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            IApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();


            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = 
                new ApplicationSettingScope(
                    configuration.EnvironmentIdentifier, 
                    configuration.ZoneOrTierIdentifier, //Was string.empty in original tests. Still the case?
                    configuration.HostIdentifier,
                    configuration.TennantIdentifier);

            service.Initialize(settings);

            Setting setting;
            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);
            bool found = settings.TryGetSetting("Services/IQOffice/ServiceEndpointAddress", out setting);


            Assert.IsTrue(found);

            Assert.AreEqual("http://123.456.11.146:4001/", setting.Value,"Are same value");

        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void RealWorld_CheckParsingOfInitMethodWithZoneDefined()
        {

            IApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();

            //string hold = configuration.ZoneOrTierIdentifier;

            configuration.ZoneOrTierIdentifier = "BackTier";

            
            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();



            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(
                configuration.EnvironmentIdentifier, 
                "BackTier",
                configuration.HostIdentifier,
            configuration.TennantIdentifier);

            service.Initialize(settings);

            Setting setting;
            Trace.WriteLine("Env:" + service.Configuration.EnvironmentIdentifier);
            Trace.WriteLine("Zone:" + service.Configuration.ZoneOrTierIdentifier);
            Trace.WriteLine("Host:" + service.Configuration.HostIdentifier);
            bool found = settings.TryGetSetting("X/Services/IQOffice/ServiceEndpointAddress", out setting);

            Trace.WriteLine("Zone:" + configuration.ZoneOrTierIdentifier);

            
            //X/Services/IQOffice/ServiceEndpointAddress", "http://123.456.11.146:4001/", true, null, null, "BackTier"
            Assert.IsTrue(found,"Setting was not found.");

            object value = setting.Value;
           Trace.WriteLine("Value:" + value);

            Assert.AreEqual("http://123.456.11.146:4001/", value, "Are same value");

        }



        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void RealWorld_ImportsDescriptionAndMetadata()
        {

            IApplicationSettingsServiceConfiguration configuration =
    XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();

            //string hold = configuration.ZoneOrTierIdentifier;

            configuration.ZoneOrTierIdentifier = "BackTier";


            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();



            ApplicationSettings settings = new ApplicationSettings();
            settings.ContextIdentifier = new ApplicationSettingScope(
                configuration.EnvironmentIdentifier,
                "BackTier",
                configuration.HostIdentifier,
            configuration.TennantIdentifier);

            service.Initialize(settings);

            Setting setting;
            bool found = settings.TryGetSetting("X/Services/IQOffice/ServiceEndpointAddress", out setting);

            Trace.WriteLine("Zone:" + configuration.ZoneOrTierIdentifier);


            //X/Services/IQOffice/ServiceEndpointAddress", "http://123.456.11.146:4001/", true, null, null, "BackTier"
            Assert.IsTrue(found, "Setting was not found.");

            object value = setting.Value;
            Trace.WriteLine("Value:" + value);

            Assert.AreEqual("http://123.456.11.146:4001/", value, "Are same value");

            Assert.IsNotNullOrEmpty(setting.Description,"Description was null");

            Assert.IsNotNullOrEmpty(setting.Metadata,"Metadata was null");


        }




        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void GetServiceConfiguration()
        {
            //ARRANGE:
            IApplicationSettingsService service = XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            //ACT:

            //ASSERT:
            var config = service.Configuration;
            Assert.IsNotNull(config);
        }














        private static void SetupApplicationSettingsServiceConfiguration()
        {
            //Get singleton configuration package:
            IApplicationSettingsServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsServiceConfiguration>();


            configuration.Initialize((ApplicationSettings)null, null, null, null);

        }



        
    }


}


