﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using XAct.Data.EF.CodeFirst;
    using XAct.Domain.Repositories;
    using XAct.Library.Settings;
    using XAct.Settings;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;
    using XAct.Settings.Initialization.DbContextSeeders;

    /// <summary>
    /// 
    /// </summary>
    public class UnitTestsEFSettingsDbContextSeeder : XActLibDbContextSeederBase<SerializedApplicationSetting>, IEFSettingsDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="EFSettingsDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="repositoryService"></param>
        public UnitTestsEFSettingsDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
            IRepositoryService repositoryService
            )
            : base(tracingService)
        {
            _environmentService = environmentService;
            _repositoryService = repositoryService;
        }



        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x =>
                new
                {
                    x.EnvironmentIdentifier,
                    x.ZoneOrTierIdentifier,
                    x.HostIdentifier,
                    x.Key
                });
        }


        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities() 
        {


            var env_ = _environmentService.EnvironmentName;
            var zon_ = "BackTier";
            var mach = _environmentService.MachineName;

            this.InternalEntities = new List<SerializedApplicationSetting>();


            //to prove General settings:
            //Val1A => Val1:EnvSpecific
            //Val1B => Val1:EnvAndHostSpecific
            //Val1D => Val1:
            //Val1:EnvMachineSpecific


            //----------------------------------------------
            //First, check that it can deterin differentiate between KeyABC and KeyDEF
            //regardless of Env/Zone/Machine:
            CreateEntity("KeyABC", "ValABC:EnvSpecific", enabled: true,
                                description: null, metadata: null,
                                environmentIdentifier: env_, zoneOrTierIdentifier: null, hostIdentifier: null,
                                isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER",
                               isWritableAuthorisationInformation: "ACCOUNTANT");
            CreateEntity("KeyDEF", "ValDEF:EnvSpecific", enabled: true,
                                description: null, metadata: null,
                                environmentIdentifier: env_, zoneOrTierIdentifier: null, hostIdentifier: null,
                                isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER",
                               isWritableAuthorisationInformation: "ACCOUNTANT");

            //----------------------------------------------
            //Second, prove that Overrides are per context(host should beet out all other options):

            //----------------------------------------------
            //to prove we can fine grain between app, zone, machine:
            //Base values setup for the next part:
            CreateEntity("KeyB1", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyB2", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

 
            CreateEntity("KeyB3", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            
            CreateEntity("KeyB4", "Val:UnSpecific", enabled: true,
                                 description: null, metadata: null,
                              environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");
 
            CreateEntity("KeyB5", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyB6", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null,
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyB7", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null,
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyB8", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null,
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyB9", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null,
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyB10", "Val:UnSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null,
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");
            //----------------------------------------------
            //----------------------------------------------
            //Override :
            CreateEntity("KeyB1", "Val:EnvSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: env_, zoneOrTierIdentifier: null, hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");
            //----------------------------------------------
            CreateEntity("KeyB2",
                               "Val:BackTierSpecific",
                               enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: null, zoneOrTierIdentifier: zon_, hostIdentifier: null,
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");
            //----------------------------------------------
            CreateEntity("KeyB3",
                               "Val:EnvBackTierSpecific",
                               enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: env_, zoneOrTierIdentifier: zon_, hostIdentifier: null,
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");
            //----------------------------------------------

            CreateEntity("KeyB4",
                   "Val:MachSpecific",
                   enabled: true,
                    description: null, metadata: null,
                   environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: mach,
                   isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");


            CreateEntity("KeyB5",
                   "Val:BackTierMachSpecific",
                   enabled: true,
                    description: null, metadata: null,
                   environmentIdentifier: null, zoneOrTierIdentifier:zon_, hostIdentifier: mach,
                   isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");


            CreateEntity("KeyB6",
                   "Val:EnvMachSpecific",
                   enabled: true,
                    description: null, metadata: null,
                   environmentIdentifier: env_, zoneOrTierIdentifier: null, hostIdentifier: mach,
                   isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyB7",
                   "Val:EnvBackTierMachSpecific",
                   enabled: true,
                    description: null, metadata: null,
                   environmentIdentifier: env_, zoneOrTierIdentifier: zon_, hostIdentifier: mach,
                   isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");


            CreateEntity("KeyB7",
                   "Val:EnvBackTierMachSpecific",
                   enabled: true,
                    description: null, metadata: null,
                   environmentIdentifier: env_, zoneOrTierIdentifier: "FrontTier", hostIdentifier: mach,
                   isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");



            //To proves we can isolate front and back settings separately:
            CreateEntity("KeyC1",
                               "Val:EnvAndBackTierSpecific",
                               enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: env_, zoneOrTierIdentifier: "BackTier", hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyC1", "Val:EnvAndFrontTierSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: env_, zoneOrTierIdentifier: "FrontTier", hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyC1", "Val:EnvSpecific", enabled: true,
                                description: null, metadata: null,
                               environmentIdentifier: env_, zoneOrTierIdentifier: null, hostIdentifier: null, 
                               isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("KeyC2", "Val:EnvAndBackTierSpecific", enabled: true,
                                description: null, metadata: null,
                                environmentIdentifier: env_, zoneOrTierIdentifier: "BackTier", hostIdentifier: null, 
                                isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            //To prove we can parse modules:
            CreateEntity("Mod1/KeyD1", "Val:EnvSpecific", enabled: true,
                                description: null, metadata: null,
                                environmentIdentifier: env_, zoneOrTierIdentifier: null, hostIdentifier: null, 
                                isUnlockedInformation: null, isReadableAuthorisationInformation: "AUTENTICATED_USER", isWritableAuthorisationInformation: "ACCOUNTANT");

            CreateEntity("Mod1/KeyD2", "Val:ALlSpecific", enabled: true,
                                description: null, metadata: null,
                                environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, isUnlockedInformation: "AUTENTICATED_USER", isReadableAuthorisationInformation: "ACCOUNTANT");
            
            CreateEntity("Mod1/KeyD3", "Val:AllSpecific", enabled: true,
                                description: null, metadata: null,
                                environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, isUnlockedInformation: "AUTENTICATED_USER", isReadableAuthorisationInformation: "ACCOUNTANT");
            //To prove we can parse escaped and tabs, etc.
            CreateEntity("Mod1/Mod2/KeyE1", "Foo:\tX", enabled: true,
                                description: null, metadata: null,
                                environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, isUnlockedInformation: "AUTENTICATED_USER", isReadableAuthorisationInformation: "ACCOUNTANT");
            CreateEntity("Mod1/Mod2/KeyE2", "Foo:\\tX", enabled: true,
                                description: null, metadata: null,
                                environmentIdentifier: null, zoneOrTierIdentifier: null, hostIdentifier: null, isUnlockedInformation: "AUTENTICATED_USER", isReadableAuthorisationInformation: "ACCOUNTANT");



            //Real world:
            CreateEntity("Caching/ReferenceCacheTimeSpanInMinutes", 20);
            CreateEntity("Caching/ShortCacheTimeInSeconds", 60);
            CreateEntity("Data/DefaultPageSize", 20);
            CreateEntity("Example", "Works");
            CreateEntity("Formatting/DateFormatDefault", "d");
            CreateEntity("Formatting/DateFormatLong", "D");
            CreateEntity("Formatting/DateFormatShort", "d");
            CreateEntity("Formatting/DateTimeDefaultFormat", "g");
            CreateEntity("Formatting/DateTimeLongFormat", "F");
            CreateEntity("Formatting/DateTimeShortFormat", "g");
            CreateEntity("Formatting/TimeDefaultFormat", "t");
            CreateEntity("Formatting/TimeLongFormat", "T");
            CreateEntity("Formatting/TimeShortFormat", "t");
            CreateEntity("Infrastructure/URLforFOO", "https://localhost:123/ORG/");
            CreateEntity("Parameters/Parameter/Challenge_Change_Email", "nsi.unit@minedu.govt.nz");
            CreateEntity("Parameters/Parameter/Challenge_Change_Text",
                               "This screen enables you to notify the FOO team that you do not agree with a change that has been made to one of your students' records. The FOO team will process your challenge via email. Note that you need to complete a separate challenge form for each student record that you believe has been incorrectly changed.");
            CreateEntity("Parameters/Parameter/esaa_url_challenge",
                               "https://org.com/useradmin-native/jsp/user/updateChallengeResponseQuestion.jsp");
            CreateEntity("Parameters/Parameter/esaa_url_context",
                               "https://org.com/useradmin-native/jsp/logon/context.jsp");
            CreateEntity("Parameters/Parameter/esaa_url_details",
                               "https://org.com/useradmin-native/jsp/user/updateProfile.jsp");
            CreateEntity("Parameters/Parameter/esaa_url_password",
                               "https://org.com/useradmin-native/jsp/user/changePassword.jsp");
            CreateEntity("Parameters/Parameter/LINK_1",
                               "http://org.com/NZEducation/EducationPolicies/TertiaryEducation/BAR/NationalStudentIndex/InformationForUsers.aspx");
            CreateEntity("Parameters/Parameter/LINK_2",
                               "http://org.com/NZEducation/EducationPolicies/Schools/SchoolOperations/FOO/InformationForParentsAndStudents/GuideForStudentsAndParents.aspx");
            CreateEntity("Parameters/Parameter/LINK_3", "http://nsi.education.govt.nz/survey/moe2.asp");
            CreateEntity("Parameters/Parameter/LINK_4",
                               "http://org.com/NZEducation/EducationPolicies/TertiaryEducation/ForTertiaryEducationInstitutions/NationalStudentIndex/AuthorisedInfoMatchingProg.aspx");
            CreateEntity("Parameters/Parameter/MOTD", "Welcome to the BLAH");
            CreateEntity("Parameters/Parameter/MOTD2",
                               "To continue, please select an option from the black menu bar above. ");
            CreateEntity("Parameters/Parameter/MOTD3", "Users of the BLAH");
            CreateEntity("Parameters/Parameter/session_timeout", 20);
            CreateEntity("PingDb", "Db Online");
            CreateEntity("Security/Session/MaxLengthInMinutes", 480);
            CreateEntity("Security/Session/TimeOutInMinutes", 20);
            CreateEntity("Services/IQOffice/SearchSepChar", "\t");
            CreateEntity("Services/IQOffice/ServiceEndpointAddress", "http://123.456.11.146:4001/");
            CreateEntity("Services/IQOffice/ServiceEndpointUserName", "APPUserId");
            CreateEntity("Services/IQOffice/ServiceEndpointUserPwd", "7F8eNUM1");
            CreateEntity("Services/IQOffice/ServiceMergeReportFileName", "UNDEFINED");
            CreateEntity("Services/IQOffice/ServiceQueryMergeMatchSpecName", "FOOInteractiveMerge.iqm");
            CreateEntity("Services/IQOffice/ServiceQuerySearchMatchSpecName", "FOOInteractive.iqm");
            CreateEntity("Services/IQOffice/ServiceSearchReportFileName", "UNDEFINED");
            CreateEntity("Services/IQOffice/ServiceStandardizationGrammarName", "BAR_MainProcess.grm");
            CreateEntity("Services/Messaging/SMTP/Enabled", true);
            CreateEntity("Services/Messaging/SMTP/FromAddress", "NOOP.No@CORP.com");
            CreateEntity("Services/Messaging/SMTP/Host", "smtp.MOEST.govt.nz");
            CreateEntity("Services/Messaging/SMTP/Password", "Ps!Af31-pN}sdfasdnP^!");
            CreateEntity("Services/Messaging/SMTP/Port", 25);
            CreateEntity("Services/Messaging/SMTP/SSL", false);
            CreateEntity("Services/Messaging/SMTP/ToAddress", "nsi.unit@minedu.govt.nz");
            CreateEntity("Services/Messaging/SMTP/UserName", "srvFOO2Notifications");
            CreateEntity("Support/ContactInfo/Address", (string) null);
            CreateEntity("Support/ContactInfo/Email", "skys@datacom.co.nz");
            CreateEntity("Support/ContactInfo/Name", "Sky Sigal");
            CreateEntity("Support/ContactInfo/Note", (string) null);
            CreateEntity("Support/ContactInfo/Phone", "+64 21 459 6440");
            CreateEntity("Support/ContactInfo/PhoneAfterHours", (string) null);


            //Real world WITH LOCATION:
            CreateEntity("X/Caching/ReferenceCacheTimeSpanInMinutes", 20, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Caching/ShortCacheTimeInSeconds", 60, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Data/DefaultPageSize", 20, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Example", "Works", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/DateFormatDefault", "d", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/DateFormatLong", "D", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/DateFormatShort", "d", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/DateTimeDefaultFormat", "g", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/DateTimeLongFormat", "F", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/DateTimeShortFormat", "g", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/TimeDefaultFormat", "t", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/TimeLongFormat", "T", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Formatting/TimeShortFormat", "t", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Infrastructure/URLforFOO", "https://localhost:123/ORG/", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/Challenge_Change_Email", "nsi.unit@minedu.govt.nz", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/Challenge_Change_Text",
                               "This screen enables you to notify the FOO team that you do not agree with a change that has been made to one of your students' records. The FOO team will process your challenge via email. Note that you need to complete a separate challenge form for each student record that you believe has been incorrectly changed.", enabled: true, description: null, metadata: null, environmentIdentifier: null,zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/esaa_url_challenge",
                               "https://org.com/useradmin-native/jsp/user/updateChallengeResponseQuestion.jsp", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/esaa_url_context",
                               "https://org.com/useradmin-native/jsp/logon/context.jsp", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/esaa_url_details",
                               "https://org.com/useradmin-native/jsp/user/updateProfile.jsp", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/esaa_url_password",
                               "https://org.com/useradmin-native/jsp/user/changePassword.jsp", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/LINK_1",
                               "http://org.com/NZEducation/EducationPolicies/TertiaryEducation/BAR/NationalStudentIndex/InformationForUsers.aspx", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/LINK_2",
                               "http://org.com/NZEducation/EducationPolicies/Schools/SchoolOperations/FOO/InformationForParentsAndStudents/GuideForStudentsAndParents.aspx", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/LINK_3", "http://nsi.education.govt.nz/survey/moe2.asp", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/LINK_4",
                               "http://org.com/NZEducation/EducationPolicies/TertiaryEducation/ForTertiaryEducationInstitutions/NationalStudentIndex/AuthorisedInfoMatchingProg.aspx", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/MOTD", "Welcome to the BLAH", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/MOTD2",
                               "To continue, please select an option from the black menu bar above. ", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/MOTD3", "Users of the BLAH", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Parameters/Parameter/session_timeout", 20, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/PingDb", "Db Online", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Security/Session/MaxLengthInMinutes", 480, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Security/Session/TimeOutInMinutes", 20, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/SearchSepChar", "\t", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/ServiceEndpointAddress", "http://123.456.11.146:4001/", enabled: true, description: "foo", metadata: "bar", environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/ServiceEndpointUserName", "APPUserId", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/ServiceEndpointUserPwd", "7F8eNUM1", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/ServiceMergeReportFileName", "UNDEFINED", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/ServiceQueryMergeMatchSpecName", "FOOInteractiveMerge.iqm", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/ServiceQuerySearchMatchSpecName", "FOOInteractive.iqm", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/ServiceSearchReportFileName", "UNDEFINED", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/IQOffice/ServiceStandardizationGrammarName", "BAR_MainProcess.grm", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/Messaging/SMTP/Enabled", true, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/Messaging/SMTP/FromAddress", "NOOP.No@CORP.com", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/Messaging/SMTP/Host", "smtp.MOEST.govt.nz", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/Messaging/SMTP/Password", "Ps!Af31-pN}sdfasdnP^!", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/Messaging/SMTP/Port", 25, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/Messaging/SMTP/SSL", false, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/Messaging/SMTP/ToAddress", "nsi.unit@minedu.govt.nz", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Services/Messaging/SMTP/UserName", "srvFOO2Notifications", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Support/ContactInfo/Address", (string)null, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Support/ContactInfo/Email", "skys@datacom.co.nz", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Support/ContactInfo/Name", "Sky Sigal", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Support/ContactInfo/Note", (string)null, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Support/ContactInfo/Phone", "+64 21 459 6440", enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");
            CreateEntity("X/Support/ContactInfo/PhoneAfterHours", (string)null, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");





            //CHECK TO SEE IF GUIDS WILL GET IN THE WAY
            //BY REINSERTING, WHICH SHOULD BE SAME ID:

#pragma warning disable 168
            var settingA = 
#pragma warning restore 168
                CreateEntity("DistributedIdCheck", (string)null, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");



            //Will Fail on next step, because it's all in one transaction...
            //I'm guessing that it gets all records it needs, in order to check locally...
            //and because it hasn't committed this record before the next record, 
            //it thinks it's inserting TWO records at one time, in the same transaction...
            //Therefore fails...
            //dbContext.ConditionallyCommit(SeedingCommitLevel.Excessively_UseOnlyForDebuggingPurposes);

            //So ensure it's committed first. Always (in case the global setting is set to Always...
            //in which case the next method would not commit...and we're back to the same bug.
            //dbContext.ConditionallyCommit(SeedingCommitLevel.Always);



            //CHECK TO SEE IF GUIDS WILL GET IN THE WAY.
            //THEY WILL IF YOU DON'T WATCH OUT.
            //THE ISSUE IS:
            //GUID Is Set because code doesn't know if item was persisted (can't be sure of value in timestamp, etc.)
            //Because it is set before doing an AddOrUpdate
            //AddOrUpdate fails, saying there is already an item with matching Key (but different Guid)...and it can't have
            //two records with same Key values.
            //So...
            //AddOrUpdate, as is, is the problem in this case.
            //* It has the advantage that it can look for an object with the given match filters (*)
            //  allowing for different Ids to be supplied (and ignored in the case of an Update)
            //* It has the disadvantage in this case that it can't tell that the Index ig ignorable
            //  as it offers a new one ...and fails on the fact that the index is not unique.

            //BY REINSERTING, WHICH SHOULD BE SAME ID:

            //HOLD ON!

            //Question: 
            //why does AddOrUpdate fail? 
            //It should be that the search criteria tried to find a value that was equal, found it,
            

            //The answer is that we we are using different values in the search filter than what is in the db
            //The search filter is ...in some case...null.
            //But our object, when we created it, we changed null to string.empty... so it doesn't see it


#pragma warning disable 168
            var settingB  = 
#pragma warning restore 168
            CreateEntity("DistributedIdCheck", (string)null, enabled: true, description: null, metadata: null, environmentIdentifier: null, zoneOrTierIdentifier: "BackTier");



        }


        public SerializedApplicationSetting CreateEntity<TValue>(
            string key, 
            TValue value,
                                       bool enabled = true,
                                        string description = null,
                                        string metadata=null,
                                        string environmentIdentifier =null,
                                       string zoneOrTierIdentifier = null,
                                       string hostIdentifier = null,
            string isUnlockedInformation = null,
            string isReadableAuthorisationInformation = null,
            string isWritableAuthorisationInformation = null


    )
        {


            //Correct it early so that the same values are used for both the filter
            //as well as the new values:


            var setting =
                new XAct.Settings.SerializedApplicationSetting()
                {
                    
                    Enabled = true,

                    EnvironmentIdentifier = environmentIdentifier??string.Empty,
                    ZoneOrTierIdentifier = zoneOrTierIdentifier ?? string.Empty,
                    HostIdentifier = hostIdentifier ?? string.Empty,
                    Key = key,

                    Metadata = metadata,
                    Description = description,

                    
                    IsUnlockedInformation = isUnlockedInformation,
                    IsReadableAuthorisationInformation = isReadableAuthorisationInformation,
                    IsWritableAuthorisationInformation = isWritableAuthorisationInformation,

                    CreatedOnUtc = DateTime.UtcNow,
                    LastModifiedOnUtc = DateTime.UtcNow

                    
                };



            setting.Set(value);


            this.InternalEntities.Add(setting);

            return setting;

        }


    }
}