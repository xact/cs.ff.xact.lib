using System.Data.Entity;

namespace XAct.UI.Views.Tests
{


    /// <summary>
    /// The Startup strategy.
    /// <para>
    /// Important:
    /// only called during XActLib development phase 
    /// when the bootstrapper setups 
    /// the Database initialization strategy.
    /// </para>
    /// <para>
    /// It in turn will call <see cref="IViewModeDbContextSeeder"/>
    /// </para>
    /// </summary>
    public interface IViewModeEFDatabaseInitializer : IDatabaseInitializer<UnitTestViewModeDbContext> { }
}