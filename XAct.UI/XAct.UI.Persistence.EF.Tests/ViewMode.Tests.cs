﻿
namespace XAct.UI.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Rules.Implementations;
    using XAct.Tests;
    using XAct.UI.Views;
    using XAct.UI.Views.Services.Implementations;

    [TestFixture]
    public class ViewModeTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            //DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>().FactoryDelegate =
            //    () => new EntityDbContext(new UnitTestDbContext());

            ////Initialize the db using the common initializer passing it one entity from this class
            ////so that it can perform a query (therefore create db as necessary):
            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<ViewModeCaseDefinition>();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void EnsureProviderIsDbOne()
        {
            IViewModeManagementService viewModeManagementService =
                XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();


            Assert.AreEqual(typeof(DbContextViewModeManagementService), viewModeManagementService.GetType());
        }


        [Test]
        public void RuleManagementServiceIsDbBasedOne()
        {
            XAct.Rules.IRuleSetManagementService ruleSetManagementService =
                DependencyResolver.Current.GetInstance<XAct.Rules.IRuleSetManagementService>();

            Assert.AreEqual(typeof(DbRuleSetManagementService), ruleSetManagementService.GetType());
        }




        [Test]
        public void Test00()
        {
            IViewModeService viewModeService =
                XAct.DependencyResolver.Current.GetInstance<IViewModeService>();

            Assert.IsNotNull(viewModeService.CurrentContext);

            Assert.IsTrue(viewModeService.CurrentContext.Name.IsNullOrEmpty());

        }

        [Test]
        public void CheckRulesCanBeRegistered()
        {
            IViewModeManagementService viewModeManagementService =
    XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.ReadOnly, null, null, null);
            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName2", ViewMode.Disabled, null, null, null);
            
        }

        [Test]
        public void CheckRulesCanBeReRegistered()
        {
            IViewModeManagementService viewModeManagementService =
    XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.ReadOnly, null, null, null);
            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.Disabled, null, null, null);

        }

        [Test]
        public void TestWhetherRulesCanBeCreatedAndRetrived()
        {
            IViewModeManagementService viewModeManagementService =
                XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();

            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName",
                                                            ViewMode.ReadOnly, null, null, null);

            ViewMode viewMode;
            viewMode = viewModeManagementService.GetViewMode("Create", "ANamespace.AModel", "APropertyName", null);
            Assert.AreEqual(viewMode, ViewMode.ReadOnly);


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName",
                                                            ViewMode.Hidden, null, null, null);

            viewMode = viewModeManagementService.GetViewMode("Create", "ANamespace.AModel", "APropertyName", null);
            Assert.AreEqual(viewMode, ViewMode.Hidden);

        }

        [Test]
        public void TestWhetherRulesCanBeCreatedAndRetrived2()
        {
            IViewModeManagementService viewModeManagementService =
                XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();


            IViewModeService viewModeService =
                XAct.DependencyResolver.Current.GetInstance<IViewModeService>();

            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName",
                                                            ViewMode.ReadOnly, null, null, null);

            ViewMode viewMode;
            viewMode = viewModeService.GetViewMode("Create", "ANamespace.AModel", "APropertyName", null);
            Assert.AreEqual(viewMode, ViewMode.ReadOnly);


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName",
                                                            ViewMode.Hidden, null, null, null);

            viewMode = viewModeService.GetViewMode("Create", "ANamespace.AModel", "APropertyName", null);
            Assert.AreEqual(viewMode, ViewMode.Hidden);

        }


        [Test]
        public void TestForGareth()
        {
            IViewModeManagementService viewModeManagementService =
    XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();

            Assert.IsTrue(typeof (IDbContextViewModeManagementService).IsAssignableFrom(viewModeManagementService.GetType()));

            viewModeManagementService.Initialize();


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.ReadOnly, null, null, null);


            ViewMode viewMode;
            viewMode = viewModeManagementService.GetViewMode("Create", "ANamespace.AModel", "APropertyName", null);
            Assert.AreEqual(viewMode, ViewMode.ReadOnly);


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.Hidden, null, null, null);

            viewMode = viewModeManagementService.GetViewMode("Create", "ANamespace.AModel", "APropertyName", null);
            Assert.AreEqual(viewMode, ViewMode.Hidden);

        }



        [Test]
        public void TestForGareth2()
        {

            //Clear the cache:
            IViewModeManagementServiceViewModeServiceState viewModeCache = XAct.DependencyResolver.Current.GetInstance<IViewModeManagementServiceViewModeServiceState>();

            viewModeCache.Clear();
            //FIX: cannot reset:
            //viewModeCache.Initialized = false;


            IViewModeManagementService viewModeManagementService =
    XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();

            //Ensure we are talking about Db provider:
            Assert.IsTrue(typeof (IDbContextViewModeManagementService).IsAssignableFrom(viewModeManagementService.GetType()));

            viewModeManagementService.Initialize();


            ViewMode viewMode;
            viewMode = viewModeManagementService.GetViewMode("View", "ExampleNamespace.ExampleViewModel", "ExampleProperty", null);
            Assert.AreEqual(viewMode, ViewMode.Display); //4734
            viewMode = viewModeManagementService.GetViewMode("Create", "ExampleNamespace.ExampleViewModel", "ExampleProperty", null);
            Assert.AreEqual(viewMode, ViewMode.Editable); //4734
            viewMode = viewModeManagementService.GetViewMode("Edit", "ExampleNamespace.ExampleViewModel", "ExampleProperty", null);
            Assert.AreEqual(viewMode, ViewMode.Hidden); //4734
            viewMode = viewModeManagementService.GetViewMode("Rework", "ExampleNamespace.ExampleViewModel", "ExampleProperty", null);
            Assert.AreEqual(viewMode, ViewMode.Display); //4734




        }

        [Test]
        public void TestForGareth3()
        {

            //Clear the cache:
            IViewModeManagementServiceViewModeServiceState viewModeCache = XAct.DependencyResolver.Current.GetInstance<IViewModeManagementServiceViewModeServiceState>();

            viewModeCache.Clear();
            //viewModeCache.Initialized = false;


            IViewModeManagementService viewModeManagementService =
    XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();

            //Ensure we are talking about Db provider:
            Assert.IsTrue(typeof(IDbContextViewModeManagementService).IsAssignableFrom(viewModeManagementService.GetType()));

            //WOrks even if not initialized
            //viewModeManagementService.Initialize();


            ViewMode viewMode;
            viewMode = viewModeManagementService.GetViewMode("View", "ExampleNamespace.ExampleViewModel", "ExampleProperty", null);
            Assert.AreEqual(viewMode, ViewMode.Display); //4734
            viewMode = viewModeManagementService.GetViewMode("Create", "ExampleNamespace.ExampleViewModel", "ExampleProperty", null);
            Assert.AreEqual(viewMode, ViewMode.Editable); //4734
            viewMode = viewModeManagementService.GetViewMode("Edit", "ExampleNamespace.ExampleViewModel", "ExampleProperty", null);
            Assert.AreEqual(viewMode, ViewMode.Hidden); //4734
            viewMode = viewModeManagementService.GetViewMode("Rework", "ExampleNamespace.ExampleViewModel", "ExampleProperty", null);
            Assert.AreEqual(viewMode, ViewMode.Display); //4734




        }


        

    }
}
