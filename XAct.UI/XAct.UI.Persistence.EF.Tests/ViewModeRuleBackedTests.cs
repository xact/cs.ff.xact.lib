
namespace XAct.UI.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Rules.Implementations;
    using XAct.Tests;
    using XAct.UI.Views;
    using XAct.UI.Views.Services.Implementations;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class ViewModesRuleBackedTests

    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
            
            //DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>().FactoryDelegate =
            //    () => new EntityDbContext(new UnitTestDbContext());

            //Initialize the db using the common initializer passing it one entity from this class
            //so that it can perform a query (therefore create db as necessary):
            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<ViewModeCaseDefinition>();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        
        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetViewModeManagementService()
        {
            IViewModeManagementService viewModeManagementService =
                DependencyResolver.Current.GetInstance<IViewModeManagementService>();

            Assert.IsNotNull(viewModeManagementService);
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Can_Get_ViewModeManagementService_Of_Expected_Type()
        {
            IViewModeManagementService viewModeManagementService =
                DependencyResolver.Current.GetInstance<IViewModeManagementService>();

            Assert.AreEqual(typeof(DbContextViewModeManagementService), viewModeManagementService.GetType());
        }


        [Test]
        public void Can_Get_InMemViewModeManagementService()
        {
            IViewModeManagementService viewModeManagementService =
#pragma warning restore 168
 DependencyResolver.Current.GetInstance<IInMemViewModeManagementService>();

            Assert.IsNotNull(viewModeManagementService);
        }

        [Test]
        public void Can_Get_InMemViewModeManagementService_OfExpectedType()
        {
            IViewModeManagementService viewModeManagementService =
#pragma warning restore 168
 DependencyResolver.Current.GetInstance<IInMemViewModeManagementService>();

            Assert.AreEqual(typeof(InMemViewModeManagementService), viewModeManagementService.GetType());
        }



        [Test]
        public void Can_GetRuleManagementService()
        {
            XAct.Rules.IRuleSetManagementService ruleSetManagementService =
                DependencyResolver.Current.GetInstance<XAct.Rules.IRuleSetManagementService>();

            Assert.IsNotNull(ruleSetManagementService);
        }


        [Test]
        public void RuleManagementService_Is_Of_Expected_Type_DbRuleSetManagementService()
        {
            XAct.Rules.IRuleSetManagementService ruleSetManagementService =
                DependencyResolver.Current.GetInstance<XAct.Rules.IRuleSetManagementService>();

            Assert.AreEqual(typeof(DbRuleSetManagementService), ruleSetManagementService.GetType());
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Can_Get_ViewModelService()
        {
#pragma warning disable 168
            IViewModeService viewModeService =
#pragma warning restore 168
                DependencyResolver.Current.GetInstance<IViewModeService>();

            Assert.IsNotNull(viewModeService);
        }




        [Test]
        public void Can_Get_CurrentContext_Name_From_ViewModelService()
        {
            IViewModeService viewModeService =
                DependencyResolver.Current.GetInstance<IViewModeService>();

            var tmp = viewModeService.CurrentContext.Name;
            viewModeService.CurrentContext.Name = "Something";

            try
            {
                Assert.IsNotNullOrEmpty(viewModeService.CurrentContext.Name);
            }
            finally
            {
                viewModeService.CurrentContext.Name = tmp;
            }
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest02()
        {
#pragma warning disable 168
            IViewModeService viewModeService =
#pragma warning restore 168
                DependencyResolver.Current.GetInstance<IViewModeService>();


#pragma warning disable 168
            IViewModeManagementService viewModeManagementService =
#pragma warning restore 168
                DependencyResolver.Current.GetInstance<IInMemViewModeManagementService>();


            //IRuleSetService ruleSetService =
            //    DependencyResolver.Current.GetInstance<IRuleSetService>();


            //viewModeManagementService.RegisterRuleStateMask();
            //viewModeService.GetViewMode("")
            // SOMECLASSNAME ctrl = new SOMECLASSNAME();
            Assert.IsTrue(true);
        }
    }


    //[Initializer("XActLib","", InitializationStage.PostInit)]
    //public class LetsDoIt : IInitializable
    //{
    //    public void Initialize()
    //    {


    //        IViewModeManagementService viewRuleManagmentService =
    //            DependencyResolver.Current.GetInstance<IViewModeManagementService>();


    //        ViewModeDbContext myDbContext = new ViewModeDbContext();
    //        //int var = myDbContext.Definitions.Count();

    //        var results = myDbContext.Definitions.Select(
    //            x =>
    //            new
    //                {
    //                    Case = x.ViewModeCase.Name,
    //                    ViewModel = x.ViewModeModelPropertyDefinition.ViewModeModelDefinition.Name,
    //                    ViewModeModelPropertyDefinition = x.ViewModeModelPropertyDefinition.Name,
    //                    Mask = (ViewMode) x.ViewModeRaw,
    //                    Note = x.Description
    //                }).OrderBy(o => o.Case).OrderBy(o => o.ViewModel).OrderBy(o => o.ViewModeModelPropertyDefinition).
    //            ToArray();

    //        foreach (var r in results)
    //        {
    //            viewRuleManagmentService.RegisterRuleStateMask(r.Case, r.ViewModel, r.ViewModeModelPropertyDefinition,
    //                                                           r.Mask);

    //            Debug.WriteLine("{0}:{1}:{2}:{3}".FormatStringInvariantCulture(r.Case, r.ViewModel,
    //                                                                           r.ViewModeModelPropertyDefinition,
    //                                                                           r.Mask));
    //        }
    //        var result = viewRuleManagmentService.GetViewMode("create", "ExampleNamespace.ExampleViewModel",
    //                                                           "ExampleProperty");

    //        var result2 = viewRuleManagmentService.GetViewMode("edit", "ExampleNamespace.ExampleViewModel",
    //                                                            "ExampleProperty");

    //        var result3 = viewRuleManagmentService.GetViewMode("view", "ExampleNamespace.ExampleViewModel",
    //                                                            "ExampleProperty");

    //        Debug.WriteLine(result);
    //        Debug.WriteLine(result2);
    //        Debug.WriteLine(result3);
    //    }
//}


}


