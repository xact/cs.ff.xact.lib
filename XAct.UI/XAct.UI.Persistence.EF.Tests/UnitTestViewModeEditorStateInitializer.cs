using System.Data.Entity;
using XAct.Initialization;
using XAct.Services;
using XAct.UI.Views;

namespace XAct.UI.Views.Tests
{
    [Initializer(InitializationStage.PostInit, "XActLib")]
    public class UnitTestViewModeEditorStateInitializer : IInitializable
    {
        public void Initialize()
        {
            //Don't want this called unless in test mode
            //as it sets the Database Initializer
            //to a DbInitialization Strategey that Drops the Db...

            IViewModeEFDatabaseInitializer viewRuleDbInitializer =
                ServiceLocatorService.Current.GetInstance<IViewModeEFDatabaseInitializer>();

            Database.SetInitializer(viewRuleDbInitializer);


            UnitTestViewModeDbContext viewModeContext = new UnitTestViewModeDbContext();

            viewModeContext.ViewStateCases.Load();
        }
    }
}