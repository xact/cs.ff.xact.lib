﻿using System.Data.Entity;
using XAct.Initialization;

namespace XAct.Settings
{
    /// <summary>
    /// Contract for a class to ensure the creation of a db
    /// and Table required for the Rules.
    /// </summary>
    public interface IUnitTestViewModeDbBootstrapper : IInitializable
    {
    }
}