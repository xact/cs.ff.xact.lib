﻿using XAct.Domain.Repositories;
using XAct.Services;
using XAct.UI.Views.Tests;

namespace XAct.UI.Tests
{
 [DefaultBindingImplementation(typeof(IContextFactory))]
 public class UnitTestViewModeDbContextFactory : IContextFactory
    {
        public XAct.Domain.Repositories.IContext Create<TContext>(string tag = null) 
            where TContext : XAct.Domain.Repositories.IContext
        {
            return new EntityDbContext(new UnitTestViewModeDbContext());
        }
    }
}
