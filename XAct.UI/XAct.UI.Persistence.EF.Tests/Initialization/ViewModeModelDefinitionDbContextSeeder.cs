namespace XAct.UI.Views.Ini
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.UI.Views.Initialization.DbContextSeeders;

    /// <summary>
    /// An implementation of 
    /// <see cref="IViewModeModelDefinitionDbContextSeeder"/>
    /// <para>
    /// Important:
    /// during XActLib development, called by initializer setup by bootstrapper.
    /// </para>
    /// <para>
    /// Important:
    /// Called by <c>IViewModeDbInitializer</c>
    /// when it is setup by the bootsrapper.
    /// </para>
    /// </summary>
    public class ViewModeModelDefinitionDbContextSeeder : UnitTestXActLibDbContextSeederBase<ViewModeModelDefinition>, IViewModeModelDefinitionDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelDefinition" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ViewModeModelDefinitionDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>
        /// Seeds the specified db context.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            this.SeedInternalHelper(dbContext, true, x => x.Id);
        }


        public override void CreateEntities()
        {
            this.InternalEntities = new List<ViewModeModelDefinition>();

            this.InternalEntities.Add(new ViewModeModelDefinition
                                                     {
                                                         Id = 1.ToGuid(),
                                                         Name = "ExampleNamespace.ExampleViewModel"
                                                     });

        } 

    }
}