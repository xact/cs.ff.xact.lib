namespace XAct.UI.Views.Initialization.DbContextSeeders.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq.Expressions;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeCaseDefinitionDbContextSeeder : UnitTestXActLibDbContextSeederBase<ViewModeCaseDefinition>, IViewModeCaseDefinitionDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelDefinitionDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ViewModeCaseDefinitionDbContextSeeder(ITracingService tracingService) : base(tracingService)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelDefinitionDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="seeders">The seeders.</param>
        public ViewModeCaseDefinitionDbContextSeeder(ITracingService tracingService, params IDbContextSeeder[] seeders) : base(tracingService, seeders)
        {
        }

        /// <summary>
        /// Seeds the internal.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.Name,SeedingCommitLevel.Always);

        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<ViewModeCaseDefinition>();

            this.InternalEntities.Add(new ViewModeCaseDefinition { Id = 1.ToGuid(), Name = "View" });
            this.InternalEntities.Add(new ViewModeCaseDefinition { Id = 2.ToGuid(), Name = "Create" });
            this.InternalEntities.Add(new ViewModeCaseDefinition { Id = 3.ToGuid(), Name = "Edit" });
            this.InternalEntities.Add(new ViewModeCaseDefinition { Id = 4.ToGuid(), Name = "Rework" });

        } 


    }
}