namespace XAct.UI.Views.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeRuleDefinitionDbContextSeeder : UnitTestXActLibDbContextSeederBase<ViewModeRuleDefinition>, IViewModeRuleDefinitionDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeRuleDefinitionDbContextSeeder" /> class.
        /// </summary>
        /// <param name="viewModeCaseDefinitionDbContextSeeder">The view mode case definition database context seeder.</param>
        /// <param name="viewModeModelPropertyDefinitionDbContextSeeder">The view mode model property definition database context seeder.</param>
        public ViewModeRuleDefinitionDbContextSeeder(
            IViewModeCaseDefinitionDbContextSeeder viewModeCaseDefinitionDbContextSeeder,
            IViewModeModelPropertyDefinitionDbContextSeeder viewModeModelPropertyDefinitionDbContextSeeder)
            : base(viewModeCaseDefinitionDbContextSeeder,viewModeModelPropertyDefinitionDbContextSeeder)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            this.SeedInternalHelper(dbContext, true, x => x.Id);
        }

        public override void CreateEntities()
        {
            InternalEntities = new List<ViewModeRuleDefinition>();
            
            InternalEntities.Add(
                              new ViewModeRuleDefinition
                              {
                                  Id = 1.ToGuid(),
                                  ViewCaseFK = 1.ToGuid(),
                                  ViewModeModelPropertyDefinitionFK = 1.ToGuid(),
                                  ViewMode = ViewMode.Display
                              });
            InternalEntities.Add(
                              new ViewModeRuleDefinition
                              {
                                  Id = 2.ToGuid(),
                                  ViewCaseFK = 2.ToGuid(),
                                  ViewModeModelPropertyDefinitionFK = 1.ToGuid(),
                                  ViewMode = ViewMode.Editable
                              });
            InternalEntities.Add(
                              new ViewModeRuleDefinition
                              {
                                  Id = 3.ToGuid(),
                                  ViewCaseFK = 3.ToGuid(),
                                  ViewModeModelPropertyDefinitionFK = 1.ToGuid(),
                                  ViewMode = ViewMode.Hidden
                              });
            InternalEntities.Add(
                              new ViewModeRuleDefinition
                              {
                                  Id = 4.ToGuid(),
                                  ViewCaseFK = 4.ToGuid(),
                                  ViewModeModelPropertyDefinitionFK = 1.ToGuid(),
                                  ViewMode = ViewMode.Display
                              });

        }
    }
}