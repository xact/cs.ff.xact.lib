namespace XAct.UI.Views.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeModelPropertyDefinitionDbContextSeeder : UnitTestXActLibDbContextSeederBase<ViewModeModelPropertyDefinition>, IViewModeModelPropertyDefinitionDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelPropertyDefinitionDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="viewModeCaseDefinitionDbContextSeeder">The view mode case definition database context seeder.</param>
        /// <param name="viewModeModelDefinitionDbContextSeeder">The view mode model definition database context seeder.</param>
        public ViewModeModelPropertyDefinitionDbContextSeeder(
            ITracingService tracingService, 
            IViewModeCaseDefinitionDbContextSeeder viewModeCaseDefinitionDbContextSeeder, 
            IViewModeModelDefinitionDbContextSeeder viewModeModelDefinitionDbContextSeeder)
            : base(tracingService, viewModeCaseDefinitionDbContextSeeder, viewModeModelDefinitionDbContextSeeder)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.Id);
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<ViewModeModelPropertyDefinition>();

            InternalEntities.Add(new ViewModeModelPropertyDefinition
                                                             {
                                                                 Id = 1.ToGuid(),
                                                                 Name = "ExampleProperty",
                                                                 ViewModeModelDefinitionFK = 1.ToGuid()
                                                             });
            
        }
    }
}