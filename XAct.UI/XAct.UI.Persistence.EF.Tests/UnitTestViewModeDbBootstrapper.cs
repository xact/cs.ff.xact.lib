using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using XAct.Bootstrapper.Tests;
using XAct.Diagnostics;
using XAct.Services;
using XAct.Settings;

namespace XAct.UI.Views.Tests
{
    /// <summary>
    /// An implementation of the <see cref="IUnitTestViewModeDbBootstrapper"/>
    /// to ensure the creation of a database as required for the resources.
    /// </summary>
    [DefaultBindingImplementation(typeof(IUnitTestViewModeDbBootstrapper),BindingLifetimeType.SingletonScope)]
    public class UnitTestHostSettingsDbBootstrapper : IUnitTestViewModeDbBootstrapper
    {
        private readonly ITracingService _tracingService;
        private readonly string _typeName;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTestHostSettingsDbBootstrapper"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public UnitTestHostSettingsDbBootstrapper(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;
            _tracingService = tracingService;
        }

        public void Initialize()
        {
            _tracingService.Trace(TraceLevel.Verbose,"{0}.{1}::Begin...",_typeName,"Initialize");

            //FIX: Initializer has to be same type of DbContext as DbContext we want (using a generic DbContext,
            //or any DbContext that is not exact same match as DbContext used for first call to Db, will cause
            //the Seed method to never be called:
            //Does not work: Database.SetInitializer(_unitTestDatabaseInitializer);

            var unitTestDatabaseInitializer = 
                new UnitTestDatabaseInitializer<UnitTestDbContext>(XAct.Shortcuts.ServiceLocate<ITracingService>());

            Database.SetInitializer<UnitTestDbContext>(unitTestDatabaseInitializer);

            
            UnitTestDbContext dbContext = new UnitTestDbContext();

            DbSet<ViewModeCaseDefinition> viewModeCases = dbContext.Set<ViewModeCaseDefinition>();

            viewModeCases.Load();

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Complete.", _typeName, "Initialize");
        }
    }


}