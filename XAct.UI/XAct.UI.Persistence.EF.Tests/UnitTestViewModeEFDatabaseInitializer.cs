using System.Data.Entity;
using XAct.Services;

namespace XAct.UI.Views.Tests
{
    /// <summary>
    /// An implementiaton of <see cref="IViewModeEFDatabaseInitializer"/>
    /// <para>
    /// Important:
    /// only called during XActLib development/Test phase 
    /// when the bootstrapper setups 
    /// the Database initialization strategy.
    /// </para>
    /// <para>
    /// It in turn will call <see cref="IViewModeDbContextSeeder"/>
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IViewModeEFDatabaseInitializer))]
    public class UnitTestViewModeEFDatabaseInitializer : DropCreateDatabaseAlways<UnitTestViewModeDbContext>, IViewModeEFDatabaseInitializer
    {
        private readonly IViewModeDbContextSeeder _viewRuleDbSeeder;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTestViewModeEFDatabaseInitializer"/> class.
        /// </summary>
        /// <param name="viewRuleDbSeeder">The view rule db seeder.</param>
        public UnitTestViewModeEFDatabaseInitializer(IViewModeDbContextSeeder viewRuleDbSeeder)
        {
            _viewRuleDbSeeder = viewRuleDbSeeder;
        }

        /// <summary>
        /// Seeds the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        protected override void Seed(UnitTestViewModeDbContext context)
        {
            _viewRuleDbSeeder.Seed(context);

            base.Seed(context);
        }

        /// <summary>
        /// Initializes the database.
        /// </summary>
        /// <param name="context">The context.</param>
        public void InitializeDatabase(DbContext context)
        {
            base.InitializeDatabase(context as UnitTestViewModeDbContext);
        }
    }
}



