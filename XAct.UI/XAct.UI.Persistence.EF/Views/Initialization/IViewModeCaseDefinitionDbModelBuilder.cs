// ReSharper disable CheckNamespace
namespace XAct.UI.Views.Initialization
// ReSharper restore CheckNamespace
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.UI.Views.Initialization.DbContextSeeders;

    /// <summary>
    /// An implementation of <see cref="IHasXActLibDbModelBuilder"/>
    /// for building the Db used by 
    /// <see cref="IViewModeManagementService"/>
    /// <para>
    /// By externalizing the ModelBuild logic outsidet of <see cref="DbContext"/> 
    /// into its own class
    /// it can also be invoked from a different starting point (ie an app)
    /// without having to go through this DbContext, which is only intended from 
    /// XActLib development.
    /// </para>
    /// <para>
    /// Important: 
    /// During dev stage, called directly from <see cref="DbContext.OnModelCreating"/>
    /// and during production, called via reflection.
    /// </para>
    /// <para>
    /// Only used to setup db relationships -- does not do data seeding 
    /// (see <see cref="IViewModeCaseDefinitionDbContextSeeder"/> which invoked by startup strategy setup in apps bootstrapper).
    /// </para>
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface IViewModeCaseDefinitionDbModelBuilder : IHasXActLibDbModelBuilder
    {

    }
}