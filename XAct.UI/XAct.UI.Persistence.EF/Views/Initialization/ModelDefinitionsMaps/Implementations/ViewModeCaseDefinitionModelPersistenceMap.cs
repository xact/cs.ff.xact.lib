﻿namespace XAct.UI.Views.Initialization.ModelDefinitionsMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeCaseDefinitionModelPersistenceMap : EntityTypeConfiguration<ViewModeCaseDefinition>, IViewModeCaseDefinitionModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeCaseDefinitionModelPersistenceMap"/> class.
        /// </summary>
        public ViewModeCaseDefinitionModelPersistenceMap()
        {

            this.ToXActLibTable("ViewModeCaseDefinition");


            this
                .HasKey(m => m.Id);

            int colOrder = 0;

            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);
            this
                .Property(x=>x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);
            this
                  
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(36)
                .HasColumnOrder(colOrder++);

            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;

        }
    }
}