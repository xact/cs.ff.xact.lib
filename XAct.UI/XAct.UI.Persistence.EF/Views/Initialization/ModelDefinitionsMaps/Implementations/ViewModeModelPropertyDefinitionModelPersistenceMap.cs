﻿namespace XAct.UI.Views.Initialization.ModelDefinitionsMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeModelPropertyDefinitionModelPersistenceMap : EntityTypeConfiguration<ViewModeModelPropertyDefinition>,IViewModeModelPropertyDefinitionModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelPropertyDefinitionModelPersistenceMap"/> class.
        /// </summary>
        public ViewModeModelPropertyDefinitionModelPersistenceMap()
        {

            this.ToXActLibTable("ViewModeModelPropertyDefinition");



            int colOrder = 0;
            this
                .HasKey(m => m.Id);
            this
                 .Property(m => m.Id)
                 .DefineRequiredGuidId(colOrder++);
            this
                .Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);
            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(128);
            this
                .HasRequired(m => m.ViewModeModelDefinition)
                .WithMany()
                .HasForeignKey(m => m.ViewModeModelDefinitionFK);

            //modelBuilder.Entity<Rules>()
            //    .HasOptional(m => m.Rules)
            //    .WithOptionalDependent().
            //    .HasForeignKey(m => m.ViewModeModelDefinitionFK);


        }
    }
}


