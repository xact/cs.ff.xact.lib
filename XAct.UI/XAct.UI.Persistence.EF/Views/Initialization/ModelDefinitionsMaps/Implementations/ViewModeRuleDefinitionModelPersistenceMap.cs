﻿namespace XAct.UI.Views.Initialization.ModelDefinitionsMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeRuleDefinitionModelPersistenceMap : EntityTypeConfiguration<ViewModeRuleDefinition>, IViewModeRuleDefinitionModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeRuleDefinitionModelPersistenceMap"/> class.
        /// </summary>
        public ViewModeRuleDefinitionModelPersistenceMap()
        {

            this.ToXActLibTable("ViewModeRuleDefinition");
            

            this
                .HasKey(m => m.Id);

            int colOrder = 0;

            this
                 .Property(m => m.Id)
                 .DefineRequiredGuidId(colOrder++);
            this
                .Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);
 
            //this.Property(m => m.RuleEngineIdentifier)
            //    .IsRequired()
            //    .HasColumnOrder(colOrder++);
            //this.Property(m => m.RuleSetIdentifier)
            //    .IsRequired()
            //    .HasColumnOrder(colOrder++);
            //this.Property(m => m.ViewModeModelPropertyDefinitionFK)
            //    .IsRequired()
            //    .HasColumnOrder(colOrder++);

            //this.Property(m => m.ViewCaseFK)
            //    .IsRequired()
            //    .HasColumnOrder(colOrder++);


            //this
            //    .Property(m => m.RuleEngineIdentifier)
            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;

            this
                .HasRequired(m => m.ViewModeCase)
                .WithMany()
                .HasForeignKey(m => m.ViewCaseFK);

            //*-1
            this
                .HasRequired(m => m.ViewModeModelPropertyDefinition)
                .WithMany()
                .HasForeignKey(m => m.ViewModeModelPropertyDefinitionFK);


        }
    }
}