﻿namespace XAct.UI.Views.Initialization.ModelDefinitionsMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeModelDefinitionModelPersistenceMap : EntityTypeConfiguration<ViewModeModelDefinition>, IViewModeModelDefinitionModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelDefinitionModelPersistenceMap"/> class.
        /// </summary>
        public ViewModeModelDefinitionModelPersistenceMap()
        {
            this.ToXActLibTable("ViewModeModelDefinition");

            this
                .HasKey(m => m.Id);

            int colOrder = 0;

            this
                 .Property(m => m.Id)
                 .DefineRequiredGuidId(colOrder++);
            this
                .Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);
 
            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++);

            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;


        }


    }
}