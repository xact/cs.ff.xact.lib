namespace XAct.UI.Views.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.UI.Views.Initialization.DbContextSeeders;
    using XAct.UI.Views.Initialization.ModelDefinitionsMaps;

    /// <summary>
    /// An implementation of <see cref="IViewModeCaseDefinitionDbModelBuilder"/>
    /// in order to define the relationships needed by the Db 
    /// backing <see cref="IViewModeManagementService"/>
    /// <para>
    /// By externalizing the ModelBuild logic outsidet of <see cref="DbContext"/> 
    /// into its own class
    /// it can also be invoked from a different starting point (ie an app)
    /// without having to go through this DbContext, which is only intended from 
    /// XActLib development.
    /// </para>
    /// <para>
    /// Important: 
    /// During XActLib dev stage, called directly from <see cref="DbContext.OnModelCreating"/>
    /// and during production, called via reflection.
    /// </para>
    /// <para>
    /// Only used to setup db relationships -- does not do data seeding 
    /// (see <see cref="IViewModeCaseDefinitionDbContextSeeder"/> which invoked by startup strategy setup in apps bootstrapper).
    /// </para>
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class ViewModeCaseDefinitionDbModelBuilder : IViewModeCaseDefinitionDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;


        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeCaseDefinitionDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ViewModeCaseDefinitionDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;
            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<ViewModeCaseDefinition>)XAct.DependencyResolver.Current.GetInstance<IViewModeCaseDefinitionModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<ViewModeModelDefinition>)XAct.DependencyResolver.Current.GetInstance<IViewModeModelDefinitionModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<ViewModeModelPropertyDefinition>)XAct.DependencyResolver.Current.GetInstance<IViewModeModelPropertyDefinitionModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<ViewModeRuleDefinition>)XAct.DependencyResolver.Current.GetInstance<IViewModeRuleDefinitionModelPersistenceMap>());
            //----------
            //----------


            //----------
        }
    }
}



/*
 * namespace XAct.UI.Views.Initialization
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IViewModeDbModelBuilder"/>
    /// in order to define the relationships needed by the Db 
    /// backing <see cref="IViewModeManagementService"/>
    /// <para>
    /// By externalizing the ModelBuild logic outsidet of <see cref="DbContext"/> 
    /// into its own class
    /// it can also be invoked from a different starting point (ie an app)
    /// without having to go through this DbContext, which is only intended from 
    /// XActLib development.
    /// </para>
    /// <para>
    /// Important: 
    /// During XActLib dev stage, called directly from <see cref="DbContext.OnModelCreating"/>
    /// and during production, called via reflection.
    /// </para>
    /// <para>
    /// Only used to setup db relationships -- does not do data seeding 
    /// (see <see cref="IViewModeDbContextSeeder"/> which invoked by startup strategy setup in apps bootstrapper).
    /// </para>
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class SingletonScopedViewModeDbModelBuilder : IViewModeDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;


        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ViewModeDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;
            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<>) XAct.DependencyResolver.Current.GetInstance<IIViewModeCaseDefinitionMap());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<>) XAct.DependencyResolver.Current.GetInstance<IIViewModeRuleDefinitionMap());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<>) XAct.DependencyResolver.Current.GetInstance<IIViewModeModeModelDefinitionMap());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<>) XAct.DependencyResolver.Current.GetInstance<IIViewModeModelPropertyDefinitionMap());
            


            //----------
            modelBuilder.Entity<ViewModeModelDefinition>()
                .HasKey(m => m.Id);

            modelBuilder.Entity<ViewModeModelDefinition>()
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(128);
            modelBuilder.Entity<ViewModeModelDefinition>()
                .DefineDescription(colIndex++)
 * ;
            //----------



            //----------
        }

}
*/