namespace XAct.UI.Views.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;

    /// <summary>
    /// A default implementation of the <see cref="IViewModeCaseDefinitionDbContextSeeder"/> contract
    /// to seed the ViewMode tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class ViewModeCaseDefinitionDbContextSeeder : XActLibDbContextSeederBase<ViewModeCaseDefinition>, IViewModeCaseDefinitionDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeCaseDefinitionDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ViewModeCaseDefinitionDbContextSeeder(ITracingService tracingService):base(tracingService)
        {
        }

        /// <summary>
        /// Seeds the internal.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.Name);
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {

            this.InternalEntities = new List<ViewModeCaseDefinition>();

            this.InternalEntities.Add(new ViewModeCaseDefinition { Id = 1.ToGuid(), Name = "View" });
            this.InternalEntities.Add(new ViewModeCaseDefinition { Id = 2.ToGuid(), Name = "Create" });
            this.InternalEntities.Add(new ViewModeCaseDefinition { Id = 3.ToGuid(), Name = "Edit" });

        }
    }
}