namespace XAct.UI.Views.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeRuleDefinitionDbContextSeeder : XActLibDbContextSeederBase<ViewModeRuleDefinition>, IViewModeRuleDefinitionDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeRuleDefinitionDbContextSeeder" /> class.
        /// </summary>
        /// <param name="viewModeCaseDefinitionDbContextSeeder">The view mode case definition database context seeder.</param>
        /// <param name="viewModeModelPropertyDefinitionDbContextSeeder">The view mode model property definition database context seeder.</param>
        public ViewModeRuleDefinitionDbContextSeeder(IViewModeCaseDefinitionDbContextSeeder viewModeCaseDefinitionDbContextSeeder,
            IViewModeModelPropertyDefinitionDbContextSeeder viewModeModelPropertyDefinitionDbContextSeeder) :
            base(viewModeCaseDefinitionDbContextSeeder, viewModeModelPropertyDefinitionDbContextSeeder)
        {
        }


        /// <summary>
        /// Seeds the internal.
        /// </summary>
        public override void CreateEntities()
        {
            //throw new System.NotImplementedException();
        }
    }
}