namespace XAct.UI.Views.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeModelPropertyDefinitionDbContextSeeder : XActLibDbContextSeederBase<ViewModeModelPropertyDefinition>, IViewModeModelPropertyDefinitionDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelPropertyDefinitionDbContextSeeder"/> class.
        /// </summary>
        /// <param name="viewModeModelDefinitionDbContextSeeder">The view mode model definition database context seeder.</param>
        public ViewModeModelPropertyDefinitionDbContextSeeder(IViewModeModelDefinitionDbContextSeeder viewModeModelDefinitionDbContextSeeder)
            : base(viewModeModelDefinitionDbContextSeeder)
        {
        }

        /// <summary>
        /// Seeds the internal.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}