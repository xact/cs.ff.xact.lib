namespace XAct.UI.Views.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeModelDefinitionDbContextSeeder : XActLibDbContextSeederBase<ViewModeModelDefinition>, IViewModeModelDefinitionDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelDefinitionDbContextSeeder"/> class.
        /// </summary>
        /// <param name="seeders">The seeders.</param>
        public ViewModeModelDefinitionDbContextSeeder(params IDbContextSeeder[] seeders) : base(seeders)
        {
        }



        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}