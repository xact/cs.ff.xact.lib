namespace XAct.UI.Views.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// 
    /// </summary>
    public interface IViewModeRuleDefinitionDbContextSeeder : IHasXActLibDbContextSeeder<ViewModeRuleDefinition>
    {

    }
}