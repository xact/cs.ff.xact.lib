namespace XAct.UI.Views.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// <para>
    /// Important:
    /// during XActLib development, called by initializer setup by bootsrapper.
    /// </para>
    /// <para>
    /// Important:
    /// Called by <c>IViewModeDbInitializer</c>
    /// when it is setup by the bootsrapper.
    /// </para>
    /// </summary>
        //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IViewModeCaseDefinitionDbContextSeeder : IHasXActLibDbContextSeeder<ViewModeCaseDefinition>
    {

    }
}