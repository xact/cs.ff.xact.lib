namespace XAct.UI.Views.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// 
    /// </summary>
    public interface IViewModeModelPropertyDefinitionDbContextSeeder : IHasXActLibDbContextSeeder<ViewModeModelPropertyDefinition>
    {

    }
}