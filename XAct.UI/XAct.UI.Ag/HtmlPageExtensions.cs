﻿using System;
using System.Windows.Browser;


namespace XAct
{
    /// <summary>
    /// Extensions to the 
    /// <see cref="HtmlDocument"/> object.
    /// </summary>
    public static class HtmlDocumentExtensions
    {



        /// <summary>
        /// Sets a persistent cookie which expires after the given number of days
        /// </summary>
        /// <param name="htmlDocument">The HtmDocument to extend</param>
        /// <param name="cookieKey">the cookie Key</param>
        /// <param name="cookieValue">the cookie Value</param>
        /// <param name="expirationInDays">The number of days before the cookie expires</param>
        public static void SetCookie(this HtmlDocument htmlDocument, string cookieKey, string cookieValue, int expirationInDays)
        {
            DateTime expiration = DateTime.UtcNow + TimeSpan.FromDays(expirationInDays);
            SetCookie(htmlDocument, cookieKey, cookieValue, expiration);
        }

        /// <summary>
        /// Sets a persistent cookie with an expiration date
        /// </summary>
        /// <param name="htmlDocument">The HtmDocument to extend</param>
        /// <param name="cookieKey">the cookie Key</param>
        /// <param name="cookieValue">the cookie Value</param>
        /// <param name="expiration"></param>
        public static void SetCookie(this HtmlDocument htmlDocument, string cookieKey, string cookieValue, DateTime expiration)
        {
            //string oldCookie = htmlDocument.GetProperty("cookie") as String;
            string cookie = String.Format("{0}={1};expires={2}", cookieKey, cookieValue, expiration.ToString("R"));
            htmlDocument.SetProperty("cookie", cookie);
        }

        /// <summary>
        /// Retrieves an existing cookie
        /// </summary>
        /// <param name="htmlDocument">The HtmDocument to extend</param>
        /// <param name="cookieKey">cookie Key</param>
        /// <returns>null if the cookie does not exist, otherwise the cookie Value</returns>
        public static string GetCookie(this HtmlDocument htmlDocument, string cookieKey)
        {
            string[] cookies = htmlDocument.Cookies.Split(';');

            cookieKey += '=';

            foreach (string cookie in cookies)
            {

                string cookieStr = cookie.Trim();

                if (!cookieStr.StartsWith(cookieKey, StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                string[] vals = cookieStr.Split('=');

                return vals.Length >= 2 ? vals[1] : string.Empty;
            }

            return null;

            
        }

        /// <summary>
        /// Deletes a specified cookie by setting its cookieValue to empty and expiration to -1 days
        /// </summary>
        /// <param name="htmlDocument">The HtmDocument to extend</param>
        /// <param name="cookieKey">the cookie Key to delete</param>
        public static void DeleteCookie(this HtmlDocument htmlDocument, string cookieKey)
        {

            //string oldCookie = htmlDocument.GetProperty("cookie") as String;

            DateTime expiration = DateTime.UtcNow - TimeSpan.FromDays(1);

            string cookie = String.Format("{0}=;expires={1}", cookieKey, expiration.ToString("R"));

            htmlDocument.SetProperty("cookie", cookie);
        }
    }
}



