<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:user="http://mycompany.com/mynamespace" >




  <xsl:template match="dataObject">
    <HTML>
      <HEAD>
        <LINK REL="stylesheet" type="text/css" href="contact.css"/>
      </HEAD>
      <BODY>
        <table class="FORM_TABLE">
          <xsl:apply-templates select="field"/>
        </table>
      </BODY>
    </HTML>
  </xsl:template>



  <xsl:template match="field">
    <xsl:choose>
      <xsl:when test="@hidden='true' or @hidden='1'">
        <!-- output nothing -->
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td class="FIELD_LABEL_CELL">
            <span class="FIELD_LABEL">
              <xsl:value-of select="@label"/>:
            </span>
          </td>
          <td class="FIELD_CONTROL_CELL">
            <xsl:choose>
              <xsl:when test="@inputType='String'">
                <input name="" type="TEXT" class="FIELD_CONTROL" >
                  <xsl:attribute name="name">
                    <xsl:value-of select="@name" />
                  </xsl:attribute>
                </input>
              </xsl:when>
              <xsl:when test="@inputType='Int32'">
                <textbox type="TEXT" class="FIELD_CONTROL">
                  <xsl:attribute name="name">
                    <xsl:value-of select="@name" />
                  </xsl:attribute>
                </textbox>
              </xsl:when>
              <xsl:otherwise>
                <input type="TEXT" class="FIELD_CONTROL">
                  <xsl:attribute name="name">
                    <xsl:value-of select="@name" />
                  </xsl:attribute>
                </input>
              </xsl:otherwise>
            </xsl:choose>
          </td>
          <td class="FIELD_VALIDATION_CELL">
            <xsl:if test="@validationMinLength>0">
              *
            </xsl:if>
          </td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
