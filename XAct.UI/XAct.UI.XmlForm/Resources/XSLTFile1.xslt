﻿<!--<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>-->


<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:user="http://mycompany.com/mynamespace"
	>


  <xsl:output method="xml" indent="yes"/>


  <xsl:template match="dataObject">
    <form>

      <header>
        <div name="The Header">
          <xsl:apply-templates select="field[@name='ID']"/>
          <xsl:apply-templates select="field[@name='Category_FK']"/>
          <xsl:apply-templates select="field[@name='Title']"/>
          <xsl:apply-templates select="field[@name='FirstName']"/>
          <xsl:apply-templates select="field[@name='LastName']"/>
        </div>
      </header>

      <body>
        <div name="ContactInfo">
          <xsl:apply-templates select="field[@name='PersEmail']"/>
          <xsl:apply-templates select="field[@name='PersPhone']"/>
          <xsl:apply-templates select="field[@name='PersCell']"/>
          <xsl:apply-templates select="field[@name='BizEmail']"/>
          <xsl:apply-templates select="field[@name='BizPhone']"/>
          <xsl:apply-templates select="field[@name='BizCell']"/>
        </div>


        <div  name="Biz Addr">
          <xsl:apply-templates select="field[@name='CompanyTitle']"/>
          <xsl:apply-templates select="field[@name='Company']"/>
          <xsl:apply-templates select="field[@name='BizStreet']"/>
          <xsl:apply-templates select="field[@name='BizStreet2']"/>
          <xsl:apply-templates select="field[@name='BizCity']"/>
          <xsl:apply-templates select="field[@name='BizState']"/>
          <xsl:apply-templates select="field[@name='BizPostalCode']"/>
          <xsl:apply-templates select="field[@name='BizCountry']"/>
        </div>
        
        <div   name="Home Addr">
          <xsl:apply-templates select="field[@name='PersStreet']"/>
          <xsl:apply-templates select="field[@name='PersStreet2']"/>
          <xsl:apply-templates select="field[@name='PersCity']"/>
          <xsl:apply-templates select="field[@name='PersState']"/>
          <xsl:apply-templates select="field[@name='PersPostalCode']"/>
          <xsl:apply-templates select="field[@name='PersCountry']"/>
        </div>
        <div name="Misc">
          <xsl:apply-templates select="field[@name='NetWorth']"/>
          <xsl:apply-templates select="field[@name='Score']"/>
          <xsl:apply-templates select="field[@name='Birthday']"/>
          <xsl:apply-templates select="field[@name='Sex']"/>
        </div>
        <div name="Notes">
          <xsl:apply-templates select="field[@name='Body']"/>
        </div>
      </body>

      <footer>
        <xsl:apply-templates select="field[@name='CreatorId']"/>
      </footer>
    </form>
  </xsl:template>


  <xsl:template match="field">
    <xsl:copy-of select ="." />
  </xsl:template>
</xsl:stylesheet>