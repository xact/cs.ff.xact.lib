<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="cssDirPath"/>
  <xsl:param name="imgDirPath"/>

  <xsl:template match="dataObject">
    <html>
      
      <head>
        <link rel="stylesheet" type="text/css" href="{$cssDirPath}app.default.css"/>
        <link rel="stylesheet" type="text/css" href="{$cssDirPath}app.preview.css"/>
      </head>
      
      <body>

        <div class="HEADER">
          <div class="TITLEBOX">
            <img class="IMG"  src="{$imgDirPath}img32_Phonebook.png"  />
            <span class="TITLE">Contact</span>
          </div>

          <div class="SUMMARY">
            <span class="FIELD">Contact:</span>
            <xsl:value-of select="field[@name='FirstName']"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="field[@name='LastName']"/>
            <br/>
            <span class="FIELD">Phone:</span>
            <xsl:value-of select="field[@name='BizPhone']"/>
            <br/>
            <span class="FIELD">Email:</span>
            <xsl:value-of select="field[@name='BizEmail']"/>
            <br/>
          </div>
        </div>

        <div class="BODY">
          <div>
            <div style="background:#C0C0FF;color:white;font-weight:bold;">
              Business
            </div>

            <span class="FIELD">Phone:</span>
            <xsl:apply-templates select="field[@name='BizPhone']"/>
            <br/>
            <span class="FIELD">Cell:</span>
            <xsl:apply-templates select="field[@name='BizCell']"/>
            <br/>
            <span class="FIELD">Fax:</span>
            <xsl:apply-templates select="field[@name='BizFax']"/>
            <br/>
            <span class="FIELD">Email:</span>
            <xsl:apply-templates select="field[@name='BizEmail']"/>

            <p/>

            <div style="border:dashed 1px #F0F0FF">
              <i>
                <xsl:value-of disable-output-escaping="yes" select="field[@name='BizTitle']"/>
              </i>
              <br/>
              <b>
                <xsl:value-of disable-output-escaping="yes" select="field[@name='BizCompany']"/>
              </b>
              <br/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='BizStreet']"/>
              <br/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='BizStreet2']"/>
              <br/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='BizCity']"/>,
              <xsl:value-of disable-output-escaping="yes" select="field[@name='BizState']"/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='BizPostalCode']"/>
              <br/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='BizCountry']"/>
              <br/>
            </div>
          </div>

          <div>
            <div style="background:#C0C0FF;color:white;font-weight:bold;">
              Personal
            </div>
            <span class="FIELD">Phone:</span>
            <xsl:apply-templates select="field[@name='HomePhone']"/>
            <br/>
            <span class="FIELD">Cell:</span>
            <xsl:apply-templates select="field[@name='HomeCell']"/>
            <br/>
            <span class="FIELD">Fax:</span>
            <xsl:apply-templates select="field[@name='HomeFax']"/>
            <br/>
            <span class="FIELD">Email:</span>
            <xsl:apply-templates select="field[@name='HomeEmail']"/>

            <p/>


            <div style="border:dashed 1px #F0F0FF">
              <xsl:value-of disable-output-escaping="yes" select="field[@name='PersStreet']"/>
              <br/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='PersStreet2']"/>
              <br/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='PersCity']"/>,
              <xsl:value-of disable-output-escaping="yes" select="field[@name='PersState']"/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='PersPostalCode']"/>
              <br/>
              <xsl:value-of disable-output-escaping="yes" select="field[@name='PersCountry']"/>
              <br/>
            </div>

          </div>

          
          
        </div>
        
        <xsl:apply-templates select="dataObject[@type='Vertex']"/>
        
          <script>
          //var sURL = window.document.location.href.toString();
          //document.getElementById("SHIT").innerHTML = "Location: "+sURL;
        </script>
        
        
      </body>
    </html>
  </xsl:template>


  <xsl:template match="dataObject[@type='Vertex']">
    <div class="FOOTER" style="backgroundColor:#F0F0FF;border:dashed 4px darkorange;">
      <span class="FIELD">
        Weight :
      </span>
      <xsl:value-of select="field[@name='Weight']"/>
      <br/>
      <span class="FIELD">
        Created :
      </span>
      <xsl:value-of select="field[@name='UTCDateCreated']"/>
      <br/>
      <span class="FIELD">
        Edited :
      </span>
      <xsl:value-of select="field[@name='UTCDateCreated']"/>
      <br/>
      <span class="FIELD">
        Accessed :
      </span>
      <xsl:value-of select="field[@name='UTCDateCreated']"/>
      <br/>

    </div>

  </xsl:template>


  </xsl:stylesheet>