<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- transform a contact data object into a XML representation for a grid row -->
  <xsl:template match="dataObject">
    <gridRow>
      <subject>
        <xsl:value-of select="field[@name='FirstName']"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="field[@name='LastName']"/>
      </subject>
      <xtra1>
        <xsl:value-of select="field[@name='Category_FK']"/>
      </xtra1>
      <xtra2>
        <xsl:value-of select="field[@name='BizEmail']"/>
      </xtra2>
      <xtra3>
        <xsl:value-of select="field[@name='BizPhone']"/>
      </xtra3>
    </gridRow>
  </xsl:template>
</xsl:stylesheet>