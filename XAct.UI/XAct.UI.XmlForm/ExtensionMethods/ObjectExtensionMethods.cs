﻿using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace XAct.UI.ExtensionMethods
{
    public static class ObjectExtensionMethods
    {
        public static void XmlNode(this object sourceObject, XmlNode xmlNode)
        {
  //          IEnumerable<IGrouping<int,PropertyInfo>> groupings = 
  //PropertyInfo.GroupBy(p => p.Name[0])
  //.OrderByDescending (g=>g.Count())//once groups formed, group by aggregate
  //.ThenByDescending 

            IEnumerable<IGrouping<string, PropertyInfo>> groupings
                = sourceObject
                    .GetType()
                    .GetProperties()
                    .GroupBy(
                        pi =>
                        (pi.GetAttributeOrDefault<DisplayAttribute>(
                        new DisplayAttribute{Order = 999, GroupName = "Default"}).GroupName));

        }
    }
}
