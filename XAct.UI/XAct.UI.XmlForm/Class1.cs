﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using System.Xml;
using System.Xml.Xsl;
using System.IO;

using XAct.Windows.Forms.Controls;


namespace XAct.UI
{

    /// <summary>
    /// <para>
    /// Form generator from and <see cref="XmlDocument"/>. 
    /// </para>
    /// <para>
    /// See Remarks.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Either takes an <see cref="XmlDocument"/> with no <c>XSLT</c>
    /// to tranform it, or it an <c>XSLT</c> document is provided, 
    /// transforms it into a document into a format that this 
    /// control can parse.
    /// </para>
    /// <para>
    /// The format of the document that will be parsed is shown below.
    /// It contains 3 'zones' (header, body, footer).
    /// The 'body' zone can contain one or more 'divisions' in it.
    /// Each 'zone' (if it contains no 'div's, or each 'div') can 
    /// contain one or more 'field' descriptions.
    /// <code>
    /// <![CDATA[
    /// <form>
    ///   <header>
    ///       <!-- A header area of one or more 'field' nodes: -->
    ///       <field name="{FieldName}" label="{FieldLabel}" dataType="System.String" hidden="false" editable="true"/>
    ///   </header>
    ///   <body>
    ///     <!-- A body area of zero or more divs/tabs: -->
    ///     <div name="{TabName}">
    ///       <!-- Each tab area containing one or more 'field' nodes: -->
    ///       <field name="{FieldName}" label="{FieldLabel}" dataType="System.String" hidden="false" editable="true"/>
    ///         <!-- Each tab having the atleast 'name'/'dataType' attributes -->
    ///     </div>
    ///     <div name="{TabName}">
    ///       ...
    ///     </div>
    ///   </body>
    ///   <footer>
    ///       <!-- A footer area of one or more 'field' nodes: -->
    ///       <field name="{FieldName}" label="{FieldLabel}" dataType="System.String" hidden="false" editable="true"/>
    ///   </footer>
    /// </form>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Each Field definition contains several attributes
    /// that are searched for when parsed into an instance
    /// of <see cref="XmlFormFieldSchema"/>.
    /// </para>
    /// </remarks>
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
    //PC:
    [DefaultEvent("FKBindingRequired")]
#endif
    public partial class XmlForm : UserControl
    {

        #region Events Raised
        /// <summary>
        /// Event raised when processing an input control 
        /// that requires Binding (such as a ComboBox).
        /// <para>
        /// Event is raised by <see cref="SetValues"/> in order to bind options,
        /// before it sets the value.
        /// </para>
        /// </summary>
        public event EventHandler<XmlFormFKInfoEventArgs> FKBindingRequired;
        #endregion

        //Control _TMPHOLDER;

        #region Constants
        //private const string C_FORM_NODE_NAME = "dataObject";
        private const string C_FORM_HEADER_NODE_NAME = "header";
        private const string C_FORM_BODY_NODE_NAME = "body";
        private const string C_FORM_FOOTER_NODE_NAME = "footer";
        private const string C_FORM_DIV_NODE_NAME = "div";
        private const string C_FORM_FIELD_NODE_NAME = "field";

        private const int C_HORIZONTALPADDING = 4;


        //Note that these two tags are used both for div and for fields:
        private const string C_NODE_DIVNAME_ATTRIBUTE_NAME = "name";
        #endregion






        #region Fields

        private bool _FormBuilt = false;

        /// <summary>
        /// Name of <c>DataObjectFactory</c> creating the Form.
        /// This is used when raising the <see cref="FKBindingRequired"/> event.
        /// </summary>
        protected string _ClassName;

        private List<XmlFormFieldSchema> _FormFieldSchemas = new List<XmlFormFieldSchema>();
        /// <summary>
        /// Collection of splitters used in between Labels and Fields.
        /// Used to keep them aligned (see <see cref="M:splitter_SplitterMoved"/>).
        /// </summary>
        protected List<Splitter> _Splitters = new List<Splitter>();


        /// <summary>
        /// The Source <see cref="XmlDocument"/>.
        /// </summary>
        protected XmlDocument _XmlDocumentSource;

        /// <summary>
        /// The result <see cref="XmlDocument"/> (after _XmlDocumentSource is transformed by XsltTransformer).
        /// </summary>
        /// <remarks>
        /// <para>
        /// Used by <see cref="CreateFormAndFields"/> to build the forms,
        /// after being built by <see cref="TransformXmlByXslt"/>.
        /// </para>
        /// </remarks>
        protected XmlDocument _XmlDocument_TransformedResult;


        /// <summary>
        /// The Xml Document containing the XSL.
        /// </summary>
        protected XmlDocument _XsltDocument;


        /*
        /// <summary>
        /// The Compiled Xslt document.
        /// </summary>
        protected XslCompiledTransform _XsltTransformer;
        */

        /// <summary>
        /// Running Max of max width needed to display all labels without cut off.
        /// Used by <see cref="CreateFormAndFields"/> in conjunction with <see cref="_LabelPanels"/>
        /// </summary>
        protected int _GlobalLabelWidth;
        /// <summary>
        /// Collection of all LabelsPanels (panels used for fields in various tabs).
        /// This is used at end of <see cref="CreateFormAndFields"/> to set a common width
        /// for all of them, as determined by <see cref="_GlobalLabelWidth"/>...
        /// </summary>
        protected List<Panel> _LabelPanels = new List<Panel>();
        /// <summary>
        /// Unused at present.
        /// </summary>
        protected List<Panel> _InputPanels = new List<Panel>();


        /// <summary>
        /// The header panel in which to put child Input controls.
        /// </summary>
        protected Panel UI_HeaderPanel;
        /// The main panel in which to put child Input controls.
        protected Panel UI_BodyPanel;
        /// <summary>
        /// The footer panel in which to put child Input controls.
        /// </summary>
        protected Panel UI_FooterPanel;
        #endregion




        #region Properties


        /// <summary>
        /// Gets the vertical spacing.
        /// </summary>
        /// <value>The vertical spacing.</value>
        [
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            // Attributes not available in Compact NET (cf: DesignTimeAttributes.xmta)
        Browsable(true),
        Category(" XAct"),
        Description("Gets the vertical spacing between input controls."),
#endif
 DefaultValue(4),
        ]
        public int VerticalSpacing
        {
            get
            {
                return _VerticalSpacing;
            }
            set
            {
                if (value != _VerticalSpacing)
                {
                    _VerticalSpacing = value;
                    Invalidate();

                }
            }
        }
        private int _VerticalSpacing = 4;


        /// <summary>
        /// Gets or sets the min width of the labels.
        /// </summary>
        /// <value>The min width of the labels.</value>
        [
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            // Attributes not available in Compact NET (cf: DesignTimeAttributes.xmta)
        Browsable(true),
        Category(" XAct"),
        Description("Gets or sets the min width of the labels."),
#endif
 DefaultValue(50),
        ]
        public int LabelMinWidth
        {
            get
            {
                return _LabelMinWidth;
            }
            set
            {
                if (value != _LabelMinWidth)
                {
                    _LabelMinWidth = value;
                    Invalidate();
                }
            }
        }
        private int _LabelMinWidth = 50;


        /// <summary>
        /// Gets or sets the labels vertical offset.
        /// </summary>
        /// <value>The label vertical offset.</value>
        [
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            // Attributes not available in Compact NET (cf: DesignTimeAttributes.xmta)
        Browsable(true),
        Category(" XAct"),
        Description("Gets or sets the labels vertical offset."),
#endif
 DefaultValue(8),
        ]
        public int LabelVerticalOffset
        {
            get
            {
                return _LabelVerticalOffset;
            }
            set
            {
                if (value != _LabelVerticalOffset)
                {
                    _LabelVerticalOffset = value;
                    Invalidate();
                }
            }
        }
        private int _LabelVerticalOffset = 8;


        /// <summary>
        /// Gets or sets the max width of the labels.
        /// </summary>
        /// <value>The max width of the labels.</value>
        [
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            // Attributes not available in Compact NET (cf: DesignTimeAttributes.xmta)
        Browsable(true),
        Category(" XAct"),
        Description("Gets or sets the max width of the labels."),
#endif
 DefaultValue(100),
        ]
        public int LabelMaxWidth
        {
            get
            {
                return _LabelMaxWidth;
            }
            set
            {
                if (value != _LabelMaxWidth)
                {
                    _LabelMaxWidth = value;
                    Invalidate();
                }
            }
        }
        private int _LabelMaxWidth = 100;


        /// <summary>
        /// Gets the XSLT argument list to be used when transforming the Xml file.
        /// </summary>
        /// <value>The XSLT argument list.</value>
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
        [
            // Attributes not available in Compact NET (cf: DesignTimeAttributes.xmta)
        Browsable(true),
        Category(" XAct"),
        Description("Gets the XSLT argument list to be used when transforming the Xml file."),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        ]
#endif
        public System.Collections.Specialized.NameValueCollection XsltArgumentList
        {
            get
            {
                return _XsltArgumentList;
            }
        }
        private System.Collections.Specialized.NameValueCollection _XsltArgumentList = new System.Collections.Specialized.NameValueCollection();
        /*
        public XsltArgumentList XsltArgumentList {
          get {
            return _XsltArgumentList;
          }
        }
        private XsltArgumentList _XsltArgumentList = new XsltArgumentList();
        */



        /// <summary>
        /// Gets or sets the path to an optional XSLT file.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Path to an Xslt file that will transform the Xml file
        /// to secondary xml file that follows the following format:
        /// <code>
        /// <![CDATA[
        /// <form>
        ///   <header>
        ///       <!-- A header area of one or more 'field' nodes: -->
        ///       <field name="{FieldName}" label="{FieldLabel}" dataType="System.String" hidden="false" editable="true">
        ///   </header>
        ///   <body>
        ///     <!-- A body area of zero or more divs/tabs: -->
        ///     <div name="{TabName}">
        ///       <!-- Each tab area containing one or more 'field' nodes: -->
        ///       <field name="{FieldName}" label="{FieldLabel}" dataType="System.String" hidden="false" editable="true">
        ///         <!-- Each tab having the atleast 'name'/'dataType' attributes -->
        ///     </div>
        ///     <div name="{TabName}">
        ///       ...
        ///     </div>
        ///   </body>
        ///   <footer>
        ///       <!-- A footer area of one or more 'field' nodes: -->
        ///       <field name="{FieldName}" label="{FieldLabel}" dataType="System.String" hidden="false" editable="true">
        ///   </footer>
        /// </form>
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <value>The XSLT path.</value>
        [
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            // Attributes not available in Compact NET (cf: DesignTimeAttributes.xmta)
        Browsable(true),
        Category(" XAct"),
        Description("Gets or sets the XSLT path."),
#endif
 DefaultValue(null),
        ]
        public string XsltPath
        {
            get
            {
                return _XsltPath;
            }
            set
            {
                if (value != _XsltPath)
                {
                    _XsltPath = value;


                    _XsltDocument = null;
                    if (File.Exists(_XsltPath))
                    {
                        _XsltDocument = new XmlDocument();
                        _XsltDocument.Load(_XsltPath);
                    }
                    /*
                    _XsltTransformer = null;
                    if (File.Exists(_XsltPath)) {
                      _XsltTransformer = new XslCompiledTransform();
                      _XsltTransformer.Load(_XsltPath);
                    }
                     */
                    TransformXmlByXslt();
                }
            }
        }
        private string _XsltPath;




        /// <summary>
        /// Gets or sets the XML path.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The path to an xml file that has the following format:
        /// <code>
        /// <![CDATA[
        /// <?xml version="1.0"?>
        /// <dataObject>
        ///   <field tab="0" name="ID" label="Id" hidden="true">aaaa-bbbb-cccc-dddd</field>
        ///   <field tab="1" name="Title" dataType="System.String" label="Title" hidden="false" editable="true" type="Text" validationMinLength="1" validationMaxLength="128" inputType="Text">Mr.</field>
        ///   <field tab="1" name="NameFirst" dataType="System.String" label="First Name" hidden="false" editable="true" type="Text" validationMinLength="0" validationMaxLength="128" inputType="Text">John</field>
        ///   ...
        /// </dataObject>
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <value>The XML path.</value>
        [
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            // Attributes not available in Compact NET (cf: DesignTimeAttributes.xmta)
        Browsable(true),
        Category(" XAct"),
        Description("Gets or sets the XML path."),
#endif
 DefaultValue(null),
        ]
        public string XmlPath
        {
            get
            {
                return _XmlPath;
            }
            set
            {
                if (value != _XmlPath)
                {
                    XmlDocument doc = null;
                    _XmlPath = value;
                    if (System.IO.File.Exists(_XmlPath))
                    {
                        doc = new XmlDocument();
                        doc.Load(_XmlPath);
                    }
                    LoadXml(doc);
                }
            }
        }
        private string _XmlPath;



        /// <summary>
        /// Gets a value indicating whether all the input fields on this form are valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
        [
            // Attributes not available in Compact NET (cf: DesignTimeAttributes.xmta)
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
        ]
#endif
        public bool IsValid
        {
            get
            {
                bool result = true;
                //Loop through each formFieldSchema
                foreach (XmlFormFieldSchema formFieldSchema in _FormFieldSchemas)
                {
                    if (!formFieldSchema.Validation.IsValid)
                    {
                        result = false;
                        break;
                    }
                }
                return result;
            }
        }
        #endregion


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="T:XmlForm"/> class.
        /// </summary>
        public XmlForm()
        {
            this.BackColor = Color.White;
            InitializeComponent();
        }
        #endregion



        #region Method Overrides - Control
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.UserControl.Load"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
#endif


#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Validating"></see> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.ComponentModel.CancelEventArgs"></see> that contains the event data.</param>
        protected override void OnValidating(CancelEventArgs e)
        {
            base.OnValidating(e);
            if (!e.Cancel)
            {
                //e.Cancel = !this.IsValid;
            }
        }
#endif

        /// <summary>
        /// Validates the value.
        /// </summary>
        /// <param name="formFieldSchema">The <see cref="XmlFormFieldSchema"/> that describes the requirements of this Field in the form.</param>
        /// <param name="value">The value.</param>
        protected virtual void ValidateValue(XmlFormFieldSchema formFieldSchema, object value)
        {
            if (formFieldSchema.Validation.IsRequired)
            {
                //
            }
        }

#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Validated"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        protected override void OnValidated(EventArgs e)
        {
            base.OnValidated(e);
        }
#endif

        /// <summary>
        /// Raises the <see cref="E:Resize"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            /*
            if (_TMPHOLDER != null) {
              _TMPHOLDER.Location = new Point(this.ClientRectangle.Right, this.ClientRectangle.Bottom);
              _TMPHOLDER.Size = new Size(16, 16);
            }
             */

        }
        #endregion

        #region Protected - Event Handlers - Field Splitters
        void splitter_SplitterMoved(object sender, EventArgs e)
        {
            if (splitting)
            {
                return;
            }
            //Turn on flag:
            splitting = true;
            int offset1 = _GetOffset((Splitter)sender);
            int splitPosition = ((Splitter)sender).SplitPosition;

            foreach (Splitter splitter in _Splitters)
            {
                if (splitter == sender)
                {
                    //Do not reapply to oneself:
                    continue;
                }
                int offset2 = _GetOffset(splitter);
                splitter.SplitPosition = splitPosition + (offset1 - offset2);
            }
            //Turn back off flag:
            splitting = false;
        }
        bool splitting = false;
        #endregion


        #region RaiseEvents - FKBindingRequired
        /// <summary>
        /// <para>
        /// Raises the <see cref="E:FKBindingRequired"/> event.
        /// </para>
        /// <para>
        /// A Handler must be provided, and <c>e.Handled</c> set to <c>true</c>, or an error will be raised.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="SetValues"/> when it comes across a <see cref="ListControl"/>
        /// or other <see cref="Control"/> that needs to be DataBound.
        /// </para>
        /// <para>
        /// An example event handler might look like this:
        /// <code>
        /// <![CDATA[
        /// private void universalInputForm1_FKBindingRequired(object sender, XmlFormFKInfoEventArgs e) {
        ///   if (e.FormFieldSchema.FKInfo.ToUpper() == "SexType".ToUpper()) {
        ///     //We theoretically know what type of control this is:
        ///     ComboBox combo = ((ComboBox)e.FormFieldSchema.InputControl);
        ///     combo.Items.Clear();
        ///     //ListItem is a simple class with two properties
        ///     //ListItem {public string Text{get;set;} public object Value{get;set;}}
        ///     List<ListItem> list = new List<ListItem>();
        ///     list.Add(new ListItem("Unknown", 0));
        ///     list.Add(new ListItem("Male", 1));
        ///     list.Add(new ListItem("Female", 2));
        ///     //Bind:
        ///     combo.DisplayMember = "Text";
        ///     combo.ValueMember = "Value";
        ///     combo.DataSource = list;
        ///     //Mark that we are done:
        ///     e.Handled = true;
        ///   }
        /// }  
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Note the problems with this type of architecture is that the UI
        /// has to have references to a lot of other assemblies before
        /// it can complete the filling in of the list.
        /// </para>
        /// <para>
        /// Even if done as a service, the service is still not 'dumb' enough
        /// to make hot swapping modules easy later.
        /// </para>
        /// </remarks>
        /// <param name="e">The <see cref="T:XmlFormFKInfoEventArgs"/> instance containing the event data.</param>
        protected void OnFKBindingRequired(XmlFormFKInfoEventArgs e)
        {
            if (FKBindingRequired == null)
            {
                throw new System.Exception(
                  string.Format(
                  "FK info is required for Field '{0}', but no event handler has been provided.",
                  e.FormFieldSchema.Name
                  ));
            }
            //Call Handler:
            FKBindingRequired(this, e);

            //Upon return, check:
            if (e.Handled == false)
            {
                throw new System.Exception(
                  string.Format(
                  "FK Handled flag must be set to true for Field '{0}'.",
                  e.FormFieldSchema.Name
                  ));
            }
        }
        #endregion


        #region Public Methods - Xml/Xsl
        /// <summary>
        /// Loads the XSLT (the argument is NOT a Path).
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes <see cref="TransformXmlByXslt"/>,
        /// which ends up calling <see cref="InvalidateAndReCreateFormAndFields"/>.
        /// </para>
        /// </remarks>
        /// <param name="xslt">The XSLT (NOT A PATH).</param>
        public void LoadXslt(string xslt)
        {
            XmlReader xmlReader = XmlReader.Create(new StringReader(xslt));
            LoadXslt(xmlReader);
        }
        /// <summary>
        /// Loads the XSLT.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes <see cref="TransformXmlByXslt"/>,
        /// which ends up calling <see cref="InvalidateAndReCreateFormAndFields"/>.
        /// </para>
        /// </remarks>
        /// <param name="xmlReader">The XML reader.</param>
        public void LoadXslt(XmlReader xmlReader)
        {
            _XsltDocument = null;

            if (xmlReader != null)
            {
                new XmlDocument();
                _XsltDocument.Load(xmlReader);
            }

            /*
            _XsltTransformer = null;
            if (xmlReader != null) {
              _XsltTransformer = new XslCompiledTransform();
              _XsltTransformer.Load(xmlReader);
            }
             */

            TransformXmlByXslt();
        }

        /// <summary>
        /// Loads the XSLT.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes <see cref="TransformXmlByXslt"/>,
        /// which ends up calling <see cref="InvalidateAndReCreateFormAndFields"/>.
        /// </para>
        /// </remarks>
        /// <param name="xmlDocument">The XML document.</param>
        public void LoadXslt(XmlDocument xmlDocument)
        {
            _XsltDocument = xmlDocument;

            /* 
            _XsltTransformer = null;
            if (xmlDocument != null) {
              _XsltTransformer = new XslCompiledTransform();
              _XsltTransformer.Load(xmlDocument);
            }
             */
            TransformXmlByXslt();
        }

        /// <summary>
        /// Loads the XML (the argument is NOT a Path).
        /// </summary>
        /// <remarks>
        /// <para>
        /// Ends up invoking <see cref="TransformXmlByXslt"/>,
        /// which ends up calling <see cref="InvalidateAndReCreateFormAndFields"/>.
        /// </para>
        /// </remarks>
        /// <param name="xml">The XML (NOT A PATH).</param>
        public void LoadXml(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            LoadXml(doc);
        }



        /// <summary>
        /// Loads the XML.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Ends up invoking <see cref="TransformXmlByXslt"/>,
        /// which ends up calling <see cref="InvalidateAndReCreateFormAndFields"/>.
        /// </para>
        /// </remarks>
        /// <param name="xmlReader">The XML reader.</param>
        public void LoadXml(XmlReader xmlReader)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlReader);
            LoadXml(doc);
        }

        /// <summary>
        /// Loads the XML.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes <see cref="TransformXmlByXslt"/>,
        /// which ends up calling <see cref="InvalidateAndReCreateFormAndFields"/>.
        /// </para>
        /// </remarks>
        /// <param name="doc">The doc.</param>
        public void LoadXml(XmlDocument doc)
        {
            _XmlDocumentSource = doc;
            TransformXmlByXslt();
        }





        #endregion

        #region Public Methods - Values

        /// <summary>
        /// Get a dictionary of the Input control's Names and their untyped Values.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetValues()
        {
            Dictionary<string, object> vals = new Dictionary<string, object>();

            foreach (XmlFormFieldSchema formFieldSchema in _FormFieldSchemas)
            {
                vals[formFieldSchema.Name] = formFieldSchema.Value;
            }
            return vals;
        }
        #endregion

        #region Protected Methods Xml/Xsl
        /// <summary>
        /// Transforms the <see cref="XmlDocument"/> by the supplied <c>Xslt</c>
        /// in order to get an <see cref="XmlDocument"/> in the format that this control
        /// expects.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by changing <see cref="XsltPath"/>,
        /// <see cref="LoadXml(XmlDocument)"/> 
        /// and <see cref="LoadXslt(XmlDocument)"/>, 
        /// <see cref="LoadXslt(XmlReader)"/>.
        /// </para>
        /// <para>
        /// Invoked by the any one of the <see cref="LoadXslt(string)"/> and 
        /// the <see cref="LoadXml(string)"/> methods.
        /// </para>
        /// </remarks>
        protected void TransformXmlByXslt()
        {

            //Clear the result doc:
            _XmlDocument_TransformedResult = null;

            if ((_XmlDocumentSource == null) || (_XsltDocument == null))
            { /*|| (_XsltTransformer == null)*/
                //Not enough to do a transformation...
                //But it may be that _XmlDocumentSource (if not null)
                //has enough to produce a good document (in other words,
                //that it is already in div/field format.
                _XmlDocument_TransformedResult = _XmlDocumentSource;
            }
            else
            {
                //Although a stream is nice, we want to work 
                //with the result as a DOM tree in order to use XPath...
                //So load it back into one self:
                _XmlDocument_TransformedResult = new XmlDocument();
                _XmlDocument_TransformedResult.LoadXml(Transform(_XmlDocumentSource, _XsltDocument));
            }

            //Rebuild the form:
            InvalidateAndReCreateFormAndFields();
        }
        #endregion

        #region Protected Methods - CreateForm
        /// <summary>
        /// Invalidates any form already built and 
        /// re invokes <see cref="CreateFormAndFields"/>.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="TransformXmlByXslt"/>.
        /// </para>
        /// </remarks>
        protected void InvalidateAndReCreateFormAndFields()
        {
            _FormBuilt = false;
            CreateFormAndFields();
            Invalidate();

        }


        /// <summary>
        /// Creates the form from the information in the <see cref="XmlDocument"/> that was created by 
        /// <see cref="TransformXmlByXslt"/>.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="InvalidateAndReCreateFormAndFields"/>, and
        /// when either <see cref="P:XslPath"/> or <see cref="P:XmlPath"/>
        /// is changed.
        /// </para>
        /// <para>
        /// Creates 3 'zones' (<c>Header</c>, <c>Body</c>, and <c>Footer</c>), 
        /// which it lays out within this control.
        /// </para>
        /// <para>
        /// The <c>Header</c> and <c>Footer</c> panels are hidden if they
        /// contain no <c>Fields</c> (<see cref="Label"/> and <c>InputControl</c>).
        /// </para>
        /// </remarks>
        protected virtual void CreateFormAndFields()
        {
            if (_FormBuilt)
            {
                return;
            }

            _FormBuilt = true;

            //Clear everything first:
            _GlobalLabelWidth = 0;
            _FormFieldSchemas.Clear();
            _LabelPanels.Clear();
            _InputPanels.Clear();
            _Splitters.Clear();
            this.Controls.Clear();

            if (_XmlDocument_TransformedResult == null)
            {
                //No document, means no nodes, so no fields...
                return;
            }


            XmlElement formNode;
            formNode = (XmlElement)_XmlDocument_TransformedResult.DocumentElement;
            if (formNode == null)
            {
                //No root node, means no nodes, so no fields...
            }
            _ClassName = formNode.GetAttribute("type");
            //int Y = 0;

            UI_HeaderPanel = CreateHeaderZone((XmlElement)formNode.SelectSingleNode(C_FORM_HEADER_NODE_NAME));
            UI_FooterPanel = CreateFooterZone((XmlElement)formNode.SelectSingleNode(C_FORM_FOOTER_NODE_NAME));

            //The body node is the main node if no body node provided:
            XmlElement bodyNode = (XmlElement)formNode.SelectSingleNode(C_FORM_BODY_NODE_NAME);
            if (bodyNode == null)
            {
                bodyNode = formNode;
            }
            UI_BodyPanel = CreateBodyZone(bodyNode);


            //int hHeight = (UI_HeaderPanel.Visible) ? UI_HeaderPanel.Height : 0;
            //int fHeight = (UI_FooterPanel.Visible)?UI_FooterPanel.Height:0;

            //UI_BodyPanel.Location = new Point(0, hHeight);
            //UI_BodyPanel.Size = new Size(this.ClientRectangle.Width, this.ClientRectangle.Height - hHeight - fHeight);
            //We make a header panel even if no elements/fields within it:



            //Size the labels across Header/Body/BodyTabPages/Footer so that they are
            //all the same width:
            //if (_LabelPanels.Count > 0) {
            //  _LabelPanels[0].Width = C_HORIZONTALPADDING + _GlobalLabelWidth + C_HORIZONTALPADDING;
            //}
            foreach (Control labelPanel in _LabelPanels)
            {
                labelPanel.Width = C_HORIZONTALPADDING + _GlobalLabelWidth + C_HORIZONTALPADDING;
            }
            if (_Splitters.Count > 0)
            {
                splitter_SplitterMoved(_Splitters[0], EventArgs.Empty);
            }

            //Add Dock.Fill first, then the other two parts which will lop off some of it:
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            //PC:
            this.Controls.AddRange(new Control[] { UI_BodyPanel, UI_HeaderPanel, UI_FooterPanel });
#else
      this.Controls.Add(UI_BodyPanel);
      this.Controls.Add(UI_HeaderPanel);
      this.Controls.Add(UI_FooterPanel);
#endif

            UI_FooterPanel.BackColor = Color.Orange;
            //_TMPHOLDER = new Button();
            //c.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            //this.Controls.Add(_TMPHOLDER);
            //UI_BodyPanel.AutoScroll = true;//(_XsltTransformer == null);

            //UI_BodyPanel.VerticalScroll.Visible=true;
            //UI_BodyPanel.VerticalScroll.Maximum = 1000;
            //UI_BodyPanel.Bounds.Height;
            int fieldsFound = _FormFieldSchemas.Count;

            //This can only be done *after* the panels have been added to the form.

            if (this.TopLevelControl is Form)
            {
                SetValues();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:ParentChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnParentChanged(EventArgs e)
        {
            if (this.TopLevelControl is Form)
            {
                SetValues();
            }
            base.OnParentChanged(e);
        }
        /// <summary>
        /// Creates the header zone panel.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="CreateFormAndFields"/>.
        /// </para>
        /// <para>
        /// Invoks by <see cref="CreateZone"/>.
        /// </para>
        /// <para>
        /// Note that the panel always is returned -- it 
        /// just might not be visible if it contains no Fields.
        /// </para>
        /// </remarks>
        /// <param name="node">The node.</param>
        /// <returns></returns>
        protected virtual Panel CreateHeaderZone(XmlElement node)
        {
            //Check Args:
            if (node == null)
            {
                //  throw new System.ArgumentNullException("node");
            }
            Panel container = CreateZone(node);
            container.Dock = DockStyle.Top;
            container.Visible = (container.Controls.Count > 0);
            return container;
        }

        /// <summary>
        /// Creates the body zone panel.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="CreateFormAndFields"/>.
        /// </para>
        /// <para>
        /// Invoks by <see cref="CreateZone"/>.
        /// </para>
        /// </remarks>
        /// <param name="node">The node.</param>
        /// <returns></returns>
        protected virtual Panel CreateBodyZone(XmlElement node)
        {
            //Check Args:
            if (node == null)
            {
                throw new System.ArgumentNullException("node");
            }
            Panel container = CreateZone(node);
            container.Dock = DockStyle.Fill;
            return container;
        }

        /// <summary>
        /// Creates the footer zone panel.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="CreateFormAndFields"/>.
        /// </para>
        /// <para>
        /// Invoks by <see cref="CreateZone"/>.
        /// </para>
        /// <para>
        /// Note that the panel always is returned -- it 
        /// just might not be visible if it contains no Fields.
        /// </para>
        /// </remarks>
        /// <param name="node">The node.</param>
        /// <returns></returns>
        protected virtual Panel CreateFooterZone(XmlElement node)
        {
            //Check Args:
            if (node == null)
            {
                //throw new System.ArgumentNullException("node");
            }
            //First we look for any Bottom fields...
            Panel container = CreateZone(node);
            //Then we add buttons...
            container.Dock = DockStyle.Bottom;
            container.Visible = (container.Controls.Count > 0);
            return container;
        }

        /// <summary>
        /// Create a container Panel, and (optionally) a Tabber within it, 
        /// </summary>
        /// <internal>
        /// This method is used by all 3 main panel routines (<see cref="M:CreateHeaderZone"/>,
        /// <see cref="M:CreateBodyZone"/>, and <see cref="M:CreateFooterZone"/>).
        /// to create a panel, and (optionally) a Tabber within it.
        /// </internal>
        /// <param name="node"></param>
        /// <returns></returns>
        protected virtual Panel CreateZone(XmlElement node)
        {
            //Check Args:
            if (node == null)
            {
                //throw new System.ArgumentNullException("node");
            }
            //NB: 
            //The passed node is a 'header', 'body', or 'footer' node...
            Panel zoneContainer = new Panel();
            zoneContainer.BackColor = Color.Transparent;

            if (node != null)
            {
                bool makeTabber = false;

                //A Body, but also a Header/Footer, can have 0 or more Div's in it...
                XmlNodeList divs = node.SelectNodes(C_FORM_DIV_NODE_NAME);
                int divCount = divs.Count;
                if (divCount > 0)
                {
                    makeTabber = true;
                    if (divCount == 1)
                    {
                        //Double check:
                        node = (XmlElement)divs[0];
                        node.SelectNodes(C_FORM_FIELD_NODE_NAME);
                        makeTabber = false;
                    }

                }
                //Make a Tabber within this zone:

                if (makeTabber)
                {
                    Control tabber = CreateTabControl(zoneContainer, divs);
                    //Now add the tabber to the Panel:
                    zoneContainer.Controls.Add(tabber);
                }
                else
                {
                    //No divs in this...

                    //We turn on scrolling on this zoneContainer 
                    //and turn off scrolling on its children...
                    ((Panel)zoneContainer).AutoScroll = true;
                    //No divs to process...
                    FillDivContainerWithFields(node, zoneContainer);

                }
            }

            return zoneContainer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// <para>
        /// It is important to note that 
        /// not all forms benefit from being laid out in a <see cref="TabControl"/>.
        /// </para>
        /// <para>
        /// This routine is only called by <see cref="M:CreateZone"/>
        /// if the node has more than one div node within it. 
        /// </para>
        /// <para>
        /// If there was none, or only one div,
        /// then <see cref="M:CreateZone"/> does not create 
        /// a <see cref="TabControl"/>, or <see cref="TabPage"/>s within it, 
        /// but calls <see cref="FillDivContainerWithFields"/> directly.
        /// </para>
        /// </remarks>
        /// <param name="parentContainer"></param>
        /// <param name="divs"></param>
        /// <returns></returns>
        protected virtual Control CreateTabControl(Control parentContainer, XmlNodeList divs)
        {
            //Check Args:
            if ((divs == null) || (divs.Count == 0))
            {
                throw new System.ArgumentNullException("divs");
            }
            TabControl tabber = new TabControl();

            tabber.Location = new Point(0, 0);
            tabber.Padding = new Point(C_HORIZONTALPADDING, C_HORIZONTALPADDING);
            tabber.Dock = DockStyle.Fill;
            tabber.Location = new Point(0, 0);
            tabber.BackColor = Color.Red;
            foreach (XmlElement divNode in divs)
            {
                TabPage tabPage = new TabPage();
                //Add the tabPage to the tabber:
                tabber.TabPages.Add(tabPage);
                //We turn on scrolling on this zoneContainer 
                //and turn off scrolling on its children...
                tabPage.AutoScroll = true;
                FillDivContainerWithFields(divNode, tabPage);
            }

            return tabber;
        }

        /// <summary>
        /// Fills the passed Container Control with two <see cref="Panel"/>s,
        /// separated by a <see cref="Splitter"/>, and then loops through
        /// the child nodes of the passed <see cref="XmlElement"/> in order to 
        /// fill the left <see cref="Panel"/>
        /// with field <see cref="Label"/>s, and the right <see cref="Panel"/> with 
        /// the <c>InputControl</c> specified in the child <see cref="XmlElement"/>.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by either <see cref="CreateTabControl"/> or <see cref="CreateZone"/>.
        /// </para>
        /// <para>
        /// Fills the passed container (either a TabPage or the Zone panel itself)
        /// with two new <see cref="Panel"/> containers -- one for <c>Fields labels</c>,
        /// and one for <c>Input controls</c>, with a  <see cref="Splitter"/> between 
        /// the two container controls.
        /// </para>
        /// <para>
        /// Once the two <see cref="Panel"/>s are created, loops through the child
        /// <c>field</c> nodes of the passed <see cref="XmlElement"/> that describes 
        /// the passed Containers fields.
        /// <br/>
        /// Each field <see cref="XmlElement"/> is parsed into a 
        /// <see cref="XmlFormFieldSchema"/> element,
        /// which is then used to create a <see cref="Label"/>, as well
        /// as the <c>InputControl</c> specified by the <see cref="XmlFormFieldSchema"/>.
        /// </para>
        /// </remarks>
        /// <param name="divNode"></param>
        /// <param name="divContainer"></param>
        protected virtual void FillDivContainerWithFields(XmlElement divNode, Control divContainer)
        {
            //Check Args:
            if (divNode == null)
            {
                throw new System.ArgumentNullException("divNode");
            }
            if (divContainer == null)
            {
                throw new System.ArgumentNullException("divContainer");
            }
            //NB:
            //The passed container will be a TabPage if there were multiple DIVs
            //or it will be a Panel if there were no Tabs, and therefore we are using
            //just the Panel container with no Tabber within it....


            divContainer.BackColor = this.BackColor;

            //Whether a Panel or Group or whatever,
            //we give it a Label if there is one:
            divContainer.Text = divNode.GetAttribute(C_NODE_DIVNAME_ATTRIBUTE_NAME);

            XmlNodeList fields = divNode.SelectNodes(C_FORM_FIELD_NODE_NAME);
            int iMax = fields.Count;
            if (iMax == 0)
            {
                return;
            }

            int Y = _VerticalSpacing;

            Panel labelsContainer = new Panel();
            Panel inputsContainer = new Panel();
            Splitter splitter = new Splitter();



            _LabelPanels.Add(labelsContainer);
            _InputPanels.Add(inputsContainer);
            _Splitters.Add(splitter);

            labelsContainer.BackColor = this.BackColor;
            labelsContainer.Dock = DockStyle.Left;
            labelsContainer.AutoScroll = false; //Turn off scrolling on this panel... 

            inputsContainer.BackColor = this.BackColor;
            inputsContainer.Dock = DockStyle.Fill;
            inputsContainer.AutoScroll = false; //Turn off scrolling on this panel... 

            //Add splitter before adding second panel:
            splitter.Dock = DockStyle.Left;
            splitter.BackColor = this.BackColor;
            splitter.SplitterMoved += new SplitterEventHandler(splitter_SplitterMoved);


            //Loop through the fields described within the container node
            foreach (XmlElement field in fields)
            {
                //Creat a new in-mem instance:
                XmlFormFieldSchema formFieldSchema = new XmlFormFieldSchema(field);
                //Cache it:
                _FormFieldSchemas.Add(formFieldSchema);

                //Build the Input and Label controls:
                Control label = CreateFieldLabelControl(formFieldSchema, Y, ref labelsContainer);
                Control input = CreateFieldInputControl(formFieldSchema, Y, ref inputsContainer);

                //Move the Y position down by the larger of the two
                //(ie ListBox would be taller than a simple label):
                Y += Math.Max(label.Height, input.Height);
                //Push it down slightly to create spacing in between labels:
                Y += _VerticalSpacing;

                //And keep a running tally of what was the max size
                //needed to show all labels:
                _GlobalLabelWidth = Math.Max(_GlobalLabelWidth, label.Width);

            }
            //When done with all the fields
            //Ensure that the Label width is between the two 
            //preferences:
            _GlobalLabelWidth = Math.Max(_LabelMinWidth, _GlobalLabelWidth);
            _GlobalLabelWidth = Math.Min(_GlobalLabelWidth, _LabelMaxWidth);





            if (divContainer is TabPage)
            {
                //TabPage sizes are handled by the Tabber control
                //so there is nothing we need to do here...
            }
            else
            {
                //Right...so its a Panel most probably...
                //so we want to say that the panel has a height
                //that is atleast the height needed to show all
                //fields in the panel:
                divContainer.Height = Y;
            }

            //Add the Dock.Full first, then splitter, then Dock.Left:
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            //PC:
            divContainer.Controls.AddRange(new Control[] { inputsContainer, splitter, labelsContainer });
#else
      divContainer.Controls.Add(inputsContainer);
      divContainer.Controls.Add(splitter);
      divContainer.Controls.Add(labelsContainer);
#endif

        }




        /// <summary>
        /// Places a <see cref="Label"/> in the passed <see cref="Panel"/> control,
        /// at the <c>Y</c> position specified, and gives it the label value specified
        /// in the <see cref="XmlFormFieldSchema"/>.
        /// <para>
        /// The <see cref="XmlFormFieldSchema"/> was created in <see cref="FillDivContainerWithFields"/>.
        /// </para>
        /// </summary>
        /// <internal>
        /// Invoked by <see cref="FillDivContainerWithFields"/> to 
        /// place a <see cref="Label"/> in the passed <c>labelsContainer</c>.
        /// </internal>
        /// <param name="formFieldSchema">The <see cref="XmlFormFieldSchema"/> that describes the requirements of this Field in the form.</param>
        /// <param name="Y">The Y position at which to create the <see cref="Control"/>.</param>
        /// <param name="labelsContainer"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the formFieldSchema property is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the labelContainer property is null.</exception>
        protected virtual Control CreateFieldLabelControl(XmlFormFieldSchema formFieldSchema, int Y, ref Panel labelsContainer)
        {
            //Check Args:
            if (formFieldSchema == null)
            {
                throw new System.ArgumentNullException("formFieldSchema");
            }
            if (labelsContainer == null)
            {
                throw new System.ArgumentNullException("labelsContainer");
            }


            //Get the label has been designated for the 
            //Input Control:
            string labelText = formFieldSchema.Label;

            Label label = new Label();

            label.BackColor = labelsContainer.BackColor;
            label.Text = labelText;
            label.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left;
            label.Location = new Point(C_HORIZONTALPADDING, Y + _LabelVerticalOffset);
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            //PC:
            label.AutoSize = true;
#endif

            //Finally, add to parent control:
            labelsContainer.Controls.Add(label);
            //Return new label:
            //Note that it is returned so that the invoker can 
            //decide which one was bigger (label or input control)
            //and move Y down by that much:
            return label;
        }

        /// <summary>
        /// <para>
        /// Creates an <c>InputControl</c> according to the specs of the passed
        /// <see cref="XmlFormFieldSchema"/> and places it in the passed <see cref="Panel"/> control,
        /// at the <c>Y</c> position specified.
        /// </para>
        /// <para>
        /// The <see cref="XmlFormFieldSchema"/> was created in <see cref="FillDivContainerWithFields"/>.
        /// </para>
        /// </summary>
        /// <remarks>
        /// Invoked by <see cref="M:FillDivContainerWithFields"/> to place 
        /// an <c>input control</c> in the passed <c>inputsContainer</c>.
        /// </remarks>
        /// <param name="formFieldSchema">The <see cref="XmlFormFieldSchema"/> that describes the requirements of this Field in the form.</param>
        /// <param name="Y">The Y position at which to create the <see cref="Control"/>.</param>
        /// <param name="inputsContainer">The container in which to create the Control.</param>
        /// <returns>The created <see cref="Control"/>.</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the formFieldSchema property is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the inputsContainer property is null.</exception>
        protected virtual Control CreateFieldInputControl(XmlFormFieldSchema formFieldSchema, int Y, ref Panel inputsContainer)
        {
            if (formFieldSchema == null)
            {
                throw new System.ArgumentNullException("formFieldSchema");
            }
            if (inputsContainer == null)
            {
                throw new System.ArgumentNullException("inputsContainer");
            }


            //Get the control that was created:
            Control inputControl = formFieldSchema.InputControl;

            //And place it:
            inputControl.Location = new Point(C_HORIZONTALPADDING, Y);

            //Adjust its width to fill parent container's width:
            //Important - we're adjusting width according to future parent -
            //even if not yet added to parent...
            inputControl.Width = inputsContainer.Width - (2 * C_HORIZONTALPADDING);
            //Blah blah...
            inputControl.BackColor = inputsContainer.BackColor;



            //And finally the all important anchor issue:
            if ((formFieldSchema.AnchorBottom) && (formFieldSchema.IsLast))
            {
                //Modify its height to be all remaining height:
                inputControl.Height = inputsContainer.Height - inputControl.Location.Y - _VerticalSpacing;
                inputControl.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left;
            }
            else
            {
                //Just anchor in usual way:
                inputControl.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left;
            }
            //It's already been setup with Enabled/Visible...

            //Finally, add to parent control:
            inputsContainer.Controls.Add(inputControl);

            //and return it:
            //Note that it is returned so that the invoker can 
            //decide which one was bigger (label or input control)
            //and move Y down by that much:
            return inputControl;

        }



        #endregion

        #region Protected Methods -  SetValues
        /// <summary>
        /// TODO:TODOC:
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="CreateFormAndFields"/>.
        /// </para>
        /// <para>
        /// Invoked by <see cref="OnParentChanged"/>.
        /// </para>
        /// </remarks>
        protected virtual void SetValues()
        {

            //Loop through each field
            foreach (XmlFormFieldSchema formFieldSchema in _FormFieldSchemas)
            {


                //Always Bind lookup data before we set value:
                //Now...what do we do if it needs more info...eg a Select/Combo?
                if (!string.IsNullOrEmpty(formFieldSchema.FKInfo))
                {
                    //Raise an event:
                    XmlFormFKInfoEventArgs args =
                      new XmlFormFKInfoEventArgs(_ClassName, formFieldSchema);

                    OnFKBindingRequired(args);
                    //It must have been handled or we 
                    //would not have gotten this far...
                }//~FKINfo

                formFieldSchema.InitializeValue();
            }//~loop

        }

        #endregion

        #region Private Methods - Misc
        private int _GetOffset(Splitter splitter)
        {
            int offset = 0;
            Control control = splitter.Parent;
            while ((control != null) && (control != this))
            {
                offset += control.Left;
                control = control.Parent;
            }
            return offset;
        }

        #endregion


        #region Protected Methods - XsltTransformation
        /// <summary>
        /// TODOC:
        /// </summary>
        /// <param name="xmlDocIn"></param>
        /// <param name="xslDocIn"></param>
        /// <returns></returns>
        public static string Transform(XmlDocument xmlDocIn, XmlDocument xslDocIn)
        {
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            //PC:
            System.Xml.Xsl.XslCompiledTransform transformer
              = new System.Xml.Xsl.XslCompiledTransform();


            transformer.Load(xslDocIn);

            StringWriter stringWriter = new StringWriter();
            //System.Xml.XPath.XPathNavigator oXMLNav = xmlDocIn.CreateNavigator();
            transformer.Transform(xmlDocIn, null, stringWriter);
            return stringWriter.ToString();
#else
      MSXML2.DOMDocument xslDoc = new MSXML2.DOMDocumentClass();
      xslDoc.load(xslDocIn.OuterXml);

      if (optionalAttributes != null) {
        foreach (string entry in optionalAttributes.AllKeys) {
          MSXML2.IXMLDOMElement attributeNode = xslDoc.selectSingleNode(string.Format("//xsl:param[@name='{0}']", entry)) as MSXML2.IXMLDOMElement;

          if (attributeNode == null) {
            attributeNode = xslDoc.createElement("xsl:param");
            attributeNode.setAttribute("name", entry);
            //Insert as first child of xsl:stylesheet node:
            //Get first child that is element:
            MSXML2.IXMLDOMNode firstChild = xslDoc.documentElement.firstChild;
            while ((firstChild != null) && (!(firstChild is MSXML2.IXMLDOMElement))) {
              firstChild = firstChild.nextSibling;
            }
            if (firstChild != null) {
              xslDoc.documentElement.insertBefore(attributeNode, xslDoc.documentElement.firstChild);
            }
            else {
              xslDoc.documentElement.appendChild(attributeNode);
            }
          }
          if (attributeNode != null) {
            attributeNode.setAttribute("select", string.Format("'{0}'", optionalAttributes[entry]));
          }
        }//~keys
      }



      //Now deal with the Xsl part:
      MSXML2.DOMDocument xmlDoc = new MSXML2.DOMDocumentClass();
      xmlDoc.loadXML(xmlDocIn.OuterXml);

      return (string)xmlDoc.transformNode(xslDoc);
#endif
        }
        #endregion


    }//Class:End
}//Namespace:End
