﻿namespace XAct.UI.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.UI.Views;
    using XAct.UI.Views.Services.Implementations;

    public class ViewModeTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...


            //Force use of InMem rules
            IoCBootStrapper.Initialize(
                true,
                new XAct.Services.BindingDescriptor<IViewModeManagementService, InMemViewModeManagementService>()
                );



        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void Test00()
        {
            IViewModeService viewModeService =
                XAct.DependencyResolver.Current.GetInstance<IViewModeService>();

            Assert.IsNotNull(viewModeService.CurrentContext);

            Assert.IsNullOrEmpty(viewModeService.CurrentContext.Name);

        }

        [Test]
        public void CheckRulesCanBeRegistered()
        {
            IViewModeManagementService viewModeManagementService =
    XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.ReadOnly, null, null, null);
            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName2", ViewMode.Disabled, null, null, null);
            
        }

        [Test]
        public void CheckRulesCanBeReRegistered()
        {
            IViewModeManagementService viewModeManagementService =
    XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.ReadOnly, null, null, null);
            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.Disabled, null, null, null);

        }

        [Test]
        public void TestWhetherRulesCanBeCreatedAndRetrived()
        {
            IViewModeManagementService viewModeManagementService =
    XAct.DependencyResolver.Current.GetInstance<IViewModeManagementService>();

            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.ReadOnly, null, null, null);

            ViewMode viewMode;
            viewMode = viewModeManagementService.GetViewMode("Create", "ANamespace.AModel", "APropertyName", null);
            Assert.AreEqual(viewMode,ViewMode.ReadOnly);


            viewModeManagementService.RegisterRuleStateMask("Create", "ANamespace.AModel", "APropertyName", ViewMode.Hidden, null, null, null);

            viewMode = viewModeManagementService.GetViewMode("Create", "ANamespace.AModel", "APropertyName", null);
            Assert.AreEqual(viewMode, ViewMode.Hidden);

        }
    }
}
