using NUnit.Framework;

namespace XAct.UI.Tests
{
    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class SelectableItemTests
    {
        #region Setup/Teardown

        /// <summary>
        ///   Sets up to do before each and every 
        ///   test within this test fixture is run.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        ///   Tear down after each and every test.
        /// </summary>
        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test(Description = "")]
        public void SelectableItemTest()
        {
            SelectableItem item = new SelectableItem{Key="A",Value="B"};
            Assert.AreEqual("A",item.Key);
            Assert.AreEqual("B", item.Value);
            Assert.IsTrue(true);
        }
        [Test(Description = "")]
        public void SelectableItemListTest()
        {
            SelectableItem item = new SelectableItem { Key = "A", Value = "B" };
            
            SelectableItemList list = new SelectableItemList();
            list.Add(item);

            Assert.AreEqual("A", list[0].Key);
            Assert.AreEqual("B", list[0].Value);
        
            Assert.AreEqual(1,list.Count);

        }
    }


}


