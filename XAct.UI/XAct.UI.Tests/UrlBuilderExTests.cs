namespace XAct.UI.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.UI.Web.Utils;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class UrlBuilderExTests
    {
        #region Setup/Teardown

        /// <summary>
        ///   Sets up to do before each and every 
        ///   test within this test fixture is run.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        ///   Tear down after each and every test.
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            GC.Collect();
        }

        #endregion

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
        }


        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }
        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UriBuilderTest1()
        {
            UriBuilderEx uriBuilderEx = new UriBuilderEx("http", "someplace.com", 88, "somedir/somefile.aspx");
            uriBuilderEx.QueryString.Add("A", "B");
            uriBuilderEx.QueryString.Add("C", "D");

            string uri = uriBuilderEx.ToString();
            Assert.AreEqual("http://someplace.com:88/somedir/somefile.aspx?A=B&C=D", uri);

        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UriBuilderTest2()
        {
            UriBuilderEx uriBuilderEx = new UriBuilderEx("http","someplace.com",88,"somedir/somefile.aspx","#someextraAnchor");
            uriBuilderEx.QueryString.Add("A","B");
            uriBuilderEx.QueryString.Add("C","D");

            string uri = uriBuilderEx.ToString();

            //See: http://bit.ly/tc70bO
            //In other words, according to the RCF the URL is split by the fragment identifier first (#), if present, and *then* parsed as an address. It's therefore imperative that the fragment identifier is placed last in the URL, which may also be combined with a query string as in:

            Assert.AreEqual("http://someplace.com:88/somedir/somefile.aspx?A=B&C=D#someextraAnchor", uri);

        }
    }


}


