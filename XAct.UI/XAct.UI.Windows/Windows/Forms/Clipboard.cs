namespace XAct.UI.Windows.Forms
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    ///   Public Enum used by XLib.Basics.cClipBoard.ScreenGrab to define Type of ScreenGrab required.
    /// </summary>
    public enum ScreenGrabType
    {
        /// <summary>
        ///   Enum Member: Flag to indicate ScreenGrab of Full Screen.
        /// </summary>
        FullScreen,
        /// <summary>
        ///   Enum Member: Flag to indicate ScreenGrab of Active Form only.
        /// </summary>
        ActiveForm
    }


    /// <summary>
    ///   A static class of helpful static methods for handling the Windows clipboard.
    /// </summary>
    public static class ClipboardHandler
    {
        /// <summary>
        ///   Example of how to clear Clipboard.
        /// </summary>
        public static void Clear()
        {
            Clipboard.SetDataObject(new object());
        }


        /// <summary>
        ///   Grabs ScreenGrab and saves it to given FileName.
        /// </summary>
        /// <param name = "qType"></param>
        /// <param name = "qFileName">FileName to save image to.</param>
        /// <returns></returns>
        public static bool ScreenGrab(ScreenGrabType qType, string qFileName)
        {
            if (qFileName == String.Empty)
            {
                throw (new ArgumentNullException("qFileName", "No FileName provided for Clipboard image."));
            }

            #region Vars

            Bitmap clipBitmap;
            ImageFormat tFormat;

            #endregion

            #region Clear Clipboard first

            Clipboard.SetDataObject(new object());

            #endregion

            #region Get Clipboard object

            IDataObject iData = Clipboard.GetDataObject();

            #endregion

            #region Send Keyboard strokes needed to fill clipboard

            if (qType == ScreenGrabType.ActiveForm)
            {
                SendKeys.SendWait("{PRTSC}"); // PrintScreen saves only activeForm
            }
            else
            {
                SendKeys.SendWait("+{PRTSC}"); // Shift+PrintScreen saves all Desktop
            }

            #endregion

            if (iData.GetDataPresent(DataFormats.Bitmap))
            {
                clipBitmap = (Bitmap)
                             iData.GetData(DataFormats.Bitmap);

                string tExt = Path.GetExtension(qFileName.ToLower());

                if (tExt == "jpg")
                {
                    tFormat = ImageFormat.Jpeg;
                }
                else if (tExt == "bmp")
                {
                    tFormat = ImageFormat.Bmp;
                }
                else if (tExt == "gif")
                {
                    tFormat = ImageFormat.Gif;
                }
                else
                {
                    tFormat = ImageFormat.Jpeg;
                    qFileName = Path.ChangeExtension(qFileName, "jpg");
                }

                try
                {
                    clipBitmap.Save(qFileName, tFormat);
                }
                catch
                {
                    throw new Exception("Could not save image from clipboard.");
                }
            }
            else
            {
                throw new Exception("Could not retrieve image off the clipboard.");
            }
            return true;
        }

//Method:End
    }


}


