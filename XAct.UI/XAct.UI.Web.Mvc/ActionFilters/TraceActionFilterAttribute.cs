﻿namespace XAct.UI.Web.Mvc
{
    using System.Web.Mvc;
    using System.Web.Routing;
    using XAct.Diagnostics;

    /// <summary>
    /// An <see cref="ActionFilterAttribute"/>
    /// that can traces invocations to TraceActions
    /// to the underlying <see cref="ITracingService"/>
    /// <para>
    /// Hint: apply the LogAction to the base controller,
    /// and all Actions invoked will be logged to 
    /// <see cref="ITracingService"/>
    /// </para>
    /// </summary>
    public class TraceActionFilterAttribute : ActionFilterAttribute
    {
        private readonly ITracingService _tracingService;
        private readonly TraceLevel _traceLevel;
        /// <summary>
        /// Initializes a new instance of the <see cref="TraceActionFilterAttribute"/> class.
        /// </summary>
        public TraceActionFilterAttribute(TraceLevel traceLevel = TraceLevel.Verbose)
        {
            _tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();
            _traceLevel = traceLevel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceActionFilterAttribute"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="traceLevel">The trace level.</param>
        public TraceActionFilterAttribute(ITracingService tracingService, TraceLevel traceLevel = TraceLevel.Verbose)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            
            _tracingService = tracingService;
            _traceLevel = traceLevel;
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Trace(filterContext.RouteData, "OnActionExecuting");
            base.OnActionExecuting(filterContext);
        }




        /// <summary>
        /// Called by the ASP.NET MVC framework after the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Trace(filterContext.RouteData, "OnActionExecuting");
            base.OnActionExecuted(filterContext);
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action result executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Trace(filterContext.RouteData, "OnResultExecuting");
            base.OnResultExecuting(filterContext);
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework after the action result executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Trace(filterContext.RouteData, "OnResultExecuted");
            base.OnResultExecuted(filterContext);
        }


        private void Trace(RouteData routeData, string methodName)
        {
            object controllerName = routeData.Values["controller"];
            object actionName = routeData.Values["action"];

            string message = "{0} controller:{1} action:{2}".FormatStringCurrentCulture(methodName, controllerName,
                                                                                     actionName);

            _tracingService.Trace(_traceLevel, message);

        }
    }
}
