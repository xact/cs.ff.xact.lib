﻿namespace XAct.UI.Web.Mvc {
    using System;
    using System.Web.Mvc;

    /// <summary>
    /// An MVC ActionFilter that ensures the method is invoked
    /// over SSL. 
    /// </summary>
    /// <remarks>
    /// Usage:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// public ActionResult About(){
    ///   ViewData["Title"] = "About Page";
    ///   return View();
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public class HttpsRequiredAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            // do not use 'filterContext.RequestContext.HttpContext.Request.Url' because of Azure port forwarding
            // http://social.msdn.microsoft.com/Forums/en-US/windowsazure/thread/9142db8d-0f85-47a2-91f7-418bb5a0c675/
            string scheme = filterContext.RequestContext.HttpContext.Request.Url.Scheme;

            if (String.Equals(scheme, "https", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }
            filterContext.Result = new HttpStatusCodeResult(400, "HTTPS Required");

            //Not sure what this adds.
            base.OnActionExecuting(filterContext);
        }


    }
}