﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Routing;

#endif


    /// <summary>
    /// Extension Methods for the HtmlHelper Type.
    /// </summary>
    /// <internal>
    /// Src: Adam Barnes GSFA project.
    /// </internal>
// ReSharper disable CheckNamespace
    public static class HtmlHelperFileBoxExtensionMethods
// ReSharper restore CheckNamespace
    {
        /// <summary>
        ///   Returns a file input element by using the specified HTML helper and the name of the form field.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="html">The HTML.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static String FileBox<TModel, TValue>(this HtmlHelper<TModel> html,
                                                     Expression<Func<TModel, TValue>> expression)
        {
            String htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            return FileBox(html, htmlFieldName);
        }

        /// <summary>
        ///   Returns a file input element by using the specified HTML helper and the name of the form field.
        /// </summary>
        /// <param name = "htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name = "name">The name of the form field and the <see cref = "System.Web.Mvc.ViewDataDictionary"/> key that is used to look up the validation errors.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static string FileBox(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.FileBox(name, (object) null);
        }

        /// <summary>
        ///   Returns a file input element by using the specified HTML helper, the name of the form field, and the HTML attributes.
        /// </summary>
        /// <param name = "htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name = "name">The name of the form field and the <see cref = "System.Web.Mvc.ViewDataDictionary"/> key that is used to look up the validation errors.</param>
        /// <param name = "htmlAttributes">An object that contains the HTML attributes for the element. The attributes are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static string FileBox(this HtmlHelper htmlHelper, string name, object htmlAttributes)
        {
            return htmlHelper.FileBox(name, new RouteValueDictionary(htmlAttributes));
        }

        /// <summary>
        ///   Returns a file input element by using the specified HTML helper, the name of the form field, and the HTML attributes.
        /// </summary>
        /// <param name = "htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name = "name">The name of the form field and the <see cref = "System.Web.Mvc.ViewDataDictionary"/> key that is used to look up the validation errors.</param>
        /// <param name = "htmlAttributes">An object that contains the HTML attributes for the element. The attributes are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static string FileBox(this HtmlHelper htmlHelper, string name, IDictionary<String, Object> htmlAttributes)
        {
            htmlHelper.ValidateIsNotDefault("htmlHelper");

            TagBuilder tagBuilder = new TagBuilder("input");

            tagBuilder.MergeAttributes(htmlAttributes);
// ReSharper disable LocalizableElement
            tagBuilder.MergeAttribute("type", "file", true);
// ReSharper restore LocalizableElement
            tagBuilder.MergeAttribute("name", name, true);
            tagBuilder.GenerateId(name);

            ModelState modelState;
            
            if (htmlHelper.ViewData.ModelState.TryGetValue(name, out modelState))
            {
                if (modelState.Errors.Count > 0)
                {
                    tagBuilder.AddCssClass(HtmlHelper.ValidationInputCssClassName);
                }
            }

            return tagBuilder.ToString(TagRenderMode.SelfClosing);
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
