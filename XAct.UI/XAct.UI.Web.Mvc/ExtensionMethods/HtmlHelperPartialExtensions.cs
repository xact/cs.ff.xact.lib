﻿namespace XAct
{
    using System;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    /// <summary>
    ///  
    /// </summary>
    public static class HtmlHelperPartialExtensions
    {
        /// <summary>
        /// Allows invocation of Partials, with correct binding on Edit post back. 
        /// <para>
        /// Awesome, G.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="helper">The helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="partialViewName">Partial name of the view.</param>
        /// <returns></returns>
        public static MvcHtmlString PartialFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string partialViewName)
        {
            string name = ExpressionHelper.GetExpressionText(expression);
            object model = ModelMetadata.FromLambdaExpression(expression, helper.ViewData).Model;

            string prefix = helper.ViewData.TemplateInfo.HtmlFieldPrefix;
            if (!string.IsNullOrEmpty(prefix))
            {
                prefix += ".";
            }
            ViewDataDictionary viewData = new ViewDataDictionary(helper.ViewData)
            {
                TemplateInfo = new TemplateInfo
                {
                    HtmlFieldPrefix = prefix + name
                }
            };

            return helper.Partial(partialViewName, model, viewData);

        }
    }
}
