﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.UI.Views;

    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// Gets the tracing service.
        /// </summary>
        private static ITracingService TracingService
        {
            get
            {
                return _tracingService ??
                       (_tracingService = DependencyResolver.Current.GetInstance<ITracingService>());
            }
        }

        private static ITracingService _tracingService;

        // ReSharper disable UnusedMember.Local
        private static IEnvironmentService EnvironmentService
        // ReSharper restore UnusedMember.Local
        {
            get
            {
                return _environmentService ??
                       (_environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>());
            }
        }

        private static IEnvironmentService _environmentService;

        private static IScaffoldingConfiguration ScaffoldingDefaults
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<IScaffoldingConfigurationManager>().Current;
            }
        }


        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|


        /// <summary>
        /// Gets the state of the editor.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="htmlHelper">The htmlHelper.</param>
        /// <returns></returns>
        public static ViewMode GetViewMode<TModel>(this HtmlHelper<TModel> htmlHelper)
        {
            object tmp;
            ViewMode viewMode = ViewMode.Undefined;
            if (htmlHelper.ViewData.ModelMetadata.AdditionalValues.TryGetValue("ViewMode", out tmp))
            {
                viewMode = (ViewMode)tmp;
            }
            return viewMode;
        }

        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        /// <summary>
        /// Adds the label and validation to display editor.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The htmlHelper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="displayEditorMvcHtmlString">The editor MVC HTML string.</param>
        /// <param name="wrapAllElements">if set to <c>true</c> [wrap all elements].</param>
        /// <returns></returns>
        public static MvcHtmlString AddLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                                Expression<Func<TModel, TProperty>> expression,
                                                                MvcHtmlString displayEditorMvcHtmlString,
                                                                bool wrapAllElements = true)
        {
            return htmlHelper.AddLabel(expression, displayEditorMvcHtmlString, () => (wrapAllElements));
        }
        // ---------|---------|---------|---------|---------|
        /// <summary>
        /// Prepends display editor with label.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The htmlHelper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="displayEditorMvcHtmlString">The editor MVC HTML string.</param>
        /// <param name="wrapAllElementsDelegate">The wrap all elements.</param>
        /// <returns></returns>
        public static MvcHtmlString AddLabel<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                                Expression<Func<TModel, TProperty>> expression,
                                                                MvcHtmlString displayEditorMvcHtmlString,
                                                                Func<bool> wrapAllElementsDelegate)
        {

            Func<ViewMode> editorStateDelegate = null;
            InitializeDelegates(ref editorStateDelegate, ref wrapAllElementsDelegate);

            string labelHtml;

            //BUILD LABEL, either as Label, or Wrapped Label (maybe a div containing a Label):
            if (string.IsNullOrEmpty(ScaffoldingDefaults.FormRowLabelContainerTag))
            {
                //We are not wrapping The Row with a div,
                //so just want label:
                labelHtml = htmlHelper.LabelFor(expression).ToString();
            }
            else
            {
                // ReSharper disable SuggestUseVarKeywordEvident
                TagBuilder labelContainerBuilder = new TagBuilder(ScaffoldingDefaults.FormRowLabelContainerTag);
                // ReSharper restore SuggestUseVarKeywordEvident
                if (!string.IsNullOrEmpty(ScaffoldingDefaults.FormRowLabelContainerClassName))
                {
                    labelContainerBuilder.AddCssClass(ScaffoldingDefaults.FormRowLabelContainerClassName);
                }
                labelContainerBuilder.InnerHtml = htmlHelper.LabelFor(expression).ToString();
                labelHtml = labelContainerBuilder.ToString();
            }


            //Build DisplayEditor, either as displayEditor(only) or wrapping it in a div or something:
            string displayEditorHtml;
            if (string.IsNullOrEmpty(ScaffoldingDefaults.FormRowEditorContainerTag))
            {
                displayEditorHtml = displayEditorMvcHtmlString.ToString();
            }
            else
            {
                // ReSharper disable SuggestUseVarKeywordEvident
                TagBuilder displayEditorContainerBuilder = new TagBuilder(ScaffoldingDefaults.FormRowEditorContainerTag);
                // ReSharper restore SuggestUseVarKeywordEvident
                if (!string.IsNullOrEmpty(ScaffoldingDefaults.FormRowEditorContainerClassName))
                {
                    displayEditorContainerBuilder.AddCssClass(ScaffoldingDefaults.FormRowEditorContainerClassName);
                }
                displayEditorContainerBuilder.InnerHtml = displayEditorMvcHtmlString.ToString();
                displayEditorHtml = displayEditorContainerBuilder.ToString();
            }


            //Build Row from 3 elements, either as is, or wrapped in something:
            string rowHtml;


            rowHtml =
            string.Format(
                CultureInfo.InvariantCulture,
                "{1}{0}{2}",
                System.Environment.NewLine,
                labelHtml,
                displayEditorHtml
                );


            if ((!wrapAllElementsDelegate.Invoke()) || (string.IsNullOrEmpty(ScaffoldingDefaults.FormRowContainerTag)))
            {
            }
            else
            {
                // ReSharper disable SuggestUseVarKeywordEvident
                TagBuilder tagBuilder = new TagBuilder(ScaffoldingDefaults.FormRowContainerTag);
                // ReSharper restore SuggestUseVarKeywordEvident
                if (!string.IsNullOrEmpty(ScaffoldingDefaults.FormRowContainerClassName))
                {
                    tagBuilder.AddCssClass(ScaffoldingDefaults.FormRowContainerClassName);
                }

                tagBuilder.InnerHtml = rowHtml;

                rowHtml = tagBuilder.ToString();
            }

            return new MvcHtmlString(rowHtml);
        }

        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|

        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <param name="wrapAllElements">if set to <c>true</c> [wrap all elements].</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true,
            bool wrapAllElements = true
            )
        {
            //Convert the fixed ViewMode and bools into delegates that will always return their value, 
            //and pass them to the next overload:
            return htmlHelper.LabelledDisplayEditorFor(expression, () => (viewMode), () => (includeValidator), () => (wrapAllElements));
        }


        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <param name="wrapAllElementsDelegate">The wrap all elements delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate,
            Func<bool> wrapAllElementsDelegate
            )
        {
            //A C# limitation is that you can't initialize Func arguments to anything other than null, so:
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate, ref wrapAllElementsDelegate);

            //Figure out the Editor first:
            MvcHtmlString editorMvcHtmlString = htmlHelper.DisplayEditorFor(expression, editorStateDelegate, includeValidatorDelegate);

            //In order to endup by prefixing with label, and wrapping whole thing in a div:
            return htmlHelper.AddLabel(expression, editorMvcHtmlString, wrapAllElementsDelegate);

        }

        // ---------|---------|---------|---------|---------|

        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <param name="wrapAllElements">if set to <c>true</c> [wrap all elements].</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object additionalViewData,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true,
            bool wrapAllElements = true
            )
        {
            return htmlHelper.LabelledDisplayEditorFor(
                expression, 
                additionalViewData, 
                () => (viewMode), 
                () => (includeValidator), 
                () => (wrapAllElements));
        }



        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <param name="wrapAllElementsDelegate">The wrap all elements delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object additionalViewData,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate,
            Func<bool> wrapAllElementsDelegate
            )
        {
            //A C# limitation is that you can't initialize Func arguments to anything other than null, so:
            InitializeDelegates(ref editorStateDelegate, ref wrapAllElementsDelegate);

            MvcHtmlString editorMvcHtmlString = 
                htmlHelper.DisplayEditorFor(
                expression, 
                additionalViewData, 
                editorStateDelegate, 
                includeValidatorDelegate);

            return htmlHelper.AddLabel(expression, editorMvcHtmlString, wrapAllElementsDelegate);
        }

        // ---------|---------|---------|---------|---------|


        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <param name="wrapAllElements">if set to <c>true</c> [wrap all elements].</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true,
            bool wrapAllElements = true
            )
        {
            return htmlHelper.LabelledDisplayEditorFor(
                expression, 
                templateName, 
                () => (viewMode), 
                () => (includeValidator), 
                () => (wrapAllElements));
        }

        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <param name="wrapAllElementsDelegate">The wrap all elements delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate,
            Func<bool> wrapAllElementsDelegate
            )
        {
            //A C# limitation is that you can't initialize Func arguments to anything other than null, so:
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate, ref wrapAllElementsDelegate);

            MvcHtmlString editorMvcHtmlString = htmlHelper.DisplayEditorFor(expression, templateName, editorStateDelegate, includeValidatorDelegate);

            return htmlHelper.AddLabel(expression, editorMvcHtmlString, wrapAllElementsDelegate);

        }

        // ---------|---------|---------|---------|---------|

        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeLabel">if set to <c>true</c> [include label].</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <param name="wrapAllElements">if set to <c>true</c> [wrap all elements].</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            object additionalViewData,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeLabel = true,
            bool includeValidator = true,
            bool wrapAllElements = true
            )
        {
            return htmlHelper.LabelledDisplayEditorFor(
                expression, 
                templateName, 
                additionalViewData, 
                () => (viewMode), 
                () => (includeValidator), 
                () => (wrapAllElements)
                );
        }

        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <param name="wrapAllElementsDelegate">The wrap all elements delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            object additionalViewData,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate,
            Func<bool> wrapAllElementsDelegate
            )
        {
            //A C# limitation is that you can't initialize Func arguments to anything other than null, so:
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate, ref wrapAllElementsDelegate);

            MvcHtmlString editorMvcHtmlString =
                htmlHelper.DisplayEditorFor(
                    expression,
                    templateName,
                    additionalViewData,
                    editorStateDelegate,
                    includeValidatorDelegate);

            return htmlHelper.AddLabel(
                expression, 
                editorMvcHtmlString, 
                wrapAllElementsDelegate);

        }

        // ---------|---------|---------|---------|---------|
        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="htmlFieldName">Name of the HTML field.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <param name="wrapAllElements">if set to <c>true</c> [wrap all elements].</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            string htmlFieldName,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true,
            bool wrapAllElements = true
            )
        {
            return htmlHelper.LabelledDisplayEditorFor(
                expression, 
                templateName, 
                htmlFieldName, 
                () => (viewMode), 
                () => (includeValidator), 
                () => (wrapAllElements));
        }


        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="htmlFieldName">Name of the HTML field.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <param name="wrapAllElementsDelegate">The wrap all elements delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            string htmlFieldName,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate,
            Func<bool> wrapAllElementsDelegate
            )
        {
            //A C# limitation is that you can't initialize Func arguments to anything other than null, so:
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate, ref wrapAllElementsDelegate);

            MvcHtmlString editorMvcHtmlString = htmlHelper.DisplayEditorFor(expression, htmlFieldName, editorStateDelegate, includeValidatorDelegate);

            return htmlHelper.AddLabel(expression, editorMvcHtmlString, wrapAllElementsDelegate);
        }

        // ---------|---------|---------|---------|---------|
        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="htmlFieldName">Name of the HTML field.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeLabel">if set to <c>true</c> [include label].</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <param name="wrapAllElements">if set to <c>true</c> [wrap all elements].</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            string htmlFieldName,
            object additionalViewData,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeLabel = true,
            bool includeValidator = true,
            bool wrapAllElements = true
            )
        {
            return htmlHelper.LabelledDisplayEditorFor(expression, templateName, htmlFieldName, additionalViewData,
                                                   () => (viewMode), 
                                                   () => (includeLabel),
                                                   () => (includeValidator), 
                                                   () => (wrapAllElements));
        }

        /// <summary>
        /// Creates a Label and DisplayEditorFor Row.
        /// <para>
        /// By default, Label and DisplayEditorFor (with or without Validator) 
        /// are all wrapped up in a div for the row.
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="htmlFieldName">Name of the HTML field.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeLabelDelegate">The include label delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <param name="wrapAllElementsDelegate">The wrap all elements delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString LabelledDisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            string htmlFieldName,
            object additionalViewData,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeLabelDelegate,
            Func<bool> includeValidatorDelegate,
            Func<bool> wrapAllElementsDelegate

            )
        {
            //A C# limitation is that you can't initialize Func arguments to anything other than null, so:
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate, ref wrapAllElementsDelegate);

            MvcHtmlString editorMvcHtmlString =
                htmlHelper.DisplayEditorFor(
                    expression,
                    templateName,
                    htmlFieldName,
                    additionalViewData,
                    editorStateDelegate, 
                    includeValidatorDelegate);

            return htmlHelper.AddLabel(expression, editorMvcHtmlString, wrapAllElementsDelegate);


        }
        // ---------|---------|---------|---------|---------|



        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|

        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true
            )
        {
            return htmlHelper.DisplayEditorFor(expression, () => (viewMode), () => (includeValidator));
        }

        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate = null
            )
        {


            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate);

            //new { }
            return DisplayEditorFor(htmlHelper, expression, null , editorStateDelegate, includeValidatorDelegate);
        }

        // ---------|---------|---------|---------|---------|

        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object additionalViewData,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true
            )
        {
            return htmlHelper.DisplayEditorFor(expression, additionalViewData, () => (viewMode), () => (includeValidator));
        }



        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object additionalViewData,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate = null
            )
        {
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate);

            return DisplayEditorFor(htmlHelper, expression, null, additionalViewData, editorStateDelegate, includeValidatorDelegate);
        }

        // ---------|---------|---------|---------|---------|

        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true
            )
        {
            return htmlHelper.DisplayEditorFor(expression, templateName, () => (viewMode), () => (includeValidator));
        }

        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate = null
            )
        {
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate);

            //new { }
            return DisplayEditorFor(htmlHelper, expression, templateName, null, editorStateDelegate, includeValidatorDelegate);

        }

        // ---------|---------|---------|---------|---------|



        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            object additionalViewData,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true
            )
        {
            return htmlHelper.DisplayEditorFor(expression, templateName, additionalViewData, () => (viewMode), () => (includeValidator));
        }



        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            object additionalViewData,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate = null
            )
        {
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate);

            return DisplayEditorFor(htmlHelper, expression, templateName, null, additionalViewData, editorStateDelegate, includeValidatorDelegate);
        }

        // ---------|---------|---------|---------|---------|


        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="htmlFieldName">Name of the HTML field.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            string htmlFieldName,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true
            )
        {
            return htmlHelper.DisplayEditorFor(expression, templateName, htmlFieldName, () => (viewMode), () => (includeValidator));
        }



        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="htmlFieldName">Name of the HTML field.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            string htmlFieldName,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate = null
            )
        {
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate);

            return DisplayEditorFor(htmlHelper, expression, templateName, htmlFieldName, null, editorStateDelegate, includeValidatorDelegate);
        }

        // ---------|---------|---------|---------|---------|




        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="includeValidator">if set to <c>true</c> [include validator].</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            string fieldName,
            object additionalViewData,
            ViewMode viewMode = ViewMode.Undefined,
            bool includeValidator = true
            )
        {
            return htmlHelper.DisplayEditorFor(expression, templateName, fieldName, additionalViewData, () => (viewMode), () => (includeValidator));
        }

        /// <summary>
        /// Renders either a DisplayFor or EditorFor depending on given ViewMode.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="htmlFieldName">Name of the HTML field.</param>
        /// <param name="additionalViewData">The additional view data.</param>
        /// <param name="editorStateDelegate">The editor state delegate.</param>
        /// <param name="includeValidatorDelegate">The include validator delegate.</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            string htmlFieldName,
            object additionalViewData,
            Func<ViewMode> editorStateDelegate,
            Func<bool> includeValidatorDelegate = null
            )
        {

            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate);

            //if (includeValidatorDelegate == null)
            //{
            //    includeValidatorDelegate = () => (true);
            //}


            //var hello = new RouteValueDictionary(additionalViewData);


            ModelMetadata modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            //if (editorStateDelegate == null)
            //{
            //    editorStateDelegate = () => (ViewMode.Undefined);
            //}




            //Get the property name from the Metadata for the Model:
            string propertyName = modelMetadata.PropertyName;

            //-------------------------------
            //The trickiest type of control are the dropdowns...
            //have to look in dropback for vars that relate to the curren var name:
            SelectListItem[] lookupData = null;
            SelectListItem lookupItem = null;
            string lookupOptionLabel = null; //the text from the ViewBag to put at the top of the Select options.
            string lookupText = null; //The text of the SelectItem it matched
            string lookupUnSelectedText = null;

            if (!propertyName.IsNullOrEmpty())
            {
                GetLookupDataIfPresent<TModel>(htmlHelper, templateName, htmlFieldName, modelMetadata, propertyName,
                                               ref lookupData, ref lookupItem, ref lookupOptionLabel, ref lookupText,
                                               ref lookupUnSelectedText);
            }


            //-------------------------------
            //Use helper:
            ViewMode viewMode = GetViewMode(htmlHelper, propertyName,  editorStateDelegate, modelMetadata);
            //-------------------------------
            //Build Editor and (optionally) a Validator:
            string editorAndValidatorHtml = BuildEditorAndValidatorPart<TModel, TProperty>(htmlHelper, expression, templateName, htmlFieldName, ref additionalViewData, includeValidatorDelegate, propertyName, lookupData, lookupOptionLabel, lookupText, viewMode);

            //--------------------
            //Wrap result in a row:

            if (!string.IsNullOrEmpty(ScaffoldingDefaults.FormRowEditorAndValidatorContainerTag))
            {
                // ReSharper disable SuggestUseVarKeywordEvident
                TagBuilder x =
                    new TagBuilder(ScaffoldingDefaults.FormRowEditorAndValidatorContainerTag);

                // ReSharper restore SuggestUseVarKeywordEvident
                if (!string.IsNullOrEmpty(ScaffoldingDefaults.FormRowEditorAndValidatorContainerClassName))
                {
                    x.AddCssClass(ScaffoldingDefaults.FormRowEditorAndValidatorContainerClassName);
                }
                x.InnerHtml = System.Environment.NewLine + editorAndValidatorHtml;

                editorAndValidatorHtml = x.ToString();
            }

            //--------------------

            return new MvcHtmlString(editorAndValidatorHtml);
        }


        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|

        private static void InitializeDelegates(ref Func<ViewMode> editorStateDelegate,
                                        ref Func<bool> includeValidatorDelegate,
                                        ref Func<bool> wrapAllElementsDelegate)
        {
            if (wrapAllElementsDelegate == null)
            {
                wrapAllElementsDelegate = () => (true);
            }
            InitializeDelegates(ref editorStateDelegate, ref includeValidatorDelegate);
        }

        // ---------|---------|---------|---------|---------|
        private static void InitializeDelegates(ref Func<ViewMode> editorStateDelegate,
                                                ref Func<bool> includeValidatorDelegate)
        {
            if (editorStateDelegate == null)
            {
                editorStateDelegate = () => (ViewMode.Undefined);
            }


            if (includeValidatorDelegate == null)
            {
                includeValidatorDelegate = () => (ScaffoldingDefaults.IncludeValidatorByDefault);
            }
        }
        // ---------|---------|---------|---------|---------|
        private static void GetLookupDataIfPresent<TModel>(HtmlHelper<TModel> htmlHelper, string templateName, string htmlFieldName, ModelMetadata modelMetadata, string propertyName, ref SelectListItem[] lookupData, ref SelectListItem lookupItem, ref string lookupOptionLabel, ref string lookupText, ref string lookupUnSelectedText)
        {
            TracingService.Trace(TraceLevel.Verbose,
                                 "DisplayEditorFor ProperyName:'{0}', TemplateName:'{1}', htmlFieldName:'{2}'",
                                 propertyName, templateName, htmlFieldName);

            //"0123456789"
            //10
            //8



            string last2Chars = propertyName.Substring(propertyName.Length - 2, 2);
            if (new[] { "ID", "Id", "Fk", "FK" }.Contains(last2Chars))
            {
                TracingService.Trace(TraceLevel.Verbose,
                                     "DisplayEditorFor ProperyName:'{0}' ends with '{1}' so looking for related ViewData List...",
                                     propertyName, last2Chars);
                //Get the 'short' base name:
                string referenceDataPropertytName = propertyName.Substring(0, propertyName.Length - 2);

                foreach (
                    string s in
                        new[]
                                {
                                    referenceDataPropertytName, referenceDataPropertytName + "Items",
                                    referenceDataPropertytName + "List", referenceDataPropertytName + "Options",
                                    referenceDataPropertytName + "s"
                                })
                {
                    IEnumerable<SelectListItem> tmpItems = htmlHelper.ViewData[s] as IEnumerable<SelectListItem>;


                    if (tmpItems != null)
                    {
                        //Cool List is found...

                        lookupData = tmpItems.ToArray();

                        TracingService.Trace(TraceLevel.Verbose,
                                             "DisplayEditorFor ProperyName:'{0}' ends with '{1}'. Found '{2}' in ViewBag...",
                                             propertyName, last2Chars, s);

                        //We're done for Edit at this point (have Value + Options to feed to Dropdwon)
                        //but for Display/Readonly, etc.. have to render actual Text, not Value.
                        //So have to search through Options:

                        //Get modal value for the FK:
                        object modelValue = modelMetadata.Model;
                        string modelValueAsString = modelValue.ToString();

                        if (modelValue == null)
                        {
                            //If no value, won't be able to find anything in lookupData
                            //therefore skip finding
                            //lookupItem
                            //lookupText
                        }
                        else
                        {
                            TracingService.Trace(TraceLevel.Verbose,
                                                 "DisplayEditorFor ProperyName:'{0}'. Now looking in Lookup list for Item with Value matching Value of '{1}'...",
                                                 propertyName,
                                                 modelValueAsString);

                            foreach (SelectListItem selectListItem in lookupData)
                            {

                                TracingService.Trace(TraceLevel.Verbose,
                                                     "...DisplayEditorFor ProperyName:'{0}': Comparing Model Value:'{1}' against SelectListItem.Value:'{2}' (Text:'{3}')...",
                                                     propertyName,
                                                     modelValueAsString,
                                                     selectListItem.Value,
                                                     selectListItem.Text);

                                if (selectListItem.Value == modelValueAsString)
                                {
                                    //If the item is not selected, the question is...is there any other item that is selected?
                                    if (!selectListItem.Selected)
                                    {
                                        SelectListItem[] alreadyCount = lookupData.Where(lu => lu.Selected).ToArray();

                                        if (alreadyCount.Length > 0)
                                        {
                                            TracingService.Trace(TraceLevel.Warning,
                                                                 "...DisplayEditorFor ProperyName:'{0}': Setting value to {1} ({2}), but know that an item is already selected: {3}",
                                                                 propertyName,
                                                                 modelValueAsString,
                                                                 selectListItem.Text,
                                                                 alreadyCount.Select(li => li.Text).JoinSafely(","));

                                            if (alreadyCount.Length > 1)
                                            //If more than one, it's a multiSelect
                                            {
                                                TracingService.Trace(TraceLevel.Warning,
                                                                     "...DisplayEditorFor ProperyName:'{0}': Setting SelectListItem.Selected=true, even though there is another one already set by someone else...worth knowing about...(may be MultiSelect)",
                                                                     propertyName);
                                            }
                                        }
                                        selectListItem.Selected = true;
                                    }
                                    //Save ref to item:
                                    lookupItem = selectListItem;
                                    //and text that we would show:
                                    lookupText = lookupItem.Text;

                                    TracingService.Trace(TraceLevel.Verbose,
                                                         "DisplayEditorFor ProperyName:'{0}'. Found it. Text:'{1}'",
                                                         propertyName, lookupText);
                                    break;
                                }
                            }

                        }


                        //A couple of other points of order:

                        string key;
                        object t;

                        key = referenceDataPropertytName + "OptionLabel";
                        if (htmlHelper.ViewData.TryGetValue(key, out t))
                        {
                            lookupOptionLabel = t as string;
                        }


                        key = referenceDataPropertytName + "Unselected";
                        if (htmlHelper.ViewData.TryGetValue(key, out t))
                        {
                            lookupUnSelectedText = t as string;
                        }

                        if (lookupItem == null)
                        {
                            lookupText = lookupUnSelectedText ?? ScaffoldingDefaults.UnselectedOptionText;
                        }


                        //If we found a FK set of items, we're done.
                        //Move on from looking for set of options, to rendering
                        break;



                    }
                }
            }
        }


        // ---------|---------|---------|---------|---------|
        private static ViewMode GetViewMode<TModel>(HtmlHelper<TModel> htmlHelper, string propertyName, Func<ViewMode> editorStateDelegate, ModelMetadata modelMetadata)
        {
            editorStateDelegate.Invoke();

            //Get the given GivenState:
            ViewMode viewMode = editorStateDelegate.Invoke();

            if (viewMode == ViewMode.Rule)
            {
                TracingService.Trace(TraceLevel.Verbose,
                                     "DisplayEditorFor ProperyName:'{0}'. ViewMode: '{1}'. Looking in metadata for Rule Value previously attached by ModelMetadataProvider...",
                                     propertyName, viewMode);

                //If Rule, get it from the Metadata put there 
                //earlier by the ViewModeModelMetadataProvider:

                object o;
                if (modelMetadata.AdditionalValues.TryGetValue("ViewMode", out o))
                {
                    viewMode = o.ConvertTo<ViewMode>();

                    TracingService.Trace(TraceLevel.Verbose,
                                         "DisplayEditorFor ProperyName:'{0}'. ViewMode in Rule Db found. ViewMode updated to '{1}'.",
                                         propertyName,
                                         viewMode);
                }
                else
                {
                    //Pretty sure it comes back as undefined if not found, 
                    //but just in case...
                    viewMode = ViewMode.Undefined;
                }
            }
            //If the default, go see if you can find it on page:
            if (viewMode == ViewMode.Undefined)
            {
                TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. ViewMode: '{1}'.  Looking in ViewData...", propertyName, viewMode);

                //As we got nothing from the db/rule engine,
                //and nothing is defined declaratively,
                //default to any on the page.

                //What was the page one?
                //Get the Page one from ViewBag:
                object o;
                if (htmlHelper.ViewData.TryGetValue("ViewMode", out o))
                {
                    viewMode = o.ConvertTo<ViewMode>();

                    TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. ViewMode in Page found. ViewMode updated to '{1}'.", propertyName, viewMode);
                }
                //else
                //{
                //    //Leave alone, as further down, Undefined = Display (not hidden).
                //}
            }

            return viewMode;

        }
        // ---------|---------|---------|---------|---------|
        private static string BuildEditorAndValidatorPart<TModel, TProperty>(HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string templateName, string htmlFieldName, ref object additionalViewData, Func<bool> includeValidatorDelegate, string propertyName, SelectListItem[] lookupData, string lookupOptionLabel, string lookupText, ViewMode viewMode)
        {

            //--------------------
            //--------------------
            //--------------------

            //Build just the Editor only (ie, DisplayFor or EditorFor, depending on role):
            string wrappedEditorHtml = BuildEditorPart<TModel, TProperty>(htmlHelper, expression, templateName, htmlFieldName, ref additionalViewData, propertyName, lookupData, lookupOptionLabel, lookupText, viewMode);

            //--------------------
            //--------------------
            //--------------------


            //Build Validator, either as validator(only) or wrapping it in a div or something:
            string editorAndValidatorHtml;

            string validatorHtml;

            if (!includeValidatorDelegate.Invoke())
            {

                //As we are not including the Validator, 
                //the result is the same as just the editor:
                validatorHtml = string.Empty;

                editorAndValidatorHtml = wrappedEditorHtml;
            }
            else
            {
                if (string.IsNullOrEmpty(ScaffoldingDefaults.FormRowValidatorContainerTag))
                {
                    //FIX: Sometimes comes back null.
                    //"If no validation error occurs or client validation is disabled, this method returns null."
                    //But when failing even when ClientSideValidation is on...wht?
                    //Aha! And when the Validator is not within the scope of a form!
                    MvcHtmlString x = htmlHelper.ValidationMessageFor(expression, ScaffoldingDefaults.ValidationMessage);
                    validatorHtml = x == null ? string.Empty : x.ToString();
                }
                else
                {
                    // ReSharper disable SuggestUseVarKeywordEvident
                    TagBuilder validatorContainerBuilder =
                        new TagBuilder(ScaffoldingDefaults.FormRowValidatorContainerTag);
                    // ReSharper restore SuggestUseVarKeywordEvident
                    if (!string.IsNullOrEmpty(ScaffoldingDefaults.FormRowValidatorContainerClassName))
                    {
                        validatorContainerBuilder.AddCssClass(ScaffoldingDefaults.FormRowValidatorContainerClassName);
                    }
                    //"If no validation error occurs or client validation is disabled, this method returns null."
                    MvcHtmlString x = htmlHelper.ValidationMessageFor(expression, ScaffoldingDefaults.ValidationMessage);
                    validatorContainerBuilder.InnerHtml = x == null ? string.Empty : x.ToString();
                    validatorHtml = validatorContainerBuilder.ToString();
                }

                //Build Row from 3 elements, either as is, or wrapped in something:
                editorAndValidatorHtml =
                        string.Format(
                            CultureInfo.InvariantCulture,
                            "{1}{0}{2}",
                            System.Environment.NewLine,
                            wrappedEditorHtml,
                            validatorHtml
                            );


            }

            //--------------------
            //--------------------
            //--------------------

            return editorAndValidatorHtml;
        }

        private static string BuildEditorPart<TModel, TProperty>(HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string templateName, string htmlFieldName, ref object additionalViewData, string propertyName, SelectListItem[] lookupData, string lookupOptionLabel, string lookupText, ViewMode viewMode)
        {
            bool stayTraditional = true;

            if (stayTraditional)
            {
                return BuildEditorPartTraditional(htmlHelper, expression, templateName, htmlFieldName,
                                                                     ref additionalViewData, propertyName, lookupData,
                                                                     lookupOptionLabel, lookupText, viewMode);
            }

            return BuildEditorPartUsingKnockout(htmlHelper, expression, templateName, htmlFieldName,
                                                                 ref additionalViewData, propertyName, lookupData,
                                                                 lookupOptionLabel, lookupText, viewMode);
        }

        private static string BuildEditorPartTraditional<TModel, TProperty>(HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string templateName, string htmlFieldName, ref object additionalViewData, string propertyName, SelectListItem[] lookupData, string lookupOptionLabel, string lookupText, ViewMode viewMode)
        {
            MvcHtmlString editorMvcHtmlString;

            switch (viewMode)
            {
                case ViewMode.Hidden:
                    editorMvcHtmlString = htmlHelper.HiddenFor(expression, additionalViewData);

                    TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. Rendering as '{1}'...", propertyName, viewMode);

                    break;
                case ViewMode.ReadOnly:

                    //Readonly is span + roundtripped hidden EditorFor
                    if ((lookupData != null) && (lookupText != null))
                    {
                        //Eg: Expression = m => m.SexFK 
                        //Needs to be Switched to Eg: Expression = m => m.LU1
                        //htmlHelper.Display("X");
                        //But that doesn't display unless there is a matching metadata property
                        //which I don't know how to add yet.
                        //editorMvcHtmlString = htmlHelper.DisplayFor(expression, templateName, htmlFieldName, additionalViewData);
                        editorMvcHtmlString =
                            new MvcHtmlString(
                            // ReSharper disable RedundantToStringCall
                            htmlHelper.Element("span", null, lookupText, additionalViewData).ToString() + System.Environment.NewLine +
                            // ReSharper restore RedundantToStringCall
                            // ReSharper disable RedundantToStringCall
                            htmlHelper.HiddenFor(expression, additionalViewData).ToString()
                            // ReSharper restore RedundantToStringCall
                            );

                        TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. Rendering as '{1}' (as *hack* SPAN of LookupText)...", propertyName, viewMode);
                    }
                    else
                    {
                        editorMvcHtmlString = new MvcHtmlString(
                            // ReSharper disable RedundantToStringCall
                            htmlHelper.DisplayFor(expression, templateName, htmlFieldName, additionalViewData).ToString()
                            // ReSharper restore RedundantToStringCall
                            + System.Environment.NewLine +
                            // ReSharper disable RedundantToStringCall
                            htmlHelper.HiddenFor(expression, additionalViewData).ToString()
                            // ReSharper restore RedundantToStringCall
                        );

                        TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. Rendering as '{1}' (as DISPLAYFOR+HIDDENFOR)...", propertyName, viewMode);
                    }
                    break;
                case ViewMode.Disabled:
                case ViewMode.Editable:

                    if (viewMode == ViewMode.Disabled)
                    {
                        TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. Rendering as '{1}'. Setting disabled='true'", propertyName, viewMode);

                        additionalViewData = new RouteValueDictionary(additionalViewData);
                        ((RouteValueDictionary)additionalViewData).Add("disabled", "true");
                    }

                    if (lookupData != null)
                    {
                        editorMvcHtmlString = htmlHelper.DropDownListFor(expression, lookupData, lookupOptionLabel, additionalViewData);
                        TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. Rendering as '{1}'. (as DROPDOWN)", propertyName, viewMode);
                    }
                    else
                    {
                        editorMvcHtmlString = htmlHelper.EditorFor(expression, templateName, htmlFieldName, additionalViewData);
                        TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. Rendering as '{1}'. (as EDITOR)", propertyName, viewMode);
                    }
                    break;
                // ReSharper disable RedundantCaseLabel
                case ViewMode.Rule:
                // ReSharper restore RedundantCaseLabel
                //Nothing specific defined, so 
                //Same as Display
                // ReSharper disable RedundantCaseLabel
                case ViewMode.Display:
                // ReSharper restore RedundantCaseLabel
                default:
                    if ((lookupData != null) && (lookupText != null))
                    {
                        //Eg: Expression = m => m.SexFK 
                        //Needs to be Switched to Eg: Expression = m => m.LU1
                        //htmlHelper.Display("X");
                        //But that doesn't display unless there is a matching metadata property
                        //which I don't know how to add yet.
                        //editorMvcHtmlString = htmlHelper.DisplayFor(expression, templateName, htmlFieldName, additionalViewData);
                        editorMvcHtmlString = htmlHelper.Element("span", null, lookupText, additionalViewData);

                        TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. Rendering as '{1}' (as *hack* SPAN of lookupText)", propertyName, viewMode);
                    }
                    else
                    {
                        editorMvcHtmlString = htmlHelper.DisplayFor(expression, templateName, htmlFieldName, additionalViewData);

                        TracingService.Trace(TraceLevel.Verbose, "DisplayEditorFor ProperyName:'{0}'. Rendering as '{1}' (as DISPLAYFOR)", propertyName, viewMode);

                    }

                    break;
            }

            //Build DisplayEditor, either as displayEditor(only) or wrapping it in a div or something:
            string wrappedEditorHtml;

            if (string.IsNullOrEmpty(ScaffoldingDefaults.FormRowEditorContainerTag))
            {
                //Convert to string:
                wrappedEditorHtml = editorMvcHtmlString.ToString();
            }
            else
            {
                // ReSharper disable SuggestUseVarKeywordEvident
                TagBuilder displayEditorContainerBuilder = new TagBuilder(ScaffoldingDefaults.FormRowEditorContainerTag);

                // ReSharper restore SuggestUseVarKeywordEvident
                if (!string.IsNullOrEmpty(ScaffoldingDefaults.FormRowEditorContainerClassName))
                {
                    displayEditorContainerBuilder.AddCssClass(ScaffoldingDefaults.FormRowEditorContainerClassName);
                }

                displayEditorContainerBuilder.InnerHtml = editorMvcHtmlString.ToString();

                //Convert Builder to string:
                wrappedEditorHtml = displayEditorContainerBuilder.ToString();
            }
            return wrappedEditorHtml;
        }
        // ---------|---------|---------|---------|---------|
        private static string BuildEditorPartUsingKnockout<TModel, TProperty>(HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string templateName, string htmlFieldName, ref object additionalViewData, string propertyName, SelectListItem[] lookupData, string lookupOptionLabel, string lookupText, ViewMode viewMode)
        {
            MvcHtmlString editorMvcHtmlString = null;
#pragma warning disable 219
            bool readonlyFlag = false;
            bool disabled = false;
#pragma warning restore 219


            switch (viewMode)
            {
                case ViewMode.Hidden:
                    editorMvcHtmlString = htmlHelper.HiddenFor(expression, additionalViewData);
                    break;
                case ViewMode.ReadOnly:
                    readonlyFlag = true;
                    break;
                case ViewMode.Disabled:
                    readonlyFlag = true;
                    disabled = true;
                    break;
                case ViewMode.Editable:
                    disabled = false;
                    break;
                case ViewMode.Rule:
                    // ReSharper restore RedundantCaseLabel
                    //Nothing specific defined, so 
                    //Same as Display
                    // ReSharper disable RedundantCaseLabel
                case ViewMode.Display:
                    // ReSharper restore RedundantCaseLabel
                default:
                    break;
            }

            return editorMvcHtmlString.ToString();
        }
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|
        // ---------|---------|---------|---------|---------|







    }

}

