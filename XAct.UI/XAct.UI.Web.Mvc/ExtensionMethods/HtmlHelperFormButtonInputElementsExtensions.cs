﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System.Web.Mvc;
    using System.Web.Routing;

#endif

    /// <summary>
    ///   Extension Methods for the HtmlHelper Type.
    /// </summary>
// ReSharper disable CheckNamespace
    public static class HtmlHelperFormElementExtensionMethods
// ReSharper restore CheckNamespace
    {
        /// <summary>
        /// Buttons the specified HTML helper.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="id">The id.</param>
        /// <param name="value">The value.</param>
        /// <param name="htmlAttributes">The HTML attributes (name, etc).</param>
        /// <returns></returns>
        public static MvcHtmlString Button (this HtmlHelper htmlHelper, string id, string value,
                                                 object htmlAttributes)
        {
            TagBuilder tagBuilder = new TagBuilder("button");

            if (!string.IsNullOrEmpty(id))
            {
                tagBuilder.GenerateId(id);
            }
            if (string.IsNullOrEmpty(value))
            {
                value = "Submit";
            }
            tagBuilder.SetInnerText(value);

            if (htmlAttributes != null)
            {
                RouteValueDictionary typedDict = new RouteValueDictionary(htmlAttributes);
                tagBuilder.MergeAttributes(typedDict);
            }
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }

        /// <summary>
        ///   Creates a Submit Button.
        /// </summary>
        /// <param name = "htmlHelper">The HTML helper.</param>
        /// <param name = "id">The id.</param>
        /// <param name = "value">The value.</param>
        /// <param name = "htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString SubmitButton(this HtmlHelper htmlHelper, string id=null, string value=null,
                                                 object htmlAttributes=null)
        {
            TagBuilder tagBuilder = new TagBuilder("input");

            if (!string.IsNullOrEmpty(id))
            {
                tagBuilder.GenerateId(id);
            }
            if (string.IsNullOrEmpty(value))
            {
                value = "Submit";
            }
            tagBuilder.Attributes["value"] = value;
            tagBuilder.Attributes["type"] = "SUBMIT";

            if (htmlAttributes != null)
            {
                RouteValueDictionary typedDict = new RouteValueDictionary(htmlAttributes);
                tagBuilder.MergeAttributes(typedDict);
            }

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
