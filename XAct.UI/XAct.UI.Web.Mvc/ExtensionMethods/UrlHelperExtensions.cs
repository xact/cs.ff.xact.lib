﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System.Web.Mvc;

#endif

    /// <summary>
    ///   Extension methods to the UrlHelper Type.
    /// </summary>
    public static class UrlHelperExtensionMethods
    {
        private const string C_BASE_CONTENT = "~/Content"; //note: no final slash

        /// <summary>
        ///   Returns the Root url of the website.
        /// </summary>
        /// <param name = "urlHelper">The URL helper.</param>
        /// <returns></returns>
        public static string Root(this UrlHelper urlHelper)
        {
            return urlHelper.Content("~/");
        }

        /// <summary>
        ///   Basics the content.
        /// </summary>
        /// <param name = "urlHelper">The URL helper.</param>
        /// <param name = "fileName">Name of the file.</param>
        /// <returns></returns>
        public static string BasicContent(this UrlHelper urlHelper, string fileName)
        {
            return urlHelper.Content(string.Format("{0}/{1}", C_BASE_CONTENT, fileName));
        }

        /// <summary>
        ///   Returns the Url to the Image in the Image directory of the application.
        /// </summary>
        /// <param name = "urlHelper">The URL helper.</param>
        /// <param name = "fileName">Name of the file.</param>
        /// <returns></returns>
        public static string SharedContent(this UrlHelper urlHelper, string fileName)
        {
            return urlHelper.Content(string.Format("{0}/Shared/{1}", C_BASE_CONTENT, fileName));
        }

        /// <summary>
        ///   Returns the Url to the Image in the Image directory of the application.
        ///   <para>
        ///     ~/{BaseContent}/Shared/Images/XXX.png
        ///   </para>
        /// </summary>
        /// <param name = "urlHelper">The URL helper.</param>
        /// <param name = "fileName">Name of the file.</param>
        /// <returns>A string.</returns>
        public static string Image(this UrlHelper urlHelper, string fileName)
        {
            return urlHelper.Content(string.Format("{0}/Shared/Images/{1}", C_BASE_CONTENT, fileName));
        }

        /// <summary>
        ///   Returns the Url to the StyleSheet in the Css directory of the application.
        ///   <para>
        ///     ~/{BaseContent}/XXX.css
        ///   </para>
        /// </summary>
        /// <param name = "urlHelper">The URL helper.</param>
        /// <param name = "fileName">Name of the file.</param>
        /// <returns></returns>
        public static string StyleSheet(this UrlHelper urlHelper, string fileName)
        {
            return urlHelper.Content(string.Format("{0}/{1}", C_BASE_CONTENT, fileName));
        }

        /// <summary>
        ///   Returns the Url to the Script in the Script directory of the application.
        ///   <para>
        ///     ~/{Scripts}/XXX.js
        ///   </para>
        /// </summary>
        /// <param name = "urlHelper">The URL helper.</param>
        /// <param name = "fileName">Name of the file.</param>
        /// <returns></returns>
        public static string Script(this UrlHelper urlHelper, string fileName)
        {
            return urlHelper.Content(string.Format("~/Scripts/{0}", fileName));
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
