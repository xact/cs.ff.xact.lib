﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System.Globalization;
    using System.Web;
    using System.Web.Mvc;

#endif


public static class ResourceExtensionMethods
    {
        public static string Resource(this HtmlHelper htmlhelper, string expression, params object[] args)
        {
            string virtualPath = GetVirtualPath(htmlhelper);

            var result = htmlhelper.Resource(expression, virtualPath, args);
            return result;
        }

        public static string Resource(this HtmlHelper htmlhelper, string virtualPath, string expression, params object[] args)
        {
            var result = GetResourceString(htmlhelper.ViewContext.HttpContext, expression, virtualPath, args);
            return result;
        }

        public static string Resource(this Controller controller, string expression, params object[] args)
        {
            var result = GetResourceString(controller.HttpContext, expression, "~/", args);
            return result;
        }

        static string GetResourceString(HttpContextBase httpContext, string expression, string virtualPath, object[] args)
        {
            //ExpressionBuilderContext context = new ExpressionBuilderContext(virtualPath);
            //ResourceExpressionBuilder builder = new ResourceExpressionBuilder();
            //ResourceExpressionFields fields = (ResourceExpressionFields)builder.ParseExpression(expression, typeof(string), context);
            //if (!string.IsNullOrEmpty(fields.ClassKey))
            //{
            //    return
            //        string.Format(
            //            (string)
            //            httpContext.GetGlobalResourceObject(fields.ClassKey, fields.ResourceKey,
            //                                                CultureInfo.CurrentUICulture), args);
            //}
            var result = string.Format(CultureInfo.CurrentUICulture,
                (string)httpContext.GetGlobalResourceObject(virtualPath, expression, CultureInfo.CurrentUICulture), args);
            //return string.Format((string)httpContext.GetLocalResourceObject(virtualPath, fields.ResourceKey, CultureInfo.CurrentUICulture), args);

            return result;
        }

        private static string GetVirtualPath(HtmlHelper htmlhelper)
        {
            WebFormView view = htmlhelper.ViewContext.View as WebFormView;

            if (view != null)
                return view.ViewPath;

            return null;
        }
    }


#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
