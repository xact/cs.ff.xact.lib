﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System.Web.Mvc;
    using System.Web.Routing;
    using XAct.Environment;

#endif

    /// <summary>
    ///   Extension Methods the HtmlHelper Type.
    /// </summary>
// ReSharper disable CheckNamespace
    public static class HtmlHelperExtensionMethods
// ReSharper restore CheckNamespace
    {
        static IDateTimeService DateTimeService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IDateTimeService>(); }
        }

        static IEnvironmentService EnvironmentService
        {
            get { return DependencyResolver.Current.GetInstance<IEnvironmentService>(); }
        }

        /// <summary>
        ///   Creates any element, with the given (optional) id, (optional) innerText, and (optional) attributes.
        /// </summary>
        /// <param name = "htmlHelper">The HTML helper.</param>
        /// <param name = "elementName">Name of the element.</param>
        /// <param name = "id">The id.</param>
        /// <param name = "innerText">The inner text.</param>
        /// <param name = "htmlAttributes">The HTML attributes.</param>
        /// <returns></returns>
        public static MvcHtmlString Element(this HtmlHelper htmlHelper, string elementName, string id, string innerText,
                                            object htmlAttributes)
        {
//Don't do it this way:
//return type: string or MvcHtmlString, depending on situation
//return string.Format(“<{0}/>”, elemName);}



            TagBuilder tagBuilder = new TagBuilder(elementName);

            if (!string.IsNullOrEmpty(id))
            {
                tagBuilder.GenerateId(id);
            }
            if (!string.IsNullOrEmpty(innerText))
            {
                tagBuilder.SetInnerText(innerText);
            }
            if (htmlAttributes != null)
            {
                RouteValueDictionary typedDict = new RouteValueDictionary(htmlAttributes);
                tagBuilder.MergeAttributes(typedDict);
            }
            string str = tagBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(str);
        }


        /// <summary>
        ///   Creates a SCRIPT include statement.
        /// </summary>
        /// <param name = "htmlHelper">The HTML helper.</param>
        /// <param name = "url">The URL.</param>
        /// <returns></returns>
        public static MvcHtmlString ScriptLink(this HtmlHelper htmlHelper, string url)
        {
            TagBuilder tagBuilder = new TagBuilder("script");
            tagBuilder.Attributes["src"] = url + "?VERSION=" +  DateTimeService.NowUTC.Ticks;
            tagBuilder.Attributes["type"] = "text/javascript";
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        /// <summary>
        ///   Creates a CSS LINK statement.
        /// </summary>
        /// <param name = "htmlHelper">The HTML helper.</param>
        /// <param name = "url">The URL.</param>
        /// <returns></returns>
        public static MvcHtmlString CssLink(this HtmlHelper htmlHelper, string url)
        {
            TagBuilder tagBuilder = new TagBuilder("link");

            tagBuilder.Attributes["href"] = url + "?VERSION=" + DateTimeService.NowUTC.Ticks;

            tagBuilder.Attributes["rel"] = "stylesheet";
            tagBuilder.Attributes["type"] = "text/css";

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
