﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace XAct
{
    public static class HtmlHelperEnumExtensionsMethods
    {


        /// <summary>
        /// Makes a DropDownList for enum values.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <para>
        /// Although not absolutely required, can decorate the Enum
        /// with 
        /// <code>
        /// <![CDATA[
        /// [TypeConverter(typeof(XAct.ComponentModel.PascalCaseWordSplittingEnumConverter))]
        /// public enum Color {
        ///   Red,
        ///   Green,
        ///   ...
        ///   BrightGreen,
        ///   BrightBlue
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// <internal>
        /// Src:http://blogs.msdn.com/b/stuartleeks/archive/2010/05/21/asp-net-mvc-creating-a-dropdownlist-helper-for-enums.aspx
        /// </internal>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEnum">The type of the enum.</typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            
            Type enumType = GetNonNullableModelType(metadata);


            //Get values 
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            //The enum type can have a non-default converter to change
            //enum values to strings:
            TypeConverter converter = TypeDescriptor.GetConverter(enumType);


            IEnumerable<SelectListItem> items =
                from value in values
                select new SelectListItem
                {
                    Text = converter.ConvertToString(value),
                    Value = value.ToString(),
                    Selected = value.Equals(metadata.Model)
                };

            if (metadata.IsNullableValueType)
            {
                items = SingleEmptyItem.Concat(items);
            }

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }

        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }

        private static readonly SelectListItem[] SingleEmptyItem = new[] { new SelectListItem { Text = "", Value = "" } };

    }
}
