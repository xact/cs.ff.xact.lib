﻿namespace XAct
{
    using System.Linq;
    using System.Web.Mvc;

    /// <summary>
    /// Extensions to the <see cref="MvcHtmlString"/> class
    /// </summary>
    public static class HtmlHelperMvcHtmlStringExtensions
    {
        /// <summary>
        /// Concats the given <see cref="MvcHtmlString"/>
        /// into one (without double encoding).
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="strings">The strings.</param>
        /// <returns></returns>
        public static MvcHtmlString Concat(this MvcHtmlString first, params MvcHtmlString[] strings)
        {
            return MvcHtmlString.Create(first.ToString() + string.Concat(strings.Select(s => s.ToString()).ToArray()));
        }
    }
}

