﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
#endif


static class Class1a
    {
        public static string Resource(this HtmlHelper htmlhelper, string expression, params object[] args)
        {
            string virtualPath = GetVirtualPath(htmlhelper);

            return htmlhelper.Resource(expression, virtualPath, args);
        }

        public static string Resource(this HtmlHelper htmlhelper, string virtualPath, string expression, params object[] args)
        {
            return GetResourceString(htmlhelper.ViewContext.HttpContext, expression, virtualPath, args);
        }

        public static string Resource(this Controller controller, string expression, params object[] args)
        {
            return GetResourceString(controller.HttpContext, expression, "~/", args);
        }

        static string GetResourceString(HttpContextBase httpContext, string expression, string virtualPath, object[] args)
        {
            //ExpressionBuilderContext context = new ExpressionBuilderContext(virtualPath);

            //ResourceExpressionBuilder builder = new ResourceExpressionBuilder();

            //ResourceExpressionFields fields = (ResourceExpressionFields)builder.ParseExpression(expression, typeof(string), context);

            //if (!string.IsNullOrEmpty(fields.ClassKey))
            //{
            //    return
            //        string.Format(
            //            (string)
            //            httpContext.GetGlobalResourceObject(fields.ClassKey, fields.ResourceKey,
            //                                                CultureInfo.CurrentUICulture), args);
            //}
            return string.Format(CultureInfo.CurrentUICulture,
                (string)httpContext.GetGlobalResourceObject(virtualPath, expression, CultureInfo.CurrentUICulture), args);

            //return string.Format((string)httpContext.GetLocalResourceObject(virtualPath, fields.ResourceKey, CultureInfo.CurrentUICulture), args);
        }

        private static string GetVirtualPath(HtmlHelper htmlhelper)
        {
            WebFormView view = htmlhelper.ViewContext.View as WebFormView;

            if (view != null)
                return view.ViewPath;

            return null;
        }
    }


#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
