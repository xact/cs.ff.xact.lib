﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System.Web.Mvc;

#endif

    /// <summary>
    /// </summary>
    public static class GoogleChartsHtmlHelperExtensionMethods
    {
        /// <summary>
        ///   Draws the chart.
        /// </summary>
        /// <param name = "helper">The helper.</param>
        /// <param name = "chartType">Type of the chart.</param>
        /// <param name = "chartData">The chart data.</param>
        /// <param name = "chartSize">Size of the chart.</param>
        /// <param name = "chartLabel">The chart label.</param>
        /// <returns></returns>
        public static string DrawChart(this HtmlHelper helper, string chartSize, string chartType, string chartLabel,
                                       string chartData)
        {
            string url = string.Format(
                "http://chart.apis.google.com/chart?chs={0}&amp;cht={1}&amp;chl={2}&amp;chd=t:{3}",
                chartSize,
                chartType,
                chartLabel,
                chartData);

            return string.Format("<img src=\"{0}\"/>", url);
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
