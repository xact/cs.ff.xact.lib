﻿namespace XAct
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using XAct.UI;

    public static class SelectListItemExtensions
    {

        /// <summary>
        /// Maps the XActLib generic 
        /// <see cref="ISelectableItem"/>
        /// to an MVC <see cref="SelectListItem"/>
        /// element.
        /// </summary>
        /// <param name="selectableItem">The selectable item.</param>
        /// <returns></returns>
        public static SelectListItem MapTo(this ISelectableItem selectableItem)
        {
            return new SelectListItem
                {
                    Selected = selectableItem.Selected, 
                    Text = selectableItem.Value.ToString(), 
                    Value = selectableItem.Key
                };
        }

        /// <summary>
        /// Maps the XActLib generic 
        /// <see cref="SelectableItemList"/>
        /// to an MVC <see cref="SelectList"/>
        /// </summary>
        /// <param name="selectableItemList">The selectable item list.</param>
        /// <returns></returns>
        public static SelectListItem[] MapTo(this SelectableItemList selectableItemList)
        {

            List<SelectListItem> tmp = new List<SelectListItem>();

            foreach (SelectableItem item in selectableItemList)
            {
                tmp.Add(item.MapTo());
            }
            return tmp.ToArray();
            //return new SelectList(tmp);
        }

        /// <summary>
        /// Maps an Mvc <see cref="SelectListItem"/>
        /// to a generic XAct <see cref="SelectableItem"/>
        /// </summary>
        /// <param name="selectListItem">The select list item.</param>
        /// <returns></returns>
        public static ISelectableItem MapTo(this SelectListItem selectListItem)
        {
            return new SelectableItem
                       {Key = selectListItem.Value, 
                           Value = selectListItem.Text, 
                           Selected = selectListItem.Selected};
        }

        /// <summary>
        /// Maps an Mvc <see cref="SelectList"/>
        /// to a generic XAct <see cref="SelectableItemList"/>
        /// </summary>
        /// <param name="selectList">The select list.</param>
        /// <returns></returns>
        public static SelectableItemList MapTo(this SelectList selectList)
        {
            SelectableItemList result = new SelectableItemList();

            foreach(SelectListItem selectListItem in selectList)
            {
                result.Add(selectListItem.MapTo());    
            }
            return result;
        }
    }
}
