﻿namespace XAct.UI.Web.Mvc
{
    using System.IO;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    /// <summary>
    /// A ModelBinder that can accept JSON as a postback.
    /// <para>
    /// Register it using "Model.Binders.DefaultBinder"
    /// <example>
    /// <code>
    /// <![CDATA[
    /// ModelBinders.Binders.DefaultBinder = new JsonModelBinder();
    /// ]]>
    /// </code>
    /// <para>
    /// As per http://haacked.com/archive/2010/04/15/sending-json-to-an-asp-net-mvc-action-method-argument.aspx
    /// Concept is superceded.
    /// </para>
    /// </example>
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Example:
    /// <example>
    /// <code>
    /// <![CDATA[
    /// $.ajax({
    ///     cache: false,
    ///     url: url,
    ///     type: "POST",
    ///     dataType: "json",
    ///     contentType: "application/json", //instead of default JQuery use of 'x-www-form-urlencoded'
    ///     //Notice that we are using JSON2.js as well as sending all args as one JSON object.
    ///     data: JSON.stringify({ searchTerms: searchTerms, pageIndex: 1, pageSize: 25 }),
    ///     success: function (data) { me.renderItems(data); },
    ///     error: function (jqXHR, textStatus, errorThrown) { me.commError(jqXHR, textStatus, errorThrown); }
    /// });
    /// ]]>
    /// </code>
    /// </example>
    /// </para>
    /// </remarks>
    /// <internal>
    /// Src: http://telldontask.wordpress.com/2010/05/26/posting-an-array-of-complex-types-using-jquery-json-to-asp-net-mvc/
    /// Src: http://lozanotek.com/blog/archive/2010/04/16/simple_json_model_binder.aspx
    /// </internal>
    public class JsonModelBinder : DefaultModelBinder
    {
        /// <summary>
        /// Binds the model by using the specified controller context and binding context.
        /// </summary>
        /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param>
        /// <param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param>
        /// <returns>The bound object.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="bindingContext "/>parameter is null.</exception>
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            //The request contentType has to be 'application/json' to
            //be relevant.
            if (!IsJSONRequest(controllerContext))
            {
                return base.BindModel(controllerContext, bindingContext);
            }

            // Get the JSON data that's been posted
            HttpRequestBase request = controllerContext.HttpContext.Request;
            //Fix: rewind the request
            request.InputStream.Position = 0;

            string jsonStringData = new StreamReader(request.InputStream).ReadToEnd();

            // Use the built-in serializer to do the work for us
            // see:http://lozanotek.com/blog/archive/2010/04/16/simple_json_model_binder.aspx 
            //for why this requires .NET40 (then again MVC3 does as well).
            return new JavaScriptSerializer()
                .Deserialize(jsonStringData, bindingContext.ModelMetadata.ModelType);
        }

// ReSharper disable InconsistentNaming
        private static bool IsJSONRequest(ControllerContext controllerContext)
// ReSharper restore InconsistentNaming
        {
            string contentType = controllerContext.HttpContext.Request.ContentType;
            return contentType.Contains("application/json");
        }
    }
}