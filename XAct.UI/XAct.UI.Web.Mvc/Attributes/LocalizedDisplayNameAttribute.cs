﻿namespace XAct.UI.Web
{
    using System;
    using System.ComponentModel;
    using System.Reflection;
    using System.Resources;

    /// <summary>
    /// Get's a localizeable specialization of the 
    /// <see cref="DisplayNameAttribute"/> that was used in Mvc2.
    /// <para>
    /// Note that the <see cref="DisplayNameAttribute"/> is superceded in Mvc3 with the 
    /// <c>DisplayAttribute</c> (note missing 'Name') only available in .NET 4.0
    /// </para>
    /// <para>
    /// Note that if both are applied, <c>DisplayNameAttribute</c> will be used.
    /// </para>
    /// </summary>
    /// <internal>
    /// Src: http://bit.ly/vxLt0W
    /// </internal>
    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {

        /// <summary>
        /// Gets or sets the key of the resource
        /// within <see cref="ResourceManager"/>,
        /// for the localized value of <see cref="DisplayName"/>.
        /// </summary>
        /// <value>
        /// The display name resource key.
        /// </value>
        public string DisplayNameResourceKey { get; set; }

        /// <summary>
        /// Gets or sets the resource manager to use when
        /// searching for a localized value.
        /// </summary>
        /// <value>
        /// The resource manager.
        /// </value>
        public ResourceManager ResourceManager { get; set; }

        /// <summary>
        /// Gets the display name for a property, event, or public void method 
        /// that takes no arguments stored in this attribute.
        /// </summary>
        /// <returns>
        /// The display name.
        ///   </returns>
        public override string DisplayName
        {
            get
            {
                var result = this.ResourceManager.GetString(this.DisplayNameResourceKey);
                return result;
            }
        }


        static string LookupResource(Type resourceManagerProvider, string resourceKey)
        {

            PropertyInfo propertyInfo = 
                resourceManagerProvider.GetProperty(
                resourceKey, 
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            if (propertyInfo == null)
            {
                //fallback:
                return resourceKey;
            }

            //Get the static property:
            ResourceManager resourceManager =
                propertyInfo.GetValue(null, null) as ResourceManager;

            if (resourceManager == null)
            {
                //fallback:
                return resourceKey;
            }


            //Get the resource:
            return resourceManager.GetString(resourceKey);

            //foreach (PropertyInfo staticProperty in 
            //    resourceManagerProvider.GetProperties(BindingFlags.Static | BindingFlags.NonPublic))
            //{
            //    if (staticProperty.PropertyType == typeof(System.Resources.ResourceManager))
            //    {
            //        System.Resources.ResourceManager resourceManager = 
            //            (System.Resources.ResourceManager)staticProperty.GetValue(null, null);
                
            //        return resourceManager.GetString(resourceKey);
            //    }
            //}

            //return resourceKey; // Fallback with the key name
        }
    }
}
