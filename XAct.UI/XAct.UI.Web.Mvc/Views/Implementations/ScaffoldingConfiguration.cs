﻿namespace XAct.UI.Views
{
    using System;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IScaffoldingConfiguration"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IScaffoldingConfiguration), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class ScaffoldingConfiguration : IScaffoldingConfiguration,ICloneable
    {
        /// <summary>
        /// Gets the form row container tag.
        /// <para>
        /// Default: "div";
        /// </para>
        /// </summary>
        public string FormRowContainerTag { get; set; }
        /// <summary>
        /// Gets the form row container className.
        /// <para>
        /// Default: "display-row";
        /// </para>
        /// </summary>
        public string FormRowContainerClassName { get; set; }

        /// <summary>
        /// Gets the form row label container tag.
        /// <para>
        /// Default: "";
        /// </para>
        /// </summary>
        public string FormRowLabelContainerTag { get; set; }

        /// <summary>
        /// Gets the form row label container className.
        /// <para>
        /// Default: "display-label-container";
        /// </para>
        /// </summary>
        public string FormRowLabelContainerClassName { get; set; }


        /// <summary>
        /// Gets the form row display/editor container tag.
        /// <para>
        /// Default: "div";
        /// </para>
        /// </summary>
        public string FormRowEditorAndValidatorContainerTag { get; set; }
        /// <summary>
        /// Gets the form row display/editor container className.
        /// <para>
        /// Default: "display-field-and-validator-container";
        /// </para>
        /// </summary>
        public string FormRowEditorAndValidatorContainerClassName { get; set; }
        



        /// <summary>
        /// Gets the form row display/editor container tag.
        /// <para>
        /// Default: "";
        /// </para>
        /// </summary>
        public string FormRowEditorContainerTag { get; set; }
        /// <summary>
        /// Gets the form row display/editor container className.
        /// <para>
        /// Default: "display-field-container";
        /// </para>
        /// </summary>
        public string FormRowEditorContainerClassName { get; set; }


        /// <summary>
        /// Gets the form row display/editor container tag.
        /// <para>
        /// Default: "";
        /// </para>
        /// </summary>
        public string FormRowValidatorContainerTag { get; set; }
        /// <summary>
        /// Gets the form row display/editor container className.
        /// <para>
        /// Default: "display-validator-container";
        /// </para>
        /// </summary>
        public string FormRowValidatorContainerClassName { get; set; }


        /// <summary>
        /// Gets the form row editor container readonly className.
        /// <para>
        /// Default: "display-editor-container-readonly";
        /// </para>
        /// </summary>
        public string FormRowEditorReadOnlyClassName { get; set; }



        /// <summary>
        /// Gets the form row display container readonly className.
        /// <para>
        /// Default: "display-editor-container-readonly";
        /// </para>
        /// </summary>
        public string FormRowDisplayReadOnlyClassName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to include Validator when creating LabelledDisplayEditor.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include validator by default]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeValidatorByDefault { get; set; }


        /// <summary>
        /// Gets the validation message.
        /// <para>
        /// Default is "*"
        /// </para>
        /// </summary>
        public string ValidationMessage { get; set; }
        /// <summary>
        /// Gets or sets the round trip prefix.
        /// </summary>
        /// <value>
        /// The round trip prefix.
        /// </value>
        public string RoundTripPrefix { get; set; }

        /// <summary>
        /// Gets or sets the round trip Suffix.
        /// </summary>
        /// <value>
        /// The round trip prefix.
        /// </value>
        public string RoundTripSuffix { get; set; }

        public string UnselectedOptionText { get; set; }


        public ScaffoldingConfiguration()
        {
            FormRowContainerTag = "div";
            FormRowLabelContainerTag = string.Empty;
            FormRowEditorContainerTag = string.Empty;
            FormRowValidatorContainerTag = string.Empty;
            FormRowEditorAndValidatorContainerTag = "div";

            FormRowContainerClassName = "displayeditor-row";
            FormRowLabelContainerClassName = "displayeditor-label-container";
            FormRowEditorContainerClassName = "displayeditor-field-container";
            FormRowValidatorContainerClassName = "displayeditor-validator-container";
            FormRowEditorAndValidatorContainerClassName = "displayeditor-field-and-validator-container";

            FormRowDisplayReadOnlyClassName = "displayeditor-editor-container-readonly";
            FormRowEditorReadOnlyClassName = "displayeditor-editor-container-readonly";

            IncludeValidatorByDefault = true;

            ValidationMessage = "*";

            RoundTripPrefix = ":|";

            RoundTripSuffix = "|:";
            //text-box single-line
            //checkbox

            UnselectedOptionText = string.Empty;
        }

        public object Clone()
        {
            return new ScaffoldingConfiguration
                       {
                           FormRowContainerClassName = FormRowContainerClassName,
                           FormRowContainerTag = FormRowContainerTag,
                           FormRowDisplayReadOnlyClassName = FormRowDisplayReadOnlyClassName,
                           FormRowEditorAndValidatorContainerClassName = FormRowEditorAndValidatorContainerClassName,
                           FormRowEditorAndValidatorContainerTag = FormRowEditorAndValidatorContainerTag,
                           FormRowEditorContainerClassName = FormRowEditorContainerClassName,
                           FormRowEditorContainerTag = FormRowEditorContainerTag,
                           FormRowEditorReadOnlyClassName = FormRowEditorReadOnlyClassName,
                           FormRowLabelContainerClassName = FormRowLabelContainerClassName,
                           FormRowLabelContainerTag = FormRowLabelContainerTag,
                           FormRowValidatorContainerClassName = FormRowValidatorContainerClassName,
                           FormRowValidatorContainerTag = FormRowValidatorContainerTag,
                           IncludeValidatorByDefault = IncludeValidatorByDefault,
                           RoundTripPrefix = RoundTripPrefix,
                           RoundTripSuffix = RoundTripSuffix,
                           UnselectedOptionText = UnselectedOptionText,
                           ValidationMessage = ValidationMessage
                       };
        }
    }
}
