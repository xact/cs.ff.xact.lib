﻿namespace XAct.UI.Views
{
    using XAct.Diagnostics;
    using XAct.Services;
    using XAct.State;

    /// <summary>
    /// Implementation of <see cref="IScaffoldingConfigurationManager "/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IScaffoldingConfigurationManager), BindingLifetimeType.Undefined, Priority.Low)]
    public class ScaffoldingConfigurationManager : WebThreadSpecificStackableContextManagementServiceBase<IScaffoldingConfiguration>, IScaffoldingConfigurationManager 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScaffoldingConfigurationManager"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="stateService">The state service.</param>
        public ScaffoldingConfigurationManager(ITracingService tracingService, IContextStateService stateService) : base(tracingService, stateService)
        {

        }


        ///// <summary>
        ///// Invoked by <see cref="ScaffoldingConfigurationManager.Create"/> to creates the source instance
        ///// before <see cref="ScaffoldingConfigurationManager.Create"/> calls <see cref="CloneSourceInstanceValuesToNewThreadInstance"/>
        ///// if <see cref="ScaffoldingConfigurationManager.IsMutable"/>=<c>true</c>.
        ///// </summary>
        ///// <internal>
        ///// Implementors can use <see cref="ScaffoldingConfigurationManager.CreateNewSourceInstanceHelper"/> to 
        ///// use the ServiceLocator to create the instance.
        ///// </internal>
        ///// <returns></returns>
        //protected override IScaffoldingConfiguration CreateNewSourceInstance()
        //{
        //    return CreateNewSourceInstanceHelper();
        //}


        ///// <summary>
        ///// Clones the source instance values to the new thread specific instance.
        ///// </summary>
        ///// <internal>
        ///// <para>
        ///// Implementators can use <see cref="WebThreadSpecificStackableContextManagementServiceBase{TContext}.CloneSourceInstanceValuesToNewThreadInstanceHelper"/> if they don't have a 
        ///// more custom solution.
        ///// </para>
        ///// </internal>
        ///// <param name="srcContext">The SRC context.</param>
        ///// <returns></returns>
        //protected override IScaffoldingConfiguration CloneSourceInstanceValuesToNewThreadInstance(IScaffoldingConfiguration srcContext)
        //{
        //    return CloneSourceInstanceValuesToNewThreadInstanceHelper(srcContext);
        //}
    }
}
