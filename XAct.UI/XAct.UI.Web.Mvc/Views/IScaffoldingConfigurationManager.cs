namespace XAct.UI.Views
{
    /// <summary>
    /// Contract for a service to return a thread specific instance of <see cref="ScaffoldingConfiguration"/>
    /// </summary>
    public interface IScaffoldingConfigurationManager : IWebThreadSpecificStackableContextManagementServiceBase<IScaffoldingConfiguration>
    {
        
    }
}