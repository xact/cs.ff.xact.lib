﻿namespace XAct.UI.Views
{
    using XAct.Configuration;

    /// <summary>
    /// Contract for the settings needed to manage automatic scaffolding.
    /// <para>
    /// Referenced by DisplayEditorFor and other HtmlHelper methods.
    /// </para>
    /// </summary>
    [ConfigurationSettings]
    public interface IScaffoldingConfiguration
    {
        /// <summary>
        /// Gets the form row container tag.
        /// <para>
        /// Default: "div";
        /// </para>
        /// </summary>
        string FormRowContainerTag { get; set; }
        /// <summary>
        /// Gets the form row container className.
        /// <para>
        /// Default: "display-row-container";
        /// </para>
        /// </summary>
        string FormRowContainerClassName { get; set; }

        
        
        /// <summary>
        /// Gets the form row label container tag.
        /// <para>
        /// Default: "";
        /// </para>
        /// </summary>
        string FormRowLabelContainerTag { get; set; }
        /// <summary>
        /// Gets the form row label container className.
        /// <para>
        /// Default: "display-label-container";
        /// </para>
        /// </summary>
        string FormRowLabelContainerClassName { get; set; }


        /// <summary>
        /// Gets the form row display/editor container tag.
        /// <para>
        /// Default: "";
        /// </para>
        /// </summary>
        string FormRowEditorAndValidatorContainerTag { get; set; }
        /// <summary>
        /// Gets the form row display/editor container className.
        /// <para>
        /// Default: "display-field-and-validator-container";
        /// </para>
        /// </summary>
        string FormRowEditorAndValidatorContainerClassName { get; set; }
        
        /// <summary>
        /// Gets the form row display/editor container tag.
        /// <para>
        /// Default: "";
        /// </para>
        /// </summary>
        string FormRowEditorContainerTag { get; set; }
        /// <summary>
        /// Gets the form row display/editor container className.
        /// <para>
        /// Default: "display-field-container";
        /// </para>
        /// </summary>
        string FormRowEditorContainerClassName { get; set; }




        /// <summary>
        /// Gets the form row display/editor container tag.
        /// <para>
        /// Default: "";
        /// </para>
        /// </summary>
        string FormRowValidatorContainerTag { get; set; }
        /// <summary>
        /// Gets the form row display/editor container className.
        /// <para>
        /// Default: "display-validator-container";
        /// </para>
        /// </summary>
        string FormRowValidatorContainerClassName { get; set; }





        
        /// <summary>
        /// Gets the form row display container readonly className.
        /// <para>
        /// Default: "display-container-editor-readonly";
        /// </para>
        /// </summary>
        string FormRowDisplayReadOnlyClassName { get; set; }

        /// <summary>
        /// Gets the form row editor container readonly className.
        /// <para>
        /// Default: "display-container-editor-readonly";
        /// </para>
        /// </summary>
        string FormRowEditorReadOnlyClassName { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether to include Validator when creating LabelledDisplayEditor.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include validator by default]; otherwise, <c>false</c>.
        /// </value>
        bool IncludeValidatorByDefault { get; set; }

        /// <summary>
        /// Gets the validation message.
        /// <para>
        /// Default is "*"
        /// </para>
        /// </summary>
        string ValidationMessage { get; set; }


        /// <summary>
        /// Gets or sets the round trip prefix.
        /// </summary>
        /// <value>
        /// The round trip prefix.
        /// </value>
        string RoundTripPrefix { get; set; }

        /// <summary>
        /// Gets or sets the round trip suffix.
        /// </summary>
        /// <value>
        /// The round trip suffix.
        /// </value>
        string RoundTripSuffix { get; set; }

        /// <summary>
        /// Gets or sets the Text to show in Dropdowns when nothing was selected.
        /// </summary>
        /// <value>
        /// The unselected option text.
        /// </value>
        string UnselectedOptionText { get; set; }
    }
}