﻿namespace XAct.UI.Views
{
    using System;
    using System.Web.Mvc;

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ViewModeCaseAttribute : FilterAttribute, IAuthorizationFilter
    {
        private readonly string _viewCaseName;


        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeCaseAttribute"/> class
        /// <para>
        /// Sets the <see cref="IViewModeService.CurrentContext"/><c>.Name</c>
        /// so that the given <paramref name="viewCaseName"/>.
        /// @@@
        /// </para>
        /// </summary>
        /// <param name="viewCaseName">The view case.</param>
        public ViewModeCaseAttribute(string viewCaseName)
        {
            _viewCaseName = viewCaseName;
        }

        /// <summary>
        /// Called when authorization is required.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            IViewModeService viewModeService =
                XAct.DependencyResolver.Current.GetInstance<IViewModeService>();

            viewModeService.CurrentContext.Name = _viewCaseName;
        }
    }
}


