﻿namespace XAct.UI.Web.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    /// <summary>
    /// A specialization of the default 
    /// <see cref="DataAnnotationsModelMetadataProvider"/>
    /// that ensures empty strings (as they are sent back from the 
    /// form) are mapped to the Model as string.Empty, rather than
    /// the default null.
    /// <para>
    /// This saves the developer from having to set 
    /// [Display(ConvertEmptyStringToNull=false] by hand on 
    /// every ViewModel string property.
    /// </para>
    /// </summary>
    /// <internal>
    /// See: http://stackoverflow.com/questions/6593669/asp-net-mvc3-can-you-post-and-bind-an-array-of-objects
    /// </internal>
    public class NonNullStringMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        /// <summary>
        /// Gets the metadata for the specified property.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <param name="containerType">The type of the container.</param>
        /// <param name="modelAccessor">The model accessor.</param>
        /// <param name="modelType">The type of the model.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>
        /// The metadata for the property.
        /// </returns>
        protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
        {
            ModelMetadata modelMetadata = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);

            //if (string.IsNullOrEmpty(propertyName))
            //{
            //    return modelMetadata;
            //}

            if (modelType == typeof(String))
            {
                modelMetadata.ConvertEmptyStringToNull = false;
            }
            return modelMetadata;
        }
    }
}
