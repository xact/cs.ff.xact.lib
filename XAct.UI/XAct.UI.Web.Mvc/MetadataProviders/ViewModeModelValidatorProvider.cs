﻿namespace XAct.UI
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using XAct.UI.Views;

    /// <summary>
    /// A specialization of DataAnnotationsModelValidatorProvider that 
    /// removes from the Property's metadata any ValidatorAttributes
    /// if the AdditionalValues attribute has a "ViewMode" entry (put there
    /// by the <see cref="ViewModeModelMetadataProvider"/>
    /// <para>
    /// Register in app's global.asax.cs.
    /// </para>
    /// </summary>
    public class ViewModeModelValidatorProvider : DataAnnotationsModelValidatorProvider
    {

        protected override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata, ControllerContext context,
                                                                     IEnumerable<Attribute> attributes)
        {


            //int check = ModelValidatorProviders.Providers.Count;
            //string propertyName = metadata.PropertyName;

            //We received IEnumerable, but we need to use an array to manipulate it:
            List<Attribute> newAttributes = new List<Attribute>(attributes);

            object t;
            if (metadata.AdditionalValues.TryGetValue("ViewMode", out t))
            {
                ViewMode editorState = (ViewMode) t;
                if (editorState != ViewMode.Editable)
                {
                    metadata.RequestValidationEnabled = false;
                    //Clear the attribute collection -- and all Validation attributes in there:
                    newAttributes.Clear();
                    //return null;
                    return new ModelValidator[0];
                }
            }

            //otherwise return what we got in:
            return base.GetValidators(metadata, context, newAttributes);
        }
        
    }

}