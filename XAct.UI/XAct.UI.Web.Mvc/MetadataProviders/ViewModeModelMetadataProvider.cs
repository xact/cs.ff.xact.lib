﻿namespace XAct.UI {
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using XAct.UI.Views;

    /// <summary>
    /// A specialization of <see cref="DataAnnotationsModelMetadataProvider"/>
    /// that checks the current <see cref="IViewModeService"/>
    /// for any ViewMode rules that affect the rendering and validation of 
    /// the current Model/Property.
    /// <para>
    /// Register in app's global.asax.cs.
    /// </para>
    /// </summary>
    public class ViewModeModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {


        /// <summary>
        /// Gets the metadata for the specified property.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <param name="containerType">The type of the container.</param>
        /// <param name="modelAccessor">The model accessor.</param>
        /// <param name="modelType">The type of the model.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>
        /// The metadata for the property.
        /// </returns>
        protected override ModelMetadata CreateMetadata(
                                 IEnumerable<Attribute> attributes,
                                 Type containerType,
                                 Func<object> modelAccessor,
                                 Type modelType,
                                 string propertyName)
        {

            ModelMetadata data = base.CreateMetadata(
                                 attributes,
                                 containerType,
                                 modelAccessor,
                                 modelType,
                                 propertyName);

            //Neat trick to get instance of actual model: http://bit.ly/LNOrHB
          //  object target = modelAccessor.Target;
         //   object container = target.GetType().GetField("container").GetValue(target);

            //No Access to current ViewBag, as this is being created to stick *into* ViewBag...

            //FullPropertyName

            if (containerType != null)
            {
                //Get Service:
                IViewModeService viewRuleService =
                    XAct.DependencyResolver.Current.GetInstance<IViewModeService>();

                //Name 
                string contextName = viewRuleService.CurrentContext.Name;
                
                ViewMode viewMode =
                    viewRuleService.GetViewMode(
                        contextName,
                        containerType,
                        propertyName);

                if (viewMode != ViewMode.Undefined)
                {
                    data.AdditionalValues.Add("ViewMode", viewMode);
                }
            }
            return data;
        }

        

    }
}