﻿namespace XAct.UI.Web.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    /// <summary>
    /// An array of <see cref="SelectListItem"/>
    /// (which I could not find in the default .NET framework...)
    /// <para>
    /// Addendum: found it: It's ... <see cref="SelectList"/>
    /// </para>
    /// </summary>
    [Obsolete("Use the .NET SelectList class instead",false)]
    public class SelectListItemList : List<SelectListItem>
    {


    }

}
