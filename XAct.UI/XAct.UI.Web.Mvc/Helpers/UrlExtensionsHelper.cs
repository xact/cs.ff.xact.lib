namespace XAct
{
    using System;
    using System.Text.RegularExpressions;
    using System.Web;

    /// <summary>
    /// 
    /// </summary>
    //
    // Taken from Troy Goode's blog http://www.squaredroot.com/post/2008/06/MVC-and-SSL.aspx
    //
    //
    public class UrlExtensionsHelper
    {

        /// <summary>
        /// Gets or sets a value indicating whether to use SSL.
        /// </summary>
        /// <value><c>true</c> if [use SSL]; otherwise, <c>false</c>.</value>
        public bool UseSSL
        {
            get;
            set;
        }


        /// <summary>
        ///   Takes a relative or absolute url and returns the fully-qualified url path.
        /// </summary>
        /// <param name = "text">The url to make fully-qualified. Ex: Home/About</param>
        /// <returns>The absolute url plus protocol, server, and port. 
        /// <![CDATA[
        /// Ex: http://localhost:1234/Home/About
        /// ]]>
        /// </returns>
        public virtual string ToFullyQualifiedUrl(string text)
        {
            //### the VirtualPathUtility doesn"t handle querystrings, so we break the original url up
            string oldUrl = text;
            string[] oldUrlArray = (oldUrl.Contains("?") ? oldUrl.Split('?') : new[] {oldUrl, ""});

            //### we"ll use the Request.Url object to recreate the current server request"s base url
            //### requestUri.AbsoluteUri = "http://domain.com:1234/Home/Index?page=123"
            //### requestUri.LocalPath = "/Home/Index"
            //### requestUri.Query = "?page=123"
            //### subtract LocalPath and Query from AbsoluteUri and you get "http://domain.com:1234", which is urlBase
            Uri requestUri = GetRequestUri();
            string localPathAndQuery = requestUri.LocalPath + requestUri.Query;
            string urlBase = requestUri.AbsoluteUri.Substring(0, requestUri.AbsoluteUri.Length - localPathAndQuery.Length);

            //### convert the request url into an absolute path, then reappend the querystring, if one was specified
            string newUrl = VirtualPathUtility.ToAbsolute(oldUrlArray[0]);

            if (!string.IsNullOrEmpty(oldUrlArray[1]))
            {
                newUrl += "?" + oldUrlArray[1];
            }

            //### combine the old url base (protocol + server + port) with the new local path
            return urlBase + newUrl;
        }

        Uri GetRequestUri()
        {
            return HttpContext.Current.Request.Url;
        }

        /// <summary>
        ///   Looks for Html links in the passed string and turns each relative or absolute url and returns the fully-qualified url path.
        /// </summary>
        /// <param name = "text">The url to make fully-qualified. Ex: <a href = "Home/About">Blah</a></param>
        /// <returns>The absolute url plus protocol, server, and port. 
        ///   <![CDATA[
        /// Ex: <a href="http://localhost:1234/Home/About">Blah</a>
        /// ]]>
        /// </returns>
        public virtual string ToFullyQualifiedLink(string text)
        {
            Regex regex = new Regex(
                "(?<Before><a.*href=\")(?!http)(?<Url>.*?)(?<After>\".+>)",
                RegexOptions.Multiline | RegexOptions.IgnoreCase
                );

            return regex.Replace(text, m =>
                                       m.Groups["Before"].Value +
                                       ToFullyQualifiedUrl(m.Groups["Url"].Value) +
                                       m.Groups["After"].Value
                );
        }

        /// <summary>
        ///   Takes a relative or absolute url and returns the fully-qualified url path using the Https protocol.
        /// </summary>
        /// <param name = "text">The url to make fully-qualified. Ex: Home/About</param>
        /// <returns>
        ///   The absolute url plus server, and port using the Https protocol. 
        ///   <![CDATA[
        /// Ex: https://localhost:1234/Home/About
        /// ]]>
        /// </returns>
        public virtual string ToSslUrl(string text)
        {
            if (!this.UseSSL) return text;
            return ToFullyQualifiedUrl(text).Replace("http:", "https:");
        }

        /// <summary>
        ///   Looks for Html links in the passed string and turns each relative or absolute url into a fully-qualified url path using the Https protocol.
        /// </summary>
        /// <param name = "text">The url to make fully-qualified. Ex: <a href = "Home/About">Blah</a></param>
        /// <returns>
        ///   The absolute url plus server, and port using the Https protocol. 
        ///   <![CDATA[
        /// Ex: <a href="https://localhost:1234/Home/About">Blah</a>
        /// ]]>
        /// </returns>
        public virtual string ToSslLink(string text)
        {
            if (!this.UseSSL) return text;
            return ToFullyQualifiedLink(text).Replace("http:", "https:");
        }
    }
}