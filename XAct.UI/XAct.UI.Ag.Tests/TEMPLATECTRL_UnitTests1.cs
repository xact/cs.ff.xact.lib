﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using NUnit.Framework;

namespace XAct.Tests.UnitTests.ForNUnit {

  /// <summary>
  /// NUNit Test Fixture.
  /// </summary>
  [TestFixture(Description = "")]
  public class TEMPLATECTRL_UnitTests1 {

    #region Setup

    /// <summary>
    /// Sets up to do before any tests 
    /// within this test fixture are run.
    /// </summary>
    [TestFixtureSetUp]
    public void TestFixtureSetUp() {
      //run once before any tests in this testfixture have been run...
      //...setup vars common to all the upcoming tests...
    }

    /// <summary>
    /// Sets up to do before each and every 
    /// test within this test fixture is run.
    /// </summary>
    [SetUp]
    public void Setup() {
    }
    #endregion


    #region Tests
    /// <summary>
    /// An Example Test.
    /// </summary>
    [Test(Description = "")]
    public void UnitTest01() {

      /*
XAct.Windows.Controls.TEMPLATECTRL ctrl = 
  new XAct.Windows.Controls.TEMPLATECTRL();
 */
      Assert.IsTrue(true);
    }
    #endregion


    #region TearDown
    /// <summary>
    /// Tear down after each and every test.
    /// </summary>
    [TearDown]
    public void TearDown() {
    }

    /// <summary>
    /// Tear down after all tests in this fixture.
    /// </summary>
    [TestFixtureTearDown]
    public void TestFixtureTearDown() {
    }
    #endregion

  }//Class:End
}//Namespace:End
