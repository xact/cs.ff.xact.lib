﻿
namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasToolTipReadOnly
    {
        /// <summary>
        /// Gets the UI element's tooltip.
        /// </summary>
        /// <value>The tooltip.</value>
        string Tooltip { get; }
    }
}

