﻿namespace XAct.UI
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for generic UI Framework-Agnostic version of a ListItem.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Used extensively by MVP framework.
    ///   </para>
    /// </remarks>
    public interface ISelectableItem : IHasEnabled, IHasOrder, IHasKeyValue<object>, IHasSelected, IHasTag, IHasFilter
    {

    }
}