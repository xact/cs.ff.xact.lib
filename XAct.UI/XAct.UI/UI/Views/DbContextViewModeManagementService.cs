﻿using System.Collections.Generic;
using System.Diagnostics;
using XAct.Diagnostics;
using XAct.Services;

namespace XAct.UI.Views
{
    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IViewModeManagementService))]
    [DefaultBindingImplementation(typeof(IDbContextViewModeManagementService))]
    public class DbContextViewModeManagementService : InMemViewModeManagementService, IDbContextViewModeManagementService
    {
        private readonly IViewModeRuleDefinitionRepositoryService _viewModeRuleRepositoryService;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DbContextViewModeManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="viewModeCache">The view rule cache.</param>
        /// <param name="viewModeRuleRepositoryService">The view state rule repository service.</param>
        public DbContextViewModeManagementService (
            ITracingService tracingService,
            IViewModeCache viewModeCache, 
            IViewModeRuleDefinitionRepositoryService viewModeRuleRepositoryService)
            : base(tracingService, viewModeCache)
        {
            _viewModeRuleRepositoryService = viewModeRuleRepositoryService;
        }

        /// <summary>
        /// Initializes the internal <see cref="IViewModeCache"/>
        /// 	<para>
        /// This base example does nothing. Overrride to read from a datastore.
        /// </para>
        /// </summary>
        public override void Initialize()
        {

            IDictionary<string,ViewModeCacheItem> results = _viewModeRuleRepositoryService.GetAllViewSetRules();

            foreach (KeyValuePair<string, ViewModeCacheItem> valuePair in results)
            {

                base.TracingService.Trace(TraceLevel.Verbose,
                                          "Registering ViewMode Rule Case='{0}' ViewMode:'{1}'",
                                                  valuePair.Key,
                                                  valuePair.Value);

                this.RegisterRuleStateMask(valuePair.Value);
            }
            base.Initialize();
        }

    }


}
