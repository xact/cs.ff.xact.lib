﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using XAct.Diagnostics;
using XAct.Domain.Repositories;
using XAct.Services;

namespace XAct.UI.Views
{
    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof (IViewModeRuleDefinitionRepositoryService))]
    public class ViewModeRuleDefinitionRepositoryService : IViewModeRuleDefinitionRepositoryService
    {
        private readonly ITracingService _tracingService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeRuleDefinitionRepositoryService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService"></param>
        public ViewModeRuleDefinitionRepositoryService(ITracingService tracingService, 
            IRepositoryService repositoryService)
        {
            _tracingService = tracingService;
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Determines whether [is entity new] [the specified aggregate root entity].
        /// </summary>
        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
        /// <returns>
        ///   <c>true</c> if [is entity new] [the specified aggregate root entity]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsEntityNew(ViewModeRuleDefinition aggregateRootEntity)
        {
            return (aggregateRootEntity.Id == 0);
        }

        /// <summary>
        /// Goes the get em.
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, ViewModeCacheItem> GetAllViewSetRules()
        {
            _tracingService.Trace(TraceLevel.Verbose,"ViewModeRuleDefinitionRepositoryService:GoGetEm");

            //_viewModeRuleRepositoryService.GetByFilter(() => true, null, null,
            //                                            q => q.OrderBy(c => c.ViewModeCase.name))

//#pragma warning disable 168
//            string connStr = ServiceLocatorService.Current.GetInstance<IHostSettingsService>().Get<string>(
//#pragma warning restore 168
//                // ReSharper disable RedundantNameQualifier
//                "XActLibConnectionStringName",
//                Db.DefaultXActLibConnectionStringSettingsName);





           var results2 =
               _repositoryService.GetByFilter<ViewModeRuleDefinition>(x=>true).Select(
                    x=> new {
                        Case = x.ViewModeCase.Name,
                        ViewModel = x.ViewModeModelPropertyDefinition.ViewModeModelDefinition.Name,
                        ViewModeModelPropertyDefinition = x.ViewModeModelPropertyDefinition.Name,
                        Tag = x.ViewModeModelPropertyDefinition.Tag,
                        Mask = (ViewMode) x.ViewModeRaw,
                        RuleEngine = x.RuleEngineIdentifier,
                        RuleSetIdentifier =x.RuleSetIdentifier,
                        Note = x.Description
                    })
                    .OrderBy(o => o.Case)
                    .ThenBy(o => o.ViewModel)
                    .ThenBy(o => o.ViewModeModelPropertyDefinition);
                
            
            var results = results2.ToArray();

            Dictionary<string, ViewModeCacheItem> dict = new Dictionary<string, ViewModeCacheItem>();

            foreach (var r in results)
            {
                string key = "{0}:{1}:{2}:{3}".FormatStringInvariantCulture(
                    r.Case,
                    r.ViewModel,
                    r.ViewModeModelPropertyDefinition,
                    r.Tag
                    );

                dict[key] = new ViewModeCacheItem(key,r.Mask,r.Note,r.RuleEngine,r.RuleSetIdentifier);
            }

            return dict;
        }
    }
}
