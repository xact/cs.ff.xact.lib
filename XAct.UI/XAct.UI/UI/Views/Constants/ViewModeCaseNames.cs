﻿

namespace XAct.UI.Views.Constants
{
    /// <summary>
    /// Collection of often used View Case names
    /// to be used when setting <see cref="ViewModeCaseDefinition.Name"/>
    /// via <see cref="IViewModeService"/>
    /// </summary>
    public class ViewModeCaseNames
    {
        /// <summary>
        /// The View is a List.
        /// <para>
        /// Value = "List"
        /// </para>
        /// </summary>
        public const string List = "List";

        /// <summary>
        /// The View is a Create form View.
        /// <para>
        /// Value = "Create"
        /// </para>
        /// </summary>
        public const string Create = "Create";

        /// <summary>
        /// The View is a View form View.
        /// <para>
        /// Value = "View"
        /// </para>
        /// </summary>
        public const string View = "View";

        /// <summary>
        /// The View is an Edit form View.
        /// <para>
        /// Value = "Edit"
        /// </para>
        /// </summary>
        public const string Edit = "Edit";
    }
}
