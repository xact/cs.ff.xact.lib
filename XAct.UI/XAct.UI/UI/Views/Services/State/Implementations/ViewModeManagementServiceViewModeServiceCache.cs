namespace XAct.UI.Views.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IViewModeManagementServiceViewModeServiceState"/>
    /// to cache the ViewMode rules
    /// registered using <see cref="IViewModeManagementService"/> 
    /// </summary>
    public class ViewModeManagementServiceViewModeServiceState : Dictionary<string, ViewModeCacheItem>, IViewModeManagementServiceViewModeServiceState, IHasXActLibServiceState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeManagementServiceViewModeServiceState"/> class
        /// <para>
        /// Note that the dictionary keys are case-insensitive.
        /// </para>
        /// </summary>
        public ViewModeManagementServiceViewModeServiceState()
            : base(StringComparer.InvariantCultureIgnoreCase)
        {
        }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IViewModeManagementServiceViewModeServiceState"/> is initialized.
        /// </summary>
        /// <value>
        ///   <c>true</c> if initialized; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized
        {
            get { return _initialized; }
            set
            {
                if (_initialized && (value==false))
                {
                    throw new ArgumentException("cannot reset ViewModeCache.Initialized to false.");
                }
                _initialized = value;
            }
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        /// <returns></returns>
        public new void Clear()
        {
            {
                base.Clear();
                _initialized = false;

            }
        }

        private bool _initialized;

    }
}