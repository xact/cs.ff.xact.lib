// ReSharper disable CheckNamespace
namespace XAct.UI.Views
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a singleton 
    /// cache of the ViewMode rules
    /// registered using <see cref="IViewModeManagementService"/> 
    /// </summary>
    public interface IViewModeManagementServiceViewModeServiceState : IDictionary<string, ViewModeCacheItem>, IHasInitialized, IHasXActLibServiceState
    {
        

    }
}