﻿namespace XAct.UI.Views.Services.Implementations
{
    using System.Collections.Generic;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IViewModeManagementService), BindingLifetimeType.Undefined, Priority.Normal /*OK: An Override Priority: overrrides InMem solution */)]
    public class DbContextViewModeManagementService : InMemViewModeManagementService, IDbContextViewModeManagementService
    {
        private readonly IViewModeRuleDefinitionRepositoryService _viewModeRuleRepositoryService;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DbContextViewModeManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="viewModeCache">The view rule cache.</param>
        /// <param name="viewModeRuleRepositoryService">The view state rule repository service.</param>
        public DbContextViewModeManagementService (
            ITracingService tracingService,
            IViewModeManagementServiceViewModeServiceState viewModeCache, 
            IViewModeRuleDefinitionRepositoryService viewModeRuleRepositoryService)
            : base(tracingService, viewModeCache)
        {
            _viewModeRuleRepositoryService = viewModeRuleRepositoryService;
        }

        /// <summary>
        /// Initializes the internal <see cref="IViewModeManagementServiceViewModeServiceState"/>
        /// 	<para>
        /// This base example does nothing. Overrride to read from a datastore.
        /// </para>
        /// </summary>
        public override void Initialize()
        {

            IDictionary<string,ViewModeCacheItem> results = _viewModeRuleRepositoryService.GetAllViewSetRules();

            foreach (KeyValuePair<string, ViewModeCacheItem> valuePair in results)
            {

                _tracingService.Trace(TraceLevel.Verbose,
                                          "Registering ViewMode Rule Case='{0}' ViewMode:'{1}'",
                                                  valuePair.Key,
                                                  valuePair.Value);

                this.RegisterRuleStateMask(valuePair.Value);
            }
            base.Initialize();
        }

    }


}
