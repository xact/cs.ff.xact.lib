﻿namespace XAct.UI.Views.Services.Implementations
{
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class ViewModeRuleDefinitionRepositoryService : XActLibServiceBase, IViewModeRuleDefinitionRepositoryService
    {
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeRuleDefinitionRepositoryService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService"></param>
        public ViewModeRuleDefinitionRepositoryService(ITracingService tracingService, 
            IRepositoryService repositoryService):base(tracingService)
        {
            _repositoryService = repositoryService;
        }


        /// <summary>
        /// Goes the get em.
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, ViewModeCacheItem> GetAllViewSetRules()
        {
            _tracingService.Trace(TraceLevel.Verbose,"ViewModeRuleDefinitionRepositoryService:GoGetEm");

            //_viewModeRuleRepositoryService.GetByFilter(() => true, null, null,
            //                                            q => q.OrderBy(c => c.ViewModeCase.name))

//#pragma warning disable 168
//            string connStr = DependencyResolver.Current.GetInstance<IHostSettingsService>().Get<string>(
//#pragma warning restore 168
//                // ReSharper disable RedundantNameQualifier
//                "XActLibConnectionStringName",
//                Db.DefaultXActLibConnectionStringSettingsName);





           var results2 =
               _repositoryService.GetByFilter<ViewModeRuleDefinition>(x=>true).Select(
                    x=> new {
                        Case = x.ViewModeCase.Name,
                        ViewModel = x.ViewModeModelPropertyDefinition.ViewModeModelDefinition.Name,
                        ViewModeModelPropertyDefinition = x.ViewModeModelPropertyDefinition.Name,
                        Tag = x.ViewModeModelPropertyDefinition.Tag,
                        Mask = x.ViewMode,
                        RuleEngine = x.RuleEngineIdentifier,
                        RuleSetIdentifier =x.RuleSetIdentifier,
                        Note = x.Description
                    })
                    .OrderBy(o => o.Case)
                    .ThenBy(o => o.ViewModel)
                    .ThenBy(o => o.ViewModeModelPropertyDefinition);
                
            
            var results = results2.ToArray();

            Dictionary<string, ViewModeCacheItem> dict = new Dictionary<string, ViewModeCacheItem>();

            foreach (var r in results)
            {
                string key = "{0}:{1}:{2}:{3}".FormatStringInvariantCulture(
                    r.Case,
                    r.ViewModel,
                    r.ViewModeModelPropertyDefinition,
                    r.Tag
                    );

                dict[key] = new ViewModeCacheItem(key,r.Mask,r.Note,r.RuleEngine,r.RuleSetIdentifier);
            }

            return dict;
        }
    }
}
