namespace XAct.UI.Views.Services.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Rules;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IViewModeManagementService"/>
    /// to persist and retrieve the desired 
    /// <see cref="ViewMode"/> for a property in a ViewModel, according 
    /// to rules already registered using an overload of
    /// <see cref="RegisterRuleStateMask(string, Type, string, ViewMode,string,string,string,string)"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// It is decorated with <see cref="DefaultBindingImplementationAttribute"/>, 
    /// with Priority set to lo win order to allow for finding alternate (ie Db based)
    /// Services first.
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(IViewModeManagementService), BindingLifetimeType.Undefined, Priority.Low /*OK: Override by Db based solution */)]
    public class InMemViewModeManagementService : XActLibServiceBase, IInMemViewModeManagementService
    {

        /// <summary>
        /// Gets the view state cache.
        /// </summary>
        /// <internal>
        /// Note that DependencyInjectionContainer creates a single instance of this so should be ok
        /// </internal>
        protected IViewModeManagementServiceViewModeServiceState ViewModeCache { get { return _viewModeCache; } }
        private readonly IViewModeManagementServiceViewModeServiceState _viewModeCache;



        /// <summary>
        /// Gets the optional <see cref="IRuleSetService"/>.
        /// </summary>
        protected IRuleSetService RuleSetService { get { return _ruleSetService; } }
        private readonly IRuleSetService _ruleSetService;



        /// <summary>
        /// Initializes a new instance of the <see cref="InMemViewModeManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="viewModeCache">The view state cache.</param>
        public InMemViewModeManagementService(ITracingService tracingService, IViewModeManagementServiceViewModeServiceState viewModeCache):base(tracingService)
        {
            _viewModeCache = viewModeCache;
            _ruleSetService = null;
            // _ruleSetService = DependencyResolver.Current.GetInstance<IRuleSetService>(false);

        }

        /// <summary>
        /// Gets the current (thread specific) <see cref="IViewModeContext"/>
        /// 	<para>
        /// This is then used by the
        /// <see cref="GetViewMode(Type,string,string)"/>
        /// overloads.
        /// </para>
        /// </summary>
        public IViewModeContext CurrentContext
        {
            get { return DependencyResolver.Current.GetInstance<IViewModeContext>(); }
        }


        /// <summary>
        /// Initializes the internal <see cref="IViewModeManagementServiceViewModeServiceState"/>.
        /// Invoke manually -- or it's invoked the first time
        /// any of the <see cref="GetViewMode(string , string , string )"/> overloads.
        /// </summary>
        public virtual void Initialize()
        {
            _tracingService.Trace(TraceLevel.Verbose, "Initializing {0}",this.GetType().Name);

            //In this case, do nothing.
                _viewModeCache.Initialized = true;
        }

        /// <summary>
        /// Register (per thread) the the given property's desired <see cref="ViewMode"/>
        /// in the specified View Case.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">The name of the case (eg: 'Create', 'Edit', 'View', etc.)</param>
        /// <param name="viewModel">The Type of the ViewModel that is being rendered as a View.</param>
        /// <param name="viewPropertyName">The (case insensitive) name of the property to display.</param>
        /// <param name="viewMode">The <see cref="ViewMode"/> to register for the Property, in the specified View Case.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <param name="note">The note.</param>
        /// <param name="ruleSetEngineIdentifier">The rule set engine identifier.</param>
        /// <param name="ruleSetIdentifier">The rule set identifier.</param>
        public void RegisterRuleStateMask(string viewModeCaseName, Type viewModel, string viewPropertyName,
                                          ViewMode viewMode, string tag = null, string note = null, string ruleSetEngineIdentifier = null, string ruleSetIdentifier = null)
        {
            RegisterRuleStateMask(viewModeCaseName, viewModel.FullName, viewPropertyName, viewMode, tag, note, ruleSetEngineIdentifier, ruleSetIdentifier);
        }


        /// <summary>
        /// Register (per thread) the the given property's desired <see cref="ViewMode"/>
        /// in the specified View Case.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">The name of the case (eg: 'Create', 'Edit', 'View', etc.)</param>
        /// <param name="viewModelFullName">The <c>FullName</c> of the <see cref="System.Type"/> of the ViewModel that is being rendered as a View.</param>
        /// <param name="viewPropertyName">The (case insensitive) name of the property to display.</param>
        /// <param name="viewMode">The <see cref="ViewMode"/> to register for the Property, in the specified View Case.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <param name="note">The note.</param>
        /// <param name="ruleSetEngineIdentifier">The rule set engine identifier.</param>
        /// <param name="ruleSetIdentifier">The rule set identifier.</param>
        public void RegisterRuleStateMask(string viewModeCaseName, string viewModelFullName, string viewPropertyName,
                                          ViewMode viewMode, string tag = null, string note = null, string ruleSetEngineIdentifier = null, string ruleSetIdentifier = null)
        {
            string key = Key(viewModeCaseName, viewModelFullName, viewPropertyName,tag);

            RegisterRuleStateMask(key, viewMode, note,ruleSetEngineIdentifier,ruleSetIdentifier);
        }

        /// <summary>
        /// Registers the rule state mask.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="note">The note.</param>
        /// <param name="ruleSetEngineIdentifier">The rule set engine identifier.</param>
        /// <param name="ruleSetIdentifier">The rule set identifier.</param>
        /// <internal>
        /// This is there so that <c>DbContextViewModeManagementService</c>
        /// can do its thing -- until I get time to refactor.
        ///   </internal>
        protected void RegisterRuleStateMask(string key, ViewMode viewMode, string note=null, string ruleSetEngineIdentifier=null,string ruleSetIdentifier=null)
        {

            _tracingService.Trace(TraceLevel.Verbose, "Registering RuleStateMask '{0}'='{1}'",key, viewMode);

            ViewModeCacheItem viewModeCacheItem = new ViewModeCacheItem(key,viewMode,null,ruleSetEngineIdentifier,ruleSetEngineIdentifier);

            RegisterRuleStateMask(viewModeCacheItem);
        }

        /// <summary>
        /// Register the given property's desired <see cref="ViewMode"/>
        /// in the specified View Case.
        /// </summary>
        /// <param name="viewModeCacheItem">The view state cache item.</param>
        public void RegisterRuleStateMask(ViewModeCacheItem viewModeCacheItem)
        {
            _viewModeCache[viewModeCacheItem.Key] = viewModeCacheItem;
        }


        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <paramref name="viewModel"/>
        /// when rendered in the already defined/current <c>View Case</c>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from 
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.) 
        /// </para>
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <returns>The <see cref="ViewMode"/></returns>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        public ViewMode GetViewMode(Type viewModel, string viewPropertyName, string tag = null)
        {
            var result = GetViewMode(viewModel.FullName, viewPropertyName,tag);
            return result;
        }

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <c>ViewModel</c>.
        /// when rendered in the already defined/current <c>View Case</c>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from 
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.) 
        /// </para>
        /// </summary>
        /// <param name="viewModelFullName">The <c>FullName</c> of the view model's <see cref="Type"/>.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>The <see cref="ViewMode"/></returns>
        public ViewMode GetViewMode(string viewModelFullName, string viewPropertyName, string tag = null)
        {
            IViewModeContext viewModeContext = this.CurrentContext;
            if (viewModeContext == null)
            {
                throw new Exception(
                    "GetViewMode was invoked before IViewModeManagementService.CurrentContext was set using ViewModeActionFilterAttribute or other option.");
            }
            if (viewModeContext.Name.IsNullOrEmpty())
            {
                throw new Exception(
                    "GetViewMode was invoked before IViewModeManagementService.CurrentContext's Name property was set using ViewModeActionFilterAttribute or other option.");
            }
            var result = GetViewMode(viewModeContext.Name, viewModelFullName, viewPropertyName,tag);
            return result;
        }


        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <paramref name="viewModel"/>
        /// when rendered in the specified <paramref name="viewModeCaseName"/>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from 
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.) 
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">Name of the view case.</param>
        /// <param name="viewModel">The view model.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <returns>The <see cref="ViewMode"/></returns>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        public ViewMode GetViewMode(string viewModeCaseName, Type viewModel, string viewPropertyName, string tag = null)
        {
            var result = GetViewMode(viewModeCaseName, viewModel.FullName, viewPropertyName,tag);
            return result;

        }

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <c>ViewModel</c>.
        /// when rendered in the specified <paramref name="viewModeCaseName"/>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">Name of the view case.</param>
        /// <param name="viewModelFullName">The <c>FullName</c> of the view model's <see cref="Type"/>.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        public ViewMode GetViewMode(string viewModeCaseName, string viewModelFullName, string viewPropertyName, string tag)
        {
            string key = Key(viewModeCaseName, viewModelFullName, viewPropertyName, tag);

            if (!_viewModeCache.Initialized)
            {
                Initialize();
            }

            _tracingService.Trace(TraceLevel.Verbose, "{0}:GetViewSate(): GetViewMode(Key={1}) alreadyInitialized. Count={2}",
                                  this.GetType().Name, 
                                  key,
                                  _viewModeCache.Count);



            ViewModeCacheItem viewModeCacheItem; 

            if (!_viewModeCache.TryGetValue(key, out viewModeCacheItem))
            {
                _tracingService.Trace(TraceLevel.Verbose, "{0}:GetViewSate(): No ViewMode Rule registered for '{1}'. Setting to '{2}'",
                    this.GetType().Name,
                    key, 
                    viewModeCacheItem);

                return ViewMode.Undefined;
            }

            if (viewModeCacheItem.ViewMode == ViewMode.Rule)
            {

                _tracingService.Trace(TraceLevel.Verbose,
                                      "{0}:GetViewSate(): ViewMode Rule for '{1}' indicating should come from a RuleEngine...'",
                                      this.GetType().Name,
                                      key);

                if (_ruleSetService == null)
                {
                    _tracingService.Trace(TraceLevel.Verbose,
                                          "{0}:GetViewSate():  but there is no RuleEngine...'",
                                          this.GetType().Name,
                                          key);

                }
                else
                {
                ViewMode result;
                    if (GetValueFromRuleEngine(key, viewModeCacheItem, out result))
                    {

                        _tracingService.Trace(TraceLevel.Verbose,
                                              "{0}:GetViewSate(): ViewMode Rule processed via secondar RuleEngine for '{1}' as '{2}'",
                                              this.GetType().Name,
                                              key,
                                              result);

                        return result;
                    }
                }

            }


            _tracingService.Trace(TraceLevel.Verbose, "{0}:GetViewSate(): ViewMode Rule found for '{1}' as '{2}'",
                this.GetType().Name,
                key, 
                viewModeCacheItem.ViewMode);
            
            return viewModeCacheItem.ViewMode;
        }

        private bool GetValueFromRuleEngine(string key, ViewModeCacheItem viewModeCacheItem, out ViewMode viewMode)
        {
            _tracingService.Trace(TraceLevel.Verbose,
                "{0}:GetViewSate(): ViewMode Rule found for '{1}'. But notice that is set to '{2}', and therefore invokes RuleEngine",
                this.GetType().Name,
                key, 
                viewModeCacheItem);


            //Currently ignoring:viewMode.RuleSetIdentifier
            if (viewModeCacheItem.RuleSetIdentifier.IsNullOrEmpty())
            {
                throw new ArgumentException("{0}:GetViewSate(): Configuration Error: ViewMode for '{1}' is set to '{2}', but RuleSetIdentifier is null.".FormatStringInvariantCulture(
                    this.GetType().Name,
                    key, 
                    viewModeCacheItem));

            }


            _tracingService.Trace(TraceLevel.Verbose, "{0}:GetViewSate(): For  '{1}', Invoking Rule Engine for RuleSet '{3}'",
                    this.GetType().Name,
                    key, 
                    viewModeCacheItem,
                    viewModeCacheItem.RuleSetIdentifier);


            RuleSetTarget<ViewModeCacheItem> ruleSetTarget = 
                new RuleSetTarget<ViewModeCacheItem>(viewModeCacheItem);

            //Save the ViewCase in the Bag, so that the target Item can pick it up:
            ruleSetTarget.ItemData["ViewMode"] = this.CurrentContext.Name;

            //Try to find the rule, and run it:
            try
            {
                //The identifier could be a int/id, or the name of the ruleset:
                Guid ruleSetId;

                if (Guid.TryParse(viewModeCacheItem.RuleSetIdentifier, out ruleSetId))
                {
                    _ruleSetService.ApplyRuleSetOn<ViewModeCacheItem>(ruleSetId, ruleSetTarget);

                }
                else
                {
                    _ruleSetService.ApplyRuleSetOn<ViewModeCacheItem>(ruleSetTarget,
                                                                       viewModeCacheItem.RuleSetIdentifier);
                }
            }
            catch (System.Exception e)
            {
                _tracingService.TraceException(TraceLevel.Warning, e, 
                    "{0}:GetViewSate(): For  '{1}': an exception occurred when trying to process rule identifier '{3}'",
                    this.GetType().Name,
                    key, 
                    viewModeCacheItem,
                     viewModeCacheItem.RuleSetIdentifier);
            }

            //Process Result:

            if(!ruleSetTarget.IsMatched)
            {
                _tracingService.Trace(TraceLevel.Verbose, "Rule Not Matched.");
            }
            else{
            _tracingService.Trace(TraceLevel.Verbose, "Rule Matched.");
                try
                {

                    viewMode = (ViewMode)ruleSetTarget.Result;

                    _tracingService.Trace(TraceLevel.Verbose,
                        "{0}:GetViewSate(): For  '{1}': parsed Target.Result value ('{4}') back from Rule ('{3}') into a ViewMode Enum value {5}.",
                    this.GetType().Name,
                    key,
                    viewModeCacheItem,
                     viewModeCacheItem.RuleSetIdentifier,
                     ruleSetTarget.Result,
                     viewMode
                     );

                    return true;

                }
                catch
                {
                    _tracingService.Trace(TraceLevel.Warning,
                        "{0}:GetViewSate(): For  '{1}': could not parse Target.Result value ('{4}') returned from Rule ('{3}') back into a ViewMode Enum value.",
                    this.GetType().Name,
                    key,
                    viewModeCacheItem,
                     viewModeCacheItem.RuleSetIdentifier,
                     ruleSetTarget.Result
                     );
                }
            }
            //As we ended up not doing anything, return the original value:
            viewMode = viewModeCacheItem.ViewMode;
            return false;
        }


        private string Key(string viewModeCaseName, string viewModelFullName, string viewPropertyName,string tag)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Building Key: {0}:{1}:{2}:{3}", viewModeCaseName, viewModelFullName, viewPropertyName, tag);
            string key = "{0}:{1}:{2}:{3}".FormatStringInvariantCulture(viewModeCaseName, viewModelFullName, viewPropertyName, tag);
            return key;
        }


    }
}