namespace XAct.UI.Views.Services.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of
    /// <see cref="IViewModeService"/>
    /// to retrieve the desired 
    /// <see cref="ViewMode"/> for a property in a ViewModel, according 
    /// to rules already registered in an internal singleton cache
    /// using <see cref="IViewModeManagementService"/>.
    /// </summary>
    public class ViewModeService : XActLibServiceBase, IViewModeService
    {
        private readonly IViewModeManagementService _viewRuleManagmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="viewRuleManagmentService">The view rule managment service.</param>
        public ViewModeService(ITracingService tracingService, IViewModeManagementService viewRuleManagmentService):base(tracingService)
        {
            _viewRuleManagmentService = viewRuleManagmentService;
        }

        /// <summary>
        /// Gets the current (thread specific) <see cref="IViewModeContext"/>
        /// 	<para>
        /// This is then used by the
        /// <see cref="GetViewMode(Type,string,string)"/>
        /// overloads.
        /// </para>
        /// </summary>
        public IViewModeContext CurrentContext
        {
            get { return _viewRuleManagmentService.CurrentContext; }
        }


        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <paramref name="viewModel"/>
        /// when rendered in the already defined/current <c>View Case</c>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        public ViewMode GetViewMode(Type viewModel, string viewPropertyName, string tag = null)
        {
            IViewModeContext viewModeContext = this.CurrentContext;
            if (viewModeContext == null)
            {
                throw new Exception(
                    "GetViewMode was invoked before IViewModeManagementService.CurrentContext was set using ViewModeActionFilterAttribute or other option.");
            }
            if (viewModeContext.Name.IsNullOrEmpty())
            {
                throw new Exception(
                    "GetViewMode was invoked before IViewModeManagementService.CurrentContext's Name property was set using ViewModeActionFilterAttribute or other option.");
            }
            return _viewRuleManagmentService.GetViewMode(this.CurrentContext.Name, viewModel.FullName, viewPropertyName,tag);
        }

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <c>ViewModel</c>.
        /// when rendered in the already defined/current <c>View Case</c>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModelFullName">The <c>FullName</c> of the view model's <see cref="Type"/>.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        public ViewMode GetViewMode(string viewModelFullName, string viewPropertyName, string tag = null)
        {
            IViewModeContext viewModeContext = this.CurrentContext;

            if (viewModeContext == null)
            {
                throw new Exception(
                    "GetViewMode was invoked before IViewModeManagementService.CurrentContext was set using ViewModeActionFilterAttribute or other option.");
            }
            if (viewModeContext.Name.IsNullOrEmpty())
            {
                throw new Exception(
                    "GetViewMode was invoked before IViewModeManagementService.CurrentContext's Name property was set using ViewModeActionFilterAttribute or other option.");
            }
            return _viewRuleManagmentService.GetViewMode(this.CurrentContext.Name, viewModelFullName, viewPropertyName,tag);
        }

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <paramref name="viewModel"/>
        /// when rendered in the specified <paramref name="viewModeCaseName"/>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">Name of the view case.</param>
        /// <param name="viewModel">The view model.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        public ViewMode GetViewMode(string viewModeCaseName, Type viewModel, string viewPropertyName, string tag = null)
        {
            return _viewRuleManagmentService.GetViewMode(viewModeCaseName, viewModel.FullName, viewPropertyName,tag);
        }

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <c>ViewModel</c>.
        /// when rendered in the specified <paramref name="viewModeCaseName"/>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">Name of the view case.</param>
        /// <param name="viewModelFullName">The <c>FullName</c> of the view model's <see cref="Type"/>.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        public ViewMode GetViewMode(string viewModeCaseName, string viewModelFullName, string viewPropertyName, string tag)
        {
            return _viewRuleManagmentService.GetViewMode(viewModeCaseName, viewModelFullName, viewPropertyName,tag);
        }
    }
}