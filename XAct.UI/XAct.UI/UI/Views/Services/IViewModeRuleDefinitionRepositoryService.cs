// ReSharper disable CheckNamespace
namespace XAct.UI.Views
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a Repository Service to retrieve <see cref="ViewModeRuleDefinition"/>s
    /// from a datastore.
    /// </summary>
    public interface IViewModeRuleDefinitionRepositoryService : IHasXActLibService
    {
        /// <summary>
        /// Goes the get em.
        /// </summary>
        IDictionary<string, ViewModeCacheItem> GetAllViewSetRules();
    }
}