namespace XAct.UI.Views
{
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IViewModeContext"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IViewModeContext), BindingLifetimeType.SingletonPerWebRequestScope, Priority.Low)]
    public class ViewModeContext : IViewModeContext
    {
        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        public string Name { get; set; }


    }
}