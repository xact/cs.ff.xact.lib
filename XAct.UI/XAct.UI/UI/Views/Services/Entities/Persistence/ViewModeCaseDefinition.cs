
namespace XAct.UI.Views
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The View case. 
    /// <para>
    /// Examples are "Create","View","Edit", and in Workflow scenarios,
    /// may include "ReEdit-n" additional cases.
    /// </para>
    /// </summary>
    [DataContract]
    public class ViewModeCaseDefinition : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasName, IHasDescription
    {
        /// <summary>
        /// Gets or sets the datastore id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets the name of the View Case (eg: 'Create', 'Edit', .
        /// <para>Member defined in<see cref="T:XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name of the view.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets an optional description of the View case/scenario.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeCaseDefinition"/> class.
        /// </summary>
        public ViewModeCaseDefinition()
        {
            this.GenerateDistributedId();
        }
    }
}