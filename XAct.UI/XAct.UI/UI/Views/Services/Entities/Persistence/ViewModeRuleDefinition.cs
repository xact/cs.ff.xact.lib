namespace XAct.UI.Views
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The definition of the rule to apply for a specific 
    /// <see cref="ViewModeModelPropertyDefinition"/>
    /// when viewed in a specific <see cref="ViewModeCase"/>
    /// </summary>
    [DataContract]
    public class ViewModeRuleDefinition : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasDescription
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the FK to the <cref name="ViewCase"/>.
        /// </summary>
        /// <value>
        /// The FK to the <see cref="ViewModeCase"/>.
        /// </value>
        [DataMember]
        public virtual Guid ViewCaseFK { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="ViewModeCase"/> 
        /// under whcih this rule is to be applied.
        /// </summary>
        /// <value>
        /// The view case.
        /// </value>
        [DataMember]
        public virtual ViewModeCaseDefinition ViewModeCase { get; set; }


        /// <summary>
        /// Gets or sets the FK to the 
        /// <see cref="ViewModeModelPropertyDefinition"/> of the <c>ViewModel</c>
        /// property this rule is for.
        /// </summary>
        /// <value>
        /// The view model property definition FK.
        /// </value>
        [DataMember]
        public virtual Guid ViewModeModelPropertyDefinitionFK { get; set; }

        /// <summary>
        /// Gets the
        /// <see cref="ViewModeModelPropertyDefinition"/> of the <c>ViewModel</c>
        /// property this rule is for.
        /// </summary>
        /// <value>
        /// The view model property definition.
        /// </value>
        [DataMember]
        public virtual ViewModeModelPropertyDefinition ViewModeModelPropertyDefinition { get; protected set; }

        /// <summary>
        /// Gets or sets the <see cref="ViewMode"/>
        /// to apply to the Property when displayed under the specified 
        /// View Case.
        /// </summary>
        /// <value>
        /// The <see cref="ViewMode"/>.
        /// </value>
        [DataMember]
        public virtual ViewMode ViewMode { get; set; }


        /// <summary>
        /// Gets or sets an (optional) identifier expressing which rule engine to query.
        /// </summary>
        /// <value>
        /// The rule set id.
        /// </value>
        [DataMember]
        public virtual string RuleEngineIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the RuleEngine specific id for a RuleSet containing Rules that determine the ViewMode of this UI element.
        /// </summary>
        /// <value>
        /// The rule set id.
        /// </value>
        [DataMember]
        public virtual string RuleSetIdentifier { get; set; }

        /// <summary>
        /// Gets or sets an optional note to help describe the rule's reason.
        /// </summary>
        /// <value>
        /// The note.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeRuleDefinition"/> class.
        /// </summary>
        public ViewModeRuleDefinition()
        {
            this.GenerateDistributedId();
        }
    }
}