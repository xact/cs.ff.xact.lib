
namespace XAct.UI.Views
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Definitions of the ViewModel objects.
    /// <para>
    /// These will be the full Namespace and Type names (not Assembly names).
    /// </para>
    /// </summary>
    [DataContract]
    public class ViewModeModelDefinition : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasName, IHasDescription
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="T:XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets an optional description of the ViewModel's use, etc.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelDefinition"/> class.
        /// </summary>
        public ViewModeModelDefinition()
        {
            this.GenerateDistributedId();
        }
    }
}