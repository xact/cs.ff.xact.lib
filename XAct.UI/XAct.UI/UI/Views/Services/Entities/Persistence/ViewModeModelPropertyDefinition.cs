namespace XAct.UI.Views
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The definition of a single property within
    /// a <see cref="ViewModeModelDefinition"/>
    /// </summary>
    [DataContract]
    public class ViewModeModelPropertyDefinition : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasName, IHasTag
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the FK to the parent 
        /// <see cref="ViewModeModelDefinition"/>.
        /// </summary>
        /// <value>
        /// The FK to the <see cref="ViewModeModelDefinition"/>.
        /// </value>
        [DataMember]
        public virtual Guid ViewModeModelDefinitionFK { get; set; }

        /// <summary>
        /// Gets or sets the parent
        /// <see cref="ViewModeModelDefinition"/>.
        /// </summary>
        /// <value>
        /// The <see cref="ViewModeModelDefinition"/>.
        /// </value>
        [DataMember]
        public virtual ViewModeModelDefinition ViewModeModelDefinition { get; protected set; }

        /// <summary>
        /// Gets the Property Name.
        /// <para>Member defined in<see cref="T:XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }


        /// <summary>
        /// Gets the tag of the object.
        /// <para>Use for specifying a specific use case.</para>
        /// <para>Member defined in<see cref="XAct.IHasTag"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public string Tag { get; set; }


        //Can't get this to work!
        //public virtual ICollection<ViewMode> Rules
        //{
        //    get { return _rules ?? (_rules = new List<ViewMode>()); }
        //    protected set { _rules = value; }
        //}
        //private ICollection<ViewMode> _rules;


        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeModelPropertyDefinition"/> class.
        /// </summary>
        public ViewModeModelPropertyDefinition()
        {
            this.GenerateDistributedId();
        }
    }
}