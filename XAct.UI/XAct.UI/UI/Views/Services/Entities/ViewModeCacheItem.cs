﻿

namespace XAct.UI.Views
{
    /// <summary>
    /// An item within <see cref="IViewModeManagementServiceViewModeServiceState"/>
    /// </summary>
    public class ViewModeCacheItem
    {
        /// <summary>
        /// Gets the key with which it's stored in the <see cref="IViewModeManagementServiceViewModeServiceState"/>.
        /// </summary>
        public string Key { get;  /*private*/ set; }

        /// <summary>
        /// Gets or sets the cached <see cref="ViewMode"/>.
        /// </summary>
        /// <value>
        /// The cached <see cref="ViewMode"/>.
        /// </value>
        public ViewMode ViewMode { get; /*private*/ set; }


        /// <summary>
        /// Gets the note.
        /// </summary>
        public string Note { get; private set; }
        /// <summary>
        /// Gets the rule engine identifier (currently only have one, but could be more in future).
        /// </summary>
        public string RuleEngineIdentifier { get; private set; }
        /// <summary>
        /// Gets or sets the serialized identifier of the RulSet (could be id or string).
        /// </summary>
        /// <value>
        /// The rule set identifier.
        /// </value>
        public string RuleSetIdentifier { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModeCacheItem"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="viewMode">State of the view.</param>
        /// <param name="note">The note.</param>
        /// <param name="ruleEngine">The rule engine.</param>
        /// <param name="ruleSetIdentifier">The rule set identifier.</param>
        public ViewModeCacheItem(string key, ViewMode viewMode, string note,string ruleEngine,string ruleSetIdentifier)
        {
            Key = key;
            ViewMode = viewMode;
            Note = note;
            RuleEngineIdentifier = ruleEngine;
            RuleSetIdentifier = ruleSetIdentifier;
        }


    }
}
