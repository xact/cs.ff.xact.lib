// ReSharper disable CheckNamespace
namespace XAct.UI.Views
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a service to register and later retrieve the desired 
    /// <see cref="ViewMode"/> for a property in a ViewModel, according 
    /// to rules already registered in an internal singleton cache
    /// using <see cref="IViewModeManagementService"/>.
    /// </summary>
    public interface IViewModeManagementService : IHasXActLibService
    {
        /// <summary>
        /// Gets the current (thread specific) <see cref="IViewModeContext"/>
        /// 	<para>
        /// This is then used by the
        /// <see cref="GetViewMode(Type,string,string)"/>
        /// overloads.
        /// </para>
        /// </summary>
        IViewModeContext CurrentContext { get; }

        /// <summary>
        /// Initializes the internal <see cref="IViewModeManagementServiceViewModeServiceState"/>.
        /// Invoke manually -- or it's invoked the first time
        /// any of the <see cref="GetViewMode(string , string , string )"/> overloads.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Register (per thread) the the given property's desired <see cref="ViewMode"/>
        /// in the specified View Case.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">The name of the case (eg: 'Create', 'Edit', 'View', etc.)</param>
        /// <param name="viewModel">The Type of the ViewModel that is being rendered as a View.</param>
        /// <param name="viewPropertyName">The (case insensitive) name of the property to display.</param>
        /// <param name="viewMode">The <see cref="ViewMode"/> to register for the Property, in the specified View Case.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <param name="note">The note.</param>
        /// <param name="ruleSetEngineIdentifier">The rule set engine identifier.</param>
        /// <param name="ruleSetIdentifier">The rule set identifier.</param>
        void RegisterRuleStateMask(string viewModeCaseName, Type viewModel, string viewPropertyName, ViewMode viewMode, string tag = null, string note = null, string ruleSetEngineIdentifier = null, string ruleSetIdentifier = null);

        /// <summary>
        /// Register (per thread) the given property's desired <see cref="ViewMode"/>
        /// in the specified View Case.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">The name of the case (eg: 'Create', 'Edit', 'View', etc.)</param>
        /// <param name="viewModelFullName">The <c>FullName</c> of the <see cref="System.Type"/> of the ViewModel that is being rendered as a View.</param>
        /// <param name="viewPropertyName">The (case insensitive) name of the property to display.</param>
        /// <param name="viewMode">The <see cref="ViewMode"/> to register for the Property, in the specified View Case.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <param name="note">The note.</param>
        /// <param name="ruleSetEngineIdentifier">The rule set engine identifier.</param>
        /// <param name="ruleSetIdentifier">The rule set identifier.</param>
        void RegisterRuleStateMask(string viewModeCaseName, string viewModelFullName, string viewPropertyName,
                                   ViewMode viewMode, string tag = null, string note = null, string ruleSetEngineIdentifier = null, string ruleSetIdentifier = null);




        /// <summary>
        /// Register the given property's desired <see cref="ViewMode"/>
        /// in the specified View Case.
        /// </summary>
        /// <param name="viewModeCacheItem">The view state cache item.</param>
        void RegisterRuleStateMask(ViewModeCacheItem viewModeCacheItem);




        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <paramref name="viewModel"/>
        /// when rendered in the already defined/current <c>View Case</c>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        ViewMode GetViewMode(Type viewModel, string viewPropertyName, string tag = null);

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <c>ViewModel</c>.
        /// when rendered in the already defined/current <c>View Case</c>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModelFullName">The <c>FullName</c> of the view model's <see cref="Type"/>.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        ViewMode GetViewMode(string viewModelFullName, string viewPropertyName, string tag = null);

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <paramref name="viewModel"/>
        /// when rendered in the specified <paramref name="viewModeCaseName"/>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">Name of the view case.</param>
        /// <param name="viewModel">The view model.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        ViewMode GetViewMode(string viewModeCaseName, Type viewModel, string viewPropertyName, string tag);

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <c>ViewModel</c>.
        /// when rendered in the specified <paramref name="viewModeCaseName"/>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from
        /// <see cref="Constants.ViewModeCaseNames"/> ('Create', 'View', 'Edit', etc.)
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">Name of the view case.</param>
        /// <param name="viewModelFullName">The <c>FullName</c> of the view model's <see cref="Type"/>.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>
        /// The <see cref="ViewMode"/>
        /// </returns>
        ViewMode GetViewMode(string viewModeCaseName, string viewModelFullName, string viewPropertyName, string tag);
    }
}