namespace XAct.UI.Views
{
    /// <summary>
    /// A specialization of the <see cref="IViewModeManagementService"/>
    /// contract.
    /// </summary>
    public interface IDbContextViewModeManagementService : IViewModeManagementService, IHasXActLibService
    {
        
    }
}