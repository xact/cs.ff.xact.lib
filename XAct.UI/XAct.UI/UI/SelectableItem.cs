﻿namespace XAct.UI
{
    using System.Runtime.Serialization;

    /// <summary>
    ///   An implementation of
    ///   <see cref = "ISelectableItem{TValue}" />
    ///   tha is cross UI-Framework (WinForm, ASP.NET, MVC, WPF, etc.)
    /// <para>
    /// This object is intended for crossing tiers, and is not suitable
    /// for data persistence. 
    /// For persistence of Records, see <c>IHasReferenceData</c>
    /// </para>
    /// </summary>
    [DataContract]
    public class SelectableItem : ISelectableItem
    {
        #region ISelectableItem Members

        /// <summary>
        ///   Gets or sets the ListItem key (in most cases
        /// this will be the string representation of the int or Guid id.
        /// </summary>
        /// <value>The key.</value>
        [DataMember]
        public string Key { get; set; }

        /// <summary>
        ///   Gets or sets the ListItem value.
        /// </summary>
        /// <value>The value.</value>
        [DataMember]
        public string Value { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder"/>.
        /// </para>
        /// </summary>
        /// <value>The order.</value>
        [DataMember]
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// <see cref="ISelectableItem{TValue}"/> is selected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if selected; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Selected { get; set; }


        /// <summary>
        /// Gets a tag for the line item.
        /// <para>
        /// Can be used to associate an image resource to.
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasTag"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public string Tag { get; set; }

        /// <summary>
        /// Gets a Filter for the line item.
        /// <para>
        /// Use for filtering against for a subset of values.
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasFilter"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public string Filter { get; set; }



        /// <summary>
        /// Gets or sets the data associated with the element.
        /// Data is not serialized in the backing datastore record,
        /// but is a bucket for transporting associated information
        /// across tiers.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [DataMember]
        public string Data { get; set; }
        #endregion


    }
}