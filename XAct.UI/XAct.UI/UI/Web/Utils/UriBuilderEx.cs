// ReSharper disable CheckNamespace
namespace XAct.UI.Web.Utils
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///   Extended version of <see cref = "UriBuilder" />.
    /// </summary>
    public class UriBuilderEx : UriBuilder
    {
        private Dictionary<string,string> _queryString;
        private string _extraValue;

        #region Properties

        /// <summary>
        ///   Dictionary of the query arguments.
        /// </summary>
        public Dictionary<string,string> QueryString
        {
            get
            {
                if (_queryString == null)
                {
                    _queryString = new Dictionary<string,string>();
                }

                return _queryString;
            }
        }

        /// <summary>
        ///   Gets or sets the name of the page.
        /// </summary>
        /// <value>The name of the page.</value>
        public string PageName
        {
            get
            {
                string path = base.Path;
                return path.Substring(path.LastIndexOf("/") + 1);
            }
            set
            {
                string path = base.Path;
                path = path.Substring(0, path.LastIndexOf("/"));
                base.Path = string.Concat(path, "/", value);
            }
        }

        #endregion

        #region Constructors overloads

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UriBuilderEx" /> class.
        /// </summary>
        public UriBuilderEx()
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UriBuilderEx" /> class.
        /// </summary>
        /// <param name = "uri">The URI.</param>
        public UriBuilderEx(string uri)
            : base(uri)
        {
            PopulateQueryString();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UriBuilderEx" /> class.
        /// </summary>
        /// <param name = "uri">The URI.</param>
        public UriBuilderEx(Uri uri)
            : base(uri)
        {
            PopulateQueryString();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UriBuilderEx" /> class.
        /// </summary>
        /// <param name = "schemeName">Name of the scheme.</param>
        /// <param name = "hostName">Name of the host.</param>
        public UriBuilderEx(string schemeName, string hostName)
            : base(schemeName, hostName)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UriBuilderEx" /> class.
        /// </summary>
        /// <param name = "scheme">The scheme.</param>
        /// <param name = "host">The host.</param>
        /// <param name = "portNumber">The port number.</param>
        public UriBuilderEx(string scheme, string host, int portNumber)
            : base(scheme, host, portNumber)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UriBuilderEx" /> class.
        /// </summary>
        /// <param name = "scheme">The scheme.</param>
        /// <param name = "host">The host.</param>
        /// <param name = "port">The port.</param>
        /// <param name = "pathValue">The path value.</param>
        public UriBuilderEx(string scheme, string host, int port, string pathValue)
            : base(scheme, host, port, pathValue)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UriBuilderEx" /> class.
        /// </summary>
        /// <param name = "scheme">An Internet access protocol.</param>
        /// <param name = "host">A DNS-style domain name or IP address.</param>
        /// <param name = "port">An IP port number for the service.</param>
        /// <param name = "path">The path to the Internet resource.</param>
        /// <param name = "extraValue">A query string or fragment identifier.</param>
        /// <exception cref = "T:System.ArgumentException">
        ///   <paramref name = "extraValue" /> is neither null nor <see cref = "F:System.String.Empty" />, nor does a valid fragment identifier begin with a number sign (#), nor a valid query string begin with a question mark (?). </exception>
        /// <exception cref = "T:System.ArgumentOutOfRangeException">
        ///   <paramref name = "port" /> is less than 0. </exception>
        public UriBuilderEx(string scheme, string host, int port, string path, string extraValue)
            : base(scheme, host, port, path, extraValue)
        {
            _extraValue = extraValue;
        }


        /// <summary>
        ///   Initializes a new instance of the <see cref = "UriBuilderEx" /> class.
        /// </summary>
        /// <param name = "scheme">An Internet access protocol.</param>
        /// <param name = "host">A DNS-style domain name or IP address.</param>
        /// <param name = "port">An IP port number for the service.</param>
        /// <param name = "path">The path to the Internet resource.</param>
        /// <param name = "extraValue">A query string or fragment identifier.</param>
        /// <param name="queryStrings">The query strings.</param>
        /// <exception cref = "T:System.ArgumentException">
        ///   <paramref name = "extraValue" /> is neither null nor <see cref = "F:System.String.Empty" />, nor does a valid fragment identifier begin with a number sign (#), nor a valid query string begin with a question mark (?). </exception>
        /// <exception cref = "T:System.ArgumentOutOfRangeException">
        ///   <paramref name = "port" /> is less than 0. </exception>
        public UriBuilderEx(string scheme, string host, int port, string path, string extraValue, Dictionary<string,string> queryStrings)
            : base(scheme, host, port, path, extraValue)
        {
            _extraValue = extraValue;
            _queryString = queryStrings;
        }
        #endregion

        #region Public methods

        /// <summary>
        ///   Returns the display string for the specified <see cref = "T:System.UriBuilder" /> instance.
        /// </summary>
        /// <returns>
        ///   The string that contains the unescaped display string of the <see cref = "T:System.UriBuilder" />.
        /// </returns>
        /// <PermissionSet>
        ///   <IPermission class = "System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version = "1" Flags = "UnmanagedCode, ControlEvidence" />
        /// </PermissionSet>
        public new string ToString()
        {
            GetQueryString();

            string result = base.Uri.AbsoluteUri;
            return result;
        }

        #endregion

        #region Private methods

        //Invoked by a couple of 
        private void PopulateQueryString()
        {
            string query = base.Query;

            if (query == string.Empty /*|| query == null*/)
            {
                return;
            }

            if (_queryString == null)
            {
                _queryString = new Dictionary<string,string>();
            }

            _queryString.Clear();
            int pos = query.LastIndexOf("?");
            query = query.Substring(pos); //remove the ?

            string[] pairs = query.Split(new[] {'&'});
            foreach (string s in pairs)
            {
                string[] pair = s.Split(new[] {'='});

                _queryString[pair[0]] = pair[1];
            }
        }

        private void GetQueryString()
        {
            int count = _queryString.Count;

            if (count == 0)
            {
                base.Query = string.Empty;
                return;
            }

            string[] keys = new string[count];
            string[] values = new string[count];
            string[] pairs = new string[count];

            _queryString.Keys.CopyTo(keys, 0);
            _queryString.Values.CopyTo(values, 0);

            for (int i = 0; i < count; i++)
            {
                pairs[i] = string.Concat(keys[i], "=", values[i]);
            }


            base.Query = string.Join("&", pairs);
            
        }

        #endregion
    }


}


