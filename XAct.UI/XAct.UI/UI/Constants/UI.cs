﻿
// ReSharper disable CheckNamespace
namespace XAct.UI.Constants
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    public partial class UI
    {
        /// <summary>
        /// 
        /// </summary>
        public partial class EditorHints
        {
            /// <summary>
            /// Constant to indicate DateTimeOnly
            /// </summary>
            public static string UIDateOnly = "UIDateOnly";

        }
    }
}
