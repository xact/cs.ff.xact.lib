﻿namespace XAct.UI
{
    using System.Collections.Generic;

    /// <summary>
    ///   Collection of 
    ///   <see cref = "ISelectableItem" />
    /// </summary>
    public class SelectableItemList : List<ISelectableItem>
    {
    }
}