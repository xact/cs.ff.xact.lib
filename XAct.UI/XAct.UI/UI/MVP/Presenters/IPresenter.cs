﻿namespace XAct.UI.Mvp.Presenters
{
    /// <summary>
    ///   The base contract for MVP compliant Presenters.
    ///   <para>
    ///     A Presenter is a UI-Framework-agnostic encapsulation
    ///     of the UI Logic, separated from the View's Rendering logic.
    ///   </para>
    /// </summary>
    /// <typeparam name = "TController">The type of the Controller.</typeparam>
    public interface IPresenter<TController>
    {
        /// <summary>
        ///   Gets or sets the controller.
        /// </summary>
        /// <value>The controller.</value>
        /// <internal>
        ///   <para>
        ///     It's public get so that a Parent Control/Page
        ///     can set it 
        ///   </para>
        ///   <para>
        ///     It's non-typed, so that a Controller can be shared
        ///     across both an MVP
        ///   </para>
        /// </internal>
        TController Controller { set; get; }

        /// <summary>
        ///   Initialises the view.
        /// </summary>
        /// <param name = "view">The view.</param>
        void InitialiseView(object view);


        /// <summary>
        ///   Method for associated View to invoke 
        ///   within OnLoad (on GET only).
        /// </summary>
        void ViewLoading();

        /// <summary>
        ///   Method for associated View to invoke 
        ///   within OnLoad (on GET only).
        /// </summary>
        void ViewInitialised();


        /// <summary>
        ///   Method for associated View to invoke 
        ///   within OnLoad (on GET &amp; POST).
        /// </summary>
        void ViewLoaded();
    }
}