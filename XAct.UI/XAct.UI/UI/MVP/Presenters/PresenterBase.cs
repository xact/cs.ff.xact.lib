﻿namespace XAct.UI.Mvp.Presenters
{
    using System.Diagnostics;
    using XAct.UI.Mvp.Views;

    /// <summary>
    ///   Abstract base class implementing
    ///   <see cref = "IPresenter{TController}" />.
    ///   <para>
    ///     This is the base class for 
    ///     any UI prsenter, in order to 
    ///     automatically handle the injection of services, etc.
    ///     that are common to every view.
    ///   </para>
    /// </summary>
    /// <typeparam name = "TView">The Type of the Controller.</typeparam>
    /// <typeparam name = "TController">The Type of the Controller.</typeparam>
    public abstract class PresenterBase<TView, TController>
        : IPresenter<TController>
        where TView : IView
    {
        #region Properties

        private TController _Controller;


        private TView _View;


        /// <summary>
        ///   The View that this Presenter is associated with.
        /// </summary>
        public TView View
        {
            get { return _View; }
            set { _View = value; }
        }

        /// <summary>
        ///   Gets or sets the controller.
        /// </summary>
        /// <value>The controller.</value>
        public TController Controller
        {
            get { return _Controller; }
            set
            {
                Debug.Assert(Equals(_Controller, null), "Can't reset the Controller associated to the presenter.");
                _Controller = value;
            }
        }

        /// <summary>
        ///   Initialises the view.
        /// </summary>
        /// <param name = "view">The view.</param>
        public void InitialiseView(object view)
        {
            _View = (TView) view;
        }

        #endregion

        #region View EventHandlers

        /// <summary>
        ///   Handles the ViewLoading event of the View control.
        /// </summary>
        public void ViewLoading()
        {
        }


        /// <summary>
        ///   Handles the ViewInitialised event of the View control.
        /// </summary>
        public virtual void ViewInitialised()
        {
        }

        /// <summary>
        ///   Handles the ViewLoaded event of the View control.
        /// </summary>
        public virtual void ViewLoaded()
        {
        }

        #endregion
    }
}