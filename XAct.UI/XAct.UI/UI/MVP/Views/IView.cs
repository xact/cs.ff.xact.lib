﻿namespace XAct.UI.Mvp.Views
{
    using System;

    /// <summary>
    ///   The base contract for MVP compliant UI Views
    ///   <para>
    ///     Provides <see cref = "ViewLoading" />
    ///     event that parent MVP triads can inject a common 
    ///     Controller if required.
    ///   </para>
    /// </summary>
    public interface IView
    {
        /// <summary>
        ///   Occurs when View is loading 
        ///   (before it invokes the Presenter's
        ///   ViewIntialised and ViewLoaded)
        ///   <para>
        ///     Good time for a parent MVP Control to inject 
        ///     a common Controller in child MVP Controls.
        ///   </para>
        /// </summary>
        event EventHandler<EventArgs> ViewLoading;
    }
}