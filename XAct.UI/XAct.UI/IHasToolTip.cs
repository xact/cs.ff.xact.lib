namespace XAct
{
    /// <summary>
    /// Contract for a UIElement.
    /// </summary>
    public interface IHasToolTip : IHasToolTipReadOnly
    {
        /// <summary>
        /// Gets or sets the UI element's tooltip.
        /// </summary>
        /// <value>The tooltip.</value>
        new string Tooltip { get; set; }
    }
}