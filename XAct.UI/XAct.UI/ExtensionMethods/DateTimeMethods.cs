﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Globalization;

#endif

    /// <summary>
    ///   Extension Methods for the DateTime Type.
    /// </summary>
    public static class DateTimeMethods
    {
        private static string format;

        /// <summary>
        ///   Converts a DateTime to NZ Date/Time Format.
        /// </summary>
        /// <param name = "date">The date.</param>
        /// <returns></returns>
        public static string ToNZDateString(this DateTime date)
        {
            format = "dd/MM/yyyy";
            return date.ToString(format, CultureInfo.CurrentCulture);
        }

        /// <summary>
        ///   Parses a NZ date string into a DateTime struct.
        /// </summary>
        /// <param name = "text">The text.</param>
        /// <returns></returns>
        public static DateTime FromNZDateString(this string text)
        {
            DateTime result;
            DateTime.TryParseExact(text, format, null, DateTimeStyles.None, out result);
            return result;
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
