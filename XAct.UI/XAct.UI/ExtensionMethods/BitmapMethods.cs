﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Runtime.InteropServices;

#endif

    
    /// <summary>
    ///   Extension Methods for the Bitmap Type.
    /// </summary>
    public static class BitmapMethods
    {
        private static Image GetTransparentGif(this Bitmap srcBmp, Color transparentColor)
        {
            PixelFormat pixelFormat = srcBmp.PixelFormat;

            int w = srcBmp.Width;
            int h = srcBmp.Width;
            Bitmap destBmp = new Bitmap(w, h, PixelFormat.Format8bppIndexed);

            ColorPalette srcPallete = srcBmp.Palette;
            ColorPalette destPallet = destBmp.Palette;

            //Copy all of the entries from the old destPallet, removing any transparency.
            for (int i = 0; i < srcPallete.Entries.Length - 1; i++)
            {
                Color srcColor = srcPallete.Entries[i];
                int alpha = srcColor.Equals(transparentColor) ? 0 : 255;
                //Set the destPallet entry, setting the transparent color based on
                //whether it is the same as the transparent color argument.
                destPallet.Entries[i] = Color.FromArgb(alpha, srcColor);
            }

            //Re-insert the destPallet.
            destBmp.Palette = destPallet;
            //Transfer the bitmap data from the source image to the new image.
            TransferBytes(srcBmp, destBmp);

            //Return the new image.
            return destBmp;
        }


        private static void TransferBytes(this Bitmap srcBmp, Bitmap destBmp)
        {
            Rectangle rect = new Rectangle(0, 0, srcBmp.Width, srcBmp.Height);
            BitmapData srcData = srcBmp.LockBits(rect, ImageLockMode.ReadOnly, srcBmp.PixelFormat);
            BitmapData destData = destBmp.LockBits(rect, ImageLockMode.WriteOnly, destBmp.PixelFormat);

            try
            {
                for (int y = 0; y < srcBmp.Height - 1; y++)
                {
                    for (int x = 0; x < srcBmp.Width - 1; x++)
                    {
                        //Calculate the offsets of the next byte to read/write.
                        int srcOffset = (srcData.Stride*y) + x;
                        int destOffset = (destData.Stride*y) + x;

                        byte b = Marshal.ReadByte(srcData.Scan0, srcOffset);

                        Marshal.WriteByte(destData, destOffset, b);
                    } //End X
                } //End Y
                srcBmp.UnlockBits(srcData);
                destBmp.UnlockBits(destData);
            }
            catch (Exception)
            {
            }
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
