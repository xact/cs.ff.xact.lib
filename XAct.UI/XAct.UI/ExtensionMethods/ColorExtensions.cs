
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct
{
    using System;
    using System.Drawing;

#endif

    
    /// <summary>
    ///   Class of Static Helper functions 
    ///   for common Color operations.
    /// </summary>
    public static class ColorExtensions
    {
        /// <summary>
        ///   Converts a color string to a hex value string.
        ///   (eg: "Green" -> "#000800")
        /// </summary>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string ColorNameToHexString(this string colorName)
        {
            Color result;

            if (string.IsNullOrEmpty(colorName))
            {
                result = Color.Empty;
            }
            else
            {
                if (colorName[0] == '#')
                {
                    return colorName.ToLower();
                }
                try
                {
                    result = ColorTranslator.FromHtml(colorName);
                }
                catch
                {
                    result = Color.Empty;
                }
            }
            return ColorToHexString(result);
        }

        /// <summary>
        ///   Converts a Color to a hex value string
        ///   (eg: Color.Green -> "#000800")
        /// </summary>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string ColorToHexString(this Color color)
        {
            string r = Convert.ToString(color.R, 16);

            if (r.Length < 2)
            {
                r = "0" + r;
            }

            string g = Convert.ToString(color.G, 16);
            if (g.Length < 2)
            {
                g = "0" + g;
            }

            string b = Convert.ToString(color.B, 16);
            if (b.Length < 2)
            {
                b = "0" + b;
            }
            string str = "#" + r + g + b;
            return str;
        }
    }



#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif

