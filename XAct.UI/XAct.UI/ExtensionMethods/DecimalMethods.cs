﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct
{
    using System;
    using System.Globalization;

#endif

    
    /// <summary>
    ///   Extension Methods for the Decimal Type.
    /// </summary>
    public static class DecimalExtensions
    {
        /// <summary>
        ///   Converts the Decimal entity to a NZ Currency string.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <returns>The formatted string.</returns>
        public static string ToNZCurrency(this decimal number)
        {
            return number.ToCurrency("en-NZ");
        }

        /// <summary>
        ///   Converts a Decimal to a Currency String in the specified local.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "localizationCode">The localisation code.</param>
        /// <returns></returns>
        public static string ToCurrency(this decimal number, string localizationCode)
        {
            return number.ToString("C", CultureInfo.CreateSpecificCulture(localizationCode));
        }

        /// <summary>
        ///   Parses a NZ currency value string into a Decimal.
        /// </summary>
        /// <param name = "text">The text.</param>
        /// <returns></returns>
        public static decimal FromNZCurrency(this string text)
        {
            return text.FromCurrency("en-NZ");
        }

        /// <summary>
        ///   Parses a currency value string into a Decimal.
        /// </summary>
        /// <param name = "text">The text.</param>
        /// <param name = "localisationCode">The localisation code.</param>
        /// <returns></returns>
        public static decimal FromCurrency(this string text, string localisationCode)
        {
            decimal result;
            Decimal.TryParse(text, NumberStyles.Currency, new CultureInfo(localisationCode), out result);
            return result;
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
