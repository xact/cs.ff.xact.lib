﻿namespace XAct.Navigation
{


    /// <summary>
    /// 
    /// </summary>
    public class NavigationProgressEventArgs : NavigationEventArgs
    {
        /// <summary>
        ///         Gets the number of bytes that have been read.
        /// </summary>
        /// <value>The bytes read.</value>
        public long BytesRead { get; set; }

        /// <summary>
        /// Gets the maximum number of bytes.
        /// </summary>
        /// <value>The bytes read.</value>
        public long MaxBytes { get; set; }
    }
}
