﻿namespace XAct.Navigation
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class NavigationExceptionEventArgs:NavigationEventArgs,IHasExceptionReadOnly
    {
        /// <summary>
        /// Gets the exception.
        /// </summary>
        /// <value>The exception.</value>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationExceptionEventArgs"/> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public NavigationExceptionEventArgs(Exception exception)
        {
            Exception = exception;
        }
    }
}
