﻿namespace XAct.Navigation
{
    using System;


    /// <summary>
    /// 
    /// </summary>
    public class NavigationEventArgs : EventArgs
    {
        /// <summary>
        /// Gets a value indicating whether this instance is navigation initiator.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is navigation initiator; otherwise, <c>false</c>.
        /// </value>
        public bool IsNavigationInitiator { get; private set; }

        /// <summary>
        /// Gets the navigator that raised the event.
        /// </summary>
        /// <value>The navigator.</value>
        public object Navigator { get; private set; }

        /// <summary>
        /// Gets the URI.
        /// </summary>
        /// <value>The URI.</value>
        public Uri Uri { get; private set; }

        /// <summary>
        /// Gets the root node of the target page's content.
        /// </summary>
        /// <value>The content.</value>
        public object Content { get; private set; }

        //public WebResponse WebResponse { get; }
    }
}
