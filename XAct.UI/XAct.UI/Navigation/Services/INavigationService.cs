﻿namespace XAct.Navigation
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface INavigationService : IHasXActLibService
    {


        /// <summary>
        /// Occurs when a new navigation is requested.
        /// </summary>
        event EventHandler<NavigationEventArgs> Navigating;

        /// <summary>
        /// Occurs when an error occurs while navigating to the requested content.
        /// </summary>
        event EventHandler<NavigationExceptionEventArgs> NavigationFailed;

        /// <summary>
        /// Occurs periodically during a download to provide navigation progress information.
        /// </summary>
        event EventHandler<NavigationProgressEventArgs> NavigationProgress;

        /// <summary>
        /// Occurs when the System.Windows.Navigation.NavigationService.StopLoading()
        /// method is called, or when a new navigation is requested while a current navigation
        /// is in progress.
        /// </summary>
        event EventHandler<NavigationEventArgs> NavigationStopped;


        /// <summary>
        /// Occurs when the content that is being navigated to has been found, and is
        /// available from the System.Windows.Navigation.NavigationService.Content property,
        /// although it may not have completed loading.
        /// </summary>
        event EventHandler<NavigationEventArgs> Navigated;

        /// <summary>
        /// Occurs when content that was navigated to has been loaded, parsed, and has
        /// begun rendering.
        /// </summary>
        event EventHandler<NavigationEventArgs> LoadCompleted;


        /// <summary>
        /// Gets a value that indicates whether there is at least one entry in back navigation
        /// history.
        /// </summary>
        /// <value>
        /// true if there is at least one entry in back navigation history; otherwise,
        /// false.
        /// </value>
        bool CanGoBack { get; }


        /// <summary>
        /// Gets a value that indicates whether there is at least one entry in forward
        /// navigation history.
        /// </summary>
        /// <value>
        /// true if there is at least one entry in forward navigation history; otherwise,
        /// false.
        /// </value>
        bool CanGoForward { get; }


        /// <summary>
        /// Gets the uniform resource identifier (URI) of the content that was last navigated
        /// to.
        /// </summary>
        /// <value>
        /// A System.Uri for the content that was last navigated to, if navigated to
        /// by using a URI; otherwise, null.
        /// </value>
        Uri CurrentSource { get; }

        /// <summary>
        /// Gets or sets the uniform resource identifier (URI) of the current content,
        /// or the URI of new content that is currently being navigated to.
        /// </summary>
        /// <value>
        /// A System.Uri that contains the URI for the current content, or the content
        /// that is currently being navigated to.
        /// </value>
        Uri Source { get; set; }


        /// <summary>
        /// Navigates to the most recent entry in back navigation history, if there is
        /// one.
        /// </summary>
        void GoBack();

        /// <summary>
        /// Navigate to the most recent entry in forward navigation history, if there
        /// is one.
        /// </summary>
        void GoForward();


        /// <summary>
        /// Navigate asynchronously to content that is contained by an object
        /// (generally a relative <see cref="Uri"/>), and optionally pass
        /// an object that contains data to be used for processing during navigation.
        /// </summary>
        /// <param name="target">
        /// An object (generally a <see cref="Uri"/>)
        /// that contains the content to navigate to.
        /// </param>
        /// <param name="navigationState">State of the navigation.</param>
        /// <returns>
        /// true if a navigation is not canceled; otherwise, false.
        /// </returns>
        /// Parameters:
        /// root:
        /// navigationState:
        /// An object that contains data to be used for processing during navigation.
        /// Returns:
        bool Navigate(object target, object navigationState=null);


        /// <summary>
        /// Reloads the current content.
        /// </summary>
        void Refresh();



        /// <summary>
        /// Adds an <see cref="INavigationHistoryEntry"/> to back history.
        /// </summary>
        void Push(INavigationHistoryEntry state);


        /// <summary>
        /// Removes the most recent <see cref="INavigationHistoryEntry"/> from back history.
        /// <para>
        /// This can be useful in cases such as coming from a Login page, 
        /// and desiring to remove the means of going back to it.
        /// </para>
        /// </summary>
        /// <returns>
        /// The most recent System.Windows.Navigation.JournalEntry in back navigation
        /// history, if there is one.
        /// </returns>
        INavigationHistoryEntry Pop();

    }
}
