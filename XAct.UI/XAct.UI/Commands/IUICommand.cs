﻿
namespace XAct.UI
{
    /// <summary>
    /// Contract for a Command backing a <see cref="IUICommandElement"/>.
    /// </summary>
    public interface IUICommand
    {
        /// <summary>
        /// Executes this instance.
        /// </summary>
        void Execute();

        /// <summary>
        /// Gets a value indicating whether this instance can be executed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance can execute; otherwise, <c>false</c>.
        /// </value>
        bool CanExecute { get; }
    }
}
