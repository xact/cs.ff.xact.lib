﻿
namespace XAct.UI
{
    /// <summary>
    /// Contract for a hierarchical navigation element.
    /// </summary>
    public interface INavigationElement : IUICommandElement , IHasHierarchyCollectionReadOnly<INavigationElement>
    {

    }
}
