namespace XAct.UI
{
    /// <summary>
    /// Contract for a visible UI Command element.
    /// </summary>
    public interface IUICommandElement : IUICommand, IHasTitleReadOnly, IHasToolTipReadOnly, IHasFilter 
    {

    }
}