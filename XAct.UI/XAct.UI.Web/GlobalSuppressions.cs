// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "XAct.UI.Web.HttpHandlers")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "XAct.UI.Web.Constants")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "XAct.Clients")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "XAct.Caching")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "XAct.Resources")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Scope = "member", Target = "XAct.UI.Web.Compilation.BaseServerObjectExpressionBuilder.#EvaluateExpression(System.Object,System.Web.UI.BoundPropertyEntry,System.Object,System.Web.Compilation.ExpressionBuilderContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member", Target = "XAct.UI.Web.Compilation.BaseServerObjectExpressionBuilder.#EvaluateExpression(System.Object,System.Web.UI.BoundPropertyEntry,System.Object,System.Web.Compilation.ExpressionBuilderContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "XAct.UI.Web.Tools.ClientResourceTools.#ConvertFilePathToResourcePath(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "0#", Scope = "member", Target = "XAct.UI.Web.Tools.ClientResourceTools.#ConvertFilePathToResourcePath(System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "virtualPathOrResourceName", Scope = "member", Target = "XAct.UI.Web.Tools.ClientResourceTools.#GetWebResourceUrl(System.Web.UI.Control,System.String,System.String[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Dir", Scope = "member", Target = "XAct.UI.Web.Tools.ClientResourceTools+Constants.#ResourceDumpDir")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "XAct.UI.Web.Tools.ClientResourceTools+Constants")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "XAct")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "ParseExpression", Scope = "member", Target = "XAct.UI.Web.Compilation.ExternalResourceExpressionBuilder.#ParseExpression(System.String,System.Type,System.Web.Compilation.ExpressionBuilderContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "ExternalResourceExpressionBuilder", Scope = "member", Target = "XAct.UI.Web.Compilation.ExternalResourceExpressionBuilder.#ParseExpression(System.String,System.Type,System.Web.Compilation.ExpressionBuilderContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "GlobalExternalResourceProvider", Scope = "member", Target = "XAct.Resources.GlobalExternalResourceProvider.#GetObject(System.String,System.Globalization.CultureInfo)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "GetObject", Scope = "member", Target = "XAct.Resources.GlobalExternalResourceProvider.#GetObject(System.String,System.Globalization.CultureInfo)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "XAct.Resources.GlobalExternalResourceProvider+Constants")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "GlobalExternalResourceProvider", Scope = "member", Target = "XAct.Resources.GlobalExternalResourceProvider.#ResourceReader")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "getResourceReader", Scope = "member", Target = "XAct.Resources.GlobalExternalResourceProvider.#ResourceReader")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "classKey", Scope = "member", Target = "XAct.Resources.GlobalExternalResourceProvider.#.ctor(XAct.Diagnostics.ITracingService,System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "GlobalExternalResourceProvider", Scope = "member", Target = "XAct.Resources.GlobalExternalResourceProvider.#.ctor(XAct.Diagnostics.ITracingService,System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "CreateGlobalResourceProvider", Scope = "member", Target = "XAct.Resources.ExternalResourceProviderFactory.#CreateGlobalResourceProvider(System.String)")]
