﻿namespace XAct.UI.Web
{
    using System;
    using System.Web;

    /// <summary>
    /// A specialization of the <see cref="XmlSiteMapProvider"/>
    /// that raises an event that the listener can respond to.
    /// </summary>
    /// <remarks>
    /// <para>
    /// One way to do that would be something like the following:
    /// <code>
    /// <![CDATA[
    /// public partial class Site : System.Web.UI.MasterPage {
    ///   protected void Page_Load(object sender, EventArgs e) {
    ///     SecurityXmlSiteMapProvider siteMapProvider = this.SiteMapDataSource.Provider as SecurityXmlSiteMapProvider;
    ///     if (siteMapProvider != null) {
    ///       siteMapProvider.CheckAccessiblity += siteMapProvider_CheckAccessiblity;
    ///     }
    ///   }
    ///   void siteMapProvider_CheckAccessiblity(object sender, NodeAccessibleEventArgs e) {
    ///    //Invoke the app's AuthenticationService, or do this in the meantime:
    ///     e.IsAccessible = !e.Node.Url.ToLower().EndsWith("pageb.aspx");
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal><para>7/18/2011: Sky</para></internal>
    public class SecurityXmlSiteMapProvider : XmlSiteMapProvider
    {
        /// <summary>
        /// Event raised by the <see cref="IsAccessibleToUser"/> for
        /// each node processed.
        /// <para>
        /// A handler can set the <see cref="NodeAccessibleEventArgs.IsAccessible"/>
        /// to control access.
        /// </para>
        /// </summary>
        /// <internal><para>7/18/2011: Sky</para></internal>
        public event EventHandler<NodeAccessibleEventArgs> CheckAccessiblity;

        /// <summary>
        /// Retrieves a Boolean value indicating whether the specified 
        /// <see cref="T:System.Web.SiteMapNode"/> 
        /// object can be viewed by the user in the specified context
        /// where a handler can set the <see cref="NodeAccessibleEventArgs.IsAccessible"/>
        /// <para>
        /// This override raised the <see cref="CheckAccessiblity"/>
        /// event.
        /// </para>
        /// </summary>
        /// <param name="context">The <see cref="T:System.Web.HttpContext"/> that contains user information.</param>
        /// <param name="node">The <see cref="T:System.Web.SiteMapNode"/> that is requested by the user.</param>
        /// <returns>
        /// true if security trimming is enabled and <paramref name="node"/> can be viewed by the user or security trimming is not enabled; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="context"/> is null.
        /// - or -
        /// <paramref name="node"/> is null.
        /// </exception>
        /// <internal><para>7/18/2011: Sky</para></internal>
        public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
        {
            //If nobody listening, we assume it's good enough.
            if (CheckAccessiblity != null)
            {
                //Raise the event:
                NodeAccessibleEventArgs nodeAccEvtArgs = new NodeAccessibleEventArgs(node);
                CheckAccessiblity(this, nodeAccEvtArgs);
                //Return the response:
                return nodeAccEvtArgs.IsAccessible;
            }
            //http://efreedom.com/Question/1-2888345/XmlSiteMapProvider-Check-User-Specified-Role
            //Otherwise fall back to base functionality:
            return base.IsAccessibleToUser(context, node);
        }
    }

    /// <summary>
    /// Event args raised by the SiteMapProvider.
    /// </summary>
    public class NodeAccessibleEventArgs : EventArgs
    {
        /// <summary>
        /// The SiteMapNode currently being processed.
        /// </summary>
        /// <value>The node.</value>
        /// <internal><para>7/18/2011: Sky</para></internal>
        public SiteMapNode Node { get; private set; }

        /// <summary>
        /// Set this value according to whether the <see cref="Node"/>
        /// should be accessible or not.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is accessible; otherwise, <c>false</c>.
        /// </value>
        /// <internal><para>7/18/2011: Sky</para></internal>
        public bool IsAccessible { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NodeAccessibleEventArgs"/> class.
        /// </summary>
        /// <param name="siteMapNode">The site map node.</param>
        /// <internal><para>7/18/2011: Sky</para></internal>
        public NodeAccessibleEventArgs(SiteMapNode siteMapNode)
        {
            Node = siteMapNode;
        }
    }
}