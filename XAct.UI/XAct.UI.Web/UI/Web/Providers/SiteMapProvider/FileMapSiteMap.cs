﻿// ReSharper disable CheckNamespace
namespace XAct.UI.Web
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Web;
    using System.Web.Caching;
    using System.Xml;
    using XAct.Environment;
    using XAct.IO;

    /// <summary>
    /// A SiteMap provider that works off of the filesystem.
    /// </summary>
    /// <internal><para>7/31/2011: Sky</para></internal>
    public class FileSystemSiteMapProvider : StaticSiteMapProvider
    {
        #region Services

// ReSharper disable InconsistentNaming
        private readonly IFSIOService _fsIOService;
// ReSharper restore InconsistentNaming

        #endregion

        //Const
// ReSharper disable InconsistentNaming
#pragma warning disable 169
        private const string C_FILE_DEFAULTNAME = "default";
        private const string C_FILE_DESCRIPTION_ATTRIBUTE = "description";
        private const string C_FILE_IMAGEURL_ATTRIBUTE = "imageUrl";
        private const string C_FILE_NAME_ATTRIBUTE = "name";
        private const string C_FILE_TARGET_ATTRIBUTE = "target";
        private const string C_FILE_VISIBLE_ATTRIBUTE = "visible";
        private const string C_FOLDER_HIDDENMARKER_FILENAME = "_hidden_";
        private const string C_FOLDER_NAME_ATTRIBUTE = "name";
#pragma warning restore 169
// ReSharper restore InconsistentNaming

        /// <summary>
        /// 
        /// </summary>
        public const string C_SITEINFO_FILENAME = "_siteinfo.xml";


        // Fields
        private readonly char[] _listSeparators = new[] {',', ';'};
        private SiteMapNode _rootNode;

        private string[] _DefaultPageNameList;
        private string[] _excludeFilesList = new string[0];
        private string[] _excludeFoldersList = new string[0];
        private string[] _fileSearchPatternList;
        private readonly List<string> _folderPaths = new List<string>();
        private string[] _folderSearchPatternList;
        private CacheDependency _fsMonitor;
        private bool _leaveExtensionsOn;
        private static readonly object _lock = new object();
        private string[] _removeFileExtensionsList;

        private IEnvironmentService EnvironmentService
        {
            get { return DependencyResolver.Current.GetInstance<IEnvironmentService>(); }
        }


        //public bool SecurityTrimmingEnabled
        //{
        //    get
        //    {
        //        return this._securityTrimmingEnabled;
        //    }
        //    //set
        //    //{
        //    //    this._securityTrimmingEnabled = true;
        //    //}
        //}
        //private bool _securityTrimmingEnabled;

        #region Properties

        private string DefaultPageName
        {
            get { return this._defaultPageName; }
        }

        private string _defaultPageName = "Default.aspx;index.apsx;index.htm;index.html";

        private string ExcludeFilesPattern
        {
            get { return this._excludeFilesPattern; }
        }

        private string _excludeFilesPattern = "*.config;*.sitemap;*.lnk;";

        private string ExcludeFoldersPattern
        {
            get { return this._excludeFoldersPattern; }
        }

        private string _excludeFoldersPattern = "app_*;bin;masterpage*;*svn;hidden;";

        private string FileSearchPattern
        {
            get { return this._fileSearchPattern; }
        }

        private string _fileSearchPattern = "*.aspx;*.htm;*.txt;*.png;*.gif;*.jpg;";

        private string FolderSearchPattern
        {
            get { return this._folderSearchPattern; }
        }

        private string _folderSearchPattern = "*";

        private bool LeaveExtensionsOn
        {
            get { return this._leaveExtensionsOn; }
        }

        private string RemoveFileExtensions
        {
            get { return this._removeFileExtensions; }
        }

        private string _removeFileExtensions;

        /// <summary>
        /// Gets the root <see cref="T:System.Web.SiteMapNode"/> object of the site map data that the current provider represents.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The root <see cref="T:System.Web.SiteMapNode"/> of the current site map data provider. The default implementation performs security trimming on the returned node.
        /// </returns>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public override SiteMapNode RootNode
        {
            get { return this._BuildSiteMap(false); }
        }


        private string RootTitle
        {
            get { return this._rootTitle; }
        }

        private string _rootTitle = "Home";

        private string RootUrl
        {
            get { return this._rootUrl; }
        }

        private string _rootUrl = "~/";

        private bool UseDefaultPageAsFolderUrl
        {
            get { return this._useDefaultPageAsFolderUrl; }
        }

        private bool _useDefaultPageAsFolderUrl = true;

        #endregion

        #region Overrides of StaticSiteMapProvider

        /// <summary>
        /// When overridden in a derived class, loads the site map information from persistent storage and builds it in memory.
        /// </summary>
        /// <returns>
        /// The root <see cref="T:System.Web.SiteMapNode"/> of the site map navigation structure.
        /// </returns>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public override SiteMapNode BuildSiteMap()
        {
            var result = this._BuildSiteMap(false);
            return result;
        }


        /// <summary>
        /// Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// The name of the provider is null.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// The name of the provider has a length of zero.
        /// </exception>
        /// <exception cref="T:System.InvalidOperationException">
        /// An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.
        /// </exception>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            this.InitializePropertiesFromConfigAttributes(config);
        }

        /// <summary>
        /// When overridden in a derived class, retrieves the root node of all the nodes that are currently managed by the current provider.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Web.SiteMapNode"/> that represents the root node of the set of nodes that the current provider manages.
        /// </returns>
        /// <internal><para>7/31/2011: Sky</para></internal>
        protected override SiteMapNode GetRootNodeCore()
        {
            return this.RootNode;
        }

        /// <summary>
        /// Gets the root <see cref="T:System.Web.SiteMapProvider"/> object in the current provider hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// An <see cref="T:System.Web.SiteMapProvider"/> that is the top-level site map provider in the provider hierarchy that the current provider belongs to.
        /// </returns>
        /// <exception cref="T:System.Configuration.Provider.ProviderException">
        /// There is a circular reference to the current site map provider.
        /// </exception>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public override SiteMapProvider RootProvider
        {
            get
            {
                if (this.ParentProvider != null)
                {
                    return this.ParentProvider.RootProvider;
                }
                return this;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemSiteMapProvider"/> class.
        /// </summary>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public FileSystemSiteMapProvider()
            : this(
                DependencyResolver.Current.GetInstance<IEnvironmentService>(),
                DependencyResolver.Current.GetInstance<IFSIOService>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemSiteMapProvider"/> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="ioService">The io service.</param>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public FileSystemSiteMapProvider(IEnvironmentService environmentService, IFSIOService ioService)
        {
            _fsIOService = ioService;
        }

        #endregion

        /// <summary>
        /// _s the build site map.
        /// </summary>
        /// <param name="forceRebuild">if set to <c>true</c> [force rebuild].</param>
        /// <returns></returns>
        /// <internal><para>7/31/2011: Sky</para></internal>
        protected SiteMapNode _BuildSiteMap(bool forceRebuild)
        {
            lock (_lock)
            {
                if (forceRebuild)
                {
                    this._rootNode = null;
                }
                if ((this._rootNode == null) || this._fsMonitor.HasChanged)
                {
                    this.Clear();
                    string rootUrl = this.RootUrl;
                    string str6 = HttpContext.Current.Server.MapPath(rootUrl);
                    string key = str6;
                    string url = rootUrl;
                    string rootTitle = this.RootTitle;
                    if (this.UseDefaultPageAsFolderUrl)
                    {
                        foreach (string str7 in this._DefaultPageNameList)
                        {
                            if (_fsIOService.FileExistsAsync(Path.Combine(str6, str7)).WaitAndGetResult())
                            {
                                url = rootUrl + "/" + str7;
                                break;
                            }
                        }
                    }
                    this._rootNode = new SiteMapNode(this, key, url, rootTitle);
                    this.AddNode(this._rootNode);
                    this._folderPaths.Clear();
                    this._BuildSiteMapFromFileSystem(this._rootNode, rootUrl);
                    this._fsMonitor = new CacheDependency(this._folderPaths.ToArray());
                }
                return this._rootNode;
            }
        }

        #region Protected

        private void _BuildSiteMapFromFileSystem(SiteMapNode currentSiteMapNode, string folderUrl)
        {
            XmlDocument document;
            XmlNodeList list2;
            string str = HttpContext.Current.Server.MapPath(folderUrl);
            string path = Path.Combine(str, "_siteinfo.xml");
            if (!_fsIOService.FileExistsAsync(path).WaitAndGetResult())
            {
                document = null;
            }
            else
            {
                //Need to convert to XDoc / XElement to make it portable
                document = new XmlDocument();
                document.Load(path);
            }
            List<SiteMapNode> list = this.CreateFileNodes(folderUrl, str);
            if (document != null)
            {
                string str4;
                string str5;
                XmlElement element =
                    document.SelectSingleNode(string.Format("/siteinfo/files/file[@{0}='{1}']", "name", "default")) as
                    XmlElement;
                int num = 0;
                if (element != null)
                {
                    num = 1;

                    //Need to convert to XDoc / XElement to make it portable
                    string str3 = element.GetAttributeValue("description", string.Empty, true);
                    str4 = element.GetAttributeValue("target", string.Empty, true);
                    str5 = element.GetAttributeValue("imageUrl", string.Empty, true);
                    foreach (SiteMapNode node in list)
                    {
                        if (!string.IsNullOrEmpty(str3))
                        {
                            node["description"] = str3;
                        }
                        if (!string.IsNullOrEmpty(str4))
                        {
                            node["target"] = str4;
                        }
                        if (!string.IsNullOrEmpty(str5))
                        {
                            node["imageUrl"] = str5;
                        }
                    }
                }
                list2 = document.SelectNodes("/siteinfo/targets/target");

                if ((list2 != null) && (list2.Count > num))
                {
                    Dictionary<string, SiteMapNode> dictionary = new Dictionary<string, SiteMapNode>();
                    foreach (SiteMapNode node in list)
                    {
                        dictionary[node.Title] = node;
                    }
                    int index = 0;
                    foreach (XmlNode node2 in list2)
                    {
                        XmlElement element2 = node2 as XmlElement;

                        if (element2 == null)
                        {
                            continue;
                        }


                        SiteMapNode node3;
                        string strA = element2.GetAttributeValue("name", string.Empty, true);
                        if ((string.Compare(strA, "default", false) == 0) || !dictionary.TryGetValue(strA, out node3))
                        {
                            continue;
                        }

                        bool flag;
                        str4 = element2.GetAttributeValue("target", string.Empty, false);
                        str5 = element2.GetAttributeValue("imageUrl", string.Empty, true);
                        string str7 = element2.GetAttributeValue("visible", string.Empty, true);
                        if (!string.IsNullOrEmpty(str4))
                        {
                            node3["target"] = str4;
                        }
                        if (!string.IsNullOrEmpty(str5))
                        {
                            node3["imageUrl"] = str5;
                        }
                        if (!string.IsNullOrEmpty(str7) && bool.TryParse(str7, out flag))
                        {
                            list.Remove(node3);
                        }
                        else
                        {
                            list.Remove(node3);
                            list.Insert(index, node3);
                            index++;
                        }
                    }
                }
            }
            foreach (SiteMapNode node4 in list)
            {
                this.AddNode(node4, currentSiteMapNode);
            }
            List<DirectoryInfo> list3 = new List<DirectoryInfo>();
            DirectoryInfo info = new DirectoryInfo(str);
            foreach (string str8 in this._folderSearchPatternList)
            {
                list3.AddRange(info.GetDirectories(str8));
            }
            List<XXX> list4 = new List<XXX>();
            foreach (DirectoryInfo info2 in list3)
            {
                SiteMapNode node5;
                if (this.CreateFolderNode(folderUrl, info2.FullName, out node5))
                {
                    this._folderPaths.Add(info2.FullName);
                    list4.Add(new XXX(node5, folderUrl + "/" + info2.Name));
                }
            }
            if (document != null)
            {
                list2 = document.SelectNodes("/siteinfo/folders/folder");
                if (list2 != null)
                {
                    int num3 = 0;
                    for (int i = 0; i < list2.Count; i++)
                    {
                        XmlElement element3 = list2[i] as XmlElement;
                        if (element3 != null)
                        {
                            string str9 = element3.GetAttributeValue("name", string.Empty, true);
                            foreach (XXX xxx in list4)
                            {
                                if (xxx.SiteMapNode.Title == str9)
                                {
                                    list4.Remove(xxx);
                                    list4.Insert(num3, xxx);
                                    num3++;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            foreach (XXX xxx2 in list4)
            {
                this.AddNode(xxx2.SiteMapNode, currentSiteMapNode);
                this._BuildSiteMapFromFileSystem(xxx2.SiteMapNode, xxx2.NodeUrl);
            }
        }

        #endregion

        private bool CreateFileNode(string folderUrl, string fullPath, out SiteMapNode resultSiteMapNode)
        {
            string fileName = Path.GetFileName(fullPath);
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fullPath);
            resultSiteMapNode = null;
            //bool flag = false;
            if (string.Compare(fileName, "_siteinfo.xml", true) == 0)
            {
                return false;
            }
            if (this.UseDefaultPageAsFolderUrl)
            {
                if (this._DefaultPageNameList.Any(str3 => string.Compare(fileName, str3, true) == 0))
                {
                    return false;
                }
            }
            if (fileName.IsInRegexList(this._excludeFilesList))
            {
                return false;
            }

            string key = fullPath;
            string url = folderUrl + "/" + fileName;
            string title = fileName;

            foreach (string str7 in this._removeFileExtensionsList)
            {
                if (fileName.EndsWith(str7, StringComparison.CurrentCultureIgnoreCase))
                {
                    title = fileNameWithoutExtension;
                    break;
                }
            }

            title = title.Replace("_", " ");
            string description = string.Empty;
            resultSiteMapNode = new SiteMapNode(this, key, url, title, description);
            return true;
        }

        private List<SiteMapNode> CreateFileNodes(string folderUrl, string directoryPath)
        {
            List<SiteMapNode> list = new List<SiteMapNode>();

            foreach (string str in this._fileSearchPatternList)
            {
                foreach (string str2 in _fsIOService.GetDirectoryFileNamesAsync(directoryPath, str).WaitAndGetResult()) //Still safe to refer to ioService, as this is not a Service class.
                {
                    SiteMapNode node;
                    if (this.CreateFileNode(folderUrl, str2, out node))
                    {
                        list.Add(node);
                    }
                }
            }
            return list;
        }

        private bool CreateFolderNode(string parentFolderUrl, string folderPath, out SiteMapNode resultSiteMapNode)
        {
            string fileName = Path.GetFileName(folderPath);


            resultSiteMapNode = null;

            if (string.Compare(fileName, "bin", true) == 0)
            {
                return false;
            }

            if (fileName.StartsWith("app_"))
            {
                return false;
            }
            if (fileName.IsInRegexList(this._excludeFoldersList))
            {
                return false;
            }
            if (_fsIOService.FileExistsAsync(Path.Combine(folderPath, "_hidden_")).WaitAndGetResult())
            {
                return false;
            }
            string key = folderPath;
            string url = parentFolderUrl + "/" + fileName;
            string title = fileName.Replace("_", " ");
            string description = string.Empty;
            if (this.UseDefaultPageAsFolderUrl)
            {
                foreach (string str6 in this._DefaultPageNameList)
                {
                    if (_fsIOService.FileExistsAsync(Path.Combine(folderPath, str6)).WaitAndGetResult())
                    {
                        url = parentFolderUrl + "/" + fileName + "/" + str6;
                        break;
                    }
                }
            }
            resultSiteMapNode = new SiteMapNode(this, key, url, title, description);
            return true;
        }


        private void InitializePropertiesFromConfigAttributes(NameValueCollection config)
        {
            this.InitializePropertiesFromConfigAttributes_DBConnectionProperties(config);
            this.InitializePropertiesFromConfigAttributes_Behavior(config);
        }

        private void InitializePropertiesFromConfigAttributes_DBConnectionProperties(NameValueCollection config)
        {
        }

        private void InitializePropertiesFromConfigAttributes_Behavior(NameValueCollection config)
        {
            int num;
            this._rootUrl = config.InitializeParam("rootUrl", "~/");
            this._rootTitle = config.InitializeParam("rootTitle", "Home");
            this._useDefaultPageAsFolderUrl = config.InitializeParam("useDefaultPageAsFolderUrl", "true").ToBool();
            this._defaultPageName = config.InitializeParam("defaultPageName",
                                                           "Default.aspx;index.apsx;index.htm;index.html");
            this._DefaultPageNameList = this._defaultPageName.Trim().Split(this._listSeparators,
                                                                           StringSplitOptions.RemoveEmptyEntries);
            this._leaveExtensionsOn = config.InitializeParam("leaveExtensionsOn", "false").ToBool();
            this._removeFileExtensions = config.InitializeParam("removeFileExtensions", "aspx");
            this._removeFileExtensionsList = this._removeFileExtensions.Split(this._listSeparators,
                                                                              StringSplitOptions.RemoveEmptyEntries);
            this._fileSearchPattern = config.InitializeParam("fileSearchPattern", "").ToLower().Trim();
            this._fileSearchPatternList = this._fileSearchPattern.Split(this._listSeparators,
                                                                        StringSplitOptions.RemoveEmptyEntries);
            if (this._fileSearchPatternList.Length == 0)
            {
                this._fileSearchPatternList = new[] {"*"};
            }
            this._folderSearchPattern = config.InitializeParam("folderSearchPattern", "").ToLower().Trim();
            this._folderSearchPatternList = this._folderSearchPattern.ToLower().Split(this._listSeparators,
                                                                                      StringSplitOptions.
                                                                                          RemoveEmptyEntries);
            if (this._folderSearchPatternList.Length == 0)
            {
                this._folderSearchPatternList = new[] {"*"};
            }
            this._excludeFilesPattern = config.InitializeParam("excludeFilesPattern", "").Trim().ToLower();
            this._excludeFilesList = this._excludeFilesPattern.ToLower().Split(this._listSeparators,
                                                                               StringSplitOptions.RemoveEmptyEntries);
            for (num = 0; num < this._excludeFilesList.Length; num++)
            {
                this._excludeFilesList[num] = this._excludeFilesList[num].WildcardPatternToRegexPattern();
            }
            this._excludeFoldersPattern = config.InitializeParam("excludeFoldersPattern", "").Trim().ToLower();
            this._excludeFoldersList = this._excludeFoldersPattern.ToLower().Split(this._listSeparators,
                                                                                   StringSplitOptions.RemoveEmptyEntries);
            for (num = 0; num < this._excludeFoldersList.Length; num++)
            {
                this._excludeFoldersList[num] = this._excludeFoldersList[num].WildcardPatternToRegexPattern();
            }
        }


        //public string MapURL(string physicalPath)
        //{
        //    string oldValue = HttpContext.Current.Server.MapPath("~");
        //    return ("~" + physicalPath.Replace(oldValue, "").Replace(@"\", "/"));
        //}


        // Nested Types
        [StructLayout(LayoutKind.Sequential)]
        private struct XXX
        {
            public readonly SiteMapNode SiteMapNode;
            public readonly string NodeUrl;

            public XXX(SiteMapNode siteMapNode, string nodeUrl)
            {
                this.SiteMapNode = siteMapNode;
                this.NodeUrl = nodeUrl;
            }
        }
    }
}