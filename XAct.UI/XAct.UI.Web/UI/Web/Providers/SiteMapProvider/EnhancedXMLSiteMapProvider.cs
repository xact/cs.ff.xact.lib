﻿namespace XAct.UI.Web
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Reflection;
    using System.Web;
    using System.Web.Caching;
    using System.Xml.Linq;

    /// <summary>
    /// Provides an enhanced XML based sitemap provider, allows you to have new functionality,
    /// which allows you to add either a single node, or a collection of nodes from an external source.
    /// 
    /// The external source is a static method which belongs to a class, this will either return a single node,
    /// or a collection of nodes. You will then map this in the XML using either the dynamicNode, or dynamicNodes 
    /// elements, setting the type, and the name of the static method that is called to return the data.
    /// </summary>
    /// <internal>
    /// <code>
    /// <![CDATA[
    /// Src:http://weblogs.asp.net/stefansedich/archive/2008/08/14/enhanced-xmlsitemapprovider-with-support-for-dynamic-nodes.aspx
    /// ]]>
    /// </code>
    /// </internal>
    public class EnhancedXMLSiteMapProvider : StaticSiteMapProvider
    {
        #region Private

        private SiteMapNode root;
        private readonly XNamespace ns = "http://schemas.microsoft.com/AspNet/SiteMap-File-1.0";
        private const string rootName = "siteMap";
        private const string nodeName = "siteMapNode";
        private const string dynamicNodeName = "dynamicNode";
        private const string dynamicNodesName = "dynamicNodes";
        private int cacheDuration = 5;
        private string siteMapFile = string.Empty;
        private string cacheKey = "6D5A8D5B-D619-4937-8693-AA75578FA922";
        private readonly object padlock = new object();

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current root, otherwise calls the BuildSiteMap method.
        /// </summary>
        /// <returns></returns>
        protected override SiteMapNode GetRootNodeCore()
        {
            var result = this.BuildSiteMap();
            return result;
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Initializes our custom provider, gets the attributes that are set in the config
        /// that enable us to customise the behaiviour of this provider.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="attributes"></param>
        public override void Initialize(string name, NameValueCollection attributes)
        {
            name.ValidateIsNotNullOrEmpty("name");
            attributes.ValidateIsNotDefault("attributes");

            base.Initialize(name, attributes);

            // Get the siteMapFile from the passed in attributes.
            this.siteMapFile = attributes["siteMapFile"];

            // If a cacheDuration was passed in set it, otherwise
            // it will default to 5 minutes.
            if (!string.IsNullOrEmpty(attributes["cacheDuration"]))
            {
                this.cacheDuration = int.Parse(attributes["cacheDuration"]);
            }

            // If a cache key was set in config, set it. 
            // Otherwise it will use the default which is a GUID.
            if (!string.IsNullOrEmpty(attributes["cacheKey"]))
            {
                this.cacheKey = attributes["cacheKey"];
            }
        }

        #endregion

        #region Sitemap Building/XML Parsing

        /// <summary>
        /// Builds the sitemap, firstly reads in the XML file, and grabs the outer root element and 
        /// maps this to become our main out root SiteMap node.
        /// </summary>
        /// <returns>The root SiteMapNode.</returns>
        public override SiteMapNode BuildSiteMap()
        {
            if (root != null && HttpContext.Current.Cache[this.cacheKey] != null)
            {
                // If sitemap already loaded and our cache key is still set,
                // checking a cache item enables us to invalidate the sitemap
                // after a given time period.
                return root;
            }

            lock (this.padlock)
            {
                XDocument siteMapXML = null;

                // Clear the current sitemap.
                this.Clear();

                try
                {
                    // Load the XML document.
                    siteMapXML = XDocument.Load(HttpContext.Current.Server.MapPath(this.siteMapFile));

                    // Get the root siteMapNode element, and map this to a .NET SiteMapNode,
                    // this becomes our root node.
                    XElement rootElement = siteMapXML.Element(ns + rootName).Element(ns + nodeName);
                    this.root = this.GetSiteMapNodeFromXMLElement(rootElement);

                    // Process our XML file, passing in the main root sitemap node and xml element.
                    this.ProcessXMLNodes(this.root, rootElement);

                    // Add our main root node.
                    AddNode(root);

                    // Create a cache item, this is used for the sole purpose of being able to invalidate our sitemap
                    // after a given time period, it also adds a dependancy on the sitemap file,
                    // so that once changed it will refresh your sitemap, unfortunately at this stage
                    // there is no dependancy for dynamic data, this could be implemented by clearing the cache item,
                    // by setting a custom cacheKey, then use this in your administration console for example to
                    // clear the cache item when the structure requires refreshing.
                    HttpContext.Current.Cache.Insert(this.cacheKey,
                                                     "",
                                                     new CacheDependency(
                                                         HttpContext.Current.Server.MapPath(this.siteMapFile)),
                                                     DateTime.Now.AddMinutes(this.cacheDuration),
                                                     Cache.NoSlidingExpiration
                        );
                }
                catch (Exception ex)
                {
                    // If there was ANY error loading or parsing the sitemap XML file, throw an exception.
                    throw new Exception("Error parsing SiteMap XML.", ex);
                }
                finally
                {
                    siteMapXML = null;
                }
            }

            // Finally return our root SiteMapNode.
            return root;
        }

        /// <summary>
        /// Recursively processes our XML document, parsing our siteMapNodes and dynamicNode(s).
        /// </summary>
        /// <param name="rootNode">The main root sitemap node.</param>
        /// <param name="rootElement">The main root XML element.</param>
        protected void ProcessXMLNodes(SiteMapNode rootNode, XElement rootElement)
        {
            SiteMapNode childNode = rootNode;

            // Loop through each element below the current root element.
            foreach (XElement node in rootElement.Elements())
            {
                if (node.Name == ns + nodeName)
                {
                    // If this is a normal siteMapNode then map the xml element
                    // to a SiteMapNode, and add the node to the current root.
                    childNode = this.GetSiteMapNodeFromXMLElement(node);
                    AddNode(childNode, rootNode);
                }
                else if (node.Name == ns + dynamicNodeName)
                {
                    // If this is a dynamic node, call the static method which is specified in the XML,
                    // then map this return to a .NET SiteMapNode, this will become our current child node.
                    EnhancedSiteMapNode dynNode =
                        this.GetDynamicData<EnhancedSiteMapNode>(GetAttributeValue(node.Attribute("type")),
                                                                 GetAttributeValue(node.Attribute("method")));
                    childNode = this.GetSiteMapNodeFromEnhancedSiteMapNode(dynNode);
                    AddNode(childNode, rootNode);

                    foreach (EnhancedSiteMapNode n in dynNode.ChildNodes)
                    {
                        // Start recursively mapping and adding any child nodes that this node contains.
                        CreateChildNodes(childNode, n);
                    }
                }
                else if (node.Name == ns + dynamicNodesName)
                {
                    // If the node is a dynamicNodes collection, use the static method specified in the XML
                    // to return the collection.
                    List<EnhancedSiteMapNode> dynamicNodes =
                        this.GetDynamicData<List<EnhancedSiteMapNode>>(GetAttributeValue(node.Attribute("type")),
                                                                       GetAttributeValue(node.Attribute("method")));

                    // For each item in the collections, recursively map the node and its children, appending to the
                    // current root node.
                    foreach (EnhancedSiteMapNode n in dynamicNodes)
                    {
                        CreateChildNodes(rootNode, n);
                    }
                }
                else
                {
                    // If the current node is not one of the known node types throw and exception
                    throw new Exception("An invalid element was found in the sitemap.");
                }

                // Continue recursively processing the XML file.
                ProcessXMLNodes(childNode, node);
            }
        }

        /// <summary>
        /// Given our custom enhanced sitemapnode object, recursively builds our sitemap,
        /// maps the custom objects to normal SiteMapNodes and builds the tree.
        /// </summary>
        /// <param name="rootNode">The root sitemap node.</param>
        /// <param name="rootElement">The root EnhancedSiteMapNode.</param>
        protected void CreateChildNodes(SiteMapNode rootNode, EnhancedSiteMapNode rootElement)
        {
            // Map our custom node to a .NET sitemapnode.
            SiteMapNode childNode = this.GetSiteMapNodeFromEnhancedSiteMapNode(rootElement);

            // Add the new child node to the root node.
            AddNode(childNode, rootNode);

            foreach (EnhancedSiteMapNode node in rootElement.ChildNodes)
            {
                // Recursively do for the current root elements children.
                CreateChildNodes(childNode, node);
            }
        }

        /// <summary>
        /// Clears the current sitemap.
        /// </summary>
        protected override void Clear()
        {
            this.root = null;
            base.Clear();
        }

        #endregion

        #region Mappers

        /// <summary>
        /// Maps an XMLElement from the XML file to a SiteMapNode.
        /// </summary>
        /// <param name="node">The element to map.</param>
        /// <returns>A SiteMapNode which represents the XMLElement.</returns>
        protected SiteMapNode GetSiteMapNodeFromXMLElement(XElement node)
        {
            // Get the URL attribute, need this so we can get the key.
            string url = GetAttributeValue(node.Attribute("url"));

            // Create a new sitemapnode, setting the key and url
            SiteMapNode smNode = new SiteMapNode(this, url)
                                     {
                                         Url = url
                                     };

            // Add each attribute to our attributes collection on the sitemapnode.
            foreach (XAttribute attribute in node.Attributes())
            {
                smNode[attribute.Name.ToString()] = attribute.Value;
            }

            // Set the other properties on the sitemapnode, 
            // these are for title and description, these come
            // from the nodes attrbutes are we populated all attributes
            // from the xml to the node.
            smNode.Title = smNode["title"];
            smNode.Description = smNode["description"];

            return smNode;
        }

        /// <summary>
        /// Maps our custom SiteMapNode to a normal .NET SiteMapNode.
        /// </summary>
        /// <param name="node">The node to map.</param>
        /// <returns>A SiteMapNode which represents our EnhancedSiteMapNode.</returns>
        protected SiteMapNode GetSiteMapNodeFromEnhancedSiteMapNode(EnhancedSiteMapNode node)
        {
            // Create a new .NET SiteMapNode, mapping across the main properties.
            SiteMapNode smNode = new SiteMapNode(this, node.Key)
                                     {
                                         Url = node.Url,
                                         Title = node.Title,
                                         Description = node.Description
                                     };

            // Add the custom attributes to the SiteMapNode.
            foreach (KeyValuePair<string, string> attribute in node.Attributes)
            {
                smNode[attribute.Key] = attribute.Value;
            }

            return smNode;
        }

        #endregion

        #region Dynamic Node/Nodes Adding

        /// <summary>
        /// Given a type and method name, will load that type
        /// and use reflection to call a static method which will 
        /// return custom data, the generic type will cast the return.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public T GetDynamicData<T>(string type, string method)
        {
            try
            {
                // Get the type.
                Type t = Type.GetType(type, true);

                // Find the method, and invoke it returning the data.
                MethodInfo methodInfo = t.GetMethod(method);
                return (T) methodInfo.Invoke(null, null);
            }
            catch (Exception ex)
            {
                // If there was an error calling the static method, to get the dynamic data,
                // create new exception.
                throw new Exception("Error calling static method to load dynamic data.", ex);
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Given an XAttribute, will either return an empty string if its value is
        /// null or the actual value.
        /// </summary>
        /// <param name="attribute">The attribe to get the value for.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public string GetAttributeValue(XAttribute attribute)
        {
            return attribute != null ? attribute.Value : string.Empty;
        }

        #endregion
    }


    /// <summary>
    /// Represents a custom SiteMapNode, used so that custom methods do not rely on system.web,
    /// and can return a node or collection of nodes to our sitemap provider, using our custom
    /// dynamicNode and dynamicNodes model.
    /// </summary>
    public class EnhancedSiteMapNode
    {
        #region Properties

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        /// <internal><para>7/19/2011: Sky</para></internal>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        /// <internal><para>7/19/2011: Sky</para></internal>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings")]
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        /// <internal><para>7/19/2011: Sky</para></internal>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        /// <internal><para>7/19/2011: Sky</para></internal>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the attributes.
        /// </summary>
        /// <value>The attributes.</value>
        /// <internal><para>7/19/2011: Sky</para></internal>
        public Dictionary<string, string> Attributes { get; set; }

        /// <summary>
        /// Gets or sets the child nodes.
        /// </summary>
        /// <value>The child nodes.</value>
        /// <internal><para>7/19/2011: Sky</para></internal>
        public List<EnhancedSiteMapNode> ChildNodes { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize, just init our collections.
        /// </summary>
        public EnhancedSiteMapNode()
        {
            this.ChildNodes = new List<EnhancedSiteMapNode>();
            this.Attributes = new Dictionary<string, string>();
        }

        #endregion
    }
}