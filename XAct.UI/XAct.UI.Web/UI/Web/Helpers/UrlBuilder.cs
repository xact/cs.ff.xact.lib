namespace XAct.UI.Web.Tools
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Text;
    using System.Web;
    using System.Web.UI;

    /// <summary>
    ///   TODO
    /// </summary>
    public class UrlBuilder : UriBuilder
    {
        private StringDictionary _queryString;

        #region Properties

        /// <summary>
        ///   Gets the Dictionary of QueryString key and values.
        /// </summary>
        /// <value>The query string Dictionary.</value>
        public StringDictionary QueryString
        {
            get
            {
                if (_queryString == null)
                {
                    _queryString = new StringDictionary();
                }

                return _queryString;
            }
        }

        /// <summary>
        ///   Gets or sets the name of the page.
        /// </summary>
        /// <value>The name of the page.</value>
        public string PageName
        {
            get
            {
                string path = base.Path;
                return path.Substring(path.LastIndexOf("/") + 1);
            }
            set
            {
                string path = base.Path;
                path = path.Substring(0, path.LastIndexOf("/"));
                base.Path = string.Concat(path, "/", value);
            }
        }

        #endregion

        #region Constructors overloads

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "UrlBuilder" /> class.
        /// </summary>
        public UrlBuilder()
        {
        }

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "UrlBuilder" /> class.
        ///   <para>
        ///     Eg: new UrlBuilder("http://xact-solutions.com")
        ///   </para>
        /// </summary>
        /// <param name = "uri">The URI.</param>
        public UrlBuilder(string uri)
            : base(uri)
        {
            PopulateQueryString();
        }

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "UrlBuilder" /> class.
        /// </summary>
        /// <param name = "uri">The URI.</param>
        public UrlBuilder(Uri uri)
            : base(uri)
        {
            PopulateQueryString();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UrlBuilder" /> class.
        ///   <para>
        ///     Eg: new UrlBuilder("http","xact-solutions.com")
        ///   </para>
        /// </summary>
        /// <param name = "schemeName">Name of the scheme.</param>
        /// <param name = "hostName">Name of the host.</param>
        public UrlBuilder(string schemeName, string hostName)
            : base(schemeName, hostName)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "UrlBuilder" /> class.
        ///   <para>
        ///     Eg: new UrlBuilder("http", "xact-solutions.com", "8080")
        ///   </para>
        /// </summary>
        /// <param name = "scheme">The scheme.</param>
        /// <param name = "host">The host.</param>
        /// <param name = "portNumber">The port number.</param>
        public UrlBuilder(string scheme, string host, int portNumber)
            : base(scheme, host, portNumber)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "UrlBuilder" /> class.
        ///   <para>
        ///     Eg: new UrlBuilder(
        ///     "http", "xact-solutions.com", "8080","default.aspx")
        ///   </para>
        /// </summary>
        /// <param name = "scheme">The scheme.</param>
        /// <param name = "host">The host.</param>
        /// <param name = "port">The port.</param>
        /// <param name = "pathValue">The path value.</param>
        public UrlBuilder(string scheme, string host, int port, string pathValue)
            : base(scheme, host, port, pathValue)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "UrlBuilder" /> class.
        ///   <para>
        ///     <![CDATA[
        /// Eg: new UrlBuilder("http", "xact-solutions.com", "8080", 
        ///                     "default.aspx", "x=1&y=2")
        /// ]]>
        ///   </para>
        /// </summary>
        /// <param name = "scheme">An Internet access protocol.</param>
        /// <param name = "host">A DNS-style domain name or IP address.</param>
        /// <param name = "port">An IP port number for the service.</param>
        /// <param name = "path">The path to the Internet resource.</param>
        /// <param name = "extraValue">A query string or fragment identifier.</param>
        /// <exception cref = "T:System.ArgumentException">
        ///   <paramref name = "extraValue" /> 
        ///   is neither null nor <see cref = "F:System.String.Empty" />, 
        ///   nor does a valid fragment identifier begin with a number sign (#), 
        ///   nor a valid query string begin with a question mark (?). 
        /// </exception>
        /// <exception cref = "T:System.ArgumentOutOfRangeException">
        ///   <paramref name = "port" /> is less than 0. </exception>
        public UrlBuilder(string scheme, string host, int port,
                          string path,
                          string extraValue)
            : base(scheme, host, port, path, extraValue)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "UrlBuilder" /> class.
        /// </summary>
        /// <param name = "page">The page.</param>
        public UrlBuilder(Page page)
            : base(page.Request.Url.AbsoluteUri)
        {
            PopulateQueryString();
        }

        #endregion

        #region Public methods

        /// <summary>
        ///   Returns the display string for the 
        ///   specified 
        ///   <see cref = "T:System.UriBuilder" /> instance.
        /// </summary>
        /// <returns>
        ///   The string that contains the unescaped 
        ///   display string of the 
        ///   <see cref = "T:System.UriBuilder" />.
        /// </returns>
        /// <PermissionSet>
        ///   <IPermission class = "System.Security.Permissions.SecurityPermission, mscorlib, 
        /// 	  Version=2.0.3600.0, Culture=neutral, 
        /// 	  PublicKeyToken=b77a5c561934e089" version = "1" Flags = "UnmanagedCode, ControlEvidence" />
        /// </PermissionSet>
        public new string ToString()
        {
            GetQueryString();

            return base.Uri.AbsoluteUri;
        }

        /// <summary>
        ///   Navigates to the Uri.
        /// </summary>
        public void Navigate()
        {
            _Navigate(true);
        }

        /// <summary>
        ///   Navigates to the uri, optionally ending response.
        /// </summary>
        /// <param name = "endResponse">if set to <c>true</c> [end response].</param>
        public void Navigate(bool endResponse)
        {
            _Navigate(endResponse);
        }

        private void _Navigate(bool endResponse)
        {
            string uri = ToString();

            HttpContext.Current.Response.Redirect(uri, endResponse);
        }

        #endregion

        #region Private methods

        /// <summary>
        ///   Convert QueryString dictionary into a string.
        ///   <para>
        ///     Note: does not prepend with '?'.
        ///   </para>
        /// </summary>
        private void PopulateQueryString()
        {
            string query = base.Query;

            if (query == string.Empty || query == null)
            {
                return;
            }

            if (_queryString == null)
            {
                _queryString = new StringDictionary();
            }

            _queryString.Clear();

            query = query.Substring(1); //remove the ?

            string[] pairs = query.Split(new[] {'&'});
            foreach (string s in pairs)
            {
                string[] pair = s.Split(new[] {'='});
                _queryString[pair[0]] = pair[1];
            }
        }

        private void GetQueryString()
        {
            int count = _queryString.Count;

            if (count == 0)
            {
                base.Query = string.Empty;
                return;
            }


            StringBuilder sb =
                new StringBuilder();

            foreach (DictionaryEntry entry in _queryString)
            {
                sb.Append("%");
                sb.Append(entry.Key);
                sb.Append("=");
                sb.Append(entry.Value);
            }
            //remove leading ampersand:
            sb.Remove(0, 1);


            base.Query = sb.ToString();
        }

        #endregion
    }


}


