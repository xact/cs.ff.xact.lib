using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace XAct.Web.Utils {


	/// <summary>
	/// Class of Static Helper functions for common Url manipulation functions.
	/// </summary>
	public static class UrlPathOLD {

















		/// <summary>
		/// Combines the specified basepath.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="basepath">The basepath.</param>
		/// <param name="relative">The relative.</param>
		/// <returns></returns>
		public static string Combine(string basepath, string relative) {
			return UrlPath.Combine(AppDomainAppVirtualPath, basepath, relative);
		}





		/// <summary>
		/// Combines the specified app path.
		/// </summary>
		/// <param name="appPath">The app path.</param>
		/// <param name="basepath">The basepath.</param>
		/// <param name="relative">The relative.</param>
		/// <returns></returns>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		public static string Combine(string appPath, string basepath, string relative) {
			string text1;
			if (string.IsNullOrEmpty(relative)) {
				throw new ArgumentNullException("relative");
			}
			if (string.IsNullOrEmpty(basepath)) {
				throw new ArgumentNullException("basepath");
			}
			UrlPath.CheckValidVirtualPath(relative);
			if (UrlPath.IsRooted(relative)) {
				text1 = relative;
			} else {
				if ((relative.Length == 1) && (relative[0] == '~')) {
					return appPath;
				}
				if (UrlPath.IsAppRelativePath(relative)) {
					if (appPath.Length > 1) {
						text1 = appPath + "/" + relative.Substring(2);
					} else {
						text1 = "/" + relative.Substring(2);
					}
				} else {
					text1 = UrlPath.SimpleCombine(basepath, relative);
				}
			}
			return UrlPath.Reduce(text1);
		}


		/// <summary>
		/// Simples the combine.
		/// </summary>
		/// <param name="basepath">The basepath.</param>
		/// <param name="relative">The relative.</param>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <returns></returns>
		public static string SimpleCombine(string basepath, string relative) {
			if ((basepath == null) || ((basepath.Length == 1) && (basepath[0] == '/'))) {
				basepath = string.Empty;
			}
			return (basepath + "/" + relative);
		}


		/// <summary>
		/// Makes the virtual path app absolute.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns></returns>
		public static string MakeVirtualPathAppAbsolute(string virtualPath) {
			return MakeVirtualPathAppAbsolute(virtualPath, AppDomainAppVirtualPath);
		}

		/// <summary>
		/// Makes the virtual path app absolute.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="virtualPath">The virtual path.</param>
		/// <param name="applicationPath">The application path.</param>
		/// <returns></returns>
		public static string MakeVirtualPathAppAbsolute(string virtualPath, string applicationPath) {
			if ((virtualPath.Length == 1) && (virtualPath[0] == '~')) {
				return applicationPath;
			}
			if (((virtualPath.Length >= 2) && (virtualPath[0] == '~')) && ((virtualPath[1] == '/') || (virtualPath[1] == '\\'))) {
				if (applicationPath.Length > 1) {
					return (applicationPath + "/" + virtualPath.Substring(2));
				}
				return ("/" + virtualPath.Substring(2));
			}
			if (!UrlPath.IsRooted(virtualPath)) {
				throw new ArgumentOutOfRangeException("virtualPath");
			}
			return virtualPath;
		}

		/// <summary>
		/// Makes the virtual path app absolute reduce and check.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns></returns>
		public static string MakeVirtualPathAppAbsoluteReduceAndCheck(string virtualPath) {
			if (virtualPath == null) {
				throw new ArgumentNullException("virtualPath");
			}
            string text1;
            text1 = virtualPath;
            text1 = UrlPath.MakeVirtualPathAppAbsolute(text1);
			text1 = UrlPath.Reduce(text1);

			if (!UrlPath.VirtualPathStartsWithAppPath(text1)) {
				throw new ArgumentException("Invalid_app_VirtualPath");
			}
			return text1;
		}

		/// <summary>
		/// Makes the virtual path app relative.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns></returns>
		public static string MakeVirtualPathAppRelative(string virtualPath) {
			return UrlPath.MakeVirtualPathAppRelative(virtualPath, AppDomainAppVirtualPath);
		}

		/// <summary>
		/// Makes the virtual path app relative.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="virtualPath">The virtual path.</param>
		/// <param name="applicationPath">The application path.</param>
		/// <returns></returns>
		public static string MakeVirtualPathAppRelative(string virtualPath, string applicationPath) {
			if (virtualPath == null) {
				throw new ArgumentNullException("virtualPath");
			}
			if (!UrlPath.VirtualPathStartsWithVirtualPath(virtualPath, applicationPath)) {
				return virtualPath;
			}
			if (virtualPath.Length == applicationPath.Length) {
				return "~";
			}
			if (applicationPath.Length == 1) {
				return ('~' + virtualPath);
			}
			return ('~' + virtualPath.Substring(applicationPath.Length));
		}



		/// <summary>
		/// Virtuals the path starts with app path.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns></returns>
		public static bool VirtualPathStartsWithAppPath(string virtualPath) {
			return UrlPath.VirtualPathStartsWithVirtualPath(virtualPath, AppDomainAppVirtualPath);
		}


		/// <summary>
		/// Virtuals the path starts with virtual path.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="virtualPath1">The virtual path1.</param>
		/// <param name="virtualPath2">The virtual path2.</param>
		/// <returns></returns>
		public static bool VirtualPathStartsWithVirtualPath(string virtualPath1, string virtualPath2) {
			if (virtualPath1 == null) {
				throw new ArgumentNullException("virtualPath1");
			}
			if (virtualPath2 == null) {
				throw new ArgumentNullException("virtualPath2");
			}
			if (!UrlPath.StringStartsWithIgnoreCase(virtualPath1, virtualPath2)) {
				return false;
			}
			if (virtualPath1.Length != virtualPath2.Length) {
				if (virtualPath2.Length == 1) {
					return true;
				}
				if (virtualPath1[virtualPath2.Length] != '/') {
					return false;
				}
			}
			return true;
		}









		/// <summary>
		/// Checks the valid virtual path.
		/// </summary>
		/// <internal>
		/// Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
		/// which was an internal class. Dumb!
		/// </internal>
		/// <param name="path">The path.</param>
		public static void CheckValidVirtualPath(string path) {
			if (UrlPath.IsAbsolutePhysicalPath(path)) {
				object[] objArray1 = new object[1] { path };
				throw new Exception("Physical_path_not_allowed");
			}
			if (path.IndexOf(':') >= 0) {
				object[] objArray2 = new object[1] { path };
				throw new Exception("Invalid_vpath");
			}
		}


		/// <summary>
		/// An extended version of string.StartsWith(x), that ignores case.
		/// </summary>
		/// <internal>
		/// Used by <see cref="VirtualPathStartsWithVirtualPath"/>
		/// </internal>
		/// <param name="s1">The s1.</param>
		/// <param name="s2">The s2.</param>
		/// <returns></returns>
		public static bool StringStartsWithIgnoreCase(string s1, string s2) {
            return s1.StartsWith(s2,System.StringComparison.InvariantCultureIgnoreCase);
		}
	}//Class:End
}//Namespace:End
