//Ref System.Design.dll

namespace XAct.UI.Web.Tools
{
    using System;
    using System.Collections;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.Design;

    /// <summary>
    ///   Class of static methods for manipulating 
    ///   absolute (http://....), relative (../res/images.gif), 
    ///   WebSiteRelative (/aspNetSite/pages/page.aspx) and
    ///   ApplicationRelative ("~/pages/page.aspx)
    ///   paths.
    /// </summary>
    public static class UrlPathTools
    {
        #region Protected Constants

        private const string C_SCHEMA_DIVIDER = "://";
        private const int C_SCHEMA_LENGTH = 10; //longest is 'resource:' ?

        /// <summary>
        ///   The Url prefix that makes it relative to root of website.
        /// </summary>
        public const char C_APP_RELATIVE_CHAR = '~';


        /// <summary>
        ///   The resource Prefix used to designate that the rest of the path 
        ///   is an embedded resource:
        /// </summary>
        private const string C_RESOURCE_PREFIX = "resource:";


        /// <summary>
        ///   The single char string Url prefix that makes it relative to root of website.
        /// </summary>
        public const string C_APP_RELATIVE_CHARACTER_STRING = "~";

        /// <summary>
        ///   ???
        /// </summary>
        public const string dummyProtocolAndServer = "file://foo";

        #endregion

        #region Static Properties

        private static string _AppDomainAppVirtualPath;

        /// <summary>
        ///   Returns AppDomain root url 
        ///   (ie: "/aspNetApp") with no slash at end.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Note that there is no slash at end.
        ///   </para>
        /// </remarks>
        public static string AppDomainAppVirtualPath
        {
            get
            {
                if (string.IsNullOrEmpty(_AppDomainAppVirtualPath))
                {
                    _AppDomainAppVirtualPath =
                        HttpRuntime.AppDomainAppVirtualPath;
                }
                return _AppDomainAppVirtualPath;
            }
        }

        /// <summary>
        ///   Returns AbsolutePath to root of Application 
        ///   (ie "C:\wwwroot/MyAspNet\") with slash on end.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Note that this has slash on end, 
        ///     unlike AppDomainAppVirtualPath
        ///   </para>
        /// </remarks>
        public static string AppDomainAbsolutePath
        {
            get
            {
                return HttpRuntime.AppDomainAppPath;
                //Same as:
                //System.AppDomain.CurrentDomain.BaseDirectory
                //System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase
                //also:
                //string appPhysicalPath = HttpContext.Current.Server.MapPath("~");
            }
        }

        #endregion

        #region Static Methods

        /// <summary>
        ///   Determines whether the path is an absolute url 
        ///   ('http://', 'file://', etc.) or not.
        ///   <para>
        ///     In other words, checks to see if the given webPath
        ///     has a schema in front of it or not.
        ///   </para>
        /// </summary>
        /// <param name = "webPath">The webPath to check.</param>
        /// <returns>
        ///   <c>true</c> if the path is an absolute URL, 
        ///   otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "webPath" /> 
        ///   is null (but not empty).
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool IsAbsoluteUrl(string webPath)
        {
            webPath.ValidateIsNotDefault(webPath);
            if (webPath == string.Empty)
            {
                return false;
            }
            if (webPath.Length < 8)
            {
                return false;
            } //"http://" ?!?
            if (webPath.Substring(0, 8).Contains(C_SCHEMA_DIVIDER))
            {
                return true;
            }
            return false;
        }


        /// <summary>
        ///   Determine whether passed Url 
        ///   is a relative url ('../res/image.gif')
        ///   and not an AppRooted url ('/res/images.gif'),
        ///   AbsoluteUrl ('http://site.com/res/images.gif')
        ///   or PhysicalPath ('d:\\....').
        ///   <para>
        /// 
        ///   </para>
        ///   <para>
        ///     Returns true if url is similar to:
        ///   </para>
        ///   <para>
        ///     <c>Image/BettyBoop.png</c>
        ///   </para>
        ///   <para>
        ///     <c>~/Image/BettyBoop.png</c>
        ///   </para>
        ///   <para>
        ///     <c>../Image/BettyBoop.png</c>
        ///   </para>
        ///   <para>
        ///     Returns false if path is similar to: 
        ///   </para>
        ///   <para>
        ///     <c>d:\websites\asite\Images\BettyBoop.png</c>
        ///   </para>
        ///   <para>
        ///     <c>\\somserver\d\websites\asite\Images\BettyBoop.png</c>
        ///   </para>
        ///   <para>
        ///     <c>http://asite/Images/BettyBoop.png</c>
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     if Path has a ":" in it, and 
        ///     starts with 'c', 'http', 'file', etc. it's rooted.
        ///   </para>
        ///   <para>
        ///     Returns false if string.Empty.
        ///   </para>
        /// </remarks>
        /// <param name = "url">The url to check.</param>
        /// <returns>
        ///   <c>true</c> 
        ///   if the path is relative.
        /// </returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "url" /> 
        ///   is null (but not empty).
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool IsRelativeUrl(string url)
        {
            url.ValidateIsNotDefault(url);
            if (url == string.Empty)
            {
                return false;
            }
            if (WebPathHasSchema(url))
            {
                return false;
            }
            if (IsWebSiteRelative(url))
            {
                return false;
            }
            if (IsAbsolutePhysicalPath(url))
            {
                return false;
            }

            //Finally!
            return true;
        }


        /// <summary>
        ///   Checks that the path is a virtual path 
        ///   (ie '<c>/site/dir/file.htm</c>',
        ///   'dir/file.htm', '~/dir/file/htm', etc.) 
        ///   (and not an absolute
        ///   physical path ('<c>d:\websites\sitea\dir\file.htm</c>') or 
        ///   absolute url ('http://....')
        /// </summary>
        /// <param name = "webPath">The web path.</param>
        /// <internal>
        ///   <para>
        ///     Invoked by <see cref = "Combine(string,string, string)" />.
        ///   </para>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool IsVirtualPath(string webPath)
        {
            if (IsAbsolutePhysicalPath(webPath))
            {
                //It's a an absolute path...
                //so it's not virtual:
                return false;
            }
            //And it must not have a schema:
            return (!WebPathHasSchema(webPath));

            /*
      if (webPath.IndexOf(':', 0, C_SCHEMA_LENGTH) > -1) {
        //It's got a ':' somewhere in it
        return false;
      }
      return true;
       */
        }


        /// <summary>
        ///   Determines whether the given path starts 
        ///   with '<c>~/</c>' or '<c>~\\</c>'.
        ///   <para>
        ///     The following will work:
        ///   </para>
        ///   <para>
        ///     <c>~/Image/BettyBoop.png</c>
        ///   </para>
        ///   <para>
        ///     <c>~\Image\BettyBoop.png</c>
        ///   </para>
        ///   The following will not:
        ///   <para>
        ///     <c>~Image/BettyBoop.png</c>
        ///   </para>
        ///   <para>
        ///     <c>/Image/BettyBoop.png</c>
        ///   </para>
        ///   <para>
        ///     <c>\\Image\\BettyBoop.png</c>
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   No error is raised if the path is a null.
        /// </remarks>
        /// <param name = "webPath">The url to check.</param>
        /// <returns>
        ///   <c>true</c> if [is app relative path] 
        ///   [the specified path]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the <paramref name = "webPath" /> 
        ///   is null (but not empty).
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool IsAppRelative(string webPath)
        {
            webPath.ValidateIsNotDefault(webPath);

            if (string.IsNullOrEmpty(webPath))
            {
                return false;
            }

            //Does it start with '~'?
            if (webPath[0] != C_APP_RELATIVE_CHAR)
            {
                return false;
            }

            //Is it longer than just '~', and followed by '\' or '/'?
            if ((webPath.Length != 1) && (webPath[1] != '/'))
            {
                return (webPath[1] == '\\');
            }
            //It's ok for it to be just "~"
            return true;
        }


        /// <summary>
        ///   Determine if Relative Url starts with '<c>/</c>'
        ///   and therefore is relative to the root of the
        ///   website.
        ///   <para>
        ///     Eg: "/Images/BettyBoop.png"
        ///   </para>
        /// </summary>
        /// <para>
        ///   Returns <c>false</c> if <c>string.Empty</c>.
        /// </para>
        /// <param name = "webPath">The url to check.</param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the <paramref name = "webPath" /> 
        ///   is null (but not empty).
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool IsWebSiteRelative(string webPath)
        {
            webPath.ValidateIsNotDefault(webPath);
            if (webPath == string.Empty)
            {
                return false;
            }
            return (webPath[0] == '/');
        }


        /// <summary>
        ///   Determines if path given is to a file on hard drive.
        ///   <para>
        ///     Returns <c>true</c> if path starts with 
        ///     '<c>D:\</c>', '<c>\\</c>', or '<c>d:/</c>'.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Returns false if string.Empty.
        ///   </para>
        ///   <para>
        ///     Fails if 'http:\\', 'd:\sub\'
        ///   </para>
        /// </remarks>
        /// <param name = "path">The path to check</param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the <paramref name = "path" /> 
        ///   is null (but not empty).
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool IsAbsolutePhysicalPath(string path)
        {
            path.ValidateIsNotDefault(path);

            if (path == string.Empty)
            {
                return false;
            }

            //If not long enough to ever be Physical path, return false:
            if (path.Length < 3)
            {
                return false;
            }

            //if Path is either format "D:\" or "\\" or "d:/":
            if ((path[1] == ':') && IsDirectorySeparatorChar(path[2], true))
            {
                //as long as next char is not "\"

                if (path.Length == 3)
                {
                    return true;
                }
                return ((path[3] != '\\') && (path[3] != '/'));
            }

            if (path.StartsWith(@"\\"))
            {
                //yeah....but it has to be the name of a machine...
                //but not sure what is min length of a name.
                //let's make it 3 chars minimum.
                return ((path.Length >= 5) && (path[2] != '\\'));
            }
            return false;
        }


        /// <summary>
        ///   Determine if char given is '/', or '\\' character.
        /// </summary>
        /// <param name = "sepChar">The character to test.</param>
        /// <returns>True if the char was one of the slashes.</returns>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool IsDirectorySeparatorChar(char sepChar)
        {
            return IsDirectorySeparatorChar(sepChar, true);
        }

        /// <summary>
        ///   Determine if char given is '/', or (optionally) '\\' character.
        /// </summary>
        /// <param name = "sepChar"></param>
        /// <param name = "eitherDirection"></param>
        /// <returns>True if the char was a slash.</returns>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool IsDirectorySeparatorChar(
            char sepChar, bool eitherDirection)
        {
            if (sepChar == '/')
            {
                return true;
            }
            if ((eitherDirection) && (sepChar == '\\'))
            {
                return true;
            }
            return false;
        }


        /// <summary>
        ///   Converts an AppRelativeUrl ("~/images/go.gif")
        ///   to an Absolute Physical Path
        ///   ("c:\inetpub\wwwroot\appfolder\images\go.gif")
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     An advantage this static method has over
        ///     Server.MapPath is that it won't 
        ///     raise an Exception if the path is already a rooted path.
        ///   </para>
        /// </remarks>
        /// <param name = "control"></param>
        /// <param name = "virtualWebPath"></param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "virtualWebPath" /> 
        ///   is null (but not empty).
        /// </exception>
        /// <intenral>
        /// 
        /// 
        /// 
        ///   @@@
        /// 
        /// 
        /// 
        /// 
        /// </intenral>
        public static string MapPath(Control control, string virtualWebPath)
        {
            virtualWebPath.ValidateIsNotDefault("virtualWebPath");

            if (virtualWebPath == string.Empty)
            {
                return string.Empty;
            }

            //If already rooted, don't try to do it again:
            if (IsAbsolutePhysicalPath(virtualWebPath))
            {
                return virtualWebPath;
            }

            HttpContext hc = HttpContext.Current;
            if (hc != null)
            {
                //returns Physical path corresponding to given virtualWebPath:
                return hc.Server.MapPath(virtualWebPath);
            }
            else
            {
                //No context means we have to sweat a bit...

                IWebApplication vWA =
                    (IWebApplication) control.Site.GetService(typeof (IWebApplication));

                if (vWA != null)
                {
                    return vWA.GetProjectItemFromUrl(virtualWebPath).PhysicalPath;
                }
                else
                {
                    throw new Exception(
                        "Error Mapping AppRelativeUrl to Path while in DesignMode.");
                }
            }
        }


        /// <summary>
        ///   Convert a Rooted Path to an App Relative Url (ie: Reverse of MapPath)
        ///   <para>
        ///     IE: Convert "c:
        ///     <c>"c:\inetpub\wwwroot\appfolder\images\go.gif"</c>
        ///     to 
        ///     <c>"~/images/go.gif"</c>. 
        ///   </para>
        /// </summary>
        /// <param name = "control"></param>
        /// <param name = "rootedPath"></param>
        /// <returns></returns>
        /// @@@
        public static string MapUrl(Control control, string rootedPath)
        {
            //Clean arguments:
            if (rootedPath == null)
            {
                rootedPath = string.Empty;
            }

            if (rootedPath == string.Empty)
            {
                return rootedPath;
            }

            //rootedPath could be rooted, or 
            if (IsAbsolutePhysicalPath(rootedPath))
            {
                string appRootDir;

                HttpContext hc = HttpContext.Current;
                if (hc != null)
                {
                    appRootDir = hc.Request.PhysicalApplicationPath.TrimEnd('\\');
                }
                else
                {
                    IWebApplication vWA =
                        (IWebApplication)
                        control.Site.GetService(typeof (IWebApplication));

                    if (vWA != null)
                    {
                        appRootDir =
                            vWA.RootProjectItem.
                                PhysicalPath.TrimEnd('\\');
                        //ie //C:\inetpub\wwwroot\appfolder\
                    }
                    else
                    {
                        throw new Exception(
                            "Error Mapping Path to AppRelativeUrl.");
                    }
                }

                //See if the rootedPath is long enough 
                //to even contain an appRootDir:
                if (rootedPath.Length >= appRootDir.Length)
                {
                    //Seems atleast long enough.
                    //So get the left part of the rootedPath 
                    //that is as long as the appRootDir:
                    //Compare it (case insensitive):
                    if (string.Compare(
                        rootedPath.Substring(0, appRootDir.Length),
                        appRootDir, true) == 0)
                    {
                        //Starts with the appRootDir!
                        //So take it off and return:
                        return ("~/" +
                                rootedPath.Substring(appRootDir.Length).Replace("\\", "/"));
                    }
                }
            }
            else if (IsRelativeUrl(rootedPath))
            {
                //already a relativeUrl?
            }
            else
            {
                //already a relativeUrl?
            }

            //Leave it alone...
            return rootedPath;
        }


        /// <summary>
        ///   Ensures that all Windows backslashes ('\\'). are converted to 
        ///   url forward slashes ('/'). 
        ///   <para>
        ///     In addition, removes all duplicate slashes.
        ///   </para>
        ///   <para>
        ///     If <c>ensureEndsWithSlashes</c> is set to true (as is often
        ///     the case when the given path is for a directory, and not a file
        ///     within a directory), ensures the path is returned with a final slash
        ///     (although not if the original path was string.Empty).
        ///   </para>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name = "path">The path to normalize slashes.</param>
        /// <param name = "ensureEndsWithSlashes">
        ///   Ensure that (if path is for a dir) it 
        ///   terminates with a slash.
        /// </param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the <paramref name = "path" /> 
        ///   argument is null.
        /// </exception>
        /// <exception cref = "System.ArgumentException">
        ///   An exception is raised if <paramref name = "path" /> 
        ///   is an AbsolutePhysicalPath.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Invoked by <see cref = "Reduce" />.
        ///   </para>
        /// 
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string NormalizeSlashes(
            string path, bool ensureEndsWithSlashes)
        {
            path.ValidateIsNotDefault("path");

            //Clean it up first:
            path = path.Trim();

            //Nothing to process? Get out early:
            if (path == string.Empty)
            {
                return path;
            }

            if (IsAbsolutePhysicalPath(path))
            {
                //path starts with "d:\" or "\\", so 
                //should not have things flipped around.
                throw new ArgumentException(
                    "path ('{0}') is an AbsolutePhysicalPath.",
                    path
                    );
            }

            path = path.Replace('\\', '/');

            string schema;
            int pos = path.IndexOf(C_SCHEMA_DIVIDER, 0, C_SCHEMA_LENGTH);
            if (pos == -1)
            {
                schema = null;
            }
            else
            {
                //http://rottenapples.com
                //0123456
                //    4
                schema = path.Substring(0, pos + 3);
                path = path.Substring(pos + 3);
            }

            while (true)
            {
                //remove duplicates:
                //But because replace does it only once, have to loop until no more 
                //found:
                string newPath = path.Replace("//", "/");
                if (newPath.Length == path.Length)
                {
                    //No changes this time, so we can get out:
                    break;
                }
                path = newPath;
            }

            //If last char is not '/', make it so:
            if ((ensureEndsWithSlashes) && (!path.EndsWith("/")))
            {
                path += "/";
            }

            return (schema == null) ? path : schema + path;

            //path = System.Web.VirtualPathUtility.MakeRelative(
            //path, 
            //page.Request.Url.AbsolutePath);
        }


        /// <summary>
        ///   Returns the absolute/rooted path.
        ///   <para>
        ///     Accepts both absolute and relative paths.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   If the path is absolute and rooted, the path is returned
        ///   as is, without change. On the other hand, if the page is
        ///   not absolute, and is relative, the path is Mapped relative
        ///   to the current page:
        /// </remarks>
        /// <param name = "page"></param>
        /// <param name = "path"></param>
        /// <returns></returns>
        /// <internal>
        /// 
        /// 
        /// 
        ///   <para>
        ///     NOT TESTED.
        ///   </para>
        /// 
        /// 
        /// 
        /// </internal>
        public static string GetPathAbsolute(string path, Page page)
        {
            //If we are not in an html context (designer?)
            //return the path, unmodified:
            if (HttpContext.Current == null)
            {
                return path;
            }

            //Otherwise, if absolute, return the path, unmodified,
            //and if not rooted, then MapPath to current page:
            return ((VirtualPathUtility.IsAbsolute(path))
                        ? path
                        : MapPath(page, path));
        }


        /// <summary>
        ///   TODO: NOT CORRECT @@@
        /// </summary>
        /// <param name = "page"></param>
        /// <param name = "path"></param>
        /// <returns></returns>
        /// <internal>
        /// 
        /// 
        /// 
        ///   <para>
        ///     NOT TESTED.
        ///   </para>
        /// 
        /// 
        /// 
        /// </internal>
        public static string GetPathRelative(string path, Page page)
        {
            //No context?
            //As in ...IDE/Designer?
            //Then all I can do is return the path:
            if (HttpContext.Current == null)
            {
                return path;
            }

            //Is it absolute?
            //If so...make it relative to the application
            return (((VirtualPathUtility.IsAbsolute(path))
                         ? VirtualPathUtility.ToAppRelative(path)
                         : path));
        }


        /// <summary>
        ///   Strips out all the '..' and '.', reducing the path
        ///   to its most compact and direct format.
        ///   <para>
        ///     Eg: converts 
        ///     'http://site.com/subdir/..//.///resources/img.gif'
        ///     to 
        ///     'http://site.com/subdir/resources/img.gif'
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Can now handle paths with schemas.
        ///   </para>
        /// </remarks>
        /// <param name = "path"></param>
        /// <returns></returns>
        /// <internal>
        /// 
        ///   <para>
        ///     This looks like it came via Reflector...Can't remember.
        ///   </para>
        /// 
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string Reduce(string path)
        {
            string result = null;

            if (path != null)
            {
                int indexOfParamsDiv = path.IndexOf('?');
                if (indexOfParamsDiv >= 0)
                {
                    //result become args:
                    result = path.Substring(indexOfParamsDiv);
                    //and we'll truncate the incoming
                    //so we concentrate on that up to there...
                    path = path.Substring(0, indexOfParamsDiv);
                }
            }

            //This originally could only handle virtual
            //paths but I've messed around with it so
            //that it can handle paths that have schemas
            //at the front.
            path = NormalizeSlashes(path, false);

            int num2 = path.Length;
            int indexOfDot = 0;
            while (true)
            {
                indexOfDot = path.IndexOf('.', indexOfDot);
                if (indexOfDot < 0)
                {
                    if (result == null)
                    {
                        //No Arguments? Fine..just return what
                        //we have...
                        return path;
                    }
                    //Stick back on Arguments before getting out
                    return (path + result);
                }
                //Is the Dot the first character?
                if (
                    ((indexOfDot == 0) || (path[indexOfDot - 1] == '/'))
                    &&
                    (
                        (((indexOfDot + 1) == num2) || (path[indexOfDot + 1] == '/'))
                        ||
                        (
                            (path[indexOfDot + 1] == '.')
                            &&
                            (((indexOfDot + 2) == num2) || (path[indexOfDot + 2] == '/'))
                        )
                    )
                    )
                {
                    break;
                }
                indexOfDot++;
            }

            ArrayList list1 = new ArrayList();
            StringBuilder builder1 = new StringBuilder();
            indexOfDot = 0;
            do
            {
                int num4 = indexOfDot;
                indexOfDot = path.IndexOf('/', (num4 + 1));
                if (indexOfDot < 0)
                {
                    indexOfDot = num2;
                }
                if ((((indexOfDot - num4) <= 3) &&
                     ((indexOfDot < 1) || (path[indexOfDot - 1] == '.'))) &&
                    (((num4 + 1) >= num2) || (path[num4 + 1] == '.')))
                {
                    if ((indexOfDot - num4) == 3)
                    {
                        if (list1.Count == 0)
                        {
                            throw new Exception("Cannot_exit_up_top_directory");
                        }
                        if ((list1.Count == 1) && IsAppRelative(path))
                        {
                            return Reduce(
                                MakePathWebSiteRooted(path));
                        }
                        builder1.Length = (int) list1[list1.Count - 1];
                        list1.RemoveRange(list1.Count - 1, 1);
                    }
                }
                else
                {
                    list1.Add(builder1.Length);
                    builder1.Append(path, num4, (indexOfDot - num4));
                }
            } while (indexOfDot != num2);
            string text2 = builder1.ToString();
            if (text2.Length == 0)
            {
                if ((num2 > 0) && (path[0] == '/'))
                {
                    text2 = "/";
                }
                else
                {
                    text2 = ".";
                }
            }
            return (text2 + result);
        }

        #endregion

        #region Nicked from System.Web.UI.Utils

        /// <summary>
        ///   Combines the specified basepath.
        /// </summary>
        /// <param name = "basePath">The basepath.</param>
        /// <param name = "relativePath">The relative.</param>
        /// <returns></returns>
        /// <internal>
        /// 
        ///   <para>
        ///     May have pinched to original code from 
        ///     <c>System.Web.Util.UrlPath</c>
        ///     via Reflector. Can't really remember.
        ///   </para>
        /// 
        ///   <para>
        ///     NOT TESTED (CF OVERLOAD).
        ///   </para>
        /// 
        /// 
        /// 
        /// </internal>
        public static string Combine(string basePath, string relativePath)
        {
            return Combine(
                basePath,
                relativePath,
                AppDomainAppVirtualPath //eg: '/aspNetApp'
                );
        }


        /// <summary>
        ///   Concatenates the <paramref name = "relativePath" /> to the 
        ///   <paramref name = "basePath" />, or the <paramref name = "relativePath" />
        ///   to the <paramref name = "webSiteRelativeAppPath" />, depending
        ///   on the <paramref name = "relativePath" /> format.
        ///   <para>
        ///     Examples are:
        ///   </para>
        ///   <para>
        ///     '/pages/' + '../images/image.gif' = '/images/image.gif'
        ///   </para>
        ///   <para>
        ///     '/pages/' + '~/images/image.gif' = '/aspNetApp/images/image.gif'
        ///   </para>
        ///   <para>
        ///     '/pages/' + '/aspNetAsp/images/image.gif' = '/aspNetApp/images/image.gif'
        ///   </para>
        /// </summary>
        /// <param name = "basePath">
        ///   The basepath ('/pages/')
        /// </param>
        /// <param name = "relativePath">
        ///   The relative path ('../images/image.gif')
        /// </param>
        /// <param name = "webSiteRelativeAppPath">
        ///   The app path (eg: '/aspNetApp')
        /// </param>
        /// <returns>A reduced concatenation of basePath and 
        ///   relative path, or reduced contenation of 
        ///   webSiteRelativeAppPath and relative path.
        /// </returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "basePath" /> 
        ///   is null or empty.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "relativePath" /> 
        ///   is null.
        /// </exception>
        /// <exception cref = "System.ArgumentException">
        ///   An exception is raised if <paramref name = "relativePath" /> 
        ///   is not virtual.
        /// </exception>
        /// <exception cref = "System.ArgumentException">
        ///   An exception is raised if <paramref name = "relativePath" /> 
        ///   is webSiteRelative, but from a different website.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
        ///     which was an internal class. Dumb!
        ///   </para>
        /// 
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string Combine(
            string basePath,
            string relativePath,
            string webSiteRelativeAppPath
            )
        {
            string result;

            relativePath.ValidateIsNotDefault("relativePath");
            basePath.ValidateIsNotDefault("basepath");

            if (relativePath.StartsWith(C_RESOURCE_PREFIX))
            {
                //if the filename already has "resoource:" prefix
                //eg ("resource:Web.Controls.MyFile.js")
                //then it too is deemed to already have a abs root, 
                //and therefore is complete:
                return relativePath;
            }

            if (basePath.StartsWith(C_RESOURCE_PREFIX))
            {
                //If this is a "resource:" basePath
                //and not the more common condition of 
                //a '/aspNetApp' AppRoot, then it's a special
                //condition.
                if (
                    (basePath.Length > C_RESOURCE_PREFIX.Length)
                    &&
                    (!basePath.EndsWith("."))
                    )
                {
                    //...And there's more to this dirpart than
                    //just "resource:"
                    //but it doesn't end with "."
                    //as in "resource:Web.Controls.MyControl"
                    //...
                    //then tack on a final "."
                    //so that it's ready for collating:
                    basePath += ".";
                }
                //In order to make:
                return basePath + relativePath;
                //result: "resource:XAct.UI.Web.WebControls.MyControl.MyFile.js"
            }

            //return NormalizeSlashes(basepath, page) + relativePath;


            if (!IsVirtualPath(relativePath))
            {
                //Can't combine two rooted parts:
                throw new ArgumentException(
                    "relative path is not virtual.");
            }
            //Does relative path start with website slash('/')?
            if (IsWebSiteRelative(relativePath))
            {
                //it does...so it is WebSiteRooted.
                //the answer is that we do not prepend with app info.
                //But it has to start from within the same application:

                if (!PathStartsWithPath(relativePath, webSiteRelativeAppPath))
                {
                    throw new ArgumentException(
                        "Relative Path is websiteRelative -- but for a different website."
                        );
                }
                result = relativePath;
                //Reduce later:
            }
            else
            {
                //the url given is not WebSiteRooted,
                if (relativePath == "~")
                {
                    //...but is AppRooted:
                    //so return just the webSiteRelativeAppPath:
                    result = webSiteRelativeAppPath;
                    //result = 'aspNetApp/'
                    //Note: No reduction required so ok to exit now.
                    return result;
                }
                else if (IsAppRelative(relativePath))
                {
                    //...but is AppRooted,
                    //but not as simple as simply pointing to root
                    //so we strip off its first '~/' and 
                    //tack it onto the webSiteRelativeAppPath
                    if (webSiteRelativeAppPath.Length == 0)
                    {
                        result = "/" + relativePath.Substring(2);
                        //result = '/aspNetApp/dir/file.htm'
                        //Reduce later...
                    }
                    else
                    {
                        result = webSiteRelativeAppPath + "/" + relativePath.Substring(2);
                        //result = '/aspNetApp/dir/file.htm'
                        //Reduce later...
                    }
                }
                else
                {
                    //The path is not WebSiteRooted, or AppRooted
                    //so we just combine the basePath to the relativePath
                    //sticking a slash between the two:
                    if (basePath.Length == 0)
                    {
                        result = string.Empty;
                    }
                    else
                    {
                        result = (basePath.EndsWith("/")) ? basePath : (basePath + "/");
                    }
                    //we know it doesn't ever start with 'slash', 
                    //so we're ok to just tack it on:
                    result += relativePath;

                    //if (
                    //  (result.StartsWith("../")) || 
                    //  (result.StartsWith("/../")) ||
                    //  (result.StartsWith("./../")) ||
                    //  (result.StartsWith("/./../")) ||
                    //  (result.StartsWith("~/../"))
                    //  ) {
                    if (result.IndexOf('.') < 4)
                    {
                        //don't allow it to be reduced as there is a bug in the Reduce
                        //method
                        return result;
                    }
                    //Reduce later:
                }
            }

            //Reduce it:
            //This works most of the time...except
            //when it turns '../myfile.htm' into 'myfile.htm'
            result = Reduce(result);
            //      return NormalizeSlashes(pathToDirectory, page) + fileName;

            return result;
        }


        /// <summary>
        ///   Makes the 
        ///   Absolute ('http://site.com/aspNetApp/'),
        ///   WebSiteRooted ('/aspNetApp/sub/file.htm') 
        ///   or virtual ('sub/file.htm')
        ///   into an AppRelative path 
        ///   ('~/sub/file.htm').
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name = "virtualWebPath">The virtual path.</param>
        /// <returns></returns>
        /// <internal>
        /// 
        ///   <para>
        ///     Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
        ///     which was an internal class. Dumb!
        ///   </para>
        /// 
        /// 
        /// 
        ///   <para>
        ///     NOT TESTED (CF OVERLOAD).
        ///   </para>
        /// 
        /// 
        /// 
        /// </internal>
        public static string MakePathAppRelative(string virtualWebPath)
        {
            return MakePathAppRelative(
                virtualWebPath,
                AppDomainAppVirtualPath //eg: '/aspNetApp'
                );
        }


        /// <summary>
        ///   Makes the 
        ///   Absolute ('http://site.com/aspNetApp/'),
        ///   WebSiteRooted ('/aspNetApp/sub/file.htm') 
        ///   or virtual ('sub/file.htm')
        ///   into an AppRelative path 
        ///   ('~/sub/file.htm').
        /// </summary>
        /// <param name = "webPath">The web path.</param>
        /// <param name = "webSiteRelativeAppPath">The application path.</param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "webPath" /> is null.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "webSiteRelativeAppPath" /> 
        ///   is null or empty.
        /// </exception>
        /// <internal>
        /// 
        ///   <para>
        ///     Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
        ///     which was an internal class. Dumb!
        ///   </para>
        /// 
        /// 
        /// 
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// 
        /// 
        /// 
        /// </internal>
        public static string MakePathAppRelative(
            string webPath,
            string webSiteRelativeAppPath)
        {
            webPath.ValidateIsNotDefault("webPath");
            webSiteRelativeAppPath.ValidateIsNotNullOrEmpty("webSiteRelativeAppPath");

            //TODO
            //FINISH THIS
            if (IsAbsolutePhysicalPath(webPath))
            {
                //Get the physical path to the root url:
                string appPhysicalPath = HttpContext.Current.Server.MapPath("~");
                if (webPath.StartsWith(appPhysicalPath,
                                       StringComparison.InvariantCultureIgnoreCase))
                {
                    return
                        "~" +
                        webPath.Replace(appPhysicalPath, "").Replace("\\", "/");
                }
            }

            if (IsAppRelative(webPath))
            {
                //Already AppRelative:
                //But note that we're taking a small 
                //liberty as we are not 100% sure that
                //we are AppRelative to *this* application.
                //It's an educated guess:
                return webPath;
            }

            string schema;

            bool throwDomainError = false;
            if (WebPathHasSchema(webPath, out schema))
            {
                //if path is absolute...

                //http://blah.com/site
                //01234567879012345679
                //http [4]+3...should be right...
                //Strip of schema:
                string tmp = webPath.Substring(schema.Length + C_SCHEMA_DIVIDER.Length);
                //Now strip off domain:
                int firstSlashPos = tmp.IndexOf('/');
                if (firstSlashPos == -1)
                {
                }
                else
                {
                    //blah.com/site
                    //01234567879012345679
                    tmp = tmp.Substring(firstSlashPos);
                    //we now have stripped off domain.com leaving
                    //first slash...which means that if
                    //we let it fall through, should get matched
                    //by PathStartsWithPath in next section...
                }
                throwDomainError = true;
                //TODO: do we want to throw an error if tmp doesn't 
                //start with webSiteRelativeAppPath?

                webPath = tmp;
                //continue to next test without checking...
            }

            if (!PathStartsWithPath(
                webPath,
                webSiteRelativeAppPath))
            {
                //path is not AppRooted to this application.
                //so we don't much enough about it 
                //to decide what to do...we just 
                //return it as is:
                if (throwDomainError)
                {
                    throw new ArgumentException(
                        "Cannot turn an absolute url for a different application" +
                        " into a ApplicationRelative path.");
                }
                return webPath;
            }

            //We know they start off the same...
            //(so we also know webPath starts '/aspNetApp'):
            //and therefore are AppRelative,
            if (webPath.Length == webSiteRelativeAppPath.Length)
            {
                //and are same length...
                //so that means they are the same thing
                //in which case, we can return the appRelative symbol:
                return "~";
            }

            //Take off the webSiteRelativeAppPath part ('/aspNetApp')
            //and replace with '~':
            return ('~' + webPath.Substring(webSiteRelativeAppPath.Length));
        }


        /// <summary>
        ///   Makes the virtual path (eg: '~/dir/file.htm') 
        ///   WebSiteRooted (eg: '/aspNetApp/dir/file.htm').
        /// </summary>
        /// <param name = "virtualWebPath">The virtual path.</param>
        /// <returns></returns>
        /// <internal>
        /// 
        ///   <para>
        ///     Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
        ///     which was an internal class. Dumb!
        ///   </para>
        /// 
        /// 
        /// 
        ///   <para>
        ///     NOT TESTED (CF OVERLOAD). 
        ///   </para>
        /// 
        /// 
        /// 
        /// </internal>
        public static string MakePathWebSiteRooted(string virtualWebPath)
        {
            return MakePathWebSiteRooted(
                virtualWebPath,
                AppDomainAppVirtualPath //eg: '/aspNetApp'
                );
        }

        /// <summary>
        ///   Makes the virtual path ('~/dir/file.htm') 
        ///   WebSiteRooted ('/aspNetApp/dir/file.htm'), based on the 
        ///   (WebSiteRooted) app path given (eg: '/aspNetApp'):
        ///   <para>
        ///     If given an already website rooted 
        ///     path ('/aspNetApp/dir/file.htm')
        ///     simply returns it.
        ///   </para>
        ///   <para>
        ///     Other options (eg website rooted but in different app) 
        ///     cause an exception.
        ///   </para>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name = "virtualWebPath">The virtual path.</param>
        /// <param name = "webSiteRelativeAppPath">The (website rooted) application path .</param>
        /// <returns>The app relative path.</returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "virtualWebPath" /> is null.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "webSiteRelativeAppPath" /> is null.
        /// </exception>
        /// <exception cref = "System.ArgumentException">
        ///   An exception is raised if 
        ///   <paramref name = "virtualWebPath" /> is neither 
        ///   virtual ('~/dir/file.htm') 
        ///   or rooted ('/...').
        /// </exception>
        /// <internal>
        /// 
        ///   <para>
        ///     Pinched via Reflector from 
        ///     <c>System.Web.Util.UrlPath</c>
        ///     which was an internal class. 
        ///     Modified to add more a check that was missing.
        ///   </para>
        /// 
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// 
        /// </internal>
        public static string MakePathWebSiteRooted(
            string virtualWebPath,
            string webSiteRelativeAppPath)
        {
            virtualWebPath.ValidateIsNotDefault("virtualWebPath");
            webSiteRelativeAppPath.ValidateIsNotDefault("webSiteRelativeAppPath");

            if (virtualWebPath == "~")
            {
                //The path is AppRooted, 
                //so return the application path only:
                return webSiteRelativeAppPath;
            }

            if (((virtualWebPath.Length >= 2)
                 &&
                 (virtualWebPath[0] == '~')) &&
                ((virtualWebPath[1] == '/') ||
                 (virtualWebPath[1] == '\\')))
            {
                //this path is AppRooted
                //we'll just combine the two parts together
                //with right slash:

                //path is either '~/ or '~\'
                if (webSiteRelativeAppPath.Length > 1)
                {
                    //And there is an application path.
                    //remove the '~/' and tack it on to 
                    //the given webSiteRelativeAppPath
                    //in order to return it:
                    return (webSiteRelativeAppPath + "/" +
                            virtualWebPath.Substring(2));
                }
                else
                {
                    //path is either '~/ or '~\'
                    //But No application path...means 
                    //There is no application path to speak of,
                    //so just root the virtualWebPath:
                    return ("/" + virtualWebPath.Substring(2));
                }
            }

            //The virtualWebPath length is 0 or 1 char long...
            //so first char has to be rooted web char (ie, '/')
            if ((virtualWebPath.Length > 0) && (virtualWebPath[0] == '/'))
            {
                //The path is WebSiteRooted.
                //It is...so ok to return it as being the full answer:
                //But one last check... it has to start with the webSiteRelativeAppPath
                if (!string.IsNullOrEmpty(webSiteRelativeAppPath))
                {
                    if (!StringStartsWithIgnoreCase(virtualWebPath, webSiteRelativeAppPath))
                    {
                        //The virtual path has to start with given app path
                        //or else its not right.
                        throw new ArgumentOutOfRangeException(
                            "Virtual Path given, since rooted to website," +
                            " must match given webSiteRelativeAppPath.");
                    }
                }

                return virtualWebPath;
            }
            //Wasn't...so whatever it is...this path is not app relative ('~'), 
            //or website rooted ('/').
            //so its just floating around...and it's not right 
            //to give an answer that would depend on the page it 
            //is being calculated from:
            throw new ArgumentOutOfRangeException("virtualWebPath");
        }


        /// <summary>
        ///   Checks to see if given virtual path ('/MyAspNet/dir/file.htm') 
        ///   starts with this webSiteRelativeAppPath ('/MyAspNet')
        /// </summary>
        /// <param name = "virtualWebPath">The virtual path.</param>
        /// <returns></returns>
        /// <internal>
        ///   <para>
        ///     Pinched via Reflector from <c>System.Web.Util.UrlPath</c>
        ///     which was an internal class. Dumb!
        ///   </para>
        /// 
        /// 
        /// 
        ///   <para>
        ///     NOT TESTED (CF OVERLOAD). 
        ///   </para>
        /// 
        /// 
        /// 
        /// </internal>
        public static bool VirtualPathStartsWithAppPath(string virtualWebPath)
        {
            return
                PathStartsWithPath(
                    virtualWebPath,
                    AppDomainAppVirtualPath //eg: '/aspNetApp'
                    );
        }


        /// <summary>
        ///   Checks to see that the first path ('/appdir/subdir/')
        ///   starts with the second path
        ///   ('/appdir/' or ('/appdir') but
        ///   not ('/appdirodirwhathavewedone').
        ///   <para>
        ///     Note that it's slightly more complex that simply
        ///     using <c>string.StartsWith()</c> since it has to
        ///     check that the first part ends with a slash, etc.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   The second argument must be longer than string.Empty.
        ///   for a comparison to begin to take place (ie, using
        ///   string.Empty will return false).
        /// </remarks>
        /// <internal>
        ///   First version pinched via Reflector from 
        ///   <c>System.Web.Util.UrlPath</c>
        ///   which was an internal class. Dumb!
        /// </internal>
        /// <param name = "path1">The path to search within.</param>
        /// <param name = "path2">The reference path 
        ///   (ie, the path to find within path1).</param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "path1" /> is null.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "path2" /> is null.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool PathStartsWithPath(string path1, string path2)
        {
            path1.ValidateIsNotDefault("path1");
            path2.ValidateIsNotDefault("path2");

            if (path2 == string.Empty)
            {
                return false;
            }
            //If the first path 
            //eg: '/someapp/somedir/somefile'
            //doesn't start with the second path
            //eg: '/someapp/somedir/'
            //then we're done...just return false:
            if (!StringStartsWithIgnoreCase(
                path1, path2))
            {
                return false;
            }
            //But they do...
            if (path1.Length == path2.Length)
            {
                //And the exact same length...so definately
                //return true:
                return true;
            }


            if (path2.EndsWith("/"))
            {
                //'/appdir/subdir/' [15]
                //'/appdir/' [8]
                //We know that since they are the same
                //it means that str1 is also at '/'
                //at this point... so its safe:
                return true;
            }

            //'/appdir/subdir/' [15]
            //'/appdir' [8]
            //a[8] = 
            if (path1[path2.Length] == '/')
            {
                return true;
            }

            return false;
        }


        /// <summary>
        ///   An extended version of string.StartsWith(x), that ignores case.
        /// </summary>
        /// <internal>
        ///   Used by 
        ///   <see cref = "PathStartsWithPath" />
        /// </internal>
        /// <param name = "s1">The s1.</param>
        /// <param name = "s2">The s2.</param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception will be raised if <paramref name = "s1" /> is null.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool StringStartsWithIgnoreCase(string s1, string s2)
        {
            s1.ValidateIsNotDefault("s1");

            return s1.StartsWith(
                s2,
                StringComparison.InvariantCultureIgnoreCase);
        }


        /// <summary>
        ///   The given path starts with a schema
        ///   (eg: 'http://' or 'file://')
        /// </summary>
        /// <param name = "webPath">The web path.</param>
        /// <returns>True if found.</returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "webPath" /> is null.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool WebPathHasSchema(string webPath)
        {
            webPath.ValidateIsNotDefault("webPath");

            if (webPath == string.Empty)
            {
                return false;
            }

            int slashPos = webPath.IndexOf(
                C_SCHEMA_DIVIDER,
                0,
                C_SCHEMA_LENGTH);

            //return true if something found:
            return (slashPos != -1);
        }


        /// <summary>
        ///   The given path starts with a schema
        ///   (eg: 'http://' or 'file://')
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     The returned schema will be without '://', 
        ///     (eg: just 'http').
        ///   </para>
        /// </remarks>
        /// <param name = "webPath">The web path.</param>
        /// <param name = "schema">
        ///   The schema if found (null if not).
        /// </param>
        /// <returns>True if found.</returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "webPath" /> is null.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static bool WebPathHasSchema(string webPath, out string schema)
        {
            webPath.ValidateIsNotDefault("webPath");

            if (webPath == string.Empty)
            {
                schema = null;
                return false;
            }
            int pos = webPath.IndexOf(C_SCHEMA_DIVIDER, 0, C_SCHEMA_LENGTH);
            if (pos == -1)
            {
                schema = null;
                return false;
            }
            //http://rottenapples.com
            //0123456
            //    4
            schema = webPath.Substring(0, pos);
            return true;
        }

        #endregion
    }


}


