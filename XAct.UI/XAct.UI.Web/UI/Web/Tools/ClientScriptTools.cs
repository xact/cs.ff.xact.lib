namespace XAct.UI.Web.Tools
{
    using System;
    using System.IO;
    using System.Web;
    using System.Web.UI;

    /// <summary>
    ///   Class of static methods to help with embedding and 
    ///   instantiating clientside scripts and style sheets for usage
    ///   by the Client.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     For Client Scripts to work with a control there are two
    ///     steps that have to be performed: Embedding and Instantiating:
    ///   </para>
    ///   <para>
    ///     Embedding the Script into the Page:
    ///     <br />
    ///     You have to send the file (but only once
    ///     even if the control is used several times on the webpage)
    ///   </para>
    ///   <para>
    ///     Instantiating the Script on the clientside:
    ///     The script must be initialized, but only once the page has 
    ///     received and parsed all its elements.
    ///     <br />
    ///   </para>
    /// </remarks>
    public class ClientScriptTools
    {
        /// <summary>
        ///   Embeds a JavaScript 'include' statement into the control's page.
        /// </summary>
        /// <remarks>
        ///   /// <para>
        ///         A simple example of its usage would be:
        ///         <code>
        ///           <![CDATA[
        /// protected override void OnPreRender(EventArgs e) {
        ///   //Handle normal stuff beforehand:
        ///   base.OnPreRender(e);
        ///   //Step 1: Ensure script file is sent to client:
        ///   //string pathToScript = 
        ///     Utils.UrlPathUtils.CombineUrl(
        ///       this.BaseUrl, 
        ///       "MyControl.js", 
        ///       this.Page);
        ///   string pathToScript = "resource:MyControl.Resources.MyControl.js";
        ///   Utils.ClientScripts.EmbedClientScriptInclude(this, pathToScript);
        ///   //Step 2: Instantiate the client class:
        ///   ClientScriptInstantiate();
        /// }
        /// ]]>
        ///         </code>
        ///         A more complex example of its usage, as one one would see
        ///         in most of our controls, would be:
        ///         <code>
        ///           <![CDATA[
        /// protected override void OnPreRender(EventArgs e) {
        ///   //Handle normal stuff beforehand:
        ///   base.OnPreRender(e);
        ///   //Step 1: Ensure script file is sent to client:
        ///   ClientScriptInclude();
        ///   //Step 2: Instantiate the client class:
        ///   ClientScriptInstantiate();
        /// }
        /// 
        /// //Helper method to build path to script and send it to client:
        /// protected virtual void ClientScriptInclude() {
        ///   //Embed standard lib is sent:
        ///   Utils.ClientScripts.EmbedClientScriptInclude(
        ///   this, 
        ///   "resource:XAct.Resources.ClientSide.W3C.js");
        /// 
        ///   //Ensure script file is sent to client:
        ///   //string pathToScript = 
        ///     Utils.UrlPathUtils.CombineUrl(
        ///     this.BaseUrl, 
        ///     "MyControl.js", 
        ///     this.Page);
        ///     
        ///   string pathToScript = "resource:MyControl.Resources.MyControl.js";
        ///   Utils.ClientScripts.EmbedClientScriptInclude(this, pathToScript);
        /// }
        /// 
        /// //Helper method to instantiate the script and pass as constructor
        /// //arguments the ID's (ClientSideIDs -- not serverside IDs!) 
        /// of the elements that the script has to manipulate:
        /// protected virtual void ClientScriptInstantiate() {
        ///   
        ///   Utils.ClientScripts.EmbedClientScriptClassInstantiation(
        /// 				this,
        /// 				"MyControl", //Name of class to instantiate:
        /// 				this.ContainerMain.MyDropDownControl.ClientID,//Client Id...
        /// 				this.ContainerMain.MyResultDiv.ClientID,//Client Id...
        /// }
        /// ]]>
        ///         </code>
        ///         that refers to a clientScript that may look like this:
        ///         <code>
        ///           <![CDATA[
        /// function MyControl (dropDownDivID, resultDivID){
        ///   //Convert ID's to client object refs:
        ///   this.dropDown = document.getElementById(dropDownDivID);
        ///   this.resultDiv = document.getElementById(resultDiv);
        /// 
        ///   //Attach Event Handlers in W3C format:
        ///   oC=this;
        ///   if (this.dropDown){
        ///     W3C.addEventListener(
        ///       this.dropDown,
        ///       "onchange",
        ///       function(e){oC.OnChange(e);});}
        /// }
        /// 
        /// var tp=CompactCatalogZone.prototype;
        /// 
        /// tp.OnChange=function(e){
        ///   	var srcElement=W3C.getEventSrc(e);
        ///   	if (!srcElement){return;}
        ///   	if (srcElement.selectedIndex < 0){return;}
        ///   	//Get the Option that is currently selected:
        ///   	var option = srcElement.options[srcElement.selectedIndex];
        ///   	//Extract attributes:
        ///   	var title = option.text;
        ///     var val = option.value;
        ///   	var description = option["description"];
        ///     //Use helper for bools...
        ///     var onPage = XTOOLS.strToBool(option["onPage"]);
        ///     //Update client interface:
        ///     this.resultDiv = "<b>"+title + "</b><br/>" + description;
        /// }
        /// ]]>
        ///         </code>
        /// 
        ///         An even more complex example of its usage, with templating, and
        ///         callbacks would be built this way:
        ///         <code>
        ///           <![CDATA[
        /// protected override void OnPreRender(EventArgs e) {
        ///   //Handle normal stuff beforehand:
        ///   base.OnPreRender(e);
        ///   //Step 1: Ensure script file is sent to client:
        ///   ClientScriptInclude();
        ///   //Step 2: Instantiate the client class:
        ///   ClientScriptInstantiate();
        /// }
        /// 
        /// //Helper method to build path to script and send it to client:
        /// protected virtual void ClientScriptInclude() {
        ///   //Ensure script file is sent to client:
        ///   //string pathToScript = 
        ///     Utils.UrlPathUtils.CombineUrl(
        ///       this.BaseUrl, 
        ///       "MyControl.js", 
        ///       this.Page);
        ///   string pathToScript = "resource:MyControl.Resources.MyControl.js";
        ///   Utils.ClientScripts.EmbedClientScriptInclude(this, pathToScript);
        /// }
        /// 
        /// //Helper method to instantiate the script and pass as constructor
        /// //arguments the ID's (ClientSideIDs -- not serverside IDs!) 
        /// protected virtual void ClientScriptInstantiate() {
        ///   //We need to prepare the arguments we want to send.
        ///   //In this case, we need 
        ///   // *) a clientSide ID...
        ///   // *) the callback script fragment
        ///   // *) a template that that is to be used by the script...
        ///   string sCallBackFunctionInvocation = 
        ///     this.Page.ClientScript.GetCallbackEventReference(
        ///     this, 
        ///     "argument", 
        ///     "oC.OnReturn", 
        ///     "context", 
        ///     "oC.OnError", 
        ///     false) + ";";
        ///    
        ///   //Instantiate the template in a temp container:
        /// 	ItemTemplateContainer container = 
        /// 	  new ItemTemplateContainer(
        /// 	    this, 
        /// 	    "{0}", 
        /// 	    this.Page.MapPath( this.BaseUrl  + "/" + "Images/file.gif"));
        /// 	    
        ///   this.ItemTemplate.InstantiateIn(container);
        /// 	container.DataBind();
        ///   //render temlate to a tmp string and escape it for 
        ///   //transmission to client, cleaning it up a bit:
        /// 	string template = getHtml(container);
        /// 	template = template.Replace("\t", "");
        /// 	template = template.Replace("\r", "");
        /// 	template = template.Replace("\n", "");
        /// 	template = template.Replace("\"", "\\\"");
        ///   
        ///   Now that we have all arguments:
        /// 	Utils.ClientScripts.EmbedClientScriptClassInstantiation(
        /// 	  this,
        /// 		"MyControl",  //ClassName
        /// 		this.sCallBackFunctionInvocation,//1 arg in this case is callback 
        /// 		this.ContainerMain.MyDropDown.ClientID,//2
        /// 		template//3
        /// 		);
        /// 				
        ///   //Helper method to convert a control to a string fragment to
        ///   //send as an argument:
        /// 	private string getHtml (WebControl control){
        /// 		//Create new stream and writer:
        /// 		using (StringWriter sw = new StringWriter()){
        /// 		  using (HtmlTextWriter hw = new HtmlTextWriter(sw)){
        /// 		    control.RenderControl(hw);
        /// 	      return sw.ToString();
        /// 	    }
        /// 	  }
        /// 	}
        /// ]]>
        ///         </code>
        ///       </para>
        ///   <para>
        ///     This method uses the Control's Page's ClientScript methods (namely
        ///     <see cref = "ClientScriptManager.IsClientScriptBlockRegistered(string)" /> and 
        ///     <see cref = "ClientScriptManager.RegisterClientScriptBlock(Type, string,string)" />)
        ///     to link to the the resource only once per page.
        ///   </para>
        ///   <para>
        ///     Note that the method determines from the relativeUrl's 
        ///     file extension what type of statement to use:
        ///     <![CDATA[
        /// <script id=\"MyJSFile" language=\"javascript\"  
        ///         src=\"..\Scripts\MyControl\MyControl.js"></script>
        /// and
        /// <link type=\"text/css\" 
        ///       rel=\"StyleSheet\" 
        ///       href=\"XResources.aspx?ruid=183\"/>
        /// ]]>
        ///   </para>
        ///   <para>
        ///     This method uses the Control's Page methods (namely
        ///     <c>Page.ClientScript.IsClientScriptBlockRegistered</c> 
        ///     and 
        ///     <c>Page.ClientScript.RegisterClientScriptBlock</c>)
        ///     to link to the the resource only once per page.
        ///   </para>
        ///   <para>
        ///     Note that the method determines from the relativeUrl's 
        ///     file extension what type of statement to use:
        ///     <![CDATA[
        /// <script id=\"MyJSFile" language=\"javascript\"  
        ///         src=\"..\Scripts\MyControl\MyControl.js"></script>
        /// and
        /// <link type=\"text/css\" rel=\"StyleSheet\" 
        ///       href=\"XResources.aspx?ruid=183\"/>
        /// ]]>
        ///   </para>
        ///   <para>
        ///     <b>VERY IMPORTANT:</b>
        ///     <br />
        ///     If no file is found on hard-drive, 
        ///     but is found as embedded resource, copies
        ///     embedded resource to file for next time. 
        ///     If you do not want this behavior (ie want to protect
        ///     intellectual property, 
        ///     set <c>leaveAsEmbeddedResource</c> to <c>true</c>.
        ///   </para>
        /// </remarks>
        public static void EmbedClientScriptInclude(
            Control control,
            string relativeUrl)
        {
            //Check Vars:
            control.ValidateIsNotDefault("control");
            relativeUrl.ValidateIsNotNullOrEmpty("relativeUrl");


            /*
      if (!Utils.UrlPathUtils.IsRelativeUrl(relativeUrl)) {
        throw new System.ArgumentNullException(
          "relativeUrl must be relativeUrl");
      }
       */

            string fileExtension = Path.GetExtension(relativeUrl);
            if (fileExtension == string.Empty)
            {
                throw new Exception(
                    "Invalid relativeUrl: has no Extension.");
            }


            //string absolutePath = Utils.UrlPathUtils.MapPath(control, relativeUrl);
            //bool fileExists = File.Exists(absolutePath);

            string clientUrl = ClientResourceTools.GetWebResourceUrl(control, relativeUrl);

            string uniqueIncludeKey =
                Path.GetFileNameWithoutExtension(relativeUrl).ToLower();

            if (!string.IsNullOrEmpty(clientUrl))
            {
                if (fileExtension == ".css")
                {
                    string styleLine =
                        string.Format(
                            "<link type=\"text/css\" rel=\"StyleSheet\" href=\"{0}\"/>\n",
                            clientUrl);

                    //DesignTime. Hack: Force a screen refresh:
                    if (HttpContext.Current != null)
                    {
                        styleLine =
                            string.Format(
                                "<B style=\"font-size:1px;\">&nbsp;{0}</B>",
                                styleLine);
                    }
                    control.Page.ClientScript.
                        RegisterClientScriptBlock(
                            control.GetType(),
                            uniqueIncludeKey,
                            styleLine);
                }
                else
                {
                    control.Page.ClientScript.RegisterClientScriptInclude(
                        uniqueIncludeKey,
                        clientUrl);
                }
            }
            else
            {
                control.Page.ClientScript.RegisterClientScriptBlock(
                    control.GetType(),
                    uniqueIncludeKey,
                    string.Format(
                        "<script>\n\\Script Include failed for '{0}' in {1}\n</script>",
                        relativeUrl,
                        control)
                    );
            }
        }

        ///<summary>
        ///  Generate a clientside script name for a class instance. 
        ///  Used by EmbedClientScriptClassInstantiation.
        ///</summary>
        ///<remarks>
        ///  <para>
        ///    Our original algorythm for calculating a unique 
        ///    name was based on an counter.
        ///    We had to change it. This explains why.
        ///  </para>
        ///  <para>
        ///    When instantiating a JavaScript class on 
        ///    the server, the name must be unique:
        ///    <code>
        ///      var uniqueName = new MyClass(...);
        ///    </code>
        ///    Because several controls may refer to the 
        ///    same instance (eg for attaching click events, etc.) the name
        ///    must not only be unique, but consistently unique.
        ///  </para>
        ///  <para>
        ///    Hence why this method creates the name by 
        ///    combining the name of the Javascript class with
        ///    the control's clientID -- which is unique for each 
        ///    control, but consistently unique no
        ///    matter whom is referring to it.
        ///  </para>
        ///</remarks>
        ///<param name = "control"></param>
        ///<param name = "scriptClassName"></param>
        ///<returns></returns>
        public static string GenClientScriptClassInstanceName(
            Control control,
            string scriptClassName)
        {
            //Check Args:
            control.ValidateIsNotDefault("control");
            scriptClassName.ValidateIsNotNullOrEmpty("scriptClassName");

            //Create a JS instance name out of the Javascript 
            //class name and the unique ID
            //of the clientID...
            //You get something like "Mapper_01_02", which will be unique
            //on the page even if more than one control is used...
            return scriptClassName + "_" + control.ClientID;
        }


        ///<summary>
        ///  Embeds the code required to instantiate a Javascript Class.
        ///</summary>
        ///<remarks>
        ///  <para>
        ///    This will inject code that might look like something like:
        ///    <code>
        ///      <![CDATA[
        /// <script>
        ///		if (typeof([ScriptClassName])!=null){
        ///		  var [ScriptClassInstanceName] = new [ScriptClassName]
        ///		    ("Control.ClientID", "Arg2", etc.);}
        /// </script>
        /// ie: something that looks like:
        /// <script>
        ///		if (typeof(MyJSClass)!=null){
        ///		  var MyJSClass_ctl00 = new MyJSClass("MyControl","Blue","Bold");
        ///	 </script>
        /// ]]>
        ///    </code>
        ///  </para>
        ///  <para>
        ///    Note that the Javascript generated checks first that the 
        ///    Javascript class exists in the page.
        ///  </para>
        ///  <para>
        ///    Note that each (optional) argument is wrapped in
        ///    double quotation marks, except for null values, which are just
        ///    sent as <c>null</c>, which Javascript understands.
        ///  </para>
        ///</remarks>
        ///<param name = "control"></param>
        ///<param name = "scriptClassName"></param>
        ///<param name = "argsForScriptInstantiation"></param>
        ///<returns>
        ///  Returns the name of the 
        ///  clienside class that will be instantiated.
        ///</returns>
        public static string EmbedClientScriptClassInstantiation(
            Control control,
            string scriptClassName,
            params string[] argsForScriptInstantiation)
        {
            //Check Args:
            control.ValidateIsNotDefault("control");
            if (string.IsNullOrEmpty(scriptClassName))
            {
                throw new Exception(
                    @"Cannot invoke EmbedClientScriptClassInstantiation if 
scriptClassName has not been set.");
            }

            //Quote each var that is being sent unless it is null:
            string[] tArgs;
            //usually I can resize 'argsForScriptInstantiation' 
            //if I must -- but because
            //its a 'param' optional, have to work with a copy:
            if (argsForScriptInstantiation == null)
            {
                tArgs = new String[0];
            }
            else
            {
                tArgs = new string[argsForScriptInstantiation.Length];
            }

            //Loop through arguments and wrap them in 
            //quotation marks, unless it is a null,
            //in which case we just print the word 'null', which js understands:
            for (int i = 0; i < tArgs.Length; i++)
            {
                tArgs[i] = (argsForScriptInstantiation[i].ToLower() != "null")
                               ? ("\"" + argsForScriptInstantiation[i] + "\"")
                               : (argsForScriptInstantiation[i].ToLower());
            }

            //We will want a unique name for the JavaScript instance of the class:
            string scriptClassInstanceName =
                GenClientScriptClassInstanceName(control, scriptClassName);


            //Make the template for the Javascript we want to run:
            string template =
                "if (typeof({1})==\"undefined\"){{alert(\"Cannot instantiate '{1}'\");}}else{{var {0} = new {1}({2});}}";

            //Now string.Format it with the vars we have:
            string tInstance =
                string.Format(
                    template,
                    scriptClassInstanceName,
                    scriptClassName,
                    string.Join(",", tArgs));

            //And embed the whole thing into the page:
            control.Page.ClientScript.RegisterStartupScript(
                control.GetType(),
                scriptClassInstanceName,
                tInstance, true);

            return scriptClassInstanceName;
        }

//Method:End


        /// <summary>
        ///   Embeds a single line of code, of your choosing. 
        ///   Wraps it in 'script' tags.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Note that this method wraps it in 'script' tag -- so 
        ///     do not add it yourself.
        ///   </para>
        /// </remarks>
        /// <param name = "control">
        ///   Control to inject created LiteralControl into.
        /// </param>
        /// <param name = "scriptFragment">
        ///   Line of Javascript code to embed directly into page
        /// </param>
        public static void EmbedClientScriptBlock(
            Control control,
            string scriptFragment)
        {
            //Check Vars:
            control.ValidateIsNotDefault("control");
            if ((scriptFragment == null) || (scriptFragment == string.Empty))
            {
                throw new ArgumentNullException("scriptFragment");
            }

            LiteralControl literal =
                new LiteralControl(
                    "\n<script>{0}</script>\n".FormatStringInvariantCulture(scriptFragment));

            literal.EnableViewState = false;
            control.Controls.Add(literal);
        }
    }


}


