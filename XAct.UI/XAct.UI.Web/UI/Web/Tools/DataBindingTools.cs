namespace XAct.UI.Web.Tools
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Reflection;
    using System.Xml;

    /// <summary>
    ///   A static class of helpful static methods 
    ///   for common Databinding operations by controls.
    /// </summary>
    public static class DataBindingTools
    {
        ///<summary>
        ///  Helper method for databinding.
        ///</summary>
        ///<remarks>
        ///  <para>
        ///    A small example of how to use it might look like this:
        ///    <code>
        ///      <![CDATA[
        /// using DataBindingHelper = XAct.UI.Web.WebControls.Common;
        /// ...
        /// protected override void OnDataBinding(EventArgs e) {
        ///		//Must ensure it all exists...
        ///		EnsureChildControls();
        ///		
        ///		IList innerSource = 
        ///		  DataBinding.GetInnerIListFromDataSource(
        ///		    _DataSource,_DataMember);
        ///		  
        ///		this.WC_ListBox.Items.Clear();
        ///		
        ///		if (innerSource != null) {
        ///			foreach (object o in InnerSource) {
        ///				string tText = DataBinding.GetFieldValue(o, _DataTextField);
        /// 			string tValue = DataBinding.GetFieldValue(o, _DataValueField);
        /// 			ListItem oItem = new ListItem(tText,tValue);
        /// 			this.WC_ListBox.Items.Add(oItem);
        /// 		}
        /// 	}
        /// }
        /// ]]>
        ///    </code>
        ///  </para>
        ///  <para>
        ///    No exception is raised when <paramref name = "dataSource" />
        ///    is null.
        ///  </para>
        ///</remarks>
        ///<returns></returns>
        public static IList GetInnerIListFromDataSource(
            object dataSource,
            string dataMember)
        {
            //see: http://msdn.microsoft.com/
            //  library/default.asp?
            //    url=/library/en-us/dnadvnet/html/vbnet08262002.asp

            if (dataSource == null)
            {
                //No source...no fun!
                return null;
            }

            if (dataSource is DataSet)
            {
                //It's a datasource...
                //in which case we really want a DataTable
                //within it..hence the need for the dataMember
                //property:
                DataSet dataSet =
                    (DataSet) dataSource;

                DataTable dataTable = null;
                //
                if (!string.IsNullOrEmpty(dataMember))
                {
                    //Get the named table:
                    dataTable = dataSet.Tables[dataMember];
                }
                else
                {
                    //No named table...so get the first:
                    dataTable = dataSet.Tables[0];
                }
                //Once we have the table, we want to return it as
                //an IList:
                return ((IListSource) dataTable).GetList();
            }
            else if (dataSource is IListSource)
            {
                //Ok...not a dataset...An IListSource
                //that will return an IList
                return ((IListSource) dataSource).GetList();
            }
            else if (dataSource is IList)
            {
                //Plain old IList itself 
                //(ie, collection accessible by index)...
                return (IList) dataSource;
            }
            /*
    else if (dataSource is System.Xml.XmlNodeList) {
      return ((XmlNodeList)dataSource;
    }
    */
            return null;
        }

        /// <summary>
        ///   Given a DataObject, with or without a FieldName, 
        ///   returns the value by that fieldname.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Used when databinding.
        ///   </para>
        ///   <para>
        ///     This is (hopefully) an improvement over the
        ///     System.Web.UI.DataBinder.GetPropertyMethod --which works
        ///     great -- but always expects a FieldName...sometimes
        ///     you just want to work with an array of simple
        ///     objects -- not an array of complex objects, with properties...
        ///   </para>
        ///   <para>
        ///     No exception is raised when <paramref name = "dataItem" />
        ///     is null.
        ///   </para>
        /// </remarks>
        /// <param name = "dataItem">The source of the data.</param>
        /// <param name = "optionalFieldName">
        ///   An optional name of a field/property of the data source
        ///   (it's optional because the dataItem can be a primitive
        ///   or value type).
        /// </param>
        /// <param name = "optionalStringFormatPattern">
        ///   An optional string.Format pattern to shape
        ///   the value before returning it.
        /// </param>
        /// <returns>
        ///   The string value of the object or its field/property.
        /// </returns>
        public static string GetFieldValue(
            object dataItem,
            string optionalFieldName,
            string optionalStringFormatPattern)
        {
            //Get the value of the object itself, or a property or field within:
            string result = GetFieldValue(dataItem, optionalFieldName);
            //Optionally format it before returning it:
            return (!string.IsNullOrEmpty(optionalStringFormatPattern))
                       ? string.Format(optionalStringFormatPattern, result)
                       : result;
        }

        /// <summary>
        ///   Given a DataObject, with or without a FieldName, 
        ///   returns the value of the Property or Field it points to.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Used when databinding.
        ///   </para>
        ///   <para>
        ///     This is (hopefully) an improvement over the
        ///     System.Web.UI.DataBinder.GetPropertyMethod --which works
        ///     great -- but always expects a FieldName...sometimes
        ///     you just want to work with an array of 
        ///     simple objects -- not an array of 
        ///     complex objects, with properties...
        ///   </para>
        ///   <para>
        ///     No exception is raised when <paramref name = "dataItem" />
        ///     is null.
        ///   </para>
        /// </remarks>
        /// <param name = "dataItem">The source of the data.</param>
        /// <param name = "optionalFieldName">
        ///   An optional name of a field/property of the data source
        ///   (it's optional because the dataItem can be a primitive
        ///   or value type).
        /// </param>
        /// <returns>
        ///   The string value of the object or its field/property.
        /// </returns>
        public static string GetFieldValue(
            object dataItem,
            string optionalFieldName)
        {
            if (dataItem == null)
            {
                return null;
            }

            if (string.IsNullOrEmpty(optionalFieldName))
            {
                //No field or property name?!...
                //Ok..we'll send back the whole thing.
                //Hopefully it was a ValueType/Primitive:
                return dataItem.ToString();
            }

            //We have a field/property name:

            //Get the type of the object:
            Type oType = dataItem.GetType();

            string result;
            if (dataItem is DataRowView)
            {
                //this is a DataRowView from a DataView

                //The result will be the View's Row's Column value:
                result =
                    ((DataRowView) dataItem)
                        [optionalFieldName].ToString();
            }
            else if (dataItem is String)
            {
                //Just a string...in which case, we want just that:
                result = dataItem as string;
            }
            else if ((dataItem is ValueType)
                     && (oType.IsPrimitive))
            {
                //It's a primitive/ValueType (int, double, guid, etc.)
                //...so we just return it 
                //converted to a string:
                result = dataItem.ToString();
            }
            else if (dataItem is XmlElement)
            {
                //Ah...an xml element...
                //in which case we want the xml element's attribute value:
                XmlAttribute oA =
                    ((XmlElement) dataItem).Attributes[optionalFieldName];
                //If the attribute did not exist, return empty string
                //anyway:
                result = (oA != null) ? oA.Value : string.Empty;
            }
            else
            {
                //this is an object or Structure
                try
                {
                    //Maybe we can find a property?
                    PropertyInfo oPI =
                        oType.GetProperty(optionalFieldName);

                    if (oPI != null)
                    {
                        result = (oPI.CanRead)
                                     ? oPI.GetValue(dataItem, null).ToString()
                                     : "[?Inaccessible:" + optionalFieldName + "?]";
                    }
                    else
                    {
                        //Ok...not a property...a field maybe?
                        FieldInfo oFI =
                            oType.GetField(optionalFieldName);

                        result = (oFI != null)
                                     ? oFI.GetValue(dataItem).ToString()
                                     : null;
                    }
                }

                catch
                {
                    //We didn't find a property or field
                    //just send back a default ToString():
                    result = dataItem.ToString();
                }
            }
            return result;
        }


        /*
    public IDataObjectFactory GetDataSource(WebControl Instance) {
      if ((!base.DesignMode && this._currentDataSourceValid) 
     &&
     (this._currentDataSource != null)) {
        return this._currentDataSource;
      }
      IDataObjectFactory source1 = null;
      string text1 = this.DataSourceID;
      if (text1.Length != 0) {
        object[] objArray1;
        Control control1 = DataBoundControlHelper.FindControl(this, text1);
        if (control1 == null) {
          objArray1 = new object[2] { this.ID, text1 };
          throw new HttpException(SR.GetString(
           "DataControl_DataSourceDoesntExist", 
           objArray1));
        }
        source1 = control1 as IDataObjectFactory;
        if (source1 == null) {
          objArray1 = new object[2] { this.ID, text1 };
          throw new HttpException(
             SR.GetString(
               "DataControl_DataSourceIDMustBeDataControl", 
               objArray1));
        }
      }
      return source1;
    }


    protected virtual void ValidateDataSource(object dataSource) {
      if (((dataSource != null) && !(dataSource is IListSource)) && 
          (!(dataSource is System.Collections.IEnumerable) && 
          !(dataSource is IDataObjectFactory))) {
        throw new ArgumentException("DataBoundControl_InvalidDataSourceType");
      }
    }
    */
    }


}


