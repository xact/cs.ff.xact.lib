
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//PC:
#endif

namespace XAct.UI.Web.Tools
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Web;

    /// <summary>
    ///   Helper class to provide methods 
    ///   commonly needed to investigate Environment properties.
    /// </summary>
    public static class EnvironmentTools
    {
        //START: COPY OF WHAT IS IN XAct.Core

        #region Constants

        /// <summary>
        ///   Constant used to save the 
        ///   ApplicationName in the Request's hashtable.
        /// </summary>
        private const string C_HTTPCONTEXT_APPLICATIONNAME = "ApplicationName";

        #endregion

        #region AppDir

        /// <summary>
        ///   Gets the Application's base path.
        /// </summary>
        /// <value>The app dir.</value>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string AppDir
        {
            get
            {
                if (string.IsNullOrEmpty(_AppDir))
                {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                    //PC:
                    if (HttpContext.Current != null)
                    {
                        _AppDir = HttpContext.Current.Request.PhysicalApplicationPath;
                    }
                    else
                    {
                        Assembly entryAssembly = GetEntryAssembly();
                        //TODO: This is coming back null in testrigs...
                        _AppDir = Path.GetDirectoryName(entryAssembly.Location);
                    }
#else
    //CE:
          _AppDir = Path.GetDirectoryName(GetEntryAssembly().Location);
#endif
                }
                return _AppDir;
            }
        }

        private static string _AppDir;


        /// <summary>
        ///   Gets the name of the currently requested page.
        /// </summary>
        /// <value>The name of the current page.</value>
        public static string CurrentPageName
        {
            get
            {
                HttpContext current = HttpContext.Current;
                if (current != null)
                {
                    return current.Request.Path.Substring(
                        current.Request.Path.LastIndexOf('/') + 1);
                }
                return null;

                //SEE ALSO:
                //System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]);
            }
        }

        #endregion

        #region GetCompanyName

        /// <summary>
        ///   Gets the name of the EntryAssembly's company.
        /// </summary>
        /// <returns></returns>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string GetCompanyName()
        {
            Assembly entryAssembly = GetEntryAssembly();
            var result = GetCompanyName(entryAssembly);
            return result;
        }

        /// <summary>
        ///   Gets the name of the given Assembly's Company.
        /// </summary>
        /// <param name = "assembly">The assembly.</param>
        /// <returns></returns>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string GetCompanyName(Assembly assembly)
        {
            if (assembly != null)
            {
                //Checked:
                //Available on CF:
                //http://msdn.microsoft.com/en-us/library/
                //  system.reflection.assemblycompanyattribute.aspx
                object[] customAttributes =
                    assembly.GetCustomAttributes(
                        typeof (AssemblyCompanyAttribute), false);

                if ((customAttributes != null)
                    &&
                    (customAttributes.Length > 0))
                {
                    AssemblyCompanyAttribute
                        companyAttribute =
                            (AssemblyCompanyAttribute)
                            customAttributes[0];

                    return companyAttribute.Company;
                }
            }

            /*
            if (string.IsNullOrEmpty(result)) {
                result = GetAppFileVersionInfo().CompanyName;
                if (result != null) {
                    result = result.Trim();
                }
            }

            if (string.IsNullOrEmpty(result)) {
                Type appMainType = GetAppMainType();

                if (appMainType != null) {
                    string str = appMainType.Namespace;
                    if (!string.IsNullOrEmpty(str)) {
                        int index = str.IndexOf(".");
                        if (index != -1) {
                            result = str.Substring(0, index);
                        }
                        else {
                            result = str;
                        }
                    }
                    else {
                        result = ProductName;
                    }
                }
            }
             */
            return string.Empty;
        }


        /*
      //string verInfo = assembly.GetName().Version;

      private static System.Diagnostics.FileVersionInfo GetAppFileVersionInfo() {
          lock (internalSyncObject) {
              if (appFileVersion == null) {
                  Type appMainType = GetAppMainType();
                  if (appMainType != null) {
                      FileIOPermission permission = new FileIOPermission(PermissionState.None);
                      permission.AllFiles = FileIOPermissionAccess.PathDiscovery | FileIOPermissionAccess.Read;
                      permission.Assert();
                      try {
                          appFileVersion = FileVersionInfo.GetVersionInfo(appMainType.Module.FullyQualifiedName);
                      }
                      finally {
                          CodeAccessPermission.RevertAssert();
                      }
                  }
                  else {
                      appFileVersion = FileVersionInfo.GetVersionInfo(ExecutablePath);
                  }
              }
          }
          return (FileVersionInfo)appFileVersion;
      }

 

 


  private static Type GetAppMainType(){
      #if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
      //PC:

      Assembly entryAssembly = GetEntryAssembly();
    
      return (entryAssembly != null)?
          //The 'EntryPoint' property is not available 
          //on CE:
          entryAssembly.EntryPoint.ReflectedType:
          null;
  #else
          throw new NotSupportedException("Env.GetAppMainType");
  #endif
      }
        */

        #endregion

        /// <summary>
        ///   The name of the application using the 
        ///   custom membership provider.
        ///   <para>
        ///     Default value is '/'
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     It is essential that the ApplicationName is set.
        ///     See <see href = "http://weblogs.asp.net/scottgu/archive/2006/04/22/443634.aspx" /> for why.
        ///   </para>
        /// </remarks>
        /// <internal>
        ///   <![CDATA[
        /// (".appDomain", "*");   138 appdomain.SetData (".appDomain", "*"); 
        /// 118 appdomain.SetData (".appPath", physicalDir);   139 appdomain.SetData (".appPath", physicalDir); 
        /// 119 appdomain.SetData (".appVPath", virtualDir);   140 appdomain.SetData (".appVPath", virtualDir); 
        /// 120 appdomain.SetData (".domainId", domain_id);   141 appdomain.SetData (".domainId", domain_id); 
        /// 121 appdomain.SetData (".hostingVirtualPath", virtualDir);   142 appdomain.SetData (".hostingVirtualPath", virtualDir); 
        /// 122 appdomain.SetData (".hostingInstallDir", Path.GetDirectoryName (typeof (Object).Assembly.CodeBase)); 
        /// ]]>
        /// </internal>
        /// <value></value>
        /// <returns>The name of the application using the custom membership provider.</returns>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string ApplicationName
        {
            get
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                //Reference to Web dll's only happens if on PC or server, not CE:
                HttpContext webContext = HttpContext.Current;
                if (webContext != null)
                {
                    //We are definately in a web application, so its safe to refer to these
                    //properties:
                    if ((string.IsNullOrEmpty(_ApplicationName)) || (_ApplicationName == "/"))
                    {
                        //Use alternate way that leaves no binding on System.Web.Hosting
                        //_ApplicationName = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
                        //The GetData(".appVPath") comes back as something like "/MyWebSite" (no final slash):
                        return (string) AppDomain.CurrentDomain.GetData(".appVPath");
                        //For the record, GetData(".appPath") comes back With a slash at the end..."D:\SYS\PROFILES\S\MYDOCUMENTS\CODE\MyWebSite\"
                    }
                    if (_ApplicationName == "~")
                    {
                        //This means we are suppossed to work with the webContext hashtable 
                        //using a key of C_HTTPCONTEXT_APPLICATIONNAME to store the value:
                        string o = (string) webContext.Items[C_HTTPCONTEXT_APPLICATIONNAME];
                        return (string.IsNullOrEmpty(o)) ? "/" : o;
                    }

                    //We are expected to use the normal value:
                    return _ApplicationName;
                }
                //We are in PC, but not in IIS:
#endif
                //We are in PC or CE:
                //And we are NOT in a web application:
                if ((string.IsNullOrEmpty(_ApplicationName)) || (_ApplicationName == "/") || (_ApplicationName == "~"))
                {
                    //We are using the default value...which should fall back on the name of the application.
                    //Note that it must not remain a "/" as it messes up the path.
                    //And must be differentiated from other applications in the 
                    //SpecialFolder.ApplicationData folder...
                    //Comes back as "MyApp.exe", no slashes. With extension.
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                    //PC:
                    _ApplicationName = (string) AppDomain.CurrentDomain.GetData("APP_NAME");
#else
    //In CE:
          _ApplicationName = Path.GetFileName(GetEntryAssemblyName());
#endif
                }

                if (string.IsNullOrEmpty(_ApplicationName))
                {
                    _ApplicationName = AppDomain.CurrentDomain.SetupInformation.ApplicationName;
                }
                if (string.IsNullOrEmpty(_ApplicationName))
                {
                    Assembly a = GetEntryAssembly();
                    _ApplicationName = a.GetName().Name;
                }

                return _ApplicationName;
            }
            set
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                //Reference to Web dll's only happens if on PC or server, not CE:
                if (_ApplicationName == "~")
                {
                    //This means we are suppossed to work with the webContext hashtable 
                    //using a key of C_HTTPCONTEXT_APPLICATIONNAME to store the value:
                    HttpContext webContext = HttpContext.Current;
                    if (webContext != null)
                    {
                        webContext.Items[C_HTTPCONTEXT_APPLICATIONNAME] = value;
                        return;
                    }
                }
#endif
                if (value != _ApplicationName)
                {
                    _ApplicationName = value;
                }
            }
        }

        private static string _ApplicationName;


        /// <summary>
        ///   Replace '|DataDirectory|' in the given path, with the path to the application's designated Data directory.
        ///   <para>
        ///     If not rooted, can be prepended with the <see cref = "AppDir" />.
        ///   </para>
        /// </summary>
        /// <param name = "tmpPath">The path to parse.</param>
        /// <param name = "ensureRooted">Whether or not to ensure the string is rooted.</param>
        /// <returns>The expanded path.</returns>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string ExpandDataDirectoryMacro(string tmpPath, bool ensureRooted)
        {
            if (string.IsNullOrEmpty(tmpPath))
            {
                return string.Empty;
            }
            //CASE INSENSITIVE REPLACEMENT OF |DATADIRECTORY| MACRO:
            string dataDirectoryPattern = "\\|DataDirectory\\|";
            if (Regex.IsMatch(tmpPath, dataDirectoryPattern, RegexOptions.IgnoreCase))
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                object dataDirectoryObject = AppDomain.CurrentDomain.GetData("DataDirectory");
                string dataDirectory =
                    (dataDirectoryObject != null)
                        ? dataDirectoryObject + @"\"
                        : AppDir + @"\Data\";
#else
    //CE:
        string dataDirectory = AppDir + @"\Data\";
#endif
                tmpPath = Regex.Replace(tmpPath, dataDirectoryPattern, dataDirectory, RegexOptions.IgnoreCase);
            }
            return (ensureRooted) ? EnsureDirectoryRooted(tmpPath) : tmpPath;
        }


        /// <summary>
        ///   Ensures that the given path, if not rooted, is 
        ///   prepended with the <see cref = "AppDir" />.
        /// </summary>
        /// <param name = "tmpPath"></param>
        /// <returns></returns>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string EnsureDirectoryRooted(string tmpPath)
        {
            if (!Path.IsPathRooted(tmpPath))
            {
                tmpPath = Path.Combine(AppDir, tmpPath);
            }
            return tmpPath;
        }

        #region GetEntryAssembly

        /// <summary>
        ///   Cross platform (Win/Web/CE) solution for getting the EntryAssembly.
        /// </summary>
        /// <returns>The Assembly</returns>
        private static string GetEntryAssemblyName()
        {
            string result;
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
            //PC:
            //http://blogs.msdn.com/asanto/archive/2003/09/08/26710.aspx
            //says to never trust EntryAssembly(), but rather, trust:
            result = Process.GetCurrentProcess().ProcessName;
#else
    //CE:
      StringBuilder sb = null;
      IntPtr hModule = GetModuleHandle(IntPtr.Zero);
      if (IntPtr.Zero != hModule) {
        sb = new StringBuilder(255);
        if (0 == GetModuleFileName(hModule, sb, sb.Capacity)) {
          sb = null;
        }
      }
      result = sb.ToString();
#endif
            return result;
        }


        /// <summary>
        ///   Cross platform (Win/Web/CE) solution for getting the name of the EntryAssembly.
        /// </summary>
        /// <returns>The name of the assembly</returns>
        public static Assembly GetEntryAssembly()
        {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
            //PC:
            //http://blogs.msdn.com/asanto/archive/2003/09/08/26710.aspx
            //says to never trust EntryAssembly(), but rather, trust:
            return Assembly.GetEntryAssembly();
            //result = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
#else
    //CE:

            System.Text.StringBuilder sb = 
      new System.Text.StringBuilder(0x100 * System.Runtime.InteropServices.Marshal.SystemDefaultCharSize);
      int returnedSize = GetModuleFileName(IntPtr.Zero, sb, 0xff);
      if (returnedSize <= 0) {
        return null;
      }
      if (returnedSize > 0xff) {
        throw new System.Exception("Returned Assembly name longer than MAX_PATH chars.");
      }
      return System.Reflection.Assembly.LoadFrom(sb.ToString());
#endif
        }

        #region Hardware Profiles

        //NOT NEEDED IN XAct.Web.Core

        #endregion

        #region Hardware Profiles - CF Only

        //NOT NEEDED IN XAct.Web.Core

        #endregion

        #region API

#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
        //PC:
#else
    [DllImport("coredll.dll")] 
    private static extern int GetDeviceUniqueID(
      [In, Out] byte[] appdata, int cbApplictionData, 
      int dwDeviceIDVersion, 
      [In, Out] byte[] deviceIDOuput, 
      ref uint pcbDeviceIDOutput);
#endif

        #endregion

        #region API

#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
        //PC:
        [DllImport("kernel32.dll", SetLastError = true)]
#else
    //CE:
    [System.Runtime.InteropServices.DllImport("coredll.dll", SetLastError = true)]
#endif
        private static extern int GetModuleFileName(IntPtr hModule, StringBuilder lpFilename, int nSize);


#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
        //PC:
        [DllImport("Kernel32.dll", SetLastError = true)]
#else
    //CE:
    [System.Runtime.InteropServices.DllImport("CoreDll.dll", SetLastError = true)]
#endif
        private static extern IntPtr GetModuleHandle(IntPtr ModuleName);

        #endregion

        #endregion

        /*OLDER SOLUTION:

    private static string GetEntryAssemblyName() {
      string result;
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
    //PC:
      //http://blogs.msdn.com/asanto/archive/2003/09/08/26710.aspx
      //says to never trust EntryAssembly(), but rather, trust:
  result = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
#else
     * //CE:
      StringBuilder sb = null;
      IntPtr hModule = GetModuleHandle(IntPtr.Zero);
      if (IntPtr.Zero != hModule) {
        sb = new StringBuilder(255);
        if (0 == GetModuleFileName(hModule, sb, sb.Capacity)) {
          sb = null;
        }
      }
      result = sb.ToString();
#endif
      return result;
    }

#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
    //PC:
[System.Runtime.InteropServices.DllImport("Kernel32.dll", SetLastError = true)]
public static extern IntPtr GetModuleHandle(IntPtr ModuleName);

[System.Runtime.InteropServices.DllImport("Kernel32.dll", SetLastError = true)]
public static extern Int32 GetModuleFileName(IntPtr hModule, StringBuilder ModuleName, Int32 cch);
#else
[System.Runtime.InteropServices.DllImport("CoreDll.dll", SetLastError = true)]
public static extern IntPtr GetModuleHandle(IntPtr ModuleName);

[System.Runtime.InteropServices.DllImport("CoreDll.dll", SetLastError = true)]
public static extern Int32 GetModuleFileName(IntPtr hModule, StringBuilder ModuleName, Int32 cch);
#endif
     */

        //END


        /*
                //See: System.Web.UI.Design.WebControls.LoginDesigner
        private void LaunchWebAdmin()
        {
            IDesignerHost host1 = (IDesignerHost)this.GetService(typeof(IDesignerHost));
            if (host1 != null)
            {
                IWebAdministrationService service1 = (IWebAdministrationService)host1.GetService(typeof(IWebAdministrationService));
                if (service1 != null)
                {
                    service1.Start(null);
                }
            }
        }
         */


        /// <summary>
        ///   Return BrowserCap object
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Among other things, one can use the BrowserCap object to check 
        ///     whether the Client browser can support Javascript.
        ///   </para>
        /// </remarks>
        public static HttpBrowserCapabilities BrowserCaps
        {
            get
            {
                if (_BrowserCaps == null)
                {
                    _BrowserCaps = new HttpBrowserCapabilities();
                }
                return _BrowserCaps;
            }
        }

        //etc...
        private static HttpBrowserCapabilities _BrowserCaps = new HttpBrowserCapabilities();


        /// <summary>
        ///   Sets threads culture info to match the client's CultureInfo 
        ///   according to his browser language settings
        ///   (and returns it).
        /// </summary>
        /// <returns></returns>
        public static CultureInfo GetClientCultureInfo()
        {
            //Get the CI of the current thread:
            CultureInfo tResult = Thread.CurrentThread.CurrentCulture;

            //Get the Current Http Context:
            HttpContext hc = HttpContext.Current;
            if (hc == null)
            {
                //no context
                //means no client
                //means no cient culture..duh:
                return tResult;
            }

            //Get client culture
            //from client browser language codes:
            if (hc.Request.UserLanguages.Length > 0)
            {
                //Get the first language
                string tLangCode = hc.Request.UserLanguages[0];
                //Get that culture:
                CultureInfo tCI2 =
                    CultureInfo.CreateSpecificCulture(tLangCode);

                if (tResult != tCI2)
                {
                    //Hey! its different...set the thread to match it...
                    tResult = Thread.CurrentThread.CurrentCulture = tCI2;
                    //And return it...
                }
            }

            return tResult;
        }
    }


}


