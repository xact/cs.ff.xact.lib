// ReSharper disable CheckNamespace

namespace XAct.UI.Web.Tools
// ReSharper restore CheckNamespace
{
    using System;
    using System.IO;
    using System.Reflection;

    /// <summary>
    ///   Class of static helper functions for 
    ///   working with embedded resources.
    /// </summary>
    public class ResourceTools
    {
        /// <summary>
        ///   Get Resource stream with given 
        ///   <c>ResourcePath</c>, by searching all Assemblies.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     ResourcePath is not case-sensitive.
        ///   </para>
        ///   <para>
        ///     To save a little time, you can set the 
        ///     <paramref name = "skipSystemAssemblies" /> to 
        ///     true, in order to skip all "System.*" assemblies
        ///     when looking for your custom resources.
        ///   </para>
        /// </remarks>
        /// <param name = "givenResourcePath">
        ///   ResourcePath to embedded Resource.
        /// </param>
        /// <param name = "foundAssembly">
        ///   Assembly that the Resource was found in.
        /// </param>
        /// <param name = "foundFullResourcePath">
        ///   Full, ResourcePath at which Resource was found in assembly.
        /// </param>
        /// <param name = "skipSystemAssemblies">
        ///   Skip the System assemblies to save a little time.
        /// </param>
        /// <returns>
        ///   Returns Stream if found.
        /// </returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "givenResourcePath" />
        ///   is null or empty.
        /// </exception>
        public static Stream GetResourceStream(
            string givenResourcePath,
            bool skipSystemAssemblies,
            out Assembly foundAssembly,
            out string foundFullResourcePath)
        {
            //Check args:
            givenResourcePath.ValidateIsNotNullOrEmpty("givenResourcePath");

            //Prepare results arguments:
            foundFullResourcePath = string.Empty;
            foundAssembly = null;


            //Get all assemblies mounted in current domain:
            Assembly[] assemblies =
                AppDomain.CurrentDomain.GetAssemblies();

            //Loop through each assembly...
            foreach (Assembly assembly in assemblies)
            {
                //Skip System resources as they are usually huge:
                if ((skipSystemAssemblies)
                    &&
                    (assembly.GetName().Name.StartsWith("System")))
                {
                    continue;
                }


                //See if you can find a stream in there
                //with the given resourcePath:
                Stream resultStream =
                    GetResourceStream(
                        assembly,
                        givenResourcePath,
                        out foundFullResourcePath);

                if (resultStream == null)
                {
                    //No stream found in this 
                    //assembly...next assembly!
                    continue;
                }

                //Found!
                //Fill in results, and get out...
                foundAssembly = assembly;
                return resultStream;
            }

            //Not found in any assembly in domain...
            return null;
        }

        /// <summary>
        ///   Get Resource stream with given <c>ResourcePath</c>, 
        ///   by searching given Assembly.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Whereas 
        ///     <see cref = "GetResourceStream(string,bool, out System.Reflection.Assembly,out string)" />
        ///     was used to look for a resource stream in all Assemblies
        ///     in the current domain, this one looks within just
        ///     one assembly.
        ///   </para>
        /// </remarks>
        /// <param name = "assembly">Assembly to search within.</param>
        /// <param name = "givenResourcePath">PartialName of Resource.</param>
        /// <param name = "foundFullResourcePath">
        ///   returned full path of Resource.
        /// </param>
        /// <returns>Returns Stream, or null if not found.</returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "assembly" /> is null.
        /// </exception>
        public static Stream GetResourceStream(
            Assembly assembly,
            string givenResourcePath,
            out string foundFullResourcePath)
        {
            if (assembly.IsNull())
            {
                throw new ArgumentNullException("assembly");
            }

            foundFullResourcePath = string.Empty;


            givenResourcePath = givenResourcePath.ToLower();
            try
            {
                //Get a list of all resources in the assembly:
                string[] resourceNames =
                    assembly.GetManifestResourceNames();

                //Now loop through each one:
                foreach (string resourcePath in resourceNames)
                {
                    //In order to see if the resource path ends with.
                    //NOTE:
                    //It's important to note that its looking 
                    //for an equality match, but an ends with.
                    //Why?
                    //Because if you move/copy a control from one assembly
                    //to another (eg: XAct.Controls.XButton.sln to 
                    //XAct.Controls.AllButtons.sln, the resource paths
                    //will differ ...but only the first part.
                    //It comes in handy to just search for resources
                    //by what you know -- eg 'resource:XAct.UI.Web.WebControls.XButton'
                    //and not all the rest, which is part the folder where
                    //its moved to in the assembly, etc...
                    if (resourcePath.ToLower().EndsWith(givenResourcePath))
                    {
                        //Found a name that might be a good match...
                        //So get the resource schema:

                        //Return that the full path to the resource is the following:
                        //Ie, the full path, not just the given 'endswith' 
                        //part that we gave it to look for:
                        foundFullResourcePath = resourcePath;

                        //And we return the stream:
                        return assembly.GetManifestResourceStream(resourcePath);
                    }
                }
            }
            catch (Exception)
            {
            }
            //Not found...
            return null;
        }

//Method:End

        /// <summary>
        ///   Writes Resource stream to File
        /// </summary>
        /// <param name = "stream"></param>
        /// <param name = "absolutePath"></param>
        /// <param name = "overwriteOk">
        ///   Ok to overwrite what ever is already there.
        /// </param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "stream" />
        ///   is null.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "absolutePath" />
        ///   is null or empy.
        /// </exception>
        /// <exception cref = "System.ArgumentException">
        ///   An exception is raised if <paramref name = "absolutePath" />
        ///   is not an absolute physical path.
        /// </exception>
        public static bool WriteResourceToFile(
            Stream stream,
            string absolutePath,
            bool overwriteOk)
        {
            //Check Args:
            if (stream.IsNull())
            {
                throw new ArgumentNullException("stream");
            }
            if (absolutePath.IsNullOrEmpty())
            {
                throw new ArgumentNullException("absolutePath");
            }
            if (!UrlPathTools.IsAbsolutePhysicalPath(absolutePath))
            {
                throw new ArgumentException(
                    "absolutePath is not an Absolute Path: ".FormatStringCurrentUICulture(absolutePath));
            }

            //Ensure directory exists
            string absolutePathDirectory = Path.GetDirectoryName(absolutePath);
            if (absolutePathDirectory.IsNullOrEmpty())
            {
                throw new ArgumentNullException("absolutePath", "absolatePath Directory was null.");
            }
              Directory.CreateDirectory(absolutePathDirectory);

            bool result = false;

            if ((overwriteOk) || (!File.Exists(absolutePath)))
            {
                //Create buffer same size as stream:
                byte[] fileBuffer = new byte[stream.Length];
                //Read stream into buffer:
                stream.Read(fileBuffer, 0, (int) stream.Length);

                //Make secondary stream to write out to:
                FileStream fileStream = null;

                try
                {
                    //try writting to it:
                    fileStream =
                        File.Open(absolutePath,
                                  FileMode.Create,
                                  FileAccess.Write,
                                  FileShare.None);

                    fileStream.Write(fileBuffer, 0, (int) stream.Length);
                    result = true;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    stream.Close();
                    if (fileStream != null)
                    {
                        fileStream.Close();
                    }
                }
            }
            return result;
        }

        /*
		 * OLD:
		 * 		public static bool GetTextResource(string qResourceName, string qPath, out string tResult){
			bool tFromFile = false;
			tResult = string.Empty;
			Stream tS = null;
			StreamReader tSR = null;
			tResult = string.Empty;

			string tPath_FileSys = qPath;
			if (!tPath_FileSys.StartsWith("~/")){tPath_FileSys = "~/" + tPath_FileSys;}
			string tPath_Resource = qPath;
			if (tPath_Resource.StartsWith("~/")){tPath_Resource=tPath_Resource.Substring(2);}
			tPath_Resource = tPath_Resource.Replace("/",".");

			System.Reflection.Assembly tA = System.Reflection.Assembly.GetExecutingAssembly();
			string tBaseName = Path.GetFileNameWithoutExtension(tA.EscapedCodeBase); 
			tS = tA.GetManifestResourceStream(tBaseName + "." + tPath_Resource + qResourceName);
			if (tS == null){
				throw new System.Exception("Resource file was not found: " + tBaseName + tPath_Resource + qResourceName);
			}

			try {
				if (File.Exists(tPath_FileSys + qResourceName)==true){
					tSR=File.OpenText(tPath_FileSys + qResourceName);
					if (tSR != null) {
						tResult = tSR.ReadToEnd();
						tFromFile=true;
					}
				}
			}
			catch {}
			finally{
				if (tSR != null){tSR.Close();}
			}

			if (tFromFile == false){
				tSR = new StreamReader(tS);
				tResult = tSR.ReadToEnd();
				tSR.Close();
			}
			return tFromFile;
		}
		 */


        /*
		/// <summary>
		/// Gets the Client relativized URL for the Resource in the *.resx file specified.
		/// See Remarks.
		/// </summary>
		/// <remarks>
		/// <para>
		/// <code>
		/// <![CDATA[
		/// override void Render(){
		///   ...
		///   myImg.Src = GetResourceUrl(this,
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		/// <param name="control">The control.</param>
		/// <param name="resourceManager">The resource manager.</param>
		/// <param name="resourceKey">The resource key.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <returns></returns>
    [Obsolete]
    public static string GetResourceUrl(Control control, 
      System.Resources.ResourceManager resourceManager, 
      string resourceKey, 
      string fileExtension) {
      
      //Check Args:
      if (control == null) {
        throw new System.ArgumentNullException("control");
      }
      if (resourceManager == null) {
        throw new System.ArgumentNullException("resourceManager");
      }
      if (string.IsNullOrEmpty(resourceKey)) {
        throw new System.ArgumentNullException("resourceKey");
      }
      if (string.IsNullOrEmpty(fileExtension)) {
        throw new System.ArgumentNullException("fileExtension");
      }

      Page currentWebPage = control.Page;
      if (currentWebPage == null) {
        throw new System.Exception("::Cannot use GetResourceUrl until Control is added to a Page.");
      }

      ResourceCacheSchema resourceInfo;
      string resourcePath = resourceManager.BaseName + "." + resourceKey;
      if (ResourceCache.ContainsKey(resourcePath)){

        resourceInfo = ResourceCache[resourcePath];
        return currentWebPage.ResolveClientUrl(resourceInfo.Url);
      }

      //Means we havn't ever processed this resource
      //during this session...
      //so let's try to figure out 
      //what would be the physical pathwhere we are going to save it
      //what the url will be to represent it:

      resourceInfo = new ResourceCacheSchema();
      resourceInfo.ResourcePath = resourcePath;

      //Save it:
      ResourceCache[resourcePath] = resourceInfo;

      //Start building physicalpath:
      string urlDirectory = "~/ClientResources/" + 
        control.GetType().Name + "/" + fileExtension + "/";

      string physicalPathDirectory = System.Web.HttpContext.Current.Server.MapPath(urlDirectory);
      string fileName = resourceKey + "." + fileExtension;
      //Ensure directory exists first:
      Directory.CreateDirectory(physicalPathDirectory);

      //Determine type of file:
      object o = resourceManager.GetObject(resourceKey);
      if (o == null) {
        throw new System.ArithmeticException(
          string.Format("::resourceKey {1} not found in {0}.", resourceManager.BaseName, resourceKey));
      }
      if (o is System.Drawing.Bitmap) {
        System.Drawing.Bitmap bmp = (System.Drawing.Bitmap)o;
        fileName = resourceKey + "." + fileExtension;
        resourceInfo.PhysicalPath = physicalPathDirectory + fileName;
        resourceInfo.Url =  urlDirectory + fileName;
        System.Type t = typeof(System.Drawing.Imaging.ImageFormat);
        System.Drawing.Imaging.ImageFormat imgFileFormat;
        switch (fileExtension) {
          case "bmp":
            imgFileFormat = System.Drawing.Imaging.ImageFormat.Bmp;
            break;
          case "gif":
            imgFileFormat = System.Drawing.Imaging.ImageFormat.Gif;
            break;
          case "jpg":
            imgFileFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
            break;
          case "png":
            imgFileFormat = System.Drawing.Imaging.ImageFormat.Png;
            break;
          default:
            imgFileFormat = System.Drawing.Imaging.ImageFormat.Png;
            break;
        }


        if (!File.Exists(resourceInfo.PhysicalPath)) {
          int w = bmp.Width;
          int h = bmp.Height;
          System.Drawing.Color transparentColor = bmp.GetPixel(8,8);//0, h - 1);

          System.Drawing.Bitmap backgroundColor = new System.Drawing.Bitmap(16, 16);
          System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(backgroundColor);
          g.FillRectangle(new System.Drawing.SolidBrush(transparentColor),new System.Drawing.Rectangle(0,0,15,15));
          backgroundColor.MakeTransparent(System.Drawing.Color.White);

          backgroundColor.Save(physicalPathDirectory + "color.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
          backgroundColor.Save(physicalPathDirectory + "color.gif", System.Drawing.Imaging.ImageFormat.Gif);

          //bmp.MakeTransparent(transparentColor);
          //The quick lousy way of dropping from 32Bpp to 8BIndexed...

          bmp.Save(resourceInfo.PhysicalPath, imgFileFormat);
          bmp = new System.Drawing.Bitmap(resourceInfo.PhysicalPath);
          System.Drawing.Imaging.PixelFormat pixelFormat = bmp.PixelFormat;
          transparentColor = bmp.GetPixel(0, h-1);//0, h - 1);
          bmp.MakeTransparent(transparentColor);
          //bmp.Save(resourceInfo.PhysicalPath, imgFileFormat);


        }
      }
      if (o is System.String) {
        string fileContents = (string)o;
        resourceInfo.PhysicalPath = physicalPathDirectory + fileName;
        resourceInfo.Url = urlDirectory + fileName;

        if (!File.Exists(resourceInfo.PhysicalPath)) {
          File.WriteAllText(resourceInfo.PhysicalPath, fileContents);
        }
      }
      return currentWebPage.ResolveClientUrl(resourceInfo.Url);
    }




    [Obsolete]
    protected class ResourceCacheSchema {
      public string ResourcePath;
      public string PhysicalPath;
      public string Url;
      public bool Exists;

    }

    [Obsolete]
    protected static System.Collections.Generic.Dictionary<string, ResourceCacheSchema> ResourceCache {
      get {
        System.Collections.Generic.Dictionary<string, ResourceCacheSchema> resourceCache;

        resourceCache = (System.Collections.Generic.Dictionary<string, ResourceCacheSchema>)System.Web.HttpContext.Current.Session["_ResourceCache"];
        if (resourceCache == null) {
          System.Web.HttpContext.Current.Session["_ResourceCache"] =
        new System.Collections.Generic.Dictionary<string, ResourceCacheSchema>();
          resourceCache = (System.Collections.Generic.Dictionary<string, ResourceCacheSchema>)System.Web.HttpContext.Current.Session["_ResourceCache"];
        }
        return resourceCache;

      }
    }
*/
    }


}


