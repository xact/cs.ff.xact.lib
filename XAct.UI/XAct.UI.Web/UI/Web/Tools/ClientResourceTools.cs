namespace XAct.UI.Web.Tools
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.UI;

    ///<summary>
    ///  Helper class to manage the process of including 
    ///  ClientSide Resources such <c>Script</c> 
    ///  and <c>Css</c>, and 
    ///  <c>Image</c> files. 
    ///  <para>
    ///    See Remarks.
    ///  </para>
    ///</summary>
    ///<remarks>
    ///  <para>
    ///    This class was developed to solve shortcomings in the Framework's
    ///    strategy for handling client side resources. 
    ///    Currently, the solutions offered are each interesting, 
    ///    technically, but do cause unnecessary difficulties 
    ///    when developing server controls.
    ///    These static methods solve some or all of these problems.
    ///  </para>
    ///  <para>
    ///    <b>About ASP.NET 1.1 Resource Handling:</b>
    ///    <br />
    ///    There wasn't any built in system -- so everyone had to 
    ///    roll their own IHttpHandler.
    ///    See <see cref = "XAct.UI.Web.HttpHandlers.ResourceHttpHandler" />.
    ///  </para>
    ///  <para>
    ///    <b>About ASP.NET 2.0 Resource Handling:</b>
    ///    With the next version of ASP.NET, MS offered a built in 
    ///    ResourceHandler that only required
    ///    a combination of a couple of steps:
    ///    <br />
    ///    <b>Step 1:</b><br />
    ///    Embedding the resource into the file by dropping a 
    ///    JavaScript file directly into the project
    ///    and right-clicking it to mark as an 'embedded' resource:
    ///    <code>
    ///      <![CDATA[
    ///	XAct/
    ///		Resources/
    ///			W3C/
    ///				W3C.js
    /// ]]>
    ///    </code>
    ///    <b>Step 2:</b><br />
    ///    Marking the resource as safe to access via an assembly
    ///    wide attribute after converting the above url to 
    ///    a 'ResourcePath' 
    ///    ('.' in folder names converted to '_', 
    ///    folder separators converted to '.' -- 
    ///    see <see cref = "ConvertFilePathToResourcePath" /> for 
    ///    more details about this process). 
    ///    An example would be:
    ///    <code>
    ///      <![CDATA[
    /// [assembly: System.Web.UI.WebResourceAttribute(
    ///   "XActResources.W3C.W3C.js", 
    ///    "application/x-javascript")]
    /// ]]>
    ///    </code>
    ///    <b>3. Refer to the Resource in your code:</b><br />
    ///    All that is left to do is refer to the embedded
    ///    resource via the same ResourcePath:
    ///    <code>
    ///      <![CDATA[
    /// clientUrl = 
    ///   control.Page.ClientScript.GetWebResourceUrl(
    ///     this.GetType(),"XAct.Resources.W3C.W3C.js");
    /// ]]>
    ///    </code>
    ///    Unfortunately, the NET 2.0 solution still has 
    ///    at least two problems...
    ///  </para>
    ///  <para>
    ///    <b>The Resource Url is dependant 
    ///      on the Assembly's <c>Default Namespace.</c></b><br />
    ///    The first problem is that this string based approach
    ///    means that if you SVN or CSV your control's code
    ///    to build it into different assemblies depending 
    ///    on your needs, you will have different paths even 
    ///    if the folder structure is identical.
    ///    In other words, you will have to 
    ///    modify <see cref = "WebResourceAttribute" /> 
    ///    for your different builds, causing unnecessary 
    ///    churning/commits in your SVN/CSV system:
    ///    <br />
    ///    Even if in both cases you use an 
    ///    <c>XAct\Web\Controls\Resources\</c> folder, the
    ///    names will change:
    ///    <code>
    ///      <![CDATA[
    /// In MySingleControl assembly:
    /// [assembly: System.Web.UI.WebResourceAttribute(
    ///   "MySingleControl.XAct.UI.Web.WebControls.Resources.MyScript.js", 
    ///   "application/x-javascript")]
    /// 
    /// and in MyControlLibrary assembly:
    /// [assembly: System.Web.UI.WebResourceAttribute(
    ///   "MyControlLibrary.XAct.UI.Web.WebControls.Resources.MyScript.js", 
    ///   "application/x-javascript")]
    /// ]]>
    ///    </code>
    ///  </para>
    ///  <para>
    ///    <b>WebDeveloper Express can't use BuiltIn Resources.</b><br />
    ///    The second problem is that this technique 
    ///    does not work with <c>Web Developer Express</c> because
    ///    <c>WDE</c> does not offer a right-click to embed resources.
    ///    <br />
    ///    This means that code will be written for WDE cannot be compiled
    ///    directly on Visual Studio, and vice-versa, without modification.
    ///    <br />
    ///    <b>Embedded Resources offer less flexibility for EndUsers:</b><br />
    ///    A third problem is that Embedded resources are nice -- but cause
    ///    undue hardship on the end programmer who may or may not like
    ///    your choice of graphics, etc.
    ///  </para>
    ///  <para>
    ///    <b>The Basis for a Solution:</b>
    ///    A solution to this dilemma is to remember the solutions that
    ///    were tried in NET 1.1 and mix and match ideas.
    /// 
    ///    The solution is to be ready for VS2005-style embedded resources
    ///    AND WDExpress-style plain old files.
    ///    <br />
    ///    How this is done, is if the resource is embedded(VS2005-style),
    ///    the first time it is accessed, it dumps the resource to the
    ///    hard-drive on the server, and then refer to these hard-drive
    ///    copies (VDExpress-Style) rather than the embedded resources.
    ///  </para>
    /// 
    /// 
    ///  <para>
    ///    This is exactly how
    ///    <see cref = "GetWebResourceUrl" /> was designed.
    ///    Its default behavior is if it is passed the url of a file on hard-drive,
    ///    it will return the url to the file on the hard-drive, without
    ///    looking for an embedded resource.
    ///    <br />
    ///    On the other hand, if passed the url for an embedded resource
    ///    (eg: 'resource:Web.Controls.MyControl.MyScript.js'), 
    ///    the first time it will not find a copy on the hard-drive, so 
    ///    will copy the resource to the hard-drive, then return 
    ///    the url to that HD file.
    ///    <br />
    ///    From then on, the file is served quickly without requiring
    ///    a Handler to server it.
    ///  </para>
    ///  <para>
    ///    The only issues left to be addressed are:
    ///    a) *where* to dump the file on the hard-drive.
    ///    b) What notation to distinguish a Resource from a File path.
    ///  </para>
    ///  <para>
    ///    <b>The Final Destination</b>
    ///    If the path given is to a real file, it is served directly
    ///    without any modification. 
    ///    <br />
    ///    On the other hand, if the path is a 
    ///    ResourcePath ('resource:myfile.js'), 
    ///    it will be dumped to the the following location
    ///    before being served from there:
    ///    <code>
    ///      <![CDATA[
    /// ~\ClientResources\{ControlFullName}\
    /// Eg:
    /// ~\ClientResources\XAct.UI.Web.WebControls.MyControl\MyScript.js
    /// ]]>
    ///    </code>
    ///  </para>
    ///  <para>
    ///    <b>Deciding on the right ResourcePath:</b><br />
    ///    The types of paths it accepts are:
    /// 
    ///    For a real file:
    ///    <code>
    ///      \RealDirectory\RealFile.js
    ///      or 
    ///      ~\RealDirectory\RealFile.js
    ///    </code>
    ///    Or for an embedded resource:
    ///    <code>
    ///      "resource:MyControl.Resources.MyScript.js"
    ///      or:
    ///      "{MyControl.Resources.MyScript.js}"
    ///      or:
    ///      "#MyControl.Resources.MyScript.js#
    ///    </code>
    ///  </para>
    ///</remarks>
    public class ClientResourceTools
    {
        #region Resources

        /// <summary>
        /// 
        /// </summary>
        public static class Constants
        {
            /// <summary>
            ///   The resource Prefix used to designate that the rest of the path 
            ///   is an embedded resource.
            ///   <para>
            ///     Value is 'resource:'
            ///   </para>
            /// </summary>
            public const string ResourcePrefix = "resource:";

            /// <summary>
            ///   The Directory where the resources will be dumped.
            /// </summary>
            public const string ResourceDumpDir = "ClientResources/";
        }

        #endregion

        #region Properties

        /// <summary>
        ///   Gets the common cache of 
        ///   <see cref = "ResourceCacheSchema" /> instances.
        /// </summary>
        /// <value>The resource cache.</value>
        private static Dictionary<string, ResourceCacheSchema> ResourceCache
        {
            get
            {
                Dictionary<string, ResourceCacheSchema> resourceCache;

                if (HttpContext.Current == null)
                {
                    //Make a dummy:
                    resourceCache = new Dictionary<string, ResourceCacheSchema>();
                }
                else
                {
                    resourceCache = (Dictionary<string, ResourceCacheSchema>)
                                    HttpContext.Current.Session["__ResourceCache"];

                    if (resourceCache == null)
                    {
                        resourceCache = new Dictionary<string, ResourceCacheSchema>();
                        HttpContext.Current.Session["__ResourceCache"] =
                            resourceCache;
                    }
                }
                return resourceCache;
            }
        }

        //static Dictionary<string, ResourceCacheSchema> resourceCache;

        #endregion

        #region Public Static Methods

        /// <summary>
        ///   Gets the default resource base path,
        ///   <para>
        ///     For a control called '<c>MyControl</c>', 
        ///     that will be '<c>resources:Resources.MyControl.</c>'
        ///   </para>
        ///   <para>
        ///     Note that the result ends with a dot.
        ///   </para>
        /// </summary>
        /// <param name = "type">The type.</param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "type" /> 
        ///   is null.
        /// </exception>
        public static string GetDefaultResourceBasePath(Type type)
        {
            type.ValidateIsNotDefault("type");

            return string.Format(
                "{0}Resources.{1}.",
                Constants.ResourcePrefix,
                type.Name);
        }


        /// <summary>
        ///   Creates a Client ready Url back to a Resource on the
        ///   server (Script, Image, etc). See Remarks.
        ///   <para>
        ///     Accepts both virtual url 
        ///     ('<c>~/Images/MyControl/image.gif</c>')
        ///     or ResourcePath 
        ///     ('<c>resource:MyControl.Resources.MyFile.js</c>'
        ///     returning an Url to the resource, 
        ///     relative to the current page.
        ///   </para>
        ///   <para>
        ///     Optionally, if the <paramref name = "virtualPathOrResourceName" />
        ///     has no directory info and is just a filename, 
        ///     appends the first non-null member of the 
        ///     <paramref name = "baseUrlsIfAny" /> argument.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     The relativeUrl can be a relative url to file
        ///     (ie: <c>"~/Images/Controls.My/image.gif"</c>)
        ///     or a ResourcePath:
        ///     "resource:MyControl.Resources.MyFile.js"
        ///   </para>
        ///   <para>
        ///     The <para>baseUrlsIfAny</para> is generally 
        ///     there for just one prefix path:
        ///     <code>
        ///       <![CDATA[
        /// GetWebResourceUrl("MyImg.gif", _BaseUrl);
        /// ]]>
        ///     </code>
        ///     but can be packed with a list of dirs so that it 
        ///     falls back to the more general params if not set:
        ///     <code>
        ///       <![CDATA[
        /// //If _Images.BaseUrl is not set, fall back to _BaseUrl:
        /// GetWebResourceUrl("MyImg.gif", _Images.BaseUrl, _BaseUrl);
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "control"></param>
        /// <param name = "virtualPathOrResourceName">
        ///   Relative Url to Resource (see Remarks) or 
        ///   hard-drive.
        /// </param>
        /// <param name = "baseUrlsIfAny">
        ///   An optional list of base directories to use. 
        ///   Uses the *first* non-empty one...
        /// </param>
        /// <returns></returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "control" />
        ///   is null.
        /// </exception>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if 
        ///   <paramref name = "virtualPathOrResourceName" />
        ///   is empty.
        /// </exception>
        public static string GetWebResourceUrl(Control control,
                                               string virtualPathOrResourceName,
                                               params string[] baseUrlsIfAny)
        {
            //DebugInIDE(virtualPathOrResourceName);

            //----------------------
            //STEP:  Check Args:
            control.ValidateIsNotDefault("control");
            virtualPathOrResourceName.ValidateIsNotNullOrEmpty("virtualPathOrResourceName");
            //----------------------
            //Clean up the name first:
            virtualPathOrResourceName = virtualPathOrResourceName.Trim();

            //----------------------
            //Get the extension of the file
            //Which is a way to check that we've been given a valid file name
            //and not a folder Path by accident:
            string fileExtension = Path.GetExtension(virtualPathOrResourceName);

            if (fileExtension.IsNullOrEmpty())
            {
                throw new Exception(
                    "Invalid virtualPathOrResourceName: has no Extension.");
            }
            //----------------------
            //STEP:  Do we have any base Paths specified -- if so, Prepend
            //with first one that is valid:
            string baseUrl = string.Empty;
            if (baseUrlsIfAny != null)
            {
                //Loop through optional base Urls
                //and exit after getting the first one that
                //is not null:
                for (int i = 0; i < baseUrlsIfAny.Length; i++)
                {
                    baseUrl = baseUrlsIfAny[i];
                    if (!string.IsNullOrEmpty(baseUrl))
                    {
                        break;
                    }
                }
                //Combine the path given with any prefix paths if any given:
                virtualPathOrResourceName =
                    UrlPathTools.Combine(
                        baseUrl,
                        virtualPathOrResourceName);
            }
            //----------------------
            //STEP: See if we have already processed this information
            //And if so, return the info without doing more work:

            //The whole idea is to not have to redo the work every time a file
            //is needed...so we use a static cache system...

            //Which contains pairs of [String Key = ResourceCacheSchema value] pairs

            Type controlType; //Type of the control that 'owns' this resource.
            string controlFullName; //The FullName of the Control.
            string cacheKey; //The unique ID for the resource.
            ResourceCacheSchema resourceInfo; //The cached info about the resource.
            //----------------------
            //STEP: Build a KEY for the static cache system
            //out of the name of the control + the script file name:
            controlType = control.GetType();
            controlFullName = controlType.FullName;
            cacheKey = controlFullName + "." + virtualPathOrResourceName;
            //DebugInIDE("CacheKey:"+ cacheKey);
            //----------------------
            //STEP: See if we have processed this before 
            //and the info is in the cache:
            if (!ResourceCache.TryGetValue(cacheKey, out resourceInfo))
            {
                //Build a new cached schema entry:
                resourceInfo =
                    BuildNewResourceCacheSchemaEntry(
                        control,
                        virtualPathOrResourceName);

                //Then save the entry for subsequent retrieval:
                ResourceCache[cacheKey] = resourceInfo;
            }
            //----------------------
            //Return the url of the control, relative to the 
            //control's page (if any):
            return (control.Page != null)
                       ? control.Page.ResolveClientUrl(resourceInfo.Url)
                       : resourceInfo.Url;
            //----------------------
        }

        #endregion

        #region Protected Static Methods

        /// <summary>
        ///   Converts a Relative or AppRelative url to a 
        ///   Resource Path. See Remarks.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Converts 
        ///     <c>'./..\..\../.././somwhere.silly/over/the/rainbow.gif'</c>
        ///     to 
        ///     <c>somewhere_silly.over.the.rainbow.gif</c>
        ///   </para>
        ///   <para>
        ///     Returns string.Empty if path is a Physical path.
        ///   </para>
        ///   <para>
        ///     Important Note:
        ///   </para>
        ///   <para>
        ///     When embedding a Resource into a project, 
        ///     Visual studio converts <c>"Images/Controls.My/image.gif"</c>
        ///     to a namespace notation of <c>"Images.Control_My.image.gif"</c>
        ///     (where <c>"."</c> is converted to <c>"_"</c>, 
        ///     and <c>"/"</c> converted to <c>"."</c>).
        ///   </para>
        ///   <para>
        ///     Unfortunately this <c>ResourcePath</c> format is impossible to 
        ///     convert back to an url without any ambiguity 
        ///     (eg: it can be <c>Images/Control_My/Image/gif</c>
        ///     or <c>Images/Control_My/Image.gif</c>...)
        ///     whereas the other direction (physical HD path to
        ///     resource path) is possible.
        ///     It is for this reason we opted on a solution that 
        ///     relies on paths to resources being
        ///     expressed as <c>"~/Images/Controls.My/image.gif"</c> rather
        ///     than <c>"Images.Control_My.image.gif"</c>. 
        ///     (see <see cref = "ConvertFilePathToResourcePath" />).
        ///   </para>
        /// </remarks>
        /// <param name = "relativeUrl"></param>
        public string ConvertFilePathToResourcePath(
            string relativeUrl)
        {
            //Check Args:
            relativeUrl.ValidateIsNotDefault("relativeUrl");
            if (UrlPathTools.IsAbsolutePhysicalPath(relativeUrl))
            {
                return string.Empty;
            }

            if (relativeUrl.Length > 2)
            {
                //Remove the leading tilda if any:
                string tmp = relativeUrl.Substring(0, 2);
                if ((tmp == "~/") || (tmp == "~\\"))
                {
                    relativeUrl = relativeUrl.Substring(2);
                }
            }
            string ext = Path.GetExtension(relativeUrl) ?? string.Empty;
            string path = relativeUrl.Substring(0, relativeUrl.Length - ext.Length);

            path = Regex.Replace(path, "(\\.{1,2}[\\\\/])*", "");
            path = Regex.Replace(path, "(\\.{1,2}[\\\\/])*", "");

            //path = Path.GetFileNameWithoutExtension(path);

            path = path.Replace(".", "_");
            path = path.Replace("\\", "/");
            path = path.Replace("/", ".");
            path = path + ext;

            return path;
        }


//        private static void DebugInIDE(string msg)
//        {
//            string fileName = "C:\\debug.txt";
//            //if (!ioService.Exists(fileName)){
        //            ioService.AppendAllText(
//                fileName,
//                DateTime.Now.ToShortTimeString()
//// ReSharper disable LocalizableElement
//                + ":" +
//// ReSharper restore LocalizableElement
//                msg +
//                System.Environment.NewLine);
//        }


        /// <summary>
        ///   Builds the new resource cache schema entry.
        /// </summary>
        /// <param name = "control">The control.</param>
        /// <param name = "virtualPathOrResourceName">
        ///   Name of the virtual path or resource.
        /// </param>
        /// <returns></returns>
        /// <exception cref = "System.IO.FileNotFoundException">
        ///   An exception is thrown if the Resource stream was not found
        /// </exception>
        private static ResourceCacheSchema BuildNewResourceCacheSchemaEntry(
            Control control,
            string virtualPathOrResourceName)
        {
            control.ValidateIsNotDefault("control");
            virtualPathOrResourceName.ValidateIsNotNullOrEmpty("virtualPathOrResourceName");

            //STEP: Build a new ResourceCacheInfo:

            //Never been processed before:
            //So we are going to be building a cache entry
            //that will save us time next time:
            //DebugInIDE("NotFound.");

            string resourceName;
            //bool resourceExists = false;
            bool fileExists = false;
            string physicalFileName;
            Assembly resourceAssembly; //Assembly of resource -- if found.
            string foundResourcePath = string.Empty; //Path to Resource -- if found.
            string virtualUrlDirectory;
            //The virtual dir where resource is written to (eg: "~/ClientResources/MyControl/Images/MyImage.gif").
            string physicalDirectory;
            //The physical dir where resource is written to (eg: "d:\wwwwroot\MyApp\ClientResources\MyControl\Images\MyImage.gif")
            string virtualUrlFileName;

// ReSharper disable SuggestUseVarKeywordEvident
            ResourceCacheSchema resourceInfo = new ResourceCacheSchema();
// ReSharper restore SuggestUseVarKeywordEvident

            //convert relative Url to physical path to
            //see if it exists on HD:
            resourceName = string.Empty;

            if (virtualPathOrResourceName.StartsWith(Constants.ResourcePrefix))
            {
                //if the resourcePath starts with 'resource:'
                //Chop it off:
                resourceName =
                    virtualPathOrResourceName.Substring(
                        Constants.ResourcePrefix.Length,
                        virtualPathOrResourceName.Length - Constants.ResourcePrefix.Length);
            }

            //Was it a Resource Path or a 'real/virtual' path?
            if (string.IsNullOrEmpty(resourceName))
            {
                //It was not a resource.
                //So it should be a physicalFileName of some kind...
                resourceInfo.Url = virtualPathOrResourceName;
                //To Map the dir, we use our helper function, which is
                //able to map even when in design mode, when the HttpContext == null:
                resourceInfo.PhysicalPath =
                    UrlPathTools.MapPath(control, resourceInfo.Url);

                resourceInfo.Exists = File.Exists(resourceInfo.PhysicalPath); //Needs to change...

                //Save our investigation, and get out:
                return resourceInfo;
            }

            //DebugInIDE("IsAResource");

            //it's a resource url:
            //eg: "MyControl.Resources.MyImage.gif"
            //or: "XAct.Web.MyControl.Resources.MyImage.gif"
            //We need a physical Directory to ensure the directory
            //exists:

            //This converts:
            // "myNamespace.MyControl.MyScript.js"
            //To:
            // "myNamespace/MyControl/MyScript.js" (leaves the last dot...)
            virtualUrlDirectory =
                Regex.Replace(resourceName, "(\\.)(?=[^\\.]+\\.)+", "/");

            virtualUrlDirectory = Path.GetDirectoryName(virtualUrlDirectory);
            virtualUrlDirectory =
                "~/" +
                Constants.ResourceDumpDir +
                virtualUrlDirectory.Replace("\\", "/");


            //virtualUrlDirectory =
            //  "~/ClientResources/" + control.GetType().Name + "/";
            //To Map the dir, we use our helper function, which is
            //able to map even when in design mode, when the HttpContext == null:
            physicalDirectory =
                UrlPathTools.MapPath(control, virtualUrlDirectory);

            //Create the *application's dump directory -- the 
            //root directory (eg: '/myAspNet/ClientResources/') 
            //where the resource streams will be dumped as files:
            CreateResourceDumpDirectory(physicalDirectory);

            // Extract from the resource path 
            // ('resource:MyControl.Resources.MyImage.gif')
            // the filenemame ('MyImage.gif')
            string resourceFileName =
                GetResourceFileName(virtualPathOrResourceName);


            physicalFileName = physicalDirectory + "\\" + resourceFileName;
            virtualUrlFileName = virtualUrlDirectory + "//" + resourceFileName;

            //DebugInIDE("PhysicalFileName:" + physicalFileName);
            if (!File.Exists(physicalFileName))
            {
                //No physical file yet -- we have to look 
                //for it in the assembly and see if we can find it:
                Stream stream = ResourceTools.GetResourceStream(
                    resourceName,
                    true,
                    out resourceAssembly,
                    out foundResourcePath);

                if (stream == null)
                {
                    //string[] check =
                    //    control.GetType().Assembly.GetManifestResourceNames();

                    //DebugInIDE("ERROR:" + "No Stream....BuildAction?");
                    throw new FileNotFoundException(
                        string.Format(
                            "::Resource not found:'{0}'." +
                            "(Maybe Resource's BuildAction was not set correctly?)",
                            virtualPathOrResourceName));
                }
                //Found the resource, Have its stream.
                //Need to write it out to the hard drive:
                //We only finally mark that there is a file when we
                //are able to successfully save to hard-drive:
                fileExists = ResourceTools.WriteResourceToFile(
                    stream,
                    physicalFileName,
                    false);

                if (!fileExists)
                {
                    //DebugInIDE("ERROR:" + "Unable to write stream to harddrive?");
                    throw new Exception(
                        string.Format(
                            "::ASP.NET Process does not have enough rights" +
                            " to write to hard-drive ('{0}').",
                            physicalFileName));
                }
            }

            //@@@resourceInfo.ResourcePath 
            resourceInfo.PhysicalPath = physicalFileName;
            resourceInfo.Url = virtualUrlFileName;
            resourceInfo.Exists = true;
            return resourceInfo;
        }


        /// <summary>
        ///   Gets the name of the resource file.
        ///   <para>
        ///     In other words, given 'resource:MyControl.Resources.MyImage.gif'
        ///     returns 'MyImage.gif'
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   Note that the algorithm is rather primitive: it's just looking
        ///   for the second dot from the right.
        /// </remarks>
        /// <param name = "virtualPathOrResourceName">
        ///   Name of the virtual path or resource.
        /// </param>
        /// <returns></returns>
        private static string GetResourceFileName(string virtualPathOrResourceName)
        {
            //Now the tricky part:
            //Trying to parse something like
            //eg: "resource:MyControl.Resources.MyImage.gif"
            //and figure out what is the name of the Control, 
            //and what is the 'myImage.gif' part:
            //We'll make an assumption...that it is 
            //the second dot from the right...

            //So convert a resourcePath
            //from: "XAct.UI.Web.WebControls.MyControl.Resources.MyFile.js"
            //to just filename:   "MyFile.js"
            int start = virtualPathOrResourceName.Length - 1;
            int end = start/2 - 1;
            int len = 0;
            int at = 0;
            int found = 0;
            while ((start > -1) && (at > -1))
            {
                len = start - end; //Count must be within the substring.
                at = virtualPathOrResourceName.LastIndexOf('.', start, len);
                if (at > -1)
                {
                    // Found:
                    found += 1;
                    if (found == 2)
                    {
                        break;
                    }
                    start = at - 1;
                }
            }
            if (found != 2)
            {
                //DebugInIDE("ERROR:" + "Not enough dots.");
                throw new Exception(
                    string.Format(
                        "::Not enough dot's in the virtualPathOrResourceName:{0}",
                        virtualPathOrResourceName));
            }

            string tmp = virtualPathOrResourceName.Substring(at + 1);
            return tmp;
        }


        /// <summary>
        ///   Creates the resource dump directory if required.
        /// </summary>
        /// <param name = "physicalDirectory">The physical directory.</param>
        private static void CreateResourceDumpDirectory(string physicalDirectory)
        {
            //DebugInIDE("virtualUrlDirectory:" + virtualUrlDirectory);
            //DebugInIDE("physicalDirectory:" + physicalDirectory);
            //Force Create the physical directory first:
            if (!Directory.Exists(physicalDirectory))
            {
                try
                {
                    Directory.CreateDirectory(physicalDirectory);
                    File.WriteAllLines(Path.Combine(physicalDirectory, "README.TXT"),
                                       new[]
                                           {
// ReSharper disable LocalizableElement
                                               "'ClientResources' is an Autogenerated Directory",
                                               "created in order to dump hard copies of embedded",
                                               "resources files (scripts, etc)."
// ReSharper restore LocalizableElement
                                           });
                }
                catch
                {
                    //DebugInIDE("ERROR:" + "Not enough rights.");
                    throw new Exception(
                        string.Format(
                            "::ASP.NET Process does not have enough" +
                            " rights to create a needed directory ('{0}').",
                            physicalDirectory));
                }
            }
        }

        #endregion

        #region Nested Classes

        /// <summary>
        ///   Class to contain cached information about an embedded Resource.
        /// </summary>
        /// <remarks>
        ///   Instantiated by <see cref = "GetWebResourceUrl" />
        ///   and cached within <see cref = "ResourceCache" /> dictionary.
        /// </remarks>
        protected class ResourceCacheSchema
        {
            /// <summary>
            ///   A flag inidicating whether the resource was found or not.
            /// </summary>
            public bool Exists;

            /// <summary>
            ///   The Physical File System Path ('d:\websites\aspNetSite\images\...')
            /// </summary>
            public string PhysicalPath;

            /// <summary>
            ///   The ResourcePath ('resouce:...') of the Resource.
            /// </summary>
            public string ResourcePath;

            /// <summary>
            ///   The WebSite Rooted Url for the Resource.
            /// </summary>
            public string Url;
        }

        #endregion
    }

}


