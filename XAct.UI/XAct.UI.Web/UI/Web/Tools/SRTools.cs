namespace XAct.UI.Web.Tools
{
    using System;
    using System.Reflection;
    using System.Web.UI.WebControls;

    /// <summary>
    ///   A wrapper class to be able to return
    ///   standard System Messages.
    /// </summary>
    public class SRTools

    {
        private static MethodInfo _SRGetStringMethodInfo;

        /// <summary>
        ///   Gets the System Resource string with the given tag.
        /// </summary>
        /// <param name = "tag">The tag.</param>
        /// <param name = "args">The optional args.</param>
        /// <returns></returns>
        public static string GetString(string tag, params object[] args)
        {
            if (_SRGetStringMethodInfo == null)
            {
                Assembly a = typeof (WebControl).Assembly;

                Type t = a.GetType("System.Web.SR", false, true);
                Type[] args2 = new[] {typeof (string), typeof (object[])};
                _SRGetStringMethodInfo = t.GetMethod("GetString", args2);
            }
            return (string) _SRGetStringMethodInfo.Invoke(null, new object[] {tag, args});
        }

/*

AVOID IF WE CAN, DUE TO PRIVATE FIELD REFLECTION

	  /// <summary>
	/// Wrapper to the <c>System.Web.CrossSiteScriptingValidation.IsDangerousUrl()</c> method.
	  /// </summary>
	  /// <param name="url"></param>
	  /// <returns></returns>
    public static bool CrossSiteScriptingValidation_IsDangerousUrl(string url) {
      if (_CrossCheck == null) {
        System.Reflection.Assembly a = typeof(System.Web.UI.WebControls.WebControl).Assembly;
        Type t = a.GetType("System.Web.CrossSiteScriptingValidation", false, true);
        Type[] args2 = new Type[] { typeof(string) };
        _CrossCheck = t.GetMethod("IsDangerousUrl",
          System.Reflection.BindingFlags.Static
        |
          System.Reflection.BindingFlags.NonPublic
          );
      }
      return (bool)_CrossCheck.Invoke(null, new object[] { url });

    }
    private static System.Reflection.MethodInfo _CrossCheck;
    */
    }


}


