﻿namespace XAct.UI.Web.Tools
{
    using System;
    using System.Web.UI;

    /// <summary>
    ///   Helper class to Save/Track and Restore ViewState:
    /// </summary>
    public static class StateManagementTools
    {
        /// <summary>
        ///   Gets the Typed view state value, or a default value if not provided.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "stateBag">The state bag.</param>
        /// <param name = "key">The key.</param>
        /// <param name = "defaultValue">The default value.</param>
        /// <returns></returns>
        public static T GetViewStateValue<T>(StateBag stateBag, string key, T defaultValue)
        {
            object o = stateBag[key];
            return (o != null) ? (T) o : defaultValue;
        }

        ///<summary>
        ///  Saves the ViewState of all provided IStateManager based objects, into one object.
        ///</summary>
        ///<para>
        ///  An example of its use would be:
        ///  <example>
        ///    <code>
        ///      <![CDATA[
        /// protected override object SaveViewState(){
        ///		StateManagement.SaveViewState(this,_MyStyle);
        ///	}
        /// ]]>
        ///    </code>
        ///  </example>
        ///</para>
        ///<param name = "stateManagedObjects">One or more stateManaged objects.</param>
        ///<returns>A single object containing recursivily nested arrays of objects.</returns>
        public static object SaveViewState(params IStateManager[] stateManagedObjects)
        {
            if ((stateManagedObjects == null) || (stateManagedObjects.Length == 0))
            {
                throw new ArgumentNullException("stateManagedObjects");
            }
            int blobCount = stateManagedObjects.Length;
            object[] blobs = new object[blobCount];
            for (int i = 0; i < stateManagedObjects.Length; i++)
            {
                blobs[i] = stateManagedObjects[0].SaveViewState();
            }
            return blobs;
        }


        /// <summary>
        ///   Helper method to <see cref = "IStateManager.TrackViewState" />.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Indtended to be called from a Control that needs to take over
        ///     ViewState tracking for complex objects:
        ///     <code>
        /// 
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "stateManagedObjects">One or more stateManaged objects.</param>
        public static void TrackViewState(params IStateManager[] stateManagedObjects)
        {
            if ((stateManagedObjects == null) || (stateManagedObjects.Length == 0))
            {
                throw new ArgumentNullException("stateManagedObjects");
            }

            //Turn on tracking, starting with last saved 
            //object first (ie nested ones first):
            for (int i = stateManagedObjects.Length - 1; i > -1; i--)
            {
                stateManagedObjects[i].TrackViewState();
            }
        }

        ///<summary>
        ///  Implementation of <see cref = "IStateManager.TrackViewState" />.
        ///  Restores view-state information from a previous request that was saved with the <see cref = "M:System.Web.UI.WebControls.WebControl.SaveViewState" /> method.
        ///</summary>
        ///<remarks>
        ///  <example>
        ///    <code>
        ///      <![CDATA[
        /// protected override object LoadViewState(object savedState){
        ///		StateManagement.LoadViewState(savedState, this,_MyStyle);
        ///	}
        /// ]]>
        ///    </code>
        ///  </example>
        ///</remarks>
        ///<param name = "savedState">An object that represents the control state to restore.</param>
        ///<param name = "stateManagedObjects">One or more stateManaged objects.</param>
        public static void LoadViewState(object savedState, params IStateManager[] stateManagedObjects)
        {
            //convert single object back into array that
            //should be exactly same size as what was created when 
            //saving, with SaveViewState:

            //Convert blob back into an array of viewstate blobs:
            object[] blobs = (object[]) savedState;

            //Check:
            if (blobs.Length != stateManagedObjects.Length)
            {
                throw new Exception("Both arrays must be same size...");
            }
            //Load up:
            for (int i = 0; i < blobs.Length; i++)
            {
                stateManagedObjects[i].LoadViewState(blobs[i]);
            }
        }
    }


}