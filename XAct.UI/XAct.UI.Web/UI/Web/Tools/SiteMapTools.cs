﻿namespace XAct.UI.Web.Tools
{
    using System.Web;
    using System.Web.UI.WebControls;

    /// <summary>
    ///   Class of Static Helper functions for common SiteMap operations.
    /// </summary>
    public static class SiteMapTools
    {
        /// <summary>
        ///   Processes the TreeNode's dataItem (the siteMapNode)'s attributes,
        ///   looking for an optional 'target' attribute.
        ///   If found, sets the MenuItem's target.
        /// </summary>
        public static bool ProcessTreeNodeDataBoundEventForTargetAttribute(object sender, TreeNodeEventArgs e)
        {
            //see: http://weblogs.asp.net/dannychen/archive/2005/03/28/396099.aspx
            TreeNode treeNode = e.Node;
            SiteMapNode siteMapNode = (SiteMapNode) treeNode.DataItem;
            if (siteMapNode == null)
            {
                return false;
            }

            string targetName = siteMapNode["target"];
            if (!string.IsNullOrEmpty(targetName))
            {
                //HttpContext.Current.Response.Write("HAVE TARGET!");
                treeNode.Target = targetName;
                string optionalTargetImage = siteMapNode["imageUrl"];
                if (optionalTargetImage != null)
                {
                    treeNode.ImageUrl = optionalTargetImage;
                }

                return true;
            }
            //HttpContext.Current.Response.Write("NO HAVE TARGET!");

            return false;
        }

        /// <summary>
        ///   Processes the menu item data bound event to
        ///   see if its underlying SiteMapNode has an optional 'target' attribute.
        ///   <para>
        ///     if it does, extracts and sets the MenuItem's target property.
        ///   </para>
        /// </summary>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.Web.UI.WebControls.MenuEventArgs" /> instance containing the event data.</param>
        /// <returns></returns>
        public static bool ProcessMenuItemDataBoundEventForTargetAttribute(object sender, MenuEventArgs e)
        {
            //see: http://weblogs.asp.net/dannychen/archive/2005/03/28/396099.aspx
            MenuItem menuItem = e.Item;
            SiteMapNode siteMapNode = (SiteMapNode) menuItem.DataItem;
            if (siteMapNode == null)
            {
                return false;
            }
            string targetName = siteMapNode["target"];
            if (!string.IsNullOrEmpty(targetName))
            {
                menuItem.Target = targetName;
                return true;
            }
            return false;
        }
    }


}