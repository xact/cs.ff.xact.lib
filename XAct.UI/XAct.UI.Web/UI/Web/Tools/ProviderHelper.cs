using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Compilation;
using System.Web.Hosting;

//COPIED FROM XAct.Core -- Modify there, and update here please.

namespace XAct.Web.Utils
{
    /// <summary>
    ///   Helper class to provide methods commonly needed to make providers.
    /// </summary>
    public class ProviderHelper
    {
        #region Constants

        /// <summary>
        ///   Constant used to save the ApplicationName in the Request's hashtable.
        /// </summary>
        protected const string C_HTTPCONTEXT_APPLICATIONNAME = "ApplicationName";

        #endregion

        #region Properties

        private static string _ApplicationName;

        /// <summary>
        ///   Gets the Application's base path.
        /// </summary>
        /// <value>The app dir.</value>
        public static string AppDir
        {
            get { return Env.AppDir; }
        }


        /// <summary>
        ///   The name of the application using the custom membership provider.
        ///   Default value is '/'
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     It is essential that the ApplicationName is set.
        ///     See <see href = "http://weblogs.asp.net/scottgu/archive/2006/04/22/443634.aspx" /> for why.
        ///   </para>
        /// </remarks>
        /// <value></value>
        /// <returns>The name of the application using the custom membership provider.</returns>
        public static string ApplicationName
        {
            get
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                HttpContext webContext = HttpContext.Current;
                if (webContext != null)
                {
                    //We are definately in a web application, so its safe to refer to these
                    //properties:
                    if ((string.IsNullOrEmpty(_ApplicationName)) || (_ApplicationName == "/"))
                    {
                        //Use alternate way that leaves no binding on System.Web.Hosting
                        //_ApplicationName = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
                        //The GetData(".appVPath") comes back as something like "/MyWebSite" (no final slash):
                        return (string) AppDomain.CurrentDomain.GetData(".appVPath");
                        //For the record, GetData(".appPath") comes back With a slash at the end..."D:\SYS\PROFILES\S\MYDOCUMENTS\CODE\MyWebSite\"
                    }
                    if (_ApplicationName == "~")
                    {
                        //This means we are suppossed to work with the webContext hashtable 
                        //using a key of C_HTTPCONTEXT_APPLICATIONNAME to store the value:
                        string o = (string) webContext.Items[C_HTTPCONTEXT_APPLICATIONNAME];
                        return (string.IsNullOrEmpty(o)) ? "/" : o;
                    }
                    //We are expected to use the normal value:
                    return _ApplicationName;
                } //~WebMode.
                //If we havn't exited yet, we are still in PC, but not in IIS.
                //But whether in PC or CE, from here on in, its the same...
#endif
                //We are in PC or CE:
                if ((string.IsNullOrEmpty(_ApplicationName)) || (_ApplicationName == "/") || (_ApplicationName == "~"))
                {
                    //We are using the default value...which should fall back on the name of the application.
                    //Note that it must not remain a "/" as it messes up the path.
                    //And must be differentiated from other applications in the 
                    //SpecialFolder.ApplicationData folder...
                    //Comes back as "MyApp.exe", no slashes. With extension.
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                    //PC:
                    _ApplicationName = (string) AppDomain.CurrentDomain.GetData("APP_NAME");
#else
    //In CE:
          _ApplicationName = Path.GetFileName(GetEntryAssembly().Location);
#endif
                }
                return _ApplicationName;
            }
            set
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                //Reference to Web dll's only happens if on PC or server, not CE:
                if (_ApplicationName == "~")
                {
                    //This means we are suppossed to work with the webContext hashtable 
                    //using a key of C_HTTPCONTEXT_APPLICATIONNAME to store the value:
                    HttpContext webContext = HttpContext.Current;
                    if (webContext != null)
                    {
                        webContext.Items[C_HTTPCONTEXT_APPLICATIONNAME] = value;
                        return;
                    }
                }
#endif
                if (value != _ApplicationName)
                {
                    _ApplicationName = value;
                }
            }
        }

        #endregion

        #region

        /// <summary>
        ///   Helper method to be invoked from a <c>Provider</c>'s <c>Initialize</c> method
        ///   to configure a property of the provider.
        /// </summary>
        /// <remarks>
        ///   Its intended usage is as follows:
        ///   <code>
        ///     <![CDATA[
        /// public class MyProvider : ProviderBase {
        ///   ...
        ///   protected virtual void Initialize(string name, NameValueCollection config) {
        ///     _ServiceUrl = InitializeParam<string>(config, "serviceUrl", string.Empty);
        ///   }
        ///   ...
        /// }
        /// ]]>
        ///   </code>
        /// </remarks>
        /// <typeparam name = "T">The Type of the property that is being set.</typeparam>
        /// <param name = "config">The NameValueCollection of attribute key/values that the Provider was passed.</param>
        /// <param name = "attributeTag">The attribute tag used to find the attribute value.</param>
        /// <param name = "defaultValue">The default value to use if the attribute is not found.</param>
        /// <param name = "required">A flag to indicate an error if the flag is not found (overrides defaultValue).</param>
        /// <returns>The typed value converted from the passed attribute string value, or the typed default value provided.</returns>
        public static T InitializeParam<T>(NameValueCollection config, string attributeTag, T defaultValue,
                                           bool required)
        {
            if ((config[attributeTag] == null) && required)
            {
                throw new ArgumentException(
                    string.Format("An attribute called '{0}' must be provided in configuration.", attributeTag));
            }
            return InitializeParam(config, attributeTag, defaultValue, null);
        }


        /// <summary>
        ///   Helper method to be invoked from a <c>Provider</c>'s <c>Initialize</c> method
        ///   to configure a property of the provider.
        /// </summary>
        /// <remarks>
        ///   Its intended usage is as follows:
        ///   <code>
        ///     <![CDATA[
        /// public class MyProvider : ProviderBase {
        ///   ...
        ///   protected virtual void Initialize(string name, NameValueCollection config) {
        ///     _ServiceUrl = InitializeParam<string>(config, "serviceUrl", string.Empty);
        ///   }
        ///   ...
        /// }
        /// ]]>
        ///   </code>
        /// </remarks>
        /// <typeparam name = "T">The Type of the property that is being set.</typeparam>
        /// <param name = "config">The NameValueCollection of attribute key/values that the Provider was passed.</param>
        /// <param name = "attributeTag">The attribute tag used to find the attribute value.</param>
        /// <param name = "defaultValue">The default value to use if the attribute is not found.</param>
        /// <returns>The typed value converted from the passed attribute string value, or the typed default value provided.</returns>
        public static T InitializeParam<T>(NameValueCollection config, string attributeTag, T defaultValue)
        {
            return InitializeParam(config, attributeTag, defaultValue, null);
        }

        /// <summary>
        ///   Helper method to be invoked from a <c>Provider</c>'s <c>Initialize</c> method
        ///   to configure a property of the provider.
        /// </summary>
        /// <typeparam name = "T">The Type of the property that is being set.</typeparam>
        /// <param name = "config">The NameValueCollection of attribute key/values that the Provider was passed.</param>
        /// <param name = "attributeTag">The attribute tag used to find the attribute value.</param>
        /// <param name = "defaultValue">The default value to use if the attribute is not found.</param>
        /// <param name = "sqlKeyWords">The SQL key words collection to add the values to (sorted by the key obviously).</param>
        /// <returns></returns>
        public static T InitializeParam<T>(NameValueCollection config, string attributeTag, T defaultValue,
                                           StringDictionary sqlKeyWords)
        {
            Type t = typeof (T);
            T returnValue = (T) (t.IsValueType ? Activator.CreateInstance(t) : null);

            return InitializeParam(config, attributeTag, ref returnValue, defaultValue, sqlKeyWords);
        }

        /// <summary>
        ///   Helper method to be invoked from a <c>Provider</c>'s <c>Initialize</c> method
        ///   to configure a property of the provider.
        /// </summary>
        /// <typeparam name = "T">The Type of the property that is being set.</typeparam>
        /// <param name = "config">The NameValueCollection of attribute key/values that the Provider was passed.</param>
        /// <param name = "attributeTag">The attribute tag used to find the attribute value.</param>
        /// <param name = "returnedValue">The returned set value.</param>
        /// <param name = "defaultValue">The default value to use if the attribute is not found.</param>
        /// <param name = "sqlKeyWords">The SQL key words collection to add the values to (sorted by the key obviously).</param>
        /// <returns></returns>
        public static T InitializeParam<T>(NameValueCollection config, string attributeTag, ref T returnedValue,
                                           T defaultValue, StringDictionary sqlKeyWords)
        {
            T returnValue;

            //Extract value form attributes:
            string tmpStringValue = config[attributeTag];

            if (!string.IsNullOrEmpty(tmpStringValue))
            {
                //We have a value...so convert to expected type:
                returnValue = (T) Convert.ChangeType(tmpStringValue, typeof (T), null);
            }
            else
            {
                //No value, so set to default value:
                returnValue = defaultValue;
            }

            if (sqlKeyWords != null)
            {
                if (attributeTag == "dbStringMatchCriteria")
                {
                    attributeTag = "LIKE";
                }
                if (attributeTag == "dbParamPlaceHolderChar")
                {
                    attributeTag = "DBPARAM";
                }
                if (!sqlKeyWords.ContainsKey(attributeTag))
                {
                    sqlKeyWords.Add(attributeTag, returnValue.ToString());
                }
                else
                {
                    //Do we override it?
                    string v = returnValue.ToString();
                    if (!string.IsNullOrEmpty(v))
                    {
                        sqlKeyWords[attributeTag] = v;
                    }
                }
            }
            return returnValue;
        }

        #endregion

        #region ConnectionString / DataDirectory / Rooted paths.

        /// <summary>
        ///   Helper Method to expand the path part of a 
        ///   <see cref = "P:ConnectionStringSetting.ConnectionString" /> property
        ///   replacing the '|DATADIRECTORY|' macro with the 
        ///   application's designated Data directory.
        ///   <para>
        ///     If not rooted, prepends the path with the <see cref = "AppDir" />.
        ///   </para>
        /// </summary>
        /// <param name = "connectionStringSettingsName">Name of the connection settings.</param>
        /// <returns>The modified ConnectionStringSetting.</returns>
        public static ConnectionStringSettings ConfigureConnectionSettings(string connectionStringSettingsName)
        {
            if (String.IsNullOrEmpty(connectionStringSettingsName))
            {
                throw new ArgumentNullException("connectionSettingsName");
            }
            ConnectionStringSettings connectionStringSetting =
                ConfigurationManager.ConnectionStrings[connectionStringSettingsName];
            if (connectionStringSetting == null)
            {
                throw new ArgumentException(string.Format("Missing connection string : '{0}'",
                                                          connectionStringSettingsName));
            }

            return ConfigureConnectionSettings(connectionStringSetting);
        }

        /// <summary>
        ///   Helper Method to expand the path part of a <see cref = "P:ConnectionStringSetting.ConnectionString" /> property
        ///   replacing the '|DATADIRECTORY|' macro with the application's designated Data directory.
        ///   <para>
        ///     If not rooted, prepends the path with the <see cref = "AppDir" />.
        ///   </para>
        /// </summary>
        /// <param name = "connectionSettings">The connectionSettings object.</param>
        /// <returns>The modified ConnectionStringSetting.</returns>
        public static ConnectionStringSettings ConfigureConnectionSettings(ConnectionStringSettings connectionSettings)
        {
            if (connectionSettings == null)
            {
                throw new ArgumentNullException("connectionSettings");
            }

            string connStr = connectionSettings.ConnectionString;
            connStr = ExpandDataDirectoryMacro(connStr, false);

            connectionSettings =
                new ConnectionStringSettings(
                    connectionSettings.Name,
                    connStr,
                    connectionSettings.ProviderName
                    );

            return connectionSettings;
        }

        /// <summary>
        ///   Replace '|DataDirectory|' in the given path, with the path to the application's designated Data directory.
        ///   <para>
        ///     If not rooted, prepends the path with the <see cref = "AppDir" />.
        ///   </para>
        /// </summary>
        /// <param name = "tmpPath">The path to parse.</param>
        /// <returns>The expanded path.</returns>
        public static string ExpandDataDirectoryMacro(string tmpPath)
        {
            return ExpandDataDirectoryMacro(tmpPath, true);
        }

        /// <summary>
        ///   Replace '|DataDirectory|' in the given path, with the path to the application's designated Data directory.
        ///   <para>
        ///     If not rooted, can be prepended with the <see cref = "AppDir" />.
        ///   </para>
        /// </summary>
        /// <param name = "tmpPath">The path to parse.</param>
        /// <param name = "ensureRooted">Whether or not to ensure the string is rooted.</param>
        /// <returns>The expanded path.</returns>
        public static string ExpandDataDirectoryMacro(string tmpPath, bool ensureRooted)
        {
            if (string.IsNullOrEmpty(tmpPath))
            {
                return string.Empty;
            }
            //CASE INSENSITIVE REPLACEMENT OF |DATADIRECTORY| MACRO:
            string dataDirectoryPattern = "\\|DataDirectory\\|";
            if (Regex.IsMatch(tmpPath, dataDirectoryPattern, RegexOptions.IgnoreCase))
            {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                //PC:
                object dataDirectoryObject = AppDomain.CurrentDomain.GetData("DataDirectory");
                string dataDirectory =
                    (dataDirectoryObject != null)
                        ? dataDirectoryObject + @"\"
                        : AppDir + @"\Data\";
#else
    //CE:
        string dataDirectory = AppDir + @"\Data\";
#endif
                tmpPath = Regex.Replace(tmpPath, dataDirectoryPattern, dataDirectory, RegexOptions.IgnoreCase);
            }
            return (ensureRooted) ? EnsureDirectoryRooted(tmpPath) : tmpPath;
        }

        /// <summary>
        ///   Ensures that the given path, if not rooted, is prepended with the <see cref = "AppDir" />.
        /// </summary>
        /// <param name = "tmpPath"></param>
        /// <returns></returns>
        public static string EnsureDirectoryRooted(string tmpPath)
        {
            if (!Path.IsPathRooted(tmpPath))
            {
                tmpPath = Path.Combine(AppDir, tmpPath);
            }
            return tmpPath;
        }

        #endregion

        #region Type Reflection

        /// <summary>
        ///   Converts the Type Name to a <see cref = "Type" />.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Type.GetType() only works for Types within assemblies already loaded.
        ///     Therefore, TypeNames
        ///   </para>
        ///   <para>
        ///     NOTE:
        ///     Addresses the bug(?) on CE/Mobile: it strips any spaces after the comma, 
        ///     in between Type name and Assembly name: it will fail on CE if there are any spaces.
        ///   </para>
        /// </remarks>
        /// <param name = "typeName">Name of the type.</param>
        /// <param name = "throwOnError">if set to <c>true</c> throw on error if the Type Name cannot be resolved to a Type.</param>
        /// <returns>The Type (not the Instance of the Type).</returns>
        public static Type GetTypeFromTypeName(string typeName, bool throwOnError)
        {
            typeName = typeName.Trim();
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
            //PC:
            if (HostingEnvironment.IsHosted)
            {
                return BuildManager.GetType(typeName, throwOnError, true);
            }
#endif
            //SLS:
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
            //PC:
#else
    //CE: Wierd bug... doesn't find it if it has a space in between Type and assembly.
    //So strip them out:
      typeName = typeName.Replace(" ", "");
#endif
            Type result = Type.GetType(typeName, false, true);

            if (result == null)
            {
                //http://forums.microsoft.com/MSDN/ShowPost.aspx?PostID=1091058&SiteID=1
                Assembly a;
                string[] sa = typeName.Split(',');
                if (sa.Length > 1)
                {
                    try
                    {
                        //Assembly.Load works with a Fully Qualified Assembly Name (ie, all to the right of the first comma)
                        //It fails (on CE atleast) with only the first part (assemblyname, no extension):
                        string fullyQualifiedAssemblyName = (typeName.Length > sa[0].Length)
                                                                ? (typeName.Substring(sa[0].Length + 1))
                                                                : typeName;
                        a = Assembly.Load(fullyQualifiedAssemblyName.Trim());
                    }
                    catch
                    {
                        a = null;
                        string shortAssemblyName = sa[1].Trim();
                        foreach (
                            string s in
                                new[] {shortAssemblyName, shortAssemblyName + ".dll", shortAssemblyName + ".exe"})
                        {
                            try
                            {
                                a = Assembly.LoadFrom(shortAssemblyName);
                                if (a != null)
                                {
                                    break;
                                }
                            }
                            catch
                            {
                            }
                        } //~loop
                    } //~catch
                    if (a != null)
                    {
                        return a.GetType(sa[0].Trim());
                    }
                } //~has assemblyname
                if (throwOnError)
                {
                    throw new Exception();
                }
            }
            return result;
        }

        #endregion

        #region Property Reflection

        /// <summary>
        ///   Helper method to extract the property value from the given object (will generally be the Config Section of the Provider).
        /// </summary>
        /// <typeparam name = "T">The Type of the Property to extract.</typeparam>
        /// <param name = "o">The instance to reflect on.</param>
        /// <param name = "propertyName">The Name of the Property to find.</param>
        /// <param name = "defaultValue">The default value to give if not found.</param>
        /// <param name = "returnValue">The returned value.</param>
        /// <returns>True if it was found, false if the default value was used.</returns>
        public static bool GetProperty<T>(object o, string propertyName, T defaultValue, out T returnValue)
        {
            Type t = o.GetType();
            PropertyInfo pInfo = t.GetProperty(propertyName,
                                               BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
            if (pInfo == null)
            {
                returnValue = defaultValue;
                return false;
            }
            if (pInfo.PropertyType != typeof (T))
            {
                returnValue = defaultValue;
                return false;
            }
            returnValue = (T) pInfo.GetValue(o, null);
            return true;
        }

        /// <summary>
        ///   Helper method to set the given object's property value (will generally be the configuration section of the Provider).
        /// </summary>
        /// <typeparam name = "T">The Type of the Property to extract.</typeparam>
        /// <param name = "o">The instance to reflect on.</param>
        /// <param name = "propertyName">The Name of the Property to find.</param>
        /// <param name = "value">The value to set the property to.</param>
        /// <returns>True if the property was set, false if the property was not set, or could not be Written to for some reason.</returns>
        public static bool SetProperty<T>(object o, string propertyName, T value)
        {
            Type t = o.GetType();
            PropertyInfo pInfo = t.GetProperty(propertyName,
                                               BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);

            if (pInfo == null)
            {
                return false;
            }
            if (pInfo.PropertyType != typeof (T))
            {
                return false;
            }
            if (!pInfo.CanWrite)
            {
                return false;
            }
            pInfo.SetValue(o, value, null);
            return true;
        }

        #endregion

        /*
    private static bool IsHostedInAspnet() {
     //GetData not supported on CE:
      return (AppDomain.CurrentDomain.GetData(".appDomain") != null);
    }
    */
    }


    /// <summary>
    ///   Helper class of methods and properties to make building Provider Managers easier.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     An example of its use to make a new manager easily and quickly is:
    ///     <code>
    ///       <![CDATA[
    /// public static class MyManager { 
    /// 
    /// #region Fields
    ///     private static 
    ///       ProviderManagerHelper<MySection,MyBaseProvider, MyProviderCollection> 
    ///       _ProviderHelper =
    ///       new ProviderManagerHelper<MySection, MyBaseProvider, MyProviderCollection>
    ///       ("XAct/Configuration/Settings", typeof(XAct.Configuration.SettingsDefaultProvider));
    ///     #endregion
    /// 
    ///     
    ///       public static MyBaseProvider Provider {
    ///         get {return _ProviderHelper.Instance;}
    ///       }
    ///       public static MyProviderCollection Providers {
    ///         get {return _ProviderHelper.Providers;}
    ///       }
    ///       public static MySection ConfigSection {
    ///         get {return _ProviderHelper.ConfigSection;}
    ///       }
    /// 
    ///     //Gets a custom property from the config section:
    ///     public static SettingsDbSettings DbSettings {
    ///       get {return _ProviderHelper.ConfigSection.DbSettings;}
    ///     }
    /// }
    /// ]]>
    ///     </code>
    ///   </para>
    /// </remarks>
    /// <typeparam name = "TConfigSection">The type of the Configuration Section to contain this Provider's specifications.</typeparam>
    /// <typeparam name = "TProviderBase">Type base type that the Provider must enherit from (usually a base abstract provider).</typeparam>
    /// <typeparam name = "TProviderCollection">The provider collection, which is, or atleast derives from <see cref = "ProviderCollection" />.</typeparam>
    public class ProviderManagerHelper<TConfigSection, TProviderBase, TProviderCollection> :
        ProviderHelper
        where TConfigSection : ConfigurationSection, new()
        where TProviderBase : ProviderBase
        //where TDefaultProvider : TProviderBase
        where TProviderCollection : ProviderCollection, new()
    {
        #region Events

        /// <summary>
        ///   Event raised when the Manager begins to initialize and instantiate the providers.
        /// </summary>
        public event EventHandler<EventArgs> Initializing;

        /// <summary>
        ///   Event raised when the Manager needs is being initialized.
        /// </summary>
        public event EventHandler<EventArgs> InitializingManager;

        /// <summary>
        ///   Event raised when the Manager begins the instantiation of a Provider.
        /// </summary>
        public event EventHandler<EventArgs> InitializingProvider;

        /// <summary>
        ///   Event raised when the Manager begins the instantiation of a Provider.
        /// </summary>
        public event EventHandler<EventArgs> InitializedProvider;

        /// <summary>
        ///   Event raised when the Manager has completed the initialization and instantiation of the providers.
        /// </summary>
        public event EventHandler<EventArgs> Initialized;

        #endregion

        #region Fields - Initialization. Same for all ProviderManagers (Do NOT Change).

        /// <summary>
        ///   Used by <see cref = "Initialize" /> for thread-locking.
        /// </summary>
        private static readonly object s_initializationLock = new object();

        /// <summary>
        ///   Used by <see cref = "Initialize" /> to indicate whether Manager has previously been Initialized.
        /// </summary>
        private static bool s_IsInitialized;

        private static bool s_IsInitializing;

        /// <summary>
        ///   Used by <see cref = "Initialize" /> to hold ref to previously thrown exception.
        /// </summary>
        private static Exception s_InitializationError;

        /// <summary>
        ///   Internal flag indicating if we are using the built in default provider.
        /// </summary>
        private static bool s_UsingBuiltInProvider;

        #endregion

        #region Properties

        private static TProviderCollection _Providers;

        /// <summary>
        ///   Gets the <see cref = "Type" /> of the Default Provider, if any.
        /// </summary>
        public readonly Type _DefaultProviderType;

        private readonly string _SectionPath;

        private TConfigSection _ConfigSection;


        private bool _Enabled;

        private TProviderBase _Instance;

        /// <summary>
        ///   Gets the configuration section of the current Provider.
        /// </summary>
        /// <value>The config section.</value>
        public TConfigSection ConfigSection
        {
            get
            {
                Initialize(false);
                return _ConfigSection;
            }
        }

        /// <summary>
        ///   Gets a value indicating whether the Provider is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled
        {
            get { return _Enabled; }
        }

        /// <summary>
        ///   Gets the singleton instance of this Provider.
        /// </summary>
        /// <value>The instance.</value>
        public TProviderBase Instance
        {
            get
            {
                Initialize(false);
                return _Instance;
            }
        }

        /// <summary>
        ///   The path to the Configuration Section.
        /// </summary>
        public string Path
        {
            get { return _SectionPath; }
        }

        /// <summary>
        ///   Gets the collection of providers defined in the config file.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     In most cases the only Provider that is of importance is the
        ///     default provider that is returned by <see cref = "P:Provider" />.
        ///   </para>
        ///   <para>
        ///     In very very few cases you may need access to other providers,
        ///     which can be optained from this collection.
        ///   </para>
        /// </remarks>
        /// <value>The providers.</value>
        /// <internal>
        ///   <tracking>
        ///     4/3/2006 - 1:33 AM - S
        ///   </tracking>
        /// </internal>
        public TProviderCollection Providers
        {
            get
            {
                Initialize(false);
                return _Providers;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "T:ProviderManagerHelper&lt;TConfigSection, TProviderBase, TProviderCollection&gt;" /> class.
        /// </summary>
        /// <param name = "path">The path.</param>
        public ProviderManagerHelper(string path)
            : this(path, null)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "T:ProviderManagerHelper&lt;TConfigSection, TProviderBase, TProviderCollection&gt;" /> class.
        /// </summary>
        /// <param name = "path">The path.</param>
        /// <param name = "defaultProviderType">Type of the default provider.</param>
        public ProviderManagerHelper(string path, Type defaultProviderType)
        {
            //XAct.Configuration.SettingsProvider
            //XACt.Configuration.SettingsDefaultProvider
            //      if (defaultProviderType.IsAssignableFrom(typeof(TProviderBase))) {
            //        throw new System.Exception();
            //      }
            Type t = typeof (TProviderBase);


            if (t.IsAssignableFrom(defaultProviderType))
            {
                //BUG !!!
                //Cannot figure out why this is being triggered. Shouldn't!!!!!!!
                //throw new System.Exception();
            }
            _DefaultProviderType = defaultProviderType;
            _SectionPath = path;
        }

        #endregion

        #region Protected Methods - Basics - Initialization. Same for all ProviderManagers (Do NOT Change).

        /// <summary>
        ///   Initialize the Permission manager feature.
        /// </summary>
        public bool Initialize(bool throwIfNotEnabled)
        {
            if (s_IsInitializing)
            {
                return false;
            }

            //To Make this class portable, one needs two vars:
            //SectionPath
            //ProviderType

            //Check if the Manager is initialized
            if (s_IsInitialized)
            {
                if (!_Enabled && throwIfNotEnabled)
                {
                    throw new Exception(
                        string.Format(
                            typeof (TConfigSection) + "." + "Initialize() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                            //PC:
                            Environment.NewLine +
#endif
                            "Permission_feature_not_enabled"
                            )
                        );
                }
                return false;
            }

            //Lock the thread
            lock (s_initializationLock)
            {
                //Check if the permissions is initialized
                if (s_IsInitialized)
                {
                    //If not enabled, throw an exception:
                    if (!(_Enabled) && (throwIfNotEnabled))
                    {
                        throw new Exception(
                            string.Format(
                                typeof (TConfigSection) + "." + "Initialize() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                                //PC:
                                Environment.NewLine +
#endif
                                "Permission_feature_not_enabled"
                                )
                            );
                    }

                    //If the previous initialization failed, throw the previous exception. 
                    //There is no need to try to initialize the manager once it has failed before.
                    if (s_InitializationError != null)
                    {
                        throw s_InitializationError;
                    }
                    //Get out early:
                    return false;
                }


                try
                {
                    //Raise event saying we are definately trying...
                    OnInitializing(EventArgs.Empty);

                    s_IsInitializing = true;
                    //STEP1: Get access to the config Section first:

                    //Gets the configuration settings for the manager:
                    //Note that 'ConfigurationManager' is only available in NET 2.0
                    //where ConfigurationSettings.GetConfig works in NET 1.1
                    //_ConfigSection = (SchemaProviderConfigSection)ConfigurationManager.GetSection(SectionPath);
                    //_ConfigSection = (SchemaProviderConfigurationSection)ConfigurationSettings.GetConfig(SectionPath);

                    //Note that if the user has not added a config section definition,
                    //this will throw an error:


                    try
                    {
                        _ConfigSection = Initialize_GetConfigSection();
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                    //Copy the property from the _ConfigSection attribute to this Manager's property:
                    //_Enabled = _ConfigSection.Enabled;
                    GetProperty(_ConfigSection, "Enabled", true, out _Enabled);

                    if (_Enabled == false)
                    {
                        if (throwIfNotEnabled)
                        {
                            throw new Exception(
                                string.Format(
                                    typeof (TConfigSection) + "." + "Initialize() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                                    //PC:
                                    Environment.NewLine +
#endif
                                    "Manager not Enabled."
                                    )
                                );
                        }
                    }


                    //If the previous initialization failed, throw the previous exception. 
                    //There is no need to try to initialize the manager if it has failed before.
                    if (s_InitializationError != null)
                    {
                        throw s_InitializationError;
                    }


                    //Make call to virtual method that can be overriden, or listened to event for:
                    OnInitializingManager(EventArgs.Empty);


                    //Ok. Ready to Rock.
                    Initialize_InstantiateProviders();

                    //Set the collection as readonly from here on in:
                    _Providers.SetReadOnly();


                    //Raise event saying we succeeded:
                    OnInitialized(EventArgs.Empty);

                    //We're done!
                    s_IsInitialized = true;
                }
                catch (Exception E)
                {
                    //Cache the exception, so that next time someone tries to call on a method of the Manager,
                    //and Initialize is called, this exception will be thrown.
                    s_InitializationError = E;
                    //Throw the exception this time.
                    throw;
                }
                finally
                {
                    s_IsInitializing = false;
                }
            } //End:Lock

            return true;
        }


        /// <summary>
        ///   Gets the ConfigurationSection specific to assembly.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Invoked by <see cref = "M:Initialize" />.
        ///   </para>
        /// </remarks>
        /// <returns></returns>
        protected TConfigSection Initialize_GetConfigSection()
        {
            System.Configuration.ConfigurationSection untypedConfigSection;

            TConfigSection typedConfigSection;

            try
            {
                untypedConfigSection = (ConfigurationSection) ConfigurationManager.GetSection(_SectionPath);
            }
            catch (Exception E)
            {
                //It will have failed not because the section did not exist,
                //but because there was a configuration error:

                throw new Exception(
                    string.Format(
                        typeof (TConfigSection) + "." + "Initialize_GetConfigSection() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                        //PC:
                        Environment.NewLine +
#endif
                        "Config File appears unparsable."
                        ),
                    E);
            }

            if (untypedConfigSection == null)
            {
                //There is no section definition...
                //but is that ok?
                if (_DefaultProviderType == null)
                {
                    throw new Exception(
                        string.Format(
                            typeof (TConfigSection) + "." + "Initialize_GetConfigSection() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                            //PC:
                            Environment.NewLine +
#endif
                            "Could not find a ConfigurationSection called '{0}' in the app.config.",
                            _SectionPath
                            )
                        );
                }
                else
                {
                    s_UsingBuiltInProvider = true;
                    untypedConfigSection = new TConfigSection();
                    if (!SetProperty<string>(untypedConfigSection, "DefaultProviderName", "Default"))
                    {
                        if (!SetProperty<string>(untypedConfigSection, "DefaultProvider", "Default"))
                        {
                            //throw new System.Exception();
                        }
                    }
                }
            }

            //Convert to typed version:
            typedConfigSection = untypedConfigSection as TConfigSection;

            if (typedConfigSection == null)
            {
                //Error: the _ConfigSection with the given name does not exist, 
                //or was not of the right type.
                throw new Exception(
                    string.Format(
                        typeof (TConfigSection) + "." + "Initialize_GetConfigSection() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                        //PC:
                        Environment.NewLine +
#endif
                        "Not of right type."
                        )
                    );
            }

            return typedConfigSection;
        }


        /// <summary>
        ///   TODO: TODOC:
        /// </summary>
        /// <internal>
        ///   <para>
        ///     Invoked by <see cref = "M:Initialize" /> only.
        ///   </para>
        /// </internal>
        protected void Initialize_InstantiateProviders()
        {
            string nameOfDefaultProvider;

            //Instantiate static Collection:
            _Providers = new TProviderCollection();

            //Get the default ProviderName:
            if (!s_UsingBuiltInProvider)
            {
                //CASE: We are using the config file (the normal way).

                //Get the name of the default provider from the config file attributes:
                //nameOfDefaultProvider = _ConfigSection.DefaultProviderName;
                if (!GetProperty(_ConfigSection, "DefaultProviderName", string.Empty, out nameOfDefaultProvider))
                {
                    GetProperty(_ConfigSection, "DefaultProvider", string.Empty, out nameOfDefaultProvider);
                }


                //If no default provider is defined we have a problem:
                if (string.IsNullOrEmpty(nameOfDefaultProvider))
                {
                    throw new Exception(
                        string.Format(
                            typeof (TConfigSection) + "." + "Initialize_InstantiateProviders() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                            //PC:
                            Environment.NewLine +
#endif
                            "Def_provider_not_specified"
                            )
                        );
                }

                System.Configuration.ProviderSettingsCollection providerSettings;
                if (
                    !GetProperty<System.Configuration.ProviderSettingsCollection>(_ConfigSection, "ProviderSettings",
                                                                                  null, out providerSettings))
                {
                    GetProperty<System.Configuration.ProviderSettingsCollection>(_ConfigSection, "Providers", null,
                                                                                 out providerSettings);
                }


                try
                {
                    Initialize_InstantiateProviders(
                        providerSettings,
                        _Providers);
                }
                catch (Exception E)
                {
                    throw new Exception(
                        string.Format(
                            typeof (TConfigSection) + "." + "Initialize_InstantiateProviders() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                            //PC:
                            Environment.NewLine +
#endif
                            ""
                            ),
                        E
                        );
                }
            }
            else
            {
                //CASE: We are using default provider.
                //We need to instantiate this provider, 
                //in order to get its name, and then instantiate it:
                TProviderBase defaultProvider =
                    (TProviderBase)
                    Activator.CreateInstance(_DefaultProviderType);

                nameOfDefaultProvider = defaultProvider.Name;
                if (string.IsNullOrEmpty(nameOfDefaultProvider))
                {
                    nameOfDefaultProvider = "Default";
                }
                NameValueCollection providerSettings =
                    new NameValueCollection();
                defaultProvider.Initialize(nameOfDefaultProvider, providerSettings);

                _Providers.Add(defaultProvider);
            }

            //Get the default Provider:
            _Instance = (TProviderBase) _Providers[nameOfDefaultProvider];

            //If we could not instantiate the provider, we have a problem:
            if (_Instance == null)
            {
                throw new Exception(
                    string.Format(
                        typeof (TConfigSection) + "." + "Initialize_InstantiateProviders() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                        //PC:
                        Environment.NewLine +
#endif
                        "Def_provider_not_found"
                        )
                    );
            }
        }

        /// <summary>
        ///   Helper Method to instantiate all members of a ProvidersSettingsCollection into the internal ProviderCollection.
        /// </summary>
        /// <param name = "providerSettingsCollection">The collection of <see cref = "ProviderSettings" /></param>
        /// <param name = "providers">The collection of Providers to be filled.</param>
        protected void Initialize_InstantiateProviders(
            System.Configuration.ProviderSettingsCollection providerSettingsCollection, TProviderCollection providers)
        {
            //Loop through the config section's nested provider settings
            //instantiating a provider each time, and adding it to the providers collection:
            foreach (System.Configuration.ProviderSettings providerSettings in providerSettingsCollection)
            {
                try
                {
                    TProviderBase provider =
                        Initialize_InstantiateProvider(
                            providerSettings
                            );
                    Initialize_InstantiateProvider(provider, providerSettings);

                    providers.Add(provider);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        /// <summary>
        ///   Initialize a single Provider from the values in the passed ProviderSettings.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Invoked by 
        ///     <c>Initialize_InstantiateProviders(ProviderSettingsCollection,{TProviderCollection})</c>.
        ///   </para>
        /// </remarks>
        /// <param name = "providerSettings">The <see cref = "ProviderSettings" /> used to create a single typed Provider.</param>
        /// <returns></returns>
        protected TProviderBase Initialize_InstantiateProvider(System.Configuration.ProviderSettings providerSettings)
        {
            try
            {
                //Although we were passed the base providerBaseType, 
                //its an abstract class -- which therefore cannot be instantiated.
                //We need the actual Type of the derived provider that is to be 
                //instantiated...
                //So, extract from the ProviderSettings that is passed, the 
                //string name of the providerToInstantiateType of provider that is to be instantiated:
                string providerToInstantiateTypeName = (providerSettings.Type == null)
                                                           ? null
                                                           : providerSettings.Type.Trim();

                if (string.IsNullOrEmpty(providerToInstantiateTypeName))
                {
                    throw new ArgumentException(
                        string.Format(
                            typeof (TConfigSection) + "." + "Initialize_InstantiateProvider() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                            //PC:
                            Environment.NewLine +
#endif
                            "Provider_no_providerToInstantiateType_name"
                            )
                        );
                }

                //Use helper method to convert the providerToInstantiateType name to a Type:

                //CE: Wierd bug... doesn't find it if it has a space in between Type and assembly.
                //So strip them out:
                Type providerToInstantiateType = GetTypeFromTypeName(providerToInstantiateTypeName, true);

                //_GetTypeFromTypeName(providerToInstantiateTypeName);

                //Did that work?
                if (providerToInstantiateType == null)
                {
                    throw new ArgumentException(
                        string.Format(
                            typeof (TConfigSection) + "." + "Initialize_InstantiateProvider() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                            //PC:
                            Environment.NewLine +
#endif
                            "Could not instantiate providerToInstantiateTypeName"
                            )
                        );
                }

                //The provider Type has to be derived from the base type,
                //so check for that:
                //if (!providerToInstantiateType.IsAssignableFrom(typeof(TProviderBase))) {
                if (!typeof (TProviderBase).IsAssignableFrom(providerToInstantiateType))
                {
                    throw new ArgumentException(
                        string.Format(
                            typeof (TConfigSection) + "." + "Initialize_InstantiateProvider() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                            //PC:
                            Environment.NewLine +
#endif
                            "Provider_must_implement_providerToInstantiateType"
                            )
                        );
                }

                //Then we return the non-initialized specific provider:
                return (TProviderBase) Activator.CreateInstance(providerToInstantiateType);
            }
            catch (Exception exception)
            {
                if (exception is Exception)
                {
                    throw;
                }

                throw new Exception(
                    string.Format(
                        typeof (TConfigSection) + "." + "Initialize_InstantiateProvider() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                        //PC:
                        Environment.NewLine +
#endif
                        exception.Message

                        //                    providerSettings.ElementInformation.Properties["providerToInstantiateType"].Source,
                        //                    providerSettings.ElementInformation.Properties["providerToInstantiateType"].LineNumber
                        )
                    );
            }
        }


        /// <summary>
        ///   Instantiates and initializes the specified provider.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Invoked by <c>Initialize_InstantiateProviders(ProviderSettingsCollection, {TProviderCollection})</c>
        ///   </para>
        /// </remarks>
        /// <param name = "baseProvider">The base abstract provider.</param>
        /// <param name = "providerSettings">The provider settings that describe the provider, used to initialize the provider.</param>
        protected void Initialize_InstantiateProvider(TProviderBase baseProvider,
                                                      System.Configuration.ProviderSettings providerSettings)
        {
            //From the providerSettings we ask for the collection of parameters 
            //which contains Name, etc., and any other attributes that were figured out...
            NameValueCollection parameters =
                providerSettings.Parameters;

            //Clone the attributes passed because the baseProvider's initialization method
            //might remove/damage items from the list (such as 'name', 'description', 'providerToInstantiateType':
            NameValueCollection clonedParameters =
                new NameValueCollection
                    (
                    parameters.Count, StringComparer.InvariantCulture
                    );
            foreach (string parameter in parameters)
            {
                clonedParameters[parameter] = parameters[parameter];
            }

            // Please note that these are the attributes that were set on
            // the actual baseProvider setting itself (not the outer 
            // providers group node)-- so the following will
            // contain the parameter for myVar:
            //  <providers>
            //		<add name="MyCustomProvider" 
            //			type="XAct.Examples.MyCustomProvider" 
            //			myVar="Testing..."   />
            //	</providers>

            //And use these parametes to initialize the Provider itself.
            try
            {
                baseProvider.Initialize(providerSettings.Name, clonedParameters);
            }
            catch (Exception E)
            {
                throw new Exception(
                    string.Format(
                        typeof (TConfigSection) + "." + "Initialize_InstantiateProvider() Error." +
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
                        //PC:
                        Environment.NewLine +
#endif
                        "Error invoking Initialize(...) Method of instantiated Provider."
                        ),
                    E
                    );
            }
            //Summary: 
            //we used the passed string alias to instantiate a specific baseProvider
            //and used the passed settings to extract the parameters and initialize this
            //specific baseProvider.
        }

        #endregion

        #region Protected - Raise Events

        /// <summary>
        ///   Raises the <see cref = "E:Initializing" /> event.
        /// </summary>
        /// <param name = "e">The <see cref = "T:System.EventArgs" /> instance containing the event data.</param>
        protected virtual void OnInitializing(EventArgs e)
        {
            if (Initializing != null)
            {
                Initializing(this, e);
            }
        }

        /// <summary>
        ///   Raises the <see cref = "E:InitializingManager" /> event
        ///   after it has initialized the Manager, but not yet initialized the Providers.
        /// </summary>
        /// <param name = "e">The <see cref = "T:System.EventArgs" /> instance containing the event data.</param>
        protected virtual void OnInitializingManager(EventArgs e)
        {
            if (InitializingManager != null)
            {
                InitializingManager(this, e);
            }
        }

        /// <summary>
        ///   Raises the <see cref = "E:InitializingProvider" /> event.
        /// </summary>
        /// <param name = "e">The <see cref = "T:System.EventArgs" /> instance containing the event data.</param>
        protected virtual void OnInitializingProvider(EventArgs e)
        {
            if (InitializingProvider != null)
            {
                InitializingProvider(this, e);
            }
        }

        /// <summary>
        ///   Raises the <see cref = "E:InitializedProvider" /> event.
        /// </summary>
        /// <param name = "e">The <see cref = "T:System.EventArgs" /> instance containing the event data.</param>
        protected virtual void OnInitializedProvider(EventArgs e)
        {
            if (InitializedProvider != null)
            {
                InitializedProvider(this, e);
            }
        }

        /// <summary>
        ///   Raises the <see cref = "E:Initialized" /> event.
        /// </summary>
        /// <param name = "e">The <see cref = "T:System.EventArgs" /> instance containing the event data.</param>
        protected virtual void OnInitialized(EventArgs e)
        {
            if (Initialized != null)
            {
                Initialized(this, e);
            }
        }

        #endregion
    }

//Class:End
}

//Namespace:End