using System;
using System.Web.UI.WebControls.WebParts;

/// <summary>
    ///   Class of Static Helper functions for common WebPart operations.
    /// </summary>
    public static class WebParts
    {
        #region Helpers - Personalization

        /// <summary>
        /// Checks the personalization scope.
        /// </summary>
        /// <param name="scope">The scope.</param>
        public static void ValidatePersonalizationScope(this PersonalizationScope scope)
        {
            if ((scope < PersonalizationScope.User) || (scope > PersonalizationScope.Shared))
            {
                throw new ArgumentOutOfRangeException("scope");
            }
        }

        #endregion
    }

