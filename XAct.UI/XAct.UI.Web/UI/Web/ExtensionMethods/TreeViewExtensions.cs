﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace XAct
{
    /// <summary>
    /// Extension methods for the TreeViewControl
    /// </summary>
    public static class TreeViewExtensions
    {

        /// <summary>
        ///   Expands the ASP TreeViewControl, when bound to a SiteMap, to the current page's node.
        /// </summary>
        /// <param name = "treeView">The tree view.</param>
        /// <returns>true if page is found.</returns>
        public static void ExpandToCurrenPage(this TreeView treeView, Page page)
        {
            if (page == null)
            {
                throw new ArgumentNullException("currentPage");
            }

            if (treeView == null)
            {
                throw new ArgumentNullException("treeView");
            }

            treeView.Nodes.ExpandTreeViewToCurrentPage(page, null);
        }

        /// <summary>
        ///   Expands the ASP TreeViewControl, when bound to a SiteMap, to the current page's node.
        /// </summary>
        /// <remarks>
        ///   Invoked by <see cref = "ExpandTreeViewToCurrentPage(Page, TreeView)" />
        /// </remarks>
        /// <param name = "currentPage">The current page.</param>
        /// <param name = "treeNodes">The tree nodes.</param>
        /// <param name = "currentPageAbsPath">The current page abs path.</param>
        /// <returns></returns>
        static bool ExpandTreeViewToCurrentPage(this TreeNodeCollection treeNodes, Page currentPage,
                                                      string currentPageAbsPath)
        {
            //Check Args:

            if (string.IsNullOrEmpty(currentPageAbsPath))
            {
                //As Submitted: "http://demos.xact-solutions.com/Demos"

                //Request.Url:"http://demos.xact-solutions.com/Demos/default.aspx"
                //Request.Url.AbsolutePath: "/Demos/default.aspx"
                //CurrentPage: "D:\WEBSITES\DEMOS.XACT-SOLUTIONS.COM\Demos\default.aspx"

                //It sometimes has %20 in there hence UrlDecode
                currentPageAbsPath =
                    currentPage.Server.UrlDecode(currentPage.MapPath(currentPage.Request.Url.AbsolutePath));

                //currentPage.Response.Write(string.Format("<div>Request.Url:{0}</div>",currentPage.Request.Url));
                //currentPage.Response.Write(string.Format("<div>Request.Url.AbsolutePath:{0}</div>",currentPage.Request.Url.AbsolutePath));
                //currentPage.Response.Write(string.Format("<div>CurrentPage:{0}</div>",currentPageAbsPath));
            }

            //currentPage.Response.Write(string.Format("<div>Children:{0}</div>",treeNodes.Count));


            bool result = false;
            // iterate through all nodes to find the current page
            foreach (TreeNode treeNode in treeNodes)
            {
                //Get Node as listed in sitemap.config:
                //eg: "~/Demos/HTCs/Default.aspx"
                //and map to local path:
                //eg: "D:\WEBSITES\DEMOS.XACT-SOLUTIONS.COM\Demos\HTCs\Default.aspx"

                string nodeUrl = treeNode.NavigateUrl;
                string nodeAbsPath = currentPage.MapPath(nodeUrl);

                //currentPage.Response.Write(string.Format("<div><li>nodeUrl:{0}</li></div>",nodeUrl));
                //currentPage.Response.Write(string.Format("<div><li>nodeAbsPath:{0}</li></div>",nodeAbsPath));

                // check to see if the value matches the current url
                //make sure its case insensitive:
                if (string.Compare(nodeAbsPath, currentPageAbsPath, true) == 0)
                {
                    //Found the current page:
                    //currentPage.Response.Write(string.Format("<div>Found Page: {0}</div>",nodeUrl));

                    // Expand the current node to see its children...
                    //then recurse up to root, expanding as we go:
                    TreeNode tmpNode = treeNode;
                    while (tmpNode != null)
                    {
                        tmpNode.Expand();
                        tmpNode = tmpNode.Parent;
                    }
                    // a match has been found so stop looking
                    result = true;
                    break;
                }
                //Not found, so

                //If not called
                treeNode.Expand();
                // recursively call this method to check the children

                if (!treeNode.ChildNodes.ExpandTreeViewToCurrentPage(currentPage, currentPageAbsPath))
                {
                    //Not found, so close up after me:
                    if (treeNode.Parent != null)
                    {
                        treeNode.Collapse();
                    }
                }
                else
                {
                    //Aha...found node in children, so get out early
                    result = true;
                    break;
                }
            } //Loop treeNode

            //currentPage.Response.Write(string.Format("<div>Done.</div>"));

            // false will be returned if the node was never found
            return result;
        }


    }
}
