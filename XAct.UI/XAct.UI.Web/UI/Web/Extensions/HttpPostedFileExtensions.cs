﻿namespace XAct
{
    using System.IO;
    using System.Web;
    using XAct.Net.Messaging;

    /// <summary>
    /// Extensions to the HttpPostedFile object
    /// </summary>
    public static class HttpPostedFileExtensions
    {
        /// <summary>
        /// Copies the contents of a System HttpPostedFileBase To a <see cref="PersistedFile"/>.
        /// </summary>
        /// <param name="httpPostedFileBase">The HTTP posted file base.</param>
        /// <param name="target">The target. If null, a new one will be created.</param>
        public static PersistedFile ToPersistedFile(this HttpPostedFileBase httpPostedFileBase, PersistedFile target=null)
        {
            if (target == null){target = new PersistedFile();}

            target.Name = httpPostedFileBase.FileName;
            target.ContentType = httpPostedFileBase.ContentType;

            target.Value = ResolveCore(httpPostedFileBase);

            return target;
        }

        /// <summary>
        /// Implementation of AutoMapper ValueResolver's primary method.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        static byte[] ResolveCore(HttpPostedFileBase source)
        {
            Stream stream = source.InputStream;
            long currentPos = stream.Position;

            stream.Position = 0;
            byte[] result = ReadAllBytes_Fixed(stream);

            //Return things to where they were:
            stream.Position = currentPos;
            return result;
        }


        static byte[] ReadAllBytes_Fixed(Stream stream)
        {
            var buffer = new byte[64 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

    }
}
