namespace XAct.UI.Web.HttpHandlers
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Reflection;
    using System.Web;

    /// <summary>
    ///   IHttpHandler to generate Thumbnails (resized/smaller) of Images requested.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Beginner web designers often upload images to a server that are far bigger than the size
    ///     they will display at, without remembering that the about of bytes sent to the client is exactly 
    ///     the same whether the image is shown at fullsize (eg: 500x700) or very small (50x70) -- it's just
    ///     the browser that interpolates a smaller rendering. This translates to unnecessary time lost
    ///     sending something that is 100 times larger...(500x700 = 350kb) versus (50x70=3.5kb)! 
    ///   </para>
    ///   <para>
    ///     When you don't have a scripting language running on the server, the general solution is to 
    ///     upload two forms of the image (small/big)...which can be not only tedious, but prone to forgetfulness...
    ///   </para>
    ///   <para>
    ///     With ASP.NET running on the server, things become a lot easer: you upload the original version of the
    ///     image (fullsize), but use an automated handler to resize it as requested -- which is what this 
    ///     handler does.
    ///   </para>
    ///   <para>
    ///     By passing this Handler the path to an image, and one or both of the x/y parameters, the 
    ///     Handler will create a subdirectory called Thumbnails (making a subdirectory makes it easier to 
    ///     delete them if you want to), and a Thumbnail name based off the arguments that it was passed.
    ///   </para>
    ///   <para>
    ///     In other words, if you request a file called 'kitchen.jpg', that is 100px wide, it will 
    ///     generate a filename called 'Templates/kitchen_100_0.jpg', which it will then look for. If it 
    ///     finds it, that is what it will send back. If it doesn't, it will go about opening the original
    ///     'kitchen.jpg', make a copy, resize it, and save it as 'Templates/kitchen_100_0.jpg' before
    ///     continuing as it normally will (ie, sends back the found/just created thumbnail).
    ///   </para>
    /// </remarks>
    public class ThumbNailHttpHandler : IHttpHandler
    {
        #region Public Properties

        private const string _HandlerUrl = "XAct.Thumbnailer.aspx";

        /// <summary>
        ///   Url for this HttpHandler.
        /// </summary>
        [
            ReadOnly(true),
            DefaultValue("XAct.Thumbnailer.aspx"),
            Description("Url for this HttpHandler.")
        ]
        public static string HandlerUrl
        {
            get { return _HandlerUrl; }
        }


        /// <summary>
        ///   Default Width for Images used if no width provided.
        /// </summary>
        public int DefaultX { get; set; }

        /// <summary>
        ///   Implementation of IHttpHandler.IsReusable.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Gets a value indicating whether another request can use the IHttpHandler instance.
        ///   </para>
        /// </remarks>
        [
            ReadOnly(true),
            DefaultValue(true)
        ]
        public bool IsReusable
        {
            get { return true; }
        }

        #endregion

        #region Methods

        /// <summary>
        ///   Implementation of IHttpHandler.ProcessRequest.
        /// </summary>
        /// <param name = "hc">The context object for the request.</param>
        public void ProcessRequest(HttpContext hc)
        {
            Process(hc, DefaultX);
        }

        #endregion

        /// <summary>
        ///   Static. Method used to ensure this IHttpHandler has correctly been registered in web.config file.
        /// </summary>
        /// <returns>Bool. Returns true if correctly installed.</returns>
        public static bool Install(bool qThrowExceptionIfUnsuccessful)
        {
            //Get the Type of the Class that owns this Static method (ie when this.GetType() won't work):
            Type handlerType = MethodBase.GetCurrentMethod().DeclaringType;
            return Installer.InstallHandler(handlerType, HandlerUrl, qThrowExceptionIfUnsuccessful);
        }

//Method End

        /// <summary>
        ///   Called internally by the framework. 
        ///   Enables processing of HTTP Web requests 
        ///   by a custom HttpHandler.
        /// </summary>
        /// <param name = "hc">Current HttpContext</param>
        /// <param name = "defaultX">Default X Value if none found.</param>
        /// <remarks>
        ///   <para>
        ///     This will only be called if this Handler 
        ///     was correctly configured in 
        ///     web.config via the Install() method!
        ///   </para>
        /// </remarks>
        public static void Process(HttpContext hc, int defaultX)
        {
            if (hc == null)
            {
                throw new ArgumentException("hc");
            }
            object o = null;
            int _X = 0;
            int _Y = 0;
            int _DefaultX = defaultX;
            string _Path = string.Empty;

            //Get the Path -- could be in different cases:
            o = hc.Request["Path"];
            if (o == null)
            {
                o = hc.Request["path"];
            }
            if (o == null)
            {
                o = hc.Request["PATH"];
            }
            if (o != null)
            {
                _Path = (string) (o);
            }

            //Get the Width -- again watch out for cases:
            o = hc.Request["x"];
            if (o == null)
            {
                o = hc.Request["X"];
            }
            if (o != null)
            {
                _X = Convert.ToInt32(o);
            }

            //Get the Height -- again watch out for cases:
            o = hc.Request["y"];
            if (o == null)
            {
                o = hc.Request["Y"];
            }
            if (o != null)
            {
                _Y = Convert.ToInt32(o);
            }

            //Get the DefaultX -- again watch out for cases:
            o = hc.Request["DefaultX"];
            if (o == null)
            {
                o = hc.Request["defaultx"];
            }
            if (o == null)
            {
                o = hc.Request["DEFAULTX"];
            }
            if (o == null)
            {
                o = hc.Request["defaultX"];
            }
            if (o != null)
            {
                _DefaultX = Convert.ToInt32(o);
            }

            //Call the helper function.
            //Why use another method for this? Because it is static, and 
            //therefore accessible from the Control as well...

            bool tSuccess = Process(hc, _Path, _X, _Y, _DefaultX);
            //No return.
        }


        /// <summary>
        ///   Finds the image specified and changes the Response.ContentType to match the type,
        ///   then fills the Response stream with a Base64 representation of the image stream.
        /// </summary>
        /// <param name = "hc">Current HttpContext</param>
        /// <param name = "qFilePath">Path to image.</param>
        /// <param name = "qX">Width in pixels</param>
        /// <param name = "qY">Height in pixels</param>
        /// <param name = "qDefaultX">Default Width in pixels.</param>
        /// <returns>true if image rendered. False if original image and thumbnail not found.</returns>
        public static bool Process(HttpContext hc, string qFilePath, int qX, int qY, int qDefaultX)
        {
            //Cropping is done a little differently:
            //create thumbnail from the original bitmap
            //dim myBitmapCropped as New Bitmap(200,100)
            //dim myGraphic = Graphics.FromImage(myBitmapCropped)
            //crop to the graphic object from the original bitmap
            //myGraphic.DrawImage(myBitmap, new Rectangle(0,0,myBitmapCropped.Width,myBitmapCropped.Height),100,100,myBitmapCropped.Width,myBitmapCropped.Height,GraphicsUnit.Pixel)
            //myGraphic.dispose()


            //Does the image exist?
            if (File.Exists(qFilePath))
            {
                //We need all the various parts because we are going to make subdirectories, etc.
                string tFileDir = Path.GetDirectoryName(qFilePath);
                string tFileName = Path.GetFileName(qFilePath);
                string tFileNameBase = Path.GetFileNameWithoutExtension(qFilePath);
                string tFileNameExt = Path.GetExtension(qFilePath);
                //I want the extension -- but I don't want the "." so 
                //that I can easily make a ContentType string later:
                if (tFileNameExt.Length > 0)
                {
                    tFileNameExt = tFileNameExt.Substring(1);
                }
                //The directory wherer the Thumbnails will live is one down:
                string tThumbNailSubDir = tFileDir + "ThumbNails\\";

                //The filename is going to be Name + X + Y + Extension -- and in lowercase:
                string tThumbNailName = tFileNameBase + "_" + qX + "_" + qY + tFileNameExt;
                tThumbNailName = tThumbNailName.ToLower();

                Bitmap oThumbNail;

                if (File.Exists(tThumbNailSubDir + tThumbNailName) == false)
                {
                    //Need to make Directory?
                    if (Directory.Exists(tThumbNailSubDir) == false)
                    {
                        Directory.CreateDirectory(tThumbNailSubDir);
                    }
                    //Need to make new Image:
                    Bitmap oBmpOrig = new Bitmap(qFilePath);

                    double tXYRatio = (oBmpOrig.Height/oBmpOrig.Width);

                    if (qDefaultX == 0)
                    {
                        qDefaultX = oBmpOrig.Width;
                    }

                    if (qX == 0)
                    {
                        if (qY == 0)
                        {
                            qX = qDefaultX;
                            qY = Convert.ToInt32(qX*Math.Round(tXYRatio));
                        }
                        else
                        {
                            qX = Convert.ToInt32(qY/Math.Round(tXYRatio));
                        }
                    }
                    if (qY == 0)
                    {
                        qY = Convert.ToInt32(qX*Math.Round(tXYRatio));
                    }

                    oThumbNail = new Bitmap(oBmpOrig, qX, qY);

                    oThumbNail.Save(tThumbNailSubDir + tThumbNailName);
                }
                else
                {
                    oThumbNail = new Bitmap(tThumbNailSubDir + tThumbNailName);
                }

                //Use Extension(minus the dot) to make response type:
                hc.Response.ContentType = "image/" + tFileNameExt.ToLower();

                if (tFileNameExt.ToLower() == "png")
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        oThumbNail.Save(memoryStream, (ImageFormat) Enum.Parse(typeof (ImageFormat), tFileNameExt, true));
                        memoryStream.WriteTo(hc.Response.OutputStream);
                    }
                }
                else
                {
                    oThumbNail.Save(hc.Response.OutputStream,
                                    (ImageFormat) Enum.Parse(typeof (ImageFormat), tFileNameExt, true));
                }
                return true;
            }
            else
            {
                hc.Response.StatusCode = 404;
                hc.Response.StatusDescription = "Response Not Found";
                hc.Response.Write("File not found.");
            }
            return false;
        }

//Method:End
    }


}


