namespace XAct.UI.Web.HttpHandlers
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Reflection;
    using System.Web;
    using System.Xml;
    using XAct.IO;

    ///<summary>
    ///  Handler for uploading files.
    ///</summary>
    ///<remarks>
    ///  <para>
    ///    Originally written to be the backend for the TextBox_RichEdit control to provide asynch
    ///    real-time uploading and using of images, but can be used with a host of other controls.
    ///  </para>
    ///  <para>
    ///    With the RichText control, the ClientSide script prepares an XML document and posts 
    ///    it to this url. 
    ///  </para>
    ///  <para>
    ///    The request format should be in the following xml format:
    ///    <![CDATA[
    ///
    /// <root>
    ///	<file>
    ///		<filename>MyFile2.dat</filename>
    ///		<charbuff>....base64 string...</charbuffer>
    ///	</file>
    ///	<file>
    ///		<filename>MyFile2.dat</filename>
    ///		<charbuff>....base64 string...</charbuffer>
    ///	</file>
    ///	...etc...
    /// </root>
    /// ]]>
    ///  </para>
    ///</remarks>
    ///<example>
    ///  <para>
    ///    The javascript that will work with it looks as follows:
    ///  </para>
    ///  <code>
    ///    <![CDATA[
    /// function FileUploader(){
    /// this._Name = "FileUploader";
    /// 	this.LocalPath;
    /// 	this.LocalFileName;
    /// 	
    /// 	this.LastFilePath;
    /// this.LastStatus;
    /// 	this.LastStatusMsg;
    /// this.Img;
    /// this.ServerPath = "./XAct/Upload/";
    /// }
    /// FileUploader.prototype.Send=function(fileName,qImgObject){
    /// var oC = this;
    /// oC.LocalPath = fileName;
    /// oC.LocalFileName = oC.LocalPath.substr(oC.LocalPath.lastIndexOf("\\")+1);
    /// 
    /// 	
    /// 	oC.Img = qImgObject;
    /// 	//Allows for asynch going...nice
    /// 	window.setTimeout(function(){oC._SendII()},100);
    /// }
    /// 
    /// FileUploader.prototype._SendII=function(){
    /// 	var oC = this;
    /// 	var oDoc,oRoot,oFileInfo,oFileName, oFileBuffer,tStream,xmlhttp;
    /// 	// create ADO-stream Object
    /// 	try {
    /// 		tStream = new ActiveXObject("ADODB.Stream");
    /// 		oDoc = new ActiveXObject("MSXML.DOMDocument");
    /// 		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    /// 	}
    /// 	catch (e){
    /// 		alert ("Error uploading File:\n" + e.message);
    /// 		return;
    /// 	}
    ///    oRoot = oDoc.createElement("root");oDoc.appendChild(oRoot);
    ///    // specify namespaces datatypes
    ///    oDoc.documentElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");
    /// 
    /// 	oFileInfo = oDoc.createElement("file");oRoot.appendChild(oFileInfo);
    /// 	oFileName = oDoc.createElement("name");oFileInfo.appendChild(oFileName);
    /// 	oFileBuffer = oDoc.createElement("buffer");oFileInfo.appendChild(oFileBuffer);
    /// 	oFileBuffer.dataType = "bin.base64";
    /// 
    /// 	//Get the LocalPath:   
    /// 	oFileName.text = oC.LocalPath;
    /// 	//Get the File contents:
    /// 	try {
    /// 		tStream.Type = 1;  // 1=adTypeBinary 
    /// 		tStream.Open(); 
    /// 		tStream.LoadFromFile(oC.LocalPath);
    /// 		oFileBuffer.nodeTypedValue = tStream.Read(-1); // -1=adReadAll
    /// 		tStream.Close();
    /// 	}catch (e){
    /// 		alert ("Error encoding file: " + "\n" + e.message);
    /// 		return;
    /// 	}
    ///    // send XML documento to Web server
    ///    xmlhttp.open("POST","./XAct.Uploader.aspx",false);
    ///    xmlhttp.send(oDoc);
    /// 
    /// 	oC.LastFilePath = oC.LocalPath;
    /// 	oC.LocalPath = "";
    /// 	oReturned = new ActiveXObject("MSXML.DOMDocument");
    /// 	oReturned.loadXML(xmlhttp.responseText);
    ///    
    /// 	var oRoot = oReturned.documentElement;
    /// 	
    /// 	
    /// 	if (!oRoot){
    /// 		oC.LastStatus = 0;
    /// 	oC.LastStatusMsg = "Incorrect XML";
    /// 	}else{
    /// 		oC.LastStatus = oRoot.childNodes[0].text;
    /// 		oC.LastStatusMsg = oRoot.childNodes[1].text;
    /// 	}
    /// 	if (!oC.LastStatus){
    /// 		alert (oC.LastStatusMsg);
    /// 	}
    /// 	if (oC.Img){
    /// 	oC.Img.src = oC.ServerPath + oC.LocalFileName;
    /// 	}
    /// }
    /// /*
    /// function DoSend(fileName,qCanvas){
    /// 	var tObj = document.all[fileName];
    /// 	if (tObj){fileName = tObj.value;}
    /// 
    /// 	var tObj = document.all[qCanvas];
    /// 	if (tObj){qCanvas =tObj}
    /// 	if (!qCanvas){qCanvas = document.body;}
    /// 
    /// 	oImg = document.createElement("IMG");
    /// 	qCanvas.appendChild(oImg);
    /// 	FU.Send(fileName,oImg);
    /// }
    /// */
    /// </script>
    /// ]]>
    ///  </code>
    ///</example>
    public class FileUploadHttpHandler : IHttpHandler
    {
        #region Public Properties

        private const string _HandlerUrl = "XAct.XUpload.aspx";

        /// <summary>
        ///   Url for this HttpHandler.
        /// </summary>
        [
            ReadOnly(true),
            DefaultValue("XAct.XUpload.aspx"),
            Description("Url for this HttpHandler.")
        ]
        public static string HandlerUrl
        {
            get { return _HandlerUrl; }
        }

        /// <summary>
        ///   Implementation of IHttpHandler.IsReusable.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Gets a value indicating whether another request can use the IHttpHandler instance.
        ///   </para>
        /// </remarks>
        [
            ReadOnly(true),
            DefaultValue(true)
        ]
        public bool IsReusable
        {
            get { return true; }
        }

        #endregion

        #region Static Properties

        private static string _UploadDir = string.Empty;

        ///<summary>
        ///  Directory where Uploaded files will be created.
        ///</summary>
        ///<remarks>
        ///  <para>
        ///    The default value is <c>~/XAct/Uploads</c>. If you want to change it, 
        ///    modify the web.config as follows:
        ///    <code>
        ///      <![CDATA[
        ///   <appSettings>
        ///		<add key="XActUploadDirectory" value="~/MyUploadDir"  />
        ///		</appSettings>
        ///	]]>
        ///    </code>
        ///  </para>
        ///</remarks>
        ///<internal>
        ///  Note that this is a Static property...
        ///</internal>
        ///<default>"~XAct/Uploads"</default>
        private static string UploadDir
        {
            get
            {
                if (_UploadDir == string.Empty)
                {
                    HttpContext hc = HttpContext.Current;
                    if (hc != null)
                    {
                        _UploadDir = ConfigurationManager.AppSettings["XActUploadDirectory"];
                    }
                    if ((_UploadDir == null) || (_UploadDir == string.Empty))
                    {
                        _UploadDir = "XAct/Uploads";
                    }
                }
                return _UploadDir;
            }
        }

        /// <summary>
        ///   Static Array that is kept in the Session object, that holds pointers to uploaded files.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     When the ISession handler is deleted, it will call 
        ///   </para>
        /// </remarks>
        /// <internal>
        ///   Note that this is a Static property...
        /// </internal>
        private static UploadFileList List
        {
            get
            {
                //Get current context:
                HttpContext hc = HttpContext.Current;
                UploadFileList tResult = null;
                if (hc != null)
                {
                    //Get the current defined file list:
                    tResult = (UploadFileList) hc.Session["XACT_UPLOAD_FILELIST"];
                    //If not defined, make one and save it in session object:
                    if (tResult == null)
                    {
                        hc.Session["XACT_UPLOAD_FILELIST"] = tResult = new UploadFileList();
                    }
                }
                //Return the file list:
                return tResult;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///   Implementation of IHttpHandler.ProcessRequest.
        /// </summary>
        /// <param name = "hc">The context object for the request.</param>
        public void ProcessRequest(HttpContext hc)
        {
            Process(hc);
        }

        #endregion

        #region Static Methods

        /// <summary>
        ///   Static. Method used to ensure this IHttpHandler has correctly been registered in web.config file.
        /// </summary>
        /// <returns>Bool. Returns true if correctly installed.</returns>
        public static bool Install(bool qThrowExceptionIfUnsuccessful)
        {
            //Get the Type of the Class that owns this Static method (ie when this.GetType() won't work):
            Type handlerType = MethodBase.GetCurrentMethod().DeclaringType;
            return Installer.InstallHandler(handlerType, HandlerUrl, qThrowExceptionIfUnsuccessful);
        }

//Method End


        /// <summary>
        ///   Used internally by ProcessRequest.
        /// </summary>
        /// <param name = "hc">The context object for the request.</param>
        /// <returns>True if upload happened without incident.</returns>
        public static bool Process(HttpContext hc)
        {
            string modeFlag = hc.Request.Params["Mode"];
            if ((modeFlag != null) || (modeFlag != string.Empty))
            {
                modeFlag = modeFlag.ToUpper();
            }
            if (modeFlag == "BINARY")
            {
                return ProcessUploadADOStream(hc);
            }
            return ProcessUploadNormal(hc);
        }

        /// <summary>
        ///   Used when Client sends file via Form and HtmlFile controls.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     This is the tradional method to handle file uploads with ASP.NET.
        ///   </para>
        ///   <para>
        ///     Because this tradional method involves click backs, and really makes RichEditors cumbersome to
        ///     use when working with images, we developed an alternate solution (see the ADOStream solution).
        ///   </para>
        /// </remarks>
        /// <param name = "hc">Current HttpContext</param>
        /// <returns>True if upload and save was successful.</returns>
        private static bool ProcessUploadNormal(HttpContext hc)
        {
            string tDir = string.Empty;

            if (!UploadDirExists())
            {
                return false;
            }


            IFSIOService ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            //Clean up any old files in the upload directory.
            ClearUploadDir();


            //Loop through each file that has been posted:
            foreach (HttpPostedFile oFile in hc.Request.Files)
            {
                // Allocate a byte buffer for reading of the file
                byte[] myData = new byte[oFile.ContentLength];
                // Read uploaded file from the Stream
                oFile.InputStream.Read(myData, 0, myData.Length);
                //Make the save path:
                //Notice that if the user sent pathing information, we 
                //DO NOT USE IT!
                string fileName = Path.GetFileName(oFile.FileName);
                string fullPath = tDir + "/" + fileName;
                Stream newFile = null;
                try
                {
                    // Create a file
                    newFile = ioService.FileOpenWriteAsync(fullPath, true).WaitAndGetResult();
                    // Write data to the file
                    newFile.Write(myData, 0, myData.Length);
                    // Close file
                    newFile.Close();
                    //Add to FileList:
                    List.ListLast.Add(fullPath);
                    List.ListAll.Add(fullPath);
                }
                catch
                {
                }
                finally
                {
                    if (newFile != null)
                    {
                        newFile.Close();
                    }
                }
            }
            return true;
        }

//Method:End

        ///<summary>
        ///  Private Method. Used when Client sends file via ADOStream/BinaryXML technique.
        ///</summary>
        ///<param name = "hc">Current HttpContext</param>
        ///<returns>True if upload and save was successful.</returns>
        ///<remarks>
        ///  This is my preferred technique as it doesn't 
        ///  require a Post/Refresh on each file upload. 
        ///  But it does require JS code to be injected into the Client's page 
        ///  in order to create the XML and send it to this IHttpHandler...
        ///</remarks>
        private static bool ProcessUploadADOStream(HttpContext hc)
        {
            //Get the incoming stream:
            Stream streamIn = hc.Request.InputStream;

            //Just in case it has been switched at some point?
            hc.Response.Expires = 0;
            hc.Response.ContentType = "text/xml";

            //Create a Doc to read the XML stream that is being sent:
            XmlDocument oXDocIn = new XmlDocument();

            //Create a Doc to write the answer to:
            XmlDocument oXMLReport = new XmlDocument();

            //Structure we want to send back is:
            //<report>
            //	 <status>True</status>
            //	 <status_msg>Doing Fine...</status_msg>
            //</report>

            //So create these nested xml nodes:
            XmlNode oReportRoot = oXMLReport.CreateElement("report");
            oXMLReport.AppendChild(oReportRoot);
            XmlNode oReportStatus = oXMLReport.CreateElement("status");
            oReportRoot.AppendChild(oReportStatus);
            XmlNode oReportStatusMsg = oXMLReport.CreateElement("status_msg");
            oReportRoot.AppendChild(oReportStatusMsg);

            //Load the incoming stream text all in one go into a new doc to parse it:
            oXDocIn.LoadXml(new StreamReader(streamIn).ReadToEnd());

            //Start counters, etc.
            int tCountTotal = 0;
#pragma warning disable 219
            int tCountSuccessful = 0;
#pragma warning restore 219
            int tBufferTransferred = 0;
            bool tResult = true;

#pragma warning disable 219
            string tStatusMsg = string.Empty;
#pragma warning restore 219

            if (!UploadDirExists())
            {
                //If the Upload Directory could not be found
                //report it.
                tResult = false;
                oReportStatusMsg.InnerText = "- Error: No Upload directory specified.";
                //We normally would get out here since we are obviously not doing 
                //to well...but we want this error message to go out as an xml doc.
                //so we need to finish up right to the end, skipping what we would
                //have done if tResult were still True...
            }


            if (tResult)
            {
                //Clean up any old files in the upload directory.
                ClearUploadDir();
            }


            //We are now ready to proceed and process the incoming xml
            //file.

            //We are expecting an incoming xml file with the following format:
            //<root>
            //	<file>
            //		<filename>MyFile.dat</filename>
            //		<charbuff>....</charbuffer>
            //	</file>
            //	<file>
            //		<filename>MyFile.dat</filename>
            //		<charbuff>....</charbuffer>
            //	</file>
            //	...etc...
            //</root>

            if (tResult)
            {
                //Are there any FILE subnodes?
                //if not - get out.
                tCountTotal = oXDocIn.DocumentElement.ChildNodes.Count;
                tBufferTransferred = 0;
                if (tCountTotal == 0)
                {
                    tResult = false;
                    oReportStatusMsg.InnerText = "- Error: No Child Nodes (file names/info) found.";
                }
            }
            if (tResult)
            {
                //Loop through each FILE node:
                foreach (XmlNode oInfo in oXDocIn.DocumentElement.ChildNodes)
                {
                    tStatusMsg = string.Empty;
                    //Each node should have atleast 2 subnodes:
                    //1) filename, 
                    //2) charbuffer
                    if (oInfo.ChildNodes.Count > 1)
                    {
                        //Get first node (FILENAME):
                        string tFileName = Path.GetFileName(oInfo.ChildNodes[0].InnerText);
                        //Get second node (base64 BYTEBUFFER):
                        Byte[] tBuffer = Convert.FromBase64String(oInfo.ChildNodes[1].InnerText);
                        //Now that we have both parts:
                        //make an outgoing stream, write to it and save it to the directory
                        //then close the outgoing file stream:
                        FileStream tStreamOut = null;
                        string fullPath = UploadDir + "/" + tFileName;

                        try
                        {
                            //Start a new file:
                            tStreamOut = File.OpenWrite(fullPath);
                            //Write to it:
                            tStreamOut.Write(tBuffer, 0, tBuffer.Length);
                            //Seems ok so far, so increment the counters and success lists:
                            tCountSuccessful += 1;
                            tBufferTransferred += tBuffer.Length;
                            List.ListLast.Add(fullPath);
                            List.ListAll.Add(fullPath);
                        }
                        catch (Exception E)
                        {
                            //Hum not going so well...
                            oReportStatusMsg.InnerText = "Error: " + E.Message;
                            tResult = false;
                            //Break out of the loop
                            break;
                        }
                        finally
                        {
                            //Either way close the open file we were writting to:
                            if (tStreamOut != null)
                            {
                                tStreamOut.Close();
                            }
                        }
                    } //End of 'enough nodes?'
                } //End Foreach
            } //End of 'if tResult!=null'
            //Report string is splittable by spaces:
            if (tResult)
            {
                oReportStatusMsg.InnerText = "+ Success: " + tCountTotal + " " + "Uploaded." + " (" + tBufferTransferred +
                                             "bytes.)";
            }
            oReportStatus.InnerText = (tResult) ? "1" : "0";

            //Write the xml of the whole outgoing xml doc to the outgoing stream:
            hc.Response.Write(oXMLReport.OuterXml);

            //Done.
            return tResult;
        }

//Method:End


        /// <summary>
        /// Deletes all files in a directory that are older than 12 hours.
        /// </summary>
        /// <returns>
        /// -1 if Error, otherwise count of files actually deleted.
        /// </returns>
        /// <remarks>
        ///   <para>
        /// Called by Process.
        ///   </para>
        ///   <para>
        /// It goes without saying that this method should be used with EXTREME care!
        /// Because it is so dangerous, it comes with two criteria built into it:
        /// a) the directory name given must include the word 'upload' somewhere in in,
        /// b) If the directory has files in it that are older than 7 days, then it's most
        /// probably NOT a current/working upload directory, so nothing is deleted.
        /// This might seem like a pain -- but the option of deleting your whole base virtual directory
        /// by accident is a lot worse...
        ///   </para>
        /// </remarks>
        public static int ClearUploadDir()
        {
            int tResult = -1;

            if (!UploadDirExists())
            {
                return tResult;
            }

            if (UploadDir.ToUpper().IndexOf("UPLOAD") < -1)
            {
                return tResult;
            }
            DateTime tCheck = DateTime.Now.Subtract(new TimeSpan(-12, 0, 0));
            DateTime tCheckWeekAgo = DateTime.Now.Subtract(new TimeSpan(-7, 0, 0, 0));

            DateTime tLastWriteTime = Directory.GetLastWriteTime(UploadDir);
            if (tLastWriteTime < tCheckWeekAgo)
            {
                //Directory remained untouched for over a week.
                //Not ok to continue.
                return tResult;
            }
            //Time to look:
            ArrayList tDeleteMe = new ArrayList();
            string[] tFiles =  DependencyResolver.Current.GetInstance<IFSIOService>().GetDirectoryFileNamesAsync(UploadDir).WaitAndGetResult();

            foreach (string tFileName in tFiles)
            {
                tLastWriteTime = Directory.GetLastWriteTime(tFileName);
                if (tLastWriteTime < tCheckWeekAgo)
                {
                    //File has been there over a week.
                    //Alarm!
                    //We're not in a valid directory!!!
                    return tResult;
                }
                if (tLastWriteTime < tCheck)
                {
                    //File has been there over [n] hours. Ok to delete at end:
                    tDeleteMe.Add(tFileName);
                }
            }

            //Ok. Sounds like it's ok to delete after all:
            //Set the counter:
            tResult = 0;
            foreach (string tFileName in tDeleteMe)
            {
                File.Delete(tFileName);
                tResult += 1;
            }

            //Get out:
            return tResult;
        }

//Method:End

        /// <summary>
        ///   Checks to see if Upload directory exists, or can be created if not.
        /// </summary>
        /// <returns>True if qDir exists.</returns>
        /// <remarks>
        ///   <para>
        ///     Returns False if dir does not exist and could not be created. True if existed, or was able to create it.
        ///   </para>
        ///   <para>
        ///     If qDir == string.Empty, returns false.
        ///   </para>
        /// </remarks>
        private static bool UploadDirExists()
        {
            string qDir = UploadDir;

            if (qDir == string.Empty)
            {
                return false;
            }
            string tPath = qDir;
            try
            {
                tPath = HttpContext.Current.Server.MapPath(qDir);
            }
            catch
            {
            }

            if (Directory.Exists(tPath))
            {
                return true;
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(tPath);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        #endregion

        #region INTERNAL (SubClasses, Enums, Etc.)

        //================================================
        //SUB CLASSES
        //================================================

        /// <summary>
        ///   Internal class used to manage files.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     As long as the program terminates correctly, this might work...
        ///   </para>
        ///   <para>
        ///     The idea is that this array belongs in memory -- within the Session object. 
        ///     When the Session object terminates, it will clear all its entries -- including
        ///     the private var that holds this array... When it clears it, this class will 
        ///     self-destruct, but just before that, it will call it's destructor... Which will
        ///     go through and delete all files that it is pointing to... Which should give 
        ///     a clean directory.
        ///   </para>
        ///   <para>
        ///     The problem is that if the program is terminated abnormally, it will not clear
        ///     the session object correctly -- and therefore will not call the terminator!
        ///     I'm not sure exactly how to guarantee that it does -- except if I could wrap the whole
        ///     app in a try/catch -- but not much chance of that...
        ///   </para>
        /// </remarks>
        private class UploadFileList
        {
            private readonly ArrayList _ListAll = new ArrayList();
            private readonly ArrayList _ListLast = new ArrayList();

            public ArrayList ListAll
            {
                get { return _ListAll; }
            }

            public ArrayList ListLast
            {
                get { return _ListLast; }
            }


            /// <summary>
            ///   Destructor.
            /// </summary>
            /// <remarks>
            ///   Calls <see cref = "CleanFiles" /> to delete all files that it still has refs to.
            /// </remarks>
            ~UploadFileList()
            {
                CleanFiles();
            }

            /// <summary>
            ///   Wbe called, clears out the list, deleting any files that still exist.
            /// </summary>
            public void CleanFiles()
            {
                while (_ListAll.Count > 0)
                {
                    string tFile = (string) ListAll[0];
                    if (File.Exists(tFile))
                    {
                        try
                        {
                            File.Delete(tFile);
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }



        #endregion
    }


}


