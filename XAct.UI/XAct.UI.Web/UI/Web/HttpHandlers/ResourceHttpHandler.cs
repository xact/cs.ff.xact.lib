//Assembly Ref: System.Design.dll

namespace XAct.UI.Web.HttpHandlers
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.Design;

    /// <summary>
    ///   Handler for returning embedded resources.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Self installs itself if the 
    ///     <see cref = "Install" /> function 
    ///     is called, and has the rights to 
    ///     do so 
    ///     (see <see cref = "XAct.UI.Web.HttpHandlers.Installer" /> for 
    ///     notes on what is required to install correctly).
    ///   </para>
    /// </remarks>
    public class ResourceHttpHandler : IHttpHandler
    {
        #region Constants

        /// <summary>
        ///   The AppRelative directory where there resources
        ///   are dumped to for quicker subsequent access.
        /// </summary>
        public const string DumpBaseDir = "~/XAct/";

        #endregion

        /// <summary>
        ///   KeyWord that <c>MakeResourceUrl</c> 
        ///   recognizes as being the same as Resource Manifest.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     This property works in conjunction with 
        ///     the <c>MakeResourceUrl</c> method.
        ///   </para>
        ///   <para>
        ///     Most solutions for embedding resources use 
        ///     the full ResoureStream path to a resource -- but
        ///     this causes a lot of portability issues -- namely, 
        ///     if the file is repackaged in an another
        ///     assembly the assembly name and/or the 
        ///     root namespace of the resource will change.
        ///   </para>
        ///   <para>
        ///     To get around this, we created a 
        ///     'virtual directory name' -- something that this class will
        ///     recognize as indicating that the image requested 
        ///     IS a resource, and to look in the current
        ///     assembly for it.
        ///   </para>
        ///   <para>
        ///     For a more complete description of how to 
        ///     attach embedded resources, 
        ///     see 'ClientResources'.
        ///   </para>
        /// </remarks>
        public const string KeyWord = "XResources";


        /// <summary>
        ///   TODO
        /// </summary>
        public static string CacheLocation
        {
            get
            {
                //But it could be made to rely on Session easily:
                string r =
                    ConfigurationManager.AppSettings["XResources_Cache"];

                return (r != null) ? r.ToLower().Substring(0, 1) : "s";
            }
        }

        #region Public Properties

        private const string _HandlerUrl = "XAct.XResource.aspx";

        /// <summary>
        ///   Url for this HttpHandler.
        /// </summary>
        [
            ReadOnly(true),
            DefaultValue("XAct.XResource.aspx"),
            Description("Url for this HttpHandler.")
        ]
        public static string HandlerUrl
        {
            get { return _HandlerUrl; }
        }

        /// <summary>
        ///   Implementation of IHttpHandler.IsReusable.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Gets a value indicating whether another request can use the IHttpHandler instance.
        ///   </para>
        /// </remarks>
        [
            ReadOnly(true),
            DefaultValue(true)
        ]
        public bool IsReusable
        {
            get { return true; }
        }

        #endregion

        #region Methods

        /// <summary>
        ///   Implementation of IHttpHandler.ProcessRequest.
        /// </summary>
        /// <param name = "hc">The context object for the request.</param>
        public void ProcessRequest(HttpContext hc)
        {
            Process(hc);
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        ///   Static. Method used to ensure this 
        ///   IHttpHandler has correctly been registered in web.config file.
        /// </summary>
        /// <returns>Bool. Returns true if correctly installed.</returns>
        public static bool Install(bool qThrowExceptionIfUnsuccessful)
        {
            //Get the Type of the Class that owns this 
            //Static method (ie when this.GetType() won't work):
            Type handlerType =
                MethodBase.GetCurrentMethod().DeclaringType;

            return Installer.InstallHandler(
                handlerType,
                HandlerUrl,
                qThrowExceptionIfUnsuccessful);
        }

        //Method End


        ///<summary>
        ///  Creates Paths to embedded Resources.
        ///</summary>
        ///<remarks>
        ///  <para>
        ///    Solves several requirements:
        ///    a) Sends to client short token rather than long path.
        ///    b) Sends to client enough information for 
        ///    receiving IHttpHandler to save the Resource to FileSystem.
        ///    c) Because all this info is sent to Client, 
        ///    and therefore roundtripped to IHttpHandler, 
        ///    no dependancies required.
        ///  </para>
        ///  <para>
        ///    The resulting paths will be one of the following formats:
        ///    <list type = "table">
        ///      <item>
        ///        <term>
        ///          "../images/bold.gif"
        ///        </term>
        ///        <description>
        ///          the path of the filename was not the 
        ///          special <c>Keyword</c>, so
        ///          we can assume it is not a resource -- so 
        ///          returned exactly as given.
        ///        </description>
        ///      </item>
        ///      <item>
        ///        <term>
        ///          XAct.Resources.aspx?ruid=39382
        ///        </term>
        ///        <description>
        ///          The path given This points to a ResourcePackage 
        ///          that can be found at Session[39382]
        ///        </description>
        ///      </item>
        ///      <item>
        ///        <term>
        ///          <![CDATA[
        ///			"a=" + resourcePackage.Assembly.GetName().Name +
        ///			"&t=" + resourcePackage.ClassType.ToString() +
        ///			"&r=" + resourcePackage.ResourcePath;
        ///			]]>
        ///        </term>
        ///        <description>
        ///        </description>
        ///      </item>
        ///      <item>
        ///        <term>
        ///        </term>
        ///        <description>
        ///        </description>
        ///      </item>
        ///    </list>
        ///  </para>
        ///</remarks>
        ///<inhouse>
        ///  The IHttpHandler reverses the process  by:
        ///  a) Checking the AppSetting for where to scan for the RUID,
        ///  b) Get the ResourcePackage pointed to by the RUID,
        ///  c) Check tResults[3] for FullFilePath.
        ///  c.1) if not built, make unique from WebSite + TypeName + FileName. 
        ///  Then find resource and save to it if File doesn't exist.
        ///  c.2) update the Assembly store so that
        ///  this check is not redone during this session. 
        ///  d) Although Images and CSS should be dumped 
        ///  to file to allow user to do as he pleases, 
        ///  some JS should remain proprietary...
        ///  therefore, resourcePackage.DumpToFileOk 
        ///  tells the IHttpHandler whether or not making a 
        ///  File is ok. If not, it then gets it from the manifest of course.
        ///  <para />
        ///  Worried about mem.storage? 
        ///  Let's see: 100 gifs per app * avg.15 chars 
        ///  of info = 1500 chars. * 100 users/machine = 
        ///  150000. 150k. Nuttin!.
        ///</inhouse>
        ///<lastmodified>SLS 7/8/2004</lastmodified>
        ///<lastmodified>SLS 8/21/2004</lastmodified>
        public static string MakeResourceUrl(
            Control control,
            string resourceFileNameFull,
            bool dumpToFileOk)
        {
            //Verify arguments:
            control.ValidateIsNotDefault("control");

            if (string.IsNullOrEmpty(resourceFileNameFull))
            {
                resourceFileNameFull = string.Empty;
            }

            //Corrections:
            string resourceDirectory =
                Path.GetDirectoryName(resourceFileNameFull);

            //Vars neeeded:
            string resultUrl = string.Empty;

            //Ensure that this Handler is Installed:
            //Get the Type of the Class that owns this 
            //Static method (ie when this.GetType() won't work):
            Install(false);


            if (string.Compare(resourceDirectory, KeyWord, true) != 0)
            {
                //This is NOT a resource because 
                //the path is NOT the special Keyword.
                //So just make the url assuming that it is a valid file path
                //from hard-drive instead of url:
                return resourceFileNameFull;
            }

            ResourcePackage resourcePackage =
                new ResourcePackage(
                    //The short name of the assembly within which the control is in.
                    control.GetType().Assembly.GetName().Name,
                    //The name of the control
                    control.GetType().Name,
                    resourceFileNameFull);

            if (HttpContext.Current == null)
            {
                //Damn: In DesignTime...
                //Skip all the fancy work, and just pass 
                //the vars themselves rather than RUID:
                resultUrl =
                    HandlerUrl + "?" +
                    resourcePackage.UrlParameters;
            }
            else
            {
                //RunTime:

                //Save the package:
                ResourcePackage.ResourcePackageStore[resourcePackage.RUID]
                    = resourcePackage;

                //Create the Url using the HandlerUrl + roundTripVars:
                resultUrl = HandlerUrl + "?ruid=" + resourcePackage.RUID;
            }
            return resultUrl;
        }

        //Method:End:MakeResourceUrl


        /// <summary>
        ///   Used internally by ProcessRequest.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///   </para>
        ///   <para>
        ///     Extracts from Request.Parameters the 
        ///     integer value of 'ruid' that was 
        ///     created by <c>MakeResourceUrl</c>,
        ///     and if not found, extracts with 'a', 't', and 'r' paremeters.
        ///   </para>
        ///   <para>
        ///     Note:
        ///     If in runtime,the Client will be requesting via 'ruid',
        ///     whereas in DesignTime, the Control could not get 
        ///     a handle on Request object or Application 
        ///     array -- so instead is sending the whole info.
        ///   </para>
        /// </remarks>
        /// <param name = "hc">The context object for the request.</param>
        /// <returns>True if upload happened without incident.</returns>
        public static bool Process(HttpContext hc)
        {
            //bool runTimeMode = (hc != null);

            hc.Response.Clear();
            ResourcePackage resourcePackage;

            string ruid = hc.Request.Params["ruid"];
            if (ruid == null)
            {
                //DesignTime:
                //Get the ResourceUID (RUID) of the Resource:
                resourcePackage =
                    ResourcePackage.Extract(
                        hc.Request.Params["a"],
                        hc.Request.Params["t"],
                        hc.Request.Params["r"]);
            }
            else
            {
                //RunTime:
                //Get the Pieces out of memory storage, using the Ruid as the key:
                resourcePackage =
                    ResourcePackage.Extract(Convert.ToInt32(ruid));
            }

            CalcDumpFileName(resourcePackage);


            //Get the Stream:
            string mimeType =
                GetResourceMimeType(resourcePackage.ResourcePath);

            string foundFullResourcePath = string.Empty;
            Stream resourceStream = null;

            if (File.Exists(resourcePackage.DumpFileName))
            {
                //Get the Stream from File:
                resourceStream = File.OpenRead(resourcePackage.DumpFileName);
            }
            else
            {
                //Get the Stream from Embedded Resource:
                resourceStream =
                    GetResourceStreamFromAssembly(resourcePackage.ResourcePath,
                                                  null,
                                                  out foundFullResourcePath);

                if ((resourceStream != null) && (resourcePackage.DumpOk))
                {
                    //We need to WRITE this to file:
                    Stream tS2 = GetResourceStreamFromAssembly(
                        resourcePackage.ResourcePath,
                        null,
                        out foundFullResourcePath);

                    byte[] tFileBuffer = new byte[tS2.Length];
                    FileStream oFS =
                        File.Open(
                            resourcePackage.DumpFileName,
                            FileMode.Open,
                            FileAccess.Write,
                            FileShare.None);

                    oFS.Write(tFileBuffer, 0, (int) tS2.Length);
                    oFS.Close();
                }
                //Now read the stream:
                //Note: the following would work -- but only for text
                //based files as it would truncate the results at the first
                //NULL.
                //StreamReader tSR = new StreamReader(resourceStream);
                //output.Write(tSR.ReadToEnd());
                //Better to use the binary reader to deal with all possibilities:
            }

            //Ready to start response:
            //1. Clear:
            hc.Response.Clear();
            //2: Mime Type:
            hc.Response.ContentType = mimeType;

            //return true;

            if (resourceStream != null)
            {
                /*
			//BinaryReader tBinaryReader;
			//byte[] tBuffer = null;
				bool noCache = 
          (
          (hc != null) && 
          (hc.Request.QueryString["NoCache"] != null)
          );

        noCache = true;

				if (!noCache) {
					hc.Response.Cache.SetExpires(DateTime.Now.AddHours(1));
					hc.Response.Cache.SetCacheability(HttpCacheability.Public);
					hc.Response.Cache.SetValidUntilExpires(false);
					hc.Response.Cache.VaryByParams["Res"] = true;
				}
				else {
					// The response is not cached
					hc.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
					hc.Response.Cache.SetCacheability(HttpCacheability.NoCache);
				}

				hc.Response.AddHeader("content-disposition", 
          string.Format("filename=\"{0}\"", resourcePackage.ResourcePath));

				tBinaryReader = new BinaryReader(resourceStream);
				tBuffer = new Byte[resourceStream.Length];
				tBinaryReader.Read(tBuffer, 0, tBuffer.Length);
				tBinaryReader.Close();
				hc.Response.BinaryWrite(tBuffer);
			}
			else {
				//3b:Cache:Clear
				hc.Response.Clear();
				//Clear cache:
				hc.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
				hc.Response.Cache.SetCacheability(HttpCacheability.NoCache);

				string errMsg = string.Empty;
				string errMsgText = "Could not find Resource specified:";
				switch (mimeType) {
					case "text/js":
					errMsg = string.Format("alert(\"{0}\n{1}\");", 
            resourcePackage.ResourcePath, errMsgText);
					break;
					case "text/vb":
					errMsg = string.Format("MsgBox(\"{0}\n{1}\")", 
            resourcePackage.ResourcePath, errMsgText);
					break;
					case "text/vbs":
					errMsg = string.Format("MsgBox(\"{0}\n{1}\")", 
            resourcePackage.ResourcePath, errMsgText);
					break;
					case "text/xml":
					errMsg = string.Format("<xml><error>{0}{1}</error></xml>", 
            resourcePackage.ResourcePath, errMsgText);
					break;
					default:
					hc.Response.ContentType = "text/html";
					hc.Response.StatusCode = 404;
					hc.Response.StatusDescription = "Response Not Found";
					hc.Response.Write(string.Format("<h3>Error</h3>{0}<br/>{1}", 
            resourcePackage.ResourcePath, 
            errMsgText));
					break;
				}//Swith:End
         */
                return false;
            } //If:End
            return false;
        }

        #endregion

        #region Static Methods

        /// <summary>
        ///   Returns the correct Mime type to be used for Response, 
        ///   based on extension of resourcePath/FileName given.
        /// </summary>
        /// <param name = "resourcePath"></param>
        /// <returns></returns>
        protected static string GetResourceMimeType(string resourcePath)
        {
            NameValueCollection mimeList = GetResourceMimeList();
            string ext = Path.GetExtension(resourcePath).ToLower();
            string result = null;
            if (ext.Length > 0)
            {
                ext = ext.Substring(1).ToLower();
                result = mimeList[ext];
            }
            if ((result == null) || (result == string.Empty))
            {
                result = "text/html";
            }
            return result;
        }


        /// <summary>
        ///   Creates an internal static list of extensions to Mime types.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Used by <see cref = "GetResourceMimeType" />.
        ///   </para>
        /// </remarks>
        /// <returns></returns>
        protected static NameValueCollection GetResourceMimeList()
        {
            NameValueCollection result = null;
            HttpContext hc = HttpContext.Current;
            string key = "__XACT_MIME_LIST";

            if (hc != null)
            {
                result = (NameValueCollection) hc.Application[key];
            }
            if (result == null)
            {
                result = new NameValueCollection();
                result.Add("gif", "image/gif");
                result.Add("jpg", "image/jpg");
                result.Add("png", "image/png");
                result.Add("bmp", "image/bmp");
                result.Add("css", "text/css");
                result.Add("js", "text/javascript");
                result.Add("xml", "text/xml");
                result.Add("xsl", "text/xsl");
                if (hc != null)
                {
                    hc.Application[key] = result;
                }
            }
            return result;
        }


        /// <summary>
        ///   Extracts a Resource Stream from the given (optional) assembly.
        /// </summary>
        /// <param name = "qOptionalAssemblyIn">Optional assembly object.</param>
        /// <param name = "givenResourcePath">PartialName of Resource.</param>
        /// <param name = "foundFullResourcePath">
        ///   Returned full Resource Path if found.
        /// </param>
        /// <returns>
        ///   Stream that contains resource, or null if not found.
        /// </returns>
        protected static Stream GetResourceStreamFromAssembly(
            object qOptionalAssemblyIn,
            string givenResourcePath,
            out string foundFullResourcePath)
        {
            //Clear Answers:
            foundFullResourcePath = string.Empty;
            Assembly resourceAssembly = null;
            Assembly executingAssembly = null;
            Stream resultStream = null;
            //Verify:
            if ((givenResourcePath == null) ||
                (givenResourcePath.Trim() == string.Empty))
            {
                return null;
            }
            //If given a string -- then try to find the assembly:
            if (qOptionalAssemblyIn is string)
            {
                resourceAssembly = GetAssemblyFromName((string) qOptionalAssemblyIn);
            }
            else if (qOptionalAssemblyIn is Assembly)
            {
                resourceAssembly = (Assembly) qOptionalAssemblyIn;
            }
            else
            {
                resourceAssembly = null;
            }


            //Look in proposed assembly (if given) first:
            if (resourceAssembly != null)
            {
                resultStream = GetResourceFromAssembly(
                    resourceAssembly,
                    givenResourcePath,
                    out foundFullResourcePath);

                if (resultStream != null)
                {
                    //Found:
                    return resultStream;
                }
            }

            //Look in current assembly next:
            executingAssembly = Assembly.GetExecutingAssembly();
            resultStream = GetResourceFromAssembly(
                executingAssembly,
                givenResourcePath,
                out foundFullResourcePath);

            if (resultStream != null)
            {
                //Found:
                return resultStream;
            }

            //If not found, look through all assemblies:
            //This is longest route, hence done last:
            //string tTest = givenResourcePath.ToLower();
            Assembly[] assemblies =
                AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly assembly in assemblies)
            {
                //Skip the ones we've already looked in:
                if (assembly == resourceAssembly)
                {
                    continue;
                }
                if (assembly == executingAssembly)
                {
                    continue;
                }
                //Skip the ones that are provided by Framework:
                if (assembly.GetName().Name.StartsWith("System"))
                {
                    continue;
                }

                //Test it:
                resultStream = GetResourceFromAssembly(
                    assembly, givenResourcePath, out foundFullResourcePath);

                if (resultStream != null)
                {
                    //Found:
                    return resultStream;
                }
            }
            return null;
        }

        /// <summary>
        ///   Extracts stream of Resource from the given assembly.
        /// </summary>
        /// <param name = "assembly">Assembly to search for Resource in.</param>
        /// <param name = "givenResourcePath">PartialName of Resource.</param>
        /// <param name = "foundFullResourcePath">returned full path of Resource.</param>
        /// <returns>Stream or null if not foudn.</returns>
        protected static Stream GetResourceFromAssembly(
            Assembly assembly,
            string givenResourcePath,
            out string foundFullResourcePath)
        {
            foundFullResourcePath = string.Empty;
            if (assembly != null)
            {
                givenResourcePath = givenResourcePath.ToLower();
                foreach (string resourcePath in assembly.GetManifestResourceNames())
                {
                    if (resourcePath.ToLower().EndsWith(givenResourcePath))
                    {
                        foundFullResourcePath = resourcePath;
                        return assembly.GetManifestResourceStream(resourcePath);
                    }
                }
            }
            return null;
        }

        //Method:End


        /// <summary>
        ///   Gets a currently loaded Assembly by the given short name.
        /// </summary>
        /// <param name = "assemblyName">Name of the assembly.</param>
        /// <returns></returns>
        protected static Assembly GetAssemblyFromName(string assemblyName)
        {
            if ((assemblyName == null) || (assemblyName == string.Empty))
            {
                throw new ArgumentException("assemblyName");
            }

            Assembly[] assemblies =
                AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly assembly in assemblies)
            {
                if (assembly.GetName().Name == assemblyName)
                {
                    return assembly;
                }
            }
            return null;
        }


        /// <summary>
        ///   Get's the last part of the name after the last "."
        ///   <para>
        ///     Given '<c>XAct.UI.Web.Controls.SomeControl</c>', 
        ///     returns '<c>SomeControl</c>'.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   The code is probably similar to 
        ///   <see cref = "Path.GetExtension" />.
        /// </remarks>
        /// <param name = "className">Name of the class.</param>
        /// <returns>Just the classname, after stripping the namespace.</returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if className is null.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Invoked by <see cref = "CalcDumpFileName" />.
        ///   </para>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        public static string GetNamesLastPart(string className)
        {
            className.ValidateIsNotDefault("className");

            //Get the last ".", and add one position:
            int pos = className.IndexOf(',');
            if (pos > -1)
            {
                //remove FQN stuff:
                className = className.Substring(0, pos).Trim();
            }

            //Get the last ".", and add one position:
            pos = className.LastIndexOf('.');
            if (pos == -1)
            {
                //No dot...we want the whole thing:
                return className;
            }
            //Move past the last "."
            pos += 1;
            //Get the length of what's there.
            int len = className.Length - pos;
            return className.Substring(pos, len);
        }

        /// <summary>
        ///   Calculate an appropriate filename for the
        ///   resource to be dumped to the hard drive.
        /// </summary>
        /// <param name = "resourcePackage">The resource package.</param>
        /// <returns></returns>
        protected static string CalcDumpFileName(ResourcePackage resourcePackage)
        {
            resourcePackage.ValidateIsNotDefault("resourcePackage");

            HttpContext hc = HttpContext.Current;
            string result = string.Empty;


            //Calc the name where the file would be on the hard drive...
            if ((resourcePackage.DumpFileName == null) ||
                (resourcePackage.DumpFileName == string.Empty))
            {
                string baseDir = DumpBaseDir;
                string subDir = string.Empty;
                string ext = Path.GetExtension(resourcePackage.ResourcePath).ToLower();

                //Get short name of class that contains embedded resource:
                //Note that this gives full namespace + className
                string className = resourcePackage.ClassType.ToString();

                //Try to extract the class's name only (we don't want the whole
                //namespace et al):
                string controlNameShort = GetNamesLastPart(className);

                //For ME only...
                //So that we are using the same Resources dir structure...

                //We Split our resources into 3 subdirs depending on extension:
                if (ext == ".js")
                {
                    subDir = "Scripts";
                }
                else if (ext == ".gif")
                {
                    subDir = "Graphics";
                }
                else if (ext == ".css")
                {
                    subDir = "Css";
                }

                //Build output filename:
                result = Path.Combine(controlNameShort, subDir);
                result = Path.Combine(result, resourcePackage.ResourcePath);
                result = Path.Combine("Resources", result);
                result = Path.Combine(baseDir, result);
                result = (hc != null)
                             ? hc.Server.MapPath(resourcePackage.DumpFileName)
                             : result;

                //TODO:resourcePackage.DumpFileName = result;
            }
            return result;
        }

        #endregion

        /// <summary>
        ///   Makes a client-ready url for the requested resource.
        /// </summary>
        /// <param name = "control">The control.</param>
        /// <param name = "resourceNameOrVirtualFilePath">The resource name or virtual file path.</param>
        /// <returns></returns>
        public static string MakeResourceUrl(Control control,
                                             string resourceNameOrVirtualFilePath)
        {
            control.ValidateIsNotDefault("control");

            resourceNameOrVirtualFilePath.ValidateIsNotNullOrEmpty("resourceNameOrVirtualFilePath");

            string result = string.Empty;
            Type controlType = control.GetType();

            //Get the context:
            HttpContext hc = HttpContext.Current;
            if (hc != null)
            {
                //We're in a web app...
                //string filePath =
                //    hc.Server.MapPath(resourceNameOrVirtualFilePath);
            }
            else
            {
                //We're not running...ie...probably in VS IDE:
                IWebApplication vWA =
                    (IWebApplication) control.Site.GetService(typeof (IWebApplication));

                if (vWA != null)
                {
                    //vWA.GetProjectItemFromUrl(
                    //fAppRootFolder = 
                    //   vWA.RootProjectItem.PhysicalPath;  //C:\inetpub\wwwroot\appfolder\
                    //fUrlToWebApp = 
                    //    vWA.RootProjectItem.Name;  // http://localhost/appfolder/
                }

                //string filePath = hc.Server.MapPath(resourceNameOrVirtualFilePath);

                if (File.Exists(resourceNameOrVirtualFilePath))
                {
                    return resourceNameOrVirtualFilePath;
                }

                result = control.Page.ClientScript.GetWebResourceUrl(
                    controlType,
                    resourceNameOrVirtualFilePath);
            }
            return result;
        }

        #region Nested type: ResourcePackage

        /// <summary>
        ///   TODO
        /// </summary>
        [
            Serializable
        ]
        public class ResourcePackage : ISerializable
        {
            #region Properties

            private readonly Assembly _Assembly;


            private readonly Type _ClassType;
            private readonly string _DumpFileName;
            private readonly bool _DumpOk;

            private readonly string _ResourcePath;

            /// <summary>
            ///   The Assembly within which the Resource stream
            ///   can be found.
            /// </summary>
            /// <value>The assembly.</value>
            public Assembly Assembly
            {
                get { return _Assembly; }
            }

            /// <summary>
            ///   Gets the type of the class that 'owns' this 
            ///   Resource.
            /// </summary>
            /// <value>The type of the class.</value>
            public Type ClassType
            {
                get { return _ClassType; }
            }

            /// <summary>
            ///   Gets the resource path.
            /// </summary>
            /// <value>The resource path.</value>
            public string ResourcePath
            {
                get { return _ResourcePath; }
            }

            /// <summary>
            ///   Gets the System FileName for the Resource.
            /// </summary>
            /// <value>The name of the dump file.</value>
            public string DumpFileName
            {
                get { return _DumpFileName; }
            }

            /// <summary>
            ///   Gets a value indicating whether its ok to dump
            ///   the file to hard-drive for further processing by end user.
            /// </summary>
            /// <value><c>true</c> if [dump ok]; otherwise, <c>false</c>.</value>
            public bool DumpOk
            {
                get { return _DumpOk; }
            }

            /// <summary>
            ///   UrlParameters used by MakeResourceUrl
            /// </summary>
            /// <remarks>
            ///   <para>
            ///     RoundTripped only when in 
            ///     DesignMode and <see cref = "RUID" /> won't work.
            ///   </para>
            /// </remarks>
            public string UrlParameters
            {
                get
                {
                    return
                        "a=" + Assembly.GetName().Name +
                        "&t=" + ClassType +
                        "&r=" + ResourcePath;
                }
            }

            /// <summary>
            ///   Unique UID calculated from Hashcode of UrlParameters.
            /// </summary>
            /// <remarks>
            ///   <para>
            ///     See <see cref = "UrlParameters" />.
            ///   </para>
            ///   <para>
            ///     This UID can only be used when not in DesignMode.
            ///     When in DesignMode, see <see cref = "UrlParameters" />.
            ///   </para>
            /// </remarks>
            public int RUID
            {
                get { return UrlParameters.ToLower().GetHashCode(); }
            }

            #endregion

            /// <summary>
            ///   Constructor.
            /// </summary>
            /// <remarks>
            ///   <para>
            ///     Constructor called by one of the 
            ///     <c>Extract</c> 
            ///     method implementations.
            ///   </para>
            /// </remarks>
            /// <param name = "assemblyName"></param>
            /// <param name = "classTypeName"></param>
            /// <param name = "resourcePath"></param>
            public ResourcePackage(
                string assemblyName,
                string classTypeName,
                string resourcePath)
            {
                _Assembly = GetAssemblyFromName(assemblyName);

                _ClassType = _Assembly.GetType(classTypeName);
                _ResourcePath = resourcePath;
                _DumpFileName = string.Empty;
                _DumpOk = false;
            }

            #region Implementation of ISerializable

            /// <summary>
            ///   Constructor required for Deserialization 
            ///   of data created by ISerializable.GetObjectData
            /// </summary>
            /// <internal>
            ///   See: http://www.15seconds.com/issue/020903.htm
            /// </internal>
            /// <param name = "info"></param>
            /// <param name = "context"></param>
            public ResourcePackage(
                SerializationInfo info,
                StreamingContext context)
            {
                _Assembly = GetAssemblyFromName(info.GetString("assemblyName"));
                _ClassType = Assembly.GetType(info.GetString("classTypeName"));
                _ResourcePath = info.GetString("resourcePath");
                _DumpFileName = info.GetString("dumpFileName");
                _DumpOk = info.GetBoolean("dumpOk");
            }

            /// <summary>
            ///   Implementation of ISerializable
            /// </summary>
            /// <internal>
            ///   See: http://www.15seconds.com/issue/020903.htm
            /// </internal>
            /// <param name = "info"></param>
            /// <param name = "context"></param>
            public void GetObjectData(
                SerializationInfo info,
                StreamingContext context)
            {
                //use the info object to add the items you want serialized
                info.AddValue("assemblyName", Assembly.GetName().Name);
                info.AddValue("classTypeName", ClassType.ToString());
                info.AddValue("resourcePath", ResourcePath);
                info.AddValue("dumpFileName", DumpFileName);
                info.AddValue("dumpOk", _DumpOk);
            }

            #endregion

            #region Static Methods

            /// <summary>
            ///   Gets the Hashtable that stores the ruid->ResourcePackage relations.
            /// </summary>
            /// <remarks>
            ///   <para>
            ///     The returned Hashtable is used by
            ///     <c>MakeResourceUrl</c> and <c>Extract</c>.
            ///   </para>
            /// </remarks>
            public static Hashtable ResourcePackageStore
            {
                get
                {
                    string key = "__XACT_RESOURCEPACKAGESTORE";
                    HttpContext hc =
                        HttpContext.Current;

                    Hashtable resourcePackageStore =
                        (CacheLocation == "a")
                            ? (Hashtable) hc.Application[key]
                            : (Hashtable) hc.Session[key];

                    if (resourcePackageStore == null)
                    {
                        if (CacheLocation == "a")
                        {
                            hc.Application[key] = new Hashtable();
                        }
                        else
                        {
                            hc.Session[key] = new Hashtable();
                        }
                    }
                    return resourcePackageStore;
                }
            }

            /// <summary>
            ///   Returns a new ResourcePackage based on the info given.
            /// </summary>
            /// <param name = "assemblyName"></param>
            /// <param name = "classTypeName"></param>
            /// <param name = "resourcePath"></param>
            /// <returns></returns>
            public static ResourcePackage Extract(
                string assemblyName,
                string classTypeName,
                string resourcePath)
            {
                return new ResourcePackage(assemblyName, classTypeName, resourcePath);
            }

            /// <summary>
            ///   Extract from the Session or Application stage bag 
            ///   the ResourcePackage pointed to by the given 'ruid'.
            /// </summary>
            /// <remarks>
            ///   <para>
            ///     The default StateBag to use is the one 
            ///     defined by <see cref = "CacheLocation" />, but
            ///     it can be overriden by an 'm' parameter.
            ///   </para>
            ///   <para>
            ///     Note:
            ///     The <see cref = "ResourcePackage" /> was saved 
            ///     in the Application/Session 
            ///     state bag by the <c>MakeResourceUrl</c> method.
            ///   </para>
            /// </remarks>
            /// <exception cref = "ArgumentException">
            ///   if ruid is an invalid number.
            /// </exception>
            /// <param name = "resourceUniqueId"></param>
            /// <returns></returns>
            public static ResourcePackage Extract(int resourceUniqueId)
            {
                HttpContext hc = HttpContext.Current;

                if (hc == null)
                {
                    throw new Exception(
                        "Cannot Extract() when HttpContext is null/DesignTime.");
                }

                Hashtable resourcePackageStore = ResourcePackageStore;
                ResourcePackage resourcePackage =
                    (ResourcePackage) resourcePackageStore[resourceUniqueId];

                if (resourcePackage == null)
                {
                    throw new ArgumentException(
                        string.Format("resourceUniqueId '{0}' was not a valid Resource ID.",
                                      resourceUniqueId));
                }

                return resourcePackage;
            }

            #endregion
        }

        #endregion


    }


}


