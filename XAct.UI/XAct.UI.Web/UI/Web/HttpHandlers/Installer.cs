namespace XAct.UI.Web.HttpHandlers
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Web;
    using System.Xml;

    /// <summary>
    ///   Class to simplify the registration of IHttpHandlers into web.config.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Used by all of the IHttpHandlers that we offer, this unifies the process 
    ///     of opening the web.config file, checking to see if the requested handler is registered,
    ///     and if not, attempting to install it. Of course, if the ASP.NET account doesn't have enough
    ///     rights to modify the file, the process fails -- but atleast warns the webadmin what to do, 
    ///     either by hand, or change the ASP.NET rights to the web.config directory. 
    ///   </para>
    /// </remarks>
    public class Installer
    {
        ///<summary>
        ///  Modifies web.config file of a virtual directory/app, in order to install
        ///  a custom IHttpHandler, such as XAct.UI.Web.Servers.ResourceHandler, ImageHandler, etc...
        ///</summary>
        ///<param name = "tType">The Type of the Handler.</param>
        ///<param name = "handlerUrl">The path filter to register into web.config (eg: '*Resources.aspx')</param>
        ///<param name = "qThrowExceptionIfUnsuccessful">If it cannot modify the web.config (probably due to file permissions) throw an error or let slide?</param>
        ///<returns>Boolean. True if config.xml had to be updated.</returns>
        ///<remarks>
        ///  Since the function that calls it is generally a static function, and therefore this.GetType()
        ///  won't work there, you can use something like to get a handle on the name of the class
        ///  that you want to install:
        ///  <code>
        ///    public static bool Install(bool qThrowExceptionIfUnsuccessful){
        ///    System.Type handlerType = MethodBase.GetCurrentMethod().DeclaringType;
        ///    return Common.InstallHandler(handlerType,_HandlerUrl);
        ///  </code>
        ///</remarks>
        public static bool InstallHandler(Type tType, string handlerUrl, bool qThrowExceptionIfUnsuccessful)
        {
            //Because parsing/checking the file takes time, we want to get out
            //as soon as possible, if we know the work has been done.
            //For this check we need a unique key, and use the global Assembly 
            //array (same for all users, unlike Session, or ViewState).

            if (HttpContext.Current == null)
            {
                return false;
            }

            //Make unique key from Type Name:
            string tKey = "XACT.HANDLER.INSTALLED." + tType;
            if (HttpContext.Current.Application[tKey] != null)
            {
                //Already checked, AND installed.
                //Get out now.
                return true;
            }

            //Ok. As far as we know, this handler has not been installed or even
            //verified during this session.
            //We need to open and parse the config.xml file and look for a sectiong that
            //looks like:
            //<system.web>
            //	<httpHandlers> 
            //		<add verb="*" path="{handlerUrl}" type="{qType},{qAssembly}" />
            //	</httpHandlers>
            //</system.web>


            XmlDocument cfgDoc = new XmlDocument();
            XmlNode tNodeSettings = null;
            XmlNode tNodeSection = null;
            XmlNode tNodeEntry = null;

            //Names of the Nodes we need to nav through:
            const string tSettingsTag = "system.web";
            const string tSectionTag = "httpHandlers";
            const string tEntryTag = "add";

            //But before we do, note that we have the 'filter', the 'type', but we
            //don't have the 'assembly':
            string tTypeName = tType.ToString();
            string tAssemblyName = tType.Assembly.GetName().Name;

            bool tInstallThisTime = false;

            string[] tTypeAttributeParts = null;
            string tTypeAttribute_Name = string.Empty;
            string tTypeAttribute_Assembly = string.Empty;
            Assembly tCheckAssembly;
            Type tCheckType;


            bool _RunTimeMode = false;
            string tConfigFileName = string.Empty;
            bool tChangesMade = false;

            XmlAttribute tA = null;


            //Get the full path to the web.config: 
            _RunTimeMode = (HttpContext.Current != null);
            tConfigFileName = _RunTimeMode ? HttpContext.Current.Server.MapPath("~/web.config") : "web.config";
            if (!File.Exists(tConfigFileName)) //OK to use File as never going to be anything but.
            {
                throw new Exception(
                    "Am trying to modify the config.web file to install a required IHttpHandler - but the file could not be found.");
            }
            //This file may or may not have been already taken care of.
            //So we need to keep a marker to see if we need to update the file
            //at the end:
            tChangesMade = false;
            //Load the file and parse:
            cfgDoc.Load(tConfigFileName);

            //Get the basic Settings Node ("system.web"):
            tNodeSettings = cfgDoc.SelectSingleNode("//" + tSettingsTag);
            //Unable to find root node. Something wrong:
            if (tNodeSettings == null)
            {
                return false;
            }
            //Find the next base node ("httpHandlers"):
            tNodeSection = tNodeSettings.SelectSingleNode(tSectionTag);
            //If not found, create it:
            if (tNodeSection == null)
            {
                tNodeSection = cfgDoc.CreateElement(tSectionTag);
                tNodeSettings.AppendChild(tNodeSection);
                tChangesMade = true;
            }
            //Find the actual entry node ("add")
            tNodeEntry = tNodeSection.SelectSingleNode(string.Format("{0}[@path=\"{1}\"]", tEntryTag, handlerUrl));

            tA = null;

            tInstallThisTime = false;


            //------------------------------------------------
            if (tNodeEntry == null)
            {
                tInstallThisTime = true;
            }
            else
            {
                //There is already a node...

                //We start off assuming its good:
                tInstallThisTime = false;

                //Note that this Installer can be used for installing any IHttpHandler -- but
                //the most common case will be for installing the core/common Handlers.
                //What can go wrong with an installer that is available in any control?
                //That it was installed by eg: XAct.UI.Web.Controls.Button -- and later that
                //control will be removed if not used -- so it's better to try to find 
                //a more stable one.
                //Two places to look for a more stable one is the webapp itself as Common could
                //easily be there-- and it would be the most common:
                //Second place to look is actually in the XAct.UI.Web.Controls.Common.dll -- which
                //if we ever get it to work with other controls is the source itself...

                //a) The syntax is incorrect (wrong: the app would have flagged and complained).
                //b) since then, the control that installed this Handler has been removed, and needs to be re-installed, but pointing to this Handler instead. (wrong, again app would have complained at startup)
                //c) app is pointing to a sub-control...

                tA = tNodeEntry.Attributes["type"];
                if (tA == null)
                {
                    //If the type is missing then this is an incorrectly installed node:
                    //Fix:
                    tInstallThisTime = true;
                }
                else
                {
                    //Found Assembly -- get the type attribute
                    //which contains 'Type,Assembly' info:

                    tTypeAttributeParts = tA.Value.Split(',');
                    tTypeAttribute_Name = tTypeAttributeParts[0].Trim();
                    tTypeAttribute_Assembly = tTypeAttributeParts[1].Trim();


                    //We start off assuming we won't have to install this time:
                    tInstallThisTime = false;

                    //But is the Type right?
                    if (tTypeAttribute_Name.ToLower() != tTypeName.ToLower())
                    {
                        //The Path is pointing to another IHttpHandler. 
                        //Fix it:
                        tInstallThisTime = true;
                    }
                    else
                    {
                        //Atleast we are on the same page about which handler the path is going
                        //to...
                        //The question remains about whether the Assembly used is the best option.
                        //The best option is using the AppRoot if it has the same Handler...

                        //Get hanlde on base assembly (the webapp):
                        string tWow = AppDomain.CurrentDomain.RelativeSearchPath;
                        string tWow2 = AppDomain.CurrentDomain.BaseDirectory;

                        tCheckAssembly = Assembly.GetEntryAssembly();
                        tCheckType = (tCheckAssembly != null) ? tCheckAssembly.GetType(tTypeName, false) : null;
                        if (tCheckType != null)
                        {
                            //Yes -- the app has a copy of the handler...
                            //Is that the one we are using?
                            if (tTypeAttribute_Assembly.ToLower() != tCheckAssembly.GetName().Name.ToLower())
                            {
                                //No...I think we should update it then:
                                tAssemblyName = tCheckAssembly.GetName().Name;
                                tInstallThisTime = true;
                            }
                            else
                            {
                                //All is fine. We found the type, the assembly, AND it was the WebApp itself.
                            }
                        }
                        else
                        {
                            //The Handler could not be found in the app itself.
                            //Therefore it can only be in one of two places:
                            //Common.dll -- or the calling Control...
//							tCheckAssembly = 
//                Assembly.LoadWithPartialName(
//                "XAct.UI.Web.Controls.Common"
//                );

                            tCheckAssembly =
                                Assembly.Load(
                                    new AssemblyName(
                                        "XAct.UI.Web.Controls.Common"
                                        ));

                            if (tCheckAssembly != null)
                            {
                                //Yep. Found the Common dll -- but does it have this Handler in it?
                                tCheckType = tCheckAssembly.GetType(tTypeName, false);
                                if (tCheckType != null)
                                {
                                    //Yes it does! Appears that we have a common handler.
                                    //Use this one instead.
                                    if (tTypeAttribute_Assembly.ToLower() != tCheckAssembly.GetName().Name.ToLower())
                                    {
                                        //No...I think we should update it then:
                                        tAssemblyName = tCheckAssembly.GetName().Name;
                                        tInstallThisTime = true;
                                    }
                                    else
                                    {
                                        //All is fine. We found the type, the assembly. 
                                    }
                                }
                            }
                            else
                            {
                                //Final check.
                                //We are not in the base asssembly, or the Common dll...
                                //just little ol me...
                                tCheckAssembly = tType.Assembly;
                                if (tTypeAttribute_Assembly.ToLower() != tCheckAssembly.GetName().Name.ToLower())
                                {
                                    //No...I think we should update it then:
                                    tAssemblyName = tCheckAssembly.GetName().Name;
                                    tInstallThisTime = true;
                                }
                                else
                                {
                                    //Mathces up to the right dll... Leave alone.
                                }
                            }
                        }
                    }
                }
            }
            //------------------------------------------------

            /*
			if (tNodeEntry == null){
				tInstallThisTime = true;
			}
			else{
				//There is already a node...
				
				//We start off assuming its good:
				tInstallThisTime = false;

				//But it's possible that 
				//a) since then, the control that installed this Handler has been removed, and needs to be re-installed, but pointing to this Handler instead. 

				tA = null;
				try {
					tA = tNodeEntry.Attributes["type"];
				}catch {
					bool tCheck = false;
				}
				if (tA == null){
					tInstallThisTime=true;
					//Not good.
				}else{
					tInstallThisTime=false;
					string[] tParts = tA.Value.Split(',');
					if (tParts[1].Trim().ToLower() != "XAct.UI.Web.Controls.Common".ToLower()){
						//Then we are not using the base one -- but an enherited one...
						//so the million dollar question is -- is there an assembly that IS
						//The common one now?
                        Assembly oLastAssembly = Assembly.Load(tParts[1].Trim());
                        if (oLastAssembly == null){
                            tInstallThisTime = true;
                        }
                    }
                }
            }
            */

            //If not found, create according to IDs:
            if (tInstallThisTime)
            {
                //If there was one, remove old one first:
                if (tNodeEntry != null)
                {
                    tNodeSection.RemoveChild(tNodeEntry);
                }


                //Make the node:
                tNodeEntry = cfgDoc.CreateElement(tEntryTag);
                tNodeSection.AppendChild(tNodeEntry);

                //Make the various attributes:
                tA = cfgDoc.CreateAttribute("verb");
                tA.Value = "*";
                tNodeEntry.Attributes.Append(tA);

                tA = cfgDoc.CreateAttribute("path");
                tA.Value = handlerUrl;
                tNodeEntry.Attributes.Append(tA);

                tA = cfgDoc.CreateAttribute("type");
                tA.Value = tType + "," + tAssemblyName;
                tNodeEntry.Attributes.Append(tA);

                tChangesMade = true;
            }

            //Done. If we have made any changes, we have to save them:
            if (tChangesMade)
            {
                try
                {
                    //We may not have rights enough to do this...
                    cfgDoc.Save(tConfigFileName);
                }
                catch (Exception E)
                {
                    //If the app doesn't have enough rights, warn
                    //the admin/user to update the file by hand:
                    Exception E2 =
                        new Exception(
                            "IMPORTANT: Need to update the web.config file to allow for resource (gif, js, etc) handling -- but access denied." +
                            System.Environment.NewLine +
                            "Please add the following by hand:" + System.Environment.NewLine +
                            tNodeSection.OuterXml.Replace(">", ">" + System.Environment.NewLine),
                            E.InnerException
                            );

                    throw (E2);
                }
            }
            //If we got here, we are done:
            //Update the flag so we don't do this over and over again, for each
            //request....
            HttpContext.Current.Application[tKey] = true;
            //return true:
            return true;
        }

//Method:End
    }


}


