﻿namespace XAct.Environment
{
    using System;
    using System.Web;

    /// <summary>
    /// Contract for an HTTP specific implementation
    /// of <see cref="IClientEnvironmentService"/>
    /// </summary>
    public interface IHttpClientPersistenceService : IClientPersistenceService, IHasXActLibService
    {

        /// <summary>
        /// Gets the cookie with the given name (null if none found).
        /// </summary>
        /// <param name="cookieName">Name of the cookie.</param>
        /// <returns></returns>
        HttpCookie GetCookie(string cookieName);

        /// <summary>
        /// Gets or Creates the specified cookie.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="value">The value.</param>
        /// <param name="key">The key.</param>
        /// <param name="duration">The duration.</param>
        /// <returns></returns>
        HttpCookie GetOrCreateRegisteredCookie(string containerName, string value, TimeSpan duration = default(TimeSpan),
                                               string key = null);
    }
}