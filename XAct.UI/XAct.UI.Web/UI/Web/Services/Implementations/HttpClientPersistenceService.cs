﻿namespace XAct.Environment.Services.Implementations
{
    using System;
    using System.Web;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// TODOC
    /// </summary>
    [DefaultBindingImplementation(typeof(IClientPersistenceService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class HttpClientPersistenceService : XActLibServiceBase, IHttpClientPersistenceService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpClientPersistenceService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="environmentService">The environment service.</param>
        public HttpClientPersistenceService(ITracingService tracingService, IDateTimeService dateTimeService, IEnvironmentService environmentService):base(tracingService)
        {
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
        }

        /// <summary>
        /// Gets the value from the specified clientside persistence Container.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetValue(string containerName, string key)
        {
            HttpCookie cookie = GetCookie(containerName);

            if (cookie == null)
            {
                return null;
            }

            if (key == null)
            {
                return cookie.Value;
            }
            
            //Safe (does not throw exception if key does not exist)
            return cookie.Values[key];
        }

        /// <summary>
        /// Sets the persisted value in the specified client side persistence container.
        /// </summary>
        /// <param name="containerName">Required name of the container.</param>
        /// <param name="value">The value.</param>
        /// <param name="duration">The duration.</param>
        /// <param name="key">The optional key. If none provided, uses technology specific strategy for default key.</param>
        public void SetValue(string containerName, string value, TimeSpan duration=default(TimeSpan), string key = null)
        {
            HttpCookie cookie = GetOrCreateRegisteredCookie(containerName,value, duration,key);

            
            if (key == null)
            {
                cookie.Value = value;
            }
            else
            {
                cookie.Values.Add(key, value);
            }

            if (duration != default(TimeSpan))
            {
                cookie.Expires = _dateTimeService.NowUTC.Add(duration);
            }
        }


        /// <summary>
        /// Clears the Clientside persistence Container,
        /// and all its contained variables.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <returns></returns>
        public void ClearContainer(string containerName)
        {
            HttpCookie cookie = GetCookie(containerName);

            if (cookie != null)
            {
                cookie.Expires = DateTime.Now;
            }
        }


        /// <summary>
        /// Gets the cookie with the given name (null if none found).
        /// </summary>
        /// <param name="cookieName">Name of the cookie.</param>
        /// <returns></returns>
        public  HttpCookie GetCookie(string cookieName)
        {
            return HttpContext.Current.Request.Cookies[cookieName];
        }

        /// <summary>
        /// Gets or Creates the specified cookie.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <param name="value">The value.</param>
        /// <param name="duration">The duration.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public  HttpCookie GetOrCreateRegisteredCookie(string containerName, string value, TimeSpan duration = default(TimeSpan), string key = null)
        {
            if (containerName.IsNullOrEmpty())
            {
                containerName = "UserSettings";
            }

            HttpCookie httpCookie =
                           HttpContext.Current.Request.Cookies[containerName];
            if (httpCookie == null)
            {
                httpCookie = new HttpCookie(containerName);
                HttpContext.Current.Request.Cookies.Add(httpCookie);
            }



            if (key == null)
            {
                httpCookie.Value = value;
            }
            else
            {
                httpCookie[key] = value;
            }

            if (duration != default(TimeSpan))
            {
                httpCookie.Expires = _dateTimeService.NowUTC.Add(duration);
            }

            return httpCookie;
        }

    }
}