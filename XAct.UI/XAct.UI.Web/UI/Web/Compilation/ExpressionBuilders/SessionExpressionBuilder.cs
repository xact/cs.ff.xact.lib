namespace XAct.UI.Web.Compilation
{
    using System.Web;
    using System.Web.Compilation;
    using XAct.Diagnostics;

    /// <summary>
    /// An ASP.NET ExpressionBuilder that returns Session values.
    /// </summary>
    /// <remarks>
    /// 	<para>
    /// Configuration:
    /// </para>
    /// 	<para>
    /// 		<code>
    /// 			<![CDATA[
    /// <system.web>
    /// <compilation>
    /// <expressionBuilders>
    /// <add expressionPrefix="Session"
    /// type="XAct.UI.Web.Compilation.SessionExpressionBuilder,
    /// XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    /// </expressionBuilders>
    /// </compilation>
    /// ...
    /// </system.web>
    /// ]]>
    /// 		</code>
    /// 	</para>
    /// 	<para>
    /// 	</para>
    /// Usage:
    /// <para>
    /// 		<code>
    /// 			<![CDATA[
    /// <asp:Label runat="server" ID="WelcomeMessage"
    /// Text="<%$ QueryString:Session %>" />
    /// ]]>
    /// 		</code>
    /// 	</para>
    /// 	<para>
    /// That said, think twice -- hell think 10 times --
    /// before you use Session variables for anything.
    /// </para>
    /// </remarks>
    /// <internal>
    /// Src: http://bit.ly/idCsDv
    /// </internal>
    [ExpressionPrefix("Session"),
        //ExpressionEditor("XAct.UI.Web.Compilation.Design.SessionExpressionEditor, XAct.UI.Web")
    ]
    public class SessionExpressionBuilder : BaseServerObjectExpressionBuilder
    {
        #region Services

        private static readonly ITracingService _tracingService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionExpressionBuilder"/> class.
        /// </summary>
        static SessionExpressionBuilder()
        {
            //As the service not only cannot be constructed, 
            //but also has to be static, we need to hard code a logger.
            //As we have a ref to XAct.Core, we can do the following.
            _tracingService = DependencyResolver.Current.GetInstance<ITracingService>();
        }

        #endregion

        #region Static methods

        /// <summary>
        /// A Factory method that returns a new instance of this
        /// <see cref="SessionExpressionBuilder"/>
        /// </summary>
        /// <returns>A instance of <see cref="SessionExpressionBuilder"/></returns>
        public static SessionExpressionBuilder Instance()
        {
            return new SessionExpressionBuilder();
        }

        #endregion

        #region Implementation of BaseServerObjectExpressionBuilder

        /// <summary>
        /// Returns the name of this
        /// <see cref="BaseServerObjectExpressionBuilder"/>
        /// subclass.
        /// </summary>
        /// <value></value>
        public override string SourceObjectName
        {
            get { return "Session"; }
        }


        /// <summary>
        /// Abstract property
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected override object GetValue(string key)
        {
            return HttpContext.Current.Session[key];
        }

        #endregion
    }
}