namespace XAct.UI.Web.Compilation
{
    using System.Web;
    using System.Web.Compilation;
    using XAct.Diagnostics;

    /// <summary>
    /// An ASP.NET ExpressionBuilder that returns Server values.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Configuration:
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <system.web>
    ///   <compilation>
    ///     <expressionBuilders>
    ///       <add expressionPrefix="ServerVariable"
    ///            type="XAct.UI.Web.Compilation.ServerVariableExpressionBuilder, 
    ///                  XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///     </expressionBuilders>
    ///   </compilation>
    ///   ...
    /// </system.web>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// </para>
    /// Usage:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <asp:Label runat="server" Text="<%$ ServerVariable:REMOTE_ADDR%>" />
    /// <asp:Label runat="server" Text="<%$ ServerVariable:REMOTE_USER%>" />
    /// <asp:Label runat="server" Text="<%$ ServerVariable:LOGON_USER%>" />
    /// <asp:Label runat="server" Text="<%$ ServerVariable:REQUEST_METHOD%>" />
    /// <asp:Label runat="server" Text="<%$ ServerVariable:SERVER_SOFTWARE%>" />
    /// <asp:Label runat="server" Text="<%$ ServerVariable:URL%>" />
    /// <asp:Label runat="server" Text="<%$ ServerVariable:HTTP_USER_AGENT%>" />
    /// <asp:Label runat="server" Text="<%$ ServerVariable:PATH_TRANSLATED%>" />
    /// <asp:Label runat="server" Text="<%$ ServerVariable:APPL_PHYSICAL_PATH%>" />
    /// ]]>
    /// </code>
    /// <para>
    /// For a full list of what variables are available: http://bit.ly/fOJiaH
    /// </para>
    /// </para>
    /// </remarks>
    /// <internal>
    /// Src: http://bit.ly/idCsDv
    /// </internal>
    [ExpressionPrefix("ServerVariable"),
     ExpressionEditor("XAct.UI.Web.Compilation.Design.ServerVariableExpressionEditor, XAct.UI.Web")]
    public class ServerVariableExpressionBuilder : BaseServerObjectExpressionBuilder
    {
        #region Services

        private static readonly ITracingService _tracingService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerVariableExpressionBuilder"/> class.
        /// </summary>
        static ServerVariableExpressionBuilder()
        {
            //As the service not only cannot be constructed, 
            //but also has to be static, we need to hard code a logger.
            //As we have a ref to XAct.Core, we can do the following.
            _tracingService = DependencyResolver.Current.GetInstance<ITracingService>();
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// A Factory method that returns a new instance of this
        /// <see cref="ServerVariableExpressionBuilder"/>
        /// </summary>
        /// <returns>A instance of <see cref="ServerVariableExpressionBuilder"/></returns>
        public static ServerVariableExpressionBuilder Instance()
        {
            return new ServerVariableExpressionBuilder();
        }

        #endregion

        #region Implementation of BaseServerObjectExpressionBuilder

        /// <summary>
        /// Returns the name of this
        /// <see cref="BaseServerObjectExpressionBuilder"/>
        /// subclass.
        /// </summary>
        /// <value></value>
        public override string SourceObjectName
        {
            get { return "ServerVariable"; }
        }

        /// <summary>
        /// Abstract property
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected override object GetValue(string key)
        {
            return HttpContext.Current.Request.ServerVariables[key];
        }

        #endregion
    }
}