namespace XAct.UI.Web.Compilation
{
    using System.Web;
    using System.Web.Compilation;
    using XAct.Diagnostics;

    /// <summary>
    /// An ASP.NET ExpressionBuilder that returns Form values.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Configuration:
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <system.web>
    ///   <compilation>
    ///     <expressionBuilders>
    ///       <add expressionPrefix="Form"
    ///            type="XAct.UI.Web.Compilation.FormExpressionBuilder, 
    ///                  XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///     </expressionBuilders>
    ///   </compilation>
    ///   ...
    /// </system.web>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// </para>
    /// Usage:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <asp:Label runat="server" ID="WelcomeMessage" 
    ///            Text="<%$ Form:Message %>" />
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal>
    /// Src: http://bit.ly/idCsDv
    /// </internal>
    [ExpressionPrefix("Form"), ExpressionEditor("XAct.UI.Web.Compilation.Design.FormExpressionEditor, XAct.UI.Web")]
    public class FormExpressionBuilder : BaseServerObjectExpressionBuilder
    {
        #region Services

        private static readonly ITracingService _tracingService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FormExpressionBuilder"/> class.
        /// </summary>
        static FormExpressionBuilder()
        {
            //As the service not only cannot be constructed, 
            //but also has to be static, we need to hard code a logger.
            //As we have a ref to XAct.Core, we can do the following.
            _tracingService = DependencyResolver.Current.GetInstance<ITracingService>();
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// A Factory method that returns a new instance of this
        /// <see cref="FormExpressionBuilder"/>
        /// </summary>
        /// <returns>A instance of <see cref="FormExpressionBuilder"/></returns>
        public static FormExpressionBuilder Instance()
        {
            return new FormExpressionBuilder();
        }

        #endregion

        #region Implementation of BaseServerObjectExpressionBuilder

        /// <summary>
        /// Returns the name of this
        /// <see cref="BaseServerObjectExpressionBuilder"/>
        /// subclass.
        /// </summary>
        /// <value></value>
        public override string SourceObjectName
        {
            get { return "Form"; }
        }

        /// <summary>
        /// Abstract property
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected override object GetValue(string key)
        {
            return HttpContext.Current.Request.Form[key];
        }

        #endregion
    }
}