namespace XAct.UI.Web.Compilation
{
    using System;
    using System.CodeDom;
    using System.ComponentModel;
    using System.Web;
    using System.Web.Compilation;
    using System.Web.UI;

    /// <summary>
    /// An abstract base ExpressionBuilder
    /// </summary>
    /// <internal>
    /// Src: http://bit.ly/idCsDv
    /// </internal>
    public abstract class BaseServerObjectExpressionBuilder : ExpressionBuilder
    {
        #region Nested type: Resources

        /// <summary>
        /// Class of strings used by 
        /// <see cref="BaseServerObjectExpressionBuilder"/>
        /// </summary>
        protected static class Resources
        {
            /// <summary>
            /// Resource string.
            /// </summary>
            public const string CannotBeConverted = "{0} value '{1}' cannot be converted to type {2}.";
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns the name of this 
        /// <see cref="BaseServerObjectExpressionBuilder"/>
        /// subclass.
        /// </summary>
        public abstract string SourceObjectName { get; }

        /// <summary>
        /// Abstract property
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected abstract object GetValue(string key);

        #endregion

        #region Implementaiton of ExpressionBuilder

        /// <summary>
        /// Returns a value indicating whether the current <see cref="T:System.Web.Compilation.ExpressionBuilder"/> object supports no-compile pages.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Web.Compilation.ExpressionBuilder"/> supports expression evaluation; otherwise, false.
        /// </returns>
        public override bool SupportsEvaluate
        {
            get { return true; }
        }

        /// <summary>
        /// Returns an object that represents an evaluated expression.
        /// </summary>
        /// <param name="target">The object containing the expression.</param>
        /// <param name="entry">The object that represents information about the property bound to by the expression.</param>
        /// <param name="parsedData">The object containing parsed data as returned by <see cref="M:System.Web.Compilation.ExpressionBuilder.ParseExpression(System.String,System.Type,System.Web.Compilation.ExpressionBuilderContext)"/>.</param>
        /// <param name="context">Contextual information for the evaluation of the expression.</param>
        /// <returns>
        /// An object that represents the evaluated expression; otherwise, null if the inheritor does not implement <see cref="M:System.Web.Compilation.ExpressionBuilder.EvaluateExpression(System.Object,System.Web.UI.BoundPropertyEntry,System.Object,System.Web.Compilation.ExpressionBuilderContext)"/>.
        /// </returns>
        public override object EvaluateExpression(object target, BoundPropertyEntry entry, object parsedData,
                                                  ExpressionBuilderContext context)
        {

            var result = GetRequestedValue(entry.Expression.Trim(), target.GetType(), entry.PropertyInfo.Name);
            return result;
        }

        /// <summary>
        /// Returns code that is used during page execution to obtain the evaluated expression.
        /// </summary>
        /// <param name="entry">The object that represents information about the property bound to by the expression.</param>
        /// <param name="parsedData">The object containing parsed data as returned by <see cref="M:System.Web.Compilation.ExpressionBuilder.ParseExpression(System.String,System.Type,System.Web.Compilation.ExpressionBuilderContext)"/>.</param>
        /// <param name="context">Contextual information for the evaluation of the expression.</param>
        /// <returns>
        /// A <see cref="T:System.CodeDom.CodeExpression"/> that is used for property assignment.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData,
                                                         ExpressionBuilderContext context)
        {
            CodeExpression[] inputParams = new CodeExpression[]
                                               {
                                                   new CodePrimitiveExpression(entry.Expression.Trim()),
                                                   new CodeTypeOfExpression(entry.DeclaringType),
                                                   new CodePrimitiveExpression(entry.PropertyInfo.Name)
                                               };

            // Return a CodeMethodInvokeExpression that will invoke the GetRequestedValue method using the specified input parameters
            return new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(GetType()),
                                                  "Instance().GetRequestedValue",
                                                  inputParams);
        }

        #endregion

        #region Public Method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="targetType"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public object GetRequestedValue(string key, Type targetType, string propertyName)
        {
            // First make sure that the server object will be available
            if (HttpContext.Current == null)
            {
                return null;
            }

            // Get the value
            object value = GetValue(key);

            // Make sure that the value exists
            if (value == null)
            {
                throw new InvalidOperationException(
                    string.Format("{0} field '{1}' not found.",
                                  SourceObjectName,
                                  key));
            }

            // If the value is being assigned to a control property we may need to convert it
            if (targetType != null)
            {
                PropertyDescriptor propDesc = TypeDescriptor.GetProperties(targetType)[propertyName];
                if (propDesc != null && propDesc.PropertyType != value.GetType() && propDesc.Converter != null)
                {
                    // Type mismatch - make sure that the value can be converted
                    if (propDesc.Converter.CanConvertFrom(value.GetType()) == false)
                    {
                        throw new InvalidOperationException(
                            Resources.CannotBeConverted.FormatStringCurrentUICulture(
                                SourceObjectName,
                                key,
                                propDesc.PropertyType));
                    }

                    return propDesc.Converter.ConvertFrom(value);
                }
            }

            // If we reach here, no type mismatch - return the value
            return value;
        }

        #endregion
    }
}