namespace XAct.UI.Web.Compilation
{
    using System.CodeDom;
    using System.Web.Compilation;
    using System.Web.UI;

    /// <summary>
    /// An ASP.NET ExpressionBuilder that evaluates Code snippets.
    /// </summary>
    /// <remarks>
    /// <para>
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <system.web>
    ///   <compilation>
    ///     <expressionBuilders>
    ///       <add expressionPrefix="Code"
    ///            type="XAct.UI.Web.Compilation.ExternalResourceExpressionBuilder, 
    ///                  XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///     </expressionBuilders>
    ///   </compilation>
    ///   ...
    /// </system.web>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <asp:Label runat="server" Text="<%$ Code:DateTime.Now %>"/>
    /// <asp:Label runat="server" Text='<%$ "Your IP Address is: " + Request.ServerVariables["REMOTE_ADDR"] %>'/>
    /// <asp:Label runat="server" Text='<%$ AMethodOnPage() %>'/>
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal>
    /// Src: http://bit.ly/ficsAI
    /// </internal>
    [ExpressionPrefix("Code"), ExpressionEditor("XAct.UI.Web.Compilation.Design.CodeExpressionEditor, XAct.UI.Web")]
    public class CodeExpressionBuilder : ExpressionBuilder
    {
        /// <summary>
        /// When overridden in a derived class, returns code that is used during page execution to obtain the evaluated expression.
        /// </summary>
        /// <param name="entry">The object that represents information about the property bound to by the expression.</param>
        /// <param name="parsedData">The object containing parsed data as returned by <see cref="M:System.Web.Compilation.ExpressionBuilder.ParseExpression(System.String,System.Type,System.Web.Compilation.ExpressionBuilderContext)"/>.</param>
        /// <param name="context">Contextual information for the evaluation of the expression.</param>
        /// <returns>
        /// A <see cref="T:System.CodeDom.CodeExpression"/> that is used for property assignment.
        /// </returns>
        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData,
                                                         ExpressionBuilderContext context)
        {
            return new CodeSnippetExpression(entry.Expression.Trim());
        }
    }
}