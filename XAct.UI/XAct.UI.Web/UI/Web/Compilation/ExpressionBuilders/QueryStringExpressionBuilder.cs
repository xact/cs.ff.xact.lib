namespace XAct.UI.Web.Compilation
{
    using System.Web;
    using System.Web.Compilation;
    using XAct.Diagnostics;

    /// <summary>
    /// An ASP.NET ExpressionBuilder that returns QueryString values.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Configuration:
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <system.web>
    ///   <compilation>
    ///     <expressionBuilders>
    ///       <add expressionPrefix="QueryString"
    ///            type="XAct.UI.Web.Compilation.QueryStringExpressionBuilder, 
    ///                  XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///     </expressionBuilders>
    ///   </compilation>
    ///   ...
    /// </system.web>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// </para>
    /// Usage:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <asp:Label runat="server" ID="WelcomeMessage" 
    ///            Text="<%$ QueryString:Message %>" />
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal>
    /// Src: http://bit.ly/idCsDv
    /// </internal>
    [ExpressionPrefix("QueryString"),
     ExpressionEditor("XAct.UI.Web.Compilation.Design.QueryStringExpressionEditor, XAct.UI.Web")]
    public class QueryStringExpressionBuilder : BaseServerObjectExpressionBuilder
    {
        #region Services

        private static readonly ITracingService _tracingService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryStringExpressionBuilder"/> class.
        /// </summary>
        static QueryStringExpressionBuilder()
        {
            //As the service not only cannot be constructed, 
            //but also has to be static, we need to hard code a logger.
            //As we have a ref to XAct.Core, we can do the following.
            _tracingService = DependencyResolver.Current.GetInstance<ITracingService>();
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// A Factory method that returns a new instance of this
        /// <see cref="QueryStringExpressionBuilder"/>
        /// </summary>
        /// <returns>A instance of <see cref="QueryStringExpressionBuilder"/></returns>
        public static QueryStringExpressionBuilder Instance()
        {
            return new QueryStringExpressionBuilder();
        }

        #endregion

        #region Implementation of BaseServerObjectExpressionBuilder

        /// <summary>
        /// Returns the name of this
        /// <see cref="BaseServerObjectExpressionBuilder"/>
        /// subclass.
        /// </summary>
        /// <value></value>
        public override string SourceObjectName
        {
            get { return "QueryString"; }
        }

        /// <summary>
        /// Abstract property
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected override object GetValue(string key)
        {
            return HttpContext.Current.Request.QueryString[key];
        }

        #endregion
    }
}