namespace XAct.UI.Web.Compilation
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Web.Compilation;
    using System.Web.UI;
    using XAct.Diagnostics;
    using XAct.Resources;

    /// <summary>
    /// Custom expression builder support for $ExternalResource expressions.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Configuration:
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <system.web>
    ///   <compilation>
    ///     <expressionBuilders>
    ///       <add expressionPrefix="ExternalResource"
    ///            type="XAct.UI.Web.Compilation.ExternalResourceExpressionBuilder, 
    ///                  XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///     </expressionBuilders>
    ///   </compilation>
    ///   ...
    /// </system.web>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Usage:
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <asp:Label runat="server" 
    ///            ID="UI_Label_Name"     
    ///            Text="<%$ ExternalResource:ClassLibrary1|Properties.Resources, UI_Label_Name %>" />
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal>
    /// Src: http://bit.ly/hARyDU
    /// </internal>
    [ExpressionPrefix("ExternalResource"),
     ExpressionEditor("XAct.UI.Web.Compilation.Design.ExternalResourceExpressionEditor, XAct.UI.Web")]
    public class ExternalResourceExpressionBuilder : ExpressionBuilder
    {
        private static ResourceProviderFactory _resourceProviderFactory;

        #region Services

        private static readonly ITracingService _tracingService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalResourceExpressionBuilder"/> class.
        /// </summary>
        static ExternalResourceExpressionBuilder()
        {
            //As the service not only cannot be constructed, 
            //but also has to be static, we need to hard code a logger.
            //As we have a ref to XAct.Core, we can do the following.
            _tracingService = DependencyResolver.Current.GetInstance<ITracingService>();
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Gets the global resource object.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <param name="classKey">The class key.</param>
        /// <param name="resourceKey">The resource key.</param>
        /// <returns></returns>
        public static object GetGlobalResourceObject(string assemblyName, string classKey, string resourceKey)
        {
            //_tracingService.DebugTrace(
            //    TraceLevel.Verbose,
            //    "ExternalResourceExpressionBuilder.GetGlobalResourceObject({0}, {1})"
            //        .FormatStringInvariantCulture(
            //            classKey,
            //            resourceKey));

            var result = GetGlobalResourceObject(assemblyName, classKey, resourceKey, null);
            return result;
        }

        /// <summary>
        /// Gets the global resource object.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <param name="classKey">The class key.</param>
        /// <param name="resourceKey">The resource key.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public static object GetGlobalResourceObject(string assemblyName, string classKey, string resourceKey,
                                                     CultureInfo culture)
        {
            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "ExternalResourceExpressionBuilder.GetGlobalResourceObject({0}, {1}, {2})"
                    .FormatStringInvariantCulture(classKey, resourceKey, culture));

            EnsureResourceProviderFactory();
            IResourceProvider provider =
                _resourceProviderFactory.CreateGlobalResourceProvider("{0},{1}".FormatStringInvariantCulture(classKey,
                                                                                                             assemblyName));

            return provider.GetObject(resourceKey, culture);
        }

        #endregion

        #region Implementation of Expresssion Builder

        /// <summary>
        /// Returns a value indicating whether the current 
        /// <see cref="T:System.Web.Compilation.ExpressionBuilder"/> 
        /// object supports no-compile pages.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Web.Compilation.ExpressionBuilder"/> 
        /// supports expression evaluation; otherwise, false.
        /// </returns>
        public override bool SupportsEvaluate
        {
            get
            {
                _tracingService.DebugTrace(
                    TraceLevel.Verbose,
                    "ExternalResourceExpressionBuilder.SupportsEvaluate");

                return true;
            }
        }

        /// <summary>
        /// Returns an object that represents an evaluated expression.
        /// </summary>
        /// <param name="target">The object containing the expression.</param>
        /// <param name="entry">
        /// The object that represents information about the property bound to by the expression.</param>
        /// <param name="parsedData">
        /// The object containing parsed data as returned by 
        /// <see cref="M:System.Web.Compilation.ExpressionBuilder.ParseExpression(System.String,System.Type,System.Web.Compilation.ExpressionBuilderContext)"/>.
        /// </param>
        /// <param name="context">Contextual information for the evaluation of the expression.</param>
        /// <returns>
        /// An object that represents the evaluated expression; 
        /// otherwise, null if the inheritor does not implement 
        /// <see cref="M:System.Web.Compilation.ExpressionBuilder.EvaluateExpression(System.Object,System.Web.UI.BoundPropertyEntry,System.Object,System.Web.Compilation.ExpressionBuilderContext)"/>.
        /// </returns>
        public override object EvaluateExpression(object target, BoundPropertyEntry entry, object parsedData,
                                                  ExpressionBuilderContext context)
        {
            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "ExternalResourceExpressionBuilder.EvaluateExpression({0}, {1}, {2}, {3})"
                    .FormatStringInvariantCulture(target, entry, parsedData, context));

            ExternalResourceExpressionFields fields
                = parsedData as ExternalResourceExpressionFields;

            EnsureResourceProviderFactory();

            if (fields == null)
            {
                throw new ArgumentException(
                    Resources.ParsedDataNotValid.FormatStringCurrentUICulture(
                        parsedData,
                        typeof (ExternalResourceExpressionFields)));
            }


            IResourceProvider provider =
                _resourceProviderFactory
                    .CreateGlobalResourceProvider("{0},{1}".FormatStringInvariantCulture(fields.ClassKey,
                                                                                         fields.AssemblyName));

            return provider.GetObject(fields.ResourceKey, null);
        }

        /// <summary>
        /// Returns code that is used during page execution to obtain the evaluated expression.
        /// </summary>
        /// <param name="entry">The object that represents information about the property bound to by the expression.</param>
        /// <param name="parsedData">
        /// The object containing parsed data as returned by 
        /// <see cref="M:System.Web.Compilation.ExpressionBuilder.ParseExpression(System.String,System.Type,System.Web.Compilation.ExpressionBuilderContext)"/>.
        /// </param>
        /// <param name="context">Contextual information for the evaluation of the expression.</param>
        /// <returns>
        /// A <see cref="T:System.CodeDom.CodeExpression"/> that is used for property assignment.
        /// </returns>
        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData,
                                                         ExpressionBuilderContext context)
        {
            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "ExternalResourceExpressionBuilder.GetCodeExpression({0}, {1}, {2})"
                    .FormatStringInvariantCulture(entry, parsedData, context));

            ExternalResourceExpressionFields fields =
                parsedData as ExternalResourceExpressionFields;

            if (fields == null)
            {
                throw new ArgumentException(
                    Resources.ParsedDataNotValid.FormatStringCurrentUICulture(
                        parsedData,
                        typeof (ExternalResourceExpressionFields)));
            }

            CodeMethodInvokeExpression exp =
                new CodeMethodInvokeExpression(
                    new CodeTypeReferenceExpression(
                        typeof (ExternalResourceExpressionBuilder)),
                    "GetGlobalResourceObject",
                    new CodePrimitiveExpression(fields.AssemblyName),
                    new CodePrimitiveExpression(fields.ClassKey),
                    new CodePrimitiveExpression(fields.ResourceKey));

            return exp;
        }

        /// <summary>
        /// Returns an object that represents the parsed expression.
        /// </summary>
        /// <param name="expression">The value of the declarative expression.</param>
        /// <param name="propertyType">The type of the property bound to by the expression.</param>
        /// <param name="context">Contextual information for the evaluation of the expression.</param>
        /// <returns>
        /// An <see cref="T:System.Object"/>
        /// containing the parsed representation of the expression; 
        /// otherwise, 
        /// null if 
        /// <see cref="M:System.Web.Compilation.ExpressionBuilder.ParseExpression(System.String,System.Type,System.Web.Compilation.ExpressionBuilderContext)"/> 
        /// is not implemented.
        /// </returns>
        public override object ParseExpression(string expression, Type propertyType, ExpressionBuilderContext context)
        {
            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "ExternalResourceExpressionBuilder.ParseExpression({0}, {1}, {2})"
                    .FormatStringInvariantCulture(expression, propertyType, context));

            if (string.IsNullOrEmpty(expression))
            {
                throw new ArgumentException(
                    String.Format(
                        Resources.TooFewParameters, expression));
            }

            ExternalResourceExpressionFields fields = null;
            string classKey = null;
            string resourceKey = null;

            string[] expParams = expression.Split(new[] {',', ':', '|'});


            if (expParams.Length != 3)
            {
                throw new ArgumentException(Resources.TooFewParameters.FormatStringCurrentUICulture(expression));
            }
            classKey = expParams[0].Trim();
            string assemblyName = expParams[1].Trim();
            resourceKey = expParams[2].Trim();

            fields = new ExternalResourceExpressionFields(assemblyName, classKey, resourceKey);

            EnsureResourceProviderFactory();
            IResourceProvider rp =
                _resourceProviderFactory
                    .CreateGlobalResourceProvider("{0},{1}".FormatStringInvariantCulture(fields.ClassKey,
                                                                                         fields.AssemblyName));

            object res = rp.GetObject(fields.ResourceKey, CultureInfo.InvariantCulture);
            if (res == null)
            {
                throw new ArgumentException(Resources.ResourceNotFound.FormatStringCurrentUICulture(expression));
            }
            return fields;
        }

        #endregion

        #region Private Methods

        private static void EnsureResourceProviderFactory()
        {
            if (_resourceProviderFactory == null)
            {
                _resourceProviderFactory = new ExternalResourceProviderFactory();
            }
        }

        #endregion

        #region Nested type: Resources

        /// <summary>
        /// A class of resource strings.
        /// </summary>
        /// <internal>
        /// Makes it easier to pull out and use class 
        /// external to this framework.
        /// </internal>
        private class Resources
        {
            /// <summary>
            /// A Resource string.
            /// </summary>
            public static string TooManyParameters = "Too Few Parameters ('{0}')";

            /// <summary>
            /// A Resource string.
            /// </summary>
            public static string TooFewParameters = "Too Few Parameters ('{0}')";

            /// <summary>
            /// A Resource string.
            /// </summary>
            public static string ResourceNotFound = "ResourceNotFound (key:{0})";

            /// <summary>
            /// A Resource string.
            /// </summary>
            public static string ParsedDataNotValid = "Could not type '{0}' as '{1}'.";
        }

        #endregion
    }

    /// <summary>
    /// Argument package to hold parsed data.
    /// </summary>
    public class ExternalResourceExpressionFields
    {
        #region Properties

        /// <summary>
        /// Gets the name of the assembly.
        /// </summary>
        /// <value>The name of the assembly.</value>
        /// <internal><para>7/27/2011: Sky</para></internal>
        public string AssemblyName
        {
            get { return _assemblyName; }
        }

        private readonly string _assemblyName;


        /// <summary>
        /// Gets the class key.
        /// </summary>
        /// <value>The class key.</value>
        public string ClassKey
        {
            get { return _classKey; }
        }

        private readonly string _classKey;

        /// <summary>
        /// Gets the resource key.
        /// </summary>
        /// <value>The resource key.</value>
        public string ResourceKey
        {
            get { return _resourceKey; }
        }

        private readonly string _resourceKey;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalResourceExpressionFields"/> class.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <param name="classKey">The class key.</param>
        /// <param name="resourceKey">The resource key.</param>
        internal ExternalResourceExpressionFields(string assemblyName, string classKey, string resourceKey)
        {
            _assemblyName = assemblyName;
            _classKey = classKey;
            _resourceKey = resourceKey;
        }

        #endregion
    }
}