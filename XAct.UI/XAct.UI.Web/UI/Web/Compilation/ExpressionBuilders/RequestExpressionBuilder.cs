namespace XAct.UI.Web.Compilation
{
    using System.Web;
    using System.Web.Compilation;
    using XAct.Diagnostics;

    /// <summary>
    /// An ASP.NET ExpressionBuilder that returns Request values.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Configuration:
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <system.web>
    ///   <compilation>
    ///     <expressionBuilders>
    ///       <add expressionPrefix="Request"
    ///            type="XAct.UI.Web.Compilation.RequestExpressionBuilder, 
    ///                  XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///     </expressionBuilders>
    ///   </compilation>
    ///   ...
    /// </system.web>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// </para>
    /// Usage:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <asp:Label runat="server" ID="WelcomeMessage" 
    ///            Text="<%$ Request:Message %>" />
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal>
    /// Src: http://bit.ly/idCsDv
    /// </internal>
    [ExpressionPrefix("Request"),
     ExpressionEditor("XAct.UI.Web.Compilation.Design.RequestExpressionEditor, XAct.UI.Web")]
    public class RequestExpressionBuilder : BaseServerObjectExpressionBuilder
    {
        #region Services

        private static readonly ITracingService _tracingService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestExpressionBuilder"/> class.
        /// </summary>
        static RequestExpressionBuilder()
        {
            //As the service not only cannot be constructed, 
            //but also has to be static, we need to hard code a logger.
            //As we have a ref to XAct.Core, we can do the following.
            _tracingService = DependencyResolver.Current.GetInstance<ITracingService>();
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// A Factory method that returns a new instance of this
        /// <see cref="RequestExpressionBuilder"/>
        /// </summary>
        /// <returns>A instance of <see cref="RequestExpressionBuilder"/></returns>
        public static RequestExpressionBuilder Instance()
        {
            return new RequestExpressionBuilder();
        }

        #endregion

        #region Implementation of BaseServerObjectExpressionBuilder

        /// <summary>
        /// Returns the name of this
        /// <see cref="BaseServerObjectExpressionBuilder"/>
        /// subclass.
        /// </summary>
        /// <value></value>
        public override string SourceObjectName
        {
            get { return "Request"; }
        }

        /// <summary>
        /// Abstract property
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected override object GetValue(string key)
        {
            return HttpContext.Current.Request[key];
        }

        #endregion
    }
}