using System;

using XAct.Windows.Controls;


namespace XAct.Tests.Apps
{


    /// <summary>
    /// A List of Demo Schemas.
    /// </summary>
    public class DemoPlayerData : DemoPresentationData
    {

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="DemoPlayerData"/> class.
        /// </summary>
        public DemoPlayerData()
            : base("XAct.Tests.Apps.Demo{0}", "XAct.Tests.Apps.Demo{0}Explanation")
        {

        }
    }
}