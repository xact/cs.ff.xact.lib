﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace XAct.Tests.Apps
{
    public partial class Demo001Explanation : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Demo001Explanation"/> class.
        /// </summary>
        public Demo001Explanation()
        {
            InitializeComponent();
        }
    }
}
