﻿namespace XAct.UI
{
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public class View : IHasId<int>, IHasNameAndDescription, IHasEnabled, IHasOrder
    {
        /// <summary>
        /// Gets the Entity's datastore Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this  <see cref="View"/> is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        public bool Enabled { get; set; }
        
        /// <summary>
        /// Gets or sets an integer hint of the item's order within it's parent <see cref="View"/>.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets whether the order can be moved or not.
        /// <para>
        /// For example, the base page's Header, Body, Footer <see cref="View"/>s
        /// cannot be moved.
        /// </para>
        /// </summary>
        /// <value>
        /// The can be moved.
        /// </value>
        public int CanBeMoved { get; set; }

        /// <summary>
        /// Gets the name of the  <see cref="View"/>.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the <see cref="View"/>.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the collection of Renderers (ASP.Classic, ASP.MVC, WPF, etc.) 
        /// registered for this <see cref="View"/>
        /// </summary>
        /// <value>
        /// The renderers.
        /// </value>
        public ICollection<ViewRenderer> Renderers { get; private set;}

        /// <summary>
        /// Gets the Collection of (optional) nested child <see cref="View"/>s.
        /// </summary>
        /// <value>
        /// The views.
        /// </value>
        public ICollection<View> Views { get; private set; } 
    }

    /// <summary>
    /// 
    /// </summary>
    public class ViewRenderer: IHasId<int>, IHasTag 
    {
        /// <summary>
        /// Gets the Entity's datastore Id.
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// Gets or sets the <see cref="View"/>
        /// this <see cref="ViewRenderer"/> describes.
        /// </summary>
        /// <value>
        /// The view FK.
        /// </value>
        public int ViewFK { get; set; }

        /// <summary>
        /// Gets or sets information regarding this View definitions's 
        /// Framework (ASP.Classic, ASP.MVC, etc.).
        /// </summary>
        /// <value>
        /// The rendering framework.
        /// </value>
        public RenderingFramework RenderingFramework {get; set; }

        /// <summary>
        /// Gets or sets the unique tag for this view.
        /// <para>
        /// This could be the url to an *.ascx, a *.cshtml, etc.
        /// </para>
        /// </summary>
        public string Tag { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class ViewControl: IHasId<int>, IHasEnabled
    {
        /// <summary>
        /// Gets the Entity's datastore Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this  <see cref="View"/> is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        public bool Enabled { get; set; }
    }

    /// <summary>
    /// The rendering mode of the View (ASP.Classic, ASP.MVC, Custom, etc.
    /// <para>
    /// Depending on the UI Framework used, the View has to point to a different 
    /// target (eg, in one case, the relative url of a *.ASCX, in the other, a *.cshtml, 
    /// in yet another, the name of a Type)
    /// The rendering framework will have to know how to parse and utilise the returned
    /// <see cref="ViewRenderer.Tag"/> info returned.
    /// </para>
    /// </summary>
    public class RenderingFramework: IHasNameAndDescription
    {
        /// <summary>
        /// Gets the Entity's datastore Id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        public string Description { get; set; }
    }

}
