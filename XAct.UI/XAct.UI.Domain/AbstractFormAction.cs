namespace XAct.UI
{
    /// <summary>
    /// 
    /// </summary>
    public class AbstractFormAction : IHasIdReadOnly<int>
    {
        /// <summary>
        /// Gets or sets the datastore id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public virtual int Id { get; /*protected*/ set; }
        /// <summary>
        /// Gets or sets the client side key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public virtual int Key { get; set; }

        /// <summary>
        /// Gets or sets the action display text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public virtual int Text { get; set; }
        /// <summary>
        /// Gets or sets the non-displayed (posted back) value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public virtual int Value { get; set; }

        /// <summary>
        /// Gets or sets the optional hint/tooltip.
        /// </summary>
        /// <value>
        /// The hint.
        /// </value>
        public virtual int Hint { get; set; }
    }
}