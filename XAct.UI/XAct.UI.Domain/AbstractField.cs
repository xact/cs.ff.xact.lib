namespace XAct.UI
{
    using System;

    /// <summary>
    ///   An XAct.UI.Domain class.
    /// </summary>
    public class AbstractField : IHasIdReadOnly<int>
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public virtual int Id { get; /*protected*/ set; }

        /// <summary>
        /// Gets or sets the clientside key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public virtual string Key { get; set; }
        /// <summary>
        /// Gets or sets the field label to render.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public virtual string Label { get; set; }

        /// <summary>
        /// Gets or sets the field type (int, string, dateTime, date, time, etc.).
        /// </summary>
        /// <value>
        /// The field type FK.
        /// </value>
        public virtual int FieldTypeFK { get; set; }
        /// <summary>
        /// Gets or sets the field type (int, string, dateTime, date, time, etc.).
        /// </summary>
        /// <value>
        /// The type of the field.
        /// </value>
        public virtual AbstractFormFieldType FieldType { get; protected set; }

        /// <summary>
        /// Gets or sets the datastore serialized value.
        /// </summary>
        /// <value>
        /// The serialized value.
        /// </value>
        public virtual string SerializedValue { get; set; }


       // public virtual SelectableItemList Options { get; set; }


        /// <summary>
        /// Gets or sets the name of the group/fieldset in which this field should 
        /// be displayed.
        /// </summary>
        /// <value>
        /// The name of the group.
        /// </value>
        public virtual string FieldSet { get; set; }

        /// <summary>
        /// Gets or sets the order of the field within the fieldset.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether -- space permitting -- 
        /// to float the item left.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [float left]; otherwise, <c>false</c>.
        /// </value>
        public virtual FlowState FloatState { get; set; }

        /// <summary>
        /// Gets or sets the edit state raw.
        /// </summary>
        /// <value>
        /// The edit state raw.
        /// </value>
        public virtual int EditStateRaw { get; set; }

        /// <summary>
        /// Gets or sets the edit state method.
        /// </summary>
        /// <value>
        /// The edit state method.
        /// </value>
        public Func<bool> EditStateMethod { get; set; }

        /// <summary>
        /// An optional hint/tooltip.
        /// </summary>
        public virtual int Hint { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="AbstractField"/> is searchable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if searchable; otherwise, <c>false</c>.
        /// </value>
        public bool Searchable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="AbstractField"/> is sortable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if sortable; otherwise, <c>false</c>.
        /// </value>
        public bool Sortable { get; set; }

        /// <summary>
        /// Gets or sets the name of the class to embed into the field.
        /// </summary>
        /// <value>
        /// The name of the class.
        /// </value>
        public virtual string ClassName { get; set; }
        /// <summary>
        /// Gets or sets the style to embed into the field.
        /// </summary>
        /// <value>
        /// The style.
        /// </value>
        public virtual string Style { get; set; }
        /// <summary>
        /// Gets or sets the attributes to embed into the field.
        /// </summary>
        /// <value>
        /// The attributes.
        /// </value>
        public virtual string Attributes { get; set; }
    }
}


/// <summary>
/// 
/// </summary>
public enum FlowState
{
    /// <summary>
    /// 
    /// </summary>
    Clear,
    /// <summary>
    /// 
    /// </summary>
    Flow,
    /// <summary>
    /// 
    /// </summary>
    AutoHeight 
}
