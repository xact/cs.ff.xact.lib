namespace XAct.UI
{
    /// <summary>
    /// 
    /// </summary>
    public class ValidationRule : IHasIdReadOnly<int>
    {
        /// <summary>
        /// Gets or sets the datastore id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public virtual int Id { get; /*protected*/ set; }


        /// <summary>
        /// Gets or sets the field this validation applies to.
        /// </summary>
        /// <value>
        /// The field.
        /// </value>
        public virtual int FieldFK { get; set; }

        /// <summary>
        /// Gets or sets the field this validation applies to.
        /// </summary>
        /// <value>
        /// The field.
        /// </value>
        public virtual AbstractField Field { get; protected set; } 


    }
}