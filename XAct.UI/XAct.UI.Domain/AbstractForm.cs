namespace XAct.UI{
    using System.Collections.Generic;

    /// <summary>
    /// Abstract form schema object
    /// </summary>
    public class AbstractForm : IHasIdReadOnly<int>
    {
        /// <summary>
        /// Gets or sets the DataStore id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public virtual int Id { get; /*protected*/ set; }

        /// <summary>
        /// Gets or sets an optional title for the form.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; protected set; }
        /// <summary>
        /// Gets or sets an optional preamble description text that 
        /// can be displayed under the title.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the fields of the form.
        /// </summary>
        /// <value>
        /// The fields.
        /// </value>
        public ICollection<AbstractField> Fields { get; set; }

        /// <summary>
        /// Gets or sets the collection of actions the user can perform.
        /// </summary>
        /// <value>
        /// The actions.
        /// </value>
        public ICollection<AbstractFormAction> Actions { get; set; }
    }
}