namespace XAct.UI
{
    /// <summary>
    /// 
    /// </summary>
    public class AbstractFormFieldType : IHasIdReadOnly<int>
    {
        /// <summary>
        /// Gets or sets the datastore id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public virtual int Id { get; /*protected*/ set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the serialized field,
        /// understandable by the target technology.
        /// </summary>
        /// <value>
        /// The type of the serialized field.
        /// </value>
        public virtual string SerializedFieldType { get; set; }

        /// <summary>
        /// Gets or sets the name of the class.
        /// </summary>
        /// <value>
        /// The name of the class.
        /// </value>
        public virtual string ClassName { get; set; }
        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        /// <value>
        /// The style.
        /// </value>
        public virtual string Style { get; set; }
        /// <summary>
        /// Gets or sets the attributes to embed into the field.
        /// </summary>
        /// <value>
        /// The attributes.
        /// </value>
        public virtual string Attributes { get; set; }
    }
}