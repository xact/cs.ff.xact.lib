﻿namespace XAct.ServiceProcess
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using System.Threading;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IWindowsServiceRunner"/>
    /// allowing the running of Services via the default Windows Service Host, 
    /// as well as from the Console, allowing for an easier debugging
    /// experience.
    /// <para>
    /// Usage Example:
    /// <![CDATA[
    /// static void Main(string[] args) {
    ///   NinjectBootstrapper.Initialize();
    ///   ServiceLocatorService.Current
    ///     .GetInstance<ITracingService>().Trace(TraceLevel.Info, "Starting....");
    ///   //Instantiate Service:
    ///   IMyService myService =
    ///     DependencyResolver.Current.GetInstance<IMyService>();
    /// 
    ///   //Services don't start doing work until their OnStart method is invoked.
    ///   //Can use ServiceBase.Run, but use IWindowsServiceRunner to 
    ///   //offer ability to run from console as well:
    ///   IWindowsServiceRunner windowsServiceProcessRunner =
    ///     DependencyResolver.Current.GetInstance<IWindowsServiceRunner>();
    ///   //Runner, depending on Environement.IsUserInteractive, runs quietly, or interacts
    ///   //with the console based on the Environement.IsUserInteractive variable:
    ///   windowsServiceProcessRunner.Execute(myService as ServiceBase);
    /// }
    /// ]]>
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IWindowsServiceRunner), BindingLifetimeType.Undefined, Priority.Low)]
    public class WindowsServiceProcessRunner : IWindowsServiceRunner 
    {

        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly IWindowsServiceManagementService _windowsServiceManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsServiceProcessRunner"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="windowsServiceManagementService">The windows service management service.</param>
        public WindowsServiceProcessRunner(
            ITracingService tracingService, 
            IEnvironmentService environmentService, 
            IWindowsServiceManagementService windowsServiceManagementService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            environmentService.ValidateIsNotDefault("environmentService");
            windowsServiceManagementService.ValidateIsNotDefault("windowsServiceManagementService");

            _tracingService = tracingService;
            _environmentService = environmentService;
            _windowsServiceManagementService = windowsServiceManagementService;
        }


        /// <summary>
        /// Runs the Service.
        /// <para>
        /// Usage Example:
        /// <![CDATA[
        /// static void Main(string[] args) {
        ///   NinjectBootstrapper.Initialize();
        ///   ServiceLocatorService.Current
        ///     .GetInstance<ITracingService>().Trace(TraceLevel.Info, "Starting....");
        ///   //Instantiate Service:
        ///   IMyService myService =
        ///     DependencyResolver.Current.GetInstance<IMyService>();
        /// 
        ///   //Services don't start doing work until their OnStart method is invoked.
        ///   //Can use ServiceBase.Run, but use IWindowsServiceRunner to 
        ///   //offer ability to run from console as well:
        ///   IWindowsServiceRunner windowsServiceProcessRunner =
        ///     DependencyResolver.Current.GetInstance<IWindowsServiceRunner>();
        ///   //Runner, depending on Environement.IsUserInteractive, runs quietly, or interacts
        ///   //with the console based on the Environement.IsUserInteractive variable:
        ///   windowsServiceProcessRunner.Execute(myService as ServiceBase);
        /// }
        /// ]]>
        /// </para>
        /// </summary>
        /// <param name="servicesToRun">The services to run.</param>
        public void Execute(params System.ServiceProcess.ServiceBase[] servicesToRun)
        {
            RunServices(servicesToRun);
        }

        /// <summary>
        /// Runs the services.
        /// </summary>
        /// <param name="servicesToRun">The services to run.</param>
        /// <param name="args">The args.</param>
        /// Either runs service as console or windows service depending on Environment.UserInteractive
        /// List of services to launch
        /// Command line parameters to pass along
        private void RunServices(System.ServiceProcess.ServiceBase[] servicesToRun, string[] args = null)
        {


            if (_environmentService.IsUserInteractive)
            {

                //TODO:A Self Installing Service...Easy enough:
                //http://blogs.microsoft.co.il/blogs/kim/archive/2009/01/04/self-installing-windows-service.aspx

                //A ServiceProcess class has two methods --OnStart and OnStop,
                //that are both protected -- hence the need for reflection to
                //access them.
                //If you call it directly, it won't get to the next line
                //until the invoqee relinquishes...which in most cases
                //will be the end....
                //CallServiceMethodByReflection(servicesToRun, "OnStart", args);


                ParameterizedThreadStart parameterizedThreadStart = CallStart;

                //ThreadStart threadDelegate = new ThreadStart(Work.DoWork);

                Thread newThread = new Thread(parameterizedThreadStart);

                newThread.Start(servicesToRun);

                WaitPrompt(servicesToRun);

                CallServiceMethodByReflection(servicesToRun, "OnStop");

                Console.WriteLine(
                    string.Format(
                    CultureInfo.CurrentUICulture,
                    "Service '{0}' OnStop invoked...",
                    servicesToRun[0].ServiceName));
            }
            else
            {
                System.ServiceProcess.ServiceBase.Run(servicesToRun);
            }
        }

        //Need a static Method that fits signature of ParameterizedThreadStart
        //taking an object
        private static void CallStart(object o)
        {
            //A bit clunky (there is no generic signature for this)
            //but as we know what we are getting, so we can easily retype it:
            System.ServiceProcess.ServiceBase[] services = (System.ServiceProcess.ServiceBase[])o;

            //Done -- can get on with normal work:
            CallServiceMethodByReflection(services, "OnStart");
        }

        private static void CallServiceMethodByReflection(IEnumerable<System.ServiceProcess.ServiceBase> services, string methodName, object[] args = null)
        {
            foreach (System.ServiceProcess.ServiceBase service in services)
            {
                CallServiceMethodByReflection(service, methodName, args);
            }
        }

        private static void CallServiceMethodByReflection(System.ServiceProcess.ServiceBase service, string methodName, object[] args = null)
        {
            //_tracingService.Trace("{0}.{1} invoked...".FormatStringCurrentUICulture(service.ServiceName,methodName));

            Type type = typeof(System.ServiceProcess.ServiceBase);
            
            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic;
            
            MethodInfo methodInfo = type.GetMethod(methodName, bindingFlags);

            if (args == null)
            {
                //OnStart is a bit different, it takes args -- OnPause, OnResume, OnStop don't.
                if (methodName == "OnStart")
                {
                    args = new object[] { new string[] { } };
                }
                else
                {
                    args = new object[] {};
                }
            }

            methodInfo.Invoke(service, args);
        }


        private static void WaitPrompt(System.ServiceProcess.ServiceBase[] ServicesToRun)
        {

            Console.WriteLine("------------------------------------------------------------------------");
            foreach (System.ServiceProcess.ServiceBase serviceBase in ServicesToRun)
            {
                Console.WriteLine(
                    string.Format(
                    CultureInfo.CurrentUICulture,
                    "Service '{0}' running...",
                    ServicesToRun[0].ServiceName));
            }
            Console.WriteLine("Instructions: press 'p' to pause, 'r'/'c' to resume, 's' or ESC to stop...");
            Console.WriteLine("------------------------------------------------------------------------");

            bool running = true;

            do
            {
                //Catch key, without displaying it:
                ConsoleKeyInfo keypress = Console.ReadKey(true);

                switch (keypress.Key)
                {
                    case ConsoleKey.P:
                        Console.WriteLine("PAUSE requested...");
                        CallServiceMethodByReflection(ServicesToRun, "OnPause");
                        break;
                    case ConsoleKey.C:
                        Console.WriteLine("CONTINUE requested...");
                        CallServiceMethodByReflection(ServicesToRun, "OnContinue");
                        break;
                    case ConsoleKey.R:
                        Console.WriteLine("RESUME requested...");
                        CallServiceMethodByReflection(ServicesToRun, "OnContinue");
                        break;
                    case ConsoleKey.Escape:
                    case ConsoleKey.S:
                        // Stop is called after the prompt, don't do it here
                        Console.WriteLine("STOP requested...");
                        running = false;
                        break;
                }
                //Give a break to this thread:
                //Not needed:
                //Thread.Sleep(100);

            } while (running);

        }



    } // END CLASS: NetServiceDebug


} // END NAMESPACE
