﻿// ReSharper disable CheckNamespace
namespace XAct.ServiceProcess
// ReSharper restore CheckNamespace
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Contract for a Service to manage the installation, uninstallation,
    /// starting and stopping of 
    /// Windows Services.
    /// </summary>
    public interface IWindowsServiceManagementService : IHasXActLibService
    {
        /// <summary>
        /// Gets or sets the timeout (default is 30 seconds).
        /// </summary>
        /// <value>
        /// The timeout.
        /// </value>
        /// <remarks>
        /// Note that Services should not have State...this is an aberation.
        /// </remarks>
        TimeSpan Timeout { get; set; }

        /// <summary>
        /// Determines whether the specified Windows Service is installed.
        /// </summary>
        /// <param name="serviceName">The name of the Windows Service.</param>
        /// <returns>
        ///   <c>true</c> if the specified Windows Service is installed; otherwise, <c>false</c>.
        /// </returns>
        bool IsInstalled(string serviceName);

        /// <summary>
        /// Installs the specified assembly (an Exe) as a Windows Service.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        void Install(Assembly assembly);

        /// <summary>
        /// Installs the specified assembly (an Exe) as a Windows Service.
        /// </summary>
        void Install(string pathToExeAssembly);

        /// <summary>
        /// Uninstalls the specified assembly (an Exe) as a Windows Service.
        /// </summary>
        void Uninstall(Assembly assembly);

        /// <summary>
        /// Uninstalls the specified assembly (an Exe) as a Windows Service.
        /// </summary>
        void Uninstall(string pathToExeAssembly);

        /// <summary>
        /// Starts the specified Windows Service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="millisecondsToWait">The milliseconds to wait (if empty, defaults to <see cref="Timeout"/>).</param>
        void Start(string serviceName, int millisecondsToWait = 0);

        /// <summary>
        /// Stops the specified Windows Service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="millisecondsToWait">The milliseconds to wait (if empty, defaults to <see cref="Timeout"/>).</param>
        void Stop(string serviceName, int millisecondsToWait = 0);

        /// <summary>
        /// Restarts the specified service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="millisecondsToWait">The milliseconds to wait (if empty, defaults to <see cref="Timeout"/>).</param>
        void Restart(string serviceName, int millisecondsToWait = 0);

        /// <summary>
        /// Gets the Status of the  the specified Windows Service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns></returns>
        /// <param name="millisecondsToWait">The milliseconds to wait (if empty, defaults to <see cref="Timeout"/>).</param>
        string Status(string serviceName, int millisecondsToWait=0);
    }
}