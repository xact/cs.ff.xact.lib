﻿// ReSharper disable CheckNamespace
namespace XAct.ServiceProcess.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Configuration.Install;
    using System.Reflection;
    using System.ServiceProcess;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IWindowsServiceManagementService"/>
    /// for a Service to manage the installation, uninstallation,
    /// starting and stopping of 
    /// Windows Services.
    /// </summary>
    public class WindowsServiceManagementService : IWindowsServiceManagementService
    {
        /// <summary>
        /// Gets or sets the timeout (default is 30 seconds).
        /// </summary>
        /// <value>
        /// The timeout.
        /// </value>
        public TimeSpan Timeout { get { return _timeout; } set {_timeout= new TimeSpan((long)Math.Max(value.TotalMilliseconds, 5000)); } }
        private TimeSpan _timeout = TimeSpan.FromMilliseconds(30 * 1000);

        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="WindowsServiceManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public WindowsServiceManagementService(ITracingService tracingService)
        {
            tracingService.ValidateIsNotDefault("tracingService");

            _tracingService = tracingService;
        }


        /// <summary>
        /// Determines whether the specified Windows Service is installed.
        /// </summary>
        /// <param name="serviceName">The name of the Windows Service.</param>
        /// <returns>
        ///   <c>true</c> if the specified Windows Service is installed; otherwise, <c>false</c>.
        /// </returns>
        public bool IsInstalled(string serviceName)
        {
            using (ServiceController serviceController = new ServiceController(serviceName))
            {
                try
                {
                    //Do something that will cause an exception if not installed:
#pragma warning disable 168
                    ServiceControllerStatus serviceControllerStatus = serviceController.Status;
#pragma warning restore 168
                    return true;
                }
                catch (InvalidOperationException)
                {
                    return false;
                }
                //other exceptions are not expected.
            }
        }
 
        /// <summary>
        /// Installs the specified assembly (an Exe) as a Windows Service.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        public void Install(Assembly assembly)
        {
            Install(assembly.Location);
        }
        /// <summary>
        /// Installs the specified assembly (an Exe) as a Windows Service.
        /// </summary>
        public void Install(string pathToExeAssembly)
        {
            pathToExeAssembly.ValidateIsNotNullOrEmpty("pathToExeAssembly");

            //string pathToExeAssembly = Assembly.GetExecutingAssembly().Location;
            // ReSharper disable RedundantExplicitArrayCreation
            //ManagedInstallerClass.InstallHelper(new string[] { pathToExeAssembly });
            // ReSharper restore RedundantExplicitArrayCreation

            //This approach requires an Windows Installer to be in the app:
// ReSharper disable UseObjectOrCollectionInitializer
            AssemblyInstaller assemblyInstaller = new AssemblyInstaller(pathToExeAssembly,null);
// ReSharper restore UseObjectOrCollectionInitializer

            assemblyInstaller.UseNewContext = true;

            assemblyInstaller.Install(null);

            assemblyInstaller.Commit(null);
        
        }
        /// <summary>
        /// Uninstalls the specified assembly (an Exe) as a Windows Service.
        /// </summary>
        public void Uninstall(Assembly assembly)
        {
            Uninstall(assembly.Location);
        }
        /// <summary>
        /// Uninstalls the specified assembly (an Exe) as a Windows Service.
        /// </summary>
        public void Uninstall(string pathToExeAssembly)
        {
            pathToExeAssembly.ValidateIsNotNullOrEmpty("pathToExeAssembly");

            // ReSharper disable RedundantExplicitArrayCreation
            //ManagedInstallerClass.InstallHelper(new string[] { "/u", pathToExeAssembly });
            // ReSharper restore RedundantExplicitArrayCreation


            //This approach requires an Windows Installer to be in the app:
// ReSharper disable UseObjectOrCollectionInitializer
            AssemblyInstaller assemblyInstaller = new AssemblyInstaller(pathToExeAssembly, null);
// ReSharper restore UseObjectOrCollectionInitializer
            assemblyInstaller.UseNewContext = true;
            assemblyInstaller.Uninstall(null);

        
        }
        /// <summary>
        /// Starts the specified Windows Service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="millisecondsToWait">The milliseconds to wait (if empty, defaults to <see cref="Timeout"/>).</param>
        public void Start(string serviceName, int millisecondsToWait = 0)
        {
            TimeSpan timeout = (millisecondsToWait == 0) ? _timeout : new TimeSpan(Math.Max(millisecondsToWait, 5000));

            using (ServiceController windowsServiceController = new ServiceController(serviceName))
            {
                    windowsServiceController.Start();
                    //Throws an exception if times out:
                    windowsServiceController.WaitForStatus(ServiceControllerStatus.Running, timeout);

            }
        }
        /// <summary>
        /// Stops the specified Windows Service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="millisecondsToWait">The milliseconds to wait (if empty, defaults to <see cref="Timeout"/>).</param>
        public void Stop(string serviceName, int millisecondsToWait = 0)
        {
            serviceName.ValidateIsNotNullOrEmpty("serviceName");

            TimeSpan timeout = (millisecondsToWait == 0) ? _timeout : new TimeSpan(Math.Max(millisecondsToWait, 5000));

            using (ServiceController windowsServiceController = new ServiceController(serviceName))
            {
                windowsServiceController.Stop();
                //Throws an exception if times out:
                windowsServiceController.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }

        }

        /// <summary>
        /// Restarts the specified service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="millisecondsToWait">The milliseconds to wait (if empty, defaults to <see cref="Timeout"/>).</param>
        public void Restart(string serviceName, int millisecondsToWait = 0)
        {
            serviceName.ValidateIsNotNullOrEmpty("serviceName");

            TimeSpan timeout = (millisecondsToWait == 0) ? _timeout : new TimeSpan(Math.Max(millisecondsToWait, 5000));

            using (ServiceController service = new ServiceController(serviceName))
            {
                    TimeSpan timeSpan = timeout;
                    DateTime startTime =  DateTime.Now;
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeSpan);

                    // count the rest of the timeout
                    long ticks = DateTime.Now.Ticks - startTime.Ticks; 

                    timeSpan = new TimeSpan(ticks);
                    service.Start();
                    //Throws an exception if times out:
                    service.WaitForStatus(ServiceControllerStatus.Running, timeSpan);
            }
        }

        /// <summary>
        /// Gets the Status of the  the specified Windows Service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="millisecondsToWait">The milliseconds to wait (if empty, defaults to <see cref="Timeout"/>).</param>
        /// <returns></returns>
        public string Status(string serviceName, int millisecondsToWait = 0)
        {
            serviceName.ValidateIsNotNullOrEmpty("serviceName");

            using (ServiceController windowsServiceController = new ServiceController(serviceName))
            {
                try
                {
                    return windowsServiceController.Status.ToString();
                }
                catch (Exception e)
                {
                    _tracingService.DebugTraceException(TraceLevel.Warning, e,
                                                        "WindowsServiceManagementService.Status Exception has occurred.");
                    throw;
                }
            }

        }



    }
}