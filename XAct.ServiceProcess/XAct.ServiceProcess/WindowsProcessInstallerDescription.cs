﻿//using System;
//using System.ServiceProcess;
//using XAct.Authentication;

//namespace XAct.ServiceProcess
//{
//    /// <summary>
//    /// An implementation of <see cref="IWindowsProcessInstallerDefinition"/>
//    /// to encapsulate the information required by
//    /// <see cref="System.ServiceProcess.ServiceInstaller"/>
//    /// (one per Service Exe) to install one or more Windows Services.
//    /// <para>
//    /// Used by <see cref="InstallerBase"/>
//    /// </para>
//    /// </summary>
//    public class WindowsProcessInstallerDescription : IWindowsProcessInstallerDefinition 
//    {

//        /// <summary>
//        /// Initializes a new instance of the <see cref="WindowsProcessInstallerDescription"/> class.
//        /// </summary>
//        /// <param name="serviceAccount">The service account.</param>
//        /// <param name="optionalServiceProcessInstallerUserNameAndPassword">The optional service process installer user name and password.</param>
//        public WindowsProcessInstallerDescription(ServiceAccount serviceAccount, IUserNameAndPassword optionalServiceProcessInstallerUserNameAndPassword=null)
//        {
//            //Validate information:
//            if (serviceAccount == ServiceAccount.User)
//            {
//                if ((optionalServiceProcessInstallerUserNameAndPassword == null)
//                    ||
//                    (optionalServiceProcessInstallerUserNameAndPassword.Name.IsNullOrEmpty()))
//                {
//                    throw new ArgumentNullException("optionalServiceProcessInstallerUserNameAndPassword");
//                }
//            }
//            else
//            {
//                if (optionalServiceProcessInstallerUserNameAndPassword != null)
//                {
//                    throw new ArgumentOutOfRangeException("serviceAccount",
//                        // ReSharper disable LocalizableElement
//                                                          "optionalServiceProcessInstallerUserNameAndPassword is not relevant if serviceAccountType is not set to ServiceAccount.User");
//                    // ReSharper restore LocalizableElement
//                }
//            }

//            ServiceAccount = serviceAccount;

//            if (optionalServiceProcessInstallerUserNameAndPassword == null)
//            {
//                return;
//            }

//            Name = optionalServiceProcessInstallerUserNameAndPassword.Name;
//            Password = optionalServiceProcessInstallerUserNameAndPassword.Password;
//        }


//        /// <summary>
//        /// Gets or sets the service account.
//        /// </summary>
//        /// <value>
//        /// The service account.
//        /// </value>
//        public ServiceAccount ServiceAccount { get; private set; }

//        /// <summary>
//        /// Gets or sets the name of the user (only set if ServiceACcount == ServiceAccount.User).
//        /// </summary>
//        /// <value>
//        /// The name of the user.
//        /// </value>
//        public string Name { get; private set; }

//        /// <summary>
//        /// Gets or sets the user password (only set if ServiceACcount == ServiceAccount.User).
//        /// </summary>
//        /// <value>
//        /// The password.
//        /// </value>
//        public string Password { get; private set; }
//    }
//}
