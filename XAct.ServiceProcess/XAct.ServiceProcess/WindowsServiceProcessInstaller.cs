﻿//using System.ServiceProcess;

//namespace XAct.ServiceProcess
//{
//    /// <summary>
//    /// Installs an executable  (*.exe) containing classes that extend ServiceBase. 
//    /// <para>
//    /// Called by installation utilities (eg: <c>InstallUtil.exe</c>), 
//    /// when installing a service application (*.exe) (writes registry values, etc.)
//    /// </para>
//    /// <para>
//    /// The <see cref="WindowsServiceProcessInstaller"/> does work common to all services in an executable. 
//    /// </para>
//    /// <para>
//    /// The class is instantiated and configured by <see cref="InstallerBase"/>.
//    /// </para>
//    /// </summary>
//    public class WindowsServiceProcessInstaller : System.ServiceProcess.ServiceProcessInstaller
//    {
//        /// <summary>
//        /// Initializes a new instance of the 
//        /// <see cref="WindowsServiceProcessInstaller"/> class.
//        /// <para>
//        /// Called by installation utilities (eg: <c>InstallUtil.exe</c>), 
//        /// when installing a service application (*.exe).
//        /// </para>
//        /// </summary>
//        /// <param name="windowsProcessInstallerDescription">The windows process installer description.</param>
//        public WindowsServiceProcessInstaller(IWindowsProcessInstallerDefinition windowsProcessInstallerDescription)
//        {
//            windowsProcessInstallerDescription.ValidateIsNotDefault("windowsProcessInstallerDescription");

//            Account = windowsProcessInstallerDescription.ServiceAccount;
//            Username = windowsProcessInstallerDescription.Name;
//            Password = windowsProcessInstallerDescription.Password;
            
//            //serviceProcessInstaller.HelpText;

//        }
//    }
//}
