﻿//using System.ServiceProcess;
//using XAct.Authentication;

//namespace XAct.ServiceProcess
//{
//    /// <summary>
//    /// Contract for the information required 
//    /// to encapsulate the information required by a 
//    /// <see cref="System.ServiceProcess.ServiceInstaller"/>
//    /// (one per Service Exe) to install one or more Windows Services.
//    /// <para>
//    /// Used by <see cref="InstallerBase"/>
//    /// </para>
//    /// </summary>
//    public interface IWindowsProcessInstallerDefinition : IReadOnlyUserNameAndPassword
//    {

//        /// <summary>
//        /// Gets or sets the type of the service account to use when installing a Windows Service.
//        /// <para>
//        /// Default is <see cref="ServiceAccount"/>.User
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The type of the service account.
//        /// </value>
//        ServiceAccount ServiceAccount { get; }

//    }
//}