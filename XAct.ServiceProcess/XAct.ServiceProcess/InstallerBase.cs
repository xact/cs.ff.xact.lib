﻿namespace XAct.ServiceProcess
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration.Install;
    using System.ServiceProcess;

    /// <summary>
    /// <para>
    /// Normally, you would not create an instance of your project installer class explicitly. 
    /// You would create it and add the RunInstallerAttribute attribute to the syntax, 
    /// but it is the install utility (InstallUtil.exe [myservice.exe]) that 
    /// actually calls, and therefore instantiates, the class.
    /// </para>
    /// </summary>
    /// <internal>
    /// MSDN:"To install a service, create a project installer class that inherits from Installer, 
    /// and set the RunInstallerAttribute on the class to true. 
    /// Within your project, instantiate  
    /// * one ServiceProcessInstaller instance per service application (*.exe), and
    /// * one ServiceInstaller instance for each service in the application. 
    /// Finally, add the ServiceProcessInstaller instance and the ServiceInstaller 
    /// instances to your project installer class.
    /// When InstallUtil.exe runs, the utility looks for classes in the service assembly 
    /// with the RunInstallerAttribute set to true. 
    /// Add classes to the service assembly by adding them to the Installers collection 
    /// associated with your project installer. 
    /// If RunInstallerAttribute is false, the install utility ignores the project installer.
    /// <para>
    /// RunInstaller is inheritable.
    /// </para>
    /// 
    /// </internal>
    [RunInstaller(true)]
    public abstract class InstallerBase : Installer
    {
        /// <summary>
        /// The InstallUtil switch to use to set the <c>AccountType</c> to 
        /// something else than the default.
        /// <para>
        /// Value is 'account'.
        /// </para>
        /// <para>
        /// Options are 'LocalSystem', 'LocalService', 'NetworkService', and 'User'
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// //running in the VS Command Prompt, in Admin Mode:
        /// installutil.exe /account=User /username=SvcAP /password=Passw0rd1 /startmode=automatic "c:\MyService.exe" 
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        public static string CmdLineAccountSwitch = "account";

        /// <summary>
        /// The InstallUtil switch to use to set the Installer's <c>UserName</c> to 
        /// something else than the default.
        /// <para>
        /// Value is 'username'.
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// //running in the VS Command Prompt, in Admin Mode:
        /// installutil.exe /account=User /username=SvcAP /password=Passw0rd1 /startmode=automatic "c:\MyService.exe" 
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        public static string CmdLineUserNameSwitch = "username";

        /// <summary>
        /// The InstallUtil switch to use to set the Installer's <c>Password</c> to 
        /// something else than the default.
        /// <para>
        /// Value is 'password'.
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// //running in the VS Command Prompt, in Admin Mode:
        /// installutil.exe /account=User /username=SvcAP /password=Passw0rd1 /startmode=automatic "c:\MyService.exe" 
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        public static string CmdLineUserPasswordSwitch = "password";

        /// <summary>
        /// The InstallUtil switch to use to set the Service <c>StartMode</c> to 
        /// something else than the default.
        /// <para>
        /// Value is 'startmode'.
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// //running in the VS Command Prompt, in Admin Mode:
        /// installutil.exe /account=User /username=SvcAP /password=Passw0rd1 /startmode=automatic "c:\MyService.exe" 
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        public static string CmdLineUserStartModeSwitch = "startmode";


        /// <summary>
        /// 
        /// </summary>
        protected ServiceProcessInstaller _serviceProcessInstaller;


        private ServiceInstaller[] _serviceInstallers;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstallerBase"/> class.
        /// </summary>
        /// <param name="serviceProcessInstaller">The service process installer.</param>
        /// <param name="serviceInstallers">The service installers.</param>
        protected InstallerBase(ServiceProcessInstaller serviceProcessInstaller, params ServiceInstaller[] serviceInstallers)
        {
            //May not always want to have an installer:
            //serviceProcessInstaller.ValidateIsNotDefault("serviceProcessInstaller");
            serviceInstallers.ValidateIsNotDefault("serviceInstallers");

            //Note that in the constructor, the Context is not yet built up
            //so you can't invoke Context.LogMessage, or ConfigureFromContextVariables....

            //Must be at least one service being installed.
            if (serviceInstallers.Length ==0)
            {
                throw new ArgumentNullException("serviceInstallers");
            }

            Init(serviceProcessInstaller, serviceInstallers);


        }


        /*
        /// <summary>
        /// Initializes a new instance of the <see cref="InstallerBase"/> class.
        /// <para>
        /// An usage example is:
        /// <code>
        /// <![CDATA[
        /// public class MyInstaller 
        ///    : 
        ///    InstallerBase(
        ///      new WindowsProcessInstallerDescription(...),
        ///      new WindowsServiceInstallerDescription(...),
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="windowsProcessInstallationInformation">The windows service installation information.</param>
        /// <param name="windowsServiceDefinitions">The 1 or more windows service definitions.</param>
        protected InstallerBase(IWindowsProcessInstallerDefinition windowsProcessInstallationInformation, params IWindowsServiceDefinition[] windowsServiceDefinitions)
        {
            if (windowsProcessInstallationInformation == null)
            {
                windowsProcessInstallationInformation = 
                    new WindowsProcessInstallerDescription(ServiceAccount.LocalSystem);
            }

            //Note that in the constructor, the Context is not yet built up
            //so you can't invoke Context.LogMessage, or ConfigureFromContextVariables....


            if (windowsProcessInstallationInformation.ServiceAccount == ServiceAccount.User)
            {
                if (windowsProcessInstallationInformation.Name.IsNullOrEmpty())
                {
                    throw new ArgumentNullException("windowsProcessInstallationInformation");
                }
            }
            else
            {
                if (
                    (windowsProcessInstallationInformation.Name.IsNullOrEmpty())
                    ||
                    (windowsProcessInstallationInformation.Password.IsNullOrEmpty())
                    )
                {
                    throw new ArgumentOutOfRangeException("windowsProcessInstallationInformation",
                                                          // ReSharper disable LocalizableElement
                                                          "windowsServiceInstallationInformation Name and Password is not relevant if serviceAccountType is not set to ServiceAccount.User");
                }
            }
        
            //Create one ServiceProcessInstaller instance per service application (*.exe)
            this.LogMessage("InstallerBase:Constructor:Creating a single ServiceProcessInstaller per Exe Assembly...");
            WindowsServiceProcessInstaller serviceProcessInstaller =
                new WindowsServiceProcessInstaller(windowsProcessInstallationInformation);
            this.LogMessage("InstallerBase:Constructor:Creating a single ServiceProcessInstaller per Exe Assembly...Done.");


            //VERY IMPORTANT: according to http://msdn.microsoft.com/en-us/library/system.serviceprocess.serviceprocessinstaller(v=vs.90).aspx
            //One can override the hardcoded values:


            this.LogMessage("InstallerBase:Constructor:Creating a single ServiceInstaller per ServiceBase...");
            List<ServiceInstaller> serviceInstallers = new List<ServiceInstaller>();

            // Create one ServiceInstaller per Service:ServiceBase
            foreach (IWindowsServiceDefinition windowsServiceDefinition in windowsServiceDefinitions)
            {
                serviceInstallers.Add(new WindowsServiceInstaller(windowsServiceDefinition));

            }
            this.LogMessage("InstallerBase:Constructor:Creating a single ServiceInstaller per ServiceBase...Done.");

            Init(serviceProcessInstaller, serviceInstallers.ToArray());
        }
        */


        /// <summary>
        /// Inits the specified service process installer.
        /// </summary>
        /// <param name="serviceProcessInstaller">The service process installer.</param>
        /// <param name="serviceInstallers">The service installers.</param>
        protected void Init(ServiceProcessInstaller serviceProcessInstaller,ServiceInstaller[] serviceInstallers )
        {
            //Note that in the constructor, the Context is not yet built up
            //so you can't invoke Context.LogMessage, or ConfigureFromContextVariables....
            
            //Therefore we need keep refs to the added items
            //in order to rework them when Context is available:
            _serviceProcessInstaller = serviceProcessInstaller;
            _serviceInstallers = serviceInstallers;

            //Add:
            this.LogMessage("InstallerBase:Init:Adding the ServiceProcessInstaller...");
            if (_serviceProcessInstaller != null)
            {
                this.Installers.Add(_serviceProcessInstaller);
            }
            this.LogMessage("InstallerBase:Init:Adding {0} ServiceInstaller(s)...".FormatStringCurrentUICulture(serviceInstallers.Length));
            this.Installers.AddRange(serviceInstallers);
        }


        /// <summary>
        /// When overridden in a derived class, performs the installation.
        /// </summary>
        /// <param name="stateSaver">An <see cref="T:System.Collections.IDictionary"/> used to save information needed to perform a commit, rollback, or uninstall operation.</param>
        /// <exception cref="T:System.ArgumentException">
        /// The <paramref name="stateSaver"/> parameter is null.
        ///   </exception>
        ///   
        /// <exception cref="T:System.Exception">
        /// An exception occurred in the <see cref="E:System.Configuration.Install.Installer.BeforeInstall"/> event handler of one of the installers in the collection.
        /// -or-
        /// An exception occurred in the <see cref="E:System.Configuration.Install.Installer.AfterInstall"/> event handler of one of the installers in the collection.
        ///   </exception>
        public override void Install(IDictionary stateSaver)
        {
            this.LogMessage("InstallerBase:Install:Begun...");

            ConfigureFromContextVariables();

            if (stateSaver.Count == 0)
            {
                this.LogMessage("StateServer Key/Value count = 0.");
            }
            else{
                this.LogMessage("StateServer Key/Value pairs:");
                foreach (KeyValuePair<object, object> pair in stateSaver)
                {
                    this.LogMessage("{0}={1}".FormatStringCurrentUICulture(pair.Key, pair.Value));
                }
            }

            this.LogMessage("InstallerBase:Install:Calling base...");
            base.Install(stateSaver);
            this.LogMessage("InstallerBase:Install:Complete.");
        }


        /// <summary>
        /// Pick up arguments that came in from the command line,
        /// or from an Install Forms.
        /// </summary>
        private void ConfigureFromContextVariables()
        {


            if (this.Context == null)
            {
                this.LogMessage("Context not ready...");
                return;
            }

            //Try to figure out which settings were set via command line
            //in order to overwrite defaults:
            ConfigureProcessInstallerAccountType();
            ConfigureProcessInstallerUserName();
            ConfigureProcessInstallerPassword();
            ConfigureServiceStartMode();


        }



        private void ConfigureProcessInstallerAccountType()
        {
            foreach (string key in new string[] {"account", "accounttype"})
            {
                if (this.Context.Parameters.ContainsKey(key))
                {

                    string value = this.Context.Parameters[key];

                    if (!value.IsNullOrEmpty())
                    {
                        try
                        {
                            _serviceProcessInstaller.Account =
                                (ServiceAccount) Enum.Parse(typeof (ServiceAccount), value);
                        }
                        catch
                        {
                            char firstChar = value.ToLower()[0];
                            switch (firstChar)
                            {
                                case 'n':
                                    _serviceProcessInstaller.Account = ServiceAccount.NetworkService;
                                    break;
                                case 'u':
                                    _serviceProcessInstaller.Account = ServiceAccount.User;
                                    break;
                                case 'l':
                                    _serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
                                    break;
                                default:
                                    _serviceProcessInstaller.Account = ServiceAccount.LocalService;
                                    break;
                            }
                            this.LogMessage(
                                "ServiceProcessInstaller Account set to {0}".FormatStringCurrentUICulture(
                                    _serviceProcessInstaller.Account));
                        }
                    }
                    break;
                }//~if
            }//~foreach
        }

        private void ConfigureProcessInstallerUserName()
        {
            foreach (string key in new[] {"username", "user"})
            {
                if (this.Context.Parameters.ContainsKey(key))
                {
                    string value = this.Context.Parameters[key];
                    if (!value.IsNullOrEmpty())
                    {
                        _serviceProcessInstaller.Username = value;
                        this.LogMessage("ServiceProcessInstaller UserName set to {0}".FormatStringCurrentUICulture(_serviceProcessInstaller.Username));
                    }
                    break;
                }//~if
            }//~foreach
        }

        private void ConfigureProcessInstallerPassword()
        {
            foreach (string key in new[] {"username", "user"})
            {
                if (this.Context.Parameters.ContainsKey(key))
                {
                    string value = this.Context.Parameters[key];
                    if (!value.IsNullOrEmpty())
                    {
                        _serviceProcessInstaller.Password = value;

                        this.LogMessage(
                            "ServiceProcessInstaller Password set to {0}".FormatStringCurrentUICulture(
                                _serviceProcessInstaller.Password.SafeString(2, false)));
                    }
                    break;
                }//~if
            }//~foreach
        }

        private void ConfigureServiceStartMode()
        {
            foreach (string key in new[] {"starttype", "startuptype", "startmode", "startupmode"})
            {
                if (this.Context.Parameters.ContainsKey(key))
                {
                    string value = this.Context.Parameters[key];

                    if (!value.IsNullOrEmpty())
                    {
                        try
                        {
                            ServiceStartMode serviceStartMode =
                                (ServiceStartMode) Enum.Parse(typeof (ServiceStartMode), value);

                            foreach (ServiceInstaller serviceInstaller in _serviceInstallers)
                            {
                                serviceInstaller.StartType = serviceStartMode;

                                this.LogMessage(
                                    "ServiceStartType of '{0}' set to '{0}'".FormatStringCurrentUICulture(
                                        serviceInstaller.ServiceName, value));
                            }
                        }
                        catch
                        {
                            this.LogMessage(
                                "Failed Setting ServiceStartType of Services to '{0}'".FormatStringCurrentUICulture(
                                    value));
                        }
                    }
                    break;
                }//~if
            }//~foreach
        }


        private void LogMessage(string message)
        {
            if (this.Context==null)
            {
                _cachedMessage.Add(message);
                            }
            else
            {
                if (_cachedMessage!=null)
                {
                    foreach(string msg in _cachedMessage)
                    {
                        this.Context.LogMessage("*" + msg);
                    }
                    this.Context.LogMessage("*" + "Installer Context now available to reference.");
                    _cachedMessage = null;
                }
                this.Context.LogMessage(message); 
            }
        }
        private List<string> _cachedMessage = new List<string>();
    }
}
