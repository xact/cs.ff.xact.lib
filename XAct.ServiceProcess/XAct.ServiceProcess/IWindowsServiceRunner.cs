﻿// ReSharper disable CheckNamespace
namespace XAct.ServiceProcess
// ReSharper restore CheckNamespace
{
    using System.ServiceProcess;

    /// <summary>
    /// Contract for a ochestrator/runner of Windows Services
    /// that allow usage from run services from the console
    /// as well.  
    /// <para>
    /// Usage Example:
    /// <![CDATA[
    /// static void Main(string[] args) {
    ///   NinjectBootstrapper.Initialize();
    ///   ServiceLocatorService.Current
    ///     .GetInstance<ITracingService>().Trace(TraceLevel.Info, "Starting....");
    ///   //Instantiate Service:
    ///   IMyService myService =
    ///     DependencyResolver.Current.GetInstance<IMyService>();
    /// 
    ///   //Services don't start doing work until their OnStart method is invoked.
    ///   //Can use ServiceBase.Run, but use IWindowsServiceRunner to 
    ///   //offer ability to run from console as well:
    ///   IWindowsServiceRunner windowsServiceProcessRunner =
    ///     DependencyResolver.Current.GetInstance<IWindowsServiceRunner>();
    ///   //Runner, depending on Environement.IsUserInteractive, runs quietly, or interacts
    ///   //with the console based on the Environement.IsUserInteractive variable:
    ///   windowsServiceProcessRunner.Execute(myService as ServiceBase);
    /// }
    /// ]]>
    /// </para>
    /// </summary>
    public interface IWindowsServiceRunner : IHasExecutableAction<ServiceBase[]>, IHasTransientBindingScope
    {
        /// <summary>
        /// Invokes the 'OnRun' method of the specified instantiated services.
        /// <para>
        /// Usage Example:
        /// <![CDATA[
        /// static void Main(string[] args) {
        ///   NinjectBootstrapper.Initialize();
        ///   ServiceLocatorService.Current
        ///     .GetInstance<ITracingService>().Trace(TraceLevel.Info, "Starting....");
        ///   //Instantiate Service:
        ///   IMyService myService =
        ///     DependencyResolver.Current.GetInstance<IMyService>();
        /// 
        ///   //Services don't start doing work until their OnStart method is invoked.
        ///   //Can use ServiceBase.Run, but use IWindowsServiceRunner to 
        ///   //offer ability to run from console as well:
        ///   IWindowsServiceRunner windowsServiceProcessRunner =
        ///     DependencyResolver.Current.GetInstance<IWindowsServiceRunner>();
        ///   //Runner, depending on Environement.IsUserInteractive, runs quietly, or interacts
        ///   //with the console based on the Environement.IsUserInteractive variable:
        ///   windowsServiceProcessRunner.Execute(myService as ServiceBase);
        /// }
        /// ]]>
        /// </para>
        /// </summary>
        /// <param name="servicesToRun">The services to run.</param>
        new void Execute(params ServiceBase[] servicesToRun);
    }

}
