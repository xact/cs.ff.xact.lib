﻿namespace XAct.ServiceProcess.Console
{
    using XAct.Diagnostics;
    using XAct.Examples;
    using XAct.Services.IoC;


    class Program
    {
// ReSharper disable UnusedParameter.Local
        static void Main(string[] args)
// ReSharper restore UnusedParameter.Local
        {

            NinjectBootstrapper.Initialize();


            XAct.DependencyResolver.Current
                .GetInstance<ITracingService>().QuickTrace("With Prefix....");


            XAct.DependencyResolver.Current
                .GetInstance<ITracingService>().Trace(TraceLevel.Info, "Starting....");

            //Instantiate Service:
            IMyService myService =
                DependencyResolver.Current.GetInstance<IMyService>();

            //Services don't start doing work until their OnStart method is invoked.
            //Can use ServiceBase.Run, but use IWindowsServiceRunner to 
            //offer ability to run from console as well:
            IWindowsServiceRunner windowsServiceProcessRunner =
                DependencyResolver.Current.GetInstance<IWindowsServiceRunner>();

            //Runner, depending on Environement.IsUserInteractive, runs quietly, or interacts
            //with the console based on the Environement.IsUserInteractive variable:
            windowsServiceProcessRunner.Execute(myService as System.ServiceProcess.ServiceBase);

        }
    }
}
