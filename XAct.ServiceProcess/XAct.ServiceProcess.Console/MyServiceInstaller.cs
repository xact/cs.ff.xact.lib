﻿using System.ServiceProcess;

namespace XAct.ServiceProcess.Console
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// Inherit from XAct.ServiceProcess.InstallerBase
    /// which is preconfigured to look for command line variables
    /// to set the InstallerAccount name, password, startup type.
    /// <para>
    /// Usage example:
    /// <![CDATA[
    /// InstallUtil.exe MyService.exe /AccountType=User /UserName=CORP\SomeOne /Password=Passw0rd1 /StartupType=Automatic
    /// ]]>
    /// </para>
    /// </remarks>
    public class MyServiceInstaller : InstallerBase
    {
        public MyServiceInstaller() : base(
            //Define default settings for AccountType, UserName, Password,
            //that can be overridden by CommandLine if you wish.
            //One per Exe being installed:
            new ServiceProcessInstaller
                {
                    Account=ServiceAccount.User, 
                    Username="HP2\\Svc.AP2", //The account *MUST* have the computer prefix or it will say "name or password is uncorrect". 
                    Password="M0catad1"
                },
            //One per Service being installed:
            new ServiceInstaller{
                StartType =ServiceStartMode.Automatic,
                ServiceName="MyService", //Must match the name given to the service.
                DisplayName = "MyService",
                Description = "..." //This is what will be displayed on the service.
            }
            )
        {
        }
    }
}
