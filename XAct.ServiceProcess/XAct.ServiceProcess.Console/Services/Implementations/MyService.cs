﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using XAct.Diagnostics;
using XAct.Environment;
using XAct.IO;
using XAct.Services;
using XAct.Settings;

namespace XAct.Examples
{
    using XAct.IO.Implementations;

    // Note decoration of our service with DefaultBindingImplementation
    //for it to be included in the autowire up process provided by XActLib.
    public class MyService : System.ServiceProcess.ServiceBase, IMyService
    {
        private bool _run = true;
        private bool _paused;

        private readonly ITracingService _tracingService;
        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;
        private readonly IDirectoryWatcherService _fileSystemWatcherService;
        private readonly IApplicationSettingsService _appSettingsService;
        private readonly IIOService _ioService;

        private readonly IDirectoryWatcher _fileSystemDirectoryWatcherInstance;

        private readonly DirectoryInfo _processedDirectory;


        public MyService(ITracingService tracingService,
            IDateTimeService dateTimeService,
            IEnvironmentService environmentService, IDirectoryWatcherService fileSystemWatcherService, IApplicationSettingsService appSettingsService, IIOService ioService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            environmentService.ValidateIsNotDefault("environmentService");
            fileSystemWatcherService.ValidateIsNotDefault("fileSystemWatcherService");
            appSettingsService.ValidateIsNotDefault("appSettingsService");
            ioService.ValidateIsNotDefault("ioService");


            //VERY IMPORTANT:This value *MUST* match what's defined in the ServiceInstaller.
            this.ServiceName = "MyService";

            _tracingService = tracingService;
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
            _fileSystemWatcherService = fileSystemWatcherService;
            _appSettingsService = appSettingsService;
            _ioService = ioService;

            DirectoryWatcherConfiguration
                fileSystemDirectoryWatcherInstanceConfiguration =
            new DirectoryWatcherConfiguration(); ;



            string folderPath = _appSettingsService.Current.TryGetSettingValue<string>("DropFolderPath", "c:\\Tmp\\DropFolder");


            string directoryInfo = folderPath;
            _processedDirectory = new DirectoryInfo(Path.Combine(directoryInfo, "Processed"));



                _ioService.DirectoryExistsAsync(directoryInfo,true).WaitAndGetResult();

            if (!_processedDirectory.Exists)
            {
                _processedDirectory.Create();
            }

            fileSystemDirectoryWatcherInstanceConfiguration.Enabled = true;
            fileSystemDirectoryWatcherInstanceConfiguration.Path = directoryInfo;
            fileSystemDirectoryWatcherInstanceConfiguration.IncludeSubDirectories = false;
            fileSystemDirectoryWatcherInstanceConfiguration.Filter = "*.*";
            fileSystemDirectoryWatcherInstanceConfiguration.WatcherChangeTypes = WatcherChangeTypes.All; // WatcherChangeTypes.Changed;


            _fileSystemDirectoryWatcherInstance =
           _fileSystemWatcherService.Register(fileSystemDirectoryWatcherInstanceConfiguration);

            //_fileSystemDirectoryWatcherInstance.Changed += new FileSystemEventHandler(_fileSystemDirectoryWatcherInstance_Changed);

            _fileSystemDirectoryWatcherInstance.Event += _fileSystemDirectoryWatcherInstance_Event;
        }


        void _fileSystemDirectoryWatcherInstance_Event(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Deleted)
            {
                return;
            } 
            _tracingService.QuickTrace("FILE '{0}' DETECTED ('{1}')".FormatStringCurrentUICulture(e.ChangeType, e.Name));
            string fileName = e.Name;
            if (e.FullPath == _processedDirectory.FullName)
            {
                return;
            }
            string newFilePath = Path.Combine(_processedDirectory.FullName, fileName);
            //IMPORTANT: As Copy past overs cause 2 changes...have to check first
            //if still there
            if (_ioService.FileExistsAsync(e.FullPath).WaitAndGetResult())
            {
                _ioService.FileCopyAsync(e.FullPath, newFilePath, true).Wait();
            }
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);

            _fileSystemDirectoryWatcherInstance.Enabled = true;

            //A simple fake service:
            while (_run)
            {
                if (!_paused)
                {
                    Console.WriteLine("[{0:t}] MyService Running..."
                        .FormatStringCurrentUICulture(_dateTimeService.NowUTC));
                }
                Thread.Sleep(1000);
            }
            //Exits loop, returning to parent thread...
        }
        protected override void OnStop()
        {
            _fileSystemDirectoryWatcherInstance.Enabled = false;
            _run = false;
            base.OnStop();
        }

        /// <summary>
        /// Add a pause so you can see effect of Pause from WindowsServiceRunner
        /// </summary>
        protected override void OnPause()
        {
            _fileSystemDirectoryWatcherInstance.Enabled = false;
            _paused = true;
            base.OnPause();
        }
        /// <summary>
        /// Add a pause so you can see effect of Pause from WindowsServiceRunner
        /// </summary>
        protected override void OnContinue()
        {
            _fileSystemDirectoryWatcherInstance.Enabled = true;
            _paused = false;
            base.OnContinue();
        }
    }
}
