using System;
using System.Linq;
using XAct.Functional.Specifications.Models;
using XAct.Messages;

namespace XAct.Functional.Specifications.Services
{
    using XAct;

#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public interface ISpecificationService :
        
        IHasXActLibService 
#pragma warning restore 1591
    {
        /// <summary>
        /// Gets the service to manage <see cref="SpecificationSet"/>
        /// to which <see cref="Specification"/>s belong.
        /// </summary>
        ISpecificationSetService SpecificationSetService {get;}

        /// <summary>
        /// Gets the service to manage <see cref="SpecificationState"/>s
        /// to which <see cref="Specification"/>s belong.
        /// </summary>
        ISpecificationStateService StateService{get;}
        
        /// <summary>
        /// Gets the service to manage <see cref="SpecificationCategory"/>s
        /// to which <see cref="Specification"/>s belong.
        /// </summary>
        ISpecificationCategoryService CategoryService { get; }

        /// <summary>
        /// Gets the specification by Id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="includeState">if set to <c>true</c> [include state].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeComments">if set to <c>true</c> [include comments].</param>
        /// <param name="includeChildSpecifications">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        Specification GetSpecification(Guid id, bool includeState = false, bool includeCategory = false,
            bool includeTags = true, bool includeComments=false, bool includeChildSpecifications = false);

        /// <summary>
        /// Persists the specified <see cref="Specification" />.
        /// </summary>
        /// <param name="specification"></param>
        void PersistOnCommit(Specification specification);
        

        /// <summary>
        /// Deletes the specified <see cref="Specification" />.
        /// </summary>
        /// <param name="specification"></param>
        void DeleteOnCommit(Specification specification);



        /// <summary>
        /// Gets the specifications.
        /// </summary>
        /// <param name="optionalSpecificationSetIds">The optional specification set ids.</param>
        /// <param name="optionalFilterByStateIds">The optional filter by state ids.</param>
        /// <param name="optionalFilterByCategoryIds">The optional filter by category ids.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeState">if set to <c>true</c> [include state].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeComments">if set to <c>true</c> [include comments].</param>
        /// <param name="includeChildSpecifications">if set to <c>true</c> [include child specifications].</param>
        /// <returns></returns>
        IQueryable<Specification> GetSpecifications(
            Guid[] optionalSpecificationSetIds,
            Guid[] optionalFilterByStateIds,
            Guid[] optionalFilterByCategoryIds,
            IPagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = true,
            bool includeState = false,
            bool includeCategory = false,
            bool includeTags = false,
            bool includeComments=false,
            bool includeChildSpecifications = false);



        /// <summary>
        /// Gets the specifications by set.
        /// </summary>
        /// <param name="specificationSetId">The specification set identifier.</param>
        /// <param name="optionalFilterByStateIds">The optional filter by state ids.</param>
        /// <param name="optionalFilterByCategoryIds">The optional filter by category ids.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeState">if set to <c>true</c> [include state].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeComments">if set to <c>true</c> [include comments].</param>
        /// <param name="includeChildSpecifications">if set to <c>true</c> [include child specifications].</param>
        /// <returns></returns>
        IQueryable<Specification> GetSpecificationsBySet(
            Guid specificationSetId,
            Guid[] optionalFilterByStateIds,
            Guid[] optionalFilterByCategoryIds,
            IPagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = true,
            bool includeState = false,
            bool includeCategory = false,
            bool includeTags = false,
            bool includeComments = false,
            bool includeChildSpecifications = false);
    }
}
