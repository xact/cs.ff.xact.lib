using System;
using System.Linq;
using XAct.Data.Repositories.Implementations;
using XAct.Diagnostics;
using XAct.Domain.Repositories;
using XAct.Environment;
using XAct.Functional.Specifications.Models;
using XAct.Messages;

namespace XAct.Functional.Specifications.Services.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ISpecificationSetService"/>
    /// to manage 
    /// <see cref="SpecificationSet"/>s.
    /// </summary>
    public class SpecificationSetService :
        ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase<SpecificationSet>, ISpecificationSetService
    {
        private readonly ISpecificationSetStateService _specificationSetStateService;
        private readonly ISpecificationSetCategoryService _specificationSetCategoryService;

        /// <summary>
        /// Gets the service to manage <see cref="SpecificationSetState"/>s
        /// to which <see cref="SpecificationSet"/>s belong.
        /// </summary>
        public ISpecificationSetStateService States { get { return _specificationSetStateService; } }

        /// <summary>
        /// Gets the service to manage <see cref="SpecificationSetCategory"/>s
        /// to which <see cref="SpecificationSet"/>s belong.
        /// </summary>
        public ISpecificationSetCategoryService Categories { get { return _specificationSetCategoryService; } }


        /// <summary>
        /// Gets the application tennant identifier.
        /// </summary>
        /// <value>
        /// The application tennant identifier.
        /// </value>
        private Guid ApplicationTennantId
        {
            get { return _applicationTennantService.Get(); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationSetService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="specificationSetStateService">The specification set state service.</param>
        /// <param name="specificationSetCategoryService">The specification set category service.</param>
        public SpecificationSetService(ITracingService tracingService,
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService,
            ISpecificationSetStateService  specificationSetStateService,
            ISpecificationSetCategoryService specificationSetCategoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {
            _specificationSetStateService = specificationSetStateService;
            _specificationSetCategoryService = specificationSetCategoryService;
        }


        /// <summary>
        /// Retrieves the specified <see cref="SpecificationSet"/>.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <returns></returns>
        public IQueryable<SpecificationSet> Retrieve(PagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = false)
        {
            var applicationTennantId = _applicationTennantService.Get();
            return includeDisabled
                ? Retrieve(x => x.ApplicationTennantId == applicationTennantId, null, pagedQuerySpecification)
                : Retrieve(x => x.ApplicationTennantId == applicationTennantId && (x.Enabled == true), null, pagedQuerySpecification);

        }

    }
}