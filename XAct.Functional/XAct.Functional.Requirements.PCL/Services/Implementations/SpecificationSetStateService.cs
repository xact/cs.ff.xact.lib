﻿using System.Linq;
using XAct.Data.Repositories.Implementations;
using XAct.Diagnostics;
using XAct.Domain.Repositories;
using XAct.Environment;
using XAct.Functional.Specifications.Models;
using XAct.Functional.Specifications.Services;
using XAct.Messages;

namespace XAct.Functional.SpecificationSets.Services.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ISpecificationSetStateService"/>
    /// to retrieve <see cref="SpecificationSetState"/> elements.
    /// </summary>
    public class SpecificationSetStateService :
        ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase<SpecificationSetState>,
        ISpecificationSetStateService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationSetStateService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public SpecificationSetStateService(ITracingService tracingService,
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {


        }

        /// <summary>
        /// Retrieves the specified <see cref="SpecificationSetState"/>.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <returns></returns>
        public IQueryable<SpecificationSetState> Retrieve(PagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = false)
        {
            var applicationTennantId = _applicationTennantService.Get();
            return includeDisabled
                ? Retrieve(x => x.ApplicationTennantId == applicationTennantId, null, pagedQuerySpecification)
                : Retrieve(x => x.ApplicationTennantId == applicationTennantId && (x.Enabled == true), null, pagedQuerySpecification);

        }

    }
}