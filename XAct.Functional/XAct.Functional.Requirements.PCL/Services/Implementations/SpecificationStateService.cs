﻿using System.Linq;
using XAct.Data.Repositories.Implementations;
using XAct.Diagnostics;
using XAct.Domain.Repositories;
using XAct.Environment;
using XAct.Functional.Specifications.Models;
using XAct.Messages;

namespace XAct.Functional.Specifications.Services.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ISpecificationStateService"/>
    /// to retrieve <see cref="SpecificationState"/> elements.
    /// </summary>
    public class SpecificationStateService :
        ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase<SpecificationState>,
        ISpecificationStateService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationStateService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public SpecificationStateService(ITracingService tracingService,
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {


        }

        /// <summary>
        /// Retrieves the specified <see cref="SpecificationSet"/>.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <returns></returns>
        public IQueryable<SpecificationState> Retrieve(PagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = false)
        {
            var applicationTennantId = _applicationTennantService.Get();
            return includeDisabled
                ? Retrieve(x => x.ApplicationTennantId == applicationTennantId, null, pagedQuerySpecification)
                : Retrieve(x => x.ApplicationTennantId == applicationTennantId && (x.Enabled == true), null, pagedQuerySpecification);

        }

    }
}