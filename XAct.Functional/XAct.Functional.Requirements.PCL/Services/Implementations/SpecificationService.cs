using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using XAct.Domain.Repositories;
using XAct.Environment;
using XAct.Expressions;
using XAct.Functional.Specifications.Models;
using XAct.Messages;

namespace XAct.Functional.Specifications.Services.Implementations
{
    using XAct;
    using XAct.Services;

#pragma warning disable 1591
    public class SpecificationService :ISpecificationService
#pragma warning restore 1591
    {
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;
        private readonly ISpecificationSetService _specificationSetService;
        private readonly ISpecificationStateService _specificationStateService;
        private readonly ISpecificationCategoryService _specificationCategoryService;

        /// <summary>
        /// Gets the service to manage <see cref="SpecificationSet"/>
        /// to which <see cref="Specification"/>s belong.
        /// </summary>
        public ISpecificationSetService SpecificationSetService { get { return _specificationSetService; } }

        /// <summary>
        /// Gets the service to manage <see cref="SpecificationState"/>s
        /// to which <see cref="Specification"/>s belong.
        /// </summary>
        public ISpecificationStateService StateService { get { return _specificationStateService; } }

        /// <summary>
        /// Gets the service to manage <see cref="SpecificationCategory"/>s
        /// to which <see cref="Specification"/>s belong.
        /// </summary>
        public ISpecificationCategoryService CategoryService { get { return _specificationCategoryService; } }




        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationService" /> class.
        /// </summary>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="specificationSetService">The specification set service.</param>
        /// <param name="specificationStateService">The specification state service.</param>
        /// <param name="specificationCategoryService">The specification category service.</param>
        public SpecificationService(
            IApplicationTennantService applicationTennantService, 
            IRepositoryService repositoryService,
            ISpecificationSetService specificationSetService,
            ISpecificationStateService specificationStateService,
            ISpecificationCategoryService specificationCategoryService)
        {
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
            _specificationSetService = specificationSetService;
            _specificationStateService = specificationStateService;
            _specificationCategoryService = specificationCategoryService;
        }









        /// <summary>
        /// Gets the specification by Id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="includeState">if set to <c>true</c> [include state].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeComments">if set to <c>true</c> [include comments].</param>
        /// <param name="includeChildSpecifications">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        public Specification GetSpecification(Guid id, bool includeState=false, bool includeCategory = false, bool includeTags = false,  bool includeComments=false, bool includeChildSpecifications = false)
        {
            var applicationTennantId = _applicationTennantService.Get();

            var result =
                _repositoryService.GetSingle<Specification>(
                    x => x.ApplicationTennantId == applicationTennantId && x.Id == id,
                    CreateIncludeSpecification(
                    includeState:includeState,
                    includeCategory:includeCategory,
                    includeTags:includeTags,
                    includeComments:includeComments,
                    includeChildSpecifications:includeChildSpecifications)
                    );

            return result;
        }

        /// <summary>
        /// Persists the specified <see cref="Specification" />.
        /// </summary>
        /// <param name="specification"></param>
        public void PersistOnCommit(Specification specification)
        {
            _repositoryService.PersistOnCommit<Specification, Guid>(specification, true);
        }

        /// <summary>
        /// Deletes the specified <see cref="Specification" />.
        /// </summary>
        /// <param name="specification"></param>
        public void DeleteOnCommit(Specification specification)
        {
            _repositoryService.DeleteOnCommit(specification);
        }








        /// <summary>
        /// Gets the Specifications in the current Application Tennant.
        /// </summary>
        /// <param name="specificationSetId">The project identifier.</param>
        /// <param name="optionalFilterByStateIds">The optional filter by state ids.</param>
        /// <param name="optionalFilterByCategoryIds">The optional filter by category ids.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeState">if set to <c>true</c> [include state].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeComments">if set to <c>true</c> [include comments].</param>
        /// <param name="includeChildSpecifications">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        public IQueryable<Specification> GetSpecificationsBySet(Guid specificationSetId,
            Guid[] optionalFilterByStateIds,
            Guid[] optionalFilterByCategoryIds,
            IPagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = true,
            bool includeState = false,
            bool includeCategory = false,
            bool includeTags = false,
            bool includeComments = false,
            bool includeChildSpecifications = false)
        {
            specificationSetId.ValidateIsNotDefault("specificationSetId");

            return GetSpecifications(new Guid[] { specificationSetId }, optionalFilterByStateIds, optionalFilterByCategoryIds, pagedQuerySpecification, includeDisabled, includeState, includeCategory, includeTags, includeComments, includeChildSpecifications);
        }

        /// <summary>
        /// Gets the specifications.
        /// </summary>
        /// <param name="optionalSpecificationSetIds">The optional specification set ids.</param>
        /// <param name="optionalFilterByStateIds">The optional filter by state ids.</param>
        /// <param name="optionalFilterByCategoryIds">The optional filter by category ids.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeState">if set to <c>true</c> [include state].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeComments">if set to <c>true</c> [include comments].</param>
        /// <param name="includeChildSpecifications">if set to <c>true</c> [include child specifications].</param>
        /// <returns></returns>
        public IQueryable<Specification> GetSpecifications(
            Guid[] optionalSpecificationSetIds,
            Guid[] optionalFilterByStateIds,
            Guid[] optionalFilterByCategoryIds,
            IPagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = true,
            bool includeState = false,
            bool includeCategory = false,
            bool includeTags = false,
            bool includeComments = false,
            bool includeChildSpecifications = false)
        {
            


            return InternalGetSpecifications(
                optionalSpecificationSetIds,
                optionalFilterByStateIds,
                optionalFilterByCategoryIds,
                pagedQuerySpecification,
                includeDisabled: includeDisabled,
                includeState:includeState,
                includeCategory: includeCategory,
                includeTags: includeTags,
                includeComments:includeComments,
                includeChildSpecifications: includeChildSpecifications
                );

        }








        private IQueryable<Specification> InternalGetSpecifications(
            Guid[] specificationSetIds,
            Guid[] optionalStateIds,
            Guid[] optionalCategoryIds,
            IPagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = true,
            bool includeState = false,
            bool includeCategory = false,
            bool includeTags = false,
            bool includeComments = false,
            bool includeChildSpecifications = false)
        {
            var applicationTennantId = _applicationTennantService.Get();



            var predicate = PredicateBuilder.True<Specification>();

            //Always include filter by applicationHost:
            predicate = predicate.And<Specification>(x => (x.ApplicationTennantId == applicationTennantId));



            if ((specificationSetIds != null) && (specificationSetIds.Length > 0))
            {
                if (specificationSetIds.Length == 0)
                {
                    Guid tmp = specificationSetIds[0];

                    predicate = predicate.And<Specification>(x => x.SpecificationSetFK == tmp);
                }
                else
                {
                    predicate = predicate.And<Specification>(x => specificationSetIds.Contains(x.SpecificationSetFK));
                }
            }



            if ((optionalStateIds != null) && (optionalStateIds.Length > 0))
            {
                if (optionalStateIds.Length == 1)
                {
                    //faster than an IN statement:
                    Guid tmp = optionalStateIds[0];
                    predicate = predicate.And<Specification>(x => x.StateFK == tmp);
                }
                else
                {
                    //Use an IN statement:
                    predicate = predicate.And<Specification>(x => (optionalStateIds.Contains(x.StateFK)));
                }
            }


            if ((optionalCategoryIds != null) && (optionalCategoryIds.Length > 0))
            {
                if (optionalCategoryIds.Length == 1)
                {
                    //faster than an IN statement:
                    Guid tmp = optionalCategoryIds[0];
                    predicate = predicate.And<Specification>(x => x.CategoryFK == tmp);
                }
                else
                {
                    //Use an IN statement:
                    predicate = predicate.And<Specification>(x => (optionalCategoryIds.Contains(x.CategoryFK)));
                }
            }

            //if (tags!=null)&& (tags.Length>0)
            //{
            //    predicate.And<DistributedSpecification>(x => ((x.Tags.Any(tags() == categoryId);
            //}


            //I think we always want to return disabled -- just have to watch out for Sum() conditions.
            if (!includeDisabled)
            {
                predicate = predicate.And<Specification>(x => x.Enabled == true);
            }

            //if (taskType.HasValue)
            //{
            //    predicate = predicate.And<Specification>(x => x.Type == taskType.Value);
            //}


            var includeSpecification = CreateIncludeSpecification(
                includeState : includeState,
                includeCategory: includeCategory,
                includeTags: includeTags,
                includeComments:includeComments,
                includeChildSpecifications: includeChildSpecifications);


            var result =
                _repositoryService.GetByFilter<Specification>(
                    predicate,
                    includeSpecification,
                    pagedQuerySpecification
                    );


            return result;
        }

        private static IncludeSpecification<Specification> CreateIncludeSpecification(
            bool includeState,
            bool includeCategory,
            bool includeTags,
            bool includeComments,
            bool includeChildSpecifications)
        {

            var includesList = new List<Expression<Func<Specification, object>>>();

            if (includeState)
            {
                includesList.Add(x => x.State);
            }

            if (includeCategory)
            {
                includesList.Add(x => x.Category);
            }

            //if (includeTags)
            //{
            //    includesList.Add(x => x.Tags);
            //}


            if (includeComments)
            {
                includesList.Add(x => x.Comments);
            }


            if (includeChildSpecifications)
            {
                includesList.Add(x => x.Children);
            }

            var includeSpecification = new IncludeSpecification<Specification>(includesList.ToArray());

            return includeSpecification;
        }
    }
}
