﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XAct.Data.Repositories.Implementations;
using XAct.Diagnostics;
using XAct.Domain.Repositories;
using XAct.Environment;
using XAct.Functional.Specifications.Models;
using XAct.Messages;

namespace XAct.Functional.Specifications.Services.Implementations
{
 




        /// <summary>
        /// An implementation of the <see cref="ISpecificationCategoryService"/>
        /// to retrieve <see cref="SpecificationCategory"/> elements.
        /// </summary>
        public class SpecificationCategoryService :
            ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase<SpecificationCategory>,
            ISpecificationCategoryService
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="SpecificationCategoryService" /> class.
            /// </summary>
            /// <param name="tracingService">The tracing service.</param>
            /// <param name="applicationTennantService">The application tennant service.</param>
            /// <param name="repositoryService">The repository service.</param>
            public SpecificationCategoryService(ITracingService tracingService,
                IApplicationTennantService applicationTennantService,
                IRepositoryService repositoryService)
                : base(tracingService, applicationTennantService, repositoryService)
            {

            }

            /// <summary>
            /// Retrieves the specified <see cref="SpecificationCategory"/>.
            /// </summary>
            /// <param name="pagedQuerySpecification">The paged query specification.</param>
            /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
            /// <returns></returns>
            public IQueryable<SpecificationCategory> Retrieve(PagedQuerySpecification pagedQuerySpecification,
                bool includeDisabled = false)
            {
                var applicationTennantId = _applicationTennantService.Get();
                return includeDisabled
                    ? Retrieve(x => x.ApplicationTennantId == applicationTennantId, null, pagedQuerySpecification)
                    : Retrieve(x => x.ApplicationTennantId == applicationTennantId && (x.Enabled == true), null, pagedQuerySpecification);
            }
        }
}

