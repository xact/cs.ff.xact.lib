using System;
using System.Linq;
using XAct.Domain.Repositories;
using XAct.Functional.Specifications.Models;
using XAct.Messages;

namespace XAct.Functional.Specifications.Services
{
    /// <summary>
    /// Contract for a service to manage <see cref="SpecificationSetState"/> elements.
    /// </summary>
    public interface ISpecificationSetStateService : IApplicationTennantIdSpecificReferenceDataRepository<SpecificationSetState, Guid>, IHasXActLibService
    {
        /// <summary>
        /// Retrieves the specified <see cref="SpecificationSet"/>.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <returns></returns>
        IQueryable<SpecificationSetState> Retrieve(PagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = false);

    }
}