﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XAct.Domain.Repositories;
using XAct.Functional.Specifications.Models;
using XAct.Messages;

namespace XAct.Functional.Specifications.Services
{
    /// <summary>
    /// Contract for a service to manage <see cref="SpecificationState"/> elements.
    /// </summary>
    public interface ISpecificationStateService : IApplicationTennantIdSpecificReferenceDataRepository<SpecificationState, Guid>, IHasXActLibService
    {
        /// <summary>
        /// Retrieves the specified <see cref="SpecificationSet"/>.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <returns></returns>
        IQueryable<SpecificationState> Retrieve(PagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = false);

    }
}
