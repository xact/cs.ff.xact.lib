﻿using System;
using System.Linq;
using XAct.Domain.Repositories;
using XAct.Functional.Specifications.Models;
using XAct.Messages;

namespace XAct.Functional.Specifications.Services
{
    /// <summary>
    /// Contract for managing 
    /// <see cref="SpecificationSet"/>
    /// </summary>
    public interface ISpecificationSetService : IApplicationTennantIdSpecificRepository<SpecificationSet, Guid>, IHasXActLibService
    {


        /// <summary>
        /// Gets the service to manage <see cref="SpecificationSetState"/>s
        /// to which <see cref="SpecificationSet"/>s belong.
        /// </summary>
        ISpecificationSetStateService States { get; }

        /// <summary>
        /// Gets the service to manage <see cref="SpecificationSetCategory"/>s
        /// to which <see cref="SpecificationSet"/>s belong.
        /// </summary>
        ISpecificationSetCategoryService Categories { get; }


        /// <summary>
        /// Retrieves the specified <see cref="SpecificationSet"/>.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <returns></returns>
        IQueryable<SpecificationSet> Retrieve(PagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = false);

    }
}