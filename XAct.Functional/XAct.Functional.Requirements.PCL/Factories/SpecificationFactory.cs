using System;
using XAct.Environment;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Factories
{
    /// <summary>
    /// A factory for creating a <see cref="SpecificationSetState" />
    /// </summary>
    public class SpecificationFactory
    {
        /// <summary>
        /// Creates a <see cref="Specification" />.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="description">The description.</param>
        /// <param name="specificationSet">The specification set.</param>
        /// <param name="stateId">The state identifier.</param>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <returns></returns>
        public static Specification Create(string text, string description, Guid specificationSet, Guid stateId, Guid categoryId,  bool enabled = true, IApplicationTennantService applicationTennantService = null)
        {
            IApplicationTennantService applicationTennantService2 =
                applicationTennantService ?? XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>();

            var x2 = new Specification
            {
                ApplicationTennantId = applicationTennantService2.Get(),
                Enabled = enabled,
                SpecificationSetFK = specificationSet,
                StateFK = stateId,
                CategoryFK = categoryId,
                Subject = text,
                Body = description,
            };
            return x2;
        }

        /// <summary>
        /// Creates a <see cref="Specification" />.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="text">The text.</param>
        /// <param name="description">The description.</param>
        /// <param name="specificationSet">The specification set.</param>
        /// <param name="state">The state.</param>
        /// <param name="category">The category.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <returns></returns>
        public static Specification Create(string code, string text, string description, SpecificationSet specificationSet, SpecificationState state, SpecificationCategory category, bool enabled = true, IApplicationTennantService applicationTennantService = null)
        {
            IApplicationTennantService applicationTennantService2 =
                applicationTennantService ?? XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>();

            var x2 = new Specification
            {
                ApplicationTennantId = applicationTennantService2.Get(),
                Enabled = enabled,
                SpecificationSet = specificationSet,
                State = state,
                Category =  category,
                Code = code,
                Subject = text,
                Body = description,

            };
            return x2;
        }

    }
}