using System;
using XAct.Environment;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Factories
{
    /// <summary>
    /// A factory for creating a <see cref="SpecificationState"/>
    /// </summary>
    public class SpecificationSetFactory
    {
        /// <summary>
        /// Creates a <see cref="SpecificationSet" />.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="description">The description.</param>
        /// <param name="stateId">The state identifier.</param>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <returns></returns>
        public static SpecificationSet Create(string text, string description, Guid stateId, Guid categoryId, bool enabled = true, IApplicationTennantService applicationTennantService = null)
        {
            IApplicationTennantService applicationTennantService2 =
                applicationTennantService ?? XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>();

            var x2 = new SpecificationSet
            {
                ApplicationTennantId = applicationTennantService2.Get(),
                Enabled = enabled,
                StateFK = stateId,
                CategoryFK = categoryId,
                Subject = text,
                Body = description,
            };
            return x2;
        }


        /// <summary>
        /// Creates a <see cref="SpecificationSet" />.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="description">The description.</param>
        /// <param name="state">The state.</param>
        /// <param name="category">The category.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <returns></returns>
        public static SpecificationSet Create(string text, string description, SpecificationSetState state, SpecificationSetCategory category, bool enabled = true, IApplicationTennantService applicationTennantService = null)
        {
            IApplicationTennantService applicationTennantService2 =
                applicationTennantService ?? XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>();

            var x2 = new SpecificationSet
            {
                ApplicationTennantId = applicationTennantService2.Get(),
                Enabled = enabled,
                State = state,
                Category = category,
                Subject = text,
                Body = description,

            };
            return x2;
        }


    }
}