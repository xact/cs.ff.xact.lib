using XAct.Environment;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Factories
{

    /// <summary>
    /// A factory for creating a <see cref="SpecificationComment" />
    /// </summary>
    public class SpecificationCommentFactory
    {
        /// <summary>
        /// Creates a <see cref="SpecificationComment" />.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <returns></returns>
        public static SpecificationComment Create(string text, bool enabled = true,
            IPrincipalService principalService=null, IDateTimeService dateTimeService = null)
        {
            if (principalService == null)
            {
                principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();
            }

            if (dateTimeService == null)
            {
                dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            }

            var x2 = new SpecificationComment
            {
                Enabled = enabled,
                Note = text,
                CreatedBy = principalService.CurrentIdentityIdentifier,
                CreatedOnUtc = dateTimeService.NowUTC
            };

            return x2;

        }
        /// <summary>
        /// Creates a <see cref="SpecificationComment" />.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="by">The by.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <returns></returns>
        public static SpecificationComment Create(string text, string by, bool enabled = true,
            IPrincipalService principalService=null, IDateTimeService dateTimeService = null)
        {

            if (dateTimeService == null)
            {
                dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            }

            var x2 = new SpecificationComment
            {
                Enabled = enabled,
                Note = text,
                CreatedBy = principalService.CurrentIdentityIdentifier,
                CreatedOnUtc = dateTimeService.NowUTC
            };

            return x2;

        }
    }
}