using XAct.Environment;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Factories
{
    /// <summary>
    /// A factory for creating a <see cref="SpecificationSetState" />
    /// </summary>
    public class SpecificationSetCategoryFactory
    {
        /// <summary>
        /// Creates a <see cref="SpecificationSetCategory"/>.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="description">The description.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <returns></returns>
        public static SpecificationSetCategory Create(string text, string description, bool enabled = true, IApplicationTennantService applicationTennantService = null)
        {
            IApplicationTennantService applicationTennantService2 =
                applicationTennantService ?? XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>();

            var x2 = new SpecificationSetCategory
            {
                ApplicationTennantId = applicationTennantService2.Get(),
                Enabled = enabled,
                Text = text,
                Description = description,
            };
            return x2;
        }

    }
}