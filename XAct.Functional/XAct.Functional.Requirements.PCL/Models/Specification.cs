﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace XAct.Functional.Specifications.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Specification : IHasXActLibEntity, IHasApplicationTennantIdSpecificDistributedGuidId, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, IHasEnabled, IHasOrder, IHasParentFK<Guid?>, IHasHierarchyCollection<Specification>, IHasCode, IHasSubjectAndBody
    {
        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasIdReadOnly{TId}" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the application tennant identifier.
        /// </summary>
        /// <value>
        /// The application tennant identifier.
        /// </value>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember]
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets or sets the requirementset this requirement belongs to.
        /// </summary>
        /// <value>
        /// The requirement set fk.
        /// </value>
        [DataMember]
        public virtual Guid SpecificationSetFK { get; set; }
        /// <summary>
        /// Gets or sets the requirementset this requirement belongs to.
        /// </summary>
        /// <value>
        /// The requirement set.
        /// </value>
        [DataMember]
        public virtual SpecificationSet SpecificationSet { get; set; }


        /// <summary>
        /// The unique textual code (eg: 'X123') for this object.
        /// </summary>
        [DataMember]
        public virtual string Code { get; set; }



        /// <summary>
        /// Gets or sets the FK to the <see cref="SpecificationState"/>.
        /// </summary>
        [DataMember]
        public Guid StateFK { get; set; }

        /// <summary>
        /// Gets or sets this Specification's Status.
        /// </summary>
        [DataMember]
        public  SpecificationState State { get; set; }


        /// <summary>
        /// Gets or sets the FK to the <see cref="SpecificationCategory"/>.
        /// </summary>
        [DataMember]
        public Guid CategoryFK { get; set; }

        /// <summary>
        /// Gets or sets this Specification's Category.
        /// </summary>
        [DataMember]
        public SpecificationCategory Category { get; set; }


        /// <summary>
        /// Gets or sets the message subject.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Body { get; set; }


        /// <summary>
        /// Gets or sets the FK of the parent Guid.
        /// </summary>
        /// <value>
        /// The parent fk.
        /// </value>
        [DataMember]
        public virtual Guid? ParentFK { get; set; }

        /// <summary>
        /// Gets the parent item if any.
        /// <para>Member defined in<see cref="IHasHierarchyCollection{T}" /></para>
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        [DataMember]
        public virtual Specification Parent { get; set; }

        /// <summary>
        /// Gets the collection of children Requirements.
        /// <para>Member defined in<see cref="IHasHierarchyCollection{T}" /></para>
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        [DataMember]
        public virtual ICollection<Specification> Children { get { return _children ?? (_children = new Collection<Specification>()); } }
        private ICollection<Specification> _children;

        /// <summary>
        /// Gets the comments.
        /// </summary>
        /// <value>
        /// The comments.
        /// </value>
        [DataMember]
        public virtual ICollection<SpecificationComment> Comments { get { return _comments ?? (_comments = new Collection<SpecificationComment>()); } }
        private ICollection<SpecificationComment> _comments;

        /// <summary>
        /// Initializes a new instance of the <see cref="Specification"/> class.
        /// </summary>
        public Specification()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }


    }
}