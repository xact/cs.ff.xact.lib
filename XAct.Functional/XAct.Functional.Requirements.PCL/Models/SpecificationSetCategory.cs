using System.Runtime.Serialization;
using System.Threading.Tasks;
using XAct.Messages;

namespace XAct.Functional.Specifications.Models
{
    /// <summary>
    /// Categorisation of <see cref="Task"/>s.
    /// <para>
    /// In a Kanban board this would ba akin to the Task's Colour/Category.
    /// </para>
    /// </summary>
    [DataContract]
    public class SpecificationSetCategory : ApplicationTennantIdSpecificReferenceDataGuidIdBase
    {



        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationCategory"/> class.
        /// </summary>
        public SpecificationSetCategory()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }

    }
}