using System;
using System.Runtime.Serialization;

namespace XAct.Functional.Specifications.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SpecificationComment : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasEnabled, IHasNote, IHasDateTimeCreatedBy, IHasOwnerFK<Guid>
    {

        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasIdReadOnly{TId}" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the owner fk.
        /// </summary>
        /// <value>
        /// The owner fk.
        /// </value>
        [DataMember]
        public virtual Guid OwnerFK { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }
        
        /// <summary>
        /// Gets or sets the note.
        /// <para>
        /// Member defined in the <see cref="IHasNote" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The note.
        /// </value>
        [DataMember]
        public virtual string Note { get; set; }
        
        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        /// </internal>
        /// <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        /// </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }
        
        /// <summary>
        /// Gets or sets the who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy" /></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public virtual string CreatedBy { get; set; }




        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationComment"/> class.
        /// </summary>
        public SpecificationComment()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }

    }
}