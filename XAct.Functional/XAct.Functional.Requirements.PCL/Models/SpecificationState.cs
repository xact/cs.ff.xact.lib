using System.Runtime.Serialization;
using XAct.Messages;

namespace XAct.Functional.Specifications.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SpecificationState : ApplicationTennantIdSpecificReferenceDataGuidIdBase
    {



        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationCategory"/> class.
        /// </summary>
        public SpecificationState()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }

    }
}