﻿using System;
using System.Runtime.Serialization;

namespace XAct.Functional.Specifications.Models
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SpecificationSet : IHasXActLibEntity, IHasApplicationTennantIdSpecificDistributedGuidId, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, IHasEnabled, IHasSubjectAndBody
    {
        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasIdReadOnly{TId}" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the application tennant identifier.
        /// </summary>
        /// <value>
        /// The application tennant identifier.
        /// </value>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember]
        public virtual int Order { get; set; }




        /// <summary>
        /// Gets or sets the FK to the <see cref="SpecificationState"/>.
        /// </summary>
        [DataMember]
        public virtual Guid StateFK { get; set; }

        /// <summary>
        /// Gets or sets this Specification's Status.
        /// </summary>
        [DataMember]
        public virtual SpecificationSetState State { get; set; }


        /// <summary>
        /// Gets or sets the FK to the <see cref="SpecificationCategory"/>.
        /// </summary>
        [DataMember]
        public virtual Guid CategoryFK { get; set; }

        /// <summary>
        /// Gets or sets this Specification's Category.
        /// </summary>
        [DataMember]
        public virtual SpecificationSetCategory Category { get; set; }



        /// <summary>
        /// Gets or sets the message subject.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Body { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationSet"/> class.
        /// </summary>
        public SpecificationSet()
        {
            this.GenerateDistributedId();

            this.Enabled = true;
        }
    }
}
