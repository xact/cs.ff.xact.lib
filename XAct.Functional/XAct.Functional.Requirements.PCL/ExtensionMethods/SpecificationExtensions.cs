﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XAct.Environment;
using XAct.Functional.Specifications.Factories;
using XAct.Functional.Specifications.Models;

namespace XAct
{
    /// <summary>
    /// Extensions to the <see cref="Specification"/> object.
    /// </summary>
    public static class SpecificationExtensions
    {
        /// <summary>
        /// Adds a comment (with current datetimeUtc and Principal identifier)
        /// to the current <see cref="Specification"/>
        /// </summary>
        /// <returns></returns>
        public static SpecificationComment AddComment(this Specification specification, string text,
            IPrincipalService principalService = null, IDateTimeService dateTimeService = null)
        {
            if (principalService == null)
            {
                principalService =
                    XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();
            }


            var result =
                SpecificationCommentFactory.Create(
                    text,
                    true,
                    principalService,
                    dateTimeService
                    );
            return result;
        }
    }
}
