using System;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using XAct.Domain.Repositories;
using XAct.Environment;
using XAct.Functional.Specifications.Factories;
using XAct.Functional.Specifications.Models;
using XAct.Functional.Specifications.Services;
using XAct.Functional.Specifications.Services.Implementations;
using XAct.Messages;
using XAct.Tests;

namespace XAct.Functional.Specifications.Tests
{
    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SpecificationCategoryServiceUnitTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void Can_Get_SpecificationCategoryService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryService>();
            Assert.IsNotNull(service);
        }

        [Test]
        public void Can_Get_SpecificationCategoryService_Of_Expected_Instance_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryService>();
            Assert.AreEqual(typeof(SpecificationCategoryService), service.GetType());
        }


        //------------------------------------------------------------
        //------------------------------------------------------------
        //------------------------------------------------------------





        [Test]
        public void Can_Get_SpecificationCategory_State_By_Id()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryService>();

            var result = service.GetById(1.ToGuid());

            Assert.IsNotNull(result);
        }


        [Test]
        public void Can_Not_Get_SpecificationCategory_State_By_Id_If_Different_App_Tennant()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryService>();

            var result = service.GetById(2.ToGuid());

            //Should be null, if it belongs to different App Tennant
            Assert.IsNull(result);
        }


        [Test]
        public void Can_Get_SpecificationCategory_States()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryService>();

            var result = service.Retrieve(new PagedQuerySpecification()).ToArray();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
            //THey should all be this tennantId:
            Assert.IsTrue(result.All(x => x.ApplicationTennantId == result.First().ApplicationTennantId));
            //Yup. This one...
            Assert.IsTrue(
                result.All(
                    x =>
                        x.ApplicationTennantId ==
                        XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>().Get()));
        }

        [Test]
        public void Can_Persist_New_Record()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryService>();
            var x = SpecificationCategoryFactory.Create("Foo Text", "Foo Description");
            service.PersistOnCommit(x);
            var x2 = SpecificationCategoryFactory.Create("Foo Text2",null);
            service.PersistOnCommit(x2);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            Assert.IsTrue(x2.Timestamp!=null);
        }

        [Test]
        public void Can_Persist_And_Retrieve_New_Record()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryService>();
            var x = SpecificationCategoryFactory.Create("Foo Text3", "Foo Description");
            var id = x.Id;
            service.PersistOnCommit(x);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            var r = service.GetById(id);

            Assert.IsTrue(r!=null);
        }
        [Test]
        [ExpectedException]
        public void Cannot_Persist_Record_With_Wrong_Guid()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryService>();
            var x = SpecificationCategoryFactory.Create("Foo Text3", "Foo Description");
            var id = x.Id;
            x.ApplicationTennantId = 2.ToGuid();
            service.PersistOnCommit(x);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

        }

    }

}