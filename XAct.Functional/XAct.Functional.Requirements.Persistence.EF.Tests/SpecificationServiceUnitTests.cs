using System.Diagnostics;
using XAct.Domain.Repositories;
using XAct.Functional.Specifications.Factories;
using XAct.Functional.Specifications.Services;
using XAct.Functional.Specifications.Services.Implementations;
using XAct.Tests;

namespace XAct.Functional.Specifications.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Messages;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SpecificationServiceUnitTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        [Test]
        public void Can_Get_SpecificationService()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();
            Assert.IsNotNull(service);
        }

        [Test]
        public void Can_Get_SpecificationService_Of_Expected_Instance_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();
            Assert.AreEqual(typeof(SpecificationService), service.GetType());
        }




        [Test]
        public void Can_Get_Specification_By_Id()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var result = service.GetSpecification(1.ToGuid());

            Assert.IsNotNull(result);
        }



        [Test]
        public void Can_Not_Get_Specification_By_Id_If_Different_App_Tennant()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var result = service.GetSpecification(2.ToGuid());

            //Should be null, if it belongs to different App Tennant
            Assert.IsNull(result);
        }


        [Test]
        public void Can_Get_Specifications()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var result = service.GetSpecifications(null,null,null, new PagedQuerySpecification()).ToArray();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }



        [Test]
        public void Can_Persist_Record()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var set = service.SpecificationSetService.GetById(1.ToGuid());
            var state = service.StateService.GetById(1.ToGuid());
            var category = service.CategoryService.GetById(1.ToGuid());

            var x = SpecificationFactory.Create("REQ1234","Foo Text3", "Foo Description",set,state,category);
            var id = x.Id;
            service.PersistOnCommit(x);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

        }


        [Test]
        [ExpectedException]
        public void Cannot_Persist_Record_With_Wrong_Guid()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var set = service.SpecificationSetService.GetById(1.ToGuid());
            var state = service.StateService.GetById(1.ToGuid());
            var category = service.CategoryService.GetById(1.ToGuid());

            var x = SpecificationFactory.Create("REQ05", "Foo Text3", "Foo Description", set, state, category);
            var id = x.Id;
            x.ApplicationTennantId = 2.ToGuid();
            service.PersistOnCommit(x);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

        }


        [Test]
        [ExpectedException]
        public void Cannot_Persist_Record_With_Wrong_Set()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var set = service.SpecificationSetService.GetById(2.ToGuid());
            var state = service.StateService.GetById(1.ToGuid());
            var category = service.CategoryService.GetById(1.ToGuid());

            var x = SpecificationFactory.Create("REQ01", "Foo Text3", "Foo Description", set, state, category);
            var id = x.Id;
            service.PersistOnCommit(x);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

        }


        [Test]
        [ExpectedException]
        public void Cannot_Persist_Record_With_Wrong_State()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var set = service.SpecificationSetService.GetById(1.ToGuid());
            var state = service.StateService.GetById(2.ToGuid());
            var category = service.CategoryService.GetById(1.ToGuid());

            var x = SpecificationFactory.Create("REQ02", "Foo Text3", "Foo Description", set, state, category);
            var id = x.Id;
            service.PersistOnCommit(x);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

        }




        [Test]
        [ExpectedException]
        public void Cannot_Persist_Record_With_Wrong_Category()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var set = service.SpecificationSetService.GetById(1.ToGuid());
            var state = service.StateService.GetById(1.ToGuid());
            var category = service.CategoryService.GetById(2.ToGuid());

            var x = SpecificationFactory.Create("REQ03", "Foo Text3", "Foo Description", set, state, category);
            var id = x.Id;
            service.PersistOnCommit(x);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

        }
























        //[Test]
        //[InitializeUnitTestDbIoCContextAttribute(true)]
        //public void CanGetTags()
        //{
        //    var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

        //    var results = service.GetSpecificationsForUser().ToArray();

        //    Assert.IsTrue(results.Length > 0);
        //}


        //[Test]
        //[InitializeUnitTestDbIoCContextAttribute(true)]
        //public void CanGetTag()
        //{
        //    var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

        //    //var result = service.GetTask("TagA");

        //    Assert.IsNotNull(result);
        //}


        //[Test]
        //[InitializeUnitTestDbIoCContextAttribute(true)]
        //public void CanRemoveTag()
        //{
        //    var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

        //    var result = service.GetSpecificationBySubject("TagA");

        //    Assert.IsNotNull(result);

        //    service.RemoveSpecificationBySubject("TagA");
        //    XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

        //    var resultB = service.GetSpecificationBySubject("TagA");

        //    Assert.IsNull(resultB);

        //}
    }
}


