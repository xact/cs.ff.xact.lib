using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Implementations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct.Functional.Specifications;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct;
    using XAct.Services;

#pragma warning disable 1591
    public class SpecificationDbContextSeeder : UnitTestXActLibDbContextSeederBase<Specification>, ISpecificationDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="specificationSetDbContextSeeder">The specification set database context seeder.</param>
        /// <param name="specificationCategoryDbContextSeeder">The specification category database context seeder.</param>
        /// <param name="specificationStateDbContextSeeder">The specification state database context seeder.</param>
        public SpecificationDbContextSeeder(
            ITracingService tracingService, 
            IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService,
            ISpecificationSetDbContextSeeder specificationSetDbContextSeeder,
            ISpecificationCategoryDbContextSeeder specificationCategoryDbContextSeeder, 
            ISpecificationStateDbContextSeeder specificationStateDbContextSeeder
            )
            : base(tracingService, 
            specificationSetDbContextSeeder, 
            specificationCategoryDbContextSeeder, 
            specificationStateDbContextSeeder)
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            this.InternalEntities.Add(new Specification
            {
                Id = 1.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                Code = "REQ-A",
                SpecificationSetFK = 1.ToGuid(),
                StateFK = 1.ToGuid(),
                CategoryFK = 1.ToGuid(),
                Subject = "SPec A",
                Body = "...."
            });


            this.InternalEntities.Add(new Specification
            {
                Id = 2.ToGuid(),
                Enabled = true,
                ApplicationTennantId = 2.ToGuid(), //Should be excluding it from App A's set...
                Order = 0,
                Code = "REQ-B",
                SpecificationSetFK = 2.ToGuid(),
                StateFK = 1.ToGuid(),
                CategoryFK = 1.ToGuid(),
                Subject = "Spec B",
                Body = "...."
            });


            this.InternalEntities.Add(new Specification
            {
                Id = 3.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId, //Should be excluding it from App A's set...
                Order = 0,
                Code = "REQ-C",
                SpecificationSetFK = 1.ToGuid(),
                StateFK = 1.ToGuid(),
                CategoryFK = 1.ToGuid(),
                Subject = "Spec C",
                Body = "...."
            });


            this.InternalEntities.Add(new Specification
            {
                Id = 4.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId, //Should be excluding it from App A's set...
                Order = 0,
                Code = "REQ-D",
                SpecificationSetFK = 1.ToGuid(),
                StateFK = 1.ToGuid(),
                CategoryFK = 1.ToGuid(),
                Subject = "Spec D",
                Body = "...."


            });


        }


    }
}

