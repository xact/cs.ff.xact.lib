using System;
using XAct.Data.EF.CodeFirst;
using XAct.Diagnostics;
using XAct.Environment;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Implementations
{
    public class SpecificationSetStateDbContextSeeder : UnitTestXActLibDbContextSeederBase<SpecificationSetState>, ISpecificationSetStateDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        public SpecificationSetStateDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService
            )
            : base(tracingService)
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            this.InternalEntities.Add(new SpecificationSetState
            {
                Id = 1.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order=0,
                Tag=null,
                Text="Active",
                Description="...",
                Filter=null
            });

            this.InternalEntities.Add(new SpecificationSetState
            {
                Id = 2.ToGuid(),
                Enabled = true,
                ApplicationTennantId = 2.ToGuid(),
                Order = 0,
                Tag = null,
                Text = "OtherState",
                Description = "...",
                Filter = null
            });

            this.InternalEntities.Add(new SpecificationSetState
            {
                Id = 3.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                Tag = null,
                Text = "Retired",
                Description = "...",
                Filter = null
            });

        }


    }
}