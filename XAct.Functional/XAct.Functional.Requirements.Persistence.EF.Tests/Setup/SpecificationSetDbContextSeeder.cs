using System;
using XAct.Data.EF.CodeFirst;
using XAct.Diagnostics;
using XAct.Environment;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Implementations
{
    public class SpecificationSetDbContextSeeder : UnitTestXActLibDbContextSeederBase<SpecificationSet>, ISpecificationSetDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="specificationSetStateDbContextSeeder">The specification set database context seeder.</param>
        /// <param name="specificationSetCategoryDbContextSeeder">The specification category database context seeder.</param>
        public SpecificationSetDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService,
            ISpecificationSetStateDbContextSeeder specificationSetStateDbContextSeeder,
            ISpecificationSetCategoryDbContextSeeder specificationSetCategoryDbContextSeeder
            )
            : base(tracingService,
                specificationSetStateDbContextSeeder,
                specificationSetCategoryDbContextSeeder)
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            this.InternalEntities.Add(new SpecificationSet
            {
                Id = 1.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                StateFK =  1.ToGuid(),
                CategoryFK = 1.ToGuid(),
                Subject = "Some Set A",
                Body="...."
            });

            this.InternalEntities.Add(new SpecificationSet
            {
                Id = 2.ToGuid(),
                Enabled = true,
                ApplicationTennantId = 2.ToGuid(), //Different Application tennant
                Order = 0,
                StateFK = 1.ToGuid(),
                CategoryFK = 1.ToGuid(),
                Subject = "Some Set B",
                Body = "...."
            });


            this.InternalEntities.Add(new SpecificationSet
            {
                Id = 3.ToGuid(),
                Enabled = false,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                StateFK = 1.ToGuid(),
                CategoryFK = 1.ToGuid(),
                Subject = "Some Set C",
                Body = "...."
            });



            this.InternalEntities.Add(new SpecificationSet
            {
                Id = 4.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                StateFK = 1.ToGuid(),
                CategoryFK = 1.ToGuid(),
                Subject = "Some Set D",
                Body = "...."
            });
        }
    }
}