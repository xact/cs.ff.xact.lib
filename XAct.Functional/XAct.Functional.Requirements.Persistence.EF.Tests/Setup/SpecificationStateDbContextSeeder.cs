using System;
using XAct.Data.EF.CodeFirst;
using XAct.Diagnostics;
using XAct.Environment;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Implementations
{
    public class SpecificationStateDbContextSeeder : UnitTestXActLibDbContextSeederBase<SpecificationState>, ISpecificationStateDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        public SpecificationStateDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService
            )
            : base(tracingService)
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            this.InternalEntities.Add(new SpecificationState
            {
                Id = 1.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order=0,
                Tag=null,
                Text="Proposed",
                Description="Specification has been proposed, but not reviewed.",
                Filter=null
            });

            this.InternalEntities.Add(new SpecificationState
            {
                Id = 2.ToGuid(),
                Enabled = true,
                ApplicationTennantId = 2.ToGuid(),
                Order = 0,
                Tag = null,
                Text = "Reviewing",
                Description = "Specification is being circulated for review.",
                Filter = null
            });

            this.InternalEntities.Add(new SpecificationState
            {
                Id = 3.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                Tag = null,
                Text = "ForAmmendment",
                Description = "Specification is not rejected, but needs more work.",
                Filter = null
            });

            this.InternalEntities.Add(new SpecificationState
            {
                Id = 4.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                Tag = null,
                Text = "Retracted",
                Description = "Specification has been retracted.",
                Filter = null
            });


            this.InternalEntities.Add(new SpecificationState
            {
                Id = 5.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                Tag = null,
                Text = "Declined",
                Description = "Specification proposal has not been accepted.",
                Filter = null
            });

            this.InternalEntities.Add(new SpecificationState
            {
                Id = 6.ToGuid(),
                Enabled = true,
                ApplicationTennantId = applicationTennantId,
                Order = 0,
                Tag = null,
                Text = "Accepted",
                Description = "Specification has been proposed, but not reviewed.",
                Filter = null
            });

        }


    }
}