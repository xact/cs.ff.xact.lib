using System;
using System.Data.Entity;
using NUnit.Framework;
using XAct.Bootstrapper.Tests;
using XAct.Domain.Repositories;
using XAct.Functional.Specifications.Factories;
using XAct.Functional.Specifications.Models;
using XAct.Functional.Specifications.Services;
using XAct.Functional.Specifications.Services.Implementations;
using XAct.Tests;

namespace XAct.Functional.Specifications.Tests
{
    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SpecificationServiceCommentsUnitTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>()
                .GetCurrent()
                .GetInnerItem<DbContext>()
                .Configuration.LazyLoadingEnabled = false;
            //XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>()
            //    .GetCurrent()
            //    .GetInnerItem<DbContext>()
            //    .Configuration.ProxyCreationEnabled = false;
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>()
                .GetCurrent()
                .GetInnerItem<DbContext>()
                .Database.Log = l => System.Diagnostics.Trace.WriteLine(((string)l));
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        [Test]
        public void Can_Get_SpecificationService()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();
            Assert.IsNotNull(service);
        }

        [Test]
        public void Can_Get_SpecificationService_Of_Expected_Instance_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();
            Assert.AreEqual(typeof (SpecificationService), service.GetType());
        }



        [Test]
        public void Can_Persist_Record()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISpecificationService>();

            var set = service.SpecificationSetService.GetById(1.ToGuid());
            var state = service.StateService.GetById(1.ToGuid());
            var category = service.CategoryService.GetById(1.ToGuid());

            var x = SpecificationFactory.Create("REQ-123", "Foo Text3", "Foo Description", set, state, category);

            x.AddComment("Hi there...");
            //Save the id of this record:
            var id = x.Id;
            service.PersistOnCommit(x);
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            var s = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>().GetSingle<Specification>(t=>t.Id == id);

            var r1= service.GetSpecification(id);
            Assert.That(r1,Is.Not.Null);
            //Assert.That(r1.SpecificationSet, Is.Null);
            Assert.That(r1.State, Is.Null,"SHould have no state");
            Assert.That(r1.Category, Is.Null,"Should have no category");
            Assert.That(r1.Comments.Count, Is.EqualTo(0),"Should have no comments.");

            r1 = service.GetSpecification(id,true);
            Assert.That(r1, Is.Not.Null,"Should have a State");
            Assert.That(r1.State, Is.Not.Null);

            r1 = service.GetSpecification(id, true,true);
            Assert.That(r1, Is.Not.Null);
            Assert.That(r1.State, Is.Not.Null,"Should have a State");
            Assert.That(r1.Category, Is.Not.Null);

            r1 = service.GetSpecification(id, true, true,true,true);
            Assert.That(r1, Is.Not.Null);
            Assert.That(r1.State, Is.Not.Null,"Should have a State");
            Assert.That(r1.Category, Is.Not.Null, "Should have a Category");
            Assert.That(r1.Comments.Count, Is.AtLeast(1), "Should have Comments");
        }

    }
}