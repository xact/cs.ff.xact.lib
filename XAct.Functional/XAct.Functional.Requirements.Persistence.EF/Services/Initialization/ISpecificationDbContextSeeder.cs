using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Specification}"/>
    /// to seed the Tip tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface ISpecificationDbContextSeeder : IHasXActLibDbContextSeeder<Specification>
    {

    }
}

