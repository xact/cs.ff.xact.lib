using XAct.Data.EF.CodeFirst;
using XAct.Diagnostics;
using XAct.Environment;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Implementations
{
    public class SpecificationSetDbContextSeeder : XActLibDbContextSeederBase<SpecificationSet>, ISpecificationSetDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="specificationSetStateDbContextSeeder">The specification set state database context seeder.</param>
        /// <param name="specificationSetCategoryDbContextSeeder">The specification set category database context seeder.</param>
        public SpecificationSetDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
            ISpecificationSetStateDbContextSeeder specificationSetStateDbContextSeeder,
            ISpecificationSetCategoryDbContextSeeder specificationSetCategoryDbContextSeeder
            )
            : base(tracingService,
                specificationSetStateDbContextSeeder,
                specificationSetCategoryDbContextSeeder)
        {
            _environmentService = environmentService;
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }


    }
}