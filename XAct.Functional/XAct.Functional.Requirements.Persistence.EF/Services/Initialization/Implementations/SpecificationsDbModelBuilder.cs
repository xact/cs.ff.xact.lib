using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Functional.Specifications.Services.Initialization.Maps;
    using XAct;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class SpecificationsDbModelBuilder : ISpecificationsDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationsDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public SpecificationsDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<SpecificationSetState>)XAct.DependencyResolver.Current.GetInstance<ISpecificationSetStateModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<SpecificationSetCategory>)XAct.DependencyResolver.Current.GetInstance<ISpecificationSetCategoryModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<SpecificationSet>)XAct.DependencyResolver.Current.GetInstance<ISpecificationSetModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<SpecificationState>)XAct.DependencyResolver.Current.GetInstance<ISpecificationStateModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<SpecificationCategory>)XAct.DependencyResolver.Current.GetInstance<ISpecificationCategoryModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<Specification>)XAct.DependencyResolver.Current.GetInstance<ISpecificationModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<SpecificationComment>)XAct.DependencyResolver.Current.GetInstance<ISpecificationCommentModelPersistenceMap>());

        }
    }
}