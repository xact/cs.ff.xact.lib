using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Implementations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct.Functional.Specifications;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct;
    using XAct.Services;

#pragma warning disable 1591
    public class SpecificationDbContextSeeder : XActLibDbContextSeederBase<Specification>, ISpecificationDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="specificationSetDbContextSeeder">The specification set database context seeder.</param>
        /// <param name="specificationCategoryDbContextSeeder">The specification category database context seeder.</param>
        /// <param name="specificationStateDbContextSeeder">The specification state database context seeder.</param>
        /// <param name="?">The ?.</param>
        public SpecificationDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
            ISpecificationSetDbContextSeeder specificationSetDbContextSeeder,
            ISpecificationCategoryDbContextSeeder specificationCategoryDbContextSeeder, 
            ISpecificationStateDbContextSeeder specificationStateDbContextSeeder
            )
            : base(tracingService, 
            specificationSetDbContextSeeder, 
            specificationCategoryDbContextSeeder, 
            specificationStateDbContextSeeder)
        {
            _environmentService = environmentService;
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }


    }
}

