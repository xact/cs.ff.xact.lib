using System;
using System.ComponentModel.DataAnnotations.Schema;
using XAct.Data;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Maps.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ISpecificationStateModelPersistenceMap"/>
    /// </summary>
    public class SpecificationStateModelPersistenceMap : ApplicationTennantSpecificReferenceDataPersistenceMapBase<SpecificationState, Guid>, ISpecificationStateModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationStateModelPersistenceMap"/> class.
        /// </summary>
        public SpecificationStateModelPersistenceMap()
            : base("SpecificationState", DatabaseGeneratedOption.None)
        {
        }
    }
}