using System.Data.Entity.ModelConfiguration;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Maps.Implementations
{
    public class SpecificationSetModelPersistenceMap : EntityTypeConfiguration<SpecificationSet>,
        ISpecificationSetModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public SpecificationSetModelPersistenceMap()
        {
            this.ToXActLibTable("SpecificationSet");

            this
                .HasKey(m => new { m.Id });


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);


            this.Property(x => x.StateFK)
                .DefineRequiredGuidFK(colOrder++);

            this.Property(x => x.CategoryFK)
                .DefineRequiredGuidFK(colOrder++);



            this.Property(x => x.Subject)
                .DefineRequired1024CharSubject(colOrder++);

            this.Property(x => x.Body)
                .DefineOptionalMaxLengthCharText(colOrder++);





            //State is required (Uncategorized, NotStarted, InProgress, ....) 
            this.HasRequired(x => x.State)
                .WithMany()
                .HasForeignKey(x => x.StateFK)
                ; //No Collection on CategorySide.


            //Category, but by ApplicationHost...
            this.HasRequired(x => x.Category)
                .WithMany()
                .HasForeignKey(x => x.CategoryFK)
                ; //No Collection on CategorySide.



        }
    }
}