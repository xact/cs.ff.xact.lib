using System;
using System.ComponentModel.DataAnnotations.Schema;
using XAct.Data;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Maps.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ISpecificationSetCategoryModelPersistenceMap"/>
    /// </summary>
    public class SpecificationSetCategoryModelPersistenceMap : ApplicationTennantSpecificReferenceDataPersistenceMapBase<SpecificationSetCategory, Guid>, ISpecificationSetCategoryModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationSetCategoryModelPersistenceMap"/> class.
        /// </summary>
        public SpecificationSetCategoryModelPersistenceMap()
            : base("SpecificationSetCategory", DatabaseGeneratedOption.None)
        {
        }
    }
}