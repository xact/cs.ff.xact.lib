using System;
using System.ComponentModel.DataAnnotations.Schema;
using XAct.Data;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Maps.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ISpecificationCategoryModelPersistenceMap"/>
    /// </summary>
    public class SpecificationCategoryModelPersistenceMap : ApplicationTennantSpecificReferenceDataPersistenceMapBase<SpecificationCategory, Guid>, ISpecificationCategoryModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationCategoryModelPersistenceMap"/> class.
        /// </summary>
        public SpecificationCategoryModelPersistenceMap()
            : base("SpecificationCategory", DatabaseGeneratedOption.None)
        {
        }
    }
}