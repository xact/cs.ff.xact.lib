using System;
using System.ComponentModel.DataAnnotations.Schema;
using XAct.Data;
using XAct.Functional.Specifications.Models;
using XAct.Functional.Specifications.Services.Initialization.Maps;

namespace XAct.Functional.SpecificationSets.Services.Initialization.Maps.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ISpecificationSetStateModelPersistenceMap"/>
    /// </summary>
    public class SpecificationSetStateModelPersistenceMap : ApplicationTennantSpecificReferenceDataPersistenceMapBase<SpecificationSetState, Guid>, ISpecificationSetStateModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificationSetStateModelPersistenceMap"/> class.
        /// </summary>
        public SpecificationSetStateModelPersistenceMap()
            : base("SpecificationSetState", DatabaseGeneratedOption.None)
        {
        }
    }
}