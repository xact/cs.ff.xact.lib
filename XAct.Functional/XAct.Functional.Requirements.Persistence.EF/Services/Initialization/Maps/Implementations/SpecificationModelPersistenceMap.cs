
namespace XAct.Functional.Specifications.Services.Initialization.Maps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct;
    using XAct.Functional.Specifications.Models;
    using XAct.Services;


    public class SpecificationModelPersistenceMap : EntityTypeConfiguration<Specification>,
                                                        ISpecificationModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public SpecificationModelPersistenceMap()
        {

            this.ToXActLibTable("Specification");

            this
                .HasKey(m => new { m.Id });


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x=>x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.SpecificationSetFK)
                .DefineRequiredGuidFK(colOrder++);

            this.Property(x => x.ParentFK)
                .DefineOptionalParentFK(colOrder++);

            this.Property(x => x.Code)
                .DefineRequired64CharCode(colOrder++);

            this.Property(x => x.StateFK)
                .DefineRequiredGuidFK(colOrder++);

            this.Property(x => x.CategoryFK)
                .DefineRequiredGuidFK(colOrder++);

            this.Property(x => x.Subject)
                .DefineRequired1024CharSubject(colOrder++);

            this.Property(x => x.Body)
                .DefineOptionalMaxLengthCharText(colOrder++);









            //--------------------------------------------------
            //--------------------------------------------------
            //--------------------------------------------------
            //--------------------------------------------------
            //RELATIONSHIPS:
            //--------------------------------------------------
            //--------------------------------------------------
            //--------------------------------------------------
            //--------------------------------------------------


             //this.HasRequired(x => x.SpecificationSet)
             //.WithMany()
             //.HasForeignKey(x => x.specif)
             //; //No Collection on CategorySide.


            //State is required (Uncategorized, NotStarted, InProgress, ....) 
            this.HasRequired(x => x.State)
                .WithMany()
                .HasForeignKey(x => x.StateFK)
                ; //No Collection on CategorySide.


            //Category, but by ApplicationHost...
            this.HasRequired(x => x.Category)
                .WithMany()
                .HasForeignKey(x => x.CategoryFK)
                ; //No Collection on CategorySide.



            //Can have 0-* Comments:
            this.HasMany(x => x.Comments)
                .WithOptional()
                .HasForeignKey(x => x.OwnerFK);


            //Recursion:
            this
                .HasMany(m => m.Children)
                .WithOptional(m => m.Parent)
                .HasForeignKey(m => m.ParentFK);


            ////The tags:
            //this
            //    .HasMany(p => p.Tags)
            //    .WithMany()
            //    .Map(mc =>
            //    {
            //        x.ToXActLibTable("Task_Joins_Tags");
            //        mc.MapLeftKey("TaskId");
            //        mc.MapRightKey("TagId");
            //    });

 
        }

    }
}
