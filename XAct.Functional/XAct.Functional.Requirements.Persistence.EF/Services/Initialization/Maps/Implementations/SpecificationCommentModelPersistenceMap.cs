using System.Data.Entity.ModelConfiguration;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization.Maps.Implementations
{
    public class SpecificationCommentModelPersistenceMap : EntityTypeConfiguration<SpecificationComment>,
        ISpecificationCommentModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public SpecificationCommentModelPersistenceMap()
        {

            this.ToXActLibTable("SpecificationComment");

            this
                .HasKey(m => new { m.Id });


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.Note)
                .DefineRequired1024CharSubject(colOrder++);


        }
    }
}