namespace XAct.Functional.Specifications.Services.Initialization.Maps
{
    public interface ISpecificationSetModelPersistenceMap : IHasXActLibModelPersistenceMap { }
}