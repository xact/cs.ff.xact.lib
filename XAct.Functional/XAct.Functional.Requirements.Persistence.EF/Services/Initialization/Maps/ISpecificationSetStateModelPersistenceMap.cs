namespace XAct.Functional.Specifications.Services.Initialization.Maps
{
    public interface ISpecificationSetStateModelPersistenceMap : IHasXActLibModelPersistenceMap { }
}