namespace XAct.Functional.Specifications.Services.Initialization.Maps
{
    public interface ISpecificationStateModelPersistenceMap : IHasXActLibModelPersistenceMap { }
}