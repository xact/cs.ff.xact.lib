using XAct.Data.EF.CodeFirst;
using XAct.Functional.Specifications.Models;

namespace XAct.Functional.Specifications.Services.Initialization
{
    public interface ISpecificationStateDbContextSeeder : IHasXActLibDbContextSeeder<SpecificationState>
    {

    }
}