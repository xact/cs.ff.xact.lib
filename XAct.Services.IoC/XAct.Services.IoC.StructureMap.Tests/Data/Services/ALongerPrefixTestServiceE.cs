﻿namespace XAct.Services.Tests.Services
{
    [DefaultBindingImplementation(typeof(ITestServiceE),BindingLifetimeType.SingletonScope, Priority.Low)]
    public class ALongerPrefixTestServiceE : IALongerPrefixTestServiceE
    {
        //Should be registered under  IALongerPrefixTestServiceE
    }
}