﻿namespace XAct.Services.Tests.Services
{
    public interface ITestServiceM : IHasSingletonBindingScope
    {
    }


    [DefaultBindingImplementation(typeof(ITestServiceM),BindingLifetimeType.SingletonScope, Priority.Low)]
    public class OneTestServiceM : ITestServiceM
    {
    }

    public class TwoTestServiceM : ITestServiceM, XAct.IHasHighBindingPriority
    {
    }




    public interface ITestServiceN : IHasSingletonBindingScope
    {
    }


    public class OneTestServiceN : ITestServiceN, XAct.IHasHighBindingPriority
    {
    }

    [DefaultBindingImplementation(typeof(ITestServiceN), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class TwoTestServiceN : ITestServiceN
    {
    }



}