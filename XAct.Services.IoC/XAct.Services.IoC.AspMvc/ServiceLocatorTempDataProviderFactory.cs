﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Services.IoC
{
    using System.Web.Mvc;

#pragma warning disable 1591
    public class ServiceLocatorTempDataProviderFactory : ITempDataProviderFactory
#pragma warning restore 1591
    {
        /// <summary>
        /// Creates an instance of <see cref="T:System.Web.Mvc.ITempDataProvider" /> for the controller.
        /// </summary>
        /// <returns>
        /// The created <see cref="T:System.Web.Mvc.ITempDataProvider" />.
        /// </returns>
        public ITempDataProvider CreateInstance()
        {
            ITempDataProvider result = XAct.DependencyResolver.Current.GetInstance<ITempDataProvider>(false);
            return result ?? new SessionStateTempDataProvider();
        }
    }

}
