//using NinjectAdapter;

namespace XAct.Services.IoC
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// A Ninject based MVC 
    /// <see cref="IDependencyResolver"/>
    /// </summary>
    /// <remarks>
    /// Usage:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// protected void Application_Start() {
    ///    AreaRegistration.RegisterAllAreas();
    /// 
    ///    RegisterGlobalFilters(GlobalFilters.Filters);
    ///    RegisterRoutes(RouteTable.Routes);
    /// 
    ///    NinjectBootstrapper.Initialize();
    ///    MvcBootstrapper.Initialize();
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal>
    /// Keep FullName of Type to disambiguate between our DependencyResolver and MVC's...
    /// </internal>
    public class ServiceLocatorDependencyResolver : System.Web.Mvc.IDependencyResolver
    {
        #region IDependencyResolver Members

        /// <summary>
        /// Resolves singly registered services that support arbitrary object creation.
        /// </summary>
        /// <param name="serviceType">The type of the requested service or object.</param>
        /// <returns>
        /// The requested service or object.
        /// </returns>
        [DebuggerHidden]
        public object GetService(Type serviceType)
        {
            //Read this:interesting?
            //http://blog.longle.net/2012/02/15/wrapping-the-ninject-kernel-with-servicelocator/

            return DependencyResolver.Current.GetInstance(serviceType,false);
        }

        /// <summary>
        /// Resolves multiply registered services.
        /// </summary>
        /// <param name="serviceType">The type of the requested services.</param>
        /// <returns>
        /// The requested services.
        /// </returns>
        [DebuggerHidden]
        public IEnumerable<object> GetServices(Type serviceType)
        {
            //using yield, so try/catch pointless.
                return XAct.DependencyResolver.Current.GetInstances(serviceType);
        }

        #endregion



    }
}
