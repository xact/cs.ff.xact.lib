﻿namespace XAct.Services.IoC
{
    using System;
    using System.Web.Mvc;

    /// <summary>
    /// 
    /// </summary>
        public class ServiceLocatorViewPageActivator : IViewPageActivator
        {

            /// <summary>
            /// </summary>
            /// <param name="controllerContext">The controller context.</param>
            /// <param name="type">The type of the controller.</param>
            /// <returns>
            /// The created view page.
            /// </returns>
            public object Create(ControllerContext controllerContext, Type type)
            {
                return XAct.DependencyResolver.Current.GetInstance(type, false);
            }
        }
}
