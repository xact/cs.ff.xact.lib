﻿namespace XAct.Services.IoC
{
    using System.Web.Mvc.Async;

    /// <summary>
    /// 
    /// </summary>
    public class ServiceLocatorAsyncActionInvokerFactory : IAsyncActionInvokerFactory
    {
        /// <summary>
        /// Creates an instance of async action invoker for the current request.
        /// </summary>
        /// <returns>
        /// The created <see cref="T:System.Web.Mvc.Async.IAsyncActionInvoker" />.
        /// </returns>
        public IAsyncActionInvoker CreateInstance()
        {
            IAsyncActionInvoker result = XAct.DependencyResolver.Current.GetInstance<IAsyncActionInvoker>(false);
            return result ?? new AsyncControllerActionInvoker();
        }
    }
}