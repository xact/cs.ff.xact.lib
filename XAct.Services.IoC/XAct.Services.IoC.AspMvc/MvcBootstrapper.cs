﻿namespace XAct.Services.IoC
{
    using System;
    using System.Web.Mvc;
    using System.Web.Mvc.Async;

    /// <summary>
    /// Bootstrapper for Mvc applications
    /// </summary>
    public static class MvcBootstrapper
    {

        /// <summary>
        /// Initializes XActLib and Mvc.
        /// </summary>
        /// <param name="registerControllerFactory">if set to <c>true</c> registers <see cref="ServiceLocatorControllerFactory" /> as the Controller Factory.</param>
        /// <param name="registerAsDependencyResolver">if set to <c>true</c> [register as dependency resolver].</param>
        /// <param name="registerViewPageActivator">if set to <c>true</c> [register view page activator].</param>
        /// <param name="modelMetadataProviderType">The (optional) model metadata provider.</param>
        /// <param name="registerTempDataProviderFactory">if set to <c>true</c> [register temporary data provider factory].</param>
        /// <param name="registerAsyncActionInvokerFactory">if set to <c>true</c> [register asynchronous action invoker factory].</param>
        public static void Initialize(bool registerControllerFactory = true, bool registerAsDependencyResolver = true, bool registerViewPageActivator = true, Type modelMetadataProviderType = null, bool registerTempDataProviderFactory = true, bool registerAsyncActionInvokerFactory = true)
        {

            //Coax the Ninject Kernel out from the returned result:
            //IKernel _kernel = (IKernel)bootstrapResponse.Result;

            //IMPORTANT:
            //Had to add to as was first getting

            //Inheritance security rules violated by type: 'Ninject.Web.Mvc.Filter.FilterContextParameter'. 
            //Derived types must either match the security accessibility of the base type or be less accessible.

            //Then as I debugged a little closer (in XAct.Lib repository rather than Examples), got this:

            //Attempt by security transparent method 
            //'XAct.Services.IoC.NinjectDependencyResolver..ctor(Boolean, Boolean, XAct.Services.IServiceBindingDescriptorBase[])' 
            //to access security critical type 'Ninject.IKernel' failed.
            //Assembly 'XAct.Services.IoC.AspMvc.Ninject, Version=1.0.0.0, Culture=neutral, PublicKeyToken=9f1417dfe4ed073e' 
            //is marked with the AllowPartiallyTrustedCallersAttribute, and uses the level 2 security transparency model.
            //Level 2 transparency causes all methods in AllowPartiallyTrustedCallers assemblies to become 
            //security transparent by default, which may be the cause of this exception.

            //The attribute seems to fix it. The rest of the lib can be as secure as it needs to be for .NET4.0
            //Note:

            //Do MVC specific stuff:

            if (registerAsDependencyResolver)
            {
               System.Web.Mvc.DependencyResolver.SetResolver(new ServiceLocatorDependencyResolver());
            }

            if (registerControllerFactory)
            {
                XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                    new BindingDescriptor<IControllerFactory, ServiceLocatorControllerFactory>());
            }
            if (registerViewPageActivator)
            {

                XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                new BindingDescriptor<IViewPageActivator, ServiceLocatorViewPageActivator>());
            }

            if (modelMetadataProviderType!=null)
            {
                //As MVC uses the default Resource resolver, and it is not registered, this will cause a minor exception:
                //ModelMetadataProviders.Current = new XAct.UI.ViewModeModelMetadataProvider();
                //When using an DependencyInjectionContainer, the preferred solution is registering it in the underlying DependencyInjectionContainer:
                XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                    new BindingDescriptor(BindingType.Custom, typeof(ModelMetadataProvider), modelMetadataProviderType)
                    );
            }

            if (registerTempDataProviderFactory)
            {
                XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                    new BindingDescriptor(BindingType.Custom, typeof(ITempDataProvider), typeof(SessionStateTempDataProvider)));
                XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                    new BindingDescriptor(BindingType.Custom, typeof(System.Web.Mvc.ITempDataProviderFactory),
                                          typeof (ServiceLocatorTempDataProviderFactory)));
            }


            if (registerAsyncActionInvokerFactory)
            {
                XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                    new BindingDescriptor(BindingType.Custom, typeof(IAsyncActionInvoker), typeof(AsyncControllerActionInvoker)));
                XAct.DependencyResolver.Current.RegisterServiceBindingInIoC(
                    new BindingDescriptor(BindingType.Custom, typeof(IAsyncActionInvokerFactory),
                                          typeof (ServiceLocatorAsyncActionInvokerFactory)));
            }
        }

    }
}
