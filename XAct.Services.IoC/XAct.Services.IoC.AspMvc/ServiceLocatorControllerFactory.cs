namespace XAct.Services.IoC
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    
    /// <summary>
    /// A Unity based Specialization of the 
    /// <see cref="DefaultControllerFactory"/>
    /// </summary>
    /// <remarks>
    /// Usage:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// protected void Application_Start() {
    ///    AreaRegistration.RegisterAllAreas();
    /// 
    ///    RegisterGlobalFilters(GlobalFilters.Filters);
    ///    RegisterRoutes(RouteTable.Routes);
    ///    UnityMvcBootstrapper.Initialize();
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public class ServiceLocatorControllerFactory : DefaultControllerFactory
    {
        /// <summary>
        /// Retrieves the controller instance for the specified request context and controller type.
        /// </summary>
        /// <param name="requestContext">The context of the HTTP request, which includes the HTTP context and route data.</param>
        /// <param name="controllerType">The type of the controller.</param>
        /// <returns>
        /// The controller instance.
        /// </returns>
        /// <exception cref="T:System.Web.HttpException">
        ///   <paramref name="controllerType"/> is null.</exception>
        ///   
        /// <exception cref="T:System.ArgumentException">
        ///   <paramref name="controllerType"/> cannot be assigned.</exception>
        ///   
        /// <exception cref="T:System.InvalidOperationException">An instance of <paramref name="controllerType"/> cannot be created.</exception>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {

            IController result = 
                (controllerType == null)
                ?
                null
                : 
                System.Web.Mvc.DependencyResolver.Current.GetService(controllerType) as IController;

            return result;
        }
    }
}
