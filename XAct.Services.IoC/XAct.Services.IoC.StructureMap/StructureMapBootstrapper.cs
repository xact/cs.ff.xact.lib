﻿
using System.Linq;
using CommonServiceLocator.StructureMapAdapter.Unofficial;
using StructureMap.Pipeline;

namespace XAct.Services.IoC
{
    using StructureMap;
    using System;
    using Microsoft.Practices.ServiceLocation;
    using XAct.Configuration;

    /// <summary>
    /// A one line solution to bootstrapping applications.
    /// <para>
    /// The static <see cref="Initialize"/> method can be used
    /// to initialize a program by creating for it an DependencyInjectionContainer,
    /// registering services with the DependencyInjectionContainer, and optionally
    /// initializing all services 
    /// decorated with the 
    /// <see cref="DefaultBindingImplementationAttribute"/>
    /// </para>
    /// <para>
    /// Example:
    /// <code>
    /// <![CDATA[
    /// class Program {
    ///   static void Main(string[] args){
    ///     StructureMapBootstrapper.Initialize(
    ///       null,
    ///       null,
    ///       null,
    ///       new ServiceRegistrationDescriptor<IEx,Ex>(
    ///         ServiceLifetimeType.SingletonPerWebRequestScope
    ///         )
    ///     );
    ///     
    ///     //All good to go...
    ///     Console.WriteLine(
    ///       XAct.DependencyResolver.Current
    ///         .GetInstance<IEnvironmentService>().Now);
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public static class StructureMapBootstrapper
    {



        /// <summary>
        /// A one line solution to bootstrapping applications.
        /// <para>
        /// Initializes the given DependencyInjectionContainer (StructureMap),
        /// (must be already registered as a Microsoft.Patterns ServiceLocator)
        /// registers in it any specified services,
        /// and registers all services
        /// decorated with the
        /// <see cref="DefaultBindingImplementationAttribute" />
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// class Program {
        /// static void Main(string[] args){
        /// StructureMapBootstrapper.Initialize(
        /// null,
        /// null,
        /// null,
        /// (IBindingDescriptorResult x)=>Console.WriteLine(x.BindingDescriptor.InterfaceType.Name),
        /// new ServiceRegistrationDescriptor<IEx,Ex>(
        /// ServiceLifetimeType.SingletonPerWebRequestScope));
        /// Console.WriteLine(
        /// XAct.DependencyResolver.Current
        /// .GetInstance<IEnvironmentService>().Now);
        /// }
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="structureMapContainerObject">The structureMap container.</param>
        /// <param name="scanForServicesToRegister">if set to <c>true</c> [scan for services to register].</param>
        /// <param name="optionalPreBindingCallback">The optional pre binding callback.</param>
        /// <param name="optionalPostBindingCallback">The optional post binding callback.</param>
        /// <param name="optionalServicesToRegister">The optional services to register.</param>
        /// <returns></returns>
        /// <exception cref="ConfigurationException">@ServiceLocator is not yet configured correctly. Consider doing the following first:
        /// IContainer stContainer = new Container();
        /// XAct.Library.Settings.IoC.DependencyInjectionContainer = stContainer;
        /// StructureMapServiceLocator structureMapServiceLocator = new StructureMapServiceLocator(stContainer);
        /// XAct.Library.Settings.IoC.DependencyInjectionContainerConnector = structureMapServiceLocator;
        /// ServiceLocator.SetLocatorProvider(() =&gt; structureMapServiceLocator);
        /// </exception>
        public static void Initialize(
            object structureMapContainerObject = null,
            bool scanForServicesToRegister = true,
            Action<IBindingDescriptor> optionalPreBindingCallback = null, 
            Action<IBindingDescriptorResult> optionalPostBindingCallback = null, 
            params IBindingDescriptorBase[] optionalServicesToRegister)
        {
            IContainer container;
            
            if (structureMapContainerObject == null)
            {

                container = new Container();
            }
            else
            {
                container = structureMapContainerObject as IContainer;
            }

            //Save a global reference to the DependencyInversionContainer:
            XAct.DependencyResolver.DependencyInjectionContainer = container;

            //We have the IoC -- but we don't have the IoC Wrapper to 
            ServiceLocatorImplBase structureMapServiceLocator = new StructureMapServiceLocator(container);

            XAct.DependencyResolver.DependencyInjectionContainerConnector = structureMapServiceLocator;
            ServiceLocator.SetLocatorProvider(() => structureMapServiceLocator);


            XAct.DependencyResolver.Current.SetInternal(structureMapServiceLocator);

            ////Finally:
            //XAct.Library.Status.Initialization.DependencyInjectionServiceLocator = 
            //    ServiceLocatorExtensions.TryGetMSPPServiceLocator<IServiceLocator>();

            //Save a global reference to the two primary methods we'll be using over and over:
            XAct.Library.Settings.IoC.RegisterBindingMethod =
// ReSharper disable RedundantLambdaParameterType
                (IBindingDescriptor bindingDescriptor) =>
// ReSharper restore RedundantLambdaParameterType
                RegisterBinding(container, bindingDescriptor);

            XAct.Library.Settings.IoC.IsBindingRegisteredMethod =
                (interfaceType, tag) => IsBindingRegistered(container, interfaceType, tag);


            XAct.Library.Settings.IoC.ResetAllMethod = Reset;


            //Call the method used to find and automatically register any services it finds:
            AppDomainExtensions.RegisterBindings(
                scanForServicesToRegister,
                optionalPreBindingCallback, 
                optionalPostBindingCallback, 
                optionalServicesToRegister
                );
        }



        /// <summary>
        /// Clears out the 
        /// <see cref="XAct.DependencyResolver.DependencyInjectionContainer"/>
        /// and 
        /// <see cref="XAct.DependencyResolver.DependencyInjectionContainerConnector"/>
        /// parameters:
        /// </summary>
        public static void Reset()
        {
            XAct.DependencyResolver.DependencyInjectionContainer = null;
            XAct.DependencyResolver.DependencyInjectionContainerConnector = null;
            //XAct.Library.Status.Initialization.DependencyInjectionServiceLocator = null;

        }









        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------

        
        private static void RegisterBinding(IContainer smContainer, IBindingDescriptor binding)
        {

            //binding =  Type interfaceType, Type instanceType,  BindingLifetimeType lifetimeType, string tag = null;
            Type interfaceType = binding.InterfaceType;
            Type instanceType = binding.ImplementationType;
            BindingLifetimeType lifetimeType = binding.ServiceLifeType;
            string tag = binding.Tag;


            //if (tag == null)
            //{
            //    tag = string.Empty;
            //}

            ILifecycle lifecycle;

            switch (lifetimeType)
            {


                case BindingLifetimeType.SingletonPerWebRequestScope:
                    lifecycle = ApplicationEnvironment.HttpContext != null ? 
                        (ILifecycle) new UniquePerRequestLifecycle() : (ILifecycle) new  ThreadLocalStorageLifecycle();
                    break;
                case BindingLifetimeType.SingletonPerThreadScope:
                    lifecycle = ApplicationEnvironment.HttpContext != null ? new UniquePerRequestLifecycle() : (ILifecycle) new  ThreadLocalStorageLifecycle();
                    break;
                case BindingLifetimeType.SingletonScope:
                    lifecycle = new ContainerLifecycle();;
                    break;
                case BindingLifetimeType.TransientScope:
                    lifecycle = new TransientLifecycle();
                    break;
                default:
// ReSharper disable LocalizableElement
                    throw new ArgumentOutOfRangeException("binding","serviceLifetimeType");
// ReSharper restore LocalizableElement

            }


            if (tag.IsNullOrEmpty())
            {
                
                smContainer.Configure(x=>x.For(interfaceType).LifecycleIs(lifecycle).Use(instanceType));
            }
            else
            {
                    smContainer.Configure(x => x.For(interfaceType).LifecycleIs(lifecycle).Add(instanceType).Named(tag));
            }

        }

        private static bool IsBindingRegistered(IContainer container, Type interfaceType, string tag=null)
        {
            if (string.IsNullOrEmpty(tag))
            {
                return container.Model.HasDefaultImplementationFor(interfaceType);
            }

            return (container.Model.InstancesOf(interfaceType).Any(x => x.Name == tag));

        }


    }
}
