﻿namespace XAct.Services.IoC
{
    using System;
    using System.Linq;
    using CommonServiceLocator.NinjectAdapter;
    using Microsoft.Practices.ServiceLocation;
    using Ninject;
    using Ninject.Syntax;
    using XAct.Services.IoC.Initialization;

    /// <summary>
    /// A one line solution to bootstrapping applications.
    /// <para>
    /// The static <see cref="Initialize"/> method can be used
    /// to initialize a program by creating for it an DependencyInjectionContainer,
    /// registering services with the DependencyInjectionContainer, and optionally
    /// initializing all services 
    /// decorated with the 
    /// <see cref="DefaultBindingImplementationAttribute"/>
    /// </para>
    /// <para>
    /// Example:
    /// <code>
    /// <![CDATA[
    /// class Program {
    ///   static void Main(string[] args){
    /// 
    ///     NinjectBootstrapper.Initialize(
    ///       new ServiceRegistrationDescriptor<IExampleService,ExampleService>(
    ///         ServiceLifetimeType.SingletonPerWebRequestScope
    ///         )
    ///     );
    ///     
    ///     //All good to go...
    ///     Console.WriteLine(
    ///       XAct.DependencyResolver.Current
    ///         .GetInstance<IEnvironmentService>().Now);
    ///   }
    ///     Console.WriteLine(
    ///       XAct.DependencyResolver.Current
    ///         .GetInstance<IExampleService>().DoSomething());
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public static class NinjectBootstrapper
    {




        /// <summary>
        /// A one line solution to bootstrapping applications.
        /// <para>
        /// Initializes the underling DependencyInjectionContainer (Ninject),
        /// registering it as a Microsoft.Patterns ServiceLocator,
        /// registers in it any specified services,
        /// and optionally registers all services
        /// decorated with the
        /// <see cref="DefaultBindingImplementationAttribute" />
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// class Program {
        /// static void Main(string[] args){
        /// IKernel kernel = new StandardKernel();
        /// XAct.Library.Settings.IoC.DependencyInjectionContainer = kernel;
        /// NinjectServiceLocator ninjectServiceLocator = new NinjectServiceLocator(kernel);
        /// XAct.Library.Settings.IoC.DependencyInjectionContainerConnector = ninjectServiceLocator;
        /// ServiceLocator.SetLocatorProvider(() => ninjectServiceLocator);
        /// NinjectBootstrapper.Initialize(kernel,
        /// new ServiceRegistrationDescriptor<IEx,Ex>(
        /// ServiceLifetimeType.SingletonPerWebRequestScope));
        /// Console.WriteLine(
        /// XAct.DependencyResolver.Current
        /// .GetInstance<IEnvironmentService>().Now);
        /// }
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="ninjectKernelObject">The ninject kernel object.</param>
        /// <param name="scanForServicesToRegister">if set to <c>true</c> [scan for services to register].</param>
        /// <param name="optionalPreBindingCallback">The optional pre binding callback.</param>
        /// <param name="optionalPostBindingCallback">The optional post binding callback.</param>
        /// <param name="optionalServicesToRegister">The optional services to register.</param>
        /// <returns></returns>
        /// <remarks>
        /// Note that the parameter must be defined as object, rather than Typed to IKernel
        /// or else you'll ge tthe following exceptions in other assemblies:
        /// <code>
        /// <![CDATA[
        /// Error	17	The type 'Ninject.IKernel' is defined in an assembly that is not referenced. You must add a reference to assembly 'Ninject, Version=3.0.0.0, Culture=neutral, PublicKeyToken=c7192dc5380945e7'.	D:\BB\CS.FF.XAct.Lib\XAct.Bootstrapper.Tests\XAct.Bootstrapper.Tests\IoCBootStrapper.cs	57	17	XAct.Bootstrapper.Tests
        /// ]]>
        /// </code>
        /// </remarks>
        public static void Initialize(
            object ninjectKernelObject=null,
            bool scanForServicesToRegister = true,
            Action<IBindingDescriptor> optionalPreBindingCallback = null,
            Action<IBindingDescriptorResult> optionalPostBindingCallback = null,
            params IBindingDescriptorBase[] optionalServicesToRegister)
        {


            IKernel ninjectKernel;


            if (ninjectKernelObject == null)
            {
                ninjectKernel = new StandardKernel();
            }
            else
            {
                ninjectKernel = ninjectKernelObject as IKernel;
            }


            //Save a global reference to the DependencyInversionContainer:
            XAct.DependencyResolver.DependencyInjectionContainer = ninjectKernel;

            //We have the IoC -- but we don't have the IoC Wrapper to 
            NinjectServiceLocator ninjectServiceLocator = new NinjectServiceLocator(ninjectKernel);
            XAct.DependencyResolver.DependencyInjectionContainerConnector = ninjectServiceLocator;
            ServiceLocator.SetLocatorProvider(() => ninjectServiceLocator);


            XAct.DependencyResolver.Current.SetInternal(ninjectServiceLocator);


            //Finally:
            //XAct.Library.Status.Initialization.DependencyInjectionServiceLocator = ServiceLocatorExtensions.TryGetMSPPServiceLocator<IServiceLocator>();
            
            //Save a global reference to the two primary methods we'll be using over and over:
            XAct.Library.Settings.IoC.RegisterBindingMethod =
// ReSharper disable RedundantLambdaParameterType
                (IBindingDescriptor bindingDescriptor) =>
// ReSharper restore RedundantLambdaParameterType
                RegisterBinding(ninjectKernel, bindingDescriptor);

            XAct.Library.Settings.IoC.IsBindingRegisteredMethod =
                (interfaceType, tag) => IsBindingRegistered(ninjectKernel, interfaceType, tag);

            XAct.Library.Settings.IoC.ResetAllMethod = Reset;
            
            //Call the method used to find and automatically register any services it finds:
            AppDomainExtensions.RegisterBindings(
                scanForServicesToRegister,
                optionalPreBindingCallback, 
                optionalPostBindingCallback, 
                optionalServicesToRegister
                );


            //IMPORTANT: By this time XAct.Library.Status.BindingResults will have been set.


        }

        /// <summary>
        /// Resets this DependencyInjectionContainer.
        /// </summary>
        public static void Reset()
        {
            XAct.DependencyResolver.DependencyInjectionContainer = null;
            XAct.DependencyResolver.DependencyInjectionContainerConnector = null;
            //XAct.Library.Status.Initialization.DependencyInjectionServiceLocator = null;
        }


// ReSharper disable UnusedParameter.Local
        private static bool IsBindingRegistered(IKernel kernel, Type interfaceType, string tag = null)
// ReSharper restore UnusedParameter.Local
        {
            return kernel.GetBindings(interfaceType).Any();
        }


        private static void RegisterBinding(IKernel kernel, IBindingDescriptor binding)
        {

            
            IBindingWhenInNamedWithOrOnSyntax<object> o = kernel.Bind(binding.InterfaceType).To(binding.ImplementationType);
            
            if (!binding.Tag.IsNullOrEmpty())
            {
                o.Named(binding.Tag);
            }

            switch (binding.ServiceLifeType)
            {
                case BindingLifetimeType.SingletonPerWebRequestScope:
                    try
                    {
                        o.InvokeMethod("InRequestScope", null);
                    }
                    catch
                    {
                        o.InThreadScope();
                    }
                    break;
                case BindingLifetimeType.SingletonPerThreadScope:
                    try
                    {
                        o.InvokeMethod("InRequestScope", null);
                    }
                    catch
                    {
                        o.InThreadScope();
                    }
                    break;
                case BindingLifetimeType.SingletonScope:
                    o.InSingletonScope();
                    break;
                case BindingLifetimeType.TransientScope:
                    o.InTransientScope();
                    break;
                default:
// ReSharper disable LocalizableElement
                    throw new ArgumentOutOfRangeException("binding","serviceLifetimeType");
// ReSharper restore LocalizableElement
            }

        }


    }

}
