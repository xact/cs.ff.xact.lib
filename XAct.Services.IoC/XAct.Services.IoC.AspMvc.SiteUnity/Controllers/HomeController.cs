﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XAct.Caching;
using XAct.Diagnostics;
using XAct.Services.IoC.AspMvc.SiteUnity.Models;
using XAct.Settings;

namespace XAct.Services.IoC.AspMvc.SiteUnity.Controllers
{
    public class HomeController : Controller
    {
        public readonly ICachingService _cachingService;
        private readonly IApplicationSettingsService _appSettingsService;
        public readonly ITracingService _tracingService;

        public HomeController(ITracingService tracingService, ICachingService cachingService, IApplicationSettingsService hostSettingsService)
        {
            _cachingService = cachingService;
            _appSettingsService = hostSettingsService;

            _tracingService = tracingService;
        }

        public ActionResult Index()
        {
            int time = _appSettingsService.Current.TryGetSettingValue<int>("SomeCacheTimespan",20*1000);

            string s;
            _cachingService.TryGet("XXX", out s, GetR, TimeSpan.FromSeconds(time), true);

            ViewBag.Message = s;
            return View();
        }
        private string GetR ()
        {
            return "Welcome to ASP.NET MVC! " + DateTime.Now.ToString();
        }

        public ActionResult About()
        {
            return View();
        }
        public ActionResult Create()
        {
            //SelectListItem[] options = new SelectListItem[]{new SelectListItem{false, }}
            //ViewBag.Preferences
            BlogVM model = new BlogVM{Title="Bah!"};


            return View(model);
        }
    }
}
