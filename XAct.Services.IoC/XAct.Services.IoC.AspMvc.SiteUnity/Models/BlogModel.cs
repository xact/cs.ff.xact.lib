﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace XAct.Services.IoC.AspMvc.SiteUnity.Models

{

    public class BlogVM
    {
        public int Id { get;set; }
        public string Title { get; set; }
        [Required(ErrorMessage = "Applicant's Tag is required.")]
        public string Tag { get; set; }

        public int PreferenceFK { get; set; }

        public ICollection<BlogEntry> Entries { get { return _entries ?? (_entries = new Collection<BlogEntry>()); } }
        private ICollection<BlogEntry> _entries;
    }

    public class BlogEntry
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }

    public class Preference
    {
        public int Id { get; set; }
        public int Text { get; set; }
    }
}