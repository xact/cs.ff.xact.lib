﻿namespace XAct.Services.Tests.Services
{
    [DefaultBindingImplementation(typeof(ITestServiceG), BindingLifetimeType.SingletonScope, Priority.Low)]
    [DefaultBindingImplementation(typeof(IAPrefixTestServiceG), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class ALongerPrefixTestServiceG : IAPrefixTestServiceG, IALongerPrefixTestServiceG
    {
        //Should be registered under  IALongerPrefixTestServiceE
    }
}