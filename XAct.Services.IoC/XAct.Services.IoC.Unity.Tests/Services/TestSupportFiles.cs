namespace XAct.Services.Tests.Services
{
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Services;


    public interface IMyService : IHasXActLibService
    {
        
    }
    [DefaultBindingImplementation(typeof(IMyService),BindingLifetimeType.Undefined,Priority.Low,"A")]
    public class MyServiceA : IMyService
    {
        
    }

    [DefaultBindingImplementation(typeof(IMyService), BindingLifetimeType.Undefined, Priority.Low, "B")]
    public class MyServiceB : IMyService
    {
        
    }

}


