﻿namespace XAct.Services.Tests.Services
{
    [DefaultBindingImplementation(typeof(ITestServiceF), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class ALongerPrefixTestServiceF : IAPrefixTestServiceF, IALongerPrefixTestServiceF
    {
        //Should be registered under  IALongerPrefixTestServiceE
    }
}