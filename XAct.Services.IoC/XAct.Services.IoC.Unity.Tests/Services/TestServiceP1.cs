﻿namespace XAct.Services.Tests.Services
{
    using System;

    [DefaultBindingImplementation(typeof(ITestServiceP<int>),BindingLifetimeType.SingletonScope, Priority.Low)]
    public class TestServiceP1 : ITestServiceP<int>
    {

    }
    [DefaultBindingImplementation(typeof(ITestServiceP<Guid>), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class TestServiceP2 : ITestServiceP<Guid>
    {

    }
}