namespace XAct.Services.Tests
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Microsoft.Practices.Unity;
    using NUnit.Framework;
    using XAct;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Implementations;
    using XAct.Environment;
    using XAct.Services.IoC;
    using XAct.Services.Tests.Services;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class UnityBasedDependencyResolverTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Register_A_Service_Using_UnityContainer()
        {

            //Bypass everything in the lib and do it by hand:
            IUnityContainer unityContainer = new UnityContainer();
            unityContainer.RegisterType(typeof (ITraceSwitchService), typeof (TraceSwitchService));

            bool isREgistered = unityContainer.IsRegistered(typeof (ITraceSwitchService));

            Assert.IsTrue(isREgistered);
        }




        [Test]
        public void Can_Resolve_A_Service()
        {
            UnityBootstrapper.Initialize();

            var service = DependencyResolver.Current.GetInstance<ITracingService>();

            Assert.IsNotNull(service);
        }


        [Test]
        public void Can_Resolve_And_Use_A_Service()
        {
            UnityBootstrapper.Initialize();

            DependencyResolver.Current.GetInstance<ITracingService>().QuickTrace("Works...");

            DateTime check = DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC;

            Assert.AreNotEqual(DateTime.MinValue, check);
        }


        [Test]
        public void UnitTest03()
        {
            IUnityContainer unityContainer = new UnityContainer();
            unityContainer.RegisterType(typeof (ITraceSwitchService), typeof (TraceSwitchService));

            Type tExtensions =
                unityContainer.GetType().Assembly.GetType("Microsoft.Practices.Unity.UnityContainerExtensions");

            object r = tExtensions.InvokeMember("IsRegistered",
                                                BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod,
                                                null, null, new object[] {unityContainer, typeof (ITraceSwitchService)});


            Assert.IsNotNull(r);
        }


        [Test]
        public void Cannot_Resolve_MyService_As_No_Name_Given()
        {
            UnityBootstrapper.Initialize();

            //Service was registered with name:
            IMyService serviceA = DependencyResolver.Current.GetInstance<IMyService>(false);

            //So should be null:
            Assert.IsNull(serviceA);

        }

        [Test]
        public void Can_Resolve_MyService_With_Given_Name()
        {
            UnityBootstrapper.Initialize();

            IMyService serviceA = DependencyResolver.Current.GetInstance<IMyService>("A");


            Assert.IsNotNull(serviceA);

            Assert.AreEqual(typeof (MyServiceA), serviceA.GetType());

        }


        [Test]
        public void GetNamedServiceB()
        {
            UnityBootstrapper.Initialize();

            IMyService serviceB = DependencyResolver.Current.GetInstance<IMyService>("B");


            Assert.IsNotNull(serviceB);

            Assert.AreEqual(typeof (MyServiceB), serviceB.GetType());

        }

        [Test]
        public void GetNamedServiceAandB()
        {
            UnityBootstrapper.Initialize();


            var r = XAct.DependencyResolver.BindingResults.BindingScanResults.BindingDescriptors.Select(
                x => x.InterfaceType == typeof (IMyService)).ToArray();

            IMyService serviceA = DependencyResolver.Current.GetInstance<IMyService>("A");

            IMyService serviceB = DependencyResolver.Current.GetInstance<IMyService>("B");


            Assert.IsTrue(r.Length > 1);
            Assert.IsNotNull(serviceA);
            Assert.IsNotNull(serviceB);

            Assert.IsTrue(serviceA is MyServiceA);
            Assert.IsTrue(serviceB is MyServiceB);

        }




        [Test]
        public void Resolve_Service_Using_Inheritence_Binding()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceA>();
            Assert.IsNotNull(service);
            Assert.AreEqual(typeof(TestServiceA),service.GetType());
        }
        [Test]
        public void Resolve_Service_Using_Attribute_Binding()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceB>();
            Assert.IsNotNull(service);
            Assert.AreEqual(typeof(TestServiceB), service.GetType());
        }


        [Test]
        public void Resolve_Service_Using_Inheritence_Binding_Via_Longest_Interface_Name()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceC>(false);
            Assert.IsNull(service,"Cis null if not called from more precise binding");

            //But will work if invoked by more precise binding:
            service = XAct.DependencyResolver.Current.GetInstance<IAPrefixTestServiceC>();
            Assert.IsNotNull(service, "C was null");
            Assert.AreEqual(typeof(APrefixTestServiceC), service.GetType());
        }


        [Test]
        public void Resolve_Service_Using_Inheritence_Binding_Via_Longest_Interface_Name2()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceD>(false);
            Assert.IsNull(service,"D was null");
            
            service = XAct.DependencyResolver.Current.GetInstance<IAPrefixTestServiceD>(false);
            Assert.IsNull(service, "D was null");

            //Can resolve against long name:
            service = XAct.DependencyResolver.Current.GetInstance<IALongerPrefixTestServiceD>();
            Assert.IsNotNull(service, "D found the most specific");
            Assert.AreEqual(typeof(ALongerPrefixTestServiceD), service.GetType());
        }





        [Test]
        public void Resolve_Service_Using_Mix_Of_Inheritence_And_Atribute_Binding()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceE>();
            Assert.IsNotNull(service);

            service = XAct.DependencyResolver.Current.GetInstance<IAPrefixTestServiceE>(false);
            Assert.IsNull(service);


            service = XAct.DependencyResolver.Current.GetInstance<IALongerPrefixTestServiceE>();
            Assert.IsNotNull(service);
            Assert.AreEqual(typeof (ALongerPrefixTestServiceE), service.GetType());
            

            

        }






        [Test]
        public void Resolve_Service_Using_Mix_Of_Inheritence_And_Atribute_Binding2()
        {

        //Was matched by Attribute:
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceF>();
            Assert.IsNotNull(service);


            service = XAct.DependencyResolver.Current.GetInstance<IAPrefixTestServiceF>(false);
            Assert.IsNull(service);

            //By longest name
            service = XAct.DependencyResolver.Current.GetInstance<IALongerPrefixTestServiceF>();
            Assert.IsNotNull(service);
           
        
            
            //Even though there is a prefix, it will match,
            //as there is nothing more specific:
            Assert.AreEqual(typeof(ALongerPrefixTestServiceF), service.GetType());
        }





        [Test]
        public void Resolve_TestServiceG()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IALongerPrefixTestServiceG>();
            Assert.IsNotNull(service);
            //Even though there is a prefix, it will match,
            //as there is nothing more specific:
            Assert.AreEqual(typeof(ALongerPrefixTestServiceG), service.GetType());
        }


        [Test]
        public void Resolve_TestServiceG2()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceG>();
            Assert.IsNotNull(service);

            Assert.AreEqual(typeof(ALongerPrefixTestServiceG), service.GetType());


            service = XAct.DependencyResolver.Current.GetInstance<IAPrefixTestServiceG>();
            Assert.IsNotNull(service);

            Assert.AreEqual(typeof(ALongerPrefixTestServiceG), service.GetType());

            //Note that it did not match this as it was neither
            //the longest name, nor specifically named in the ApplicationBinding:
            var service2 = XAct.DependencyResolver.Current.GetInstance<IAPrefixTestServiceG>(false);
            Assert.IsNotNull(service2);
            Assert.AreEqual(typeof(ALongerPrefixTestServiceG), service2.GetType());

        }


        [Test]
        public void Resolve_TestService_With_Correct_Priority()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceM>();

            Assert.IsNotNull(service,"Was null....");
            Assert.AreEqual(typeof(TwoTestServiceM),service.GetType());
        }
        [Test]
        public void Resolve_TestService_With_Correct_Priority2()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceN>();

            Assert.IsNotNull(service,"Was null...");
            Assert.AreEqual(typeof(OneTestServiceN), service.GetType());
        }


        [Test]
        public void Resolve_Service_With_Generic_Interface_Using_Interfaces()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceO<int>>();

            Assert.IsNotNull(service);
            Assert.AreEqual(typeof (IntTestServiceO), service.GetType());
        }

        [Test]
        public void Resolve_Service_With_Generic_Interface2_Using_Interfaces()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceO<Guid>>();

            Assert.IsNotNull(service);
            Assert.AreEqual(typeof (GuidTestServiceO), service.GetType());
        }






        [Test]
        public void Resolve_Service_With_Generic_Interface_Using_Attributes()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceP<int>>();

            Assert.IsNotNull(service);
            Assert.AreEqual(typeof(TestServiceP1), service.GetType());
        }

        [Test]
        public void Resolve_Service_With_Generic_Interface2_Using_Attributes()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceP<Guid>>();

            Assert.IsNotNull(service);
            Assert.AreEqual(typeof(TestServiceP2), service.GetType());
        }

        [Test]
        public void Resolve_Service_With_Generic_Interface_Using_Attributes_2()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITestServiceQ<int>>();

            Assert.IsNotNull(service);

            Assert.AreEqual(typeof(TestServiceQ<int>), service.GetType());
        }

    
    }

}
 