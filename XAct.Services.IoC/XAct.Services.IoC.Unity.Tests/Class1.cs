﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Services.Tests
{
    using Microsoft.Practices.Unity;
    using NUnit.Framework;
    using XAct.Diagnostics;
    using XAct.Services.IoC;
    using XAct.Tests;

    [TestFixture]
    public class OpenGenericExtensionTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_A_Normal_Service()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITracingService>();

            Assert.That(service, Is.Not.Null);
        }

        [Test]
        public void Can_Get_Hold_Of_Container()
        {
            object container = XAct.DependencyResolver.DependencyInjectionContainer;
            UnityContainer unityContainer = container as UnityContainer;

            Assert.That(unityContainer, Is.Not.Null);
        }


        //[Test]
        //public void Can_Register_An_Open_Service()
        //{

        //    //See: http://elegantcode.com/2009/12/18/advanced-unity-connecting-implementations-to-open-generic-types/
        //var unityContainer = XAct.DependencyResolver.DependencyInjectionContainer  as UnityContainer;

        //unityContainer.AddNewExtension<OpenGenericExtension>()
        //             .Configure<IOpenGenericExtension>()
        //             .RegisterClosedImpl<OrderCanceledEvent>(typeof(IHandler<>));

        //    var handler = container.Resolve<IHandler<OrderCanceledMessage>>();

        //    Assert.AreEqual(handler.GetType(), typeof(OrderCanceledEvent));


        //    Assert.That(service, Is.Not.Null);
        //}

    }
}
