﻿namespace XAct.Services.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;
    using XAct;
    using XAct.Diagnostics.Performance;
    using XAct.Interception.CallHandlers;
    using XAct.Security;

    public class InterceptAttribute : Attribute
    {

    }

    [InterceptAttribute]
    public interface IExample :IHasXActLibService
    {
        string Foo();
    }


    [InterceptAttribute]
    public class Example : IExample
    {
        [PerformanceCounter("Cat", "Perf", true, null, PerformanceCounterUpdateType.Automatic)]
        public virtual string Foo()
        {
            return "Hi";
        }
    }



}