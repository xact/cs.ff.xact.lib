﻿namespace XAct.Services.Tests
{
    using System;
    using Microsoft.Practices.Unity.InterceptionExtension;
    using NUnit.Framework;
    using XAct.Diagnostics.Performance;
    using XAct.Interception.CallHandlers;
    using XAct.Services.IoC;
    using XAct.Tests;

    [TestFixture]
    public class PerformanceCounterCallHandlerTests
    {


        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

            VirtualClassInterceptor.Execute<InterceptAttribute, PerformanceCounterAttribute>(
                "MyPolicy", false, new PerformanceCounterCallHandler());

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest01()
        {
            var example = XAct.DependencyResolver.Current.GetInstance<IExample>();


            Assert.IsNotNull(example);

            Assert.IsNotNull(example.Foo());

        }
    }
}
