using XAct;

//using Microsoft.Practices.ServiceLocation;

namespace NAMESPACENAME.Tests
{
    using System;
    using System.Collections;
    using System.Linq;
    using NUnit.Framework;
    using Ninject;
    using XAct;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Implementations;
    using XAct.Environment;
    using XAct.Services;
    using XAct.Services.IoC;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class UnitTests2
    {
        #region Setup/Teardown

        /// <summary>
        ///   Sets up to do before each and every 
        ///   test within this test fixture is run.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        ///   Tear down after each and every test.
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            GC.Collect();
        }

        #endregion

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest01()
        {
            NinjectBootstrapper.Initialize(
                null,
                true,
                null, 
                null,
                new BindingDescriptor(
                    BindingType.Custom,                     
                    typeof(ITracingService), 
                    typeof(ReplacementTracingService)
                    ),
                new BindingDescriptor(
                    BindingType.Custom,
                    typeof(IMyFirstService),
                    typeof(MyFirstService)
                    )

                );

            ITracingService tracingService = 
                DependencyResolver.Current.GetInstance<ITracingService>();

#pragma warning disable 168
            string result = tracingService.GetType().FullName;
#pragma warning restore 168

            DependencyResolver.Current.GetInstance<ITracingService>().QuickTrace("Works...");

#pragma warning disable 168
            DateTime check = DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC;
#pragma warning restore 168
        }

        /// <summary>
        /// Units the test02.
        /// </summary>
        [Test]
        public void UnitTest02()
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<ITraceSwitchService>().To<TraceSwitchService>();

#pragma warning disable 168
            bool exists = kernel.GetBindings(typeof(ITraceSwitchService)).FirstOrDefault()!=null;
#pragma warning restore 168
        }

        /// <summary>
        /// Units the test03.
        /// </summary>
        [Test]
        public void UnitTest03()
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<ITraceSwitchService>().To<TraceSwitchService>();

#pragma warning disable 168
            IKernel o = kernel;
            object result = o.InvokeMethod("GetBindings", new[] {typeof (ITraceSwitchService)});
            object result2 = result.InvokeMethod("FirstOrDefault",new[] {result});
            object result3 = result.InvokeMethod("FirstOrDefault",null);
            Type t = kernel.GetType().Assembly.GetType("Ninject.Planning.Bindings.IBinding");


            IEnumerable r2 = result as IEnumerable;

            foreach(object v in r2)
           {
               //That should be enough info to prove that a binding exists...
               break;

           } 


            bool exists = kernel.GetBindings(typeof(ITraceSwitchService)).FirstOrDefault() != null;
#pragma warning restore 168
        }



        [Test]
        public void UnitTest04()
        {
            IKernel kernel = new StandardKernel();
            
            kernel.Bind<Ex1>().ToSelf().InSingletonScope();
            kernel.Get<Ex1>().Value = 3;
            Assert.AreEqual(kernel.Get<Ex1>().Value,3);


        }
        
        
        class Ex1{ public int Value { get; set; }}




        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest10()
        {
            BindingDescriptorGroup group1 = 
                new BindingDescriptorGroup(
                    new BindingDescriptor(BindingType.Custom, typeof(IMyFirstService), typeof(MyFirstService)));

            BindingDescriptorGroup group2 =
                new BindingDescriptorGroup(new BindingDescriptor(BindingType.Custom,typeof(IMyFirstService2), typeof(MyFirstService2)), 
                    group1);

            //1
            //2
            //3
            


            NinjectBootstrapper.Initialize(
                null,
                true,
                null,null,
                group2,
                new BindingDescriptor<IMyFirstService3,MyFirstService3>()
                );

#pragma warning disable 219
            IMyFirstService tracingService1;
            IMyFirstService2 tracingService2;
            IMyFirstService3 tracingService3;
#pragma warning restore 219



            //IMyFirstService tracingServiceA;
            //IMyFirstService2 tracingServiceB;
            //IMyFirstService3 tracingServiceC;



            try
            {
                //tracingServiceA = ServiceLocator.Current.GetInstance(typeof(IMyFirstService)) as IMyFirstService;

                tracingService1 = DependencyResolver.Current.GetInstance<IMyFirstService>();
            }catch
            {
                tracingService1 = null;
            }

            try
            {
                //tracingServiceB = ServiceLocator.Current.GetInstance(typeof(IMyFirstService2)) as IMyFirstService2;

                tracingService2 = DependencyResolver.Current.GetInstance<IMyFirstService2>();
            }catch
            {
                tracingService2 = null;
            }

            try
            {
                //tracingServiceC = ServiceLocator.Current.GetInstance(typeof(IMyFirstService3)) as IMyFirstService3;

                tracingService3 = DependencyResolver.Current.GetInstance<IMyFirstService3>();
            }catch
            {
                tracingService3 = null;
            }
        }


    
    }





    public class ReplacementTracingService : SystemDiagnosticsTracingService
    {
        public ReplacementTracingService(ITraceSwitchService traceSwitchService) : base(traceSwitchService) { }

        protected override void WriteLine(TraceLevel traceLevel, string message)
        {
            base.WriteLine(traceLevel, message);
        }
    }



}



public interface IMyFirstService : IHasXActLibService
{
    string SayHello();
    string SayHello(string name);
}

public class MyFirstService : IMyFirstService
{
    public string SayHello()
    {
        return "Hi!";
    }
    public string SayHello(string name)
    {
        return "Hi {0}!".FormatStringCurrentUICulture(name);
    }
}


public interface IMyFirstService2
{
    string SayHello();
    string SayHello(string name);
}

public class MyFirstService2 : IMyFirstService2
{
    public string SayHello()
    {
        return "Hi!";
    }
    public string SayHello(string name)
    {
        return "Hi {0}!".FormatStringCurrentUICulture(name);
    }
}


public interface IMyFirstService3
{
    string SayHello();
    string SayHello(string name);
}

public class MyFirstService3 : IMyFirstService3
{
    public string SayHello()
    {
        return "Hi!";
    }
    public string SayHello(string name)
    {
        return "Hi {0}!".FormatStringCurrentUICulture(name);
    }
}



