﻿namespace XAct.Security
{
    using System.Reflection;
    using Microsoft.Practices.Unity.InterceptionExtension;

    /// <summary>
    /// <para>
    /// <code>
    /// <![CDATA[
    ///   Microsoft.Practices.Unity.IUnityContainer unityContainer = 
    /// XAct.Library.Initialization.Status.BindingResults.DependencyInjectionContainer 
    /// as Microsoft.Practices.Unity.IUnityContainer;
    /// unityContainer.AddNewExtension<Interception>();
    /// unityContainer.Configure<Interception>()
    /// .AddPolicy("TheNameYouLike")
    /// .AddMatchingRule(new AuthorizationByAOPRule())
    /// .AddCallHandler(new BetterAuthenticationHandler());
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public class AuthorizationByAOPRule : IMatchingRule
    {
        /// <summary>
        /// Matcheses the specified member.
        /// </summary>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        public bool Matches(MethodBase member)
        {
            AuthorizationAttribute attribute = member.GetAttribute<AuthorizationAttribute>();
         
            if (attribute != null)
            {
            
                //    if (attribute.Excluded)
            //    {
            //        AuthorizationByAOPAttribute classAttribute = member.ReflectedType.GetAttribute<AuthorizationByAOPAttribute>();
            //        if (classAttribute != null)
            //        {
            //            return false;
            //        }
            //    }

                return true;
            }
            return false;
            //return member.Name.StartsWith("DoSomethingTricky");
        }
    }
}
