﻿namespace XAct.Services.IoC
{
    using System;
    using System.Linq;
    using Microsoft.Practices.Unity;

    /// <summary>
    /// 
    /// </summary>
    public class OpenGenericExtension : UnityContainerExtension, IOpenGenericExtension
    {
        /// <summary>
        /// Initial the container with this extension's functionality.
        /// </summary>
        /// <remarks>
        /// When overridden in a derived class, this method will modify the given
        /// <see cref="T:Microsoft.Practices.Unity.ExtensionContext" /> by adding strategies, policies, etc. to
        /// install it's functions into the container.
        /// </remarks>
        protected override void Initialize()
        {

        }

        /// <summary>
        /// Registers the closed implementation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="openGenericInterface">The open generic interface.</param>
        public void RegisterClosedImpl<T>(Type openGenericInterface)
        {
         
            var closedType = typeof(T);

            //See: http://elegantcode.com/2009/12/18/advanced-unity-connecting-implementations-to-open-generic-types/
            closedType.GetInterfaces()
                      .Where(x => x.IsGenericType)
                      .Where(x => x.GetGenericTypeDefinition() == openGenericInterface)
                      .ToList()
                      .ForEach(x => Container.RegisterType(x, closedType));
        }
    }
}