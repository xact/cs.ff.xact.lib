﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Services.IoC
{
    using Microsoft.Practices.Unity;

    /// <summary>
    /// 
    /// </summary>
    public interface IOpenGenericExtension : IUnityContainerExtensionConfigurator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="openInterface"></param>
        void RegisterClosedImpl<T>(Type openInterface);
    }
}
