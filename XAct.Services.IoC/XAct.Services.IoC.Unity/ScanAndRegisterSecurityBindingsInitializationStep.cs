﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace XAct.Services.IoC
{
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.InterceptionExtension;

    /// <summary>
    /// 
    /// </summary>
    public static class VirtualClassInterceptor
    {
        /// <summary>
        /// Creates Proxies of classes decorated with the specified attribute,
        /// and attaches a behavior to the methods decorated the method attribute (the method
        /// attributes can be on an an underlying interface method).
        /// </summary>
        /// <typeparam name="TClassInterceptAttribute">The type of the class intercept attribute.</typeparam>
        /// <typeparam name="TMethodInterceptAttribute">The type of the method intercept attribute.</typeparam>
        /// <param name="policyName">Name of the policy.</param>
        /// <param name="throwExceptionIfNoDecoratedClassDefinitionsFound">if set to <c>true</c> [throw exception if no decorated class definitions found].</param>
        /// <param name="callHandlers">The call handlers.</param>
        /// <exception cref="System.ArgumentException">Could not find any class definitions decorated with Attribute of type  + typeof(TClassInterceptAttribute).Name +  that has all public methods marked as virtual.</exception>
        /// <exception cref="System.Exception">Development error: all Methods of '{0}' must be marked virtual in order to be able to AOP/Proxy wrap
        ///                             .FormatStringInvariantCulture(serviceInstanceType.FullName)</exception>
        public static void Execute<TClassInterceptAttribute,TMethodInterceptAttribute>(string policyName, bool throwExceptionIfNoDecoratedClassDefinitionsFound=true, params ICallHandler[] callHandlers)
            where TClassInterceptAttribute : Attribute
            where TMethodInterceptAttribute : Attribute
        {

            Microsoft.Practices.Unity.IUnityContainer unityContainer =
                XAct.DependencyResolver.DependencyInjectionContainer
                as Microsoft.Practices.Unity.IUnityContainer;


            //Configure Unity to be Interception ready:
            unityContainer.AddNewExtension<Interception>();

            //Find Service and Service Contracts decorated with our Attribute
            //(not on the methods in this case, but the actual class):
            KeyValuePair<Type, TClassInterceptAttribute>[] services =
                AppDomain.CurrentDomain.GetTypesDecoratedWithAttribute<TClassInterceptAttribute>(null, true, false);

            //The above list will contain both instances and interface types.
            //We only want the actual Instance Types used to back the Service Hosts
            //as ServiceHostFactory can only deal with Instance Types
            Type[] serviceFacedeInstanceTypes =
                services
                .Where(kvp => (!kvp.Key.IsInterface && !kvp.Key.IsAbstract))
                .Select(s => s.Key).ToArray();


            if (throwExceptionIfNoDecoratedClassDefinitionsFound)
            {
                throw new ArgumentException("Could not find any class definitions decorated with Attribute of type " + typeof(TClassInterceptAttribute).Name + " that has all public methods marked as virtual.");
            }

            //Ensure that all methods of all types are marked virtual:
            foreach (Type serviceInstanceType in serviceFacedeInstanceTypes)
            {

                //Have a class that is inheriting from base interface.
                //See: http://stackoverflow.com/questions/17558674/register-same-unity-interception-call-handler-for-all-registered-types

                //Watch out: IsVirtual will pick up both class methods that are marked virtual and ones
                //that were defined lower down in an interface. Therefore check to see IsFinal to ensure
                //that it is not marked with IsFinal. The reason for keeping the isVirtual is 
                //to check for methods came *from* interfaces...

                if (!serviceInstanceType.EnsureAllPublicMethodsAreMarkedVirtual())
                {
                    throw new Exception(
                        "Development error: all Methods of '{0}' must be marked virtual in order to be able to AOP/Proxy wrap"
                            .FormatStringInvariantCulture(serviceInstanceType.FullName));
                }
            }


            //And register each as having an Interceptor:
            foreach (Type serviceInstanceType in serviceFacedeInstanceTypes)
            {
                //Notice that we are registering the Service Facade *INSTANCE* type, and not the backing COntract.
                //This is because the ServiceHostFactory is going to make ServiceHosts that accept a Type -- but 
                //WCF Framework only accepts an *instance* type, and rejects Contracts.

                unityContainer.RegisterType(serviceInstanceType,
                                            new Interceptor<VirtualMethodInterceptor>(),
                                            new InterceptionBehavior<PolicyInjectionBehavior>());

                //Our interceptor is going to be our instance of
                //AuthenticationCallHandler (defined above), 
                //which in turn is going to invoke AuthorisationByMethodService
                //to compare method name against an instore knowledge of roles to method name.



            }

            var policyDefinition =
                unityContainer
                .Configure<Interception>()
                .AddPolicy(policyName)
                .AddMatchingRule(new XAct.Services.Interception.ExtendedCustomAttributeMatchingRule<TMethodInterceptAttribute>());

            foreach (ICallHandler callHandler in callHandlers)
            {
                policyDefinition.AddCallHandler(callHandler);
            }


        }
    }
}