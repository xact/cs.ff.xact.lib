﻿namespace XAct.Services.IoC
{
    using System;
    using Microsoft.Practices.ServiceLocation;
    using Microsoft.Practices.Unity;
    using XAct.Configuration;
    using XAct.Services.IoC.Initialization;

    /// <summary>
    /// A one line solution to bootstrapping applications.
    /// <para>
    /// The static <see cref="Initialize"/> method can be used
    /// to initialize a program by creating for it an DependencyInjectionContainer,
    /// registering services with the DependencyInjectionContainer, and optionally
    /// initializing all services 
    /// decorated with the 
    /// <see cref="DefaultBindingImplementationAttribute"/>
    /// </para>
    /// <para>
    /// Example:
    /// <code>
    /// <![CDATA[
    /// class Program {
    ///   static void Main(string[] args){
    ///     UnityBootstrapper.Initialize(
    ///       null,
    ///       null,
    ///       null,
    ///       new ServiceRegistrationDescriptor<IEx,Ex>(
    ///         ServiceLifetimeType.SingletonPerWebRequestScope
    ///         )
    ///     );
    ///     
    ///     //All good to go...
    ///     Console.WriteLine(
    ///       XAct.DependencyResolver.Current
    ///         .GetInstance<IEnvironmentService>().Now);
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public static class UnityBootstrapper
    {



        /// <summary>
        /// A one line solution to bootstrapping applications.
        /// <para>
        /// Initializes the given DependencyInjectionContainer (Unity),
        /// (must be already registered as a Microsoft.Patterns ServiceLocator)
        /// registers in it any specified services,
        /// and registers all services
        /// decorated with the
        /// <see cref="DefaultBindingImplementationAttribute" />
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// class Program {
        /// static void Main(string[] args){
        /// UnityBootstrapper.Initialize(
        /// null,
        /// null,
        /// null,
        /// (IBindingDescriptorResult x)=>Console.WriteLine(x.BindingDescriptor.InterfaceType.Name),
        /// new ServiceRegistrationDescriptor<IEx,Ex>(
        /// ServiceLifetimeType.SingletonPerWebRequestScope));
        /// Console.WriteLine(
        /// XAct.DependencyResolver.Current
        /// .GetInstance<IEnvironmentService>().Now);
        /// }
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="unityContainerObject">The unity container.</param>
        /// <param name="scanForServicesToRegister">if set to <c>true</c> [scan for services to register].</param>
        /// <param name="optionalPreBindingCallback">The optional pre binding callback.</param>
        /// <param name="optionalPostBindingCallback">The optional post binding callback.</param>
        /// <param name="optionalServicesToRegister">The optional services to register.</param>
        /// <returns></returns>
        /// <exception cref="ConfigurationException">@ServiceLocator is not yet configured correctly. Consider doing the following first:
        /// IUnityContainer unityContainer = new UnityContainer();
        /// XAct.Library.Settings.IoC.DependencyInjectionContainer = unityContainer;
        /// UnityServiceLocator unityServiceLocator = new UnityServiceLocator(unityContainer);
        /// XAct.Library.Settings.IoC.DependencyInjectionContainerConnector = unityServiceLocator;
        /// ServiceLocator.SetLocatorProvider(() =&gt; unityServiceLocator);</exception>
        /// <remarks>
        /// Note that the parameter must be defined as object, rather than Typed to IKernel
        /// or else you'll ge tthe following exceptions in other assemblies:
        /// <code>
        /// <![CDATA[
        /// Error	16	The type 'Microsoft.Practices.Unity.IUnityContainer' is defined in an assembly that is not referenced. You must add a reference to assembly 'Microsoft.Practices.Unity, Version=2.1.505.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'.	D:\BB\CS.FF.XAct.Lib\XAct.Bootstrapper.Tests\XAct.Bootstrapper.Tests\IoCBootStrapper.cs	53	17	XAct.Bootstrapper.Tests
        /// ]]>
        /// </code>
        /// </remarks>
        public static void Initialize(
            object unityContainerObject = null,
            bool scanForServicesToRegister = true,
            Action<IBindingDescriptor> optionalPreBindingCallback = null, 
            Action<IBindingDescriptorResult> optionalPostBindingCallback = null, 
            params IBindingDescriptorBase[] optionalServicesToRegister)
        {
            IUnityContainer unityContainer;
            
            if (unityContainerObject == null)
            {
                unityContainer = new UnityContainer();
            }
            else
            {
                unityContainer = unityContainerObject as IUnityContainer;
            }

            //Save a global reference to the DependencyInversionContainer:
            XAct.DependencyResolver.DependencyInjectionContainer = unityContainer;

            //We have the IoC -- but we don't have the IoC Wrapper to 
            ServiceLocatorImplBase unityServiceLocator = new UnityServiceLocator(unityContainer);

            XAct.DependencyResolver.DependencyInjectionContainerConnector = unityServiceLocator;
            ServiceLocator.SetLocatorProvider(() => unityServiceLocator);


            XAct.DependencyResolver.Current.SetInternal(unityServiceLocator);

            ////Finally:
            //XAct.Library.Status.Initialization.DependencyInjectionServiceLocator = 
            //    ServiceLocatorExtensions.TryGetMSPPServiceLocator<IServiceLocator>();

            //Save a global reference to the two primary methods we'll be using over and over:
            XAct.Library.Settings.IoC.RegisterBindingMethod =
// ReSharper disable RedundantLambdaParameterType
                (IBindingDescriptor bindingDescriptor) =>
// ReSharper restore RedundantLambdaParameterType
                RegisterBinding(unityContainer, bindingDescriptor);

            XAct.Library.Settings.IoC.IsBindingRegisteredMethod =
                (interfaceType, tag) => IsBindingRegistered(unityContainer, interfaceType, tag);


            XAct.Library.Settings.IoC.ResetAllMethod = Reset;


            //Call the method used to find and automatically register any services it finds:
            AppDomainExtensions.RegisterBindings(
                scanForServicesToRegister,
                optionalPreBindingCallback, 
                optionalPostBindingCallback, 
                optionalServicesToRegister
                );


        }



        /// <summary>
        /// Clears out the 
        /// <see cref="XAct.DependencyResolver.DependencyInjectionContainer"/>
        /// and 
        /// <see cref="XAct.DependencyResolver.DependencyInjectionContainerConnector"/>
        /// parameters:
        /// </summary>
        public static void Reset()
        {
            XAct.DependencyResolver.DependencyInjectionContainer = null;
            XAct.DependencyResolver.DependencyInjectionContainerConnector = null;
            //XAct.Library.Status.Initialization.DependencyInjectionServiceLocator = null;

        }









        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------------------

        
        private static void RegisterBinding(IUnityContainer unityContainer, IBindingDescriptor binding)
        {

            //binding =  Type interfaceType, Type instanceType,  BindingLifetimeType lifetimeType, string tag = null;
            Type interfaceType = binding.InterfaceType;
            Type instanceType = binding.ImplementationType;
            BindingLifetimeType lifetimeType = binding.ServiceLifeType;
            string tag = binding.Tag;

            //if (tag == null)
            //{
            //    tag = string.Empty;
            //}

            LifetimeManager lifetimeManager;

            switch (lifetimeType)
            {


                case BindingLifetimeType.SingletonPerWebRequestScope:
                    lifetimeManager = ApplicationEnvironment.HttpContext != null ? new HttpContextLifetimeManager() : new PerThreadLifetimeManager();
                    break;
                case BindingLifetimeType.SingletonPerThreadScope:
                    lifetimeManager = ApplicationEnvironment.HttpContext != null ? new HttpContextLifetimeManager() : new PerThreadLifetimeManager();
                    break;
                case BindingLifetimeType.SingletonScope:
                    lifetimeManager = new ContainerControlledLifetimeManager();
                    break;
                case BindingLifetimeType.TransientScope:
                    lifetimeManager = new TransientLifetimeManager();
                    break;
                default:
// ReSharper disable LocalizableElement
                    throw new ArgumentOutOfRangeException("binding","serviceLifetimeType");
// ReSharper restore LocalizableElement

            }

            unityContainer.RegisterType(interfaceType, instanceType, tag, lifetimeManager);


        }

        private static bool IsBindingRegistered(IUnityContainer unityContainer, Type interfaceType, string tag=null)
        {
            if (string.IsNullOrEmpty(tag))
            {
                return unityContainer.IsRegistered(interfaceType);
            }
            return unityContainer.IsRegistered(interfaceType, tag);
        }
    }
}
