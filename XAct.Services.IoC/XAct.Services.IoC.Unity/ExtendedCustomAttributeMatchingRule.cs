﻿namespace XAct.Services.Interception
{
    using System;
    using System.Reflection;
    using Microsoft.Practices.Unity.InterceptionExtension;

    /// <summary>
    /// <para>
    /// <code>
    /// <![CDATA[
    ///   Microsoft.Practices.Unity.IUnityContainer unityContainer = 
    /// XAct.Library.Status.Initialization.DependencyInjectionContainer 
    /// as Microsoft.Practices.Unity.IUnityContainer;
    /// unityContainer.AddNewExtension<Interception>();
    /// unityContainer.Configure<Interception>()
    /// .AddPolicy("TheNameYouLike")
    /// .AddMatchingRule(new AuthorizationByAOPRule())
    /// .AddCallHandler(new BetterAuthenticationHandler());
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public class ExtendedCustomAttributeMatchingRule<TAttribute> : IMatchingRule
        where TAttribute : Attribute
    {


        /// <summary>
        /// Matcheses the specified member.
        /// </summary>
        /// <param name="member">The member.</param>
        /// <returns></returns>
        public bool Matches(MethodBase member)
        {
            Type interfaceOrClassType = member.DeclaringType;

#pragma warning disable 168
            string fullName = "{0}.{1}".FormatStringInvariantCulture(interfaceOrClassType.Name, member.Name);
#pragma warning restore 168

            MethodInfo methodInfo = (MethodInfo)member;

            //See if method has attribute:
            TAttribute attribute = methodInfo.GetAttributeRecursively<TAttribute>(true);

            if (attribute != null)
            {

                //We have attribute -- but it might be excluded.
                IHasEnabled hasEnabled = attribute as IHasEnabled;

                if ((hasEnabled != null) && (!hasEnabled.Enabled))
                {
                    return false;
                }

                IHasExcluded hasExcluded = attribute as IHasExcluded;
                if ((hasExcluded != null) && (hasExcluded.Excluded))
                {
                    return false;
                }

                return true;
            }

            //We didn't find an attribute on the method...but on the class?

            TAttribute classAttribute = interfaceOrClassType.GetAttributeRecursively<TAttribute>(true);

            return classAttribute != null;
        }
    }
}
