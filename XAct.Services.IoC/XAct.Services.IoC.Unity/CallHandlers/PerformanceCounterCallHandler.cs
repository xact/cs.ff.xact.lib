﻿using System.Reflection;
using Microsoft.Practices.Unity.InterceptionExtension;
using XAct;

namespace XAct.Interception.CallHandlers
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using XAct.Diagnostics.Performance;
    using XAct.Domain.Repositories;
    using XAct.Messages;


#pragma warning disable 1591
#pragma warning restore 1591

    /// <summary>
    /// 
    /// </summary>
    public class PerformanceCounterCallHandler : ICallHandler
    {

        private IPerformanceCounterService PerformanceCounterService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>(); }
        }

 


        /// <summary>
        /// Order in which the handler will be executed
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Implement this method to execute your handler processing.
        /// </summary>
        /// <param name="input">Inputs to the current call to the target.</param>
        /// <param name="getNext">Delegate to execute to get the next delegate in the handler
        /// chain.</param>
        /// <returns>
        /// Return value from the target.
        /// </returns>
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            //We need the method's FQN in orde to authenticate it:
            MethodInfo methodInfo = (MethodInfo) input.MethodBase;

            PerformanceCounterAttribute[] performanceCounterAttributes =
                methodInfo.GetAttributesRecursively<PerformanceCounterAttribute>(true);



            IMethodReturn methodReturn;
            if ((performanceCounterAttributes == null) || (performanceCounterAttributes.Length == 0))
            {
                //This invokes the next item in the Injection pipeline, or eventually calls your method
                methodReturn = getNext().Invoke(input, getNext);

                return methodReturn;
            }

            

            System.Diagnostics.Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            //This invokes the next item in the Injection pipeline, or eventually calls your method
            methodReturn = getNext().Invoke(input, getNext);

            stopwatch.Stop();
            //Get pieces of info we will need:
            TimeSpan elapsed = stopwatch.Elapsed;
            bool isException = methodReturn.Exception != null;
            IResponse response = methodReturn.ReturnValue as IResponse;
            bool? isSuccess = null;
            if (response != null)
            {
                isSuccess = response.Success;
            }

            foreach (PerformanceCounterAttribute performanceCounterAttribute in performanceCounterAttributes.OrderBy(x => x.Order))
            {

                string performanceCategoryName = performanceCounterAttribute.CategoryName;
                string performanceCounterName = performanceCounterAttribute.Name;

                string tag = performanceCounterAttribute.Tag;


                bool resetSubOperationCounters = performanceCounterAttribute.ResetSubOperationCounters;

                if (!performanceCounterAttribute.Enabled)
                {
                    continue;
                }


                try
                {
                    switch (performanceCounterAttribute.Type)
                    {
                        case PerformanceCounterUpdateType.Automatic:
                            IAutoPerformanceCounterSetter autoPerformanceCounterSetter = XAct.DependencyResolver.Current.GetInstance<IAutoPerformanceCounterSetter>();

                            autoPerformanceCounterSetter.SetPerformanceCountersAutomatically(performanceCategoryName, performanceCounterName, tag, elapsed, isException, isSuccess, resetSubOperationCounters);
                            break;
                            /*
                        case PerformanceCounterUpdateType.SetUnitOfWorkOperationCounter:
                            if (unitOfWorkService != null)
                            {
                                int count = unitOfWorkService.GetContextOperationCounter(tag);
                                PerformanceCounterService.Set(performanceCounterName, count);
                            }
                            break;
                        case PerformanceCounterUpdateType.SetUnitOfWorkOperationDurationTicks:
                            if (unitOfWorkService != null)
                            {
                                TimeSpan timeSpan = unitOfWorkService.GetContextOperationDuration(tag);
                                PerformanceCounterService.Set(performanceCounterName, timeSpan.Ticks);
                            }
                            break;
                        case PerformanceCounterUpdateType.IncrementNonExceptionCounter:
                            if (!isException)
                            {
                                PerformanceCounterService.Offset(performanceCounterName);
                            }
                            break;
                        case PerformanceCounterUpdateType.IncrementResponseCounter:
                            if (response != null)
                            {
                                PerformanceCounterService.Offset(performanceCounterName);
                            }
                            break;
                        case PerformanceCounterUpdateType.IncrementResponseSuccessCounter:
                            if (isSuccess.HasValue && isSuccess.Value)
                            {
                                PerformanceCounterService.Offset(performanceCounterName);
                            }
                            break;
                        case PerformanceCounterUpdateType.IncrementResponseFailureCounter:
                            if (isSuccess.HasValue && !isSuccess.Value)
                            {
                                PerformanceCounterService.Offset(performanceCounterName);
                            }
                            break;
                        case PerformanceCounterUpdateType.IncrementExceptionCounter:
                            if (isException)
                            {
                                PerformanceCounterService.Offset(performanceCounterName);
                            }
                            break;
                        case PerformanceCounterUpdateType.IncrementDurationTicks:
                            //Note thate these types of counters require two -- one for PerformanceCounterType.Elapsed
                            //and one to act as a base Base.
                            PerformanceCounterService.Offset(performanceCounterName, (int) elapsed.Ticks);
                            break;
                        case PerformanceCounterUpdateType.SetDurationTicks:
                            PerformanceCounterService.Set(performanceCounterName, elapsed.Ticks);
                            break;
                        case PerformanceCounterUpdateType.IncrementInvoked:
                             */
                        default:
                            PerformanceCounterService.Offset(performanceCounterAttribute.CategoryName, performanceCounterName, null, amount: 1, raiseExceptionIfNotFound: false);
                            break;
                    }
                }
                catch
                {
                }
            } //~loop 

            return methodReturn;
        }


    }
}