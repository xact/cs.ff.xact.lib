﻿

//namespace App.Core.Infrastructure.Security
//{
//    using System;
//    using System.Reflection;
//    using System.Security.Principal;
//    using Microsoft.Practices.Unity.InterceptionExtension;
//    using XAct;
//    using XAct.Diagnostics;
//    using XAct.Environment;
//    using XAct.Security;
//    using XAct.Services.Comm.ServiceModel;

//    /// <summary>
//    /// An AOP Call Handler registered within
//    /// <see cref="App.Core.Infrastructure.Integration.Steps.ScanAndRegisterSecurityBindingsInitializationStep"/>
//    /// (an Initialization step)
//    /// to monitor all Service Facade calls.
//    /// </summary>
//    public class AuthenticationCallHandler : ICallHandler
//    {

//        ITracingService TracingService
//        {
//            get { return XAct.DependencyResolver.Current.GetInstance<ITracingService>(); }
//        }


//        IAuthorisationByMethodService AuthorisationByMethodService
//        {
//            get
//            {
//                return _authorisationByMethodService ??
//                       (_authorisationByMethodService =
//                        XAct.DependencyResolver.Current.GetInstance<IAuthorisationByMethodService>());
//            }
//        }

//        private IPrincipalService PrincipalService
//        {
//            get { return XAct.DependencyResolver.Current.GetInstance<IPrincipalService>(); }
//        }

//        private IAuthorisationByMethodService _authorisationByMethodService;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="AuthenticationCallHandler"/> class.
//        /// </summary>
//        public AuthenticationCallHandler()
//        {

//        }

//        public int Order { get; set; }



//        /// <summary>
//        /// Implement this method to execute your handler processing.
//        /// </summary>
//        /// <param name="input">Inputs to the current call to the target.</param>
//        /// <param name="getNext">Delegate to execute to get the next delegate in the handler
//        /// chain.</param>
//        /// <returns>
//        /// Return value from the target.
//        /// </returns>
//        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
//        {
//            //We need the method's FQN in orde to authenticate it:
//            MethodInfo methodInfo = (MethodInfo)input.MethodBase;

//            //Once we have the method, we can pass it to a service to 
//            //see how the current principal fits within a matrix of Roles to Methods:
//            AuthorizationResult authorisationResult;
//            //This method checks for an AnonymyousAttribute. If found, that's good enough.
//            //If not found, it checks against a Matrix of MethodName=>Role. 
//            //If there is an entry, returns the value, or else false:
//            bool result = this.AuthorisationByMethodService.AuthenticatePrincipalByMethod(methodInfo, out authorisationResult);


//            //Not anon? Not found in matrix? Continue checking against attributes:
//            //Secondary check, directly against Attributes attached to method:
//            if (!result)
//            {
//                TracingService.Trace(TraceLevel.Verbose, "AuthenticationCallHander: Method is not marked as Anon, nor authorized by checking against cached Db authorisation matrix. Checking Attributes on Method itself.");

//                IPrincipal principal = PrincipalService.Principal;

//                if (principal != null)
//                {
//                    TracingService.Trace(TraceLevel.Verbose, "AuthenticationCallHander: Have principal and identity: " + principal.Identity.Name.SafeString(3));

//                    //Service didn't find anything in db...
//                    //So
//                    XAct.Security.AuthorizationAttribute authorisationByAopAttribute =
//                        methodInfo.GetAttributeRecursively<XAct.Security.AuthorizationAttribute>(true);

//                    if (authorisationByAopAttribute != null)
//                    {
//                        TracingService.Trace(TraceLevel.Verbose, "AuthenticationCallHander: Have found an AuthorizationAttribute");
//                        if (authorisationByAopAttribute.Enabled)
//                        {
//                            TracingService.Trace(TraceLevel.Verbose, "AuthenticationCallHander: AuthorizationAttribute is enabled.");

//                            if (!authorisationByAopAttribute.Roles.IsNullOrEmpty())
//                            {
//                                TracingService.Trace(TraceLevel.Verbose, "AuthenticationCallHander: AuthorizationAttribute has Roles");

//                                foreach (string acceptedRole in authorisationByAopAttribute.Roles.Split(';', '|', ','))
//                                {
//                                    string[] parts = acceptedRole.Split(':');
//                                    string roleName = parts[0];
//                                    bool accept = (parts.Length <= 1) || (parts[1].ToUpperInvariant() != "D");
//                                    if (principal.IsInRole(roleName))
//                                    {
//                                        if (accept)
//                                        {
//                                            authorisationResult = AuthorizationResult.Authorized;
//                                            TracingService.Trace(TraceLevel.Verbose, "AuthenticationCallHander: allowing User being listed as NotAuthorized for this method.");
//                                            result = true;
//                                        }
//                                        else
//                                        {
//                                            authorisationResult = AuthorizationResult.NotAuthorized;
//                                            TracingService.Trace(TraceLevel.Warning, "AuthenticationCallHander: denying access due to User being listed as NotAuthorized for this method.");
//                                            result = false;
//                                        }
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }

//            }


//            if (!result)
//            {

//                TracingService.Trace(TraceLevel.Verbose, "AuthenticationCallHander: Gone through all roles, still not authorized");

//                //Hum....
//                //User is defined.
//                //service said that the method was not marked as anonymous,
//                //and doesn't have roles in its cache that allow the current principal through

//                Exception exception;
//                string methodName = methodInfo.Name;
//                string messageText = methodName;

//                Exception innerException =
//                    new Exception("PrincipalService.CurrentIdentityIdentifier: " +
//                                  PrincipalService.CurrentIdentityIdentifier.SafeString(3));

//                if (authorisationResult == AuthorizationResult.NotAuthenticated)
//                {
//                    TracingService.Trace(TraceLevel.Warning, "AuthenticationCallHander: Raising NotAuthenticatedException for " + methodName);


//                    var notAuthenticatedException =
//                        new NotAuthenticatedException(messageText, innerException);

//                    exception = MakeFaultExceptionIfNecessary(notAuthenticatedException);

//                }
//                else
//                {
//                    TracingService.Trace(TraceLevel.Warning, "AuthenticationCallHander: Raising NotAuthorizedException for " + methodName);

//                    NotAuthorizedException notAuthorizedException =
//                        new NotAuthorizedException(messageText, innerException);

//                    exception = MakeFaultExceptionIfNecessary(notAuthorizedException);

//                }


//                IMethodReturn resultException =
//                    input.CreateExceptionMethodReturn(exception);

//                //resultException = input.CreateMethodReturn(exception);

//                //resultException.ReturnValue = exception;

//                return resultException;

//            }

//            //This invokes the next item in the Injection pipeline, or eventually calls your method
//            IMethodReturn methodReturn = getNext().Invoke(input, getNext);

//            return methodReturn;
//        }


//        private Exception MakeFaultExceptionIfNecessary<TException>(TException innerException)
//            where TException : Exception
//        {
//            Exception exception;

//            if (System.ServiceModel.OperationContext.Current != null)
//            {
//                TracingService.Trace(TraceLevel.Verbose, "AuthenticationCallHander: Wrapping Exception using SoapExceptionFactory");
//                exception = SoapExceptionFactory.CreateFaultException(true, innerException);
//            }
//            else
//            {
//                exception = (Exception)innerException;
//            }

//            return exception;
//        }
//    }
//}
