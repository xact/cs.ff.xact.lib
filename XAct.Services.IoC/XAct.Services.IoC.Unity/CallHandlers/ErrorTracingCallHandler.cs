﻿namespace XAct.Interception.CallHandlers
{
    using Microsoft.Practices.Unity.InterceptionExtension;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class ErrorTracingCallHandler : ICallHandler
    {
        private ITracingService TracingService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<ITracingService>(); }
        }

        /// <summary>
        /// Order in which the handler will be executed
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Implement this method to execute your handler processing.
        /// </summary>
        /// <param name="input">Inputs to the current call to the target.</param>
        /// <param name="getNext">Delegate to execute to get the next delegate in the handler
        /// chain.</param>
        /// <returns>
        /// Return value from the target.
        /// </returns>
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {

            IMethodReturn methodReturn = getNext()(input, getNext);

            if (methodReturn.Exception == null)
            {
                return methodReturn;
            }

            try
            {
                TracingService.TraceException(
                    TraceLevel.Error,
                    methodReturn.Exception,
                    "Intercepted Exception: " + methodReturn.Exception.Message);
            }
// ReSharper disable EmptyGeneralCatchClause
            catch
// ReSharper restore EmptyGeneralCatchClause
            {

            }

            ////the method you intercepted caused an exception
            ////check if it is really a method
            //if (input.MethodBase.MemberType == MemberTypes.Method)
            //{
            //    MethodInfo method = (MethodInfo) input.MethodBase;
            //    if (method.ReturnType == typeof (void))
            //    {
            //        //you should only return null if the method you intercept returns void
            //        return null;
            //    }
            //    //if the method is supposed to return a value type (like int) 
            //    //returning null causes an exception
            //}
            return methodReturn;
        }
    }
}