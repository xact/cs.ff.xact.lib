﻿
//namespace XAct.Interception.CallHandlers
//{
//    using System.Reflection;
//    using App.Core.Infrastructure.Integration.Steps;
//    using App.Core.Infrastructure.Services;
//    using Microsoft.AspNet.SignalR.Hubs;
//    using Microsoft.Practices.Unity.InterceptionExtension;



//    /// <summary>
//    /// An AOP Call Handler registered within
//    /// <see cref="ScanAndRegisterSecurityBindingsInitializationStep"/>
//    /// (an Initialization step)
//    /// to monitor all Service Facade calls.
//    /// </summary>
//    public class AuthenticationCallHandler : ICallHandler
//    {

//        IAuthorizationByMethodService AuthorizationByMethodService
//        {
//            get
//            {
//                return _authorisationByMethodService ??
//                       (_authorisationByMethodService =
//                        XAct.DependencyResolver.Current.GetInstance<IAuthorizationByMethodService>());
//            }
//        }

//        private IAuthorizationByMethodService _authorisationByMethodService;

//        public AuthenticationCallHandler()
//        {

//        }

//        public int Order { get; set; }
//        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
//        {


//            MethodInfo methodInfo = (MethodInfo)input.MethodBase;

//            //Once we have the method, we can pass it to a service to 
//            //see how the current principal fits within a matrix of Roles to Methods:

//            bool result = this.AuthorizationByMethodService.AuthenticatePrincipalByMethod(methodInfo);



//            if (!result)
//            {
//                return input.CreateExceptionMethodReturn(new NotAuthorizedException(methodInfo.Name));
//            }

//            //If it's not valid, what do we do?

//            ////service.Trace.AppendLine(entryAttrib.Message);
//            //Put any logic here you would like to invoke BEFORE the method is invoked.
//            //Console.WriteLine("Before method Invocation happened");

//            //This invokes the next item in the Injection pipeline, or eventually
//            //calls your method
//            var methodReturn = getNext().Invoke(input, getNext);

//            return methodReturn;
//        }
//    }
//}
