﻿
//namespace XAct.Interception.CallHandlers
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Text;
//    using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;
//    using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
//    using BusinessServices;
//    using Microsoft.Practices.Unity.InterceptionExtension;
//    using Services.Audit;
//    using System.Web;
//    using Security;
//    using System.Reflection;
//    using Common.Configuration;
//    using Security.SecurityEntities;
//    using Security.Manager;


//        /// <summary>
//        /// An <see cref="ICallHandler"/> that runs validation of a call's parameters
//        /// before calling the target.
//        /// </summary>
//        [ConfigurationElementType(typeof(AuditCallHandlerData))]
//        public class AuditCallHandler : ICallHandler
//        {

//            /// <summary>
//            /// Creates a <see cref="AuditCallHandler"/>.
//            /// </summary>
//            public AuditCallHandler()
//            {

//            }

//            public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
//            {
//                IMethodReturn result = getNext()(input, getNext);
//                this.ExecutePostProcess(input);
//                return result;
//            }

//            public String GetAuditMessage(IMethodInvocation input)
//            {
//                Object[] atts = input.MethodBase.GetCustomAttributes(typeof(AuditMessageAttribute), true);
//                if (atts.Length == 0)
//                    return null;

//                AuditMessageAttribute att = (AuditMessageAttribute)atts[0];
//                return att.Message;
//            }

//            public string GetNameClass_Method(IMethodInvocation input)
//            {
//                string className = input.Target.ToString();
//                string clase = input.MethodBase.DeclaringType.Name;
//                string[] lineNameSpace = new string[3];
//                lineNameSpace = className.Split('.');

//                return lineNameSpace[1];
//            }

//            public string GetParametersMethod(IMethodInvocation input)
//            {
//                ParameterInfo[] parameters = input.MethodBase.GetParameters();
//                string param = null;
//                if (parameters.Length == 0)
//                    return null;
//                for (int i = 0; i < parameters.Length; i++)
//                {
//                    if (param == null)
//                    {
//                        param = parameters[i].Name;
//                    }
//                    else
//                    {
//                        param = param + "; " + parameters[i].Name;
//                    }
//                }
//                return param;
//            }

//            public void ExecutePostProcess(IMethodInvocation input)
//            {
//                EventLog logging = new EventLog();
//                string message = this.GetAuditMessage(input);
//                if (message != null)
//                {
//                    logging.Description = message;
//                }
//                else
//                {
//                    string propertiesMessage = this.LogPropertiesMessageProvider(input.MethodBase.Name);
//                    if (propertiesMessage != null)
//                    {
//                        logging.Description = propertiesMessage;
//                    }
//                    else
//                    {
//                        logging.Description = this.GetNameClass_Method(input) + "." + input.MethodBase.Name.ToString();
//                    }

//                }
//                logging.Date = DateTime.Now;
//                logging.User = this.GetUserLogged();
//                logging.Parameters = this.GetParametersMethod(input);
//            }

//            private string LogPropertiesMessageProvider(string param)
//            {
//                string property = null;
//                string path = System.Configuration.ConfigurationManager.AppSettings["AUDIT_CONFIG_FILE"];
//                PropertiesParamsProvider config = new PropertiesParamsProvider(path);
//                property = config.GetValue(param);
//                return property;
//            }

//            public User GetUserLogged()
//            {
//                // Si se está ejecutando desde un TestCase no existe un contexto HTTP
//                if (HttpContext.Current == null)
//                    return null;

//                return UserManager.GetUser(HttpContext.Current.User.Identity.Name);
//            }

//        }
//    }
//}
