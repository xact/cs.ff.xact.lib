﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using XAct.Services.IoC.Initialization;

namespace XAct.Services.IoC.AspMvc.Ninject.Site
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            //NOTE: This is all you need to add to your app to setup 
            // * instantiates a Ninject Container
            // * registers it as a MSPP ServiceLocator
            // * Optionally, registers it as an MVC DependencyResolver.
            // * Register binding information for any custom services (first).
            // * Register binding information for any XAct library services not already
            //   taken by custom services

            NinjectBootstrapper.Initialize();

            MvcBootstrapper.Initialize();

        }
    }
}