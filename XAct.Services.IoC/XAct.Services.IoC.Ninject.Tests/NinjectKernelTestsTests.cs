namespace NAMESPACENAME.Tests
{
    using System;
    using System.Collections;
    using System.Linq;
    using NUnit.Framework;
    using Ninject;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Implementations;
    using XAct.Environment;
    using XAct.Services.IoC;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class NinjectKernelTestsTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest01()
        {
            NinjectBootstrapper.Initialize();

            DependencyResolver.Current.GetInstance<ITracingService>().QuickTrace("Works...");

#pragma warning disable 168
            DateTime check = DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC;
#pragma warning restore 168
        }

        /// <summary>
        /// Units the test02.
        /// </summary>
        [Test]
        public void UnitTest02()
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<ITraceSwitchService>().To<TraceSwitchService>();
            bool exists = kernel.GetBindings(typeof(ITraceSwitchService)).FirstOrDefault()!=null;
        }

        /// <summary>
        /// Units the test03.
        /// </summary>
        [Test]
        public void UnitTest03()
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<ITraceSwitchService>().To<TraceSwitchService>();

            IKernel o = kernel;
            object result = o.InvokeMethod("GetBindings", new[] {typeof (ITraceSwitchService)});
            object result2 = result.InvokeMethod("FirstOrDefault",new[] {result});
            object result3 = result.InvokeMethod("FirstOrDefault",null);
            Type t = kernel.GetType().Assembly.GetType("Ninject.Planning.Bindings.IBinding");


            IEnumerable r2 = result as IEnumerable;

            foreach(object v in r2)
           {
               //That should be enough info to prove that a binding exists...
               break;

           } 


            bool exists = kernel.GetBindings(typeof(ITraceSwitchService)).FirstOrDefault() != null;
        }



        [Test]
        public void UnitTest04()
        {
            IKernel kernel = new StandardKernel();

            
            kernel.Bind<Ex1>().ToSelf().InSingletonScope();
            kernel.Get<Ex1>().Value = 3;
            Assert.AreEqual(kernel.Get<Ex1>().Value,3);


        }
            class Ex1{ public int Value { get; set; }}
    }


}


