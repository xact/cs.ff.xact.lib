namespace XAct.Entertainment.Quotes.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for the <see cref="IHasXActLibDbModelBuilder"/>
    /// specific to setting up XActLib Messagings capabilities.
    /// </summary>
    public interface IQuotesDbModelBuilder : IHasXActLibDbModelBuilder
    {


    }
}