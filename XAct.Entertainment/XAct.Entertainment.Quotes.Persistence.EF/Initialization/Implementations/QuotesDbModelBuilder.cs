namespace XAct.Entertainment.Quotes.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Entertainment.Quotes;
    using XAct.Entertainment.Quotes.Initialization.DbContextSeeders;
    using XAct.Entertainment.Quotes.Initialization.ModelPersistenceMaps;

    public class QuotesDbModelBuilder :  IQuotesDbModelBuilder
    {

        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuotesDbModelBuilder"/> class.
        /// </summary>
        public QuotesDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }


        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add((EntityTypeConfiguration<Quote>)XAct.DependencyResolver.Current.GetInstance<IQuoteModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<QuoteCategory>)XAct.DependencyResolver.Current.GetInstance<IQuoteCategoryModelPersistenceMap>());
        }
    }
}