namespace XAct.Entertainment.Quotes.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Data.EF.CodeFirst.Seeders.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Entertainment.Quotes;

    public class QuoteDbContextSeeder : DbContextSeederBase<Quote>, IQuoteDbContextSeeder
    {
        private readonly ITracingService _tracingService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="quoteCategoryDbContextSeeder">The quote category database context seeder.</param>
        public QuoteDbContextSeeder(
            ITracingService tracingService,
            IRepositoryService repositoryService,
            IQuoteCategoryDbContextSeeder quoteCategoryDbContextSeeder)
            : base(new[] { quoteCategoryDbContextSeeder })
        {
            _tracingService = tracingService;
            _repositoryService = repositoryService;
        }


        /// <summary>
        /// Seeds the specified database context.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: No default seeding required.", _typeName, "Seed");
            //Create and Cache the entities:

            CreateEntities();

            //Cleanup first (clear all and commit):
            var set = dbContext.Set<Quote>();
            set.RemoveRange(set);
            dbContext.SaveChanges();

            SeedInternalHelper(dbContext,false);
        }

        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority

            this.InternalEntities = new List<Quote>();
             

            this.InternalEntities.Add(new Quote { Text = "Hard work pays off in the future. Laziness pays off now.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "3 out of 4 people make up 75% of the population.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "24 hours in a day, 24 beers in a case. Coincidence? You decide.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A banker is a fellow who lends you his umbrella when the sun is shining and wants it back the minute it begins to rain.", Source = "Mark Twain", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Accomplishing the impossible means only that the boss will add it to your regular duties.", Source = "Doug Larson", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A chicken is an egg's way of making another egg", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A closed mouth gathers no foot.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A conscience is what hurts when all your other parts feel so good.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A day for firm decisions! Or is it?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A device that will cut your fuel bills in half - scissors", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A dog is a dog unless he is facing you. Then he is Mr. Dog.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A government that robs Peter to pay Paul can always depend on the support of Paul.", Source = "George Bernard Shaw", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Always try to make other people happy, even if you have to leave them alone to do it", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A neat desk is a sign of a cluttered desk drawer.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A neat desk is the sign of a sick mind.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A sentence is something you should never put a preposition at the end of.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A signature always reveals a man's character - and sometimes even his name.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A sine curve goes off to infinity, or at least to the end of the blackboard.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A sure cure for seasickness is to sit under a tree.", Source = "Spike Milligan.", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "All men can fly, but sadly, only in one direction - down.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "All my life I wanted to be someone. I guess I should have been more specific.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Anyone who isn't confused really doesn't understand the situation.", Source = "Edward R. Murrow", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Always remember you're unique, just like everyone else.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Anything worth taking seriously is worth making fun of.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Artificial intelligence is no match for natural stupidity.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Beauty is only skin deep, but ugly goes right to the bone", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Before you criticize someone, you should walk a mile in their shoes. That way, when you criticize them, you're a mile away, and you have their shoes", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Be nice to your kids. They'll choose your nursing home.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Boomerangs are coming back, Ski slopes are going downhill, but Exit signs are on the way out.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Breaking the law of gravity isn't painful it's getting caught that hurts.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Caution: do not look into laser with remaining eye.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Change is inevitable, except from a vending machine.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Democracy is four wolves and a lamb voting on what to have for lunch.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Did you know the word gullible is not in the dictionary?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Diplomacy is the art of saying 'good doggie' while looking for a bigger stick.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Don't personify computers - we don't like it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Don't use a big word where a diminutive one will suffice.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Don't worry about avoiding temptation - as you grow older, it starts avoiding you.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Don't worry about the world ending today, it's already tomorrow in Australia", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Eagles may soar, but weasels don't get sucked into jet engines.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Entertainment is just talking louder than the audience.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Everyone has a photographic memory, some just don't have film.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Everyone is someone else's weirdo.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Experience is something you get just after you need it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Free jazz is so called because no-one would ever buy it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Frisbeetarianism: The belief that when you die, your soul goes up on the roof and gets stuck.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Give a man a fish and you feed him for a day. Teach a man to fish, and you can sell him some tackle", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Grandma used to say�'Money is not the key to happiness. If you have money you can always have a key made.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Hangover: The wrath of grapes.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Happiness makes up in height what it lacks in length.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "As a computer, I find your faith in technology amusing.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Ask me about my vow of silence.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Atheism is a non-prophet organisation.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "He who laughs last didn't understand the joke", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "He's supposed to sound like he's in pain, he's a folk singer.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "How do you tell when you run out of invisible ink?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I am not young enough to know everything.", Source = "Oscar Wilde", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I didn't say it was your fault. I said I was going to blame you.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I refuse to have a battle of wits with an unarmed opponent.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I used to be indecisive, now I'm not so sure.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I wonder how much deeper the ocean would be without sponges", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you tell the truth, you don't have to remember anything.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you think nobody cares about you, try missing a couple of mortgage payments.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I'm feeling argumentative. Please contradict me.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I'm having a deja vu experience, just like last time.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If it weren't for the last minute, nothing would get done.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If life was fair, Elvis would be alive and all the impersonators would be dead.' Johnny Carson", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If one synchronized swimmer drowns, do the rest have to drown too?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If today is the first day of the rest of your life, what was yesterday?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you ate pasta and antipasta, would you still be hungry?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you strangle a smurf, what color does it turn?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you lend someone �20, and never see that person again, it was probably worth it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you want someone to catch something, throw it at their nose.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It doesn't matter how you get there if you don't know where you're going.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It is dangerous to be right in matters on which the established authorities are wrong.", Source = "Voltaire", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It's a small world, but I'd hate to have to paint it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It's not the heights that bother me it's falling on the bottoms of them.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I've got a mind like a.. a.. what's that thing called?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I've had amnesia for as long as I can remember", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Life is what happens to you while you're busy making other plans.", Source = "John Lennon", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Millions yearn for immortality who don't know what to do with themselves on a rainy Sunday afternoon.", Source = "Susan Ertz", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "My favourite scientific theory is that the rings of Saturn are made of lost airline luggage.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "My supply of brain cells is finally down to a manageable size", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never ask a barber if he thinks you need a haircut.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never attribute to malice what can be adequately explained by stupidity.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never test the depth of the water with both feet.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "No issue is so small that it can't be blown out of proportion.", Source = "Stuart Hughes", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "No one is listening until you make a mistake.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "No problem is so big that you can't run away from it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Of course, you may smoke - but please don't exhale.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "On the other hand, I have some fingers.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Personifiers of the world! Unite! You have nothing to lose but Mr. Dignity!", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Pound for pound, the amoeba is the most vicious animal on earth.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Rome wasn't built in a day, it just looks like that.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Smash forehead on keyboard to continue.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Some days you are the fly, other days you are the windscreen.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "That's OK. I don't remember my name either.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The amount of sleep needed by the average person is ten minutes more.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The early bird gets the worm, but the early worm gets eaten.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The face of a child can say it all, especially the mouth part of the face.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The lion and the calf shall lie down together but the calf won't get much sleep.", Source = "Woody Allen", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The race is not always to the swift, nor the battle to the strong - but that's the way to bet.", Source = "Damon Runyon", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The trouble with being punctual is that nobody's there to appreciate it.", Source = "Franklin P. Jones", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There are two kinds of people, those who do the work and those who take the credit. Try to be in the first group; there is less competition there.", Source = "Indira Gandhi", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There are three kinds of people - those who can count, and those who can't.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There's no future in time travel.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Time flies like an arrow, fruit flies like a banana", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "To steal ideas from one person is plagiarism; to steal from many is research.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "'Veni, Vidi, Velcro' - I came, I saw, I stuck around.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "'Veni, Vidi, Visa'. I came. I Saw. I did a little shopping.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "What sets us apart from animals is that we're not afraid of vacuum cleaners.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "When a stupid man is doing something he is ashamed of, he always declares that it is his duty.", Source = "George Bernard Shaw", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "You don't need an engine to go downhill.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It's important to have an open mind, but not so open that your brains fall out.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The DOPELER Effect is the tendency of stupid ideas to seem smarter when they come at you rapidly", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "25% of the general population believe in statistics. The other 80% don't trust them at all.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "95% of statistics are just made up anyway.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A new study shows that licking the sweat off a frog can cure depression. The down side is, the minute you stop licking, the frog gets depressed again.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Anything not nailed down is mine. Anything I can pry loose is not nailed down.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I always win. Except when I lose, but then I just don't count it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I went to a restaurant that was so expensive that they didn't have prices on the menu - just little faces with varying expressions of horror.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The person who knows how to laugh at himself will never cease to be amused", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Where in the nursery rhyme does it say Humpty Dumpty is an egg?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It is impossible to make anything foolproof because fools are so ingenious. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The trouble with doing something right the first time, is nobody appreciates how difficult it was.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Everybody lies, but it doesn't matter since nobody listens.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "When I was born, I was so surprised, I couldn't talk for a year and a half!", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "To err is human, to blame the next guy even more so. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There are two rules for success... 1) Never tell everything you know. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Be nice to other people; they outnumber you 5.5 billion to one. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Courage is the art of being the only one that knows you're scared to death.", Source = "Earl Wilson", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Even if you're on the right track, you'll get run over if you just sit there.", Source = "Will Rogers", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I have given up anarchy. Too many rules - hating the government and all that stuff.", Source = "G.H. Hill", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The only reason some people get lost in thought because it is unfamiliar territory.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Talk is cheap because supply exceeds demand", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Start every day off with a smile and get it over with. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Just because something doesn't do what you planned it to do doesn't mean it's useless.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There are two kinds of people, those who finish what they start and so on. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It is the dull man who is always sure, and the sure man who is always dull.", Source = "H. L. Mencken", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A great many people think they are thinking when they are merely rearranging their prejudices.", Source = "William James", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Half of the people in the world are below average. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Male zebras have white stripes, female zebras have black stripes.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Computers are useless. They can only give answers.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There has been an alarming increase in the number of things you know absolutely nothing about.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never keep up with the Joneses. Drag them down to your level. It's cheaper.", Source = "Quentin Crisp", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There are only two industries that refer to their customers as 'users'.", Source = "Edward Tufte", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I loathe people who keep dogs. They are cowards who haven't got the guts to bite people themselves.", Source = "August Strindberg", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Even if you win the rat race, you're still a rat. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "For every problem there is a solution which is simple, neat and wrong. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Natives who beat drums to drive off evil spirits are objects of scorn to smart people who blow horns to break up traffic jams.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "All sweeping generalisations are wrong.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I avoid clich�s like the plague.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "My sources are unreliable, but their information is fascinating.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Believe those who seek the truth; doubt those that find it.", Source = "Andre Gide", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "When you find yourself in a hole, the first thing to do is stop digging.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Democracy is the worst possible form of government, except for all the others.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "We have the finest politicians that money can buy.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never explain -- your Friends do not need it and your enemies will not believe you anyway.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If it's stupid but works, it isn't stupid.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It is a miracle that curiosity survives formal education.", Source = "Albert Einstein", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I used to know everything, till everything changed.", Source = "Sgt Phil Girvan", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "You can have peace, or you can have freedom. Don't ever count on having both at once.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Anything free is worth the price.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Always listen to experts. They'll tell you what can't be done, and why. Then go and do it anyway.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "All that is necessary for the forces of evil to take root in the world is for enough good men to do nothing.", Source = "Edmund Burke", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I find television very educational. Every time someone switches it on I go into another room and read a good book.", Source = "Groucho Marx", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Wisdom has two parts: 1) Having a lot to say, and 2) not saying it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I'm not stupid. I just have a command of thoroughly useless information.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "People will accept your ideas much more readily if you tell them Benjamin Franklin said it first.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The two most common elements in the universe are hydrogen and stupidity.", Source = "Isaac Asimov", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Compromise is the art of dividing a cake so everyone believes he got the biggest piece.", Source = "Ludwig Erhard", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It is the nature of emergencies to be inconvenient.", Source = "Terri Weiner", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It's extremely unlucky to be superstitious, for no other reason than it is always unlucky to be colossally stupid.", Source = "Stephen Fry", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Opportunity is missed by most people because it is dressed in overalls and looks like work.", Source = "Thomas Edison", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Don't go around saying the world owes you a living. The world owes you nothing; it was here first.", Source = "Mark Twain", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It's better to be boldly decisive and risk being wrong than to agonize at length and be right too late.", Source = "Marilyn Kennedy", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A good plan today is better than a perfect plan tomorrow. -Gen.' George Patton", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Man will occassionally stumble over the truth, but most of the time he will pick himself up and continue on.", Source = "Winston Churchill", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It is better to have enough ideas for some of them to be wrong than to always be right by having no ideas at all.", Source = "Edward de Bono", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The world is moving so fast these days that the man who says it can't be done is generally interrupted by someone doing it.", Source = "Elbert Hubbard", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It's a small world, but I wouldn't like to paint it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I don't suffer from insanity, I enjoy every minute of it.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It's lonely at the top, but the food is better.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Pride is what we have.  Vanity is what others have.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Give me ambiguity or give me something else.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Consciousness: that annoying time between naps.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Why is abbreviation such a long word?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "We have enough youth, how about a fountain of Smart?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Time is what keeps everything from happening all at once.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Computers in the future may weigh no more than 1.5 tons.", Source = "Popular Mechanics, 1949", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I think there is a world market for maybe five computers.", Source = "Thomas Watson, chairman of IBM, 1943", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you don't find what you want in the Index, look very carefully through the entire catalogue.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Computers make very fast, very accurate mistakes.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Computers are not intelligent. They only think they are.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Imagine if every thursday your shoes exploded if you tied them in the usual way. This happens to us all the time with computers, and nobody thinks of complaining!", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Tell a man there are 300 billion stars in the universe and he'll believe you. Tell him a seat has wet paint on it and he'll have to touch it to be sure.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you need anything please don't hesitate to ask someone else first.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Some mornings, it's just not worth chewing through the leather straps.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The statistics on sanity are that one out of every four people is suffering from some form of mental illness. Think of your three best friends. If they're okay, then it's you.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If it weren't for electricity we'd all be watching television by candlelight.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "You cannot have everything. I mean, where would you put it?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I will not eat oysters. I want my food dead. Not sick, not wounded, dead.", Source = "Woody Allen", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If toast always lands butter-side down, and cats always land on their feet, what happens if you drop a buttered cat?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Nouvelle Cuisine, roughly translated, means: I can't believe I paid ninety-six pounds and I'm still hungry.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I went to a restaurant that serves 'breakfast at any time'. So I ordered French Toast during the Renaissance.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I went to a General Store, but they wouldn't sell me anything specific.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A positive attitude will not solve all your problems, but it will annoy enough people to make it worth the effort.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "This year's Psychic Fair will be held on June 23rd. In the event of rain, it will be held the previous Saturday.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "'Of course,' said my grandfather, pulling a gun from his belt as he stepped from the Time Machine, 'There's no paradox if I shoot YOU'.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "In theory, there is no difference between theory and practice. But, in practice, there is.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The only difference between me and a madman is that I'm not mad.", Source = "Salvador Dali", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never interrupt your enemy when he is making a mistake.", Source = "Napoleon Bonaparte", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I have not failed. I've just found 10,000 ways that won't work.", Source = "Thomas Alva Edison", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "A witty saying proves nothing.", Source = "Voltaire", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Some drink at the fountain of knowledge...others just gargle.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It's not an optical illusion, it just looks that way.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "When all else fails, read the instructions.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Smile, it makes people wonder what you're thinking.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Practice makes perfeckt.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you can remember the '60s, then you weren't there.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There is no surer way to ruin a good discussion than to contaminate it with facts.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I can't make predictions and I never will", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The second best policy is dishonesty", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If it boils and is watched, it can't be a pot.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I argue very well. Ask any of my remaining friends. I can win an argument on any topic, against any opponent. People know this, and steer clear of me at parties.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If at first you don't succeed, then skydiving definitely isn't for you.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If a vegetarian eats vegetables, what does a humanitarian eat?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "How can a slim chance and a fat chance be the same?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "For business reasons, I must maintain the outward signs of sanity.", Source = "Mark Twain", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The trouble with the world is that the stupid are cocksure and the intelligent are full of doubt.", Source = "Bertrand Russell", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Only newspaper editors and people with tapeworms should ever refer to themselves as, 'we'", Source = "Mark Twain", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It is better to remain silent and be thought a fool than to open one's mouth and remove all doubt", Source = "Mark Twain", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never preceed anything more complex than a Frisbee throw with, 'Hey boss, watch this!", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It is best to mince one's words very finely, it makes them much easier to eat afterwards.'- Oscar Wilde", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never give up, never give up, never, never, never give up!", Source = "Winston Churchill", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Underneath the skin, all humanity are brothers and I would gladly skin humanity alive to prove it.", Source = "Ellsworth Toohey", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There is nothing either good or bad, but thinking makes it so.", Source = "Shakespeare", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "I guess when you turn off the main road, you have to be prepared to see some funny houses.", Source = "Stephen King", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never let your sense of morals prevent you from doing what is right.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "When I was a kid my favorite relative was Uncle Caveman. After school we'd all go play in his cave, and every once in a while he would eat one of us. It wasn't until later that I found out that Uncle Caveman was a bear.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Take my advice: If the recipe calls for \"3 handfuls of lava\" it's really not worth the effort.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Those are my principles. If you don't like them I have others.", Source = "Groucho Marx.", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Beer is proof that God loves us and wants us to be happy", Source = "Benjamin Franklin", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "There are 10 types of people in this world; those who understand the binary number system and those who do not", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "He was a great patriot, a humanitarian, a loyal friend; provided, of course, he really is dead.", Source = "Voltaire.", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Outside of the killings, Washington has one of the lowest crime rates in the country", Source = "Mayor Marion Barry, Washington DC", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Every time I close the door on reality it comes in through the windows.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Thirty-five is when you finally get your head together and your body starts falling apart.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The tragedy of life doesn't lie in not reaching your goal. The tragedy lies in having no goal to reach.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "No great man ever complains of want of opportunity.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "It isn't pollution that's harming the environment, it's the impurities in our air and water that are doing it", Source = "Dan Quayle, Former US Vice President", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "What if, at this very moment, you are living up to your full potential?", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Sanity calms, but madness is more interesting.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Don't talk about yourself so much... we'll do that when you leave.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you try and don't succeed, cheat. Repeat until caught. Then lie.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Never say \"OOPS!\" always say \"Ah, Interesting!\"", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "This isn't burger king, you can't have it your way.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "If you must choose between two evils, pick the one you've never tried before.", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Everything that can be invented has been invented.", Source = "Charles H. Duell, Commissioner, U.S. Office of Patents, 1899.", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "The abdomen, the chest, and the brain will forever be shut from the intrusion of the wise and humane surgeon. -Sir John Eric Ericksen, British surgeon, appointed Surgeon-Extraordinary to Queen Victoria 1873.", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Louis Pasteur's theory of germs is ridiculous fiction.'-Pierre Pachet, Professor of Physiology at Toulouse, 1872", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Who the hell wants to hear actors talk?", Source = "H.M. Warner, Warner Brothers, 1927.", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "We don't like their sound, and guitar music is on the way out.", Source = "Decca Recording Co. rejecting the Beatles, 1962.", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Heavier-than-air flying machines are impossible.", Source = "Lord Kelvin, president, Royal Society, 1895.", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Two times a week, we go to a nice restaurant, a little wine, good food and companionship. She goes Mondays, I go Fridays. ", Source = "", Enabled = true });
            this.InternalEntities.Add(new Quote { Text = "Women will never be equal to men until they can walk down the street with a bald head and a beer gut, and still think they are beautiful ", Source = "", Enabled = true });

            //transfer:
        }
    }
}