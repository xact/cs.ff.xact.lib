namespace XAct.Entertainment.Quotes.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Data.EF.CodeFirst.Seeders.Implementations;
    using XAct.Domain.Repositories;
    using XAct.Entertainment.Quotes;

    public class QuoteCategoryDbContextSeeder : DbContextSeederBase<QuoteCategory>, IQuoteCategoryDbContextSeeder
    {
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteCategoryDbContextSeeder"/> class.
        /// </summary>
        /// <param name="repositoryService">The repository service.</param>
        public QuoteCategoryDbContextSeeder(IRepositoryService repositoryService)
        {
            _repositoryService = repositoryService;
        }

        public override void SeedInternal(DbContext dbContext)
        {

            CreateEntities();

            SeedInternalHelper(dbContext,true,x=>x.Text);

        }

         public override void CreateEntities()
         {
             this.InternalEntities = new List<QuoteCategory>();

             this.InternalEntities.Add(new QuoteCategory { ResourceFilter = string.Empty ,Text = "Trivia", Enabled = true });

         }
    }
}