namespace XAct.Entertainment.Quotes.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Entertainment.Quotes;

    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IQuoteCategoryDbContextSeeder : IHasXActLibDbContextSeeder<QuoteCategory>
    {
        
    }
}