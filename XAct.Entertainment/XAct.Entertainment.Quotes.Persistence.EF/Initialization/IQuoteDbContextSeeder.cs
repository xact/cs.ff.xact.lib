namespace XAct.Entertainment.Quotes.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Entertainment.Quotes;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Message}"/>
    /// to seed the Messaging tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IQuoteDbContextSeeder : IHasXActLibDbContextSeeder<Quote>
    { 

    }
}