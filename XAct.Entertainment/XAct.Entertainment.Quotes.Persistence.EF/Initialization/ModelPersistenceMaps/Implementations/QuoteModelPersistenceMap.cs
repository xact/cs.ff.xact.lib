namespace XAct.Entertainment.Quotes.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Entertainment.Quotes.Initialization.DbContextSeeders;

    public class QuoteModelPersistenceMap : EntityTypeConfiguration<Quote>, IQuoteModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteModelPersistenceMap" /> class.
        /// </summary>
        public QuoteModelPersistenceMap()
        {
            this.ToXActLibTable("Quote");


            this.HasKey(t => t.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this
                .Property(m => m.Source)
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.SourceAdditionalInfo)
                .HasMaxLength(2048)//2048 is (25lx80c).
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Text)
                //(50lines x 80chars) Should be sufficient. Any longer and you're waffling. They want Help Not Tolstoy. 
                .DefineOptional4000CharText(colOrder++)
                ;



        }
    }
}