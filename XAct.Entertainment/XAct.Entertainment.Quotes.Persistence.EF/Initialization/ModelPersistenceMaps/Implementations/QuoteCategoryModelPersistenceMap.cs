namespace XAct.Entertainment.Quotes.Initialization.ModelPersistenceMaps.Implementations
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using XAct.Data;

    public class QuoteCategoryModelPersistenceMap : ReferenceDataPersistenceMapBase<QuoteCategory,Guid>, IQuoteCategoryModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteModelPersistenceMap" /> class.
        /// </summary>
        public QuoteCategoryModelPersistenceMap():base("QuoteCategory",DatabaseGeneratedOption.None)
        {

        }
    }
}