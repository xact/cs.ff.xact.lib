namespace XAct.Entertainment.Quotes.Tests
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class QuoteServiceUnitTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Can_Get_QuoteService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IQuoteService>();

            Assert.IsNotNull(service);
        }


        [Test]
        public void Can_Get_QuoteManagementService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IQuoteManagementService>();

            Assert.IsNotNull(service);
        }

        [Test]
        public void Can_Get_Quote()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IQuoteService>();

            var quote = service.GetQuote();

            Assert.IsNotNull(quote);
        }

        [Test]
        public void Can_Get_Quote_With_Text()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IQuoteService>();

            var quote = service.GetQuote();

            Assert.IsNotNullOrEmpty(quote.Text);
        }


        [Test]
        public void Can_Get_Quote_Text()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IQuoteManagementService>();

            var quote = service.GetQuote();
            Console.WriteLine(quote.Text);

            Assert.IsNotNullOrEmpty(quote.Text);
        }
        [Test]
        public void Can_Get_5_Quote_Text()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IQuoteManagementService>();

            Quote quote=null;
            for (int i = 0; i < 5; i++)
            {
                quote = service.GetQuote();
                Console.WriteLine(quote.Text);
            }

            Assert.IsNotNullOrEmpty(quote.Text);
        }

        [Test]
        public void Can_Get_Random_Quotes()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IQuoteManagementService>();

            Quote last = null;
            int score = 0;
            for (int i = 0; i < 100; i++)
            {
                var quote = service.GetQuote();
                if (last != null)
                {
                    if (quote.Text == last.Text)
                    {
                        score++;
                    }

                }
                last = quote;
            }

            //It's possible that *some* were the same as the previous one, but highly suspicious if 
            //3% were the same...
            Assert.IsTrue(score<3);
        }

    }
}


