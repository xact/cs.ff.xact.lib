
namespace XAct.Entertainment.Quotes
{

    /// <summary>
    /// Service to render a random quote
    /// </summary>
    public interface IQuoteService: IHasXActLibService
    {
#pragma warning disable 1591
        Quote GetQuote();
#pragma warning restore 1591
    }
}
