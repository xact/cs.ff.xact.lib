namespace XAct.Entertainment.Quotes
{
    using System;

    /// <summary>
    /// Contract for a singleton configuration object used to configure
    /// the <see cref="IQuoteManagementService"/>
    /// 
    /// </summary>
    public interface IQuoteManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// The duration to cache quotes
        /// before re-querying the datastore.
        /// <para>
        /// The default duration is 15 minutes.
        /// </para>
        /// </summary>
        TimeSpan CacheDuration { get; set; }
    }
}