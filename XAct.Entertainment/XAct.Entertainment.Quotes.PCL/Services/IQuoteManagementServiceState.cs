namespace XAct.Entertainment.Quotes
{
    using System;

    /// <summary>
    /// Contract for a singleton configuration object shared 
    /// between instances of <see cref="IQuoteManagementService"/>
    /// </summary>
    public interface IQuoteManagementServiceState :IHasXActLibServiceState
    {
        /// <summary>
        /// Gets or sets the UTC DateTime of the last time the <see cref="Cache"/>
        /// was refreshed.
        /// </summary>
        DateTime LastFetched { get; set; }


        /// <summary>
        /// Gets or sets the Cache of <see cref="Quote"/>s.
        /// </summary>
        Quote[] Cache { get; set; }
    }
}