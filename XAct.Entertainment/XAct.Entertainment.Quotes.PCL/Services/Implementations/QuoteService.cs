namespace XAct.Entertainment.Quotes.Services.Implementations
{

    /// <summary>
    /// An implementation of the <see cref="IQuoteService"/>
    /// </summary>
    public class QuoteService : IQuoteService
    {
        private readonly IQuoteManagementService _quoteManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteService"/> class.
        /// </summary>
        /// <param name="quoteManagementService">The quote management service.</param>
        public QuoteService(IQuoteManagementService quoteManagementService)
        {
            _quoteManagementService = quoteManagementService;
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns></returns>
        public Quote GetQuote()
        {
            var result = _quoteManagementService.GetQuote();

            return result;
        }
    }
}