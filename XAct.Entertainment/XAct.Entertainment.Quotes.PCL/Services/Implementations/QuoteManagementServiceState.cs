namespace XAct.Entertainment.Quotes.Services.Implementations
{
    using System;

    /// <summary>
    /// Implementation of the <see cref="IQuoteManagementServiceState"/>
    /// contract.
    /// </summary>
    public class QuoteManagementServiceState : IQuoteManagementServiceState
    {
        /// <summary>
        /// Gets or sets the UTC DateTime of the last time the <see cref="Cache" />
        /// was refreshed.
        /// </summary>
        public DateTime LastFetched { get; set; }
        /// <summary>
        /// Gets or sets the Cache of <see cref="Quote" />s.
        /// </summary>
        public Quote[] Cache { get; set; }
    }
}