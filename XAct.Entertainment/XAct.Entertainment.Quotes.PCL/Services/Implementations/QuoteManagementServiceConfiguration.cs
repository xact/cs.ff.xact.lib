namespace XAct.Entertainment.Quotes.Services.Implementations
{
    using System;

    /// <summary>
    /// An implementation of the <see cref="IQuoteManagementServiceConfiguration"/>
    /// </summary>
    public class QuoteManagementServiceConfiguration : IQuoteManagementServiceConfiguration
    {
        /// <summary>
        /// The duration to cache quotes
        /// before re-querying the datastore.
        /// <para>
        /// The default duration is 15 minutes.
        /// </para>
        /// </summary>
        public TimeSpan CacheDuration { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteManagementServiceConfiguration"/> class.
        /// </summary>
        public QuoteManagementServiceConfiguration()
        {
            CacheDuration = TimeSpan.FromMinutes(15);
        }
    }
}