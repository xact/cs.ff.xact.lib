namespace XAct.Entertainment.Quotes.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IQuoteManagementService"/>
    /// </summary>
    public class QuoteManagementService : XActLibServiceBase, IQuoteManagementService
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IDateTimeService _dateTimeService;
        private readonly IRepositoryService _repositoryService;
        private readonly IQuoteManagementServiceState _quoteManagementServiceState;
        private readonly IQuoteManagementServiceConfiguration _quoteManagementServiceConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuoteManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="quoteManagementServiceState">State of the quote management service.</param>
        /// <param name="quoteManagementServiceConfiguration">The quote management service configuration.</param>
        /// <param name="repositoryService">The repository service.</param>
        public QuoteManagementService(ITracingService tracingService,

                                      IEnvironmentService environmentService,
                                      IDateTimeService dateTimeService,
                                      IQuoteManagementServiceState quoteManagementServiceState,
                                      IQuoteManagementServiceConfiguration quoteManagementServiceConfiguration,
                                      IRepositoryService repositoryService
            )
            : base(tracingService)
        {
            _environmentService = environmentService;
            _dateTimeService = dateTimeService;
            _repositoryService = repositoryService;
            _quoteManagementServiceState = quoteManagementServiceState;
            _quoteManagementServiceConfiguration = quoteManagementServiceConfiguration;
        }

        /// <summary>
        /// Gets the next random quote.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Quote GetQuote()
        {

            RefreshQuoteCacheAsNeeded();

            if (_quoteManagementServiceState.Cache.Length == 0)
            {
                return null;
            }

            int position = _environmentService.Random.Next(_quoteManagementServiceState.Cache.Length-1);
            Quote quote = _quoteManagementServiceState.Cache[position];

            return quote;
        }

        private void RefreshQuoteCacheAsNeeded()
        {
            if (_dateTimeService.NowUTC >
                _quoteManagementServiceState.LastFetched.Add(_quoteManagementServiceConfiguration.CacheDuration))
            {
                lock (_quoteManagementServiceState)
                {
                    List<Quote> results = new List<Quote>();

                    int count = _repositoryService.Count<Quote>();

                    for (int i = 0; i < 10; i++)
                    {
                        int index = _environmentService.Random.Next(count/10);

                        var tmp = _repositoryService
                                    .GetByFilter<Quote>(
                                        x => x.Enabled == true,
                                        null,
                                        new PagedQuerySpecification(index, 10),
                                        y=>y.OrderBy(o=>o.Id)
                                        )
                                        
                                        .ToArray();


                        //tmp.Shuffle();
                        results.Add(tmp);
                    }

                    _quoteManagementServiceState.Cache = results.ToArray();
                    _quoteManagementServiceState.Cache.Shuffle();


                    _quoteManagementServiceState.LastFetched = _dateTimeService.NowUTC;
                }
            }
        }
    }
}