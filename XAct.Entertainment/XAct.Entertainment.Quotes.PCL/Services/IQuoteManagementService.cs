namespace XAct.Entertainment.Quotes
{
    /// <summary>
    /// Service to manage Quotes.
    /// </summary>
    public interface IQuoteManagementService : IHasXActLibService
    {
        /// <summary>
        /// Gets the next random quote.
        /// </summary>
        Quote GetQuote();
    }
}