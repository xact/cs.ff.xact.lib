using System;

namespace XAct.Entertainment.Quotes
{
    using System.Runtime.Serialization;


    /// <summary>
    /// The datastore model used to persist and retrieve a single quote.
    /// </summary>
    [DataContract]
    public class Quote : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasText, IHasEnabled
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [DataMember]
        public virtual string Text { get; set; }

        /// <summary>
        /// Gets or sets the source/author.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        [DataMember]
        public virtual string Source { get; set; }

        /// <summary>
        /// Gets or sets optional additional info regarding the Source/Author.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        [DataMember]
        public virtual string SourceAdditionalInfo { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="Quote"/> class.
        /// </summary>
        public Quote()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            //IMPORTANT:
            //This is one of the very few cases where I don't use 
            //this.GenerateDistributedId();
            //The reason being that the DistributedId is timebased
            //whereas the default one is non-sequential
            //which is better for randomness:
            this.Id = Guid.NewGuid();

            this.Enabled = true;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
    }
}
