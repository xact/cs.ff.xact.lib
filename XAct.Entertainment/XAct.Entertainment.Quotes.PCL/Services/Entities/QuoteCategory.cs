namespace XAct.Entertainment.Quotes
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Messages;

    /// <summary>
    /// The Category of the quote.
    /// <para>
    /// Examples might be: Science, Love, Politics, etc.
    /// </para>
    /// </summary>
    [DataContract]
    public class QuoteCategory : ReferenceDataGuidIdBase, IHasXActLibEntity
    {
        
    }
}