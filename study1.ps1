﻿
function RecurseUpForParentFile ($folderPath, $filter="*.csproj", $maxIterations=8, $iteration=0) {
    # if we reached to top, get out:

    if ($iteration -ge $maxIterations){
        return $null;
    }
    # if we've gone too high, get out:
    if ((Test-Path $folderPath) -ne $True){
        return $null;
    }

    # look for files in the current folder that match the filter
    $result = Get-ChildItem $folderPath -filter $filter

    # if we found one, awesome...get the first
    if ($result -ne $null){
        return $result;
    }

    # recurse in higher folder:
    $parentFolderPath = Split-Path $folderPath -parent
    RecurseUpForParentFile $parentFolderPath $filter $maxIterations (1+$iteration);

}

function FindAllProjectsMentionedInSolutionFile($solutionFileObject){

    Write-Host("Scanning $($solutionFileObject.Name) For projects...");

    # ensure the given path is an object: 
    $solutionFileObject = get-item $solutionFileObject;

    Write-Host("Searching for all Projects mentioned in $($solutionFileObject.FullName)");


    # use regex to get the ref to projects:
    # Note that the *.sln contains lines like: 
    # Project("{2150E333-8FDC-42A3-9474-1A3956D46DE8}") = "UNITTESTS", "UNITTESTS", "{AE893EBB-3393-40F5-9C85-2726CBCB99F3}"
    # this gets the file partial paths:
    $tmpResults = get-content $solutionFileObject | 
        ? { $_ -match "^Project" } | 
        %{ $_ -match ".*=(.*)$" | out-null ; $matches[1] } | 
        %{ $_.Split(",")[1].Trim().Trim('"') }

    # but it's not good enough...the paths are partial.

    # look through
    $results = New-Object System.Collections.ArrayList;
    foreach($line in $tmpResults){
        # correct each line, by prepending with the directory of the solution
        # the project file was found within...making it a full path:
        $line2 = join-path $solutionFileObject.Directory $line
        if ((Test-Path $line2) -eq $False){
            Write-Host("...Mentioned in *.sln file, but could not find: " + $line2) -ForegroundColor gray;
            continue;
        }
        #Write-Host ("...Found:" + $line2) -ForegroundColor Gray;
        $results += get-item($line2);
    }
    Write-Host("Returning $($results.Length) Resolved Preject References in Solution, from original $($tmpResults.Count)");

    return $results;
}

function BuildDictionaryOfProjects($fullProjectList){

    if ($Global:ProjectRefs -eq $null){
        $Global:ProjectRefs =  @{};
    }

    $dict = $Global:ProjectRefs;

    $totalProjectRef =0;
    for ($i=0; $i -lt $fullProjectList.Count; $i++){
        
        $projectFileObject = $fullProjectList[$i];

        # Load up the Xml within the *.csproj file:
        [xml]$doc = Get-Content $projectFileObject;

        # Tricky...need to get the namespace, or you'll continually get nothing back
        # when you SelectNodes:
        $nsmgr = New-Object System.Xml.XmlNamespaceManager -ArgumentList $doc.NameTable
        $nsmgr.AddNamespace('a',$doc.Project.GetAttribute("xmlns"))

        # Get the nodes, using the ns in the XPath:    
        $projectNodes = $doc.SelectNodes("a:Project/a:ItemGroup/a:ProjectReference", $nsmgr);

        #Note that all the nodes contain partial refs, relative to the project it was found within.
        #That's not good for comparisons. 
        #In addition, keeping them as Xml nodes means a hit to the harddrive for all subsequent calls
        #to the file. Much faster to convert them now to FileInfo objects, and store them as such.

        # convert xml nodes to more useful (and real) fileInfo object:
        $results = New-Object System.Collections.ArrayList ;#$();
        foreach($projectNode in $projectNodes){
            #get the file partial path:
            $x = $projectNode.GetAttribute("Include");
            # the path will be Relative, so make it Absolute...except that it has "..." in it.
            $relatedProjectPath = join-path $projectFileObject.Directory $x;
            # if path project target doesn't exist, can't do much with it...
            if ((Test-Path $relatedProjectPath) -eq $False){
                # do not damage:
                continue;
            }

            # path has "..." in it -- not easy to compare with, so correct that if you can:
            $relatedProjectFileObject = get-item($relatedProjectPath);

            #Now that's ok.
            $results += $relatedProjectFileObject;
        }

        # save the array of file objects against the fullname of the csproj they were found within:
        $dict.Set_Item($projectFileObject.FullName.ToLowerInvariant(), $results)

        $totalProjectRef += $results.Count;

        #Write-Host("...[$i/$($fullProjectList.Count)] Building ProjectRefs dict. Added $($projectNodes.Count) References within `"$($projectFileObject.FullName)`"" ) -ForegroundColor Gray;
    }
    Write-Host("...Found $totalProjectRef ProjectRefs within $($fullProjectList.Count) Project files.") -ForegroundColor Gray;
}




function GetProjectsOfChangedFiles($fullProjectList){ 

    # ask git to give us the list of files changed:
    $status = invoke-expression("git show --name-only --pretty=`"format:`" | sort")

    $lastObject;
    $results = New-Object System.Collections.ArrayList;

    $fileCount =0;  
    ForEach($line in $status){
    
        # test to see if line is a valid file name:
        $e = $ErrorActionPreference; $ErrorActionPreference = "silentlycontinue";
        $fileExists = (Test-Path $line);
        $ErrorActionPreference = $e;

        # some of the lines Git returns are not filenames:
        if ($fileExists -ne $True){
          Write-Host("...Git status line contains ref to file that does not exist:" ) -ForegroundColor Gray;
          Write-Host("......: `"$line`"" ) -ForegroundColor Gray;
          continue;
        }
    

        # convert string to object:
        $fileObject = get-item $line;

        $fileCount = $fileCount +1;

        # file exists, so look for *.csproj and *.sln that belongs to this code file:
        $fileDirectoryObject = $fileObject.Directory;


        #look for project file associated to the file that changed:
        $projectFileObject = RecurseUpForParentFile $fileDirectoryObject "*.csproj";

        # as we are Ordered, probably the same project as las iteration:
        if ( ($lastObject -ne $null) -and ( [string]::Compare($projectFileObject.FullName, $lastObject.Project.FullName, $true) -eq 0) ) {
          # Write-Host("...Same Project as last time, so skipping on to next file.") -ForegroundColor Gray;
          continue;
        }

        #look for solution file associated to the file that changed:
        $solutionFileObject = RecurseUpForParentFile $fileDirectoryObject "*.sln";

        # what other projects in the solution mention this project?
        # want to find all projects mentioned in the Solution (ie: $fullProjectList)
        # go through that list, opening each one, looking for projects that reference this 
        # path.
        # if found, repeat going through fullprojectlist, 
        # looking for projects that reference that new item
        # until none found.
        # return list.



        $myObject = New-Object System.Object
        $myObject | Add-Member -type NoteProperty -name "File" -Value $fileObject;
        $myObject | Add-Member -type NoteProperty -name "Project" -Value $projectFileObject;
        $myObject | Add-Member -type NoteProperty -name "Solution" -Value $solutionFileObject;

        #save for next iteration
        $lastObject = $myObject;

        $results += $myObject;

        #Write-Host("...Changed File: " + $lastObject.File.FullName) -ForegroundColor Gray;
        #Write-Host("...Changed File Proj: " + $lastObject.Project.FullName) -ForegroundColor Gray;
        #Write-Host("...Changed File Sln: " + $lastObject.Solution.FullName) -ForegroundColor Gray;
    

    }

    Write-Host("There were $($status.Count) Status Lines,($($fileCount) of them files), but they all belong to only $($results.Count) Projects:") -ForegroundColor White; 

    foreach($myObject in $results){
        Write-Host("...: " + $myObject.Project.Name) -ForegroundColor Gray;
    }
    

    # Do this in second pass as it makes it clearer what projects we are working with:

     
    Write-Host("Iterating through [$($results.Count)] of them looking for references to them: Begin...") -ForegroundColor Gray;;

    for ($i=0; $i -lt $results.Count; $i++){
        $myObject = $results[$i];

        # what other projects does this project references/depends on (higher up the chain)?
        #$dependencies = FindAllProjectFileObjectsMentionedInProjectFile($projectFileObject);
        #$dependencies = $dependencies | Sort-Object -Property FullName -Unique 


        Write-Host("...[$i/$($results.Count)] Looking for projects that reference: `"$($myObject.Project)`"");
        $affected = FindAllProjectFileObjectsMentioningGivenProjectFileObject $myObject.Project $fullProjectList;

        $myObject | Add-Member -type NoteProperty -name "Dependencies" -Value $dependencies;
        $myObject | Add-Member -type NoteProperty -name "Affected" -Value $affected;
    }
 
    Write-Host("Iterating through [$($results.Count)] of them looking for references to them: Done.");

    return $results;
}






function GetProjectNodesWithinProjectFile($projectFileObject, $referencedRrojectFileObject=$null){
 
    #Write-Host("Reading Xml from: " + $projectFileObject.FullName);


    $dict = $Global:ProjectRefs;

    if ($dict.Contains($projectFileObject.FullName.ToLowerInvariant()) -eq $False){
        # MOST Project files should have been coming in from the original Solution file...
        # BUT there may be an edge case where you have to do the work now....
        Write-Host "Having to parse through $($projectFileObject.FullName)"  -ForegroundColor Red
        # Load up the Xml within the *.csproj file:
        [xml]$doc = Get-Content $projectFileObject;

        # Tricky...need to get the namespace, or you'll continually get nothing back
        # when you SelectNodes:
        $nsmgr = New-Object System.Xml.XmlNamespaceManager -ArgumentList $doc.NameTable
        $nsmgr.AddNamespace('a',$doc.Project.GetAttribute("xmlns"))

        # Get the nodes, using the ns in the XPath:    
        $projectNodes = $doc.SelectNodes("a:Project/a:ItemGroup/a:ProjectReference", $nsmgr);

        # convert xml nodes to more useful (and real) fileInfo object:
        $results = $();
        foreach($projectNode in $projectNodes){
            #get the file partial path:
            $x = $projectNode.GetAttribute("Include");
            # the path will be Relative, so make it Absolute...except that it has "..." in it.
            $relatedProjectPath = join-path $projectFileObject.Directory $x;
            # if path project target doesn't exist, can't do much with it...
            if ((Test-Path $relatedProjectPath) -eq $False){
                # do not damage:
                continue;
            }
            # path has "..." in it -- not easy to compare with, so correct that if you can:
            $relatedProjectFileObject = get-item($relatedProjectPath);

            #Now that's ok.
            $results += $relatedProjectFileObject;
        }
        $dict.Set_Item($projectFileObject.FullName.ToLowerInvariant(),$results);

    }else{
        $projectNodes = $dict.Get_Item($projectFileObject.FullName.ToLowerInvariant());
    }


    # non-typical behaviour
    # we don't want to return null -- always convert to an array of projectNodes
    # with complete paths:
    
    $results = New-Object System.Collections.ArrayList ;
    $totalProjects = $projectNodes.Count;
    foreach($relatedProjectFileObject in $projectNodes){

        # now that we have absolute paths:
        # if given a filter, we filter out any that do not match:
        if ($referencedRrojectFileObject -ne $null){
            
            if ([string]::Compare($relatedProjectFileObject.FullName, $referencedRrojectFileObject.FullName, $True) -ne 0){
                # what ever this include is pointing at, it's not the project in question
                # so move on to next:
                # Write-Host ("...filtered out as they don't match: `"$($relatedProjectFileObject.FullName.ToLowerInvariant())`" != `"$($referencedRrojectFileObject.FullName.ToLowerInvariant())`"") -ForegroundColor gray;
                continue;
            }
            #Write-Host "...Match: `"$($relatedProjectFileObject.Name.ToLower())`" == `"$($referencedRrojectFileObject.Name.ToLower())`"" -ForegroundColor Gray;
        }
        # add to the list 
        $results += $relatedProjectFileObject;# ) | Out-Null ;
    }
    

    # return an array of xml nodes, for someone else to proccess:
    if ($results.Count -gt 0){
       #Write-Host "...Found 'Project[@Include]' statements: [found: $totalProjects, returning: $($results.Count)]" -ForegroundColor gray;
    }

    return $results;
}






function FindAllProjectFileObjectsMentionedInProjectFile($projectFileObject, $maxIterations=3,  $iteration=0) {

    if ($iteration -ge $maxIterations){
        return $null;
    }

    Write-Host("Finding All Referenced Projects Mentioned in this Project:" + $projectFileObject);
    Write-Host("...iteration: $iteration < Max:$maxIterations");

    $results =New-Object System.Collections.ArrayList;

    $projectNodes = GetProjectNodesWithinProjectFile $projectFileObject;
    Write-Host("...Found: $($projectNodes.Count) project nodes in the *.csproj file");

    # Go through each node, and if it's valid (should be...)
    # convert to a File Object:
    foreach($projectNode in $projectNodes){
        

        $x = $projectNode.GetAttribute("Include");
        $relatedProjectPath = join-path $projectFileObject.Directory $x;


        if ((Test-Path $relatedProjectPath) -eq $True){

            $relatedProjectFileObject = get-item($relatedProjectPath);
            Write-Host("...Include: $($relatedProjectFileObject.Name)");

            $results += $relatedProjectFileObject;

            #$relatedArray = FindAllProjectFileObjectsMentionedInProjectFile $relatedProjectFileObject $maxIterations (1+$iteration);

            if ($relatedArray -ne $null){
#                $relatedArray = $relatedArray | Sort-Object -Property FullName -Unique 
#                 $results += $relatedArray;
#                $results = $results | Sort-Object -Property FullName -Unique 
            }

        }
    }
    #Got that...
    # but not unique
    
    $results = $results | Sort-Object -Property FullName -Unique 
    return $results;
}



# get a list of all projects that if this project were recompiled, 
# they too have to be recompiled.
function FindAllProjectFileObjectsMentioningGivenProjectFileObject($projectFileObject, $fullProjectList, $maxIterations=5,  $iteration=0) {

    #Write-Host ("...[Iteration: $iteration, Max: $maxIterations, Looking for references to `"$($projectFileObject.Name)`"]") -ForegroundColor "gray";

    if ($iteration -ge $maxIterations){
        Write-Host "...At Max Iteration. Exiting recursion." -ForegroundColor DarkMagenta;
        return $null;
    }


    $results = New-Object System.Collections.ArrayList;

 
    # iterate through all projects in the solution, 
    # looking for Projects that mention the given $projectFileObject:
    foreach($fullProjectListItem in $fullProjectList){

        # Write-Host ("[Iteration: $iteration, Max: $maxIterations, Looking in `"$($fullProjectListItem.Name)`" for references to `"$($projectFileObject.Name)`"]") -ForegroundColor "gray";
        
        # get all nodes in the solution, that mention this project:
        $affectedProjectNodes = GetProjectNodesWithinProjectFile ($fullProjectListItem) $projectFileObject;


        if ($affectedProjectNodes.Count -eq 0) {
          #Write-Host "...: `"$($fullProjectListItem.Name)`"  had no reference to `"$($projectFileObject.Name)`"" -ForegroundColor "gray";
          continue;
        }

        # Note: can safely discard, as $affectedProjectNodes would have 
        #had only one node in it anyway, pointing at $fullProjectListItem.
        $affectedProjectNodes = $null;

        # add the node to results:
        # Write-Host("...Found, and adding to Results array: `"$($fullProjectListItem.Name)`".  It depends on this `"$($projectFileObject.Name)`"...");
        $results += $fullProjectListItem;

        # but we also want to know all projects that reference this project as well:
        # so iterate deeper, looking for projects that depend on this project...

        #Write-Host "...and iterating deeper looking for references to: $($fullProjectListItem.Name)" -ForegroundColor "gray";

        $subresults = FindAllProjectFileObjectsMentioningGivenProjectFileObject $fullProjectListItem $fullProjectList $maxIterations (1+$iteration);

        if ($subresults -eq $null){
            continue;
        }

        # Write-Host("...Adding $($subresults.Count) nodes...");
        $results += $subresults;
    }

    $results = $results | Sort-Object -Property FullName -Unique 
    
    if ($iteration -eq 0) {
        if ($results.Count -eq 0){
            #Write-Host("...Projects that reference `"$($projectFileObject.Name)`" are: None.") -ForegroundColor "gray" -b DarkRed;
        }
        else{
            #Write-Host("...Projects that reference `"$($projectFileObject.Name)`" are:") -ForegroundColor "gray" -b DarkRed;
            foreach($ref in $results){
                # Write-Host("......: `"$($ref.Name)`"")  -ForegroundColor "gray" -BackgroundColor DarkRed;
            }
        }
    }
    return $results;
}



function GetAffectedProjects($solutionFilePath){

    $solutionFilePath = get-item($solutionFilePath);


    $fullProjectList = FindAllProjectsMentionedInSolutionFile($solutionFilePath);

    BuildDictionaryOfProjects($fullProjectList);

    $projectSummaries = GetProjectsOfChangedFiles($fullProjectList);

    # Write-Host("[$($projectSummaries.Count)] Sumaries:") -ForegroundColor "gray" -b DarkRed;

    $results = New-Object System.Collections.ArrayList;


    ForEach($x in $projectSummaries){

        # no idea how it makes 19...but gives back 20. Bug from hell.
        #if ($projectSummaries[0].Project -eq $null){
        #  continue;
        #}

        $results += $x.Affected;


        #    if ($x.Affected.Count -eq 0){
        #        Write-Host("[$($x.Affected.Count)] Projects that reference `"$($x.Project.Name)`".") -ForegroundColor "gray" -b DarkRed;
        #    }
        #    else{
        #        Write-Host("[$($x.Affected.Count)] Projects that reference `"$($x.Project.Name)`":") -ForegroundColor "gray" -b DarkRed;
        #        foreach($ref in $x.Affected){
        #             Write-Host("......: `"$($ref.Name)`"")  -ForegroundColor "gray" -BackgroundColor DarkRed;
        #        }
        #    }

    }

    $results = $results | Sort-Object -Property FullName -Unique 


    return $results;

}


function GetAffectedNugetProjects($solutionFilePath){

    $solutionFilePath = get-item($solutionFilePath);

    $affectedProjects = GetAffectedProjects ($solutionFilePath);

    $results = New-Object System.Collections.ArrayList;
    # we only want projects that are backed with a mirroring *.nuspec
    foreach($affectedProject in $affectedProjects){

     $a = [System.IO.Path]::GetDirectoryName($affectedProject.FullName);
     $b = [System.IO.Path]::GetFileNameWithoutExtension($affectedProject.FullName)
     $c = ".nuspec";

     $nuspec = [System.IO.Path]::Combine($a,$b+$c);

    if ((Test-Path $nuspec) -eq $True){
      $results+=$affectedProject;
    }
  }
  return $results;
}


