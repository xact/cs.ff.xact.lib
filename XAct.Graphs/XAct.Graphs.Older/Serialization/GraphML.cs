﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace XAct.Graphs.Serialization {



	

	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// <para>
	/// For the a primer on GraphML <see href="http://graphml.graphdrawing.org/primer/graphml-primer.html">this</see>.
	/// </para>
	/// <para>
	/// This XmlDocument based class is only suitable for small graphs.
	/// </para>
	/// </remarks>
	public class GraphML {



	XmlDocument _XmlDoc;


		public static XmlDocument CreateBlankGraphMLDocument(bool includeXSD) {



			XmlNameTable nt = new NameTable();
			XmlNamespaceManager ns = new XmlNamespaceManager(nt);
			XmlDocument xmlDoc = new XmlDocument(nt);


			string defaultNSUri = "http://graphml.graphdrawing.org/xmlns";

			XmlElement rootElement = xmlDoc.CreateElement("graphml", defaultNSUri);
			xmlDoc.AppendChild(rootElement);

			if (includeXSD) {
				AppendAttribute(rootElement, "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
				AppendAttribute(rootElement, "xsi:schemaLocation", "http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd");
			}
			return xmlDoc;
		}

		/// <summary>
		/// Registers a Key prior to serializing the Graph.
		/// </summary>
		/// <param name="xmlDocument"></param>
		/// <param name="keyFor"></param>
		/// <param name="keyAlias"></param>
		/// <param name="keyName"></param>
		/// <param name="keyType"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static XmlElement DefineKey(XmlDocument xmlDocument, GraphMLKeyFor keyFor, string keyAlias, string keyName, GraphMLKeyDataType keyType, string defaultValue) {
			if (xmlDocument == null) {
				throw new System.ArgumentNullException("xmlDocument");
			}
			if (string.IsNullOrEmpty(keyAlias)) {
				throw new System.ArgumentNullException("keyAlias");
			}
			if (string.IsNullOrEmpty(keyName)) {
				throw new System.ArgumentNullException("keyName");
			}

			XmlElement element = xmlDocument.CreateElement("key");
			AppendAttribute(element, "id", keyAlias);
			AppendAttribute(element, "for", keyFor.ToString());
			AppendAttribute(element, "attr.name", keyName);
			AppendAttribute(element, "attr.type", keyType.ToString().Substring(1));

			xmlDocument.DocumentElement.AppendChild(element);

			if (!string.IsNullOrEmpty(defaultValue)) {
				XmlElement childElement = xmlDocument.CreateElement("default");
				childElement.InnerText = defaultValue;
				element.AppendChild(childElement);
			}
			return element;
		}

		public static XmlElement DefineGraph(XmlDocument xmlDocument, string graphId, string edgeDefault) {
			if (xmlDocument == null); {
				throw new System.ArgumentNullException("xmlDocument");
			}
			if (graphId == null) {
				throw new System.ArgumentNullException("graphId");
			}
			if (string.IsNullOrEmpty(edgeDefault)) { edgeDefault = "directed"; }

			XmlElement element = xmlDocument.CreateElement("graph");
			AppendAttribute(element,"id", graphId);
			AppendAttribute(element, "edgeDefault", edgeDefault);

			xmlDocument.DocumentElement.AppendChild(element);
			return element;
		}

		

		public static XmlElement DefineEdge(XmlElement graphNode, string sourceId, string targetId, bool directed) {
			XmlElement element = DefineEdge(graphNode, sourceId, targetId);
			AppendAttribute(element, "directed", directed.ToString().ToLower());
			return element;
		}

		public static XmlElement DefineEdge(XmlElement graphNode, string sourceId, string targetId) {
			if (graphNode == null) {
				throw new System.ArgumentNullException("graphNode");
			}
			if (graphNode.Name != "graph") {
				throw new System.ArgumentException("Node is not of type 'graph'");
			}
			if (string.IsNullOrEmpty(sourceId)) {
				throw new System.ArgumentNullException("sourceId");
			}
			if (string.IsNullOrEmpty(targetId)) {
				throw new System.ArgumentNullException("targetId");
			}

			XmlDocument xmlDoc = graphNode.OwnerDocument;
			XmlElement element = xmlDoc.CreateElement("edge");
			AppendAttribute(element, "source", sourceId);
			AppendAttribute(element, "target", targetId);

			graphNode.AppendChild(element);

			return element;
		}

		public static XmlElement DefineVertex(XmlElement graphNode, string vertexId) {
			if (graphNode == null) {
				throw new System.ArgumentNullException("graphNode");
			}
			if (graphNode.Name != "graph") {
				throw new System.ArgumentException("Node is not of type 'graph'");
			}
			if (string.IsNullOrEmpty(vertexId)) {
				throw new System.ArgumentNullException("vertexId");
			}

			XmlDocument xmlDoc = graphNode.OwnerDocument;
			XmlElement element = xmlDoc.CreateElement("node");
			AppendAttribute(element, "id", vertexId);

			graphNode.AppendChild(element);

			return element;
		}
		public static void AttachDataToElement(XmlElement element, Dictionary<string, string> valuePairs) {
			if (element == null) {
				throw new System.ArgumentNullException("element");
			}
			if (valuePairs == null) {
				throw new System.ArgumentNullException("valuePairs");
			}
			foreach (System.Collections.Generic.KeyValuePair<string, string> pair in valuePairs) {
				XmlElement childElement = element.OwnerDocument.CreateElement(pair.Key);
				childElement.InnerText = pair.Value;
				element.AppendChild(childElement);
			}
		}

		public static XmlElement AttachDataToElement(XmlElement element, string dataKey, string dataValue) {
			if (element == null) {
				throw new System.ArgumentNullException("v");
			}
			if (string.IsNullOrEmpty(dataKey)) {
				throw new System.ArgumentNullException("dataKey");
			}
			XmlElement childElement = element.OwnerDocument.CreateElement(dataKey);
			childElement.InnerText = dataValue;
			element.AppendChild(childElement);

			return element;
		}


		protected static XmlAttribute AppendAttribute(XmlElement element, string name, string value) {
			if (element == null) {
				throw new System.ArgumentNullException("element");
			}
			if (string.IsNullOrEmpty(name)) {
				throw new System.ArgumentNullException("name");
			}
			if (value == null) { value = string.Empty; }

			XmlDocument xmlDoc = element.OwnerDocument;

			XmlAttribute attribute = xmlDoc.CreateAttribute(name);
			attribute.Value = value;
			element.SetAttributeNode(attribute);

			return attribute;
		}

	}//Class:End
}//Namespace:End
