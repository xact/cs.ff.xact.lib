﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// Event Args package for serialization/deserialization events to do with vertices.
	/// </summary>
	public class GraphMLNodeEventArgs<TVertex> : GraphMLItemEventArgs<TVertex> {

		/// <summary>
		/// Gets the vertex.
		/// </summary>
		/// <value>The vertex.</value>
		public TVertex Vertex {
			get { return _Item; }
		}

		#region Constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphMLNodeEventArgs{TNode}"/> class.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		public GraphMLNodeEventArgs(TVertex vertex)
			: base(vertex,false) {
		}
		#endregion
	}
}
