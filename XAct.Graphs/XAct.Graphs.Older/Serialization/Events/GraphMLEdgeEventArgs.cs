﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// Event Args package for serialization/deserialization events to do with vertices.
	/// </summary>
	public class GraphMLEdgeEventArgs<TEdge> : GraphMLItemEventArgs<TEdge> {

		/// <summary>
		/// Gets the edge.
		/// </summary>
		/// <value>The edge.</value>
		public TEdge Edge {
			get { return _Item; }
		}



		#region Constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphMLNodeEventArgs{TNode}"/> class.
		/// </summary>
		/// <param name="edge">The edge.</param>
		public GraphMLEdgeEventArgs(TEdge edge)
			: base(edge, false) {
		}
		#endregion
	}
}
