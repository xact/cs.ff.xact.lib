﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// Abstract base class for 
	/// <see cref="GraphMLNodeEventArgs{TNode}"/> 
	/// and
	/// <see cref="GraphMLEdgeEventArgs{TEdge}"/> 
	/// </summary>
	/// <typeparam name="TItem"></typeparam>
	abstract public class GraphMLItemEventArgs<TItem> : EventArgs 
	{

		/// <summary>
		/// The generic protected item.
		/// </summary>
		protected TItem _Item;

		/// <summary>
		/// Gets the data associated with the Item.
		/// To be filled in by the event handlers of VertexSerializing and EdgeSerializing.
		/// </summary>
		/// <value>The data.</value>
		public Dictionary<string, string> Data {
			get {
				return _Data;
			}
		}
		private Dictionary<string, string> _Data = new Dictionary<string, string>();

		/// <summary>
		/// Initializes a new instance of the <see cref="GraphMLItemEventArgs&lt;TItem&gt;"/> class.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="allowDefault">if set to <c>true</c> [allow default].</param>
		public GraphMLItemEventArgs(TItem item, bool allowDefault) {
			if ((!allowDefault) && (item == null)) {
				throw new System.ArgumentNullException("item");
			}
			
			_Item = item;
		}

	}//Class:End
}
