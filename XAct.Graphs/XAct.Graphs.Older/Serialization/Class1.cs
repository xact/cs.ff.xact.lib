﻿
using System;
using System.Collections.Generic;
using System.Text;

using System.Xml;
using System.Xml.XPath;

using XAct.Data;


namespace XAct.Graphs.Serialization {
	public class XmlPersistance {
		const string C_TAG_VERTEXID = "vertexId";
		const string C_TAG_RECORDID = "recordId";
		const string C_TAG_DATASOURCEID = "dataSourceId";
		string[] _ReservedVertexAttributeTags = new string[] { C_TAG_VERTEXID, "weight", C_TAG_DATASOURCEID, C_TAG_RECORDID, "created", "edited", "sync", "linkTo" };

		protected XmlDocument _XmlDoc;
		protected XmlElement _XmlRootNode;
		XmlNamespaceManager _XmlNameSpaceManager;

		#region Properties
		public string FileName {
			get { return _FileName; }
		}
		protected string _FileName;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="XmlPersistance"/> class.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		public XmlPersistance(string fileName) {
		}

		#endregion

		public void ProcessDocument() {


			_XmlDoc = new XmlDocument();
			_XmlDoc.Load(_FileName);
			_XmlRootNode = _XmlDoc.DocumentElement;

			_XmlNameSpaceManager = new XmlNamespaceManager(_XmlDoc.NameTable);
			_XmlNameSpaceManager.AddNamespace(string.Empty, _XmlDoc.NamespaceURI);

			ProcessDataSourceNodes();
		}
		
		
		public void ProcessDataSourceNodes() {
			string xpath = "//dataSource";
			
			XmlNodeList dataSourceGroup =  _XmlDoc.SelectNodes(xpath, _XmlNameSpaceManager);
			foreach (XmlNode dataSourceNode in dataSourceGroup) {
				if (!(dataSourceNode is XmlElement)) {
					continue;
				}
				ProcessDataSourceNode((XmlElement)dataSourceNode);
			}
		}


		protected void ProcessDataSourceNode(XmlElement dataSourceNode) {

			string dataSourceName = GetAttributeValue(dataSourceNode,"name",string.Empty);
			if (string.IsNullOrEmpty(dataSourceName)) {
				throw new System.ArgumentNullException("dataSourceNode");
			}
		 XAct.Data.DataObjectSchema  dataObjectSchema =
			 XAct.Data.SchemaManager.DataObjectSchemas.GetByDataStoreName(dataSourceName);


int dataSourceId =XAct.Data.DataObjectFactoryManager.Get(dataSourceName).Id;




			string xpath = "//record";

			XmlNodeList records = _XmlDoc.SelectNodes(xpath, _XmlNameSpaceManager);

			
			foreach (XmlNode record in records) {
				if (!(record is XmlElement)) {
					continue;
				}

				string recordId;

				string recordSql = CreateSqlFromRecordNode(dataObjectSchema, (XmlElement)record, out recordId);
				if (recordSql == null) {
					continue;
				}
				string vertexSql = CreateSqlForVertex(dataObjectSchema, (XmlElement)record, recordId);
			}
		}

		protected string CreateSqlFromRecordNode(DataObjectSchema dataObjectSchema, XmlElement recordNode, out string recordId) {
			Dictionary<string, string> columnEntries = new Dictionary<string,string>();

			CreateSqlFromRecordNodeAttributes(recordNode, ref columnEntries);
			CreateSqlFromRecordNodeChildNodes(recordNode, ref columnEntries);

			if ( columnEntries.Count == 0) {
				//No values given so might as well get out:
				recordId = string.Empty;
				return null;
			}

			string primaryKeyName = dataObjectSchema.PrimaryKey.Name;

			if (!columnEntries.ContainsKey(primaryKeyName)) {
				columnEntries[primaryKeyName] = Guid.NewGuid().ToString("N");
			}

			recordId = columnEntries[primaryKeyName];

		
			string[] keys = new string[columnEntries.Count];
			string[] values = new string[columnEntries.Count];
			columnEntries.Keys.CopyTo(keys,0);
			columnEntries.Values.CopyTo(values, 0);

			StringBuilder sb = new StringBuilder();
			sb.Append(string.Format("INSERT INTO {0}", dataObjectSchema.DataStoreName));
			sb.Append(" (");
			sb.Append(string.Join(", ", keys));
			sb.Append(")");
			sb.Append(" VALUES");
			sb.Append(string.Join(", ", values));
			return sb.ToString();

		}

		
		protected void CreateSqlFromRecordNodeChildNodes(XmlElement recordNode, ref Dictionary<string,string> columnEntries) {

			foreach (XmlNode childNode in recordNode.ChildNodes) {
				if (!(childNode is XmlElement)) {
					continue;
				}
				if (!(string.IsNullOrEmpty(childNode.InnerText))) {
					columnEntries[childNode.Name] = "'" + childNode.InnerText + "'";
				}
			}
		}

		protected void CreateSqlFromRecordNodeAttributes(XmlElement recordNode, ref Dictionary<string,string> columnEntries) {
			foreach (XmlAttribute attribute in recordNode.Attributes) {
				if (Array.Exists(_ReservedVertexAttributeTags,  
							delegate(string arrayItem) {
								return (string.Compare(arrayItem,attribute.Name,true)==0);
							}
						)) {
					//cannot use this attribute for record -- its one of the reserved names:
					continue;
				}
				string value = GetAttributeValue(attribute, string.Empty);

				if (value == string.Empty) {
					continue;
				}

				if (!(string.IsNullOrEmpty(value))) {
					columnEntries[attribute.Name]=("'" + value + "'");
				}
			}
		}

		protected string CreateSqlForVertex(DataObjectSchema dataObjectSchema, XmlElement recordNode, string recordId) {
			if (dataObjectSchema == null) {
				throw new System.ArgumentNullException("dataObjectSchema");
			}
			if (recordNode == null) {
				throw new System.ArgumentNullException("recordNode");
			}
			Dictionary<string, string> columnEntries = new Dictionary<string, string>();
			
			foreach (XmlAttribute attribute in recordNode.Attributes) {
				if (!Array.Exists(_ReservedVertexAttributeTags,  
							delegate(string arrayItem) {
								return (string.Compare(arrayItem,attribute.Name,true)==0);
							}
						)) {
					//if it is not a reserved word, we don't care about it...
					continue;
				}
				//Its one of the reserved/vertex attribute tags.
				string value = GetAttributeValue(attribute, string.Empty);
				if (!(string.IsNullOrEmpty(value))) {
					columnEntries[attribute.Name] = value;
				}
			}

			if (!columnEntries.ContainsKey(C_TAG_VERTEXID)) {
				columnEntries[C_TAG_VERTEXID] = Guid.NewGuid().ToString();
			}

			if (!columnEntries.ContainsKey(C_TAG_RECORDID)) {
				columnEntries[C_TAG_RECORDID] = recordId;
			}
			if (!columnEntries.ContainsKey(C_TAG_DATASOURCEID)) {
				columnEntries[C_TAG_DATASOURCEID] = "0";//SchemaManager.Instance.DataObjectSchemas.
			}

			string[] keys = new string[columnEntries.Count];
			string[] values = new string[columnEntries.Count];
			columnEntries.Keys.CopyTo(keys, 0);
			columnEntries.Values.CopyTo(values, 0);

			StringBuilder sb = new StringBuilder();
			sb.Append(string.Format("INSERT INTO {0}", dataObjectSchema.DataStoreName));
			sb.Append(" (");
			sb.Append(string.Join(", ", keys));
			sb.Append(")");
			sb.Append(" VALUES");
			sb.Append(string.Join(", ", values));
			return sb.ToString();


		}



		protected string GetAttributeValue(XmlElement element, string tag, string defaultValue){
			
			if (element == null){
				return defaultValue;
			}
			return GetAttributeValue(element.GetAttributeNode("tag"), defaultValue);
		}
		protected string GetAttributeValue(XmlAttribute attribute , string defaultValue) {
			if (attribute == null) {
				return defaultValue;
			}
			string result = attribute.Value;
			if (result != null) {
				result = result.Trim();
			}
			if (string.IsNullOrEmpty(result)) { result = defaultValue; }
			return result;
		}


		protected virtual void CreateVertexInfo() {
			//		record vertexId="" weight="" dataSourceId="" recordId="" created="" edited="" sync="" linkTo=""

		}

		
	}
}

