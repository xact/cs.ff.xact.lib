﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Xml;

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// Abstract base class for Deserializing a Graph.
	/// </summary>
	/// <remarks>
	/// <para>
	/// For a primer on the GraphML format,
	/// see <see href="http://graphml.graphdrawing.org/primer/graphml-primer.html"/>.
	/// </para>
	/// </remarks>
	public class GraphMLReader {


		#region Events Raised
		/// <summary>
		/// Event raised after a Graph has been fully deserialized.
		/// </summary>
		public event EventHandler GraphDeserialized;

		/// <summary>
		/// Event raised after a Key has been fully deserialized.
		/// </summary>
		public event EventHandler KeyDeserialized;

		/// <summary>
		/// Event raised after a Vertex/Node has been fully deserialized.
		/// </summary>
		public event EventHandler<GraphMLNodeEventArgs<GraphMLNode>> NodeDeserialized;

		/// <summary>
		/// Event raised after an Edge has been fully deserialized.
		/// </summary>
		public event EventHandler<GraphMLEdgeEventArgs<GraphMLEdge>> EdgeDeserialized;

		/// <summary>
		/// Event raised after a Data element of a Vertex or Edge has been fully deserialized.
		/// </summary>
		public event EventHandler DataDeserialized;
		#endregion

		#region Constants
		/// <summary>
		/// The document's root node XmlElement Name. 
		/// </summary>
		const string C_GRAPHML_NODE = "graphml";
		/// <summary>
		/// A key's XmlElement Name. 
		/// </summary>
		const string C_KEY_NAME = "key";
		/// <summary>
		/// A graph's XmlElement Name. 
		/// </summary>
		const string C_GRAPH_NAME = "graph";
		/// <summary>
		/// A node's XmlElement Name. 
		/// </summary>
		const string C_NODE_NAME = "node";
		/// <summary>
		/// A node or edge's XmlElement Name. 
		/// </summary>
		const string C_DATA_NAME = "data";
		/// <summary>
		/// An edge's XmlElement Name. 
		/// </summary>
		const string C_EDGE_NAME = "edge";
		#endregion

		/// <summary>
		/// Dictionary of registered Key's.
		/// </summary>
		Dictionary<GraphMLKeyFor, Dictionary<string, string>> _DefinedKeys_AN = new Dictionary<GraphMLKeyFor, Dictionary<string, string>>();



		/// <summary>
		/// The XmlReader used to read the xml stream.
		/// </summary>
		protected XmlTextReader _XmlReader;


		/// <summary>
		/// The vertex in question when <see cref="NodeDeserialized"/> is raised.
		/// </summary>
		public object CurrentVertex {
			get {
				return _CurrentVertex;
			}
		}
		private object _CurrentVertex;

		/// <summary>
		/// The edge in question when <see cref="EdgeDeserialized"/> is raised.
		/// </summary>
		public object CurrentEdge {
			get {
				return _CurrentEdge;
			}
		}
		private object _CurrentEdge;


		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="GraphMLReader"/> class.
		/// </summary>
		/// <param name="fileName">path to the Xml file to read.</param>
		public GraphMLReader(string fileName) {
			if (string.IsNullOrEmpty(fileName)) {
				throw new System.ArgumentNullException("fileName");
			}
			if (!System.IO.File.Exists(fileName)) {
				throw new System.ArgumentException(
					string.Format("FileName given ('{0}') does not exist.",fileName));
			}
			System.IO.StreamReader s = new System.IO.StreamReader(fileName);
			_XmlReader = new XmlTextReader(s);
			_XmlReader.MoveToContent();
			
		}
		#endregion

		#region Raise Events
		/// <summary>
		/// Raises the <see cref="E:GraphDeserialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OnGraphDeserialized(EventArgs e) {
			if (GraphDeserialized != null) {
				GraphDeserialized(this, e);
			}
		}
		/// <summary>
		/// Raises the <see cref="E:KeyDeserialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OnKeyDeserialized(EventArgs e) {
			if (KeyDeserialized != null) {
				KeyDeserialized(this, e);
			}
		}
		/// <summary>
		/// Raises the <see cref="E:NodeDeserialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OnNodeDeserialized(GraphMLNodeEventArgs<GraphMLNode> e) {
			if (NodeDeserialized != null) {
				NodeDeserialized(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:EdgeDeserialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OnEdgeDeserialized(GraphMLEdgeEventArgs<GraphMLEdge> e) {
			if (EdgeDeserialized != null) {
				EdgeDeserialized(this, e);
			}
		}
		/// <summary>
		/// Raises the <see cref="E:DataDeserialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OnDataDeserialized(EventArgs e) {
			if (DataDeserialized != null) {
				DataDeserialized(this, e);
			}
		}

		#endregion

		/// <summary>
		/// Parses the given root node of the document.
		/// </summary>
		virtual protected void ParseRootNode() {

			VerifyNodeName(C_GRAPHML_NODE);
			

			int startDepth = _XmlReader.Depth;
			MoveToNextElement();

			//We should be *within* the RootNode
			while (_XmlReader.Depth > startDepth) {
				switch (_XmlReader.Name) {
					case C_KEY_NAME:
						ExtractKey();
						break;
					case C_GRAPH_NAME:
						ExtractGraph();
						break;
					default:
						break;
				}
			}
			//But we are out of it into the next sibling (error) or parent (xml)
		}

		/// <summary>
		/// Parses the given graph node.
		/// </summary>
		virtual protected void ExtractGraph() {
			VerifyNodeName(C_GRAPH_NAME);

			Dictionary<string, string> attributes = GetElementAttributes();
			string graphId = GetElementId(attributes);
			string defaultEdgeDirection = GetElementAttributeValue(attributes,"edgeDefault",false, GraphMLDefaultEdgeDirection.directed.ToString());

			GraphMLDefaultEdgeDirection graphDirection = (GraphMLDefaultEdgeDirection)Enum.Parse(typeof(GraphMLDefaultEdgeDirection), defaultEdgeDirection);

			/*
parse.nodes="11" parse.edges="12" 
            parse.maxindegree="2" parse.maxoutdegree="3"
            parse.nodeids="canonical" parse.edgeids="free" 
            parse.order="nodesfirst">
			 */

			int startDepth = _XmlReader.Depth;
			MoveToNextElement();
			//We should be *within* the Graph node
			while (_XmlReader.Depth > startDepth) {
				switch (_XmlReader.Name) {
					case C_KEY_NAME:
						ExtractKey();
						break;
					case C_NODE_NAME:
						ExtractNode();
						break;
					case C_EDGE_NAME:
						ExtractEdge();
						break;
					default:
						break;
				}
			}
			//Raise event afterwards:
			OnGraphDeserialized(EventArgs.Empty);

			//We are now OUT of the graph into a sibling node (graph) or parent node (graphml).

			//We return to caller (ParseRootNode)
		}

		/// <summary>
		/// Parses the given key node.
		/// These <i>should</i> come before any graph nodes.
		/// </summary>
		virtual protected void ExtractKey() {

			VerifyNodeName(C_KEY_NAME);


			string keyId;
			string keyFor;
			string keyName;
			string keyType;

			Dictionary<string, string> attributes = GetElementAttributes();
			
			keyId = GetElementId(attributes);
			keyFor = GetElementAttributeValue(attributes, "for", false, "all");
			keyName = GetElementAttributeValue(attributes, "attr.name", true, null);
			keyType = GetElementAttributeValue(attributes, "attr.type", true, null);

			//Convert string to enum value:
			GraphMLKeyFor keyFor2 = (GraphMLKeyFor)Enum.Parse(typeof(GraphMLKeyFor),keyFor);

			//And use to register the Key:
			_DefinedKeys_AN[keyFor2][keyId] = keyName;

			//Raise event afterwards:
			OnKeyDeserialized(EventArgs.Empty);

			//Return to caller (ExtractGraph)

		}





		/// <summary>
		/// Parses the given vertex/node node.
		/// </summary>
		virtual protected void ExtractNode() {
			VerifyNodeName(C_NODE_NAME);


			int attributeCount = _XmlReader.AttributeCount;
			if (attributeCount == 0) {
				throw new System.Exception("A node must have atleast some attributes.");
			}

			Dictionary<string, string> attributes = GetElementAttributes();
			string vertexId = GetElementId(attributes);

			int startDepth = _XmlReader.Depth;
			MoveToNextElement();
			//We should be *within* the Vertex here:
			while (_XmlReader.Depth > startDepth) {
				switch (_XmlReader.Name) {
					case C_DATA_NAME:
						string keyName;
						string keyValue;
						ExtractDataElement(GraphMLKeyFor.node, out keyName, out keyValue);
						//dataArray[keyName] = keyValue;
						break;
					case C_GRAPH_NAME:
						ExtractGraph();
						break;
					default:
						break;
				}
			}


			//Raise event afterwards:
			OnNodeDeserialized(new GraphMLNodeEventArgs<GraphMLNode>(new GraphMLNode(vertexId, attributes)));

			//Clear the current Vertex:
			_CurrentVertex = null;
			//Return to caller (ExtractGraph)
		}

		/// <summary>
		/// Parses the given edge node.
		/// </summary>
		virtual protected void ExtractEdge() {
			VerifyNodeName(C_EDGE_NAME);


			int attributeCount = _XmlReader.AttributeCount;
			if (attributeCount == 0) {
				throw new System.Exception("A node must have atleast some attributes.");
			}

			Dictionary<string, string> attributes = GetElementAttributes();
			string sourceId = GetElementAttributeValue(attributes,"source",true,null);
			string targetId = GetElementAttributeValue(attributes, "target", true, null);

			int startDepth = _XmlReader.Depth;
			MoveToNextElement();
			//We should be *within* an Edge here:
			while (_XmlReader.Depth > startDepth) {
				switch (_XmlReader.Name) {
					case C_DATA_NAME:
						string keyName;
						string keyValue;
						ExtractDataElement(GraphMLKeyFor.edge, out keyName, out keyValue);
						//dataArray[keyName] = keyValue;
						break;
					default:
						break;
				}
			}
			//But we are out of it, into a sibling (edge or vertex), or parent (graph):
			

			//Raise event afterwards:
			OnEdgeDeserialized(new GraphMLEdgeEventArgs<GraphMLEdge>(new GraphMLEdge(sourceId, targetId, attributes)));

			//Clear the current Edge:
			_CurrentEdge = null;

			//Return to caller (ExtractGraph)
		}






		/// <summary>
		/// Parses the given vertex or edge data node.
		/// </summary>
		virtual protected void ExtractDataElement(GraphMLKeyFor keyFor, out string keyName, out string keyValue) {
			VerifyNodeName(C_DATA_NAME);


			if (_XmlReader.AttributeCount < 1) {
				throw new System.ArgumentException("Missing attributes on data.");
			}

			Dictionary<string, string> attributes = GetElementAttributes();
			string keyAlias = GetElementAttributeValue(attributes, C_KEY_NAME, true, null);
			
			//keyName;

			if (!TryGetKeyName(keyFor, keyAlias, out keyName)) {
				throw new System.Exception("Cannot find registered key for alias given.");
			}
			
			keyValue = _XmlReader.ReadContentAsString();



			int startDepth = _XmlReader.Depth;
			MoveToNextElement();
			//We should be *within* a Data here:
			while (_XmlReader.Depth > startDepth) {
				switch (_XmlReader.Name) {
					default:
						break;
				}
			}

			//Raise event afterwards:
			OnDataDeserialized(EventArgs.Empty);


		}















		/// <summary>
		/// Helper method to move to the next sibling node, without a specifying its node name.
		/// </summary>
		/// <returns></returns>
		virtual protected int MoveToNextSibling() {
			if (_XmlReader.NodeType == XmlNodeType.Attribute) {
				//Restart at the current node:
				_XmlReader.MoveToElement();
			}
			int depth = _XmlReader.Depth;
			MoveToNextElement();
			while (_XmlReader.Depth > depth) {
				MoveToNextElement();
			}
			return Comparer<int>.Default.Compare(_XmlReader.Depth, depth);
		}

		/// <summary>
		/// Moves to next element.
		/// </summary>
		/// <returns></returns>
		virtual protected int MoveToNextElement() {
			int depth = _XmlReader.Depth;
			while (!_XmlReader.EOF && _XmlReader.NodeType != XmlNodeType.Element) {
				_XmlReader.Read();
			}
			return Comparer<int>.Default.Compare(_XmlReader.Depth,depth);
		}

		/// <summary>
		/// Verifies that the stream's current Node's name is as expected.
		/// </summary>
		/// <param name="name">The name.</param>
		protected void VerifyNodeName(string name) {
			if (_XmlReader.Name != name) {
				throw new System.ArgumentException(
					string.Format(
					"Extract{0}() called on a '{2}' rather than a '{1}'.",
					PCase(name),name,_XmlReader.Name));
			}
		}


		/// <summary>
		/// Gets the given XmlElement's id attribute value.
		/// </summary>
		/// <param name="attributeValues">The attribute values.</param>
		/// <returns></returns>
		protected string GetElementId(Dictionary<string, string> attributeValues) {
			return GetElementAttributeValue(attributeValues, "id", true, null);
		}

		/// <summary>
		/// Gets the given node's specified attribute value.
		/// </summary>
		/// <param name="attributeValues">The attribute values.</param>
		/// <param name="attributeName">Name of the attribute.</param>
		/// <param name="isRequired">if set to <c>true</c> [is required].</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		protected string GetElementAttributeValue(Dictionary<string,string> attributeValues, string attributeName, bool isRequired, string defaultValue) {
			if (!attributeValues.ContainsKey(attributeName)) {
				if (isRequired){
					//Move back to current node:
					_XmlReader.MoveToElement();
					//In order to get its name:
					string nodeName = _XmlReader.Name;
					//for the message:
					throw new System.ArgumentException(string.Format("Missing '{0}' on {1}", attributeName, nodeName));
				}
				return (defaultValue == null) ? string.Empty : defaultValue;
			}

			string attributeValue = attributeValues[attributeName] ;
			return (!string.IsNullOrEmpty(attributeValue)) ? attributeValue : defaultValue;

		}

		/// <summary>
		/// Gets the current element node's attributes as a dictionary of key/value pairs.
		/// </summary>
		/// <returns></returns>
		protected Dictionary<string, string> GetElementAttributes() {
			Dictionary<string, string> results = new Dictionary<string, string>();
			for (int i = 0; i < _XmlReader.AttributeCount; i++) {
				_XmlReader.MoveToAttribute(i);
				string attributeName = _XmlReader.Name;
				_XmlReader.ReadAttributeValue(); 
				results[attributeName] = _XmlReader.Value;
			}
			return results;
		}

		/// <summary>
		/// Clears the Key cache.
		/// </summary>
		protected void InitializeKeyCache() {
			_DefinedKeys_AN[GraphMLKeyFor.all] = new Dictionary<string, string>();
			_DefinedKeys_AN[GraphMLKeyFor.edge] = new Dictionary<string, string>();
			_DefinedKeys_AN[GraphMLKeyFor.graph] = new Dictionary<string, string>();
			_DefinedKeys_AN[GraphMLKeyFor.node] = new Dictionary<string, string>();

		}

		/// <summary>
		/// Gets the Alias for the given key Name.
		/// <para>
		/// Throws an exception if the name has not been registered before.
		/// </para>
		/// </summary>
		/// <param name="keyFor">The key for.</param>
		/// <param name="keyAlias">The alias for the key.</param>
		/// <param name="keyName">The resulting Name of the key.</param>
		/// <returns></returns>
		protected bool TryGetKeyName(GraphMLKeyFor keyFor, string keyAlias, out string keyName) {
			if (_DefinedKeys_AN[keyFor].ContainsKey(keyAlias)) {
				keyName = _DefinedKeys_AN[keyFor][keyAlias];
				return true;
			}
			else if (_DefinedKeys_AN[GraphMLKeyFor.all].ContainsKey(keyAlias)) {
				keyName = _DefinedKeys_AN[GraphMLKeyFor.all][keyAlias];
				return true;
			}
			keyName = null;
			return false;
		}

		/// <summary>
		/// PrettyCases the given string.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		protected string PCase(string text) {
			return text.Substring(0, 1).ToUpper() + text.Substring(1);
		}
	}
}
