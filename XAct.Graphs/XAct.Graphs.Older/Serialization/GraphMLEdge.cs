﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// A light weight graph Edge element.
	/// </summary>
	public class GraphMLEdge {

		#region Properties
		/// <summary>
		/// Gets the source vertex's id.
		/// </summary>
		/// <value>The source id.</value>
		public string SourceId { get { return _SourceId; } }
		private string _SourceId;

		/// <summary>
		/// Gets the target vertex's id.
		/// </summary>
		/// <value>The target id.</value>
		public string TargetId { get { return _TargetId; } }
		private string _TargetId;

		/// <summary>
		/// Gets the Dictionary of Key/Value data elements associated to this Edge.
		/// </summary>
		/// <value>The data.</value>
		public Dictionary<string, string> Data { get { return _Data; } }
		private Dictionary<string, string> _Data;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphMLEdge"/> class.
		/// </summary>
		/// <param name="sourceId">The source id.</param>
		/// <param name="targetId">The target id.</param>
		/// <param name="attributes">The edge's data elements.</param>
		public GraphMLEdge(string sourceId, string targetId, Dictionary<string, string> attributes) {
			_SourceId = sourceId;
			_TargetId = targetId;

			if (attributes == null) {
				attributes = new Dictionary<string, string>();
			}
			_Data = attributes;
		}
		#endregion

	}//Class:End
}//Namespace:End
