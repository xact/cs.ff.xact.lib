﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace XAct.Graphs.Serialization {

	/// <summary>
	/// Abstract base class for serializing a Graph.
	/// </summary>
	/// <remarks>
	/// <para>
	/// For a primer on the GraphML format,
	/// see <see href="http://graphml.graphdrawing.org/primer/graphml-primer.html"/>.
	/// </para>
	/// </remarks>
	abstract public class GraphMLSerializer<TGraph, TVertex, TEdge>
		where TGraph : IGraphEngine
		where TVertex : GraphVertex
		where TEdge : GraphEdge {

		#region Constants
		const string C_GRAPHML_NODE = "graphml";
		const string C_GRAPH_NAME = "graph";
		const string C_NODE_NAME = "node";
		const string C_DATA_NAME = "data";
		const string C_EDGE_NAME = "edge";
		const string C_KEY_NAME = "key";

		const string C_GUID_FORMAT = "N";
		#endregion

		#region Events Raised
		/// <summary>
		/// Event raised before a Graph has begun being serialized.
		/// </summary>
		public event EventHandler GraphSerializing;
		/// <summary>
		/// Event raised after a Graph has been fully serialized.
		/// </summary>
		public event EventHandler GraphSerialized;

		/// <summary>
		/// Event raised after a Key has been fully serialized.
		/// </summary>
		public event EventHandler KeySerialized;

		/// <summary>
		/// Event raised before a Node has begun being serialized.
		/// </summary>
		public event EventHandler<GraphMLNodeEventArgs<TVertex>> NodeSerializing;

		/// <summary>
		/// Event raised after a Vertex/Node has been fully serialized.
		/// </summary>
		public event EventHandler<GraphMLNodeEventArgs<TVertex>> NodeSerialized;

		/// <summary>
		/// Event raised before an Edge has begun being serialized.
		/// </summary>
		public event EventHandler<GraphMLEdgeEventArgs<TEdge>> EdgeSerializing;

		/// <summary>
		/// Event raised after an Edge has been fully serialized.
		/// </summary>
		public event EventHandler<GraphMLEdgeEventArgs<TEdge>> EdgeSerialized;

		/// <summary>
		/// Event raised after a Data element of a Vertex or Edge has been fully serialized.
		/// </summary>
		public event EventHandler DataSerialized;
		#endregion


		#region Fields

		System.Collections.Stack _TagStack = new System.Collections.Stack();


		Dictionary<GraphMLKeyFor, Dictionary<string, string>> _DefinedKeys_NA = new Dictionary<GraphMLKeyFor, Dictionary<string, string>>();


		//int _Depth;

		string defaultNSUri = "http://graphml.graphdrawing.org/xmlns";

		#endregion

		#region Properties
		/// <summary>
		/// Gets the full path of the file to serialize the Graph to.
		/// </summary>
		/// <value>The name of the file.</value>
		public string FileName {
			get {
				return _FileName;
			}
		}
		private string _FileName;


		/// <summary>
		/// Gets the stream used to serialize the Graph to.
		/// </summary>
		/// <value>The stream.</value>
		protected Stream Stream {
			get {
				return _Stream;
			}
		}
		private Stream _Stream;


		/// <summary>
		/// Gets the XmlTextWriter used to serialize the graph to the stream.
		/// </summary>
		/// <value>The XML text writer.</value>
		protected XmlTextWriter XmlTextWriter {
			get {
				return _XmlTextWriter;
			}
		}
		XmlTextWriter _XmlTextWriter;



		/// <summary>
		/// The vertex in question when <see cref="NodeSerialized"/> is raised.
		/// </summary>
		public object CurrentVertex {
			get {
				return _CurrentVertex;
			}
		}
		private object _CurrentVertex;

		/// <summary>
		/// The edge in question when <see cref="EdgeSerialized"/> is raised.
		/// </summary>
		public object CurrentEdge {
			get {
				return _CurrentEdge;
			}
		}
		private object _CurrentEdge;
		#endregion


		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphMLSerializer{TGraph, TVertex, TEdge}"/> class.
		/// </summary>
		public GraphMLSerializer(string fileName) {
			_FileName = fileName;
			Initialize();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Clears the Key cache.
		/// Call this method before re-serializing a graph.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by Constructor.
		/// </para>
		/// </remarks>
		public void Initialize() {
			InitializeKeyCache();
		}

		/// <summary>
		/// Registers a Key property Name.
		/// <para>
		/// See Remarks.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Due to the structure of a <c>GraphML</c> indicating that the Key definitions
		/// come at the beginning of the xml document, this method
		/// must be used only prior to serializing the document.
		/// </para>
		/// </remarks>
		/// <param name="keyFor">The key for.</param>
		/// <param name="keyAlias">The key alias.</param>
		/// <param name="keyName">Name of the key.</param>
		public void RegisterKey(GraphMLKeyFor keyFor, string keyAlias, string keyName) {
			if (string.IsNullOrEmpty(keyName)) {
				throw new System.ArgumentNullException("keyName");
			}


			if (!_DefinedKeys_NA[keyFor].ContainsKey(keyName)) {
				_DefinedKeys_NA[keyFor][keyName] = keyAlias;
			}
		}

		/// <summary>
		/// Serializes the given Graph.
		/// <para>
		/// Intended for override.
		/// </para>
		/// </summary>
		virtual public void SerializeGraph(TGraph graph, GraphMLDefaultEdgeDirection defaultEdgeDirection) {

			//Initialize the stream.
			_Stream = new MemoryStream();
			_XmlTextWriter = new XmlTextWriter(_Stream, null);

			//Start serializing the graph.
			WriteDocumentRootStartElement();

			OnGraphSerializing(EventArgs.Empty);


			//Serialize the graph(s):
			string graphId = "graph";
			WriteGraphStartElement(graphId, defaultEdgeDirection);



			foreach (TVertex vertex in graph.Vertices) {
				SerializeVertex(vertex);
			}
			foreach (TEdge edge in graph.Edges) {
				SerializeEdge(edge);
			}


			//Close all oepn tags:
			CloseTags(C_GRAPH_NAME);
			OnGraphSerialized(EventArgs.Empty);

			CloseTags(C_GRAPHML_NODE);

			//_XmlTextWriter.Flush();
		}



		#endregion


		#region Raise Events
		/// <summary>
		/// Raises the <see cref="E:GraphSerializing"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		virtual protected void OnGraphSerializing(EventArgs e) {
			if (GraphSerializing != null) {
				GraphSerializing(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:GraphSerialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		virtual protected void OnGraphSerialized(EventArgs e) {
			if (GraphSerialized != null) {
				GraphSerialized(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:KeySerialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		virtual protected void OnKeySerialized(EventArgs e) {
			if (KeySerialized != null) {
				KeySerialized(this, e);
			}
		}


		/// <summary>
		/// Raises the <see cref="E:NodeSerializing"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		virtual protected void OnNodeSerializing(GraphMLNodeEventArgs<TVertex> e) {
			if (NodeSerializing != null) {
				NodeSerializing(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:NodeSerialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		virtual protected void OnNodeSerialized(GraphMLNodeEventArgs<TVertex> e) {
			if (NodeSerialized != null) {
				NodeSerialized(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:EdgeSerializing"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		virtual protected void OnEdgeSerializing(GraphMLEdgeEventArgs<TEdge> e) {
			if (EdgeSerializing != null) {
				EdgeSerializing(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:EdgeSerialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		virtual protected void OnEdgeSerialized(GraphMLEdgeEventArgs<TEdge> e) {
			if (EdgeSerialized != null) {
				EdgeSerialized(this, e);
			}
		}
		/// <summary>
		/// Raises the <see cref="E:DataSerialized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		virtual protected void OnDataSerialized(EventArgs e) {
			if (DataSerialized != null) {
				DataSerialized(this, e);
			}
		}

		#endregion


		#region Protected Methods
		/// <summary>
		/// Writes to the outgoing stream an opening/start 
		/// GraphML document's root <c>graphml</c> element, with its
		/// namespace declarations.
		/// <para>
		/// NOTE: Because it is only the Start element, it will 
		/// have to be followed with an End element before closing the stream.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// <code>
		/// <![CDATA[
		/// <?xml version="1.0" encoding="UTF-8"?>
		///   <graphml	xmlns="http://graphml.graphdrawing.org/xmlns"  
		///							xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		///							xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns 
		///							http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
		///		...
		///		</graphml>
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		protected void WriteDocumentRootStartElement() {

			//See: http://forums.microsoft.com/MSDN/ShowPost.aspx?PostID=210800&SiteID=1
			_XmlTextWriter.WriteStartElement(C_GRAPHML_NODE);
			_TagStack.Push(C_GRAPHML_NODE);

			_XmlTextWriter.WriteAttributeString("xmlns", null, defaultNSUri);
			_XmlTextWriter.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
			_XmlTextWriter.WriteAttributeString("xsi", "schemaLocation", null, "http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd");

			//Do NOT close the tag...
		}




		/// <summary>
		/// Writes to the outgoing stream an opening/start 
		/// <c>graph</c> element, with an <c>id</c> and <c>edgeDefault</c> attribute.
		/// <para>
		/// NOTE: Because it is only the Start element, it will 
		/// have to be followed with an End element before closing the stream.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// <code>
		/// <![CDATA[
		/// ...
		/// <graph id="G" edgedefault="undirected">
		/// ...
		/// </graph>
		/// ...
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		/// <param name="graphId">The graph id.</param>
		/// <param name="defaultEdgeDirection">The default edge direction.</param>
		protected void WriteGraphStartElement(string graphId, GraphMLDefaultEdgeDirection defaultEdgeDirection) {

			if (graphId == null) {
				throw new System.ArgumentNullException("graphId");
			}

			_XmlTextWriter.WriteStartElement(C_GRAPH_NAME);
			_TagStack.Push(C_GRAPH_NAME);

			_XmlTextWriter.WriteAttributeString("id", graphId);
			_XmlTextWriter.WriteAttributeString("edgeDefault", defaultEdgeDirection.ToString());

			//Do NOT close the tag...
		}




		/// <summary>
		/// Writes to the outgoing stream a complete 
		/// <c>key</c> element, with an <c>id</c>, <c>for</c>, <c>attr.name</c>, and <c>attr.type</c> attribute.
		/// <para>
		/// May optionally also wrap a <c>default</c> sub element.
		/// </para>.
		/// </summary>
		/// <remarks>
		/// <para>
		/// <code>
		/// <![CDATA[
		/// <graphml ...> 
		/// <!-- define keys *before* defining graph that will reference back to keys -->
		/// <key id="d0" for="node" attr.name="color" attr.type="string">
		///   <default>yellow</default>
		/// </key>
		/// ...
		/// <graph>
		/// </graph>
		/// ...
		/// </graphml>
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		/// <param name="keyFor"></param>
		/// <param name="keyAlias"></param>
		/// <param name="keyName"></param>
		/// <param name="keyType"></param>
		/// <param name="defaultValue"></param>
		protected void WriteKeyElement(GraphMLKeyFor keyFor, string keyAlias, string keyName, GraphMLKeyDataType keyType, string defaultValue) {
			if (string.IsNullOrEmpty(keyAlias)) {
				throw new System.ArgumentNullException("keyAlias");
			}
			if (string.IsNullOrEmpty(keyName)) {
				throw new System.ArgumentNullException("keyName");
			}


			_XmlTextWriter.WriteStartElement(C_KEY_NAME);
			_TagStack.Push(C_KEY_NAME);

			RegisterKey(keyFor, keyAlias, keyName);

			_XmlTextWriter.WriteAttributeString("id", keyAlias);
			_XmlTextWriter.WriteAttributeString("for", keyFor.ToString());
			_XmlTextWriter.WriteAttributeString("attr.name", keyName);
			_XmlTextWriter.WriteAttributeString("attr.type", keyType.ToString().Substring(1));

			if (!(string.IsNullOrEmpty(defaultValue))) {
				_XmlTextWriter.WriteElementString("default", defaultValue);

			}

			_XmlTextWriter.WriteEndElement();
			_TagStack.Pop();

		}


		/// <summary>
		/// Serializes the vertex.
		/// <para>
		/// Intended for override.
		/// </para>
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		virtual protected void SerializeVertex(TVertex vertex) {
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}


			bool directed = false;

			GraphMLNodeEventArgs<TVertex> args = new GraphMLNodeEventArgs<TVertex>(vertex);

			Dictionary<string, string> keyValues = null;
			OnNodeSerializing(args);

			WriteNodeElement(vertex.Id.ToString(C_GUID_FORMAT), directed, keyValues);

			//Raise event:
			_CurrentVertex = vertex;
			OnNodeSerialized(args);
		}

		/// <summary>
		/// Writes to the outgoing stream a complete (start+end)
		/// <c>node</c> element.
		/// <para>
		/// Overload.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// A serialized edge node may end up looking like this:
		/// <code>
		/// <![CDATA[
		/// ...
		/// <key id="d0" for="node" attr.name="color" attr.type="string">
		///   <default>yellow</default>
		/// </key>
		/// ...
		/// <graph id="G" edgedefault="undirected">
		///   ...
		///   <node id="n0">
		///     <data key="d0">green</data>
		///   </node>
		///   ...
		/// </graph>
		/// ...
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		protected void WriteNodeElement(string vertexId, bool directed) {
			WriteNodeElement(vertexId, directed, null);
		}


		/// <summary>
		/// Writes to the outgoing stream a complete (start+end)
		/// <c>node</c> element.
		/// <para>
		/// Overload.
		/// </para>.
		/// </summary>
		protected void WriteNodeElement(string vertexId, bool directed, Dictionary<string, string> keyValues) {
			WriteNodeStartElement(vertexId);

			if (keyValues != null) {
				WriteDataElements(GraphMLKeyFor.node, keyValues);
			}

			//Close the node element:
			_XmlTextWriter.WriteEndElement();
			_TagStack.Pop();

		}


		/// <summary>
		/// Writes to the outgoing stream an opening/start 
		/// <c>node</c> element, with its <c>id</c> attribute.
		/// <para>
		/// NOTE: Because it is only the Start element, it will 
		/// have to be followed with an End element before closing the stream.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by 
		/// <see cref="WriteNodeElement(string, bool)"/>
		/// or 
		/// <see cref="WriteNodeElement(string, bool, Dictionary{string, string})"/>
		/// </para>
		/// </remarks>
		protected void WriteNodeStartElement(string vertexId) {
			if (string.IsNullOrEmpty(vertexId)) {
				throw new System.ArgumentNullException("vertexId");
			}

			_XmlTextWriter.WriteStartElement(C_NODE_NAME);
			_TagStack.Push(C_NODE_NAME);

			_XmlTextWriter.WriteAttributeString("id", vertexId);
		}









		/// <summary>
		/// Serialize the given edge to the outgoing stream.
		/// <para>
		/// Intended for override.
		/// </para>
		/// </summary>
		/// <param name="edge"></param>
		virtual protected void SerializeEdge(TEdge edge) {
			if (edge == null) {
				throw new System.ArgumentNullException("edge");
			}

			GraphMLEdgeEventArgs<TEdge> args = new GraphMLEdgeEventArgs<TEdge>(edge);

			Dictionary<string, string> keyValues = null;
			OnEdgeSerializing(args);

			WriteEdgeElement(edge.SourceId.ToString(C_GUID_FORMAT), edge.TargetId.ToString(C_GUID_FORMAT), keyValues);

			//Raise event:
			_CurrentEdge = edge;
			OnEdgeSerialized(args);
		}




		/// <summary>
		/// Writes to the outgoing stream a complete (start+end)
		/// <c>edge</c> element.
		/// <para>
		/// Overload.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// A serialized edge node may end up looking like this:
		/// <code>
		/// <![CDATA[
		/// ...
		/// <key id="d0" for="node" attr.name="color" attr.type="string">
		///   <default>yellow</default>
		/// </key>
		/// ...
		/// <graph id="G" edgedefault="undirected">
		///   ...
		///   <edge id="e0" source="n0" target="n2"> 
		///     <data key="d0">orange</data>
		///   </edge>
		///   ...
		/// </graph>
		/// ...
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		/// <param name="sourceId"></param>
		/// <param name="targetId"></param>
		/// <param name="keyValues"></param>
		/// <param name="directed"></param>
		protected void WriteEdgeElement(string sourceId, string targetId, Dictionary<string, string> keyValues, bool directed) {

			WriteEdgeStartElement(sourceId, targetId,true, directed);

			WriteDataElements(GraphMLKeyFor.edge, keyValues);

			_XmlTextWriter.WriteEndElement();
			_TagStack.Pop();

		}


		/// <summary>
		/// Writes to the outgoing stream a complete (start+end)
		/// <c>edge</c> element.
		/// <para>
		/// Overload.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// A serialized edge node may end up looking like this:
		/// <code>
		/// <![CDATA[
		/// ...
		/// <key id="d0" for="node" attr.name="color" attr.type="string">
		///   <default>yellow</default>
		/// </key>
		/// ...
		/// <graph id="G" edgedefault="undirected">
		///   ...
		///   <edge id="e0" source="n0" target="n2"> 
		///     <data key="d0">orange</data>
		///   </edge>
		///   ...
		/// </graph>
		/// ...
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		/// <param name="sourceId">The source id.</param>
		/// <param name="targetId">The target id.</param>
		/// <param name="keyValues">The key values.</param>
		protected void WriteEdgeElement(string sourceId, string targetId, Dictionary<string, string> keyValues) {

			WriteEdgeStartElement(sourceId, targetId, false, false);
			WriteDataElements(GraphMLKeyFor.edge, keyValues);

			_XmlTextWriter.WriteEndElement();
			_TagStack.Pop();
		}




		/// <summary>
		/// Writes to the outgoing stream an opening/start 
		/// <c>edge</c> element, with its <c>source</c> and <c>target</c> attributes,
		/// and optionally a <c>directed</c> attribute.
		/// <para>
		/// NOTE: Because it is only the Start element, it will 
		/// have to be followed with an End element before closing the stream.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by 
		/// <see cref="WriteEdgeElement(string, string, Dictionary{string, string})"/>
		/// or 
		/// <see cref="WriteEdgeElement(string, string, Dictionary{string, string}, bool)"/>
		/// </para>
		/// </remarks>
		protected void WriteEdgeStartElement(string sourceId, string targetId, bool includeDirection, bool isDirected) {
			if (string.IsNullOrEmpty(sourceId)) {
				throw new System.ArgumentNullException("sourceId");
			}
			if (string.IsNullOrEmpty(targetId)) {
				throw new System.ArgumentNullException("targetId");
			}

			_XmlTextWriter.WriteStartElement(C_EDGE_NAME);
			_TagStack.Push(C_EDGE_NAME);

			_XmlTextWriter.WriteAttributeString("source", sourceId);
			_XmlTextWriter.WriteAttributeString("target", targetId);

			if (includeDirection) {
				_XmlTextWriter.WriteAttributeString("directed", isDirected.ToString());
			}
		}



		/// <summary>
		/// Writes to the outgoing stream a set of complete (start+end)
		/// <c>data</c> elements as children of a <c>node</c> or <c>edge</c> element.
		/// </summary>
		/// <param name="keyFor">Indication of whether the data is a child of a node or edge.</param>
		/// <param name="keyValues">The key values.</param>
		protected void WriteDataElements(GraphMLKeyFor keyFor, Dictionary<string, string> keyValues) {
			foreach (System.Collections.Generic.KeyValuePair<string, string> pair in keyValues) {
				WriteDataElement(keyFor, pair.Key, pair.Value);
			}
		}


		/// <summary>
		/// Writes to the outgoing stream a complete (start+end)
		/// <c>data</c> element as a child of a <c>node</c> or <c>edge</c> element.
		/// </summary>
		/// <remarks>
		/// <para>
		/// A serialized value may look like somewhat either of the following examples:
		/// <code>
		/// <![CDATA[
		///   ...
		///   <node id="n0">
		///     <data key="d0">green</data>
		///   </node>
		///   ...
		///   ...
		///   <edge id="e0" source="n0" target="n2"> 
		///     <data key="d0">orange</data>
		///   </edge>
		///   ...
		/// ]]>
		/// </code>
		/// </para>
		/// <para>
		/// The keyName must have been registered first.
		/// </para>
		/// </remarks>
		/// <param name="keyFor">Indication of whether the data is a child of a node or edge.</param>
		/// <param name="keyName">Name of the key. </param>
		/// <param name="value">The value of the key.</param>
		protected void WriteDataElement(GraphMLKeyFor keyFor, string keyName, string value) {
			//Get the alias of the key name, or throw an error.
			string keyAlias;
			if (!TryGetKeyAlias(keyFor, keyName, out keyAlias)) {
				throw new System.ArgumentException("KeyName not registered yet.");
			}


			_XmlTextWriter.WriteStartElement(C_DATA_NAME);
			_TagStack.Push(C_DATA_NAME);

			_XmlTextWriter.WriteAttributeString(C_KEY_NAME, keyAlias);
			_XmlTextWriter.WriteAttributeString(C_KEY_NAME, keyAlias);


			_XmlTextWriter.WriteEndElement();
			_TagStack.Pop();

		}
		#endregion


		#region Protected Methods - Basics
		/// <summary>
		/// Clears the Key Name/Alias dictionary.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="Initialize"/>
		/// </para>
		/// </remarks>
		protected void InitializeKeyCache() {
			if (_DefinedKeys_NA.Count > 0) {
				return;
			}


			_DefinedKeys_NA[GraphMLKeyFor.all] = new Dictionary<string, string>();
			_DefinedKeys_NA[GraphMLKeyFor.edge] = new Dictionary<string, string>();
			_DefinedKeys_NA[GraphMLKeyFor.graph] = new Dictionary<string, string>();
			_DefinedKeys_NA[GraphMLKeyFor.node] = new Dictionary<string, string>();

		}

		/// <summary>
		/// Determines whether the key name is registered.
		/// </summary>
		/// <param name="keyFor">The key for.</param>
		/// <param name="keyName">Name of the key.</param>
		/// <returns>
		/// 	<c>true</c> if [is key name registered] [the specified key for]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsKeyNameRegistered(GraphMLKeyFor keyFor, string keyName) {
			if ((_DefinedKeys_NA[keyFor].ContainsKey(keyName)) || (_DefinedKeys_NA[GraphMLKeyFor.all].ContainsKey(keyName))) {
				return true;
			}
			return false;
		}

		/// <summary>
		/// Gets the Alias for the given key Name.
		/// <para>
		/// Throws an exception if the name has not been registered before.
		/// </para>
		/// </summary>
		/// <param name="keyFor">The key for.</param>
		/// <param name="keyName">Name of the key.</param>
		/// <param name="keyAlias">The returned alias of the key name.</param>
		/// <returns></returns>
		protected bool TryGetKeyAlias(GraphMLKeyFor keyFor, string keyName, out string keyAlias) {
			if (_DefinedKeys_NA[keyFor].ContainsKey(keyName)) {
				keyAlias = _DefinedKeys_NA[keyFor][keyName];
				return true;
			}
			else if (_DefinedKeys_NA[GraphMLKeyFor.all].ContainsKey(keyName)) {
				keyAlias = _DefinedKeys_NA[GraphMLKeyFor.all][keyName];
				return true;
			}
			keyAlias = null;
			return false;
		}

		/// <summary>
		/// Renders to the outgoing stream closing tags for all currently open/started elements.
		/// </summary>
		protected void CloseTags() {
			CloseTags(string.Empty);
		}

		/// <summary>
		/// Renders to the outgoing stream closing tags for all currently open/started elements
		/// up to and including the given tag name.
		/// </summary>
		/// <param name="tagName">Name of the tag.</param>
		protected void CloseTags(string tagName) {
			IEnumerator<string> enumerator = (IEnumerator<string>)_TagStack.GetEnumerator();

			while (enumerator.MoveNext()){
				
				string tag  = enumerator.Current;
				_XmlTextWriter.WriteEndElement();
				if (string.Compare(tag, tagName, true) == 0) {
					break;
				}
			}
		}
		#endregion


	}//Class:End
}
