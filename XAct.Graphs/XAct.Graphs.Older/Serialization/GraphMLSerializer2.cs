﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// 
	/// </summary>
	public class GraphMLSerializer2 : GraphMLSerializer<XAct.Graphs.IGraphEngine,GraphVertex,GraphEdge> {


		/// <summary>
		/// Initializes a new instance of the <see cref="GraphMLSerializer2"/> class.
		/// </summary>
		/// <param name="fileName"></param>
		public GraphMLSerializer2(string fileName)
			: base(fileName) {
		}


		#region Overrides
		/// <summary>
		/// Raises the <see cref="E:GraphSerializing"/> event.
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void OnGraphSerializing(EventArgs e) {
			base.OnGraphSerializing(e);
		}

		/*
		protected override void OnDataKeysSerializing(EventArgs e){
			base.OnDataKeysSerializing(e);
		}
		*/

		/// <summary>
		/// Raises the <see cref="E:NodeSerializing"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void OnNodeSerializing(GraphMLNodeEventArgs<GraphVertex> e) {
			base.OnNodeSerializing(e);
		}
		/// <summary>
		/// Raises the <see cref="E:EdgeSerializing"/> event.
		/// </summary>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void OnEdgeSerializing(GraphMLEdgeEventArgs<GraphEdge> e) {
			base.OnEdgeSerializing(e);
		}
		#endregion
	}
}
