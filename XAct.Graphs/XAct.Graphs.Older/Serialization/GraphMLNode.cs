﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// A light weight graph Vertex element.
	/// </summary>
	public class GraphMLNode {

		#region Properties
		/// <summary>
		/// Gets the Vertex Id.
		/// </summary>
		/// <value>The id.</value>
		public string Id { get { return _Id; } }
		private string _Id;

		/// <summary>
		/// Gets the Dictionary of Key/Value data elements associated to this Vertex.
		/// </summary>
		/// <value>The data.</value>
		public Dictionary<string, string> Data { get { return _Data; } }
		private Dictionary<string, string> _Data;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphMLNode"/> class.
		/// </summary>
		/// <param name="id">The unique id of the Vertex.</param>
		/// <param name="attributes">The vertex's data elements.</param>
		public GraphMLNode(string id, Dictionary<string, string> attributes) {
			_Id = id;
			if (attributes == null) {
				attributes = new Dictionary<string, string>();
			}
			_Data = attributes;
		}
		#endregion

	}//Class:End
}//Namespace:End
