DLL XAct.graphEngine.

La Dll d�finie une interface (GraphProviderBase) pour le graphEngine et propose
une impl�mentation (GraphDbCachedProvider) bas�e sur l'utilisation d'une base de donn�es local
avec un cache en memoire pour optimisation des temps. 

le point d'entree de la Dll est une singleton instance
d'un GraphProviderBase accessible avec la classe statique 
GraphManager :

GraphProviderBase engine = GraphManager.Provider.

le fichier xml de configuration permet de specifier l'implementation
souhait�e et les diff�rents parametres pour l'instanciation du provider.


une instance de GraphProviderBase permet d'interoger/modifier un graphe
local (avec l'implementation GraphDbCachedProvider le graphe est stock� dans une
base de donn�es).
un graphe est constitu� de vertex (class Vertex) et de edges (classe Edge) qui
relient ces vertex entre eux.
chaque GraphVertex a un identifiant unique (Guid) et un DataObject qui lui est associ�
accessible par la propri�t� Data.



Le graphEngine ne s'occupe effectivement que de l'interogation et modification
des edges et vertex
en ce qui concerne les dataObjects associ�s, le graphEngine sous traite
le travail de creation/supression/update au DataSource associ� au DataObject.


voir le projet TestEngine pour un exemple fonctionel d'utilisation.


DESIGN:

from GraphDbCachedProviderBase.CreateVertex documentation :

    /// The returned GraphVertex is always a newly created one, but they can be multiple
    /// vertex pointing to the same data object.
    /// If we want to change this behaviour we should consider droping the Guid Id field
    /// and make the couple (DataSourceId, RecordId) the primary field of the GraphVertex table
    /// which I think is a step backward for clean GraphEngine/DataEngine separation.
    /// 
    /// SKY: le fait de pouvoir avoir plusieurs GraphVertex qui pointent vers le meme datarecord
    ///      permet de resoudre de maniere simple ton probleme de droits sur les folders priv�s.
    ///      puisqu'il suffit de creer un nouveau vertex pour que celui ci ne soit accessible
    ///      uniquement sur par les lien dont on peut decider les droits d'acces


...
il n'y a aucune raison d'optimiser les commandes de modification du graphe
AddEdge, RemoveEdge, Move, CreateVertex, etc.
puisque avec l'impl�mentation courante ces m�thodes ne font que
modifier les representation en m�moire du graphes.
Les requetes SQL qui effectuent le flush des modifications sur la base donn�e
locale des modifs en memoire sont faites de facon periodique par le flush thread
d�di�.
C'est donc cette partie qu'il faut optimiser pour limiter le nombre
de commandes SQL.


EXTERNAL CLASSES:

* GraphProviderBase (todo: renommer en GraphEngineBase, IQuickGraphWrapper?)
  Il s'agit essentiellement de la d�finition de l'interface du GraphEngine.
  La classe est une classe abstraite et non une interface puisqu'elle h�rite
  de la classe ProviderBase.
  

* GraphManager
  la classe est statique. 
  elle instancie avec les m�canique des Provider de System.Web.Configuration
  une instance singleton du GraphEngine accessible par la propri�t� Provider.

* Vertex

Represente un noeud du graph.
Un vertex possede un identifiant unique (Guid) Id,
des date de creation, edition
l'etat de synchronisation du vertex en memoire vis � vis des graphes
local et distant,
et une reference sur le DataObject associ� (propri�t� Data).
Etant donn� un GraphVertex v on peut obtenir ces arcs sortant avec la m�thode :

IEnumerable<GraphEdge> GraphProviderBase.OutEdges(v)

pour cr�er un nouveau vertex :
Vertex GraphEngine.CreateVertex(dataObject dataObject);

pour supprimer un nouveau vertex : 
GraphEngine.RemoveVertex(v);

pour repercuter (de facon asynchrone) les modifications du vertex sur les graphes 
local et distant :
GraphEngine.UpdateVertex(v);

* Edge

represente un lien entre deux noeud, le vertex source (Source) et destination (Target).

pour creer un nouveau edge :
GraphEngine.AddEdge(Guid sourceId, Guid targetId)

pour supprimer un edge :
GraphEngine.RemoveEdge(Guid sourceId, Guid targetId)
ou 
GraphEngine.RemoveEdge(GraphEdge e)

* SyncStatus

bitmask specifiant l'�tat de synchronisation d'un vertex ou edge en memoire
vis � vis de leur representation stocker sur les graphes local et distant.


* IDataObjectFactory



* DataObject


* IDataObjectFilter



INTERNAL CLASSES:

* GraphDbVertexRecord
represent une ligne de la table des GraphVertex dans la base de donn�es.


* GraphMRUItemList

represente une collection de vertex.
les vertex sont tri�s selon leur ordre de derniere utilisation
et aussi accessible rapidement (par une table de hashage) � partir
de leur Id.


* GraphTimeStampedItem

classe utilis�e uniquement par GraphMRUItemList pour associ� � un vertex la date
de sa derniere utilisation.


* GraphDbCachedProvider

Implementation de GraphEngine qui utilise une base de donn�es locale
pour stocker le graphe.


* DbDataSource

Implementation concrete d'un IDataObjectFactory qui utilise une base de donn�e
pour stoker les dataObjects.




QUESTIONS to SKY:

why the use of DataObjectDictionary instead of Dictionary<Guid, DataObject> ?


DEPENDENCIES:
There is a System.Web.dll due to the need to find AppDir property under all 3 major platforms.
I thought of doing it via reflection to not have that dep...but it would load it in mem anyway
so might as well have it known up front.


PORTING TO CE:
FUCK! No IIdentity/IPrincipal objects.
Havn't looked closely at this, but maybe the following
can help:
http://www.eggheadcafe.com/articles/20041009.asp
