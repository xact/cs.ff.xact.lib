﻿
namespace XAct.Graphs {
	/// <summary>
	/// TODO
	/// </summary>
	public enum GraphRetrievalStatus {
		/// <summary>
		/// The object has not yet been initialized.
		/// </summary>
		Empty=0,
		/// <summary>
		/// The object is a proxy for an object not yet received from the server.
		/// </summary>
		IsProxy=1,
		/// <summary>
		/// The object is a proxy, and a request has been issued to have it replaced with a full/real object.
		/// </summary>
		Requested=2,
		/// <summary>
		/// The full data for the object has been received.
		/// </summary>
		Received=3
	}
}
