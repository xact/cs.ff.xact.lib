using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Text.RegularExpressions;

using XAct.Helpers;

namespace XAct.Graphs {


  /// <summary>
  /// Class to create and keep providers that use Db tables
  /// for backend storage, up to date.
  /// </summary>
  /// <remarks>
  /// <para>
  /// TODO: Needs a better description.
  /// </para>
  /// </remarks>
  public class DbSchemaInstaller {

    /// <summary>
    /// Name of Db Table used to check schema versions:
    /// </summary>
    private const string C_DBSCHEMATABLENAME = "DbSchemaVersionInfo";


    #region Fields
    /// <summary>
    /// Gets the unique name/alias for this provider's versioning info.
    /// </summary>
    private string _Tag;

    /// <summary>
    /// Gets the connection settings for the local DB where graph structure is stored.
    /// </summary>
    /// <value>The connection settings.</value>
    private ConnectionStringSettings _ConnectionSettings;

    /// <summary>
    /// The prefix used to mark parameters (For SqlServer its '@', etc.)
    /// </summary>
    private string _ParamPrefix;
    private StringDictionary _SqlKeyWords;
    #endregion


    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DbSchemaInstaller"/> class.
    /// </summary>
    /// <param name="tag">The tag.</param>
    /// <param name="connectionSettings">The connection settings.</param>
    /// <param name="sqlKeyWords">The SQL key words.</param>
    /// <param name="paramPrefix">The param prefix.</param>
    public DbSchemaInstaller(string tag, ConnectionStringSettings connectionSettings, StringDictionary sqlKeyWords, string paramPrefix) {
      //Check Args:
      if (string.IsNullOrEmpty(tag)) {
        throw new System.ArgumentNullException("tag");
      }
      if (connectionSettings == null) {
        throw new System.ArgumentNullException("connectionSettings");
      }
      if (paramPrefix == null) {
        throw new System.ArgumentNullException("paramPrefix");
      }
      if (sqlKeyWords == null) {
        throw new System.ArgumentNullException("sqlKeyWords");
      }
      if (sqlKeyWords.Count == 0) {
        throw new System.ArithmeticException("sqlKeyWords.Count cannot be 0.");
      }

      _Tag = tag;
      _ConnectionSettings = connectionSettings;
      _SqlKeyWords = sqlKeyWords;
      _ParamPrefix = paramPrefix;
    }
    #endregion


    #region Public Methods
    /// <summary>
    /// Initializes this instance.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Finds, or creates, a table called <c>DbSchemaVersionInfo</c>
    /// then processes any script files that are of a later 
    /// version than the recorded version.
    /// </para>
    /// </remarks>
    public void Initialize() {
      EnsureDbTrackingTableExists();
      Version version = GetVersion();
      SortedDictionary<string, string> scripts = GetScripts(_Tag);
      if (version == null) {
        version = new Version(0,0,0,0);
      }
      ProccessScripts(version, scripts);
    }
    #endregion
    [System.Diagnostics.DebuggerHidden()]
    private void EnsureDbTrackingTableExists() {

      string sql = string.Format(
                "CREATE TABLE {0}" +
                "(" +
                "Name VARCHAR(32) UNIQUE NOT NULL," +
                "NameLowered VARCHAR(32) UNIQUE NOT NULL," +
                "Version VARCHAR(20)," +
                "Note VARCHAR(128)" +
                ")"
                ,
                C_DBSCHEMATABLENAME
                );


      try {
        using (IDbConnection connection = CreateConnection()) {
          using (IDbCommand command = connection.CreateCommand()) {
            command.CommandText = sql;
            command.ExecuteNonQuery();
          }
        }
      }
      catch (System.Exception e){
        System.Diagnostics.Debug.WriteLine(e.Message);
        //There was an error: most probably, table already exists.
      }
    }

    /// <summary>
    /// Gets the latest known version of this assembly from the DbSchemaVersion table.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that it will returns null and not an empty Version
    /// if no entry exists in the Db table (ie, the first time).
    /// </para>
    /// </remarks>
    /// <returns>A <see cref="System.Version"/> or null.</returns>
    private System.Version GetVersion() {
      //Check Args:
      string sql = string.Format( 
        "SELECT Version FROM {0} WHERE NameLowered=@NameLowered",
        C_DBSCHEMATABLENAME);

      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = connection.CreateCommand()) {
          command.CommandText = FormatSql(sql);
          DbHelpers.AttachParam(command, "NameLowered", _Tag.ToLower());
          SqlTrace.TraceCommand(command);
          object result = command.ExecuteScalar();
          if (result != null) {
            return new Version((string)result);
          }
        }
      }
      return null;
    }

    private SortedDictionary<string, string> GetScripts(string tag) {
      return GetScripts(tag, null);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <internal>
    /// Notice how it collects together Resources, 
    /// and then goes for files.
    /// This means that if any scripts are provided
    /// in file format, with the same name as an embedded Resource, 
    /// they are deemed to supercede the Resource.
    /// </internal>
    /// <param name="tag"></param>
    /// <param name="pathName"></param>
    /// <returns></returns>
    private SortedDictionary<string, string> GetScripts(string tag, string pathName) {
      //Check Args:
      if (string.IsNullOrEmpty(tag)) {
        throw new System.ArgumentNullException("tag");
      }

      System.IO.Stream resourceStream;

      SortedDictionary<string, string> result =
        new SortedDictionary<string, string>(System.StringComparer.InvariantCultureIgnoreCase);

      // get a reference to the current assembly
      Assembly a = Assembly.GetExecutingAssembly();

      // get a list of resource names from the manifest
      string[] tmp = a.GetManifestResourceNames();


      List<string> resNames = new List<string>(tmp);
      //At this point we should only have sql files, and in ascending order
      //(ie first one is oldest script):

      foreach (string resName in resNames) {
        if (!Regex.Match(resName, "(" + tag + ").*([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)(.*)(\\.sql$)", RegexOptions.IgnoreCase).Success) {
          continue;
        }
        System.Diagnostics.Debug.WriteLine(resName);
        resourceStream = a.GetManifestResourceStream(resName);
        if (resourceStream != null) {
          System.IO.TextReader r = new System.IO.StreamReader(resourceStream);
          string contents = r.ReadToEnd();
          result[resName.ToLower()] = contents;
        }
      }

      if (System.IO.Directory.Exists(pathName)) {
        tmp = System.IO.Directory.GetFiles(pathName);
        foreach (string fullPath in tmp) {
          string fileName = System.IO.Path.GetFileName(fullPath);
          if (!Regex.Match(fileName, "(" + tag + ").*([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)(.*)(\\.sql$)", RegexOptions.IgnoreCase).Success) {
            continue;
          }
          System.IO.FileStream fileStream = new System.IO.FileStream(fullPath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
          System.IO.TextReader r = new System.IO.StreamReader(fileStream);
          string contents = r.ReadToEnd();
          result[fileName.ToLower()] = contents;
        }
      }

      foreach (string s in result.Keys) {
        //System.Diagnostics.Debug.WriteLine(s);
      }
      return result;

    }

    private void ProccessScripts(Version version, SortedDictionary<string, string> scripts) {
      //Check Args:
      if (scripts == null) {
        throw new System.ArgumentNullException("scripts");
      }

      //bool aScriptProcessed = false;
      //We know list is in order from oldest to newest:
      foreach (System.Collections.Generic.KeyValuePair<string, string> entry in scripts) {
        //We know it will match:
        string capture = Regex.Match(entry.Key, "[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+").Value;
        Version scriptVersion = new Version(capture);
        if (scriptVersion.CompareTo(version) > 0) {
          //The script is later than current version...run it:
          if (ProccessScript(entry.Value)){
            UpdateVersionNoteInDb(scriptVersion, "Updated on " + System.DateTime.Now.ToString());
            //aScriptProcessed = true;
          };
        }
      }
      //if (aScriptProcessed){
      //}
    }

    private bool ProccessScript(string scriptText) {
      try {

        using (IDbConnection connection = CreateConnection()) {
          using (IDbTransaction transaction = connection.BeginTransaction()) {
            using (IDbCommand command = connection.CreateCommand()) {
              command.CommandText = FormatSql(scriptText);
              command.ExecuteNonQuery();
            }
            transaction.Commit();
          }
          
        }
        return true;
      }
      catch {
        return false;
      }
    }



    private void UpdateVersionNoteInDb(System.Version versionInfo, string note) {
      //Check Args:
      if (versionInfo == null) {
        throw new System.ArgumentNullException("versionInfo");
      }
      //Prepare vars:
      if (note == null) {
        note = string.Empty;
      }
      string insertSql =
        string.Format("INSERT INTO {0} (Name, NameLowered, Version, Note) VALUES(@Name,@NameLowered, @Version,@Note)",
        C_DBSCHEMATABLENAME);
      string updateSql =
        string.Format("UPDATE {0} SET Version=@Version, Note=@Note WHERE NameLowered=@NameLowered",
        C_DBSCHEMATABLENAME);


      using (IDbConnection connection = CreateConnection()) {
        //Ask for version for this name:
        //If it comes back as null, means line doesn't exist.
        //Note that the value can never be null due to Arg check above...
        System.Version version = GetVersion();

        string versionStr = versionInfo.ToString(4);
        using (IDbCommand command = connection.CreateCommand()) {
          if (version == null) {
            //INSERT:
            command.CommandText = FormatSql(insertSql);
            //Attach params in order (for ODBC compliance):
            AttachParam(command, "Name", _Tag);
            AttachParam(command, "NameLowered", _Tag.ToLower());
            AttachParam(command, "Version", versionStr);
            AttachParam(command, "Note", note);
          }
          else {
            //UPDATE:
            command.CommandText = FormatSql(updateSql);
            //Attach params in order (for ODBC compliance):
            AttachParam(command, "Version", versionStr);
            AttachParam(command, "Note", note);
            AttachParam(command, "NameLowered", _Tag.ToLower());
          }
          command.ExecuteNonQuery();
        }
      }
    }





    #region Protected Helper Methods - Db Methods
    /// <summary>
    /// Creates an open connection.
    /// </summary>
    /// <returns></returns>
    protected virtual IDbConnection CreateConnection() {
      return DbHelpers.CreateConnection(_ConnectionSettings);
    }

    /// <summary>
    /// Creates and attaches the param.
    /// </summary>
    /// <param name="command">The command.</param>
    /// <param name="paramName">Name of the param.</param>
    /// <param name="paramValue">The param value.</param>
    protected virtual void AttachParam(IDbCommand command,string paramName, object paramValue) {
      DbHelpers.AttachParam(command, paramName, paramValue);
    }
    /// <summary>
    /// Formats the given template, expanding configuration variables.
    /// </summary>
    /// <internal>
    /// This is wrapper around <see cref="M:Support.DbHelpers.FormatStringEx"/>
    /// because the functionality is a bit beyond what I want to include
    /// again in this class...
    /// </internal>
    /// <param name="sql"></param>
    /// <returns></returns>
    protected virtual string FormatSql(string sql) {
      return DbHelpers.FormatStringEx(sql, _SqlKeyWords, _ParamPrefix);
    }

    #endregion


  }//Class:End
}//Namespace:End