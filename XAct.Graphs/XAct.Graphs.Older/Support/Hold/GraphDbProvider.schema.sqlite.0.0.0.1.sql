CREATE TABLE {dataSourceTable} 
( 
{dataSourceId} INTEGER UNIQUE NOT NULL, 
{dataSourceName} TEXT UNIQUE NOT NULL, 
{dataSourceProviderName} TEXT, 
{dataSourceConnectionString} TEXT, 
{dataSourceTableName} TEXT NOT NULL, 
{dataSourceKeyColumn} TEXT NOT NULL, 
PRIMARY KEY ({dataSourceId}) 
);


CREATE TABLE {vertexTable}
(
{vertexId} TEXT UNIQUE NOT NULL,
{vertexdataSourceId} INTEGER NOT NULL,
{vertexRecordId} TEXT UNIQUE NOT NULL,
{vertexCreated} TEXT NOT NULL,
{vertexEdited} TEXT,
{vertexSync}  INTEGER NOT NULL,
{vertexDeleted} INTEGER NOT NULL,
PRIMARY KEY ({vertexId})
);


CREATE TABLE {edgeTable}
(
{edgeSource} TEXT UNIQUE NOT NULL,
{edgeTarget} TEXT UNIQUE NOT NULL,
{edgeSync} INTEGER NOT NULL,
{edgeDeleted} INTEGER NOT NULL,
PRIMARY KEY ({edgeSource},{edgeTarget})
);

