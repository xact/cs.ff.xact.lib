CREATE TABLE TMP_{edgeTable}
(
{edgeSource} TEXT NOT NULL,
{edgeTarget} TEXT NOT NULL,
{edgeWeight} INTEGER NOT NULL,
{edgeSync} INTEGER NOT NULL,
{edgeDeleted} INTEGER NOT NULL,
PRIMARY KEY ({edgeSource},{edgeTarget})
);

INSERT INTO TMP_{edgeTable}
( {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}, {edgeDeleted})
SELECT {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}, {edgeDeleted}
FROM {edgeTable};

DROP TABLE {edgeTable};


CREATE TABLE {edgeTable}
(
{edgeSource} TEXT NOT NULL,
{edgeTarget} TEXT NOT NULL,
{edgeWeight} INTEGER NOT NULL,
{edgeSync} INTEGER NOT NULL,
{edgeDeleted} INTEGER NOT NULL,
PRIMARY KEY ({edgeSource},{edgeTarget})
);

INSERT INTO {edgeTable}
( {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}, {edgeDeleted})
SELECT {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}, {edgeDeleted}
FROM TMP_{edgeTable};

DROP TABLE TMP_{edgeTable};
