CREATE TABLE TMP_{vertexTable}
(
{vertexId} TEXT UNIQUE NOT NULL,
{vertexCreated} TEXT NOT NULL,
{vertexEdited} TEXT,
{vertexSync}  INTEGER NOT NULL,
{vertexdataSourceId} INTEGER NOT NULL,
{vertexRecordId} TEXT UNIQUE NOT NULL,
{vertexDeleted} INTEGER NOT NULL,
PRIMARY KEY ({vertexId})
);


INSERT INTO TMP_{vertexTable}
( {vertexId}, {vertexCreated}, {vertexEdited}, {vertexSync}, {vertexDataSourceId}, {vertexRecordId}, {vertexDeleted})
SELECT {vertexId}, {vertexCreated}, {vertexEdited}, {vertexSync}, {vertexDataSourceId}, {vertexRecordId}, {vertexDeleted}
FROM {vertexTable};

DROP TABLE {vertexTable};

CREATE TABLE {vertexTable}
(
{vertexId} TEXT UNIQUE NOT NULL,
{vertexWeight} INTEGER NOT NULL,
{vertexCreated} TEXT NOT NULL,
{vertexEdited} TEXT,
{vertexSync}  INTEGER NOT NULL,
{vertexdataSourceId} INTEGER NOT NULL,
{vertexRecordId} TEXT UNIQUE NOT NULL,
{vertexDeleted} INTEGER NOT NULL,
PRIMARY KEY ({vertexId})
);

INSERT INTO {vertexTable}
( {vertexId}, {vertexWeight}, {vertexCreated}, {vertexEdited}, {vertexSync}, {vertexDataSourceId}, {vertexRecordId}, {vertexDeleted})
SELECT {vertexId}, 0, {vertexCreated}, {vertexEdited}, {vertexSync}, {vertexDataSourceId}, {vertexRecordId}, {vertexDeleted}
FROM TMP_{vertexTable};

DROP TABLE TMP_{vertexTable};





CREATE TABLE TMP_{edgeTable}
(
{edgeSource} TEXT UNIQUE NOT NULL,
{edgeTarget} TEXT UNIQUE NOT NULL,
{edgeSync} INTEGER NOT NULL,
{edgeDeleted} INTEGER NOT NULL,
PRIMARY KEY ({edgeSource},{edgeTarget})
);

INSERT INTO TMP_{edgeTable}
( {edgeSource}, {edgeTarget}, {edgeSync}, {edgeDeleted})
SELECT {edgeSource}, {edgeTarget}, {edgeSync}, {edgeDeleted}
FROM {edgeTable};

DROP TABLE {edgeTable};


CREATE TABLE {edgeTable}
(
{edgeSource} TEXT UNIQUE NOT NULL,
{edgeTarget} TEXT UNIQUE NOT NULL,
{edgeWeight} INTEGER NOT NULL,
{edgeSync} INTEGER NOT NULL,
{edgeDeleted} INTEGER NOT NULL,
PRIMARY KEY ({edgeSource},{edgeTarget})
);

INSERT INTO {edgeTable}
( {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}, {edgeDeleted})
SELECT {edgeSource}, {edgeTarget}, 0, {edgeSync}, {edgeDeleted}
FROM TMP_{edgeTable};

DROP TABLE TMP_{edgeTable};
