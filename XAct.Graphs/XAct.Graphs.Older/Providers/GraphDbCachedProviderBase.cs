using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;

using XAct.Core.Utils;

namespace XAct.Graphs {
  /// <summary>
  /// Abstract base Graph Provider that caches Vertices in memory, 
  /// using secondary thread to manage batch persitence of changes to
  /// a DB/SQL storage system.
  /// </summary>
  /// <remarks>
  /// <para>
  /// This is an abstract class that enherits from, and extends, 
  /// <see cref="GraphProviderBase"/>. 
  /// </para>
  /// <para>
	/// See <see cref="GraphDbCachedProvider"/> for an implementation example.
  /// </para>
  /// </remarks>
  abstract public class GraphDbCachedProviderBase : GraphProviderBase 
	{

    #region Fields - Configuration Section related

    /// <summary>
    /// Attribute name for connection setting to local database.
    /// </summary>
    public static readonly string C_ATTRIBUTETAG_CONNECTIONSETTINGSNAME = "connectionName";

    /// <summary>
    /// Attribute name for database settings (table name, columns name etc.)
    /// </summary>
    public static readonly string C_ATTRIBUTETAG_DBSETTINGSNAME = "dbSettings";

		/// <summary>
		/// Attribute name for database param prefix symbol (generally '@')
		/// </summary>
		public static readonly string C_ATTRIBUTETAG_DBPARAM = "paramPrefix";


		/// <summary>
		/// Attribute name for database Guid.ToString() format code (default is 'D') (should be N for SqlServer).
		/// </summary>
		public static readonly string C_ATTRIBUTETAG_DBGUIDFORMATCODE = "guidFormatCode";

		/// <summary>
		/// Attribute name for database installation script name prefix.
		/// </summary>
		public static readonly string C_ATTRIBUTETAG_INSTALLSCRIPTSPREFIX = "installScriptsPrefix";

    #endregion


    #region Properties
		/// <summary>
		/// Gets the install script prefix to use when searching for embedded/file db scripts.
		/// </summary>
		/// <value>The install script prefix.</value>
		virtual protected string InstallScriptsPrefix {
			get {
				return _InstallScriptsPrefix;
			}
		}
		private string _InstallScriptsPrefix;
		
		/// <summary>
    /// Gets the connection settings for the local DB where graph structure is stored.
    /// </summary>
    /// <value>The connection settings.</value>
    virtual public ConnectionStringSettings ConnectionSettings {
      get {
        return _ConnectionSettings;
      }
    }
    /// <summary>
    /// TODO: TODOC:
    /// </summary>
    private ConnectionStringSettings _ConnectionSettings;



		/// <summary>
		/// Gets or sets the param prefix
		/// (default is '@').
		/// </summary>
		/// <value>The param prefix.</value>
		virtual public string ParamPrefix {
			get { 
				return _ParamPrefix;
			}
		}
		private string _ParamPrefix;

		/// <summary>
		/// Gets the format code for Guid.ToString() operations.
		/// (default is 'D').
		/// </summary>
		/// <value>The param prefix.</value>
		virtual public string GuidFormatCode {
			get {
				return _GuidFormatCode;
			}
		}
		private string _GuidFormatCode;

    /// <summary>
    /// Gets the dbSettings used to get the actual table name and column name
    /// for (edge, vertex and datasource tables)
    /// </summary>
		/// <remarks>
		/// Set by constructor.
		/// </remarks>
    public DbSettings DbSettings {
      get {
        return _DbSettings;
      }
    }
		/// <summary>
		/// NOTE: Set by constructor.
		/// </summary>
    private DbSettings _DbSettings;


    #endregion

    #region Public Methods - Implementation of ProviderBase

    /// <summary>
    /// Initializes the Graph Engine.
    /// called by Manager/Provider mechanic.
    /// </summary>
    /// <param name="name">The friendly name of the provider.</param>
    /// <param name="config">A collection of the name/value pairs representing the
    /// provider-specific attributes specified in the configuration for this provider.</param>
    public override void Initialize(string name, NameValueCollection config) {

      traceInformation("GraphDbCachedProvider Initialization");

			base.Initialize(name, config);

			//Get Connection Info:
			string connectionStringSettingsName =	ProviderHelper.InitializeParam<string>(config, C_ATTRIBUTETAG_CONNECTIONSETTINGSNAME, null, true);
			_ConnectionSettings = ProviderHelper.ConfigureConnectionSettings(connectionStringSettingsName);
			if (_ConnectionSettings == null) {
				throw new Exception(String.Format("missing connection setting : '{0}'", connectionStringSettingsName));
			}



			//Init local simple vars:
			_InstallScriptsPrefix = ProviderHelper.InitializeParam<string>(config, C_ATTRIBUTETAG_INSTALLSCRIPTSPREFIX, string.Empty, false);
			_ParamPrefix = ProviderHelper.InitializeParam(config, C_ATTRIBUTETAG_DBPARAM, "@");
			_GuidFormatCode = ProviderHelper.InitializeParam(config, C_ATTRIBUTETAG_DBGUIDFORMATCODE, "D");

			//Init local complex vars:
			GraphSection section = ((GraphSection)ConfigurationManager.GetSection(GraphManager.SectionPath));
			string dbSettingsName = ProviderHelper.InitializeParam<string>(config, C_ATTRIBUTETAG_DBSETTINGSNAME, string.Empty, false);
			_DbSettings = section.DbSettings[dbSettingsName];
      if (_DbSettings == null) {
				//TODO: This is not portable to CE
				//as is, because CE doesn't know how to init the DbSettings...
				//and don't yet understand where PC/Full is doing it...
				_DbSettings = new DbSettings();
				string check1 = _DbSettings.Name; //Note: Marked with required -- but will be blank.
				string check2 = _DbSettings.EdgeTable; //Note: nicely filled in...
				//throw new Exception(String.Format("dbSettings '{0}' missing", dbSettingsName));
      }


      Initialization_CreateTables();


      //WARNING *don't instantiate DataObjectFactoryCollection here*
      // because the graph engine is not fully constructed.
      // do lazy data source initialisation instead

#if ZERO

      _DataSourceCollection = new DataObjectFactoryCollection(this);
#endif

    }

    #endregion


    #region Protected Methods - Basics - Db

    /// <summary>
    /// Creates the connection to the local storage.
    /// Note: The connection is opened.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Intended for use in a <c>using</c> statement so that the
    /// connection go back to pool after commands execution.
    /// (according to msdn this is a cheap operation)
    /// this is better that having one connection open
    /// because of thread safety reasons (entre autres).
    /// </para>
    /// </remarks>
    /// <returns></returns>
    virtual protected IDbConnection CreateConnection() {
      return DbHelpers.CreateConnectionII(_ConnectionSettings);
    }



    #endregion

    #region Protected Abstract Methods - Configuration
    /// <summary>
    /// TODO: TODOC:
    /// </summary>
    protected abstract void Initialization_CreateTables();
    #endregion

    


    #region Protected Static Helper Methods - SQL
		/// <summary>
		/// Formats the given sql with the variables specified in <see cref="P:DbSettings"/>.
		/// <para>
		/// Note: Uses String.Format *after* expanding custom tags.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Base method available to all Db based providers that derive from this abstract class.
		/// </para>
		/// </remarks>
		/// <internal>
		/// This is wrapper around <see cref="M:Support.DbHelpers.FormatStringEx"/>
		/// </internal>
		/// <param name="sql"></param>
		/// <returns></returns>
		protected string FormatSql(string sql) {
			return DbHelpers.FormatStringEx(sql, _DbSettings.SqlKeyWords, _ParamPrefix);
			
		}

    /// <summary>
    /// Creates an IN clause for objects given.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Does not return surrounding brackets.
    /// </para>
    /// </remarks>
    /// <param name="items"></param>
    /// <param name="prefix"></param>
    /// <returns></returns>
    protected string CreateINClause<T>(IEnumerable<T>  items, string prefix) {
			return DbHelpers.ConvertListToInClause(items, null, prefix, true, false, false);
    }


    /// <summary>
    /// Creates an IN clause for objects given.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Does not return surrounding brackets.
    /// </para>
    /// </remarks>
    /// <param name="vertexIds"></param>
    /// <param name="prefix"></param>
    /// <returns></returns>
    protected string CreateINClause(IEnumerable<Guid> vertexIds, string prefix) {
			//THE N FORMAT WORKS ON Sqlite 
			//BUT FAILS ON SQLSERVER
			//@@@

			return DbHelpers.ConvertListToInClause(vertexIds, _GuidFormatCode, prefix, true, false, false);
    }

    #endregion

    


  }//Class:End
}//Namespace:End
