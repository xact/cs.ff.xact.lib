using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;

using System.Threading;
using System.Diagnostics;

namespace XAct.Graphs {


  /// <summary>
  /// Abstract Provider base class for GraphDbCachedProvider.
  /// Every GraphDbCachedProvider implementation should inherit from this class
  /// </summary>
  /// <remarks>
  /// <para>
  /// See <see cref="T:DbProvider"/> for an implementation of this base 
  /// class, based on generic DBMS datastore.
  /// </para>
	/// <para>
	/// This is an abstract base class that does not predetermine
	/// the storage solution. For a Db based implementation, 
	/// see <see cref="GraphDbCachedProviderBase"/>.
	/// </para>
	/// </remarks>
	/// <internal>
	/// <para>
	/// Architecturally, a Cached Graph is somewhat like the defuault
	/// Boost BiDirectional Graph, but there are important differences.
	/// </para>
	/// <para>
	/// The essence of the idea is that the Vertex and Edge lists are 
	/// MRU lists rather than traditional lists. 
	/// </para>
	/// <para>
	/// Each time a Vertex is related to, its is moved on to the front 
	/// of the MRU, leading to rarely used vertices being pushed to the 
	/// end of the MRU list.
	/// </para>
	/// <para>
	/// A second thread periodically cleans out vertices that have are
	/// beyond a certain artificial limit of the lists size (eg 5000 vertices).
	/// threads (Persist/Clean) that do different work.
	/// </para>
	/// <para>
	/// This obviously noticeably changes the architecture of the traditional
	/// Graph -- which expects all Edges and Vertices to be already in memory -- 
	/// to having two parts to it: 'inMem', and 'storage'.
	/// For example, a GetVertex(id) first looks for the Vertex in memory, and if
	/// not found, requests for it from the 'storage' area.
	/// </para>
	/// <para>
	/// This is somewhat easy enough to do with the Vertices -- the problem
	/// is really the Edges.
	/// If an Edge is removed from memory, how is a request for OutVertices to work?
	/// If Edges are being dropped (eg: 3 out of a 7 of a vertices edges have become stale)
	/// then it stands to reason that there is no clear way for a request to know whether
	/// to query a datastore for out-edges. In other words, all vertices with no out-edges
	/// (leaves or single nodes) will cause a db hit for no good reason.
	/// </para>
	/// <para>
	/// A solution to this dilema is to to ensure that when a Vertex is mounted in memory, 
	/// it needs to also load its Out-Edges, even if the Targets have not been loaded.
	/// A Bidirectional graph also means that it has to load its In-Edges, even if not
	/// all the Sources have been loaded (note that we know that in 99% of the cases atleast one Source has).
	/// </para>
	/// <para>
	/// It turns out that to implement all this is not as complicated as it at first appears.
	/// Remember that we generally use a Dictionary{Graph,EdgeList} structure to hold the out and in edges.
	/// This means that if a Vertex is removed from the dictionary, its 
	/// edge definitions automatically go with it as well (bar other pointers to the same memory address)
	/// ...simple, easy, clean.
	/// NOTE:
	/// You just have to watch out out for meta caches such as 
	/// an _CacheAllEdges array, which makes it more difficult
	/// to cleanup...
	/// </para>
	/// <para>With this implemented, it ensures that there is no reason to hit the 
	/// db when requesting an Out-Edge: either it has been mounted in memory, or it doesn't exist.
	/// What about Edges added after being mounted in memory? Through what? All edges
	/// should be added via this graph's methods. 
	/// NOTE:
	/// On another machine in a farm?
	/// This could be handled via an 'BestBefore' expiration date before it gets too stale
	/// to consume -- or some kind of farm-wide event. To be designed laer.
	/// </para>
	/// <para>
	/// One of the tricks to remember is that doing
	/// <code>
	/// vertex = _Cache[id];
	/// or
	/// EdgeList l = _Cache[id];
	/// </code>
	/// is no longer good enough. You have to actually
	/// always trigger the full check/load/return sequence by doing
	/// <code>
	/// vertex = GetVertex(id);
	/// </code>
	/// </para>
	/// <para>
	/// Is there any chance a QuickGraph algorythm will bypass all this mechanism?
	/// No. Or atleast, I don't think so. None of the quickgraph algo's 
	/// have direct access to a Graph's private or proteced inner lists -- only the public
	/// interface methods to manipulate them.
	/// </para>
	/// <para>
	/// What about Graph.VertexCount, EdgeCount, enumerations, etc.?
	/// That's the toughest question.
	/// I think that that's a rare situation, where one has to query the backend (atleast for counts)
	/// and not the in-mem.
	/// Or it could be a complicated Init() sequence, plus keeping count of what was added/removed, etc...
	/// Except I don't see a good reason for it. Count should be a rare request -- and therefore
	/// no need to introduce a whole set of potential bugs for very little gain.
	/// As for iterations. Shit.
	/// I think I see no way other to do this properly than iterating over the 
	/// backend...Although that's awful in terms of speed.
	/// </para>
	/// <para>
	/// Another point to cover is the issue of Edges working with Ids rather than Target Vertex.
	/// The point is that if a Vertex is not loaded into memory, it makes no sense to have an Edge
	/// pointing to a Null.
	/// So you point to a Guid, which is wrapped by a custom get; which lazy fetches the Vertex
	/// via the GetVertex() method which retrieves it from mem, or from storage.
	/// Note that the edge does *not* hold on to the src and target via a private property
	/// but *always* retrieves the vertex via the GetVertex(). This avoids the havoc of extra
	/// pointers pointing to different vertices.
	/// </para>
	/// <para>
	/// It unfortunately means also that the Generic Edge definition cannot allow
	/// just any old Edge -- but an edge that ensures a wrapping around of GetVertex()
	/// </para>
	/// </internal>
	abstract public partial class GraphProviderBase : 
		ProviderBase,
		IGraphEngine,
		IGraphSyncServer
	{



		#region Events Raised

		/// <summary>
		/// Event raised after a Vertex is created.
		/// </summary>
		public event EventHandler<GraphVertexEventArgs> VertexCreated;

		/// <summary>
		/// Event raised after a Vertex is Updated.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Raised if Vertex is updated in memory, but not yet necessarily persisted to storage.
		/// </para>
		/// </remarks>
		public event EventHandler<GraphVertexEventArgs> VertexUpdated;

		/// <summary>
		/// Cancelable Event raised before a Vertex is released from memory by the Cleanup thread.
		/// </summary>
		public event EventHandler<CancelVertexEventArgs> VertexReleasing;

		/// <summary>
		/// Event raised after a Vertex is released from memory by the Cleanup thread.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Releasing a Vertex from memory causes no change to the underlying storage.
		/// </para>
		/// </remarks>
		public event EventHandler<GuidEventArgs> VertexReleased;


		/// <summary>
		/// Cancelable Event raised before a Vertex is deleted.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Raised prior to removing the Vertex from the Graph.
		/// </para>
		/// </remarks>
		public event EventHandler<CancelVertexEventArgs> VertexDeleting;

		/// <summary>
		/// Event raised after a Vertex is deleted.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Raised after a Vertex is deleted from the Graph, but not yet necessarily removed from storage.
		/// </para>
		/// </remarks>
		public event EventHandler<GraphVertexEventArgs> VertexDeleted;






		/// <summary>
		/// Cancelable event raised before an Edge is created.
		/// </summary>
		public event EventHandler<CancelEdgeEventArgs> EdgeCreating;

		/// <summary>
		/// Event raised after an Edge is created.
		/// </summary>
		/// <para>
		/// Raised after an Edge is created in memory, but not yet necessarily persisted to storage.
		/// </para>
		public event EventHandler<EdgeEventArgs> EdgeCreated;

		/// <summary>
		/// Cancelable event raised before an Edge is updated.
		/// </summary>
		public event EventHandler<CancelEdgeEventArgs> EdgeUpdating;

		/// <summary>
		/// Event raised after an Edge is updated.
		/// </summary>
		/// <para>
		/// Raised after an Edge is updated in memory, but not yet necessarily persisted to storage.
		/// </para>
		public event EventHandler<EdgeEventArgs> EdgeUpdated;

		/// <summary>
		/// Cancelable event raised before an Edge is about to be deleted.
		/// </summary>
		public event EventHandler<CancelEdgeEventArgs> EdgeDeleting;

		/// <summary>
		/// Event raised after an Edge is Deleted.
		/// </summary>
		/// <para>
		/// Raised after an Edge is deleted from the Graph, but not yet necessarily removed to storage.
		/// </para>
		public event EventHandler<EdgeEventArgs> EdgeDeleted;




		/// <summary>
		/// Event raised when a Vertex is asked for that is not in the local cache.
		/// NOTE:
		/// This event can be used to try getting the Vertex from a remote cache.
		/// </summary>
		public event EventHandler<GuidEventArgs> VertexMissing;

#if ZERO

    /// <summary>
    /// Event raised when a Vertex is asking for an exterior
    /// provider to fill its Data object.
    /// </summary>
    //public event EventHandler<DataObjectEventArgs> DataObjectFillRequest;


    /// <summary>
    /// Event raised when a Vertex is asking for an exterior
    /// provider to save its Data object.
    /// </summary>
    //public event EventHandler<DataObjectSaveEventArgs> DataObjectSaveRequest;


    /// <summary>
    /// Event raised when a Vertex is asking for an exterior
    /// provider to delete its Data object.
    /// </summary>
    //public event EventHandler<Events.DataObjectEventArgs> DataObjectDeleteRequest;

#endif

		#endregion

		#region Fields

		//Boost<GraphVertex, GraphEdge> _Boost = new Boost<GraphVertex, GraphEdge>(GraphVertex, GraphEdge)();


    /// <summary>
    /// Tracing Source.
    /// </summary>
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
    protected static TraceSource myTrace = new TraceSource("GraphProvider");
#endif
    #endregion

		

		#region Properties
		/// <summary>
		/// Gets the Application's base path.
		/// </summary>
		/// <value>The app dir.</value>
		protected static string AppDir {
			get {
				return XAct.Core.Utils.Env.AppDir;
			}
		}
		#endregion

		#region Properties - User
		/// <summary>
    /// Id key of current User.
    /// </summary>
    /// <internal>
    /// <![CDATA[
    /// IMPORTANT:
    /// Merging asp.net with thread principal and getting it to work:
    /// http://www.artima.com/forums/flat.jsp?forum=152&thread=69672
    /// Maybe (?):
    /// http://msmvps.com/blogs/theproblemsolver/archive/2006/01/12/80905.aspx
    /// ]]>
    /// </internal>
    /// <value></value>
    virtual public System.Security.Principal.IIdentity UserId {
      get {
        //I *hate* this....
        //What I really want is a UserId.
        //but the Generic IIDentity/IPrincipal doesn't carry it around.
        //Only the name.
        //Which means the non-deterministic UserName of a person is as good
        //as it could get.....unless you fix it to a specific type of  
        return System.Threading.Thread.CurrentPrincipal.Identity;
      }
    }
    #endregion


		#region Non-Abstract Methods - Vertexes



#if ZERO
    /// <summary>
    /// Gets a collection of all the vertices registered for the given dataSource typeName.
    /// </summary>
    /// <param name="typename"></param>
    /// <returns>A GraphVertexList (may be zero-size, but never null).</returns>
    /// <exception cref="ArgumentNullException">An exception is raised if typename is null/empty.</exception>
    public GraphVertexList GetVertices(string typename) {
      //Check Args:
      if (string.IsNullOrEmpty(typename)) {
        throw new System.ArgumentNullException("typename");
      }
      return GetVertices(DataSourceManager.Instance[typename]);
      //return GetVertices(GetDataSource(typename));
    }
#endif

		#endregion

		#region Non-Abstract Methods - Edges





    /// <summary>
    /// Returns an enumeration of vertices whose have a out Edge pointing
    /// to the Vertex with the specified id.
    /// </summary>
    /// <param name="vertexId">The Vertex id.</param>
    /// <exception cref="VertexNotFoundException">An Exception is raised if the specified Vertex is not found</exception>
    /// <returns></returns>
    public IEnumerable<GraphVertex> InVertices(Guid vertexId) {
			
      //No Check Done. OK.
      GraphEdgeList needsCleaning = new GraphEdgeList();
      foreach (GraphEdge edge in InEdges(vertexId)) {
        GraphVertex v = edge.Source;
        yield return v;
      }
    }


		/// <summary>
		/// Return an enumeration of the vertices pointed to by the out.edges for the Vertex with the
		/// specified Id.
		/// the returned enumeration is never null, but can be empty
		/// </summary>
		/// <param name="vertexId">The Vertex Id.</param>
		/// <returns>An enumerable collection of Vertex instances (zero-size or more, unless exception raised).</returns>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertexId is null/empty.</exception>
		public IEnumerable<GraphVertex> OutVertices(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

			GraphVertex v = GetVertex(vertexId);
			return OutVertices(v);

		}



		/// <summary>
		/// Return an enumeration of the vertices pointed to by the out.edges for the Vertex with the
		/// specified Id.
		/// the returned enumeration is never null, but can be empty
		/// </summary>
		/// <param name="vertex">The Vertex.</param>
		/// <returns>An enumerable collection of Vertex instances (zero-size or more, unless exception raised).</returns>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertex is null.</exception>
		public IEnumerable<GraphVertex> OutVertices(GraphVertex vertex) {
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			GraphEdgeList needsCleaning = new GraphEdgeList();

			foreach (GraphEdge edge in OutEdges(vertex.Id)) {
				//Gets target to force GraphManager to *Load* vertex if it is not in memory...
				GraphVertex v = edge.Target;
				yield return v;
			}
		}

    





    /// <summary>
    /// Add the specifiedEdges.
    /// Returns <c>true</c> only if all Edges were created.
    /// Returns <c>false</c> if any Edge already existed.
    /// </summary>
    /// <param name="edgesCreated">The Result number of new edges actually created.</param>
    /// <param name="edges">TheEdges definitions.</param>
    /// <returns>
    /// the number ofEdges effectively created
    /// </returns>
    /// <remarks>
    /// 	<para>
    /// Invokes <see cref="M:OnEdgeCreated"/> for each Edge successfully created.
    /// </para>
    /// 	<para>
    /// No error is raised if an Edge already exists between the two
    /// Vertex instances:
    /// it just returns <c>false</c>, and <see cref="M:OnEdgeCreated"/>
    /// is never invoked.
    /// </para>
    /// </remarks>
    /// <para>
    /// Returns <c>true</c> only if an Edge was created.
    /// Returns <c>false</c> if an Edge already existed.
    /// </para>
    virtual public bool AddEdges(out int edgesCreated, params GraphEdge[] edges) {
      //Check Args:
      if (edges == null) {
        throw new System.ArgumentNullException("edgesDef");
      }
      edgesCreated = 0;

			foreach (GraphEdge edge in edges) {
        if (AddEdge(edge)) {
					++edgesCreated;
        }
      }
			return (edgesCreated == edges.Length);
    }


    #endregion

		#region Protected - Raise Events

		/// <summary>
		/// Raises the <see cref="E:VertexCreated"/> event.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="M:CreateVertex"/>
		/// </para>
		/// </remarks>
		/// <param name="e">The <see cref="T:GraphVertexEventArgs"/> instance containing the event data.</param>
		virtual protected void OnVertexCreated(GraphVertexEventArgs e) {
			if (VertexCreated != null) {
				VertexCreated(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:VertexDeleting"/> event.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="RemoveVertex(Guid)"/>.
		/// </para>
		/// </remarks>
		/// <param name="e">The <see cref="T:CancelVertexEventArgs"/> instance containing the event data.</param>
		virtual protected void OnVertexRemoving(CancelVertexEventArgs e) {
			if (VertexDeleting != null) {
				VertexDeleting(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:VertexDeleted"/> event.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="M:RemoveVertex"/> if not cancelled.
		/// </para>
		/// </remarks>
		/// <param name="e">The <see cref="T:GuidEventArgs"/> instance containing the event data.</param>
		virtual protected void OnVertexRemoved(GraphVertexEventArgs e) {
			if (VertexDeleted != null) {
				VertexDeleted(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:VertexUpdated"/> event.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="M:UpdateVertex"/>
		/// </para>
		/// </remarks>
		/// <param name="e">The <see cref="T:GraphVertexEventArgs"/> instance containing the event data.</param>
		virtual protected void OnVertexUpdated(GraphVertexEventArgs e) {
			if (VertexUpdated != null) {
				VertexUpdated(this, e);
			}
		}


		/// <summary>
		/// Raises the <see cref="E:EdgeCreating"/> event.
		/// </summary>
		/// <param name="e">The <see cref="T:CancelEdgeEventArgs"/> instance containing the event data.</param>
		virtual protected void OnEdgeCreating(CancelEdgeEventArgs e) {
			if (EdgeCreating != null) {
				EdgeCreating(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:EdgeCreated"/> event.
		/// </summary>
		/// <param name="e">The <see cref="T:EdgeEventArgs"/> instance containing the event data.</param>
		virtual protected void OnEdgeCreated(EdgeEventArgs e) {
			if (EdgeCreated != null) {
				EdgeCreated(this, e);
			}
		}



		/// <summary>
		/// Raises the <see cref="E:EdgeUpdating"/> event.
		/// </summary>
		/// <param name="e">The <see cref="T:CancelEdgeEventArgs"/> instance containing the event data.</param>
		virtual protected void OnEdgeUpdating(CancelEdgeEventArgs e) {
			if (EdgeUpdating != null) {
				EdgeUpdating(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:EdgeUpdated"/> event.
		/// </summary>
		/// <param name="e">The <see cref="T:EdgeEventArgs"/> instance containing the event data.</param>
		virtual protected void OnEdgeUpdated(EdgeEventArgs e) {
			if (EdgeUpdated != null) {
				EdgeUpdated(this, e);
			}
		}


		/// <summary>
		/// Raises the <see cref="E:EdgeDeleting"/> event.
		/// </summary>
		/// <param name="e">The <see cref="CancelEdgeEventArgs"/> instance containing the event data.</param>
		virtual protected void OnEdgeDeleting(CancelEdgeEventArgs e) {
			if (EdgeDeleting != null) {
				EdgeDeleting(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:EdgeDeleted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="T:EdgeEventArgs"/> instance containing the event data.</param>
		virtual protected void OnEdgeDeleted(EdgeEventArgs e) {
			if (EdgeDeleted != null) {
				EdgeDeleted(this, e);
			}
		}



		/// <summary>
		/// Raises the <see cref="E:VertexMissing"/> event.
		/// </summary>
		/// <internal>
		/// OLIVIER: This is never called....what's it for?
		/// </internal>
		/// <param name="e">The <see cref="T:GuidEventArgs"/> 
		/// instance containing the event data.</param>
		virtual protected void OnVertexMissing(GuidEventArgs e) {
			if (VertexMissing != null) {
				VertexMissing(this, e);
			}
		}



		/// <summary>
		/// Raises the <see cref="E:VertexReleasing"/> event.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked before a non-deleted Vertex has been released from memory.
		/// </para>
		/// </remarks>
		/// <param name="e">The <see cref="T:CancelVertexEventArgs"/> instance containing the event data.</param>
		virtual protected void OnVertexReleasing(CancelVertexEventArgs e) {
			if (VertexReleasing != null) {
				VertexReleasing(this, e);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:VertexReleased"/> event.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked after a Vertex has been released from memory.
		/// </para>
		/// </remarks>
		/// <param name="e">The <see cref="T:GraphVertexEventArgs"/> instance containing the event data.</param>
		virtual protected void OnVertexReleased(GuidEventArgs e) {
			if (VertexReleased != null) {
				VertexReleased(this, e);
			}
		}


#if ZERO
    /// <summary>
    /// Raises the <see cref="E:DataObjectFillRequest"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:DataObjectEventArgs"/> instance containing the event data.</param>
    virtual protected void OnDataObjectFillRequest(DataObjectEventArgs e) {
      if (DataObjectFillRequest != null) {
        DataObjectFillRequest(this, e);
      } else {
        throw new System.Exception("OnDataObjectFillRequest was called, but there was no Handler defined for it.");
      }
    }
#endif

		#endregion

    #region Protected Methods - Tracing

    /// <summary>
    /// all following methods are helpers function which
    /// call myTrace.TraceEvent(evType, 0, message,...)
    /// </summary>
    /// <param name="evType"></param>
    /// <param name="message"></param>
    protected void trace(TraceEventType evType, string message) {
      myTrace.TraceEvent(evType, 0, message);
    }

    /// <summary>
    /// Traces the specified TraceEventType.
    /// </summary>
    /// <param name="evType">The TraceEventType.</param>
    /// <param name="format">The format.</param>
    /// <param name="args">The args.</param>
    protected void trace(TraceEventType evType, string format, params Object[] args) {
      myTrace.TraceEvent(evType, 0, format, args);
    }

    /// <summary>
    /// TODO
    /// </summary>
    /// <param name="message"></param>
    protected void traceVerbose(string message) {
      trace(TraceEventType.Verbose, message);
    }

    /// <summary>
    /// TODO
    /// </summary>
    /// <param name="format"></param>
    /// <param name="args"></param>
    protected void traceVerbose(string format, params Object[] args) {
      trace(TraceEventType.Verbose, format, args);
    }

    /// <summary>
    /// Traces the information.
    /// </summary>
    /// <param name="message">The message.</param>
    protected void traceInformation(string message) {
      trace(TraceEventType.Information, message);
    }

    /// <summary>
    /// Traces the information.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="args">The args.</param>
    protected void traceInformation(string format, params Object[] args) {
      trace(TraceEventType.Information, format, args);
    }

    /// <summary>
    /// Traces the error.
    /// </summary>
    /// <param name="message">The message.</param>
    protected void traceError(string message) {
      trace(TraceEventType.Error, message);
    }

    /// <summary>
    /// Traces the error.
    /// </summary>
    /// <param name="format">The format.</param>
    /// <param name="args">The args.</param>
    protected void traceError(string format, params Object[] args) {
      trace(TraceEventType.Error, format, args);
    }

    #endregion
    
  }//Class:End
}//Namespace:End
