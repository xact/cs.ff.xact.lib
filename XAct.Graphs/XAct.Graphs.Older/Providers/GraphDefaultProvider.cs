using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Graphs {
  /// <summary>
  /// Default Graph Provider (derived from <see cref="T:GraphDbCachedProvider"/>).
  /// Name is 'Default'.
  /// </summary>
  /// <remarks>
  /// <para>
  /// <b>Configuration:</b><br/>
  /// <code>
  /// <![CDATA[
  ///   <configSections>
  ///     <sectionGroup name="Graphs">
  ///       <sectionGroup name="XAct">
  ///         <section name="Graph" type="XAct.Graphs.GraphSection, GraphEngine"/>
  ///       </sectionGroup>
  ///     </sectionGroup>
  ///   </configSections>
  ///   ...
  ///   <Graph defaultProvider="Default">
  ///      <providers>
  ///        <add name="Default" type="XAct.Graphs.GraphDefaultProvider, XAct.Graphs.GraphManager"
  ///             connectionName="localDB" 
  ///             dbSettings="default"
  ///             cleanupInterval="10" 
  ///             cleanupMinVertices="0" 
  ///             cleanupTimeOut="20"
  ///             flushInterval="60"/>
  ///      </providers>
  ///
  ///      <dbSettings>
  ///        <add name="default"
  ///             dataSourceTable="DataSource"
  ///             vertexTable="Vertex"
  ///             edgeTable="Edge"/>
  ///      </dbSettings>
  ///    </Graph>
  /// ]]>
  /// </code>
  /// </para>
  /// </remarks>
  public class GraphDefaultProvider : GraphDbCachedProvider {

    #region Implementation of ProviderBase
    /// <summary>
    /// Gets the friendly name used to refer to the provider during configuration.
    /// </summary>
    /// <value></value>
    /// <returns>The friendly name used to refer to the provider during configuration.</returns>
    public override string Name {
      get {
        return string.IsNullOrEmpty(base.Name) ? "Default" : base.Name;
      }
    }
    #endregion


  }//Class:End
}//Namespace:Ends