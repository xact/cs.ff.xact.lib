﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics; 
using System.Text;

using System.Timers;

using XAct.Core.Generics;
using XAct.Core.Utils;

namespace XAct.Graphs {
	abstract public partial class GraphProviderBase
		{


		#region Fields - Configuration Section related
		/// <summary>
		/// Attribute name for the cleanup thread interval (in seconds).
		/// </summary>
		public static readonly string cleanupIntervalAttributeName = "cleanupInterval";

		/// <summary>
		/// Attribute name for the minimum number of vertice in memory before we cleanup.
		/// </summary>
		public static readonly string cleanupMinVerticesAttributeName = "cleanupMinVertices";

		/// <summary>
		/// Attribute name for cleanup thread's GraphVertex timeout value.
		/// </summary>
		public static readonly string cleanupTimeOutAttributeName = "cleanupTimeOut";

		/// <summary>
		/// Attribute name for the flush thread interval (in seconds).
		/// </summary>
		public static readonly string flushIntervalAttributeName = "flushInterval";

		#endregion //End Fields (All)


		#region Public Methods - Implementation of ProviderBase

		/// <summary>
		/// Initializes the Graph Engine.
		/// called by Manager/Provider mechanic.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the
		/// provider-specific attributes specified in the configuration for this provider.</param>
		public override void Initialize(string name, NameValueCollection config) {

			traceInformation("GraphProviderBase Initialization");

			base.Initialize(name, config);

			//WARNING *don't instantiate DataObjectFactoryCollection here*
			// because the graph engine is not fully constructed.
			// do lazy data source initialisation instead

#if ZERO

      _DataSourceCollection = new DataObjectFactoryCollection(this);
#endif

			/* cached graph initialisation */

			string txt = config[cleanupIntervalAttributeName];

			if (!String.IsNullOrEmpty(txt)) {

				try {
					_CleanupInterval = Convert.ToInt32(txt);
				}
				catch (Exception e) {

					throw new Exception(String.Format("bad value for '{0}' attribute : '{1}'",
																										cleanupIntervalAttributeName, txt), e);
				}
			}

			txt = config[cleanupMinVerticesAttributeName];

			if (!String.IsNullOrEmpty(txt)) {

				try {

					_CleanupVertexCountThreshold = Convert.ToInt32(txt);

				}
				catch (Exception e) {

					throw new Exception(String.Format("bad value for '{0}' attribute : '{1}'",
																									 cleanupMinVerticesAttributeName, txt), e);
				}
			}

			txt = config[cleanupTimeOutAttributeName];

			if (!String.IsNullOrEmpty(txt)) {

				try {

					int seconds = Convert.ToInt32(txt);
					_cleanupVertexTimeout = new TimeSpan(0, 0, seconds);

				}
				catch (Exception e) {

					throw new Exception(String.Format("bad value for '{0}' attribute : '{1}'",
																										cleanupTimeOutAttributeName, txt), e);
				}
			}


			_CachedAllVertices = new GraphMRUVertexList();

			_CachedVertexOutEdges = new GraphVertexIdEdgeDictionary();


			/* cleanup stuffs */

			_CleanupTimer = new Timer();
			_CleanupTimer.Elapsed += OnCleanupEvent;
			_CleanupTimer.Interval = this.CleanupInterval * 1000;
			_CleanupTimer.Enabled = true;


			/* Flush stuffs */

			txt = config[flushIntervalAttributeName];
			if (!String.IsNullOrEmpty(txt)) {
				try {
					_FlushInterval = Convert.ToInt32(txt);
				}
				catch (Exception e) {
					throw new Exception(String.Format("bad value for '{0}' attribute : '{1}'",
																											flushIntervalAttributeName, txt), e);
				}
			}

			_FlushTimer = new Timer();
			_FlushTimer.Elapsed += OnFlushEvent;
			_FlushTimer.Interval = _FlushInterval * 1000;
			_FlushTimer.Enabled = true;


			/* everything is initialized */

			traceInformation(
@"GraphDbCachedProvider initialised. 
cleanup interval = {0}, minVertices = {1} timeout = {2}
flush period = {3}",
											 this.CleanupInterval, this.CleanupVertexCountThreshold, this.CleanupVertexTimeout, this.FlushInterval);
		}

		#endregion



		#region Public Methods - Implementation of GraphProviderBase - Vertices

		/// <summary>
		/// Performs a DepthFirstSearch starting from the given vertex,
		/// ensuring the vertices are in Memory.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <param name="depth">The max depth (default is 4).</param>
		public void Spider(GraphVertex vertex, int depth) {
			if (depth == 0) {
				depth = 4;
			}
			DepthFirstUndirectedSearchAlgorythm search = new DepthFirstUndirectedSearchAlgorythm(this, vertex,depth);
			search.Compute(vertex);
			//QuickGraph.Algorithms.Observers.VertexRecorderObserver<GraphVertex,GraphEdge>
		}

		/// <summary>
		/// Create a new GraphVertex with the specified data source id and data record id.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invokes <see cref="M:GraphProviderBase.OnVertexCreated"/>.
		/// </para>
		/// <para>
		/// The <see cref="P:Vertex.Data"/> property is set to <c>null</c>.
		/// </para>
		/// <para>
		/// A potential candidate for something to do with 
		/// <see cref="QuickGraph.IVertexFactory{TVertex}"/>
		/// </para>
		/// </remarks>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <param name="dataObjectFactoryId">The data source id.</param>
		/// <param name="dataRecordId">The data record id.</param>
		/// <returns>a newly created vertex</returns>
		/// <exception cref="ArgumentNullException">An Exception is raised if dataObjectFactoryId is 0.</exception>
		/// <exception cref="ArgumentNullException">An Exception is raised if dataRecordId is empty.</exception>
		public GraphVertex CreateVertex(int dataObjectFactoryId, string dataRecordId) {
			//Check Args:
			if (dataObjectFactoryId == 0) {
				throw new System.ArgumentNullException("dataObjectFactoryId");
			}
			if (string.IsNullOrEmpty(dataRecordId)) {
				throw new System.ArgumentNullException("dataRecordId");
			}

			GraphVertex vertex = new GraphVertex();
			((IVertexInitialization)vertex).InitializeAsNew(dataObjectFactoryId, dataRecordId);

			AddVertex(vertex);

			/* NEVER UPDATE HERE...WAIT TILL UPDATE CREATED:
			if (UpdateInRealTime) {
				Flush(vertex);
			}
			*/

			OnVertexCreated(new GraphVertexEventArgs(vertex));

			return vertex;
		}


		/// <summary>
		/// Updates the vertex.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invokes <see cref="M:OnVertexUpdated"/>.
		/// </para>
		/// </remarks>
		/// <param name="vertex">The vertex.</param>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertex is null.</exception>
		public bool UpdateVertex(GraphVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//Get from *all*, or load from db into memory (*all*):
			GraphVertex checkVertex = GetVertex(vertex.Id);


			//OLIVIER: What is this '(checkVertex != vertex)' check for really???
			if (checkVertex != vertex) {
				//#warning "we should replace the old (checkVertex) vertex with the new one (vertex)"
				throw new Exception("update vertex error (checkVertex != vertex)");
			}


			//Update the vertex edited date:
			//(IMPORTANT: Which also will set the the Status to UpdatePending):
			vertex.UTCDateEdited = DateTime.Now.ToUniversalTime();

			if (UpdateInRealTime) {
				//Keep the storage up to date, rather than waiting 
				//for a future time:
				Flush(vertex);
			}

			//Raise an event that the GraphVertex has been updated (in memory only at this point):
			OnVertexUpdated(new GraphVertexEventArgs(vertex));

			//Always true:
			return true;
		}

		/// <summary>
		/// Updates the passed vertex.
		/// </summary>
		/// <param name="vertices">The vertices.</param>
		/// <returns></returns>
		/// <remarks>
		/// this method is used to signal that the vertex has been modified in memory
		/// and that we want to commit thoses changes in local persistent storage.
		/// </remarks>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertex list is null.</exception>
		public bool UpdateVertices(IEnumerable<GraphVertex> vertices) {
			bool result = true;
			foreach (GraphVertex vertex in Vertices) {
				result &= UpdateVertex(vertex);
			}
			return result;
		}





		/// <summary>
		/// Removes (deletes) the vertex. 
		/// <para>
		/// Removes the memory from the graph, then marks its record as 
		/// DeletePending to be handled by Flush later.
		/// </para>
		/// <para>
		/// Raises <see cref="OnVertexRemoving"/> and <see cref="OnVertexRemoved(GraphVertexEventArgs)"/>.
		/// </para>
		/// </summary>
		/// <param name="vertexId">The vertex Id.</param>
		/// <returns>
		/// true if vertex has been deleted, false otherwise.
		/// </returns>
		/// <remarks>
		/// <para>
		/// Invokes  <see cref="M:OnVertexRemoving"/> prior to
		/// deleting the Vertex, and <see cref="M:OnVertexRemoved"/> afterwards.
		/// </para>
		/// </remarks>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the vertex is not found.</exception>
		public bool RemoveVertex(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

			//You can't remove a vertex that doesn't exist somewhere
			//So one should throw an error requesting the removal
			//of a Guid that doesn't exist.

			//Get from *all*, or load from db into memory (*all*):
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(vertexId);

			//Invoke the QuickGraph method:
			return RemoveVertex(vertex);

		}



		/// <summary>
		/// Removes (deletes) the vertices in the given list from both memory and underlying storage .
		/// </summary>
		/// <param name="vertexIdList">The vertex id list.</param>
		/// <returns>True if all vertexes deleted.</returns>
		/// <remarks>
		/// </remarks>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertex list is null.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the vertex is not found.</exception>
		public bool RemoveVertices(IEnumerable<Guid> vertexIdList) {
			int deleted = 0;
			foreach (Guid id in vertexIdList) {
				if (RemoveVertex(id)) {
					deleted++;
				}
			}
			return true;
		}

		/// <summary>
		/// Returns the GraphVertex with the specified Id.
		/// Returns null if not found.
		/// Searches memory cache first, and if not found, loads it into memory from storage.    
		/// </summary>
		/// <remarks>
		/// <para>
		/// No error if GraphVertex record not found -- returns null.
		/// </para>
		/// </remarks>
		/// <param name="vertexId">The vertex's unique Id.</param>
		/// <returns>
		/// Return the vertex with the specified id,
		/// or null if the GraphVertex doesn't exist.
		/// </returns>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
		public GraphVertex GetVertex(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

			GraphVertex vertex;
			if (!TryGetVertex(vertexId, out vertex)) {
				throw new System.ArgumentException("Vertex not found.");
			}
			return vertex;
		}

		/// <summary>
		/// Tries to return the Vertex with the given Id.
		/// <para>
		/// Unlike <see cref="GetVertex(Guid)"/>, does not throw an error if the vertex is not found.
		/// </para>
		/// </summary>
		/// <param name="vertexId"></param>
		/// <param name="vertex"></param>
		/// <returns></returns>
		public bool TryGetVertex(Guid vertexId, out GraphVertex vertex) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

			lock (this) {
				//See if the vertex is already in mem (*all* collection):
				vertex = inMemGetVertex(vertexId);

				if (vertex == null) {
					// not in memory, so see if we can load it up from local storage
					// into *all* collection:
					vertex = storageVertexGet(vertexId);
				}
			}

			return (vertex != null);
		}



		/// <summary>
		/// Gets the vertices that match the specified GraphVertex 
		/// Searches memory cache first, and if not found, loads it into memory from storage.    
		/// </summary>
		/// <remarks>
		/// 	<para>
		/// Collects together the Vertexes from memory,
		/// and if not found in memory, searchs within storage to complete the collection.
		/// </para>
		/// <para>
		/// No error is raised if not all vertices are found, but it does return <c>False</c>
		/// in that case.
		/// </para>
		/// </remarks>
		/// <param name="vertexIds">A list of </param>
		/// <param name="resultCollection">A vertex collection to append the results to.</param>
		/// <param name="missingVertexIds"></param>
		/// <returns>True if all vertices found.</returns>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertexIds is null.</exception>
		/// <exception cref="ArgumentNullException">An exception is raised if the resultCollection is null.</exception>
		public bool GetVertices(ICollection<Guid> vertexIds, ref GraphVertexList resultCollection, out ICollection<Guid> missingVertexIds) {
			if (vertexIds == null) {
				throw new System.ArgumentNullException("vertexIds");
			}
			if (resultCollection == null) {
				throw new System.ArgumentNullException("resultCollection");
			}

			lock (this) {
				// First, get vertices already in memory,
				//without raising an error that there are some missing:
				ICollection<Guid> missingInMemVertexIds;
				if (inMemGetVertices(vertexIds, ref resultCollection, out missingInMemVertexIds)) {
					//Clear the 'missing' collection:
					missingVertexIds = null;
					return true;
				}
				else {
					missingVertexIds = missingInMemVertexIds;
					//If there were any that were missing,
					//use that list to query in storage:
					ICollection<Guid> missingStorageVertexIds;
					if (storageGetVertices(missingInMemVertexIds, ref resultCollection, out missingStorageVertexIds)) {
						//Clear the 'missing' collection:
						missingVertexIds = null;
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Gets the vertices from the specified data source id and
		/// with the specified objects 
		/// </summary>
		/// <remarks>
		/// <para>
		/// No error is raised if not all vertices are found, but it does return <c>False</c>
		/// in that case.
		/// </para>
		/// <para>
		/// Searches for vertices in memory first. Any not found are then searched for in storage.
		/// </para>
		/// </remarks>
		/// <param name="dataObjectFactoryId">The data source id.</param>
		/// <param name="dataObjectIds">The dataObject </param>
		/// <param name="resultCollection">A vertex collection to append the results to.</param>
		/// <param name="missingRecordIds">A result collection of the record ids not found.</param>
		/// <param name="createIfNotFound">if <c>true</c>, 
		/// and if there is object ids with no correspondant vertices then the
		/// vertices are created on the fly.</param>
		/// <returns>True if vertices for each record id were found.</returns>
		/// <exception cref="ArgumentNullException">An exception is raised if dataObjectFactoryId is 0.</exception>
		/// <exception cref="ArgumentNullException">An exception is raised if dataObjectIds is null.</exception>
		public bool GetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds, ref GraphVertexList resultCollection, out ICollection<string> missingRecordIds, bool createIfNotFound) {
			//Check Args:
			if (dataObjectFactoryId == 0) {
				throw new System.ArgumentNullException("dataObjectFactoryId");
			}
			if (dataObjectIds == null) {
				throw new System.ArgumentNullException("dataObjectIds");
			}
			lock (this) {
				ICollection<string> inMemMissingRecordIds;
				// First, get vertices already in memory:
				if (inMemGetVertices(dataObjectFactoryId, dataObjectIds, ref resultCollection, out inMemMissingRecordIds)) {
					//Have all vertices:
					missingRecordIds = null;
					return true;
				}
				//Missing some vertices:
				missingRecordIds = inMemMissingRecordIds;


				//Get the missing vertices from local storage:
				ICollection<string> storageMissingRecordIds;
				if (storageGetVertices(dataObjectFactoryId, inMemMissingRecordIds, ref resultCollection, out storageMissingRecordIds)) {
					//All found.
					missingRecordIds = null;
					return true;
				}

				missingRecordIds = storageMissingRecordIds;


				if (createIfNotFound) {
					// create remaining missing vertices ...
					GraphVertexList newVertices = new GraphVertexList();
					Collections.ForEach<string>(missingRecordIds,
						delegate(string recordId) {
							traceInformation("adding missing vertex for data source = {0} record id = {0}",
								dataObjectFactoryId, recordId);
							newVertices.Add(CreateVertex(dataObjectFactoryId, recordId));
						});
					resultCollection.AddRange(newVertices);
					//Clear the missing list:
					missingRecordIds = null;
					return true;
				}//End:createIfNotFound
			}//End:Lock


			return false;
		}

		#endregion

		#region Public Methods - Implementation of GraphProviderBase - GraphVertex Attributes
		/// <summary>
		/// For Internal use only.
		/// Initializes a Vertex's User Attributes.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Intended to only be called by <see cref="T:Vertex"/>'s Indexer.
		/// </para>
		/// </remarks>
		/// <param name="vertex">The GraphVertex whose user attributes are to be initialized.</param>
		/// <returns>True if attributes were found.</returns>
		/// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
		public bool InitializeUserAttributes(GraphVertex vertex) {
			return storageVertexUserAttributesLoad(vertex);
		}
		#endregion

		#region Public Methods - Implementation of GraphProviderBase - Edges


		/// <summary>
		/// Returns an edge between the two endpoints.
		/// <para>
		/// Unlike <see cref="TryGetEdge(GraphVertex, GraphVertex, out GraphEdge)"/>, throws an exception if no edge is found.
		/// </para>
		/// </summary>
		/// <param name="source">The source vertex.</param>
		/// <param name="target">The target vertex.</param>
		/// <returns></returns>
		public GraphEdge GetEdge(GraphVertex source, GraphVertex target) {

			GraphEdge foundEdge;
			if (!TryGetEdge(source, target, out foundEdge)) {
				throw new System.ArgumentException("Cannot find edge specified.");
			}
			return foundEdge;
		}

		/// <summary>
		/// Tries to return an Edge between the two specified endpoints.
		/// </summary>
		/// <param name="source">The source vertex.</param>
		/// <param name="target">The target vertex.</param>
		/// <param name="foundEdge">The found edge.</param>
		/// <returns></returns>
		public bool TryGetEdge(GraphVertex source, GraphVertex target, out GraphEdge foundEdge) {
			if ((source == null) || (target == null)){
				foundEdge = null;
				return false;
			}

			return TryGetEdge(source.Id, target.Id, out foundEdge);
		}


		/// <summary>
		/// Tries to return an Edge between the two specified endpoint 
		/// <para>
		/// If needed, lazy-loads the specified vertices.
		/// </para>
		/// </summary>
		/// <param name="sourceId">The source id.</param>
		/// <param name="targetId">The target id.</param>
		/// <param name="foundEdge">The found edge.</param>
		/// <returns></returns>
		public bool TryGetEdge(Guid sourceId, Guid targetId, out GraphEdge foundEdge) {

			if ((sourceId == Guid.Empty) || (targetId == Guid.Empty)) {
				//No way tht I can ascertain if an edge exists if I don't have id's of both ends.
				foundEdge = null;
				return false;
			}

			foundEdge = null;
			GraphVertex source;

			if (!TryGetVertex(sourceId, out source)) {
				foundEdge = null;
				return false;
			}
				foreach (GraphEdge edge in _CachedVertexOutEdges[sourceId]) {
					if (edge.TargetId == targetId) {
						foundEdge = edge;
						return true;
					}
				}
			
			
			//No use searching through in-edge cache of target
			//because if in one, its in the other...
			foundEdge = null;
			return false;

		}





		/// <summary>
		/// Returns an enumeration of the in-edges of the vertex with the specified id.
		/// </summary>
		/// <param name="vertexId">The vertex id.</param>
		/// <returns>
		/// An enumerable collection of GraphEdge instances (zero sized or more)
		/// </returns>
		/// <exception cref="ArgumentNullException">An Exception is raised if the argument is Empty.</exception>
		/// <exception cref="VertexNotFoundException">An Exception is raised if the GraphVertex is not found</exception>
		public IEnumerable<GraphEdge> InEdges(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

			//Get from *all*, or load from db into memory (*all*):
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(vertexId);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			return _CachedVertexInEdges[vertex.Id];
		}


		/// <summary>
		/// Returns the number of in.edges for the vertex with the specified Id.
		/// </summary>
		/// <param name="vertexId">The vertex Id.</param>
		/// <returns>
		/// Returns 0 or more, unless exception raised.
		/// </returns>
		/// <remarks>
		/// </remarks>
		/// <exception cref="ArgumentNullException">An exception is raised if the argument is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the GraphVertex is not found.</exception>
		public int InDegree(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}
			//Get from *all*, or load from db into memory:
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(vertexId);


			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			return _CachedVertexInEdges[vertex.Id].Count;
		}


		/// <summary>
		/// Returns the number of out.edges for the vertex with the specified Id.
		/// </summary>
		/// <param name="vertexId">The vertex Id.</param>
		/// <returns>
		/// Returns 0 or more, unless exception raised.
		/// </returns>
		/// <remarks>
		/// </remarks>
		/// <exception cref="ArgumentNullException">An exception is raised if the argument is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the GraphVertex is not found.</exception>
		public int OutDegree(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}
			//Get from *all*, or load from db into memory (*all*):
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(vertexId);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			return _CachedVertexOutEdges[vertex.Id].Count;
		}

		/// <summary>
		/// Returns an Enumeration of the out-edges of the GraphVertex with the specified Id.
		/// </summary>
		/// <param name="vertexId">The vertex Id.</param>
		/// <returns>
		/// An enumerable collection of GraphEdge instances (zero-size or more, unless exception raised).
		/// </returns>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the GraphVertex is not found.</exception>
		public IEnumerable<GraphEdge> OutEdges(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}
			//Get from *all*, or load from db into memory (*all*):
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(vertexId);

			//Because GetVertex ensures the vertex is in mem,
			//and also got its Out-Edges, we can safely count in-mem
			//without hitting the Db:
			return _CachedVertexOutEdges[vertex.Id];
		}


		/// <summary>
		/// Determines whether the graph contains the specified edge.
		/// <para>
		/// IMPORTANT:
		/// Comparison is done by sourceId and targetId -- not by Edge or Vertex entity. 
		/// </para>
		/// </summary>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <param name="sourceId">The Id of the source endpoint vertex.</param>
		/// <param name="targetId">The Id of the target endpoint vertex.</param>
		/// <returns>
		/// 	<c>true</c> if the edge contains an edge between the two vertices; otherwise, <c>false</c>.
		/// </returns>
		public bool ContainsEdge(Guid sourceId, Guid targetId) {

			//We can simplify the code by looking for an edge, 
			//and if found, we're home:


			GraphEdge foundEdge;
			return (TryGetEdge(sourceId, targetId, out foundEdge));
		}

		/// <summary>
		/// Create a new Edge between the specified Source and Target vertices.
		/// <para>
		/// IMPORTANT: The edge is created, but is not added to the graph until AddEdge(TEdge) is invoked.
		/// </para>
		/// </summary>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <param name="sourceId">The source Vertex Id.</param>
		/// <param name="targetId">The target Vertex Id.</param>
		/// <returns>A new Edge.</returns>
		public GraphEdge CreateEdge(Guid sourceId, Guid targetId) {
			//Check Args:
			if (sourceId == Guid.Empty) {
				throw new System.ArgumentNullException("sourceId");
			}
			if (targetId == Guid.Empty) {
				throw new System.ArgumentNullException("targetId");
			}
			return new GraphEdge(sourceId, targetId);
		}

		/// <summary>
		/// Adds a new Edge between a source Vertex and a target Vertex.
		/// Returns <c>true</c> only if an Edge was created.
		/// Returns <c>false</c> if an Edge already existed.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invokes a cancelable <see cref="E:OnEdgeCreating"/> before creating the Edge,
		/// and then <see cref="E:OnEdgeCreated"/> if successful.
		/// </para>
		/// <para>
		/// No error is raised if an Edge already exists between the two 
		/// Vertex instances:
		/// it just returns <c>false</c>, and <see cref="M:OnEdgeCreated"/>
		/// is never invoked.
		/// </para>
		/// </remarks>
		/// <param name="sourceId">The source Vertex Id.</param>
		/// <param name="targetId">The target Vertex Id.</param>
		/// <returns>
		/// Returns <c>true</c> only if an Edge was created.
		/// Returns <c>false</c> if an Edge already existed.
		/// </returns>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="System.ArgumentNullException">Exception raised if sourceId is null/empty.</exception>
		/// <exception cref="System.ArgumentNullException">Exception raised if targetId is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">Exception raised if sourceVertex is not found.</exception>
		/// <exception cref="VertexNotFoundException">Exception raised if targetVertex is not found.</exception>
		public bool AddEdge(Guid sourceId, Guid targetId) {
			//Create a new edge:
			GraphEdge edge = CreateEdge(sourceId, targetId);

			return AddEdge(edge);

		}


		/// <summary>
		/// Adds a new Edge between a source Vertex and a target Vertex.
		/// Returns <c>true</c> only if a new Edge was created.
		/// Returns <c>false</c> if an Edge between the two endpoints already existed.
		/// <para>
		/// Wrapper to <see cref="AddEdge(Guid,Guid)"/>.
		/// </para>
		/// </summary>
		/// <param name="sourceVertex">The source Vertex.</param>
		/// <param name="targetVertex">The target Vertex.</param>
		/// <returns>
		/// Returns <c>true</c> only if an Edge was created.
		/// Returns <c>false</c> if an Edge already existed.
		/// </returns>
		/// <remarks>
		/// <para>
		/// Note that it first invokes <see cref="ContainsEdge(Guid, Guid)"/> to see if an 
		/// edge exists between the two endpoints: 
		/// the comparison is done by Id's, and not Edge or vertex entities.
		/// </para>
		/// 	<para>
		/// Invokes a cancelable <see cref="E:OnEdgeCreating"/> before creating the Edge,
		/// and then <see cref="E:OnEdgeCreated"/> if successful.
		/// </para>
		/// 	<para>
		/// No error is raised if an Edge already exists between the two
		/// Vertex instances:
		/// it just returns <c>false</c>, and <see cref="M:OnEdgeCreated"/>
		/// is never invoked.
		/// </para>
		/// </remarks>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the sourceVertex is null.</exception>
		/// <exception cref="ArgumentNullException">An exception is raised if the targetVertex is null.</exception>
		public bool AddEdge(GraphVertex sourceVertex, GraphVertex targetVertex) {
			//Check Args:
			if (sourceVertex == null) {
				throw new System.ArgumentNullException("sourceVertex");
			}
			if (targetVertex == null) {
				throw new System.ArgumentNullException("targetVertex");
			}
			return AddEdge(sourceVertex.Id, targetVertex.Id);

		}


		/// <summary>
		/// Updates the Edge between the source Vertex Id and target Vertex Id.
		/// </summary>
		/// <param name="sourceId">The source Vertex Id.</param>
		/// <param name="targetId">The target Vertex Id.</param>
		/// <returns>
		/// Return true if Edge is updated.
		/// </returns>
		/// <remarks>Invokes <see cref="M:OnEdgeUpdated"/> if successful.</remarks>
		/// <exception cref="VertexNotFoundException">An Exception is raised if the source or target Vertex cannot be found</exception>
		public bool UpdateEdge(Guid sourceId, Guid targetId) {
			//Check Args:
			if (sourceId == Guid.Empty) {
				throw new System.ArgumentNullException("sourceId");
			}
			if (targetId == Guid.Empty) {
				throw new System.ArgumentNullException("targetId");
			}

			lock (this) {
				//Ensure these vertices exist:
				//Get from *all*, or load from db into memory (*all*):
				//Raise an error if no vertex found.
				GraphVertex sourceVertex = GetVertex(sourceId);
				GraphVertex targetVertex = GetVertex(targetId);

				//Create a new edge:
				GraphEdge edge = CreateEdge(sourceVertex, targetVertex);

				if (edge == null) {
					return false;
				}

				//Raise a cancelable event prior to deleting the edge:
				CancelEdgeEventArgs arg = new CancelEdgeEventArgs(edge);
				OnEdgeUpdating(arg);

				if (arg.Cancel) {
					// edge deletion canceled, return false
					return false;
				}
				// Change state so that thread later creates in local and remote storage:
				((ICachedElement)edge).SyncStatus |= OfflineModelState.UpdatePending;

				if (UpdateInRealTime) {
					Flush(edge);
				}

				// raise event after creating it:
				OnEdgeUpdated(new EdgeEventArgs(edge));

				return true;
			}//~lock
		}



		/// <summary>
		/// Removes the Edge between the source Vertex Id and target Vertex Id.
		/// if the Edge doesn't exists, do nothing.
		/// <para>
		/// Raises events before and after operation.
		/// </para>
		/// <para>
		/// Returns <c>true</c> if an Edge is removed.
		/// </para>
		/// <para>
		/// Throws an error if no edge was found.
		/// </para>
		/// </summary>
		/// <remarks>
		/// 	<para>
		/// No error is raised if the source Vertex,
		/// or target Vertex, or an Edge between
		/// the two, is not found to exist.
		/// </para>
		/// 	<para>
		/// Invokes a cancelable <see cref="M:GraphProviderBase.OnEdgeDeleting"/> before deleting the Vertex,
		/// and then <see cref="M:OnEdgeDeleted"/> if successful.
		/// </para>
		/// 
		/// </remarks>
		/// 	<param name="sourceId">The source Vertex Id.</param>
		/// 	<param name="targetId">The target Vertex Id.</param>
		/// 	<returns>
		/// Returns <c>true</c> only if an Edge is removed,
		/// Returns <c>false</c> if no Edge already existed.
		/// </returns>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="VertexNotFoundException">An Exception is raised if the source or target Vertex cannot be found</exception>
		public bool RemoveEdge(Guid sourceId, Guid targetId) {
			//Check Args:
			if (sourceId == Guid.Empty) {
				throw new System.ArgumentNullException("sourceId");
			}
			if (targetId == Guid.Empty) {
				throw new System.ArgumentNullException("targetId");
			}
			int check1 = _CachedVertexInEdges.Count;
			int check2 = _CachedVertexOutEdges.Count;

			//Ensures the vertices are in mem,
			//and therefore, their OutEdges:
			GraphVertex source = GetVertex(sourceId);
			GraphVertex target	 = GetVertex(targetId);

			//Throws an error if the edge cannot be found.
			GraphEdge edge  = GetEdge(source, target);
			
			//And remove from 
			return RemoveEdge(edge);

		}




		/*
							edge = _CachedVertexOutEdges[sourceId].Find(
				delegate(GraphEdge e) {
					return e.TargetId == targetId;
				}
		*/


		#endregion




		#region Public Methods - Implementation of GraphProviderBase - Flushing

		/// <summary>
		/// Flush everything which is not sync with local storage
		/// </summary>
		/// <remarks>
		/// <para>
		/// Periodically called by the Flush Thread to persist
		/// any changes to the underlying local datastore.
		/// </para>
		/// </remarks>
		public void Flush() {

			lock (this) {

				//#warning "this is where we need to optimise SQL statement"

				// then persist changes to deleted in-memory vertices:
				Flush(_CachedDeletePendingVertices.Items);

				// then persist changes to removed edges:
				Flush(_CachedDeletePendingEdges);

				// then persist changes to active in-memory vertices:
				Flush(_CachedAllVertices.Items);

				// then persist changes to edges:
				foreach (System.Collections.Generic.KeyValuePair<Guid,GraphEdgeList> pair in _CachedVertexInEdges) {
					Flush(pair.Value);
				}

				foreach (System.Collections.Generic.KeyValuePair<Guid, GraphEdgeList> pair in _CachedVertexOutEdges) {
					Flush(pair.Value);
				}

			}
		}



		/// <summary>
		/// Flush the vertices if not in sync with local storage.
		/// </summary>
		/// <param name="vertexList">Enumerable list of GraphVertex elements.</param>
		/// <exception cref="System.ArgumentNullException">Exception raised if argument is null.</exception>
		protected void Flush(IEnumerable<GraphVertex> vertexList) {
			//Check Args:
			if (vertexList == null) {
				throw new System.ArgumentNullException("vertexList");
			}

			List<Guid> verticesToMarkForDeletion = new List<Guid>();
			List<Guid> verticesToDelete = new List<Guid>();
			GraphVertexList verticesManipulated = new GraphVertexList();
			GraphVertexList verticesToCreate = new GraphVertexList();
			GraphVertexList verticesToUpdate = new GraphVertexList();

			foreach (GraphVertex vertex in vertexList) {
				if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.LocalPending) == 0) {
					//Vertex has already been persisted to local storage
					//so there is nothing to do:
					continue;
				}
				//------------------------------------
				lock (this) {

					if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.LocalDeletionPending) != 0) {
						//The vertex was marked for local deleting...
						//IMPORTANT:
						//This supercedes any LocalCreatePending or LocalUpdatePending flags that may 
						//have been set as well.
						if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.RemoteDeletionPending) != 0) {
							//...and is marked for remote deletion as well, 
							//so we can't delete it yet (or we would lose this information upon local shutdown).
							//ie: this is a serious deletion (not just a local cleanup)...
							//So mark the GraphVertex *record* as needing to be deleted from from remote db.
							//(does not delete from local storage yet):
							verticesToMarkForDeletion.Add(vertex.Id);
							//This means that the vertex record .Deleted flag is 1, and therefore filters
							//it out from GetVertex requests etc...
						}
						else {
							//This is a local delete only (ie a local release of storage resources).
							//And does not have to worry about remote deletion being completed...
							//So just delete locally and we're done:
							verticesToDelete.Add(vertex.Id);
						}

					}
					else if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.LocalCreationPending) != 0) {
						//This vertex was created in memory, but 
						//was not persisted to local storage yet...
						//So create the new record:
						verticesToCreate.Add(vertex);

						//IMPORTANT:
						//Note that Create persists the same information 
						//as Update, so there is no reason to worry about the Update afterwards...
					}
					else {
						//This vertex has been persisted to local storage at some point
						//in the past, (ie was not marked as LocalCreationPending),
						//but changes have been made since then...
						//Update the record in local storage:
						Debug.Assert((((ICachedElement)vertex).SyncStatus & OfflineModelState.LocalUpdatePending) != 0);
						verticesToUpdate.Add(vertex);
					}
					verticesManipulated.Add(vertex);
				}//~lock
			}//~foreach
			//------------------------------------
			storageVertexRecordMarkForDeletion(verticesToMarkForDeletion);
			storageVertexRecordDelete(verticesToDelete);
			storageVertexRecordCreate(verticesToCreate);
			storageVertexRecordUpdate(verticesToUpdate);
			//------------------------------------
			foreach (GraphVertex vertex in verticesManipulated) {
				//IMPORTANT:
				//Note that 'storageVertexRecordCreate' and 'storageVertexRecordUpdate'
				//only *sent* flags that had RemotePending still up, but
				//both methods had not modified the actual values of the Vertex.
				//This was so that if they failed, one had nothing to rollback here.
				//On the other hand, one must not forget to update the flags
				//in the GraphVertex to match what was written to the Db....
				//So....
				//Wrap up by updating the in-memory GraphVertex SyncStatus, leaving only 
				//the RemotePending flag up if already up (which it will be):
				((ICachedElement)vertex).SyncStatus &= OfflineModelState.RemotePending;
			}
			//------------------------------------
		}



		/// <summary>
		/// Flush/Persist the in-memory Edges to the local storage.
		/// </summary>
		/// <param name="edgeList">Enumerable list of GraphEdge elements.</param>
		/// <exception cref="System.ArgumentNullException">Exception raised if argument is null.</exception>
		protected void Flush(IEnumerable<GraphEdge> edgeList) {
			//Check Args:
			if (edgeList == null) {
				throw new System.ArgumentNullException("edgeList");
			}

			GraphEdgeList edgesManipulated = new GraphEdgeList();
			GraphEdgeList edgesToMarkForDeletion = new GraphEdgeList();
			GraphEdgeList edgesToDelete = new GraphEdgeList();
			GraphEdgeList edgesToCreate = new GraphEdgeList();
			GraphEdgeList edgesToUpdate = new GraphEdgeList();


			//throw new Exception("The method or operation is not implemented.");
			foreach (GraphEdge edge in edgeList) {
				if ((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalPending) == 0) {
					//No changes...move on to next one...
					continue;
				}

			
				lock (this) {

					if ((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalDeletionPending) != 0) {
						//The edge was marked for local deleting...
						if ((((ICachedElement)edge).SyncStatus & OfflineModelState.RemoteDeletionPending) != 0) {
							//...and is marked for remote deletion as well, 
							//then this is a serious deletion (not just a local cleanup)...
							//So mark the record as needing to be deleted from from remote db.
							//(does not delete from local storage yet):
							edgesToMarkForDeletion.Add(edge);
						}
						else {
							//This is a local delete only (ie a local release of storage resources).
							//And does not have to worry about remote deletion being completed...
							//So just delete locally and we're done:
							edgesToDelete.Add(edge);
						}

					}
					else if ((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalCreationPending) != 0) {
						//This vertex was created in memory, but 
						//was not persisted to local storage yet...
						//So create the new record:
						edgesToCreate.Add(edge);

					}
					else {
						//This vertex has been persisted to local storage at some point
						//in the past, but changes have been made...
						//Update the record in local storage:
						Debug.Assert((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalUpdatePending) != 0);
						edgesToUpdate.Add(edge);
					}
					edgesManipulated.Add(edge);
				}//~lock
			}//~foreach
			//------------------------------------
			storageEdgeRecordMarkForDeletion(edgesToMarkForDeletion);
			storageEdgeRecordDelete(edgesToDelete);
			storageEdgeRecordCreate(edgesToCreate);
			storageEdgeRecordUpdate(edgesToUpdate);
			//------------------------------------
			foreach (GraphEdge edge in edgesManipulated) {
				//IMPORTANT:
				//Note that 'storageEdgeRecordCreate' and 'storageEdgeRecordUpdate'
				//only *sent* flags that had RemotePending still up, but
				//both methods had not modified the actual values of the Edge.
				//This was so that if they failed, one had nothing to rollback here.
				//On the other hand, one must not forget to update the flags
				//in the GraphEdge to match what was written to the Db....
				//So....
				//Wrap up by updating the in-memory GraphEdge SyncStatus, leaving only 
				//the RemotePending flag up if already up (which it will be):
				((ICachedElement)edge).SyncStatus &= OfflineModelState.RemotePending;
			}
			//------------------------------------
		}


		/// <summary>
		/// Flush/Persist the in-memory Vertex to the local storage.
		/// </summary>
		/// <internal>
		/// Optimize this as being a passthru to Flush(vertexList), rather than
		/// the other way around.
		/// </internal>
		/// <param name="vertex">The vertex to flush.</param>
		/// <exception cref="System.ArgumentNullException">An Exception is raised if the vertex is null.</exception>
		protected void Flush(GraphVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			traceInformation("Flush GraphVertex [Id:{0}, Status:{1}].", vertex.Id, ((ICachedElement)vertex).SyncStatus);

			if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.LocalPending) == 0) {
				//Vertex has already been persisted to local storage
				//so there is nothing to do:
				return;
			}

			lock (this) {

				if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.LocalDeletionPending) != 0) {
					//The vertex was marked for local deleting...
					//IMPORTANT:
					//This supercedes any LocalCreatePending or LocalUpdatePending flags that may 
					//have been set as well.
					if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.RemoteDeletionPending) != 0) {
						//...and is marked for remote deletion as well, 
						//so we can't delete it yet (or we would lose this information upon local shutdown).
						//ie: this is a serious deletion (not just a local cleanup)...
						//So mark the GraphVertex *record* as needing to be deleted from from remote db.
						//(does not delete from local storage yet):
						List<Guid> tmpVertexIds = new List<Guid>();
						tmpVertexIds.Add(vertex.Id);
						storageVertexRecordMarkForDeletion(tmpVertexIds);
						//This means that the vertex record .Deleted flag is 1, and therefore filters
						//it out from GetVertex requests etc...
					}
					else {
						//This is a local delete only (ie a local release of storage resources).
						//And does not have to worry about remote deletion being completed...
						//So just delete locally and we're done:
						List<Guid> tmpVertexIds = new List<Guid>();
						tmpVertexIds.Add(vertex.Id);
						storageVertexRecordDelete(tmpVertexIds);
					}

				}
				else if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.LocalCreationPending) != 0) {
					//This vertex was created in memory, but 
					//was not persisted to local storage yet...
					//So create the new record:
					GraphVertexList tmpVertices = new GraphVertexList();
					tmpVertices.Add(vertex);
					storageVertexRecordCreate(tmpVertices);
					//IMPORTANT:
					//Note that Create persists the same information 
					//as Update, so there is no reason to worry about the Update afterwards...
				}
				else {
					//This vertex has been persisted to local storage at some point
					//in the past, (ie was not marked as LocalCreationPending),
					//but changes have been made since then...
					//Update the record in local storage:
					Debug.Assert((((ICachedElement)vertex).SyncStatus & OfflineModelState.LocalUpdatePending) != 0);

					GraphVertexList tmpVertices = new GraphVertexList();
					tmpVertices.Add(vertex);
					storageVertexRecordUpdate(tmpVertices);
				}

				//IMPORTANT:
				//Note that 'storageVertexRecordCreate' and 'storageVertexRecordUpdate'
				//only *sent* flags that had RemotePending still up, but
				//both methods had not modified the actual values of the Vertex.
				//This was so that if they failed, one had nothing to rollback here.
				//On the other hand, one must not forget to update the flags
				//in the GraphVertex to match what was written to the Db....
				//So....
				//Wrap up by updating the in-memory GraphVertex SyncStatus, leaving only 
				//the RemotePending flag up if already up (which it will be):
				((ICachedElement)vertex).SyncStatus &= OfflineModelState.RemotePending;
			}//~lock
		}





		/// <summary>
		/// Flush/Persist the in-memory Edges to the local storage.
		/// </summary>
		/// <internal>
		/// Optimize this as being a passthru to Flush(vertexList), rather than
		/// the other way around.
		/// </internal>
		/// <param name="edge">The GraphEdge to flush.</param>
		/// <exception cref="System.ArgumentNullException">An Exception is raised if the edge is null.</exception>
		protected void Flush(GraphEdge edge) {
			//Check Args:
			if (edge == null) {
				throw new System.ArgumentNullException("edge");
			}


			if ((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalPending) == 0) {
				return;
			}

			/*
			if ((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalUpdatePending) == 0) {
				//Edge has already been persisted to local storage
				//so there is nothing to do:
				return;
			}
			 */

			lock (this) {

				if ((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalDeletionPending) != 0) {
					//The edge was marked for local deleting...
					if ((((ICachedElement)edge).SyncStatus & OfflineModelState.RemoteDeletionPending) != 0) {
						//...and is marked for remote deletion as well, 
						//then this is a serious deletion (not just a local cleanup)...
						//So mark the record as needing to be deleted from from remote db.
						//(does not delete from local storage yet):
						storageEdgeRecordMarkForDeletion(new GraphEdgeList(edge));
					}
					else {
						//This is a local delete only (ie a local release of storage resources).
						//And does not have to worry about remote deletion being completed...
						//So just delete locally and we're done:
						storageEdgeRecordDelete(new GraphEdgeList(edge));
					}

				}
				else if ((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalCreationPending) != 0) {
					//This vertex was created in memory, but 
					//was not persisted to local storage yet...
					//So create the new record:
					storageEdgeRecordCreate(new GraphEdgeList(edge));

				}
				else {
					//This vertex has been persisted to local storage at some point
					//in the past, but changes have been made...
					//Update the record in local storage:
					Debug.Assert((((ICachedElement)edge).SyncStatus & OfflineModelState.LocalUpdatePending) != 0);
					storageEdgeRecordUpdate(new GraphEdgeList(edge));
				}

				//IMPORTANT:
				//Note that 'storageEdgeRecordCreate' and 'storageEdgeRecordUpdate'
				//only *sent* flags that had RemotePending still up, but
				//both methods had not modified the actual values of the Edge.
				//This was so that if they failed, one had nothing to rollback here.
				//On the other hand, one must not forget to update the flags
				//in the GraphEdge to match what was written to the Db....
				//So....
				//Wrap up by updating the in-memory GraphEdge SyncStatus, leaving only 
				//the RemotePending flag up if already up (which it will be):
				((ICachedElement)edge).SyncStatus &= OfflineModelState.RemotePending;
			}//~lock

		}


		#endregion


		#region Public Methods - Sync
		/// <summary>
		/// Get Ids of all new (DateCreated>LastSyncDate) Vertices 
		/// pointing to records within a collection of DataSources, 
		/// that are not marked as already transmitted to Client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that this method does not load vertices into in-memory cache,
		/// (although the use of <c>GetVertices</c> could be used subsequently
		/// to do this).
		/// </para>
		/// </remarks>
		/// <param name="dataSourceIds">Iterable Collection of DataSource </param>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		Guid[] IGraphSyncServer.GetNewVertexIds(ICollection<int> dataSourceIds, Guid clientMachineId, DateTime lastSyncDate) {
			return storageForSyncGetNewVertexIds(dataSourceIds, clientMachineId, lastSyncDate);
		}

		/// <summary>
		/// Get Ids of all new Vertices (DateCreated>LastSyncDate) 
		/// pointing to specified records in a specified datasource,
		/// that are not marked as already transmitted to client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that this method does not load vertices into in-memory cache.
		/// </para>
		/// </remarks>
		/// <param name="dataObjectFactoryId">A single dataSource id.</param>
		/// <param name="dataObjectIds">An iterable Collection of dataObject record </param>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		Guid[] IGraphSyncServer.GetNewVertexIds(int dataObjectFactoryId, ICollection<string> dataObjectIds, Guid clientMachineId, DateTime lastSyncDate) {
			return storageForSyncGetNewVertexIds(dataObjectFactoryId, dataObjectIds, clientMachineId, lastSyncDate);
		}

		/// <summary>
		/// Get Ids of all updated (DateModified>LastSyncDate) Vertices 
		/// pointing to records within a collection of DataSources, 
		/// that are not marked as already transmitted to Client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that this method does not load vertices into in-memory cache
		/// (although the use of <c>GetVertices</c> could be used subsequently
		/// to do this).
		/// </para>
		/// </remarks>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		Guid[] IGraphSyncServer.GetUpdatedVertexIds(Guid clientMachineId, DateTime lastSyncDate) {
			return storageForSyncGetUpdatedVertexIds(clientMachineId, lastSyncDate);
		}

		/// <summary>
		/// Get a collection of GraphVertex Ids that have been deleted
		/// on Server, that are still present on Client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The ID's returned are to Vertices that no longer exist 
		/// in this graph.
		/// </para>
		/// </remarks>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		Guid[] IGraphSyncServer.GetDeletedVertexIds(Guid clientMachineId) {
			return storageForSyncGetDeletedVertexIds(clientMachineId);

		}




		//      this.storageForSyncMarkTransmissionBegun;
		//      this.storageForSyncMarkTransmissionAborted;
		//      this.storageForSyncMarkTransmissionSuccessful;


		/// <summary>
		/// Marks a vertex as having been already on a Client.
		/// Intended to be used by the GaphSyncManager when 
		/// a GraphVertex that has already been created on the Client is now being duplicated 
		/// on the Server (which ends up being the same thing as created on the server
		/// and successfully sent to Client).
		/// </summary>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="vertexId">The vertex of the id.</param>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
		void IGraphSyncServer.SaveVertexIdInVerticesSent(Guid clientMachineId, Guid vertexId) {
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

			storageForSyncMarkVertexIdAsSent(clientMachineId, vertexId);
		}


		void IGraphSyncServer.RemoveVertexIdInVerticesSent(Guid clientMachineId, Guid vertexId) {
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

			storageForSyncRemoveVertexIdFromVerticesSent(clientMachineId, vertexId);
		}

		#endregion

		#region Protected - Event Handlers - Remote Synchronization

		/// <summary>
		/// method called when the creation of a vertex has been accepted remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexCreateAccepted(object sender, GraphVertexEventArgs e) {
			//Get from *all*, or load from db into memory (*all*):
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(e.Vertex.Id);

			if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.RemoteCreationPending) == 0) {
				traceError("creation remotely accepted: already sync");
			}
			//Unset status of remote creation needed:
			((ICachedElement)vertex).SyncStatus &= ~OfflineModelState.RemoteCreationPending;
			//Set: state it for local update (flush):
			((ICachedElement)vertex).SyncStatus |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// Vertexes the creation remotely rejected.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexCreateRejected(object sender, GraphVertexEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		/// <summary>
		/// method called when the update of a vertex has been accepted remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexUpdateAccepted(object sender, GraphVertexEventArgs e) {
			//Get from *all*, or load from db into memory (*all*):
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(e.Vertex.Id);

			if ((((ICachedElement)vertex).SyncStatus & OfflineModelState.RemoteUpdatePending) == 0) {
				traceError("vertex update remotely accepted: already sync");
			}
			//Unset -- no more remote update required:
			((ICachedElement)vertex).SyncStatus &= ~OfflineModelState.RemoteUpdatePending;
			//Set: status for local flush later:
			//OLIVIER:
			//TODO: Wait! Is this not going to reset local vertex date as later than remote vertex edit date?
			((ICachedElement)vertex).SyncStatus |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// method called when the update of a vertex has been rejected remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexUpdateRejected(object sender, GraphVertexEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		/// <summary>
		/// method called when the deletion of a vertex has been accepted remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexDeleteAccepted(object sender, GraphVertexEventArgs e) {

			Guid vertexId = e.Vertex.Id;

			lock (this) {

				_CachedDeletePendingVertices.Remove(vertexId);

				GraphEdgeList newCollection = new GraphEdgeList();
				newCollection.AddRange(Collections.Filter(_CachedDeletePendingEdges,
					delegate(GraphEdge edge) {
						return edge.SourceId != vertexId && edge.TargetId != vertexId;
					}));
				_CachedDeletePendingEdges = newCollection;

				List<Guid> tmpVertexIdList = new List<Guid>();
				tmpVertexIdList.Add(vertexId);
				storageVertexRecordDelete(tmpVertexIdList);
			}
		}

		/// <summary>
		/// method called when the deletion of a vertex has been rejected remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexDeleteRejected(object sender, GraphVertexEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}





		/// <summary>
		/// Event handler for when an GraphEdge has been remotely created.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		public void OnRemoteEdgeCreateAccepted(object sender, EdgeEventArgs e) {

			//Find the edge that has the source/target mentioned in the args:
			GraphEdge edge = _CachedVertexInEdges[e.Edge.TargetId].Find(
				delegate(GraphEdge edge2) {
					return edge2.SourceId == e.Edge.SourceId;
				}
			);


			Debug.Assert(edge != null);
			//UnSet:
			((ICachedElement)edge).SyncStatus &= ~OfflineModelState.RemoteCreationPending;
			//Set: status for local flush later:
			((ICachedElement)edge).SyncStatus |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// Method called when the creation of an edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		public void OnRemoteEdgeCreateRejected(object sender, EdgeEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		/// <summary>
		/// Event handler for when an GraphEdge has been remotely updated.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		public void OnRemoteEdgeUpdateAccepted(object sender, EdgeEventArgs e) {
			//Find the edge that has the source/target mentioned in the args:
			GraphEdge edge = _CachedDeletePendingEdges.Find(delegate(GraphEdge e2) {
				return e2.SourceId == e.Edge.SourceId && e2.TargetId == e.Edge.TargetId;
			});
			//UnSet:
			((ICachedElement)edge).SyncStatus &= ~OfflineModelState.RemoteUpdatePending;
			//Set: status for local flush later:
			((ICachedElement)edge).SyncStatus |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// Method called when the update of an edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		public void OnRemoteEdgeUpdateRejected(object sender, EdgeEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		/// <summary>
		/// Event handler for when an GraphEdge has been remotely deleted.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		public void OnRemoteEdgeDeleteAccepted(object sender, EdgeEventArgs e) {
			//Find the edge that has the source/target mentioned in the args:
			GraphEdge edge = _CachedDeletePendingEdges.Find(delegate(GraphEdge e2) {
				return e2.SourceId == e.Edge.SourceId && e2.TargetId == e.Edge.TargetId;
			});
			//Unset:
			((ICachedElement)edge).SyncStatus &= ~OfflineModelState.RemoteDeletionPending;
			//Set: status for local flush later:
			((ICachedElement)edge).SyncStatus |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// Method called when the deletion of an edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		public void OnRemoteEdgeDeleteRejected(object sender, EdgeEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion



	}//Class:End
}//Namespace:End
