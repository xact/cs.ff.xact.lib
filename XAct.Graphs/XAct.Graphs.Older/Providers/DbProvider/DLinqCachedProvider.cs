using System;
using System.Collections.Specialized;
using System.Configuration;
//PC Only using System.Timers;


namespace XAct.Graphs {


	/// <summary>
	/// A Generic Db implementation of <see cref="GraphDbCachedProviderBase"/>.
	/// In other words, a Graph Provider that caches Vertices in memory, until secondary
	/// threads persist changes to a SQL-99 compliant DBMS storage system.
	/// </summary>
	/// <remarks>
	/// <para>
	/// <b>Configuration:</b><br/>
	/// <code>
	/// <![CDATA[
	///   <configSections>
	///     <sectionGroup name="Graphs">
	///       <sectionGroup name="XAct">
	///         <section name="Graph" type="XAct.Graphs.GraphSection, GraphEngine"/>
	///       </sectionGroup>
	///     </sectionGroup>
	///   </configSections>
	///   ...
	///   <Graph defaultProvider="Default">
	///      <providers>
	///        <add name="GraphDbCachedProvider" type="XAct.Graphs.GraphDbCachedProvider, XAct.Graphs.GraphManager"
	///             connectionName="connectionName"
	///             dbSettings="default"
	///             cleanupInterval="10"
	///             cleanupMinVertices="0"
	///             cleanupTimeOut="20"
	///             flushInterval="60"/>
	///      </providers>
	///
	///      <dbSettings>
	///        <add name="default"
	///             dataSourceTable="DataSource"
	///             vertexTable="Vertex"
	///             edgeTable="Edge"/>
	///      </dbSettings>
	///    </Graph>
	/// ]]>
	/// </code>
	/// </para>
	/// </remarks>
	abstract public class GraphDLinqCachedProviderBase : GraphProviderBase {


		#region Constants - Configuration Section related

		/// <summary>
		/// Attribute name for connection setting to local database.
		/// </summary>
		public static readonly string C_ATTRIBUTETAG_CONNECTIONSETTINGSNAME = "connectionName";

		/// <summary>
		/// Attribute name for database settings (table name, columns name etc.)
		/// </summary>
		public static readonly string C_ATTRIBUTETAG_DBSETTINGSNAME = "dbSettings";
		#endregion

		#region Properties
		/// <summary>
		/// Gets the connection settings for the local DB where graph structure is stored.
		/// </summary>
		/// <value>The connection settings.</value>
		virtual public ConnectionStringSettings ConnectionSettings {
			get {
				return _ConnectionSettings;
			}
		}
		/// <summary>
		/// TODO: TODOC:
		/// </summary>
		private ConnectionStringSettings _ConnectionSettings;

		/// <summary>
		/// Gets the dbSettings used to get the actual table name and column name
		/// for (edge, vertex and datasource tables)
		/// </summary>
		public DbSettings DbSettings {
			get {
				return _DbSettings;
			}
		}
		private DbSettings _DbSettings;

		#endregion

		#region Public Methods - Implementation of ProviderBase

		/// <summary>
		/// Initializes the Graph Engine.
		/// called by Manager/Provider mechanic.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the
		/// provider-specific attributes specified in the configuration for this provider.</param>
		public override void Initialize(string name, NameValueCollection config) {

			traceInformation("GraphDbCachedProvider Initialization");
			base.Initialize(name, config);

			//Get Connection Info:
			string connectionStringSettingsName = ProviderHelper.InitializeParam<string>(config, C_ATTRIBUTETAG_CONNECTIONSETTINGSNAME, null, true);
			_ConnectionSettings = ProviderHelper.ConfigureConnectionSettings(connectionStringSettingsName);
			if (_ConnectionSettings == null) {
				throw new Exception(String.Format("missing connection setting : '{0}'", connectionStringSettingsName));
			}


			Initialization_CreateTables();


			//WARNING *don't instantiate DataObjectFactoryCollection here*
			// because the graph engine is not fully constructed.
			// do lazy data source initialisation instead

#if ZERO

      _DataSourceCollection = new DataObjectFactoryCollection(this);
#endif

		}

		#endregion



	}


	/*
	public class GraphSqlServerDLinqCachedProviderBase : GraphDLinqCachedProviderBase {




		#region Protected Methods - Implementation of GraphProviderBase / Storage / TVertex Related

		/// <summary>
		/// create a new vertex in the local storage.
		/// </summary>
		/// <param name="vertices">The vertices to create new records for.</param>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		override protected void storageVertexRecordCreate(ICollection<TVertex> vertices) {
			//Check Args:
			if (vertices == null) {
				throw new System.ArgumentNullException("vertex");
			}
			if (vertices.Count == 0) {
				return;
			}

			using (GraphContext dc = new GraphContext()) {

			}

			using (IDbConnection connection = CreateConnection()) {
				using (IDbTransaction transaction = connection.BeginTransaction()) {
					foreach (TVertex vertex in vertices) {
						//SQL Limitation: We can only create one record at a time...
						using (IDbCommand command = CreateCommand_VertexCreateRecord(connection, vertex)) {
							command.ExecuteNonQuery();
						}//~command
						//Now that we have saved the vertex, save its attributes:
						storageVertexAttributesWrite(connection, vertex);
					}
					transaction.Commit();
				}//~transaction.
			}//~connection
		}


		/// <summary>
		///  update the vertex in the local storage.
		/// </summary>
		/// <param name="vertices">The vertices to create new records for.</param>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		override protected void storageVertexRecordUpdate(ICollection<TVertex> vertices) {
			//Check Args:
			if (vertices == null) {
				throw new System.ArgumentNullException("vertices");
			}
			if (vertices.Count == 0) {
				return;
			}

			using (IDbConnection connection = CreateConnection()) {
				using (IDbTransaction transaction = connection.BeginTransaction()) {
					foreach (TVertex vertex in vertices) {
						//SQL Limitation: We can only update one record at a time...
						using (IDbCommand command = CreateCommand_VertexUpdateRecord(connection, vertex)) {
							command.ExecuteNonQuery();
						}//~command
						//Now that we have saved the vertex, save its attributes:
						storageVertexAttributesWrite(connection, vertex);
					}
					transaction.Commit();
				}//~transaction.
			}//~connection
		}


		/// <summary>
		///  mark the given vertex as deleted in the local storage
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that this only marks the local TVertex record
		/// for Remote deletion. It does not remove the TVertex record.
		/// </para>
		/// </remarks>
		/// <param name="vertexIds">The ids of vertices.</param>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is Empty.</exception>
		override protected void storageVertexRecordMarkForDeletion(ICollection<Guid> vertexIds) {
			//Check Args:
			if (vertexIds == null) {
				throw new System.ArgumentNullException("vertexIds");
			}
			if (vertexIds.Count == 0) {
				return;
			}


			//traceInformation("local storage mark deleted vertex (Id={0})", vertexId);

			using (IDbConnection connection = CreateConnection()) {
				using (IDbTransaction transaction = connection.BeginTransaction()) {
					//First, Mark from remote deletion all in-edges and out-edges to this vertex:
					using (IDbCommand command = CreateCommand_VertexRecordMarkForDeletionEdges(connection, vertexIds)) {
						command.ExecuteNonQuery();
					}
					//Finally, Mark this TVertex for Remote deletion.
					using (IDbCommand command = CreateCommand_VertexRecordMarkForDeletion(connection, vertexIds)) {
						command.ExecuteNonQuery();
					}
					transaction.Commit();
				}
			}
		}


		/// <summary>
		/// Suppress in the local storage the records for the vertex
		/// and its adjacent edges
		/// </summary>
		/// <param name="vertexIds">The Ids of the vertices.</param>
		/// <exception cref="ArgumentNullException">An exception is raised if the vertexId argument is Empty.</exception>
		override protected void storageVertexRecordDelete(ICollection<Guid> vertexIds) {
			//Check Args:
			if (vertexIds == null) {
				throw new System.ArgumentNullException("vertexIds");
			}
			if (vertexIds.Count == 0) {
				return;
			}

			using (IDbConnection connection = CreateConnection()) {
				using (IDbTransaction transaction = connection.BeginTransaction()) {

					//First, Delete all in-edge and out-edge (adjacent edge) records:
					using (IDbCommand command = CreateCommand_VertexDeleteAdjacentEdges(connection, vertexIds)) {
						command.ExecuteNonQuery();
					}

					//then delete all global and user attributes for that vertex:
					using (IDbCommand command = CreateCommand_VertexDeleteAllAttributesByVertexId(connection, vertexIds)) {
						command.ExecuteNonQuery();
					}//~command

					//then delete the vertex record itself
					using (IDbCommand command = CreateCommand_VertexDeleteByVertexId(connection, vertexIds)) {
						command.ExecuteNonQuery();
					}//~command

					transaction.Commit();
				}//~transaction
			}//~connection
		}


		/// <summary>
		/// Gets the vertex from the local storage.
		/// Returns null if not found.
		/// </summary>
		/// <remarks>
		/// <para>
		/// No error if TVertex record not found -- returns null.
		/// </para>
		/// <para>
		/// Invoked by <see cref="M:GetVertex"/> when the
		/// TVertex cannot be found already in memory.
		/// </para>
		/// </remarks>
		/// <param name="vertexId">The vertex's unique Id.</param>
		/// <returns>
		/// Return the vertex with the specified Guid,
		/// or null if the TVertex doesn't exist.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is Empty.</exception>
		override protected TVertex storageVertexGet(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

			GraphDbVertexRecord dbVertex;

			TVertex vertex;
			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_VertexGetByVertexId(connection, vertexId)) {
					using (IDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow)) {
						if (reader.Read() == false) {
							// vertex not found
							return null;
						}
						dbVertex = new GraphDbVertexRecord(reader);
					}
				}//Close vertex connection


				vertex = dbVertex.CreateVertex();

				//Get GLOBAL Attributes via second command:
				storageGlobalAttributesLoad(connection, vertex);
			}//Connection:Close


			//We got the Vertex, now we get the associated data:



			//OLIVIER: Maybe the DataObject should be Lazy-Loaded...Need fast vertex traversal, no?



			return inMemAddVertex(vertex);

		}


		/// <summary>
		/// Get a collection of Vertices from storage that match the vertex ids given.
		/// </summary>
		/// <param name="vertexIds">Ids to look for.</param>
		/// <param name="resultCollection">Result Collection of TVertex elements found.</param>
		/// <param name="vertexIdsNotFound">Result List of vertex Ids not found.</param>
		/// <returns><c>true</c>if all vertices are found.</returns>
		/// <remarks>
		/// Called by the public
		/// <see cref="GraphProviderBase.GetVertices(ICollection{Guid}, ref GraphVertexList, out ICollection{Guid})"/> method,
		/// for all vertices not found
		/// by <see cref="GraphProviderBase.inMemGetVertices(ICollection{Guid}, ref GraphVertexList, out ICollection{Guid})"/>.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the arguments are null.</exception>
		override protected bool storageGetVertices(ICollection<Guid> vertexIds, ref GraphVertexList resultCollection, out ICollection<Guid> vertexIdsNotFound) {
			//Check Args:
			if (vertexIds == null) {
				throw new ArgumentNullException("vertexIds");
			}
			if (resultCollection == null) {
				throw new ArgumentNullException("resultCollection");
			}


			//Build an IN clause:


			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_VerticesGetByVertexId(connection, vertexIds)) {

					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							//Create the tmp holder:
							GraphDbVertexRecord dbVertex = new GraphDbVertexRecord(reader);

							//Create the vertex from the dbVertex:
							TVertex vertex = dbVertex.CreateVertex();
							//Get GLOBAL Attributes via second command:
							storageGlobalAttributesLoad(connection, vertex);

							resultCollection.Add(inMemAddVertex(vertex));

						}//End:Loop
					}
				}//Close:Command
			}//Close:Connection



			//Iterate through the collection of vertices that were
			//found in memory, and extract a list of
			//just their vertex id's:
			List<Guid> foundVertexIds = ExtractIds(resultCollection);


			//Filter the dataObjectIds collection *argument*
			//for all that were not found in memory:
			vertexIdsNotFound =
				new List<Guid>(
				Collections.Filter<Guid>(
							foundVertexIds,
							delegate(Guid id) {
								return !vertexIds.Contains(id);
							}
							));

			return (vertexIdsNotFound.Count == 0);
		}


		/// <summary>
		/// Gets the vertices from the specified data source id and
		/// with the specified objects
		/// </summary>
		/// <remarks>
		/// <para>
		/// Called by the public <see cref="GraphProviderBase.GetVertices(int, ICollection{string}, ref GraphVertexList, out ICollection{string}, bool)"/> method,
		/// for all vertices not found by <see cref="GraphProviderBase.inMemGetVertices(int, ICollection{string}, ref GraphVertexList, out ICollection{string})"/>.
		/// </para>
		/// </remarks>
		/// <param name="dataObjectFactoryId">The data source id.</param>
		/// <param name="dataObjectIds">The dataObject </param>
		/// <param name="resultCollection">Result Collection of TVertex elements found.</param>
		/// <param name="dataObjectIdsNotFound"></param>
		/// <returns>
		/// True if vertices for all data objects ids were found.
		/// </returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the arguments are null.</exception>
		override protected bool storageGetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds, ref GraphVertexList resultCollection, out ICollection<string> dataObjectIdsNotFound) {
			//Check Args:
			if (dataObjectFactoryId == 0) {
				throw new ArgumentNullException("dataObjectFactoryId");
			}
			if (dataObjectIds == null) {
				throw new ArgumentNullException("dataObjectIds");
			}
			if (resultCollection == null) {
				throw new ArgumentNullException("resultCollection");
			}

			using (IDbConnection connection = CreateConnection()) {
				List<GraphDbVertexRecord> dbRecords = new List<GraphDbVertexRecord>();

				using (IDbCommand command = CreateCommand_VerticesGetByRecordIds(connection, dataObjectFactoryId, dataObjectIds)) {
					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							//Create the tmp holder:
							dbRecords.Add(new GraphDbVertexRecord(reader));
						}//End:While
					}//~reader
				}//~command

				//Create the vertex from the two:
				foreach (GraphDbVertexRecord dbVertex in dbRecords) {
					TVertex vertex = dbVertex.CreateVertex();

					//We need to get the attributes for this vertex:

					//Get GLOBAL Attributes via second command:
					storageGlobalAttributesLoad(connection, vertex);

					resultCollection.Add(inMemAddVertex(vertex));
				}//~foreach GraphDbVertexRecord
			}//Close:Connection

			//At this point we have 0 or more vertices...
			//But check for missing recordIds:

			//Iterate through the collection of vertices that were
			//found in memory, and extract a list of
			//just their vertex recordId's:
			List<string> foundRecordIds =
				new List<string>(Collections.Map<TVertex, string>(resultCollection,
				delegate(TVertex v) {
					return v.DataRecordId;
				}));

			//Filter the dataObjectIds collection *argument*
			//for all that were not found in memory:
			dataObjectIdsNotFound =
				new List<string>(
				Collections.Filter<string>(dataObjectIds,
				delegate(string id) {
					return !foundRecordIds.Contains(id);
				}));

			return (dataObjectIdsNotFound.Count == 0);
		}


		#endregion

		#region Protected methods - Implementation of GraphProviderBase / Storage / VertexAtttributes

		/// <summary>
		/// Load attribute values from storage into the Vertex's user attributes array.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <returns><c>true</c>if attributes found.</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		override protected bool storageVertexUserAttributesLoad(TVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//Get *once*:
			object userId = this.UserId;


			bool foundAttributes = false;
			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_UserAttributesLoadByVertexId(connection, vertex.Id, userId)) {
					using (IDataReader reader = command.ExecuteReader()) {
						foundAttributes = true;
						while (reader.Read()) {
							string key = reader.GetString(0);
							string val = reader.GetString(1);
							vertex.UserAttributes[key] = val;
						}
					}
				}
			}
			return foundAttributes;
		}


		/// <summary>
		/// Save the Vertex's user attributes to storage.
		/// </summary>
		/// <param name="vertices">The vertices.</param>
		/// <returns><c>true</c>if update.</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		override protected bool storageVertexUserAttributesSave(ICollection<TVertex> vertices) {
			if (vertices == null) {
				throw new System.ArgumentNullException("vertices");
			}
			//Get *once*:
			object userId = this.UserId;



			using (IDbConnection connection = CreateConnection()) {
				using (IDbTransaction transaction = connection.BeginTransaction()) {
					List<Guid> vertexIds = ExtractIds(vertices);
					using (IDbCommand command = CreateCommand_UserAttributesDeleteByVertexId(connection, userId, vertexIds)) {
						command.ExecuteNonQuery();
					}
					foreach (TVertex vertex in vertices) {
						GraphVertexAttributeDictionary attributes = vertex.UserAttributes;
						if (!attributes.IsDirty) {
							//No save required.
							return false;
						}
						foreach (System.Collections.Generic.KeyValuePair<string, string> attribute in attributes) {
							using (IDbCommand command = CreateCommand_UserAttributeSaveSingleByVertexId(connection, vertex.Id, userId, attribute.Key, attribute.Value)) {
								command.ExecuteNonQuery();
							}//~command
						}//~foreach
					}
					transaction.Commit();
				}//~transaction
			}//~connection

			return true;
		}




		#endregion

		#region Protected Methods - Implementation of GraphProviderBase / Storage / VertexIds
		/// <summary>
		/// Get a collection of TVertex Ids that are neighbours
		/// to the specified TVertex
		/// </summary>
		/// <param name="vertexIds">The vertex </param>
		/// <param name="clientMachineId">The client machine id.</param>
		/// <param name="depth">The depth.</param>
		/// <returns>
		/// A collection of TVertex Ids (never null, but can be of size 0).
		/// </returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		override protected List<Guid> storageVertexIdsGetInOutVertexIds(ICollection<Guid> vertexIds, Guid clientMachineId, int depth) {
			if (vertexIds == null) {
				throw new System.ArgumentNullException("vertexIds");
			}
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}

			List<Guid> result = new List<Guid>(vertexIds);
			if (vertexIds.Count == 0) {
				return result;
			}

			result.AddRange(storageVertexIdsGetInVertexIds(vertexIds, depth));
			result.AddRange(storageVertexIdsGetOutVertexIds(vertexIds, depth));

			return result;
		}


		/// <summary>
		/// Get a collection of TVertex Ids that are anscestors
		/// to the specified TVertex
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="storageVertexIdsGetInOutVertexIds"/>.
		/// </para>
		/// </remarks>
		/// <param name="vertexIds">The vertex </param>
		/// <param name="depth">The depth.</param>
		/// <returns>
		/// A collection of TVertex Ids (never null, but can be of size 0).
		/// </returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
		override protected List<Guid> storageVertexIdsGetInVertexIds(ICollection<Guid> vertexIds, int depth) {
			//Check Args:
			if (vertexIds == null) {
				throw new System.ArgumentNullException("vertexIds");
			}

			List<Guid> result = new List<Guid>();
			//If nothing to do, get out early:
			if (vertexIds.Count == 0) {
				return result;
			}

			//Clone the list because we will be clearing it:
			List<Guid> startIds = new List<Guid>(vertexIds);

			using (IDbConnection connection = CreateConnection()) {
				for (int i = 0; i < depth; i++) {
					//Get parents:
					using (IDbCommand command = CreateCommand_VertexGetInVertexIds(connection, vertexIds)) {
						startIds = new List<Guid>();
						using (IDataReader reader = command.ExecuteReader()) {
							while (reader.Read()) {
								GraphDbEdgeRecord dbEdgeRecord = new GraphDbEdgeRecord(reader, 0);
								if (!result.Contains(dbEdgeRecord.TargetId)) {
									//this will be a new child for next iteration:
									startIds.Add(dbEdgeRecord.TargetId);
									//And will be part of the final result:
									result.Add(dbEdgeRecord.TargetId);
								}//~if
							}//~while
						}//~reader
					}//~command
				}//~for depth
			}//~connection
			return result;
		}


		/// <summary>
		/// Get a collection of TVertex Ids that are descendents
		/// to the specified TVertex
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="storageVertexIdsGetInOutVertexIds"/>.
		/// </para>
		/// </remarks>
		/// <param name="vertexIds">The vertex </param>
		/// <param name="depth">The depth.</param>
		/// <returns>
		/// A collection of TVertex Ids (never null, but can be of size 0).
		/// </returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
		override protected List<Guid> storageVertexIdsGetOutVertexIds(ICollection<Guid> vertexIds, int depth) {
			//Check Args:
			if (vertexIds == null) {
				throw new System.ArgumentNullException("vertexIds");
			}

			List<Guid> result = new List<Guid>();
			//If nothing to do, get out early:
			if (vertexIds.Count == 0) {
				return result;
			}

			//Clone the list because we will be clearing it:
			List<Guid> startIds = new List<Guid>(vertexIds);

			using (IDbConnection connection = CreateConnection()) {
				for (int i = 0; i < depth; i++) {
					//Get parents:
					using (IDbCommand command = CreateCommand_VertexGetOutVertexIds(connection, vertexIds)) {
						startIds = new List<Guid>();
						using (IDataReader reader = command.ExecuteReader()) {
							while (reader.Read()) {
								GraphDbEdgeRecord dbEdgeRecord = new GraphDbEdgeRecord(reader, 0);
								if (!result.Contains(dbEdgeRecord.TargetId)) {
									//this will be a new child for next iteration:
									startIds.Add(dbEdgeRecord.TargetId);
									//And will be part of the final result:
									result.Add(dbEdgeRecord.TargetId);
								}//~if
							}//~while
						}//~reader
					}//~command
				}//~for depth
			}//~connection
			return result;
		}


		#endregion

		#region Protected Methods - Implementation of GraphProviderBase / Storage / TEdge Related

		/// <summary>
		/// Get from the local storage the out edge for the specified vertex.
		/// Return an GraphEdgeList (never null).
		/// </summary>
		/// <param name="vertices">A collection of Vertices.</param>
		/// <returns>A Collection of Edges (never null).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		override protected GraphEdgeList storageVertexGetInEdges(ICollection<TVertex> vertices) {
			//Check Args:
			if (vertices == null) {
				throw new System.ArgumentNullException("vertices");
			}
			//storageVertexGetInEdges
			GraphEdgeList res = new GraphEdgeList();

			List<Guid> vertexIds = ExtractIds(vertices);

			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_VertexGetInEdges(connection, vertexIds)) {
					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							TEdge edge = new TEdge(reader);
							res.Add(edge);
						}
					}
				}
			}
			return res;
		}




		/// <summary>
		/// Get from the local storage the out edge for the specified vertex.
		/// Returns an GraphEdgeList (never null).
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <returns>GraphEdgeList (never null).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		override protected GraphEdgeList storageVertexGetOutEdges(TVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			GraphEdgeList res = new GraphEdgeList();

			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_VertexGetOutEdges(connection, vertex.Id)) {
					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							TEdge edge = new TEdge(reader);
							res.Add(edge);
						}
					}
				}
			}

			return res;
		}


		/// <summary>
		/// Create the Edge's record in the local storage.
		/// Transacted, so if one fails, rolls back all.
		/// </summary>
		/// <param name="edges">The edges.</param>
		/// <remarks>Transacted, so if operation fails, rolls back.</remarks>
		/// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
		override protected void storageEdgeRecordCreate(ICollection<TEdge> edges) {
			//Check Args:
			if (edges == null) {
				throw new System.ArgumentNullException("edges");
			}
			using (IDbConnection connection = CreateConnection()) {
				using (IDbTransaction transaction = connection.BeginTransaction()) {
					foreach (TEdge edge in-edges) {
						using (IDbCommand command = CreateCommand_EdgeRecordCreate(connection, edge)) {
							try {
								command.ExecuteNonQuery();
							}
							catch {

							}
						}
					}
					transaction.Commit();
				}
			}
		}

		/// <summary>
		/// Update the Edge's record in local storage.
		/// Transacted, so if one fails, rolls back all.
		/// </summary>
		/// <param name="edges">The edges.</param>
		/// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
		override protected void storageEdgeRecordUpdate(ICollection<TEdge> edges) {
			//Check Args:
			if (edges == null) {
				throw new System.ArgumentNullException("edges");
			}
			using (IDbConnection connection = CreateConnection()) {
				using (IDbTransaction transaction = connection.BeginTransaction()) {
					foreach (TEdge edge in-edges) {
						using (IDbCommand command = CreateCommand_EdgeRecordUpdate(connection, edge)) {
							command.ExecuteNonQuery();
						}
					}
					transaction.Commit();
				}
			}
		}

		/// <summary>
		/// Update the Edge's record in local storage to indicate that the TEdge is deleted.
		/// IMPORTANT: that this is not a Delete, but a setting of the record's Deleted flag to true.
		/// </summary>
		/// <param name="edges">The edges.</param>
		/// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
		override protected void storageEdgeRecordMarkForDeletion(ICollection<TEdge> edges) {
			//Check Args:
			if (edges == null) {
				throw new System.ArgumentNullException("edges");
			}
			if (edges.Count == 0) {
				//Get out early, so that
				//CreateCommand_EdgeRecordMarkForDeletion
				//is not called (makes a slight mess dealing with WHERE...
				return;
			}

			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_EdgeRecordMarkForDeletion(connection, edges)) {
					command.ExecuteNonQuery();
				}
			}
		}


		/// <summary>
		/// Delete the Edge's record from the local storage.
		/// </summary>
		/// <param name="edges">The Collection of Edges.</param>
		/// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
		override protected void storageEdgeRecordDelete(ICollection<TEdge> edges) {
			//Check Args:
			if (edges == null) {
				throw new System.ArgumentNullException("edges");
			}
			if (edges.Count == 0) {
				//Get out early, so that
				//CreateCommand_EdgeRecordMarkForDeletion
				//is not called (makes a slight mess dealing with WHERE...
				return;
			}
			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_EdgeRecordDelete(connection, edges)) {
					command.ExecuteNonQuery();
				}
			}
		}
		#endregion

		#region Protected Methods - Implementation of GraphProviderBase / Sync
		/// <summary>
		/// Get Ids of all new (DateCreated&gt;LastSyncDate) Vertices
		/// pointing to records within a collection of DataSources,
		/// that are not marked as already transmitted to Client.
		/// </summary>
		/// <param name="dataSourceIds">Iterable Collection of DataSource </param>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>
		/// A collection of TVertex Ids (never null, but can be of size 0).
		/// </returns>
		/// <remarks>
		/// Note that this method does not load vertices into in-memory cache.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		override protected Guid[] storageForSyncGetNewVertexIds(ICollection<int> dataSourceIds, Guid clientMachineId, DateTime lastSyncDate) {
			if (dataSourceIds == null) {
				throw new System.ArgumentNullException("dataSourceIds");
			}
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}
			List<Guid> result = new List<Guid>();

			if (dataSourceIds.Count == 0) {
				return result.ToArray();
			}

			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command =
					CreateCommand_ForSyncGetNewVertexIdsByDataSourceIds(connection, dataSourceIds, clientMachineId, lastSyncDate)) {
					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							result.Add(reader.GetGuid(0));
						}
					}
				}//~command
			}//~connection
			return result.ToArray();
		}


		/// <summary>
		/// Get Ids of all new Vertices (DateCreated&gt;LastSyncDate)
		/// pointing to specified records in a specified datasource,
		/// that are not marked as already transmitted to client.
		/// </summary>
		/// <param name="dataObjectFactoryId">A single dataSource id.</param>
		/// <param name="dataObjectIds">An iterable Collection of dataObject record </param>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>
		/// A collection of TVertex Ids (never null, but can be of size 0).
		/// </returns>
		/// <remarks>
		/// Note that this method does not load vertices into in-memory cache.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		override protected Guid[] storageForSyncGetNewVertexIds(int dataObjectFactoryId, ICollection<string> dataObjectIds, Guid clientMachineId, DateTime lastSyncDate) {
			if (dataObjectFactoryId == 0) {
				throw new System.ArgumentException("dataObjectFactoryId");
			}
			if (dataObjectIds == null) {
				throw new System.ArgumentNullException("dataObjectIds");
			}
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}
			List<Guid> result = new List<Guid>();

			if (dataObjectIds.Count == 0) {
				return result.ToArray();
			}
			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command =
					CreateCommand_ForSyncGetNewVertexIdsByDataSourceIdAndDataObjectIds(connection, dataObjectFactoryId, dataObjectIds, clientMachineId, lastSyncDate)) {
					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							//Create a vertex, add it to memory if not already added,
							//and add the result to the result collection:
							result.Add(reader.GetGuid(0));
						}
					}
				}//~command
			}//~connection
			return result.ToArray();
		}



		/// <summary>
		/// Get Ids of all updated (DateModified&gt;LastSyncDate) Vertices
		/// pointing to records within a collection of DataSources,
		/// that are not marked as already transmitted to Client.
		/// </summary>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>
		/// A collection of TVertex Ids (never null, but can be of size 0).
		/// </returns>
		/// <remarks>
		/// Note that this method does not load vertices into in-memory cache.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		override protected Guid[] storageForSyncGetUpdatedVertexIds(Guid clientMachineId, DateTime lastSyncDate) {
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}

			List<Guid> result = new List<Guid>();

			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_ForSyncGetUpdatedVertexIds(connection, clientMachineId, lastSyncDate)) {
					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							result.Add(reader.GetGuid(0));
						}
					}
				}//~command
			}//~connection
			return result.ToArray();
		}



		/// <summary>
		/// Get a collection of TVertex Ids that have been deleted
		/// on Server, that are still present on Client.
		/// </summary>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <returns>
		/// A collection of TVertex Ids (never null, but can be of size 0).
		/// </returns>
		/// <remarks>
		/// Cannot return a collection of Vertices, as they no longer
		/// exist in the Database. We only know what the Ids were
		/// due to traces within the <c>VerticesSent</c> table.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		override protected Guid[] storageForSyncGetDeletedVertexIds(Guid clientMachineId) {
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}

			List<Guid> result = new List<Guid>();

			using (IDbConnection connection = CreateConnection()) {
				//Create the <c>View</c>:
				using (IDbCommand command = CreateCommand_ForSyncGetDeletedVertexIdsI(connection, clientMachineId)) {
					command.ExecuteNonQuery();
				}
				//Outer Left Join, using the <c>View</c>:
				using (IDbCommand command = CreateCommand_ForSyncGetDeletedVertexIdsII(connection, clientMachineId)) {
					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							result.Add(reader.GetGuid(0));
						}
					}
				}//~command
			}//~connection
			return result.ToArray();
		}



		/// <summary>
		/// Mark that the given TVertex Ids are being transmitted to the client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// If successful, call <see cref="storageForSyncMarkTransmissionSuccessful"/>,
		/// if not, invoke <see cref="storageForSyncMarkTransmissionAborted"/> to clean up.
		/// </para>
		/// </remarks>
		/// <param name="clientMachineId">The client machine id.</param>
		/// <param name="vertexIds">The vertex </param>
		override protected void storageForSyncMarkTransmissionBegun(Guid clientMachineId, ICollection<Guid> vertexIds) {
			using (IDbConnection connection = CreateConnection()) {
				//Create the <c>View</c>:
				using (IDbCommand command = CreateCommand_ForSyncMarkTransmissionBegun(connection, clientMachineId, vertexIds)) {
					command.ExecuteNonQuery();
				}
			}
		}

		/// <summary>
		/// Deletes references to Vertices that were transmitted to a client
		/// but didn't make it (eg: communication was interrupted).
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked to cleanup a transmission that was organized
		/// with the help of <see cref="storageForSyncMarkTransmissionBegun"/>.
		/// </para>
		/// </remarks>
		/// <param name="clientMachineId">The client machine id.</param>
		override protected void storageForSyncMarkTransmissionAborted(Guid clientMachineId) {
			using (IDbConnection connection = CreateConnection()) {
				//Create the <c>View</c>:
				using (IDbCommand command = CreateCommand_ForSyncMarkTransmissionAborted(connection, clientMachineId)) {
					command.ExecuteNonQuery();
				}
			}
		}

		/// <summary>
		/// Marks all vertices that are being transmitted, as being received by client.
		/// </summary>
		/// <param name="clientMachineId">The client machine id.</param>
		override protected void storageForSyncMarkTransmissionSuccessful(Guid clientMachineId) {
			using (IDbConnection connection = CreateConnection()) {
				//Create the <c>View</c>:
				using (IDbCommand command = CreateCommand_ForSyncMarkTransmissionSuccessful(connection, clientMachineId)) {
					command.ExecuteNonQuery();
				}
			}
		}


		/// <summary>
		/// Marks the TVertex as having been successfully transmitted to the client.
		/// </summary>
		/// <param name="clientMachineId">The client machine id.</param>
		/// <param name="vertexId">The id of the vertex.</param>
		/// <remarks>
		/// Used by the GraphSyncManager upon receiving a request to create a new TVertex.
		/// In other words, it is creating a Server TVertex of a TVertex that for sure already
		/// exists on the client, which is in all senses the same as a local server vertex
		/// having been transmitted to the remote client...
		/// </remarks>
		override protected void storageForSyncMarkVertexIdAsSent(Guid clientMachineId, Guid vertexId) {
			//Check Args:
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}
			using (IDbConnection connection = CreateConnection()) {
				//Create the <c>View</c>:
				using (IDbCommand command = CreateCommand_ForSyncMarkVertexIdAsSent(connection, clientMachineId, vertexId)) {
					command.ExecuteNonQuery();
				}
			}
		}


		/// <summary>
		/// Removes the reference to the TVertex from the VerticesSent table.
		/// </summary>
		/// <param name="clientMachineId">The client machine id.</param>
		/// <param name="vertexId">The id of the vertex.</param>
		/// <remarks>
		/// Used by the GraphSyncManager upon receiving a request to delete an existing Vertex,
		/// undoing the work done by <see cref="storageForSyncMarkVertexIdAsSent"/>.
		/// </remarks>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is null.</exception>
		override protected void storageForSyncRemoveVertexIdFromVerticesSent(Guid clientMachineId, Guid vertexId) {
			//Check Args:
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}
			using (IDbConnection connection = CreateConnection()) {
				//Create the <c>View</c>:
				using (IDbCommand command = CreateCommand_ForSyncRemoveVertexIdFromVerticesSent(connection, clientMachineId, vertexId)) {
					command.ExecuteNonQuery();
				}
			}
		}
		#endregion

		#region Protected Methods - Implementation of GraphDbCachedProviderBase - Initialize Tables

		/// <summary>
		/// Create the underlying Tables used by this provider.
		/// </summary>
		override protected void Initialization_CreateTables() {

			System.Reflection.Assembly assembly = this.GetType().Assembly;//System.Reflection.Assembly.GetEntryAssembly();

			//string dirPath = AppDir + "\\data\\scripts\\";

			XAct.Data.DbSchemaInstaller installer =
				 new DbSchemaInstaller(
				 assembly,
				 this.DbSettings.InstallScriptsPrefix,
				 null,
				 this.ConnectionSettings.Name,
				 this.DbSettings.SqlKeyWords,
				 this.DbSettings.ParamPrefix);

			installer.Initialize();
		}//Method:End



		#endregion




	}//Class:End
	*/
}//Namespace:End


