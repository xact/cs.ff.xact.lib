All *.sql script files in here are marked as Embedded Resources
(ie, right-click, Properties, Build=Embed)

The naming format for the resource files are
{TAG}.{VERSION}.sql

The Tag is a name for all scripts to do with setting up this provider.
In this case its 'GraphDbCachedProvider.Schema.Sqlite'.

/* 
BEGIN TRANSACTION;
CREATE TEMPORARY TABLE t1_backup(a,b);
INSERT INTO t1_backup SELECT a,b FROM t1;
DROP TABLE t1;
CREATE TABLE t1(a,b);
INSERT INTO t1 SELECT a,b FROM t1_backup;
DROP TABLE t1_backup;
COMMIT;
*/
