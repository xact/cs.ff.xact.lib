CREATE TABLE {dataSourceTable} 
( 
{dataSourceId} INTEGER UNIQUE NOT NULL, 
{dataSourceName} NVARCHAR(24) UNIQUE NOT NULL, 
{dataSourceProviderName} NTEXT, 
{dataSourceConnectionString} NTEXT, 
{dataSourceTableName} NVARCHAR(24) NOT NULL, 
{dataSourceKeyColumn} NVARCHAR(24) NOT NULL, 
PRIMARY KEY ({dataSourceId}) 
);


CREATE TABLE {vertexTable}
(
{vertexId} NVARCHAR(36) UNIQUE NOT NULL,
{vertexWeight} INTEGER NOT NULL,
{vertexCreated} DATETIME NOT NULL,
{vertexEdited} DATETIME NOT NULL,
{vertexSync}  INTEGER NOT NULL,
{vertexdataSourceId} INTEGER NOT NULL,
{vertexRecordId} NVARCHAR(64) UNIQUE NOT NULL,
{vertexDeleted} INTEGER NOT NULL,
PRIMARY KEY ({vertexId})
);

CREATE TABLE {edgeTable}
(
{edgeSource} NVARCHAR(36) NOT NULL,
{edgeTarget} NVARCHAR(36) NOT NULL,
{edgeWeight} INTEGER NOT NULL,
{edgeSync} INTEGER NOT NULL,
{edgeDeleted} INTEGER NOT NULL,
PRIMARY KEY ({edgeSource},{edgeTarget})
);


CREATE TABLE {vertexAttributesTable}
(
{vertexAttributesVertexId} NVARCHAR(36) UNIQUE NOT NULL,
{vertexAttributesUserId} NVARCHAR(36) NOT NULL,
{vertexAttributesKey} NVARCHAR(24) NOT NULL,
{vertexAttributesValue} NTEXT,
PRIMARY KEY ({vertexAttributesVertexId})
);

CREATE TABLE {verticesSentTable}
(
{verticesSentMachineId} NVARCHAR(36) NOT NULL,
{verticesSentUserId} NVARCHAR(36) NOT NULL,
{verticesSentVertexId} NVARCHAR(36) NOT NULL,
{verticesSentStatus} INTEGER NOT NULL,
{verticesSentImplied} INTEGER NOT NULL,
PRIMARY KEY ({verticesSentMachineId},{verticesSentVertexId})
);

