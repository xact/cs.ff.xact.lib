﻿CREATE     TABLE {dataSourceTable} 
( 
{dataSourceId} INTEGER UNIQUE NOT NULL, 
{dataSourceName} NVARCHAR(24) UNIQUE NOT NULL, 
{dataSourceProviderName} NVARCHAR(256) NOT NULL DEFAULT '', 
{dataSourceConnectionString} NVARCHAR(1024) NOT NULL DEFAULT '', 
{dataSourceTableName} NVARCHAR(24) NOT NULL, 
{dataSourceKeyColumn} NVARCHAR(24) NOT NULL, 
PRIMARY KEY ({dataSourceId}) 

);


CREATE     TABLE {vertexTable}
(
{vertexId} UNIQUEIDENTIFIER UNIQUE NOT NULL,
{vertexWeight} INTEGER NOT NULL,
{vertexCreated} DATETIME NOT NULL,
{vertexEdited} DATETIME NOT NULL,
{vertexSync}  INTEGER NOT NULL,
{vertexdataSourceId} INTEGER NOT NULL,
{vertexRecordId} NVARCHAR(64) UNIQUE NOT NULL,
{vertexDeleted} BIT NOT NULL,
PRIMARY KEY ({vertexId})
);

CREATE TABLE {edgeTable}
(
{edgeSource} UNIQUEIDENTIFIER NOT NULL,
{edgeTarget} UNIQUEIDENTIFIER NOT NULL,
{edgeWeight} INTEGER NOT NULL,
{edgeSync} INTEGER NOT NULL,
{edgeDeleted} BIT NOT NULL,
PRIMARY KEY ({edgeSource},{edgeTarget}),
UNIQUE ({edgeSource}, {edgeTarget})
);


CREATE TABLE {vertexAttributesTable}
(
{vertexAttributesVertexId} UNIQUEIDENTIFIER UNIQUE NOT NULL,
{vertexAttributesUserId} UNIQUEIDENTIFIER NOT NULL,
{vertexAttributesKey} NVARCHAR(24) NOT NULL,
{vertexAttributesValue} NTEXT,
PRIMARY KEY ({vertexAttributesVertexId})
);

CREATE TABLE {verticesSentTable}
(
{verticesSentMachineId} UNIQUEIDENTIFIER NOT NULL,
{verticesSentUserId} UNIQUEIDENTIFIER NOT NULL,
{verticesSentVertexId} UNIQUEIDENTIFIER NOT NULL,
{verticesSentStatus} INTEGER NOT NULL,
{verticesSentImplied} INTEGER NOT NULL,
PRIMARY KEY ({verticesSentMachineId},{verticesSentVertexId})
);

