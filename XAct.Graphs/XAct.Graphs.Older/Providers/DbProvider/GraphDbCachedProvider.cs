using System;
using System.Data;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Diagnostics;
//PC Only using System.Timers;

using XAct.Core.Utils;
using XAct.Data;

using XAct.Core.Generics;

namespace XAct.Graphs {


  /// <summary>
	/// A Generic Db implementation of <see cref="GraphDbCachedProviderBase"/>.
  /// In other words, a Graph Provider that caches Vertices in memory, until secondary
  /// threads persist changes to a SQL-99 compliant DBMS storage system.
  /// </summary>
  /// <remarks>
  /// <para>
  /// <b>Configuration:</b><br/>
  /// <code>
  /// <![CDATA[
  ///   <configSections>
  ///     <sectionGroup name="Graphs">
  ///       <sectionGroup name="XAct">
  ///         <section name="Graph" type="XAct.Graphs.GraphSection, GraphEngine"/>
  ///       </sectionGroup>
  ///     </sectionGroup>
  ///   </configSections>
  ///   ...
  ///   <Graph defaultProvider="Default">
  ///      <providers>
  ///        <add name="GraphDbCachedProvider" type="XAct.Graphs.GraphDbCachedProvider, XAct.Graphs.GraphManager"
  ///             connectionName="localDB" 
  ///             dbSettings="default"
  ///             cleanupInterval="10" 
  ///             cleanupMinVertices="0" 
  ///             cleanupTimeOut="20"
  ///             flushInterval="60"/>
  ///      </providers>
  ///
  ///      <dbSettings>
  ///        <add name="default"
  ///             dataSourceTable="DataSource"
  ///             vertexTable="Vertex"
  ///             edgeTable="Edge"/>
  ///      </dbSettings>
  ///    </Graph>
  /// ]]>
  /// </code>
  /// </para>
  /// </remarks>
	abstract public class GraphDbCachedProvider : GraphDbCachedProviderBase 
	{




    #region  Constructors

    /// <summary>
    /// GraphDbCachedProvider constructor.
    /// initialisation is done in Initialize method which is called with
    /// MSDN provider specific methods
    /// </summary>
    public GraphDbCachedProvider()
      : base() {
      traceInformation("GraphDbCachedProvider construction");
			System.AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

			System.AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);

		}



    #endregion


		#region Protected Methods - Implementation of GraphProviderBase / Storage / GraphVertex Related

		/// <summary>
    /// create a new vertex in the local storage.
    /// </summary>
    /// <param name="vertices">The vertices to create new records for.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
    override protected void storageVertexRecordCreate(ICollection<GraphVertex> vertices) {
      //Check Args:
      if (vertices == null) {
        throw new System.ArgumentNullException("vertex");
      }
      if (vertices.Count == 0) {
        return;
      }

      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {
          foreach (GraphVertex vertex in vertices) {
            //SQL Limitation: We can only create one record at a time...
            using (IDbCommand command = CreateCommand_VertexCreateRecord(connection, vertex)) {
              command.ExecuteNonQuery();
            }//~command
            //Now that we have saved the vertex, save its attributes:
            storageVertexAttributesWrite(connection, vertex);
          }
          transaction.Commit();
        }//~transaction.
      }//~connection
    }


    /// <summary>
    ///  update the vertex in the local storage.
    /// </summary>
    /// <param name="vertices">The vertices to create new records for.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
    override protected void storageVertexRecordUpdate(ICollection<GraphVertex> vertices) {
      //Check Args:
      if (vertices == null) {
        throw new System.ArgumentNullException("vertices");
      }
      if (vertices.Count == 0) {
        return;
      }

      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {
          foreach (GraphVertex vertex in vertices) {
            //SQL Limitation: We can only update one record at a time...
            using (IDbCommand command = CreateCommand_VertexUpdateRecord(connection, vertex)) {
              command.ExecuteNonQuery();
            }//~command
            //Now that we have saved the vertex, save its attributes:
            storageVertexAttributesWrite(connection, vertex);
          }
          transaction.Commit();
        }//~transaction.
      }//~connection
    }

    
    /// <summary>
    ///  mark the given vertex as deleted in the local storage
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that this only marks the local GraphVertex record
    /// for Remote deletion. It does not remove the GraphVertex record.
    /// </para>
    /// </remarks>
    /// <param name="vertexIds">The ids of vertices.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is Empty.</exception>
    override protected void storageVertexRecordMarkForDeletion(ICollection<Guid> vertexIds) {
      //Check Args:
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }
      if (vertexIds.Count == 0) {
        return;
      }
      //traceInformation("local storage mark deleted vertex (Id={0})", vertexId);

      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {
          //First, Mark from remote deletion all in-edges and out-edges to this vertex:
          using (IDbCommand command = CreateCommand_VertexRecordMarkForDeletionEdges(connection, vertexIds)) {
            command.ExecuteNonQuery();
          }
          //Finally, Mark this GraphVertex for Remote deletion.
          using (IDbCommand command = CreateCommand_VertexRecordMarkForDeletion(connection, vertexIds)) {
            command.ExecuteNonQuery();
          }
          transaction.Commit();
        }
      }
    }


    /// <summary>
    /// Suppress in the local storage the records for the vertex
    /// and its adjacent edges
    /// </summary>
    /// <param name="vertexIds">The Ids of the vertices.</param>
    /// <exception cref="ArgumentNullException">An exception is raised if the vertexId argument is Empty.</exception>
    override protected void storageVertexRecordDelete(ICollection<Guid> vertexIds) {
      //Check Args:
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }
      if (vertexIds.Count == 0) {
        return;
      }

      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {

          //First, Delete all in-edge and out-edge (adjacent edge) records:
          using (IDbCommand command = CreateCommand_VertexDeleteAdjacentEdges(connection, vertexIds)) {
            command.ExecuteNonQuery();
          }

          //then delete all global and user attributes for that vertex:
          using (IDbCommand command = CreateCommand_VertexDeleteAllAttributesByVertexId(connection, vertexIds)) {
            command.ExecuteNonQuery();
          }//~command

          //then delete the vertex record itself
          using (IDbCommand command = CreateCommand_VertexDeleteByVertexId(connection, vertexIds)) {
            command.ExecuteNonQuery();
          }//~command

          transaction.Commit();
        }//~transaction
      }//~connection
    }

    
    /// <summary>
    /// Gets the vertex from the local storage.
    /// Returns null if not found.
    /// </summary>
    /// <remarks>
    /// <para>
    /// No error if GraphVertex record not found -- returns null.
    /// </para>
    /// <para>
    /// Invoked by <see cref="M:GetVertex"/> when the 
    /// GraphVertex cannot be found already in memory.
    /// </para>
    /// </remarks>
    /// <param name="vertexId">The vertex's unique Id.</param>
    /// <returns>
    /// Return the vertex with the specified Guid,
    /// or null if the GraphVertex doesn't exist.
    /// </returns>
		/// 
    /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is Empty.</exception>
    override protected GraphVertex storageVertexGet(Guid vertexId) {
      //Check Args:
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }

      GraphDbVertexRecord dbVertex;

      GraphVertex vertex;
      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_VertexGetByVertexId(connection, vertexId)) {
          using (IDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow)) {
            if (reader.Read() == false) {
              // vertex not found 
              return null;
            }
            dbVertex = new GraphDbVertexRecord(reader);
          }
        }//Close vertex connection


        vertex = (GraphVertex)dbVertex.CreateVertex();

				if (((ISyncVertex)vertex).RetrievalStatus	 == GraphRetrievalStatus.IsProxy) {
					throw new NotSupportedException();
					//throw event...
				}

        //Get GLOBAL Attributes via second command:
        storageGlobalAttributesLoad(connection, vertex);
      }//Connection:Close


      //We got the Vertex, now we get the associated data:

      /* OLIVIER: ARGH!!!!!  The fucking Graph engine
       * only should know about Vertexes, Edges, and a DataSourceID
       * It should NOT have all this crap with DataSources...This 
       * is where we are going all wrong...
       * Raise an event. Let another dll, a DataMapper dll, find
       * the DataObject from the DataSourceId, and fill it, and return
       * it... I'm a 100% sure of this.
      //Use an event to request the event arg from *exterior* 
      //to fill the DataObject
      Events.DataObjectEventArgs args = new Events.DataObjectEventArgs(vertex);
      OnDataObjectFillRequest(args);
       */


      //OLIVIER: Maybe the DataObject should be Lazy-Loaded...Need fast vertex traversal, no?

      /*
      DataObject dataObject = dbVertex.DataSource.GetDataObject(dbVertex.DataObjectId);

      if (dataObject == null) { // data not found, throw
        throw new Exception(string.Format("DataObject for GraphVertex {0} not found.", vertexId));
      }
      */



      return inMemAddVertex(vertex);

    }


    /// <summary>
    /// Get a collection of Vertices from storage that match the vertex ids given.
    /// </summary>
    /// <param name="vertexIds">Ids to look for.</param>
    /// <param name="resultCollection">Result Collection of GraphVertex elements found.</param>
    /// <param name="vertexIdsNotFound">Result List of vertex Ids not found.</param>
    /// <returns><c>true</c>if all vertices are found.</returns>
    /// <remarks>
    /// Called by the public 
    /// <see cref="GraphProviderBase.GetVertices(ICollection{Guid}, ref GraphVertexList, out ICollection{Guid})"/> method, 
    /// for all vertices not found 
    /// by <see cref="GraphProviderBase.inMemGetVertices(ICollection{Guid}, ref GraphVertexList, out ICollection{Guid})"/>.
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the arguments are null.</exception>
    override protected bool storageGetVertices(ICollection<Guid> vertexIds, ref GraphVertexList resultCollection, out ICollection<Guid> vertexIdsNotFound) {
      //Check Args:
      if (vertexIds == null) {
        throw new ArgumentNullException("vertexIds");
      }
      if (resultCollection == null) {
        throw new ArgumentNullException("resultCollection");
      }


      //Build an IN clause:


      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_VerticesGetByVertexId(connection,vertexIds)) {

          using (IDataReader reader = command.ExecuteReader()) {
            while (reader.Read()) {
              //Create the tmp holder:
              GraphDbVertexRecord dbVertex = new GraphDbVertexRecord(reader);

              //Create the vertex from the dbVertex:
              GraphVertex vertex = dbVertex.CreateVertex();
              //Get GLOBAL Attributes via second command:
              storageGlobalAttributesLoad(connection, vertex);

              resultCollection.Add(inMemAddVertex(vertex));

            }//End:Loop
          }
        }//Close:Command
      }//Close:Connection


      
      //Iterate through the collection of vertices that were 
      //found in memory, and extract a list of 
      //just their vertex id's:
      List<Guid> foundVertexIds = ExtractIds(resultCollection);


      //Filter the dataObjectIds collection *argument* 
      //for all that were not found in memory:
      vertexIdsNotFound =
        new List<Guid>(
        XAct.Core.Utils.Collections.Filter<Guid>(
              foundVertexIds,
              delegate(Guid id) {
                return !vertexIds.Contains(id);
              }
              ));
      
      return (vertexIdsNotFound.Count == 0);
    }


    /// <summary>
    /// Gets the vertices from the specified data source id and
    /// with the specified objects 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Called by the public <see cref="GraphProviderBase.GetVertices(int, ICollection{string}, ref GraphVertexList, out ICollection{string}, bool)"/> method,
    /// for all vertices not found by <see cref="GraphProviderBase.inMemGetVertices(int, ICollection{string}, ref GraphVertexList, out ICollection{string})"/>.
    /// </para>
    /// </remarks>
    /// <param name="dataObjectFactoryId">The data source id.</param>
    /// <param name="dataObjectIds">The dataObject </param>
    /// <param name="resultCollection">Result Collection of GraphVertex elements found.</param>
    /// <param name="dataObjectIdsNotFound"></param>
    /// <returns>
    /// True if vertices for all data objects ids were found.
    /// </returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the arguments are null.</exception>
    override protected bool storageGetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds, ref GraphVertexList resultCollection, out ICollection<string> dataObjectIdsNotFound) {
      //Check Args:
      if (dataObjectFactoryId == 0) {
        throw new ArgumentNullException("dataObjectFactoryId");
      }
      if (dataObjectIds == null) {
        throw new ArgumentNullException("dataObjectIds");
      }
      if (resultCollection == null) {
        throw new ArgumentNullException("resultCollection");
      }

      using (IDbConnection connection = CreateConnection()) {
        List<GraphDbVertexRecord> dbRecords = new List<GraphDbVertexRecord>();

        using (IDbCommand command = CreateCommand_VerticesGetByRecordIds(connection, dataObjectFactoryId, dataObjectIds)){
          using (IDataReader reader = command.ExecuteReader()) {
            while (reader.Read()) {
              //Create the tmp holder:
              dbRecords.Add(new GraphDbVertexRecord(reader));
            }//End:While
          }//~reader
        }//~command

        //Create the vertex from the two:
        foreach (GraphDbVertexRecord dbVertex in dbRecords) {
          GraphVertex vertex = dbVertex.CreateVertex();

          //We need to get the attributes for this vertex:

          //Get GLOBAL Attributes via second command:
          storageGlobalAttributesLoad(connection, vertex);

          resultCollection.Add(inMemAddVertex(vertex));
        }//~foreach GraphDbVertexRecord
      }//Close:Connection

      //At this point we have 0 or more vertices...
      //But check for missing recordIds:

      //Iterate through the collection of vertices that were 
      //found in memory, and extract a list of 
      //just their vertex recordId's:
      List<string> foundRecordIds =
        new List<string>(Collections.Map<GraphVertex, string>(resultCollection,
        delegate(GraphVertex v) {
          return v.DataRecordId;
        }));

      //Filter the dataObjectIds collection *argument* 
      //for all that were not found in memory:
      dataObjectIdsNotFound =
        new List<string>(
        Collections.Filter<string>(dataObjectIds,
        delegate(string id) {
          return !foundRecordIds.Contains(id);
        }));

      return (dataObjectIdsNotFound.Count == 0);
    }


    #endregion

    #region Protected methods - Implementation of GraphProviderBase / Storage / VertexAtttributes

    /// <summary>
    /// Load attribute values from storage into the Vertex's user attributes array.
    /// </summary>
    /// <param name="vertex">The vertex.</param>
    /// <returns><c>true</c>if attributes found.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
    override protected bool storageVertexUserAttributesLoad(GraphVertex vertex) {
      //Check Args:
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }

      //Get *once*:
      object userId = this.UserId;


      bool foundAttributes = false;
      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_UserAttributesLoadByVertexId(connection,vertex.Id,userId)) {
          using (IDataReader reader = command.ExecuteReader()) {
            foundAttributes = true;
            while (reader.Read()) {
              string key = reader.GetString(0);
              string val = reader.GetString(1);
              vertex.UserAttributes[key] = val;
            }
          }
        }
      }
      return foundAttributes;
    }


    /// <summary>
    /// Save the Vertex's user attributes to storage.
    /// </summary>
    /// <param name="vertices">The vertices.</param>
    /// <returns><c>true</c>if update.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
    override protected bool storageVertexUserAttributesSave(ICollection<GraphVertex> vertices) {
			if (vertices == null) {
        throw new System.ArgumentNullException("vertices");
      }
      //Get *once*:
      object userId = this.UserId;



      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {
          List<Guid> vertexIds = ExtractIds(vertices);
          using (IDbCommand command = CreateCommand_UserAttributesDeleteByVertexId(connection, userId,vertexIds)) {
            command.ExecuteNonQuery();
          }
          foreach (GraphVertex vertex in vertices) {
            GraphVertexAttributeDictionary attributes = vertex.UserAttributes;
            if (!attributes.IsDirty) {
              //No save required.
              return false;
            }
            foreach (System.Collections.Generic.KeyValuePair<string, string> attribute in attributes) {
              using (IDbCommand command = CreateCommand_UserAttributeSaveSingleByVertexId(connection, vertex.Id, userId, attribute.Key, attribute.Value)) {
                command.ExecuteNonQuery();
              }//~command
            }//~foreach
          }
          transaction.Commit();
        }//~transaction
      }//~connection

      return true;
    }




		/*
		override protected void storageVertexGlobalAttributesLoad(GraphVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = connection.CreateCommand()) {
					command.CommandText = sqlForLoadGlobalAttributesByVertexId;
		 * 
		 *           _sqlForLoadGlobalAttributesByVertexId  = 
						FormatSql(
						"SELECT {vertexAttributesKey}, {vertexAttributesValue}" +
						" FROM {vertexAttributesTable}" +
						" WHERE" +
						" {vertexAttributesVertexId}=@vertexId" + 
						" AND" + 
						" {vertexAttributesUserId} IS NULL"
						);

					DbHelpers.AttachParam(command, "vertexId", vertex.Id);

					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							string key = reader.GetString(0);
							string val = reader.GetString(1);
							vertex.UserAttributes[key] = val;
						}
					}
				}
			}
		}
		*/



		#endregion

		#region Protected Methods - Implementation of GraphProviderBase / Storage / VertexIds
		/// <summary>
    /// Get a collection of GraphVertex Ids that are neighbours
    /// to the specified GraphVertex 
    /// </summary>
    /// <param name="vertexIds">The vertex </param>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <param name="depth">The depth.</param>
    /// <returns>
    /// A collection of GraphVertex Ids (never null, but can be of size 0).
    /// </returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    override protected List<Guid> storageVertexIdsGetInOutVertexIds(ICollection<Guid> vertexIds, Guid clientMachineId, int depth) {
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      List<Guid> result = new List<Guid>(vertexIds);
      if (vertexIds.Count == 0) {
        return result;
      }

      result.AddRange(storageVertexIdsGetInVertexIds(vertexIds, depth));
      result.AddRange(storageVertexIdsGetOutVertexIds(vertexIds, depth));

      return result;
    }


    /// <summary>
    /// Get a collection of GraphVertex Ids that are anscestors
    /// to the specified GraphVertex 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexIdsGetInOutVertexIds"/>.
    /// </para>
    /// </remarks>
    /// <param name="vertexIds">The vertex </param>
    /// <param name="depth">The depth.</param>
    /// <returns>
    /// A collection of GraphVertex Ids (never null, but can be of size 0).
    /// </returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    override protected List<Guid> storageVertexIdsGetInVertexIds(ICollection<Guid> vertexIds, int depth) {
      //Check Args:
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      List<Guid> result = new List<Guid>();
      //If nothing to do, get out early:
      if (vertexIds.Count == 0) {
        return result;
      }

      //Clone the list because we will be clearing it:
      List<Guid> startIds = new List<Guid>(vertexIds);

      using (IDbConnection connection = CreateConnection()) {
        for (int i = 0; i < depth; i++) {
          //Get parents:
          using (IDbCommand command = CreateCommand_VertexGetInVertexIds(connection, vertexIds)) {
            startIds = new List<Guid>();
            using (IDataReader reader = command.ExecuteReader()) {
              while (reader.Read()) {
                GraphDbEdgeRecord dbEdgeRecord = new GraphDbEdgeRecord(reader, 0);
                if (!result.Contains(dbEdgeRecord.TargetId)) {
                  //this will be a new child for next iteration:
                  startIds.Add(dbEdgeRecord.TargetId);
                  //And will be part of the final result:
                  result.Add(dbEdgeRecord.TargetId);
                }//~if
              }//~while
            }//~reader
          }//~command
        }//~for depth
      }//~connection
      return result;
    }


    /// <summary>
    /// Get a collection of GraphVertex Ids that are descendents
    /// to the specified GraphVertex 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexIdsGetInOutVertexIds"/>.
    /// </para>
    /// </remarks>
    /// <param name="vertexIds">The vertex </param>
    /// <param name="depth">The depth.</param>
    /// <returns>
    /// A collection of GraphVertex Ids (never null, but can be of size 0).
    /// </returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    override protected List<Guid> storageVertexIdsGetOutVertexIds(ICollection<Guid> vertexIds, int depth) {
      //Check Args:
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      List<Guid> result = new List<Guid>();
      //If nothing to do, get out early:
      if (vertexIds.Count == 0) {
        return result;
      }

      //Clone the list because we will be clearing it:
      List<Guid> startIds = new List<Guid>(vertexIds);

      using (IDbConnection connection = CreateConnection()) {
        for (int i = 0; i < depth; i++) {
          //Get parents:
          using (IDbCommand command = CreateCommand_VertexGetOutVertexIds(connection, vertexIds)) {
            startIds = new List<Guid>();
            using (IDataReader reader = command.ExecuteReader()) {
              while (reader.Read()) {
                GraphDbEdgeRecord dbEdgeRecord = new GraphDbEdgeRecord(reader, 0);
                if (!result.Contains(dbEdgeRecord.TargetId)) {
                  //this will be a new child for next iteration:
                  startIds.Add(dbEdgeRecord.TargetId);
                  //And will be part of the final result:
                  result.Add(dbEdgeRecord.TargetId);
                }//~if
              }//~while
            }//~reader
          }//~command
        }//~for depth
      }//~connection
      return result;
    }


    #endregion

    #region Protected Methods - Implementation of GraphProviderBase / Storage / GraphEdge Related
		/// <summary>
		/// Get the count of all edges registered in the database, that are not deleted.
		/// </summary>
		/// <returns>A The count of all non-deleted edges.</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		protected override int storageEdgeCount() {
			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_EdgeCount(connection)) {
					return (int)command.ExecuteScalar();
				}
			}
		}

		/// <summary>
		/// Enumerates through all the edge records in the dbms.
		/// </summary>
		/// <returns></returns>
		protected override IEnumerable<GraphEdge> storageEdgeEnumerateAll() {
			using (IDbConnection connection = CreateConnection()) {
				using (IDbCommand command = CreateCommand_GetAllEdges(connection)) {
					using (IDataReader reader = command.ExecuteReader()) {
						while (reader.Read()) {
							yield return new GraphEdge(reader);
						}
					}
				}
			}
		}

    /// <summary>
    /// Get from the local storage the out edge for the specified vertex.
    /// Return an GraphEdgeList (never null).
    /// </summary>
    /// <param name="vertices">A collection of Vertices.</param>
    /// <returns>A Collection of Edges (never null).</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		override protected GraphEdgeList storageVertexGetInEdges(ICollection<GraphVertex> vertices) {
      //Check Args:
      if (vertices == null) {
        throw new System.ArgumentNullException("vertices");
      }
      //storageVertexGetInEdges
      GraphEdgeList res = new GraphEdgeList();

      List<Guid> vertexIds = ExtractIds(vertices);

      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_VertexGetInEdges(connection, vertexIds)){
          using (IDataReader reader = command.ExecuteReader()) {
            while (reader.Read()) {
              GraphEdge edge = new GraphEdge(reader);
              res.Add(edge);
            }
          }
        }
      }
      return res;
    }




    /// <summary>
    /// Get from the local storage the out edge for the specified vertex.
    /// Returns an GraphEdgeList (never null).
    /// </summary>
    /// <param name="vertex">The vertex.</param>
    /// <returns>GraphEdgeList (never null).</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
		override protected GraphEdgeList storageVertexGetOutEdges(GraphVertex vertex) {
      //Check Args:
			if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }

      GraphEdgeList res = new GraphEdgeList();

      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_VertexGetOutEdges(connection,vertex.Id)){
          using (IDataReader reader = command.ExecuteReader()) {
            while (reader.Read()) {
              GraphEdge edge = new GraphEdge(reader);
              res.Add(edge);
            }
          }
        }
      }

      return res;
    }


    /// <summary>
    /// Create the Edge's record in the local storage.
    /// Transacted, so if one fails, rolls back all.
    /// </summary>
    /// <param name="edges">The edges.</param>
    /// <remarks>Transacted, so if operation fails, rolls back.</remarks>
    /// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
    override protected void storageEdgeRecordCreate(ICollection<GraphEdge> edges) {
      //Check Args:
      if (edges == null) {
        throw new System.ArgumentNullException("edges");
      }
      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {
          foreach (GraphEdge edge in edges) {
            using (IDbCommand command = CreateCommand_EdgeRecordCreate(connection, edge)) {
              try {
                command.ExecuteNonQuery();
              }
              catch {
                
              }
            }
          }
          transaction.Commit();
        }
      }
    }

    /// <summary>
    /// Update the Edge's record in local storage.
    /// Transacted, so if one fails, rolls back all.
    /// </summary>
    /// <param name="edges">The edges.</param>
    /// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
    override protected void storageEdgeRecordUpdate(ICollection<GraphEdge> edges) {
      //Check Args:
      if (edges == null) {
        throw new System.ArgumentNullException("edges");
      }
      using (IDbConnection connection = CreateConnection()) {
        using (IDbTransaction transaction = connection.BeginTransaction()) {
          foreach (GraphEdge edge in edges) {
            using (IDbCommand command = CreateCommand_EdgeRecordUpdate(connection, edge)) {
              command.ExecuteNonQuery();
            }
          }
          transaction.Commit();
        }
      }
    }

    /// <summary>
    /// Update the Edge's record in local storage to indicate that the GraphEdge is deleted.
    /// IMPORTANT: that this is not a Delete, but a setting of the record's Deleted flag to true.
    /// </summary>
    /// <param name="edges">The edges.</param>
    /// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
    override protected void storageEdgeRecordMarkForDeletion(ICollection<GraphEdge> edges) {
      //Check Args:
      if (edges == null) {
        throw new System.ArgumentNullException("edges");
      }
      if (edges.Count == 0) {
        //Get out early, so that 
        //CreateCommand_EdgeRecordMarkForDeletion 
        //is not called (makes a slight mess dealing with WHERE...
        return;
      }

      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_EdgeRecordMarkForDeletion(connection,edges)) {
          command.ExecuteNonQuery();
        }
      }
    }


    /// <summary>
    /// Delete the Edge's record from the local storage.
    /// </summary>
    /// <param name="edges">The Collection of Edges.</param>
    /// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
    override protected void storageEdgeRecordDelete(ICollection<GraphEdge> edges) {
      //Check Args:
      if (edges == null) {
        throw new System.ArgumentNullException("edges");
      }
      if (edges.Count == 0) {
        //Get out early, so that 
        //CreateCommand_EdgeRecordMarkForDeletion 
        //is not called (makes a slight mess dealing with WHERE...
        return;
      }
      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_EdgeRecordDelete(connection,edges)) {
          command.ExecuteNonQuery();
        }
      }
    }
    #endregion

    #region Protected Methods - Implementation of GraphProviderBase / Sync
    /// <summary>
    /// Get Ids of all new (DateCreated&gt;LastSyncDate) Vertices
    /// pointing to records within a collection of DataSources,
    /// that are not marked as already transmitted to Client.
    /// </summary>
    /// <param name="dataSourceIds">Iterable Collection of DataSource </param>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <param name="lastSyncDate">The date of the last Sync request.</param>
    /// <returns>
    /// A collection of GraphVertex Ids (never null, but can be of size 0).
    /// </returns>
    /// <remarks>
    /// Note that this method does not load vertices into in-memory cache.
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    override protected Guid[] storageForSyncGetNewVertexIds(ICollection<int> dataSourceIds, Guid clientMachineId, DateTime lastSyncDate) {
      if (dataSourceIds == null) {
        throw new System.ArgumentNullException("dataSourceIds");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      List<Guid> result = new List<Guid>();

      if (dataSourceIds.Count == 0) {
        return result.ToArray();
      }

      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command =
          CreateCommand_ForSyncGetNewVertexIdsByDataSourceIds(connection, dataSourceIds, clientMachineId, lastSyncDate)) {
          using (IDataReader reader = command.ExecuteReader()) {
            while (reader.Read()) {
              result.Add(reader.GetGuid(0));
            }
          }
        }//~command
      }//~connection
      return result.ToArray();
    }


    /// <summary>
    /// Get Ids of all new Vertices (DateCreated&gt;LastSyncDate)
    /// pointing to specified records in a specified datasource,
    /// that are not marked as already transmitted to client.
    /// </summary>
    /// <param name="dataObjectFactoryId">A single dataSource id.</param>
    /// <param name="dataObjectIds">An iterable Collection of dataObject record </param>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <param name="lastSyncDate">The date of the last Sync request.</param>
    /// <returns>
    /// A collection of GraphVertex Ids (never null, but can be of size 0).
    /// </returns>
    /// <remarks>
    /// Note that this method does not load vertices into in-memory cache.
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    override protected Guid[] storageForSyncGetNewVertexIds(int dataObjectFactoryId, ICollection<string> dataObjectIds, Guid clientMachineId, DateTime lastSyncDate) {
      if (dataObjectFactoryId == 0) {
        throw new System.ArgumentException("dataObjectFactoryId");
      }
      if (dataObjectIds == null) {
        throw new System.ArgumentNullException("dataObjectIds");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      List<Guid> result = new List<Guid>();

      if (dataObjectIds.Count == 0) {
        return result.ToArray();
      }
      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = 
          CreateCommand_ForSyncGetNewVertexIdsByDataSourceIdAndDataObjectIds(connection, dataObjectFactoryId, dataObjectIds, clientMachineId, lastSyncDate)) {
          using (IDataReader reader = command.ExecuteReader()) {
            while (reader.Read()) {
              //Create a vertex, add it to memory if not already added,
              //and add the result to the result collection:
              result.Add(reader.GetGuid(0));
            }
          }
        }//~command
      }//~connection
      return result.ToArray();
    }

    

    /// <summary>
    /// Get Ids of all updated (DateModified&gt;LastSyncDate) Vertices
    /// pointing to records within a collection of DataSources,
    /// that are not marked as already transmitted to Client.
    /// </summary>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <param name="lastSyncDate">The date of the last Sync request.</param>
    /// <returns>
    /// A collection of GraphVertex Ids (never null, but can be of size 0).
    /// </returns>
    /// <remarks>
    /// Note that this method does not load vertices into in-memory cache.
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    override protected Guid[] storageForSyncGetUpdatedVertexIds(Guid clientMachineId, DateTime lastSyncDate) {
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      List<Guid> result = new List<Guid>();

      using (IDbConnection connection = CreateConnection()) {
        using (IDbCommand command = CreateCommand_ForSyncGetUpdatedVertexIds(connection, clientMachineId, lastSyncDate)) {
          using (IDataReader reader = command.ExecuteReader()) {
            while (reader.Read()) {
              result.Add(reader.GetGuid(0));
            }
          }
        }//~command
      }//~connection
      return result.ToArray();
    }

    

    /// <summary>
    /// Get a collection of GraphVertex Ids that have been deleted
    /// on Server, that are still present on Client.
    /// </summary>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <returns>
    /// A collection of GraphVertex Ids (never null, but can be of size 0).
    /// </returns>
    /// <remarks>
    /// Cannot return a collection of Vertices, as they no longer
    /// exist in the Database. We only know what the Ids were
    /// due to traces within the <c>VerticesSent</c> table.
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    override protected Guid[] storageForSyncGetDeletedVertexIds(Guid clientMachineId) {
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      List<Guid> result = new List<Guid>();

      using (IDbConnection connection = CreateConnection()) {
        //Create the <c>View</c>:
        using (IDbCommand command = CreateCommand_ForSyncGetDeletedVertexIdsI(connection, clientMachineId)) {
          command.ExecuteNonQuery();
        }
        //Outer Left Join, using the <c>View</c>:
        using (IDbCommand command = CreateCommand_ForSyncGetDeletedVertexIdsII(connection, clientMachineId)) {
          using (IDataReader reader = command.ExecuteReader()) {
            while (reader.Read()) {
              result.Add(reader.GetGuid(0));
            }
          }
        }//~command
      }//~connection
      return result.ToArray();
    }



    /// <summary>
    /// Mark that the given GraphVertex Ids are being transmitted to the client.
    /// </summary>
    /// <remarks>
    /// <para>
    /// If successful, call <see cref="storageForSyncMarkTransmissionSuccessful"/>,
    /// if not, invoke <see cref="storageForSyncMarkTransmissionAborted"/> to clean up.
    /// </para>
    /// </remarks>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <param name="vertexIds">The vertex </param>
    override protected void storageForSyncMarkTransmissionBegun(Guid clientMachineId, ICollection<Guid> vertexIds) {
      using (IDbConnection connection = CreateConnection()) {
        //Create the <c>View</c>:
        using (IDbCommand command = CreateCommand_ForSyncMarkTransmissionBegun(connection, clientMachineId, vertexIds)) {
          command.ExecuteNonQuery();
        }
      }
    }

    /// <summary>
    /// Deletes references to Vertices that were transmitted to a client
    /// but didn't make it (eg: communication was interrupted).
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked to cleanup a transmission that was organized
    /// with the help of <see cref="storageForSyncMarkTransmissionBegun"/>.
    /// </para>
    /// </remarks>
    /// <param name="clientMachineId">The client machine id.</param>
    override protected void storageForSyncMarkTransmissionAborted(Guid clientMachineId) {
      using (IDbConnection connection = CreateConnection()) {
        //Create the <c>View</c>:
        using (IDbCommand command = CreateCommand_ForSyncMarkTransmissionAborted(connection, clientMachineId)) {
          command.ExecuteNonQuery();
        }
      }
    }

    /// <summary>
    /// Marks all vertices that are being transmitted, as being received by client.
    /// </summary>
    /// <param name="clientMachineId">The client machine id.</param>
    override protected void storageForSyncMarkTransmissionSuccessful(Guid clientMachineId) {
      using (IDbConnection connection = CreateConnection()) {
        //Create the <c>View</c>:
        using (IDbCommand command = CreateCommand_ForSyncMarkTransmissionSuccessful(connection, clientMachineId)) {
          command.ExecuteNonQuery();
        }
      }
    }


    /// <summary>
    /// Marks the GraphVertex as having been successfully transmitted to the client.
    /// </summary>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <param name="vertexId">The id of the vertex.</param>
    /// <remarks>
    /// Used by the GraphSyncManager upon receiving a request to create a new GraphVertex.
    /// In other words, it is creating a Server GraphVertex of a GraphVertex that for sure already
    /// exists on the client, which is in all senses the same as a local server vertex
    /// having been transmitted to the remote client...
    /// </remarks>
    override protected void storageForSyncMarkVertexIdAsSent(Guid clientMachineId, Guid vertexId) {
      //Check Args:
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }
      using (IDbConnection connection = CreateConnection()) {
        //Create the <c>View</c>:
        using (IDbCommand command = CreateCommand_ForSyncMarkVertexIdAsSent(connection, clientMachineId, vertexId)) {
          command.ExecuteNonQuery();
        }
      }
    }


    /// <summary>
    /// Removes the reference to the GraphVertex from the VerticesSent table.
    /// </summary>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <param name="vertexId">The id of the vertex.</param>
    /// <remarks>
    /// Used by the GraphSyncManager upon receiving a request to delete an existing Vertex,
    /// undoing the work done by <see cref="storageForSyncMarkVertexIdAsSent"/>.
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is null.</exception>
    override protected void storageForSyncRemoveVertexIdFromVerticesSent(Guid clientMachineId, Guid vertexId) {
      //Check Args:
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }
      using (IDbConnection connection = CreateConnection()) {
        //Create the <c>View</c>:
        using (IDbCommand command = CreateCommand_ForSyncRemoveVertexIdFromVerticesSent(connection, clientMachineId, vertexId)) {
          command.ExecuteNonQuery();
        }
      }
    }
    #endregion

    #region Protected Methods - Implementation of GraphDbCachedProviderBase - Initialize Tables

    /// <summary>
    /// Create the underlying Tables used by this provider.
    /// </summary>
    override protected void Initialization_CreateTables() {

      System.Reflection.Assembly assembly = this.GetType().Assembly;//System.Reflection.Assembly.GetEntryAssembly();

      //string dirPath = AppDir + "\\data\\scripts\\";


			

     XAct.Data.DbSchemaInstaller installer =
        new DbSchemaInstaller(
        assembly,
				this.InstallScriptsPrefix,
        null,
        this.ConnectionSettings.Name,
        this.DbSettings.SqlKeyWords,
        this.ParamPrefix);

      installer.Initialize();
    }//Method:End



    #endregion




    #region Protected Methods - Helper Methods - Storage / VertexAttributes
    /// <summary>
    /// TODOC:
    /// </summary>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertex"></param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertex argument is null.</exception>
		protected void storageGlobalAttributesLoad(IDbConnection connection, GraphVertex vertex) {
			//Check Args:
			if (connection == null) {
				throw new System.ArgumentNullException("command");
			}
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			vertex.GlobalAttributes.Clear();

			//Get Global Attributes via second command:
			using (IDbCommand command = CreateCommand_GlobalAttributesLoadByVertexId(connection, vertex.Id)) {
				using (IDataReader reader = command.ExecuteReader()) {
					while (reader.Read()) {
						string key = reader.GetString(0);
						string val = reader.GetString(1);
						//Now what...
						vertex.GlobalAttributes[key] = val;
					}
				}
			}//~command
		}


    /// <summary>
    /// Helper method used by <see cref="storageVertexRecordCreate"/>
    /// and <see cref="storageVertexRecordUpdate"/>
    /// </summary>
    /// <param name="connection">The Connection being used.</param>
    /// <param name="vertex">The vertex being processed.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertex argument is null.</exception>
    protected void storageVertexAttributesWrite(IDbConnection connection, GraphVertex vertex) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("command");
      }
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }
      //Delete Global Attributes:
      List<Guid> tmpVertexIds = new List<Guid>();
      tmpVertexIds.Add(vertex.Id);

      using (IDbCommand command = CreateCommand_GlobalAttributesDeleteByVertexId(connection, tmpVertexIds)) {
        command.ExecuteNonQuery();
      }

      //Save Global Attributes:
      foreach (System.Collections.Generic.KeyValuePair<string, string> attribute in vertex.GlobalAttributes) {
        using (IDbCommand command = CreateCommand_GlobalAttributeSaveSingleByVertexId(connection,vertex.Id,attribute.Key,attribute.Value)){
          command.ExecuteNonQuery();
        }
      }

      //Save Local Attributes *FOR ALL USERS*:
      foreach (object userId in ((IUserVertex)vertex).UserAttributes) {
        GraphVertexAttributeDictionary attributes = ((IUserVertex)vertex).UserAttributes[userId];

        if (attributes.IsDirty) {
          //Delete Local Attributes:
          List<Guid> tmpVertexIds2 = new List<Guid>();
          tmpVertexIds2.Add(vertex.Id);
          using (IDbCommand command = CreateCommand_UserAttributesDeleteByVertexId(connection, userId, tmpVertexIds2)) {
            command.ExecuteNonQuery();
          }

          //vertex.GlobalAttributes.GetEnumerator2();

          foreach (System.Collections.Generic.KeyValuePair<string, string> attribute in vertex.GlobalAttributes) {
            using (IDbCommand command = CreateCommand_UserAttributeSaveSingleByVertexId(connection, vertex.Id, userId, attribute.Key, attribute.Value)) {
              command.ExecuteNonQuery();
            }//~command
          }//~foreach
        }//~isDirty
      }//~userAttributes



    }


    /// <summary>
    /// Creates an IDbCommand to load all global attributes for a given vertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageGlobalAttributesLoad"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection"></param>
    /// <param name="vertexId">The id of the vertex.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_GlobalAttributesLoadByVertexId(IDbConnection connection, Guid vertexId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexId.Equals(Guid.Empty)) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      
      if (!_SQL.ContainsKey("CreateCommand_GlobalAttributesLoadByVertexId")) {
        _SQL["CreateCommand_GlobalAttributesLoadByVertexId"] =
          FormatSql(
            "SELECT {vertexAttributesKey}, {vertexAttributesValue}" +
            " FROM {vertexAttributesTable}" +
            " WHERE" +
            " {vertexAttributesVertexId}=@vertexId" +
            " AND" +
            " {vertexAttributesUserId} IS NULL"
            );

      }
      IDbCommand command = connection.CreateCommand();
      command.CommandText = _SQL["CreateCommand_GlobalAttributesLoadByVertexId"];

      DbHelpers.AttachParam(command, "vertexId", vertexId);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to delete all global attributes for a vertex.
    /// (This is done prior to resaving them).
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexAttributesWrite"/>
    /// </para>
    /// </remarks>
    /// <param name="connection"></param>
    /// <param name="vertexId">The id of the vertex.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_GlobalAttributesDeleteByVertexId(IDbConnection connection, ICollection<Guid> vertexId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexId.Equals(Guid.Empty)) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      if (!_SQL.ContainsKey("CreateCommand_GlobalAttributesDeleteByVertexId")) {
        _SQL["CreateCommand_GlobalAttributesDeleteByVertexId"] =
          FormatSql(
            "DELETE FROM {vertexAttributesTable}" +
            " WHERE" +
            " {vertexAttributesVertexId}=@vertexId" +
            " AND" +
            " {vertexAttributesUserId} IS NULL"
            );
      }

      IDbCommand command = connection.CreateCommand();
      command.CommandText = _SQL["CreateCommand_GlobalAttributesDeleteByVertexId"];

      DbHelpers.AttachParam(command, "vertexId", vertexId);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to save a single global attribute, by vertex id.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexAttributesWrite"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="vertexId">The vertex id.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the key argument is null.</exception>
    virtual protected IDbCommand CreateCommand_GlobalAttributeSaveSingleByVertexId(IDbConnection connection, Guid vertexId, string key, string value) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexId.Equals(Guid.Empty)) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      if (string.IsNullOrEmpty(key)) {
        throw new System.ArgumentNullException("key");
      }
      if (!_SQL.ContainsKey("CreateCommand_GlobalAttributeSaveSingleByVertexId")) {
        _SQL["CreateCommand_GlobalAttributeSaveSingleByVertexId"] =
            FormatSql(
            "INSERT INTO {vertexAttributesTable}" +
            " (" +
            " {vertexAttributesVertexId}," +
            " {vertexAttributesUserId}," +
            " {vertexAttributesKey}," +
            " {vertexAttributesValue}" +
            " )" +
            " VALUES" +
            " (" +
            " @vertexId," +
            " NULL," +
            " @key" +
            " @value" +
            " )"
          );
      }

      IDbCommand command = connection.CreateCommand();
      command.CommandText = _SQL["CreateCommand_GlobalAttributeSaveSingleByVertexId"];

      DbHelpers.AttachParam(command, "vertexId", vertexId);
      DbHelpers.AttachParam(command, "key", key);
      DbHelpers.AttachParam(command, "value", value);

      SqlTrace.TraceCommand(command);
      return command;
    }


    #endregion



    #region virtual protected Methods  - For Synching

    /// <summary>
    /// Creates an IDbCommand to be used 
    /// by <see cref="storageForSyncGetNewVertexIds(ICollection{int}, Guid , DateTime)"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncGetNewVertexIds(ICollection{int},Guid, DateTime)"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection to create the command on.</param>
    /// <param name="dataObjectFactoryId">The DataSource's Id.</param>
    /// <param name="dataObjectIds">An iterable collection of dataobject </param>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="lastSyncDate">The date of the last Sync.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectFactoryId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncGetNewVertexIdsByDataSourceIdAndDataObjectIds(IDbConnection connection, int dataObjectFactoryId, ICollection<string> dataObjectIds, Guid clientMachineId, DateTime lastSyncDate) {
      //Check args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObjectFactoryId == 0) {
        throw new System.ArgumentException("dataObjectFactoryId");
      }
      if (dataObjectIds == null) {
        throw new System.ArgumentNullException("dataObjectIds");
      }
      if (clientMachineId.Equals(Guid.Empty)) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      if (!_SQL.ContainsKey("CreateCommand_ForSyncGetNewVertexIdsByDataSourceIdAndDataObjectIds")) {
        _SQL["CreateCommand_ForSyncGetNewVertexIdsByDataSourceIdAndDataObjectIds"] =
          FormatSql(
          //Gets all Vertexes that have not been sent,
          //And that have a create date after the given date:
        "SELECT {vertexId}"+
        " FROM {vertexTable} as V" +
        " LEFT OUTER JOIN" + //<-- We want the join to return even Nulls in right (VS)
        " {verticesSentTable} AS VS" +
        " ON V.{vertexId}=VS.{verticesSentVertexId}" +
        " WHERE " +
        " V.{vertexDataSourceId} IN ({0})" + //Multiple DataSources.
        " AND"+
        " V.{vertexDateCreated}>@lastSyncDate" +
        " AND" +
        " VS.{verticesSentMachineId}=@clientMachineId" +
        " AND" +
        " VS.{verticesSentVertexId} IS NULL" //<-- We only want V's that were not sent.
          );
      };
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(dataObjectIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_ForSyncGetNewVertexIdsByDataSourceIdAndDataObjectIds"],INClause);

      DbHelpers.AttachParam(command, "lastSyncDate", lastSyncDate);
      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to be used by <see cref="storageForSyncGetNewVertexIds(ICollection{int}, Guid , DateTime)"/>
    /// </summary>
    /// <remarks>
    /// Invoked by <see cref="storageForSyncGetNewVertexIds(ICollection{int}, Guid, DateTime)"/>.
    /// </remarks>
    /// <param name="connection">The connection to create the command on.</param>
    /// <param name="dataSourceIds">An iterable collection of dataSource </param>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="lastSyncDate">The last Sync date.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncGetNewVertexIdsByDataSourceIds(IDbConnection connection, ICollection<int> dataSourceIds, Guid clientMachineId, DateTime lastSyncDate) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataSourceIds == null) {
        throw new System.ArgumentNullException("dataObjectIds");
      }
      if (clientMachineId.Equals(Guid.Empty)) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      if (!_SQL.ContainsKey("CreateCommand_ForSyncGetNewVertexIdsByDataSourceIds")) {
        _SQL["CreateCommand_ForSyncGetNewVertexIdsByDataSourceIds"] =
          FormatSql(
          //Gets all Vertexes that have not been sent,
          //And that have a create date after the given date:
        "SELECT {vertexId}"+
        " FROM {vertexTable} as V" +
        " LEFT OUTER JOIN" + //<-- We want the join to return even Nulls in right (VS)
        " {vertexIdsSentTable} AS VS" +
        " ON V.{vertexId}=VS.{vertexIdsSentVertexId}" +
        " WHERE " +
        " V.{vertexDataSourceId} IN ({0})" +
        " AND" +
        " V.{vertexDateCreated}>@lastSyncDate" +
        " AND" +
        " VS.{vertexIdsSentMachineId}=@clientMachineId" +
        " AND" +
        " VS.{vertexIdsSentVertexId} IS NULL" //<-- We only want V's that were not sent.
          );
      };

      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(dataSourceIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_ForSyncGetNewVertexIdsByDataSourceIds"],INClause);

      DbHelpers.AttachParam(command, "lastSyncDate", lastSyncDate);
      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to be used by <see cref="storageForSyncGetUpdatedVertexIds(Guid , DateTime)"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncGetUpdatedVertexIds"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="clientMachineId">The client machine id.</param>
    /// <param name="lastSyncDate">The last sync date.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncGetUpdatedVertexIds(IDbConnection connection, Guid clientMachineId, DateTime lastSyncDate) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (clientMachineId.Equals(Guid.Empty)) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      IDbCommand command = connection.CreateCommand();
      if (!_SQL.ContainsKey("CreateCommand_ForSyncGetUpdatedVertexIds")) {
        _SQL["CreateCommand_ForSyncGetUpdatedVertexIds"] =
          //Gets all Vertexes that have not been sent,
          //And that have a create date after the given date:
          FormatSql(
            "SELECT {vertexId}"+
            " FROM {vertexTable} as V" +
            " INNER JOIN" + //<-- We want the join to return V's that are already in right (VS) 
            " {verticesSentTable} AS VS" +
            " ON V.{vertexId}=VS.{verticesSentVertexId}" +
            " WHERE " +
            " V.{vertexDateModified}>@lastSyncDate" +
            " AND" +
            " VS.{vertexIdsSentMachineId}=@clientMachineId"
              );
      };
      command.CommandText = _SQL["CreateCommand_ForSyncGetUpdatedVertexIds"];

      DbHelpers.AttachParam(command, "lastSyncDate", lastSyncDate);
      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to be used by <see cref="storageForSyncGetDeletedVertexIds(Guid)"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncGetDeletedVertexIds"/>.
    /// </para>
    /// <para>
    /// Creates a Temporary View that is then referred to by
    /// <see cref="CreateCommand_ForSyncGetDeletedVertexIdsII"/>.
    /// </para>
    /// <para>
    /// Important: TODO: Temporary Views are *not* portable SQL. 
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="clientMachineId">The client machine id.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncGetDeletedVertexIdsI(IDbConnection connection, Guid clientMachineId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (clientMachineId.Equals(Guid.Empty)) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      IDbCommand command = connection.CreateCommand();
      if (!_SQL.ContainsKey("CreateCommand_ForSyncGetDeletedVertexIdsI_" + clientMachineId)) {
				string VIEW_NAME = "VS_" + clientMachineId.ToString(this.GuidFormatCode);//OK

        _SQL["CreateCommand_ForSyncGetDeletedVertexIdsI_" + clientMachineId] =
          FormatSql(
          "CREATE TEMPORARY VIEW " + VIEW_NAME + " AS " + 
          " SELECT VS.{verticesSentVertexId}"+
        " FROM {verticesSentTable} as VS" +
        " WHERE"+
        " {verticesSentMachineId}=@machineId"
          );
      };

      command.CommandText = _SQL["CreateCommand_ForSyncGetDeletedVertexIdsI_" + clientMachineId];

      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to be used by <see cref="storageForSyncGetDeletedVertexIds(Guid)"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncGetDeletedVertexIds"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="clientMachineId">The client machine id.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncGetDeletedVertexIdsII(IDbConnection connection, Guid clientMachineId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      if (!_SQL.ContainsKey("CreateCommand_ForSyncGetDeletedVertexIdsII_" + clientMachineId)) {

        string VIEW_NAME = "VS_" + clientMachineId.ToString(this.GuidFormatCode);//OK

        _SQL["CreateCommand_ForSyncGetDeletedVertexIdsII_" + clientMachineId] =
          FormatSql(
          //Gets all Vertexes that have not been sent,
          //And that have a create date after the given date:
        "SELECT VS.{verticesSentVertexId}"+
        " FROM " + VIEW_NAME + " AS VS" +
        " LEFT OUTER JOIN" + //<-- We want the join to return V's that are NULL in VS 
        " {vertexTable} AS V" +
        " ON VS.{verticesSentVertexId}=V.{vertexId}" +
        " WHERE" +
        " V.{vertexId} IS NULL"
        );
      }

      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_ForSyncGetDeletedVertexIdsII_" + clientMachineId];

      SqlTrace.TraceCommand(command);
      return command;
    }




    /// <summary>
    /// Creates an IDbCommand to change the status of Vertices sent.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncMarkTransmissionBegun"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <param name="vertexIds">The vertex </param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncMarkTransmissionBegun(IDbConnection connection, Guid clientMachineId, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }
      if (!_SQL.ContainsKey("CreateCommand_ForSyncMarkTransmissionBegun")) {
        _SQL["CreateCommand_ForSyncMarkTransmissionBegun"] =
          FormatSql(
          "UPDATE {verticesSentTable}" +
          " SET" +
          " {verticesSentStatus}=1," +
          " WHERE " +
          " {verticesSentMachineId}=@clientMachineId"  + 
          " AND" +
          " {verticesVertexId} IN ({0})"
        );
      }
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_ForSyncMarkTransmissionBegun"],INClause);

      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to delete any entries that are still in the VerticesSent table, 
    /// that have a status of Pending (ie, unconfirmed that the Client has received them).
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncMarkTransmissionAborted"/>.
    /// </para>
    /// <para>
    /// 'Pending' is the value 1.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncMarkTransmissionAborted(IDbConnection connection, Guid clientMachineId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      if (!_SQL.ContainsKey("CreateCommand_ForSyncMarkTransmissionAborted")) {
        _SQL["CreateCommand_ForSyncMarkTransmissionAborted"] =
          FormatSql(
          "DELETE FROM {verticesSentTable}" +
          " WHERE" +
          " {verticesSentMachineId}=@clientMachineId" +
          " AND" +
          " {verticesSentStatus}=1"
          );
      }
      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_ForSyncMarkTransmissionAborted"];

      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to update the status of VerticesSent from Pending to Confirmed.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncMarkTransmissionAborted"/>.
    /// </para>
    /// <para>
    /// 'Pending' is the value 1.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncMarkTransmissionSuccessful(IDbConnection connection, Guid clientMachineId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }

      if (!_SQL.ContainsKey("CreateCommand_ForSyncMarkTransmissionSuccessful")) {
        _SQL["CreateCommand_ForSyncMarkTransmissionSuccessful"] =
          FormatSql(
          "UPDATE {verticesSentTable}" +
          " SET" +
          " {verticesSentStatus}=0," +
          " WHERE " +
          " {verticesSentMachineId}=@clientMachineId" +
          " AND" +
          " {verticesSentStatus}=1"
          );
      }
      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_ForSyncMarkTransmissionSuccessful"];

      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to update the status of VerticesSent from Pending to Confirmed.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncMarkVertexIdAsSent"/>.
    /// </para>
    /// <para>
    /// 'Sent' value is 0.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <param name="vertexId">The id of the vertex.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncMarkVertexIdAsSent(IDbConnection connection, Guid clientMachineId, Guid vertexId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }
      if (!_SQL.ContainsKey("CreateCommand_ForSyncMarkVertexIdAsSent")) {
        _SQL["CreateCommand_ForSyncMarkVertexIdAsSent"] =
          FormatSql(
          "INSERT INTO {verticesSentTable}" +
          " (" +
          " {verticesSentMachineId}," +
          " {verticesSentVertexId}" +
          " {verticesSentStatus}"+
          " )" +
          " VALUES" +
          " (" +
          " @clientMachineId," +
          " @vertexId," +
          " 0" +  //Mark it as sent...
          " )"
        );
      }

      
      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_ForSyncMarkVertexIdAsSent"];

      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);
      DbHelpers.AttachParam(command, "vertexId", vertexId);

      SqlTrace.TraceCommand(command);
      return command;

    }

    /// <summary>
    /// Creates an IDbCommand to update the status of VerticesSent from Pending to Confirmed.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageForSyncRemoveVertexIdFromVerticesSent"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="clientMachineId">The client machine id.</param>
    /// <param name="vertexId">The id of the vertex.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_ForSyncRemoveVertexIdFromVerticesSent(IDbConnection connection, Guid clientMachineId, Guid vertexId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (clientMachineId == Guid.Empty) {
        throw new System.ArgumentNullException("clientMachineId");
      }
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }
      if (!_SQL.ContainsKey("CreateCommand_ForSyncRemoveVertexIdFromVerticesSent")) {
        _SQL["CreateCommand_ForSyncRemoveVertexIdFromVerticesSent"] =
          FormatSql(
          "DELETE FROM {verticesSentTable}"+
          " WHERE"+
          " {verticesSentMachineId}=@clientMachineId" +
          " AND" +
          " {verticesVertexId}=@vertexId"
        );
      }

      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_ForSyncRemoveVertexIdFromVerticesSent"];

      DbHelpers.AttachParam(command, "clientMachineId", clientMachineId);
      DbHelpers.AttachParam(command, "vertexId", vertexId);

      SqlTrace.TraceCommand(command);
      return command;

    }

    /// <summary>
    /// Creates an IDbCommand to return all In-Edges connected to the specified GraphVertex 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexIdsGetInOutVertexIds"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="vertexIds">The vertex </param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexGetInVertexIds(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexGetInVertexIds")) {
        _SQL["CreateCommand_VertexGetInVertexIds"] =
          FormatSql(
          "SELECT FROM {edgeTable}"+
          " WHERE"+
          " {edgeCID} IN ({0})"
          );
      }
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_VertexGetInVertexIds"], INClause);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to return all Out-Edges connected to the specified GraphVertex 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexIdsGetInOutVertexIds"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="vertexIds">The vertex </param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexGetOutVertexIds(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexGetOutVertexIds")) {
        _SQL["CreateCommand_VertexGetOutVertexIds"] =
          FormatSql(
          "SELECT FROM {edgeTable}" +
          " WHERE" +
          " {edgePID} IN ({0})"
          );
      }
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_VertexGetOutVertexIds"], INClause);

      SqlTrace.TraceCommand(command);
      return command;
    }
  
    #endregion

    #region virtual protected Methods - CreateCommands - Vertices
    /// <summary>
    /// Creates an IDbCommand for creating a new GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="M:storageVertexRecordCreate"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertex"></param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertex argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexCreateRecord(IDbConnection connection, GraphVertex vertex) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexCreateRecord")) {
        _SQL["CreateCommand_VertexCreateRecord"] =
            FormatSql(
            "INSERT INTO {vertexTable}" +
            " (" +
            " {vertexId}, {vertexWeight}," +
            " {vertexDataSourceId}, {vertexRecordId}," +
            " {vertexCreated}, {vertexEdited}, {vertexSync}, {vertexDeleted}" +
            " )" +
            " VALUES" +
            " (" +
            " @vertexId, @vertexWeight," +
            " @vertexDataSourceId, @vertexRecordId," +
            " @vertexCreated, @vertexEdited, @vertexSync, @vertexDeleted" +
            " )");
      }
      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_VertexCreateRecord"];

      DbHelpers.AttachParam(command, "vertexId", vertex.Id);
      DbHelpers.AttachParam(command, "vertexWeight", vertex.Weight);
      DbHelpers.AttachParam(command, "vertexDataSourceId", vertex.DataSourceId);
      DbHelpers.AttachParam(command, "vertexRecordId", vertex.DataRecordId);
      DbHelpers.AttachParam(command, "vertexCreated", vertex.UTCDateCreated);
      DbHelpers.AttachParam(command, "vertexEdited", vertex.UTCDateEdited);
      //IMPORTANT:
      //The write happens in two steps:
      //First we write the value to the DbParam leaving only the Remote flags up.
      //If it *FAILS*, the object has not been modified, so need to save/rollback the GraphVertex values as well.
      //If it succeeds, we then have to do the changes to the flags in a secondary step:
      DbHelpers.AttachParam(command, "vertexSync", (int)(((ICachedElement)vertex).SyncStatus & OfflineModelState.RemotePending));
      DbHelpers.AttachParam(command, "vertexDeleted", false);


      SqlTrace.TraceCommand(command);
      return command;
    }





    /// <summary>
    /// Creates an IDbCommand for updating a single existing Vertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="M:CreateCommand_VertexCreateRecord"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertex"></param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertex argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_VertexUpdateRecord(IDbConnection connection, GraphVertex vertex) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }

      if (!_SQL.ContainsKey("CreateCommand_UpdateVertexRecord")) {
        _SQL["CreateCommand_UpdateVertexRecord"] =
            FormatSql(
            "UPDATE {vertexTable}" +
            " SET" +
            " {vertexWeight}=@vertexWeight," +
            " {vertexEdited}=@vertexEdited," +
            " {vertexSync}=@vertexSync" +
            " WHERE " +
            "({vertexId}=@vertexId)"
            );
      }
      IDbCommand command = connection.CreateCommand();

      command.CommandText = _SQL["CreateCommand_UpdateVertexRecord"];

      DbHelpers.AttachParam(command, "vertexWeight", vertex.Weight);
      DbHelpers.AttachParam(command, "vertexEdited", vertex.UTCDateEdited);
      //IMPORTANT:
      //The write happens in two steps:
      //First we write the value to the DbParam leaving only the Remote flags up.
      //If it *FAILS*, the object has not been modified, so need to save/rollback the GraphVertex values as well.
      //If it succeeds, we then have to do the changes to the flags in a secondary step:
      DbHelpers.AttachParam(command, "vertexSync", (int)(((ICachedElement)vertex).SyncStatus & OfflineModelState.RemotePending));
      //Condition:
      DbHelpers.AttachParam(command, "vertexId", vertex.Id);

      SqlTrace.TraceCommand(command);
      return command;
    }



    /// <summary>
    /// Creates an IDbCommand to mark one or more vertices' in and out-edges as deleted.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexRecordMarkForDeletion"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexIds">The ids of the vertices.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexRecordMarkForDeletionEdges(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexRecordMarkForDeletionEdges")) {
        _SQL["CreateCommand_VertexRecordMarkForDeletionEdges"] =
            FormatSql(
            "UPDATE {edgeTable}" +
            " SET" +
            " {edgeDeleted}=@edgeDeleted" + //1
            " WHERE" +
            " {edgeSource} IN ({0})" +
            " OR" +
            " {edgeTarget} IN ({1})"
            );
      }
      IDbCommand command = connection.CreateCommand();

      StringBuilder sbSource = new StringBuilder();
      StringBuilder sbTarget = new StringBuilder();
      foreach (Guid vertexId in vertexIds){
        sbSource.Append(", ");
        //BUGFIX: Convert to String because some IDbDataParameter (Sqlite...) garble the output (maybe converting binary to string?).
				sbSource.Append("'" + vertexId.ToString(this.GuidFormatCode) + "'");//OK

        sbTarget.Append(", ");
        //BUGFIX: Convert to String because some IDbDataParameter (Sqlite...) garble the output (maybe converting binary to string?).
				sbTarget.Append("'" + vertexId.ToString(this.GuidFormatCode) + "'");//OK
      }
      //Remove first COMMA:
      if (sbSource.Length > 0) {
        sbSource.Remove(0, 2);
      }
      if (sbTarget.Length > 0) {
        sbTarget.Remove(0, 2);
      }
      command.CommandText = string.Format(_SQL["CreateCommand_VertexRecordMarkForDeletionEdges"], sbSource.ToString(), sbTarget.ToString());

      DbHelpers.AttachParam(command, "edgeDeleted", true);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to mark a vertex as deleted.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexRecordMarkForDeletion"/>,
    /// after it has called <see cref="CreateCommand_VertexRecordMarkForDeletionEdges"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexIds">The ids of the vertices.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexRecordMarkForDeletion(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexRecordMarkForDeletion")) {
        _SQL["CreateCommand_VertexRecordMarkForDeletion"] =
            FormatSql(
            "UPDATE {vertexTable}" +
            " SET {vertexDeleted}=@vertexDeleted" + //1
            " WHERE" +
            " {vertexId} IN ({0})"
            );
      }
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_VertexRecordMarkForDeletion"],INClause);

      DbHelpers.AttachParam(command, "vertexDeleted", true);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to delete a Vertex's adjacent edges.
    /// </summary>
    /// <remarks>
    /// Invoked by <see cref="storageVertexRecordDelete"/>.
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexIds">The ids of the vertices.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexDeleteAdjacentEdges(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }


      if (!_SQL.ContainsKey("CreateCommand_VertexDeleteAdjacentEdges")) {
        _SQL["CreateCommand_VertexDeleteAdjacentEdges"] =
                      FormatSql(
            "DELETE FROM {edgeTable}" +
            " WHERE" +
            " {edgeSource} IN ({0})" + 
            " OR"+
            " {edgeTarget} IN ({0})"
            );

      }

      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_VertexDeleteAdjacentEdges"],INClause);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to delete one or more Vertice's attributes 
    /// (including global and all users' attributes).
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexRecordDelete"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexIds">The ids of the vertices.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexDeleteAllAttributesByVertexId(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexDeleteAllAttributesByVertexId")) {
        _SQL["CreateCommand_VertexDeleteAllAttributesByVertexId"] =
            FormatSql(
            "DELETE FROM {vertexAttributesTable}" +
            " WHERE" +
            " {vertexAttributesVertexId} IN ({0})"
            );
      }
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_VertexDeleteAllAttributesByVertexId"],INClause);

      SqlTrace.TraceCommand(command);
      return command;
    }

    /// <summary>
    /// Creates an IDbCommand to delete one or more Vertices.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexRecordDelete"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexIds">The ids of the vertices.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexDeleteByVertexId(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexDeleteByVertexId")) {
        _SQL["CreateCommand_VertexDeleteByVertexId"] =
          FormatSql(
            "DELETE FROM {vertexTable}" +
            " WHERE" +
            " {vertexId} IN ({0})"
        );
      }
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds,null);
      command.CommandText = string.Format(_SQL["CreateCommand_VertexDeleteByVertexId"],INClause);

      SqlTrace.TraceCommand(command);
      return command;
    }




    /// <summary>
    /// Creates an IDbCommand to get a single GraphVertex by its Id.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexGet"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection to use.</param>
    /// <param name="vertexId">The id of the vertex.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_VertexGetByVertexId(IDbConnection connection, Guid vertexId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexGetByVertexId")) {
        _SQL["CreateCommand_VertexGetByVertexId"] =
            FormatSql(
            "SELECT {vertexFields}" +
            " FROM {vertexTable} as V" +
            " WHERE" +
            " V.{vertexId}=@vertexId" +
            " AND" +
            " V.{vertexDeleted}=0"
            );
      }
      IDbCommand command = connection.CreateCommand();
      command.CommandText = _SQL["CreateCommand_VertexGetByVertexId"];
      DbHelpers.AttachParam(command, "vertexId", vertexId);

      SqlTrace.TraceCommand(command);
      return command;
    }




    /// <summary>
    /// Creates an IDbCommand to get a single GraphVertex by its Id.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="GraphProviderBase.storageGetVertices(int, ICollection{string}, ref GraphVertexList , out ICollection{string})"/>.
    /// </para>
    /// <para>
    /// Returns multiple Vertices.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection to use.</param>
    /// <param name="vertexIds">The ids of the vertices.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VerticesGetByVertexId(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }

      if (!_SQL.ContainsKey("CreateCommand_VerticesGetByVertexId")) {
        _SQL["CreateCommand_VerticesGetByVertexId"] =
            FormatSql(
            "SELECT {vertexFields}" +
            " FROM {vertexTable} as V" +
            " WHERE" +
            " V.{vertexId}=@vertexId" +
            " AND" +
            " V.{vertexDeleted}=0"
            );
      }
      IDbCommand command = connection.CreateCommand();
      string sqlINClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_VerticesGetByVertexId"], sqlINClause);

      DbHelpers.AttachParam(command, "vertexDeleted", false);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to get vertices by their Record 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageGetVertices(int, ICollection{string}, ref GraphVertexList, out ICollection{string})"/>.
    /// </para>
    /// <para>
    /// Returns multiple Vertices.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="dataObjectFactoryId">The data source id.</param>
    /// <param name="dataObjectIds">The data object </param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectFactoryId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VerticesGetByRecordIds(IDbConnection connection, int dataObjectFactoryId, ICollection<string> dataObjectIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (dataObjectFactoryId == 0) {
        throw new System.ArgumentNullException("dataObjectFactoryId");
      }

      if (dataObjectIds == null) {
        throw new System.ArgumentNullException("dataObjectIds");
      }

      if (!_SQL.ContainsKey("CreateCommand_VerticesGetByRecordIds")) {
        _SQL["CreateCommand_VerticesGetByRecordIds"] =
            FormatSql(
            "SELECT {vertexFields}" +
            " FROM {vertexTable} as V" +
            " WHERE"+
            " V.{vertexDeleted}=0" +
            " AND"+
            " V.{vertexDataSourceId}=@vertexDataSourceId" +
            " AND"+
            " V.{vertexRecordId} IN ({0})"
            );
      }
      IDbCommand command = connection.CreateCommand();

      //Build Sql IN clause:
      //string sqlINClause = CreateINClause(dataObjectIds, null);
      string sqlInClause;
      StringBuilder sb = new StringBuilder();
      foreach (string id in dataObjectIds) {
        sb.AppendFormat(", '{0}'", id);
      }
      if (sb.Length != 0) {
        sb.Remove(0, 1);
      }
      sqlInClause = sb.ToString();

      command.CommandText = string.Format(_SQL["CreateCommand_VerticesGetByRecordIds"], sqlInClause);

      DbHelpers.AttachParam(command, "vertexDeleted", false);
      DbHelpers.AttachParam(command, "vertexDataSourceId", dataObjectFactoryId);

      SqlTrace.TraceCommand(command);
      return command;
    }

#endregion

    #region virtual protected Methods - CreateCommands - VertexAttributes
    /// <summary>
    /// Creates an IDbCommand to retrieve user attributes for a vertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexUserAttributesLoad"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexId">The id of the vertex.</param>
    /// <param name="userId">The user id.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the userId argument is null.</exception>
    virtual protected IDbCommand CreateCommand_UserAttributesLoadByVertexId(IDbConnection connection, Guid vertexId, object userId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }
      if (userId == null) {
        throw new System.ArgumentNullException("userId");
      }

      if (!_SQL.ContainsKey("CreateCommand_UserAttributesLoadByVertexId")) {
        _SQL["CreateCommand_UserAttributesLoadByVertexId"] =
            FormatSql(
            "SELECT {vertexAttributesKey}, {vertexAttributesValue}" +
            " FROM {vertexAttributesTable}" +
            " WHERE" +
            " {vertexAttributesVertexId}=@vertexId" +
            " AND" +
            " {vertexAttributesUserId}=@userId"
            );
      }
      IDbCommand command = connection.CreateCommand();
      command.CommandText = _SQL["CreateCommand_UserAttributesLoadByVertexId"];

      DbHelpers.AttachParam(command, "vertexId", vertexId);
      DbHelpers.AttachParam(command, "userId", userId);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to delete all user attributes of a vertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexUserAttributesSave"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexIds">The vertex </param>
    /// <param name="userId">The user id.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the userId argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_UserAttributesDeleteByVertexId(IDbConnection connection, object userId, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertexIds");
      }
      if (userId == null) {
        throw new System.ArgumentNullException("userId");
      }

      if (!_SQL.ContainsKey("CreateCommand_UserAttributesDeleteByVertexId")) {
        _SQL["CreateCommand_UserAttributesDeleteByVertexId"] =
            FormatSql(
            "DELETE FROM {vertexAttributesTable}" +
            " WHERE" +
            " {vertexAttributesVertexId} IN ({0})" +
            " AND" +
            " {vertexAttributesUserId}=@userId"
            );
      }
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_UserAttributesDeleteByVertexId"],INClause);

      DbHelpers.AttachParam(command, "userId", userId);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to save a user attribute for a vertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexUserAttributesSave"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexId">The vertex id.</param>
    /// <param name="userId">The user id.</param>
    /// <param name="key">The attribute key.</param>
    /// <param name="value">The attribute value.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the userId argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the key argument is null.</exception>
    virtual protected IDbCommand CreateCommand_UserAttributeSaveSingleByVertexId(IDbConnection connection, Guid vertexId, object userId, string key, string value) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }
      if (userId == null) {
        throw new System.ArgumentNullException("userId");
      }
      if (string.IsNullOrEmpty(key)) {
        throw new System.ArgumentNullException("key");
      }

      if (!_SQL.ContainsKey("CreateCommand_UserAttributeSaveSingleByVertexId")) {
        _SQL["CreateCommand_UserAttributeSaveSingleByVertexId"] =
            FormatSql(
            "INSERT INTO {vertexAttributesTable}" +
            " (" +
            " {vertexAttributesVertexId}," +
            " {vertexAttributesUserId}," +
            " {vertexAttributesKey}," +
            " {vertexAttributesValue}" +
            " )" +
            " VALUES" +
            " (" +
            " @vertexId," +
            " @userId," +
            " @key" +
            " @value" +
            " )"
        );
      }
      IDbCommand command = connection.CreateCommand();
      command.CommandText = _SQL["CreateCommand_UserAttributeSaveSingleByVertexId"];

      DbHelpers.AttachParam(command, "vertexId", vertexId);
      DbHelpers.AttachParam(command, "userId", userId);
      DbHelpers.AttachParam(command, "key", key);
      DbHelpers.AttachParam(command, "value", value);

      SqlTrace.TraceCommand(command);
      return command;

    }


    #endregion

    #region virtual protected Methods - CreateCommands - Edge
		/// <summary>
		/// Creates an IDbCommand to count all edges in the database, that are not marked as deleted.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="storageEdgeCount"/>.
		/// </para>
		/// </remarks>
		/// <param name="connection">The database connection.</param> 
		/// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
		virtual protected IDbCommand CreateCommand_EdgeCount(IDbConnection connection) {
			//Check Args:
			if (connection == null) {
				throw new System.ArgumentNullException("connection");
			}
			if (!_SQL.ContainsKey("CreateCommand_EdgeCount")) {
				_SQL["CreateCommand_EdgeCount"] =
					FormatSql(
						"SELECT" +
						" COUNT({edgeSource})" +
						" FROM {edgeTable}" +
						" WHERE" +
						" {edgeDeleted}=@edgeDeleted" //0
						);
			};
			IDbCommand command = connection.CreateCommand();
			command.CommandText = string.Format(_SQL["CreateCommand_EdgeCount"]);
			return command;
		}




		/// <summary>
		/// Creates an IDbCommand to get all in-edge records in the database.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="storageEdgeEnumerateAll"/>.
		/// </para>
		/// </remarks>
		/// <param name="connection">The database connection.</param> 
		/// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
		virtual protected IDbCommand CreateCommand_GetAllEdges(IDbConnection connection) {
			//Check Args:
			if (connection == null) {
				throw new System.ArgumentNullException("connection");
			}
			if (!_SQL.ContainsKey("CreateCommand_GetAllEdges")) {
				_SQL["CreateCommand_GetAllEdges"] =
					FormatSql(
						"SELECT" +
						" {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}" +
						" FROM {edgeTable}" +
						" WHERE" +
						" {edgeDeleted}=@edgeDeleted" //0
						);
			};
			IDbCommand command = connection.CreateCommand();
			command.CommandText = string.Format(_SQL["CreateCommand_GetAllEdges"]);
			return command;
		}


    /// <summary>
    /// Creates an IDbCommand to get all in-edge records for a given vertex id.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexGetInEdges"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexIds">The ids of the vertices.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
    virtual protected IDbCommand CreateCommand_VertexGetInEdges(IDbConnection connection, ICollection<Guid> vertexIds) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexIds == null) {
        throw new System.ArgumentNullException("vertices");
      }

      if (!_SQL.ContainsKey("CreateCommand_VertexGetInEdges")) {
        _SQL["CreateCommand_VertexGetInEdges"] =
                      FormatSql(
            "SELECT" +
            " {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}" +
            " FROM {edgeTable}" +
            " WHERE" +
            " {edgeTarget} IN ({0})" +
            " AND" +
            " {edgeDeleted}=@edgeDeleted" //0
            );
      };
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateINClause(vertexIds, null);
      command.CommandText = string.Format(_SQL["CreateCommand_VertexGetInEdges"], INClause);

      DbHelpers.AttachParam(command, "edgeDeleted", false);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to get all out-edge records for a given vertex id.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageVertexGetOutEdges"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="vertexId">The id of the vertex.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    virtual protected IDbCommand CreateCommand_VertexGetOutEdges(IDbConnection connection, Guid vertexId) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (vertexId == Guid.Empty) {
        throw new System.ArgumentNullException("vertexId");
      }

      IDbCommand command = connection.CreateCommand();

      if (!_SQL.ContainsKey("CreateCommand_VertexGetOutEdges")) {
        _SQL["CreateCommand_VertexGetOutEdges"] =
            FormatSql(
            "SELECT" +
            " {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}" +
            " FROM {edgeTable}" +
            " WHERE" +
            " {edgeSource}=@edgeSource" +
            " AND" +
            " {edgeDeleted}=@edgeDeleted" //0
            );
      }
      command.CommandText = _SQL["CreateCommand_VertexGetOutEdges"];

      DbHelpers.AttachParam(command, "edgeSource", vertexId);
      DbHelpers.AttachParam(command, "edgeDeleted", false);

      SqlTrace.TraceCommand(command);
      return command;

    }



    /// <summary>
    /// Creates an IDbCommand to create a new GraphEdge record.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageEdgeRecordCreate"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="edge"></param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the edge argument is null.</exception>
    virtual protected IDbCommand CreateCommand_EdgeRecordCreate(IDbConnection connection, GraphEdge edge) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (edge == null) {
        throw new System.ArgumentNullException("edge");
      }

      IDbCommand command = connection.CreateCommand();
      if (!_SQL.ContainsKey("CreateCommand_EdgeRecordCreate")) {
        _SQL["CreateCommand_EdgeRecordCreate"] =
          FormatSql(
            "INSERT INTO {edgeTable}" +
            " (" +
            " {edgeSource}, {edgeTarget}, {edgeWeight}, {edgeSync}, {edgeDeleted}" +
            " )" +
            " VALUES" +
            " (" +
            " @edgeSource, @edgeTarget, @edgeWeight, @edgeSync, @edgeDeleted" +
            " )");
      };

      command.CommandText = _SQL["CreateCommand_EdgeRecordCreate"];

      DbHelpers.AttachParam(command, "edgeSource", edge.SourceId);
      DbHelpers.AttachParam(command, "edgeTarget", edge.TargetId);
      DbHelpers.AttachParam(command, "edgeWeight", edge.Weight);

      //IMPORTANT:
      //The write happens in two steps:
      //First we write the value to the DbParam leaving only the Remote flags up.
      //If it *FAILS*, the object has not been modified, so need to save/rollback the GraphVertex values as well.
      //If it succeeds, we then have to do the changes to the flags in a secondary step:

      //IMPORTANT: SyncStatus needs to be saved as INT to be portable (Sqlite for example will save it as a string):
      DbHelpers.AttachParam(command, "edgeSync", ((ICachedElement)edge).SyncStatus & OfflineModelState.RemotePending);
      DbHelpers.AttachParam(command, "edgeDeleted", false);

      SqlTrace.TraceCommand(command);
      return command;
    }


    /// <summary>
    /// Creates an IDbCommand to update an existing Edge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageEdgeRecordUpdate"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The database connection.</param> 
    /// <param name="edge"></param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the edge argument is null.</exception>
    virtual protected IDbCommand CreateCommand_EdgeRecordUpdate(IDbConnection connection, GraphEdge edge) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (edge == null) {
        throw new System.ArgumentNullException("edge");
      }

      IDbCommand command = connection.CreateCommand();

      if (!_SQL.ContainsKey("CreateCommand_EdgeRecordUpdate")) {
        _SQL["CreateCommand_EdgeRecordUpdate"] =
            FormatSql(
          "UPDATE {edgeTable}" +
            " SET" +
            " {edgeWeight}=@edgeWeight," +
            " {edgeSync}=@edgeSync" +
            " WHERE" +
            " {edgeSource}=@edgeSource" +
            " AND" +
            " {edgeTarget}=@edgeTarget"
            );
      };

      command.CommandText = _SQL["CreateCommand_EdgeRecordUpdate"];


      DbHelpers.AttachParam(command, "edgeWeight", edge.Weight);
      //IMPORTANT:
      //The write happens in two steps:
      //First we write the value to the DbParam leaving only the Remote flags up.
      //If it *FAILS*, the GraphEdge has not been modified, so need to save/rollback the GraphVertex values as well.
      //If it succeeds, we then have to do the changes to the flags in a secondary step:
      DbHelpers.AttachParam(command, "edgeSync", ((ICachedElement)edge).SyncStatus & OfflineModelState.RemotePending);

      DbHelpers.AttachParam(command, "edgeSource", edge.Source);
      DbHelpers.AttachParam(command, "edgeTarget", edge.Target);

      SqlTrace.TraceCommand(command);
      return command;
    }



    /// <summary>
    /// Creates an IDbCommand to mark edge records for deletion.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageEdgeRecordMarkForDeletion"/>.
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="edges">The Collection of Edges.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the edges argument is null.</exception>
    virtual protected IDbCommand CreateCommand_EdgeRecordMarkForDeletion(IDbConnection connection, ICollection<GraphEdge> edges) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (edges == null) {
        throw new System.ArgumentNullException("edges");
      }

      if (!_SQL.ContainsKey("CreateCommand_EdgeRecordMarkForDeletion")) {
        _SQL["CreateCommand_EdgeRecordMarkForDeletion"] =
            FormatSql(
            "UPDATE {edgeTable}" +
            " SET" +
            " {edgeDeleted}=@edgeDeleted," + //1
            " {edgeSync}=@edgeSync" +
            " WHERE" +
            " {0}"
            );
      }
      //@@@

      IDbCommand command = connection.CreateCommand();

      string INClause = CreateInClauseFromEdge(edges);
      command.CommandText = string.Format(_SQL["CreateCommand_EdgeRecordMarkForDeletion"], INClause);

      DbHelpers.AttachParam(command, "edgeDeleted", true);
      //IMPORTANT:
      //The write happens in two steps:
      //First we write the value to the DbParam leaving only the Remote flags up.
      //If it *FAILS*, the object has not been modified, so need to save/rollback the GraphVertex values as well.
      //If it succeeds, we then have to do the changes to the flags in a secondary step:

      //OfflineModelState.RemoteDeletionPending;


      //HACK:
      //Do to batching, and therefore no longer being able to address each edge
      //individually, I can no longer do:
      //DbHelpers.AttachParam(command, "edgeSync", (int)(((ICachedElement)edge).SyncStatus & OfflineModelState.RemotePending));
      //But we know that the vertex is marked to be deleted...which preceeds
      //all other operations...so, we can just go ahead and strip out any other
      //values that were there before:
      DbHelpers.AttachParam(command, "edgeSync", OfflineModelState.RemotePending);

      SqlTrace.TraceCommand(command);
      return command;
    }




    /// <summary>
    /// Creates an IDbCommand to delete an GraphEdge record.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="storageEdgeRecordDelete"/>
    /// </para>
    /// </remarks>
    /// <param name="connection">The connection.</param>
    /// <param name="edges">The edges.</param>
    /// <returns>An IDbCommand with command text and all parameters already initialized.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the connection argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the edges argument is null.</exception>
    virtual protected IDbCommand CreateCommand_EdgeRecordDelete(IDbConnection connection, ICollection<GraphEdge> edges) {
      //Check Args:
      if (connection == null) {
        throw new System.ArgumentNullException("connection");
      }
      if (edges == null) {
        throw new System.ArgumentNullException("edges");
      }
      if (!_SQL.ContainsKey("CreateCommand_EdgeRecordDelete")) {
        _SQL["CreateCommand_EdgeRecordDelete"] =
            FormatSql(
            "DELETE FROM {edgeTable}" +
            " WHERE" +
            " {0}"
            );
      }
      //@@
      IDbCommand command = connection.CreateCommand();

      string INClause = CreateInClauseFromEdge(edges);
      command.CommandText = string.Format(_SQL["CreateCommand_EdgeRecordDelete"], INClause);

      SqlTrace.TraceCommand(command);
      return command;
    }






    #endregion


    #region HelperMethods 

    /// <summary>
    /// Extract the Ids from the given Vertices.
    /// </summary>
    /// <param name="vertices">A Collection of Vertices</param>
    /// <returns>A list of their Guids</returns>
    virtual protected List<Guid> ExtractIds(ICollection<GraphVertex> vertices) {
      return
       new List<Guid>(
         Collections.Map<GraphVertex, Guid>(
           vertices,
           delegate(GraphVertex v) {
             return v.Id;
           }));

    }

    /// <summary>
    /// Creates the in clause from a collection of edges.
    /// Creates a SQL fragment like: <c>("[OR] (edgeSource='...' AND edgeTarget='....')</c>
    /// </summary>
    /// <remarks>
    /// Creates a SQL fragment in the format of <c>("[OR] (edgeSource='...' AND edgeTarget='....')</c>
    /// </remarks>
    /// <param name="edges">The edges.</param>
    /// <returns></returns>
    virtual protected string CreateInClauseFromEdge(ICollection<GraphEdge> edges) {
      StringBuilder sb = new StringBuilder();
      foreach (GraphEdge edge in edges) {
        sb.Append(" OR ");
        sb.Append(" (");
        //BUGFIX: Convert to String because some IDbDataParameter (Sqlite...) garble the output (maybe converting binary to string?).
				sb.Append(" {edgeSource}='" + edge.SourceId.ToString(this.GuidFormatCode) + "'");//OK
        sb.Append(" AND");
        //BUGFIX: Convert to String because some IDbDataParameter (Sqlite...) garble the output (maybe converting binary to string?).
				sb.Append(" {edgeTarget}='" + edge.TargetId.ToString(this.GuidFormatCode) + "'");//OK
        sb.Append(" )");
      }
      //Remove first OR:
      if (sb.Length > 0) {
        sb.Remove(0, 4);
      }

      return FormatSql(sb.ToString());
    }


    #endregion


  } //Class:End

}//Namespace:End
