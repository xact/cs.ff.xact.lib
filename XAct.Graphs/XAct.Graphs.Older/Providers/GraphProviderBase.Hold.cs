﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs {
	/* class IQuickGraphWrapper {
    /// <summary>
    /// Removes the Edges.
    /// Returns <c>true</c> only if all Edges were created.
    /// Returns <c>false</c> if any Edge already existed.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invokes several times a cancelable <see cref="E:OnEdgeDeleting"/> before deleting the Edge,
    /// and then <see cref="E:OnEdgeDeleted"/> if successful.
    /// </para>
    /// <para>
    /// No error is raised if any of the the source Vertex, 
    /// or target Vertex, or an Edge between
    /// the two, is not found to exist.
    /// </para>
    /// </remarks>
    /// <param name="edgesRemoved">Result Number of Edges removed.</param>
    /// <param name="edges">TheEdges definition.</param>
    /// <returns>
    /// the number ofEdges that have been removed
    /// + the number ofEdges that didn't exists on method call time
    /// </returns>
    bool RemoveEdges(out int edgesRemoved, params GraphEdgeDef[] edges);
	 }
	 */
	/*
			/// <summary>
			/// Removes the Edge between the Source Vertex and Target Vertex.
			/// Returns <c>true</c> only if an Edge is removed, 
			/// Returns <c>false</c> if no Edge already existed.
			/// </summary>
			/// <remarks>
			/// <para>
			/// Invokes a cancelable <see cref="E:OnEdgeDeleting"/> before deleting the Edge,
			/// and then <see cref="E:OnEdgeDeleted"/> if successful.
			/// </para>
			/// <para>
			/// No error is raised if the source Vertex, 
			/// or target Vertex, or an Edge between
			/// the two, is not found to exist.
			/// </para>
			/// </remarks>
			/// <returns>
			/// Returns <c>true</c> only if an Edge is removed, 
			/// Returns <c>false</c> if no Edge already existed.
			/// </returns>
			/// <param name="sourceVertex">The source Vertex.</param>
			/// <param name="targetVertex">The target Vertex.</param>
			/// <exception cref="ArgumentNullException">An exception is raised if the sourceVertex is null.</exception>
			/// <exception cref="ArgumentNullException">An exception is raised if the targetVertex is null.</exception>
			bool RemoveEdge(TVertex sourceVertex, TVertex targetVertex);
			*/

	/*
    /// <summary>
    /// Adds a new Edge between a source Vertex and a target Vertex.
    /// Returns <c>true</c> only if an Edge was created.
    /// Returns <c>false</c> if an Edge already existed.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invokes a cancelable <see cref="E:OnEdgeCreating"/> before creating the Edge,
    /// and then <see cref="E:OnEdgeCreated"/> if successful.
    /// </para>
    /// <para>
    /// No error is raised if an Edge already exists between the two 
    /// Vertex instances:
    /// it just returns <c>false</c>, and <see cref="M:OnEdgeCreated"/>
    /// is never invoked.
    /// </para>
    /// </remarks>
    /// <param name="sourceVertex">The source Vertex.</param>
    /// <param name="targetVertex">The target Vertex.</param>
    /// <returns>
    /// Returns <c>true</c> only if an Edge was created.
    /// Returns <c>false</c> if an Edge already existed.
    /// </returns>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the sourceVertex is null.</exception>
    /// <exception cref="ArgumentNullException">An exception is raised if the targetVertex is null.</exception>
    bool AddEdge(TVertex sourceVertex, TVertex targetVertex);
	 */

	partial class GraphProviderBase 
	{
			/*
/// <summary>
		/// Removes the edges.
		/// </summary>
		/// <param name="count">The count of successful edges created.</param>
		/// <param name="edges">The edges definition.</param>
		/// <returns>
		/// the number of edges that have been removed
		/// + the number of edges that didn't exists on method call time
		/// </returns>
		virtual public bool RemoveEdges(out int count, params GraphEdgeDef[] edges) {

			count = 0;
			foreach (GraphEdgeDef e in-edges) {
				if (RemoveEdge(e.SourceId, e.TargetId)) {
					++count;
				}
			}
			return (count == edges.Length);
		}
		 * 
		 * 
		 */
	}
}
