This is the base abstract Provider from which all GraphProviders
enherit. See DbProvider.



OLIVIER: Should GraphMRUItemList be a propery of DbProvider or GraphProviderBase...

OLIVIER: This is totally wrong: vertex.Data.DataSource... should be vertex.DataSource
because 
a) In the long run, GraphVertex engine should be pushed towards Generic DataObjects, not just
derivatives of DataObject
b) the DataObject should be just that: the data record...not extra methods that are 
really part of Graph engine.





DEPENDENCIES:
There is a System.Web.dll due to the need to find AppDir property under all 3 major platforms.
I thought of doing it via reflection to not have that dep...but it would load it in mem anyway
so might as well have it known up front.