﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Timers;

using XAct.Core.Generics;
using XAct.Core.Utils;

namespace XAct.Graphs {
	abstract public partial class GraphProviderBase 
	{



		#region Fields - InMem Graph GraphVertex and GraphEdge Caches

		/// <summary>
		/// The in.mem vertex collection.
		/// The vertex are sorted by their last used (MRU collection)
		/// </summary>
		protected GraphMRUVertexList _CachedAllVertices = new GraphMRUVertexList();



		int _EdgeCountInMemory = 0;


		/// <summary>
		/// Protected Dictionary of GraphVertex to a collection of its in.Edge instances.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that the dictionaries are ID based -- not the whole Vertex.
		/// </para>
		/// </remarks>
		protected GraphVertexIdEdgeDictionary
			_CachedVertexInEdges = new GraphVertexIdEdgeDictionary();

		/// <summary>
		/// Protected Dictionary of GraphVertex to a collection of its out.Edge instances.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that the dictionaries are ID based -- not the whole Vertex.
		/// </para>
		/// </remarks>
		protected GraphVertexIdEdgeDictionary
			_CachedVertexOutEdges = new GraphVertexIdEdgeDictionary();


		/* Removed stuffs */

		/// <summary>
		/// Internal cache of recently removed GraphVertex elements that have not yet
		/// been removed from the local storage.
		/// </summary>
		/// <remarks>
		/// <para>
		/// <see cref="T:Vertex"/> elements are removed from 
		/// <see cref="_CachedAllVertices"/> and moved to this collection
		/// by the <see cref="M:RemoveVertex"/>.
		/// </para>
		/// </remarks>
		protected GraphMRUVertexList _CachedDeletePendingVertices = new GraphMRUVertexList();

		/// <summary>
		/// where we store recently removed edges  
		/// </summary>
		protected GraphEdgeList _CachedDeletePendingEdges = new GraphEdgeList();

		#endregion

		#region Fields - InMem Graph Cleanup Thread related


		/// <summary>
		///     timer thread who launch *periodically* the cleanup routine
		/// </summary>
		protected Timer _CleanupTimer;

		/// <summary>
		/// Gets the interval (in Seconds) between Cleanup sweeps by the Cleanup thread.
		/// Default: 5 Minutes.
		/// </summary>
		/// <value>The cleanup interval (in seconds).</value>
		public int CleanupInterval {
			get {
				return _CleanupInterval;
			}
		}
		private int _CleanupInterval = 5 * 60;


		/// <summary>
		/// The Minimum TimeSpan for an unused GraphVertex before we 
		/// deam it as 'stale' and ready for cleanup.
		/// Default: 20 minutes. 
		/// </summary>
		public TimeSpan CleanupVertexTimeout {
			get {
				return _cleanupVertexTimeout;
			}
		}
		private TimeSpan _cleanupVertexTimeout = new TimeSpan(0, 20, 0);

		/// <summary>
		/// The minimum number of GraphVertex elements to allow in cache before 
		/// we procceed with looking for 'stale' (see <see cref="CleanupVertexTimeout"/>) GraphVertex elements. 
		/// Default: 500 GraphVertex elements.
		/// </summary>
		public int CleanupVertexCountThreshold {
			get {
				return _CleanupVertexCountThreshold;
			}
		}
		private int _CleanupVertexCountThreshold = 500;

		#endregion

		#region Fields - InMem Graph Flush thread related


		/// <summary>
		/// Timer thread which launches 
		/// periodically the <see cref="OnFlushEvent"/>.
		/// </summary>
		protected Timer _FlushTimer;

		/// <summary>
		/// Gets the interval (in Seconds) between Flush sweeps by the Flush thread.
		/// Default: 2 Minutes.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The Flush thread scans all vertices and edges at regular intervals
		/// and persists to local storage any that have changes not yet recorded. 
		/// </para>
		/// </remarks>
		/// <value>The Flush interval (in seconds).</value>
		public int FlushInterval {
			get {
				return _FlushInterval;
			}
			set {
				_FlushInterval = value;
				if (_FlushTimer != null) {
					_FlushTimer.Interval = (_FlushInterval * 1000);
				}
			}
		}
		private int _FlushInterval = 30;

		#endregion



		#region Properties - InMem Graph Health

		/// <summary>
		/// Gets a value indicating whether to update changes to local storage in real time.
		/// <para>
		/// Default value is <c>true</c>.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// <code>
		/// <![CDATA[
		/// 
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		/// <value><c>true</c> if update in real time; otherwise, <c>false</c>.</value>
		public bool UpdateInRealTime {
			get {
				return _UpdateInRealTime;
			}
		}
		private bool _UpdateInRealTime = true;

		/// <summary>
		/// Gets the number of active Vertex elements in memory.
		/// </summary>
		/// <value>The vertices in memory count.</value>
		public int VerticesInMemory {
			get {
				return _CachedAllVertices.Count;
			}
		}

    /// <summary>
		/// Gets the number of Edge elements in memory.
		/// <para>
		/// Which is not the same as <see cref="M:QuickGraph.IEdgeListGraph{GraphVertex, GraphEdge}.EdgeCount"/>.
		/// </para>
    /// </summary>
		/// <remarks>
		/// <para>
		/// Most graphs algorythms that deal with the edge count will want to know
		/// about all the edges in the graph -- not just the ones in memory.
		/// </para>
		/// </remarks>
    /// <value>The edges in memory count.</value>
    public int EdgeCountInMemory {
      get {
				return _EdgeCountInMemory;
      }
    }

		/// <summary>
		/// Gets the edges in memory.
		/// </summary>
		/// <value>The edges in memory.</value>
		public IEnumerable<GraphEdge> EdgesInMemory {
			get {
				//TODO:
				//To do this properly, we will need to filter the diff
				//between In and Out, or keep a global cache of all edges...
				return null;
				/* 
		 * 
						foreach (EdgeList edges in this.vertexOutEdges.Values)
								foreach (GraphEdge edge in edges)
										yield return edge;
				}
		 */
			}
		}

		#endregion



		#region Protected Methods - InMem Initialize Timer
		/// <summary>
		/// Initializes the timer.
		/// </summary>
		protected void InitializeTimer() {
			_FlushTimer = new Timer();
			_FlushTimer.Elapsed += OnFlushEvent;
			_FlushTimer.Interval = _FlushInterval * 1000;
			_FlushTimer.Enabled = true;
		}
		#endregion

		#region Protected Methods - InMem GraphVertex Manipulation

		/// <summary>
		/// Gets the vertex from in-memory cache.
		/// <para>
		/// Returns null if not found.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// No error if vertex not found -- returns null.
		/// </para>
		/// <para>
		/// Invoked by <see cref="GetVertex"/>.
		/// If the result is null, <see cref="GetVertex"/>
		/// then calls <see cref="storageVertexGet"/>,
		/// in order to find it in the persistent storage mechanism.
		/// </para>
		/// </remarks>
		/// <param name="vertexId">The vertex's unique Id.</param>
		/// <returns>
		/// Return the vertex with the specified id,
		/// or null if the vertex doesn't exist.
		/// </returns>
		virtual protected GraphVertex inMemGetVertex(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}
			GraphVertex vertex;
			//Note: By using TryFind(), if found, node is automatically "ascended"
			_CachedAllVertices.TryFind(vertexId, out vertex);
			return vertex;
		}

		/// <summary>
		/// Gets the vertices already in the
		///  in-memory cache with the specified data source id
		/// and data object 
		/// </summary>
		/// <param name="dataObjectFactoryId">The data source id.</param>
		/// <param name="dataObjectIds">The object </param>
		/// <param name="resultCollection">A collection of vertices to append to (may be zero size, but never null).</param>
		/// <param name="missingDataObjectIds">Result collection of all Record Ids not found.</param>
		/// <returns>True if all vertices found.</returns>
		virtual protected bool inMemGetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds, ref GraphVertexList resultCollection, out ICollection<string> missingDataObjectIds) {
			//Check Args:
			if (dataObjectFactoryId == 0) {
				throw new System.ArgumentNullException("dataObjectFactoryId");
			}
			if (dataObjectIds == null) {
				throw new System.ArgumentNullException("dataObjectIds");
			}
			if (resultCollection == null) {
				throw new System.ArgumentNullException("vertexCollection");
			}

			//Get the collection from in memory:
			foreach (GraphVertex vertex in _CachedAllVertices.Items) {
				if (vertex.DataSourceId == dataObjectFactoryId && dataObjectIds.Contains(vertex.DataRecordId)) {
					resultCollection.Add(vertex);
				}
			}


			//IMPORTANT:
			// on ne peut pas faire le Ascend dans le precedent foreach
			// parce que on ne peut pas modifier la collection qu'on est en train
			// de parcourir
			foreach (GraphVertex vertex in resultCollection) {
				_CachedAllVertices.Ascend(vertex.Id);
			}



			//Iterate through the collection of vertices that were 
			//found in memory, and extract a list of 
			//just their vertex recordId's:
			List<string> foundRecordIds = new List<string>(XAct.Core.Utils.Collections.Map<GraphVertex, string>(resultCollection,
				delegate(GraphVertex v) {
					return v.DataRecordId;
				}));


			//Filter the dataObjectIds collection *argument* 
			//for all that were not found in memory:
			missingDataObjectIds =
				new List<string>(
				Collections.Filter<string>(dataObjectIds,
				delegate(string id) {
					return !foundRecordIds.Contains(id);
				}));

			//Return flag as to whether all were found or not:
			return (new List<string>(missingDataObjectIds).Count == 0);

		}

		/// <summary>
		/// Gets the vertices withe the specified vertex ids from the in memory cache.
		/// </summary>
		/// <param name="vertexIds">The vertex ids to search for.</param>
		/// <param name="resultCollection">A collection of vertices to append to (may be zero size, but never null).</param>
		/// <param name="missingVertexIds">The ids of the vertices not found.</param>
		/// <returns>True if all vertices found.</returns>
		virtual protected bool inMemGetVertices(ICollection<Guid> vertexIds, ref GraphVertexList resultCollection, out ICollection<Guid> missingVertexIds) {
			if (vertexIds == null) {
				throw new System.ArgumentNullException("dataObjectIds");
			}
			if (resultCollection == null) {
				throw new System.ArgumentNullException("resultCollection");
			}


			lock (this) {
				missingVertexIds = new List<Guid>();

				foreach (Guid vertexId in vertexIds) {
					bool found = false;
					foreach (GraphVertex vertex in _CachedAllVertices.Items) {
						if (vertex.Id.Equals(vertexId)) {
							resultCollection.Add(vertex);
							found = true;
							break;
						}
					}
					if (!found) {
						missingVertexIds.Add(vertexId);
					}
				}
			}
			return (missingVertexIds.Count == 0);
		}


		/// <summary>
		/// Add the vertex to the in memory GraphVertex cache.
		/// NOTE: If the vertex, or a duplicate vertex (that has the same Id), 
		/// is already in memory, it will return this cached vertex. 
		/// For this reason, the returned GraphVertex *may* be a different GraphVertex than the one
		/// submitted.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="storageVertexGet(Guid)"/>
		/// and by 
		/// <see cref="storageGetVertices(int, ICollection{string} , ref GraphVertexList , out ICollection{string} )"/>
		/// </para>
		/// </remarks>
		/// <param name="vertex">The vertex to add.</param>
		/// <returns>The Cached GraphVertex (which may be different than the argument).</returns>
		virtual protected GraphVertex inMemAddVertex(GraphVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//Check, add if not in the cache already,
			//and return GraphVertex that is in the cache:
			//Which may or may not be the same vertex...
			vertex = inMemInsertOrUpdateCache(vertex);

			//Cache the out-edges for this Vertex:
			GraphEdgeList outEdges = storageVertexGetOutEdges(vertex);
			_CachedVertexOutEdges[vertex.Id] = outEdges;
		
			//DELETE THIS: _CachedAllEdges.AddRange(outEdges);

			//Cache the in-edges for this Vertex:
			GraphVertexList tmpVertexIdList = new GraphVertexList();
			tmpVertexIdList.Add(vertex);

			GraphEdgeList inEdges = storageVertexGetInEdges(tmpVertexIdList);
			_CachedVertexInEdges[vertex.Id] = inEdges;

			//DELETE THIS: _CachedAllEdges.AddRange(inEdges);

			return vertex;
		}

		/// <summary>
		/// Inserts the given vertex in cache if not already present
		/// or update it (i.e. put it at the top of MRU list) if already in cache.
		/// returns the vertex in cache.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <returns></returns>
		virtual protected GraphVertex inMemInsertOrUpdateCache(GraphVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			GraphVertex vInCache;
			//Note: By using TryFind(), if found, node is automatically "ascended"
			if (!_CachedAllVertices.TryFind(vertex.Id, out vInCache)) { // not in cache
				_CachedAllVertices.Add(vertex);
				vInCache = vertex;
				traceVerbose("vertex {0} not in cache", vertex.Id);
			}
			else {
				traceVerbose("vertex {0} already in cache", vertex.Id);
			}

			return vInCache;
		}

		/// <summary>
		/// Returns true if the vertex with the specified Id is currently loaded in cache.
		/// </summary>
		/// <param name="vertexId">The vertex Id.</param>
		/// <returns>
		/// 	<c>true</c> if [is in cache] [the specified vertex Id]; otherwise, <c>false</c>.
		/// </returns>
		virtual protected bool inMemIsVertexInCache(Guid vertexId) {
			//Check Args:
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}
			GraphVertex vertex;
			//Note: By using TryFind(), if found, node is automatically "ascended"
			_CachedAllVertices.TryFind(vertexId, out vertex);
			return (vertex!=null);
		}

		/// <summary>
		/// Tries to release deleted and old/long dormant vertices to 
		/// keep the in-memory graph from getting too big.
		/// </summary>
		/// <internal>
		/// <para>
		/// Invoked by <see cref="OnCleanupEvent"/>.
		/// </para>
		/// <para>
		/// OLIVIER: This should maybe be an abstract set of Stubs in Provider
		/// CleanupVertices(){....
		/// bool CleanupVerticesInternal()
		/// </para>
		/// </internal>
		virtual protected void inMemReleasePendingOperations() {

			lock (this) {

				//Calc cutoff point:
				DateTime cutOffStamp = DateTime.Now - _cleanupVertexTimeout;

				//--------------------------------------------------
				//Step: Dump Vertices that have been deleted from memory:
				while ((_CachedDeletePendingVertices.Count) > 0) {
					//Peek (ie, get handle on, without removing) the Last Used vertex:
					GraphTimeStampedVertex timeStampedItem = _CachedDeletePendingVertices.PeekLastUsed();

					//If the tv's stamp is *later* than the 
					//threshold, then we stop peeking/removing
					//(since we are always getting the LastUsed):
					if (timeStampedItem.Stamp > cutOffStamp) {
						break;
					}

					//We did not exit, so GraphVertex is 
					//an '*old* vertex that is to be removed
					//GraphMRUItemVertexList, and be flushed first:
					GraphVertex vertex = (GraphVertex)timeStampedItem.Item;
					Flush(vertex);
					_CachedDeletePendingVertices.Remove(vertex.Id);

					traceVerbose("Deleted GraphVertex released [Id={0}].", vertex.Id);
				}
				//--------------------------------------------------
				//Step: Dump *all* Deleted Edges
				//IMPORTANT: Notice that we are not checking Time here...
				//OLIVIER: Why?
				foreach (GraphEdge edge in _CachedDeletePendingEdges) {
					Flush(edge);
					//_CachedDeletePendingEdges.Remove(edge);//Correct, but slow...
				}
				_CachedDeletePendingEdges.Clear();//Faster.

				//--------------------------------------------------
				//Step: Dump dormant active Vertices:
				while ((_CachedAllVertices.Count > _CleanupVertexCountThreshold)) {
					//Peek (ie, get handle on, without removing) the Last Used vertex:
					GraphTimeStampedVertex timeStampedItem = _CachedAllVertices.PeekLastUsed();

					//If the tv's stamp is *later* than the 
					//threshold, then we stop peeking/removing
					//(since we are always getting the LastUsed):
					if (timeStampedItem.Stamp > cutOffStamp) {
						break;
					}

					//Raise an event *before* deleting the 
					//Vertex from memory, so that the operation can be cancelled
					//if someone still needs the vertex:
					CancelVertexEventArgs arg = new CancelVertexEventArgs(timeStampedItem.Item);
					OnVertexReleasing(arg);

					if (arg.Cancel) {
						// cleanup canceled, put the vertex in top of the cache
						traceVerbose("Vertex cleanup canceled");
						_CachedAllVertices.Ascend(timeStampedItem.Item.Id);
						//Move on to next Last Used Vertex:
						continue;
					}

					//We did not exit, so GraphVertex is 
					//an '*old* vertex that is to be removed
					//GraphMRUItemVertexList, and be flushed first:
					GraphVertex vertex = timeStampedItem.Item;

					//In case there are any changes, do them just before dumping:
					Flush(vertex);
					//Ok. We've persisted any 
					//changes...let's dump it:
					inMemReleaseVertex(vertex.Id);

					//Raise an event after we are finished:
					OnVertexReleased(new GuidEventArgs(vertex.Id));

					traceVerbose("Vertex released [Id={0}].", vertex.Id);
				}

				//--------------------------------------------------
				//Step: Dump dormant Edges:
				//These are not dumped on their own -- we dump the vertices
				//which dumped the edges.
				//--------------------------------------------------
			}//End:Lock
		}



		/// <summary>
		/// Releases the specified vertex from memory, if it is in memory.
		/// <para>
		/// Does NOT delete the Vertex.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="inMemReleasePendingOperations"/>.
		/// </para>
		/// </remarks>
		/// <param name="vertexId">The vertex Id.</param>
		virtual protected void inMemReleaseVertex(Guid vertexId) {

			if (_CachedAllVertices.Contains(vertexId)) {
				_CachedVertexInEdges.Remove(vertexId);
				_CachedVertexOutEdges.Remove(vertexId);
				_CachedAllVertices.Remove(vertexId);
			}

		}

		/// <summary>
		/// Mark the edges adjacent to this GraphVertex as deleted (in.mem).
		/// i.e. set their sync status to <c>LocalDeletionPending|RemoteDeletionPending</c>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invoked by <see cref="RemoveVertex(GraphVertex)"/>.
		/// </para>
		/// </remarks>
		/// <param name="vertex">The vertex</param>
		virtual protected void inMemDeleteAdjacentEdges(GraphVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			inMemDeleteInEdges(vertex);
			inMemDeleteOutEdges(vertex);

		}


		/// <summary>
		/// Delete the in-edges from in memory.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		virtual protected void inMemDeleteInEdges(GraphVertex vertex) {

			_CachedDeletePendingEdges.AddRange(_CachedVertexInEdges[vertex.Id]);
			_CachedVertexInEdges[vertex.Id].Clear();
		}

		/// <summary>
		/// Ins the out-edges from the memory cache.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		virtual protected void inMemDeleteOutEdges(GraphVertex vertex) {
			_CachedDeletePendingEdges.AddRange(_CachedVertexOutEdges[vertex.Id]);
			_CachedVertexOutEdges[vertex.Id].Clear();
		}

		#endregion



		#region Protected - Event Handlers - Cleanup/Flush thread event handlers
		/// <summary>
		/// Event handler called periodically by the graph's Flush thread.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
		protected void OnFlushEvent(object source, ElapsedEventArgs e) {
			traceVerbose("OnFlush Event Begin [now={0}].", DateTime.Now);
			Flush();
			traceVerbose("OnFlush Event End [now={0}]", DateTime.Now);
		}


		/// <summary>
		/// Event Handler Called periodically by the graph cleanup thread.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
		protected void OnCleanupEvent(object source, ElapsedEventArgs e) {
			traceVerbose("OnCleanup Started [now={0}].", DateTime.Now);
			inMemReleasePendingOperations();
			

			traceVerbose("OnCleanup Complete [now={0}].", DateTime.Now);
		}
		#endregion



	}
}
