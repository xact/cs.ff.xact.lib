﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace XAct.Graphs {

		internal static class GraphContracts {
			/// <summary>
			/// Asserts the specified value is true.
			/// If false, throws an error.
			/// </summary>
			/// <param name="value">if set to <c>true</c> [value].</param>
			[Conditional("DEBUG")]
			public static void Assert(bool value) {
				if (!value)
					throw new InvalidOperationException();
			}

			/// <summary>
			/// Asserts the specified value is true.
			/// If false, throws an error.
			/// </summary>
			/// <param name="value">if set to <c>true</c> [value].</param>
			/// <param name="message">The message.</param>
			[Conditional("DEBUG")]
			public static void Assert(bool value, string message) {
				if (!value)
					throw new InvalidOperationException(message);
			}

			[Conditional("DEBUG")]
			public static void AssumeNotNull<T>(T v, string parameterName) {
				if (object.Equals(v, null))
					throw new ArgumentNullException(parameterName);
			}

			/// <summary>
			/// Assert check to ensure the given Vertex is in the given Vertices set.
			/// </summary>
			/// <param name="g"></param>
			/// <param name="v">The vertex.</param>
			/// <param name="parameterName"></param>
			[Conditional("DEBUG")]
			public static void AssumeInVertexSet(
					QuickGraph.IVertexSet<GraphVertex> g,
					GraphVertex v,
					string parameterName) {
				AssumeNotNull(g, "g");
				AssumeNotNull(v, parameterName);
				if (!g.ContainsVertex(v))
					throw new QuickGraph.VertexNotFoundException(parameterName);
			}

			/// <summary>
			/// Assert check to ensure the vertex is not in the given set.
			/// </summary>
			/// <param name="g"></param>
			/// <param name="v">The vertex.</param>
			/// <param name="parameterName"></param>
			[Conditional("DEBUG")]
			public static void AssumeNotInVertexSet(
					QuickGraph.IVertexSet<GraphVertex> g,
					GraphVertex v,
					string parameterName) {
				AssumeNotNull(g, "g");
				AssumeNotNull(v, parameterName);
				if (g.ContainsVertex(v))
					throw new ArgumentException("vertex already in set", parameterName);
			}


			[Conditional("DEBUG")]
			public static void AssumeInVertexSet(
					QuickGraph.IVertexAndEdgeSet<GraphVertex, GraphEdge> g,
					GraphEdge e,
					string parameterName){
				AssumeNotNull(g, "g");
				AssumeNotNull(e, parameterName);
				AssumeInVertexSet(g, e.Source, parameterName + ".Source");
				AssumeInVertexSet(g, e.Target, parameterName + ".Target");
			}

			[Conditional("DEBUG")]
			public static void AssumeInEdgeSet(
					QuickGraph.IVertexAndEdgeSet<GraphVertex, GraphEdge> g,
					GraphEdge e,
					string parameterName){
				AssumeInVertexSet(g, e, parameterName);
				if (!g.ContainsEdge(e))
					throw new QuickGraph.EdgeNotFoundException(parameterName);
			}

		}
}
