﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs {

	/// <summary>
	/// A depth first search algorithm for undirected graphs
	/// </summary>
	/// <reference-ref
	///     idref="gross98graphtheory"
	///     chapter="4.2"
	///     />
	public class DepthFirstUndirectedSearchAlgorythm :

		QuickGraph.Algorithms.RootedAlgorithmBase<GraphVertex,
		QuickGraph.IUndirectedGraph<GraphVertex, GraphEdge>>,
			QuickGraph.Algorithms.IVertexColorizerAlgorithm<GraphVertex, GraphEdge>,
			QuickGraph.Algorithms.IVertexPredecessorRecorderAlgorithm<GraphVertex, GraphEdge>,
			QuickGraph.Algorithms.IVertexTimeStamperAlgorithm<GraphVertex, GraphEdge>,
			QuickGraph.Algorithms.ITreeBuilderAlgorithm<GraphVertex, GraphEdge> {

		#region Events Raised
		/// <summary>
		/// Event raised prior to the root node being Visited.
		/// </summary>
		public event QuickGraph.VertexEventHandler<GraphVertex> StartVertex;
		/// <summary>
		/// Event raised when a Vertex has been visited, and marked as Gray, but prior to investigating its Edges.
		/// </summary>
		public event QuickGraph.VertexEventHandler<GraphVertex> DiscoverVertex;
		/// <summary>
		/// Event raised when investigating a visited vertex's edges, prior to traversing it to another Vertex.
		/// </summary>
		public event QuickGraph.EdgeEventHandler<GraphVertex, GraphEdge> ExamineEdge;
		/// <summary>
		/// Event raised when traversing a visited node to another node.
		/// </summary>
		public event QuickGraph.EdgeEventHandler<GraphVertex, GraphEdge> TreeEdge;
		/// <summary>
		/// Event raised when encountering a Gray colored vertex.
		/// </summary>
		public event QuickGraph.EdgeEventHandler<GraphVertex, GraphEdge> BackEdge;
		/// <summary>
		/// Event raised when encountering a Black colored vertex.
		/// </summary>
		public event QuickGraph.EdgeEventHandler<GraphVertex, GraphEdge> ForwardOrCrossEdge;
		/// <summary>
		/// Event raised when a Vertex has been visited in full, and is colored as Black.
		/// </summary>
		public event QuickGraph.VertexEventHandler<GraphVertex> FinishVertex;
		#endregion




		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="DepthFirstUndirectedSearchAlgorythm"/> class.
		/// </summary>
		/// <param name="graph">The graph.</param>
		/// <param name="rootVertexStartingPoint">The root vertex starting point.</param>
		/// <param name="maxDepth">The max depth.</param>
		public DepthFirstUndirectedSearchAlgorythm(QuickGraph.IUndirectedGraph<GraphVertex, GraphEdge> graph, GraphVertex rootVertexStartingPoint, int maxDepth) :
			this(graph, new Dictionary<GraphVertex, QuickGraph.GraphColor>()) {

			if (rootVertexStartingPoint == null) {
				throw new System.ArgumentNullException("rootVertexStartingPoint");
			}
			if (maxDepth == 0) {
				throw new System.ArgumentNullException("maxDepth");
			}

			base.RootVertex = rootVertexStartingPoint;
			_MaxDepth = maxDepth;

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DepthFirstUndirectedSearchAlgorythm"/> class.
		/// </summary>
		/// <param name="visitedGraph">The visited graph.</param>
		/// <param name="_Colors">The _ colors.</param>
		public DepthFirstUndirectedSearchAlgorythm(
				QuickGraph.IUndirectedGraph<GraphVertex, GraphEdge> visitedGraph,
				IDictionary<GraphVertex, QuickGraph.GraphColor> _Colors
				)
			: base(visitedGraph) {

			//Check Args:
			if (_Colors == null) {
				throw new System.ArgumentNullException("VertexColors");
			}


			this._Colors = _Colors;
		}
		#endregion


		#region Properties

		/// <summary>
		/// Gets the dictionary of visited vertices, and their visited/non-visited colors.
		/// </summary>
		/// <value>The vertex colors.</value>
		public IDictionary<GraphVertex, QuickGraph.GraphColor> VertexColors {
			get {
				return this._Colors;
			}
		}
		private IDictionary<GraphVertex, QuickGraph.GraphColor> _Colors;

		/// <summary>
		/// Gets or sets the max depth to search the graph.
		/// </summary>
		/// <value>The max depth.</value>
		public int MaxDepth {
			get {
				return this._MaxDepth;
			}
			set {
				this._MaxDepth = value;
			}
		}
		private int _MaxDepth = int.MaxValue;
		#endregion

		#region Public Methods
		/// <summary>
		/// Resets the color of each vertex to white.
		/// </summary>
		public void Initialize() {
			VertexColors.Clear();
		}

		#endregion


		#region Protected Methods - Raise Events

		/// <summary>
		/// Raised the <see cref="StartVertex"/> event.
		/// Prior to visiting the <c>RootVertex</c>.
		/// </summary>
		/// <param name="v">The v.</param>
		private void OnStartVertex(GraphVertex v) {
			if (StartVertex != null) {
				StartVertex(this, new QuickGraph.VertexEventArgs<GraphVertex>(v));
			}
		}

		/// <summary>
		/// <summary>
		/// Raised the <see cref="DiscoverVertex"/> event.
		/// </summary>
		/// </summary>
		/// <param name="v">The v.</param>
		private void OnDiscoverVertex(GraphVertex v) {
			if (DiscoverVertex != null)
				DiscoverVertex(this, new QuickGraph.VertexEventArgs<GraphVertex>(v));
		}

		/// <summary>
		/// Raised the <see cref="ExamineEdge"/> event.
		/// Event raise when we are examining an Edge as to whether we will be traversing it.
		/// </summary>
		/// <param name="e">The e.</param>
		private void OnExamineEdge(GraphEdge e) {
			if (ExamineEdge != null)
				ExamineEdge(this, new QuickGraph.EdgeEventArgs<GraphVertex, GraphEdge>(e));
		}

		/// <summary>
		/// Raised the <see cref="TreeEdge"/> event.
		/// Event raised when we are traversing/using an edge to get to a new vertex.
		/// </summary>
		/// <param name="e">The e.</param>
		private void OnTreeEdge(GraphEdge e) {
			if (TreeEdge != null)
				TreeEdge(this, new QuickGraph.EdgeEventArgs<GraphVertex, GraphEdge>(e));
		}

		/// <summary>
		/// Raised the <see cref="BackEdge"/> event.
		/// Event raised when an in or out vertex is grey.
		/// </summary>
		/// <param name="e">The e.</param>
		private void OnBackEdge(GraphEdge e) {
			if (BackEdge != null)
				BackEdge(this, new QuickGraph.EdgeEventArgs<GraphVertex, GraphEdge>(e));
		}

		/// <summary>
		/// Raised the <see cref="ForwardOrCrossEdge"/> event.
		/// Event raised when a in or out vertex is black (ie we've looped).
		/// </summary>
		/// <param name="e">The e.</param>
		private void OnForwardOrCrossEdge(GraphEdge e) {
			if (ForwardOrCrossEdge != null)
				ForwardOrCrossEdge(this, new QuickGraph.EdgeEventArgs<GraphVertex, GraphEdge>(e));
		}

		/// <summary>
		/// Raised the <see cref="FinishVertex"/> event.
		/// Event raised when a Vertex and its children have all been visited, and 
		/// is colored as Black.
		/// </summary>
		/// <param name="v">The v.</param>
		private void OnFinishVertex(GraphVertex v) {
			if (FinishVertex != null)
				FinishVertex(this, new QuickGraph.VertexEventArgs<GraphVertex>(v));
		}
		#endregion


		#region Protected Methods
		/// <summary>
		/// The main computation of the algorythm.
		/// </summary>
		protected override void InternalCompute() {
			// put all vertex to white
			Initialize();

			// if there is a starting vertex, start whith him:
			if (this.RootVertex != null) {
				OnStartVertex(this.RootVertex);
				Visit(this.RootVertex, 0);
			}

		}
		void Visit(GraphVertex vertex, int depth) {
			if (this.IsAborting) {
				return;
			}
			if (vertex == null) {
				throw new ArgumentNullException("vertex");
			}
			if (depth > this._MaxDepth) {
				return;
			}

			//Mark it as visited:
			_Colors[vertex] = QuickGraph.GraphColor.Gray;

			//And raise event:
			OnDiscoverVertex(vertex);

			//Make a null...
			GraphVertex nextVertex = default(GraphVertex);

			//Go through edges that are adjacent to this vertex (in and out):
			foreach (GraphEdge e in VisitedGraph.AdjacentEdges(vertex)) {

				if (this.IsAborting) {
					return;
				}

				//Raise event:
				OnExamineEdge(e);

				if (vertex.Equals(e.Source)) {
					//if it is an Out edge, next nextVertex is the edge target:
					nextVertex = e.Target;
				}
				else {
					//if it is an In edge, next nextVertex is the edge source:
					nextVertex = e.Source;
				}

				//Get the color of the next nextVertex:
				QuickGraph.GraphColor nextVertexColor = _Colors[nextVertex];

				if (nextVertexColor == QuickGraph.GraphColor.White) {
					//Never seen this vertex yet:
					OnTreeEdge(e);
					//Recurse deeper:
					Visit(nextVertex, depth + 1);
				}
				else if (nextVertexColor == QuickGraph.GraphColor.Gray) {
					//Already been here:
					OnBackEdge(e);
				}
				else {
					//Is Black...
					OnForwardOrCrossEdge(e);
				}
			}

			//Mark the vertex as fully investigated.
			VertexColors[vertex] = QuickGraph.GraphColor.Black;

			//Raise event that we have Blacked this vertex:
			OnFinishVertex(vertex);
		}
		#endregion

	}//Class:End
}

