﻿

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// Enumeration of the default edges of a graph.
	/// </summary>
	public enum GraphMLDefaultEdgeDirection {
		/// <summary>
		/// The graph's edge's are, by default, directed.
		/// </summary>
		directed,
		/// <summary>
		/// The graph's edge's are, by default, undirected.
		/// </summary>
		undirected
	}
}
