﻿

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// Enumeration of the various types a Key value can be in.
	/// </summary>
	public enum GraphMLKeyDataType {
		/// <summary>
		/// A boolean.
		/// </summary>
		Gboolean,
		/// <summary>
		/// An integer.
		/// </summary>
		Gint,
		/// <summary>
		/// A long.
		/// </summary>
		Glong,
		/// <summary>
		/// A float.
		/// </summary>
		Gfloat,
		/// <summary>
		/// A double.
		/// </summary>
		Gdouble,
		/// <summary>
		/// A string.
		/// </summary>
		Gstring
	}
}
