﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs.Serialization {
	/// <summary>
	/// Enumeration of what a Key is defined for.
	/// </summary>
	public enum GraphMLKeyFor {
		/// <summary>
		/// The Key can be used on all elements.
		/// </summary>
		all,
		/// <summary>
		/// The Key is defined for a graph element.
		/// </summary>
		graph,
		/// <summary>
		/// The Key is defined for a node element.
		/// </summary>
		node,
		/// <summary>
		/// The Key is defined for an edge element.
		/// </summary>
		edge,
	}
}
