﻿using System;
using XAct.Collections;

namespace XAct.Graphs
{
    /// <summary>
    /// The Cache structure used by the InMem Graph engine.
    /// </summary>
    public class InMemGraphCacheCollections<TId>
    {
        /// <summary>
        /// The in.mem vertex collection.
        /// The vertex are sorted by their last used (MRU collection)
        /// </summary>
        public MRUList<IGraphVertex<TId>, TId> AllVertices;

        /// <summary>
        /// Protected Dictionary of GraphVertex to a collection of its in.Edge instances.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that the dictionaries are ID based -- not the whole Vertex.
        /// </para>
        /// </remarks>
        public  GraphVertexIdEdgeDictionary<TId> VertexInEdges;

        /// <summary>
        /// Protected Dictionary of GraphVertex to a collection of its out.Edge instances.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that the dictionaries are ID based -- not the whole Vertex.
        /// </para>
        /// </remarks>
        public GraphVertexIdEdgeDictionary<TId> VertexOutEdges;


        /* Removed stuffs */

        /// <summary>
        /// Internal cache of recently removed GraphVertex elements that have not yet
        /// been removed from the local storage.
        /// </summary>
        /// <remarks>
        /// <para>
        /// <see cref="IGraphVertex{TId}"/> elements are removed from 
        /// <see cref="_Cache.AllVertices"/> and moved to this collection
        /// by the <see cref="IVertexService{TId}.RemoveVertex"/>.
        /// </para>
        /// </remarks>
        public MRUList<IGraphVertex<TId>, TId> DeletePendingVertices;

        /// <summary>
        /// where we store recently removed edges  
        /// </summary>
        public IGraphEdgeList<TId> DeletePendingEdges;


        public InMemGraphCacheCollections(Func<IGraphVertex<TId>,TId> anonymousMethodToAccessVertexIdentifier)
        {
            AllVertices = new MRUList<IGraphVertex<TId>, TId>(anonymousMethodToAccessVertexIdentifier);
            DeletePendingVertices = new MRUList<IGraphVertex<TId>, TId>(anonymousMethodToAccessVertexIdentifier);
            DeletePendingEdges = new IGraphEdgeList<TId>();
            VertexOutEdges = new GraphVertexIdEdgeDictionary<TId>();
            VertexInEdges = new GraphVertexIdEdgeDictionary<TId>();

        }
    }
}