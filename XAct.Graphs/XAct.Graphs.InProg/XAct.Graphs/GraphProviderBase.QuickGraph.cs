﻿using System;
using System.Collections.Generic;

using QuickGraph;
using XAct.Domain;


namespace XAct.Graphs {
	abstract public partial class GraphProviderBase <TId>
	{


		#region Events Raised - Implementation of QuickGraph.IMutableEdgeListGraph)
		/// <summary>
		/// Event raised when an edge is added to the graph.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		public event QuickGraph.EdgeEventHandler<IGraphVertex<TId>, IGraphEdge<TId>> EdgeAdded;

		/// <summary>
		/// Event raised when an edge is removed from the graph.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		public event QuickGraph.EdgeEventHandler<IGraphVertex<TId>, IGraphEdge<TId>> EdgeRemoved;
		#endregion

		#region Events Raised - Implementation of QuickGraph.IMutableVertexListGraph
		/// <summary>
		/// Event raised when a vertex is added to the graph.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		public event QuickGraph.VertexEventHandler<IGraphVertex<TId>> VertexAdded;

        /// <summary>
		/// Event raised when a vertex is removed from the graph.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		public event QuickGraph.VertexEventHandler<IGraphVertex<TId>> VertexRemoved;
		#endregion





		#region Properties
		/// <summary>
		/// Gets the System.Type of the vertex.
		/// </summary>
		/// <value>The type of the vertex.</value>
		public Type VertexType {
			get {
				return typeof(IGraphVertex<TId>);
			}
		}

		/// <summary>
		/// Gets the System.Type of the edge.
		/// </summary>
		/// <value>The type of the edge.</value>
		public Type EdgeType {
			get {
				return typeof(IGraphEdge<TId>);
			}
		}
		#endregion


		#region Properties - Implementation of QuickGraph.IVertexSet<IGraphVertex<TId>, IGraphEdge<TId>>
		/// <summary>
		/// Gets a value indicating whether this instance is vertices empty.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is vertices empty; otherwise, <c>false</c>.
		/// </value>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IVertexSet{TVertex}"/>
		/// </para>
		/// </remarks>
		virtual public bool IsVerticesEmpty {
			get {
				return (_Cache.AllVertices.Count) == 0;
			}
		}

		/// <summary>
		/// Returns the number of Vertices in the Graph.
		/// </summary>
		/// <value></value>
		/// <remarks>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IVertexSet{TVertex}"/>
		/// </remarks>
		virtual public int VertexCount {
			get {
				
				return _Cache.AllVertices.Count;
			}
		}

		/// <summary>
		/// Returns an enumarable collection of the vertices in the graph.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IVertexSet{TVertex}"/>
		/// </para>
		/// </remarks>
		virtual public IEnumerable<IGraphVertex<TId>> Vertices {
			get {
				foreach (GraphTimeStampedVertex tsVertex in _Cache.AllVertices) {
					yield return tsVertex.Item;
				}
			}
		}
		#endregion

		#region Properties - Implementation of QuickGraph.IGraph<IGraphVertex<TId>, IGraphEdge<TId>>

		/// <summary>
		/// Gets a value indicating whether the graph is a directed graph.
		/// </summary>
		/// <value></value>
		/// <remarks>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </remarks>
		virtual public bool IsDirected {
			get {
				return true;
			}
		}

		/// <summary>
		/// Gets a value indicating whether a graph can contain more 
		/// than one route between two vertices.
		/// </summary>
		/// <value></value>
		/// <remarks>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </remarks>
		virtual public bool AllowParallelEdges {
			get {
				return false;
			}
		}

		#endregion

		#region Properties - Implementation of QuickGraph.IMutableVertexListGraph<IGraphVertex<TId>, IGraphEdge<TId>>
		//abstract public 
		//VertexAdded
		//VertexRemoved
		#endregion

		#region Methods - Implementation of QuickGraph.IVertexFactory<IGraphVertex<TId>>
		/// <summary>
		/// Creates the vertex.
		/// <para>
		/// IMPORTANT: 
		/// But does not add it to the graph unil AddVertex() is invoked on it.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IVertexFactory{TVertex}"/>.
		/// </para>
		/// </remarks>
		/// <returns></returns>
		public IGraphVertex<TId> CreateVertex() {
			return new IGraphVertex<TId>();
		}
		#endregion

		#region Methods - Implementation of QuickGraph.IMutableIncidenceGraph<IGraphVertex<TId>, IGraphEdge<TId>


		/// <summary>
		/// Remove the specified out edge if the predicate is met.
		/// </summary>
		/// <param name="vertex">The vertex</param>
		/// <param name="predicate">The predicate to match.</param>
		/// <returns></returns>
		/// <remarks>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableIncidenceGraph{TVertex, IGraphEdge{TId}}"/>
		/// </remarks>
		virtual public int RemoveOutEdgeIf(IGraphVertex<TId> vertex, QuickGraph.EdgePredicate<IGraphVertex<TId>, IGraphEdge<TId>> predicate) {

			GraphContracts.AssumeInVertexSet(this, vertex, "vertex");
			if (predicate.IsNull()) {throw new ArgumentNullException("predicate");}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:

			//Make a tmp array:
			IGraphEdgeList<TId> edgesToRemove = new IGraphEdgeList<TId>();

			//Because we have a vertex, it means that GetVertex() was used 
			//at some point, which means we have all its out edges in memory
			//which this includes all its out-edges in storage as well...
			//So. Good. We don't have to load up any edges to do this operation.

			//Loop through out-edges of vertex:
			foreach (IGraphEdge<TId> edge in _Cache.VertexOutEdges[vertex.Id]) {
				//Looking for edges that meet given condition:
				if (predicate(edge))
					edgesToRemove.Add(edge);
			}

			//Loop through all selected edges:
			foreach (IGraphEdge<TId> edge in edgesToRemove) {
				RemoveEdge(edge);
			}
			return edgesToRemove.Count;
		}

		/// <summary>
		/// Removes the OutEdges leading away from the given Vertex.
		/// <para>
		/// In other words, Moves the Edge to the <see cref="_Cache.DeletePendingEdges"/>
		/// array to be deleted from the datastore on the next <see cref="Flush()"/>.
		/// </para>
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <remarks>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableIncidenceGraph{TVertex, IGraphEdge{TId}}"/>
		/// </remarks>
		virtual public void ClearOutEdges(IGraphVertex<TId> vertex) {
			
			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			foreach (IGraphEdge<TId> edge in _Cache.VertexOutEdges[vertex.Id]) {
				RemoveEdge(edge);
			}

		}

		#endregion

		#region Methods - Implementation of QuickGraph.IIncidenceGraph<IGraphVertex<TId>, IGraphEdge<TId>>

		/// <summary>
		/// Determines whether the Graph contains an Edge between the Source and Target vertex.
		/// <para>
		/// IMPORTANT: Comparison is done by sourceId and targetId -- not by Edge or Vertex entity. 
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IIncidenceGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		/// <param name="source">The source vertex.</param>
		/// <param name="target">The target vertex.</param>
		/// <returns>
		/// 	<c>true</c> if the edge contains an edge between the two vertices; otherwise, <c>false</c>.
		/// </returns>
		public bool ContainsEdge(IGraphVertex<TId> source, IGraphVertex<TId> target) {

			IGraphEdge<TId> foundEdge;
			return TryGetEdge(source, target, out foundEdge);
		}


		
		/// <summary>
		/// Tries to get all edges between the source and target vertex.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="target">The target.</param>
		/// <param name="edges">The found Edges.</param>
		/// <returns>
		/// 	<c>true</c> if the graph contains an edge between the source and target; otherwise, <c>false</c>.
		/// </returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IIncidenceGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		public bool TryGetEdges(
					IGraphVertex<TId> source,
					IGraphVertex<TId> target,
					out IEnumerable<IGraphEdge<TId>> edges) {

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:

			//And if we have a target, it too has been mounted via GetVertex.
			//So it has an in-edges array we can safely address:

			
			//If there is an edge between the two, it will be in both the source and target's arrays.

			IGraphEdgeList<TId> results = new IGraphEdgeList<TId>();
			foreach(IGraphEdge<TId> edge in _Cache.VertexOutEdges[source.Id]){
				if (edge.TargetId == target.Id){
					results.Add(edge);
				}
			}

			edges = results;

			//We do not need to go through In-Edges of Target, because in this 
			//case, we know that Source has been loaded, so we only need to 
			//work with Source's out targets to get to an edge between the two...
			return (results.Count > 0);


		}
		#endregion

		#region Abstract Methods - Implementation of QuickGraph.IImplicitGraph<IGraphVertex<TId>, IGraphEdge<TId>>
		/// <summary>
		/// Determines whether the Vertex has no out-edges.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <returns>
		/// 	<c>true</c> if the vertex has no Out-Edges.
		/// </returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IImplicitGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		public bool IsOutEdgesEmpty(IGraphVertex<TId> vertex) {

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			return (_Cache.VertexOutEdges[vertex.Id].Count == 0);

		}


		/// <summary>
		/// Returns the number of Out Edges.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IImplicitGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		/// <param name="vertex">The Vertex.</param>
		/// <returns>Count of OutEdges (zero or more).</returns>
		/// <exception cref="ArgumentNullException">An exception is raised if the Vertex is null.</exception>
		public int OutDegree(IGraphVertex<TId> vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}
			
			//vertex = GetVertex(vertex);
			
			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			return _Cache.VertexOutEdges[vertex.Id].Count;

			//Note:
			//Although the code is identical, and could be combined, 
			//we avoid calling OutDegree(Guid), 
			//in order to skip a call to GetVertex(id);
		}


		/// <summary>
		/// Returns an enumeration of the out-edges of the Vertex with the specified Id.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IImplicitGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		/// <param name="vertex">The Vertex.</param>
		/// <returns>An enumerable collection of Edge instances (zero-size or more, unless exception raised).</returns>
		/// <exception cref="ArgumentNullException">An exception is raised if the Vertex is null.</exception>
		public IEnumerable<IGraphEdge<TId>> OutEdges(IGraphVertex<TId> vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			return _Cache.VertexOutEdges[vertex.Id];
			
			//return OutEdges(vertex.Id);
		}


		/// <summary>
		/// Gets the [n]th Out-Edge of the Vertex.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IImplicitGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// <para>
		/// Will throw an out of range exception if index is not within the vertex's edge count.
		/// </para>
		/// </remarks>
		/// <param name="vertex">The vertex.</param>
		/// <param name="index">The index.</param>
		/// <returns></returns>
		public IGraphEdge<TId> OutEdge(IGraphVertex<TId> vertex, int index) {
			IGraphEdgeList<TId> edges = _Cache.VertexOutEdges[vertex.Id];
			if (index >= edges.Count){
				throw new ArgumentOutOfRangeException ("index");
			}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			return _Cache.VertexOutEdges[vertex.Id][index];
		}
		#endregion

		#region Methods - Implementation of QuickGraph.IBiDirectionalGraph<IGraphVertex<TId>, IGraphEdge<TId>>
		/// <summary>
		/// Determines whether a vertex has no in-edges.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IBidirectionalGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		/// <param name="vertex">The vertex.</param>
		/// <returns>True if there the vertex has no in-edges.</returns>
		public bool IsInEdgesEmpty(IGraphVertex<TId> vertex) {

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an in-edges array we can safely address:
			return (_Cache.VertexInEdges[vertex.Id].Count == 0);
		}

		/// <summary>
		/// Returns the number of in-edges to the given Vertex.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IBidirectionalGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		/// <param name="vertex">The vertex.</param>
		/// <returns>The number of in-edges.</returns>
		public int InDegree(IGraphVertex<TId> vertex) {
            ////Check Args:
            //if (vertex == null) {
            //    throw new System.ArgumentNullException("vertex");
            //}

            ////vertex = GetVertex(vertex);

            ////If we have a source vertex, it has been mounted via GetVertex.
            ////So it has an in-edges array we can safely address:
            //return _Cache.VertexOutEdges[vertex.Id].Count;
		}


		/// <summary>
		/// Enumerates through the vertex's in-edges.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IBidirectionalGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		/// <param name="vertex">The vertex.</param>
		/// <returns>An enumeration of edges.</returns>
		public IEnumerable<IGraphEdge<TId>> InEdges(IGraphVertex<TId> vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an in-edges array we can safely address:
			return _Cache.VertexInEdges[vertex.Id];

		}


		/// <summary>
		/// Returns the [n]th in-edge to the Vertex.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <param name="index">The position of the edge to look for.</param>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IBidirectionalGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		/// <returns>The found edge.</returns>
		public IGraphEdge<TId> InEdge(IGraphVertex<TId> vertex, int index) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			IGraphEdgeList<TId> edges = _Cache.VertexOutEdges[vertex.Id];
			if (index >= edges.Count) {
				throw new ArgumentOutOfRangeException("index");
			}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an in-edges array we can safely address:
			return _Cache.VertexInEdges[vertex.Id][index];
		}

		/// <summary>
		/// Returns the number of Degrees for the Vertex (includes in-edges and out-edges).
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IBidirectionalGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		/// <returns>The sum of in and out edges coming and going to the vertex.</returns>
		public int Degree(IGraphVertex<TId> vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			return _Cache.VertexOutEdges[vertex.Id].Count + _Cache.VertexInEdges[vertex.Id].Count;
		}
		#endregion

		#region Methods - Implementation of QuickGraph.IMutableBidirectionalGraph<IGraphVertex<TId>, IGraphEdge<TId>>

		/// <summary>
		/// Remove in-edges pointing to the given vertex, if the predicate is met.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <param name="edgePredicate">The predicate to match.</param>
		/// <returns></returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableBidirectionalGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		public int RemoveInEdgeIf(IGraphVertex<TId> vertex, EdgePredicate<IGraphVertex<TId>, IGraphEdge<TId>> edgePredicate) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:
			throw new NotImplementedException();
		}

		/// <summary>
		/// Removes in-edges pointing to the given vertex.
		/// <para>
		/// In other words, Moves the Edge to the <see cref="_Cache.DeletePendingEdges"/>
		/// array to be deleted from the datastore on the next <see cref="Flush()"/>.
		/// </para>
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableBidirectionalGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		public void ClearInEdges(IGraphVertex<TId> vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an in-edges array we can safely address...

			//But we have to clone/copy it because you can't delete and change the size of the array
			//while in a foreach loop:
			IGraphEdgeList<TId> tmpCopy = new IGraphEdgeList<TId>();
			tmpCopy.AddRange(_Cache.VertexInEdges[vertex.Id]);
			
			foreach (IGraphEdge<TId> edge in tmpCopy) {
				//We do each one individually in order to 
				//trigger cancellable events on each edge:
				RemoveEdge(edge);
			}

			_Cache.VertexInEdges[vertex.Id].Clear();

		}

		/// <summary>
		/// Removes (deletes) the in-edges and out-edges pointing to the given vertex.
		/// <para>
		/// In other words, Moves the Edges to the <see cref="_Cache.DeletePendingEdges"/>
		/// array to be deleted from the datastore on the next <see cref="Flush()"/>.
		/// </para>
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableBidirectionalGraph{TVertex, IGraphEdge{TId}}"/>
		/// </para>
		/// </remarks>
		public void ClearEdges(IGraphVertex<TId> vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			//vertex = GetVertex(vertex);

			//If we have a source vertex, it has been mounted via GetVertex.
			//So it has an out-edges array we can safely address:

			ClearInEdges(vertex);
			ClearOutEdges(vertex);
		}
		#endregion

		//ContainsVertex

		#region Abstract Methods - Implementation of QuickGraph.IMutableVertexAndEdgeListGraph<IGraphVertex<TId>, IGraphEdge<TId>>
		/// <summary>
		/// 
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableVertexAndEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		/// <param name="e"></param>
		/// <returns></returns>
		public bool AddVerticesAndEdge(IGraphEdge<TId> e) {

				throw new NotImplementedException();
		}
		#endregion

		#region Abstract Methods - Implementation of QuickGraph.IVertexSet<IGraphVertex<TId>, IGraphEdge<TId>>

		/// <summary>
		/// Determines if the graph contains the specified vertex.
		/// <para>
		/// Note that this is checked by Id.
		/// </para>
		/// </summary>
		/// <param name="vertex"></param>
		/// <returns></returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IVertexSet{TVertex}"/>
		/// </para>
		/// </remarks>
		public bool ContainsVertex(IGraphVertex<TId> vertex) {
			IGraphVertex<TId> vertex2;
			//If it is found, then it exists...
			return (TryGetVertex(vertex.Id, out vertex2));
		}
		#endregion

		#region Abstract Methods - Implementation of QuickGraph.IMutableGraph<IGraphVertex<TId>, IGraphEdge<TId>>
		/// <summary>
		/// Clears (Deletes) this graph of all edges and vertices.
		/// </summary>
		/// <remarks>Implementation of a method/property defined in <see cref="QuickGraph.IMutableGraph{TVertex, IGraphEdge{TId}}"/></remarks>
		public void Clear() {

			//Commit all pending changes first:
				Flush();
				//Then clear out from memory caches:
				_Cache.AllVertices.Clear();
				_Cache.VertexInEdges.Clear();
				_Cache.VertexOutEdges.Clear();

				//VERY IMPORTANT: WE ONLY CLEAR FROM MEMORY -- NOT FROM HARD DRIVE.
			//JUST TOO DAMN DANGEROUS I THINK.
		}
		#endregion

		#region Abstract Methods - IMplementation of QuickGraph.IEdgeFactory<IGraphVertex<TId>, IGraphEdge<TId>>
		/// <summary>
		/// Create a new Edge between the specified Source and Target vertices.
		/// <para>
		/// IMPORTANT: The edge is created, but is not added to the graph until AddEdge(TEdge) is invoked.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IEdgeFactory{TVertex, TEdge}"/>.
		/// </para>
		/// </remarks>
		/// <param name="source">The source Vertex.</param>
		/// <param name="target">The target Vertex.</param>
		/// <returns></returns>
		public IGraphEdge<TId> CreateEdge(IGraphVertex<TId> source, IGraphVertex<TId> target) {
			//Check Args:
			//Check Args:
			if (source == null) {
				throw new System.ArgumentNullException("source");
			}
			if (target == null) {
				throw new System.ArgumentNullException("target");
			}

			if (source.Id == Guid.Empty) {
				throw new System.ArgumentNullException("sourceId");
			}
			if (target.Id == Guid.Empty) {
				throw new System.ArgumentNullException("targetId");
			}

			//Create a new edge:
			return new IGraphEdge<TId>(source.Id, target.Id);
		}
		#endregion

		#region Abstract Methods - Implementation of QuickGraph.IMutableEdgeListGraph<IGraphVertex<TId>, IGraphEdge<TId>>


		/// <summary>
		/// Remove an Edge from the graph if the predicate is met.
		/// </summary>
		/// <param name="predicate">The predicate to match.</param>
		/// <returns></returns>
		/// <remarks>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>
		/// </remarks>
		public int RemoveEdgeIf(QuickGraph.EdgePredicate<IGraphVertex<TId>, IGraphEdge<TId>> predicate) {
			GraphContracts.AssumeNotNull(predicate, "predicate");
			IGraphEdgeList<TId> edges = new IGraphEdgeList<TId>();

			//Because we have a Vertex, it must have been gotten through
			//a GetVertex() operation, which means that we also enherited
			//all its In and Out Edges. 
			//So. Good. It means we can safely assume we have all parts
			//without having to do an expensive hit on the hard-drive.

			//TODO: WARNING:
			//Except that this.Edges will cause an iteration through the whole
			//hard drive!

			//Make a tmp array to hold filtered edges:
			List<IGraphEdge<TId>> filteredEdges = new List<IGraphEdge<TId>>();

			//Look through all edges:
			//Which need only be the persisted OutEdges, as 
			//persisted InEdges are the same 
			//thing, from a different point of view:
			foreach (IGraphEdge<TId> edge in this.Edges) {
				if (predicate(edge)) {
					filteredEdges.Add(edge);
				}
			}

			//Now that we have a filtered subset:
			foreach (IGraphEdge<TId> edge in filteredEdges) {
				//we can remove them one at a time
				//which will raise events accordingly:
				this.RemoveEdge(edge);
			}
			return edges.Count;
		}

		#endregion

		#region Public Methods - Implementation of QuickGraph.IEdgeListGraph<IGraphVertex<TId>, IGraphEdge<TId>>
		/// <summary>
		/// Gets a value indicating whether this graph has no edges.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		/// <value>
		/// 	<c>true</c> if this instance is edges empty; otherwise, <c>false</c>.
		/// </value>
		public bool IsEdgesEmpty {
			get {
				//Use provided property 
				//(which will rely on storage method rather than in memory).
				return (this.EdgeCount == 0);
			}
		}

		/// <summary>
		/// Gets the number of edges in the graph 
		/// <para>
		/// Note that this is determined from what is on storage, 
		/// not just <see cref="EdgeCountInMemory"/>.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		/// <value>The edge count.</value>
		public int EdgeCount {
			get {
				//Use count from storage, not from Memory:
				return _graphRepository.storageEdgeCount();
			}
		}

		/// <summary>
		/// Gets all the edges in the graph.
		/// <para>
		/// WARNING: 
		/// </para>
		/// <para>
		/// This is an expensive operation, as it is an interaction
		/// of all edges in storage, not in memory.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// For iterating only what is in memory, see <see cref="EdgesInMemory"/>.
		/// </para>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		/// <value>The edges.</value>
		public IEnumerable<IGraphEdge<TId>> Edges {
			get {
				//In addition to being expensive in terms of iterating through
				//all records in Db, 
				//the objects returned are not the same instances of objects
				//already in memory, so they have to be hotswapped each and every one:
				//Holy shit.

				//TODO:
				//Maybe another way would be to 
				//a) lock the list
				//b) return those in memory
				//c) building a list of edge defs
				//d) ask for all on hard-drive that are not in list/in mem...
				//f) which would make for one big sql statement, but less slow in terms of hot-swapping.



                foreach (IGraphEdge<TId> edge in _graphRepository.storageEdgeEnumerateAll())
                {
						IGraphEdge<TId> returnedItem = edge;

						if (_Cache.VertexInEdges.ContainsKey(edge.TargetId)){
							IGraphEdge<TId> foundEdge = _Cache.VertexInEdges[edge.TargetId].Find(
														delegate(IGraphEdge<TId> e) {
															return e.SourceId == edge.SourceId;
														}
													);
							if (foundEdge != null) {
								returnedItem = foundEdge;
							}
						}
						else if (_Cache.VertexOutEdges.ContainsKey(edge.SourceId)) {
							IGraphEdge<TId> foundEdge = _Cache.VertexOutEdges[edge.SourceId].Find(
														delegate(IGraphEdge<TId> e) {
															return e.TargetId == edge.TargetId;
														}
													);
							if (foundEdge != null) {
								returnedItem = foundEdge;
							}
						}
						else {
							//Do no changes:
						}
						yield return returnedItem;
					}

			}
		}




		/// <summary>
		/// Determines whether the graph contains the specified edge.
		/// <para>
		/// IMPORTANT:
		/// Comparison is done by sourceId and targetId -- not by Edge or Vertex entity. 
		/// </para>
		/// </summary>
		/// <param name="edge">The edge.</param>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		/// <returns>
		/// 	<c>true</c> if the edge contains an edge between the two vertices; otherwise, <c>false</c>.
		/// </returns>
		public bool ContainsEdge(IGraphEdge<TId> edge) {
			//Invoke the OB solution
			//which will ensure the source gets mounted in memory if still on HD:
			return ContainsEdge(edge.Source, edge.Target);
		}

		#endregion

		#region Methods - Implementation of QuickGraph.IMutableVertexListGraph<IGraphVertex<TId>, IGraphEdge<TId>>
		/// <summary>
		/// Adds a range of vertexes to the graph. 
		/// </summary>
		/// <param name="vertices">The vertices to add.</param>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		public void AddVertexRange(IEnumerable<IGraphVertex<TId>> vertices) {
			//Check Args:
			GraphContracts.AssumeNotNull(vertices, "vertices");

			lock (this) {
				foreach (IGraphVertex<TId> vertex in vertices) {
					//Use nested method to add to all cache arrays,
					//And raise events:
					AddVertex(vertex);
				}
			}
		}


		/// <summary>
		/// Adds the given vertex to the graph.
		/// </summary>
		/// <param name="vertex">The vertex to add.</param>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		public void AddVertex(IGraphVertex<TId> vertex) {

            GraphContracts.AssumeNotNull(vertex, "vertex");

			if (((ISyncVertex)vertex).RetrievalStatus == GraphRetrievalStatus.Empty) {
				throw new System.ArgumentException("Cannot add an invalid Vertex (one whose RetrievalStatus == Empty) to the graph.");
			}
			
				//We don't add the same vertex twice:
			lock (this) {
				//Do not re-add the same vertex:
				if (this.ContainsVertex(vertex)) {
					return;
				}
					_Cache.AllVertices.Add(vertex);
					_Cache.VertexOutEdges[vertex.Id] = new IGraphEdgeList<TId>();
					_Cache.VertexInEdges[vertex.Id] = new IGraphEdgeList<TId>();
				}

			if (UpdateInRealTime) {
			//	Flush(vertex);
			}

			OnVertexAdded(new QuickGraph.VertexEventArgs<IGraphVertex<TId>>(vertex));
		}



		/// <summary>
		/// Removes the vertex if the predicate is met.
		/// <para>
		/// WARNING:
		/// </para>
		/// <para>
		/// Causes Iteration through all vertices in storage (not just in Mem).
		/// </para>
		/// </summary>
		/// <param name="predicate">The predicate.</param>
		/// <returns>Returns the number of edges removed.</returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// </remarks>
		public int RemoveVertexIf(QuickGraph.VertexPredicate<IGraphVertex<TId>> predicate) {
			//Check Args:
			GraphContracts.AssumeNotNull(predicate, "predicate");


			//Build a list to hold ref to vertices found that match predicate:
			IGraphVertexList verticesToRemove = new IGraphVertexList();

			//Loop through all vertices in graph:
			//WARNING:
			//Iterates through *all* vertices in Storage (NOT just InMem)
			foreach (IGraphVertex<TId> vertex in this.Vertices) {
				if (predicate(vertex)) {
					//And if condition met, add to tmp list:
					verticesToRemove.Add(vertex);
				}
			}

			//Now delete all those in tmp list:
			int removed = 0;
			foreach (IGraphVertex<TId> vertex in verticesToRemove) {
				//causing events,etc on each one:
				if (this.RemoveVertex(vertex)) {
					removed++;
				}
			}

			//Return the number of verticesToRemove removed.
			return removed;
		}


		/// <summary>
		/// Removes a vertex from the graph.
		/// <para>
		/// Raises <see cref="VertexRemoved"/> event.
		/// </para>
		/// </summary>
		/// <param name="vertex">The vertex to remove.</param>
		/// <returns></returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, IGraphEdge{TId}}"/>.
		/// </para>
		/// <para>
		/// Calls <see cref="OnVertexRemoving"/> prior to deletion,
		/// and <see cref="OnVertexRemoved(GraphVertexEventArgs)"/> after completion, if not cancelled.
		/// </para>
		/// </remarks>
		/// <internal>
		/// Not sure whether raising QuickGraph event is useful in any sense:
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the Vertex is null.</exception>
		public bool RemoveVertex(IGraphVertex<TId> vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}
			lock (this) {


				//Raise event before deleting Vertex:
				CancelVertexEventArgs e = new CancelVertexEventArgs(vertex);
				OnVertexRemoving(e);

				if (e.Cancel) {
					traceInformation("Vertex deletion canceled [id={0}].", vertex.Id);
					//Return false as operation cancelled:
					return false;
				}

				// 2. Set: state so that thread later persists/deletes from local storage:
				((ICachedElement)vertex).modelState |= OfflineModelState.DeletionPending;

				// 3. mark adjacent edges for deletion from memory:
				inMemDeleteAdjacentEdges(vertex);

				// 4. Remove IGraphVertex<TId> from *all* and move to *removed* collections:
				_Cache.AllVertices.Remove(vertex.Id);


				_Cache.DeletePendingVertices.Add(vertex);

				// 5. do not do local storage flush here
				if (UpdateInRealTime) {
					Flush(vertex);
				}
				//Raise an event after completed deleting vertex:
				OnVertexRemoved(new GraphVertexEventArgs(vertex));

			}

			//Raise a QuickGraph event as well? :
			GraphVertexEventArgs args = new GraphVertexEventArgs(vertex);
			OnVertexRemoved(args);

			return true;
		}


		#endregion
		#region Methods - Implementation of QuickGraph.IUndirectedGraph
		/// <summary>
		/// Enumerates throught the vertex's adjacent edges.
		/// </summary>
		/// <param name="v">The vertex.</param>
		/// <returns></returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IUndirectedGraph{TVertex, TEdge}"/>.
		/// </para>
		/// </remarks>
		public IEnumerable<IGraphEdge<TId>> AdjacentEdges(IGraphVertex<TId> v) {
			foreach(IGraphEdge<TId> edge in InEdges(v)){
				yield return edge;
			}
			foreach(IGraphEdge<TId> edge in InEdges(v)){
				yield return edge;
			}
		}

		/// <summary>
		/// Returns the number of adjacent edges to the given vertex.
		/// </summary>
		/// <param name="v">The vertex.</param>
		/// <returns></returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IUndirectedGraph{TVertex, TEdge}"/>.
		/// </para>
		/// </remarks>
		public int AdjacentDegree(IGraphVertex<TId> v) {
			return _Cache.VertexInEdges[v.Id].Count + _Cache.VertexOutEdges[v.Id].Count;
		}

		/// <summary>
		/// Determines if the given vertex has no adjacent edges..
		/// </summary>
		/// <param name="v">The vertex.</param>
		/// <returns>
		/// 	<c>true</c> if [is adjacent edges empty] [the specified v]; otherwise, <c>false</c>.
		/// </returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IUndirectedGraph{TVertex, TEdge}"/>.
		/// </para>
		/// </remarks>
		public bool IsAdjacentEdgesEmpty(IGraphVertex<TId> v) {
			return (AdjacentDegree(v) == 0);
		}


		/// <summary>
		/// Returns the [n]th adjacent edge.
		/// </summary>
		/// <param name="v">The vertex.</param>
		/// <param name="index">The index.</param>
		/// <returns></returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IUndirectedGraph{TVertex, TEdge}"/>.
		/// </para>
		/// </remarks>
		public IGraphEdge<TId> AdjacentEdge(IGraphVertex<TId> v, int index) {
			if (index < _Cache.VertexInEdges.Count) {
				return _Cache.VertexInEdges[v.Id][index];
			}
			index -= _Cache.VertexInEdges.Count;
			if (index < _Cache.VertexInEdges.Count){
				return _Cache.VertexOutEdges[v.Id][index];
			}
			throw new System.ArgumentException("index");
		}

		/*
		/// <summary>
		/// Determines whether the graph contains an edge between the source and target vertex.
		/// </summary>
		/// <param name="source">The source vertex.</param>
		/// <param name="target">The target vertex.</param>
		/// <returns>
		/// 	<c>true</c> if an edge was found..
		/// </returns>
		/// <remarks>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IUndirectedGraph{TVertex, TEdge}"/>.
		/// </para>
		/// </remarks>
		public bool ContainsEdge(IGraphVertex<TId> source, IGraphVertex<TId> target) {
			
			if (_Cache.VertexInEdges[source]
			throw new NotImplementedException();
		}
		//#region Methods - Implementation of QuickGraph.IIncidenceGraph<IGraphVertex<TId>, IGraphEdge<TId>>
		*/
		#endregion






	}
}
