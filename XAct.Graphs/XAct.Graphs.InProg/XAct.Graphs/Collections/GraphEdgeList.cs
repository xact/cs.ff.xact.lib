using System.Collections.Generic;

namespace XAct.Graphs {
  
  /// <summary>
  /// A list of edge instances.
  /// </summary>
  public class IGraphEdgeList<TId> : List<IGraphEdge<TId>> {

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="IGraphEdgeList{TId}"/> class.
		/// </summary>
		public IGraphEdgeList() {
		}

		/// <summary>
        /// Initializes a new instance of the <see cref="IGraphEdgeList{TId}"/> class.
		/// </summary>
		/// <param name="edges">The edges.</param>
		public IGraphEdgeList(params IGraphEdge<TId>[] edges){
			base.AddRange(edges);
		}
		#endregion
	}//Class:End
}//Namespace:End
