using System.Collections.Generic;

namespace XAct.Graphs {

  /// <summary>
  /// A list of vertex instances.
  /// </summary>
  public class IGraphVertexList : List<IGraphVertex> {
  }//Class:End
}//Namespace:End