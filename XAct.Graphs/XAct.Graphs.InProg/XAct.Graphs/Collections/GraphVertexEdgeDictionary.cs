﻿using System;
using System.Collections.Generic;

namespace XAct.Graphs {

	/// <summary>
	/// Dictionary of Vertexes Ids to Edges List (generally the Vertex's OutEdges).
	/// </summary>
	public class GraphVertexIdEdgeDictionary<TId>  : Dictionary<TId, IGraphEdgeList<TId>>
    {
	
    }
}
