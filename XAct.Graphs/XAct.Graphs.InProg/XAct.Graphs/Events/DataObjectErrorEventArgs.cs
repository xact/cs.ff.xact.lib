using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Graphs {

  /// <summary>
  /// Event arg associated with an dataObject and an exception.
  /// </summary>
  public class DataObjectErrorEventArgs : DataObjectEventArgs {


    #region Properties

    /// <summary>
    /// Gets the error.
    /// </summary>
    /// <value>The error.</value>
    public Exception Error {
      get {
        return _Error;
      }
    }
    private Exception _Error;


    /// <summary>
    /// Gets or sets a value indicating whether the failed operation
    /// should be retried.
    /// </summary>
    /// <value><c>true</c> if retry; otherwise, <c>false</c>.</value>
    public bool Retry {
      get {
        return _Retry;
      }
      set {
        _Retry = value;
      }
    }
    private bool _Retry;

    #endregion


    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DataObjectErrorEventArgs"/> class.
    /// </summary>
    /// <param name="dataObjectFactoryId">The Id of the DataObject Source.</param>
    /// <param name="dataObject">The dataObject.</param>
    /// <param name="error">The error.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the dataObject argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the error argument is null.</exception>
    public DataObjectErrorEventArgs(int dataObjectFactoryId, object dataObject, Exception error)
      : base(dataObjectFactoryId, dataObject) {
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }
      _Error = error;
      _Retry = false;
    }

    #endregion

  }//Class:End
}//Namespace:End