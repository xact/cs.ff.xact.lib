
namespace XAct.Graphs {

    
    /// <summary>
  /// Cancelable Event with a GraphVertex property
  /// </summary>
	public class CancelVertexEventArgs : GraphItemCancelEventArgsBase<IGraphVertex> {
    

    /// <summary>
    /// Gets the vertex.
    /// </summary>
    /// <value>The vertex.</value>
        public IGraphVertex Vertex
        {
            get
            {
                return _Item;
            }
        }

    /// <summary>
    /// Initializes a new instance of the <see cref="CancelVertexEventArgs"/> class.
    /// </summary>
    /// <param name="vertex">The vertex.</param>
		public CancelVertexEventArgs(IGraphVertex vertex)
			: base(vertex, false) {

    }
   

  }//Class:End
}//Namespace:End