﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs {
	public abstract class CancelEventArgs<T> : System.ComponentModel.CancelEventArgs {

		public T Item {
			get {
				return _Item;
			}
		}
		protected T _Item;

		public CancelEventArgs(T item) {
			_Item = item;
		}

	}
}
