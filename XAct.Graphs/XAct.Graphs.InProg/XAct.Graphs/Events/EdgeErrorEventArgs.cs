using System;

namespace XAct.Graphs {

  /// <summary>
  /// Event arg associated with an edge and an exception.
  /// </summary>
	public class EdgeErrorEventArgs : EdgeEventArgs {


#region Properties

    /// <summary>
    /// Gets the error.
    /// </summary>
    /// <value>The error.</value>
    public Exception Exception {
      get { return _exception; }
    }
    private readonly Exception _exception;


      /// <summary>
      /// Gets or sets a value indicating whether the failed operation
      /// should be retried.
      /// </summary>
      /// <value><c>true</c> if retry; otherwise, <c>false</c>.</value>
      public bool Retry { get; set; }

      #endregion



    /// <summary>
    /// Initializes a new instance of the <see cref="EdgeErrorEventArgs"/> class.
    /// </summary>
    /// <param name="edge">The edge.</param>
    /// <param name="exception">The error.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the edge argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the error argument is null.</exception>
		public EdgeErrorEventArgs(IGraphEdge edge, Exception exception)
      : base(edge) {

			_exception = exception;
      Retry = false;
    }


  }//Class:End
}//Namespace:End