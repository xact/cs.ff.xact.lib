
namespace XAct.Graphs {

  /// <summary>
  /// Event arg with an edge property
  /// </summary>
  public class EdgeEventArgs : GraphItemEventArgsBase<IGraphEdge> {

#region Properties
    /// <summary>
    /// Gets the edge.
    /// </summary>
    /// <value>The edge.</value>
		public IGraphEdge Edge {
      get {
        return Item;
      }
    }
#endregion

#region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="EdgeEventArgs"/> class.
    /// </summary>
    /// <param name="edge">The edge.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the edge argument is null.</exception>
		public EdgeEventArgs(IGraphEdge edge) : base(edge,false) {
    }
   
#endregion


        public IGraphEdge Item { get; set; }
  }//Class:End
}//Namespace:Endfc