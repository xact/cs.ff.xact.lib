using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Graphs {
  /// <summary>
  /// event arg with an Guid property
  /// </summary>
  public class GuidEventArgs : GraphItemEventArgsBase<Guid> {

#region Properties
    /// <summary>
    /// Gets the Id.
    /// </summary>
    /// <value>The Id.</value>
    public Guid Id {
      get {
				return _Item; 
      }
    }
    #endregion

#region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="T:GuidEventArgs"/> class.
    /// </summary>
    /// <param name="Id">The Id.</param>
    public GuidEventArgs(Guid Id) :base(Id,true){
    }
    #endregion

  }//Class:End
}//Namespace:End
