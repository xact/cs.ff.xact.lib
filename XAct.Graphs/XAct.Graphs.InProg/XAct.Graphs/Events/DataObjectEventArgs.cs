using System;
using System.Collections.Generic;
using System.Text;


namespace XAct.Graphs {

  /// <summary>
  /// Event Arg with a object property
  /// </summary>
  public class DataObjectEventArgs : GraphItemEventArgsBase<object> {

    #region Properties
		/// <summary>
		/// Gets the dataObject.
		/// </summary>
		/// <value>The dataObject.</value>
		public object DataObject {
			get {
				return _Item;
			}
		}
		
		/// <summary>
    /// Gets the data object source id.
    /// </summary>
    /// <value>The data object source id.</value>
    public int DataObjectFactoryId {
      get {
        return _DataObjectFactoryId;
      }
    }
    private int _DataObjectFactoryId;
    #endregion


    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DataObjectEventArgs"/> class.
    /// </summary>
    /// <param name="dataObjectFactoryId">Id of the dataObjectFactory.</param>
    /// <param name="dataObject">The dataObject.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the dataObject argument is null.</exception>
    public DataObjectEventArgs(int dataObjectFactoryId, object dataObject) :base(dataObject,false){
      _DataObjectFactoryId = dataObjectFactoryId;
      //TODO: _DataObjectId = dataObjectId;
    }

    #endregion

  }//Class:End
}//Namespace:End
