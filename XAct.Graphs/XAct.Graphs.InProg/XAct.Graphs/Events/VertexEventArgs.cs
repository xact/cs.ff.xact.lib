using System;
using System.Collections.Generic;
using System.Text;


namespace XAct.Graphs {

	/// <summary>
	/// Event Arguments for events relating to a Vertex.
	/// <para>
	/// Extends <see cref="QuickGraph.VertexEventArgs{GraphVertex}"/>.
	/// </para>
	/// </summary>
	public class VertexEventArgs : QuickGraph.VertexEventArgs<GraphVertex> 
		{

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="VertexEventArgs"/> class.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		public VertexEventArgs(GraphVertex vertex) : base(vertex){
			
		}
		#endregion
	}
}//Namespace:End
