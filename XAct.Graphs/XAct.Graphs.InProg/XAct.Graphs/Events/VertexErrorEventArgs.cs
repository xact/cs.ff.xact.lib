using System;
using System.Collections.Generic;
using System.Text;


namespace XAct.Graphs {

  /// <summary>
  /// Event Arg with a GraphVertex property and Error property.
  /// </summary>
  public class VertexErrorEventArgs : GraphVertexEventArgs {

    #region Properties


    /// <summary>
    /// Gets the error.
    /// </summary>
    /// <value>The error.</value>
    public Exception Error {
      get { return _Error; }
    }
    private Exception _Error;

    /// <summary>
    /// Gets or sets a value indicating whether the failed operation 
    /// should be retried.
    /// </summary>
    /// <value><c>true</c> if retry; otherwise, <c>false</c>.</value>
    public bool Retry {
      get { return _Retry; }
      set { _Retry = value;  }
    }
    private bool _Retry;

#endregion

    
#region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="T:GraphVertexEventArgs"/> class.
    /// </summary>
    /// <param name="vertex">The vertex.</param>
    /// <param name="error">The error.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the vertex argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the error argument is null.</exception>
    public VertexErrorEventArgs(GraphVertex vertex, Exception error)
      : base(vertex) {
      if (error == null) {
        throw new System.ArgumentNullException("error");
      }
      _Error = error;
      _Retry = false;
    }

#endregion

  }//Class:End
}//Namespace:End
