﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs {
	/// <summary>
	/// Event Args package pertaining to a Vertex.
	/// </summary>
	public class GraphVertexEventArgs : GraphItemEventArgsBase<GraphVertex> {

		/// <summary>
		/// Gets the vertex the event is about.
		/// </summary>
		/// <value>The vertex.</value>
		public GraphVertex Vertex {
			get {
				return _Item;
			}
		}


		/// <summary>
		/// Initializes a new instance of the <see cref="GraphVertexEventArgs"/> class.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		public GraphVertexEventArgs(GraphVertex vertex)
			: base(vertex, false) {
		}
	}
}
