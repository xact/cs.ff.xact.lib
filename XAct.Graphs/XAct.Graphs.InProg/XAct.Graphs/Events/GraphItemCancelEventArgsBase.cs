﻿using System;
using System.Collections.Generic;

namespace XAct.Graphs {

	/// <summary>
	/// Generic Cancellable EventArgs package.
	/// </summary>
	/// <typeparam name="TItem">The type of the item.</typeparam>
	public abstract class GraphItemCancelEventArgsBase<TItem> : EventArgs {

		/// <summary>
		/// Gets a value indicating if this event should be cancelled.
		/// </summary>
		public bool Cancel {
			get {
				return _cancel;
			}
			set {
				if (_cancel)
				{
                    //Already cancelled.
				    return;
				}
			    _cancel = value;
			}
		}
		private bool _cancel;

		/// <summary>
		/// The generic item this argument package is about.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Enherit from this class and do something like the following:
		/// <code>
		/// <![CDATA[
		/// public Vertex Vertex {
		///		get {
		///		return _Item;
		///		}
		/// }
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		protected TItem _Item;

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphItemEventArgsBase&lt;TItem&gt;"/> class.
		/// </summary>
		/// <param name="item">The item.</param>
		protected GraphItemCancelEventArgsBase(TItem item) :this(item,false){
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="GraphItemEventArgsBase&lt;TItem&gt;"/> class.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="allowDefaultValue">if set to <c>true</c> allow a null(default) item.</param>
		protected GraphItemCancelEventArgsBase(TItem item, bool allowDefaultValue) {
			if ((!allowDefaultValue) && (Comparer<TItem>.Default.Compare(item, default(TItem))==0)) {
				throw new ArgumentNullException("item");
			}
			_Item = item;
		}
		#endregion
	}
}
