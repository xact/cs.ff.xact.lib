

namespace XAct.Graphs {
  
  /// <summary>
    /// Cancelable event with an <see cref="IGraphEdge"/> property
  /// </summary>
	public class CancelEdgeEventArgs : GraphItemCancelEventArgsBase<IGraphEdge> {

    /// <summary>
    /// Gets the edge.
    /// </summary>
    /// <value>The edge.</value>
		public IGraphEdge Edge {get { return _Item; }}

    /// <summary>
    /// Initializes a new instance of the <see cref="CancelEdgeEventArgs"/> class.
    /// </summary>
    /// <param name="edge">The edge.</param>
		public CancelEdgeEventArgs(IGraphEdge edge)
			: base(edge, false) {
    }
  }//Class:End
}//Namespace:End