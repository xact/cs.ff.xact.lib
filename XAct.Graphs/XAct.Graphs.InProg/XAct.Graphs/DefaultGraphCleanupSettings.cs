﻿
namespace XAct.Graphs
{
    public class DefaultGraphCleanupSettings : IGraphCleanupSettings
    {
        public int CleanupInterval { get; set; }
        public int CleanupMinVertices { get; set; }
        public int CleanupTimeOut { get; set; }
        public int FlushInterval { get; set; }

        public DefaultGraphCleanupSettings ()
        {
            this.CleanupInterval = 10;
            this.CleanupMinVertices = 0;
            this.CleanupTimeOut = 20;
            this.FlushInterval = 60;
        }
    }
}
