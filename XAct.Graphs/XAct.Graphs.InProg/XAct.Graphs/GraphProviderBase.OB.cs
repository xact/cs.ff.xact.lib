﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics; 

using System.Timers;

using XAct.Diagnostics;
using XAct.Domain;

namespace XAct.Graphs {
	abstract public partial class GraphProviderBase : IGraphService
	{

	    private readonly ITracingService _tracingService;
	    private readonly IGraphRepository _graphRepository;

        public GraphProviderBase(ITracingService tracingService, IGraphRepository graphRepository)
        {
            _tracingService = tracingService;
            _graphRepository = graphRepository;
        }

	    #region Public Methods - Implementation of ProviderBase

		/// <summary>
		/// Initializes the Graph Engine.
		/// called by Manager/Provider mechanic.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the
		/// provider-specific attributes specified in the configuration for this provider.</param>
		public override void Initialize(string name, NameValueCollection config) {


			//WARNING *don't instantiate DataObjectFactoryCollection here*
			// because the graph engine is not fully constructed.
			// do lazy data source initialisation instead

#if ZERO

      _DataSourceCollection = new DataObjectFactoryCollection(this);
#endif



			_CachedAllVertices = new GraphMRUVertexList();

			_CachedVertexOutEdges = new GraphVertexIdEdgeDictionary();


			/* cleanup stuffs */

			_CleanupTimer = new Timer();
			_CleanupTimer.Elapsed += OnCleanupEvent;
			_CleanupTimer.Interval = this.CleanupInterval * 1000;
			_CleanupTimer.Enabled = true;


			/* Flush stuffs */


			_FlushTimer = new Timer();
			_FlushTimer.Elapsed += OnFlushEvent;
			_FlushTimer.Interval = _FlushInterval * 1000;
			_FlushTimer.Enabled = true;


			/* everything is initialized */

			traceInformation(
@"GraphDbCachedProvider initialised. 
cleanup interval = {0}, minVertices = {1} timeout = {2}
flush period = {3}",
											 this.CleanupInterval, this.CleanupVertexCountThreshold, this.CleanupVertexTimeout, this.FlushInterval);
		}

		#endregion




		#region Public Methods - Implementation of GraphProviderBase - GraphVertex Attributes
		/// <summary>
		/// For Internal use only.
		/// Initializes a Vertex's User Attributes.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Intended to only be called by <see cref="IGraphVertex"/>'s Indexer.
		/// </para>
		/// </remarks>
		/// <param name="vertex">The GraphVertex whose user attributes are to be initialized.</param>
		/// <returns>True if attributes were found.</returns>
		/// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
		public bool InitializeUserAttributes(GraphVertex vertex) {
			return _graphRepository.storageVertexUserAttributesLoad(vertex);
		}
		#endregion

	



		#region Public Methods - Implementation of GraphProviderBase - Flushing

		/// <summary>
		/// Flush everything which is not sync with local storage
		/// </summary>
		/// <remarks>
		/// <para>
		/// Periodically called by the Flush Thread to persist
		/// any changes to the underlying local datastore.
		/// </para>
		/// </remarks>
		public void Flush() {

			lock (this) {

				//#warning "this is where we need to optimise SQL statement"

				// then persist changes to deleted in-memory vertices:
				Flush(_CachedDeletePendingVertices.Items);

				// then persist changes to removed edges:
				Flush(_CachedDeletePendingEdges);

				// then persist changes to active in-memory vertices:
				Flush(_CachedAllVertices.Items);

				// then persist changes to edges:
				foreach (System.Collections.Generic.KeyValuePair<Guid,IGraphEdgeList> pair in _CachedVertexInEdges) {
					Flush(pair.Value);
				}

				foreach (System.Collections.Generic.KeyValuePair<Guid, IGraphEdgeList> pair in _CachedVertexOutEdges) {
					Flush(pair.Value);
				}

			}
		}



		/// <summary>
		/// Flush the vertices if not in sync with local storage.
		/// </summary>
		/// <param name="vertexList">Enumerable list of GraphVertex elements.</param>
		/// <exception cref="System.ArgumentNullException">Exception raised if argument is null.</exception>
		protected void Flush(IEnumerable<GraphVertex> vertexList) {
			//Check Args:
			if (vertexList == null) {
				throw new System.ArgumentNullException("vertexList");
			}

			List<Guid> verticesToMarkForDeletion = new List<Guid>();
			List<Guid> verticesToDelete = new List<Guid>();
			IGraphVertexList verticesManipulated = new IGraphVertexList();
			IGraphVertexList verticesToCreate = new IGraphVertexList();
			IGraphVertexList verticesToUpdate = new IGraphVertexList();

			foreach (GraphVertex vertex in vertexList) {
				if ((((IModelState)vertex).ModelState & OfflineModelState.LocalPending) == 0) {
					//Vertex has already been persisted to local storage
					//so there is nothing to do:
					continue;
				}
				//------------------------------------
				lock (this) {

					if ((((IModelState)vertex).ModelState & OfflineModelState.LocalDeletionPending) != 0) {
						//The vertex was marked for local deleting...
						//IMPORTANT:
						//This supercedes any LocalCreatePending or LocalUpdatePending flags that may 
						//have been set as well.
						if ((((IModelState)vertex).ModelState & OfflineModelState.RemoteDeletionPending) != 0) {
							//...and is marked for remote deletion as well, 
							//so we can't delete it yet (or we would lose this information upon local shutdown).
							//ie: this is a serious deletion (not just a local cleanup)...
							//So mark the GraphVertex *record* as needing to be deleted from from remote db.
							//(does not delete from local storage yet):
							verticesToMarkForDeletion.Add(vertex.Id);
							//This means that the vertex record .Deleted flag is 1, and therefore filters
							//it out from GetVertex requests etc...
						}
						else {
							//This is a local delete only (ie a local release of storage resources).
							//And does not have to worry about remote deletion being completed...
							//So just delete locally and we're done:
							verticesToDelete.Add(vertex.Id);
						}

					}
					else if ((((IModelState)vertex).ModelState & OfflineModelState.LocalCreationPending) != 0) {
						//This vertex was created in memory, but 
						//was not persisted to local storage yet...
						//So create the new record:
						verticesToCreate.Add(vertex);

						//IMPORTANT:
						//Note that Create persists the same information 
						//as Update, so there is no reason to worry about the Update afterwards...
					}
					else {
						//This vertex has been persisted to local storage at some point
						//in the past, (ie was not marked as LocalCreationPending),
						//but changes have been made since then...
						//Update the record in local storage:
						Debug.Assert((((IModelState)vertex).ModelState & OfflineModelState.LocalUpdatePending) != 0);
						verticesToUpdate.Add(vertex);
					}
					verticesManipulated.Add(vertex);
				}//~lock
			}//~foreach
			//------------------------------------
		 _graphRepository.storageVertexRecordMarkForDeletion(verticesToMarkForDeletion);
         _graphRepository.storageVertexRecordDelete(verticesToDelete);
         _graphRepository.storageVertexRecordCreate(verticesToCreate);
         _graphRepository.storageVertexRecordUpdate(verticesToUpdate);
			//------------------------------------
			foreach (GraphVertex vertex in verticesManipulated) {
				//IMPORTANT:
				//Note that 'storageVertexRecordCreate' and 'storageVertexRecordUpdate'
				//only *sent* flags that had RemotePending still up, but
				//both methods had not modified the actual values of the Vertex.
				//This was so that if they failed, one had nothing to rollback here.
				//On the other hand, one must not forget to update the flags
				//in the GraphVertex to match what was written to the Db....
				//So....
				//Wrap up by updating the in-memory GraphVertex modelState, leaving only 
				//the RemotePending flag up if already up (which it will be):
				((IModelState)vertex).ModelState &= OfflineModelState.RemotePending;
			}
			//------------------------------------
		}



		/// <summary>
		/// Flush/Persist the in-memory Edges to the local storage.
		/// </summary>
		/// <param name="edgeList">Enumerable list of GraphEdge elements.</param>
		/// <exception cref="System.ArgumentNullException">Exception raised if argument is null.</exception>
		protected void Flush(IEnumerable<GraphEdge> edgeList) {
			//Check Args:
			if (edgeList == null) {
				throw new System.ArgumentNullException("edgeList");
			}

			IGraphEdgeList edgesManipulated = new IGraphEdgeList();
			IGraphEdgeList edgesToMarkForDeletion = new IGraphEdgeList();
			IGraphEdgeList edgesToDelete = new IGraphEdgeList();
			IGraphEdgeList edgesToCreate = new IGraphEdgeList();
			IGraphEdgeList edgesToUpdate = new IGraphEdgeList();


			//throw new Exception("The method or operation is not implemented.");
			foreach (GraphEdge edge in edgeList) {
				if ((((IModelState)edge).ModelState & OfflineModelState.LocalPending) == 0) {
					//No changes...move on to next one...
					continue;
				}

			
				lock (this) {

					if ((((IModelState)edge).ModelState & OfflineModelState.LocalDeletionPending) != 0) {
						//The edge was marked for local deleting...
						if ((((IModelState)edge).ModelState & OfflineModelState.RemoteDeletionPending) != 0) {
							//...and is marked for remote deletion as well, 
							//then this is a serious deletion (not just a local cleanup)...
							//So mark the record as needing to be deleted from from remote db.
							//(does not delete from local storage yet):
							edgesToMarkForDeletion.Add(edge);
						}
						else {
							//This is a local delete only (ie a local release of storage resources).
							//And does not have to worry about remote deletion being completed...
							//So just delete locally and we're done:
							edgesToDelete.Add(edge);
						}

					}
					else if ((((IModelState)edge).ModelState & OfflineModelState.LocalCreationPending) != 0) {
						//This vertex was created in memory, but 
						//was not persisted to local storage yet...
						//So create the new record:
						edgesToCreate.Add(edge);

					}
					else {
						//This vertex has been persisted to local storage at some point
						//in the past, but changes have been made...
						//Update the record in local storage:
						Debug.Assert((((IModelState)edge).ModelState & OfflineModelState.LocalUpdatePending) != 0);
						edgesToUpdate.Add(edge);
					}
					edgesManipulated.Add(edge);
				}//~lock
			}//~foreach
			//------------------------------------
            _graphRepository.storageEdgeRecordMarkForDeletion(edgesToMarkForDeletion);
            _graphRepository.storageEdgeRecordDelete(edgesToDelete);
            _graphRepository.storageEdgeRecordCreate(edgesToCreate);
            _graphRepository.storageEdgeRecordUpdate(edgesToUpdate);
			//------------------------------------
			foreach (GraphEdge edge in edgesManipulated) {
				//IMPORTANT:
				//Note that 'storageEdgeRecordCreate' and 'storageEdgeRecordUpdate'
				//only *sent* flags that had RemotePending still up, but
				//both methods had not modified the actual values of the Edge.
				//This was so that if they failed, one had nothing to rollback here.
				//On the other hand, one must not forget to update the flags
				//in the GraphEdge to match what was written to the Db....
				//So....
				//Wrap up by updating the in-memory GraphEdge modelState, leaving only 
				//the RemotePending flag up if already up (which it will be):
				((IModelState)edge).ModelState &= OfflineModelState.RemotePending;
			}
			//------------------------------------
		}


		/// <summary>
		/// Flush/Persist the in-memory Vertex to the local storage.
		/// </summary>
		/// <internal>
		/// Optimize this as being a passthru to Flush(vertexList), rather than
		/// the other way around.
		/// </internal>
		/// <param name="vertex">The vertex to flush.</param>
		/// <exception cref="System.ArgumentNullException">An Exception is raised if the vertex is null.</exception>
		protected void Flush(GraphVertex vertex) {
			//Check Args:
			if (vertex == null) {
				throw new System.ArgumentNullException("vertex");
			}

			traceInformation("Flush GraphVertex [Id:{0}, Status:{1}].", vertex.Id, ((IModelState)vertex).ModelState);

			if ((((IModelState)vertex).ModelState & OfflineModelState.LocalPending) == 0) {
				//Vertex has already been persisted to local storage
				//so there is nothing to do:
				return;
			}

			lock (this) {

				if ((((IModelState)vertex).ModelState & OfflineModelState.LocalDeletionPending) != 0) {
					//The vertex was marked for local deleting...
					//IMPORTANT:
					//This supercedes any LocalCreatePending or LocalUpdatePending flags that may 
					//have been set as well.
					if ((((IModelState)vertex).ModelState & OfflineModelState.RemoteDeletionPending) != 0) {
						//...and is marked for remote deletion as well, 
						//so we can't delete it yet (or we would lose this information upon local shutdown).
						//ie: this is a serious deletion (not just a local cleanup)...
						//So mark the GraphVertex *record* as needing to be deleted from from remote db.
						//(does not delete from local storage yet):
						List<Guid> tmpVertexIds = new List<Guid>();
						tmpVertexIds.Add(vertex.Id);
                        _graphRepository.storageVertexRecordMarkForDeletion(tmpVertexIds);
						//This means that the vertex record .Deleted flag is 1, and therefore filters
						//it out from GetVertex requests etc...
					}
					else {
						//This is a local delete only (ie a local release of storage resources).
						//And does not have to worry about remote deletion being completed...
						//So just delete locally and we're done:
						List<Guid> tmpVertexIds = new List<Guid>();
						tmpVertexIds.Add(vertex.Id);
                        _graphRepository.storageVertexRecordDelete(tmpVertexIds);
					}

				}
				else if ((((IModelState)vertex).ModelState & OfflineModelState.LocalCreationPending) != 0) {
					//This vertex was created in memory, but 
					//was not persisted to local storage yet...
					//So create the new record:
					IGraphVertexList tmpVertices = new IGraphVertexList();
					tmpVertices.Add(vertex);
                    _graphRepository.storageVertexRecordCreate(tmpVertices);
					//IMPORTANT:
					//Note that Create persists the same information 
					//as Update, so there is no reason to worry about the Update afterwards...
				}
				else {
					//This vertex has been persisted to local storage at some point
					//in the past, (ie was not marked as LocalCreationPending),
					//but changes have been made since then...
					//Update the record in local storage:
					Debug.Assert((((IModelState)vertex).ModelState & OfflineModelState.LocalUpdatePending) != 0);

					IGraphVertexList tmpVertices = new IGraphVertexList();
					tmpVertices.Add(vertex);
                    _graphRepository.storageVertexRecordUpdate(tmpVertices);
				}

				//IMPORTANT:
				//Note that 'storageVertexRecordCreate' and 'storageVertexRecordUpdate'
				//only *sent* flags that had RemotePending still up, but
				//both methods had not modified the actual values of the Vertex.
				//This was so that if they failed, one had nothing to rollback here.
				//On the other hand, one must not forget to update the flags
				//in the GraphVertex to match what was written to the Db....
				//So....
				//Wrap up by updating the in-memory GraphVertex modelState, leaving only 
				//the RemotePending flag up if already up (which it will be):
				((IModelState)vertex).ModelState &= OfflineModelState.RemotePending;
			}//~lock
		}





		/// <summary>
		/// Flush/Persist the in-memory Edges to the local storage.
		/// </summary>
		/// <internal>
		/// Optimize this as being a passthru to Flush(vertexList), rather than
		/// the other way around.
		/// </internal>
		/// <param name="edge">The GraphEdge to flush.</param>
		/// <exception cref="System.ArgumentNullException">An Exception is raised if the edge is null.</exception>
		protected void Flush(GraphEdge edge) {
			//Check Args:
			if (edge == null) {
				throw new System.ArgumentNullException("edge");
			}


			if ((((IModelState)edge).ModelState & OfflineModelState.LocalPending) == 0) {
				return;
			}

			/*
			if ((((IModelState)edge).ModelState & OfflineModelState.LocalUpdatePending) == 0) {
				//Edge has already been persisted to local storage
				//so there is nothing to do:
				return;
			}
			 */

			lock (this) {

				if ((((IModelState)edge).ModelState & OfflineModelState.LocalDeletionPending) != 0) {
					//The edge was marked for local deleting...
					if ((((IModelState)edge).ModelState & OfflineModelState.RemoteDeletionPending) != 0) {
						//...and is marked for remote deletion as well, 
						//then this is a serious deletion (not just a local cleanup)...
						//So mark the record as needing to be deleted from from remote db.
						//(does not delete from local storage yet):
                        _graphRepository.storageEdgeRecordMarkForDeletion(new IGraphEdgeList(edge));
					}
					else {
						//This is a local delete only (ie a local release of storage resources).
						//And does not have to worry about remote deletion being completed...
						//So just delete locally and we're done:
                        _graphRepository.storageEdgeRecordDelete(new IGraphEdgeList(edge));
					}

				}
				else if ((((IModelState)edge).ModelState & OfflineModelState.LocalCreationPending) != 0) {
					//This vertex was created in memory, but 
					//was not persisted to local storage yet...
					//So create the new record:
                    _graphRepository.storageEdgeRecordCreate(new IGraphEdgeList(edge));

				}
				else {
					//This vertex has been persisted to local storage at some point
					//in the past, but changes have been made...
					//Update the record in local storage:
					Debug.Assert((((IModelState)edge).ModelState & OfflineModelState.LocalUpdatePending) != 0);
                    _graphRepository.storageEdgeRecordUpdate(new IGraphEdgeList(edge));
				}

				//IMPORTANT:
				//Note that 'storageEdgeRecordCreate' and 'storageEdgeRecordUpdate'
				//only *sent* flags that had RemotePending still up, but
				//both methods had not modified the actual values of the Edge.
				//This was so that if they failed, one had nothing to rollback here.
				//On the other hand, one must not forget to update the flags
				//in the GraphEdge to match what was written to the Db....
				//So....
				//Wrap up by updating the in-memory GraphEdge modelState, leaving only 
				//the RemotePending flag up if already up (which it will be):
				((IModelState)edge).ModelState &= OfflineModelState.RemotePending;
			}//~lock

		}


		#endregion


		#region Public Methods - Sync
		/// <summary>
		/// Get Ids of all new (DateCreated>LastSyncDate) Vertices 
		/// pointing to records within a collection of DataSources, 
		/// that are not marked as already transmitted to Client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that this method does not load vertices into in-memory cache,
		/// (although the use of <c>GetVertices</c> could be used subsequently
		/// to do this).
		/// </para>
		/// </remarks>
		/// <param name="dataSourceIds">Iterable Collection of DataSource </param>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		Guid[] IGraphSyncServer.GetNewVertexIds(ICollection<int> dataSourceIds, Guid clientMachineId, DateTime lastSyncDate) {
            return _graphRepository.storageForSyncGetNewVertexIds(dataSourceIds, clientMachineId, lastSyncDate);
		}

		/// <summary>
		/// Get Ids of all new Vertices (DateCreated>LastSyncDate) 
		/// pointing to specified records in a specified datasource,
		/// that are not marked as already transmitted to client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that this method does not load vertices into in-memory cache.
		/// </para>
		/// </remarks>
		/// <param name="dataObjectFactoryId">A single dataSource id.</param>
		/// <param name="dataObjectIds">An iterable Collection of dataObject record </param>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		Guid[] IGraphSyncServer.GetNewVertexIds(int dataObjectFactoryId, ICollection<string> dataObjectIds, Guid clientMachineId, DateTime lastSyncDate) {
            return _graphRepository.storageForSyncGetNewVertexIds(dataObjectFactoryId, dataObjectIds, clientMachineId, lastSyncDate);
		}

		/// <summary>
		/// Get Ids of all updated (DateModified>LastSyncDate) Vertices 
		/// pointing to records within a collection of DataSources, 
		/// that are not marked as already transmitted to Client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Note that this method does not load vertices into in-memory cache
		/// (although the use of <c>GetVertices</c> could be used subsequently
		/// to do this).
		/// </para>
		/// </remarks>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="lastSyncDate">The date of the last Sync request.</param>
		/// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		Guid[] IGraphSyncServer.GetUpdatedVertexIds(Guid clientMachineId, DateTime lastSyncDate) {
            return _graphRepository.storageForSyncGetUpdatedVertexIds(clientMachineId, lastSyncDate);
		}

		/// <summary>
		/// Get a collection of GraphVertex Ids that have been deleted
		/// on Server, that are still present on Client.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The ID's returned are to Vertices that no longer exist 
		/// in this graph.
		/// </para>
		/// </remarks>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		Guid[] IGraphSyncServer.GetDeletedVertexIds(Guid clientMachineId) {
            return _graphRepository.storageForSyncGetDeletedVertexIds(clientMachineId);

		}




		//      this.storageForSyncMarkTransmissionBegun;
		//      this.storageForSyncMarkTransmissionAborted;
		//      this.storageForSyncMarkTransmissionSuccessful;


		/// <summary>
		/// Marks a vertex as having been already on a Client.
		/// Intended to be used by the GaphSyncManager when 
		/// a GraphVertex that has already been created on the Client is now being duplicated 
		/// on the Server (which ends up being the same thing as created on the server
		/// and successfully sent to Client).
		/// </summary>
		/// <param name="clientMachineId">The unique Id of the client machine.</param>
		/// <param name="vertexId">The vertex of the id.</param>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
		/// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
		void IGraphSyncServer.SaveVertexIdInVerticesSent(Guid clientMachineId, Guid vertexId) {
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

            _graphRepository.storageForSyncMarkVertexIdAsSent(clientMachineId, vertexId);
		}


		void IGraphSyncServer.RemoveVertexIdInVerticesSent(Guid clientMachineId, Guid vertexId) {
			if (clientMachineId == Guid.Empty) {
				throw new System.ArgumentNullException("clientMachineId");
			}
			if (vertexId == Guid.Empty) {
				throw new System.ArgumentNullException("vertexId");
			}

            _graphRepository.storageForSyncRemoveVertexIdFromVerticesSent(clientMachineId, vertexId);
		}

		#endregion

		#region Protected - Event Handlers - Remote Synchronization

		/// <summary>
		/// method called when the creation of a vertex has been accepted remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexCreateAccepted(object sender, GraphVertexEventArgs e) {
			//Get from *all*, or load from db into memory (*all*):
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(e.Vertex.Id);

			if ((((IModelState)vertex).ModelState & OfflineModelState.RemoteCreationPending) == 0) {
				traceError("creation remotely accepted: already sync");
			}
			//Unset status of remote creation needed:
			((IModelState)vertex).ModelState &= ~OfflineModelState.RemoteCreationPending;
			//Set: state it for local update (flush):
			((IModelState)vertex).ModelState |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// Vertexes the creation remotely rejected.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexCreateRejected(object sender, GraphVertexEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		/// <summary>
		/// method called when the update of a vertex has been accepted remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexUpdateAccepted(object sender, GraphVertexEventArgs e) {
			//Get from *all*, or load from db into memory (*all*):
			//Raise an error if no vertex found.
			GraphVertex vertex = GetVertex(e.Vertex.Id);

			if ((((IModelState)vertex).ModelState & OfflineModelState.RemoteUpdatePending) == 0) {
				traceError("vertex update remotely accepted: already sync");
			}
			//Unset -- no more remote update required:
			((IModelState)vertex).ModelState &= ~OfflineModelState.RemoteUpdatePending;
			//Set: status for local flush later:
			//OLIVIER:
			//TODO: Wait! Is this not going to reset local vertex date as later than remote vertex edit date?
			((IModelState)vertex).ModelState |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// method called when the update of a vertex has been rejected remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexUpdateRejected(object sender, GraphVertexEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		/// <summary>
		/// method called when the deletion of a vertex has been accepted remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexDeleteAccepted(object sender, GraphVertexEventArgs e) {

			Guid vertexId = e.Vertex.Id;

			lock (this) {

				_CachedDeletePendingVertices.Remove(vertexId);

				IGraphEdgeList newCollection = new IGraphEdgeList();
				newCollection.AddRange(Collections.Filter(_CachedDeletePendingEdges,
					delegate(GraphEdge edge) {
						return edge.SourceId != vertexId && edge.TargetId != vertexId;
					}));
				_CachedDeletePendingEdges = newCollection;

				List<Guid> tmpVertexIdList = new List<Guid>();
				tmpVertexIdList.Add(vertexId);
			 _graphRepository.storageVertexRecordDelete(tmpVertexIdList);
			}
		}

		/// <summary>
		/// method called when the deletion of a vertex has been rejected remotely
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		public void OnRemoteVertexDeleteRejected(object sender, GraphVertexEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}





		/// <summary>
		/// Event handler for when an <see cref="IGraphEdge"/> has been remotely created.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		public void OnRemoteEdgeCreateAccepted(object sender, EdgeEventArgs e) {

			//Find the edge that has the source/target mentioned in the args:
			GraphEdge edge = _CachedVertexInEdges[e.Edge.TargetId].Find(
				delegate(GraphEdge edge2) {
					return edge2.SourceId == e.Edge.SourceId;
				}
			);


			Debug.Assert(edge != null);
			//UnSet:
			((IModelState)edge).ModelState &= ~OfflineModelState.RemoteCreationPending;
			//Set: status for local flush later:
			((IModelState)edge).ModelState |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// Method called when the creation of an edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		public void OnRemoteEdgeCreateRejected(object sender, EdgeEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		/// <summary>
		/// Event handler for when an <see cref="IGraphEdge"/> has been remotely updated.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		public void OnRemoteEdgeUpdateAccepted(object sender, EdgeEventArgs e) {
			//Find the edge that has the source/target mentioned in the args:
			GraphEdge edge = _CachedDeletePendingEdges.Find(delegate(GraphEdge e2) {
				return e2.SourceId == e.Edge.SourceId && e2.TargetId == e.Edge.TargetId;
			});
			//UnSet:
			((IModelState)edge).ModelState &= ~OfflineModelState.RemoteUpdatePending;
			//Set: status for local flush later:
			((IModelState)edge).ModelState |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// Method called when the update of an edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		public void OnRemoteEdgeUpdateRejected(object sender, EdgeEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		/// <summary>
		/// Event handler for when an <see cref="IGraphEdge"/> has been remotely deleted.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		public void OnRemoteEdgeDeleteAccepted(object sender, EdgeEventArgs e) {
			//Find the edge that has the source/target mentioned in the args:
			GraphEdge edge = _CachedDeletePendingEdges.Find(delegate(GraphEdge e2) {
				return e2.SourceId == e.Edge.SourceId && e2.TargetId == e.Edge.TargetId;
			});
			//Unset:
			((IModelState)edge).ModelState &= ~OfflineModelState.RemoteDeletionPending;
			//Set: status for local flush later:
			((IModelState)edge).ModelState |= OfflineModelState.LocalUpdatePending;
		}

		/// <summary>
		/// Method called when the deletion of an edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		public void OnRemoteEdgeDeleteRejected(object sender, EdgeEventArgs e) {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion



	}//Class:End
}//Namespace:End
