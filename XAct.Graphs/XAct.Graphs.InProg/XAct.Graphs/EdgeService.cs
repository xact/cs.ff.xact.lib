﻿
using System;
using System.Collections.Generic;
using XAct.Diagnostics;
using XAct.Domain;

namespace XAct.Graphs
{
    public class EdgeService<TId> : EdgeServiceBase<TId>
    {

        #region Constructors
        public EdgeService(ITracingService tracingService, IGraphService<TId> graphService, Func<TId, TId, IGraphEdge<TId>> abstractMethodToCreateEdges) : base(tracingService, graphService, abstractMethodToCreateEdges) { }
        #endregion

        #region Public Methods - Implementation of GraphProviderBase - Edges

        /// <summary>
        /// Tries to return an Edge between the two specified endpoint 
        /// <para>
        /// If needed, lazy-loads the specified vertices.
        /// </para>
        /// </summary>
        /// <param name="sourceId">The source id.</param>
        /// <param name="targetId">The target id.</param>
        /// <param name="foundEdge">The found edge.</param>
        /// <returns></returns>
        public bool TryGetEdge(Guid sourceId, Guid targetId, out IGraphEdge foundEdge)
        {

            if ((sourceId == Guid.Empty) || (targetId == Guid.Empty))
            {
                //No way tht I can ascertain if an edge exists if I don't have id's of both ends.
                foundEdge = null;
                return false;
            }

            foundEdge = null;
            GraphVertex source;

            if (!TryGetVertex(sourceId, out source))
            {
                foundEdge = null;
                return false;
            }
            foreach (IGraphEdge edge in  _CachedVertexOutEdges[sourceId])
            {
                if (edge.TargetId == targetId)
                {
                    foundEdge = edge;
                    return true;
                }
            }


            //No use searching through in-edge cache of target
            //because if in one, its in the other...
            foundEdge = null;
            return false;

        }





        /// <summary>
        /// Returns an enumeration of the in-edges of the vertex with the specified id.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns>
        /// An enumerable collection of IGraphEdge instances (zero sized or more)
        /// </returns>
        /// <exception cref="ArgumentNullException">An Exception is raised if the argument is Empty.</exception>
        /// <exception cref="VertexNotFoundException">An Exception is raised if the GraphVertex is not found</exception>
        public IEnumerable<IGraphEdge> InEdges(Guid vertexId)
        {
            //Check Args:
            if (vertexId == Guid.Empty)
            {
                throw new System.ArgumentNullException("vertexId");
            }

            //Get from *all*, or load from db into memory (*all*):
            //Raise an error if no vertex found.
            GraphVertex vertex = GetVertex(vertexId);

            //If we have a source vertex, it has been mounted via GetVertex.
            //So it has an out-edges array we can safely address:
            return _CachedVertexInEdges[vertex.Id];
        }


        /// <summary>
        /// Returns the number of in.edges for the vertex with the specified Id.
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// Returns 0 or more, unless exception raised.
        /// </returns>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the argument is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the GraphVertex is not found.</exception>
        public int InDegree(Guid vertexId)
        {
            //Check Args:
            if (vertexId == Guid.Empty)
            {
                throw new System.ArgumentNullException("vertexId");
            }
            //Get from *all*, or load from db into memory:
            //Raise an error if no vertex found.
            GraphVertex vertex = GetVertex(vertexId);


            //If we have a source vertex, it has been mounted via GetVertex.
            //So it has an out-edges array we can safely address:
            return _CachedVertexInEdges[vertex.Id].Count;
        }


        /// <summary>
        /// Returns the number of out.edges for the vertex with the specified Id.
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// Returns 0 or more, unless exception raised.
        /// </returns>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the argument is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the GraphVertex is not found.</exception>
        public int OutDegree(Guid vertexId)
        {
            //Check Args:
            if (vertexId == Guid.Empty)
            {
                throw new System.ArgumentNullException("vertexId");
            }
            //Get from *all*, or load from db into memory (*all*):
            //Raise an error if no vertex found.
            GraphVertex vertex = GetVertex(vertexId);

            //If we have a source vertex, it has been mounted via GetVertex.
            //So it has an out-edges array we can safely address:
            return _CachedVertexOutEdges[vertex.Id].Count;
        }

        /// <summary>
        /// Returns an Enumeration of the out-edges of the GraphVertex with the specified Id.
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// An enumerable collection of IGraphEdge instances (zero-size or more, unless exception raised).
        /// </returns>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the GraphVertex is not found.</exception>
        public IEnumerable<IGraphEdge> OutEdges(Guid vertexId)
        {
            //Check Args:
            if (vertexId == Guid.Empty)
            {
                throw new System.ArgumentNullException("vertexId");
            }
            //Get from *all*, or load from db into memory (*all*):
            //Raise an error if no vertex found.
            GraphVertex vertex = GetVertex(vertexId);

            //Because GetVertex ensures the vertex is in mem,
            //and also got its Out-Edges, we can safely count in-mem
            //without hitting the Db:
            return _CachedVertexOutEdges[vertex.Id];
        }


        




        /// <summary>
        /// Updates the Edge between the source Vertex Id and target Vertex Id.
        /// </summary>
        /// <param name="sourceId">The source Vertex Id.</param>
        /// <param name="targetId">The target Vertex Id.</param>
        /// <returns>
        /// Return true if Edge is updated.
        /// </returns>
        /// <remarks>Invokes <see cref="OnEdgeUpdated"/> if successful.</remarks>
        /// <exception cref="VertexNotFoundException">An Exception is raised if the source or target Vertex cannot be found</exception>
        public bool UpdateEdge(Guid sourceId, Guid targetId)
        {
            //Check Args:
            if (sourceId == Guid.Empty)
            {
                throw new System.ArgumentNullException("sourceId");
            }
            if (targetId == Guid.Empty)
            {
                throw new System.ArgumentNullException("targetId");
            }

            lock (this)
            {
                //Ensure these vertices exist:
                //Get from *all*, or load from db into memory (*all*):
                //Raise an error if no vertex found.
                GraphVertex sourceVertex = GetVertex(sourceId);
                GraphVertex targetVertex = GetVertex(targetId);

                //Create a new edge:
                IGraphEdge edge = CreateEdge(sourceVertex, targetVertex);

                if (edge == null)
                {
                    return false;
                }

                //Raise a cancelable event prior to deleting the edge:
                CancelEdgeEventArgs arg = new CancelEdgeEventArgs(edge);
                OnEdgeUpdating(arg);

                if (arg.Cancel)
                {
                    // edge deletion canceled, return false
                    return false;
                }
                // Change state so that thread later creates in local and remote storage:
                ((ICachedElement)edge).modelState |= OfflineModelState.UpdatePending;

                if (UpdateInRealTime)
                {
                    Flush(edge);
                }

                // raise event after creating it:
                OnEdgeUpdated(new EdgeEventArgs(edge));

                return true;
            }//~lock
        }





        /*
                            edge = _CachedVertexOutEdges[sourceId].Find(
                delegate(IGraphEdge e) {
                    return e.TargetId == targetId;
                }
        */


        #endregion

    }
}
