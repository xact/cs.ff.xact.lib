﻿using System;
using XAct.Diagnostics;
using System.Collections.Generic;

namespace XAct.Graphs
{
    public abstract class EdgeServiceBase<TId>: IEdgeService<TId>
    {

        #region Events Raised
        /// <summary>
        /// Cancelable event raised before an <see cref="IGraphEdge{TId}"/> is about to be created.
        /// </summary>
        public event EventHandler<CancelEdgeEventArgs> EdgeCreating;

        /// <summary>
        /// Event raised after an <see cref="IGraphEdge{TId}"/> is Created.
        /// </summary>
        public event EventHandler<EdgeEventArgs> EdgeCreated;

        /// <summary>
        /// Cancelable event raised before an <see cref="IGraphEdge{TId}"/> is updated.
        /// </summary>
        public event EventHandler<CancelEdgeEventArgs> EdgeUpdating;

        /// <summary>
        /// Event raised after an <see cref="IGraphEdge{TId}"/> is updated.
        /// </summary>
        /// <para>
        /// Raised after an <see cref="IGraphEdge{TId}"/> is updated in memory, but not yet necessarily persisted to storage.
        ///   </para>
        public event EventHandler<EdgeEventArgs> EdgeUpdated;

        /// <summary>
        /// Cancelable event raised before an <see cref="IGraphEdge{TId}"/> is about to be deleted.
        /// </summary>
        public event EventHandler<CancelEdgeEventArgs> EdgeDeleting;

        /// <summary>
        /// Event raised after an <see cref="IGraphEdge{TId}"/> is Deleted.
        /// </summary>
        public event EventHandler<EdgeEventArgs> EdgeDeleted;
        #endregion
        
        #region Services
        private readonly ITracingService _tracingService;
        //Owner Service:
        private readonly IGraphService<TId> _graphService;
        #endregion

        #region Fields 
        private Func<TId, TId, IGraphEdge<TId>> _methodToCreateEdges;
        #endregion

        #region Properties
        protected ITracingService TracingService {get {return _tracingService;}}
        protected IGraphService<TId> GraphService { get { return _graphService; } }
        #endregion

        #region Constructors
        protected EdgeServiceBase(ITracingService tracingService, IGraphService<TId> graphService, Func<TId, TId, IGraphEdge<TId>> methodToCreateEdges = null)
        {
            if (tracingService == null){throw new ArgumentNullException("tracingService");}
            if (graphService == null){throw new ArgumentNullException("graphService");}

            _tracingService = tracingService;
            _graphService = graphService;

            _methodToCreateEdges = methodToCreateEdges ?? ((sourceId, targetId) => new GraphEdge<TId>(sourceId, targetId));
        }
        #endregion  





        #region Impelementation of IEdgeService 
        //----------------------------------------------------------------------------------------------------
        //CRUD / CREATE
        //----------------------------------------------------------------------------------------------------

        /// <summary>
        /// Create a new Edge between the specified Source and Target vertices.
        /// <para>
        /// IMPORTANT: The edge is created, but is not added to the graph until AddEdge(TEdge) is invoked.
        /// </para>
        /// </summary>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <param name="sourceId">The source Vertex Id.</param>
        /// <param name="targetId">The target Vertex Id.</param>
        /// <returns>A new Edge.</returns>
        /// <internal>
        /// CREATE
        /// </internal>
        public IGraphEdge<TId> CreateEdge(TId sourceId, TId targetId)
        {
            //Check Args:
            if (sourceId.IsNotInitialized())
            {
                throw new ArgumentNullException("sourceId");
            }
            if (targetId.IsNotInitialized())
            {
                throw new ArgumentNullException("targetId");
            }

            return _methodToCreateEdges(sourceId, targetId);
        }


        /// <summary>
        /// Adds a new Edge between a source Vertex and a target Vertex.
        /// Returns <c>true</c> only if a new Edge was created.
        /// Returns <c>false</c> if an Edge between the two endpoints already existed.
        /// <para>
        /// Wrapper to <see cref="AddEdge(TId,TId)"/>.
        /// </para>
        /// </summary>
        /// <param name="sourceVertex">The source Vertex.</param>
        /// <param name="targetVertex">The target Vertex.</param>
        /// <returns>
        /// Returns <c>true</c> only if an Edge was created.
        /// Returns <c>false</c> if an Edge already existed.
        /// </returns>
        /// <remarks>
        /// <para>
        /// Note that it first invokes <see cref="ContainsEdge(TId, TId)"/> to see if an 
        /// edge exists between the two endpoints: 
        /// the comparison is done by Id's, and not Edge or vertex entities.
        /// </para>
        /// 	<para>
        /// Invokes a cancelable <see cref="OnEdgeCreating"/> before creating the Edge,
        /// and then <see cref="OnEdgeCreated"/> if successful.
        /// </para>
        /// 	<para>
        /// No error is raised if an Edge already exists between the two
        /// Vertex instances:
        /// it just returns <c>false</c>, and <see cref="OnEdgeCreated"/>
        /// is never invoked.
        /// </para>
        /// </remarks>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <exception cref="ArgumentNullException">An exception is raised if the sourceVertex is null.</exception>
        /// <exception cref="ArgumentNullException">An exception is raised if the targetVertex is null.</exception>
        public bool AddEdge(IGraphVertex<TId> sourceVertex, IGraphVertex<TId> targetVertex)
        {
            //Check Args:
            if (sourceVertex == null)
            {
                throw new ArgumentNullException("sourceVertex");
            }
            if (targetVertex == null)
            {
                throw new ArgumentNullException("targetVertex");
            }
            return AddEdge(sourceVertex.Id, targetVertex.Id);

        }

        /// <summary>
        /// Adds a new Edge between a source Vertex and a target Vertex.
        /// Returns <c>true</c> only if an Edge was created.
        /// Returns <c>false</c> if an Edge already existed.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes a cancelable <see cref="OnEdgeCreating"/> before creating the Edge,
        /// and then <see cref="OnEdgeCreated"/> if successful.
        /// </para>
        /// <para>
        /// No error is raised if an Edge already exists between the two 
        /// Vertex instances:
        /// it just returns <c>false</c>, and <see cref="OnEdgeCreated"/>
        /// is never invoked.
        /// </para>
        /// </remarks>
        /// <param name="sourceId">The source Vertex Id.</param>
        /// <param name="targetId">The target Vertex Id.</param>
        /// <returns>
        /// Returns <c>true</c> only if an Edge was created.
        /// Returns <c>false</c> if an Edge already existed.
        /// </returns>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <exception cref="ArgumentNullException">Exception raised if sourceId is null/empty.</exception>
        /// <exception cref="ArgumentNullException">Exception raised if targetId is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">Exception raised if sourceVertex is not found.</exception>
        /// <exception cref="VertexNotFoundException">Exception raised if targetVertex is not found.</exception>
        public bool AddEdge(TId sourceId, TId targetId)
        {
            //Create a new edge:
            IGraphEdge<TId> edge = CreateEdge(sourceId, targetId);

            return AddEdge(edge);
        }

		/// <summary>
		/// Add a newly created Edge to the graph.
		/// </summary>
		/// <param name="edge">The edge to add.</param>
		/// <returns>
		/// </returns>
		/// <remarks>
		/// <para>
		/// </para>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, GraphEdge}"/>
		/// </para>
		/// </remarks>
        public bool AddEdge(IGraphEdge<TId> edge)
        {
            if (edge == null)
            {
                throw new System.ArgumentException("edge");
            }


            //Ensure these vertices exist:
            //Get from *all*, or load from db into memory (*all*):
            //Raise an error if no vertex found.
            IGraphVertex<TId> sourceVertex = edge.Source;
            IGraphVertex<TId> targetVertex = edge.Target;


            if ((sourceVertex == null) || (targetVertex == null))
            {
                throw new System.ArgumentException("Cannot Add an edge when one or more of the two endpoints don't exist.");
            }


            //Raise a cancelable event prior to deleting the edge:
            CancelEdgeEventArgs arg = new CancelEdgeEventArgs(edge);

            OnEdgeCreating(arg);

            if (arg.Cancel)
            {
                // edge deletion canceled, return false
                return false;
            }


            InternalAddEdge(edge);

            // raise event after creating it:
            OnEdgeCreated(new EdgeEventArgs(edge));

            return true;
        }

        protected abstract bool InternalAddEdge(IGraphEdge<TId> edge);

        //----------------------------------------------------------------------------------------------------
        //CRUD / RETRIEVE
        //----------------------------------------------------------------------------------------------------



        public bool ContainsEdge(IGraphEdge<TId> edge)
        {
            if (edge == null)
            {
                return false;
            }

            return ContainsEdge(edge.SourceId, edge.TargetId);
        }

        public bool ContainsEdge(IGraphVertex<TId> source, IGraphVertex<TId> target)
        {
            return (GetEdge(source.Id, target.Id, false) != null);
        }


        /// <summary>
        /// Determines whether the graph contains the specified edge.
        /// <para>
        /// IMPORTANT:
        /// Comparison is done by sourceId and targetId -- not by Edge or Vertex entity. 
        /// </para>
        /// </summary>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <param name="sourceId">The Id of the source endpoint vertex.</param>
        /// <param name="targetId">The Id of the target endpoint vertex.</param>
        /// <returns>
        /// 	<c>true</c> if the edge contains an edge between the two vertices; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsEdge(TId sourceId, TId targetId)
        {
            //We can simplify the code by looking for an edge, 
            //and if found, we're home:
            IGraphEdge<TId> foundEdge;

            return ( GetEdge(sourceId, targetId, false) != null);
        }


        /// <summary>
        /// Tries to return an Edge between the two specified endpoints.
        /// </summary>
        /// <param name="source">The source vertex.</param>
        /// <param name="target">The target vertex.</param>
        /// <param name="throwExceptionIfNotFound">if set to <c>true</c> [throw exception if not found].</param>
        /// <returns></returns>
        /// <internal>
        /// RETRIEVE
        ///   </internal>
        public IGraphEdge<TId> GetEdge(IGraphVertex<TId> source, IGraphVertex<TId> target, bool throwExceptionIfNotFound=true)
        {
            if ((source == null) || (target == null))
            {
                if (throwExceptionIfNotFound)
                {
                    throw new ArgumentException("Cannot find edge specified.");
                }
                return null;
            }

            return GetEdge(source.Id, target.Id,throwExceptionIfNotFound);
        }


        /// <summary>
        /// Returns an edge between the two endpoints.
        /// </summary>
        /// <param name="source">The source vertex.</param>
        /// <param name="target">The target vertex.</param>
        /// <param name="throwExceptionIfNotFound">if set to <c>true</c> [throw exception if not found].</param>
        /// <returns></returns>
        /// <internal>
        /// RETRIEVE
        /// </internal>
        public abstract IGraphEdge<TId> GetEdge(TId source, TId target, bool throwExceptionIfNotFound = true);


        //----------------------------------------------------------------------------------------------------
        //CRUD / UPDATE
        //----------------------------------------------------------------------------------------------------
        public abstract bool UpdateEdge(TId sourceId, TId targetId);


        //----------------------------------------------------------------------------------------------------
        //CRUD / DELETE
        //----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Removes the Edge between the source Vertex Id and target Vertex Id.
        /// if the Edge doesn't exists, do nothing.
        /// <para>
        /// Raises events before and after operation.
        /// </para>
        /// <para>
        /// Returns <c>true</c> if an Edge is removed.
        /// </para>
        /// <para>
        /// Throws an error if no edge was found.
        /// </para>
        /// </summary>
        /// <remarks>
        /// 	<para>
        /// No error is raised if the source Vertex,
        /// or target Vertex, or an Edge between
        /// the two, is not found to exist.
        /// </para>
        /// 	<para>
        /// Invokes a cancelable <see cref="OnEdgeDeleting"/> before deleting the Vertex,
        /// and then <see cref="OnEdgeDeleted"/> if successful.
        /// </para>
        /// 
        /// </remarks>
        /// 	<param name="sourceId">The source Vertex Id.</param>
        /// 	<param name="targetId">The target Vertex Id.</param>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <exception cref="VertexNotFoundException">An Exception is raised if the source or target Vertex cannot be found</exception>
        public void RemoveEdge(TId sourceId, TId targetId)
        {
            //Check Args:
            if (sourceId.IsNotInitialized())
            {
                throw new ArgumentNullException("sourceId");
            }
            if (targetId.IsNotInitialized())
            {
                throw new ArgumentNullException("targetId");
            }
            //int check1 = _CachedVertexInEdges.Count;
            //int check2 = _CachedVertexOutEdges.Count;

            //Ensures the vertices are in mem,
            //and therefore, their OutEdges:
            IGraphVertex<TId> source = this.GraphService.Vertices.GetVertex(sourceId);
            IGraphVertex<TId> target = this.GraphService.Vertices.GetVertex(targetId);

            //Throws an error if the edge cannot be found.
            IGraphEdge<TId> edge = GetEdge(source, target);

            //And remove from 
            return RemoveEdge(edge);

        }

        /// <summary>
		/// Remove an Edge from the graph.
		/// <para>
		/// Returns <c>true</c> only if an Edge is removed,
		/// Returns <c>false</c> if no Edge already existed.
		/// </para>
		/// </summary>
		/// <param name="edge">The edge to remove.</param>
		/// <returns>
		/// Returns <c>true</c> only if an Edge is removed,
		/// Returns <c>false</c> if no Edge already existed.
		/// </returns>
		/// <remarks>
		/// <para>
		/// Note that this is the core method,
		/// to which the other signatures of <c>RemoveEdge</c>
		/// end up.
		/// </para>
		/// 	<para>
		/// Invokes <see cref="OnEdgeDeleted"/> if the Edge is successfully removed.
		/// </para>
		/// 	<para>
		/// No error is raised if the source Vertex,
		/// or target Vertex, or the Edge between
		/// the two, is not found to exist.
		/// </para>
		/// <para>
		/// Implementation of a method/property defined in <see cref="QuickGraph.IMutableEdgeListGraph{TVertex, GraphEdge}"/>
		/// </para>
		/// </remarks>
		public bool RemoveEdge(IGraphEdge<TId> edge) {
			//Check Args:
			if (edge == null) {
				throw new System.ArgumentNullException("edge");
			}

			//Raise a cancelable event prior to deleting the edge:
			CancelEdgeEventArgs arg = new CancelEdgeEventArgs(edge);
			OnEdgeDeleting(arg);

			if (arg.Cancel) {
				// edge deletion canceled, return false
				return false;
			}

		    InternalRemoveEdge(edge);

            //Raise event:
			OnEdgeDeleted(new EdgeEventArgs(edge));


			//Raise QuickGraph event?
			//this.OnEdgeRemoved(new QuickGraph.EdgeEventArgs<GraphVertex, GraphEdge>(edge));

			return true;
		}




            


        protected abstract bool InternalRemoveEdge(IGraphEdge<TId> edge);

        //----------------------------------------------------------------------------------------------------
        //MISC
        //----------------------------------------------------------------------------------------------------
        public abstract IEnumerable<IGraphEdge<TId>> InEdges(TId vertexId);


        public abstract int InDegree(TId vertexId);


        public abstract int OutDegree(TId vertexId);

        public abstract IEnumerable<IGraphEdge<TId>> OutEdges(TId vertexId);
        #endregion












        #region Raise Events
        /// <summary>
        /// Raises the <see cref="EdgeCreating"/> event.
        /// </summary>
        /// <param name="e">The <see cref="CancelEdgeEventArgs"/> instance containing the event data.</param>
        protected void OnEdgeCreating(CancelEdgeEventArgs e)
        {
            if (EdgeCreating != null)
            {
                EdgeCreating(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="EdgeCreated"/> event.
        /// </summary>
        /// <param name="e">The <see cref="EdgeEventArgs"/> instance containing the event data.</param>
        protected void OnEdgeCreated(EdgeEventArgs e)
        {
            if (EdgeCreated != null)
            {
                EdgeCreated(this, e);
            }
        }



        /// <summary>
        /// Raises the <see cref="EdgeUpdating"/> event.
        /// </summary>
        /// <param name="e">The <see cref="CancelEdgeEventArgs"/> instance containing the event data.</param>
        protected void OnEdgeUpdating(CancelEdgeEventArgs e)
        {
            if (EdgeUpdating != null)
            {
                EdgeUpdating(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="EdgeUpdated"/> event.
        /// </summary>
        /// <param name="e">The <see cref="EdgeEventArgs"/> instance containing the event data.</param>
        protected void OnEdgeUpdated(EdgeEventArgs e)
        {
            if (EdgeUpdated != null)
            {
                EdgeUpdated(this, e);
            }
        }


        /// <summary>
        /// Raises the <see cref="EdgeDeleting"/> event.
        /// </summary>
        /// <param name="e">The <see cref="CancelEdgeEventArgs"/> instance containing the event data.</param>
        protected void OnEdgeDeleting(CancelEdgeEventArgs e)
        {
            if (EdgeDeleting != null)
            {
                EdgeDeleting(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="EdgeDeleted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="EdgeEventArgs"/> instance containing the event data.</param>
        protected void OnEdgeDeleted(EdgeEventArgs e)
        {
            if (EdgeDeleted != null)
            {
                EdgeDeleted(this, e);
            }
        }
        #endregion





        ///// <summary>
        ///// Raises the <see cref="EdgeAdded"/> event.
        ///// </summary>
        ///// <param name="args">The <see cref="QuickGraph.EdgeEventArgs&lt;GraphVertex, GraphEdge&gt;"/> instance containing the event data.</param>
        //virtual protected void OnEdgeAdded(QuickGraph.EdgeEventArgs<GraphVertex, GraphEdge> args)
        //{
        //    if (EdgeAdded != null)
        //    {
        //        EdgeAdded(this, args);
        //    }
        //}

        ///// <summary>
        ///// Raises the <see cref="EdgeRemoved"/> event.
        ///// </summary>
        ///// <param name="args">The <see cref="QuickGraph.EdgeEventArgs&lt;GraphVertex, GraphEdge&gt;"/> instance containing the event data.</param>
        //virtual protected void OnEdgeRemoved(QuickGraph.EdgeEventArgs<GraphVertex, GraphEdge> args)
        //{
        //    if (EdgeRemoved != null)
        //    {
        //        EdgeRemoved(this, args);
        //    }
        //}



    }
}
