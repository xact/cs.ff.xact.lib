﻿using System;

namespace XAct.Graphs
{
    public partial class GraphEdge<TId>
    {

        #region Implementation of IEdgeDef
        /// <summary>
        /// Gets the Id of the source Vertex.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that we we keep a reference to the GraphVertex by its Id,
        /// and not directly to the GraphVertex itself. 
        /// This forces us to always use the GraphProvider for the actual GraphVertex ref.
        /// </para>
        /// </remarks>
        /// <value>The source GraphVertex Id.</value>
        public TId SourceId 
        {
            get
            {
                return _sourceId;
            }
            set
            {
                if (_sourceId != default(TId)){throw new ArgumentException("Cannot reset SourceId once it is set.");}

                _sourceId = value;
            }
        
        }
        private TId _sourceId;

        /// <summary>
        /// Gets the Id of the target Vertex.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that we we keep a reference to the GraphVertex by its Id,
        /// and not directly to the GraphVertex itself. 
        /// This forces us to always use the GraphProvider for the actual GraphVertex ref.
        /// </para>
        /// </remarks>
        /// <value>The target GraphVertex Id.</value>
        public TId TargetId
        {
            get
            {
                return _targetId;
            }
            set
            {
                if (_targetId != default(TId)) { throw new ArgumentException("Cannot reset TargetId once it is set."); }

                _targetId = value;
            }
        }
        private TId _targetId;
        #endregion

    }
}
