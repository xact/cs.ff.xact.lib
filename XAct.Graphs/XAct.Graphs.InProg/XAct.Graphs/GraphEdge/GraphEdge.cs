using System;
using XAct.Domain;


namespace XAct.Graphs {

  /// <summary>
  /// Represent an Edge  between a source GraphVertex and a target Vertex.
  /// </summary>
  public partial class GraphEdge<TId> : IGraphEdge<TId>{




    /// <summary>
    /// Initializes a new instance of the <see cref="GraphEdge"/> class.
    /// </summary>
    /// <param name="sourceId">The source GraphVertex Id.</param>
    /// <param name="targetId">The target GraphVertex Id.</param>
    public GraphEdge(TId sourceId, TId targetId) {
      _sourceId = sourceId;
      _targetId = targetId;
      _modelState = OfflineModelState.CreationPending;
    }



    /// <summary>
    /// Compares two GraphEdge instances.
    /// </summary>
    /// <param name="other">The other edge to compare to.</param>
    /// <returns></returns>
    public int CompareTo(GraphEdge other) {

      int res = SourceId.CompareTo(other.SourceId);

      if (res == 0) { // same source
        res = TargetId.CompareTo(other.TargetId);
      }

      return res;
    }


    /// <summary>
    /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
    /// </returns>
    public override string ToString() {
      return String.Format("Edge [{0}=>{1}] [Status:{2}]", SourceId.ToString(), TargetId.ToString(), _modelState);
    }



    public int CompareTo(object obj)
    {
        GraphEdge other = obj as GraphEdge;

        if (other == null)
        {
            return -1;
        }

        int res = SourceId.CompareTo(other.SourceId);

        if (res == 0)
        { // same source
            res = TargetId.CompareTo(other.TargetId);
        }

        return res;
    }
  }//Class:End
}//Namespace:End
