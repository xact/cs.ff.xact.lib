﻿using XAct.Domain;

namespace XAct.Graphs
{
    public partial class GraphEdge<TId>
    {
		/// <summary>
    /// Gets or sets the modelState of this edge.
    /// </summary>
    /// <value>the Sync status.</value>
    OfflineModelState IModelState.ModelState {
      get {
        return _modelState;
      }
      set {
        _modelState = value;
      }
    }
    private OfflineModelState _modelState;
    }
}
