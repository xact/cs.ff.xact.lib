﻿using Microsoft.Practices.ServiceLocation;

namespace XAct.Graphs
{
    public partial class GraphEdge<TId> 
    {
        private static IVertexService VertexService
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IVertexService>();
            }
        }


        public GraphVertex<TId> Source
        {
            get
            {
                return VertexService.GetVertex(_sourceId);
            }
        }

        public GraphVertex<TId> Target
        {
            get
            {
                return VertexService.GetVertex(_targetId);
            }
        }
    }
}
