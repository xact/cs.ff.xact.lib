﻿using XAct.Domain;

namespace XAct.Graphs
{
    public partial class GraphEdge<TId>
    {
        /// <summary>
        /// Gets the Edge's weight.
        /// </summary>
        /// <value>The weight.</value>
        public int Weight
        {
            get
            {
                return _weight;
            }
            set
            {
                if (value != _weight)
                {
                    _weight = value;
                    //Changes have been made that need to be saved:
                    _modelState |= OfflineModelState.UpdatePending;
                }
            }
        }
        private int _weight;

    }
}
