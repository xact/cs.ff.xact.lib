﻿using System;
using System.Collections.Generic;
using System.Linq;
using XAct.Diagnostics;

namespace XAct.Graphs
{
    public abstract class VertexServiceBase<TId> : IVertexService<TId>
    {

        #region Services
        private readonly ITracingService _tracingService;
        private readonly IGraphRepository _graphRepository;
        #endregion

        #region Properties
        protected ITracingService TracingService { get { return _tracingService; } }
        protected IGraphRepository GraphRepository { get { return _graphRepository; } }
        #endregion

        protected VertexServiceBase(ITracingService tracingService, IGraphRepository graphRepository)
        {
            _tracingService = tracingService;
            _graphRepository = graphRepository;
        }

        #region Events Raised
        /// <summary>
        /// Event raised after a <see cref="GraphVertex"/> is created.
        /// </summary>
        public event EventHandler<GraphVertexEventArgs> VertexCreated;

        /// <summary>
        /// Event raised before a <see cref="GraphVertex"/> is deleted (Cancelable).
        /// </summary>
        public event EventHandler<CancelVertexEventArgs> VertexDeleting;

        /// <summary>
        /// Event raised after a <see cref="GraphVertex"/> is deleted.
        /// </summary>
        public event EventHandler<GraphVertexEventArgs> VertexDeleted;

        /// <summary>
        /// Event raised before a <see cref="GraphVertex"/> is released (Cancelable).
        /// </summary>
        public event EventHandler<CancelVertexEventArgs> VertexReleasing;

        /// <summary>
        /// Event raised after a <see cref="GraphVertex"/> is released.
        /// </summary>
        public event EventHandler<GuidEventArgs> VertexReleased;

        /// <summary>
        /// Event raised after a <see cref="GraphVertex"/> is Updated.
        /// </summary>
        public event EventHandler<GraphVertexEventArgs> VertexUpdated;

        /// <summary>
        /// Event raised when a <see cref="GraphVertex"/> is asked for that is not in the local db.
        /// This even can be used to try getting the <see cref="GraphVertex"/> from a remote server.
        /// </summary>
        public event EventHandler<GuidEventArgs> VertexMissing;
        #endregion

        public IQueryable<IGraphVertex> Update(IQueryable<IGraphVertex> vertices)
        {
            bool result = true;

            foreach (IGraphVertex vertex in vertices)
            {
                result &= UpdateVertex(vertex);
            }

            return result;
        }

        /// <summary>
        /// Updates the passed vertex.
        /// </summary>
        /// <param name="vertices">The vertices.</param>
        /// <returns></returns>
        /// <remarks>
        /// this method is used to signal that the vertex has been modified in memory
        /// and that we want to commit thoses changes in local persistent storage.
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex list is null.</exception>
        public bool UpdateVertices(IEnumerable<IGraphVertex> vertices)
        {
            bool result = true;

            foreach (IGraphVertex vertex in vertices)
            {
                result &= UpdateVertex(vertex);
            }
            return result;
        }

        /// <summary>
        /// Removes (deletes) the vertices in the given list from both memory and underlying storage .
        /// </summary>
        /// <param name="vertexIdList">The vertex id list.</param>
        /// <returns>True if all vertexes deleted.</returns>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex list is null.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the vertex is not found.</exception>
        public bool RemoveVertices(IEnumerable<TId> vertexIdList)
        {
            int deleted = 0;

            foreach (TId id in vertexIdList)
            {
                if (RemoveVertex(id))
                {
                    deleted++;
                }
            }
            return true;
        }


        /// <summary>
        /// Removes (deletes) the vertex. 
        /// <para>
        /// Removes the memory from the graph, then marks its record as 
        /// DeletePending to be handled by Flush later.
        /// </para>
        /// <para>
        /// Raises <see cref="OnVertexRemoving"/> and <see cref="OnVertexRemoved(GraphVertexEventArgs)"/>.
        /// </para>
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// true if vertex has been deleted, false otherwise.
        /// </returns>
        /// <remarks>
        /// <para>
        /// Invokes  <see cref="OnVertexRemoving"/> prior to
        /// deleting the Vertex, and <see cref="OnVertexRemoved"/> afterwards.
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the vertex is not found.</exception>
        public bool RemoveVertex(TId vertexId)
        {
            //Check Args:
            if (vertexId.IsNotInitialized())
            {
                throw new System.ArgumentNullException("vertexId");
            }

            //You can't remove a vertex that doesn't exist somewhere
            //So one should throw an error requesting the removal
            //of a Guid that doesn't exist.

            //Get from *all*, or load from db into memory (*all*):
            //Raise an error if no vertex found.
            IGraphVertex vertex = GetVertex(vertexId);

            //Invoke the QuickGraph method:
            return RemoveVertex(vertex);

        }




        /// <summary>
        /// Returns the GraphVertex with the specified Id.
        /// Returns null if not found.
        /// Searches memory cache first, and if not found, loads it into memory from storage.    
        /// </summary>
        /// <remarks>
        /// <para>
        /// No error if GraphVertex record not found -- returns null.
        /// </para>
        /// </remarks>
        /// <param name="vertexId">The vertex's unique Id.</param>
        /// <returns>
        /// Return the vertex with the specified id,
        /// or null if the GraphVertex doesn't exist.
        /// </returns>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
        public IGraphVertex<TId> GetVertex(TId vertexId)
        {
            //Check Args:
            if (vertexId.IsNotInitialized())
            {
                throw new System.ArgumentNullException("vertexId");
            }

            GraphVertex vertex;
            if (!TryGetVertex(vertexId, out vertex))
            {
                throw new System.ArgumentException("Vertex not found.");
            }
            return vertex;
        }



        public abstract bool TryGetVertex(TId vertexId, out IGraphVertex<TId> vertex);

        public abstract bool GetVertices(ICollection<TId> vertexIds, ref IGraphVertexList resultCollection, out ICollection<TId> missingVertexIds);

        public abstract bool GetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds,
                                ref IGraphVertexList resultCollection, out ICollection<string> missingRecordIds,
                                bool createIfNotFound);

        public abstract IGraphVertex CreateVertex(int dataObjectFactoryId, string dataRecordId);

        public abstract bool UpdateVertex(IGraphVertex vertex);












        #region Protected - Raise Events

        /// <summary>
        /// Raises the <see cref="VertexCreated"/> event.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="CreateVertex"/>
        /// </para>
        /// </remarks>
        /// <param name="e">The <see cref="GraphVertexEventArgs"/> instance containing the event data.</param>
        protected void OnVertexCreated(GraphVertexEventArgs e)
        {
            if (VertexCreated != null)
            {
                VertexCreated(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="VertexDeleting"/> event.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="RemoveVertex(TId)"/>.
        /// </para>
        /// </remarks>
        /// <param name="e">The <see cref="CancelVertexEventArgs"/> instance containing the event data.</param>
        protected void OnVertexRemoving(CancelVertexEventArgs e)
        {
            if (VertexDeleting != null)
            {
                VertexDeleting(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="VertexDeleted"/> event.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="RemoveVertex"/> if not cancelled.
        /// </para>
        /// </remarks>
        /// <param name="e">The <see cref="GuidEventArgs"/> instance containing the event data.</param>
        protected void OnVertexRemoved(GraphVertexEventArgs e)
        {
            if (VertexDeleted != null)
            {
                VertexDeleted(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="VertexUpdated"/> event.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="UpdateVertex"/>
        /// </para>
        /// </remarks>
        /// <param name="e">The <see cref="GraphVertexEventArgs"/> instance containing the event data.</param>
        protected void OnVertexUpdated(GraphVertexEventArgs e)
        {
            if (VertexUpdated != null)
            {
                VertexUpdated(this, e);
            }
        }


        /// <summary>
        /// Raises the <see cref="VertexMissing"/> event.
        /// </summary>
        /// <internal>
        /// OLIVIER: This is never called....what's it for?
        /// </internal>
        /// <param name="e">The <see cref="GuidEventArgs"/> 
        /// instance containing the event data.</param>
        protected void OnVertexMissing(GuidEventArgs e)
        {
            if (VertexMissing != null)
            {
                VertexMissing(this, e);
            }
        }



        /// <summary>
        /// Raises the <see cref="VertexReleasing"/> event.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked before a non-deleted <see cref="GraphVertex"/> has been released from memory.
        /// </para>
        /// </remarks>
        /// <param name="e">The <see cref="CancelVertexEventArgs{TVertex}"/> instance containing the event data.</param>
        protected void OnVertexReleasing(CancelVertexEventArgs e)
        {
            if (VertexReleasing != null)
            {
                VertexReleasing(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="VertexReleased"/> event.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked after a <see cref="GraphVertex"/> has been released from memory.
        /// </para>
        /// </remarks>
        /// <param name="e">The <see cref="GraphVertexEventArgs"/> instance containing the event data.</param>
        protected void OnVertexReleased(GuidEventArgs e)
        {
            if (VertexReleased != null)
            {
                VertexReleased(this, e);
            }
        }


#if ZERO
    /// <summary>
    /// Raises the <see cref="DataObjectFillRequest"/> event.
    /// </summary>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual protected void OnDataObjectFillRequest(DataObjectEventArgs e) {
      if (DataObjectFillRequest != null) {
        DataObjectFillRequest(this, e);
      } else {
        throw new System.Exception("OnDataObjectFillRequest was called, but there was no Handler defined for it.");
      }
    }
#endif

        #endregion


    }
}
