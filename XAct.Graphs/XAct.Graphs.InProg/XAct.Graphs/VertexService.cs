﻿using System;
using System.Collections.Generic;
using System.Linq;
using XAct.Diagnostics;

namespace XAct.Graphs
{
    public class VertexService : VertexServiceBase
    {

        public VertexService(ITracingService tracingService, IGraphRepository graphRepository)
            :
            base(tracingService,graphRepository)
        {
        }


        /// <summary>
        /// Tries to return the Vertex with the given Id.
        /// <para>
        /// Unlike <see cref="GetVertex(Guid)"/>, does not throw an error if the vertex is not found.
        /// </para>
        /// </summary>
        /// <param name="vertexId"></param>
        /// <param name="vertex"></param>
        /// <returns></returns>
        public bool TryGetVertex(Guid vertexId, out IGraphVertex vertex)
        {
            //Check Args:
            if (vertexId == Guid.Empty)
            {
                throw new System.ArgumentNullException("vertexId");
            }

            lock (this)
            {
                //See if the vertex is already in mem (*all* collection):
                vertex = _inMemGraphService.Vertices.GetVertex(vertexId);

                if (vertex == null)
                {
                    // not in memory, so see if we can load it up from local storage
                    // into *all* collection:
                    vertex = _graphRepository.storageVertexGet(vertexId);
                }
            }

            return (vertex != null);
        }



        /// <summary>
        /// Gets the vertices that match the specified GraphVertex 
        /// Searches memory cache first, and if not found, loads it into memory from storage.    
        /// </summary>
        /// <remarks>
        /// 	<para>
        /// Collects together the Vertexes from memory,
        /// and if not found in memory, searchs within storage to complete the collection.
        /// </para>
        /// <para>
        /// No error is raised if not all vertices are found, but it does return <c>False</c>
        /// in that case.
        /// </para>
        /// </remarks>
        /// <param name="vertexIds">A list of </param>
        /// <param name="resultCollection">A vertex collection to append the results to.</param>
        /// <param name="missingVertexIds"></param>
        /// <returns>True if all vertices found.</returns>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertexIds is null.</exception>
        /// <exception cref="ArgumentNullException">An exception is raised if the resultCollection is null.</exception>
        public bool GetVertices(ICollection<Guid> vertexIds, ref IGraphVertexList resultCollection, out ICollection<Guid> missingVertexIds)
        {
            if (vertexIds == null)
            {
                throw new System.ArgumentNullException("vertexIds");
            }
            if (resultCollection == null)
            {
                throw new System.ArgumentNullException("resultCollection");
            }

            lock (this)
            {
                // First, get vertices already in memory,
                //without raising an error that there are some missing:
                ICollection<Guid> missingInMemVertexIds;
                if (_inMemGraphService.Vertices.inMemGetVertices(vertexIds, ref resultCollection, out missingInMemVertexIds))
                {
                    //Clear the 'missing' collection:
                    missingVertexIds = null;
                    return true;
                }
                else
                {
                    missingVertexIds = missingInMemVertexIds;
                    //If there were any that were missing,
                    //use that list to query in storage:
                    ICollection<Guid> missingStorageVertexIds;
                    if (_graphRepository.storageGetVertices(missingInMemVertexIds, ref resultCollection, out missingStorageVertexIds))
                    {
                        //Clear the 'missing' collection:
                        missingVertexIds = null;
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the vertices from the specified data source id and
        /// with the specified objects 
        /// </summary>
        /// <remarks>
        /// <para>
        /// No error is raised if not all vertices are found, but it does return <c>False</c>
        /// in that case.
        /// </para>
        /// <para>
        /// Searches for vertices in memory first. Any not found are then searched for in storage.
        /// </para>
        /// </remarks>
        /// <param name="dataObjectFactoryId">The data source id.</param>
        /// <param name="dataObjectIds">The dataObject </param>
        /// <param name="resultCollection">A vertex collection to append the results to.</param>
        /// <param name="missingRecordIds">A result collection of the record ids not found.</param>
        /// <param name="createIfNotFound">if <c>true</c>, 
        /// and if there is object ids with no correspondant vertices then the
        /// vertices are created on the fly.</param>
        /// <returns>True if vertices for each record id were found.</returns>
        /// <exception cref="ArgumentNullException">An exception is raised if dataObjectFactoryId is 0.</exception>
        /// <exception cref="ArgumentNullException">An exception is raised if dataObjectIds is null.</exception>
        public bool GetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds, ref IGraphVertexList resultCollection, out ICollection<string> missingRecordIds, bool createIfNotFound)
        {
            //Check Args:
            if (dataObjectFactoryId == 0)
            {
                throw new System.ArgumentNullException("dataObjectFactoryId");
            }
            if (dataObjectIds == null)
            {
                throw new System.ArgumentNullException("dataObjectIds");
            }
            lock (this)
            {
                ICollection<string> inMemMissingRecordIds;
                // First, get vertices already in memory:
                if (_inMemGraphService.Vertices.inMemGetVertices(dataObjectFactoryId, dataObjectIds, ref resultCollection, out inMemMissingRecordIds))
                {
                    //Have all vertices:
                    missingRecordIds = null;
                    return true;
                }
                //Missing some vertices:
                missingRecordIds = inMemMissingRecordIds;


                //Get the missing vertices from local storage:
                ICollection<string> storageMissingRecordIds;
                if (_graphRepository.storageGetVertices(dataObjectFactoryId, inMemMissingRecordIds, ref resultCollection, out storageMissingRecordIds))
                {
                    //All found.
                    missingRecordIds = null;
                    return true;
                }

                missingRecordIds = storageMissingRecordIds;


                if (createIfNotFound)
                {
                    // create remaining missing vertices ...
                    IGraphVertexList newVertices = new IGraphVertexList();
                    Collections.ForEach<string>(missingRecordIds,
                        delegate(string recordId)
                        {
                            traceInformation("adding missing vertex for data source = {0} record id = {0}",
                                dataObjectFactoryId, recordId);
                            newVertices.Add(CreateVertex(dataObjectFactoryId, recordId));
                        });
                    resultCollection.AddRange(newVertices);
                    //Clear the missing list:
                    missingRecordIds = null;
                    return true;
                }//End:createIfNotFound
            }//End:Lock


            return false;
        }



        /// <summary>
        /// Create a new GraphVertex with the specified data source id and data record id.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes <see cref="GraphProviderBase.OnVertexCreated"/>.
        /// </para>
        /// <para>
        /// The <see cref="P:Vertex.Data"/> property is set to <c>null</c>.
        /// </para>
        /// <para>
        /// A potential candidate for something to do with 
        /// <see cref="QuickGraph.IVertexFactory{TVertex}"/>
        /// </para>
        /// </remarks>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <param name="dataObjectFactoryId">The data source id.</param>
        /// <param name="dataRecordId">The data record id.</param>
        /// <returns>a newly created vertex</returns>
        /// <exception cref="ArgumentNullException">An Exception is raised if dataObjectFactoryId is 0.</exception>
        /// <exception cref="ArgumentNullException">An Exception is raised if dataRecordId is empty.</exception>
        public GraphVertex CreateVertex(int dataObjectFactoryId, string dataRecordId)
        {
            //Check Args:
            if (dataObjectFactoryId == 0)
            {
                throw new System.ArgumentNullException("dataObjectFactoryId");
            }
            if (string.IsNullOrEmpty(dataRecordId))
            {
                throw new System.ArgumentNullException("dataRecordId");
            }

            IGraphVertex vertex = new GraphVertex();
            ((IVertexInitialization)vertex).InitializeAsNew(dataObjectFactoryId, dataRecordId);

            AddVertex(vertex);

            /* NEVER UPDATE HERE...WAIT TILL UPDATE CREATED:
            if (UpdateInRealTime) {
                Flush(vertex);
            }
            */

            OnVertexCreated(new GraphVertexEventArgs(vertex));

            return vertex;
        }


        /// <summary>
        /// Updates the vertex.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes <see cref="OnVertexUpdated"/>.
        /// </para>
        /// </remarks>
        /// <param name="vertex">The vertex.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex is null.</exception>
        public bool UpdateVertex(IGraphVertex vertex)
        {
            //Check Args:
            if (vertex == null)
            {
                throw new System.ArgumentNullException("vertex");
            }

            //Get from *all*, or load from db into memory (*all*):
            GraphVertex checkVertex = GetVertex(vertex.Id);


            //OLIVIER: What is this '(checkVertex != vertex)' check for really???
            if (checkVertex != vertex)
            {
                //#warning "we should replace the old (checkVertex) vertex with the new one (vertex)"
                throw new Exception("update vertex error (checkVertex != vertex)");
            }


            //Update the vertex edited date:
            //(IMPORTANT: Which also will set the the Status to UpdatePending):
            vertex.LastModifiedOn = DateTime.Now.ToUniversalTime();

            if (UpdateInRealTime)
            {
                //Keep the storage up to date, rather than waiting 
                //for a future time:
                Flush(vertex);
            }

            //Raise an event that the GraphVertex has been updated (in memory only at this point):
            OnVertexUpdated(new GraphVertexEventArgs(vertex));

            //Always true:
            return true;
        }


    }
}
