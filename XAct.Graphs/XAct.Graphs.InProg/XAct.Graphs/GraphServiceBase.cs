﻿
using System;

namespace XAct.Graphs
{
    public abstract class GraphServiceBase<TId>
    {

        #region Events
        /// <summary>
        /// Event raised when a vertex is added to the graph.
        /// </summary>
        public event EventHandler<IGraphVertex<TId>> VertexAdded;

        /// <summary>
        /// Event raised when a vertex is removed from the graph.
        /// </summary>
        public event EventHandler<IGraphVertex<TId>> VertexRemoved;
        #endregion



        #region Raise Events
        /// <summary>
        /// Raises the <see cref="VertexAdded"/> event.
        /// </summary>
        /// <param name="args">The <see cref="QuickGraph.VertexEventArgs{TVertex}"/> instance containing the event data.</param>
        virtual protected void OnVertexAdded(QuickGraph.VertexEventArgs<IGraphVertex<TId>> args)
        {
            if (VertexAdded != null)
            {
                VertexAdded(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="VertexRemoved"/> event.
        /// </summary>
        /// <param name="args">The <see cref="QuickGraph.VertexEventArgs{TVertex}"/> instance containing the event data.</param>
        virtual protected void OnVertexRemoved(QuickGraph.VertexEventArgs<IGraphVertex<TId>> args)
        {
            if (VertexRemoved != null)
            {
                VertexRemoved(this, args);
            }
        }
        #endregion


    }

}
