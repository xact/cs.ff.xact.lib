﻿
using System;

namespace XAct.Graphs
{
    public class GraphCleanupService : IGraphCleanupService
    {
        private readonly IGraphCleanupSettings _graphCleanupSettings;


        #region Fields - InMem Graph Cleanup Thread related


        /// <summary>
        ///     timer thread who launch *periodically* the cleanup routine
        /// </summary>
        protected Timer _CleanupTimer;

        /// <summary>
        /// Gets the interval (in Seconds) between Cleanup sweeps by the Cleanup thread.
        /// Default: 5 Minutes.
        /// </summary>
        /// <value>The cleanup interval (in seconds).</value>
        public int CleanupInterval
        {
            get
            {
                return _CleanupInterval;
            }
        }
        private int _CleanupInterval = 5 * 60;


        /// <summary>
        /// The Minimum TimeSpan for an unused GraphVertex before we 
        /// deam it as 'stale' and ready for cleanup.
        /// Default: 20 minutes. 
        /// </summary>
        public TimeSpan CleanupVertexTimeout
        {
            get
            {
                return _cleanupVertexTimeout;
            }
        }
        private TimeSpan _cleanupVertexTimeout = new TimeSpan(0, 20, 0);

        /// <summary>
        /// The minimum number of GraphVertex elements to allow in cache before 
        /// we procceed with looking for 'stale' (see <see cref="CleanupVertexTimeout"/>) GraphVertex elements. 
        /// Default: 500 GraphVertex elements.
        /// </summary>
        public int CleanupVertexCountThreshold
        {
            get
            {
                return _CleanupVertexCountThreshold;
            }
        }
        private int _CleanupVertexCountThreshold = 500;

        #endregion

        #region Fields - InMem Graph Flush thread related


        /// <summary>
        /// Timer thread which launches 
        /// periodically the <see cref="OnFlushEvent"/>.
        /// </summary>
        protected Timer _FlushTimer;

        /// <summary>
        /// Gets the interval (in Seconds) between Flush sweeps by the Flush thread.
        /// Default: 2 Minutes.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The Flush thread scans all vertices and edges at regular intervals
        /// and persists to local storage any that have changes not yet recorded. 
        /// </para>
        /// </remarks>
        /// <value>The Flush interval (in seconds).</value>
        public int FlushInterval
        {
            get
            {
                return _FlushInterval;
            }
            set
            {
                _FlushInterval = value;
                if (_FlushTimer != null)
                {
                    _FlushTimer.Interval = (_FlushInterval * 1000);
                }
            }
        }
        private int _FlushInterval = 30;

        #endregion



        public GraphCleanupService(IGraphCleanupSettings graphCleanupSettings)
        {
            _graphCleanupSettings = graphCleanupSettings;
        }
    }
}
