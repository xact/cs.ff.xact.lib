﻿using System;
using System.Collections.Generic;
using XAct.Diagnostics;

namespace XAct.Graphs
{
    public class GraphService: IGraphService
    {
        
        public GraphService(ITracingService tracingService, IGraphRepository graphRepository)
        {
            
        }


        #region Public Methods - Implementation of GraphProviderBase - Vertices

        /// <summary>
        /// Performs a DepthFirstSearch starting from the given vertex,
        /// ensuring the vertices are in Memory.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <param name="depth">The max depth (default is 4).</param>
        public void Spider(GraphVertex vertex, int depth)
        {
            if (depth == 0)
            {
                depth = 4;
            }
            DepthFirstUndirectedSearchAlgorythm search = new DepthFirstUndirectedSearchAlgorythm(this, vertex, depth);
            search.Compute(vertex);
            //QuickGraph.Algorithms.Observers.VertexRecorderObserver<GraphVertex,GraphEdge>
        }

        #endregion

    }
}
