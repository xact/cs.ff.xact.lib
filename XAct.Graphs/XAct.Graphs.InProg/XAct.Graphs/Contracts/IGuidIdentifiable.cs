﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;

namespace XAct.Graphs {

	/// <summary>
	/// Interface for elements identifieable by a unique Id (such as GraphVertex).
	/// </summary>
	public interface IGuidIdentifiable {

		/// <summary>
		/// Gets the unique Id of this instance.
		/// </summary>
		/// <value>The unique Id.</value>
		Guid Id { get; }

	}
}
