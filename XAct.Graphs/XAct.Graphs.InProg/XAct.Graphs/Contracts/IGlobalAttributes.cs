﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs {
	/// <summary>
	/// Interface/Contract for elements that have shared attributes.
	/// <para>
	/// See also <see cref="IUserVertex"/>.
	/// </para>
	/// </summary>
	public interface  IGlobalAttributes {
		/// <summary>
		/// The shared attributes of the element.
		/// </summary>
				GraphVertexAttributeDictionary GlobalAttributes {get;}
	}
}
