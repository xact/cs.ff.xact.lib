﻿
using System;
using XAct.Domain;

namespace XAct.Graphs
{
    public interface IGraphEdge<TId> : QuickGraph.IEdge<IGraphVertex>, IEdgeDef<TId>, IWeighted, IModelState, IComparable 
    {

        
    }
}
