﻿

namespace XAct.Graphs
{
    public interface IGraphService<TId>
    {
        IVertexService<TId> Vertices { get; }

        IEdgeService<TId> Edges { get; }
    }
}
