﻿

namespace XAct.Graphs {
	/// <summary>
	/// Public Interface for Graph based on QuickGraph.
	/// </summary>
	public interface IGraphEngine : 
		IQuickGraphWrapper<GraphVertex, GraphEdge> {

	}
}
