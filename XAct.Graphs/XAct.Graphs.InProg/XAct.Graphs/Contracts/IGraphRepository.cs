﻿using System;
using System.Collections.Generic;

namespace XAct.Graphs
{
    public interface IGraphRepository 
    {
        #region Abstract Protected Methods - storage / GraphVertex related

        /// <summary>
        /// Create a new  vertex record in the local storage.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
        void storageVertexRecordCreate(ICollection<GraphVertex> vertex);

        /// <summary>
        ///  Updates the vertex record in the local storage.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
        void storageVertexRecordUpdate(ICollection<GraphVertex> vertex);


        /// <summary>
        ///  Mark the given vertex as deleted in the local storage's vertex record. 
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that this only marks the local vertex record
        /// for Remote deletion. It does not remove the vertex record.
        /// </para>
        /// </remarks>
        /// <param name="vertexId">The vertex Id.</param>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is Empty.</exception>
        void storageVertexRecordMarkForDeletion(ICollection<Guid> vertexId);

        /// <summary>
        /// Suppress in the local storage the vertex record 
        /// and its adjacent edges records.
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertexId argument is Empty.</exception>
        void storageVertexRecordDelete(ICollection<Guid> vertexId);



        /// <summary>
        /// Gets the vertex from the local storage.
        /// Returns null if not found.
        /// </summary>
        /// <remarks>
        /// <para>
        /// No error if vertex record not found -- returns null.
        /// </para>
        /// <para>
        /// Invoked by <see cref="IVertexService.GetVertex"/> when the 
        /// vertex cannot be found already in memory.
        /// </para>
        /// </remarks>
        /// <param name="vertexId">The vertex's unique Id.</param>
        /// <returns>
        /// Return the vertex with the specified id,
        /// or null if the vertex doesn't exist.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is Empty.</exception>
        GraphVertex storageVertexGet(Guid vertexId);



        /// <summary>
        /// Get a collection of vertex elements from storage that match the ids given.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Called by the public 
        /// <see cref="IVertexService.GetVertices(ICollection{Guid},ref IGraphVertexList, out ICollection{Guid})"/> method, 
        /// for all vertices not found by <see cref="GraphProviderBase.inMemGetVertices(ICollection{Guid}, ref GraphVertexList, out ICollection{Guid})"/>.
        /// </para>
        /// </remarks>
        /// <param name="vertexIds">Ids to look for.</param>
        /// <param name="resultCollection">Result Collection of vertex elements found.</param>
        /// <param name="vertexIdsNotFound">Result List of vertex Ids not found.</param>
        /// <returns>True if all vertices are found.</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the arguments are null.</exception>
        bool storageGetVertices(ICollection<Guid> vertexIds, ref IGraphVertexList resultCollection, out ICollection<Guid> vertexIdsNotFound);


        /// <summary>
        /// Gets the vertices from the specified data source id and
        /// with the specified objects 
        /// </summary>
        /// <remarks>
        /// <para>
        /// Called by the public 
        /// <see cref="GraphProviderBase.GetVertices(int, ICollection{string}, ref GraphVertexList, out ICollection{string}, bool)"/> method,
        /// for all vertices not found by 
        /// <see cref="inMemGetVertices(int, ICollection{string}, ref GraphVertexList, out ICollection{string})"/>.
        /// </para>
        /// </remarks>
        /// <param name="dataObjectFactoryId">The data source id.</param>
        /// <param name="dataObjectIds">The dataObject </param>
        /// <param name="resultCollection">Result Collection of vertex elements found.</param>
        /// <param name="dataObjectIdsNotFound">Result List of dataObject Ids not found.</param>
        /// <returns>True if vertex elements for all data objects ids were found.</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the arguments are null.</exception>
        bool storageGetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds, ref IGraphVertexList resultCollection, out ICollection<string> dataObjectIdsNotFound);
        #endregion




        #region Abstract Protected Methods - storage / VertexIds related
        /// <summary>
        /// Get a collection of vertex Ids that are neighbours
        /// to the specified vertex 
        /// </summary>
        /// <param name="vertexIds">The vertex </param>
        /// <param name="clientMachineId">The client machine id.</param>
        /// <param name="depth">The depth.</param>
        /// <returns>A collection of vertex Ids (never null, but can be of size 0).</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
        List<Guid> storageVertexIdsGetInOutVertexIds(ICollection<Guid> vertexIds, Guid clientMachineId, int depth);


        /// <summary>
        /// Get a collection of vertex Ids that are anscestors
        /// to the specified vertex 
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="storageVertexIdsGetInOutVertexIds"/>.
        /// </para>
        /// </remarks>
        /// <param name="vertexIds">The vertex </param>
        /// <param name="depth">The depth.</param>
        /// <returns>
        /// A collection of vertex Ids (never null, but can be of size 0).
        /// </returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
        List<Guid> storageVertexIdsGetInVertexIds(ICollection<Guid> vertexIds, int depth);


        /// <summary>
        /// Get a collection of vertex Ids that are descendents
        /// to the specified vertex 
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="storageVertexIdsGetInOutVertexIds"/>.
        /// </para>
        /// </remarks>
        /// <param name="vertexIds">The vertex </param>
        /// <param name="depth">The depth.</param>
        /// <returns>
        /// A collection of vertex Ids (never null, but can be of size 0).
        /// </returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexIds argument is null.</exception>
        List<Guid> storageVertexIdsGetOutVertexIds(ICollection<Guid> vertexIds, int depth);
        #endregion

        #region Abstract Protected Methods - storage / GraphVertex Attributes related
        /// <summary>
        /// Load attribute values from storage into the Vertex's user attributes array.
        /// Returns true only if attributes were found in storage.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns>true if attributes found.</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
        bool storageVertexUserAttributesLoad(GraphVertex vertex);

        /// <summary>
        /// Save the Vertex's user attributes to storage.
        /// </summary>
        /// <param name="vertices">The vertices.</param>
        /// <returns>true if update.</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
        bool storageVertexUserAttributesSave(ICollection<GraphVertex> vertices);
        #endregion

        #region Abstract Protected Methods - storage / GraphEdge related

        /// <summary>
        /// Returns the count of all edges in persistent storage.
        /// <para>
        /// Not the same as <see cref="EdgeCountInMemory"/>.
        /// </para>
        /// </summary>
        /// <returns>Count of all Edge records in persistent storage.</returns>
        int storageEdgeCount();


        /// <summary>
        /// Enumerates through all edge records in persisten storage.
        /// <para>
        /// See Remarks.
        /// </para>
        /// </summary>
        /// <remarks>
        /// Because this creates new Edge instances, the results will not
        /// be the same instance as what already exists in <see cref="_CachedVertexInEdges"/>
        /// and <see cref="_CachedVertexOutEdges"/>, so you have to be careful to compare them by
        /// <see cref="IEdgeDef"/>.
        /// </remarks>
        /// <returns></returns>
        IEnumerable<GraphEdge> storageEdgeEnumerateAll();

        /// <summary>
        /// Create the Edge's record in the local storage.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Transacted, so if operation fails, rolls back.
        /// </para>
        /// </remarks>
        /// <param name="edges">The edges.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
        void storageEdgeRecordCreate(ICollection<GraphEdge> edges);


        /// <summary>
        /// Update the Edge's record in local storage.
        /// </summary>
        /// <param name="edges">The edges.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
        void storageEdgeRecordUpdate(ICollection<GraphEdge> edges);

        /// <summary>
        /// Update the Edge's record in local storage to indicate that the GraphEdge is deleted.
        /// IMPORTANT: that this is not a Delete, but a setting of the record's Deleted flag to true.
        /// </summary>
        /// <param name="edges">The edges.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
        void storageEdgeRecordMarkForDeletion(ICollection<GraphEdge> edges);

        /// <summary>
        /// Delete the Edge's record from the local storage.
        /// IMPORTANT: This actually deletes the record from the local storage (unlike <see cref="storageEdgeRecordMarkForDeletion"/>).
        /// </summary>
        /// <param name="edges">The Collection of Edges.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the edge argument is null.</exception>
        void storageEdgeRecordDelete(ICollection<GraphEdge> edges);

        /// <summary>
        /// Get from the local storage the an GraphEdgeList of the in Edges for the specified Vertex.
        /// </summary>
        /// <param name="vertices">A Collection of Vertices.</param>
        /// <returns>A Collection of Edges.</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
        IGraphEdgeList storageVertexGetInEdges(ICollection<GraphVertex> vertices);


        /// <summary>
        /// Get from the local storage the an GraphEdgeList of the out Edges for the specified Vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the argument is null.</exception>
        IGraphEdgeList storageVertexGetOutEdges(GraphVertex vertex);
        #endregion

        #region Abstract Protected Methods - storage / Syncronization
        /// <summary>
        /// Get Ids of all new (DateCreated>LastSyncDate) Vertices 
        /// pointing to records within a collection of DataSources, 
        /// that are not marked as already transmitted to Client.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Because the results depends on the correct status of
        /// the what has correctly been received by the client, 
        /// ensure that <see cref="storageForSyncMarkTransmissionAborted"/>
        /// before this method is called.
        /// </para>
        /// <para>
        /// Note that this method does not load vertices into in-memory cache,
        /// (although the use of <c>GetVertices</c> could be used subsequently
        /// to do this).
        /// </para>
        /// </remarks>
        /// <param name="dataSourceIds">Iterable Collection of DataSource </param>
        /// <param name="clientMachineId">The unique Id of the client machine.</param>
        /// <param name="lastSyncDate">The date of the last Sync request.</param>
        /// <returns>A collection of vertex Ids (never null, but can be of size 0).</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
        Guid[] storageForSyncGetNewVertexIds(ICollection<int> dataSourceIds, Guid clientMachineId, DateTime lastSyncDate);


        /// <summary>
        /// Get Ids of all new Vertices (DateCreated>LastSyncDate) 
        /// pointing to specified records in a specified datasource,
        /// that are not marked as already transmitted to client.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Because the results depends on the correct status of
        /// the what has correctly been received by the client, 
        /// ensure that <see cref="storageForSyncMarkTransmissionAborted"/>
        /// before this method is called.
        /// </para>
        /// <para>
        /// Note that this method does not load vertices into in-memory cache.
        /// </para>
        /// </remarks>
        /// <param name="dataObjectFactoryId">A single dataSource id.</param>
        /// <param name="dataObjectIds">An iterable Collection of dataObject record </param>
        /// <param name="clientMachineId">The unique Id of the client machine.</param>
        /// <param name="lastSyncDate">The date of the last Sync request.</param>
        /// <returns>A collection of vertex Ids (never null, but can be of size 0).</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
        Guid[] storageForSyncGetNewVertexIds(int dataObjectFactoryId, ICollection<string> dataObjectIds, Guid clientMachineId, DateTime lastSyncDate);


        /// <summary>
        /// Get Ids of all updated (DateModified>LastSyncDate) Vertices 
        /// pointing to records within a collection of DataSources, 
        /// that are not marked as already transmitted to Client.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Because the results depends on the correct status of
        /// the what has correctly been received by the client, 
        /// ensure that <see cref="storageForSyncMarkTransmissionAborted"/>
        /// before this method is called.
        /// </para>
        /// <para>
        /// Note that this method does not load vertices into in-memory cache
        /// (although the use of <c>GetVertices</c> could be used subsequently
        /// to do this).
        /// </para>
        /// </remarks>
        /// <param name="clientMachineId">The unique Id of the client machine.</param>
        /// <param name="lastSyncDate">The date of the last Sync request.</param>
        /// <returns>A collection of vertex Ids (never null, but can be of size 0).</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
        Guid[] storageForSyncGetUpdatedVertexIds(Guid clientMachineId, DateTime lastSyncDate);



        /// <summary>
        /// Get a collection of vertex Ids that have been deleted
        /// on Server, that are still present on Client.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Because the results depends on the correct status of
        /// the what has correctly been received by the client, 
        /// ensure that <see cref="storageForSyncMarkTransmissionAborted"/>
        /// before this method is called.
        /// </para>
        /// <para>
        /// The ID's returned are to Vertices that no longer exist 
        /// in this graph.
        /// </para>
        /// </remarks>
        /// <param name="clientMachineId">The unique Id of the client machine.</param>
        /// <returns>A collection of vertex Ids (never null, but can be of size 0).</returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
        Guid[] storageForSyncGetDeletedVertexIds(Guid clientMachineId);


        /// <summary>
        /// Mark that the given vertex Ids are being transmitted to the client.
        /// </summary>
        /// <remarks>
        /// <para>
        /// If successful, call <see cref="storageForSyncMarkTransmissionSuccessful"/>,
        /// if not, invoke <see cref="storageForSyncMarkTransmissionAborted"/> to clean up.
        /// </para>
        /// </remarks>
        /// <param name="clientMachineId">The client machine id.</param>
        /// <param name="vertexIds">The vertex </param>
        void storageForSyncMarkTransmissionBegun(Guid clientMachineId, ICollection<Guid> vertexIds);

        /// <summary>
        /// Deletes references to any Vertices that were transmitted to a client
        /// but didn't make it (eg: communication was interrupted).
        /// </summary>
        /// <remarks>
        /// <para>
        /// <b>Important:</b><br/>
        /// In order to not get false results, this method must be called prior to invoking
        /// <see cref="storageForSyncGetNewVertexIds(ICollection{int}, Guid, DateTime)"/>,
        /// <see cref="storageForSyncGetNewVertexIds(int, ICollection{string}, Guid , DateTime)"/>,
        /// <see cref="storageForSyncGetUpdatedVertexIds"/>, or
        /// <see cref="storageForSyncGetDeletedVertexIds"/>.
        /// </para>
        /// <para>
        /// Invoked to cleanup a transmission that was organized
        /// with the help of <see cref="storageForSyncMarkTransmissionBegun"/>.
        /// </para>
        /// </remarks>
        /// <param name="clientMachineId">The client machine id.</param>
        void storageForSyncMarkTransmissionAborted(Guid clientMachineId);


        /// <summary>
        /// Marks all vertices that are being transmitted, as being received by client.
        /// </summary>
        /// <remarks>
        /// <para>
        /// </para>
        /// </remarks>
        /// <param name="clientMachineId">The client machine id.</param>
        void storageForSyncMarkTransmissionSuccessful(Guid clientMachineId);


        /// <summary>
        /// Marks the vertex as having been successfully transmitted to the client.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Used by the GraphSyncManager upon receiving a request to create a new vertex.
        /// In other words, it is creating a Server vertex of a vertex that for sure already
        /// exists on the client, which is in all senses the same as a local server vertex
        /// having been transmitted to the remote client...
        /// </para>
        /// </remarks>
        /// <param name="clientMachineId">The client machine id.</param>
        /// <param name="vertexId">The id of the vertex.</param>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is null.</exception>
        void storageForSyncMarkVertexIdAsSent(Guid clientMachineId, Guid vertexId);
        //--------------------------
        //--------------------------
        //--------------------------


        /// <summary>
        /// Removes the reference to the vertex from the VerticesSent table.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Used by the GraphSyncManager upon receiving a request to delete an existing Vertex,
        /// undoing the work done by <see cref="storageForSyncMarkVertexIdAsSent"/>.
        /// </para>
        /// </remarks>
        /// <param name="clientMachineId">The client machine id.</param>
        /// <param name="vertexId">The id of the vertex.</param>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is null.</exception>
        void storageForSyncRemoveVertexIdFromVerticesSent(Guid clientMachineId, Guid vertexId);

        // void storageGetImplied(ICollection<Guid> vertexIds, int depth);
        #endregion

    
    }
}
