using System;
using System.Collections.Generic;
using System.Text;
using XAct.Data;

namespace XAct.Graphs {
  /// <summary>
  /// Interface for Vertices that implement attributes on a per user basis.
  /// </summary>
  public interface IUserVertex {

    /// <summary>
    /// Get the whole UserAttribute package.
		/// <para>
		/// IMPORTANT:
		/// </para>
		/// <para>
		/// Protect access to this <code>get;</code>.
		/// </para>
    /// </summary>
    IDictionary<object, GraphVertexAttributeDictionary> UserAttributes {
      get;
    }
  }//Class:End
}//Namespace:End