﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs {

	/// <summary>
	/// Interface for vertices that are partially synchronizeable.
	/// </summary>
	public interface ISyncVertex {
		/// <summary>
		/// Gets a value indicating whether this element is a local proxy 
		/// for an element that is known (eg from an Edge), but not yet been received with a server.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is a local proxy; otherwise, <c>false</c>.
		/// </value>
		GraphRetrievalStatus RetrievalStatus { get; }
	}
}
