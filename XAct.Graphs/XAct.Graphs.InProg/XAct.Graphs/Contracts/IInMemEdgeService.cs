﻿
using System.Collections.Generic;

namespace XAct.Graphs
{
    public interface IInMemEdgeService<TId> : IEdgeService<TId>
    {
        /// <summary>
        /// Gets the number of Edge elements in memory.
        /// <para>
        /// Which is not the same as <see cref="QuickGraph.IEdgeListGraph{GraphVertex, GraphEdge}.EdgeCount"/>.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Most graphs algorythms that deal with the edge count will want to know
        /// about all the edges in the graph -- not just the ones in memory.
        /// </para>
        /// </remarks>
        /// <value>The edges in memory count.</value>
        int EdgeCountInMemory { get; }

        /// <summary>
        /// Gets the edges in memory.
        /// </summary>
        /// <value>The edges in memory.</value>
        IEnumerable<IGraphEdge<TId>> EdgesInMemory { get; }
}
}
