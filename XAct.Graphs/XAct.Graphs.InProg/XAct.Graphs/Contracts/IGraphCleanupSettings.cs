﻿namespace XAct.Graphs
{
    public interface IGraphCleanupSettings
    {
        /// <summary>
        /// Cleanup Thread Interval interval (in seconds)
        /// </summary>
        int CleanupInterval { get; set; }
        /// <summary>
        /// Minimum number of vertices in memory before we cleanup
        /// </summary>
        int CleanupMinVertices { get; set; }
        /// <summary>
        /// Cleanup thread's GraphVertex timeout value.
        /// </summary>
        int CleanupTimeOut { get; set; }
        /// <summary>
        /// flush thread interval (in seconds)
        /// </summary>
        int FlushInterval { get; set; }
    }
}