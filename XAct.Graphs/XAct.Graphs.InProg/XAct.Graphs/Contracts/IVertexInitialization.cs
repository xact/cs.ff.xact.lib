﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Graphs  {
	/// <summary>
	/// TODO
	/// </summary>
	public interface IVertexInitialization {



		/// <summary>
		/// Initializes the vertex as a proxy.
		/// </summary>
		/// <param name="vertexId">The vertex id.</param>
		/// <exception cref="ArgumentNullException">An Exception is raised if vertexId is empty.</exception>
		void InitializeAsProxy(Guid vertexId);

		/// <summary>
		/// Initializes the object as a new record.
		/// </summary>
		/// <param name="dataObjectFactoryId">The data object factory id.</param>
		/// <param name="dataRecordId">The data record id.</param>
		void InitializeAsNew(int dataObjectFactoryId, string dataRecordId);


		/// <summary>
		/// Initializes a new instance of the <see cref="IGraphVertex"/> class.
		/// Invoked by Constructor.
		/// </summary>
		/// <param name="vertexId">The vertex id.</param>
		/// <param name="weight">The Vertex's weight.</param>
		/// <param name="dataObjectFactoryId">The dataObject's datasource's id.</param>
		/// <param name="dataRecordId">The dataObject's id.</param>
		/// <param name="utcDateCreated">The date created.</param>
		/// <param name="utcDateEdited">The date edited.</param>
		/// <param name="syncStatus">The sync status.</param>
		/// <exception cref="ArgumentNullException">An Exception is raised if vertexId is Guid.Empty.</exception>
		/// <exception cref="ArgumentNullException">An Exception is raised if dataObjectFactoryId is 0.</exception>
		/// <exception cref="ArgumentNullException">An Exception is raised if dataRecordId is empty.</exception>
		void InitializeFromDataStore(Guid vertexId, int weight, int dataObjectFactoryId, string dataRecordId, DateTime utcDateCreated, DateTime utcDateEdited, OfflineModelState syncStatus);

	}
}
