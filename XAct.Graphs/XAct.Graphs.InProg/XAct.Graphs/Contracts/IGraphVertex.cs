﻿using System;
using XAct.Domain;
using XAct.Data;

namespace XAct.Graphs
{
    public interface IGraphVertex<TId> : IGlobalAttributes, IUserVertex, IDataSourceIdentified, IAuditable, IModelState, IDisposable, ISyncVertex, IVertexInitialization 
    {
        TId Id { get; }
    }
}
