﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XAct.Graphs
{
    public interface IVertexService<TId>
    {

        #region Events Raised
        /// <summary>
        /// Event raised after a <see cref="GraphVertex"/> is created.
        /// </summary>
        event EventHandler<GraphVertexEventArgs> VertexCreated;

        /// <summary>
        /// Event raised before a <see cref="GraphVertex"/> is deleted (Cancelable).
        /// </summary>
        event EventHandler<CancelVertexEventArgs> VertexDeleting;

        /// <summary>
        /// Event raised after a <see cref="GraphVertex"/> is deleted.
        /// </summary>
        event EventHandler<GraphVertexEventArgs> VertexDeleted;

        /// <summary>
        /// Event raised before a <see cref="GraphVertex"/> is released (Cancelable).
        /// </summary>
        event EventHandler<CancelVertexEventArgs> VertexReleasing;

        /// <summary>
        /// Event raised after a <see cref="GraphVertex"/> is released.
        /// </summary>
        event EventHandler<GuidEventArgs> VertexReleased;

        /// <summary>
        /// Event raised after a <see cref="GraphVertex"/> is Updated.
        /// </summary>
        event EventHandler<GraphVertexEventArgs> VertexUpdated;
        /// <summary>
        /// Event raised when a <see cref="GraphVertex"/> is asked for that is not in the local db.
        /// This even can be used to try getting the <see cref="GraphVertex"/> from a remote server.
        /// </summary>
        event EventHandler<GuidEventArgs> VertexMissing;

        #endregion



        /// <summary>
        /// Create a new GraphVertex with the specified data source id and data record id.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes <see cref="VertexCreated"/>.
        /// </para>
        /// <para>
        /// The <see cref="IGraphVertex{TId}.Data"/> property is set to <c>null</c>.
        /// </para>
        /// <para>
        /// A potential candidate for something to do with 
        /// <see cref="QuickGraph.IVertexFactory{TVertex}"/>
        /// </para>
        /// </remarks>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <param name="dataObjectFactoryId">The data source id.</param>
        /// <param name="dataRecordId">The data record id.</param>
        /// <returns>a newly created vertex</returns>
        /// <exception cref="ArgumentNullException">An Exception is raised if dataObjectFactoryId is 0.</exception>
        /// <exception cref="ArgumentNullException">An Exception is raised if dataRecordId is empty.</exception>
        IGraphVertex<TId> CreateVertex(int dataObjectFactoryId, string dataRecordId);




        IQueryable<IGraphVertex<TId>> Update(IQueryable<IGraphVertex<TId>> vertices);

        /// <summary>
        /// Updates the passed vertex.
        /// </summary>
        /// <param name="vertices">The vertices.</param>
        /// <returns></returns>
        /// <remarks>
        /// this method is used to signal that the vertex has been modified in memory
        /// and that we want to commit thoses changes in local persistent storage.
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex list is null.</exception>
        bool UpdateVertices(IEnumerable<IGraphVertex<TId>> vertices);
        
        /// <summary>
        /// Removes (deletes) the vertices in the given list from both memory and underlying storage .
        /// </summary>
        /// <param name="vertexIdList">The vertex id list.</param>
        /// <returns>True if all vertexes deleted.</returns>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex list is null.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the vertex is not found.</exception>
        bool RemoveVertices(IEnumerable<TId> vertexIdList);

        /// <summary>
        /// Removes (deletes) the vertex. 
        /// <para>
        /// Removes the memory from the graph, then marks its record as 
        /// DeletePending to be handled by Flush later.
        /// </para>
        /// <para>
        /// Raises <see cref="VertexDeleting"/> and <see cref="VertexDeleted"/>.
        /// </para>
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// true if vertex has been deleted, false otherwise.
        /// </returns>
        /// <remarks>
        /// <para>
        /// Invokes  <see cref="VertexDeleting"/> prior to
        /// deleting the Vertex, and <see cref="VertexDeleted"/> afterwards.
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the vertex is not found.</exception>
        bool RemoveVertex(TId vertexId);


        /// <summary>
        /// Returns the <see cref="IGraphVertex{TId}"/> with the specified Id.
        /// Returns null if not found.
        /// Searches memory cache first, and if not found, loads it into memory from storage.    
        /// </summary>
        /// <remarks>
        /// <para>
        /// No error if IGraphVertex record not found -- returns null.
        /// </para>
        /// </remarks>
        /// <param name="vertexId">The vertex's unique Id.</param>
        /// <returns>
        /// Return the vertex with the specified id,
        /// or null if the GraphVertex doesn't exist.
        /// </returns>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
        IGraphVertex<TId> GetVertex(TId vertexId);

        /// <summary>
        /// Tries to return the Vertex with the given Id.
        /// <para>
        /// Unlike <see cref="GetVertex(TId)"/>, does not throw an error if the vertex is not found.
        /// </para>
        /// </summary>
        /// <param name="vertexId"></param>
        /// <param name="vertex"></param>
        /// <returns></returns>
        bool TryGetVertex(TId vertexId, out IGraphVertex<TId> vertex);



        /// <summary>
        /// Gets the vertices that match the specified GraphVertex 
        /// Searches memory cache first, and if not found, loads it into memory from storage.    
        /// </summary>
        /// <remarks>
        /// 	<para>
        /// Collects together the Vertexes from memory,
        /// and if not found in memory, searchs within storage to complete the collection.
        /// </para>
        /// <para>
        /// No error is raised if not all vertices are found, but it does return <c>False</c>
        /// in that case.
        /// </para>
        /// </remarks>
        /// <param name="vertexIds">A list of </param>
        /// <param name="resultCollection">A vertex collection to append the results to.</param>
        /// <param name="missingVertexIds"></param>
        /// <returns>True if all vertices found.</returns>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertexIds is null.</exception>
        /// <exception cref="ArgumentNullException">An exception is raised if the resultCollection is null.</exception>
        bool GetVertices(ICollection<TId> vertexIds, ref IGraphVertexList resultCollection,
                         out ICollection<TId> missingVertexIds);



        /// <summary>
        /// Gets the vertices from the specified data source id and
        /// with the specified objects 
        /// </summary>
        /// <remarks>
        /// <para>
        /// No error is raised if not all vertices are found, but it does return <c>False</c>
        /// in that case.
        /// </para>
        /// <para>
        /// Searches for vertices in memory first. Any not found are then searched for in storage.
        /// </para>
        /// </remarks>
        /// <param name="dataObjectFactoryId">The data source id.</param>
        /// <param name="dataObjectIds">The dataObject </param>
        /// <param name="resultCollection">A vertex collection to append the results to.</param>
        /// <param name="missingRecordIds">A result collection of the record ids not found.</param>
        /// <param name="createIfNotFound">if <c>true</c>, 
        /// and if there is object ids with no correspondant vertices then the
        /// vertices are created on the fly.</param>
        /// <returns>True if vertices for each record id were found.</returns>
        /// <exception cref="ArgumentNullException">An exception is raised if dataObjectFactoryId is 0.</exception>
        /// <exception cref="ArgumentNullException">An exception is raised if dataObjectIds is null.</exception>
        bool GetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds,
                         ref IGraphVertexList resultCollection, out ICollection<string> missingRecordIds,
                         bool createIfNotFound);






        /// <summary>
        /// Updates the vertex.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes <see cref="VertexUpdated"/>.
        /// </para>
        /// </remarks>
        /// <param name="vertex">The vertex.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex is null.</exception>
        bool UpdateVertex(IGraphVertex<TId> vertex);

    }
}
