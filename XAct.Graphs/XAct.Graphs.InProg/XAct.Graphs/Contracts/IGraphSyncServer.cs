using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Graphs {
  /// <summary>
  /// 
  /// </summary>
  public interface IGraphSyncServer {


    /// <summary>
    /// Get Ids of all new (DateCreated>LastSyncDate) Vertices 
    /// pointing to records within a collection of DataSources, 
    /// that are not marked as already transmitted to Client.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that this method does not load vertices into in-memory cache,
    /// (although the use of <c>GetVertices</c> could be used subsequently
    /// to do this).
    /// </para>
    /// </remarks>
    /// <param name="dataSourceIds">Iterable Collection of DataSource </param>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <param name="lastSyncDate">The date of the last Sync request.</param>
    /// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    Guid[] GetNewVertexIds(ICollection<int> dataSourceIds, Guid clientMachineId, DateTime lastSyncDate);

        /// <summary>
    /// Get Ids of all new Vertices (DateCreated>LastSyncDate) 
    /// pointing to specified records in a specified datasource,
    /// that are not marked as already transmitted to client.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that this method does not load vertices into in-memory cache.
    /// </para>
    /// </remarks>
    /// <param name="dataObjectFactoryId">A single dataSource id.</param>
    /// <param name="dataObjectIds">An iterable Collection of dataObject record </param>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <param name="lastSyncDate">The date of the last Sync request.</param>
    /// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObjectIds argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    Guid[] GetNewVertexIds(int dataObjectFactoryId, ICollection<string> dataObjectIds, Guid clientMachineId, DateTime lastSyncDate);

          /// <summary>
    /// Get Ids of all updated (DateModified>LastSyncDate) Vertices 
    /// pointing to records within a collection of DataSources, 
    /// that are not marked as already transmitted to Client.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that this method does not load vertices into in-memory cache
    /// (although the use of <c>GetVertices</c> could be used subsequently
    /// to do this).
    /// </para>
    /// </remarks>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <param name="lastSyncDate">The date of the last Sync request.</param>
    /// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataSourceIds argument is null.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    Guid[] GetUpdatedVertexIds(Guid clientMachineId, DateTime lastSyncDate);

      
    /// <summary>
    /// Get a collection of GraphVertex Ids that have been deleted
    /// on Server, that are still present on Client.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The ID's returned are to Vertices that no longer exist 
    /// in this graph.
    /// </para>
    /// </remarks>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <returns>A collection of GraphVertex Ids (never null, but can be of size 0).</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    Guid[] GetDeletedVertexIds(Guid clientMachineId);



    /// <summary>
    /// Marks a vertex as having been already on a Client.
    /// Intended to be used by the GaphSyncManager when 
    /// a GraphVertex that has already been created on the Client is now being duplicated 
    /// on the Server (which ends up being the same thing as created on the server
    /// and successfully sent to Client).
    /// </summary>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <param name="vertexId">The vertex of the id.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    void SaveVertexIdInVerticesSent(Guid clientMachineId, Guid vertexId);

    /// <summary>
    /// UnMarks a vertex as being on a Client.
    /// Intended to be used by the GaphSyncManager when 
    /// a GraphVertex that has already been deleted on the Client 
    /// is now being removed from the Server. 
    /// </summary>
    /// <param name="clientMachineId">The unique Id of the client machine.</param>
    /// <param name="vertexId">The vertex of the id.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the clientMachineId argument is empty.</exception>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertexId argument is empty.</exception>
    void RemoveVertexIdInVerticesSent(Guid clientMachineId, Guid vertexId);


  }//Class:End
}//Namespace:End
