﻿using System;
using XAct.Domain;

namespace XAct.Data {

    /// <summary>
	/// Interface to implement UTC date tracking on an Item, such as Vertex record.
	/// </summary>
	public interface IDateTrackable :IAuditable {


		/// <summary>
		/// Gets or sets the date this GraphVertex has been last edited, expressed in UTC.
		/// Updates the modelState to 'UpdatePending'.
		/// </summary>
		/// <value>The date edited.</value>
		DateTime LastModifiedOn {get;set;}

		/// <summary>
		/// Gets or sets the date this GraphVertex has last been accessed (viewed), expressed in UTC.
		/// </summary>
		/// <remarks>
		/// This field is not used/updated right now.
		/// It could be useful for automatically cleaning up the local DB
		/// ... but implementing this means that we must do an SQL UPDATE
		/// every time we extract a vertex from the DB  -- which brings with it
		/// performance issues ...
		/// </remarks>
		/// <value>The date accessed.</value>
		DateTime DateAccessed {get;set;}

	}
}
