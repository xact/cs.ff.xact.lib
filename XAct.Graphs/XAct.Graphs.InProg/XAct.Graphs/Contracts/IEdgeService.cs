﻿
using System;
using System.Collections.Generic;

namespace XAct.Graphs
{
    public interface IEdgeService<TId>
    {

        #region Events Raised
        /// <summary>
        /// Cancelable event raised before an <see cref="IGraphEdge{TId}"/> is about to be created.
        /// </summary>
        event EventHandler<CancelEdgeEventArgs> EdgeCreating;

        /// <summary>
        /// Event raised after an <see cref="IGraphEdge{TId}"/> is Created.
        /// </summary>
        event EventHandler<EdgeEventArgs> EdgeCreated;

        /// <summary>
        /// Cancelable event raised before an <see cref="IGraphEdge{TId}"/> is updated.
        /// </summary>
        event EventHandler<CancelEdgeEventArgs> EdgeUpdating;

        /// <summary>
        /// Event raised after an <see cref="IGraphEdge{TId}"/> is updated.
        /// </summary>
        /// <para>
        /// Raised after an <see cref="IGraphEdge{TId}"/> is updated in memory, but not yet necessarily persisted to storage.
        /// </para>
        event EventHandler<EdgeEventArgs> EdgeUpdated;

        /// <summary>
        /// Cancelable event raised before an <see cref="IGraphEdge{TId}"/> is about to be deleted.
        /// </summary>
        event EventHandler<CancelEdgeEventArgs> EdgeDeleting;

        /// <summary>
        /// Event raised after an <see cref="IGraphEdge{TId}"/> is Deleted.
        /// </summary>
        event EventHandler<EdgeEventArgs> EdgeDeleted;
        #endregion




        //----------------------------------------------------------------------------------------------------
        //CRUD / CREATE
        //----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Create a new Edge between the specified Source and Target vertices.
        /// <para>
        /// IMPORTANT: The edge is created, but is not added to the graph until AddEdge(TEdge) is invoked.
        /// </para>
        /// </summary>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <param name="sourceId">The source Vertex Id.</param>
        /// <param name="targetId">The target Vertex Id.</param>
        /// <returns>A new Edge.</returns>
        IGraphEdge<TId> CreateEdge(TId sourceId, TId targetId);


        /// <summary>
        /// Adds a new Edge between a source Vertex and a target Vertex.
        /// Returns <c>true</c> only if an Edge was created.
        /// Returns <c>false</c> if an Edge already existed.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invokes a cancelable <see cref="EdgeCreating"/> before creating the Edge,
        /// and then <see cref="EdgeCreated"/> if successful.
        /// </para>
        /// <para>
        /// No error is raised if an Edge already exists between the two 
        /// Vertex instances:
        /// it just returns <c>false</c>, and <see cref="EdgeCreated"/>
        /// is never invoked.
        /// </para>
        /// </remarks>
        /// <param name="sourceId">The source Vertex Id.</param>
        /// <param name="targetId">The target Vertex Id.</param>
        /// <returns>
        /// Returns <c>true</c> only if an Edge was created.
        /// Returns <c>false</c> if an Edge already existed.
        /// </returns>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <exception cref="System.ArgumentNullException">Exception raised if sourceId is null/empty.</exception>
        /// <exception cref="System.ArgumentNullException">Exception raised if targetId is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">Exception raised if sourceVertex is not found.</exception>
        /// <exception cref="VertexNotFoundException">Exception raised if targetVertex is not found.</exception>
        bool AddEdge(TId sourceId, TId targetId);


        /// <summary>
        /// Adds a new Edge between a source Vertex and a target Vertex.
        /// Returns <c>true</c> only if a new Edge was created.
        /// Returns <c>false</c> if an Edge between the two endpoints already existed.
        /// <para>
        /// Wrapper to <see cref="AddEdge(TId,TId)"/>.
        /// </para>
        /// </summary>
        /// <param name="sourceVertex">The source Vertex.</param>
        /// <param name="targetVertex">The target Vertex.</param>
        /// <returns>
        /// Returns <c>true</c> only if an Edge was created.
        /// Returns <c>false</c> if an Edge already existed.
        /// </returns>
        /// <remarks>
        /// <para>
        /// Note that it first invokes <see cref="ContainsEdge(TId, TId)"/> to see if an 
        /// edge exists between the two endpoints: 
        /// the comparison is done by Id's, and not Edge or vertex entities.
        /// </para>
        /// 	<para>
        /// Invokes a cancelable <see cref="EdgeCreating"/> before creating the Edge,
        /// and then <see cref="EdgeCreated"/> if successful.
        /// </para>
        /// 	<para>
        /// No error is raised if an Edge already exists between the two
        /// Vertex instances:
        /// it just returns <c>false</c>, and <see cref="EdgeCreated"/>
        /// is never invoked.
        /// </para>
        /// </remarks>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <exception cref="ArgumentNullException">An exception is raised if the sourceVertex is null.</exception>
        /// <exception cref="ArgumentNullException">An exception is raised if the targetVertex is null.</exception>
        bool AddEdge(IGraphVertex<TId> sourceVertex, IGraphVertex<TId> targetVertex);


        bool AddEdge(IGraphEdge<TId> edge);




        //----------------------------------------------------------------------------------------------------
        //CRUD / RETRIEVE
        //----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Determines whether the graph contains an edge between the source and target vertex.
        /// <para>
        /// IMPORTANT:
        /// Comparison is done by sourceId and targetId -- not by Edge. 
        /// </para>
        /// </summary>
        /// <param name="edge">The edge.</param>
        /// <returns>
        /// 	<c>true</c> if the graph contains an edge between the two vertices; otherwise, <c>false</c>.
        /// </returns>
        bool ContainsEdge(IGraphEdge<TId> edge);


        
        /// <summary>
        /// Determines whether the graph contains an edge between the source and target vertex.
        /// <para>
        /// IMPORTANT:
        /// Comparison is done by sourceId and targetId -- not by Edge or Vertex entity. 
        /// </para>
        /// </summary>
        /// <param name="source">The source vertex.</param>
        /// <param name="target">The target vertex.</param>
        /// <returns>
        /// 	<c>true</c> if the edge contains an edge between the two vertices; otherwise, <c>false</c>.
        /// </returns>
        bool ContainsEdge(IGraphVertex<TId> source, IGraphVertex<TId> target);

        /// <summary>
        /// Determines whether the graph contains an edge between the source and target vertex.
        /// <para>
        /// IMPORTANT:
        /// Comparison is done by sourceId and targetId -- not by Edge or Vertex entity. 
        /// </para>
        /// </summary>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <param name="sourceId">The Id of the source endpoint vertex.</param>
        /// <param name="targetId">The Id of the target endpoint vertex.</param>
        /// <returns>
        /// 	<c>true</c> if the edge contains an edge between the two vertices; otherwise, <c>false</c>.
        /// </returns>
        bool ContainsEdge(TId sourceId, TId targetId);



        /// <summary>
        /// Returns an existing <see cref="IGraphEdge{TId}"/> connecting the source and target vertices.
        /// </summary>
        /// <param name="source">The source vertex.</param>
        /// <param name="target">The target vertex.</param>
        /// <param name="throwExceptionIfNotFound">if set to <c>true</c> [throw exception if not found].</param>
        /// <returns></returns>
        IGraphEdge<TId> GetEdge(TId source, TId target, bool throwExceptionIfNotFound = true);


        /// <summary>
        /// Returns an existing <see cref="IGraphEdge{TId}"/> connecting the source and target vertices.
        /// </summary>
        /// <param name="source">The source vertex.</param>
        /// <param name="target">The target vertex.</param>
        /// <param name="throwExceptionIfNotFound">if set to <c>true</c> [throw exception if not found].</param>
        /// <returns></returns>
        IGraphEdge<TId> GetEdge(IGraphVertex<TId> source, IGraphVertex<TId> target, bool throwExceptionIfNotFound = true);


        //----------------------------------------------------------------------------------------------------
        //CRUD / UPDATE
        //----------------------------------------------------------------------------------------------------
        /// <summary>
        /// Updates the Edge between the source Vertex Id and target Vertex Id.
        /// </summary>
        /// <param name="sourceId">The source Vertex Id.</param>
        /// <param name="targetId">The target Vertex Id.</param>
        /// <returns>
        /// Return true if Edge is updated.
        /// </returns>
        /// <remarks>Invokes <see cref="EdgeUpdated"/> if successful.</remarks>
        /// <exception cref="VertexNotFoundException">An Exception is raised if the source or target Vertex cannot be found</exception>
        bool UpdateEdge(TId sourceId, TId targetId);

        //----------------------------------------------------------------------------------------------------
        //CRUD / DELETE
        //----------------------------------------------------------------------------------------------------

        /// <summary>
        /// Removes the Edge between the source Vertex Id and target Vertex Id.
        /// if the Edge doesn't exists, do nothing.
        /// <para>
        /// Raises events before and after operation.
        /// </para>
        /// <para>
        /// Returns <c>true</c> if an Edge is removed.
        /// </para>
        /// <para>
        /// Throws an error if no edge was found.
        /// </para>
        /// </summary>
        /// <remarks>
        /// 	<para>
        /// No error is raised if the source Vertex,
        /// or target Vertex, or an Edge between
        /// the two, is not found to exist.
        /// </para>
        /// 	<para>
        /// Invokes a cancelable <see cref="EdgeDeleting"/> before deleting the Vertex,
        /// and then <see cref="EdgeDeleted"/> if successful.
        /// </para>
        /// </remarks>
        /// 	<param name="sourceId">The source Vertex Id.</param>
        /// 	<param name="targetId">The target Vertex Id.</param>
        /// 	<returns>
        /// Returns <c>true</c> only if an Edge is removed,
        /// Returns <c>false</c> if no Edge already existed.
        /// </returns>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <exception cref="VertexNotFoundException">An Exception is raised if the source or target Vertex cannot be found</exception>
        bool RemoveEdge(TId sourceId, TId targetId);


        //----------------------------------------------------------------------------------------------------
        //MISC
        //----------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns an enumeration of the in-edges of the vertex with the specified id.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns>
        /// An enumerable collection of <see cref="IGraphEdge{TId}"/> instances (zero sized or more)
        /// </returns>
        /// <exception cref="ArgumentNullException">An Exception is raised if the argument is Empty.</exception>
        /// <exception cref="VertexNotFoundException">An Exception is raised if the <see cref="IGraphVertex{TId}"/> is not found</exception>
        IEnumerable<IGraphEdge<TId>>  InEdges(TId vertexId);


        /// <summary>
        /// Returns the number of in.edges for the vertex with the specified Id.
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// Returns 0 or more, unless exception raised.
        /// </returns>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the argument is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the <see cref="IGraphVertex{TId}"/> is not found.</exception>
        int InDegree(TId vertexId);


        /// <summary>
        /// Returns the number of out.edges for the vertex with the specified Id.
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// Returns 0 or more, unless exception raised.
        /// </returns>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ArgumentNullException">An exception is raised if the argument is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the <see cref="IGraphVertex{TId}"/> is not found.</exception>
        int OutDegree(TId vertexId);


        /// <summary>
        /// Returns an Enumeration of the out-edges of the <see cref="IGraphVertex{TId}"/> with the specified Id.
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// An enumerable collection of <see cref="IGraphEdge{TId}"/> instances (zero-size or more, unless exception raised).
        /// </returns>
        /// <internal>
        /// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
        /// </internal>
        /// <exception cref="ArgumentNullException">An exception is raised if the vertex id is null/empty.</exception>
        /// <exception cref="VertexNotFoundException">An exception is raised if the <see cref="IGraphVertex{TId}"/> is not found.</exception>
        IEnumerable<IGraphEdge<TId>> OutEdges(TId vertexId);








    }
}
