using System;
using System.Collections.Generic;

namespace XAct.Graphs {

	/// <summary>
	/// Interface for a graph Engine.
	/// </summary>
	public interface IQuickGraphWrapper<TVertex, TEdge> :

			QuickGraph.IGraph<TVertex, TEdge>,
			//QuickGraph.IMutableGraph<TVertex, TEdge>,
			QuickGraph.IMutableVertexListGraph<TVertex, TEdge>,
			QuickGraph.IMutableEdgeListGraph<TVertex, TEdge>,
			//QuickGraph.IMutableVertexAndEdgeListGraph<TVertex, TEdge>,
			//QuickGraph.IVertexAndEdgeListGraph<TVertex, TEdge>,
			QuickGraph.IEdgeListAndIncidenceGraph<TVertex, TEdge>,
			//QuickGraph.IMutableIncidenceGraph<TVertex, TEdge>,
			//QuickGraph.IBidirectionalGraph<TVertex, TEdge>,
			QuickGraph.IMutableBidirectionalGraph<TVertex, TEdge>,
		QuickGraph.IUndirectedGraph<TVertex,TEdge>,
	    QuickGraph.IEdgeFactory<TVertex, TEdge>,
		QuickGraph.IVertexFactory<TVertex>

			where TEdge : QuickGraph.IEdge<TVertex> ,IEdgeDef<Guid> 
	{


		#region Events Raised

		/// <summary>
		/// Event raised after a Vertex is created.
		/// </summary>
		event EventHandler<GraphVertexEventArgs> VertexCreated;

		/// <summary>
		/// Event raised before a Vertex is deleted (Cancelable).
		/// </summary>
		event EventHandler<CancelVertexEventArgs> VertexDeleting;

		/// <summary>
		/// Event raised after a Vertex is deleted.
		/// </summary>
		event EventHandler<GraphVertexEventArgs> VertexDeleted;

		/// <summary>
		/// Event raised before a Vertex is released (Cancelable).
		/// </summary>
		event EventHandler<CancelVertexEventArgs> VertexReleasing;

		/// <summary>
		/// Event raised after a Vertex is released.
		/// </summary>
		event EventHandler<GuidEventArgs> VertexReleased;

		/// <summary>
		/// Event raised after a Vertex is Updated.
		/// </summary>
		event EventHandler<GraphVertexEventArgs> VertexUpdated;

		/// <summary>
		/// Cancelable event raised before an Edge is about to be created.
		/// </summary>
		event EventHandler<CancelEdgeEventArgs> EdgeCreating;


		/// <summary>
		/// Event raised after an Edge is Created.
		/// </summary>
		event EventHandler<EdgeEventArgs> EdgeCreated;

		/// <summary>
		/// Cancelable event raised before an Edge is about to be deleted.
		/// </summary>
		event EventHandler<CancelEdgeEventArgs> EdgeDeleting;

		/// <summary>
		/// Event raised after an Edge is Deleted.
		/// </summary>
		event EventHandler<EdgeEventArgs> EdgeDeleted;


		/// <summary>
		/// Event raised when a Vertex is asked for that is not in the local db.
		/// This even can be used to try getting the Vertex from a remote server.
		/// </summary>
		event EventHandler<GuidEventArgs> VertexMissing;

		#endregion







		#region Vertex related Methods - Custom
		/// <summary>
		/// Updates the Vertex.
		/// </summary>
		/// <remarks>
		/// this method is used to signal that the Vertex has been modified in memory
		/// and that we want to commit thoses changes in local persistent storage.
		/// </remarks>
		/// <internal>
		/// ***** CUSTOM METHOD: NO QuickGraph Equivalent.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the Vertex is null.</exception>
		/// <param name="vertex">the Vertex.</param>
		bool UpdateVertex(TVertex vertex);

		/// <summary>
		/// Return the Vertex with the specified id.
		/// <para>
		/// Returns <c>null</c> if not found.
		/// </para>
		/// </summary>
		/// <remarks>
		/// <para>
		/// No error if Vertex record not found -- returns null.
		/// </para>
		/// </remarks>
		/// <param name="vertexId">the Vertex's unique Id.</param>
		/// <returns>
		/// Return the Vertex with the specified id,
		/// or null if the Vertex doesn't exist.
		/// </returns>
		/// <internal>
		/// ***** CUSTOM METHOD: NO QuickGraph Equivalent.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the Vertex id is null/empty.</exception>
		TVertex GetVertex(Guid vertexId);

		/// <summary>
		/// Gets the vertices that match the specified Vertex 
		/// </summary>
		/// <param name="vertexIds">A list of </param>
		/// <param name="resultCollection">A Vertex collection to append the results to.</param>
		/// <param name="missingVertexIds">Return collection of Vertex Ids not found.</param>
		/// <returns><c>true</c>if all vertices found.</returns>
		/// <internal>
		/// ***** CUSTOM METHOD: NO QuickGraph Equivalent.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the VertexIds is null.</exception>
		/// <exception cref="ArgumentNullException">An exception is raised if the resultCollection is null.</exception>
		bool GetVertices(ICollection<Guid> vertexIds, ref IGraphVertexList resultCollection, out ICollection<Guid> missingVertexIds);


		/// <summary>
		/// Gets the vertices from the specified data source id and
		/// with the specified objects 
		/// </summary>
		/// <remarks>
		/// <para>
		/// No error is raised if not all vertices are found, but it does return <c>False</c>
		/// in that case.
		/// </para>
		/// </remarks>
		/// <param name="dataObjectFactoryId">The data source id.</param>
		/// <param name="dataObjectIds">The dataObject </param>
		/// <param name="resultCollection">A Vertex collection to append the results to.</param>
		/// <param name="missingRecordIds">A result collection of the record ids not found.</param>
		/// <param name="createIfNotFound">if <c>true</c>, 
		/// and if there is object ids with no correspondant vertices then the
		/// vertices are created on the fly.</param>
		/// <returns><c>true</c>if vertices for each record id were found.</returns>
		/// <internal>
		/// ***** CUSTOM METHOD: NO QuickGraph Equivalent.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if dataObjectFactoryId is 0.</exception>
		/// <exception cref="ArgumentNullException">An exception is raised if dataObjectIds is null.</exception>
		bool GetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds, ref IGraphVertexList resultCollection, out ICollection<string> missingRecordIds, bool createIfNotFound);


		/// <summary>
		/// Performs a DepthFirstSearch starting from the given vertex,
		/// ensuring the vertices are in Memory.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		/// <param name="depth">The max depth (default is 4).</param>
		void Spider(GraphVertex vertex, int depth);
		#endregion

		#region Vertex related Methods - Custom - User/Global Data Attributes
		/// <summary>
		/// Initializes a Vertex's User Attributes.
		/// </summary>
		/// <param name="vertex">the Vertex whose user attributes are to be initialized.</param>
		/// <returns><c>true</c>if attributes were found.</returns>
		/// <internal>
		/// ***** CUSTOM METHOD: NO QuickGraph Equivalent.
		/// </internal>
		/// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
		bool InitializeUserAttributes(TVertex vertex);
		#endregion

		#region Vertex related Methods - QuickGraph Overloads

		/// <summary>
		/// Create a new Vertex with the specified data source id and data record id.
		/// <remarks>
		/// <para>
		/// </para>
		/// <para>
		/// The Data property is null.
		/// </para>
		/// <para>
		/// Override of <see cref="QuickGraph.IVertexFactory{TVertex}.CreateVertex()"/>
		/// </para>
		/// </remarks>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// </summary>
		/// <param name="dataObjectFactoryId">The data source id.</param>
		/// <param name="dataRecordId">The data record id.</param>
		/// <returns>a newly created vertex</returns>
		TVertex CreateVertex(int dataObjectFactoryId, string dataRecordId);

	
		/// <summary>
		/// Removes(Deletes) the Vertex from the graph (in memory and underlying storage)
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invokes  <see cref="OnVertexRemoving"/> prior to 
		/// deleting the Vertex, and <see cref="OnVertexRemoved"/> afterwards,
		/// if not cancelled.
		/// </para>
		/// <para>
		/// Override of <see cref="QuickGraph.IMutableVertexListGraph{TVertex, GraphEdge}.RemoveVertex(TVertex)"/>
		/// </para>
		/// </remarks>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <param name="vertexId">the Vertex Id.</param>
		/// <returns><c>true</c>if Vertex has been deleted, <c>false</c> otherwise.</returns>
		/// <exception cref="ArgumentNullException">An exception is raised if the Vertex id is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the Vertex is not found.</exception>
		bool RemoveVertex(Guid vertexId);


		#endregion




		#region Edges related Methods - Custom
		/// <summary>
		/// Returns an enumeration of vertices whose have a out Edge pointing
		/// to the Vertex with the specified id.
		/// </summary>
		/// <remarks>
		/// <para>
		/// </para>
		/// </remarks>
		/// <internal>
		/// ***** CUSTOM METHOD: NO QuickGraph Equivalent.
		/// </internal>
		/// <param name="vertexId">the Vertex id.</param>
		/// <exception cref="VertexNotFoundException">An Exception is raised if the specified Vertex is not found</exception>
		/// <returns></returns>
		IEnumerable<GraphVertex> InVertices(Guid vertexId);
		/// <summary>
		/// Return an enumeration of the vertices pointed to by the out.edges for the Vertex with the
		/// specified Id.
		/// the returned enumeration is never null, but can be empty
		/// </summary>
		/// <internal>
		/// ***** CUSTOM METHOD: NO QuickGraph Equivalent.
		/// </internal>
		/// <param name="vertexId">the Vertex Id.</param>
		/// <returns>An enumerable collection of Vertex instances (zero-size or more, unless exception raised).</returns>
		/// <exception cref="ArgumentNullException">An exception is raised if the VertexId is null/empty.</exception>
		IEnumerable<GraphVertex> OutVertices(Guid vertexId);

		/// <summary>
		/// Add the specifiedEdges.
		/// Returns <c>true</c> only if all Edges were created.
		/// Returns <c>false</c> if any Edge already existed.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invokes a cancelable <see cref="E:OnEdgeCreating"/> before creating the Edge,
		/// and then <see cref="E:OnEdgeCreated"/> for each Edge created successful.
		/// </para>
		/// <para>
		/// No error is raised if an Edge already exists between the two 
		/// Vertex instances:
		/// it just returns <c>false</c>, and <see cref="OnEdgeCreated"/>
		/// is never invoked.
		/// </para>
		/// </remarks>
		/// <para>
		/// Returns <c>true</c> only if an Edge was created.
		/// Returns <c>false</c> if an Edge already existed.
		/// </para>
		/// <internal>
		/// ***** CUSTOM METHOD: NO QuickGraph Equivalent.
		/// </internal>
		/// <param name="edgesCreated">The Result number of new edges actually created.</param>
		/// <param name="edgesDef">TheEdges definitions.</param>
		/// <returns>the number ofEdges effectively created</returns>
		bool AddEdges(out int edgesCreated, params TEdge[] edgesDef);

		#endregion

		#region Edges related Methods - QuickGraph Overloads
		/// <summary>
		/// Returns the number of in.edges for the Vertex with the specified Id.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Override of <see cref="QuickGraph.IBidirectionalGraph{TVertex, TEdge}.InDegree(TVertex)"/>
		/// </para>
		/// </remarks>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the argument is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the Vertex is not found.</exception>
		/// <param name="vertexId">the Vertex Id.</param>
		/// <returns>Count of InEdges (zero or more).</returns>
		int InDegree(Guid vertexId);


		/// <summary>
		/// Returns an enumeration of the inEdges of the Vertex with the specified id.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Override of <see cref="QuickGraph.IBidirectionalGraph{TVertex, TEdge}.InEdges(TVertex)"/>
		/// </para>
		/// </remarks>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="VertexNotFoundException">An Exception is raised if the Vertex is not found</exception>
		/// <param name="vertexId">the Vertex id.</param>
		/// <returns>An enumerable collection of Edge instances (zero sized or more)</returns>
		IEnumerable<TEdge> InEdges(Guid vertexId);


		/// <summary>
		/// Returns the number of out.edges for the Vertex with the specified Id.
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the argument is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the Vertex is not found.</exception>
		/// <param name="vertexId">the Vertex Id.</param>
		/// <returns>Count of OutEdges (zero or more).</returns>
		int OutDegree(Guid vertexId);


		/// <summary>
		/// Returns an enumeration of the outEdges of the Vertex with the specified Id.
		/// </summary>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the Vertex id is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">An exception is raised if the Vertex is not found.</exception>
		/// <param name="vertexId">the Vertex Id.</param>
		/// <returns>An enumerable collection of Edge instances (zero-size or more, unless exception raised).</returns>
		IEnumerable<TEdge> OutEdges(Guid vertexId);


				/// <summary>
		/// Determines whether the graph contains the specified edge.
		/// <para>
		/// IMPORTANT:
		/// Comparison is done by sourceId and targetId -- not by Edge or Vertex entity. 
		/// </para>
		/// </summary>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <param name="sourceId">The Id of the source endpoint vertex.</param>
		/// <param name="targetId">The Id of the target endpoint vertex.</param>
		/// <returns>
		/// 	<c>true</c> if the edge contains an edge between the two vertices; otherwise, <c>false</c>.
		/// </returns>
		bool ContainsEdge(Guid sourceId, Guid targetId);



		/// <summary>
		/// Create a new Edge between the specified Source and Target vertices.
		/// <para>
		/// IMPORTANT: The edge is created, but is not added to the graph until AddEdge(TEdge) is invoked.
		/// </para>
		/// </summary>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <param name="sourceId">The source Vertex Id.</param>
		/// <param name="targetId">The target Vertex Id.</param>
		/// <returns>A new Edge.</returns>
		TEdge CreateEdge(Guid sourceId, Guid targetId);

		/// <summary>
		/// Adds a new Edge between a source Vertex and a target Vertex.
		/// Returns <c>true</c> only if an Edge was created.
		/// Returns <c>false</c> if an Edge already existed.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invokes a cancelable <see cref="E:OnEdgeCreating"/> before creating the Edge,
		/// and then <see cref="E:OnEdgeCreated"/> if successful.
		/// </para>
		/// <para>
		/// No error is raised if an Edge already exists between the two 
		/// Vertex instances:
		/// it just returns <c>false</c>, and <see cref="OnEdgeCreated"/>
		/// is never invoked.
		/// </para>
		/// </remarks>
		/// <param name="sourceId">The source Vertex Id.</param>
		/// <param name="targetId">The target Vertex Id.</param>
		/// <returns>
		/// Returns <c>true</c> only if an Edge was created.
		/// Returns <c>false</c> if an Edge already existed.
		/// </returns>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="System.ArgumentNullException">Exception raised if sourceId is null/empty.</exception>
		/// <exception cref="System.ArgumentNullException">Exception raised if targetId is null/empty.</exception>
		/// <exception cref="VertexNotFoundException">Exception raised if sourceVertex is not found.</exception>
		/// <exception cref="VertexNotFoundException">Exception raised if targetVertex is not found.</exception>
		bool AddEdge(Guid sourceId, Guid targetId);


		/// <summary>
		/// Adds a new Edge between a source Vertex and a target Vertex.
		/// Returns <c>true</c> only if a new Edge was created.
		/// Returns <c>false</c> if an Edge between the two endpoints already existed.
		/// <para>
		/// Wrapper to <see cref="AddEdge(Guid,Guid)"/>.
		/// </para>
		/// </summary>
		/// <param name="sourceVertex">The source Vertex.</param>
		/// <param name="targetVertex">The target Vertex.</param>
		/// <returns>
		/// Returns <c>true</c> only if an Edge was created.
		/// Returns <c>false</c> if an Edge already existed.
		/// </returns>
		/// <remarks>
		/// <para>
		/// Note that it first invokes <see cref="ContainsEdge(Guid, Guid)"/> to see if an 
		/// edge exists between the two endpoints: 
		/// the comparison is done by Id's, and not Edge or vertex entities.
		/// </para>
		/// 	<para>
		/// Invokes a cancelable <see cref="E:OnEdgeCreating"/> before creating the Edge,
		/// and then <see cref="E:OnEdgeCreated"/> if successful.
		/// </para>
		/// 	<para>
		/// No error is raised if an Edge already exists between the two
		/// Vertex instances:
		/// it just returns <c>false</c>, and <see cref="OnEdgeCreated"/>
		/// is never invoked.
		/// </para>
		/// </remarks>
		/// <internal>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </internal>
		/// <exception cref="ArgumentNullException">An exception is raised if the sourceVertex is null.</exception>
		/// <exception cref="ArgumentNullException">An exception is raised if the targetVertex is null.</exception>
		bool AddEdge(GraphVertex sourceVertex, GraphVertex targetVertex);

		/// <summary>
		/// Removes the Edge between the source Vertex Id and target Vertex Id.
		/// if the Edge doesn't exists, do nothing.
		/// Returns <c>true</c> only if an Edge is removed, 
		/// Returns <c>false</c> if no Edge already existed.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Invokes a cancelable <see cref="E:OnEdgeDeleting"/> before deleting the Edge,
		/// and then <see cref="E:OnEdgeDeleted"/> if successful.
		/// </para>
		/// <para>
		/// No error is raised if the source Vertex, 
		/// or target Vertex, or an Edge between
		/// the two, is not found to exist.
		/// </para>
		/// </remarks>
		/// <exception cref="VertexNotFoundException">An Exception is raised if the source or target Vertex cannot be found</exception>
		/// <param name="sourceId">The source Vertex Id.</param>
		/// <param name="targetId">The target Vertex Id.</param>
		/// <returns>
		/// Returns <c>true</c> only if an Edge is removed, 
		/// Returns <c>false</c> if no Edge already existed.
		/// </returns>
		/// <internal>
		/// <para>
		/// ***** METHOD OVERLOAD OF A QUICKGRAPH METHOD.
		/// </para>
		/// <para>
		/// Used by iSight.
		/// </para>
		/// </internal>
		bool RemoveEdge(Guid sourceId, Guid targetId);





		#endregion




		#region *Public* Methods - called asynchronously on remote response

		/// <summary>
		/// Method called when the creation of a Vertex has been accepted remotely,
		/// in order to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The event sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		void OnRemoteVertexCreateAccepted(object sender, GraphVertexEventArgs e);

		/// <summary>
		/// Method called when the creation of a Vertex has been rejected remotely
		/// to notify the graph engine  and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The event sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		void OnRemoteVertexCreateRejected(object sender, GraphVertexEventArgs e);

		/// <summary>
		/// Method called when the update of a Vertex has been accepted remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The event sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		void OnRemoteVertexUpdateAccepted(object sender, GraphVertexEventArgs e);

		/// <summary>
		/// Method called when the update of a Vertex has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The event sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		void OnRemoteVertexUpdateRejected(object sender, GraphVertexEventArgs e);

		/// <summary>
		/// Method called when the deleteion of a Vertex has been accepted remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The event sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		void OnRemoteVertexDeleteAccepted(object sender, GraphVertexEventArgs e);

		/// <summary>
		/// Method called when the deletion of a Vertex has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The event sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.GraphVertexEventArgs"/> instance containing the event data.</param>
		void OnRemoteVertexDeleteRejected(object sender, GraphVertexEventArgs e);


		/// <summary>
		/// Event handler for when an Edge has been remotely created.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		void OnRemoteEdgeCreateAccepted(object sender, EdgeEventArgs e);


		/// <summary>
		/// Method called when the creation of an Edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		void OnRemoteEdgeCreateRejected(object sender, EdgeEventArgs e);

		/// <summary>
		/// Event handler for when an Edge has been remotely updated.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		void OnRemoteEdgeUpdateAccepted(object sender, EdgeEventArgs e);


		/// <summary>
		/// Method called when the update of an Edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		void OnRemoteEdgeUpdateRejected(object sender, EdgeEventArgs e);


		/// <summary>
		/// Event handler for when an Edge has been remotely deleted.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		void OnRemoteEdgeDeleteAccepted(object sender, EdgeEventArgs e);


		/// <summary>
		/// Method called when the deletion of an Edge has been rejected remotely
		/// to notify the graph engine and update its internal representation
		/// accordingly.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The method is marked <c>public</c>, rather than <c>protected</c>
		/// so that GraphSyc's initialization can attach it to its own events.
		/// </para>
		/// </remarks>
		/// <internal>
		/// TODO: this will probably change as we implement synchronisation stuffs.
		/// </internal>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
		void OnRemoteEdgeDeleteRejected(object sender, EdgeEventArgs e);
		#endregion



	}//Class:End
}//Namespace:End
