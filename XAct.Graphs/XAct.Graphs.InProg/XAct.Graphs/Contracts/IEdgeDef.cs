﻿
namespace XAct.Graphs {
	/// <summary>
	/// Interface to provide references to Vertices by their IDs only. 
	/// <para>
	/// The advantage of referencing Vertices by Ids rather than entities
	/// is that it allows to not have to have the whole graph in memory.
	/// </para>
	/// </summary>
	/// <remarks>
	/// For a distributed peer-to-peer graph scenario, 
	/// see <see cref="IDistributedEdgeDef"/>
	/// </remarks>
	public interface IEdgeDef<TEdgeId> {

		/// <summary>
		/// Gets the Id of the source Vertex.
		/// </summary>
        TEdgeId SourceId { get; }

		/// <summary>
		/// Gets the Id of the target Vertex.
		/// </summary>
        TEdgeId TargetId { get; }
	}
}
