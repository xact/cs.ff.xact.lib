﻿
namespace XAct.Graphs  {
	/// <summary>
	/// Interface/Contract for Weighted Graph elements (Vertex and/or Edge).
	/// </summary>
	public interface IWeighted {
		/// <summary>
		/// The weight of the Item.
		/// </summary>
		int Weight { get; set; }
	}
}
