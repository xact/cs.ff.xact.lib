using System;
using System.Collections.Generic;
using XAct.Data;
using XAct.Domain;

//using System.Xml.Serialization;


namespace XAct.Graphs {

  /// <summary>
  /// Represents a single GraphVertex in the Graph.
  /// </summary>
  /// <remarks>
  /// <para>
  /// Each GraphVertex has a unique <see cref="P:Id"/>, 
  /// as well as status flags such as the date it was created (<see cref="P:DateCreated"/>)
  /// the date it was last edited (<see cref="P:UTCDateEdited"/>), and 
  /// the status of the syncronization of the vertex 
  /// relative to its local *and* remote database  (<see cref="P:modelState"/>).
  /// </para>
  /// <para>
  /// Each vertex has a reference to a DataObject to which it
  /// points, via its <see cref="P:Data"/> property.
  /// </para>
  /// <para>
  /// One creates and deletes Vertexes using methods provided by the GraphProvider:
  /// <code>
  /// <![CDATA[
  /// GraphVertex v = GraphManager.Provider.CreateVertex(dataObject);
  /// ...
  /// GraphManager.Provider.RemoveVertex(v);
  /// ]]>
  /// </code>
  /// </para>
  /// <para>
  /// One uses the methods of the GraphProvider to query for arcs/edges out from 
  /// the vertex, an example of which would be:
  /// <code>
  /// <![CDATA[
  /// IEnumerable<GraphEdge> e = GraphManager.Provider.OutEdges(v);
  /// ]]>
  /// </code>
  /// </para>
  /// </remarks>
    public partial class GraphVertex<TId> : IGraphVertex<TId>
    {

    #region Properties - Implementation of IGuidIdentifiable

    /// <summary>
    /// Gets the unique Id of this instance.
    /// </summary>
    /// <value>The unique Id.</value>
    public TId Id {
      get {
        return _Id;
      }
    }
    private TId _Id;
	#endregion

    #region Properties - Implementation of IDataSourceIdentifier

    /// <summary>
    /// Gets the identifier that specifies the source the data object.
    /// </summary>
    public IDataSourceIdentifier DataSource { get { return _dataSource; } }
      private IDataSourceIdentifier _dataSource;


    /// <summary>
    /// Gets the DataObject associated with this Vertex.
    /// Note that the DataObject can be of any type
    /// and must be boxed before usage.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The dataObject is an instance of a record, generally
    /// hydrated from a DB record.
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// Contact o = (Contact)v.Data;
    /// string firstName = o.FirstName;
    /// ]]>
    /// </code>
    /// If the data object is of type <c>Contact</c> this would
    /// be something like:
    /// <code>
    /// <![CDATA[
    /// Contact o = (Contact)v.Data;
    /// string firstName = (string)o.FirstName;
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <value>The DataObject.</value>
    public object Data {
      get {
        return _Data;
      }
      set {
        _Data = value;
      }
    }
    private object _Data;
    #endregion

    #region Properties - Implementation of IUserVertex
    /// <summary>
    /// Gets the user specific attributes for this vertex.
    /// </summary>
    /// <remarks>
    /// TODO:
    /// The attributes mechanic is not currently implemented.
    /// The attributes representation is subject to changes.
    /// </remarks>
    /// <value>The user attributes.</value>
    public GraphVertexAttributeDictionary UserAttributes {
      get {
        //Get thread of current user:
        object userId = GraphManager.UserId;
        //If there is no collection, the db has never been checked
        //for user attributes for this user yet,
        //so make first, then load it with 0 or more attributes:
        if (!_UserAttributes.ContainsKey(userId)) {
          _UserAttributes[userId] = new GraphVertexAttributeDictionary();
          _UserAttributes[userId].Changed += new EventHandler(UserAttributes_Changed);

            bool attributesFound = GraphManager.InitializeUserAttributes(this);
        }
        return _UserAttributes[userId];
      }
    }
    //This is a dictionary, of dictionary:
    private Dictionary<object, GraphVertexAttributeDictionary> _UserAttributes =
      new Dictionary<object, GraphVertexAttributeDictionary>();

    #endregion

    #region Properties - Implementation of ICachedElement
		/// <summary>
    /// Gets or sets the syncro status flags of this Vertex.
    /// </summary>
    /// <value>the Sync status.</value>
    OfflineModelState IModelState.ModelState {
      get {
        return _modelState;
      }
      set {
        _modelState = value;
      }
    }
    private OfflineModelState _modelState;


    #endregion





		#region Properties - Implementation of ISyncVertex
		/// <summary>
		/// Gets a value indicating whether this element is a local proxy 
		/// for an element that is known (eg from an Edge), but not yet been received with a server.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is a local proxy; otherwise, <c>false</c>.
		/// </value>
		GraphRetrievalStatus ISyncVertex.RetrievalStatus {
			get {
				return _RetrievalStatus;
			}
		}
		private GraphRetrievalStatus _RetrievalStatus;
		#endregion

		#region Properties - Implementation of IGlobalAttributes
		/// <summary>
		/// Gets the global attributes associated to this vertex.
		/// </summary>
		/// <remarks>
		/// TODO:
		/// The attributes mechanic is not currently implemented.
		/// The attributes representation is subject to changes.
		/// </remarks>
		/// <value>The global attributes.</value>
		public GraphVertexAttributeDictionary GlobalAttributes {
			get {
				return _GlobalAttributes;
			}
		}
		private GraphVertexAttributeDictionary _GlobalAttributes;
		#endregion

		#region Properties - Implementation of IUserVertex 
		Dictionary<object, GraphVertexAttributeDictionary> IUserVertex.UserAttributes {
      get {
        return _UserAttributes;
      }
    }
    #endregion

    #region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphVertex"/> class.
		/// </summary>
		public GraphVertex() {
			_RetrievalStatus = GraphRetrievalStatus.Empty;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="GraphVertex"/> class,
		/// as a Local Proxy vertex for one not yet received from a server.
		/// </summary>
		/// <param name="vertexId">The vertex id.</param>
		public GraphVertex(TId vertexId) {
			_RetrievalStatus = GraphRetrievalStatus.IsProxy;
		}


    /// <summary>
    /// Initializes a new instance of the <see cref="GraphVertex"/> class
    /// with the specified data source and record 
		/// <para>
		/// The vertex must be added to the graph to save it locally first, and then 
		/// have it later copied to the server.
		/// </para>
    /// </summary>
    /// <param name="dataObjectFactoryId">The data source id.</param>
    /// <param name="dataRecordId">The data record id.</param>
    /// <exception cref="ArgumentNullException">An Exception is raised if dataObjectFactoryId is 0.</exception>
    /// <exception cref="ArgumentNullException">An Exception is raised if dataRecordId is empty.</exception>
    public GraphVertex(int dataObjectFactoryId, string dataRecordId){
			((IVertexInitialization)this).InitializeAsNew(dataObjectFactoryId, dataRecordId);
		}


    /// <summary>
    /// Initializes a new instance of the <see cref="IGraphVertex"/> class.
    /// </summary>
    /// <param name="vertexId">The vertex id.</param>
    /// <param name="weight">The Vertex's weight.</param>
    /// <param name="dataObjectFactoryId">The dataObject's datasource's id.</param>
    /// <param name="dataRecordId">The dataObject's id.</param>
    /// <param name="utcDateCreated">The date created.</param>
    /// <param name="utcDateEdited">The date edited.</param>
    /// <param name="syncStatus">The sync status.</param>
    /// <exception cref="ArgumentNullException">An Exception is raised if vertexId is not initialized.</exception>
    /// <exception cref="ArgumentNullException">An Exception is raised if dataObjectFactoryId is 0.</exception>
    /// <exception cref="ArgumentNullException">An Exception is raised if dataRecordId is empty.</exception>
		public GraphVertex(TId vertexId, int weight, int dataObjectFactoryId, string dataRecordId, DateTime utcDateCreated, DateTime utcDateEdited, OfflineModelState syncStatus) {
			((IVertexInitialization)this).InitializeFromDataStore(vertexId, weight, dataObjectFactoryId, dataRecordId, utcDateCreated, utcDateEdited, syncStatus);
		}

    #endregion 

    #region Implementation of IDisposable
    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, 
    /// or resetting unmanaged resources.
    /// </summary>
    public void Dispose() {
      _GlobalAttributes = null;
      _UserAttributes = null;
      _Data = null; 
    }
    #endregion


		#region Protected Methods

		/// <summary>
		/// Initializes the vertex as a proxy.
		/// </summary>
		/// <param name="vertexId">The vertex id.</param>
		/// <exception cref="ArgumentNullException">An Exception is raised if vertexId is empty.</exception>
		void IVertexInitialization.InitializeAsProxy(TId vertexId) {
			if (vertexId.IsNotInitialized()) {
				throw new System.ArgumentNullException("vertexId");
			}
			_Id = vertexId;
			_RetrievalStatus = GraphRetrievalStatus.IsProxy;
		}

		/// <summary>
		/// Initializes the object as a new record.
		/// </summary>
		/// <param name="dataObjectFactoryId">The data object factory id.</param>
		/// <param name="dataRecordId">The data record id.</param>
		void IVertexInitialization.InitializeAsNew(int dataObjectFactoryId, string dataRecordId) {
			DateTime now = DateTime.Now;
			((IVertexInitialization)this).InitializeFromDataStore(Guid.NewGuid(), 0, dataObjectFactoryId, dataRecordId, now, now, OfflineModelState.CreationPending);
		}


		/// <summary>
    /// Initializes a new instance of the <see cref="IGraphVertex"/> class.
		/// Invoked by Constructor.
    /// </summary>
    /// <param name="vertexId">The vertex id.</param>
    /// <param name="weight">The Vertex's weight.</param>
    /// <param name="dataObjectFactoryId">The dataObject's datasource's id.</param>
    /// <param name="dataRecordId">The dataObject's id.</param>
    /// <param name="utcDateCreated">The date created.</param>
    /// <param name="utcDateEdited">The date edited.</param>
    /// <param name="syncStatus">The sync status.</param>
    /// <exception cref="ArgumentNullException">An Exception is raised if vertexId is not initialized.</exception>
    /// <exception cref="ArgumentNullException">An Exception is raised if dataObjectFactoryId is 0.</exception>
    /// <exception cref="ArgumentNullException">An Exception is raised if dataRecordId is empty.</exception>
		void IVertexInitialization.InitializeFromDataStore(TId vertexId, int weight, int dataObjectFactoryId, string dataRecordId, DateTime utcDateCreated, DateTime utcDateEdited, OfflineModelState syncStatus) {
      if (vertexId.IsNotInitialized()) {
        throw new System.ArgumentNullException("vertexId");
      }
      if (dataObjectFactoryId == 0) {
        throw new System.ArgumentNullException("dataObjectFactoryId");
      }
      if (string.IsNullOrEmpty(dataRecordId)) {
        throw new System.ArgumentNullException("dataRecordId");
      }

			if (_RetrievalStatus == GraphRetrievalStatus.Received) {
				throw new System.ArgumentException("Cannot Initialize a Vertex if it has already been initialized.");
			}

      _Id = vertexId;
      _Weight = weight;

      _DataSourceId = dataObjectFactoryId;
      _DataRecordId = dataRecordId;

      _UTCDateCreated = utcDateCreated.ToUniversalTime();
      _UTCDateEdited = utcDateEdited.ToUniversalTime();

      _modelState = syncStatus;

      _Data = null;

      _GlobalAttributes = new GraphVertexAttributeDictionary();
      _GlobalAttributes.Changed += new EventHandler(_GlobalAttributes_Changed);

			//Mark that we have this vertex is now initialized (and doesn't need any further fetching, etc.)
			_RetrievalStatus = GraphRetrievalStatus.Received;
    }
		#endregion


		#region Protected - Event Handlers
		/// <summary>
    /// Handles the Changed event of the Global Attributes.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
    virtual protected void _GlobalAttributes_Changed(object sender, EventArgs e) {
      _modelState |= OfflineModelState.UpdatePending;
    }


    /// <summary>
    /// Handles the Changed event of the all User Attributes.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
    virtual protected void UserAttributes_Changed(object sender, EventArgs e) {
      _modelState |= OfflineModelState.UpdatePending;
    }
    #endregion



    #region Public Methods - Implementation of IComparable

    /// <summary>
    /// Compares two vertices.
    /// </summary>
    /// <remarks>
    /// The GraphVertex are compared by their Id only.
    /// </remarks>
    /// <param name="other">Vertex to Compare.</param>
    /// <returns></returns>
    public int CompareTo(GraphVertex other) {
      return Id.CompareTo(other.Id);
    }

		/*
    /// <summary>
    /// Compares to.
    /// </summary>
    /// <param name="other">The other.</param>
    /// <returns></returns>
    public int CompareTo(GraphVertex other) {
      GraphVertex v = other as GraphVertex;
      if (v == null) {
        throw new ArgumentException("Cannot compare two objects", "other");
      }
      return CompareTo(v);
    }
		*/
    #endregion


    #region Method Overrides - ToString(), etc.

    /// <summary>
    /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
    /// </returns>
    public override string ToString() {
      return String.Format("(Id: {0}, creation: {1}, edition : {2}, record: {3}",
                           Id, CreatedOn, LastModifiedOn, Data);
    }

    #endregion

  }//Class:End
}//Namespace:End
