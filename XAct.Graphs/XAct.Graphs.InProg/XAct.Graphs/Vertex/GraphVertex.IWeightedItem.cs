﻿
using XAct.Domain;

namespace XAct.Graphs
{
    public partial class GraphVertex : IWeighted
    {
		/// <summary>
		/// Gets the Vertex's weight.
		/// </summary>
		/// <value>The weight.</value>
		public int Weight {
			get {
				return _Weight;
			}
			set {
				if (value != _Weight) {
					_Weight = value;
					//Changes have been made that need to be saved:
					_modelState |= OfflineModelState.UpdatePending;
				}
			}
		}
		private int _Weight;
		
    }
}
