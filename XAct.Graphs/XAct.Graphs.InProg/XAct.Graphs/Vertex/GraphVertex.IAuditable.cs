﻿using System;
using XAct.Domain;


namespace XAct.Graphs
{
    public partial class GraphVertex<TId> : IUTCDateTrackable
    {

        #region Properties = Implementation of IUTCDateTrackable

        /// <summary>
        /// Gets the date this GraphVertex was created, expressed in UTC.
        /// </summary>
        /// <value>The date created.</value>
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set
            {
                if (_CreatedOn.HasValue)
                {
                    throw new ArgumentException("Cannot change value of CreatedOn Property");
                }
                _CreatedOn = value;
            }
        }
        private DateTime? _CreatedOn;

        /// <summary>
        /// Gets or sets the date this GraphVertex has been last edited, expressed in UTC.
        /// Updates the modelState to 'UpdatePending'.
        /// </summary>
        /// <value>The date edited.</value>
        public DateTime? LastModifiedOn
        {
            get
            {
                return _UTCDateEdited;
            }
            set
            {
                _UTCDateEdited = (value.HasValue)?value.Value.ToUniversalTime:null();

                //Changes have been made that need to be saved:
                _modelState |= OfflineModelState.UpdatePending;
            }
        }
        private DateTime? _UTCDateEdited;


        /// <summary>
        /// Gets or sets the date this GraphVertex has last been accessed (viewed), expressed in UTC.
        /// </summary>
        /// <remarks>
        /// This field is not used/updated right now.
        /// It could be useful for automatically cleaning up the local DB
        /// based on the last time the GraphVertex has been extracted ...
        /// ... but implementing this means that we must do an SQL UPDATE
        /// every time we extract a vertex from the DB which bring
        /// performance issues ...
        /// </remarks>
        /// <value>The date accessed.</value>
        public DateTime? DateAccessed
        {
            get
            {
                return _UTCDateAccessed;
            }
            set
            {
                _UTCDateAccessed = (value.HasValue) ? value.Value.ToUniversalTime : null();

                /*
                //Changes have been made that need to be saved:
                _modelState |= OfflineModelState.UpdatePending;
                 */
            }
        }
        private DateTime? _UTCDateAccessed;
        #endregion
    }
}