using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Graphs {

  /// <summary>
  /// Exception raised by the GraphDbCachedProvider when a requested GraphVertex is not found.
  /// </summary>
  public class VertexIdException : Exception {

    #region Properties
    /// <summary>
    /// Gets the Id of the GraphVertex that was not found.
    /// </summary>
    /// <value>The vertex Id.</value>
    public Guid VertexId {
      get {
        return _VertexId;
      }
    }
    Guid _VertexId;

    #endregion


    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="T:VertexIdException"/> class.
    /// </summary>
    /// <param name="Id">The Id.</param>
    public VertexIdException(Guid Id) {
      _VertexId = Id;
    }
    #endregion


  }//Class:End
}//Namespace:End