using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Graphs {

  /// <summary>
  /// Exception raised by the GraphDbCachedProvider when a requested GraphVertex is not found.
  /// </summary>
  public class VertexNotFoundException : VertexIdException {

    #region Properties

    /// <summary>
    /// Gets a message that describes the current exception.
    /// </summary>
    /// <value></value>
    /// <returns>The error message that explains the reason for the exception, or an empty string("").</returns>
    public override string Message {
      get {
        return String.Format("Vertex [{0}] not found.", VertexId);
      }
    }

    #endregion


    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="T:VertexNotFoundException"/> class.
    /// </summary>
    /// <param name="Id">The Id.</param>
    public VertexNotFoundException(Guid Id) : base(Id){
    }
    #endregion


  }//Class:End
}//Namespace:End