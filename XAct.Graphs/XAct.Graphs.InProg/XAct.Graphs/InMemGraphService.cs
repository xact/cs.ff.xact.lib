﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using XAct.Diagnostics;

namespace XAct.Graphs
{
    public partial class InMemGraphService : GraphServiceBase, IInMemGraphService
    {

        public IInMemVertexService Vertices { get; }

        public IInMemEdgeService Edges { get; }

        private GraphDictionaries _Cache = new GraphDictionaries();

        private readonly ITracingService _tracingService;

        private readonly IGraphRepository _graphRepository;

        public InMemGraphService(ITracingService tracingService, IGraphRepository graphRepository)
        {
            _tracingService = _tracingService;
            _graphRepository = graphRepository;
        }

        #region Properties - InMem Graph Health

        /// <summary>
        /// Gets a value indicating whether to update changes to local storage in real time.
        /// <para>
        /// Default value is <c>true</c>.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// <code>
        /// <![CDATA[
        /// 
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <value><c>true</c> if update in real time; otherwise, <c>false</c>.</value>
        public bool UpdateInRealTime {
            get {
                return _UpdateInRealTime;
            }
        }
        private bool _UpdateInRealTime = true;

        /// <summary>
        /// Gets the number of active Vertex elements in memory.
        /// </summary>
        /// <value>The vertices in memory count.</value>
        public int VerticesInMemory {
            get {
                return _Cache.AllVertices.Count;
            }
        }
        #endregion



        #region Protected Methods - InMem Initialize Timer
        /// <summary>
        /// Initializes the timer.
        /// </summary>
        protected void InitializeTimer() {
            _FlushTimer = new Timer();
            _FlushTimer.Elapsed += OnFlushEvent;
            _FlushTimer.Interval = _FlushInterval * 1000;
            _FlushTimer.Enabled = true;
        }
        #endregion

        #region Protected Methods - InMem GraphVertex Manipulation

        /// <summary>
        /// Gets the vertex from in-memory cache.
        /// <para>
        /// Returns null if not found.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// No error if vertex not found -- returns null.
        /// </para>
        /// <para>
        /// Invoked by <see cref="GetVertex"/>.
        /// If the result is null, <see cref="GetVertex"/>
        /// then calls <see cref="IGraphRepository.storageVertexGet"/>,
        /// in order to find it in the persistent storage mechanism.
        /// </para>
        /// </remarks>
        /// <param name="vertexId">The vertex's unique Id.</param>
        /// <returns>
        /// Return the vertex with the specified id,
        /// or null if the vertex doesn't exist.
        /// </returns>
        public IGraphVertex GetVertex(Guid vertexId) {
            //Check Args:
            if (vertexId == Guid.Empty) {
                throw new System.ArgumentNullException("vertexId");
            }

            GraphVertex vertex;
			
            //Note: By using TryFind(), if found, node is automatically "ascended"
			
            _Cache.AllVertices.TryFind(vertexId, out vertex);
			
            return vertex;
        }

        /// <summary>
        /// Gets the vertices already in the
        ///  in-memory cache with the specified data source id
        /// and data object 
        /// </summary>
        /// <param name="dataObjectFactoryId">The data source id.</param>
        /// <param name="dataObjectIds">The object </param>
        /// <param name="resultCollection">A collection of vertices to append to (may be zero size, but never null).</param>
        /// <param name="missingDataObjectIds">Result collection of all Record Ids not found.</param>
        /// <returns>True if all vertices found.</returns>
        virtual protected bool inMemGetVertices(int dataObjectFactoryId, ICollection<string> dataObjectIds, ref IGraphVertexList resultCollection, out ICollection<string> missingDataObjectIds) {
            //Check Args:
            if (dataObjectFactoryId == 0) {
                throw new System.ArgumentNullException("dataObjectFactoryId");
            }
            if (dataObjectIds == null) {
                throw new System.ArgumentNullException("dataObjectIds");
            }
            if (resultCollection == null) {
                throw new System.ArgumentNullException("vertexCollection");
            }


            //Get the collection from in memory:
            foreach (GraphVertex vertex in _Cache.AllVertices.Items) {
                if (vertex.DataSourceId == dataObjectFactoryId && dataObjectIds.Contains(vertex.DataRecordId)) {
                    resultCollection.Add(vertex);
                }
            }


            //IMPORTANT:
            // on ne peut pas faire le Ascend dans le precedent foreach
            // parce que on ne peut pas modifier la collection qu'on est en train
            // de parcourir
            foreach (GraphVertex vertex in resultCollection) {
                _Cache.AllVertices.Ascend(vertex.Id);
            }



            //Iterate through the collection of vertices that were 
            //found in memory, and extract a list of 
            //just their vertex recordId's:
            List<string> foundRecordIds = new List<string>(XAct.Core.Utils.Collections.Map<GraphVertex, string>(resultCollection,
                                                                                                                delegate(GraphVertex v) {
                                                                                                                                            return v.DataRecordId;
                                                                                                                }));


            //Filter the dataObjectIds collection *argument* 
            //for all that were not found in memory:
            missingDataObjectIds =
                new List<string>(
                    Collections.Filter<string>(dataObjectIds,
                                               delegate(string id) {
                                                                       return !foundRecordIds.Contains(id);
                                               }));

            //Return flag as to whether all were found or not:
            return (new List<string>(missingDataObjectIds).Count == 0);

        }

        /// <summary>
        /// Gets the vertices withe the specified vertex ids from the in memory cache.
        /// </summary>
        /// <param name="vertexIds">The vertex ids to search for.</param>
        /// <param name="resultCollection">A collection of vertices to append to (may be zero size, but never null).</param>
        /// <param name="missingVertexIds">The ids of the vertices not found.</param>
        /// <returns>True if all vertices found.</returns>
        virtual protected bool inMemGetVertices(ICollection<Guid> vertexIds, ref IGraphVertexList resultCollection, out ICollection<Guid> missingVertexIds) {
            if (vertexIds == null) {
                throw new System.ArgumentNullException("dataObjectIds");
            }
            if (resultCollection == null) {
                throw new System.ArgumentNullException("resultCollection");
            }


            lock (this) {
                missingVertexIds = new List<Guid>();

                foreach (Guid vertexId in vertexIds) {
                    bool found = false;
                    foreach (GraphVertex vertex in _Cache.AllVertices.Items) {
                        if (vertex.Id.Equals(vertexId)) {
                            resultCollection.Add(vertex);
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        missingVertexIds.Add(vertexId);
                    }
                }
            }
            return (missingVertexIds.Count == 0);
        }


        /// <summary>
        /// Add the vertex to the in memory GraphVertex cache.
        /// NOTE: If the vertex, or a duplicate vertex (that has the same Id), 
        /// is already in memory, it will return this cached vertex. 
        /// For this reason, the returned GraphVertex *may* be a different GraphVertex than the one
        /// submitted.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="IGraphRepository.storageVertexGet(Guid)"/>
        /// and by 
        /// <see cref="IGraphRepository.storageGetVertices(int, ICollection{string} , ref IGraphVertexList , out ICollection{string} )"/>
        /// </para>
        /// </remarks>
        /// <param name="vertex">The vertex to add.</param>
        /// <returns>The Cached GraphVertex (which may be different than the argument).</returns>
        virtual protected GraphVertex inMemAddVertex(GraphVertex vertex) {
            //Check Args:
            if (vertex == null) {
                throw new System.ArgumentNullException("vertex");
            }

            //Check, add if not in the cache already,
            //and return GraphVertex that is in the cache:
            //Which may or may not be the same vertex...
            vertex = inMemInsertOrUpdateCache(vertex);

            //Cache the out-edges for this Vertex:
            IGraphEdgeList outEdges = _graphRepository.storageVertexGetOutEdges(vertex);
            _Cache.VertexOutEdges[vertex.Id] = outEdges;
		
            //DELETE THIS: _Cached.AllEdges.AddRange(outEdges);

            //Cache the in-edges for this Vertex:
            IGraphVertexList tmpVertexIdList = new IGraphVertexList();
            tmpVertexIdList.Add(vertex);

            IGraphEdgeList inEdges = _graphRepository.storageVertexGetInEdges(tmpVertexIdList);
            _Cache.VertexInEdges[vertex.Id] = inEdges;

            //DELETE THIS: _Cached.AllEdges.AddRange(inEdges);

            return vertex;
        }

        /// <summary>
        /// Inserts the given vertex in cache if not already present
        /// or update it (i.e. put it at the top of MRU list) if already in cache.
        /// returns the vertex in cache.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        virtual protected GraphVertex inMemInsertOrUpdateCache(GraphVertex vertex) {
            //Check Args:
            if (vertex == null) {
                throw new System.ArgumentNullException("vertex");
            }

            GraphVertex vInCache;
            //Note: By using TryFind(), if found, node is automatically "ascended"
            if (!_Cache.AllVertices.TryFind(vertex.Id, out vInCache)) { // not in cache
                _Cache.AllVertices.Add(vertex);
                vInCache = vertex;
                _tracingService.Trace(TraceLevel.Verbose, "vertex {0} not in cache", vertex.Id);
            }
            else {
                _tracingService.Trace(TraceLevel.Verbose, "vertex {0} already in cache", vertex.Id);
            }

            return vInCache;
        }

        /// <summary>
        /// Returns true if the vertex with the specified Id is currently loaded in cache.
        /// </summary>
        /// <param name="vertexId">The vertex Id.</param>
        /// <returns>
        /// 	<c>true</c> if [is in cache] [the specified vertex Id]; otherwise, <c>false</c>.
        /// </returns>
        virtual protected bool inMemIsVertexInCache(Guid vertexId) {
            //Check Args:
            if (vertexId == Guid.Empty) {
                throw new System.ArgumentNullException("vertexId");
            }
            GraphVertex vertex;
            //Note: By using TryFind(), if found, node is automatically "ascended"
            _Cache.AllVertices.TryFind(vertexId, out vertex);
            return (vertex!=null);
        }

        /// <summary>
        /// Tries to release deleted and old/long dormant vertices to 
        /// keep the in-memory graph from getting too big.
        /// </summary>
        /// <internal>
        /// <para>
        /// Invoked by <see cref="OnCleanupEvent"/>.
        /// </para>
        /// <para>
        /// OLIVIER: This should maybe be an abstract set of Stubs in Provider
        /// CleanupVertices(){....
        /// bool CleanupVerticesInternal()
        /// </para>
        /// </internal>
        virtual protected void inMemReleasePendingOperations() {

            lock (this) {

                //Calc cutoff point:
                DateTime cutOffStamp = DateTime.Now - _cleanupVertexTimeout;

                //--------------------------------------------------
                //Step: Dump Vertices that have been deleted from memory:
                while ((_Cache.DeletePendingVertices.Count) > 0) {
                    //Peek (ie, get handle on, without removing) the Last Used vertex:
                    GraphTimeStampedVertex timeStampedItem = _Cache.DeletePendingVertices.PeekLastUsed();

                    //If the tv's stamp is *later* than the 
                    //threshold, then we stop peeking/removing
                    //(since we are always getting the LastUsed):
                    if (timeStampedItem.Stamp > cutOffStamp) {
                        break;
                    }

                    //We did not exit, so GraphVertex is 
                    //an '*old* vertex that is to be removed
                    //GraphMRUItemVertexList, and be flushed first:
                    GraphVertex vertex = (GraphVertex)timeStampedItem.Item;
                    Flush(vertex);
                    _Cache.DeletePendingVertices.Remove(vertex.Id);

                    _tracingService.Trace(TraceLevel.Verbose, "Deleted GraphVertex released [Id={0}].", vertex.Id);
                }
                //--------------------------------------------------
                //Step: Dump *all* Deleted Edges
                //IMPORTANT: Notice that we are not checking Time here...
                //OLIVIER: Why?
                foreach (GraphEdge edge in _Cache.DeletePendingEdges) {
                    Flush(edge);
                    //_Cache.DeletePendingEdges.Remove(edge);//Correct, but slow...
                }
                _Cache.DeletePendingEdges.Clear();//Faster.

                //--------------------------------------------------
                //Step: Dump dormant active Vertices:
                while ((_Cache.AllVertices.Count > _CleanupVertexCountThreshold)) {
                    //Peek (ie, get handle on, without removing) the Last Used vertex:
                    GraphTimeStampedVertex timeStampedItem = _Cache.AllVertices.PeekLastUsed();

                    //If the tv's stamp is *later* than the 
                    //threshold, then we stop peeking/removing
                    //(since we are always getting the LastUsed):
                    if (timeStampedItem.Stamp > cutOffStamp) {
                        break;
                    }

                    //Raise an event *before* deleting the 
                    //Vertex from memory, so that the operation can be cancelled
                    //if someone still needs the vertex:
                    CancelVertexEventArgs arg = new CancelVertexEventArgs(timeStampedItem.Item);
                    OnVertexReleasing(arg);

                    if (arg.Cancel) {
                        // cleanup canceled, put the vertex in top of the cache
                        _tracingService.Trace(TraceLevel.Verbose, "Vertex cleanup canceled");
                        _Cache.AllVertices.Ascend(timeStampedItem.Item.Id);
                        //Move on to next Last Used Vertex:
                        continue;
                    }

                    //We did not exit, so GraphVertex is 
                    //an '*old* vertex that is to be removed
                    //GraphMRUItemVertexList, and be flushed first:
                    GraphVertex vertex = timeStampedItem.Item;

                    //In case there are any changes, do them just before dumping:
                    Flush(vertex);
				
                    //Ok. We've persisted any 
                    //changes...let's dump it:
                    inMemReleaseVertex(vertex.Id);

                    //Raise an event after we are finished:
                    OnVertexReleased(new GuidEventArgs(vertex.Id));

                    _tracingService.Trace(TraceLevel.Verbose, "Vertex released [Id={0}].", vertex.Id);
                }

                //--------------------------------------------------
                //Step: Dump dormant Edges:
                //These are not dumped on their own -- we dump the vertices
                //which dumped the edges.
                //--------------------------------------------------
            }//End:Lock
        }



        /// <summary>
        /// Releases the specified vertex from memory, if it is in memory.
        /// <para>
        /// Does NOT delete the Vertex.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="inMemReleasePendingOperations"/>.
        /// </para>
        /// </remarks>
        /// <param name="vertexId">The vertex Id.</param>
        virtual protected void inMemReleaseVertex(Guid vertexId) {

            if (_Cache.AllVertices.Contains(vertexId)) {
                _Cache.VertexInEdges.Remove(vertexId);
                _Cache.VertexOutEdges.Remove(vertexId);
                _Cache.AllVertices.Remove(vertexId);
            }

        }

        /// <summary>
        /// Mark the edges adjacent to this GraphVertex as deleted (in.mem).
        /// i.e. set their sync status to <c>LocalDeletionPending|RemoteDeletionPending</c>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Invoked by <see cref="RemoveVertex(GraphVertex)"/>.
        /// </para>
        /// </remarks>
        /// <param name="vertex">The vertex</param>
        virtual protected void inMemDeleteAdjacentEdges(GraphVertex vertex) {
            //Check Args:
            if (vertex == null) {
                throw new System.ArgumentNullException("vertex");
            }

            inMemDeleteInEdges(vertex);
            inMemDeleteOutEdges(vertex);

        }


        /// <summary>
        /// Delete the in-edges from in memory.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        virtual protected void inMemDeleteInEdges(GraphVertex vertex) {

            _Cache.DeletePendingEdges.AddRange(_Cache.VertexInEdges[vertex.Id]);
            _Cache.VertexInEdges[vertex.Id].Clear();
        }

        /// <summary>
        /// Ins the out-edges from the memory cache.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        virtual protected void inMemDeleteOutEdges(GraphVertex vertex) {
            _Cache.DeletePendingEdges.AddRange(_Cache.VertexOutEdges[vertex.Id]);
            _Cache.VertexOutEdges[vertex.Id].Clear();
        }

        #endregion



        #region Protected - Event Handlers - Cleanup/Flush thread event handlers
        /// <summary>
        /// Event handler called periodically by the graph's Flush thread.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        protected void OnFlushEvent(object source, ElapsedEventArgs e) {
            _tracingService.Trace(TraceLevel.Verbose, "OnFlush Event Begin [now={0}].", DateTime.Now);
            Flush();
            _tracingService.Trace(TraceLevel.Verbose, "OnFlush Event End [now={0}]", DateTime.Now);
        }


        /// <summary>
        /// Event Handler Called periodically by the graph cleanup thread.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        protected void OnCleanupEvent(object source, ElapsedEventArgs e) {
            _tracingService.Trace(TraceLevel.Verbose, "OnCleanup Started [now={0}].", DateTime.Now);
            inMemReleasePendingOperations();
			

            _tracingService.Trace(TraceLevel.Verbose, "OnCleanup Complete [now={0}].", DateTime.Now);
        }
        #endregion



    }
}