﻿using System;
using System.Collections.Generic;
using XAct.Diagnostics;
using XAct.Domain;

namespace XAct.Graphs
{
    public class InMemEdgeService<TId> : EdgeServiceBase<TId>, IInMemEdgeService<TId>
    {

        #region Fields
        int _edgeCountInMemory = 0;
        private readonly InMemGraphCacheCollections<TId> _Cache;
        private readonly IGraphCleanupSettings _defaultGraphCleanupSettings;
        #endregion

        #region Constructors
        public InMemEdgeService(
            ITracingService tracingService, 
            IGraphService<TId> graphService, 
            InMemGraphCacheCollections<TId> inMemGraphCacheCollections,
            IGraphCleanupSettings defaultGraphCleanupSettings,
            Func<TId, TId, IGraphEdge<TId>> methodToCreateEdges = null) :
            base(tracingService, graphService, methodToCreateEdges)
        {
            if (inMemGraphCacheCollections == null)
            {
                throw new ArgumentNullException("inMemGraphCacheCollections");
            }
            if (defaultGraphCleanupSettings == null)
            {
                throw new ArgumentNullException("defaultGraphCleanupSettings");
            }

            _Cache = inMemGraphCacheCollections;
            _defaultGraphCleanupSettings = defaultGraphCleanupSettings;
        }
        #endregion


        #region Implementation of IInMemEdgeService
        /// <summary>
        /// Gets the number of Edge elements in memory.
        /// <para>
        /// Which is not the same as <see cref="QuickGraph.IEdgeListGraph{GraphVertex, GraphEdge}.EdgeCount"/>.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Most graphs algorythms that deal with the edge count will want to know
        /// about all the edges in the graph -- not just the ones in memory.
        /// </para>
        /// </remarks>
        /// <value>The edges in memory count.</value>
        public int EdgeCountInMemory
        {
            get
            {
                return _edgeCountInMemory;
            }
        }

        /// <summary>
        /// Gets the edges in memory.
        /// </summary>
        /// <value>The edges in memory.</value>
        public IEnumerable<IGraphEdge<TId>> EdgesInMemory
        {
            get
            {
                //TODO:
                //To do this properly, we will need to filter the diff
                //between In and Out, or keep a global cache of all edges...
                return null;
                /* 
         * 
                        foreach (EdgeList edges in this.vertexOutEdges.Values)
                                foreach (GraphEdge edge in edges)
                                        yield return edge;
                }
         */
            }
        }
        #endregion


        //----------------------------------------------------------------------------------------------------
        //CRUD / CREATE
        //----------------------------------------------------------------------------------------------------


        protected override bool InternalAddEdge(IGraphEdge<TId> edge)
        {
         _Cache.VertexOutEdges[edge.SourceId].Add(edge);
            _Cache.VertexInEdges[edge.TargetId].Add(edge);

            //Even if we added it to two arrays, we're still talking about the
            //same edge, so we increment by 1, and only 1, the number of edges in memory:
            _EdgeCountInMemory++;

            // Change state so that thread later creates in local and remote storage:
            ((ICachedElement)edge).modelState |= OfflineModelState.CreationPending;

            if (UpdateInRealTime)
            {
                Flush(edge);
            }

        }
        //----------------------------------------------------------------------------------------------------
        //CRUD / RETRIEVE
        //----------------------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------------------
        //CRUD / UPDATE
        //----------------------------------------------------------------------------------------------------


        //----------------------------------------------------------------------------------------------------
        //CRUD / DELETE
        //----------------------------------------------------------------------------------------------------

        protected override bool InternalRemoveEdge(IGraphEdge<TId> edge)
        {
            // Remove edge from source's out-edges: 
            lock (this)
            {

                _Cache.VertexOutEdges[edge.SourceId].Remove(edge);

                // Remoe edge from target's in-edges:
                if (!_Cache.VertexInEdges.ContainsKey(edge.TargetId))
                {

                }
                else
                {
                    _Cache.VertexInEdges[edge.TargetId].Remove(edge);
                }
                this._edgeCountInMemory--;
            }


            //And Move it out to the 'deleting later' list:
            _Cache.DeletePendingEdges.Add(edge);

            //As well as change state so that thread 
            //later deletes from local and remote storage:
            ((IModelState)edge).ModelState = OfflineModelState.DeletionPending;

            if (UpdateInRealTime)
            {
                Flush(edge);
            }


        }
        //----------------------------------------------------------------------------------------------------
        //INTERNAL
        //----------------------------------------------------------------------------------------------------


        //----------------------------------------------------------------------------------------------------
        //MISC
        //----------------------------------------------------------------------------------------------------
        public override IEnumerable<IGraphEdge<TId>> InEdges(TId vertexId)
        {
            throw new System.NotImplementedException();
        }

        public override int InDegree(TId vertexId)
        {
        }

        public override int InDegrees(IGraphVertex<TId> vertex)
        {
            //Check Args:
            if (vertex == null)
            {
                throw new System.ArgumentNullException("vertex");
            }

            //vertex = GetVertex(vertex);

            //If we have a source vertex, it has been mounted via GetVertex.
            //So it has an in-edges array we can safely address:
            return _Cache.VertexOutEdges[vertex.Id].Count;
        }

        public override int OutDegree(TId vertexId)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerable<IGraphEdge<TId>> OutEdges(TId vertexId)
        {
            throw new System.NotImplementedException();
        }

    }
}