using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Graphs {
  /// <summary>
  /// Alias for <see cref="GraphSyncSection"/>.
  /// </summary>
  public class GraphSyncConfigSection : GraphSyncSection {
  }
}
