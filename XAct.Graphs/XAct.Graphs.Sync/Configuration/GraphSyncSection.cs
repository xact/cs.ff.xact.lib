using System;
using System.Configuration;

namespace XAct.Graphs {

  /// <summary>
  /// represent in memory the graph section in the xml config file.
  /// </summary>
  /// <remarks>
  /// <para>
  /// <![CDATA[
  /// ]]>
  /// </para>
  /// </remarks>
  public class GraphSyncSection : ConfigurationSection {

    /// <summary>
    /// Default Provider name (attribut "defaultProvider").
    /// </summary>
    /// <value>The default provider.</value>
    [ConfigurationProperty("defaultProvider", IsRequired = true)]
    public string DefaultProvider {
      get {
        return (string)base["defaultProvider"];
      }
      set {
        base["defaultProvider"] = value;
      }
    }


    /// <summary>
    /// Gets a collection of the registered Providers.
    /// </summary>
    /// <value>The providers.</value>
    [ConfigurationProperty("providers")]
    public ProviderSettingsCollection Providers {
      get {
        return (ProviderSettingsCollection)base["providers"];
      }
    }

  }//Class:End
}//Namespace:End