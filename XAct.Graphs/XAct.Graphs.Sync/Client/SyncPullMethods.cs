using System;
using System.Collections.Generic;
using System.Text;

using XAct.Graphs;
using XAct.Data;
using XAct.SmartClients.DisconnectedAgent; //For refs to Request, etc.

namespace XAct.Graphs {

  /// <summary>
  /// Class of Callback Methods
  /// invoked by the Request dispatcher upon
  /// return from the server after completing SyncPull webmethods.
  /// </summary>
  public class SyncPullCallBacks {

    #region Properties
    /// <summary>
    /// Gets the graph sync provider.
    /// </summary>
    /// <value>The graph sync provider.</value>
    public GraphSyncProviderBase GraphSyncProvider {
      get {
        return GraphSyncManager.Instance;
      }
    }
    #endregion


   #region Public Methods.
    /// <summary>
    /// Public method invoked by the RequestManager upon return
    /// of a Request for a PullSync 
    /// having been successfully processed by the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// If the remote system accepted the request, invokes 
    /// <see cref="OnRemotePullSyncAccepted"/>.
    /// </para>
    /// <para>
    /// If the remote server denied the request, invokes
    /// <see cref="OnRemotePullSyncCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="request">The Request package.</param>
    /// <param name="param">The params array sent.</param>
    /// <param name="result">The result of the Request.</param>
    public void syncPullCallback(Request request, object[] param, object result) {
      throw new System.NotImplementedException();
    }

    /// <summary>
    /// Public method invoked by the RequestManager upon an error 
    /// occuring during the processing of the request 
    /// for a PullSync.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invokes <see cref="OnRemotePullSyncError"/>.
    /// </para>
    /// <para>
    /// If an event handler sets the <see cref="P:DataObjectErrorEventArgs.Retry"/> flag 
    /// to <c>true</c>, the <see cref="Request"/> is requeued.
    /// </para>
    /// </remarks>
    /// <param name="request">The request.</param>
    /// <param name="error">The error.</param>
    /// <returns></returns>
    public OnExceptionAction syncPullErrorCallback(Request request, Exception error) {
      throw new System.NotImplementedException();
    }


   #endregion


    #region Fire *Provider* Events
    /// <summary>
    /// TODO:
    /// </summary>
    /// <remarks>
    /// <para>
    /// Raised by <see cref="syncPullCallback"/>.
    /// </para>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemotePullSyncCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemotePullSyncAccepted(EventArgs e) {
      throw new System.NotImplementedException();
      //GraphSyncProvider.OnRemotePullSyncAccepted(e);
    }

    /// <summary>
    /// TODO:
    /// </summary>
    /// <remarks>
    /// <para>
    /// Raised by <see cref="syncPullCallback"/>.
    /// </para>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemotePullSyncAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemotePullSyncCancelled(EventArgs e) {
      throw new System.NotImplementedException();
      //GraphSyncProvider.OnRemotePullSyncCancelled(e);
    }

    /// <summary>
    /// TODO:
    /// </summary>
    /// <remarks>
    /// <para>
    /// Raised by <see cref="syncPullErrorCallback"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemotePullSyncError(EventArgs e) {
      throw new System.NotImplementedException();
      //GraphSyncProvider.OnRemotePullSyncCreateError(e);
    }
    #endregion

  }//End:Class
}//Namespace:End
