using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using XAct.SmartClients.DisconnectedAgent;
using XAct.Data;
using XAct.Graphs;

using System.Web;

using XAct.Helpers;

namespace XAct.Graphs {

  /// <summary>
  /// Static class which give acces to the methods and properties of the
  /// GraphSyncProvider Singleton.
  /// </summary>
  public partial class GraphSyncManager { /*MANAGER:NEW*/

    #region Fields
    /// <summary>
    /// XPath to the Configuration Section in the app.config file.
    /// </summary>
    public static readonly string SectionPath = "XAct/Graphs/Sync";


    private static ProviderManagerHelper<
GraphSyncSection,
GraphSyncProviderBase,
GraphSyncProviderCollection>
_ProviderHelper;

    #endregion

    #region Static Constructors
    /// <summary>
    /// Initializes the <see cref="T:GraphSyncManager"/> class.
    /// </summary>
    static GraphSyncManager() {
      _ProviderHelper = new ProviderManagerHelper<GraphSyncSection, GraphSyncProviderBase, GraphSyncProviderCollection>(SectionPath);
    }
    #endregion

  }//Class:End
}//Namespace:End
