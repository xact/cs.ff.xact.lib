using System;
using System.Configuration.Provider;

namespace XAct.Graphs {

  /// <summary>
  /// A collection of GraphSyncProviderBase derived providers.
  /// </summary>
  public class GraphSyncProviderCollection : ProviderCollection {


    #region Properties

    /// <summary>
    /// Gets the <see cref="T:GraphSyncProviderBase"/> with the specified name.
    /// </summary>
    /// <value>A GraphEngine provider.</value>
    public new GraphSyncProviderBase this[string name] {
      get {
        return (GraphSyncProviderBase)base[name];
      }
    }

    #endregion


    #region Methods Overrides
    /// <summary>
    /// Adds a provider to the collection.
    /// </summary>
    /// <param name="provider">The provider to be added.</param>
    /// <exception cref="T:System.ArgumentException">The <see cref="P:System.Configuration.Provider.ProviderBase.Name"></see> of provider is null.- or -The length of the <see cref="P:System.Configuration.Provider.ProviderBase.Name"></see> of provider is less than 1.</exception>
    /// <exception cref="T:System.ArgumentNullException">provider is null.</exception>
    /// <exception cref="T:System.NotSupportedException">The collection is read-only.</exception>
    /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
    public override void Add(ProviderBase provider) {

      if (!(provider is GraphSyncProviderBase)) {
        throw new ArgumentException("invalid graph sync provider type", "provider");
      }

      base.Add(provider);
    }

    #endregion

  }//Class:End
}//Namespace:End
