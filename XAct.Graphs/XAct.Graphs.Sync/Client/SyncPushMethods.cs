using System;
using System.Collections.Generic;
using System.Text;

using XAct.Graphs;
using XAct.Data;
using XAct.SmartClients.DisconnectedAgent; //For refs to Request, etc.

namespace XAct.Graphs {

  /// <summary>
  /// Class of Callback Methods
  /// invoked by the Request dispatcher upon
  /// return from the server after completing SyncPush webmethods.
  /// </summary>
  public class SyncPushMethods {

    #region Properties
    /// <summary>
    /// Gets the graph sync provider.
    /// </summary>
    /// <value>The graph sync provider.</value>
    public GraphSyncProviderBase GraphSyncProvider {
      get {
        return GraphSyncManager.Instance;
      }
    }
    #endregion


    #region Public Methods - GraphVertex Related.
    /// <summary>
    /// Public method invoked by the RequestManager upon the Request 
    /// for creating, updating, or deleting a GraphVertex 
    /// having been successfully processed by the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// If the remote system accepted the request, and depending on the
    /// <c>Action</c> requested, invokes
    /// one of <see cref="OnRemoteVertexCreateAccepted"/>,
    /// <see cref="OnRemoteVertexUpdateAccepted"/>, or
    /// <see cref="OnRemoteVertexDeleteAccepted"/>.
    /// </para>
    /// <para>
    /// If the remote server denied the request, depending on the 
    /// <c>Action</c> requested, invokes
    /// one of <see cref="OnRemoteVertexCreateCancelled"/>,
    /// <see cref="OnRemoteVertexUpdateCancelled"/>, or
    /// <see cref="OnRemoteVertexDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="request">The Request package.</param>
    /// <param name="param">The params array sent.</param>
    /// <param name="result">The result of the Request.</param>
    public void vertexCallback(Request request, object[] param, object result) {

      //Extract the GraphVertex, which was the first
      //argument passed when creating the Request package
      //in 'createVertexRequest':
      XmlVertex xmlVertex = (XmlVertex)param[0];
      GraphVertex vertex = xmlVertex.Deserialize();

      bool ok = (bool)result;

      //Get description of Request:
      string description = request.Description;

      //Parse the Action from the Tag... Hum...
      GraphSyncDisconnectedProvider.Action action = (GraphSyncDisconnectedProvider.Action)Enum.Parse(typeof(GraphSyncDisconnectedProvider.Action), request.Behavior.Tag);

      VertexEventArgs e = new VertexEventArgs(vertex);

      switch (action) {
        case GraphSyncDisconnectedProvider.Action.Create:
          if (ok) {
            OnRemoteVertexCreateAccepted(e);
          }
          else {
            OnRemoteVertexCreateCancelled(e);
          }
          break;
        case GraphSyncDisconnectedProvider.Action.Update:
          if (ok) {
            OnRemoteVertexUpdateAccepted(e);
          }
          else {
            OnRemoteVertexUpdateCancelled(e);
          }
          break;
        case GraphSyncDisconnectedProvider.Action.Delete:
          if (ok) {
            OnRemoteVertexDeleteAccepted(e);
          }
          else {
            OnRemoteVertexDeleteCancelled(e);
          }
          break;
        default:
          throw new Exception(String.Format("invalid action '{0}' for remote edge operation", action));
      }
    }


    /// <summary>
    /// Public method invoked by the RequestManager upon an error 
    /// occuring during the processing of the request for
    /// creating, updating, or deleting a GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Depending on the <see cref="Request"/>'s <see cref="GraphSyncDisconnectedProvider.Action"/>,
    /// invokes one of 
    /// <see cref="OnRemoteVertexCreateError"/>,
    /// <see cref="OnRemoteVertexUpdateError"/>, or 
    /// <see cref="OnRemoteVertexDeleteError"/>.
    /// </para>
    /// <para>
    /// If an event handler sets the <see cref="P:VertexErrorEventArgs.Retry"/> flag 
    /// to <c>true</c>, the <see cref="Request"/> is requeued.
    /// </para>
    /// </remarks>
    /// <param name="request">The request.</param>
    /// <param name="error">The error.</param>
    /// <returns></returns>
    public OnExceptionAction vertexErrorCallback(Request request, Exception error) {
      //Extract the GraphVertex, which was the first
      //argument passed when creating the Request package
      //in 'createVertexRequest':
      XmlVertex xmlVertex = (XmlVertex)request.Parameters[0];
      GraphVertex vertex = xmlVertex.Deserialize();

      //Get description of Request:
      string description = request.Description;

      //Parse the Action from the Tag... Hum...
      GraphSyncDisconnectedProvider.Action action = (GraphSyncDisconnectedProvider.Action)Enum.Parse(typeof(GraphSyncDisconnectedProvider.Action), request.Behavior.Tag);

      //Create an error EventArgs package:
      VertexErrorEventArgs e = new VertexErrorEventArgs(vertex, error);

      //Raise an event so that someone can decide
      //whether to retry or not,
      //remove the vertices, whatever...
      switch (action) {
        case GraphSyncDisconnectedProvider.Action.Create:
          //Raise GraphVertex Error:
          OnRemoteVertexCreateError(e);
          break;
        case GraphSyncDisconnectedProvider.Action.Update:
          //Raise GraphVertex Error:
          OnRemoteVertexUpdateError(e);
          break;
        case GraphSyncDisconnectedProvider.Action.Delete:
          //Raise GraphVertex Error:
          OnRemoteVertexDeleteError(e);
          break;
        default:
          throw new Exception(String.Format("invalid action '{0}' for edge operation", action));
      }
      //Retry? The default is false:
      return e.Retry ? OnExceptionAction.Retry : OnExceptionAction.Dismiss;
    }
    #endregion



    #region Fire *Provider* Events - GraphVertex events

    // ----------
    // --- CREATE
    // ----------

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexCreateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Raised by <c>vertexCallBack</c>.
    /// </para>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteVertexCreateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexCreateAccepted(VertexEventArgs e) {
      GraphSyncProvider.OnRemoteVertexCreateAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexCreateCancelled"/> event.
    /// This event is raised when the Remote system has declined to create the GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteVertexCreateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexCreateCancelled(VertexEventArgs e) {
      GraphSyncProvider.OnRemoteVertexCreateCancelled(e);
    }
    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexCreateError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexCreateError(VertexErrorEventArgs e) {
      GraphSyncProvider.OnRemoteVertexCreateError(e);
    }

    // ----------
    // --- UPDATE
    // ----------


    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexUpdateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteVertexUpdateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexUpdateAccepted(VertexEventArgs e) {
      GraphSyncProvider.OnRemoteVertexUpdateAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexUpdateCancelled"/> event.
    /// This event is raised when the Remote system has declined to update the GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteVertexUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexUpdateCancelled(VertexEventArgs e) {
      GraphSyncProvider.OnRemoteVertexUpdateCancelled(e);
    }
    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexUpdateError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexUpdateError(VertexErrorEventArgs e) {
      GraphSyncProvider.OnRemoteVertexUpdateError(e);
    }





    // ----------
    // --- DELETE
    // ----------

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexDeleteAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteVertexDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexDeleteAccepted(VertexEventArgs e) {
      GraphSyncProvider.OnRemoteVertexDeleteAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexDeleteCancelled"/> event.
    /// This event is raised when the Remote system has declined to delete the GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteVertexDeleteAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexDeleteCancelled(VertexEventArgs e) {
      GraphSyncProvider.OnRemoteVertexDeleteCancelled(e);
    }
    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteVertexDeleteError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteVertexDeleteError(VertexErrorEventArgs e) {
      GraphSyncProvider.OnRemoteVertexDeleteError(e);
    }

    #endregion


  }//Class:End
}//Namespace:End