using System;
using System.Collections.Generic;
using System.Text;

using XAct.Graphs;
using XAct.Data;
using XAct.SmartClients.DisconnectedAgent; //For refs to Request, etc.

namespace XAct.Graphs {


  /// <summary>
  /// Class of Callback Methods
  /// invoked by the Request dispatcher upon
  /// return from the server after completing SyncPush webmethods
  /// involving Edges.
  /// </summary>
  public class SyncPushEdgeCallBacks {

    #region Properties
    /// <summary>
    /// Gets the graph sync provider.
    /// </summary>
    /// <value>The graph sync provider.</value>
    public GraphSyncProviderBase GraphSyncProvider {
      get {
        return GraphSyncManager.Instance;
      }
    }
    #endregion


    #region Public Methods - GraphEdge Related
    /// <summary>
    /// Public method invoked by the RequestManager upon the Request 
    /// for creating, updating, or deleting an GraphEdge 
    /// having been successfully processed by the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// If the remote system accepted the request, and depending on the
    /// <c>Action</c> requested, invokes
    /// one of <see cref="OnRemoteEdgeCreateAccepted"/>,
    /// <see cref="OnRemoteEdgeUpdateAccepted"/>, or
    /// <see cref="OnRemoteEdgeDeleteAccepted"/>.
    /// </para>
    /// <para>
    /// If the remote server denied the request, depending on the 
    /// <c>Action</c> requested, invokes
    /// one of <see cref="OnRemoteEdgeCreateCancelled"/>,
    /// <see cref="OnRemoteEdgeUpdateCancelled"/>, or
    /// <see cref="OnRemoteEdgeDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="request">The Request package.</param>
    /// <param name="param">The params array sent.</param>
    /// <param name="result">The result of the Request.</param>
    public void edgeCallback(Request request, object[] param, object result) {
      //Extract the XmlEdge, which was the first
      //argument passed when creating the Request package
      //in 'createEdgeRequest':
      XmlEdge xmlEdge = (XmlEdge)param[0];
      //Convert the XmlEdge back into a real GraphEdge:
      GraphEdge edge = new GraphEdge(xmlEdge.SourceId, xmlEdge.TargetId);


      bool ok = (bool)result;

      //Get description of Request:
      string description = request.Description;

      //Parse the Action from the Tag... Hum...
      GraphSyncDisconnectedProvider.Action action = (GraphSyncDisconnectedProvider.Action)Enum.Parse(typeof(GraphSyncDisconnectedProvider.Action), request.Behavior.Tag);

      //Create an event package and raise the appropiate
      //event depending on whether the remote server accepted
      //or denied the request.
      EdgeEventArgs e = new EdgeEventArgs(edge);

      //Perform a two step switch between the six different endings:
      switch (action) {
        case GraphSyncDisconnectedProvider.Action.Create:
          if (ok) {
            OnRemoteEdgeCreateAccepted(e);
          }
          else {
            OnRemoteEdgeCreateCancelled(e);
          }
          break;
        case GraphSyncDisconnectedProvider.Action.Update:
          if (ok) {
            OnRemoteEdgeUpdateAccepted(e);
          }
          else {
            OnRemoteEdgeUpdateCancelled(e);
          }
          break;

        case GraphSyncDisconnectedProvider.Action.Delete:
          if (ok) {
            OnRemoteEdgeDeleteAccepted(e);
          }
          else {
            OnRemoteEdgeDeleteCancelled(e);
          }
          break;

        default:
          throw new Exception(String.Format("invalid action '{0}' for remote edge operation", action));
      }
    }


    /// <summary>
    /// Public method invoked by the RequestManager upon an error 
    /// occuring during the processing of the request for
    /// creating, updating, or deleting a GraphEdge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Depending on the <see cref="Request"/>'s <see cref="GraphSyncDisconnectedProvider.Action"/>,
    /// invokes one of 
    /// <see cref="OnRemoteEdgeCreateError"/>,
    /// <see cref="OnRemoteEdgeUpdateError"/>, or 
    /// <see cref="OnRemoteEdgeDeleteError"/>.
    /// </para>
    /// <para>
    /// If an event handler sets the <see cref="P:EdgeErrorEventArgs.Retry"/> flag 
    /// to <c>true</c>, the <see cref="Request"/> is requeued.
    /// </para>
    /// </remarks>
    /// <param name="request">The request.</param>
    /// <param name="error">The error.</param>
    /// <returns></returns>
    public OnExceptionAction edgeErrorCallback(Request request, Exception error) {
      //Extract the XmlEdge, which was the first
      //argument passed when creating the Request package
      //in 'createEdgeRequest':
      XmlEdge xmlEdge = (XmlEdge)request.Parameters[0];
      //Convert the XmlEdge back into a real GraphEdge:
      GraphEdge edge = new GraphEdge(xmlEdge.SourceId, xmlEdge.TargetId);

      //Get description of Request:
      string description = request.Description;

      //Parse the Action from the Tag... Hum...
      GraphSyncDisconnectedProvider.Action action = (GraphSyncDisconnectedProvider.Action)Enum.Parse(typeof(GraphSyncDisconnectedProvider.Action), request.Behavior.Tag);

      //Create an error EventArgs package:
      EdgeErrorEventArgs e = new EdgeErrorEventArgs(edge, error);

      //Raise an event so that someone can decide
      //whether to retry or not,
      //remove the edges, whatever...

      //Perform a two step switch between the three different endings:
      switch (action) {
        case GraphSyncDisconnectedProvider.Action.Create:
          OnRemoteEdgeCreateError(e);
          break;
        case GraphSyncDisconnectedProvider.Action.Update:
          OnRemoteEdgeUpdateError(e);
          break;
        case GraphSyncDisconnectedProvider.Action.Delete:
          OnRemoteEdgeDeleteError(e);
          break;
        default:
          throw new Exception(String.Format("invalid action '{0}' for edge operation", action));
      }

      //Retry? The default is false:
      return e.Retry ? OnExceptionAction.Retry : OnExceptionAction.Dismiss;
    }
    #endregion

    #region Fire *Provider* Events - GraphEdge



    // ----------
    // --- CREATE
    // ----------

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeCreateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteEdgeCreateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeCreateAccepted(EdgeEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeCreateAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeCreateCancelled"/> event.
    /// This event is raised when the Remote system has declined to create the GraphEdge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteEdgeCreateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeCreateCancelled(EdgeEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeCreateCancelled(e);
    }
    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeCreateError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeCreateError(EdgeErrorEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeCreateError(e);
    }
    // ----------
    // --- UPDATE
    // ----------

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeUpdateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteEdgeUpdateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeUpdateAccepted(EdgeEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeUpdateAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeUpdateCancelled"/> event.
    /// This event is raised when the Remote system has declined to delete the GraphEdge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteEdgeUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeUpdateCancelled(EdgeEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeUpdateCancelled(e);
    }
    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeUpdateError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeUpdateError(EdgeErrorEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeUpdateError(e);
    }

    // ----------
    // --- DELETE
    // ----------

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeDeleteAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteEdgeDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeDeleteAccepted(EdgeEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeDeleteAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeDeleteCancelled"/> event.
    /// This event is raised when the Remote system has declined to delete the GraphEdge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteEdgeDeleteAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeDeleteCancelled(EdgeEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeDeleteCancelled(e);
    }
    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteEdgeDeleteError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteEdgeDeleteError(EdgeErrorEventArgs e) {
      GraphSyncProvider.OnRemoteEdgeDeleteError(e);
    }
    #endregion


  }//Class:End
}//Namespace:End
