using System;
using System.Collections.Generic;
using System.Text;

using XAct.Graphs;
using XAct.Data;
using XAct.SmartClients.DisconnectedAgent; //For refs to Request, etc.

namespace XAct.Graphs {


  /// <summary>
  /// Class of Callback Methods
  /// invoked by the Request dispatcher upon
  /// return from the server after completing SyncPush webmethods
  /// involving DataObjects.
  /// </summary>
  public class SyncPushDataObjectCallBacks {

    #region Properties
    /// <summary>
    /// Gets the graph sync provider.
    /// </summary>
    /// <value>The graph sync provider.</value>
    public GraphSyncProviderBase GraphSyncProvider {
      get {
        return GraphSyncManager.Instance;
      }
    }
    #endregion

    #region Public Methods - DataObject Related

    /// <summary>
    /// Public method invoked by the RequestManager upon the Request 
    /// for creating, updating, or deleting a DataObject 
    /// having been successfully processed by the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// If the remote system accepted the request, and depending on the
    /// <c>Action</c> requested, invokes
    /// one of <see cref="OnRemoteDataObjectCreateAccepted"/>,
    /// <see cref="OnRemoteDataObjectUpdateAccepted"/>, or
    /// <see cref="OnRemoteDataObjectDeleteAccepted"/>.
    /// </para>
    /// <para>
    /// If the remote server denied the request, depending on the 
    /// <c>Action</c> requested, invokes
    /// one of <see cref="OnRemoteDataObjectCreateCancelled"/>,
    /// <see cref="OnRemoteDataObjectUpdateCancelled"/>, or
    /// <see cref="OnRemoteDataObjectDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="request">The Request package.</param>
    /// <param name="param">The params array sent.</param>
    /// <param name="result">The result of the Request.</param>
    public void dataObjectCallback(Request request, object[] param, object result) {
      //Extract the XmlDataObject, which was the first
      //argument passed when creating the Request package
      //in 'createDataObjectRequest':
      XmlDataObject xmlDataObject = (XmlDataObject)param[0];
      //Convert the XmlDataObject back into a real DataObject:
      IDataObjectFactory dataObjectFactory;
      object dataObject;
      xmlDataObject.Deserialize(out dataObjectFactory, out dataObject);

      bool ok = (bool)result;

      //Get description of Request:
      string description = request.Description;

      //Parse the Action from the Tag... Hum...
      GraphSyncDisconnectedProvider.Action action = (GraphSyncDisconnectedProvider.Action)Enum.Parse(typeof(GraphSyncDisconnectedProvider.Action), request.Behavior.Tag);

      //Create an event package and raise the appropiate
      //event depending on whether the remote server accepted
      //or denied the request.
      DataObjectEventArgs e = new DataObjectEventArgs(xmlDataObject.DataObjectFactoryId, dataObject);

      //Perform a two step switch between the six different endings:
      switch (action) {
        case GraphSyncDisconnectedProvider.Action.Create:
          if (ok) {
            OnRemoteDataObjectCreateAccepted(e);
          }
          else {
            OnRemoteDataObjectCreateCancelled(e);
          }
          break;
        case GraphSyncDisconnectedProvider.Action.Update:
          if (ok) {
            OnRemoteDataObjectUpdateAccepted(e);
          }
          else {
            OnRemoteDataObjectUpdateCancelled(e);
          }
          break;

        case GraphSyncDisconnectedProvider.Action.Delete:
          if (ok) {
            OnRemoteDataObjectDeleteAccepted(e);
          }
          else {
            OnRemoteDataObjectDeleteCancelled(e);
          }
          break;

        default:
          throw new Exception(String.Format("invalid action '{0}' for remote dataObject operation", action));
      }
    }


    /// <summary>
    /// Public method invoked by the RequestManager upon an error 
    /// occuring during the processing of the request for
    /// creating, updating, or deleting a DataObject.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Depending on the <see cref="Request"/>'s <see cref="GraphSyncDisconnectedProvider.Action"/>,
    /// invokes one of 
    /// <see cref="OnRemoteDataObjectCreateError"/>,
    /// <see cref="OnRemoteDataObjectUpdateError"/>, or 
    /// <see cref="OnRemoteDataObjectDeleteError"/>.
    /// </para>
    /// <para>
    /// If an event handler sets the <see cref="P:DataObjectErrorEventArgs.Retry"/> flag 
    /// to <c>true</c>, the <see cref="Request"/> is requeued.
    /// </para>
    /// </remarks>
    /// <param name="request">The request.</param>
    /// <param name="error">The error.</param>
    /// <returns></returns>
    public OnExceptionAction dataObjectErrorCallback(Request request, Exception error) {
      //Extract the XmlDataObject, which was the first
      //argument passed when creating the Request package
      //in 'createDataObjectRequest':
      XmlDataObject xmlDataObject = (XmlDataObject)request.Parameters[0];
      //Convert the XmlDataObject back into a real DataObject:
      IDataObjectFactory dataObjectFactory;
      object dataObject;
      xmlDataObject.Deserialize(out dataObjectFactory, out dataObject);

      //Get description of Request:
      string description = request.Description;

      //Parse the Action from the Tag... Hum...
      GraphSyncDisconnectedProvider.Action action = (GraphSyncDisconnectedProvider.Action)Enum.Parse(typeof(GraphSyncDisconnectedProvider.Action), request.Behavior.Tag);

      //Create an error EventArgs package:
      DataObjectErrorEventArgs e = new DataObjectErrorEventArgs(xmlDataObject.DataObjectFactoryId, dataObject, error);

      //Raise an event so that someone can decide
      //whether to retry or not,
      //remove the dataObjects, whatever...

      //Perform a two step switch between the three different endings:
      switch (action) {
        case GraphSyncDisconnectedProvider.Action.Create:
          OnRemoteDataObjectCreateError(e);
          break;
        case GraphSyncDisconnectedProvider.Action.Update:
          OnRemoteDataObjectUpdateError(e);
          break;
        case GraphSyncDisconnectedProvider.Action.Delete:
          OnRemoteDataObjectDeleteError(e);
          break;
        default:
          throw new Exception(String.Format("invalid action '{0}' for dataObject operation", action));
      }

      //Retry? The default is false:
      return e.Retry ? OnExceptionAction.Retry : OnExceptionAction.Dismiss;
    }


    #endregion

    #region Fire *Provider* Events - DataObjects



    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectCreateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteDataObjectCreateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectCreateAccepted(DataObjectEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectCreateAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectCreateCancelled"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteDataObjectCreateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectCreateCancelled(DataObjectEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectCreateCancelled(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectCreateError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="DataObjectErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectCreateError(DataObjectErrorEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectCreateError(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectUpdateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteDataObjectUpdateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectUpdateAccepted(DataObjectEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectUpdateAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectUpdateCancelled"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectUpdateCancelled(DataObjectEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectUpdateCancelled(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectUpdateError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="DataObjectErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectUpdateError(DataObjectErrorEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectUpdateError(e);
    }


    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectDeleteAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// the callback would have invoked <see cref="OnRemoteDataObjectDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectDeleteAccepted(DataObjectEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectDeleteAccepted(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectDeleteCancelled"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the callback would have invoked <see cref="OnRemoteDataObjectDeleteAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectDeleteCancelled(DataObjectEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectDeleteCancelled(e);
    }

    /// <summary>
    /// Fires the GraphSyncProvider's <see cref="E:RemoteDataObjectDeleteError"/> event.
    /// </summary>
    /// <param name="e">The <see cref="DataObjectErrorEventArgs"/> instance containing the event data.</param>
    virtual protected void OnRemoteDataObjectDeleteError(DataObjectErrorEventArgs e) {
      GraphSyncProvider.OnRemoteDataObjectDeleteError(e);
    }
    #endregion


  }//Class:End
}//Namespace:End
