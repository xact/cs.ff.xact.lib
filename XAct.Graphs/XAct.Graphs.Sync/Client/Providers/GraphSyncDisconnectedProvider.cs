using System;
using System.Collections.Generic;
using System.Text;

using XAct.Graphs;
using XAct.Data;
using XAct.SmartClients.DisconnectedAgent;

namespace XAct.Graphs {

  /// <summary>
  /// Implementation of the 
  /// <see cref="GraphSyncProviderBase"/> abstract provider.
  /// Provides synchronization between two 
  /// Graphs managed by GraphManagers.
  /// </summary>
  public class GraphSyncDisconnectedProvider : GraphSyncProviderBase {

    #region Enums

    /// <summary>
    /// Enumeration of the type of operation to invoke.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Used by <see cref="RemoteCreateVertex"/> to transmit to 
    /// <see cref="createVertexRequest"/>, which embeds it as a tag string,
    /// and later is parsed back into an enum value by <see cref="SyncPushVertexCallBacks.vertexCallback"/>.
    /// </para>
    /// </remarks>
    public enum Action {
      /// <summary>
      /// Create a new GraphVertex/Vertices.
      /// </summary>
      Create,
      /// <summary>
      /// Update an existing GraphVertex/Vertices.
      /// </summary>
      Update,
      /// <summary>
      /// Delete an existing GraphVertex/Vertices.
      /// </summary>
      Delete,
    }
    #endregion

    #region Fields

    private static readonly string queueFileName = @"c:\isightQueue.xml";


    private IDataObjectFactoryCollection DataSources {
      get {
        return DataObjectFactoryManager.Instance;
      }
    }

    #endregion


    #region Properties
    /// <summary>
    /// Gets the Microsoft Practices Disconnected Service Request Manager.
    /// </summary>
    /// <value>The request manager.</value>
    public RequestManager RequestManager{
      get {
        //Ensure initilalized:
        InitializeRequestManager();
        return _RequestManager;
      }
  }
    private RequestManager _RequestManager;

    /// <summary>
    /// Gets the URL of the default webservice.
    /// </summary>
    /// <value>The URL.</value>
    public string RequestServiceUrl {
      get {
        if (string.IsNullOrEmpty(_RequestServiceUrl)) {
          _RequestServiceUrl = Properties.Settings.Default.XAct_Graphs_GraphSync_WebServices_GraphSyncServices; //XAct_Sync_SyncServer_SyncService

        }
        return _RequestServiceUrl;
      }
    }
    private string _RequestServiceUrl;


    /// <summary>
    /// Gets the URL of the SyncPush webservices.
    /// If empty, returns <see cref="RequestServiceUrl"/>.
    /// </summary>
    /// <value>The URL.</value>
    public string RequestSyncPushServiceUrl {
      get {
        if (string.IsNullOrEmpty(_RequestSyncPushServiceUrl)) {
          _RequestSyncPushServiceUrl = RequestServiceUrl;

        }
        return _RequestSyncPushServiceUrl;
      }
    }
    private string _RequestSyncPushServiceUrl;


    /// <summary>
    /// Gets the URL of the SyncPull webservices.
    /// If empty, returns <see cref="RequestServiceUrl"/>.
    /// </summary>
    /// <value>The URL.</value>
    public string RequestSyncPullServiceUrl {
      get {
        if (string.IsNullOrEmpty(_RequestSyncPullServiceUrl)) {
          _RequestSyncPullServiceUrl = RequestServiceUrl;
        }
        return _RequestSyncPullServiceUrl;
      }
    }
    private string _RequestSyncPullServiceUrl;

    /// <summary>
    /// Gets the URL of the vertex webservices.
    /// If empty, returns <see cref="RequestSyncPushServiceUrl"/>.
    /// </summary>
    /// <value>The URL.</value>
    public string RequestSyncPushVertexServiceUrl {
      get {
        if (string.IsNullOrEmpty(_RequestSyncPushVertexServiceUrl)) {
          _RequestSyncPushVertexServiceUrl = this.RequestSyncPushServiceUrl;
        }
        return _RequestSyncPushVertexServiceUrl;
      }
    }
    private string _RequestSyncPushVertexServiceUrl;


    /// <summary>
    /// Gets the URL of the edge webservices.
    /// If empty, returns <see cref="RequestSyncPushServiceUrl"/>.
    /// </summary>
    /// <value>The URL.</value>
    public string RequestSyncPushEdgeServiceUrl {
      get {
        if (string.IsNullOrEmpty(_RequestSyncPushEdgeServiceUrl)) {
          _RequestSyncPushEdgeServiceUrl = this.RequestSyncPushServiceUrl;
        }
        return _RequestSyncPushEdgeServiceUrl;
      }
    }
    private string _RequestSyncPushEdgeServiceUrl;


    /// <summary>
    /// Gets the URL of the DataObject webservices.
    /// If empty, returns <see cref="RequestSyncPushServiceUrl"/>.
    /// </summary>
    /// <value>The URL.</value>
    public string RequestSyncPushDataObjectServiceUrl {
      get {
        if (string.IsNullOrEmpty(_RequestSyncPushDataObjectServiceUrl)) {
          _RequestSyncPushDataObjectServiceUrl = this.RequestSyncPushServiceUrl;
        }
        return _RequestSyncPushDataObjectServiceUrl;
      }
    }
    private string _RequestSyncPushDataObjectServiceUrl;

    #endregion


    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="T:GraphSyncDisconnectedProvider"/> class.
    /// </summary>
    public GraphSyncDisconnectedProvider() {
      InitializeRequestManager();
    }
    #endregion

    #region Method Overrides - Implementation of ProviderBase
    /// <summary>
    /// Initializes the provider.
    /// </summary>
    /// <param name="name">The friendly name of the provider.</param>
    /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
    /// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
    /// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"></see> on a provider after the provider has already been initialized.</exception>
    /// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
      base.Initialize(name, config);
    }
    #endregion

    #region Public Methods - Implementation of GraphSyncProviderBase - GraphVertex Handling
    /// <summary>
    /// Request the creation of a GraphVertex on the Remote system.
    /// </summary>
    /// <param name="vertex">The vertex.</param>
    /// <remarks>
    /// 	<para>
    /// Invoked by <see cref="M:GraphSyncProviderBase.GraphManager_VertexCreated"/>
    /// event handler for the GraphManager's <see cref="E:GraphProviderBase.VertexCreated"/> event.
    /// </para>
    /// 	<para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteCreateVertex(GraphVertex vertex) {
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }
      Request request = createVertexRequest(Action.Create, vertex);
      RequestManager.RequestQueue.Enqueue(request);
    }


    /// <summary>
    /// Request the Update of a GraphVertex from the Remote system.
    /// </summary>
    /// <param name="vertex">The vertex.</param>
    /// <remarks>
    /// 	<para>
    /// Invoked by <see cref="GraphSyncProviderBase.GraphManager_VertexUpdated"/>
    /// event handler for the GraphManager's <see cref="E:GraphProviderBase.VertexUpdated"/> event.
    /// </para>
    /// 	<para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteUpdateVertex(GraphVertex vertex) {
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }
      Request request = createVertexRequest(Action.Update, vertex);
      RequestManager.RequestQueue.Enqueue(request);
    }

    /// <summary>
    /// Request the Deletion of a GraphVertex from the Remote system.
    /// </summary>
    /// <param name="vertex">The vertex.</param>
    /// <remarks>
    /// 	<para>
    /// Invoked by <see cref="GraphSyncProviderBase.GraphManager_VertexDeleted"/>
    /// event handler for the GraphManager's <see cref="E:GraphProviderBase.VertexDeleted"/> event.
    /// </para>
    /// 	<para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteDeleteVertex(GraphVertex vertex) {
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }
      Request request = createVertexRequest(Action.Delete, vertex);
      RequestManager.RequestQueue.Enqueue(request);
    }
    #endregion

    #region Public Methods - Implementation of GraphSyncProviderBase - GraphEdge Handling
    /// <summary>
    /// Request the creation of an GraphEdge on the Remote system.
    /// </summary>
    /// <param name="edge">The edge.</param>
    /// <remarks>As it is an asynchronous system, returns no value at this time.</remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteCreateEdge(GraphEdge edge) {
      if (edge == null) {
        throw new System.ArgumentNullException("edge");
      }
      Request request = createEdgeRequest(Action.Create, edge);
      _RequestManager.RequestQueue.Enqueue(request);
    }

    /// <summary>
    /// Request the creation of an GraphEdge on the Remote system.
    /// </summary>
    /// <param name="edge">The edge.</param>
    /// <remarks>As it is an asynchronous system, returns no value at this time.</remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteUpdateEdge(GraphEdge edge) {
      if (edge == null) {
        throw new System.ArgumentNullException("edge");
      }
      Request request = createEdgeRequest(Action.Update, edge);
      _RequestManager.RequestQueue.Enqueue(request);
    }

    /// <summary>
    /// Request the removal of an GraphEdge on the Remote system.
    /// </summary>
    /// <param name="edge">The edge.</param>
    /// <remarks>As it is an asynchronous system, returns no value at this time.</remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteDeleteEdge(GraphEdge edge) {
      if (edge == null) {
        throw new System.ArgumentNullException("edge");
      }
      Request request = createEdgeRequest(Action.Delete, edge);
      _RequestManager.RequestQueue.Enqueue(request);
    }
#endregion

    #region Public Methods - Implementation of GraphSyncProviderBase - DataObjects Handling

    /// <summary>
    /// Request the creation of a DataObject on the Remote system.
    /// </summary>
    /// <param name="dataObjectFactory"></param>
    /// <param name="dataObject"></param>
    /// <remarks>As it is an asynchronous system, returns no value at this time.</remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteCreateDataObject(IDataObjectFactory dataObjectFactory, object dataObject) {
      //Check Args:
      if (dataObjectFactory == default(IDataObjectFactory)) {
        throw new System.ArgumentNullException("dataObjectFactory");
      }
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }

      Request request = createDataObjectRequest(Action.Create, dataObjectFactory.Id, dataObject);
      _RequestManager.RequestQueue.Enqueue(request);

    }


    /// <summary>
    /// Request the updating of a DataObject on the Remote system.
    /// </summary>
    /// <param name="dataObjectFactory"></param>
    /// <param name="dataObject"></param>
    /// <remarks>As it is an asynchronous system, returns no value at this time.</remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteUpdateDataObject(IDataObjectFactory dataObjectFactory, object dataObject) {
      //Check Args:
      if (dataObjectFactory == default(IDataObjectFactory)) {
        throw new System.ArgumentNullException("dataObjectFactory");
      }
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }

      Request request = createDataObjectRequest(Action.Update, dataObjectFactory.Id, dataObject);
      _RequestManager.RequestQueue.Enqueue(request);

    }

    /// <summary>
    /// Request the removal of a DataObject from the Remote system.
    /// </summary>
    /// <param name="dataObjectFactory"></param>
    /// <param name="dataObject"></param>
    /// <remarks>As it is an asynchronous system, returns no value at this time.</remarks>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    override public void RemoteDeleteDataObject(IDataObjectFactory dataObjectFactory, object dataObject) {
      //Check Args:
      if (dataObjectFactory == default(IDataObjectFactory)) {
        throw new System.ArgumentNullException("dataObjectFactory");
      }
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }

      Request request = createDataObjectRequest(Action.Delete, dataObjectFactory.Id, dataObject);
      _RequestManager.RequestQueue.Enqueue(request);
    }
#endregion

    #region Public Methods - Implementation of GraphSyncProviderBase - SyncPull 
    /// <summary>
    /// Request a Pulled Synchronization of
    /// all New Vertices (vertices created on server since lastSyncDate),
    /// all Updated Vertices (vertices changed on server since lastSyncDate),
    /// all Deleted Vertices (vertices deleted on server since lastSyncDate),
    /// as well as all pertinent Attributes, DataObject and GraphEdge information.
    /// </summary>
    override public void RequestSyncPull() {
      
      //Request request = createSyncRequest();
      //_RequestManager.RequestQueue.Enqueue(request);
    }


    #endregion

    #region Protected Methods - Create Request Objects

    /// <summary>
    /// Creates a <see cref="Request"/> around the given GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="RemoteCreateVertex"/>, <see cref="RemoteUpdateVertex"/>,
    /// and <see cref="RemoteDeleteVertex"/>.
    /// </para>
    /// </remarks>
    /// <param name="action">The action flag.</param>
    /// <param name="vertex">The vertex.</param>
    /// <returns>A Request package to be enqueued by the RequestManager.</returns>
    /// <exception cref="System.ArgumentNullException">Exception raised if argument is null.</exception>
    protected Request createVertexRequest(Action action, GraphVertex vertex) {
      //Check Args:
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }

      //A string description of the queued operation (can be used to display in UI):
      string description;
      //Embedded info that receiving callback can use to fork:
      string tag = action.ToString();
      //The serializeable/queueable ref to the Web Service Proxy to invoke:
      CommandCallback methodToInvoke;
      //The serializeable/queueable ref to the local method to instantiate upon successful invocation of web service:
      CommandCallback onSuccessCallBackMethod;
      //The serializeable/queueable ref to the local method to instantiate if something goes wrong:
      CommandCallback onErrorCallBackMethod;

      //Get the Type of the web service proxy class
      //(ie the local proxy for the remote webservice class):
      Type webServiceType = typeof(WebServices.GraphSyncServices);

      /*
      SyncServer.CredentialSoapHeader header = new XAct.Graphs.SyncServer.CredentialSoapHeader();
      header.UserName = "bla";
      header.Password = "pass";
      */

      switch (action) {
        case Action.Create:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "VertexCreate");
          description = string.Format("GraphVertex Create [{0}]", vertex.Id);
          break;
        case Action.Update:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "VertexUpdate");
          description = string.Format("GraphVertex Update [{0}]", vertex.Id);
          break;
        case Action.Delete:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "VertexDelete");
          description = string.Format("GraphVertex Removal [{0}]", vertex.Id);
          break;
        default:
          throw new Exception(String.Format("invalid action '{0}' for remote edge action", action));
      }

      Type callBackClassType = this.GetType();
      //Create the callback from callback class/methodName:
      onSuccessCallBackMethod = new CommandCallback(typeof(SyncPushVertexCallBacks), "vertexCallback");
      //Create the callback from callback class/methodName, in case there was an error:
      onErrorCallBackMethod = new CommandCallback(typeof(SyncPushVertexCallBacks), "vertexErrorCallback");



      //Once we have the CommandCallback created,
      //we can use them as args for creating a Request 
      //package:
      Request request = 
        new Request(
        description, //Description
        tag, //Tag
        Int32.MaxValue, //Cost
        methodToInvoke,//MethodToCall
        onSuccessCallBackMethod,//CallBack if successfully processed by server.
        onErrorCallBackMethod,//CallBack if there was an error.
        new XmlVertex(vertex));//Args....

      //Set the url of the request manually:
      request.Url = this.RequestSyncPushVertexServiceUrl;


      //And return:
      return request;
    }


    /// <summary>
    /// Creates a <see cref="Request"/> around the given GraphEdge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="RemoteCreateEdge"/>, <see cref="RemoteUpdateEdge"/>,
    /// and <see cref="RemoteDeleteEdge"/>.
    /// </para>
    /// </remarks>
    /// <param name="action">The action flag.</param>
    /// <param name="edge">The edge.</param>
    /// <returns>A Request package to be enqueued by the RequestManager.</returns>
    /// <exception cref="System.ArgumentNullException">Exception raised if argument is null.</exception>
    protected Request createEdgeRequest(Action action, GraphEdge edge) {
      //Check Args:
      if (edge == null) {
        throw new System.ArgumentNullException("edge");
      }

      //A string description of the queued operation (can be used to display in UI):
      string description;
      //Embedded info that receiving callback can use to fork:
      string tag = action.ToString();
      //The serializeable/queueable ref to the Web Service Proxy to invoke:
      CommandCallback methodToInvoke;
      //The serializeable/queueable ref to the local method to instantiate upon successful invocation of web service:
      CommandCallback onSuccessCallBackMethod;
      //The serializeable/queueable ref to the local method to instantiate if something goes wrong:
      CommandCallback onErrorCallBackMethod;

      //Get the Type of the web service proxy class
      //(ie the local proxy for the remote webservice class):
      Type webServiceType = typeof(WebServices.GraphSyncServices);

      switch (action) {
        case Action.Create:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "EdgeCreate");
          description = string.Format("GraphEdge Create [{0}:{1}]",edge.SourceId, edge.TargetId);
          break;
        case Action.Update:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "EdgeUpdate");
          description = string.Format("GraphEdge Update [{0}:{1}]", edge.SourceId, edge.TargetId);
          break;
        case Action.Delete:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "EdgeDelete");
          description = string.Format("GraphEdge Removal [{0}:{1}]", edge.SourceId, edge.TargetId);
          break;
        default:
          throw new Exception(String.Format("invalid action '{0}' for remote edge action", action));
      }

      //Create the callback from callback class/methodName:
      onSuccessCallBackMethod = new CommandCallback(typeof(SyncPushEdgeCallBacks), "edgeCallback");
      //Create the callback from callback class/methodName, in case there was an error:
      onErrorCallBackMethod = new CommandCallback(typeof(SyncPushEdgeCallBacks), "edgeErrorCallback");



      //Once we have the CommandCallback created,
      //we can use them as args for creating a Request 
      //package:
      Request request = 
        new Request(
        description, //Description
        tag, //Tag
        Int32.MaxValue, //Cost
        methodToInvoke,//MethodToCall
        onSuccessCallBackMethod,//CallBack if successfully processed by server.
        onErrorCallBackMethod,//CallBack if there was an error.
        new XmlEdge(edge));//Args...


      //Set the url of the request manually:
      request.Url = this.RequestSyncPushEdgeServiceUrl;

      return request;
    }


    /// <summary>
    /// Creates a <see cref="Request"/> around the given DataObject.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="RemoteCreateDataObject"/>, <see cref="RemoteUpdateDataObject"/>,
    /// and <see cref="RemoteDeleteDataObject"/>.
    /// </para>
    /// </remarks>
    /// <param name="action">The action flag.</param>
    /// <param name="dataObjectFactoryId">The DataObject source Id.</param>
    /// <param name="dataObject">The dataObject.</param>
    /// <returns>A Request package to be enqueued by the RequestManager.</returns>
    /// <exception cref="System.ArgumentNullException">Exception raised if argument is null.</exception>
    protected Request createDataObjectRequest(Action action, int dataObjectFactoryId, object dataObject) {
      //CheckArgs:
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }


      //A string description of the queued operation (can be used to display in UI):
      string description;
      //Embedded info that receiving callback can use to fork:
      string tag = action.ToString();
      //The serializeable/queueable ref to the Web Service Proxy to invoke:
      CommandCallback methodToInvoke;
      //The serializeable/queueable ref to the local method to instantiate upon successful invocation of web service:
      CommandCallback onSuccessCallBackMethod;
      //The serializeable/queueable ref to the local method to instantiate if something goes wrong:
      CommandCallback onErrorCallBackMethod;

      //Get the Type of the web service proxy class
      //(ie the local proxy for the remote webservice class):
      Type webServiceType = typeof(WebServices.GraphSyncServices);

      switch (action) {
        case Action.Create:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "RecordCreate");
          description = string.Format("DataObject Create [{0}]", dataObject.ToString());
          break;
        case Action.Update:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "RecordUpdate");
          description = string.Format("DataObject Update [{0}]", dataObject.ToString());
          break;
        case Action.Delete:
          //Create CallBackMethod by getting handle on callback class/methodName:
          methodToInvoke = new CommandCallback(webServiceType, "RecordDelete");
          description = string.Format("DataObject Removal [{0}]", dataObject.ToString());
          break;
        default:
          throw new Exception(String.Format("invalid action '{0}' for remote edge action", action));
      }
      onSuccessCallBackMethod = new CommandCallback(typeof(SyncPushDataObjectCallBacks), "createDataObjectCallback");
      onErrorCallBackMethod = new CommandCallback(typeof(SyncPushDataObjectCallBacks), "createDataObjectErrorCallback");
      //Once we have the CommandCallback created,
      //we can use them as args for creating a Request 
      //package:
      Request request =
        new Request(
        description, //Description
        tag, //Tag
        Int32.MaxValue, //Cost
        methodToInvoke,//MethodToCall
        onSuccessCallBackMethod,//CallBack if successfully processed by server.
        onErrorCallBackMethod,//CallBack if there was an error.
        new XmlDataObject(dataObjectFactoryId,dataObject));//Args...

      //---------------------------
      //request.Call = new CommandCallback(webServiceType, "CreateRecord");

      //---------------------------
      //Set the url of the request manually:
      request.Url = this.RequestSyncPushDataObjectServiceUrl;

      return request;

    }

    /// <summary>
    /// Creates a <see cref="Request"/> for a full pull sync based on the given lastSyncDate.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="RemoteCreateDataObject"/>, <see cref="RemoteUpdateDataObject"/>,
    /// and <see cref="RemoteDeleteDataObject"/>.
    /// </para>
    /// </remarks>
    /// <param name="lastSyncDate">Date of last completed Sync job.</param>
    /// <returns>A Request package to be enqueued by the RequestManager.</returns>
    protected virtual Request createSyncRequest(DateTime lastSyncDate) {

      //A string description of the queued operation (can be used to display in UI):
      string description;
      //Embedded info that receiving callback can use to fork:
      string tag = string.Empty;
      //The serializeable/queueable ref to the Web Service Proxy to invoke:
      CommandCallback methodToInvoke;
      //The serializeable/queueable ref to the local method to instantiate upon successful invocation of web service:
      CommandCallback onSuccessCallBackMethod;
      //The serializeable/queueable ref to the local method to instantiate if something goes wrong:
      CommandCallback onErrorCallBackMethod;

      //Get the Type of the web service proxy class
      //(ie the local proxy for the remote webservice class):
      Type webServiceType = typeof(WebServices.GraphSyncServices);

      methodToInvoke = new CommandCallback(webServiceType, "SyncPull");
      description = string.Format("Sync Pull Request");

      onSuccessCallBackMethod = new CommandCallback(typeof(SyncPullCallBacks), "syncPullCallback");
      onErrorCallBackMethod = new CommandCallback(typeof(SyncPullCallBacks), "syncPullErrorCallback");

      //Once we have the CommandCallback created,
      //we can use them as args for creating a Request 
      //package:
      Request request =
        new Request(
        description, //Description
        tag, //Tag
        Int32.MaxValue, //Cost
        methodToInvoke,//MethodToCall
        onSuccessCallBackMethod,//CallBack if successfully processed by server.
        onErrorCallBackMethod,//CallBack if there was an error.
        lastSyncDate);//Args...

      //---------------------------
      //request.Call = new CommandCallback(webServiceType, "CreateRecord");

      //---------------------------
      //Set the url of the request manually:
      request.Url = this.RequestSyncPullServiceUrl;

      return request;
    }
    #endregion

    #region Protected Methods - Initialization
    /// <summary>
    /// Initializes the MSPP DisconnectedAgent RequestManager.
    /// </summary>
    virtual protected void InitializeRequestManager() {
      if (_RequestManager != null) {
        return;
      }
      _RequestManager = RequestManager.Instance;
      //Initialize the Provider, with where to keep the queue,
      //what connection monitor to use, etc.
      IRequestQueue queue =
        new XAct.SmartClients.DisconnectedAgent.Implementations.DataSetQueryStore("queue", queueFileName);

      XAct.SmartClients.ConnectionMonitor.ConnectionMonitor connManager =
        XAct.SmartClients.ConnectionMonitor.ConnectionMonitorFactory.CreateFromConfiguration();

      IConnectionMonitor connAdapter = new ConnectionMonitorAdapter(connManager);

      IRequestDispatcher requestDispatcher = new WebServiceRequestDispatcher();

      _RequestManager.Initialize(queue, connAdapter, requestDispatcher);

      //And then start the thread:
      _RequestManager.StartAutomaticDispatch();
    }
    #endregion

  }//Class:End
}//Namespace:End
