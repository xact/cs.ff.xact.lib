using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration.Provider;


using XAct.Data;



namespace XAct.Graphs {

  /// <summary>
  /// Base abstract Provider for synchronization between two 
  /// Graphs managed by GraphManagers.
  /// </summary>
  public abstract class GraphSyncProviderBase : ProviderBase {


    #region Events Raised - GraphVertex
    /// <summary>
    /// Event raised after the remote system has accepted the creation of a GraphVertex.
    /// </summary>
    public event EventHandler<VertexEventArgs> RemoteVertexCreateAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the creation of a GraphVertex.
    /// </summary>
    public event EventHandler<VertexEventArgs> RemoteVertexCreateCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the creation of a GraphVertex on the remote system.
    /// </summary>
    public event EventHandler<VertexErrorEventArgs> RemoteVertexCreateError;

    /// <summary>
    /// Event raised after the remote system has accepted the update of a GraphVertex.
    /// </summary>
    public event EventHandler<VertexEventArgs> RemoteVertexUpdateAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the update of a GraphVertex.
    /// </summary>
    public event EventHandler<VertexEventArgs> RemoteVertexUpdateCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the updating of a GraphVertex on the remote system.
    /// </summary>
    public event EventHandler<VertexErrorEventArgs> RemoteVertexUpdateError;

    /// <summary>
    /// Event raised after the remote system has accepted the removal of a GraphVertex.
    /// </summary>
    public event EventHandler<VertexEventArgs> RemoteVertexDeleteAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the removal of a GraphVertex.
    /// </summary>
    public event EventHandler<VertexEventArgs> RemoteVertexDeleteCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the removal of a GraphVertex from the remote system.
    /// </summary>
    public event EventHandler<VertexErrorEventArgs> RemoteVertexDeleteError;
    #endregion

    #region Events Raised - GraphEdge
    /// <summary>
    /// Event raised after the remote system has accepted the creation of a GraphEdge.
    /// </summary>
    public event EventHandler<EdgeEventArgs> RemoteEdgeCreateAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the creation of a GraphEdge.
    /// </summary>
    public event EventHandler<EdgeEventArgs> RemoteEdgeCreateCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the creation of a GraphEdge on the remote system.
    /// </summary>
    public event EventHandler<EdgeErrorEventArgs> RemoteEdgeCreateError;

    /// <summary>
    /// Event raised after the remote system has accepted the update of a GraphEdge.
    /// </summary>
    public event EventHandler<EdgeEventArgs> RemoteEdgeUpdateAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the update of a GraphEdge.
    /// </summary>
    public event EventHandler<EdgeEventArgs> RemoteEdgeUpdateCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the updating of a GraphEdge on the remote system.
    /// </summary>
    public event EventHandler<EdgeErrorEventArgs> RemoteEdgeUpdateError;

    /// <summary>
    /// Event raised after the remote system has accepted the removal of a GraphEdge.
    /// </summary>
    public event EventHandler<EdgeEventArgs> RemoteEdgeDeleteAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the removal of a GraphEdge.
    /// </summary>
    public event EventHandler<EdgeEventArgs> RemoteEdgeDeleteCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the removal of a GraphEdge from the remote system.
    /// </summary>
    public event EventHandler<EdgeErrorEventArgs> RemoteEdgeDeleteError;
    #endregion

    #region Events Raised - DataObject
    /// <summary>
    /// Event raised after the remote system has accepted the creation of a DataObject.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> RemoteDataObjectCreateAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the creation of a DataObject.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> RemoteDataObjectCreateCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the creation of a DataObject on the remote system.
    /// </summary>
    public event EventHandler<DataObjectErrorEventArgs> RemoteDataObjectCreateError;

    /// <summary>
    /// Event raised after the remote system has accepted the update of a DataObject.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> RemoteDataObjectUpdateAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the update of a DataObject.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> RemoteDataObjectUpdateCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the updating of a DataObject on the remote system.
    /// </summary>
    public event EventHandler<DataObjectErrorEventArgs> RemoteDataObjectUpdateError;

    /// <summary>
    /// Event raised after the remote system has accepted the removal of a DataObject.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> RemoteDataObjectDeleteAccepted;
    /// <summary>
    /// Event raised after the remote system has rejected the removal of a DataObject.
    /// </summary>
    public event EventHandler<DataObjectEventArgs> RemoteDataObjectDeleteCancelled;
    /// <summary>
    /// Event raised when an error has occurred during the removal of a DataObject from the remote system.
    /// </summary>
    public event EventHandler<DataObjectErrorEventArgs> RemoteDataObjectDeleteError;
    #endregion



    #region Properties 
    /// <summary>
    /// Gets the GraphManager's Provider.
    /// </summary>
    /// <value>The graph provider.</value>
    protected GraphProviderBase GraphProvider {
      get {
        return GraphManager.Instance;
      }
    }


    protected DataObjectFactoryProviderBase DataObjectFactoryManager {
      get {
        return DataObjectFactoryManager;
      }
    }

    #endregion

    #region Constructors/Destructors
    /// <summary>
    /// Initializes a new instance of the <see cref="T:GraphSyncProviderBase"/> class.
    /// </summary>
    public GraphSyncProviderBase()
      : base() {
    }
    /// <summary>
    /// Releases unmanaged resources and performs other cleanup operations before the
    /// <see cref="T:XAct.Graphs.GraphSyncProviderBase"/> is reclaimed by garbage collection.
    /// </summary>
    ~GraphSyncProviderBase() {
      DeWireupGraphProviderEventHandlers();
    }
    #endregion

    #region Method Overrides - Implementation of ProviderBase ('Initialize')
    /// <summary>
    /// Initializes the provider.
    /// </summary>
    /// <param name="name">The friendly name of the provider.</param>
    /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
    /// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
    /// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"></see> on a provider after the provider has already been initialized.</exception>
    /// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
      base.Initialize(name, config);
    }
    #endregion


    #region Public Abstract Methods - GraphVertex
    /// <summary>
    /// Request the creation of a GraphVertex on the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="M:GraphSyncProviderBase.GraphManager_VertexCreated"/> 
    /// event handler for the GraphManager's <see cref="E:GraphManager.VertexCreated"/> event. 
    /// </para>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="vertex">The vertex.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    public abstract void RemoteCreateVertex(GraphVertex vertex);
    /// <summary>
    /// Request the Update of a GraphVertex from the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="GraphSyncProviderBase.GraphManager_VertexUpdated"/>
    /// event handler for the GraphManager's <see cref="E:GraphManager.VertexUpdated"/> 
    /// event.
    /// </para>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="vertex">The vertex.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    public abstract void RemoteUpdateVertex(GraphVertex vertex);

    /// <summary>
    /// Request the Deletion of a GraphVertex from the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="GraphSyncProviderBase.GraphManager_VertexDeleted"/>
    /// event handler for the GraphManager's <see cref="E:GraphManager.VertexDeleted"/> 
    /// event.
    /// </para>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="vertex">The vertex.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    public abstract void RemoteDeleteVertex(GraphVertex vertex);
    #endregion

    #region Public Abstract Methods - GraphEdge
    /// <summary>
    /// Request the creation of an GraphEdge on the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="edge">The edge.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    public abstract void RemoteCreateEdge(GraphEdge edge);

    /// <summary>
    /// Request the update of an GraphEdge on the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="edge">The edge.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    public abstract void RemoteUpdateEdge(GraphEdge edge);

    /// <summary>
    /// Request the removal of an GraphEdge on the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="edge">The edge.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    public abstract void RemoteDeleteEdge(GraphEdge edge);
    #endregion

    #region Public Abstract Methods - DataObject
    /// <summary>
    /// Request the creation of a DataObject on the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="dataObjectFactory">The dataObject Source.</param>
    /// <param name="dataObject">The DataObject.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if dataObject the argument is null.</exception>
    public abstract void RemoteCreateDataObject(IDataObjectFactory dataObjectFactory, object dataObject);
    /// <summary>
    /// Request the updating of a DataObject on the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="dataObjectFactory">The DataObject Source.</param>
    /// <param name="dataObject">The DataObject.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    public abstract void RemoteUpdateDataObject(IDataObjectFactory dataObjectFactory, object dataObject);
    /// <summary>
    /// Request the removal of a DataObject from the Remote system.
    /// </summary>
    /// <remarks>
    /// <para>
    /// As it is an asynchronous system, returns no value at this time.
    /// </para>
    /// </remarks>
    /// <param name="dataObjectFactory">The DataObject Source.</param>
    /// <param name="dataObject">The DataObject.</param>
    /// <exception cref="System.ArgumentNullException">An Exception is raised if the argument is null.</exception>
    public abstract void RemoteDeleteDataObject(IDataObjectFactory dataObjectFactory, object dataObject);
    #endregion

    #region Public Abstract Methods - SyncPull
    /// <summary>
    /// Request a Pulled Synchronization of
    /// all New Vertices (vertices created on server since lastSyncDate),
    /// all Updated Vertices (vertices changed on server since lastSyncDate),
    /// all Deleted Vertices (vertices deleted on server since lastSyncDate),
    /// as well as all pertinent Attributes, DataObject and GraphEdge information.
    /// </summary>
    public abstract void RequestSyncPull();
    #endregion


    #region Public Methods
    /// <summary>
    /// Initialize the Provider.
    /// </summary>
    public void Initialize() {
      WireupGraphProviderEvents();
      WireupGraphProviderEventHandlers();
      WireupDataObjectFactoryManagerEvents();
    }
    #endregion


    #region *Public* Raise Events - GraphVertex events
    // --------------------------------------------------
    //IMPORTANT:
    //These methods are public, rather than protected
    //so that the methods within CallBack classes can fire them, 'remotely'.
    //Ie, these are really 'Fire' Methods.... 
    // --------------------------------------------------



    // ----------
    // --- CREATE
    // ----------

    /// <summary>
    /// Raises the <see cref="E:RemoteVertexCreateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/>
    /// method.
    /// </para>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method
    /// would have raised <see cref="RemoteVertexCreateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexCreateAccepted(VertexEventArgs e) {
      if (RemoteVertexCreateAccepted != null) {
        RemoteVertexCreateAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteVertexCreateCancelled"/> event.
    /// This event is raised when the Remote system has declined to create the GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/>
    /// method.
    /// </para>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method
    /// would have raised <see cref="RemoteVertexCreateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexCreateCancelled(VertexEventArgs e) {
      if (RemoteVertexCreateCancelled != null) {
        RemoteVertexCreateCancelled(this, e);
      }
    }
    /// <summary>
    /// Raises the <see cref="E:RemoteVertexCreateError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method)
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexCreateError(VertexErrorEventArgs e) {
      if (RemoteVertexCreateError != null) {
        RemoteVertexCreateError(this, e);
      }
    }

    // ----------
    // --- UPDATE
    // ----------


    /// <summary>
    /// Raises the <see cref="E:RemoteVertexUpdateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method
    /// would have raised <see cref="RemoteVertexUpdateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexUpdateAccepted(VertexEventArgs e) {
      if (RemoteVertexUpdateAccepted != null) {
        RemoteVertexUpdateAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteVertexUpdateCancelled"/> event.
    /// This event is raised when the Remote system has declined to update the GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method
    /// would have raised <see cref="RemoteVertexUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexUpdateCancelled(VertexEventArgs e) {
      if (RemoteVertexUpdateCancelled != null) {
        RemoteVertexUpdateCancelled(this, e);
      }
    }
    /// <summary>
    /// Raises the <see cref="E:RemoteVertexUpdateError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method)
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexUpdateError(VertexErrorEventArgs e) {
      if (RemoteVertexUpdateError != null) {
        RemoteVertexUpdateError(this, e);
      }
    }





    // ----------
    // --- DELETE
    // ----------

    /// <summary>
    /// Raises the <see cref="E:RemoteVertexDeleteAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method
    /// would have raised <see cref="RemoteVertexDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexDeleteAccepted(VertexEventArgs e) {
      if (RemoteVertexDeleteAccepted != null) {
        RemoteVertexDeleteAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteVertexDeleteCancelled"/> event.
    /// This event is raised when the Remote system has declined to delete the GraphVertex.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method
    /// would have raised <see cref="RemoteVertexDeleteAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexDeleteCancelled(VertexEventArgs e) {
      if (RemoteVertexDeleteCancelled != null) {
        RemoteVertexDeleteCancelled(this, e);
      }
    }
    /// <summary>
    /// Raises the <see cref="E:RemoteVertexDeleteError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushVertexCallBacks"/>'s 
    /// <see cref="SyncPushVertexCallBacks.vertexCallback"/> method)
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.VertexErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteVertexDeleteError(VertexErrorEventArgs e) {
      if (RemoteVertexDeleteError != null) {
        RemoteVertexDeleteError(this, e);
      }
    }

#endregion

    #region *Public* Raise Events - GraphEdge
    // --------------------------------------------------
    //IMPORTANT:
    //These methods are public, rather than protected
    //so that local CallBack methods can fire them remotely.
    // --------------------------------------------------



    // ----------
    // --- CREATE
    // ----------

    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeCreateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method
    /// would have raised <see cref="RemoteEdgeCreateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeCreateAccepted(EdgeEventArgs e) {
      if (RemoteEdgeCreateAccepted != null) {
        RemoteEdgeCreateAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeCreateCancelled"/> event.
    /// This event is raised when the Remote system has declined to create the GraphEdge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method
    /// would have raised <see cref="RemoteEdgeCreateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeCreateCancelled(EdgeEventArgs e) {
      if (RemoteEdgeCreateCancelled != null) {
        RemoteEdgeCreateCancelled(this, e);
      }
    }
    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeCreateError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method)
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeCreateError(EdgeErrorEventArgs e) {
      if (RemoteEdgeCreateError != null) {
        RemoteEdgeCreateError(this, e);
      }
    }
    // ----------
    // --- UPDATE
    // ----------

    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeUpdateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method
    /// would have raised <see cref="RemoteEdgeUpdateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeUpdateAccepted(EdgeEventArgs e) {
      if (RemoteEdgeUpdateAccepted != null) {
        RemoteEdgeUpdateAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeUpdateCancelled"/> event.
    /// This event is raised when the Remote system has declined to delete the GraphEdge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method
    /// would have raised <see cref="RemoteEdgeUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeUpdateCancelled(EdgeEventArgs e) {
      if (RemoteEdgeUpdateCancelled != null) {
        RemoteEdgeUpdateCancelled(this, e);
      }
    }
    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeUpdateError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method)
    /// would have raised <see cref="RemoteEdgeUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeUpdateError(EdgeErrorEventArgs e) {
      if (RemoteEdgeUpdateError != null) {
        RemoteEdgeUpdateError(this, e);
      }
    }

    // ----------
    // --- DELETE
    // ----------

    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeDeleteAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method
    /// would have raised <see cref="RemoteEdgeDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeDeleteAccepted(EdgeEventArgs e) {
      if (RemoteEdgeDeleteAccepted != null) {
        RemoteEdgeDeleteAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeDeleteCancelled"/> event.
    /// This event is raised when the Remote system has declined to delete the GraphEdge.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method
    /// would have raised <see cref="RemoteEdgeDeleteAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeDeleteCancelled(EdgeEventArgs e) {
      if (RemoteEdgeDeleteCancelled != null) {
        RemoteEdgeDeleteCancelled(this, e);
      }
    }
    /// <summary>
    /// Raises the <see cref="E:RemoteEdgeDeleteError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushEdgeCallBacks"/>'s 
    /// <see cref="SyncPushEdgeCallBacks.edgeCallback"/> method)
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="T:XAct.Graphs.EdgeErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteEdgeDeleteError(EdgeErrorEventArgs e) {
      if (RemoteEdgeDeleteError != null) {
        RemoteEdgeDeleteError(this, e);
      }
    }
    #endregion

    #region *Public* Raise Events - DataObjects
    // --------------------------------------------------
    //IMPORTANT:
    //These methods are public, rather than protected
    //so that local CallBack methods can fire them remotely.
    // --------------------------------------------------



    // ----------
    // --- CREATE
    // ----------

    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectCreateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method
    /// would have raised <see cref="RemoteDataObjectCreateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectCreateAccepted(DataObjectEventArgs e) {
      if (RemoteDataObjectCreateAccepted != null) {
        RemoteDataObjectCreateAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectCreateCancelled"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method
    /// would have raised <see cref="RemoteDataObjectCreateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectCreateCancelled(DataObjectEventArgs e) {
      if (RemoteDataObjectCreateCancelled != null) {
        RemoteDataObjectCreateCancelled(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectCreateError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method)
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectCreateError(DataObjectErrorEventArgs e) {
      if (RemoteDataObjectCreateError != null) {
        RemoteDataObjectCreateError(this, e);
      }
    }

    // ----------
    // --- UPDATE
    // ----------
    
    
    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectUpdateAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method
    /// would have raised <see cref="RemoteDataObjectUpdateCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectUpdateAccepted(DataObjectEventArgs e) {
      if (RemoteDataObjectUpdateAccepted != null) {
        RemoteDataObjectUpdateAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectUpdateCancelled"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectUpdateCancelled(DataObjectEventArgs e) {
      if (RemoteDataObjectUpdateCancelled != null) {
        RemoteDataObjectUpdateCancelled(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectUpdateError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method)
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectUpdateError(DataObjectErrorEventArgs e) {
      if (RemoteDataObjectUpdateError != null) {
        RemoteDataObjectUpdateError(this, e);
      }
    }

    // ----------
    // --- DELETE
    // ----------

    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectDeleteAccepted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had rejected the request, 
    /// <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method
    /// would have raised <see cref="RemoteDataObjectDeleteCancelled"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectDeleteAccepted(DataObjectEventArgs e) {
      if (RemoteDataObjectDeleteAccepted != null) {
        RemoteDataObjectDeleteAccepted(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectDeleteCancelled"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method
    /// would have raised <see cref="RemoteDataObjectDeleteAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectDeleteCancelled(DataObjectEventArgs e) {
      if (RemoteDataObjectDeleteCancelled != null) {
        RemoteDataObjectDeleteCancelled(this, e);
      }
    }

    /// <summary>
    /// Raises the <see cref="E:RemoteDataObjectDeleteError"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that if the server had accepted the request, 
    /// the Disconnected Service Request Manager 
    /// (not <see cref="SyncPushDataObjectCallBacks"/>'s 
    /// <see cref="SyncPushDataObjectCallBacks.dataObjectCallback"/> method)
    /// would have raised <see cref="RemoteDataObjectUpdateAccepted"/>.
    /// </para>
    /// </remarks>
    /// <param name="e">The <see cref="DataObjectErrorEventArgs"/> instance containing the event data.</param>
    virtual public void OnRemoteDataObjectDeleteError(DataObjectErrorEventArgs e) {
      if (RemoteDataObjectDeleteError != null) {
        RemoteDataObjectDeleteError(this, e);
      }
    }
    #endregion


    #region Protected - Event Handlers - GraphManager - GraphVertex events
    /// <summary>
    /// Event Handler for the GraphManager's <see cref="E:GraphProviderBase.VertexCreated"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.VertexCreated"/>
    /// when a new GraphVertex is created. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to create a duplicate of the GraphVertex on
    /// the Server.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the GraphVertex in question.</param>
    virtual protected void GraphManager_VertexCreated(object sender, VertexEventArgs e) {
      RemoteCreateVertex(e.Vertex);
    }

    /// <summary>
    /// Event Handler for the GraphManager's <see cref="E:GraphProviderBase.VertexUpdated"/>  event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.VertexUpdated"/>
    /// when an existing GraphVertex is updated. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to update the duplicate GraphVertex that already
    /// exists on the server.
    /// </para>
    /// <para>
    /// Note that due to the flag system, if the vertex does not yet exist
    /// on the server, the process will be overriden by a call to 
    /// <see cref="GraphManager_VertexCreated"/>.
    /// </para>
    /// <para>
    /// On the other hand, if the vertex was deleted Client-side,
    /// both <see cref="GraphManager_VertexUpdated"/> and <see cref="GraphManager_VertexCreated"/>
    /// will be overriden by a call to <see cref="GraphManager_VertexDeleted"/>.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the GraphVertex in question.</param>
    virtual protected void GraphManager_VertexUpdated(object sender, VertexEventArgs e) {
      RemoteUpdateVertex(e.Vertex);
    }



    /// <summary>
    /// Event Handler for the GraphManager's <see cref="GraphProviderBase.VertexDeleted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.VertexDeleted"/>
    /// when an existing GraphVertex is deleted. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to delete the duplicate GraphVertex 
    /// that exists on the server.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the GraphVertex in question.</param>
    virtual protected void GraphManager_VertexDeleted(object sender, VertexEventArgs e) {
      RemoteDeleteVertex(e.Vertex);
    }
    #endregion

    #region Protected - Event Handlers - GraphManager - GraphEdge events
    /// <summary>
    /// Event Handler for the GraphManager's <see cref="E:GraphProviderBase.EdgeCreated"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.EdgeCreated"/>
    /// when a new GraphEdge is created. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to create a duplicate of the GraphEdge on
    /// the Server.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the GraphEdge in question.</param>
    virtual protected void GraphManager_EdgeCreated(object sender, EdgeEventArgs e) {
      RemoteCreateEdge(e.Edge);
    }

    /// <summary>
    /// Event Handler for the GraphManager's <see cref="E:GraphProviderBase.EdgeUpdated"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.EdgeUpdated"/>
    /// when an existing GraphEdge is updated. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to update the duplicate GraphEdge that already
    /// exists on the server.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the GraphEdge in question.</param>
    virtual protected void GraphManager_EdgeUpdated(object sender, EdgeEventArgs e) {
      RemoteUpdateEdge(e.Edge);
    }

    /// <summary>
    /// Event Handler for the GraphManager's <see cref="E:GraphProviderBase.EdgeDeleted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.VertexEdge"/>
    /// when an existing GraphEdge is deleted. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to delete the duplicate GraphEdge 
    /// that exists on the server.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the GraphEdge in question.</param>
    virtual protected void GraphManager_EdgeDeleted(object sender, EdgeEventArgs e) {
      RemoteDeleteEdge(e.Edge);
    }
    #endregion

    #region Protected - Event Handlers - GraphManager - DataObject events
    /// <summary>
    /// Event Handler for the GraphManager's <see cref="E:GraphProviderBase.DataObjectCreated"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.DataObjectCreated"/>
    /// when a new DataObject is created. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to create a duplicate of the DataObject on
    /// the Server.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the DataObject in question.</param>
    void DataObjectFactoryManager_DataObjectCreated(object sender, XAct.Data.DataObjectEventArgs e) {

      RemoteCreateDataObject(e.DataObjectFactory, e.DataObject);
    }

    /// <summary>
    /// Event Handler for the GraphManager's <see cref="E:GraphProviderBase.DataObjectUpdated"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.DataObjectUpdated"/>
    /// when an existing DataObject is updated. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to update the duplicate DataObject that already
    /// exists on the server.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the DataObject in question.</param>
    void DataObjectFactoryManager_DataObjectUpdated(object sender, XAct.Data.DataObjectEventArgs e) {
      RemoteUpdateDataObject(e.DataObjectFactory, e.DataObject);
    }

    /// <summary>
    /// Event Handler for the GraphManager's <see cref="E:GraphProviderBase.DataObjectDeleted"/> event.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The GraphManager raises the <see cref="E:GraphProviderBase.VertexDataObject"/>
    /// when an existing DataObject is deleted. 
    /// </para>
    /// <para>
    /// Therefore, on a Client, the SyncManager listens for this event
    /// in order to qeueue up a Request to delete the duplicate DataObject 
    /// that exists on the server.
    /// </para>
    /// </remarks>
    /// <param name="sender">The Provider</param>
    /// <param name="e">The Event Args package that contains a reference to the DataObject in question.</param>
    void DataObjectFactoryManager_DataObjectDeleted(object sender, XAct.Data.DataObjectEventArgs e) {
      RemoteDeleteDataObject(e.DataObjectFactory, e.DataObject);
    }
    #endregion

    #region Protected Methods - Initialization - GraphManager

    /// <summary>
    /// Attaches event handlers to the GraphManager's events.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="Initialize()"/>.
    /// </para>
    /// </remarks>
    virtual protected void WireupGraphProviderEvents() {
      GraphProvider.VertexCreated += new EventHandler<VertexEventArgs>(GraphManager_VertexCreated);
      GraphProvider.VertexUpdated += new EventHandler<VertexEventArgs>(GraphManager_VertexUpdated);
      GraphProvider.VertexDeleted += new EventHandler<VertexEventArgs>(GraphManager_VertexDeleted);
      GraphProvider.EdgeCreated += new EventHandler<EdgeEventArgs>(GraphManager_EdgeCreated);
      GraphProvider.EdgeUpdated += new EventHandler<EdgeEventArgs>(GraphManager_EdgeUpdated);
      GraphProvider.EdgeDeleted += new EventHandler<EdgeEventArgs>(GraphManager_EdgeDeleted);
    }

    /// <summary>
    /// Attaches event handlers to the DataObjectFactoryManager's events.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="Initialize()"/>.
    /// </para>
    /// </remarks>
    virtual protected void WireupDataObjectFactoryManagerEvents() {
      DataObjectFactoryManager.DataObjectCreated += new EventHandler<XAct.Data.DataObjectEventArgs>(DataObjectFactoryManager_DataObjectCreated);
      DataObjectFactoryManager.DataObjectUpdated += new EventHandler<XAct.Data.DataObjectEventArgs>(DataObjectFactoryManager_DataObjectUpdated);
      DataObjectFactoryManager.DataObjectDeleted += new EventHandler<XAct.Data.DataObjectEventArgs>(DataObjectFactoryManager_DataObjectDeleted);
    }

    /// <summary>
    /// Attaches the GraphManager's public event handlers to this provider's events.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="Initialize()"/>.
    /// </para>
    /// </remarks>
    /// <internal>
    /// This was unusual: usually event handlers are protected -- 
    /// but in the case of the GraphManager, they were made public
    /// so that this method, exterior to the GraphManager, could attach event handlers to it
    /// as it needed to.
    /// </internal>
    virtual protected void WireupGraphProviderEventHandlers() {
      //Attach the GraphManager's *public* eventhandlers:
      RemoteVertexCreateAccepted += GraphProvider.OnRemoteVertexCreateAccepted;
      RemoteVertexUpdateAccepted += GraphProvider.OnRemoteVertexUpdateAccepted;
      RemoteVertexDeleteAccepted += GraphProvider.OnRemoteVertexDeleteAccepted;

      RemoteEdgeCreateAccepted += GraphProvider.OnRemoteEdgeCreateAccepted;
      RemoteEdgeUpdateAccepted += GraphProvider.OnRemoteEdgeUpdateAccepted;
      RemoteEdgeDeleteAccepted += GraphProvider.OnRemoteEdgeDeleteAccepted;
    }


    /// <summary>
    /// Detaches GraphManager's public event handlers from this provider's events.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by the destructor.
    /// </para>
    /// </remarks>
    virtual protected void DeWireupGraphProviderEventHandlers() {
      RemoteVertexCreateAccepted -= GraphProvider.OnRemoteVertexCreateAccepted;
      RemoteVertexUpdateAccepted -= GraphProvider.OnRemoteVertexUpdateAccepted;
      RemoteVertexDeleteAccepted -= GraphProvider.OnRemoteVertexDeleteAccepted;

      RemoteEdgeCreateAccepted -= GraphProvider.OnRemoteEdgeCreateAccepted;
      RemoteEdgeUpdateAccepted -= GraphProvider.OnRemoteEdgeUpdateAccepted;
      RemoteEdgeDeleteAccepted -= GraphProvider.OnRemoteEdgeDeleteAccepted;
    }
    #endregion

  }//Class:End
}//Namespace:End
