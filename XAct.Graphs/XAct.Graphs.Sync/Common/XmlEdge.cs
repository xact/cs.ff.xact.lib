using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using XAct.Graphs;


namespace XAct.Graphs {

  /// <summary>
  /// Xml serializable class which represents an GraphEdge.
  /// Used to serialize an GraphEdge in order to transmit is as a Request argument.
  /// </summary>
  [XmlRoot("edge")]
  public class XmlEdge {

    /// <summary>
    /// Gets or sets the source vertex id.
    /// </summary>
    /// <value>The source id.</value>
    [XmlAttribute("sourceId")]
    public Guid SourceId {
      get {
        return _SourceId;
      }
      set {
        _SourceId = value;
      }
    }
    private Guid _SourceId;

    /// <summary>
    /// Gets or sets the target vertex id.
    /// </summary>
    /// <value>The target id.</value>
    [XmlAttribute("targetId")]
    public Guid TargetId {
      get {
        return _TargetId;
      }
      set {
        _TargetId = value;
      }
    }
    private Guid _TargetId;

    /// <summary>
    /// Gets or sets the weight of the GraphEdge.
    /// </summary>
    /// <value>The weight.</value>
    [XmlAttribute("weight")]
    public int Weight {
      get {
        return _Weight;
      }
      set {
        _Weight = value;
      }
    }
    private int _Weight;


    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="T:XmlEdge"/> class.
    /// </summary>
    public XmlEdge() {
      // A parameterless constructor is needed in order to be serializeable.
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:XmlEdge"/> class.
    /// </summary>
    /// <param name="edge">The edge.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the edge argument is null.</exception>
    public XmlEdge(GraphEdge edge) {
      if (edge == null) {
        throw new System.ArgumentNullException("edge");
      }
      _SourceId = edge.SourceId;
      _TargetId = edge.TargetId;
      _Weight = edge.Weight;
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Constructs an GraphEdge instance from the serialized information in this class.
    /// </summary>
    /// <returns>A new GraphEdge instance.</returns>
    public GraphEdge Deserialize() {
      return new GraphEdge(_SourceId, _TargetId);
    }
    #endregion
  }//Class:End
}//Namespace:End
