/*
using System;
using System.Web.Services.Protocols;

namespace XAct.Graphs.Server {
  public class AuthRequiredSoapExtension : SoapExtension {

    /// <summary>
    /// When overridden in a derived class, 
    /// allows a SOAP extension to receive a <see cref="T:System.Web.Services.Protocols.SoapMessage"></see> to process at each <see cref="T:System.Web.Services.Protocols.SoapMessageStage"></see>.
    /// </summary>
    /// <param name="message">The <see cref="T:System.Web.Services.Protocols.SoapMessage"></see> to process.</param>
    override public void ProcessMessage(SoapMessage message) {

      switch (message.Stage) {
        case SoapMessageStage.AfterDeserialize:
          if (!authenticate(message)) {
            throw new SoapException("unhautoroized", SoapException.ClientFaultCode);
          }
          break;
        case SoapMessageStage.BeforeDeserialize:
        case SoapMessageStage.AfterSerialize:
        case SoapMessageStage.BeforeSerialize:
          break;

      }
    }

    protected bool authenticate(SoapMessage message) {
      foreach (SoapHeader header in message.Headers) {
        if (header is CredentialSoapHeader) {
          CredentialSoapHeader credentials = (CredentialSoapHeader)header;
          if (credentials.UserName == "olive" && credentials.Password == "olive") {
            return true;
          }
        }
      }
      return false;
    }

    /// <summary>
    /// When overridden in a derived class, allows a SOAP extension to initialize data specific to a class implementing an XML Web service at a one time performance cost.
    /// </summary>
    /// <param name="serviceType">The type of the class implementing the XML Web service to which the SOAP extension is applied.</param>
    /// <returns>
    /// The <see cref="T:System.Object"></see> that the SOAP extension initializes for caching.
    /// </returns>
    override public object GetInitializer(Type serviceType) {
      return null;
    }
    /// <summary>
    /// When overridden in a derived class, allows a SOAP extension to initialize data specific to an XML Web service method using an attribute applied to the XML Web service method at a one time performance cost.
    /// </summary>
    /// <param name="methodInfo">A <see cref="T:System.Web.Services.Protocols.LogicalMethodInfo"></see> representing the specific function prototype for the XML Web service method to which the SOAP extension is applied.</param>
    /// <param name="attribute">The <see cref="T:System.Web.Services.Protocols.SoapExtensionAttribute"></see> applied to the XML Web service method.</param>
    /// <returns>
    /// The <see cref="T:System.Object"></see> that the SOAP extension initializes for caching.
    /// </returns>
    override public object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute) {
      return null;
    }
    /// <summary>
    /// When overridden in a derived class, allows a SOAP extension to initialize itself using the data cached in the <see cref="M:System.Web.Services.Protocols.SoapExtension.GetInitializer(System.Web.Services.Protocols.LogicalMethodInfo,System.Web.Services.Protocols.SoapExtensionAttribute)"></see> method.
    /// </summary>
    /// <param name="initializer">The <see cref="T:System.Object"></see> returned from <see cref="M:System.Web.Services.Protocols.SoapExtension.GetInitializer(System.Web.Services.Protocols.LogicalMethodInfo,System.Web.Services.Protocols.SoapExtensionAttribute)"></see> cached by ASP.NET.</param>
    override public void Initialize(object initializer) {
    }

  }//Class:End
}//Namespace:End

*/