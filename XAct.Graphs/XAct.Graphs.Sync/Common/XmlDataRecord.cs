using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;

using XAct.Data;
using XAct.Helpers;

namespace XAct.Graphs {

  /// <summary>
  /// Xml serializable class which represents a data object
  /// and its data object source.
  /// Used to serialize a DataObject in order to transmit is as a Request argument.
  /// </summary>
  [XmlRoot("dataRecord")]
  public class XmlDataObject {


    #region Properties
    /// <summary>
    /// Gets or sets the Id of the DataObject's DataSource's Id.
    /// </summary>
    [XmlAttribute("dataObjectFactoryId")]
    public int DataObjectFactoryId {
      get {
        return _DataObjectFactoryId;
      }
      set {
        _DataObjectFactoryId = value;
      }
    }
    private int _DataObjectFactoryId;

    /*
    /// <summary>
    /// Gets or sets the DataObject's Id.
    /// Note that it is a string representation of the record's int/guid/multi-key's unique key.
    /// </summary>
    /// <value>The data object's id.</value>
    [XmlAttribute("dataObjectId")]
    public string DataObjectId {
      get {
       IDataObjectFactory dataObjectFactory = DataObjectFactoryManager.Instance[dataObjectFactoryId];
      _DataObjectFactoryId = dataObjectFactoryId;

     * return _DataObjectId;
      }
    }
    private string _DataObjectId;
    */

    /// <summary>
    /// Gets or sets the serialized DataObject.
    /// </summary>
    [XmlElement("contents")]
    public XmlNode Contents {
      get {
        return _xmlContents;
      }
      set {
        _xmlContents = value;
      }
    }
    private XmlNode _xmlContents;
    #endregion


    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="T:XmlDataObject"/> class.
    /// </summary>
    public XmlDataObject() {
      // A parameterless constructor is needed in order to be serializeable.
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="T:XmlDataObject"/> class.
    /// </summary>
    /// <param name="dataObjectFactoryId">The Id of the DataObject's Source.</param>
    /// <param name="dataObject">The dataObject.</param>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the dataObject argument is null.</exception>
    public XmlDataObject(int dataObjectFactoryId, object dataObject) {
      if (dataObject == null) {
        throw new System.ArgumentNullException("dataObject");
      }
      
      //_DataObjectId = dataObject.Id;
      _xmlContents = XmlSerializationHelper.ToXmlNode(dataObject);
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Deserializes this XmlDataObject into the out parameters data source
    /// and data object.
    /// </summary>
    /// <param name="dataObjectFactory">The data source.</param>
    /// <param name="dataObject">The data object.</param>
    public void Deserialize(out IDataObjectFactory dataObjectFactory, out object dataObject) {
      dataObjectFactory = DataObjectFactoryManager.Get(_DataObjectFactoryId);
      dataObject = dataObjectFactory.Load(_xmlContents);
    }
    #endregion
  }//Class:End
}//Namespace:End
