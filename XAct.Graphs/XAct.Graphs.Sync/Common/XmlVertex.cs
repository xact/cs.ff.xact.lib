using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using XAct.Graphs;


namespace XAct.Graphs {

  /// <summary>
  /// Xml serializable class which represents a GraphVertex.
  /// Used to serialize a GraphVertex in order to transmit is as a Request argument.
  /// </summary>
  [XmlRoot("vertex")]
  public class XmlVertex {

    #region Properties
    /// <summary>
    /// Gets or sets the vertex id.
    /// </summary>
    /// <value>The id.</value>
    [XmlAttribute("id")]
    public Guid Id {
      get {
        return _Id;
      }
      set {
        _Id = value;
      }
    }
    private Guid _Id;

    /// <summary>
    /// Gets or sets the vertex weight.
    /// </summary>
    /// <value>The vertex weight.</value>
    [XmlAttribute("weight")]
    public int Weight {
      get {
        return _Weight;
      }
      set {
        _Weight = value;
      }
    }
    private int _Weight;

    /// <summary>
    /// Gets or sets the DataObject's source id.
    /// </summary>
    /// <value>The dataobject source id.</value>
    [XmlAttribute("dataObjectFactoryId")]
    public int DataObjectFactoryId {
      get {
        return _DataObjectFactoryId;
      }
      set {
        _DataObjectFactoryId = value;
      }
    }
    private int _DataObjectFactoryId;

    /// <summary>
    /// Gets or sets the data object id.
    /// Note that it is a string representation of the record's int/guid/multi-key's unique key.
    /// </summary>
    /// <value>The data object id.</value>
    [XmlAttribute("dataObjectId")]
    public string DataObjectId {
      get {
        return _DataObjectId;
      }
      set {
        _DataObjectId = value;
      }
    }
    private string _DataObjectId;

    /// <summary>
    /// Gets or sets the date created.
    /// </summary>
    /// <value>The date created.</value>
    [XmlAttribute("dateCreated")]
    public DateTime UTCDateCreated {
      get {
        return _UTCDateCreated;
      }
      set {
        _UTCDateCreated = value.ToUniversalTime();
      }
    }
    private DateTime _UTCDateCreated;

    /// <summary>
    /// Gets or sets the date edited.
    /// </summary>
    /// <value>The date edited.</value>
    [XmlAttribute("dateEdited")]
    public DateTime UTCDateEdited {
      get {
        return _UTCDateEdited;
      }
      set {
        _UTCDateEdited = value;
      }
    }
    private DateTime _UTCDateEdited;


    /// <summary>
    /// Gets or sets the out edges of this GraphVertex.
    /// </summary>
    /// <value>The out edges.</value>
    [XmlArray("outEdges"), XmlArrayItem("edge", typeof(Guid))]
    List<Guid> OutEdges {
      get {
        return _OutEdges;
      }
      set {
        _OutEdges = value;
      }
    }
    private List<Guid> _OutEdges = new List<Guid>();
    #endregion


    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="T:XmlVertex"/> class.
    /// </summary>
    public XmlVertex() {
      // A parameterless constructor is needed in order to be serializeable.
    }


    /// <summary>
    /// Creates a new XmlVertex from a vertex.
    /// </summary>
    /// <param name="vertex">The vertex.</param>
    /// <returns>A newly created XmlVertex.</returns>
    /// <exception cref="System.ArgumentNullException">An exception is raised if the vertex argument is null.</exception>
    public XmlVertex(GraphVertex vertex) {
      if (vertex == null) {
        throw new System.ArgumentNullException("vertex");
      }

      _DataObjectId = vertex.DataRecordId;
      _DataObjectFactoryId = vertex.DataSourceId;
      _UTCDateCreated = vertex.UTCDateCreated.ToUniversalTime();
      _UTCDateEdited = vertex.UTCDateEdited.ToUniversalTime();
      _Id = vertex.Id;

      /*
      IGraphEngine GraphProvider = GraphManager.Provider;
      
      foreach (GraphEdge e in GraphProvider.OutEdges(vertex)) {
        _OutEdges.Add(e.TargetId);
      }
      */
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Constructs a GraphVertex instance from the serialized information in this class.
    /// </summary>
    /// <returns>A new GraphVertex instance.</returns>
    public GraphVertex Deserialize() {
      return new GraphVertex(Id, Weight, DataObjectFactoryId, DataObjectId, UTCDateCreated, UTCDateEdited, SyncStatus.Clean);
    }
    #endregion

  }//Class:End
}//Namespace:End

