//Because 'WebMethod' is not supported on CE
//this part is not included...
#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Diagnostics;

using System.Data;


using XAct.Graphs;
using XAct.Web.Services.Authentication;

using System.Collections.Generic;
using System.Xml;

namespace XAct.Graphs {

  /// <summary>
  /// WebService Class of WebMethods that a GraphSyncProvider
  /// client methods invokes.
  /// </summary>
  [WebService(Namespace = "http://xact.com/GraphSync")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  public class SyncServices : System.Web.Services.WebService {




    #region Properties
/*
    public AuthenticationSoapHeader Credentials {
      get {
        return _Credentials;
      }
      set {
        _Credentials = value;
      }
    }
    AuthenticationSoapHeader _Credentials;
    */
    #endregion



    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="T:SyncServices"/> class.
    /// </summary>
    public SyncServices() {
    }

    #endregion


    #region Graph related methods - GraphVertex

    /// <summary>
    /// Duplicate a Client's GraphVertex on the server.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This method is called by the Client 
    /// in order to duplicate on the Server 
    /// a GraphVertex that already exists on the Client.
    /// </para>
    /// </remarks>
    /// <internal>
    /// IMPORTANT:
    /// A Save from Client to Server of new GraphVertex 
    /// must add it to Server�s VerticesSent table
    /// since it is already on Client...
    /// </internal>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="xmlVertex">The xml representation of the vertex.</param>
    /// <returns></returns>
    [WebMethod(Description="Create a new GraphVertex on the Server.")]
    public bool VertexCreate(Guid clientMachineId, XmlVertex xmlVertex) {
      //Check Args:
      if (xmlVertex == null) {
        throw new System.ArgumentNullException("xmlVertex");
      }
      //Extract GraphVertex:
      GraphVertex vertex = xmlVertex.Deserialize();
      //And the build it:
      //TODO:
      //GraphManager.CreateVertex(vertex);

      //Get access to Server-only methods of the GraphManager:
      IGraphSyncServer graphManagerPlus =  (IGraphSyncServer)GraphManager.Instance;
      //And save a ref to this vertex in the VerticesSent table:
      graphManagerPlus.SaveVertexIdInVerticesSent(clientMachineId, vertex.Id);


      throw new System.NotSupportedException();
    }

    /// <summary>
    /// Update an existing GraphVertex on the server.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This method is called by the Client 
    /// in order to update on the Server 
    /// a GraphVertex that already exists on both 
    /// the Client and Server, but has been 
    /// changed by the Client.
    /// </para>
    /// </remarks>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="xmlVertex">The xml representation of the vertex.</param>
    /// <returns></returns>
    [WebMethod(Description = "Update an existing GraphVertex on the Server.")]
    public bool VertexUpdate(Guid clientMachineId, XmlVertex xmlVertex) {
      //Check Args:
      if (xmlVertex == null) {
        throw new System.ArgumentNullException("xmlVertex");
      }
      //Extract GraphVertex from argument:
      GraphVertex vertex = xmlVertex.Deserialize();

      //Update it:
      return GraphManager.UpdateVertex(vertex);

    }

    /// <summary>
    /// Delete an existing GraphVertex on the server.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This method is called by the Client 
    /// in order to remove from the Server 
    /// a GraphVertex that has been Deleted
    /// from the Client.
    /// </para>
    /// </remarks>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="xmlVertex">The xml representation of the vertex.</param>
    /// <returns></returns>
    [WebMethod(Description = "Delete an existing GraphVertex from the Server.")]
    public bool VertexDelete(Guid clientMachineId, XmlVertex xmlVertex) {
      //Check Args:
      if (xmlVertex == null) {
        throw new System.ArgumentNullException("xmlVertex");
      }
      //Extract GraphVertex from argument:
      GraphVertex vertex = xmlVertex.Deserialize();

      //Update it:
      if (!GraphManager.DeleteVertex(vertex)) {
        return false;
      }

      //Get access to Server-only methods of the GraphManager:
      IGraphSyncServer graphManagerPlus = (IGraphSyncServer)GraphManager.Instance;


      //And remove ref to this vertex in the VerticesSent table(for this client only):
      graphManagerPlus.RemoveVertexIdInVerticesSent(clientMachineId, vertex.Id);

      return true;
    }

#endregion

    #region Graph related methods - GraphEdge

    /// <summary>
    /// Create a new GraphEdge on the server.
    /// </summary>
    /// <param name="xmlEdge">The xml representation of the edge.</param>
    /// <returns></returns>
    [WebMethod(Description = "Create a new GraphEdge on the Server.")]
    public bool EdgeCreate(XmlEdge xmlEdge) {
      if (xmlEdge == null) {
        throw new System.ArgumentNullException("xmlEdge");
      }
      return GraphManager.AddEdge(xmlEdge.SourceId,xmlEdge.TargetId);
    }

    /// <summary>
    /// Update an existing GraphEdge on the server.
    /// </summary>
    /// <param name="xmlEdge">The xml representation of the edge.</param>
    /// <returns></returns>
    [WebMethod(Description = "Update an existing GraphEdge on the Server.")]
    public bool EdgeUpdate(XmlEdge xmlEdge) {
      if (xmlEdge == null) {
        throw new System.ArgumentNullException("xmlEdge");
      }
      //Extract GraphEdge:
      GraphEdge edge = xmlEdge.Deserialize();
      //Update it:
      return GraphManager.UpdateEdge(xmlEdge.SourceId,xmlEdge.TargetId);
    }

    /// <summary>
    /// Delete an GraphEdge on the server.
    /// </summary>
    /// <param name="xmlEdge">The xml representation of the edge.</param>
    /// <returns></returns>
    [WebMethod(Description = "Delete an existing GraphEdge from the Server.")]
    public bool EdgeDelete(XmlEdge xmlEdge) {
      if (xmlEdge == null) {
        throw new System.ArgumentNullException("xmlEdge");
      }
      //Extract GraphEdge:
      GraphEdge edge = xmlEdge.Deserialize();
      //Delete it:
      return GraphManager.RemoveEdge(xmlEdge.SourceId, xmlEdge.TargetId);
    }

    #endregion

    #region Graph related methods - DataObject

    /// <summary>
    /// Create a new DataObject on the server.
    /// </summary>
    /// <internal>
    /// 
    /// </internal>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="xmlRecord">The xml representation of the DataObject.</param>
    /// <returns></returns>
    [WebMethod(Description = "Create a new DataObject record on the Server.", EnableSession = true)]
    public bool RecordCreate(Guid clientMachineId, XmlDataObject xmlRecord) {
      //    [SoapHeader("Credentials")]

      //Check Args:
      if (xmlRecord == null) {
        throw new System.ArgumentNullException("xmlRecord");
      }
      if (!User.Identity.IsAuthenticated) {
        throw new Exception("unauthenticated user");
      }

      //Extract DataObject:
      XAct.Data.IDataObjectFactory dataObjectFactory;
      object dataObject;
      xmlRecord.Deserialize(out dataObjectFactory, out dataObject);

      //Original/Olivier:
      return dataObjectFactory.StoreNewDataObject(dataObject);
    }


    /// <summary>
    /// Update an existing DataObject on the server.
    /// </summary>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="xmlRecord">The xml representation of the DataObject.</param>
    /// <returns></returns>
    [WebMethod(Description = "Delete an existing DataObject record on the Server.", EnableSession = true)]
    public bool RecordRecord(Guid clientMachineId, XmlDataObject xmlRecord) {
      //Check Args:
      if (xmlRecord == null) {
        throw new System.ArgumentNullException("xmlRecord");
      }
      //Extract DataObject:
      XAct.Data.IDataObjectFactory dataObjectFactory;
      object dataObject;
      xmlRecord.Deserialize(out dataObjectFactory, out dataObject);

      return dataObjectFactory.UpdateDataObject(dataObject);
    }

    /// <summary>
    /// Delete an existing DataObject on the server.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <internal>
    /// 
    /// </internal>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="xmlRecord">The xml representation of the DataObject.</param>
    /// <returns></returns>
    [WebMethod(Description = "Delete an existing DataObject record from the Server.", EnableSession = true)]
    public bool RecordDelete(Guid clientMachineId, XmlDataObject xmlRecord) {
      //Check Args:
      if (xmlRecord == null) {
        throw new System.ArgumentNullException("xmlRecord");
      }
      //Extract DataObject:
      XAct.Data.IDataObjectFactory dataObjectFactory;
      object dataObject;
      xmlRecord.Deserialize(out dataObjectFactory, out dataObject);

      return dataObjectFactory.Delete(dataObject);
    }

    #endregion


    #region Graph - Sync Method
    /// <summary>
    /// Pulls the sync.
    /// </summary>
    /// <param name="clientMachineId">The unique client machine Id.</param>
    /// <param name="lastSyncDate">The last sync date.</param>
    /// <returns></returns>
    [WebMethod(Description = "Initiate a full Pull Sync of all modifications on server.", EnableSession = true)]
    public XmlDocument PullSync(Guid clientMachineId, DateTime lastSyncDate) {

      IGraphSyncServer graphManagerPlus =  (IGraphSyncServer)GraphManager.Instance;

      ICollection<int> subscribedDataSources = null;

      Guid[] vertexIdsNew = graphManagerPlus.GetNewVertexIds(subscribedDataSources,clientMachineId,lastSyncDate);
      Guid[] vertexIdsUpdated = graphManagerPlus.GetUpdatedVertexIds(clientMachineId, lastSyncDate);
      Guid[] vertexIdsDeleted = graphManagerPlus.GetDeletedVertexIds(clientMachineId);

      //The goal here is to collect together all 
      //NEW records (records that have CreateDate>LastSyncDate and are not in VerticesSent table),
      //UPDATED records (records that are in VerticesSent table, and have ModifiedDate>LastSyncDate),
      //DELETED vertex ids (ids that are in VerticesSent, but that don't have any matching Vertices).
      //MOVED vertices
      //      //XAct.Data.DbDataObjectFactory dataObjectFactory =  XAct.Data.DataObjectFactoryManager.Instance[dataObjectFactoryId];

      throw new System.NotImplementedException();
    }

    #endregion

    /*
    #region Testing method

    [WebMethod(EnableSession = true)]
    public bool Authenticate(string applicationName, string userName, string password) {
      return AuthenticationSoapExtension.Validate(applicationName, userName, password, true);
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("Credentials")]
    public Guid GetUserId() {
      if (!User.Identity.IsAuthenticated) {
        throw new Exception("unauthenticated user");
      }
      iSightPrincipal p = (iSightPrincipal)User;
      Trace.TraceInformation("getuserId: {0}:{1}, {2}", p.Identity.Name, p.Domain, p.UserId);
      return p.UserId;
    }

    [WebMethod(Description = "for test stuffs ...")]
    public void Test() {
      //System.Security.Principal.IPrincipal principal = base.User;
    }

    [WebMethod(Description = "method for testing the connection")]
    public bool Ping() {
      return true;
    }

    #endregion
    */

    #region Testing

    /// <summary>
    /// method for testing the connection in an unsecured way.
    /// </summary>
    /// <returns></returns>
    [WebMethod(Description = "method for testing the connection in an unsecured way.")]
    public bool Test() {
      return true;
    }


    /// <summary>
    /// method for testing the connection in a secured way.
    /// </summary>
    /// <returns></returns>
    [WebMethod(Description = "method for testing the connection in a secured way.")]
    [XAct.Web.Services.Authentication.AuthenticationSoapExtension]
    public bool TestSecure() {
      return true;
    }
    #endregion
  }//Class:End
}//Namespace:End

#endif