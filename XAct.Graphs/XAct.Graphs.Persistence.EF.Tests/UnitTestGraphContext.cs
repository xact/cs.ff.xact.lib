﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using XAct.Graphs;

namespace XAct.Graphs.Tests
{

//    using (var context = new ExtendedNorthwindEntities())
//{
//  context.Log = Console.Out;   // ... 
//}
    /// <summary>
    /// 
    /// </summary>
    public class UnitTestGraphContext : DbContext
    {

        public DbSet<DistributedGraphEdge> Edges { get; set; }

        public DbSet<DistributedGraphVertex> Vertices { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //_tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

           //Keep tables as Vertex -- not Vertexes
           modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Entity<GraphEdge>().
            //    .Property(s => s.Source).
            //    .is();


  
        }
    }
}
