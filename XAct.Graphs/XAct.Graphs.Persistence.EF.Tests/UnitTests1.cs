////using NUnit.Framework;

//namespace XAct.Graphs.Tests
//{
//    using NUnit.Framework;
//    using XAct.Bootstrapper.Tests;
//    using XAct.Tests;

//    /// <summary>
//    ///   NUNit Test Fixture.
//    /// </summary>
//    [TestFixture]
//    public class UnitTests1
//    {
//        /// <summary>
//        ///   Sets up to do before any tests 
//        ///   within this test fixture are run.
//        /// </summary>
//        [TestFixtureSetUp]
//        public void TestFixtureSetUp()
//        {
//            //run once before any tests in this testfixture have been run...
//            //...setup vars common to all the upcoming tests...
//            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

//    //        DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>().FactoryDelegate =
//    //() => new EntityDbContext(new UnitTestDbContext());

//            //Register more before moving on to calling Repos:
//            DoMoreInitializationThanJustLibStuff();

//            //Initialize the db using the common initializer passing it one entity from this class
//            //so that it can perform a query (therefore create db as necessary):
//            DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<GraphVertex>();

//        }



//        // ReSharper disable MemberCanBeMadeStatic.Local
//        static void DoMoreInitializationThanJustLibStuff()
//        // ReSharper restore MemberCanBeMadeStatic.Local
//        {


//            //The EF Context:



////            //SHould be internal:
////            IServiceLocatorService serviceLocatorService = ServiceLocatorService.Current;

////            ServiceLocatorService.Current.RegisterServiceBindingInIoCByReflection(
////                new BindingDescriptor(typeof(IGraphService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>>), typeof(GraphService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>>)));


////            //Invoked from within Internal Edge, so must be Internal
////            ServiceLocatorService.Current.RegisterServiceBindingInIoCByReflection(
////                new BindingDescriptor(
////                    typeof(IGraphPersistenceService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>>),
////                    typeof(GraphPersistenceService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>, IGraphEdge<DistributedGraphVertex>>)));

////            ServiceLocatorService.Current.RegisterServiceBindingInIoCByReflection(
////                new BindingDescriptor(
////                    typeof(IGraphMruService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>>),
////                    typeof(GraphMruService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>>)));

////            DependencyResolver.Current.GetInstance<IGraphService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>>>();


////            //or in clumps:
////            //kernel.Load(new ALogicalGroupOfServicesModule());

////            Database.SetInitializer(new UnitTestGraphDbContextInitializer());


////            // ReSharper disable UnusedVariable
////#pragma warning disable 168
////            // ReSharper disable RedundantNameQualifier
////            IGraphPersistenceService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>> test1 = DependencyResolver.Current.GetInstance<IGraphPersistenceService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>>>();
////            IGraphMruService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>> test2 = DependencyResolver.Current.GetInstance<IGraphMruService<DistributedGraphVertex, int, IGraphEdge<DistributedGraphVertex>>>();
////#pragma warning restore 168
////            // ReSharper restore RedundantNameQualifier
////            // ReSharper restore UnusedVariable
//        }


//        ///// <summary>
//        /////   An Example Test.
//        ///// </summary>
//        //[Test]
//        //public void UnitTest01()
//        //{
//        //    //Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsTrue(true);
//        //}
//    }


//}


