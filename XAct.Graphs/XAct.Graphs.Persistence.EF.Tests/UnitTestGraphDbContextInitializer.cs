﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using XAct.Graphs;

namespace XAct.Graphs.Tests
{

    public class UnitTestGraphDbContextInitializer : DropCreateDatabaseIfModelChanges<UnitTestGraphContext>
    {
        private readonly IGraphDbContextSeeder _graphDbContextSeeder;

        public UnitTestGraphDbContextInitializer(IGraphDbContextSeeder graphDbContextSeeder)
        {
            _graphDbContextSeeder = graphDbContextSeeder;
        }

        protected override void Seed(UnitTestGraphContext context)
        {
            _graphDbContextSeeder.Seed(context);
        }

    }
    
}