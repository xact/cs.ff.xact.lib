﻿namespace XAct.Graphs.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Graphs.Initialization.DbContextSeeders;
    using XAct.Library.Settings;




    /// <summary>
    /// A default implementation of the <see cref="IGraphRepositoryVertexDbContextSeeder"/> contract
    /// to seed the Graph tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class GraphRepositoryVertexDbContextSeeder : 
        XActLibDbContextSeederBase<GraphRepositoryVertex>, 
        IGraphRepositoryVertexDbContextSeeder, IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphRepositoryVertexDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="graphRepositoryEdgeDbContextSeeder">The graph repository edge database context seeder.</param>
        public GraphRepositoryVertexDbContextSeeder(ITracingService tracingService, IGraphRepositoryEdgeDbContextSeeder graphRepositoryEdgeDbContextSeeder)
            : base(tracingService, graphRepositoryEdgeDbContextSeeder)
        {
        }


        public override void CreateEntities()
        {

            this.InternalEntities = new List<GraphRepositoryVertex>
                {
                    new GraphRepositoryVertex
                        {
                            Id = 1.ToGuid(),
                            GraphId = 1.ToGuid(),
                            Weight = 0,
                            DataSourceId = Guid.Empty,
                            DataSourceSerializedIdentities = null,
                            Tag = "A"
                        },
                    new GraphRepositoryVertex
                        {
                            Id = 2.ToGuid(),
                            GraphId = 1.ToGuid(),
                            Weight = 0,
                            DataSourceId = Guid.Empty,
                            DataSourceSerializedIdentities = null,
                            Tag = "B"
                        },
                    new GraphRepositoryVertex
                        {
                            Id = 3.ToGuid(),
                            GraphId = 1.ToGuid(),
                            Weight = 0,
                            DataSourceId = Guid.Empty,
                            DataSourceSerializedIdentities = null,
                            Tag = "C"
                        },
                };


        }

    }
}