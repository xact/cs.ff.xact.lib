﻿namespace XAct.Graphs.Tests
{
    using System.Data.Entity;
    using System.Collections.Generic;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Graphs.Initialization.DbContextSeeders;
    using XAct.Library.Settings;

    public class GraphRepositoryPackageSchemaDbContextSeeder : XActLibDbContextSeederBase<GraphRepositoryPackageSchema>,
                                                               IGraphRepositoryPackageSchemaDbContextSeeder,
                                                               IHasMediumBindingPriority
    {


        /// <summary>
        /// Graphes the repository package schema.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public GraphRepositoryPackageSchemaDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }



        public override void CreateEntities()
        {
            this.InternalEntities = new List<GraphRepositoryPackageSchema>();

            this.InternalEntities.Add(new GraphRepositoryPackageSchema { Id = 1.ToGuid(), Description = "DescA", Name = "NameA" });


        }

    }
}