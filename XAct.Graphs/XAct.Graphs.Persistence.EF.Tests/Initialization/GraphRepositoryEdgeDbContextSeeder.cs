﻿namespace XAct.Graphs.Tests
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Graphs.Initialization.DbContextSeeders;


    public class GraphRepositoryEdgeDbContextSeeder : XActLibDbContextSeederBase<GraphRepositoryEdge>,
                                                      IGraphRepositoryEdgeDbContextSeeder, IHasMediumBindingPriority
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="GraphRepositoryVertexDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="graphRepositoryVertexDbContextSeeder">The graph repository vertex database context seeder.</param>
        public GraphRepositoryEdgeDbContextSeeder(ITracingService tracingService,
                                                  IGraphRepositoryVertexDbContextSeeder
                                                      graphRepositoryVertexDbContextSeeder)
            : base(tracingService,
            graphRepositoryVertexDbContextSeeder)
        {
        }

        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {


            this.InternalEntities = new List<GraphRepositoryEdge>
                {
                    new GraphRepositoryEdge
                        {
                            SourceFK = 1.ToGuid(),
                            TargetFK = 2.ToGuid(),
                            Weight = 0
                        },
                    new GraphRepositoryEdge
                        {
                            SourceFK = 2.ToGuid(),
                            TargetFK = 3.ToGuid(),
                            Weight = 0
                        },
                    new GraphRepositoryEdge
                        {
                            SourceFK = 3.ToGuid(),
                            TargetFK = 2.ToGuid(),
                            Weight = 0
                        }
                };


        }
    }
}
