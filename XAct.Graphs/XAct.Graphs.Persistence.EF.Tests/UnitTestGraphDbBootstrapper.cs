using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using XAct.Bootstrapper.Tests;
using XAct.Diagnostics;
using XAct.Services;

namespace XAct.Graphs
{
    /// <summary>
    /// An implementation of the <see cref="IUnitTestGraphsDbBootstrapper"/>
    /// to ensure the creation of a database as required for the resources.
    /// </summary>
    [DefaultBindingImplementation(typeof(IUnitTestGraphsDbBootstrapper),BindingLifetimeType.SingletonScope)]
    public class UnitTestGraphsDbContextDbBootstrapper : IUnitTestGraphsDbBootstrapper
    {
        private readonly ITracingService _tracingService;
        private string _typeName;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="UnitTestGraphsDbContextDbBootstrapper"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public UnitTestGraphsDbContextDbBootstrapper(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;
            _tracingService = tracingService;
        }


        /// <summary>
        /// Ensures the Graphs tables Exists in the current db.
        /// </summary>
        public void Initialize()
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Begin...", _typeName, "Initialize");

            UnitTestGraphsDbContext dbContext = new UnitTestGraphsDbContext();

            //Could take the time to make a dedicated Initializer, but can't be f'ed:
            UnitTestDatabaseInitializer<UnitTestGraphsDbContext> unitTestDatabaseInitializer =
                new UnitTestDatabaseInitializer<UnitTestGraphsDbContext>(XAct.Shortcuts.ServiceLocate<ITracingService>());


            Database.SetInitializer(unitTestDatabaseInitializer);

            //Force creation of Db:
            DbSet<GraphSet> GraphSets = dbContext.Set<GraphSet>();
            GraphSets.FirstOrDefault(); //.Load();

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Complete.", _typeName, "Initialize");
        }
    }


}