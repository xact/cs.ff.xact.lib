﻿using XAct.Initialization;

namespace XAct.Graphs.Tests
{
    /// <summary>
    /// Contract for a class to ensure the creation of 
    /// a db and required Table.
    /// </summary>
    public interface IUnitTestGraphDbBootstrapper :IInitializable
    {
    }
}