﻿//using System;
//using NUnit.Framework;

//namespace XAct.Graphs.Tests
//{
//    using XAct.Domain.Repositories;
//    using XAct.Graphs.Services.Implementations;
//    using XAct.Tests;

//    /// <summary>
//    /// 
//    /// </summary>
//    [TestFixture]
//    public class GraphRepositoryServiceTests
//    {

//        /// <summary>
//        ///   Sets up to do before any tests 
//        ///   within this test fixture are run.
//        /// </summary>
//        [TestFixtureSetUp]
//        public void TestFixtureSetUp()
//        {

//            //run once before any tests in this testfixture have been run...
//            //...setup vars common to all the upcoming tests...
//            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
//        }


//        [Test]
//        public void Can_Get_DefaultGraphRepositoryService()
//        {
//            var service = XAct.DependencyResolver.Current.GetInstance<IDefaultGraphRepositoryService>();

//            Assert.IsNotNull(service);
//        }

//        [Test]
//        public void Can_Get_DefaultGraphRepositoryService_Of_Expected_Type()
//        {
//            var service = XAct.DependencyResolver.Current.GetInstance<IDefaultGraphRepositoryService>();

//            Assert.AreEqual(typeof(DefaultGraphRepositoryService), service.GetType());
//        }


//        [Test]
//        public void Can_Retrieve_Vertex_Without_Edges()
//        {
//            var service = XAct.DependencyResolver.Current.GetInstance<IDefaultGraphRepositoryService>();


//            var vertex = service.GetVertex(1.ToGuid());

//            Assert.IsNotNull(vertex);
//        }

//        [Test]
//        public void Can_Retrieve_Vertex_With_Edges()
//        {
//            var service = XAct.DependencyResolver.Current.GetInstance<IDefaultGraphRepositoryService>();


//            var vertex = service.GetVertex(1.ToGuid());

//            Assert.IsNotNull(vertex);
//        }

//        [Test]
//        [ExpectedException]
//        public void Can_Persist_New_Vertex_Validate_Required_GraphId()
//        {
//            var graphService = XAct.DependencyResolver.Current.GetInstance<IDefaultGraphRepositoryService>();
//            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

//            var vertex = new GraphRepositoryVertex
//                {
//                    //Id =
//                    //GraphId = ,

//                    DataSourceId = Guid.Empty,
//                    DataSourceSerializedIdentities = null,

//                    Tag="NewA",
//                    Weight=0
//                };

//            graphService.PersistVertex(vertex);

//            uowService.Current.Commit();

//            //Should not get here
//            Assert.IsFalse(true);
//        }


//        [Test]
//        [ExpectedException]
//        public void Can_Persist_New_Vertex_Validated_Does_Not_Require_DataSourceInfo()
//        {
//            var graphService = XAct.DependencyResolver.Current.GetInstance<IDefaultGraphRepositoryService>();
//            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

//            var vertex = new GraphRepositoryVertex
//            {
//                //Id =

//                GraphId = 1.ToGuid(),

//                DataSourceId = Guid.Empty,
//                DataSourceSerializedIdentities = null,

//                Tag = "NewA",
//                Weight = 0
//            };

//            graphService.PersistVertex(vertex);

//            uowService.Current.Commit();

//            Assert.IsTrue(true);
//        }

//        [Test]
//        [ExpectedException]
//        public void Can_Update_Vertex()
//        {
//            var graphService = XAct.DependencyResolver.Current.GetInstance<IDefaultGraphRepositoryService>();
//            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

//            var vertex = new GraphRepositoryVertex
//            {
//                //Id =

//                GraphId = 1.ToGuid(),

//                DataSourceId = Guid.Empty,
//                DataSourceSerializedIdentities = null,

//                Tag = "NewB",
//                Weight = 0
//            };

//            var origId = vertex.Id;

//            graphService.PersistVertex(vertex);

//            uowService.Current.Commit();

//            var savedVertexId = vertex.Id;

//            var vertexDup = graphService.GetVertex(savedVertexId);

//            vertexDup.Tag = "NewB2";

//            graphService.PersistVertex(vertexDup);

//            Assert.AreNotEqual(origId, savedVertexId, "Thought the Id would change");
//            Assert.AreNotEqual(vertex, vertexDup, "Don't think these two will be same instance?");
//            Assert.AreNotEqual("NewB2", vertex.Tag,"Didn't think orig vertex would be updated in real time.");

//            var vertexTrip = graphService.GetVertex(savedVertexId);

//            Assert.AreEqual("NewB2", vertexTrip.Tag, "Should have updated.");

//        }



//    }
//}
