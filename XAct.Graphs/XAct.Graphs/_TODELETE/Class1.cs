﻿

//namespace XAct
//{
//    using XAct.Graphs;

//    /// <summary>
//    /// Extensions to the <see cref="GraphRepositoryVertex"/>
//    /// </summary>
//    public static class GraphRepositoryVertexExtensions
//    {


//        /// <summary>
//        /// Maps the repository object to a Vertex that can be used
//        /// within a Graph.
//        /// </summary>
//        /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
//        /// <param name="graphRepositoryVertex">The graph repository vertex.</param>
//        /// <returns></returns>
//        public static TGraphVertex MapTo<TGraphVertex>(this GraphRepositoryVertex graphRepositoryVertex)
//        where TGraphVertex : class, IGraphVertex, new()
//        {
//            TGraphVertex result = new TGraphVertex
//                {
//                    GraphId = graphRepositoryVertex.GraphId,
//                    Id = graphRepositoryVertex.Id,
//                    Tag = graphRepositoryVertex.Tag,
//                    DataSourceId = graphRepositoryVertex.DataSourceId,
//                    DataSourceSerializedIdentities = graphRepositoryVertex.DataSourceSerializedIdentities,
//                };

//            return result;

//        }
//    }
//}
