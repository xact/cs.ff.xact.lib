﻿////TRACING : http://efwrappers.codeplex.com/

//// ReSharper disable CheckNamespace
//namespace XAct.Graphs.Services.Implementations
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Data.Entity;
//    using System.Linq;
//    using XAct.Domain.Repositories;
//    using XAct.Services;

//    /// <summary>
//    /// 
//    /// </summary>
//    [DefaultBindingImplementation(typeof(IGraphRepositoryService))]
//    [DefaultBindingImplementation(typeof(IGraphRepositoryService<GraphRepositoryVertex, GraphRepositoryEdge>))]
//    public class GraphRepositoryService : GraphRepositoryService<GraphRepositoryVertex, GraphRepositoryEdge>, IGraphRepositoryService
//    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="GraphRepositoryService" /> class.
//        /// </summary>
//        /// <param name="unitOfWorkManagementService">The unit of work management service.</param>
//        public GraphRepositoryService(IUnitOfWorkService unitOfWorkManagementService)
//            :base(unitOfWorkManagementService)
//        {
            
//        }
//    }








//    public abstract class GraphRepositoryService<TGraphVertex,TGraphEdge> :
//        EntityDbContextGenericRepositoryBaseBase, 
//        IGraphRepositoryService<TGraphVertex,TGraphEdge>
//        where TGraphVertex: GraphRepositoryVertex,new()
//        where TGraphEdge : GraphRepositoryEdge,new()
//    {


//        /// <summary>
//        /// Initializes a new instance of the <see cref="GraphRepositoryService{TGraphVertex, TGraphEdge}"/> class.
//        /// </summary>
//        /// <param name="unitOfWorkManagementService">The unit of work management service.</param>
//        protected GraphRepositoryService(IUnitOfWorkService unitOfWorkManagementService)
//            : base(unitOfWorkManagementService)
//        {
//        }

//        /// <summary>
//        /// Gets the <see cref="GraphPackageSchema"/>.
//        /// </summary>
//        /// <param name="uniqueName">The name.</param>
//        /// <returns></returns>
//        public GraphPackageSchema GetGraphDescription(string uniqueName)
//        {
//            return base.GetSingle<GraphPackageSchema>(gd => gd.Name == uniqueName);
//        }

//        /// <summary>
//        /// Gets the <see cref="GraphPackageSchema"/>.
//        /// </summary>
//        /// <param name="guid">The GUID.</param>
//        /// <returns></returns>
//        public GraphPackageSchema GetGraphDescription(Guid guid)
//        {
//            return base.GetSingle<GraphPackageSchema>(gd => gd.Id == guid);
//        }

//        /// <summary>
//        /// Deletes the <see cref="GraphPackageSchema"/>.
//        /// </summary>
//        /// <param name="guid">The GUID.</param>
//        public void DeleteGraphPackageSchema(Guid guid)
//        {
//            base.DeleteOnCommit<GraphPackageSchema>(gd=>gd.Id==guid);
//        }






//        public void Persist(GraphPackageSchema graphDefinition)
//        {
//            this.PersistOnCommit(graphDefinition, gd=>gd.Id== Guid.Empty);
//        }

//        /// <summary>
//        /// Determines if there is a Vertex with the given Id.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <returns></returns>
//        public bool VertexExists(Guid vertexId)
//        {
//            vertexId.ValidateIsNotDefault("vertexId");

//            return base.Contains<TGraphVertex>(v => v.Id == vertexId);
//        }


//        /// <summary>
//        /// Gets the vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <param name="includeEdges">if set to <c>true</c> [include edges].</param>
//        /// <returns></returns>
//        public TGraphVertex GetVertex(Guid vertexId, bool includeEdges=true)
//        {
//            return includeEdges
//                       ? base.GetSingle<TGraphVertex>(v => v.Id == vertexId)
//                       : base.GetSingle<TGraphVertex>(v => v.Id == vertexId,
//                                                      new IncludeSpecification<TGraphVertex>(v => v.InEdges,
//                                                                                             v => v.OutEdges));
//        }

//        /// <summary>
//        /// Persists the vertex.
//        /// </summary>
//        /// <param name="vertex">The vertex.</param>
//        public void PersistVertex(TGraphVertex vertex)
//        {
//            base.PersistOnCommit(vertex,v => v.Id == Guid.Empty);
            
//        }

//        /// <summary>
//        /// Removes the vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        public void RemoveVertex(Guid vertexId)
//        {

//            TGraphVertex entry = new TGraphVertex{Id = vertexId};
            
//            base.GetDbContext().Entry(entry).State = EntityState.Deleted;
//        }

//        /// <summary>
//        /// Removes the vertex.
//        /// </summary>
//        /// <param name="vertex">The vertex.</param>
//        public void RemoveVertex(TGraphVertex vertex)
//        {
//            base.DeleteOnCommit(vertex);
//        }


//        /// <summary>
//        /// Gets edges whose target are the given vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <param name="includeVertices">if set to <c>true</c>, include source vertices.</param>
//        /// <returns></returns>
//        public TGraphEdge[] GetInEdges(Guid vertexId, bool includeVertices = false)
//        {
//            return includeVertices 
//                ? base.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId).ToArray()
//                : base.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId,new IncludeSpecification<TGraphEdge>(e=>e.Source)).ToArray();
//        }


//        /// <summary>
//        /// Gets edges whose source are the given vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <param name="includeVertices">if set to <c>true</c>, include target vertices.</param>
//        /// <returns></returns>
//        public TGraphEdge[] GetOutEdges(Guid vertexId, bool includeVertices = false)
//        {
//            return includeVertices 
//                ? base.GetByFilter<TGraphEdge>(e => e.SourceFK == vertexId).ToArray() 
//                : base.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId,new IncludeSpecification<TGraphEdge>(e=>e.Target)).ToArray();
//        }

//        /// <summary>
//        /// Gets all (incoming and outgoing edges) edges
//        /// whose source is the given vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <param name="includeVertices">if set to <c>true</c>, include source and target vertices.</param>
//        /// <returns></returns>
//        public TGraphEdge[] GetEdges(Guid vertexId, bool includeVertices = false)
//        {
//            return includeVertices 
//                ? base.GetByFilter<TGraphEdge>(e => e.SourceFK == vertexId || e.TargetFK == vertexId).ToArray() 
//                : base.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId, new IncludeSpecification<TGraphEdge>(e => e.Target,e=>e.Source)).ToArray();
//        }

//        /// <summary>
//        /// Get the given edge.
//        /// </summary>
//        /// <param name="edgeId">The edge id.</param>
//        /// <param name="includeVertices">if set to <c>true</c>, include source vertices.</param>
//        /// <returns></returns>
//        public TGraphEdge GetEdge(Guid edgeId, bool includeVertices = false)
//        {
//            return includeVertices
//                ? base.GetSingle<TGraphEdge>(e => e.Id == edgeId)
//                : base.GetSingle<TGraphEdge>(e => e.Id == edgeId, new IncludeSpecification<TGraphEdge>(e => e.Target, e => e.Source));
//        }

//        public bool EdgeExists(Guid edgeId)
//        {
//            edgeId.ValidateIsNotDefault("edgeId");

//            return base.Contains<TGraphVertex>(v => v.Id == edgeId);
//        }


//        /// <summary>
//        /// Persists the edge.
//        /// </summary>
//        /// <param name="edge">The edge.</param>
//        public void PersistEdge(TGraphEdge edge)
//        {
//            base.PersistOnCommit(edge,e=>e.Id == Guid.Empty);
//        }



//        /// <summary>
//        /// Deletes the specified edge.
//        /// <para>
//        /// No exception is raised if the edge is not found.
//        /// </para>
//        /// </summary>
//        /// <param name="edgeId">The edge id.</param>
//        public void DeleteEdge(Guid edgeId)
//        {
//            TGraphEdge entry = new TGraphEdge { Id = edgeId };

//            base.GetDbContext().Entry(entry).State = EntityState.Deleted;
//        }

//        /// <summary>
//        /// Deletes the specified edge.
//        /// <para>
//        /// No exception is raised if the edge is not found.
//        /// </para>
//        /// </summary>
//        /// <param name="edge">The edge.</param>
//        public void DeleteEdge(TGraphEdge edge)
//        {
//            base.DeleteOnCommit(edge);
//        }

//        /// <summary>
//        /// Gets from the datastore the vertex specified by the identifier.
//        /// </summary>
//        /// <param name="graphId">The graph id.</param>
//        /// <param name="vertexId">The vertex datastore identifier.</param>
//        /// <returns></returns>
//        public TGraphVertex GetVertex(Guid graphId, Guid vertexId)
//        {
//            //this.GetSingle<GraphVertex>(v => v.Id == vertexId, new IncludeSpecification<GraphVertex>(v => v.InEdges));

//           TGraphVertex result= this.GetSingle<TGraphVertex>(v => ((v.GraphId == graphId) &&  (v.Id == vertexId)), new IncludeSpecification("InEdges","OutEdges"));

//            return result;
//        }

//    }
//}
