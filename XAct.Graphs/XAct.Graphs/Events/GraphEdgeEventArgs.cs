﻿
// ReSharper disable CheckNamespace
namespace XAct.Graphs {
// ReSharper restore CheckNamespace

    /// <summary>
	/// Event Args package pertaining to a Edge.
	/// </summary>
    public class GraphEdgeEventArgs<TGraphVertex, TGraphEdge> : GraphItemEventArgsBase<TGraphEdge>
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class, IGraphEdge<TGraphVertex>, new()

    {

		/// <summary>
		/// Gets the vertex the event is about.
		/// </summary>
		/// <value>The vertex.</value>
		public TGraphEdge Edge {
			get {
				return _Item;
			}
		}


		/// <summary>
        /// Initializes a new instance of the <see cref="GraphEdgeEventArgs{TGraphVertex, TGraphEdge}"/> class.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		public GraphEdgeEventArgs(TGraphEdge vertex)
			: base(vertex, false) {
		}
	}
}
