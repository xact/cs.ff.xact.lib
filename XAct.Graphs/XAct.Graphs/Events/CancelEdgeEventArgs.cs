

namespace XAct.Graphs {
  
  /// <summary>
    /// Cancelable event with an <see cref="IGraphEdge{TVertex}"/> property
  /// </summary>
	public class CancelEdgeEventArgs<TGraphVertex> : 
        GraphItemCancelEventArgsBase<IGraphEdge<TGraphVertex>>
        where TGraphVertex : class, IGraphVertex, new()
    {

    /// <summary>
    /// Gets the edge.
    /// </summary>
    /// <value>The edge.</value>
        public IGraphEdge<TGraphVertex> Edge { get { return _item; } }

    /// <summary>
    /// Initializes a new instance of the <see cref="CancelEdgeEventArgs{TGraphVertex}"/> class.
    /// </summary>
    /// <param name="edge">The edge.</param>
        public CancelEdgeEventArgs(IGraphEdge<TGraphVertex> edge) : base(edge, false) {
    }
  }
}
