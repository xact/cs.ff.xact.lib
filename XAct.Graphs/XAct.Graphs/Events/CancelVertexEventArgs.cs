
// ReSharper disable CheckNamespace
namespace XAct.Graphs {
// ReSharper restore CheckNamespace

    
    /// <summary>
  /// Cancelable Event with a GraphVertex property
  /// </summary>
	public class CancelVertexEventArgs<TGraphVertex> : CancelEventArgsBase<TGraphVertex>
        where TGraphVertex : class, IGraphVertex, new()
    {
    

    /// <summary>
    /// Gets the vertex.
    /// </summary>
    /// <value>The vertex.</value>
        public TGraphVertex Vertex
        {
            get
            {
                return _item;
            }
        }

    /// <summary>
    /// Initializes a new instance of the <see cref="CancelVertexEventArgs{TGraphVertex}"/> class.
    /// </summary>
    /// <param name="vertex">The vertex.</param>
		public CancelVertexEventArgs(TGraphVertex vertex)
			: base(vertex, false) {

    }
   

  }
}
