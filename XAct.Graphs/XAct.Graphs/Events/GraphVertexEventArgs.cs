﻿
// ReSharper disable CheckNamespace
namespace XAct.Graphs {
// ReSharper restore CheckNamespace

    /// <summary>
	/// Event Args package pertaining to a Vertex.
	/// </summary>
    public class GraphVertexEventArgs<TGraphVertex> : GraphItemEventArgsBase<TGraphVertex>
        where TGraphVertex : class, IGraphVertex, new()
    {

		/// <summary>
		/// Gets the vertex the event is about.
		/// </summary>
		/// <value>The vertex.</value>
		public TGraphVertex Vertex {
			get {
				return _Item;
			}
		}


		/// <summary>
		/// Initializes a new instance of the <see cref="GraphVertexEventArgs{TGraphVertex}"/> class.
		/// </summary>
		/// <param name="vertex">The vertex.</param>
		public GraphVertexEventArgs(TGraphVertex vertex)
			: base(vertex, false) {
		}
	}
}
