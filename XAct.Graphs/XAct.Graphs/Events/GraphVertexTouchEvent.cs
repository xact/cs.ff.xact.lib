//using XAct.Events;

//namespace XAct.Graphs
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public class GraphVertexTouchEvent : IEvent
//    {
//        /// <summary>
//        /// Gets the graph id -- used by <see cref="GraphEdgeBase{TVertex}"/>
//        /// when Event Aggregating/broadcasting to the right <see cref="GraphMruCleaner{TVertex,TEdge}"/>
//        /// </summary>
//        /// <value>
//        /// The graph identifier.
//        /// </value>
//        public IGraph Graph { get; private set; }

//        /// <summary>
//        /// Gets or sets the vertex.
//        /// </summary>
//        /// <value>
//        /// The vertex.
//        /// </value>
//        public XAct.IHasGuidIdentity Vertex
//        {
//            get { return _vertex; }
//        }

//        private readonly XAct.IHasGuidIdentity _vertex;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="GraphVertexTouchEvent" /> class.
//        /// </summary>
//        /// <param name="graph">The graph identifier.</param>
//        /// <param name="vertex">The vertex.</param>
//        public GraphVertexTouchEvent(IGraph graph, XAct.IHasGuidIdentity vertex)
//        {
//            Graph = graph;
//            _vertex = vertex;
//        }
//    }
//}