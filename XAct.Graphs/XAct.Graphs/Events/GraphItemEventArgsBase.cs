﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs {
    using System;
    using System.Collections.Generic;

// ReSharper restore CheckNamespace

	/// <summary>
	/// Abstract base class for Event Args.
	/// </summary>
	/// <typeparam name="TItem"></typeparam>
	public abstract class GraphItemEventArgsBase<TItem> : EventArgs
        //where TGraphVertex : class, IGraphVertex, new()
    {

		/// <summary>
		/// The generic item this argument package is about.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Enherit from this class and do something like the following:
		/// <code>
		/// <![CDATA[
		/// public Vertex Vertex {
		///		get {
		///		return _Item;
		///		}
		/// }
		/// ]]>
		/// </code>
		/// </para>
		/// </remarks>
		protected TItem _Item;

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphItemEventArgsBase&lt;TItem&gt;"/> class.
		/// </summary>
		/// <param name="item">The item.</param>
        protected GraphItemEventArgsBase(TItem item)
            : this(item, false)
        {
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="GraphItemEventArgsBase&lt;TItem&gt;"/> class.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="allowDefaultValue">if set to <c>true</c> allow a null(default) item.</param>
		protected GraphItemEventArgsBase(TItem item, bool allowDefaultValue) {
			if ((!allowDefaultValue) && (Comparer<TItem>.Default.Compare(item, default(TItem))==0)) {
				throw new ArgumentNullException("item");
			}
			_Item = item;
		}
		#endregion
	}
}
