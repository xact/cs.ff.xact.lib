﻿
// ReSharper disable CheckNamespace
namespace XAct.Graphs {
// ReSharper restore CheckNamespace

	/// <summary>
	/// Generic Cancellable EventArgs package.
	/// </summary>
	/// <typeparam name="TItem">The type of the item.</typeparam>
	public abstract class GraphItemCancelEventArgsBase<TItem> : CancelEventArgsBase<TItem>
        //where TGraphVertex : class, IGraphVertex, new()
    {


		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GraphItemEventArgsBase&lt;TItem&gt;"/> class.
		/// </summary>
		/// <param name="item">The item.</param>
		protected GraphItemCancelEventArgsBase(TItem item) :base(item){
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="GraphItemEventArgsBase&lt;TItem&gt;"/> class.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="allowDefaultValue">if set to <c>true</c> allow a null(default) item.</param>
		protected GraphItemCancelEventArgsBase(TItem item, bool allowDefaultValue) : base(item,allowDefaultValue){}
		#endregion
	}
}
