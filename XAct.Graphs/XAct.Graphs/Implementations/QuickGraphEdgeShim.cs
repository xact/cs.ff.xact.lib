﻿

// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A shim/wrapper to insert non QuickGraph compliant Edges into 
    /// a QuickGraph Graph.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The QuickGraph library requires that all Edges implement it's IEdge interface.
    /// But you don't want to apply that directly to your domain edges, or you'll
    /// drag a ref to that library all up and down your stack.
    /// </para>
    /// <para>
    /// A solution is to use a shim/wrapper to insert your own edges (that don't 
    /// implement QG's edge interface) into the graph.
    /// </para>
    /// <para>
    /// To retrieve your own inner edge, use the <see cref="GetInnerItem{T}"/>
    /// property.
    /// </para>
    /// </remarks>
    /// <typeparam name="TVertex">The type of the vertex.</typeparam>
    public class QuickGraphEdgeShim<TVertex> : IQuickGraphEdgeShim<TVertex>,QuickGraph.IEdge<TVertex>
        where TVertex : class, IGraphVertex, new()
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickGraphEdgeShim{TVertex}" /> class.
        /// </summary>
        /// <param name="edge">The edge.</param>
        public QuickGraphEdgeShim(IGraphEdge<TVertex> edge)
        {
            _innerItem = edge;
        }

        /// <summary>
        /// Gets the source.
        /// <para>
        /// Method defined in <see cref="QuickGraph.IEdge{TVertex}"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public TVertex Source
        {
// ReSharper disable RedundantThisQualifier
            get { return _innerItem.Source; }
// ReSharper restore RedundantThisQualifier
        }

        /// <summary>
        /// Gets the target.
        /// <para>
        /// Method defined in <see cref="QuickGraph.IEdge{TVertex}"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public TVertex Target
        {
// ReSharper disable RedundantThisQualifier
            get { return _innerItem.Target; }
// ReSharper restore RedundantThisQualifier
        }

        /// <summary>
        /// Gets or sets the inner Edge item.
        /// <para>
        /// Method defined in <see cref="IHasInnerItemReadOnly"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The inner item.
        /// </value>
        public T GetInnerItem<T> ()
        {
            return _innerItem.ConvertTo<T>();
        }

        private readonly IGraphEdge<TVertex> _innerItem;

    }

 }
