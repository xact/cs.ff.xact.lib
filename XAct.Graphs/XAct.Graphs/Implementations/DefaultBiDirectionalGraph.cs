﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Diagnostics;
    using XAct.Graphs.Implementations;

    /// <summary>
    /// A default specialization of 
    /// <see cref="BidirectionalGraph{TGraphVertex, TGraphEdge}"/>
    /// for working with the default <see cref="GraphVertex"/>
    /// and <see cref="GraphEdge"/>
    /// <para>
    /// Exposed as a property of 
    /// <see cref="GraphPackage{TGraphVertex,TGraphEdge}"/>
    /// </para>
    /// </summary>
    public class DefaultBidirectionalGraph :
        BidirectionalGraph<GraphVertex, GraphEdge>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultBidirectionalGraph" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="graphPersistenceService">The graph repository service.</param>
        /// <param name="graphId">The graph id.</param>
        public DefaultBidirectionalGraph(
            ITracingService tracingService,
            IGraphPersistenceService<GraphVertex, IGraphEdge<GraphVertex>> graphPersistenceService,
            Guid graphId)
            :
                base(tracingService, graphPersistenceService, graphId)
        {


        }

    }


}

