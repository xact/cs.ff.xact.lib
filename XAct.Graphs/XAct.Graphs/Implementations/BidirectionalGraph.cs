﻿
// ReSharper disable CheckNamespace

namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using QuickGraph.Algorithms;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// A Bidirectional Graph implementation of <see cref="IGraph{TGraphVertex,TGraphEdge}"/>
    /// <para>
    /// In order to not drag a reference to QuickGraph up and down the stack,
    /// this is a wrapper of a QuickGraph Bidirectional graph, that 
    /// can be retrieved as needed, using <see cref="IHasInnerItemReadOnly"/>
    /// to get to it.
    /// </para>
    /// </summary>
    /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
    /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
    public class BidirectionalGraph<TGraphVertex, TGraphEdge>
        :
        XActLibServiceBase  ,
        IGraph<TGraphVertex, TGraphEdge>
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class, IGraphEdge<TGraphVertex>, new()
    {


        private readonly IGraphPersistenceService<TGraphVertex, IGraphEdge<TGraphVertex>> _graphPersistenceService;


        //Notice the Graph is for our normal Vertices, but with a Shim Edge.
        //The shim edge implements the QuickGraph Contract.
        private readonly QuickGraph.BidirectionalGraph<TGraphVertex, QuickGraphEdgeShim<TGraphVertex>> _quickGraph;

        //TODO: can't remember what this does:
        private readonly Dictionary<TGraphEdge, QuickGraphEdgeShim<TGraphVertex>> _refs =
            new Dictionary<TGraphEdge, QuickGraphEdgeShim<TGraphVertex>>();


        //The mru cleaner has a reference to this Graph.
        //From there, it can get the InnerObject (the QuickGraph)
        //It can remove stale nodes by invoking this Graph's Remove node,
        //persisting them if there are any changes.
        private readonly GraphMruCleaner<TGraphVertex, TGraphEdge> _mruCleaner;


        /// <summary>
        /// Gets the Graph id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public virtual Guid Id { get; private set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="BidirectionalGraph{TGraphVertex,TGraphEdge}" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="graphPersistenceService">The graph persistence service.</param>
        /// <param name="id">The id.</param>
        public BidirectionalGraph(ITracingService tracingService, 
            IGraphPersistenceService<TGraphVertex, IGraphEdge<TGraphVertex>> graphPersistenceService, 
            Guid id)
            : base(tracingService)
        {


            _graphPersistenceService = graphPersistenceService;

            Id = id;

            //ok QuickGraph.AdjacencyGraph<TGraphVertex, QGEdgeShim<TGraphVertex>> X;
            //NO QuickGraph.ArrayAdjacencyGraph<TGraphVertex, QGEdgeShim<TGraphVertex>> X;
           //NO QuickGraph.ArrayBidirectionalGraph<TGraphVertex, QGEdgeShim<TGraphVertex>> X;
            //NO QuickGraph.ArrayUndirectedGraph<TGraphVertex, QGEdgeShim<TGraphVertex>> X;
            //NO QuickGraph.BidirectionAdapterGraph<TGraphVertex, QGEdgeShim<TGraphVertex>> X;
           //ok QuickGraph.BidirectionalGraph<TGraphVertex, QGEdgeShim<TGraphVertex>> X;
           
          
            // ReSharper disable RedundantNameQualifier
            //Leave ref to QG on to not cause incorrect binding.
            _quickGraph = new QuickGraph.BidirectionalGraph<TGraphVertex, QuickGraphEdgeShim<TGraphVertex>>();
            // ReSharper restore RedundantNameQualifier

            _mruCleaner = new GraphMruCleaner<TGraphVertex, TGraphEdge>(this);
        }

        /// <summary>
        /// Gets the inner item ( the QuickGraph, typed as an object).
        /// <para>
        /// This can be useful to pass the QG graph to a QG walker/algorithm.
        /// </para>
        /// </summary>
        /// <value>
        /// The inner item.
        /// </value>
        T IHasInnerItemReadOnly.GetInnerItem<T>()
        {
             return _quickGraph.ConvertTo<T>(); 
        }



        /// <summary>
        /// Loads the graph data.
        /// </summary>
        public void LoadGraphData()
        {
            _graphPersistenceService.GetGraphData(this.Id);
        }
        




        /// <summary>
        /// Loads the vertices.
        /// </summary>
        /// <param name="ids">The ids.</param>
        public void LoadVertices(params Guid[] ids)
        {
            foreach (Guid id in ids)
            {
                var kv = _graphPersistenceService.GetVertexAndEdges(id);

                //Add it to the quickGraph:
                _quickGraph.AddVertex(kv.Key);


                kv.Value.ForEach(x => _quickGraph.AddEdge(new QuickGraphEdgeShim<TGraphVertex>(x)));
            }
        }

        /// <summary>
        /// Loads the vertices.
        /// </summary>
        /// <param name="dataSourceIdentifiers">The data source identifiers.</param>
        public void LoadVertices(params IHasDataSourceIdentifier[] dataSourceIdentifiers)
        {
            foreach (IHasDataSourceIdentifier dataSourceIdentifier in dataSourceIdentifiers)
            {
                var r = _graphPersistenceService.GetVertexAndEdges(dataSourceIdentifier);

                //Add it to the quickGraph:
                _quickGraph.AddVertex(r.Key);
                r.Value.ForEach(x => _quickGraph.AddEdge(new QuickGraphEdgeShim<TGraphVertex>(x)));
            }
        }

        /// <summary>
        /// Persists this instance.
        /// </summary>
        public void Persist()
        {
            foreach (var v in _quickGraph.Vertices)
            {
                _graphPersistenceService.PersistVertex(v);
            }
            foreach (var e in _quickGraph.Edges)
            {
                _graphPersistenceService.PersistEdge(e.GetInnerItem<TGraphEdge>());
            }
        }



        /// <summary>
        /// Adds the vertex to the graph.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public void AddVertex(TGraphVertex vertex)
        {
            vertex.ValidateIsNotDefault("vertex");

            _tracingService.Trace(TraceLevel.Verbose, "Adding Vertex {0}", vertex.ToString());

            //Graphs should be able to be created in memory, 
            //as a scratchpad if you will,
            //without having to persist every single time.

            //That said, MRUCleaner persists records as they are culled,
            //so newly added records will be invoked, 
            

            //Add it to the quickGraph:
            _quickGraph.AddVertex(vertex);
        }


        /// <summary>
        /// Removes the vertex from the graph,
        /// and any Edges that have this vertex as their
        /// Source or Target.
        /// <para>
        /// Periodically Invoked by MruCleaner.
        /// </para>
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public void RemoveVertex(TGraphVertex vertex)
        {
            vertex.ValidateIsNotDefault("vertex");

            _tracingService.Trace(TraceLevel.Verbose, "Removing Vertex {0}", vertex.ToString());

            //Note that MRUCleaner invokes this method when culling it.
            //So it's not the right place to invoke Delete.

            _quickGraph.RemoveVertex(vertex);
        }




        /// <summary>
        /// Adds the edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        public void AddEdge(TGraphEdge edge)
        {
            //holdMeTight = edge.GetType();

            QuickGraphEdgeShim<TGraphVertex> edgeQgWrapper = new QuickGraphEdgeShim<TGraphVertex>(edge);

            //Associate the new wrapper to the core (see why below)
            _refs[edge] = edgeQgWrapper;
            //edge.EdgeWrapper = edgeQgWrapper;

            edge.Graph = this;

            _quickGraph.AddEdge(edgeQgWrapper);
        }


        /// <summary>
        /// Edges the count.
        /// </summary>
        /// <returns></returns>
        public int EdgeCount()
        {
            return _quickGraph.EdgeCount;
        }
        /// <summary>
        /// Vertexes the count.
        /// </summary>
        /// <returns></returns>
        public int VertexCount()
        {
            return _quickGraph.VertexCount;
        }

        /// <summary>
        /// Removes the edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        public void RemoveEdge(TGraphEdge edge)
        {
            //Removal is more complicated.
            //Because if the object being passed gets wrapped with a wrapper, it's 
            //*not* the same wrapper as what was used to Add it in the first place.
            //To get back to the orig wrapper .. either keep a ref between each inner and outer object (possible)
            //or keep *another* list...prefer option A.

            QuickGraphEdgeShim<TGraphVertex> edgeShim;

            if (!_refs.TryGetValue(edge, out edgeShim))
            {

            }
            else
            {
                _quickGraph.RemoveEdge(edgeShim);
            }

            //Not any more...
            //QGEdgeShim<TGraphVertex> edgeQgWrapper = (QGEdgeShim<TGraphVertex>)edge.EdgeWrapper;

        }

        /// <summary>
        /// Gets the in edges for the vertex
        /// (Edges that have this vertex as their Target).
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        public IEnumerable<TGraphEdge> GetInEdges(TGraphVertex vertex)
        {
            vertex.ValidateIsNotDefault("vertex");

            //As the shim objects are returned, 
            //unwrap in order to return desired inner edge object
            //(that does not implement QuickGRaph's IEdge interfaace):
            return _quickGraph.InEdges(vertex).Select(e => e.GetInnerItem<TGraphEdge>());
        }

        /// <summary>
        /// Gets the out edges for the vertex
        /// (Edges that have this vertex as their Source).
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public IEnumerable<TGraphEdge> GetOutEdges(TGraphVertex vertex)
        {
            vertex.ValidateIsNotDefault("vertex");

            //As the shim objects are returned, 
            //unwrap in order to return desired inner edge object
            //(that does not implement QuickGRaph's IEdge interfaace):
            return _quickGraph.OutEdges(vertex).Select(e => e.GetInnerItem<TGraphEdge>());
        }


        /// <summary>
        /// Gets the edges (in and out) for the vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        public IEnumerable<TGraphEdge> GetAllEdges(TGraphVertex vertex)
        {
            vertex.ValidateIsNotDefault("vertex");

            foreach (TGraphEdge edge in this.GetInEdges(vertex))
            {
                yield return edge;
            }

            foreach (TGraphEdge edge in this.GetOutEdges(vertex))
            {
                yield return edge;
            }
        }


        /// <summary>
        /// Prepares the vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public void CompletePartialVertex(TGraphVertex vertex)
        {

            // Updates the Vertex MRU stack in order to better cleanup later.
            _mruCleaner.Touch(vertex);

            if (!vertex.IsIncomplete)
            {
                //If this is not a placeholder capable vertex, 
                //or is, but is already complete, there's
                //nothing to do:
                return;
            }

            //It's incomplete: we need to hydrate the Vertex...

            //Use Id instead to fetch it from some service...
            _graphPersistenceService.RefreshVertex(vertex);


            //If hydrated, set flag that it is so
            vertex.IsIncomplete = false;

            //We should not need to add it to the Graph, as it's already 
            //been added when the edge brought it in
            //via lazy loading, or other.
        }


        /// <summary>
        /// Shortests the path dijkstra.
        /// </summary>
        /// <param name="rootVertex">The root vertex.</param>
        /// <param name="targetVertex">The target vertex.</param>
        /// <param name="shortestPathAlgorithm">The shortest path algorithm.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException">shortestPathAlgorithm</exception>
        public IEnumerable<TGraphEdge> ShortestPathDijkstra(TGraphVertex rootVertex, TGraphVertex targetVertex,  ShortestPathAlgorithm shortestPathAlgorithm=ShortestPathAlgorithm.Dijkstra)
        {
            Func<QuickGraphEdgeShim<TGraphVertex>, double> edgeCost = null;

            if (edgeCost == null)
            {
                edgeCost = edge => 1; // constant cost
            } 

            // compute shortest paths
            QuickGraph.TryFunc<TGraphVertex, IEnumerable<QuickGraphEdgeShim<TGraphVertex>>> tryGetPaths;

            switch (shortestPathAlgorithm)
            {
                case ShortestPathAlgorithm.Dijkstra:
                    tryGetPaths = _quickGraph.ShortestPathsDijkstra(edgeCost, rootVertex);
                    break;
                case ShortestPathAlgorithm.BellmanFord:
                    tryGetPaths = _quickGraph.ShortestPathsBellmanFord(edgeCost, rootVertex);
                    break;
                case ShortestPathAlgorithm.Dag:
                    tryGetPaths = _quickGraph.ShortestPathsDag(edgeCost, rootVertex);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("shortestPathAlgorithm");
            }

            IEnumerable<QuickGraphEdgeShim<TGraphVertex>> path;

            if (tryGetPaths(targetVertex, out path))
            {
                foreach (QuickGraphEdgeShim<TGraphVertex> edgeShim in path)
                {
                    TGraphEdge edge = edgeShim.GetInnerItem<TGraphEdge>();
                    yield return edge;
                }
            }

        }



        /// <summary>
        /// Topologicals the sort.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TGraphVertex> TopologicalSort()
        {
            return _quickGraph.TopologicalSort();
        }
    }
}
