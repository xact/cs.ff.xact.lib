﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;
    using System.Linq;
    using XAct.Collections;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IGraphPruner), BindingLifetimeType.Undefined, Priority.Low)]
    [DefaultBindingImplementation(typeof(IGraphPruner<GraphVertex, GraphEdge>), BindingLifetimeType.Undefined, Priority.Low)]
    public class GraphMruCleaner : GraphMruCleaner<GraphVertex, GraphEdge>, IGraphPruner
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GraphMruCleaner" /> class.
        /// </summary>
        /// <param name="graph">The graph.</param>
        public GraphMruCleaner(IMRUGraph<GraphVertex, GraphEdge> graph) : base(graph)
        {
            
        }
    }


        
        /// <summary>
    /// An implementation of <see cref="IGraphPruner{TGraphVertex, TGraphEdge}"/>
    /// to release vertices from memory.
    /// </summary>
    /// <typeparam name="TGraphVertex">The type of the vertex.</typeparam>
    /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
    public class GraphMruCleaner<TGraphVertex, TGraphEdge> :
        IGraphPruner<TGraphVertex, TGraphEdge>
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class, IGraphEdge<TGraphVertex>, new()

    {

        /// <summary>
        /// Default preferred maximum size of the graph.
        /// </summary>
        public const int PreferredMaximumNumberOfNodes = 1024;

        /// <summary>
        /// Event raised to persis the given vertex.
        /// </summary>
        public event EventHandler<CancelVertexEventArgs<TGraphVertex>> CancellablePersistVertex;

        
        /// <summary>
        /// Occurs when a vertex is about to be converted into a placeholder vertex
        /// removing its data from memory. 
        /// <para>
        /// Note that this is just befoe it gets persisted.
        /// </para>
        /// </summary>
        public event EventHandler<CancelVertexEventArgs<TGraphVertex>> CancellableCullingVertex;


        private readonly MRUList<TGraphVertex, Guid> _mruList;

        private readonly IMRUGraph<TGraphVertex, TGraphEdge> _graph;

        /// <summary>
        /// Gets or sets the preferred max number of Vertices to keep in the graph.
        /// <para>
        /// Default value is 1024 Vertices..which is of course totally arbitrary.
        /// </para>
        /// </summary>
        public int PreferredMaxNumberofVertices
        {
            get { return _preferredMaxNumberofVertices; }
            set { _preferredMaxNumberofVertices = Math.Max(value, PreferredMaximumNumberOfNodes); }
        }
        private int _preferredMaxNumberofVertices = PreferredMaximumNumberOfNodes;





        /// <summary>
        /// Initializes a new instance of the <see cref="GraphMruCleaner&lt;TGraphVertex, TGraphEdge&gt;" /> class.
        /// </summary>
        /// <param name="graph">The graph.</param>
        public GraphMruCleaner(
            IMRUGraph<TGraphVertex,TGraphEdge> graph
            )
        {
            _preferredMaxNumberofVertices = PreferredMaximumNumberOfNodes;

            _graph = graph;

            _mruList = new MRUList<TGraphVertex, Guid>(v => v.Id);


        }



        /// <summary>
        /// Traces the specified vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public void Touch(TGraphVertex vertex)
        {
            _mruList.MakeNewest(vertex);
        }

        /// <summary>
        /// Culls this graph of old/stale vertices.
        /// </summary>
        public void ReleaseVertices()
        {
            int counter = 0;

            //Iterate through elements from Oldest to Newest:
            foreach (TGraphVertex vertex in _mruList)
            {
                counter++;

                if (counter < _preferredMaxNumberofVertices)
                {
                    //Still within acceptable size. 
                    //Don't remove.
                    continue;
                }

                //We have removed it from the MRU List
                //Time to remove it from the GRaph -- and the Edges that go to it.

                //NO! It's more complicated than just removing the vertex:
                //GraphService.RemoveVertex(vertex);
                //Because if you do, you remove the hint from the Source to this
                //Target (it thinks there is NO target).
                //One can only remove from mem Vertices that have Sources that 
                //are PlaceHolders themselves.
                //Ones that are PlaceHolders, but have Source NonPlaceHolders
                //must stay in memory.

                IHasIncomplete v = vertex as IHasIncomplete;

                //Instead, one has to:
                if ((v==null) ||(!v.IsIncomplete))
                {
                    //It's not a placeholder -- it's the fully hydrated element.

                    //Check with user has any objections...
                    if (VerifyWhetherToCancelCull(vertex))
                    {
                        //Go to next vertex
                        continue;
                    } 

                    //We probably can cull now...but it may need saving first.

                    //Pass it to the Saving mechanism 
                    //PS: Leave it to the IGraphPersistenceService
                    //to determine if the edge is dirty or not.
                    if (!PersistVertex(vertex))
                    {
                        //Failed to persist changes, so don't remove the vertex...
                        continue;
                    }
                    

                    //Once saved, it's ok to 
                    //clear the vertex's data (lightens it up):
                    //but don't yet remove it from the graph...
                    if (v!=null){ v.Clear();}

                    //Note that the vertex is still part of the graph...
                    //it's just a 'lighter' vertex...

                }

                //But that's only the first pass!
                //We've lightened it...but it's not safe to 
                //remove unless all the the vertices
                //that surround this vertex are ALL placeholders/light versions.
                bool safeToRemove = VerifyIfSafeToRemove2(vertex);

                //Seems we are light/partial AND all areound us are too.
                if (safeToRemove)
                {
                    
                    //All the edges are pointing to PlaceHolders.
                    //Safe to remove.

                    //Now remove it from the graph, without deleting it frmo the datastore:
                    _graph.RemoveVertex(vertex);

                    //And don't forget to remove it from the linked list!
                    this._mruList.Remove(vertex);
                }

                //That's alright...any missed will get picked up next time around.

            } //End loop
        }

        private bool VerifyIfSafeToRemove2(TGraphVertex vertex)
        {
            bool safeToRemove =
                _graph.GetInEdges(vertex).Select(e => e.Source as IHasIncomplete).All(v => v.IsIncomplete)
                &
                _graph.GetOutEdges(vertex).Select(e => e.Target as IHasIncomplete).All(v => v.IsIncomplete);

            return safeToRemove;
        }



        #region RaiseEvents

        /// <summary>
        /// Raises an optional cancellable event, and returns true
        /// if event handler wants to cancel culling of this item.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        protected bool VerifyWhetherToCancelCull(TGraphVertex vertex)
        {
            if (this.CancellableCullingVertex == null)
            {
                //Don't bother
                return false;
            }

            //Raise the event:
            CancelVertexEventArgs<TGraphVertex> cancelVertexEventArgs =
                new CancelVertexEventArgs<TGraphVertex>(vertex);

            OnCancelCullingVertex(cancelVertexEventArgs);

            return cancelVertexEventArgs.Cancel;
        }


        /// <summary>
        /// Raises the <see cref="CancellableCullingVertex"/> event.
        /// <para>
        /// Invoked by <see cref="VerifyWhetherToCancelCull"/> 
        /// </para>
        /// </summary>
        /// <param name="eventArgs">The <see cref="XAct.Graphs.CancelVertexEventArgs&lt;TGraphVertex&gt;"/> instance containing the event data.</param>
        protected void OnCancelCullingVertex(CancelVertexEventArgs<TGraphVertex> eventArgs)
        {

            if (this.CancellableCullingVertex == null) { return; }

            CancellableCullingVertex(this, eventArgs);
        }


        /// <summary>
        /// Raises an optional cancellable event, and returns true
        /// if event handler wants to cancel culling of this item.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        protected bool PersistVertex(TGraphVertex vertex)
        {
            if (this.CancellablePersistVertex == null)
            {
                //Don't bother
                return false;
            }

            //Raise the event:
            CancelVertexEventArgs<TGraphVertex> cancelVertexEventArgs =
                new CancelVertexEventArgs<TGraphVertex>(vertex);

            OnCancelPersistVertex(cancelVertexEventArgs);

            return cancelVertexEventArgs.Cancel;
        }


        /// <summary>
        /// Raises the <see cref="CancellablePersistVertex"/> event.
        /// <para>
        /// Invoked by <see cref="VerifyWhetherToCancelCull"/> 
        /// </para>
        /// </summary>
        /// <param name="eventArgs">The <see cref="XAct.Graphs.CancelVertexEventArgs&lt;TGraphVertex&gt;"/> instance containing the event data.</param>
        protected void OnCancelPersistVertex(CancelVertexEventArgs<TGraphVertex> eventArgs)
        {

            if (this.CancellablePersistVertex == null) { return; }

            CancellablePersistVertex(this, eventArgs);
        }

        #endregion



    }
}
