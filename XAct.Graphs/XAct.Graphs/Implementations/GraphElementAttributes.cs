// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Graphs.Implementations;

    /// <summary>
    /// A cache of a single user's attributes
    /// for a single element vertex/edge.
    /// <para>
    /// Used by <see cref="GraphElementsAttributes" />,
    /// which is a cache of all element attributes per user
    /// (keeping element attributes stored per user, not per node,
    /// separating them better for security as well as making them
    /// easier to dump at the end of a session).
    /// </para>
    /// <para>
    /// <see cref="GraphElementsAttributes"/> is in
    /// turned managed by an implementation of 
    /// <see cref="IGraphVertexAttributeService"/>
    /// </para>
    /// <para>
    /// TODO: To be serializable out of proc, it's going to have to be serializable. 
    /// </para>
    /// </summary>
    public class GraphElementAttributes
    {

        /// <summary>
        /// Gets or sets the UTC date on which the Db was last checked for attributes
        /// </summary>
        /// <value>
        /// The last checked UTC.
        /// </value>
        public DateTime? LastCheckedUtc { get; set; }
        
        /// <summary>
        /// Gets or sets a single user's attributes for a single element.
        /// </summary>
        /// <value>
        /// The attributes. Null if no attributes.
        /// </value>
        /// <remarks>
        /// The use of an array rather than a list, makes it a little
        /// more difficult to make updates, but saves memory space.
        /// </remarks>
        public GraphEntityAttribute2[] Attributes { get; set; }

    }
}