﻿
// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a service to manage a Graph of vertices.
    /// </summary>
    public interface IGraphService : IHasXActLibService
    {

        /// <summary>
        /// Creates a new <see cref="BidirectionalGraph{TGraphVertex,TGraphEdge}"/>
        /// in which to persist <see cref="IGraphVertex"/> and <see cref="IGraphEdge{TIGraphVertex}"/>
        /// entities.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="graphType">Type of the graph.</param>
        /// <returns></returns>
        IGraphPackage<TGraphVertex, TGraphEdge> CreateGraph<TGraphVertex, TGraphEdge>(string name, string description, GraphType graphType = GraphType.Bidirectional)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new();


        /// <summary>
        /// Creates the graph.
        /// </summary>
        /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
        /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
        /// <param name="graphId">The graph id.</param>
        /// <param name="graphType">Type of the graph.</param>
        /// <returns></returns>
        IGraph<TGraphVertex, TGraphEdge> CreateGraph<TGraphVertex, TGraphEdge>(Guid graphId, GraphType graphType = GraphType.Bidirectional)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new();

        
        /// <summary>
        /// Get's an existing <see cref="BidirectionalGraph{TGraphVertex,TGraphEdge}"/>
        /// in which to persist <see cref="IGraphVertex"/> and <see cref="IGraphEdge{TIGraphVertex}"/>
        /// entities.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        IGraphPackage<TGraphVertex, TGraphEdge> GetGraphByName<TGraphVertex, TGraphEdge>(string name)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new();

        /// <summary>
        /// Get's an existing <see cref="BidirectionalGraph{TGraphVertex,TGraphEdge}"/>
        /// in which to persist <see cref="IGraphVertex"/> and <see cref="IGraphEdge{TIGraphVertex}"/>
        /// entities.
        /// </summary>
        /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
        /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
        /// <param name="graphId">The graph's unique id.</param>
        /// <returns></returns>
        IGraphPackage<TGraphVertex, TGraphEdge> GetGraphById<TGraphVertex, TGraphEdge>(Guid graphId)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new();

        /// <summary>
        /// Delete's an existing <see cref="BidirectionalGraph{TGraphVertex,TGraphEdge}"/>
        /// in which to persist <see cref="IGraphVertex"/> and <see cref="IGraphEdge{TIGraphVertex}"/>
        /// entities.
        /// </summary>
        /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
        /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
        /// <param name="graphId">The graph's unique id.</param>
        /// <returns></returns>
        void DeleteGraph<TGraphVertex, TGraphEdge>(Guid graphId)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new();



    }
}
