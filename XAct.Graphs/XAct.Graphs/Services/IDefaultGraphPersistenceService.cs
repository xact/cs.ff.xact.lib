﻿namespace XAct.Graphs
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDefaultGraphPersistenceService : IGraphPersistenceService<GraphVertex, GraphEdge>,
                                                       IHasXActLibService
    {

    }
}