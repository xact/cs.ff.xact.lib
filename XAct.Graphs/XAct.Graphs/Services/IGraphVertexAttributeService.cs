// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a service to return the current user's 
    /// attributes for graph Edges.
    /// </summary>
    public interface IGraphVertexAttributeService : IHasXActLibService
    {
        /// <summary>
        /// Sets the attribute value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        /// <param name="value">The value.</param>
        void SetAttributeValue<T>(GraphElementType graphElementType, Guid elementGuid, string attributeKey, T value);

        /// <summary>
        /// Removes the attribute value.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        void RemoveAttributeValue(GraphElementType graphElementType, Guid elementGuid, string attributeKey);

        /// <summary>
        /// Gets the attribute value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        T GetAttributeValue<T>(GraphElementType graphElementType, Guid elementGuid, string key);

        /// <summary>
        /// Gets the current thread's user's vertex/edge attributes.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The edge/vertex guid.</param>
        /// <param name="forceRefresh">if set to <c>true</c> [force refresh].</param>
        /// <returns>
        /// Returns null if no entities
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">entityType</exception>
        GraphEntityAttribute2[] GetUserAttributes(GraphElementType graphElementType, Guid elementGuid, bool forceRefresh = false);

        /// <summary>
        /// Resets the attributes so that next request loads from a datastore
        /// before returning any found attributes.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="entityGuid">The entity GUID.</param>
        void ResetAttributes(GraphElementType graphElementType, Guid entityGuid);
    }
}