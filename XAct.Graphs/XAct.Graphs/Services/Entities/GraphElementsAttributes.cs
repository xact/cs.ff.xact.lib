// ReSharper disable CheckNamespace
namespace XAct.Graphs.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A single user's cache of the 
    /// <see cref="GraphElementAttributes"/>s for all vertices/edges 
    /// that have been traversed/rendered (and not yet culled).
    /// <para>
    /// Used by an implementation of 
    /// <see cref="IGraphVertexAttributeService"/>.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// In a multiuser website, on request startup, the user's cache is loaded
    /// with only the items currently being displayed. This is a much smaller
    /// set of vertices/edges than all the vertices/edges with user-specific
    /// nodes that could be traversed.
    /// </para>
    /// <para>
    /// The much larger set of user-specific values needed for traversals 
    /// are loaded only before a traversal operation is invoked.
    /// </para>
    /// <para>
    /// This cache is cleared on request end.
    /// </para>
    /// </remarks>
    public class GraphElementsAttributes : Dictionary<Guid, GraphElementAttributes>
    {
    }

}