﻿namespace XAct.Graphs
{
    using System;
    using System.Collections.Generic;


    /// <summary>
    /// A base contract for handling Graph elements.
    /// </summary>
    /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
    /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
    public interface IGraphRepositoryService<TGraphVertex,TGraphEdge> :
        IHasXActLibService
        where TGraphVertex : GraphRepositoryVertex
        where TGraphEdge : GraphRepositoryEdge
    {

        /// <summary>
        /// Gets the <see cref="GraphRepositoryPackageSchema"/>,
        /// which describes a <see cref="GraphPackage{TVertex,TEdge}"/>
        /// </summary>
        /// <param name="uniqueName">The name.</param>
        /// <returns></returns>
        GraphRepositoryPackageSchema GetGraphDescription(string uniqueName);

        /// <summary>
        /// Gets the <see cref="GraphRepositoryPackageSchema"/>.
        /// which describes a <see cref="GraphPackage{TVertex,TEdge}"/>
        /// </summary>
        /// <param name="guid">The GUID.</param>
        /// <returns></returns>
        GraphRepositoryPackageSchema GetGraphDescription(Guid guid);


        /// <summary>
        /// Deletes the <see cref="GraphRepositoryPackageSchema"/>
        /// definition of a 
        /// <see cref="GraphPackage{TVertex,TEdge}"/>
        /// </summary>
        /// <param name="guid">The GUID.</param>
        void DeleteGraphPackageSchema(Guid guid);



        /// <summary>
        /// Saves new or updated <see cref="GraphRepositoryPackageSchema"/>.
        /// which describes a <see cref="GraphPackage{TVertex,TEdge}"/>
        /// </summary>
        /// <param name="graphDefinition">The graph definition.</param>
        void PersistGraphPackageSchema(GraphRepositoryPackageSchema graphDefinition);



        /// <summary>
        /// Gets the graph data.
        /// </summary>
        /// <param name="graphId">The graph identifier.</param>
        /// <returns></returns>
        KeyValue<TGraphVertex[], TGraphEdge[]> GetGraphData(Guid graphId);

        /// <summary>
        /// Determines if there is a Vertex with the given Id.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns></returns>
        bool VertexExists(Guid vertexId);

        /// <summary>
        /// Gets from the datastore the vertex specified by the identifier.
        /// </summary>
        /// <param name="vertexId">The vertex datastore identifier.</param>
        /// <returns></returns>
        TGraphVertex GetVertex(Guid vertexId);

        /// <summary>
        /// Gets from the datastore the vertex specified by the datasource info.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <returns></returns>
        TGraphVertex GetVertex(IHasDataSourceIdentifier dataSourceIdentifier);


        /// <summary>
        /// Gets from the datastore the vertex specified by the identifier.
        /// </summary>
        /// <param name="vertexId">The vertex datastore identifier.</param>
        /// <returns></returns>
        KeyValue<TGraphVertex,TGraphEdge[]> GetVertexAndEdges(Guid vertexId);


        /// <summary>
        /// Gets from the datastore the vertex specified by the identifier.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <returns></returns>
        KeyValue<TGraphVertex, TGraphEdge[]> GetVertexAndEdges(IHasDataSourceIdentifier dataSourceIdentifier);



        /// <summary>
        /// Gets from the datastore the vertex specified by the identifier.
        /// </summary>
        /// <returns></returns>
        void PersistVertex(TGraphVertex vertex);


       /// <summary>
       /// Removes the vertex.
       /// </summary>
       /// <param name="vertexId">The vertex id.</param>
       void RemoveVertex(Guid vertexId);

       /// <summary>
       /// Removes the vertex.
       /// </summary>
       /// <param name="vertexDto">The vertex dto.</param>
       void RemoveVertex(TGraphVertex vertexDto);



       /// <summary>
       /// Gets edges whose target are the given vertex.
       /// </summary>
       /// <param name="vertexId">The vertex id.</param>
       /// <param name="includeVertices">if set to <c>true</c>, include source vertices.</param>
       /// <returns></returns>
       TGraphEdge[] GetInEdges(Guid vertexId, bool includeVertices=false);

       /// <summary>
       /// Gets the edges whose source is the given vertex.
       /// </summary>
       /// <param name="vertexId">The vertex id.</param>
       /// <param name="includeVertices">if set to <c>true</c>, include target vertices.</param>
       /// <returns></returns>
       TGraphEdge[] GetOutEdges(Guid vertexId, bool includeVertices = false);

       /// <summary>
       /// Gets all (incoming and outgoing edges) edges
       /// whose source is the given vertex.
       /// </summary>
       /// <param name="vertexId">The vertex id.</param>
       /// <param name="includeVertices">if set to <c>true</c>, include source and target vertices.</param>
       /// <returns></returns>
       TGraphEdge[] GetEdges(Guid vertexId, bool includeVertices = false);

        /// <summary>
        /// Get the given edge.
        /// </summary>
        /// <param name="edgeId">The edge id.</param>
       /// <param name="includeVertices">if set to <c>true</c>, include source vertices.</param>
       /// <returns></returns>
        TGraphEdge GetEdge(Guid edgeId, bool includeVertices = false);


        /// <summary>
        /// Determines whether there is and record with the given Id.
        /// </summary>
        /// <param name="edgeId">The edge id.</param>
        /// <returns></returns>
        bool EdgeExists(Guid edgeId);


        /// <summary>
        /// Persists the edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        void PersistEdge(TGraphEdge edge);

        /// <summary>
        /// Deletes the specified edge.
        /// <para>
        /// No exception is raised if the edge is not found.
        /// </para>
        /// </summary>
        /// <param name="edgeId">The edge id.</param>
        void DeleteEdge(Guid edgeId);

        /// <summary>
        /// Deletes the specified edge.
        /// </summary>
        /// <param name="edgeId">The edge id.</param>
        void DeleteEdge(TGraphEdge edgeId);



    }
}
