﻿namespace XAct.Graphs
{
    using System;

    /// <summary>
    /// Contract for managing Diagrams made up of Edges and Vertices.
    /// </summary>
    public interface IGraphDiagramService : XAct.Domain.Repositories.ISimpleRepository<GraphDiagram,Guid>, IHasXActLibService
    {

    }
}
