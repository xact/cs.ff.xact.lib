﻿// ReSharper disable CheckNamespace

namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;


    /// <summary>
    /// A service to persist Graph Vertices.
    /// </summary>
    /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
    /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
    /// <internal>
    /// Note that this service only handles persistence of the
    /// Vertices -- not the DataSource they point to.
    ///   </internal>
    public interface IGraphPersistenceService<TGraphVertex, TGraphEdge>
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class, IGraphEdge<TGraphVertex>
    {

        /// <summary>
        /// Gets the graph description.
        /// </summary>
        /// <param name="uniqueName">The name.</param>
        /// <returns></returns>
        GraphRepositoryPackageSchema GetGraph(string uniqueName);

        /// <summary>
        /// Gets the graph description.
        /// </summary>
        /// <param name="guid">The GUID.</param>
        /// <returns></returns>
        GraphRepositoryPackageSchema GetGraph(Guid guid);


        /// <summary>
        /// Deletes the <see cref="GraphRepositoryPackageSchema"/>.
        /// </summary>
        /// <param name="guid">The GUID.</param>
        void DeleteGraph(Guid guid);

        /// <summary>
        /// Persists the specified graph definition.
        /// </summary>
        /// <param name="graphDefinition">The graph definition.</param>
        void PersistGraph(GraphRepositoryPackageSchema graphDefinition);


        /// <summary>
        /// Gets the graph data.
        /// </summary>
        /// <param name="graphId">The graph identifier.</param>
        /// <returns></returns>
        KeyValue<TGraphVertex[], TGraphEdge[]> GetGraphData(Guid graphId);


        /// <summary>
        /// Determine if there is a record for the vertex.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns></returns>
        bool VertexExists(Guid vertexId);




        /// <summary>
        /// Gets the specified vertex.
        /// <para>
        /// Note: Only called by IGraph{V,E} to Complete an incomplete Graph.
        /// </para>
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns></returns>
        TGraphVertex GetVertex(Guid vertexId);



        /// <summary>
        /// Gets the vertex.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <returns></returns>
        TGraphVertex GetVertex(IHasDataSourceIdentifier dataSourceIdentifier);


        /// <summary>
        /// Gets the specified vertex.
        /// <para>
        /// Note: Only called by IGraph{V,E} to Complete an incomplete Graph.
        /// </para>
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns></returns>
        KeyValue<TGraphVertex,TGraphEdge[]> GetVertexAndEdges(Guid vertexId);


        /// <summary>
        /// Gets the vertex.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <returns></returns>
        KeyValue<TGraphVertex,TGraphEdge[]> GetVertexAndEdges(IHasDataSourceIdentifier dataSourceIdentifier);

        /// <summary>
        /// Refreshes a Vertex with current database values.
        /// Used by <c>DefaultBiDirectionalGraph.Prepare</c>
        /// </summary>
        /// <param name="graphVertex"></param>
        void RefreshVertex(TGraphVertex graphVertex);

        /// <summary>
        /// Persists the specified vertex to a persistent storage (on next commit).
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        void PersistVertex(TGraphVertex vertex);

        /// <summary>
        /// Deletes the vertex (on next commit), as well as Edges that connect to it.
        /// </summary>
        /// <param name="graphVertex">The graph vertex.</param>
        void DeleteVertex(TGraphVertex graphVertex);









        /// <summary>
        /// Determine if there is a record for the edge.
        /// </summary>
        /// <param name="edgeId">The edge id.</param>
        /// <returns></returns>
        bool EdgeExists(Guid edgeId);



        /// <summary>
        /// Adds the edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        void PersistEdge(TGraphEdge edge);



    }
}