﻿
namespace XAct.Graphs
{
    using System;

    /// <summary>
    /// Contract for getting Attributes for a single Element
    /// </summary>
    public interface IGraphVertexAttributeRepositoryService : IHasXActLibService
    {

        /// <summary>
        /// Persist the new or existing specific element's attribute value.
        /// </summary>
        /// <param name="graphElementAttribute">The graph vertex attribute.</param>
        void SetAttributeValue(GraphRepositoryVertexAttribute graphElementAttribute);

        /// <summary>
        /// Gets the specific element'sattribute value.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        /// <returns></returns>
        GraphRepositoryVertexAttribute GetAttributeValue(GraphElementType graphElementType, Guid elementGuid, string attributeKey);

        /// <summary>
        /// Removes the specific element's attribute value.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        void RemoveAttributeValue(GraphElementType graphElementType, Guid elementGuid, string attributeKey);

        /// <summary>
        /// Cleares the specific element's attribute values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        void CleareAttributeValues<T>(GraphElementType graphElementType, Guid elementGuid);




        /// <summary>
        /// Gets the current user's attributes for a single edge/vertex.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="vertexGuid">The vertex's GUID.</param>
        /// <returns></returns>
        /// <internal>
        /// Does not need GraphId argument as one cannot get a ElementId without a GraphId
        ///   </internal>
        GraphRepositoryVertexAttribute[] GetSharedAttributes(GraphElementType graphElementType, Guid vertexGuid);


        /// <summary>
        /// Gets the current user's attributes for a single edge/vertex.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="vertexGuid">The vertex's GUID.</param>
        /// <returns></returns>
        /// <internal>
        /// Does not need GraphId argument as one cannot get a ElementId without a GraphId
        ///   </internal>
        GraphRepositoryVertexAttribute[] GetCurrentUserAttributes(GraphElementType graphElementType, Guid vertexGuid);

        /// <summary>
        /// Persists the given <see cref="GraphRepositoryVertexAttribute"/>
        /// on next commit.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        void PersistOnCommit(params GraphRepositoryVertexAttribute[] attributes);
    }

}
