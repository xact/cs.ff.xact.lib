﻿namespace XAct.Graphs.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;

    /// <summary>
    /// Base class to build <see cref="DefaultGraphRepositoryService"/>
    /// </summary>
    /// <typeparam name="TGraphVertex"></typeparam>
    /// <typeparam name="TGraphEdge"></typeparam>
    public abstract class GraphRepositoryServiceBase<TGraphVertex, TGraphEdge> :
        XActLibServiceBase,
        IGraphRepositoryService<TGraphVertex, TGraphEdge>

        where TGraphVertex : GraphRepositoryVertex, new()
        where TGraphEdge : GraphRepositoryEdge, new()
    {
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphRepositoryServiceBase{TGraphVertex, TGraphEdge}" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        protected GraphRepositoryServiceBase(ITracingService tracingService, IRepositoryService repositoryService)
            : base(tracingService)
        {
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Gets the <see cref="GraphRepositoryPackageSchema"/>.
        /// </summary>
        /// <param name="uniqueName">The name.</param>
        /// <returns></returns>
        public GraphRepositoryPackageSchema GetGraphDescription(string uniqueName)
        {
            return _repositoryService.GetSingle<GraphRepositoryPackageSchema>(gd => gd.Name == uniqueName);
        }

        /// <summary>
        /// Gets the <see cref="GraphRepositoryPackageSchema"/>.
        /// </summary>
        /// <param name="guid">The GUID.</param>
        /// <returns></returns>
        public GraphRepositoryPackageSchema GetGraphDescription(Guid guid)
        {
            return _repositoryService.GetSingle<GraphRepositoryPackageSchema>(gd => gd.Id == guid);
        }

        /// <summary>
        /// Deletes the <see cref="GraphRepositoryPackageSchema"/>.
        /// </summary>
        /// <param name="guid">The GUID.</param>
        public void DeleteGraphPackageSchema(Guid guid)
        {
            _repositoryService.DeleteOnCommit<GraphRepositoryPackageSchema>( gd => gd.Id == guid);
        }






        /// <summary>
        /// Persists the specified graph definition.
        /// </summary>
        /// <param name="graphDefinition">The graph definition.</param>
        public void PersistGraphPackageSchema(GraphRepositoryPackageSchema graphDefinition)
        {
            _repositoryService.PersistOnCommit<GraphRepositoryPackageSchema, Guid>(graphDefinition, true);
        }


        /// <summary>
        /// Gets the graph data.
        /// </summary>
        /// <param name="graphId">The graph identifier.</param>
        /// <returns></returns>
        public KeyValue<TGraphVertex[], TGraphEdge[]> GetGraphData(Guid graphId)
        {
           KeyValue<TGraphVertex[], TGraphEdge[]> results = new KeyValue<TGraphVertex[], TGraphEdge[]>();


            results.Key = _repositoryService.GetByFilter<TGraphVertex>(x => x.GraphId == graphId).ToArray();

            Guid[] ids = results.Key.Select(x => x.Id).ToArray();

            results.Value = _repositoryService.GetByFilter<TGraphEdge>(x => ids.Contains(x.SourceFK)||(ids.Contains(x.TargetFK))).ToArray();

            return results;

        }


        /// <summary>
        /// Determines if there is a Vertex with the given Id.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns></returns>
        public bool VertexExists(Guid vertexId)
        {
            vertexId.ValidateIsNotDefault("vertexId");

            return _repositoryService.Contains<TGraphVertex>(v => v.Id == vertexId);
        }


        /// <summary>
        /// Gets the vertex.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns></returns>
        public TGraphVertex GetVertex(Guid vertexId)
        {
            var result =  
                       _repositoryService.GetSingle<TGraphVertex>(v => v.Id == vertexId);
            return result;
        }

        /// <summary>
        /// Gets the vertex.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <returns></returns>
        public TGraphVertex GetVertex(IHasDataSourceIdentifier dataSourceIdentifier)
        {
            var result =
                _repositoryService.GetSingle<TGraphVertex>(
                    v =>
                    ((v.DataSourceId == dataSourceIdentifier.DataSourceId) &&
                     (v.DataSourceSerializedIdentities == dataSourceIdentifier.DataSourceSerializedIdentities)));



            return result;

        }


        /// <summary>
        /// Gets from the datastore the vertex specified by the identifier.
        /// </summary>
        /// <param name="vertexId">The vertex datastore identifier.</param>
        /// <returns></returns>
        public KeyValue<TGraphVertex, TGraphEdge[]> GetVertexAndEdges(Guid vertexId)
        {
            var results = new KeyValue<TGraphVertex, TGraphEdge[]>();
            var vertex = GetVertex(vertexId);
            if (vertex != null)
            {
                results.Key = vertex;
                results.Value = GetEdges(vertex.Id,false);
            }
            return results;
        }


        /// <summary>
        /// Gets from the datastore the vertex specified by the identifier.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <returns></returns>
        public KeyValue<TGraphVertex, TGraphEdge[]> GetVertexAndEdges(IHasDataSourceIdentifier dataSourceIdentifier)
        {
            var results = new KeyValue<TGraphVertex, TGraphEdge[]>();
            var vertex = GetVertex(dataSourceIdentifier);
            if (vertex != null)
            {
                results.Key = vertex;
                results.Value = GetEdges(vertex.Id, false);
            }
            return results;
            
        }


        /// <summary>
        /// Persists the vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public void PersistVertex(TGraphVertex vertex)
        {
            if (vertex.GraphId == Guid.Empty)
            {
                throw new ArgumentException("vertex.GraphId");
            }

            if (vertex.DataSourceId == Guid.Empty)
            {
                //throw new ArgumentException("vertex.DataSoruceId");
                if (!vertex.DataSourceSerializedIdentities.IsNullOrEmpty())
                {
                    throw new ArgumentException("vertex.DataSoruceId");
                }
            }
            else
            {
                if (vertex.DataSourceSerializedIdentities.IsNullOrEmpty())
                {
                    throw new ArgumentException("vertex.DataSoruceSerializedIdentities");
                }
            }

            _repositoryService.PersistOnCommit<TGraphVertex, Guid>(vertex, true);

        }

        /// <summary>
        /// Removes the vertex.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        public void RemoveVertex(Guid vertexId)
        {
            _repositoryService.DeleteOnCommit<TGraphVertex,Guid>(vertexId);

        }

        /// <summary>
        /// Removes the vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public void RemoveVertex(TGraphVertex vertex)
        {
            _repositoryService.DeleteOnCommit(vertex);
        }


        /// <summary>
        /// Gets edges whose target are the given vertex.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <param name="includeVertices">if set to <c>true</c>, include source vertices.</param>
        /// <returns></returns>
        public TGraphEdge[] GetInEdges(Guid vertexId, bool includeVertices = false)
        {
            return includeVertices
                       ? _repositoryService.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId).ToArray()
                       : _repositoryService.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId, 
                       new IncludeSpecification<TGraphEdge>(e => e.Source)).ToArray();
        }


        /// <summary>
        /// Gets edges whose source are the given vertex.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <param name="includeVertices">if set to <c>true</c>, include target vertices.</param>
        /// <returns></returns>
        public TGraphEdge[] GetOutEdges(Guid vertexId, bool includeVertices = false)
        {
            return includeVertices
                       ? _repositoryService.GetByFilter<TGraphEdge>(e => e.SourceFK == vertexId).ToArray()
                       : _repositoryService.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId, 
                       new IncludeSpecification<TGraphEdge>(e => e.Target)).ToArray();
        }

        /// <summary>
        /// Gets all (incoming and outgoing edges) edges
        /// whose source is the given vertex.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <param name="includeVertices">if set to <c>true</c>, include source and target vertices.</param>
        /// <returns></returns>
        public TGraphEdge[] GetEdges(Guid vertexId, bool includeVertices = false)
        {
            return includeVertices
                       ? _repositoryService.GetByFilter<TGraphEdge>(e => e.SourceFK == vertexId || e.TargetFK == vertexId).ToArray()
                       : _repositoryService.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId, new IncludeSpecification<TGraphEdge>(e => e.Target, e => e.Source)).ToArray();
        }

        /// <summary>
        /// Get the given edge.
        /// </summary>
        /// <param name="edgeId">The edge id.</param>
        /// <param name="includeVertices">if set to <c>true</c>, include source vertices.</param>
        /// <returns></returns>
        public TGraphEdge GetEdge(Guid edgeId, bool includeVertices = false)
        {
            return includeVertices
                       ? _repositoryService.GetSingle<TGraphEdge>(e => e.Id == edgeId)
                       : _repositoryService.GetSingle<TGraphEdge>(e => e.Id == edgeId, new IncludeSpecification<TGraphEdge>(e => e.Target, e => e.Source));
        }

        /// <summary>
        /// Determine if Edge record exists.
        /// </summary>
        /// <param name="edgeId">The edge identifier.</param>
        /// <returns></returns>
        public bool EdgeExists(Guid edgeId)
        {
            edgeId.ValidateIsNotDefault("edgeId");

            return _repositoryService.Contains<TGraphEdge>(e => e.Id == edgeId);
        }


        /// <summary>
        /// Persists the edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        public void PersistEdge(TGraphEdge edge)
        {
            //Validate:
            edge.SourceFK.ValidateIsNotDefault("edge.SourceFK");
            edge.TargetFK.ValidateIsNotDefault("edge.TargetFK");

            //Guid graphId = Guid.Empty;
            //if (edge.Source != null)
            //{
            //    graphId = edge.Source.GraphId;
            //    if (edge.Target != null)
            //    {
            //        if (edge.Target.GraphId != graphId)
            //        {
            //            throw new ArgumentException("Edge is binding Vertices of two different Graphs.");
            //        }
            //    }
            //}

            _repositoryService.PersistOnCommit<TGraphEdge,Guid>(edge, true);
        }



        /// <summary>
        /// Deletes the specified edge.
        /// <para>
        /// No exception is raised if the edge is not found.
        /// </para>
        /// </summary>
        /// <param name="edgeId">The edge id.</param>
        public void DeleteEdge(Guid edgeId)
        {
            _repositoryService.DeleteOnCommit<TGraphEdge,Guid>(edgeId);
        }

        /// <summary>
        /// Deletes the specified edge.
        /// <para>
        /// No exception is raised if the edge is not found.
        /// </para>
        /// </summary>
        /// <param name="edge">The edge.</param>
        public void DeleteEdge(TGraphEdge edge)
        {
            _repositoryService.DeleteOnCommit(edge);
        }

        /// <summary>
        /// Gets from the datastore the vertex specified by the identifier.
        /// </summary>
        /// <param name="graphId">The graph id.</param>
        /// <param name="vertexId">The vertex datastore identifier.</param>
        /// <returns></returns>
        public TGraphVertex GetVertex(Guid graphId, Guid vertexId)
        {
            //this.GetSingle<GraphVertex>(v => v.Id == vertexId, new IncludeSpecification<GraphVertex>(v => v.InEdges));

            TGraphVertex result = 
                _repositoryService.GetSingle<TGraphVertex>(
                v => (
                    (v.GraphId == graphId) 
                    && 
                    (v.Id == vertexId)), 
                    new IncludeSpecification("InEdges", "OutEdges"));

            return result;
        }

    }
}