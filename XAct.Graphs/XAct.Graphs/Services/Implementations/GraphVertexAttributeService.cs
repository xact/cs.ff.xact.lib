﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;
    using XAct.State;


    /// <summary>
    /// An implementation of 
    /// <see cref="IGraphVertexAttributeService"/>,
    /// which is a <see cref="IGraphVertexAttributeService"/>
    /// contract.
    /// </summary>
    public class GraphVertexAttributeService : XActLibServiceBase, IGraphVertexAttributeService
    {

        /// <summary>
        /// The session key for vertex attributes.
        /// </summary>
        public const string CacheVertexKey = "_Graph.VertexAttributes";
        /// <summary>
        /// The session key for edge attributes.
        /// </summary>
        public const string CacheEdgeKey = "_Graph.EdgeAttributes";

        private readonly IDateTimeService _dateTimeService;

        private readonly IPrincipalService _principalService;
        private readonly IContextStateService _contextStateService;

        private readonly IGraphVertexAttributeRepositoryService _graphVertexAttributeRepositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphVertexAttributeService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="contextStateService">The context state service.</param>
        /// <param name="graphVertexAttributeRepositoryService">The graph repository service.</param>
        public GraphVertexAttributeService(
            ITracingService tracingService, 
            IDateTimeService dateTimeService,
            IPrincipalService principalService,
            IContextStateService contextStateService,
            IGraphVertexAttributeRepositoryService graphVertexAttributeRepositoryService):base(tracingService)
        {
            _dateTimeService = dateTimeService;
            _principalService = principalService;
            _contextStateService = contextStateService;
            _graphVertexAttributeRepositoryService = graphVertexAttributeRepositoryService;
        }

        /// <summary>
        /// Gets the attribute value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public T GetAttributeValue<T>(GraphElementType graphElementType, Guid elementGuid, string key)
        {
            //Get all attributes for this entity:
            GraphEntityAttribute2[] attributes = GetUserAttributes(graphElementType, elementGuid);

            //Do we have one with this key?
            GraphEntityAttribute2 result = attributes.FirstOrDefault(a => a.Key == key);

            //If so, return it, in the expected type -- or return null:
            return (result != null) ? result.Value.ConvertTo<T>() : default(T);
        }



        /// <summary>
        /// Sets the singular attribute value.
        /// <para>
        /// Note that the value will be serialized in such a manner 
        /// that it can be subsequently retrieved as a typed variable, 
        /// rather than just <c>object</c> or <c>string</c>.
        /// </para>.
        /// </summary>
        /// <internal>
        /// Note that all updates to Attributes will be via this service,
        /// one at a time, rather than through a collection of attributes.
        /// <para>
        /// This is so that the <c>GetAttributeValue</c> 
        /// can be invested with enough logic to fall through to a 
        /// Base (general value) Service.
        /// </para>
        /// </internal>
        /// <typeparam name="T"></typeparam>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        /// <param name="value">The value.</param>
        public void SetAttributeValue<T>(GraphElementType graphElementType, Guid elementGuid, string attributeKey, T value)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Setting Attribute");

            ////If the value is null/0, we might as well remove it:
            ////Maybe not a good idea, as 0 could be a valid value, and if null
            ////will return base value, which might not be same as null...
            //if (value.IsDefault())
            //{
            //    RemoveAttributeValue(graphElementType,elementGuid,attributeKey);
            //    return;
            //}
        

        //Get all attributes for this entity:
            GraphElementAttributes attributesPkg = GraphElementUserAttributesCache(elementGuid);
            GraphEntityAttribute2[] attributes = attributesPkg.Attributes;

            //Persist it:
            GraphRepositoryVertexAttribute graphElementAttributeDto =
                _graphVertexAttributeRepositoryService.GetAttributeValue(graphElementType, elementGuid, attributeKey);

            if (graphElementAttributeDto == null)
            {
                graphElementAttributeDto = new GraphRepositoryVertexAttribute();
                graphElementAttributeDto.UserIdentifier = _principalService.Principal.Identity.Name;
                graphElementAttributeDto.ElementType = graphElementType;
                graphElementAttributeDto.ElementId = elementGuid;
                
            }

            //Use extension method to convert value to the 
            //3 target serialization properties:
            graphElementAttributeDto.SerializeValue(value);

            //Use repo to persist changes:
            _graphVertexAttributeRepositoryService.SetAttributeValue(graphElementAttributeDto);

            //Now that we have saved the object, we can 
            //replace the element in mem:
            int pos = attributes.IndexOf(a => a.Key == attributeKey);
            if (pos > -1)
            {
                //Exists in list, so replace it:
                attributes[pos] = new GraphEntityAttribute2(attributes[pos].Key, value);
            }else
            {
                //it's new value so append it:
                List<GraphEntityAttribute2> r2 = new List<GraphEntityAttribute2>(attributes);
                r2.Add(new GraphEntityAttribute2(attributeKey,value));
// ReSharper disable RedundantAssignment
                attributesPkg.Attributes = attributes = r2.ToArray();
// ReSharper restore RedundantAssignment
            }

            //Done. A bit graceless...but...
        }



        /// <summary>
        /// Removes the attribute value.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        public void RemoveAttributeValue(GraphElementType graphElementType, Guid elementGuid, string attributeKey)
        {
            //Get all attributes for this entity:
            GraphElementAttributes attributesPkg = GraphElementUserAttributesCache(elementGuid);
            GraphEntityAttribute2[] attributes = attributesPkg.Attributes;


            //Persist it:
            _graphVertexAttributeRepositoryService.RemoveAttributeValue(graphElementType, elementGuid, attributeKey);

            //Then replace the element in mem:
            int pos = attributes.IndexOf(a => a.Key == attributeKey);
            if (pos > -1)
            {
                List<GraphEntityAttribute2> r2 = new List<GraphEntityAttribute2>(attributes);
                r2.RemoveAt(pos);
                // ReSharper disable RedundantAssignment
                attributesPkg.Attributes = attributes = r2.ToArray();
                // ReSharper restore RedundantAssignment
            }
            //else
            //{
            //    //not there, therefore nothing to remove.
            //}
            //Done. A bit graceless...but...


        }

        /// <summary>
        /// Resets the attributes so that next request loads from a datastore
        /// before returning any found attributes.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="entityGuid">The entity GUID.</param>
        public void ResetAttributes(GraphElementType graphElementType, Guid entityGuid)
        {
            GraphElementUserAttributesCache(entityGuid).LastCheckedUtc = null;
        }

        //----------------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------


        /// <summary>
        /// Gets the current thread's user's vertex/edge attributes.
        /// <para>
        /// Note that we do *NOT* ever return the list. This service is the only 
        /// means to getting to a attribute value (via <c>GetAttribute</c>)
        /// so that it can given enough logic to fall back to a base/general/all users
        /// value.
        /// </para>
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The edge/vertex guid.</param>
        /// <param name="forceRefresh">if set to <c>true</c> [force refresh].</param>
        /// <returns>
        /// Returns null if no entities
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">entityType</exception>
        public GraphEntityAttribute2[] GetUserAttributes(GraphElementType graphElementType, Guid elementGuid, bool forceRefresh = false)
        {

            //From the Request context, get the cache of values of this single element:
            GraphElementAttributes elementAttributes = GraphElementUserAttributesCache(elementGuid);


            //If the element's cache is empty, stale, or forced, refresh from db:
            if ((forceRefresh)||(!elementAttributes.LastCheckedUtc.HasValue)||(_dateTimeService.NowUTC.Subtract(elementAttributes.LastCheckedUtc.Value)>TimeSpan.FromMinutes(5)))
            {
                //Time to recheck, by calling the 

                //Hit the db, but then convert the EF/Db specific property to 
                //a local entity, with less properties (we need to kep them small
                //due to volumen in memory.
                //For example, we no longer need the guid of the owner (it's not a navigation entity)
                //nor the id of the user, nor, etc... We can shrink it.
                List<GraphEntityAttribute2> r = new List<GraphEntityAttribute2>();
                
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (GraphRepositoryVertexAttribute graphVertexAttribute in _graphVertexAttributeRepositoryService.GetCurrentUserAttributes(graphElementType, elementGuid))
                {
                    //Convert and build up list:
                    r.Add(graphVertexAttribute.Map());   
                }

                // ReSharper restore LoopCanBeConvertedToQuery
                elementAttributes.Attributes = r.ToArray();

                //Set flag for next check:
                elementAttributes.LastCheckedUtc = _dateTimeService.NowUTC;
            }

            //Done for this Vertex:
            return elementAttributes.Attributes;
        }








        /// <summary>
        /// Gets the collection of attributes for just one elemement:
        /// </summary>
        /// <param name="elementGuid"></param>
        /// <returns></returns>
        private GraphElementAttributes GraphElementUserAttributesCache(Guid elementGuid)
        {

            //Get this user's cache of the attributes of all elements:
            GraphElementsAttributes cache = GetCurrentUserGraphElementAttributes();

            //Within that cache, find the attributes for just the current element:
            GraphElementAttributes attributes;
            if (!cache.TryGetValue(elementGuid, out attributes))
            {
                //if not found, create a new object,
                //but don't set the LastChecked flag, as we havn't done that...
                cache[elementGuid] = new GraphElementAttributes();
            }

            return attributes;
        }

        /// <summary>
        /// Gets the collection of all known attributes for all vertices of the current user:
        /// </summary>
        /// <returns></returns>
        private GraphElementsAttributes GetCurrentUserGraphElementAttributes()
        {
            //Get a collection of *all* vertices for this user:
            GraphElementsAttributes cache = (GraphElementsAttributes)_contextStateService.GetMemberValue(CacheVertexKey);

            //If first time asked for during this request, 
            //have to create it:
            if (cache == null)
            {
                cache = new GraphElementsAttributes();
                _contextStateService.SetMemberValue(CacheVertexKey, cache);
            }

            return cache;
        }
    }
}
