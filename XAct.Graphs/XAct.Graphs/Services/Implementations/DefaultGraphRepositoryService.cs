﻿//TRACING : http://efwrappers.codeplex.com/

// ReSharper disable CheckNamespace
namespace XAct.Graphs.Services.Implementations
// ReSharper restore CheckNamespace
{
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof (IGraphRepositoryService<GraphRepositoryVertex, GraphRepositoryEdge>),BindingLifetimeType.SingletonScope, Priority.Low)]
    public class DefaultGraphRepositoryService : GraphRepositoryServiceBase<GraphRepositoryVertex, GraphRepositoryEdge>,
                                          IDefaultGraphRepositoryService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultGraphRepositoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public DefaultGraphRepositoryService(ITracingService tracingService, IRepositoryService repositoryService)
            : base(tracingService, repositoryService)
        {

        }
    }
}
