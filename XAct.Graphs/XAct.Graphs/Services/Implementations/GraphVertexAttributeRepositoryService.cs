﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Linq;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class GraphVertexAttributeRepositoryService :
        IGraphVertexAttributeRepositoryService
    {
        private readonly IPrincipalService _principalService;
        private readonly IRepositoryService _repositoryService;


        /// <summary>
        /// Initializes a new instance of the <see cref="GraphVertexAttributeRepositoryService" /> class.
        /// </summary>
        /// <param name="principalService">The principal service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public GraphVertexAttributeRepositoryService(
            IPrincipalService principalService,
            IRepositoryService repositoryService)
        {
            _principalService = principalService;
            _repositoryService = repositoryService;
        }


        /// <summary>
        /// Gets the attribute value.
        /// </summary>
        /// <param name="currentUser">if set to <c>true</c> [current user].</param>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        /// <returns></returns>
        public GraphRepositoryVertexAttribute GetAttributeValue(bool currentUser, GraphElementType graphElementType, Guid elementGuid, string attributeKey)
        {
            if (currentUser)
            {
                return _repositoryService.GetSingle<GraphRepositoryVertexAttribute>(
                    ga =>
                        ga.UserIdentifier == _principalService.CurrentIdentityIdentifier
                        && ga.ElementType == graphElementType
                        && ga.Id == elementGuid
                        && ga.Key == attributeKey);
            }
                return _repositoryService.GetSingle<GraphRepositoryVertexAttribute>(
                    ga =>
                        ga.ElementType == graphElementType
                        && ga.Id == elementGuid
                        && ga.Key == attributeKey);
        }


        /// <summary>
        /// Removes the specific element's attribute value.
        /// </summary>
        /// <param name="currentUser">if set to <c>true</c> [current user].</param>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        public void RemoveAttributeValue(bool currentUser, GraphElementType graphElementType, Guid elementGuid, string attributeKey)
        {
            if (currentUser)
            {
                _repositoryService.DeleteOnCommit<GraphRepositoryVertexAttribute>(
                    ga => 
                        ga.ElementType == graphElementType 
                        && ga.UserIdentifier == _principalService.CurrentIdentityIdentifier
                        && ga.Id == elementGuid 
                        && ga.Key == attributeKey);
            }else
            {
                _repositoryService.DeleteOnCommit<GraphRepositoryVertexAttribute>(
                    ga =>
                        ga.ElementType == graphElementType
                        && ga.Id == elementGuid
                        && ga.Key == attributeKey);
                
            }
        }

        /// <summary>
        /// Cleares the specific element's attribute values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="currentUser">if set to <c>true</c> [current user].</param>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        public void CleareAttributeValues<T>(bool currentUser, GraphElementType graphElementType, Guid elementGuid)
        {
            if (currentUser)
            {
                _repositoryService.DeleteOnCommit<GraphRepositoryVertexAttribute>(
                    ga => 
                        ga.ElementType == graphElementType
                        && ga.UserIdentifier == _principalService.CurrentIdentityIdentifier
                        && ga.Id == elementGuid);
            }
            else
            {
                _repositoryService.DeleteOnCommit<GraphRepositoryVertexAttribute>(
                    ga => 
                        ga.ElementType == graphElementType 
                        && ga.Id == elementGuid);
                
            }
        }








        /// <summary>
        /// Gets the current user's attributes for a single edge/vertex.
        /// </summary>
        /// <param name="currentUser">if set to <c>true</c> [current user].</param>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The vertex's GUID.</param>
        /// <returns></returns>
        /// <internal>
        /// Does not need GraphId argument as one cannot get a ElementId without a GraphId
        ///   </internal>
        public GraphRepositoryVertexAttribute[] GetSharedAttributes(bool currentUser, GraphElementType graphElementType, Guid elementGuid)
        {
            GraphRepositoryVertexAttribute[] result;

            if (currentUser)
            {

                result =
                    _repositoryService.GetByFilter<GraphRepositoryVertexAttribute>(
                        a =>
                        (a.ElementType == graphElementType) &&
                        (a.ElementId == elementGuid) &&
                        (a.UserIdentifier == null)
                        ).ToArray();
                return result;
            }
            result =
                _repositoryService.GetByFilter<GraphRepositoryVertexAttribute>(
                    a =>
                    (a.ElementType == graphElementType) &&
                    (a.ElementId == elementGuid) &&
                    (a.UserIdentifier == null)
                    ).ToArray();

            return result;
        }



        /// <summary>
        /// Gets the current user's attributes for a single edge/vertex.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The vertex's GUID.</param>
        /// <returns></returns>
        /// <internal>
        /// Does not need GraphId argument as one cannot get a ElementId without a GraphId
        ///   </internal>
        public GraphRepositoryVertexAttribute[] GetCurrentUserAttributes(GraphElementType graphElementType, Guid elementGuid)
        {
            GraphRepositoryVertexAttribute[] result =
                _repositoryService.GetByFilter<GraphRepositoryVertexAttribute>(
                    a =>
                    (a.ElementType == graphElementType) &&
                    (a.ElementId == elementGuid) &&
                    (a.UserIdentifier == _principalService.Principal.Identity.Name)).ToArray();

            return result;
        }


        /// <summary>
        /// Persists the given <see cref="GraphRepositoryVertexAttribute"/>
        /// on next commit.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        public void PersistOnCommit(params GraphRepositoryVertexAttribute[] attributes)
        {

            foreach (GraphRepositoryVertexAttribute attribute in attributes)
            {
// ReSharper disable RedundantTypeArgumentsOfMethod
                _repositoryService.PersistOnCommit<GraphRepositoryVertexAttribute>(attribute, a=>a.Id == Guid.Empty);
// ReSharper restore RedundantTypeArgumentsOfMethod
            }
        }










        /// <summary>
        /// Persist the new or existing specific element's attribute value.
        /// </summary>
        /// <param name="graphElementAttribute">The graph vertex attribute.</param>
        public void SetAttributeValue(GraphRepositoryVertexAttribute graphElementAttribute)
        {
            _repositoryService.PersistOnCommit(graphElementAttribute, a => a.Id == Guid.Empty);
        }

        /// <summary>
        /// Gets the specific element'sattribute value.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public GraphRepositoryVertexAttribute GetAttributeValue(GraphElementType graphElementType, Guid elementGuid, string attributeKey)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes the specific element's attribute value.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <param name="attributeKey">The attribute key.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void RemoveAttributeValue(GraphElementType graphElementType, Guid elementGuid, string attributeKey)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Cleares the specific element's attribute values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="elementGuid">The element GUID.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CleareAttributeValues<T>(GraphElementType graphElementType, Guid elementGuid)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the current user's attributes for a single edge/vertex.
        /// </summary>
        /// <param name="graphElementType">Type of the graph element.</param>
        /// <param name="vertexGuid">The vertex's GUID.</param>
        /// <returns></returns>
        /// <internal>
        /// Does not need GraphId argument as one cannot get a ElementId without a GraphId
        ///   </internal>
        /// <exception cref="System.NotImplementedException"></exception>
        public GraphRepositoryVertexAttribute[] GetSharedAttributes(GraphElementType graphElementType, Guid vertexGuid)
        {
            throw new NotImplementedException();
        }

    }
}
