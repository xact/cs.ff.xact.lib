﻿namespace XAct.Graphs.Implementations
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Default implementation of the
    /// <see cref="IDefaultGraphPersistenceService"/> (which is a
    /// <see cref="IGraphPersistenceService{TGraphVertex, TGraphEdge}"/>)
    /// contract.
    /// </summary>
    /// <internal>
    /// Services have to be registered both with and 
    /// without generic typeparams, as users might be using the non 
    /// generic interface, but the other parts of the system
    /// will be invoking the service using generic typeparams.
    /// </internal>

    [DefaultBindingImplementation(typeof(IGraphPersistenceService< GraphVertex, IGraphEdge<GraphVertex>>),BindingLifetimeType.SingletonScope, Priority.Low)]
    public class DefaultGraphPersistenceService : GraphPersistenceServiceBase<GraphVertex, GraphEdge>, IDefaultGraphPersistenceService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultGraphPersistenceService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="graphRepositoryService">The graph repository service.</param>
        public DefaultGraphPersistenceService(ITracingService tracingService, IDefaultGraphRepositoryService graphRepositoryService)
            :base(tracingService, graphRepositoryService)
        {
            
        }
    }
}