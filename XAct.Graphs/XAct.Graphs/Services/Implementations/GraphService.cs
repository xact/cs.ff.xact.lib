﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IGraphService" />
    /// to create Bidirectional graph of objects.
    /// </summary>
    public class GraphService : XActLibServiceBase, IGraphService

    {
        private readonly IDefaultGraphPersistenceService _persistenceService;
        //private readonly IDistributedIdService _distributedIdService;

        //Cache of GraphPackageSchema.Guid to IGraph
        private readonly List<IHasIdReadOnly<Guid>> _cache = new List<IHasIdReadOnly<Guid>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="persistenceService">The persistence service.</param>
        public GraphService(ITracingService tracingService, IDefaultGraphPersistenceService persistenceService):base(tracingService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            _persistenceService = persistenceService;
          //  _distributedIdService = distributedIdService;
        }


        /// <summary>
        /// Creates a new Graph object.
        /// </summary>
        /// <returns></returns>
        public IGraph<TGraphVertex, TGraphEdge> CreateGraph<TGraphVertex, TGraphEdge>(Guid graphId, GraphType graphType = GraphType.Bidirectional)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new()
        {

            throw new NotImplementedException();

            //return new BidirectionalGraph<TGraphVertex, TGraphEdge>(
            //    _tracingService,

            //    graphId);
        }


        /// <summary>
        /// Creates the create.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="graphType">Type of the graph.</param>
        /// <returns></returns>
        public IGraphPackage<TGraphVertex, TGraphEdge> CreateGraph<TGraphVertex, TGraphEdge>(string name, string description, GraphType graphType = GraphType.Bidirectional)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new()
        {
            GraphRepositoryPackageSchema graphPackageSchema = _persistenceService.GetGraph(name);

            if (graphPackageSchema != null)
            {
// ReSharper disable LocalizableElement
// ReSharper disable NotResolvedInText
                throw new ArgumentException("name","name already used.");
// ReSharper restore NotResolvedInText
// ReSharper restore LocalizableElement
            }

            GraphRepositoryPackageSchema graphPackageSChema = 
                new GraphRepositoryPackageSchema
                    {
                        Name = name, 
                        Description = description
                    };

            //Now that it exists
            _persistenceService.PersistGraph(graphPackageSChema);


            var result = GetGraphById<TGraphVertex, TGraphEdge>(graphPackageSChema.Id);
            return result;
        }


        /// <summary>
        /// Creates and registers a new Graph.
        /// </summary>
        /// <typeparam name="TGraphVertex"></typeparam>
        /// <typeparam name="TGraphEdge"></typeparam>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public IGraphPackage<TGraphVertex, TGraphEdge> GetGraphByName<TGraphVertex, TGraphEdge>(string name)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new()
        {
            //Get db record describing Graphpackage:
            GraphRepositoryPackageSchema graphPackageSchema = _persistenceService.GetGraph(name);

            if (graphPackageSchema == null)
            {
// ReSharper disable LocalizableElement
                throw new ArgumentOutOfRangeException("name", "Package not found in Datastore.");
// ReSharper restore LocalizableElement
            }

            IHasIdReadOnly<Guid> check = _cache.SingleOrDefault(x => x.Id == graphPackageSchema.Id);

            IGraphPackage<TGraphVertex, TGraphEdge> result;

            if (check != null)
            {
                result = (IGraphPackage<TGraphVertex, TGraphEdge>) check;

                //Survived...return previously builtup graph:
                return result;
            }

            //Found in db a record, but graph is not yet in mem:

            IGraph<TGraphVertex, TGraphEdge> graph =
// ReSharper disable RedundantArgumentDefaultValue
                CreateGraph<TGraphVertex, TGraphEdge>(graphPackageSchema.Id, GraphType.Bidirectional);
// ReSharper restore RedundantArgumentDefaultValue

            //Combine as a package:
            result = new GraphPackage<TGraphVertex, TGraphEdge>(graphPackageSchema, graph);


            //Cache:
            _cache.Add(result);

            return result;

        }

        /// <summary>
        /// Gets the graph.
        /// </summary>
        /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
        /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
        /// <param name="graphId">The graph id.</param>
        /// <returns></returns>
        public IGraphPackage<TGraphVertex, TGraphEdge> GetGraphById<TGraphVertex, TGraphEdge>(Guid graphId)
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class,IGraphEdge<TGraphVertex>, new()
        {
            //Get db record describing Graphpackage:
            GraphRepositoryPackageSchema graphPackageSchema = _persistenceService.GetGraph(graphId);

            if (graphPackageSchema == null)
            {
// ReSharper disable LocalizableElement
                throw new ArgumentOutOfRangeException("graphId", "Package not found in Datastore.");
// ReSharper restore LocalizableElement
            }

            IHasIdReadOnly<Guid> check = _cache.SingleOrDefault(x => x.Id == graphPackageSchema.Id);

            IGraphPackage<TGraphVertex, TGraphEdge> result;
            if (check != null)
            {
                result = (IGraphPackage<TGraphVertex, TGraphEdge>)check;
                
                //Survived...return previously builtup graph:
                return result;
            }

            //Found in db a record, but graph is not yet in mem:

            IGraph<TGraphVertex, TGraphEdge> graph =
// ReSharper disable RedundantArgumentDefaultValue
                CreateGraph<TGraphVertex, TGraphEdge>(graphPackageSchema.Id, GraphType.Bidirectional);
// ReSharper restore RedundantArgumentDefaultValue

            //Combine as a package:
            result = new GraphPackage<TGraphVertex, TGraphEdge>(graphPackageSchema, graph);


            //Cache:
            _cache.Add(result);

            return result;
        }



        /// <summary>
        /// Delete's an existing <see cref="BidirectionalGraph{TGraphVertex,TGraphEdge}"/>
        /// in which to persist <see cref="IGraphVertex"/> and <see cref="IGraphEdge{TIGraphVertex}"/>
        /// entities.
        /// </summary>
        /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
        /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
        /// <param name="graphId">The graph's unique id.</param>
        /// <returns></returns>
        public void DeleteGraph<TGraphVertex, TGraphEdge>(Guid graphId)
            where TGraphVertex : class, IGraphVertex, new()
            where TGraphEdge : class,IGraphEdge<TGraphVertex>, new()
        {
            //Get db record describing Graphpackage:
            GraphRepositoryPackageSchema graphPackageSchema = _persistenceService.GetGraph(graphId);
            

            _persistenceService.DeleteGraph(graphId);

            IHasIdReadOnly<Guid> check = _cache.SingleOrDefault(x => x.Id == graphPackageSchema.Id);
            
            if (check != null)
            {
                _cache.Remove(check);
            }


        }





    }
}
