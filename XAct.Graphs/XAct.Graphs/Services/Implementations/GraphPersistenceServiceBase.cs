﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;


    /// <summary>
    /// An implementation of the
    /// <see cref="IGraphPersistenceService{TGraphVertex,TGraphEdge}" />
    /// contract.
    /// </summary>
    /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
    /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
    public abstract class GraphPersistenceServiceBase<TGraphVertex, TGraphEdge> : 
        IGraphPersistenceService<TGraphVertex, TGraphEdge>
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class, IGraphEdge<TGraphVertex>,new()
    {
        private readonly ITracingService _tracingService;
        private readonly IDefaultGraphRepositoryService _graphRepositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphPersistenceServiceBase&lt;TGraphVertex, TGraphEdge&gt;"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="graphRepositoryService">The graph repository service.</param>
        public GraphPersistenceServiceBase(ITracingService tracingService, IDefaultGraphRepositoryService graphRepositoryService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            graphRepositoryService.ValidateIsNotDefault("graphRepositoryService");

            _tracingService = tracingService;
            _graphRepositoryService = graphRepositoryService;

        }


        /// <summary>
        /// Gets the graph description.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public GraphRepositoryPackageSchema GetGraph(string name)
        {
            return _graphRepositoryService.GetGraphDescription(name);
        }

        /// <summary>
        /// Gets the graph description.
        /// </summary>
        /// <param name="guid">The GUID.</param>
        /// <returns></returns>
        public GraphRepositoryPackageSchema GetGraph(Guid guid)
        {
            return _graphRepositoryService.GetGraphDescription(guid);
        }

        /// <summary>
        /// Deletes the graph package.
        /// </summary>
        /// <param name="guid">The GUID.</param>
        public void DeleteGraph(Guid guid)
        {
            _graphRepositoryService.DeleteGraphPackageSchema(guid);
        }

        /// <summary>
        /// Persists the specified graph definition.
        /// </summary>
        /// <param name="graphDefinition">The graph definition.</param>
        public void PersistGraph(GraphRepositoryPackageSchema graphDefinition)
        {
            
            _graphRepositoryService.PersistGraphPackageSchema(graphDefinition);

        }













        /// <summary>
        /// Gets the graph data.
        /// </summary>
        /// <param name="graphId">The graph identifier.</param>
        /// <returns></returns>
        public KeyValue<TGraphVertex[], TGraphEdge[]> GetGraphData(Guid graphId)
        {
            KeyValue<TGraphVertex[], TGraphEdge[]> results = new KeyValue<TGraphVertex[], TGraphEdge[]>();
 
            KeyValue<GraphRepositoryVertex[], GraphRepositoryEdge[]> r = _graphRepositoryService.GetGraphData(graphId);

            results.Key = (r.Key.Select(MapTo)).ToArray();
            results.Value = (r.Value.Select(MapTo)).ToArray();

            return results;
        }




        /// <summary>
        /// Determine if there is a record for the vertex.
        /// </summary>
        /// <param name="vertexId">The vertex id.</param>
        /// <returns></returns>
        public bool VertexExists(Guid vertexId)
        {
            return _graphRepositoryService.VertexExists(vertexId);
        }



        /// <summary>
        /// Gets the specified vertex.
        /// <para>
        /// Note: Only called by IGraph{V,E} to Complete an incomplete Graph.
        /// </para>
        /// </summary>
        /// <param name="vertexId">The id of the vertex.</param>
        /// <returns></returns>
        public TGraphVertex GetVertex(Guid vertexId)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Retrieving Vertex (Id='{0}')", vertexId);

            GraphRepositoryVertex graphRepositoryVertex=
                _graphRepositoryService.GetVertex(vertexId);

            //Note: as the result is only a vertex -- without edges -- 
            //I'm not benefiting from the older way of getting edgs at the same time.
            var result = MapTo(graphRepositoryVertex);

            return result;
        }


        /// <summary>
        /// Gets the vertex.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <returns></returns>
        public TGraphVertex GetVertex(IHasDataSourceIdentifier dataSourceIdentifier)
        {
            GraphRepositoryVertex graphRepositoryVertex =
                _graphRepositoryService.GetVertex(dataSourceIdentifier);

            //Note: as the result is only a vertex -- without edges -- 
            //I'm not benefiting from the older way of getting edgs at the same time.
            var result = MapTo(graphRepositoryVertex);

            return result;
        }


        /// <summary>
        /// Gets the vertex and edges.
        /// </summary>
        /// <param name="vertexId">The vertex identifier.</param>
        /// <returns></returns>
        public KeyValue<TGraphVertex,TGraphEdge[]> GetVertexAndEdges(Guid vertexId)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Retrieving Vertex (Id='{0}')", vertexId);

            KeyValue<TGraphVertex, TGraphEdge[]> results = new KeyValue<TGraphVertex, TGraphEdge[]>();

            KeyValue<GraphRepositoryVertex,GraphRepositoryEdge[]> tmpResults =
                _graphRepositoryService.GetVertexAndEdges(vertexId);

            //Note: as the result is only a vertex -- without edges -- 
            //I'm not benefiting from the older way of getting edgs at the same time.
            results.Key = MapTo(tmpResults.Key);
            results.Value = tmpResults.Value.Select(e => MapTo(e)).ToArray();

            return results;
        }

        /// <summary>
        /// Gets the vertex and edges.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <returns></returns>
        public KeyValue<TGraphVertex, TGraphEdge[]> GetVertexAndEdges(IHasDataSourceIdentifier dataSourceIdentifier)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Retrieving Vertex (Id='{0}')", dataSourceIdentifier);

            KeyValue<TGraphVertex, TGraphEdge[]> results = new KeyValue<TGraphVertex, TGraphEdge[]>();

            KeyValue<GraphRepositoryVertex, GraphRepositoryEdge[]> tmpResults =
                _graphRepositoryService.GetVertexAndEdges(dataSourceIdentifier);

            //Note: as the result is only a vertex -- without edges -- 
            //I'm not benefiting from the older way of getting edgs at the same time.
            results.Key = MapTo(tmpResults.Key);
            results.Value = tmpResults.Value.Select(e => MapTo(e)).ToArray();

            return results;
        }


        /// <summary>
        /// Refreshes the vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public void RefreshVertex(TGraphVertex vertex)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Retrieving Vertex (Id='{0}')", vertex.Id);

            GraphRepositoryVertex graphRepositoryVertex =
                _graphRepositoryService.GetVertex(vertex.Id);

            //Map values from returned object to current object:
            vertex.Tag = graphRepositoryVertex.Tag;
            vertex.DataSourceId = graphRepositoryVertex.DataSourceId;
            vertex.DataSourceSerializedIdentities = graphRepositoryVertex.DataSourceSerializedIdentities;

        }


        /// <summary>
        /// Persists the specified vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        public void PersistVertex(TGraphVertex vertex)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Persting changes to Vertex (Id='{0}')", vertex.Id);

            //Maps it back to an entity,
            //but the entity is *not* attached to a db context:
            GraphRepositoryVertex graphRepositoryVertex = this.MapTo(vertex);

            //Persist it, whether new or old:
            _graphRepositoryService.PersistVertex(graphRepositoryVertex);

            //IMPORTANT:
            //Because the RepositoryService.Persist now takes advantage of Timestamp, 
            //and doesn't generate an Id if it doesn't have to, the following 
            //is no longer needed either (it never was a good solution anyway, as it required
            //a Commit to work:
            //vertex.Id = graphRepositoryVertex.Id;
        }


        /// <summary>
        /// Deletes the vertex (on next commit), as well as Edges that connect to it.
        /// </summary>
        /// <param name="graphVertex">The graph vertex.</param>
        public void DeleteVertex(TGraphVertex graphVertex)
        {
            _graphRepositoryService.RemoveVertex(graphVertex.Id);
        }


























        /// <summary>
        /// Determine if there is a record for the edge.
        /// </summary>
        /// <param name="edgeId">The edge id.</param>
        /// <returns></returns>
        public bool EdgeExists(Guid edgeId)
        {
            return _graphRepositoryService.EdgeExists(edgeId);
        }





        /// <summary>
        /// Adds the edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        public void PersistEdge(TGraphEdge edge)
        {
            GraphRepositoryEdge graphRepositoryEdge = MapTo(edge);

            _graphRepositoryService.PersistEdge(graphRepositoryEdge);


            //IMPORTANT:
            //Because the RepositoryService.Persist now takes advantage of Timestamp, 
            //and doesn't generate an Id if it doesn't have to, the following 
            //is no longer needed either (it never was a good solution anyway, as it required
            //a Commit to work:
            //if (edge.Id == Guid.Empty)
            //{
            //    edge.Id = edge.Id;
            //}

        }








        #region Virtual Map Methods
        /// <summary>
        /// Maps the specified GraphVertex to the destination <see cref="GraphRepositoryVertex" />
        /// </summary>
        /// <param name="graphVertex">The graph vertex.</param>
        /// <param name="refetchFromDataStore">if set to <c>true</c> [refetch from data store].</param>
        /// <returns></returns>
        protected virtual GraphRepositoryVertex MapTo(TGraphVertex graphVertex,bool refetchFromDataStore=true)
        {

            GraphRepositoryVertex result = (refetchFromDataStore)
                                               ? _graphRepositoryService.GetVertex(graphVertex.Id)
                                               : null;

            if (result == null)
            {
                result = new GraphRepositoryVertex();
                result.GraphId = graphVertex.GraphId;
                result.Id = graphVertex.Id;
                result.Tag = graphVertex.Tag;
                result.DataSourceId = graphVertex.DataSourceId;
            }
            result.DataSourceSerializedIdentities = graphVertex.DataSourceSerializedIdentities;
        

            return result;

        }
        /// <summary>
        /// Maps the specified <see cref="GraphRepositoryVertex"/> to the target GraphVertex.
        /// </summary>
        /// <param name="graphRepositoryVertex">The graph repository vertex.</param>
        /// <returns></returns>
        protected virtual TGraphVertex MapTo(GraphRepositoryVertex graphRepositoryVertex)
        {
            TGraphVertex result = new TGraphVertex
                                      {
                                          GraphId = graphRepositoryVertex.GraphId,
                                          Id = graphRepositoryVertex.Id,
                                          Tag = graphRepositoryVertex.Tag,
                                          DataSourceId = graphRepositoryVertex.DataSourceId,
                                          DataSourceSerializedIdentities = graphRepositoryVertex.DataSourceSerializedIdentities,
                                      };

            return result;

        }

        /// <summary>
        /// Maps the specified GraphEdge to the destination <see cref="GraphRepositoryEdge" />
        /// </summary>
        /// <param name="graphEdge">The graph edge.</param>
        /// <param name="refetchFromDataStore">if set to <c>true</c> [refetch from data store].</param>
        /// <returns></returns>
        protected virtual GraphRepositoryEdge MapTo(TGraphEdge graphEdge, bool refetchFromDataStore = true)
        {
            GraphRepositoryEdge result;


            result = (refetchFromDataStore) ? _graphRepositoryService.GetEdge(graphEdge.Id) : null;

            if (result == null)
            {
                result = new GraphRepositoryEdge();
                result.Id = graphEdge.Id;
                result.SourceFK = graphEdge.SourceFK;
                result.TargetFK = graphEdge.TargetFK;
            }

            result.Weight = graphEdge.Weight;

            return result;
        }

        /// <summary>
        /// Maps the specified <see cref="GraphRepositoryEdge"/> to the target GraphEdge.
        /// </summary>
        /// <param name="graphEdge">The graph edge.</param>
        /// <returns></returns>
        protected virtual TGraphEdge MapTo(GraphRepositoryEdge  graphEdge)
        {
            TGraphEdge result = new TGraphEdge
                                    {
                                        Id=graphEdge.Id,
                                        SourceFK=graphEdge.SourceFK,
                                        TargetFK=graphEdge.TargetFK,
                                        //The tricky part is that if you refer to 
                                        //source and target properties, and lazyloading is enabled, 
                                        //an expensive call will be triggered just looking at the variables...
                                    };

            if (graphEdge.IsSourceRetrieved())
            {
                result.Source = MapTo(graphEdge.Source);
            }
            if (graphEdge.IsTargetRetrieved())
            {
                result.Target = MapTo(graphEdge.Target);
            }

            return result;
        }

        #endregion

    }
}
