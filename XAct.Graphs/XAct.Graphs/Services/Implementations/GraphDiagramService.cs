// ReSharper disable CheckNamespace
namespace XAct.Graphs.Implementations
// ReSharper restore CheckNamespace
{
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
 
    /// <summary>
    /// An implementation of
    /// <see cref="IGraphDiagramService"/>
    /// to manage Diagrams made up of Graph Edges and Vertices
    /// </summary>
    public class GraphDiagramService : DistributedGuidIdRepositoryServiceBase<GraphDiagram>,IGraphDiagramService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GraphDiagramService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public GraphDiagramService(ITracingService tracingService, IRepositoryService repositoryService):base(tracingService, repositoryService)
        {
        }
    }
}