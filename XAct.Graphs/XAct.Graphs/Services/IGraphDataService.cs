﻿namespace XAct.Graphs
{
    /// <summary>
    /// Contract for a service that will work with the data of the Graph
    /// </summary>
    public interface IGraphDataService : IHasXActLibService
    {
        
    }
}
