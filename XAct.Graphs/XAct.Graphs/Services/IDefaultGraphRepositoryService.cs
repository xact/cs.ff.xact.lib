﻿namespace XAct.Graphs
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDefaultGraphRepositoryService : IGraphRepositoryService<GraphRepositoryVertex, GraphRepositoryEdge>, IHasXActLibService
    {
                
    }
}