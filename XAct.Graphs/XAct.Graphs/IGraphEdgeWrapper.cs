﻿

namespace XAct.Graphs
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
    public interface IGraphEdgeWrapper<TGraphVertex>
    {
        /// <summary>
        /// Gets the Wrapper that was used to put the object in the Graph.
        /// <para>
        /// IMPORTANT: Implement Explitly.
        /// </para>
        /// </summary>
        QuickGraph.IEdge<TGraphVertex> EdgeWrapper { get; set; }
    }
}
