﻿
// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using XAct.Graphs;

    /// <summary>
    /// 
    /// </summary>
    public static class GraphElementAttributeExtensions
    {
        /// <summary>
        /// Maps the given <see cref="GraphRepositoryVertexAttribute"/>
        /// to a new <see cref="GraphEntityAttribute2"/>.
        /// </summary>
        /// <remarks>
        /// Used by <see cref="IGraphVertexAttributeService"/>
        /// </remarks>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static GraphEntityAttribute2 Map(this GraphRepositoryVertexAttribute source)
        {
            return new GraphEntityAttribute2 (source.Key, source.DeserializeValue());
        }
    }
}
