using XAct;
using XAct.Graphs;

/// <summary>
/// Extension methods for IGraphEdge{TVertex} based edges.
/// </summary>
public static class IGraphEdgeExtensions
{


    /// <summary>
    /// Gets the graph persistence service.
    /// </summary>
    public static IGraphPersistenceService<TGraphVertex, IGraphEdge<TGraphVertex>> GetGraphPersistenceService<TGraphVertex>(this IGraphEdge<TGraphVertex> edge )
        where TGraphVertex : class, IGraphVertex, new()

    {
        return
            DependencyResolver.Current.GetInstance
                <IGraphPersistenceService<TGraphVertex, IGraphEdge<TGraphVertex>>>(false);
    }

}