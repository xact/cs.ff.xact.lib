﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface IGraphPruner : IGraphPruner<GraphVertex, GraphEdge>
    {

    }


    /// <summary>
    /// The contract for a service to cull stale vertices in 
    /// order to allow working with graphs that don't fit in available memory.
    /// </summary>
    /// <remarks>
    /// Many graphs are too large to fit in memory.
    /// To allow for this, vertices that are no longer being used (stale)
    /// should be removed.
    /// This service, allows for this.
    /// </remarks>
    /// <typeparam name="TGraphVertex">The type of the vertex.</typeparam>
    /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
// ReSharper disable UnusedTypeParameter
    public interface IGraphPruner<TGraphVertex, TGraphEdge>
// ReSharper restore UnusedTypeParameter
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : IGraphEdge<TGraphVertex>
    {

        /// <summary>
        /// Occurs when a vertex is about to be converted into a placeholder vertex
        /// removing its data from memory. 
        /// <para>
        /// Note that this is just befoe it gets persisted.
        /// </para>
        /// </summary>
        event EventHandler<CancelVertexEventArgs<TGraphVertex>> CancellableCullingVertex;

        /// <summary>
        /// Gets or sets the preferred max number of Vertices to keep in the graph.
        /// </summary>
        int PreferredMaxNumberofVertices { get; set; }

        /// <summary>
        /// Puts the vertex at the top of the internal MruList.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        void Touch(TGraphVertex vertex);

        /// <summary>
        /// Culls this graph of old/stale vertices.
        /// </summary>
        void ReleaseVertices();
    }
}
