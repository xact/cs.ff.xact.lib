﻿namespace XAct.Graphs.Tests.Entities
{
    using System.Runtime.Serialization;
    using XAct.Data.Models.Implementations;

    [DataContract]
    public class SomeVertex : DefaultDataSourceIdentifierArangoDbDocument
    {
        [DataMember]
        public string Tag { get; set; }
    }
}
