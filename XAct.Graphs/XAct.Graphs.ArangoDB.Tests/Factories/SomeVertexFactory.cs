﻿namespace XAct.Graphs.Tests.Factories
{
    using System;
    using XAct.Graphs.Tests.Entities;

    public class SomeVertexFactory
    {
        public static SomeVertex Create(Guid dataSourceId, string serializedId, string tag)
        {
            return new SomeVertex
                {
                    DataSourceId = dataSourceId,
                    DataSourceSerializedIdentities = serializedId,
                    Tag = tag
                };

        }
    }
}
