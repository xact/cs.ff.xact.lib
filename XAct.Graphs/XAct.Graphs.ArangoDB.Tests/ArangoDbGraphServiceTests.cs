namespace XAct.Graphs.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using NUnit.Framework;
    using RestSharp;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Data.Models;
    using XAct.Data.Models.Implementations;
    using XAct.Data.Services;
    using XAct.Data.Services.Configuration;
    using XAct.Data.Services.Configuration.Implementations;
    using XAct.Graphs.Services;
    using XAct.Graphs.Tests.Entities;
    using XAct.Graphs.Tests.Factories;
    using XAct.Settings;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class ArangoDbGraphServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

#pragma warning disable 168
            bool runTests = RunADTests;
#pragma warning restore 168
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        #region helpers

        private bool RunADTests
        {
            get
            {
                var hostSettingsService = XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();
                if (!
                    hostSettingsService
                        .Get<string>("RunAndangoDbServiceTests")
                        .ToBool())
                {
                    return false;
                }

                var config =
                    XAct.DependencyResolver.Current.GetInstance<IArangoDbServiceConfiguration>();

                if (!config.ConnectionInfos.Contains(x => x.Alias == "Test"))
                {
                    var t = new ArangoDbConnectionInfo
                        {
                            IsSecure = hostSettingsService.Get<bool>("AdangoDbServerIsSecure"),
                            HostName = hostSettingsService.Get<string>("AdangoDbServerHost"),
                            UserName = hostSettingsService.Get<string>("AdangoDbServerUserName"),
                            Password = hostSettingsService.Get<string>("AdangoDbServerPassword"),
                            DatabaseName = hostSettingsService.Get<string>("AdangoDbServerDatabase"),
                            Alias = "Test",
                            IsDefaultContext = true
                        };

                    config.ConnectionInfos.Add(t);
                }

                config.PingInfo.DatabaseAlias = hostSettingsService.Get<string>("AdangoDbServerPingAlias");
                config.PingInfo.CollectionName = hostSettingsService.Get<string>("AdangoDbServerPingCollection");

                return true;


            }
        }

        #endregion



        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [Test]
        public void Can_Create_ArangoDBService()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();


            Assert.IsNotNull(service);
        }



        [Test]
        public void Can_Create_ArangoDBService_Of_ExpectedType()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();


            Assert.AreEqual(typeof (ArangoDbService), service.GetType());
        }



        [Test]
        public void Can_Create_ArangoDBService_And_Ping_Test_Connection()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }

            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();

            var r = service.Ping();
            if (!r)
            {
                var config =
                    XAct.DependencyResolver.Current.GetInstance<IArangoDbServiceConfiguration>();

                service.CreateCollection(config.PingInfo.CollectionName, false);

                r = service.Ping();
            }
            Assert.IsTrue(r);

        }

        [Test]
        public void Can_Create_A_Collection()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }

            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();

            service.CreateCollection("SomeVertex", false);

            bool exists = service.CollectionExists("SomeVertex");
            Assert.IsNotNull(exists);

        }

        [Test]
        public void Can_Repeatedly_Create_A_Collection()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }

            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();

            service.CreateCollection("SomeVertex", false);
            service.CreateCollection("SomeVertex", false);
            service.CreateCollection("SomeVertex", false);
            service.CreateCollection("SomeVertex", false);
            service.CreateCollection("SomeVertex", false);

            bool exists = service.CollectionExists("SomeVertex");
            Assert.IsNotNull(exists);

        }


        [Test]
        public void Can_Save_A_Document()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();

            service.CreateCollection("SomeVertex", false);

            var someVertex = SomeVertexFactory.Create(1.ToGuid(), Guid.NewGuid().ToString(), "A");
            Assert.IsNullOrEmpty(someVertex._id, "Id should still be null.");

            service.PersistDocument("SomeVertex", someVertex);

            Assert.IsNotNullOrEmpty(someVertex._id, "Id should have been filed in.");
        }

        [Test]
        public void Can_Save_And_Retrieve_A_Document()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }

            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();

            service.CreateCollection("SomeVertex", false);

            var someVertex = SomeVertexFactory.Create(2.ToGuid(), Guid.NewGuid().ToString(), "A");
            Assert.IsNullOrEmpty(someVertex._id, "Id should still be null.");

            service.PersistDocument("SomeVertex", someVertex);

            Assert.IsNotNullOrEmpty(someVertex._id, "Id should have been filed in.");


            var secondVertex = service.GetDocument<SomeVertex>(someVertex._id);

            Console.WriteLine(secondVertex._id);
            Assert.IsNotNull(secondVertex);
            Assert.AreEqual(someVertex._id, secondVertex._id);
            Assert.AreNotEqual(someVertex, secondVertex);

        }

        [Test]
        public void Can_Save_And_Retrieve_Two_Document_And_Join_Them()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();
            var graphService = XAct.DependencyResolver.Current.GetInstance<IArangoDbGraphService>();

            service.CreateCollection("SomeVertex", false);
            service.CreateCollection("SomeEdge", true);

            var someFirstVertex = SomeVertexFactory.Create(2.ToGuid(), Guid.NewGuid().ToString(), "VertexA");
            service.PersistDocument("SomeVertex", someFirstVertex);

            var someSecondVertex = SomeVertexFactory.Create(2.ToGuid(), Guid.NewGuid().ToString(), "VertexB");
            service.PersistDocument("SomeVertex", someSecondVertex);


            var edge = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someFirstVertex,
                                                                                  someSecondVertex);

            Assert.IsNotNull(edge);
            Assert.IsNotNullOrEmpty(edge._id);
        }

        [Test]
        public void Can_Save_And_Retrieve_Four_Document_And_Join_Them()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();
            var graphService = XAct.DependencyResolver.Current.GetInstance<IArangoDbGraphService>();

            //service.CreateCollection("SomeVertex", false);
            //service.CreateCollection("SomeEdge", true);

            var someVertex1 = SomeVertexFactory.Create(10.ToGuid(), Guid.NewGuid().ToString(), "GroupA");
            service.PersistDocument("SomeVertex", someVertex1);

            var someVertex2 = SomeVertexFactory.Create(10.ToGuid(), Guid.NewGuid().ToString(), "GroupA");
            service.PersistDocument("SomeVertex", someVertex2);

            var someVertex3 = SomeVertexFactory.Create(10.ToGuid(), Guid.NewGuid().ToString(), "GroupA");
            service.PersistDocument("SomeVertex", someVertex3);

            var someVertex4 = SomeVertexFactory.Create(10.ToGuid(), Guid.NewGuid().ToString(), "GroupA");
            service.PersistDocument("SomeVertex", someVertex4);

            var edge1 = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex1,
                                                                                   someVertex2);

            var edge1B = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex1,
                                                                                   someVertex3);
            var edge1C = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex1,
                                                                                   someVertex4);

            var edge2 = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex2,
                                                                                   someVertex3);

            var edge3 = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex3,
                                                                                   someVertex4);

            var edge4 = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex4,
                                                                                   someVertex1);

            Console.WriteLine(someVertex1.ToJSON(Encoding.UTF8));
            Console.WriteLine(someVertex2.ToJSON(Encoding.UTF8));
            Console.WriteLine(someVertex3.ToJSON(Encoding.UTF8));
            Console.WriteLine(someVertex4.ToJSON(Encoding.UTF8));


            Assert.IsNotNull(edge1._id);
            Assert.IsNotNull(edge1B._id);
            Assert.IsNotNull(edge1C._id);
            Assert.IsNotNull(edge2._id);
            Assert.IsNotNull(edge3._id);
            Assert.IsNotNull(edge4._id);
        }











        [Test]
        public void List_Graphs()
        {

            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            //var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();
            var graphService = XAct.DependencyResolver.Current.GetInstance<IArangoDbGraphService>();

            Assert.IsNotNull(graphService.GetGraphNames());

        }

        [Test]
        [Ignore("See Create_List_Drop_A_Graph")]
        public void Create_A_Graph()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            //var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();
            var graphService = XAct.DependencyResolver.Current.GetInstance<IArangoDbGraphService>();

            var graphName = "Test_" + Guid.NewGuid().ToString();
            var edgeCollection = "SomeEdge";
            var sourceVertexCollection = "SomeVertex";
            var targetVertexCollection = "SomeVertex";

            Assert.IsTrue(graphService.CreateGraph(graphName,edgeCollection,sourceVertexCollection,targetVertexCollection));

        }
        [Test]
        [Ignore("See Create_List_Drop_A_Graph")]
        public void Create_And_List_A_Graph()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            //var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();
            var graphService = XAct.DependencyResolver.Current.GetInstance<IArangoDbGraphService>();
            
            var graphName = "Test_" + Guid.NewGuid().ToString();
            var edgeCollection = "SomeEdge";
            var sourceVertexCollection = "SomeVertex";
            var targetVertexCollection = "SomeVertex";

            Assert.IsTrue(graphService.CreateGraph(graphName, edgeCollection, sourceVertexCollection, targetVertexCollection));


            Assert.IsTrue(graphService.EnsureGraphExists(graphName));


        }



        [Test]
        [Ignore("See Create_List_Drop_A_Graph")]
        public void DropAGraph()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            //var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();
            var graphService = XAct.DependencyResolver.Current.GetInstance<IArangoDbGraphService>();

            var graphName = "Test_" + Guid.NewGuid().ToString();
            var edgeCollection = "SomeEdge";
            var sourceVertexCollection = "SomeVertex";
            var targetVertexCollection = "SomeVertex";

            Assert.IsTrue(graphService.CreateGraph(graphName, edgeCollection, sourceVertexCollection, targetVertexCollection));

            Assert.IsTrue(graphService.EnsureGraphExists(graphName));

            Assert.IsTrue(graphService.DeleteGraph(graphName));



        }



        [Test]
        public void Create_List_Drop_A_Graph()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            //var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();
            var graphService = XAct.DependencyResolver.Current.GetInstance<IArangoDbGraphService>();

            var graphName = "Test_" + Guid.NewGuid().ToString();
            var edgeCollection = "SomeEdge";
            var sourceVertexCollection = "SomeVertex";
            var targetVertexCollection = "SomeVertex";

            Assert.IsTrue(graphService.CreateGraph(graphName, edgeCollection, sourceVertexCollection, targetVertexCollection));

            Assert.IsTrue(graphService.EnsureGraphExists(graphName));

            Assert.IsTrue(graphService.DeleteGraph(graphName));



        }

        [Test]
        public void Query_For_All_Vertices_In_Graph()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();

            string graphName = "Test";


            string statement = @"
FOR e in GRAPH_VERTICES
    (
        @graphName, 
        {},
        {}
    )
RETURN e";

            var results =
 service.Context.QueryList(statement,
                               new KeyValue<string, object>() { Key = "graphName", Value = graphName }
                               );

            Console.WriteLine(results.ToJSON(Encoding.UTF8));

            Assert.IsTrue(results.Count>0);
        }

        [Test]
        public void Query_For_All_Vertices_In_Graph_As_Typed()
        {
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();

            string graphName = "Test";


            string statement = @"
FOR e in GRAPH_VERTICES
    (
        @graphName, 
        {},
        {}
    )
RETURN e";

            var results =
 service.Context.QueryList<SomeVertex>(statement,
                               new KeyValue<string, object>() { Key = "graphName", Value = graphName }
                               );

            Console.WriteLine(results.ToJSON(Encoding.UTF8));

            Assert.IsTrue(results.Count > 0);
        }



        [Test]
        public void Get_OutGoing_Vertices_From_A_Starting_Vertex()
        {
            //See: https://docs.arangodb.com/Aql/GraphOperations.html
            if (!RunADTests)
            {
                Assert.Ignore("The 'RunAdangoDB' appSetting is not set to 'true'. Ignoring test.");
                return;
            }
            var service = XAct.DependencyResolver.Current.GetInstance<IArangoDbService>();
            var graphService = XAct.DependencyResolver.Current.GetInstance<IArangoDbGraphService>();

            var graphName = "Test_" + Guid.NewGuid().ToString();
            var edgeCollection = "SomeEdge";
            var sourceVertexCollection = "SomeVertex";
            var targetVertexCollection = "SomeVertex";

            Assert.IsTrue(graphService.CreateGraph(graphName, edgeCollection, sourceVertexCollection, targetVertexCollection));


#pragma warning disable 168
            var someVertex1 = SomeVertexFactory.Create(10.ToGuid(), Guid.NewGuid().ToString(), "GroupA");
            service.PersistDocument("SomeVertex", someVertex1);

            var someVertex2 = SomeVertexFactory.Create(10.ToGuid(), Guid.NewGuid().ToString(), "GroupA");
            service.PersistDocument("SomeVertex", someVertex2);

            var someVertex3 = SomeVertexFactory.Create(10.ToGuid(), Guid.NewGuid().ToString(), "GroupA");
            service.PersistDocument("SomeVertex", someVertex3);

            var someVertex4 = SomeVertexFactory.Create(10.ToGuid(), Guid.NewGuid().ToString(), "GroupA");
            service.PersistDocument("SomeVertex", someVertex4);

            var edge1 = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex1,
                                                                                   someVertex2);

            var edge1B = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex1,
                                                                                   someVertex3);
            var edge1C = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex1,
                                                                                   someVertex4);

            var edge2 = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex2,
                                                                                   someVertex3);

            var edge3 = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex3,
                                                                                   someVertex4);

            var edge4 = graphService.CreateEdgeDocument<DefaultEdgeDocument>("SomeEdge", someVertex4,
                                                                                   someVertex1);


            var results = graphService.GetOutDocuments<SomeVertex, DefaultEdgeDocument>(graphName, someVertex1);
#pragma warning restore 168

            Assert.IsNotNull(results);
            Assert.IsNotNull(results.Length==3);

            results = graphService.GetOutDocuments<SomeVertex, DefaultEdgeDocument>(graphName, someVertex2);
#pragma warning restore 168

            Assert.IsNotNull(results);
            Assert.IsNotNull(results.Length == 1);
        }


        


    }


}