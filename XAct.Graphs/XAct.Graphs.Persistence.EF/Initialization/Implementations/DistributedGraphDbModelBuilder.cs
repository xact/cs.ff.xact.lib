namespace XAct.Graphs.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Graphs.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class DistributedGraphDbModelBuilder : IGraphDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributedGraphDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public DistributedGraphDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");
            //TODO


            modelBuilder.Configurations.Add((EntityTypeConfiguration<GraphRepositoryPackageSchema>)XAct.DependencyResolver.Current.GetInstance<IGraphRepositoryPackageSchemaModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<GraphRepositoryEdge>)XAct.DependencyResolver.Current.GetInstance<IGraphRepositoryEdgeModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<GraphRepositoryVertex>)XAct.DependencyResolver.Current.GetInstance<IGraphRepositoryVertexModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<GraphRepositoryVertexAttribute>)XAct.DependencyResolver.Current.GetInstance<IGraphRepositoryVertexAttributeModelPersistenceMap>());

            





        }
    }
}
