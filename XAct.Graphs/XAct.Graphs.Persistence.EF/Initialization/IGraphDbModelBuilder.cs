namespace XAct.Graphs.Initialization
{
    using XAct.Initialization;

    /// <summary>
    /// 
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface IGraphDbModelBuilder : XAct.Data.EF.CodeFirst.IHasXActLibDbModelBuilder
    {

    }
}