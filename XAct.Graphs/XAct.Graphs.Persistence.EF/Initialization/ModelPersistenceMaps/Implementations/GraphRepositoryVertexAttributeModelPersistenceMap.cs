﻿namespace XAct.Graphs.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class GraphRepositoryVertexAttributeModelPersistenceMap :
        EntityTypeConfiguration<GraphRepositoryVertexAttribute>,
        IGraphRepositoryVertexAttributeModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="GraphRepositoryVertexAttributeModelPersistenceMap"/> class.
        /// </summary>
        public GraphRepositoryVertexAttributeModelPersistenceMap()
        {

            this.ToXActLibTable("GraphVertexAttribute");

            int colOrder = 0;
#pragma warning disable 168
            //int indexMember = 1; //Indexs of db's are 1 based.
#pragma warning restore 168

            this
                .HasKey(m => m.Id);

            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.ElementType)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.ElementId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.UserIdentifier)
                .DefineRequired64CharUserIdentifier(colOrder++);

            this
                .Property(m => m.Key)
                .DefineRequired64CharKey(colOrder++)
                ;

            this
                .Property(m => m.SerializedValueType)
                .DefineRequired1024CharSerializationValueType(colOrder++);

            this
                .Property(m => m.SerializationMethod)
                .DefineRequiredSerializationMethod(colOrder++);
            this
                .Property(m => m.SerializedValue)
                .DefineOptional4000CharSerializationValue(colOrder++);

        }
    }
}

