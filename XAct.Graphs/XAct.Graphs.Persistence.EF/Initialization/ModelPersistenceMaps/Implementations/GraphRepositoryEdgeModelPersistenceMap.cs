﻿namespace XAct.Graphs.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    public class GraphRepositoryEdgeModelPersistenceMap : EntityTypeConfiguration<GraphRepositoryEdge>, IGraphRepositoryEdgeModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphRepositoryEdgeModelPersistenceMap"/> class.
        /// </summary>
        public GraphRepositoryEdgeModelPersistenceMap()
        {
            this.ToXActLibTable("GraphEdge");
            

            int colOrder = 0;
#pragma warning disable 168
            //int indexMember = 1; //Indexs of db's are 1 based.
#pragma warning restore 168

            this
                .HasKey(m => m.Id);
            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this
                .Property(m => m.SourceFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.TargetFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Weight)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            //Relationships
            //this
            //    .HasRequired(a => a.Source)
            //    .WithMany()
            //    .HasForeignKey(a => a.SourceFK);


            //this
            //    .HasRequired(a => a.Target)
            //    .WithMany()
            //    .HasForeignKey(a => a.TargetFK);


        }
    }
}
