﻿namespace XAct.Graphs.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    public class GraphRepositoryPackageSchemaModelPersistenceMap : EntityTypeConfiguration<GraphRepositoryPackageSchema>,
                                                                   IGraphRepositoryPackageSchemaModelPersistenceMap
    {
        public GraphRepositoryPackageSchemaModelPersistenceMap()
        {
            this.ToXActLibTable("Graph");

            this
                .HasKey(m => m.Id);

            int colOrder = 0;
#pragma warning disable 168
            //int indexMember = 1; //Indexs of db's are 1 based.
#pragma warning restore 168

            this
                .HasKey(m => m.Id);
            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++)
                ;

            this
                .Property(m => m.ApplicationTennantId)
                .DefineRequiredApplicationTennantId(colOrder++)
                ;
            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(32)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;
        }
    }
}