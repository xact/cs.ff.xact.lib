﻿namespace XAct.Graphs.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class GraphRepositoryVertexModelPersistenceMap : EntityTypeConfiguration<GraphRepositoryVertex>, IGraphRepositoryVertexModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphRepositoryVertexModelPersistenceMap"/> class.
        /// </summary>
        public GraphRepositoryVertexModelPersistenceMap()
        {
            this.ToXActLibTable("GraphVertex");
            
                int colOrder = 0;
#pragma warning disable 168
                //int indexMember = 1; //Indexs of db's are 1 based.
#pragma warning restore 168

                this
                    .HasKey(m => m.Id);
                this
                    .Property(m => m.Id)
                    .DefineRequiredGuidId(colOrder++);

                this
                    .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

                this
                    .Property(m => m.GraphId)
                    .IsRequired()
                    .HasColumnOrder(colOrder++)
                    ;

                this
                    .Property(m => m.Weight)
                    .IsRequired()
                    .HasColumnOrder(colOrder++)
                    ;


                this
                    .Property(m => m.DataSourceId)
                    .IsOptional()
                    .HasColumnOrder(colOrder++)
                    ;
                this
                    .Property(m => m.DataSourceSerializedIdentities)
                    .IsOptional()
                    .HasColumnOrder(colOrder++)
                    ;

                this
                    .Property(m => m.Tag)
                    .DefineOptional256CharTag(colOrder++);


            

        }
    }
}
