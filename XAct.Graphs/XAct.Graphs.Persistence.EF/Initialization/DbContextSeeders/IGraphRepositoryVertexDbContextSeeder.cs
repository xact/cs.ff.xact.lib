namespace XAct.Graphs.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{GraphRepositoryVertex}"/>
    /// to seed the Graph tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IGraphRepositoryVertexDbContextSeeder : IHasXActLibDbContextSeeder<GraphRepositoryVertex>
    {
        
    }
}