﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs.Initialization.DbContextSeeders.Implementations
// ReSharper restore CheckNamespace
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="IGraphRepositoryVertexDbContextSeeder"/> contract
    /// to seed the Graph tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class GraphRepositoryVertexDbContextSeeder : XActLibDbContextSeederBase<GraphRepositoryVertex>, IGraphRepositoryVertexDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="GraphRepositoryVertexDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public GraphRepositoryVertexDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }
    }
}
