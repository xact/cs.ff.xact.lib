namespace XAct.Graphs.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IGraphRepositoryPackageSchemaDbContextSeeder : IHasXActLibDbContextSeeder<GraphRepositoryPackageSchema>
    {
        
    }
}