﻿//namespace XAct.Graphs.Services.Implementations
//{
//    using System;
//    using System.Linq;
//    using XAct.Diagnostics;
//    using XAct.Domain.Repositories;
//    using XAct.Services;

//    public abstract class GraphRepositoryServiceBase<TGraphVertex, TGraphEdge> :
//        XActLibServiceBase,
//        IGraphRepositoryService<TGraphVertex, TGraphEdge>

//        where TGraphVertex : GraphRepositoryVertex, new()
//        where TGraphEdge : GraphRepositoryEdge, new()
//    {
//        private readonly IRepositoryService _repositoryService;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="GraphRepositoryServiceBase{TGraphVertex, TGraphEdge}" /> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        /// <param name="repositoryService">The repository service.</param>
//        protected GraphRepositoryServiceBase(ITracingService tracingService, IRepositoryService repositoryService)
//            : base(tracingService)
//        {
//            _repositoryService = repositoryService;
//        }

//        /// <summary>
//        /// Gets the <see cref="GraphPackageSchema"/>.
//        /// </summary>
//        /// <param name="uniqueName">The name.</param>
//        /// <returns></returns>
//        public GraphPackageSchema GetGraphDescription(string uniqueName)
//        {
//            return _repositoryService.GetSingle<GraphPackageSchema>(gd => gd.Name == uniqueName);
//        }

//        /// <summary>
//        /// Gets the <see cref="GraphPackageSchema"/>.
//        /// </summary>
//        /// <param name="guid">The GUID.</param>
//        /// <returns></returns>
//        public GraphPackageSchema GetGraphDescription(Guid guid)
//        {
//            return _repositoryService.GetSingle<GraphPackageSchema>(gd => gd.Id == guid);
//        }

//        /// <summary>
//        /// Deletes the <see cref="GraphPackageSchema"/>.
//        /// </summary>
//        /// <param name="guid">The GUID.</param>
//        public void DeleteGraphPackageSchema(Guid guid)
//        {
//            _repositoryService.DeleteOnCommit<GraphPackageSchema>( gd => gd.Id == guid);
//        }






//        public void Persist(GraphPackageSchema graphDefinition)
//        {
//            _repositoryService.PersistOnCommit<GraphPackageSchema, Guid>(graphDefinition, true);
//        }

//        /// <summary>
//        /// Determines if there is a Vertex with the given Id.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <returns></returns>
//        public bool VertexExists(Guid vertexId)
//        {
//            vertexId.ValidateIsNotDefault("vertexId");

//            return _repositoryService.Contains<TGraphVertex>(v => v.Id == vertexId);
//        }


//        /// <summary>
//        /// Gets the vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <param name="includeEdges">if set to <c>true</c> [include edges].</param>
//        /// <returns></returns>
//        public TGraphVertex GetVertex(Guid vertexId, bool includeEdges = true)
//        {
//            return includeEdges
//                       ? _repositoryService.GetSingle<TGraphVertex>(v => v.Id == vertexId)
//                       : _repositoryService.GetSingle<TGraphVertex>(v => v.Id == vertexId,
//                                                                    new IncludeSpecification<TGraphVertex>(v => v.InEdges,
//                                                                                                           v => v.OutEdges));
//        }

//        /// <summary>
//        /// Persists the vertex.
//        /// </summary>
//        /// <param name="vertex">The vertex.</param>
//        public void PersistVertex(TGraphVertex vertex)
//        {
//            _repositoryService.PersistOnCommit(vertex, v => v.Id == Guid.Empty);

//        }

//        /// <summary>
//        /// Removes the vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        public void RemoveVertex(Guid vertexId)
//        {
//            _repositoryService.DeleteOnCommit<TGraphVertex,Guid>(vertexId);

//        }

//        /// <summary>
//        /// Removes the vertex.
//        /// </summary>
//        /// <param name="vertex">The vertex.</param>
//        public void RemoveVertex(TGraphVertex vertex)
//        {
//            _repositoryService.DeleteOnCommit(vertex);
//        }


//        /// <summary>
//        /// Gets edges whose target are the given vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <param name="includeVertices">if set to <c>true</c>, include source vertices.</param>
//        /// <returns></returns>
//        public TGraphEdge[] GetInEdges(Guid vertexId, bool includeVertices = false)
//        {
//            return includeVertices
//                       ? _repositoryService.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId).ToArray()
//                       : _repositoryService.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId, new IncludeSpecification<TGraphEdge>(e => e.Source)).ToArray();
//        }


//        /// <summary>
//        /// Gets edges whose source are the given vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <param name="includeVertices">if set to <c>true</c>, include target vertices.</param>
//        /// <returns></returns>
//        public TGraphEdge[] GetOutEdges(Guid vertexId, bool includeVertices = false)
//        {
//            return includeVertices
//                       ? _repositoryService.GetByFilter<TGraphEdge>(e => e.SourceFK == vertexId).ToArray()
//                       : _repositoryService.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId, new IncludeSpecification<TGraphEdge>(e => e.Target)).ToArray();
//        }

//        /// <summary>
//        /// Gets all (incoming and outgoing edges) edges
//        /// whose source is the given vertex.
//        /// </summary>
//        /// <param name="vertexId">The vertex id.</param>
//        /// <param name="includeVertices">if set to <c>true</c>, include source and target vertices.</param>
//        /// <returns></returns>
//        public TGraphEdge[] GetEdges(Guid vertexId, bool includeVertices = false)
//        {
//            return includeVertices
//                       ? _repositoryService.GetByFilter<TGraphEdge>(e => e.SourceFK == vertexId || e.TargetFK == vertexId).ToArray()
//                       : _repositoryService.GetByFilter<TGraphEdge>(e => e.TargetFK == vertexId, new IncludeSpecification<TGraphEdge>(e => e.Target, e => e.Source)).ToArray();
//        }

//        /// <summary>
//        /// Get the given edge.
//        /// </summary>
//        /// <param name="edgeId">The edge id.</param>
//        /// <param name="includeVertices">if set to <c>true</c>, include source vertices.</param>
//        /// <returns></returns>
//        public TGraphEdge GetEdge(Guid edgeId, bool includeVertices = false)
//        {
//            return includeVertices
//                       ? _repositoryService.GetSingle<TGraphEdge>(e => e.Id == edgeId)
//                       : _repositoryService.GetSingle<TGraphEdge>(e => e.Id == edgeId, new IncludeSpecification<TGraphEdge>(e => e.Target, e => e.Source));
//        }

//        public bool EdgeExists(Guid edgeId)
//        {
//            edgeId.ValidateIsNotDefault("edgeId");

//            return _repositoryService.Contains<TGraphVertex>(v => v.Id == edgeId);
//        }


//        /// <summary>
//        /// Persists the edge.
//        /// </summary>
//        /// <param name="edge">The edge.</param>
//        public void PersistEdge(TGraphEdge edge)
//        {
//            _repositoryService.PersistOnCommit(edge, e => e.Id == Guid.Empty);
//        }



//        /// <summary>
//        /// Deletes the specified edge.
//        /// <para>
//        /// No exception is raised if the edge is not found.
//        /// </para>
//        /// </summary>
//        /// <param name="edgeId">The edge id.</param>
//        public void DeleteEdge(Guid edgeId)
//        {
//            _repositoryService.DeleteOnCommit<TGraphEdge,Guid>(edgeId);
//        }

//        /// <summary>
//        /// Deletes the specified edge.
//        /// <para>
//        /// No exception is raised if the edge is not found.
//        /// </para>
//        /// </summary>
//        /// <param name="edge">The edge.</param>
//        public void DeleteEdge(TGraphEdge edge)
//        {
//            _repositoryService.DeleteOnCommit(edge);
//        }

//        /// <summary>
//        /// Gets from the datastore the vertex specified by the identifier.
//        /// </summary>
//        /// <param name="graphId">The graph id.</param>
//        /// <param name="vertexId">The vertex datastore identifier.</param>
//        /// <returns></returns>
//        public TGraphVertex GetVertex(Guid graphId, Guid vertexId)
//        {
//            //this.GetSingle<GraphVertex>(v => v.Id == vertexId, new IncludeSpecification<GraphVertex>(v => v.InEdges));

//            TGraphVertex result = 
//                _repositoryService.GetSingle<TGraphVertex>(v => ((v.GraphId == graphId) && (v.Id == vertexId)), new IncludeSpecification("InEdges", "OutEdges"));

//            return result;
//        }

//    }
//}