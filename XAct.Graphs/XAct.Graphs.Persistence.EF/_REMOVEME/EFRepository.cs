﻿using System.Data;
using System.Linq;
using XAct.Graphs.Entities;

namespace XAct.Graphs
{
    public class EFRepository : IGraphRepository<GraphVertex, int, IGraphEdge<GraphVertex>>
    {
            private readonly GraphContext _graphContext = new GraphContext();

            public void Persist(params GraphVertex[] vertices)
            {
                foreach(GraphVertex vertex in vertices)
                {
                    bool isDetached = ((int)_graphContext.Entry(vertex).State).BitIsSet((int)EntityState.Modified);

                    if (vertex.Id == default(int))
                    {
                        _graphContext.Vertices.Add(vertex);
                    }
                    else
                    {
                        _graphContext.Vertices.Attach(vertex);
                        _graphContext.Entry(vertex).State = EntityState.Modified;
                    }
                }
                _graphContext.SaveChanges();
            }


            public void PersistEdge(params GraphEdge[] edges)
            {
                foreach (GraphEdge edge in edges)
                {
                    if (edge.Id == default(int))
                    {
                        _graphContext.Edges.Add(edge);
                    }
                    else
                    {
                        _graphContext.Edges.Attach(edge);
                        _graphContext.Entry(edge).State = EntityState.Modified;
                    }
                }
                _graphContext.SaveChanges();
            }

            public GraphVertex GetVertex(int identifier)
            {
                throw new System.NotImplementedException();
            }


            private static void UpdateEdge(GraphContext context, GraphEdge edge)
            {
                context.Edges.Attach(edge);
                context.Entry(edge).State = EntityState.Modified;
            }


    }
}
