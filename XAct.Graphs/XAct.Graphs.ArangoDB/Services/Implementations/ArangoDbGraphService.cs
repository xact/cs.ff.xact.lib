namespace XAct.Graphs.Services
{
    using XAct.Data.Models;
    using XAct.Data.Services;
    using XAct.Graphs.Api;


    /// <summary>
    /// 
    /// </summary>
    public class ArangoDbGraphService : IArangoDbGraphService
    {
        private readonly IArangoDbService _arangoDbService;
        private readonly IArangoDbContextService _adangoDbContextService;
        private readonly IAPIGraphRequestsService _apiGraphRequestsService;







        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public IArangoDbContext Context
        {
            get
            {
                return _adangoDbContextService.Context;
            }
        }




        /// <summary>
        /// Initializes a new instance of the <see cref="ArangoDbGraphService" /> class.
        /// </summary>
        /// <param name="arangoDbDocumentManagementService">The arango database document management service.</param>
        /// <param name="adangoDbContextService">The adango database context service.</param>
        /// <param name="apiGraphRequestsService">The API graph requests service.</param>
        public ArangoDbGraphService(IArangoDbService arangoDbDocumentManagementService,
            IArangoDbContextService adangoDbContextService,
                                              IAPIGraphRequestsService apiGraphRequestsService

            //IAdangoDbContextService adangoDbContextService
            )
        {
            _arangoDbService = arangoDbDocumentManagementService;
            _adangoDbContextService = adangoDbContextService;
            _apiGraphRequestsService = apiGraphRequestsService;
        }


        #region Graphs
        /// <summary>
        /// Gets a list of existing Graphs in the current Database.
        /// </summary>
        /// <returns></returns>
        public string[] GetGraphNames()
        {
            return _apiGraphRequestsService.GetGraphNames();
        }

        /// <summary>
        /// Determins if a graph of that name exists in the current database.
        /// </summary>
        /// <param name="graphName"></param>
        /// <returns></returns>
        public bool EnsureGraphExists(string graphName)
        {
            return _apiGraphRequestsService.EnsureGraphExists(graphName);
        }

        /// <summary>
        /// Creates a new  Graph in the current database.
        /// </summary>
        /// <param name="graphName">Name of the graph.</param>
        /// <param name="edgeCollectionName">Name of the edge collection.</param>
        /// <param name="sourceVertexCollectionName">Name of the source vertex collection.</param>
        /// <param name="targetVertexCollectionName">Name of the target vertex collection.</param>
        /// <returns></returns>
        public bool CreateGraph(string graphName, string edgeCollectionName, string sourceVertexCollectionName,
                         string targetVertexCollectionName)
        {
            return _apiGraphRequestsService.CreateGraph(graphName, edgeCollectionName, sourceVertexCollectionName,
                                                        targetVertexCollectionName);
        }

        /// <summary>
        /// Deletes the specified graph if it exists in the current database.
        /// </summary>
        /// <param name="graphName"></param>
        /// <returns></returns>
        public bool DeleteGraph(string graphName)
        {
            return _apiGraphRequestsService.DeleteGraph(graphName);
        }

        #endregion


        #region Vertices
        /// <summary>
        /// Determines whether a Document exists.
        /// <para>
        /// Prefer testing against <see cref="GetVertexDocument{TModel}" />
        /// </para>
        /// </summary>
        /// <param name="documentId">The document identifier.</param>
        /// <returns></returns>
        public bool VertexDocumentExists<TModel>(string documentId)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            return _arangoDbService.DocumentExists<TModel>(documentId);
        }

        /// <summary>
        /// Create or Update a Collection Document object.
        /// <code>
        /// <![CDATA[
        /// //Create a document as an array, or map it to a Type:
        /// document = new Document("{someProp:\"foo/123\"}");
        /// or
        /// //docuemnt = new Document()
        /// .String("foo", "foo string value 1")
        /// .Int("bar", 12345);
        /// ]]>
        /// </code>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="model">The model to persist.</param>
        public void PersistVertexDocument<TModel>(string collectionName, TModel model)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            _arangoDbService.PersistDocument<TModel>(collectionName,model);
        }

        /// <summary>
        /// Gets a single Document by it's Id.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public TModel GetVertexDocument<TModel>(string id) where TModel : class, IArangoDbRecordHandle, new()
        {
            return _arangoDbService.GetDocument<TModel>(id);
        }



        /// <summary>
        /// Deletes a Vertex document.
        /// </summary>
        /// <param name="id"></param>
        public void DeleteVertexDocument<TModel>(string id)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            _arangoDbService.DeleteDocument<TModel>(id);
        }


        /// <summary>
        /// Deletes a Vertex document.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public void DeleteVertexDocument<TModel>(TModel model)
            where TModel : class, IArangoDbRecordHandle, new()
        {
            _arangoDbService.DeleteDocument<TModel>(model._id);
        }
        #endregion



        #region Edges
        /// <summary>
        /// Deletes the Edge Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="edgeModel">The edge model.</param>
        public void DeleteEdge<TEdgeModel>(string collection, TEdgeModel edgeModel)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            this.Context.DeleteEdgeDocument<TEdgeModel>(collection, edgeModel._from,edgeModel._to);
        }




        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public TEdgeModel CreateEdgeDocument<TEdgeModel>(string collectionName, IArangoDbRecordHandle source, IArangoDbRecordHandle target)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            var result = this.Context.CreateEdgeDocument<TEdgeModel>(collectionName, source, target);
            return result;

        }




        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        /// <returns></returns>
        public TEdgeModel CreateEdgeDocument<TEdgeModel>(string collectionName, string sourceId, string targetId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            var result = this.Context.CreateEdgeDocument<TEdgeModel>(collectionName, sourceId, targetId);
            return result;
        }

        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeDocument">The edge document.</param>
        public void CreateEdgeDocument<TEdgeModel>(string collectionName, TEdgeModel edgeDocument)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            this.Context.CreateEdgeDocument<TEdgeModel>(collectionName, edgeDocument);
        }





        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeId">The edge identifier.</param>
        /// <returns></returns>
        public TEdgeModel GetEdgeDocument<TEdgeModel>(string collectionName, string edgeId) where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            var result = this.Context.GetEdgeDocument<TEdgeModel>(collectionName, edgeId);
            return result;
        }

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeId">The edge identifier.</param>
        public void DeleteEdgeDocument<TEdgeModel>(string collectionName, string edgeId) where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            this.Context.DeleteEdgeDocument<TEdgeModel>(collectionName, edgeId);
        }

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        public void DeleteEdgeDocument<TEdgeModel>(string collectionName, IArangoDbRecordHandle sourceId, IArangoDbRecordHandle targetId) where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            this.Context.DeleteEdgeDocument<TEdgeModel>(collectionName, sourceId, targetId);
        }

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        public void DeleteEdgeDocument<TEdgeModel>(string collectionName, string sourceId, string targetId) where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            this.Context.DeleteEdgeDocument<TEdgeModel>(collectionName, sourceId, targetId);
        }

        /// <summary>
        /// Gets the in vertices.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="edgeCollection"></param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TEdgeModel[] GetInEdgeDocuments<TModel, TEdgeModel>(string edgeCollection, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {

            var result = this.Context.GetInEdgeDocuments<TModel, TEdgeModel>(edgeCollection, source);
            return result;
        }
        #endregion




        /// <summary>
        /// Gets the out vertices.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="edgeCollection"></param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TEdgeModel[] GetOutEdgeDocuments<TModel, TEdgeModel>(string edgeCollection, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            var result = this.Context.GetOutEdgeDocuments<TModel, TEdgeModel>(edgeCollection, source);
            return result;
        }

        /// <summary>
        /// Gets the in documents.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="graphName">The edge collection.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TModel[] GetInDocuments<TModel, TEdgeModel>(string graphName, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            var result = this.Context.GetInDocuments<TModel, TEdgeModel>(graphName, source);
            return result;
        }

        /// <summary>
        /// Gets the out documents.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="graphName">The edge collection.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TModel[] GetOutDocuments<TModel, TEdgeModel>(string graphName, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new()
        {
            var result = this.Context.GetOutDocuments<TModel, TEdgeModel>(graphName, source);
            return result;
        }

    }
}