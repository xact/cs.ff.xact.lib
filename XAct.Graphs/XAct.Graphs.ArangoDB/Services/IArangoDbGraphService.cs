namespace XAct.Graphs.Services
{
    using XAct.Data.Services;

    /// <summary>
    /// Contract for a service to manage a Graph within an ArangoDb Database
    /// </summary>
    public interface IArangoDbGraphService : 
        IArangoDbGraphDefinitionOperations, 
        IArangoDbGraphVertexOperations,
        IArangoDbGraphEdgeOperations,
        IHasXActLibService
    {








    }
}