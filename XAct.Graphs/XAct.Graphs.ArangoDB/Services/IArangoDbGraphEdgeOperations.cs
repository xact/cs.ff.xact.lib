namespace XAct.Graphs.Services
{
    using XAct.Data.Models;

    /// <summary>
    /// Contract of Operations implemented by <see cref="IArangoDbGraphService"/>
    /// </summary>
    public interface IArangoDbGraphEdgeOperations
    {

        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        TEdgeModel CreateEdgeDocument<TEdgeModel>(string collectionName, IArangoDbRecordHandle source, IArangoDbRecordHandle target)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        /// <returns></returns>
        TEdgeModel CreateEdgeDocument<TEdgeModel>(string collectionName, string sourceId, string targetId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeDocument">The edge document.</param>
        void CreateEdgeDocument<TEdgeModel>(string collectionName, TEdgeModel edgeDocument)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Creates the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeId">The edge identifier.</param>
        /// <returns></returns>
        TEdgeModel GetEdgeDocument<TEdgeModel>(string collectionName, string edgeId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="edgeId">The edge identifier.</param>
        void DeleteEdgeDocument<TEdgeModel>(string collectionName, string edgeId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        void DeleteEdgeDocument<TEdgeModel>(string collectionName, IArangoDbRecordHandle sourceId, IArangoDbRecordHandle targetId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Deletes the <typeparamref name="TEdgeModel" /> based Document.
        /// </summary>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="targetId">The target identifier.</param>
        void DeleteEdgeDocument<TEdgeModel>(string collectionName, string sourceId, string targetId)
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();


        /// <summary>
        /// Gets the in vertices.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="edgeCollection"></param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TEdgeModel[] GetInEdgeDocuments<TModel, TEdgeModel>(string edgeCollection, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Gets the out vertices.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="edgeCollection"></param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TEdgeModel[] GetOutEdgeDocuments<TModel, TEdgeModel>(string edgeCollection, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Gets the in documents.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="graphName">The edge collection.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TModel[] GetInDocuments<TModel, TEdgeModel>(string graphName, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();

        /// <summary>
        /// Gets the out documents.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TEdgeModel">The type of the edge model.</typeparam>
        /// <param name="graphName">The edge collection.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TModel[] GetOutDocuments<TModel, TEdgeModel>(string graphName, TModel source)
            where TModel : class, IArangoDbRecordHandle, new()
            where TEdgeModel : class, IArangoDbRecordHandle, IArangoDbEdgeRecordHandle, new();


    }
}