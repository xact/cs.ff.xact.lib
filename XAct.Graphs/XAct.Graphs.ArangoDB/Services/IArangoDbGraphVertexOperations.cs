namespace XAct.Graphs.Services
{
    using XAct.Data.Models;

    /// <summary>
    /// Contract of Operations implemented by <see cref="IArangoDbGraphService"/>
    /// </summary>
    public interface IArangoDbGraphVertexOperations
    {


        ///// <summary>
        ///// Gets the context.
        ///// </summary>
        ///// <value>
        ///// The context.
        ///// </value>
        ////IArangoDbContext Context { get; }

        /// <summary>
        /// Determines whether a Vertex Document exists.
        /// <para>
        /// Prefer testing against <see cref="GetVertexDocument{TModel}"/>
        /// </para>
        /// </summary>
        /// <param name="documentId">The document identifier.</param>
        /// <returns></returns>
        bool VertexDocumentExists<TModel>(string documentId)
            where TModel : class, IArangoDbRecordHandle, new();

        /// <summary>
        /// Create or Update a Vertex Collection Document object.
        /// <code>
        /// <![CDATA[
        /// //Create a document as an array, or map it to a Type:
        /// document = new Document("{someProp:\"foo/123\"}");
        ///or
        /// //docuemnt = new Document()
        /// .String("foo", "foo string value 1")
        /// .Int("bar", 12345);
        /// ]]>
        /// </code>
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="collectionName">Name of the collection.</param>
        /// <param name="model">The model to persist.</param>
        void PersistVertexDocument<TModel>(string collectionName, TModel model)
            where TModel : class, IArangoDbRecordHandle, new();


        /// <summary>
        /// Gets a single Vertex Document by it's Id.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        TModel GetVertexDocument<TModel>(string id)
            where TModel : class, IArangoDbRecordHandle, new();


        /// <summary>
        /// Deletes a vertex document.
        /// </summary>
        /// <param name="id"></param>
        void DeleteVertexDocument<TModel>(string id)
            where TModel : class, IArangoDbRecordHandle, new();

        /// <summary>
        /// Deletes a vertex document.
        /// </summary>
        void DeleteVertexDocument<TModel>(TModel model)
            where TModel : class, IArangoDbRecordHandle, new();



    }
}