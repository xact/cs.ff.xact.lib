﻿namespace XAct.Graphs.Api
{
    using XAct.Data.Services;

    /// <summary>
    /// Contract for a service to invoke remote API endpoints.
    /// </summary>
    public interface IAPIGraphRequestsService : IArangoDbGraphDefinitionOperations, IHasXActLibService
    {


    }

}
