﻿namespace XAct.Graphs.Api
{
    using System;
    using System.Linq;
    using System.Net;
    using RestSharp;
    using XAct.Data.Services;
    using XAct.Data.Services.Configuration;
    using XAct.Data.Services.Configuration.Implementations;
    using XAct.Graphs.Services;
    using XAct.Graphs.Services.Api.Models;
    using XAct.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class APIGraphRequestsService : IAPIGraphRequestsService
    {
        private readonly IArangoDbServiceConfiguration _configuration;
        private readonly IArangoDbContextService _adangoDbContextService;
        private string _baseUrl = "_api/gharial";




        /// <summary>
        /// Initializes a new instance of the <see cref="APIGraphRequestsService" /> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="adangoDbContextService">The adango database context service.</param>
        public APIGraphRequestsService(IArangoDbServiceConfiguration configuration, IArangoDbContextService adangoDbContextService)
        {
            _configuration = configuration;
            _adangoDbContextService = adangoDbContextService;
        }

        /// <summary>
        /// Gets a list of existing Graphs in the current Database.
        /// </summary>
        /// <returns></returns>
        public string[] GetGraphNames()
        {
            var restClient = CreateRestApiClient();
            var response = restClient.Execute<ListGraphResponse>(
                new RestRequest
                    {
                        Resource = _baseUrl
                    });

            var data = response.Data;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = data.graphs.Select(x => x.name).ToArray();
                return result;
            }
            return null;
        }

        /// <summary>
        /// Determins if a graph of that name exists in the current database.
        /// </summary>
        /// <param name="graphName"></param>
        /// <returns></returns>
        public bool EnsureGraphExists(string graphName)
        {
            var restClient = CreateRestApiClient();

            var response2 =
                restClient.Execute<GetGraphResponse>(new RestRequest {Resource = _baseUrl + "/" + graphName});

            if (response2.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            //404
            return false;

        }

        /// <summary>
        /// Creates a new  Graph.
        /// </summary>
        /// <param name="graphName">Name of the graph.</param>
        /// <param name="edgeCollectionName">Name of the edge collection.</param>
        /// <param name="sourceVertexCollectionName">Name of the source vertex collection.</param>
        /// <param name="targetVertexCollectionName">Name of the target vertex collection.</param>
        /// <returns></returns>
        public bool CreateGraph(string graphName, string edgeCollectionName, string sourceVertexCollectionName,
                                string targetVertexCollectionName)
        {

            CreateGraphRequest request = 
                new CreateGraphRequest(
                    graphName, 
                    new GraphEdgeDefinition(edgeCollectionName, sourceVertexCollectionName, targetVertexCollectionName));

            var restClient = CreateRestApiClient();

            var restRequest = new RestRequest
                {
                    Method = Method.POST,
                    Resource = _baseUrl,
                    RequestFormat = DataFormat.Json
                };

            restRequest.AddBody(request);

            var response = restClient.Execute<CreateGraphResponse>(restRequest);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                return true;
            }
            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                //already exists.
            }
            return false;
        }


        /// <summary>
        /// Deletes the specified graph if it exists in the current database.
        /// </summary>
        /// <param name="graphName"></param>
        /// <returns></returns>
        public bool DeleteGraph(string graphName)
        {

            var restClient = CreateRestApiClient();

            var response2 =
                restClient.Execute<DropGraphResponse>(new RestRequest
                    {
                        Method = Method.DELETE,
                        Resource = "_api/gharial" + "/" + graphName
                    });

            if (response2.StatusCode == HttpStatusCode.OK)
            {
                return true;

            }
            return false;

        }









        private RestClient CreateRestApiClient()
        {
            


            ArangoDbConnectionInfo t;

            t = _adangoDbContextService.ContextInfo;

            var uriBuilder = new UriBuilder(t.IsSecure ? "https" : "http", t.HostName, t.Port, "_db/" + t.Alias + "/");
            //uriBuilder.UserName="host";
            //uriBuilder.Password = null;

            var baseUrl = uriBuilder.Uri;

            RestSharp.RestClient restClient = new RestClient(baseUrl);
            restClient.Authenticator = new HttpBasicAuthenticator(t.HostName, t.Password);

            return restClient;
        }



    }
}