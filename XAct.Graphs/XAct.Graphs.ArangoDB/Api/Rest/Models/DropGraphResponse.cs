﻿namespace XAct.Graphs.Services.Api.Models
{
    using XAct.Data.Models.Implementations;

#pragma warning disable 1591
    public class DropGraphResponse : ApiResponseBase
    {
        public bool dropped { get; set; }
    }
#pragma warning restore 1591
}