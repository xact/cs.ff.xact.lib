﻿namespace XAct.Graphs.Services.Api.Models
{
#pragma warning disable 1591
    public class CreateGraphRequest : GraphSpecs
    {
        public CreateGraphRequest(string graphName, GraphEdgeDefinition edgeDefinition)
        {
            name = graphName;
            edgeDefinitions.Add(edgeDefinition);
        }
    }
#pragma warning restore 1591


}