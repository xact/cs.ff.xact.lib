﻿namespace XAct.Graphs.Services.Api.Models
{
    using XAct.Data.Models.Implementations;

#pragma warning disable 1591
    public class CreateGraphResponse : ApiResponseBase
    {
        public GraphSpecs graph { get; set; }
    }
#pragma warning restore 1591
}