﻿namespace XAct.Graphs.Services.Api.Models
{
    using System.Collections.Generic;
    using XAct.Data.Models.Implementations;

#pragma warning disable 1591
    public class ListGraphResponse : ApiResponseBase
    {
        public List<GraphSpecs> graphs { get; set; }
    }
#pragma warning restore 1591
}