﻿namespace XAct.Graphs.Services.Api.Models
{
    using System.Collections.Generic;
    using XAct.Data.Models.Implementations;

#pragma warning disable 1591
    public class GraphSpecs : RecordHandleBase
    {
        private List<GraphEdgeDefinition> _edgeDefinitions;
        private List<object> _orphanCollections;

        public string name { get; set; }


        public List<GraphEdgeDefinition> edgeDefinitions
        {
            get { return _edgeDefinitions ?? (_edgeDefinitions = new List<GraphEdgeDefinition>()); }
            set { _edgeDefinitions = value; }
        }

        public List<object> orphanCollections
        {
            get { return _orphanCollections ?? (_orphanCollections = new List<object>()); }
            set { _orphanCollections = value; }
        }

        public GraphSpecs()
        {

        }
#pragma warning restore 1591
    }
}