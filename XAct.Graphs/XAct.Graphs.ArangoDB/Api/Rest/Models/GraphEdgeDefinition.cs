﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Graphs.Services.Api.Models
{
#pragma warning disable 1591
    public class GraphEdgeDefinition
    {
        private List<string> _from;
        private List<string> _to;

        public string collection { get; set; }

        public List<string> from
        {
            get { return _from ?? (_from = new List<string>()); }
            set { _from = value; }
        }

        public List<string> to
        {
            get { return _to ?? (_to = new List<string>()); }
            set { _to = value; }
        }

        public GraphEdgeDefinition()
        {
        }

        public GraphEdgeDefinition(string edgeCollection, string sourceVertexCollection, string targetVertexCollection)
        {
            collection = edgeCollection;
            from.Add(sourceVertexCollection);
            to.Add(targetVertexCollection);
        }

    }
#pragma warning restore 1591

}
