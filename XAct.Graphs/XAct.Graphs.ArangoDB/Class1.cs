﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace XAct.Graphs
//{
//    using System.Net.Http;
//    using System.Net.Http.Headers;

//    public class X
//    {
//        public void Create()
//        {
//            using (var client = new HttpClient())
//            {
//                client.BaseAddress = new Uri("http://http://localhost:8529/");
//                client.DefaultRequestHeaders.Accept.Clear();
//                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

//                // New code:
//                HttpResponseMessage response = await client.GetAsync("_api/gharial");

//                if (response.IsSuccessStatusCode)
//                {
//                    Product product = await response.Content.ReadAsAsync<Product>();

//                    Console.WriteLine("{0}\t${1}\t{2}", product.Name, product.Price, product.Category);
//                }
//            }
//        }
//    }

//    public class HttpClientDispatcher : IDisposable
//    {
//        private HttpClient _client;

//        public void Init(ArangoClientSettings settings)
//        {
//            _client = new HttpClient
//            {
//                BaseAddress = new Uri(settings.GetBaseAddress())
//            };
//        }

//        public ArangoResponse<T> Send<T>(IOperationThatReturns<T> operation)
//        {
//            using (var request = GetRequest(operation))
//            {
//                DateTime begin = DateTime.UtcNow;
//                DateTime end;

//                ArangoResponse<T> arangoResponse;

//                using (var response = _client.SendAsync(request))
//                {
//                    response.Wait();

//                    end = DateTime.UtcNow;

//                    arangoResponse = PostExecute(operation, response.Result);
//                }

//                arangoResponse.Status.ServerDuration = end - begin;

//                return arangoResponse;
//            }
//        }

//        private ArangoResponse<T> PostExecute<T>(IOperationThatReturns<T> operation, HttpResponseMessage response)
//        {
//            var contentTask = response.Content.ReadAsStringAsync();

//            contentTask.Wait();

//            var content = contentTask.Result;

//            if (string.IsNullOrWhiteSpace(content))
//            {
//                throw new InvalidOperationException("Response Content is empty");
//            }

//            JObject deserializeObject = JsonConvert.DeserializeObject<JObject>(content);

//            var responseStatus = new ArangoResponseStatus
//            {
//                Error = deserializeObject.GetValueSafe<bool>("error"),
//                Code = deserializeObject.GetValueSafe<int>("code"),
//                ErrorMessage = deserializeObject.GetValueSafe<string>("errorMessage"),
//                ErrorNum = deserializeObject.GetValueSafe<int>("errorNum"),
//                StatusCode = response.StatusCode
//            };

//            var typedResponse = new ArangoResponse<T>
//            {
//                Status = responseStatus
//            };

//            if (responseStatus.Error)
//            {
//                return typedResponse;
//            }

//            var afterComplete = operation as IGetResultAfterExecution<T>;

//            if (afterComplete != null)
//            {
//                var result = afterComplete.GetResult(deserializeObject, typedResponse);

//                typedResponse.Result = result;
//            }
//            else
//            {
//                JToken result;

//                if (!(operation is IIgnoreResultField) && deserializeObject.TryGetValue("result", out result))
//                {
//                    typedResponse.Result = result.ToObject<T>();
//                }
//                else
//                {
//                    typedResponse.Result = deserializeObject.ToObject<T>();
//                }
//            }

//            return typedResponse;
//        }

//        private HttpRequestMessage GetRequest<T>(IOperationThatReturns<T> operation)
//        {
//            var resource = operation.Resource.Resource;

//            var withParameters = operation as IWithUrlParameters;

//            if (withParameters != null)
//            {
//                var parameters = withParameters.GetUrlParameters();

//                resource = resource.TokenReplace(parameters);
//            }

//            HttpRequestMessage request = new HttpRequestMessage(operation.Resource.Method, resource);

//            var withBody = operation as IWithBody;

//            if (withBody != null)
//            {
//                object body = withBody.GetBody();

//                if (body is Stream)
//                {
//                    request.Content = new StreamContent((Stream)body);

//                    return request;
//                }

//                string json;

//                if (body is string)
//                {
//                    json = body.ToString();
//                }
//                else
//                {
//                    json = JsonConvert.SerializeObject(body);
//                }

//                request.Content = new StringContent(json, Encoding.UTF8, "application/json");
//            }

//            return request;
//        }

//        public void Dispose()
//        {
//            if (_client != null)
//            {
//                _client.Dispose();
//            }
//        }
//    }
//}
