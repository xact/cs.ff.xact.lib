// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;


    public interface IGraph : IHasIdReadOnly<Guid>, IHasInnerItemReadOnly { }

    /// <summary>
    /// Contract for a wrapper of a QuickGraph library.
    /// <para>
    /// Wrapped in order to not cause Quickgraph dependencies to drag out to other assemblies.
    /// </para>
    /// </summary>
    /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
    /// <typeparam name="TGraphEdge">The type of the graph edge.</typeparam>
    public interface IGraph<TGraphVertex, TGraphEdge> : 
        IGraph,
        IGraphForIncompleteVertices<TGraphVertex>, 
        IMRUGraph<TGraphVertex, TGraphEdge> 
        where TGraphVertex : class, IGraphVertex, new() 
        where TGraphEdge : class, IGraphEdge<TGraphVertex>, new()
    {




        /// <summary>
        /// Edges the count.
        /// </summary>
        /// <returns></returns>
        int EdgeCount();

        /// <summary>
        /// Vertexes the count.
        /// </summary>
        /// <returns></returns>
        int VertexCount();

        
        void LoadGraphData();
        void LoadVertices(params Guid[] ids);

        void LoadVertices(params IHasDataSourceIdentifier[] dataSourceIdentifiers);

        void Persist();

        /// <summary>
        /// Adds the vertex to the graph,
        /// and underlying DataStore.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        void AddVertex(TGraphVertex vertex);


        ///// <summary>
        ///// Removes the vertex.
        ///// </summary>
        ///// <internal>
        ///// Not needed, as already defined in <see cref="IMRUGraph{TGraphVertex,TGraphEdge}"/>
        ///// </internal>
        ///// <param name="vertex">The vertex.</param>
        //void RemoveVertex(TGraphVertex vertex);

        /// <summary>
        /// Adds the edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        void AddEdge(TGraphEdge edge);

        /// <summary>
        /// Removes the edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        void RemoveEdge(TGraphEdge edge);



        ///// <summary>
        ///// Removes the vertex from the graph.
        ///// </summary>
        ///// <internal>
        ///// Not needed, as already defined in <see cref="IMRUGraph{TGraphVertex,TGraphEdge}"/>
        ///// </internal>
        ///// <param name="vertex">The vertex.</param>
        //void RemoveVertex(TGraphVertex vertex);

        ///// <summary>
        ///// Gets the given vertex's in edges.
        ///// </summary>
        ///// <internal>
        ///// Not needed, as already defined in <see cref="IMRUGraph{TGraphVertex,TGraphEdge}"/>
        ///// </internal>
        ///// <param name="vertex">The vertex.</param>
        ///// <returns></returns>
        //IEnumerable<TGraphEdge> GetInEdges(TGraphVertex vertex);

        ///// <summary>
        ///// Gets the given vertex's out edges.
        ///// </summary>
        ///// <param name="vertex">The vertex.</param>
        ///// <returns></returns>
        //IEnumerable<TGraphEdge> GetOutEdges(TGraphVertex vertex);


        /// <summary>
        /// Gets the edges (in and out) for the vertex.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        IEnumerable<TGraphEdge> GetAllEdges(TGraphVertex vertex);


        /// <summary>
        /// Shortests the path dijkstra.
        /// </summary>
        /// <param name="rootVertex">The root vertex.</param>
        /// <param name="targetVertex">The target vertex.</param>
        /// <param name="shortestPathAlgorithm">The shortest path algorithm.</param>
        /// <returns></returns>
        IEnumerable<TGraphEdge> ShortestPathDijkstra(TGraphVertex rootVertex, TGraphVertex targetVertex,
                                                     //Func<IQGEdgeShim<TGraphVertex>, double> edgeCost = null,
                                                     ShortestPathAlgorithm shortestPathAlgorithm =
                                                         ShortestPathAlgorithm.Dijkstra);

        /// <summary>
        /// Topologicals the sort.
        /// </summary>
        /// <returns></returns>
        IEnumerable<TGraphVertex> TopologicalSort();

    }
}