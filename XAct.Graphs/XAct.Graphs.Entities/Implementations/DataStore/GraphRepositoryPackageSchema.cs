﻿// ReSharper disable CheckNamespace

using XAct.Entities;

namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Entity used to register and create a new 
    /// <see cref="GraphPackage{TGraphVertex,TGraphEdge>"/>
    /// using <c>IGraphService.Create</c>
    /// </summary>
    [DataContract]
    public class GraphRepositoryPackageSchema : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp,  
        IHasApplicationTennantId,
        IHasNameAndDescription
    {

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }

        /// <summary>
        /// Gets the name of the graph.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="GraphRepositoryPackageSchema"/> class.
        /// </summary>
        public GraphRepositoryPackageSchema()
        {
            this.GenerateDistributedId();
        }

    }
}
