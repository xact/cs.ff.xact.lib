﻿// ReSharper disable CheckNamespace

using XAct.Entities;

namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A DTO edge object the Repository retrieves,
    /// as part of <see cref="GraphRepositoryVertex"/>
    /// objects, which the invoker 
    /// (<c>IGraphPersistenceService</c>)
    /// converts to <see cref="GraphEdge"/> objects
    /// before <c>IGraphService</c> puts them in the required graph.
    /// </summary>
    /// <internal>
    /// The decision to not use the same EF object for the repository 
    /// and the graph was due to two factors: keeping the graph entities
    /// small (less vars and no change tracking), as well as the EF vertex 
    /// entities have in/out edges (allowing for edges being picked up in the
    /// same request as the vertex), whereas QuickGraph stores its edges
    /// in a separate list, and would not use them.
    /// </internal>
    [DataContract]
    public partial class GraphRepositoryEdge : IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp, 
        //Maybe not needed as GraphId is enough filtering? IHasApplicationTennantId, 
        IHasWeight /*IHasGraphId*/
    {

        /// <summary>
        /// Gets or sets the Datastore identifier for this edge.
        /// <para>
        /// Implementation of <see cref="IHasGuidIdentity"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }



        //[DataMember]
        //public Guid ApplicationTennantId { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        ///// <summary>
        ///// Gets or sets the graph this Edge belongs to.
        ///// </summary>
        ///// <value>
        ///// The graph identifier.
        ///// </value>
        //public Guid GraphId { get; set; }


        /// <summary>
        /// Gets or sets the weight of the object.
        /// <para>
        /// Defined in the <see cref="IHasWeight"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        /// <internal>
        /// In many way's I should not be putting Weight (same for Tag)
        /// this far down...but frankly don't much see the point of having
        /// a Node without *some* form of data...
        /// </internal>
        [DataMember]
        public virtual int Weight { get; set; }




        /// <summary>
        /// Gets the source Vertex.
        /// </summary>
        /// <internal>
        /// When dealing with EF, 
        /// Inheritors should provide a <c>TargetId</c>
        /// or <c>SourceFK</c>. It's not abs required,
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        [DataMember]
        public virtual Guid SourceFK { get; set; }
        
        public /*NO:virtual*/ GraphRepositoryVertex Source { get { return _source; } set { _source = value; } }
        [DataMember]
        private GraphRepositoryVertex _source;


        /// <summary>
        /// Gets the target Vertex.
        /// </summary>
        /// <internal>
        /// When dealing with EF, 
        /// Inheritors should provide a <c>TargetId</c>
        /// or <c>SourceFK</c>. It's not abs required,
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        [DataMember]
        public virtual Guid TargetFK { get; set; }

        public /*NO:virtual*/ GraphRepositoryVertex Target { get { return _target; } set { _target = value; } }
        [DataMember]
        private GraphRepositoryVertex _target;


        /// <summary>
        /// Determines whether [is source retrieved].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is source retrieved]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsSourceRetrieved()
        {
            return (_source != null);
        }
        /// <summary>
        /// Determines whether [is target retrieved].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is target retrieved]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsTargetRetrieved()
        {
            return (_target != null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphRepositoryEdge"/> class.
        /// </summary>
        public GraphRepositoryEdge()
        {
            this.GenerateDistributedId();
        }

    }
}
