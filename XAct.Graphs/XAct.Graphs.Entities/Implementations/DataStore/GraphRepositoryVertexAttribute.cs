// ReSharper disable CheckNamespace

using XAct.Entities;

namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A base class to define key/values associated to a single Graph entity.
    /// <para>
    /// Contains an identity that allows filtering the Value per User.
    /// <para>
    /// Empty Guids are Values are Common/base values shared by all users.
    /// </para>
    /// </para>
    /// </summary>
    [DataContract]
    public class GraphRepositoryVertexAttribute: IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp, 
        //IHasApplicationTennantId,
        IHasSerializedTypeValueAndMethod, IHasUserIdentifier
    {
        /// <summary>
        /// Gets or sets the datastore id of this 
        /// <see cref="GraphRepositoryVertexAttribute"/>.
        /// <para>
        /// The Guid can be a normal guid, or a Guid returned
        /// by <c>IDistributedIdentityService</c>
        /// </para>
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public virtual Guid Id { get; set; }


        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        //[DataMember]
        //public virtual Guid ApplicationTennantId { get; set; }

        /// <summary>
        /// Gets or sets the type of the element.
        /// </summary>
        /// <value>
        /// The type of the element.
        /// </value>
        [DataMember]
        public virtual GraphElementType ElementType { get;set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// <para>
        /// Member defined in the <see cref="IHasUserIdentifier" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public virtual string UserIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the Vertex/Edge Element's id.
        /// </summary>
        /// <value>
        /// The id of the ownder Edge.
        /// </value>
        [DataMember]
        public virtual Guid ElementId { get; set; }


        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }
        

        /// <summary>
        /// Gets or sets the serialization method.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The serialization method.
        /// </value>
        [DataMember]
        public virtual SerializationMethod SerializationMethod { get; set; }
        

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember]
        public virtual string SerializedValueType { get; set; }

        /// <summary>
        /// The SerializedValue.
        /// </summary>
        [DataMember]
        public virtual string SerializedValue { get; set; }


        public GraphRepositoryVertexAttribute()
        {
            this.GenerateDistributedId();
        }
    }
}