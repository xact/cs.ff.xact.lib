﻿// ReSharper disable CheckNamespace

namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// </internal>
    public class GraphEdge : GraphEdge<GraphVertex>, IGraphEdge<GraphVertex>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GraphEdge{TGraphVertex}" /> class.
        /// </summary>
        public GraphEdge()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphEdge{TGraphVertex}" /> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        public GraphEdge(GraphVertex source, GraphVertex target)
            : base(source, target)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphEdge{TGraphVertex}" /> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        public GraphEdge(Guid source, Guid target)
            : base(source, target)
        {
        }

        
    }
}
