﻿
// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// The values are immutable in order to take up minimal memory 
    /// space (there are often lots of Attributes in mem
    /// at any one time).
    /// <para>
    /// Attribute values are updated using implementations
    /// of
    /// <c>IGraphVertexAttributeService</c>
    /// and 
    /// <c>IGraphEdgeAttributeService</c>
    /// </para>
    /// </internal>
    public class GraphEntityAttribute2 : IHasKeyValueReadOnly<object>
    {
        /// <summary>
        /// Gets or sets the per-user, per-vertex(or edge) attribute key.
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string Key { get { return _key; } }
        private readonly string _key;

        /// <summary>
        /// Gets or sets the per-user, per-vertex(or edge) attribute value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value { get { return _value; } }
        private readonly object _value;

        /// <summary>
        /// Graphes the entity attribute2.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public GraphEntityAttribute2 (string key, object value )
        {
            _key = key;
            _value = value;
        }
    }

}
