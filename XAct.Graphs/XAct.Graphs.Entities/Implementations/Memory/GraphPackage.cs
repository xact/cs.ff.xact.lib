﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    public class GraphPackage<TGraphVertex,TGraphEdge> : IGraphPackage<TGraphVertex,TGraphEdge>
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class, IGraphEdge<TGraphVertex>, new()
        
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public virtual Guid Id { get; private set; }
        
        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        public virtual string Name { get; private set; }
        
        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public virtual string Description { get; private set; }


        /// <summary>
        /// Gets the bidirectional graph.
        /// </summary>
        /// <value>
        /// The graph.
        /// </value>
        public virtual IGraph<TGraphVertex, TGraphEdge> Graph { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="GraphPackage{TGraphVertex,TGraphEdge}" /> class.
        /// </summary>
        /// <param name="graphPackageSchema">The graph package schema.</param>
        /// <param name="graph">The graph.</param>
        public GraphPackage(GraphRepositoryPackageSchema graphPackageSchema, IGraph<TGraphVertex, TGraphEdge> graph)
            : this(graphPackageSchema.Id, graphPackageSchema.Name, graphPackageSchema.Description, graph)
        {
            
        } 

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphPackage{TGraphVertex,TGraphEdge}" /> class.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="graph">The graph.</param>
        public GraphPackage(Guid id, string name, string description, IGraph<TGraphVertex, TGraphEdge> graph)
        {
            //this.GenerateId();
            Id = id;
            Name = name;
            Description = description;
            Graph = graph;
        }

    }
}
