// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// An abstract implementation of
    /// <see cref="IGraphEdge{TGraphVertex}"/>
    /// <para>
    /// Does not implement the QG.IEdge interface 
    /// (directly or implicitly) so that a Reference
    /// to the QG lib is not needed up and down the stack.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// To use QG, wrap this Edge in a <c>QGEdgeShim{TVertex}</c>
    /// before adding it to a QG graph.
    /// </para>
    /// <para>
    /// Intentionally left abstract so that end developers
    /// always define their own Edges and Vertices, in their
    /// own assembly -- so that they don't use the ones defined
    /// here. By doing so, they can leave this assembly being
    /// talked to by only App.Infrastructure.Svcs.
    /// </para>
    /// <para>
    /// If entities were defined here, and they used it, all assemblies
    /// would have to nuget xact.graphs -- and end up with a ref to
    /// QG...
    /// </para>
    /// </remarks>
    /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
    public abstract class GraphEdge<TGraphVertex> :
        IGraphEdge<TGraphVertex>,
        IHasGuidIdentity,
        IHasTag
        where TGraphVertex : class, IGraphVertex, new()
    {

        /// <summary>
        /// Gets or sets the graph this Edge belongs to.
        /// </summary>
        /// <value>
        /// The graph identifier.
        /// </value>
        public IGraphForIncompleteVertices<TGraphVertex> Graph { get; set; }

        /// <summary>
        /// Gets or sets the Datastore identifier for this edge.
        /// <para>
        /// Implementation of <see cref="IHasGuidIdentity"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the weight of the object.
        /// <para>
        /// Defined in the <see cref="IHasWeight"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        /// <internal>
        /// In many way's I should not be putting Weight (same for Tag)
        /// this far down...but frankly don't much see the point of having
        /// a Node without *some* form of data...
        /// </internal>
        public virtual int Weight { get; set; }

        /// <summary>
        /// Gets or sets an optional custom tag or note associated with an object.
        /// <para>
        /// On simple nodes, can be used to bury some custom data.
        /// </para>
        /// <para>
        /// Defined in the <see cref="IHasTag"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The optional tag.
        /// </value>
        /// <internal>
        /// In many way's I should not be putting Weight (same for Tag)
        /// this far down...but frankly don't much see the point of having
        /// a Node without *some* form of data...
        /// </internal>
        public virtual string Tag { get; set; }



        /// <summary>
        /// Gets or sets the source vertex id.
        /// </summary>
        /// <value>
        /// The source id.
        /// </value>
        /// <internal>
        /// In mem Edge entities don't need the SourceId -- just the Source.
        /// But EF is happier when an entity also exposes the Id
        /// </internal>
        /// <internal>
        /// It's not abs required for EF
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        public virtual Guid SourceFK
        {
            get { return _sourceFK; }
            set
            {
                if (_sourceFK != Guid.Empty)
                {
                    throw new ArgumentException("SourceFK already been set.");
                }
                _sourceFK = value;
            }
        }

        private Guid _sourceFK;

        /// <summary>
        /// Gets or sets the target vertex id.
        /// </summary>
        /// <value>
        /// The target id.
        /// </value>
        /// <internal>
        /// In mem Edge entities don't need the SourceId -- just the Source.
        /// But EF is happier when an entity also exposes the Id
        /// </internal>
        /// <internal>
        /// It's not abs required for EF
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        public virtual Guid TargetFK
        {
            get { return _targetFK; }
            set
            {
                if (_targetFK != Guid.Empty)
                {
                    throw new ArgumentException("TargetFK already been set.");
                }
                _targetFK = value;
            }
        }
        private Guid _targetFK;




        /// <summary>
        /// Gets or sets the source Vertex.
        /// <para>
        /// Cannot set/reset Target once set.
        /// </para>
        /// <para>
        /// Vertex Id must match Id of <see cref="Source"/>.
        /// </para>
        /// </summary>
        /// <internal>
        /// When dealing with EF, 
        /// Inheritors should provide a <c>TargetId</c>
        /// or <c>SourceFK</c>. It's not abs required,
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        public virtual TGraphVertex Source
        {
            get
            {
                if (_source == null)
                {
                    _source = new TGraphVertex { Id = TargetFK };
                }

                //PrepareVertex has to be on the the interface
                //that QuickGraph exposes, so that invocations
                //by it, cause it to hydrate records.

                if (_source != null)
                {
                    this.Graph.CompletePartialVertex(_source);
                }
                return _source;
            }
            set
            {
                if ((value == null)||(value.Id != SourceFK))
                {
                    throw new ArgumentException("Invalid Source.");
                }
                _source = value;

            }
        }

        private TGraphVertex _source;


        /// <summary>
        /// Gets or sets the target Vertex.
        /// <para>
        /// Cannot set/reset Target once set.
        /// </para>
        /// <para>
        /// Vertex Id must match Id of <see cref="Source"/>.
        /// </para>
        /// </summary>
        /// <internal>
        /// When dealing with EF, 
        /// Inheritors should provide a <c>TargetId</c>
        /// or <c>SourceFK</c>. It's not abs required,
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        public virtual TGraphVertex Target
        {
            get
            {
                if (_target == null)
                {
                    _target = new TGraphVertex {Id = TargetFK};
                }
                if (_target != null)
                {
                    this.Graph.CompletePartialVertex(_target);
                }
                return _target;
            }
            set
            {
                if ((value == null) || (value.Id != TargetFK))
                {
                    throw new ArgumentException("Invalid Source.");
                }
                _target = value;
            }
        }

        private TGraphVertex _target;


        /// <summary>
        /// Initializes a new instance of the <see cref="GraphEdge{TGraphVertex}" /> class.
        /// </summary>
        protected GraphEdge()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphEdge{TGraphVertex}" /> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        protected GraphEdge(TGraphVertex source ,TGraphVertex target)
        {
            _source = source;
            _target = target;

            _sourceFK = _source.Id;
            _targetFK = _target.Id;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphEdge{TGraphVertex}" /> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        protected GraphEdge(Guid source, Guid target)
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            SourceFK = source;
            TargetFK = target;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string sourceTag = (_source != null)&&(!_source.Tag.IsNullOrEmpty()) ? _source.Tag:".";
            string targetTag = (_target != null)&&(!_target.Tag.IsNullOrEmpty()) ? _target.Tag:".";

            return "'{0}'[{2}]->'{1}'[{3}]".FormatStringInvariantCulture(_sourceFK, _targetFK, sourceTag,targetTag);
        }

    }
}