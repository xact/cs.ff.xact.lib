﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a graph element to define which graph it belongs to,
    /// in order to use it when invoking the GraphPersistenceService 
    /// to Complete itself.
    /// </summary>
    /// <internal>
    /// Implemented by <see cref="IGraphVertex"/>,
    /// and maybe also by <see cref="IGraphEdge{TGraphVertex}"/>
    /// <para>
    /// IMPORTANT:
    /// It has to be a Guid, and not IGraph{V,T} as it is applied
    /// to a V...which knows about itself, but doesn't know about
    /// E...therefore is not enough to build up generic signature
    /// of Graph.
    /// </para>
    /// </internal>
    public interface IHasGraphId
    {
        /// <summary>
        /// Gets or sets the id of the graph this entity belongs to.
        /// </summary>
        /// <value>
        /// The graph id.
        /// </value>
        Guid GraphId { get; set; }

    }
}
