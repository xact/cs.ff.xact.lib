﻿
// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    public interface IGraphVertex : IHasGuidIdentity, IHasTag, IHasGraphId, IHasDataSourceIdentifier, IHasIncomplete
    {

    }

}
