﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for Edges added to the graph
    /// that
    /// <c>IGraphService</c>
    /// manages.
    /// </summary>
    /// <internal>
    /// Intentionaly does *not* inherit from and instead intentially
    /// Mirrors exactly the QuickGraph contract: 
    /// <![CDATA[
    ///  public interface IEdge<TVertex>{
    ///   TVertex Source { get; }
    ///   TVertex Target { get; }
    /// }
    /// ]]>
    /// </internal>
    /// <typeparam name="TGraphVertex">The type of the graph vertex.</typeparam>
// ReSharper disable TypeParameterCanBeVariant
    //Do not make it out (IGraphForImpleteVertices needs it as in)
    public interface IGraphEdge <TGraphVertex> : IHasId<Guid>, IHasWeight 
// ReSharper restore TypeParameterCanBeVariant
        where TGraphVertex : class, IGraphVertex, new()
    {



        /// <summary>
        /// Gets or sets the source vertex id.
        /// </summary>
        /// <value>
        /// The source id.
        /// </value>
        /// <internal>
        /// In mem Edge entities don't need the SourceId -- just the Source.
        /// But EF is happier when an entity also exposes the Id
        /// </internal>
        /// <internal>
        /// It's not abs required for EF
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        Guid SourceFK { get; set; }

        /// <summary>
        /// Gets or sets the target vertex id.
        /// </summary>
        /// <value>
        /// The target id.
        /// </value>
        /// <internal>
        /// In mem Edge entities don't need the SourceId -- just the Source.
        /// But EF is happier when an entity also exposes the Id
        /// </internal>
        /// <internal>
        /// It's not abs required for EF
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        Guid TargetFK { get; set; }


        /// <summary>
        /// Gets or sets the graph this Edge belongs to.
        /// </summary>
        /// <value>
        /// The graph identifier.
        /// </value>
        IGraphForIncompleteVertices<TGraphVertex> Graph { get; set; }

        /// <summary>
        /// Gets or sets the source vertex.
        /// <para>
        /// Cannot set/reset Target once set.
        /// </para>
        /// <para>
        /// Vertex Id must match Id of <see cref="Source"/>.
        /// </para>
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        /// <internal>
        /// When dealing with EF, 
        /// Inheritors should provide a <c>SourceId</c>
        /// or <c>SourceFK</c>. It's not abs required,
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        TGraphVertex Source { get; set; }


        /// <summary>
        /// Gets or sets the target vertex.
        /// <para>
        /// Cannot set/reset Target once set.
        /// </para>
        /// <para>
        /// Vertex Id must match Id of <see cref="Source"/>.
        /// </para>
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        /// <internal>
        /// When dealing with EF, 
        /// Inheritors should provide a <c>TargetId</c>
        /// or <c>SourceFK</c>. It's not abs required,
        /// but allows for developers to economically set the SourceId
        /// without having to call back to server to get the entity.
        /// </internal>
        TGraphVertex Target { get; set; }


    }
}
