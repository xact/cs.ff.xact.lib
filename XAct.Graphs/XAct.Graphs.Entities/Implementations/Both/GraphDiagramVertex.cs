﻿namespace XAct.Graphs
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GraphDiagramVertex : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasIntCoordinate
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the X axis value.
        /// </summary>
        [DataMember]
        public virtual int X { get; set; }
        
        /// <summary>
        /// Gets or sets the Z axis value.
        /// </summary>
        [DataMember]
        public virtual int Y { get; set; }

        /// <summary>
        /// Gets or sets the Z axis or layer value.
        /// </summary>
        [DataMember]
        public virtual int Z { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="GraphDiagramVertex"/> class.
        /// </summary>
        public GraphDiagramVertex()
        {
            this.GenerateDistributedId();
        }
    }
}