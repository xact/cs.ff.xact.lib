﻿namespace XAct.Graphs
{
    using System.Runtime.Serialization;
    using XAct;
    using System;

    /// <summary>
    /// A Diagram of nodes.
    /// </summary>
    [DataContract]
    public class GraphDiagram : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasNameAndDescription
    {
        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphDiagram"/> class.
        /// </summary>
        public GraphDiagram()
        {
            this.GenerateDistributedId();
        }
    }
}
