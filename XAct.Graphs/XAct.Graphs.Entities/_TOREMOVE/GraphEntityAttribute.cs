//using System;

//// ReSharper disable CheckNamespace
//namespace XAct.Graphs
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// A base class to define key/values associated to a single Graph entity.
//    /// <para>
//    /// Contains an identity that allows filtering the Value per User.
//    /// <para>
//    /// Empty Guids are Values are Common/base values shared by all users.
//    /// </para>
//    /// </para>
//    /// </summary>
//    public class GraphEntityAttribute
//        : IHasGuidIdentity, IHasSerializedTypeValueAndMethod, IHasUserIdentifier
//    {
//        /// <summary>
//        /// Gets or sets the datastore id of this 
//        /// <see cref="GraphEntityAttribute"/>.
//        /// <para>
//        /// The Guid can be a normal guid, or a Guid returned
//        /// by <c>IDistributedIdentityService</c>
//        /// </para>
//        /// </summary>
//        /// <value>The id.</value>
//        public Guid Id { get; set; }



//        /// <summary>
//        /// Gets or sets the user identifier.
//        /// <para>
//        /// Member defined in the <see cref="IHasUserIdentifier" /> contract.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The user identifier.
//        /// </value>
//        public string UserIdentifier { get; set; }

//        /// <summary>
//        /// Gets or sets the owner Edge's id.
//        /// </summary>
//        /// <value>
//        /// The id of the ownder Edge.
//        /// </value>
//        public Guid EntityId { get; set; }



//        /// <summary>
//        /// Gets or sets the type of the entity
//        /// this attribute is attached to.
//        /// </summary>
//        /// <value>
//        /// The type of the entity.
//        /// </value>
//        public GraphEntity EntityType { get { return (GraphEntity)EntityTypeInt; } set { EntityTypeInt = (int) value; } }
//        public int EntityTypeInt { get; set; }


//        /// <summary>
//        /// Gets or sets the key.
//        /// </summary>
//        /// <value>
//        /// The key.
//        /// </value>
//        public virtual string Key { get; set; }


        

//        /// <summary>
//        /// Gets or sets the serialization method.
//        /// <para>
//        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The serialization method.
//        /// </value>
//        public SerializationMethod SerializationMethod
//        {
//            get { return (SerializationMethod) SerializationMethodRaw; }
//            set { SerializationMethodRaw = (int) value; }
//        }

//        /// <summary>
//        /// Gets or sets the int backing <see cref="SerializationMethod"/>
//        /// (as EF can't serialize enums).
//        /// </summary>
//        /// <value>
//        /// The serialization method raw.
//        /// </value>
//        public int SerializationMethodRaw { get; set; }


        

//        /// <summary>
//        /// Gets or sets the type.
//        /// </summary>
//        /// <value>The type.</value>
//        public virtual string SerializedValueType { get; set; }

//        /// <summary>
//        /// The SerializedValue.
//        /// </summary>
//        public virtual string SerializedValue { get; set; }


//    }
//}