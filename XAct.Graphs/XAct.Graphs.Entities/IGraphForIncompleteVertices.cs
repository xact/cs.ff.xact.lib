﻿
// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract that makes Graphs capable of calling
    /// back to the Graph in order to Complete
    /// Vertices marked with <see cref="IHasIncomplete"/>
    /// </summary>
    public interface IGraphForIncompleteVertices<TGraphVertex>
        where TGraphVertex : class, IGraphVertex, new()
    {
        /// <summary>
        /// Prepares the specified vertex.
        /// <para>
        /// Invoked by <see cref="GraphEdge{TVertex}"/>
        /// Source and Target properties.
        /// </para>
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        void CompletePartialVertex(TGraphVertex vertex);
    }
}
