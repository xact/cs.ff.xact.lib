namespace XAct.Graphs
{
    /// <summary>
    /// Enumeration of Entity types.
    /// </summary>
    public enum GraphElementType
    {
        /// <summary>
        /// The entity is an Vertex
        /// </summary>
        Vertex,

        /// <summary>
        /// The entity is an Edge
        /// </summary>
        Edge,

    }
}