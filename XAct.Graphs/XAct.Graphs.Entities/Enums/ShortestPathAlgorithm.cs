namespace XAct.Graphs
{
    /// <summary>
    /// 
    /// </summary>
    public enum ShortestPathAlgorithm
    {
        /// <summary>
        /// The dijkstra
        /// </summary>
        Dijkstra=1,
        /// <summary>
        /// The bellman ford
        /// </summary>
        BellmanFord=2,
        /// <summary>
        /// The dag
        /// </summary>
        Dag=3

    }
}