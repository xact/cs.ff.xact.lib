﻿
namespace XAct.Graphs
{
    /// <summary>
    /// The type of graph.
    /// </summary>
    public enum GraphType
    {
        /// <summary>
        /// No value selected.
        /// <para>
        /// An error state.
        /// </para>
        /// <para>
        /// Value =0
        /// </para>
        /// </summary>
        Undefined=0,

        /// <summary>
        /// Create a Bidirectional graph.
        /// <para>
        /// Value =1
        /// </para>
        /// </summary>
        Bidirectional=1
    }
}
