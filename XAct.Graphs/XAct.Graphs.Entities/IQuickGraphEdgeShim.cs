namespace XAct.Graphs
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TVertex">The type of the vertex.</typeparam>
    public interface IQuickGraphEdgeShim<TVertex> : IHasInnerItemReadOnly  //<IGraphEdge<TVertex>>
        where TVertex : class, IGraphVertex, new()
    {
        
    }
}