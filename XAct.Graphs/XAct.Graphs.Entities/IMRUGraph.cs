// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for an <see cref="IGraph{TGraphVertex,TGraphEdge}"/>
    /// that can be pruned using an MRU list.
    /// </summary>
    /// <typeparam name="TGraphVertex">The type of the vertex.</typeparam>
    /// <typeparam name="TGraphEdge">The type of the edge.</typeparam>
    public interface IMRUGraph<TGraphVertex, TGraphEdge>
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class, IGraphEdge<TGraphVertex>, new()
    {


        /// <summary>
        /// Removes the vertex from the graph.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        void RemoveVertex(TGraphVertex vertex);

        /// <summary>
        /// Gets the given vertex's in edges.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        IEnumerable<TGraphEdge> GetInEdges(TGraphVertex vertex);

        /// <summary>
        /// Gets the given vertex's out edges.
        /// </summary>
        /// <param name="vertex">The vertex.</param>
        /// <returns></returns>
        IEnumerable<TGraphEdge> GetOutEdges(TGraphVertex vertex);
    }
}