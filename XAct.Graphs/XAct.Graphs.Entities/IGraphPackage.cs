﻿// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a Graph handle (ie, name, description, etc. + inner Bidirectional graph).
    /// </summary>
    public interface IGraphPackage<TGraphVertex, TGraphEdge> : IHasIdReadOnly<Guid>, IHasNameAndDescriptionReadOnly
        where TGraphVertex : class, IGraphVertex, new()
        where TGraphEdge : class, IGraphEdge<TGraphVertex>, new()
    {

        /// <summary>
        /// Gets the Graph that belongs to this defined and registered Package.
        /// </summary>
        /// <value>
        /// The <see cref="IGraph{TGraphVertex,TGraphEdge}"/>.
        /// </value>
        IGraph<TGraphVertex, TGraphEdge> Graph { get;  } 

    }
}
