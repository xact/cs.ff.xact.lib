﻿namespace XAct.Tests.Counters.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Initialization.DbContextSeeders;
    using XAct.Environment;
    using XAct.Library.Settings;
    using XAct.Metrics;
    using XAct.Services;

    public class CounterDbContextSeeder : XActLibDbContextSeederBase<Counter>, ICounterDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly IDateTimeService _dateTimeService;

        Guid ApplicationTennantId
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>().Get(); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CounterDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        public CounterDbContextSeeder(ITracingService tracingService,IDateTimeService dateTimeService)
            : base(tracingService)
        {
            _dateTimeService = dateTimeService;
        }

        public override void SeedInternal(DbContext dbContext)
        {

            SeedInternalHelper(dbContext, true, x => new {x.ApplicationTennantId, x.Target, x.Key});
        }

        public override void CreateEntities() 
        {
            this.InternalEntities = new List<Counter>();



            this.InternalEntities.Add(
                new Counter
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = this.ApplicationTennantId,
                    Key = "CounterA",
                    Target = null,
                    Value = 123,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                });

            this.InternalEntities.Add(
                new Counter
                    {
                        Id = Guid.NewGuid(),
                        ApplicationTennantId = this.ApplicationTennantId,
                        Key = "CounterA",
                        Target = "ssigal",
                        Value = 123,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                    });
            

            this.InternalEntities.Add(
                new Counter
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = this.ApplicationTennantId,
                    Key = "CounterA",
                    Target = "bjones",
                    Value = 234,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                });
            

            this.InternalEntities.Add(
                new Counter
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = this.ApplicationTennantId,
                    Key = "CounterB",
                    Target = "ssigal",
                    Value = 456,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                });




        }
    }
}
