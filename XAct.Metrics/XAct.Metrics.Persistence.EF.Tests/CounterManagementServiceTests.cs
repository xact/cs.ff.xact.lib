namespace XAct.Metric.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Metrics;
    using XAct.Metrics.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class CounterManagementServiceTests
    {
        private readonly string _fakeUserId = "ssigal";

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void Can_Get_CounterService()
        {
            ICounterService service = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            Assert.IsNotNull(service);
        }

        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void CounterService_Is_AvailableAndExpectedType()
        {
            ICounterService service = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            Assert.AreEqual(typeof(CounterService), service.GetType());
        }

        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void CanGetUsersValueForCounterA()
        {

            string key = "CounterA";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            int score = counterService.GetValue(key, _fakeUserId);


            Assert.IsTrue(score > 0, "Test #1");
            Assert.IsTrue(score < 130, "Test #2");
        }

        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void CanGetUsersValueForNonExistentCounter()
        {

            string key = "CounterX";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

#pragma warning disable 168
            int score = counterService.GetValue(key, _fakeUserId);
#pragma warning restore 168


            Assert.IsTrue(true);
        }


        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void CanSetUsersValueForNonExistentCounter()
        {

            string key = "CounterY";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            counterService.SetValue(key, 123456, _fakeUserId);

            int score = counterService.GetValue(key, _fakeUserId);

            Assert.AreEqual(123456, score);
        }



        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void CanAddValueToAUser()
        {

            string key = "CounterA";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            int score = counterService.GetValue(key, _fakeUserId);

            Assert.IsTrue(score > 0, "Test #1");
            Assert.IsTrue(score < 130, "Test #2"); //Proves it is not for "bjones" which is different user/same key

            counterService.SetValue(key, score + 2, _fakeUserId);

            int score2 = counterService.GetValue(key, _fakeUserId);

            Assert.AreEqual(score + 2, score2);
        }

        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void CanGetAppValueForExistentCounter()
        {

            string key = "CounterA";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            int score = counterService.GetValue(key);

            Assert.IsTrue(score > 0);
        }


        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void CanGetAppValueForNonExistentCounter()
        {

            string key = "CounterY";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            counterService.GetValue(key);



            Assert.IsTrue(true);
        }



        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void Can_Set_NonExistent_Counter_And_Retrieve_It_Again()
        {

            string key = "CounterY1";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            counterService.SetValue(key, 1234);

            int score = counterService.GetValue(key);

            Assert.AreEqual(1234, score);
        }

        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void Can_Set_NonExistent_Counter_And_Retrieve_It_Again_Whether_Target_Is_Null_Or_StringEmpty()
        {

            string key = "CounterY2";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            counterService.SetValue(key, 1234);

            int score = counterService.GetValue(key,string.Empty);

            Assert.AreEqual(1234, score);
        }


        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void Can_Set_User_Reputation_Score()
        {

            string key = "User/Reputation";
            string userId = "smithj";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            counterService.OffsetValue(key, 5, userId);
            counterService.OffsetValue(key, 3, userId);
            counterService.OffsetValue(key, -4, userId);

            int score = counterService.GetValue(key, userId);

            Assert.AreEqual(4, score);
        }

        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void Can_Set_User_Reputation_Score_And_Retrieve_It_With_Case_Insensitivity()
        {

            string key = "User/Reputation";
            string userId = "smithb";
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            counterService.OffsetValue(key, 5, userId);
            counterService.OffsetValue(key, 3, userId);
            counterService.OffsetValue(key, -7, userId);

            int score = counterService.GetValue(key, userId.ToUpper());

            Assert.AreEqual(1, score);
        }

        [Test]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void Can_Set_An_Operation_Counter()
        {
            //Note that this is better suited for perfomranceCounter, but still:

            string key = "Operation/SomeOperation";
            string targetId = null;
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            counterService.OffsetValue(key, 5);
            counterService.OffsetValue(key, 3);
            counterService.OffsetValue(key, -7);

            int score = counterService.GetValue(key, targetId);

            Assert.AreEqual(1, score);
        }

        [Test]
        [Ignore("In a single thread environment -- as in testing -- the following doesn't work.")]
        [Category("XActLibService")]
        [Category("CounterService")]
        public void CanSetValueWithoutCausingCommitOfOuterContext()
        {

            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            ICounterService counterService = XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            string key = "CounterZ";

            Counter counter = new Counter();
            counter.ApplicationTennantId = Guid.Empty;
            counter.Key = key;
            counter.Target = null;
            counter.Value = 12345;
            var a = counter.Timestamp;
            Assert.IsNull(a, "As not persisted or saved, a timestamp is null.");

            //Manually persist
            //But don't commit...
            repositoryService.PersistOnCommit<Counter, Guid>(counter, true);

            var b = counter.Timestamp;
            Assert.IsTrue(b==null,"As persisted, not saved, b timestamp is still null.");

            //Use service to save same entry,
            //which will retrieve same object, as it knows about it now (as we asked for it to be persisted/tracked)
            counterService.SetValue(key, 1234);

            //As it was saved by service, and config states we commit immediately:
            var c = counter.Timestamp;
            Assert.IsNotNull(c, "As persisted (and therefore saved), c timestamp is *not* null.");


            //Get the value.
            //As it was commited, we are sure it is up to date:
            counterService.GetValue(key);
            var d = counter.Timestamp;
            //As key is suppossed to return same counter
            Assert.IsNotNull(d);

            Assert.AreEqual(a, b,"a=b=null");
            Assert.AreNotEqual(a, c,"a!=c");
            Assert.AreNotEqual(a, d, "a!=d");
            Assert.AreNotEqual(c, d, "c=d");

        }



    }
}


