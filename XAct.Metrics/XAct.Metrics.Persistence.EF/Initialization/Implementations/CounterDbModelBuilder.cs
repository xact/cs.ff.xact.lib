﻿namespace XAct.Diagnostics.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics.Initialization.ModelPersistenceMaps;
    using XAct.Metrics;

    /// <summary>
    /// 
    /// </summary>
    public class CounterDbModelBuilder : ICounterDbModelBuilder
    {
        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add((EntityTypeConfiguration<Counter>)XAct.DependencyResolver.Current.GetInstance<ICounterModelPersistenceMap>());

        }
    }
}