﻿namespace XAct.Diagnostics.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// 
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface ICounterDbModelBuilder : IHasXActLibDbModelBuilder
    {
    }
}