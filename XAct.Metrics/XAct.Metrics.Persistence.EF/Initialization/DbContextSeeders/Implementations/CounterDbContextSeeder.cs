﻿namespace XAct.Diagnostics.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Metrics;

    /// <summary>
    /// Implementation of <see cref="ICounterDbContextSeeder"/>
    /// required for <see cref="ICounterService"/>
    /// </summary>
    public class CounterDbContextSeeder : XActLibDbContextSeederBase<Counter>, ICounterDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CounterDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public CounterDbContextSeeder(ITracingService tracingService) : base(tracingService)
        {
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }


    }
}
