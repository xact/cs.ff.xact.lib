
namespace XAct.Diagnostics.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Metrics;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Conter}"/>
    /// to seed the <c>MethodAuthorization</c> tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface ICounterDbContextSeeder : IHasXActLibDbContextSeeder<Counter>
    {

    }
}