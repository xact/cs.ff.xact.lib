﻿namespace XAct.Diagnostics.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Metrics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    //[DefaultBindingImplementation(typeof(ICounterModelPersistenceMap), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class CounterModelPersistenceMap : EntityTypeConfiguration<Counter>, ICounterModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CounterModelPersistenceMap"/> class.
        /// </summary>
        public CounterModelPersistenceMap()
        {
            this.ToXActLibTable("Counter");

            // Composite key
            this
                .HasKey(m => m.Id);

            int colOrder = 0;
            int indexMember = 1; //one based.


            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.ApplicationTennantId)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id_Key", indexMember++) { IsUnique = true }))
                        .DefineRequiredApplicationTennantId(colOrder++)
                ;


            this.Property(m => m.Target)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id_Key", indexMember++) { IsUnique = true }))
                ;


            this.Property(m => m.Key)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id_Key", indexMember++) { IsUnique = true }))
                ;


            this.Property(m => m.Value)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(m => m.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++);
                ;

            this.Property(m => m.MarkedForDeletionDateTimeUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.MarkedForDeletionKey)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
        }
    }
}