﻿namespace XAct.Metrics
{
    using System;
    using XAct.Metrics.Configuration;

    /// <summary>
    /// Contract for a service to manage <see cref="Counter"/>s.
    /// <para>
    /// Note that this service addresses a separate concern than <c>IPerformanceCounterService</c>
    /// </para>
    /// <para>
    /// Whereas PerformanceCounters, which --
    /// due to their high-precion requirements
    /// are stored in a thread-safe memory space -- 
    /// are not suitable to be incremented by more than one machine, 
    /// these counters are persisted in a datastore.
    /// </para>
    /// <para>
    /// The service is suitable for long term counter values being accumulated.
    /// </para>
    /// <para>
    /// An example would be <c>IReputationService</c> which is updated only 
    /// upon some operations.
    /// </para>
    /// </summary>
    public interface ICounterService : IHasXActLibService
    {
        /// <summary>
        /// Shared singelton Configuration.
        /// </summary>
        ICounterServiceConfiguration Configuration { get; }


        
        /// <summary>
        /// Gets the counter's value, or returns 0 if no counter found.
        /// <para>
        /// Important: when using an ORM (eg: EF), the Counter value is retrieved from a Datastore first.
        /// </para>
        /// </summary>
        /// <param name="key">The counter key (eg 'Users', where targetId is a unique userIdentifier, or 'Operation/SomeOperation', where 'targetId' is null.</param>
        /// <param name="targetId">The optional target identifier (not all counters distinguish by user/similar).</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <returns></returns>
        int GetValue(string key, string targetId=null, Guid? applicationTennantId = null);


        /// <summary>
        /// Sets the counter's value.
        /// <para>
        /// Important: when using an ORM (eg: EF), the Counter value is retrieved from a Datastore first.
        /// </para>
        /// </summary>
        /// <param name="key">The counter key (eg 'Users', where targetId is a unique userIdentifier, or 'Operation/SomeOperation', where 'targetId' is null.</param>
        /// <param name="targetId">The optional target identifier (not all counters distinguish by user/similar).</param>
        /// <param name="value">The new counter value (not an offset).</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        void SetValue(string key, int value, string targetId = null, Guid? applicationTennantId = null);



        /// <summary>
        /// Offsets the specified counter's value by the supplied offset.
        /// <para>
        /// Important: when using an ORM (eg: EF), the Counter value is retrieved from a Datastore first,
        /// and the UnitOfWork Committed immediately after updating it. 
        /// </para>
        /// <para>
        /// It is unconfirmed wheterh EF is capable of optimising the two calls into one.
        /// (see: http://pastebin.com/Ew4pubNM)
        /// </para>
        /// <para>
        /// Note: an example of where the Max/Min values are useful is when the Counters are used to back
        /// Reputations, where the reputation might have a max of 50, and a minimum of -50.
        /// </para>
        /// </summary>
        /// <param name="key">The counter key (eg 'Users', where targetId is a unique userIdentifier, or 'Operation/SomeOperation', where 'targetId' is null.</param>
        /// <param name="valueOffset">The offset value.</param>
        /// <param name="targetId">The optional target identifier (not all counters distinguish by user/similar).</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="minValueAllowed">The minimum value allowed.</param>
        /// <param name="maxValueAllowed">The maximum value allowed.</param>
        void OffsetValue(string key, int valueOffset, string targetId = null, Guid? applicationTennantId = null, int minValueAllowed = 0, int maxValueAllowed = int.MaxValue);

        /// <summary>
        /// Removes the specified counter.
        /// </summary>
        /// <param name="key">The counter key (eg 'Users', where targetId is a unique userIdentifier, or 'Operation/SomeOperation', where 'targetId' is null.</param>
        /// <param name="targetId">The optional target identifier (not all counters distinguish by user/similar).</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        void Remove(string key, string targetId = null, Guid? applicationTennantId = null);

    }
}