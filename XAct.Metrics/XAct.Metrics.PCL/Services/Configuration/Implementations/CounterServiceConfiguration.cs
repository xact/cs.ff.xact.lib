﻿namespace XAct.Metrics.Configuration.Implementations
{
    using XAct.Metrics.Implementations;

    /// <summary>
    /// An implementation of the <see cref="ICounterServiceConfiguration"/> contract.
    /// </summary>
    public class CounterServiceConfiguration : ICounterServiceConfiguration
    {
        /// <summary>
        /// A flag to commit immediately.
        /// <para>
        /// Default is true.
        /// </para>
        /// </summary>
        public bool CommitImmediately { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CounterServiceConfiguration"/> class.
        /// </summary>
        public CounterServiceConfiguration()
        {
            CommitImmediately = true;
        }
    }
}