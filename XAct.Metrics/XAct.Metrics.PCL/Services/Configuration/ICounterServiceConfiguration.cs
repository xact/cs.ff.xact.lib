﻿namespace XAct.Metrics.Configuration
{

    /// <summary>
    /// Contract for a singleton object to configure
    /// <see cref="ICounterService"/>
    /// </summary>
    public interface ICounterServiceConfiguration :IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// A flag to commit immediately. 
        /// <para>
        /// Default is true.
        /// </para>
        /// </summary>
        bool CommitImmediately { get; set; }
    }
}