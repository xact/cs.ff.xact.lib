﻿namespace XAct.Metrics.Implementations
{
    using System;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Metrics.Configuration;
    using XAct.Services;


    /// <summary>
    /// An implementation of the <see cref="ICounterService" />
    /// to manage <see cref="Counter"/>s across application tennants.
    /// <para>
    /// Note that this service addresses a separate concern than <c>IPerformanceCounterService</c>
    /// </para>
    /// <para>
    /// Whereas PerformanceCounters, which --
    /// due to their high-precion requirements
    /// are stored in a thread-safe memory space -- 
    /// are not suitable to be incremented by more than one machine, 
    /// these counters are persisted in a datastore.
    /// </para>
    /// <para>
    /// The service is suitable for long term counter values being accumulated.
    /// </para>
    /// <para>
    /// An example would be <c>IReputationService</c> which is updated only 
    /// upon some operations.
    /// </para>
    /// </summary>
    public class CounterService : ICounterService
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IDateTimeService _dateTimeService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;
        private readonly ICounterServiceConfiguration _counterServiceConfiguration;


        private Guid ApplicationTennantId
        {
            get { return _applicationTennantService.Get(); }
        }

        /// <summary>
        /// Shared singelton Configuration.
        /// </summary>
        public ICounterServiceConfiguration Configuration
        {
            get { return _counterServiceConfiguration; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CounterService" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="counterServiceConfiguration">The counter service configuration.</param>
        public CounterService(IEnvironmentService environmentService,
            IDateTimeService dateTimeService,
                                        IApplicationTennantService applicationTennantService,
                                        IRepositoryService repositoryService,
            ICounterServiceConfiguration counterServiceConfiguration)
        {
            _environmentService = environmentService;
            _dateTimeService = dateTimeService;
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
            _counterServiceConfiguration = counterServiceConfiguration;
        }

        /// <summary>
        /// Gets the counter's value, or returns 0 if no counter found.
        /// <para>
        /// Important: when using an ORM (eg: EF), the Counter value is retrieved from a Datastore first.
        /// </para>
        /// </summary>
        /// <param name="key">The counter key (eg 'Users', where targetId is a unique userIdentifier, or 'Operation/SomeOperation', where 'targetId' is null.</param>
        /// <param name="targetId">The optional target identifier (not all counters distinguish by user/similar).</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <returns></returns>
        public int GetValue(string key, string targetId = null, Guid? applicationTennantId = null)
        {
            bool isNew;

            Counter counter = GetCounterFromDataStore(key, out isNew, targetId, applicationTennantId);

            return (counter != null) ? counter.Value : 0;
        }



        /// <summary>
        /// Sets the counter's value.
        /// <para>
        /// Important: when using an ORM (eg: EF), the Counter value is retrieved from a Datastore first.
        /// </para>
        /// </summary>
        /// <param name="key">The counter key (eg 'Users', where targetId is a unique userIdentifier, or 'Operation/SomeOperation', where 'targetId' is null.</param>
        /// <param name="value">The new counter value (not an offset).</param>
        /// <param name="targetId">The optional target identifier (not all counters distinguish by user/similar).</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        public void SetValue(string key, int value, string targetId = null, Guid? applicationTennantId = null)
        {
            SaveRepeatedlyUntilNoException(key, 
                targetId, 
                applicationTennantId,
                x=>x.Value = value
                );
        }


        /// <summary>
        /// Offsets the specified counter's value by the supplied offset.
        /// <para>
        /// Important: when using an ORM (eg: EF), the Counter value is retrieved from a Datastore first,
        /// and the UnitOfWork Committed immediately after updating it.
        /// </para>
        /// <para>
        /// It is unconfirmed wheterh EF is capable of optimising the two calls into one.
        /// (see: http://pastebin.com/Ew4pubNM)
        /// </para>
        /// <para>
        /// Note: an example of where the Max/Min values are useful is when the Counters are used to back
        /// Reputations, where the reputation might have a max of 50, and a minimum of -50.
        /// </para>
        /// </summary>
        /// <param name="key">The counter key (eg 'Users', where targetId is a unique userIdentifier, or 'Operation/SomeOperation', where 'targetId' is null.</param>
        /// <param name="valueOffset">The offset value.</param>
        /// <param name="targetId">The optional target identifier (not all counters distinguish by user/similar).</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="minValueAllowed">The minimum value allowed.</param>
        /// <param name="maxValueAllowed">The maximum value allowed.</param>
        public void OffsetValue(string key, int valueOffset, string targetId = null, Guid? applicationTennantId = null,
                                int minValueAllowed = 0, int maxValueAllowed = int.MaxValue)
        {
            SaveRepeatedlyUntilNoException(key,
               targetId,
               applicationTennantId,
               x =>
                   {
                       //Offset counter:
                       x.Value += valueOffset;

                       //While ensuring it is within bounds:
                       if (x.Value <= minValueAllowed)
                       {
                           x.Value = minValueAllowed;
                       }

                       if (x.Value >= maxValueAllowed)
                       {
                           x.Value = maxValueAllowed;
                       }

                   }
                );
        }


        /// <summary>
        /// Removes the specified target identifier.
        /// <para>
        /// Important: the Counter value is retrieved from a Datastore first.
        /// </para>
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="targetId">The target identifier.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        public void Remove(string key, string targetId = null, Guid? applicationTennantId = null)
        {
            bool isNew;

            Counter counter = GetCounterFromDataStore(key, out isNew, targetId, applicationTennantId);

            if (isNew)
            {
                return;
            }

            _repositoryService.DeleteOnCommit(counter);
        }





        #region Private


        private Counter GetCounterFromDataStore(string key, out bool isNew, string targetId = null,
                                                Guid? applicationTennantId = null)
        {
            if (!applicationTennantId.HasValue)
            {
                applicationTennantId = ApplicationTennantId;
            }

            //This is so comparison's don't have to check for both null and empty string.
            if (targetId.IsNull())
            {
                targetId = string.Empty;
            }

            Guid applicationTennantIdX = applicationTennantId.Value;


            Counter counter =
                _repositoryService.GetSingle<Counter>(x => (
                                                               (x.ApplicationTennantId == applicationTennantIdX)
                                                               &&
                                                               (x.Target == targetId)
                                                               &&
                                                               (x.Key == key)
                                                           ));


            if (counter == null)
            {

                counter = new Counter
                    {
                        ApplicationTennantId = applicationTennantIdX,
                        Target = targetId,
                        Key = key
                    };

                isNew = true;
            }
            else
            {
                isNew = false;
            }

            return counter;

        }


        private void SaveRepeatedlyUntilNoException(string key, string targetId, Guid? applicationTennantId,
                                            Action<Counter> updateCounter)
        {
            int attemptsToSave = 0;
            int maxTries = 3;
            bool saveFailed = true;


            if (applicationTennantId == null)
            {
                applicationTennantId = _applicationTennantService.Get();
            }


#pragma warning disable 168
            using (var u = new UnitOfWorkThreadScope())
#pragma warning restore 168
            {
                do
                {
                    try
                    {
                        if (attemptsToSave > maxTries)
                        {
                            break;
                        }
                        attemptsToSave++;

                        bool isNew = false;
                        Counter counter = GetCounterFromDataStore(key, out isNew, targetId, applicationTennantId);

                        updateCounter.Invoke(counter);


                        counter.LastModifiedOnUtc = _dateTimeService.NowUTC;

                        _repositoryService.PersistOnCommit<Counter, Guid>(counter, true);


                        //TODO: This would be slow in a busy app (several counters per request):
                        //But without it, the value won't be correct in Get, 
                        //unless we add Attach.
                        if (_counterServiceConfiguration.CommitImmediately)
                        {
                            _repositoryService.GetContext().Commit(CommitType.Default);
                        }

                        saveFailed = false;
                    }
                    //correct, but Non Portable code:
                    //catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException e)
                    //{
                    //    e.Entries.Single().Reload(); 
                    //}
                    catch (System.Exception e)
                    {
                        string exceptionName = e.GetType().Name;
                        if (exceptionName != "DbUpdateConcurrencyException")
                        {
                            //Get out as it's not a reload scenario:
                            break;
                        }
                    }
                } while (saveFailed);
            } //~scope
        }


        #endregion
    }
}