﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Metrics.Entities.Internal
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    public class CounterEntry : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, IHasKeyValue<int>, IHasDateTimeCreatedOnUtc
    {
        /// <summary>
        /// Unique id of the counter.
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// FK of the entity being tracked.
        /// Usually the User Id.
        /// <para>
        /// Part of the entity's Composite Id.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string TargetId { get; set; }



        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// <para>
        /// Part of the entity's Composite Id.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether the entry is a summary entry
        /// (a rollup of earlier entries).
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is summary]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool IsSummary { get; set; }


        /// <summary>
        /// Gets or sets the unique key of the Counter.
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// <para>
        /// Part of the entity's Composite Id.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Key { get; set; }

        /// <summary>
        /// Gets or sets the Counter's value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [DataMember]
        public virtual int Value { get; set; }

        /// <summary>
        /// Gets or sets the date/time this record was last updated.
        /// </summary>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }


        
        /// <summary>
        /// Initializes a new instance of the <see cref="CounterEntry"/> class.
        /// </summary>
        public CounterEntry()
        {
            this.GenerateDistributedId();

        }

    }
}
