﻿namespace XAct.Metrics
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The counter record.
    /// <para>
    /// The Counter's key can be any string, such as
    /// "Operation/abc" 
    /// "User/xyz"
    /// "Operation/abc/User/xyz"
    /// etc. 
    /// </para>
    /// <para>
    /// An example is the counter used to measure a User's reputation ("User/Reputation/{usedId}")</para>
    /// <para>
    /// The object's key is a composite of the application identifier,
    /// application tennant id, entity id, and key name.
    /// </para>
    /// <para>
    /// By design the entity is non-relational to other entities
    /// (ie has no FK to another table) so that it can be used
    /// to track various objects -- not just an application, or 
    /// user, as is the most common use case.
    /// </para>
    /// </summary>
    [DataContract]
    public class Counter : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, IHasKeyValue<int>, IHasDateTimeModifiedOnUtc, IHasSyncronizedDeleteInformation
    {
        private string _target;

        /// <summary>
        /// Unique id of the counter.
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


 
        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// <para>
        /// Part of the entity's Composite Id.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }



        /// <summary>
        /// Gets or sets the unique key of the Counter.
        /// <para>
        /// Examples are:
        /// "Operations/SomeOperation" 
        /// "Users/Reputation"
        /// etc., whereas the <see cref="Target"/>
        /// property holds a unique reference to a Target entity
        /// (such as the UserId, when the <see cref="Key"/>
        /// is something like "Users/Reputation"
        /// </para>
        /// <para>Member defined in <see cref="IHasKey" /></para>
        /// <para>
        /// Part of the entity's Composite Id.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Key { get; set; }

        /// <summary>
        /// Gets an optional reference to an entity being tracked.
        /// <para>
        /// This could be the UniqueId of a user.
        /// </para>
        /// <para>
        /// Part of the entity's Composite Id.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Target
        {
            get { return _target; }
            set
            {
                //As target is optional, 
                //we need to simplify it to always be a comparison 
                //string.Empty, not null:
                _target = value??string.Empty;
            }
        }


        /// <summary>
        /// Gets or sets the Counter's sum value.
        /// </summary>
        [DataMember]
        public virtual int Value { get; set; }


        /// <summary>
        /// Gets or sets the date/time this record was last updated.
        /// </summary>
        [DataMember]
        public virtual DateTime? LastModifiedOnUtc { get; set; }



        /// <summary>
        /// A random value, different per server.
        /// <para>
        /// Not used at present.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid MarkedForDeletionKey { get; set; }

        /// <summary>
        /// A flag used to delete records when updating Sum values.
        /// <para>
        /// Not used at present.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual DateTime? MarkedForDeletionDateTimeUtc { get; set; }

        /// <summary>
        /// Counters the entry.
        /// </summary>
        public Counter()
        {
            this.GenerateDistributedId();
            _target = string.Empty;
        }
    }
}
