﻿//namespace XAct.Metrics.Implementations
//{
//    using System;
//    using XAct.Services;

//    /// <summary>
//    /// Implementation of the <see cref="ICounterService"/>
//    /// to manage conuters for a target entity/user.
//    /// </summary>
//    public class CounterService : ICounterService
//    {
//        private readonly ICounterManagementService _counterManagementService;


//        /// <summary>
//        /// Initializes a new instance of the <see cref="CounterService" /> class.
//        /// </summary>
//        /// <param name="counterManagementService">The counter management service.</param>
//        public CounterService( ICounterManagementService counterManagementService)
//        {
//            _counterManagementService = counterManagementService;
//        }

//        /// <summary>
//        /// Gets the counter's value, or returns 0 if no counter found.
//        /// <para>
//        /// Important: the Counter value is retrieved from a Datastore first.
//        /// </para>
//        /// </summary>
//        /// <param name="targetId">The target identifier (often a User's Id).</param>
//        /// <param name="key">The key for a counter specific to the <paramref name="targetId" />.</param>
//        /// <returns></returns>
//        public int GetValue(string key, Guid? targetId)
//        {
//            return _counterManagementService.GetValue(key, targetId);
//        }


//        /// <summary>
//        /// Sets the value.
//        /// <para>
//        /// Important: the Counter value is retrieved from a Datastore first.
//        /// </para>
//        /// </summary>
//        /// <param name="targetId">The target identifier (often a User's Id).</param>
//        /// <param name="key">The key for a counter specific to the <paramref name="targetId" />.</param>
//        /// <param name="value">The value.</param>
//        public void SetValue(string key, int value, Guid? targetId)
//        {
//            _counterManagementService.SetValue(key, value, targetId);
//        }


//        /// <summary>
//        /// Removes the specified <see cref="Counter" />.
//        /// <para>
//        /// Important: the Counter value is retrieved from a Datastore first.
//        /// </para>
//        /// <para>
//        /// Important: the Context will be Saved immediately afterwards. 
//        /// </para>
//        /// <para>
//        /// It is unconfirmed wheterh EF is capable of optimising the two calls into one.
//        /// (see: http://pastebin.com/Ew4pubNM)
//        /// </para>
//        /// </summary>
//        /// <param name="targetId">The target identifier.</param>
//        /// <param name="key">The key.</param>
//        public void Remove(string key, Guid? targetId)
//        {
//            _counterManagementService.Remove(key,targetId);
//        }


//    }
//}