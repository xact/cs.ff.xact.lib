﻿
//namespace XAct.Metrics
//{
//    using System;

//    /// <summary>
//    /// Contract for a service to manage 
//    /// counters for an entity.
//    /// </summary>
//    public interface ICounterService : IHasXActLibService
//    {
//        /// <summary>
//        /// Gets the counter's value, or returns 0 if no counter found.
//        /// <para>
//        /// Important: the Counter value is retrieved from a Datastore first.
//        /// </para>
//        /// </summary>
//        /// <param name="targetId">The target identifier (often a User's Id).</param>
//        /// <param name="key">The key for a counter specific to the <paramref name="targetId"/>.</param>
//        /// <returns></returns>
//        int GetValue(string key, Guid? targetId);

//        /// <summary>
//        /// Sets the value.
//        /// <para>
//        /// Important: the Counter value is retrieved from a Datastore first.
//        /// </para>
//        /// </summary>
//        /// <param name="key">The key for a counter specific to the <paramref name="targetId" />.</param>
//        /// <param name="value">The value.</param>
//        /// <param name="targetId">The target identifier.</param>
//        void SetValue(string key, int value, Guid? targetId);


//        /// <summary>
//        /// Removes the specified counter.
//        /// <para>
//        /// Important: the Counter value is retrieved from a Datastore first.
//        /// </para>
//        /// </summary>
//        /// <param name="targetId">The target identifier.</param>
//        /// <param name="key">The key.</param>
//        void Remove(string key, Guid? targetId);

//    }
//}
