namespace XAct.Tasks.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ITaskCategoryDbContextSeeder"/>
    /// </summary>
    public class TaskCategoryDbContextSeeder : XActLibDbContextSeederBase<TaskCategory>, ITaskCategoryDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskCategoryDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public TaskCategoryDbContextSeeder(ITracingService tracingService):base(tracingService)
        {
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}