namespace XAct.Tasks.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskProjectDbContextSeeder : XActLibDbContextSeederBase<TaskProject>, ITaskProjectDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProjectDbContextSeeder" /> class.
        /// </summary>
        /// <param name="taskProjectCategoryDbContextSeeder">The task project category database context seeder.</param>
        public TaskProjectDbContextSeeder(ITaskProjectCategoryDbContextSeeder taskProjectCategoryDbContextSeeder) :
                                              base(taskProjectCategoryDbContextSeeder)
        {
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}