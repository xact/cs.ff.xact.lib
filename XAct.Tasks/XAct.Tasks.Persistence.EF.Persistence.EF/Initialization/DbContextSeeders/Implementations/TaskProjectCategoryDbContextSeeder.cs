namespace XAct.Tasks.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskProjectCategoryDbContextSeeder : XActLibDbContextSeederBase<TaskProjectCategory>, ITaskProjectCategoryDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProjectCategoryDbContextSeeder"/> class.
        /// </summary>
        public TaskProjectCategoryDbContextSeeder(ITracingService tracingService) : base(tracingService)
        {
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }

    }
}