﻿namespace XAct.Tasks.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Tasks.Models;

    /// <summary>
    /// A default implementation of the <see cref="ITaskDbContextSeeder"/> contract
    /// to seed the HostSettings tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class TaskDbContextSeeder : XActLibDbContextSeederBase<Task>, ITaskDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskDbContextSeeder"/> class.
        /// </summary>
        /// <param name="taskProjectDbContextSeeder">The task project category database context seeder.</param>
        /// <param name="taskStateDbContextSeeder">The task state database context seeder.</param>
        /// <param name="taskCategoryDbContextSeeder">The task category database context seeder.</param>
        public TaskDbContextSeeder(            ITaskProjectDbContextSeeder taskProjectDbContextSeeder,
            ITaskStateDbContextSeeder taskStateDbContextSeeder, 
            ITaskCategoryDbContextSeeder taskCategoryDbContextSeeder
            )
            :
            base(taskProjectDbContextSeeder, taskStateDbContextSeeder,taskCategoryDbContextSeeder)
        {
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}
