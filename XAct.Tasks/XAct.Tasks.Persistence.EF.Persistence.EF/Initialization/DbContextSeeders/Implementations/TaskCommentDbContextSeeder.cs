namespace XAct.Tasks.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskCommentDbContextSeeder : XActLibDbContextSeederBase<TaskComment>, ITaskCommentDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskStateDbContextSeeder" /> class.
        /// </summary>
        /// <param name="taskDbContextSeeder">The task database context seeder.</param>
        public TaskCommentDbContextSeeder(ITaskDbContextSeeder taskDbContextSeeder)
            : base(taskDbContextSeeder)
        {

        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}