namespace XAct.Tasks.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskStateDbContextSeeder : XActLibDbContextSeederBase<TaskState>, ITaskStateDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskStateDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public TaskStateDbContextSeeder(ITracingService tracingService) : base(tracingService)
        {
            
        }
        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}