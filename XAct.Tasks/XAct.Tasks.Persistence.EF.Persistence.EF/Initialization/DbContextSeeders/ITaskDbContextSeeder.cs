namespace XAct.Tasks.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Tasks.Models;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Task}"/>
    /// to seed a table.
    /// </summary>
    public interface ITaskDbContextSeeder : IHasXActLibDbContextSeeder<Task>
    {

    }
}