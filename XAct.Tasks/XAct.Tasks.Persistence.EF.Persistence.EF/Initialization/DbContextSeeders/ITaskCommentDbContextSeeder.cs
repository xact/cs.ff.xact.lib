namespace XAct.Tasks.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Tasks.Models;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{TaskComment}"/>
    /// to seed a table.
    /// </summary>
    public interface ITaskCommentDbContextSeeder : IHasXActLibDbContextSeeder<TaskComment>
    {

    }
}