namespace XAct.Tasks.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public interface ITaskProjectDbContextSeeder : IHasXActLibDbContextSeeder<TaskProject>
    {
    }
}