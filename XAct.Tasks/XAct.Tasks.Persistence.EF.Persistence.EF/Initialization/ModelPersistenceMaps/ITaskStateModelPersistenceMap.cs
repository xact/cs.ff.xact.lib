﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps
{
    using XAct.Tasks.Models;

    /// <summary>
    /// PersistenceMap for <see cref="TaskState"/> models.
    /// </summary>
    public interface ITaskStateModelPersistenceMap : IHasXActLibModelPersistenceMap { }
}