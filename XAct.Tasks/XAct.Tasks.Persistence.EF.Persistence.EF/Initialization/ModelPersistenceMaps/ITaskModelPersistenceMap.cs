﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps
{
    using XAct.Tasks.Models;

    /// <summary>
    /// PersistenceMap for <see cref="Task"/> models.
    /// </summary>
    public interface ITaskModelPersistenceMap : IHasXActLibModelPersistenceMap { }
}