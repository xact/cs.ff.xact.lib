﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps
{
    using XAct.Tasks.Models;

    /// <summary>
    /// PersistenceMap for <see cref="TaskCategory"/> models
    /// </summary>
    public interface ITaskCategoryModelPersistenceMap : IHasXActLibModelPersistenceMap { }
}