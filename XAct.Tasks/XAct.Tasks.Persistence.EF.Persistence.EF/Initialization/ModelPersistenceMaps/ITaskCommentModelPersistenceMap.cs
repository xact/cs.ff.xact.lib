﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps
{
    using XAct.Tasks.Models;

    /// <summary>
    /// PersistenceMap for <see cref="TaskComment"/> models
    /// </summary>
    public interface ITaskCommentModelPersistenceMap : IHasXActLibModelPersistenceMap { }
}