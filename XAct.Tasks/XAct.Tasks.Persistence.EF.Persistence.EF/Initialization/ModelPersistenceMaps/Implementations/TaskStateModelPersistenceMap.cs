﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps.Implementations
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using XAct.Data;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ITaskStateModelPersistenceMap"/>
    /// </summary>
    public class TaskStateModelPersistenceMap :
        ApplicationTennantSpecificReferenceDataPersistenceMapBase<TaskState, Guid>,
        ITaskStateModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskStateModelPersistenceMap"/> class.
        /// </summary>
        public TaskStateModelPersistenceMap()
            : base("TaskState", DatabaseGeneratedOption.None)
        {
        }
    }
}