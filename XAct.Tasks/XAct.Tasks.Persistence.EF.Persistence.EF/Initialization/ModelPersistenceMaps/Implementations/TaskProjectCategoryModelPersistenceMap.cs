﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps.Implementations
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using XAct.Data;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskProjectCategoryModelPersistenceMap :
        ApplicationTennantSpecificReferenceDataPersistenceMapBase<TaskProjectCategory, Guid>,
        ITaskProjectCategoryModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProjectCategoryModelPersistenceMap"/> class.
        /// </summary>
        public TaskProjectCategoryModelPersistenceMap()
            : base("TaskProjectCategory", DatabaseGeneratedOption.None)
        {
        }
    }
}