﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ITaskProjectModelPersistenceMap"/>
    /// </summary>
    public class TaskProjectModelPersistenceMap : EntityTypeConfiguration<TaskProject>, ITaskProjectModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskModelPersistenceMap"/> class.
        /// </summary>
        public TaskProjectModelPersistenceMap()
        {
            this.ToXActLibTable("TaskProject");

            int colOrder = 0;
            int indexMember = 1; //Indexs of db's are 1 based.

            this
                .HasKey(m => m.Id);

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x => x.ApplicationTennantId)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Task_ApplicationTennantId_Assignee", indexMember++)
                        {
                            IsUnique = false
                        }))
                        .DefineRequiredApplicationTennantId(colOrder++)
                ;

            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.Order)
                .DefineRequiredOrder(colOrder++);

        }
    }
}