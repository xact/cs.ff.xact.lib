﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps.Implementations
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using XAct.Data;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ITaskCategoryModelPersistenceMap"/>
    /// </summary>
    public class TaskCategoryModelPersistenceMap : ApplicationTennantSpecificReferenceDataPersistenceMapBase<TaskCategory, Guid>, ITaskCategoryModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskCategoryModelPersistenceMap"/> class.
        /// </summary>
        public TaskCategoryModelPersistenceMap()
            : base("TaskCategory", DatabaseGeneratedOption.None)
        {
        }
    }
}