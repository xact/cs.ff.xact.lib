﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskCommentModelPersistenceMap : EntityTypeConfiguration<TaskComment>, ITaskCommentModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskCommentModelPersistenceMap"/> class.
        /// </summary>
        public TaskCommentModelPersistenceMap()
        {
            this.ToXActLibTable("TaskComment");

            int colOrder = 0;
#pragma warning disable 168
            //int indexMember = 1; //Indexs of db's are 1 based.
#pragma warning restore 168

            this
                .HasKey(m => m.Id);

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.TaskFK)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++);

            this.Property(x => x.UserIdentifier)
                .DefineRequired64CharUserIdentifier(colOrder++);

            this.Property(x => x.Text)
                .HasColumnOrder(colOrder++);

        }

    }
}