﻿namespace XAct.Tasks.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;
    using XAct.Tasks.Models;


    /// <summary>
    /// 
    /// </summary>
    public class TaskModelPersistenceMap : EntityTypeConfiguration<Task>, ITaskModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskModelPersistenceMap"/> class.
        /// </summary>
        public TaskModelPersistenceMap()
        {
            this.ToXActLibTable("Task");

            int colOrder = 0;
            int indexMember = 1; //Indexs of db's are 1 based.

            this
                .HasKey(m => m.Id);

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x => x.ApplicationTennantId)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Task_ApplicationTennantId_Assignee", indexMember++)
                            {
                                IsUnique = false
                            }))
                .DefineRequiredApplicationTennantId(colOrder++);

            this.Property(x => x.Enabled)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.Order)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            //Optional because Top Task don't have parents.
            this.Property(x => x.ParentFK)
                .IsOptional()
                .HasColumnOrder(colOrder++);


            //Required as Projects must be declared. Think of it as a first level categorisation.
            this.Property(x => x.ProjectFK)
                .IsRequired()
                .HasColumnOrder(colOrder++);


            this.Property(x => x.CategoryFK)
                .IsOptional()
                .HasColumnOrder(colOrder++);

            this.Property(x => x.StateFK)
                .IsRequired()
                .HasColumnOrder(colOrder++);


            this.Property(x => x.UserIdentifier)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Task_ApplicationTennantId_Assignee", indexMember++)
                            {
                                IsUnique = false
                            }))
                            .DefineRequired64CharUserIdentifier(colOrder++)
                ;



            this.Property(x => x.Subject)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.Body)
                //(50lines x 80chars) Should be sufficient. Any longer and you're waffling. They want Help Not Tolstoy. 
                .DefineOptional4000CharText(colOrder++);
            ;

            this.Property(x => x.ProjectedStartDateTimeUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Task_ProjectedStartDateTimeUtc", 1) {IsUnique = false}))
                ;
            this.Property(x => x.ProjectedEndDateTimeUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Task_ProjectedEndDateTimeUtc", 1) {IsUnique = false}))
                ;
            this.Property(x => x.StartDateTimeUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Task_StartDateTimeUtc", 1) {IsUnique = false}))
                ;
            this.Property(x => x.EndDateTimeUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Task_EndDateTimeUtc", 1) {IsUnique = false}))
                ;


            //Relationships:

            //IMPORTANT:
            //but don't forget that when we show State, or Category to the User
            //it has to be filtered by ApplicationHost.


            this.HasRequired(x => x.Project)
                .WithMany()
                .HasForeignKey(x=>x.ProjectFK)
                ; //No Collection on CategorySide.


            //State is required (Uncategorized, NotStarted, InProgress, ....) 
            this.HasRequired(x => x.State)
                .WithMany()
                .HasForeignKey(x => x.StateFK)
                ; //No Collection on CategorySide.


            //Category, but by ApplicationHost...
            this.HasOptional(x => x.Category)
                .WithMany()
                .HasForeignKey(x => x.CategoryFK)
                ; //No Collection on CategorySide.





            this.HasMany(x => x.Comments)
                .WithOptional()
                .HasForeignKey(x=>x.TaskFK);


            //Recursion:
            this
                .HasMany(m => m.Children)
                .WithOptional(m => m.Parent)
                .HasForeignKey(m => m.ParentFK);


            //The tags:
            this
                .HasMany(p => p.Tags)
                .WithMany()
                .Map(x =>
                         {
                             x.ToXActLibTable("Task_Joins_Tags");
                             x.MapLeftKey("TaskId");
                             x.MapRightKey("TagId");
                         });

        }
    }
}

