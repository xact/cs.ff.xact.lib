﻿// ReSharper disable CheckNamespace

namespace XAct.Tasks.Initialization.Implementations
// ReSharper restore CheckNamespace
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Tasks.Initialization.ModelPersistenceMaps;
    using XAct.Tasks.Models;


    /// <summary>
    /// Implementation of <see cref="ITaskDbModelBuilder"/>
    /// in order to create HostSettings capabilities in a CodeFirst managed Db.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class TaskDbModelBuilder : ITaskDbModelBuilder
    {
        private readonly ITracingService _tracingService;
        private readonly string _typeName;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskDbModelBuilder"/> class.
        /// </summary>
        public TaskDbModelBuilder(ITracingService tracingService)
        {
            _tracingService = tracingService;
            _typeName = this.GetType().Name;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<TaskProjectCategory>)XAct.DependencyResolver.Current.GetInstance<ITaskProjectCategoryModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<TaskProject>)XAct.DependencyResolver.Current.GetInstance<ITaskProjectModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<TaskState>)XAct.DependencyResolver.Current.GetInstance<ITaskStateModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<TaskCategory>)XAct.DependencyResolver.Current.GetInstance<ITaskCategoryModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<TaskComment>)XAct.DependencyResolver.Current.GetInstance<ITaskCommentModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<Task>)XAct.DependencyResolver.Current.GetInstance<ITaskModelPersistenceMap>());

        }
    }
}

