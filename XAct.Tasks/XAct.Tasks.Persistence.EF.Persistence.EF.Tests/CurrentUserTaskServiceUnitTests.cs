namespace XAct.Tags.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Messages;
    using XAct.Tasks;
    using XAct.Tasks.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class CurrentUserTaskServiceUnitTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetCurrentUserTaskService()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ICurrentUserTaskService>();
            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetCurrentUserTaskServiceOfExpectedInstanceType()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ICurrentUserTaskService>();
            Assert.AreEqual(typeof(CurrentUserTaskService), service.GetType());
        }

        [Test]
        public void Can_Get_Project_Category_By_Id()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITaskProjectCategoryService>();

            var result = service.GetById(1.ToGuid());

            Assert.IsNotNull(result);
        }


        [Test]
        public void Can_Get_Project_Categories()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITaskProjectCategoryService>();

            var result = service.Retrieve(new PagedQuerySpecification()).ToArray();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }



        [Test]
        public void Can_Get_Project_By_Id()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITaskProjectService>();

            var result = service.GetById(1.ToGuid());

            Assert.IsNotNull(result);
        }


        [Test]
        public void Can_Get_Projects()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITaskProjectService>();

            var result = service.Retrieve(new PagedQuerySpecification()).ToArray();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }




        [Test]
        public void Can_Get_Task_Category_By_Id()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITaskCategoryService>();

            var result = service.GetById(1.ToGuid());

            Assert.IsNotNull(result);
        }


        [Test]
        public void Can_Get_Task_Categories()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITaskCategoryService>();

            var result = service.Retrieve(new PagedQuerySpecification()).ToArray();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }




        [Test]
        public void Can_Get_Task_Status_By_Id()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITaskStateService>();

            var result = service.GetById(1.ToGuid());

            Assert.IsNotNull(result);
        }


        [Test]
        public void Can_Get_Task_Statuses()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITaskStateService>();

            var result = service.Retrieve(new PagedQuerySpecification()).ToArray();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }





        [Test]
        public void CanGetTaskByIdIfInCurrentApplication()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ICurrentUserTaskService>();

            /*
                    Id = 1.ToGuid(),
                    ApplicationTennantId = _applicationTennantService.Get(),
                    Enabled = true,
                    Subject = "TaskA",
                    Body = "TaskBodyA",
             */

            var result = service.GetTask(1.ToGuid());

            Assert.IsNotNull(result);

            Assert.AreEqual("TaskA", result.Subject);

        }



        //[Test]
        //[InitializeUnitTestDbIoCContextAttribute(true)]
        //public void CanGetTags()
        //{
        //    var service = XAct.DependencyResolver.Current.GetInstance<ICurrentUserTaskService>();

        //    var results = service.GetCurrentUserTasksForUser().ToArray();

        //    Assert.IsTrue(results.Length > 0);
        //}


        //[Test]
        //[InitializeUnitTestDbIoCContextAttribute(true)]
        //public void CanGetTag()
        //{
        //    var service = XAct.DependencyResolver.Current.GetInstance<ICurrentUserTaskService>();

        //    //var result = service.GetTask("TagA");

        //    Assert.IsNotNull(result);
        //}


        //[Test]
        //[InitializeUnitTestDbIoCContextAttribute(true)]
        //public void CanRemoveTag()
        //{
        //    var service = XAct.DependencyResolver.Current.GetInstance<ICurrentUserTaskService>();

        //    var result = service.GetCurrentUserTaskBySubject("TagA");

        //    Assert.IsNotNull(result);

        //    service.RemoveCurrentUserTaskBySubject("TagA");
        //    XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

        //    var resultB = service.GetCurrentUserTaskBySubject("TagA");

        //    Assert.IsNull(resultB);

        //}
    }
}


