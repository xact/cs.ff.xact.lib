﻿namespace XAct.Tags.Tests.Initialization
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Linq;
    using XAct;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Library.Settings;
    using XAct.Tasks.Initialization.DbContextSeeders;
    using XAct.Tasks.Models;

    public class TaskDbContextSeeder : UnitTestXActLibDbContextSeederBase<Task>, ITaskDbContextSeeder
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IDistributedIdService _distributedIdService;
        private readonly IDateTimeService _dateTimeService;
        private readonly ITaskProjectDbContextSeeder _taskProjectDbContextSeeder;
        private readonly ITaskStateDbContextSeeder _taskStateDbContextSeeder;
        private readonly ITaskCategoryDbContextSeeder _taskCategoryDbContextSeeder;


        /// <summary>
        /// Initializes a new instance of the <see cref="TaskDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="distributedIdService">The distributed identifier service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="taskProjectDbContextSeeder">The task project database context seeder.</param>
        /// <param name="taskStateDbContextSeeder">The task state database context seeder.</param>
        /// <param name="taskCategoryDbContextSeeder">The task category database context seeder.</param>
        public TaskDbContextSeeder(
            ITracingService tracingService, 
            IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService, 
            IDistributedIdService distributedIdService,
            IDateTimeService dateTimeService,

            ITaskProjectDbContextSeeder taskProjectDbContextSeeder,
            ITaskStateDbContextSeeder taskStateDbContextSeeder, 
            ITaskCategoryDbContextSeeder taskCategoryDbContextSeeder
            )
            : base(tracingService, taskProjectDbContextSeeder, taskStateDbContextSeeder, taskCategoryDbContextSeeder)
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
            _distributedIdService = distributedIdService;
            _dateTimeService = dateTimeService;
            _taskProjectDbContextSeeder = taskProjectDbContextSeeder;
            _taskStateDbContextSeeder = taskStateDbContextSeeder;
            _taskCategoryDbContextSeeder = taskCategoryDbContextSeeder;
        }
        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.Subject);
        }

        public override void CreateEntities() 
        {

            this.InternalEntities = new List<Task>();
             
            var tmpId = 1.ToGuid();
            TaskCategory category = _taskCategoryDbContextSeeder.Entities.Single<TaskCategory>(x=>x.Id==tmpId);

            tmpId = 1.ToGuid();
            TaskState state = _taskStateDbContextSeeder.Entities.Single<TaskState>(x => x.Id == tmpId);

            tmpId = 1.ToGuid();
            TaskProject project = _taskProjectDbContextSeeder.Entities.Single(x => x.Id == tmpId);

            const string currentUserIdentifier = "xyz";

            var applicationTennantId = _applicationTennantService.Get();

            this.InternalEntities.Add(new Task
                {
                    Id = 1.ToGuid(),
                    Enabled = true,
                    ApplicationTennantId = applicationTennantId,
                    ProjectFK = project.Id,
                    CategoryFK = category.Id,
                    StateFK = state.Id,
                    UserIdentifier = currentUserIdentifier,
                    Subject = "TaskA",
                    Body = "TaskBodyA. Has no parent. And no time.",
                });



            this.InternalEntities.Add(new Task
                {
                    Id = 2.ToGuid(),
                    Enabled = true,
                    ApplicationTennantId = applicationTennantId,
                    ProjectFK = project.Id,
                    UserIdentifier = currentUserIdentifier,
                    CategoryFK = category.Id,
                    StateFK = state.Id,
                    Subject = "TaskB. Has no parent. Projected to start 3 days ago, and finish before 5 days from now.",
                    Body = "TaskBodyB",
                    ProjectedStartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-3),
                    ProjectedEndDateTimeUtc = _dateTimeService.NowUTC.AddDays(3)
                });

            this.InternalEntities.Add( new Task
                {
                    Id = 3.ToGuid(),
                    ApplicationTennantId = applicationTennantId,
                    ProjectFK = project.Id,
                    UserIdentifier = currentUserIdentifier,
                    Enabled = true,
                    CategoryFK = category.Id,
                    StateFK = state.Id,
                    Subject = "TaskC",
                    Body = "TaskBodyC. Has no parent. Projected. And actually started yesterday",
                    ProjectedStartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-3),
                    ProjectedEndDateTimeUtc = _dateTimeService.NowUTC.AddDays(5),
                    StartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-1),
                });

            
            this.InternalEntities.Add( new Task
                {
                    Id = 4.ToGuid(),
                    ApplicationTennantId = applicationTennantId,
                    ProjectFK = project.Id,
                    UserIdentifier = currentUserIdentifier,
                    Enabled = true,
                    CategoryFK = category.Id,
                    StateFK = state.Id,
                    Subject = "TaskD",
                    Body = "TaskBodyD. Has no parent. Project, Started, And actually ended yesterday.",
                    ProjectedStartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-5),
                    ProjectedEndDateTimeUtc = _dateTimeService.NowUTC.AddDays(5),
                    StartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-3),
                    EndDateTimeUtc = _dateTimeService.NowUTC.AddDays(-1)
                });


            this.InternalEntities.Add( new Task
            {
                Id = 11.ToGuid(),
                ApplicationTennantId = applicationTennantId,
                ParentFK = 1.ToGuid(),
                ProjectFK = project.Id,
                UserIdentifier = currentUserIdentifier,
                Enabled = true,
                CategoryFK = category.Id,
                StateFK = state.Id,
                Subject = "TaskA1",
                Body = "TaskBodyA1 - a task that is a subset of A (1). Projected, Started, not Ended.",
                ProjectedStartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-5),
                ProjectedEndDateTimeUtc = _dateTimeService.NowUTC.AddDays(5),
                StartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-3)
            });

            this.InternalEntities.Add( new Task
            {
                Id = 12.ToGuid(),
                ApplicationTennantId = applicationTennantId,
                ParentFK = 1.ToGuid(),
                ProjectFK = project.Id,
                UserIdentifier = currentUserIdentifier,
                Enabled = true,
                CategoryFK = category.Id,
                StateFK = state.Id,
                Subject = "TaskA2",
                Body = "TaskBodyA2 - a task that is a subset of A (1). Projected, Started, and Ended.",
                ProjectedStartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-5),
                ProjectedEndDateTimeUtc = _dateTimeService.NowUTC.AddDays(5),
                StartDateTimeUtc = _dateTimeService.NowUTC.AddDays(-3),
                EndDateTimeUtc = _dateTimeService.NowUTC.AddDays(-1)
            });

        }
    }

}
