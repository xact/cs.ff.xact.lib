namespace XAct.Tasks.Tests.Initialization
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Tasks.Initialization.DbContextSeeders;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskProjectCategoryDbContextSeeder : UnitTestXActLibDbContextSeederBase<TaskProjectCategory>, ITaskProjectCategoryDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProjectCategoryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        public TaskProjectCategoryDbContextSeeder(ITracingService tracingService,
                                                  IApplicationTennantService applicationTennantService)
            : base(tracingService)
        {
            _applicationTennantService = applicationTennantService;
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.Text);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void CreateEntities()
        {
            this.InternalEntities = new List<TaskProjectCategory>();

            var applicationTennantId = _applicationTennantService.Get();


            Guid tmpId;
            tmpId = 1.ToGuid();
            this.InternalEntities.Add(new TaskProjectCategory { Id = tmpId, ApplicationTennantId = applicationTennantId, ResourceFilter=string.Empty, Text = "Family" });

            tmpId = 2.ToGuid();
            this.InternalEntities.Add(new TaskProjectCategory { Id = tmpId, ApplicationTennantId = applicationTennantId, ResourceFilter = string.Empty, Text = "Personal" });

            tmpId = 3.ToGuid();
            this.InternalEntities.Add(new TaskProjectCategory { Id = tmpId, ApplicationTennantId = applicationTennantId, ResourceFilter = string.Empty, Text = "ProjA" });


        }

    }
}


