namespace XAct.Tasks.Initialization
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Tasks.Initialization.DbContextSeeders;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskStateDbContextSeeder : UnitTestXActLibDbContextSeederBase<TaskState>, ITaskStateDbContextSeeder
    {
        private readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskStateDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        public TaskStateDbContextSeeder(ITracingService tracingService,
                                        IApplicationTennantService applicationTennantService)
            : base(tracingService)
        {
            _applicationTennantService = applicationTennantService;
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.Text);
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<TaskState>();

            Guid tmpId;

            var applicationTennantId = _applicationTennantService.Get();



            tmpId = 1.ToGuid();
            this.InternalEntities.Add(
                new TaskState
                    {
                        Id = tmpId,
                        ApplicationTennantId = applicationTennantId,
                        ResourceFilter = string.Empty,
                        Text = "Backlog"
                    });
            tmpId = 2.ToGuid();
            this.InternalEntities.Add(
                new TaskState
                    {
                        Id = tmpId,
                        ApplicationTennantId = applicationTennantId,
                        ResourceFilter = string.Empty,
                        Text = "Backlog"
                    });
            tmpId = 3.ToGuid();
            this.InternalEntities.Add(
                new TaskState
                    {
                        Id = tmpId,
                        ApplicationTennantId = applicationTennantId,
                        ResourceFilter = string.Empty,
                        Text = "InProgress"
                    });
            tmpId = 4.ToGuid();
            this.InternalEntities.Add(
                new TaskState
                    {
                        Id = tmpId,
                        ApplicationTennantId = applicationTennantId,
                        ResourceFilter = string.Empty,
                        Text = "Done"
                    });
            tmpId = 5.ToGuid();
            this.InternalEntities.Add(
                new TaskState
                    {
                        Id = tmpId,
                        ApplicationTennantId = applicationTennantId,
                        ResourceFilter = string.Empty,
                        Text = "Archive"
                    });

        }
    }
}