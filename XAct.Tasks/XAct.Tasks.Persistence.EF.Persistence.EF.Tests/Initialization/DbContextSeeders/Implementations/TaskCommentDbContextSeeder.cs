﻿
namespace XAct.Tasks.Tests.Initialization
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Library.Settings;
    using XAct.Tasks.Initialization;
    using XAct.Tasks.Initialization.DbContextSeeders;
    using XAct.Tasks.Initialization.DbContextSeeders.Implementations;
    using XAct.Tasks.Models;

    public class TaskCommentDbContextSeeder : UnitTestXActLibDbContextSeederBase<TaskComment>, ITaskCommentDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly ITaskDbContextSeeder _taskDbContextSeeder;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tasks.Initialization.DbContextSeeders.Implementations.TaskCommentDbContextSeeder" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="taskDbContextSeeder">The task database context seeder.</param>
        public TaskCommentDbContextSeeder(IDateTimeService dateTimeService, ITracingService tracingService, 
            ITaskDbContextSeeder taskDbContextSeeder
            )
            : base(tracingService, taskDbContextSeeder)
        {
            _dateTimeService = dateTimeService;
            _taskDbContextSeeder = taskDbContextSeeder;
        }

        public override void SeedInternal(DbContext dbContext)
        {
            this.SeedInternalHelper(dbContext,true,x=>x.Text);
        }

        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority

            this.InternalEntities = new List<TaskComment>();

            Guid tmpId;
            tmpId = 1.ToGuid();

            XAct.Tasks.Models.Task task = _taskDbContextSeeder.Entities.Single(x => x.Id == tmpId);

            this.InternalEntities.Add(
                new TaskComment
                    {
                        Id = tmpId,
                        TaskFK = task.Id,      
                        UserIdentifier = "abc",
                        Text = "Db", 
                        LastModifiedOnUtc=_dateTimeService.NowUTC
                        
                    });

        }
    }
}
