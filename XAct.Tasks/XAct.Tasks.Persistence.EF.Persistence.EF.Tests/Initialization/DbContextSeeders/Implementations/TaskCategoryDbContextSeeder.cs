namespace XAct.Tasks.Initialization
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Library.Settings;
    using XAct.Tasks.Initialization.DbContextSeeders;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ITaskCategoryDbContextSeeder"/>
    /// </summary>
    public class TaskCategoryDbContextSeeder : UnitTestXActLibDbContextSeederBase<TaskCategory>, ITaskCategoryDbContextSeeder
    {
        private readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskCategoryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="taskStateDbContextSeeder">The task state database context seeder.</param>
        public TaskCategoryDbContextSeeder(ITracingService tracingService,
                                           IApplicationTennantService applicationTennantService,
                                           ITaskStateDbContextSeeder taskStateDbContextSeeder)
            : base(tracingService, taskStateDbContextSeeder)
        {
            _applicationTennantService = applicationTennantService;
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.Text);
        }
        

        public override void CreateEntities()
        {

            this.InternalEntities = new List<TaskCategory>();


            var applicationTennantId = _applicationTennantService.Get();

            Guid tmpId;
            tmpId = 1.ToGuid();
            this.InternalEntities.Add(new TaskCategory { Id = tmpId, ApplicationTennantId = applicationTennantId, ResourceFilter=string.Empty, Text = "Db" });
            tmpId = 2.ToGuid();
            this.InternalEntities.Add(new TaskCategory { Id = tmpId, ApplicationTennantId = applicationTennantId, ResourceFilter = string.Empty, Text = "Application" });
            tmpId = 3.ToGuid();
            this.InternalEntities.Add(new TaskCategory { Id = tmpId, ApplicationTennantId = applicationTennantId, ResourceFilter = string.Empty, Text = "UX" });
            tmpId = 4.ToGuid();
            this.InternalEntities.Add(new TaskCategory { Id = tmpId, ApplicationTennantId = applicationTennantId, ResourceFilter = string.Empty, Text = "Delivery" });

        }
    }
}