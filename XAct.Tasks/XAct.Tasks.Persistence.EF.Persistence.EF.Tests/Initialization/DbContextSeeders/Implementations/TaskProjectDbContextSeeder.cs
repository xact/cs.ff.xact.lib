namespace XAct.Tasks.Tests.Initialization
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Linq;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Library.Settings;
    using XAct.Tasks.Initialization;
    using XAct.Tasks.Initialization.DbContextSeeders;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskProjectDbContextSeeder : UnitTestXActLibDbContextSeederBase<TaskProject>, ITaskProjectDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly ITaskProjectCategoryDbContextSeeder _taskProjectCategoryDbContextSeeder;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProjectDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="taskProjectCategoryDbContextSeeder">The task project category database context seeder.</param>
        public TaskProjectDbContextSeeder(
            ITracingService tracingService,
            IApplicationTennantService applicationTennantService,
            ITaskProjectCategoryDbContextSeeder taskProjectCategoryDbContextSeeder) :
            base(tracingService, taskProjectCategoryDbContextSeeder)
        {
            _applicationTennantService = applicationTennantService;
            _taskProjectCategoryDbContextSeeder = taskProjectCategoryDbContextSeeder;
        }


        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true,x=>x.Text);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void CreateEntities()
        {
            this.InternalEntities = new List<TaskProject>();

            
            var tmpId = 1.ToGuid();

            TaskProjectCategory category1 = 
                _taskProjectCategoryDbContextSeeder.Entities.Single<TaskProjectCategory>(x => x.Id == tmpId);

            tmpId = 2.ToGuid();
            TaskProjectCategory category2 =
                _taskProjectCategoryDbContextSeeder.Entities.Single<TaskProjectCategory>(x => x.Id == tmpId);

            var applicationTennantId = _applicationTennantService.Get();

            this.InternalEntities.Add(
                                         new TaskProject
                                             {
                                                 Id = 1.ToGuid(),
                                                 ApplicationTennantId = applicationTennantId,
                                                 CategoryFK = category1.Id,
                                                 Enabled = true,
                                                 Order = 0,
                                                 Text = "ProjA",
                                                 Description = "foo",
                                             });

            this.InternalEntities.Add(
                             new TaskProject
                             {
                                 Id = 2.ToGuid(),
                                 ApplicationTennantId = applicationTennantId,
                                 CategoryFK = category2.Id,
                                 Enabled = true,
                                 Order = 0,
                                 Text = "ProjB",
                                 Description = "foo",
                             });


        }


    }
}