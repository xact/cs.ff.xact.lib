namespace XAct.Tasks.Models
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TTask"></typeparam>
    public interface IHasHierarchicalScheduledTaskWithDistributedGuidId<TTask> :

        IHasHierarchyCollectionReadOnly<TTask>,
        IHasScheduledTaskWithDistributedGuidId
        where TTask : IHasScheduledTaskWithDistributedGuidId
    {

    }
}