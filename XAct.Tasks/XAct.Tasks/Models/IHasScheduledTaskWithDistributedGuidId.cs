namespace XAct.Tasks.Models
{
    using System;

    /// <summary>
    /// Contract for a <see cref="Task"/> 
    /// that has a projected start/end datetime, and an actual start/end datetime.
    /// </summary>
    public interface IHasScheduledTaskWithDistributedGuidId : 
    IHasTaskWithDistributedGuidId,
        IHasScheduledTask<Guid>
    { 

    }
}