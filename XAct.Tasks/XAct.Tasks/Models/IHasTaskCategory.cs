namespace XAct.Tasks.Models
{
    using System;

    /// <summary>
    /// Contract for a <c>Task</c>
    /// that has a <see cref="Category"/>
    /// </summary>
    public interface IHasOptionalTaskCategory
    {
        /// <summary>
        /// Gets or sets the task category fk.
        /// </summary>
        /// <value>
        /// The task category fk.
        /// </value>
        Guid? CategoryFK { get; set; }

        /// <summary>
        /// Gets or sets the task category.
        /// </summary>
        TaskCategory Category { get; set; }
    }
}