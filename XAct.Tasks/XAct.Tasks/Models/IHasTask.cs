namespace XAct.Tasks.Models
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IHasTask<TId> :
        IHasId<TId>,
        IHasEnabled,
        IHasOrder,
        IHasSubjectAndBody
        where TId : struct
    {

    }

}