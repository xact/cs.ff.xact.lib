namespace XAct.Tasks.Models
{
    using System;

    /// <summary>
    /// Contract for a <c>Project</c>
    /// that has a <see cref="Category"/>
    /// </summary>
    public interface IHasProjectCategory
    {
        /// <summary>
        /// Gets or sets the Project category fk.
        /// </summary>
        /// <value>
        /// The Project category fk.
        /// </value>
        Guid CategoryFK { get; set; }

        /// <summary>
        /// Gets or sets the Project category.
        /// </summary>
        TaskProjectCategory Category { get; set; }
    }
}