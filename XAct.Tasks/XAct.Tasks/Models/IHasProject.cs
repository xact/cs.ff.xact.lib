namespace XAct.Tasks.Models
{
    using System;

    /// <summary>
    /// Contract for <see cref="Task"/> entities
    /// that belong to a <see cref="Project"/>
    /// </summary>
    public interface IHasProject
    {

        /// <summary>
        /// Gets or sets the FK of the <see cref="Project"/>
        /// this <see cref="Task"/> belongs to.
        /// </summary>
        Guid ProjectFK { get; set; }


        /// <summary>
        /// Gets or sets the <see cref="Project"/>
        /// </summary>
        TaskProject Project { get; set; }
    }
}