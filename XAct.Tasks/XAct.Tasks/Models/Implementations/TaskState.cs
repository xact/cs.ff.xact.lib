namespace XAct.Tasks.Models
{
    using System.Runtime.Serialization;
    using XAct.Messages;

    /// <summary>
    /// State of the <see cref="Task"/>s.
    /// In a KanBan board, this would be akin to the board's Column.
    /// </summary>
    [DataContract]
    public class TaskState : ApplicationTennantIdSpecificReferenceDataGuidIdBase, IHasXActLibEntity
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskState"/> class.
        /// </summary>
        public TaskState()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }
    }
}