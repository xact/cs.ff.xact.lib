﻿using System;

namespace XAct.Tasks.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using XAct.Categorization;

    /// <summary>
    /// A Model of a Task.
    /// <para>
    /// Tasks are viewed within a single <c>ApplicationHost</c>.
    /// </para>
    /// <para>
    /// Tasks are generally viewed in the context of a single Project (when viewed within a Kanban board).
    /// </para>
    /// <para>
    /// A Task is 
    /// grouped by <see cref="TaskState"/> (in a Kanban board, this is a Column), 
    /// and categorized by <see cref="TaskCategory"/>.
    /// </para>
    /// <para>
    /// <see cref="TaskCategory"/> is an optional filtering mechanism.
    /// </para>
    /// <para>
    /// A <see cref="Task"/> is assigned to a single User.
    /// That said, note that another person can complete the Task -- which will set the User to that person.
    /// </para>
    /// <para>
    /// Comments can be associated to a Task.
    /// </para>
    /// </summary>
    [DataContract(Namespace = XAct.Library.Constants.DataContracts.BaseDataContractNamespace)]
    public class Task : HierarchicalTaskBase, IHasXActLibEntity, IHasTaskState, IHasOptionalTaskCategory, IHasProject, IHasTagsReadOnly,
                        IHasDistributedTagsReadOnly
    {

        /// <summary>
        /// The FK of the Category this Task.
        /// </summary>
        [DataMember]
        public Guid ProjectFK { get; set; }



        /// <summary>
        /// ThTaskProjectory of this Task.
        /// </summary>
        [DataMember]
        public TaskProject Project { get; set; }


        /// <summary>
        /// The FK of the <see cref="TaskState"/> of this Task.
        /// </summary>
        [DataMember]
        public Guid StateFK { get; set; }



        /// <summary>
        /// The Category of this Task.
        /// </summary>
        [DataMember]
        public TaskState State { get; set; }



        /// <summary>
        /// The FK of the <see cref="TaskCategory"/> this Task.
        /// </summary>
        [DataMember]
        public Guid? CategoryFK { get; set; }



        /// <summary>
        /// The <see cref="TaskCategory"/> of this Task.
        /// </summary>
        [DataMember]
        public TaskCategory Category { get; set; }



        /// <summary>
        /// Gets the comments.
        /// </summary>
        public ICollection<TaskComment> Comments
        {
            get { return _comments ?? (_comments = new Collection<TaskComment>()); }
        }

        private Collection<TaskComment> _comments;

        /// <summary>
        /// Gets the optional tags associated to this task
        /// </summary>
        [IgnoreDataMember]
        public ICollection<Tag> Tags
        {
            get { return _tags ?? (_tags = new Collection<Tag>()); }
            //private set { _tags = value; }
        }

        [DataMember] private ICollection<Tag> _tags;

        /// <summary>
        /// Initializes a new instance of the <see cref="Task"/> class.
        /// </summary>
        public Task()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }
    }
}
