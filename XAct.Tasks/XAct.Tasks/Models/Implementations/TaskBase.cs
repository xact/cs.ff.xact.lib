﻿namespace XAct.Tasks.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Namespace = XAct.Library.Constants.DataContracts.BaseDataContractNamespace)]
    public abstract class TaskBase :
        IHasApplicationTennantId,
        IHasUserIdentifier
    {

        /// <summary>
        /// Gets or sets the datastore identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the timespan.
        /// </summary>
        /// <value>
        /// The timespan.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }



        /// <summary>
        /// Gets or sets the <see cref="Task"/> type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [DataMember]
        public virtual TaskType Type { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }



        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        [DataMember]
        public virtual int Order { get; set; }


        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public virtual string UserIdentifier { get; set; }



        /// <summary>
        /// Gets or sets the message subject.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Body { get; set; }



        /// <summary>
        /// Gets or sets the projected started date.
        /// </summary>
        [DataMember]
        public virtual DateTime? ProjectedStartDateTimeUtc { get; set; }
        /// <summary>
        /// Gets or sets the projected end date.
        /// </summary>
        [DataMember]
        public virtual DateTime? ProjectedEndDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets the actual started date.
        /// </summary>
        [DataMember]
        public virtual DateTime? StartDateTimeUtc { get; set; }
        /// <summary>
        /// Gets or sets the actual end date.
        /// </summary>
        [DataMember]
        public virtual DateTime? EndDateTimeUtc { get; set; }



        

        /// <summary>
        /// Initializes a new instance of the <see cref="Task"/> class.
        /// </summary>
        protected TaskBase()
        {
            this.Enabled = true;
        }

    }
}