namespace XAct.Tasks.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using XAct.Categorization;
    using XAct.Messages;


    /// <summary>
    /// Categorisation of <see cref="Task"/>s.
    /// <para>
    /// In a Kanban board this would ba akin to the Task's Colour/Category.
    /// </para>
    /// </summary>
    [DataContract]
    public class TaskCategory : ApplicationTennantIdSpecificReferenceDataGuidIdBase,
                                IHasXActLibEntity,
                                IHasDistributedTagsReadOnly
    {


        /// <summary>
        /// Gets the collection of optional tags associated to this object.
        /// </summary>
        public ICollection<Tag> Tags
        {
            get { return _Tags ?? (_Tags = new Collection<Tag>()); }
        }

        [DataMember(Name = "Tags")] private ICollection<Tag> _Tags;


        /// <summary>
        /// Initializes a new instance of the <see cref="TaskCategory"/> class.
        /// </summary>
        public TaskCategory()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }

    }
}