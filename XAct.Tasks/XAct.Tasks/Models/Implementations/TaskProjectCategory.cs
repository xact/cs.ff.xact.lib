namespace XAct.Tasks.Models
{
    using System.Runtime.Serialization;
    using XAct.Messages;


    /// <summary>
    /// Categorization of <see cref="TaskProject"/>
    /// </summary>
    [DataContract]
    public class TaskProjectCategory : ApplicationTennantIdSpecificReferenceDataGuidIdBase, IHasXActLibEntity
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProjectCategory"/> class.
        /// </summary>
        public TaskProjectCategory()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }
    }
}