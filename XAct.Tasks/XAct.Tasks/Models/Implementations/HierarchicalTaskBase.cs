﻿namespace XAct.Tasks.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// A Model of a Task, with an Id that is suitable for Distributed infrastructure (read 'Mobile-capable').
    /// </summary>
    [DataContract(Namespace = XAct.Library.Constants.DataContracts.BaseDataContractNamespace)]
    public abstract class HierarchicalTaskBase : HierarchicalTaskBase<Task>
    {
        
    }



    /// <summary>
    /// A Model of a Task, with an Id that is suitable for Distributed infrastructure (read 'Mobile-capable').
    /// </summary>
    [DataContract(Namespace = XAct.Library.Constants.DataContracts.BaseDataContractNamespace)]
    public abstract class HierarchicalTaskBase<TTask> : TaskBase,
    IHasHierarchicalScheduledTaskWithDistributedGuidId<TTask>

        where TTask:IHasScheduledTaskWithDistributedGuidId
    {


        /// <summary>
        /// Gets or sets the FK of the parent <see cref="Task"/>.
        /// <para>
        /// If this is a top-tier task (ie, <see cref="Type"/> is set to <see cref="TaskType.Project"/>)
        /// the value can be Null.
        /// </para>
        /// </summary>
        [DataMember]
        public Guid? ParentFK { get; set; }

        /// <summary>
        /// Gets or sets the parent <see cref="Task"/>
        /// <para>
        /// Note that if <see cref="Type"/> is set to <see cref="TaskType.Project"/>
        /// this can be null.
        /// </para>
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        [DataMember]
        public TTask Parent { get; set; }


        /// <summary>
        /// Gets the collection of children <see cref="Task"/>.
        /// </summary>
        public ICollection<TTask> Children
        {
            get { return _children ?? (_children = new Collection<TTask>()); } 
            //private set {_children = new Collection<DistributedTask>();}
        }
        [DataMember]
        private ICollection<TTask> _children;






        /// <summary>
        /// Initializes a new instance of the <see cref="Task"/> class.
        /// </summary>
        protected HierarchicalTaskBase()
        {
            this.GenerateDistributedId();
        }

    }
}