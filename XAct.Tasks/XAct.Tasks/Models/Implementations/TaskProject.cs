﻿namespace XAct.Tasks.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using XAct.Categorization;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Projects are *very* similar to <see cref="Task"/>s -- enough so that
    /// it becomes interesting enough to wonder whether they should both
    /// be the same entity, stored in the same datastore.
    /// <para>
    /// Considerations as to whether to use the same table:
    /// </para>
    /// <para>
    /// Advantages:
    /// * Although most sql datastores have no hierarchical methods, SQL Server sort of does.
    ///     * If it could just iterate up, we're done. But if it's a separate table, it has to update a second table when ParentFK==default.
    ///     * Then again: if it's a StoredProc capable platform, that's possible. And if repositoryBased, then it's a trivial (but slow) iteration process.
    /// * 
    /// Considerations:
    /// * A task is definately an Action to be completed. A Project is a collection of child Actions...which is very similar to a HierarchicalTask...
    /// Disadvantages:
    /// * A project is a reporting device, and should be kept separate from Tasks.
    /// * Selecting a List of Projects to render is more complex (where Type=Project)
    /// * ProjectCategorisation might be different that TaskCategorisation.
    /// * Project might end up needing extra fields.
    /// </para>
    /// <para>
    /// To err on the side of caution, will keep them separate for now.
    /// </para>
    /// </internal>
    public class TaskProject :
        IHasXActLibEntity, 
        IHasApplicationTennantIdSpecificDistributedGuidId,
        IHasDistributedGuidIdAndTimestamp,    
        IHasEnabled, 
        IHasOrder,        
        IHasApplicationTennantId,
        IHasTextAndDescription, 
        IHasTagsReadOnly, 
        IHasProjectCategory
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether [enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enabled]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }



        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        [DataMember]
        public virtual string Text { get; set; }


        /// <summary>
        /// Gets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// The FK of the Category this Task.
        /// </summary>
        [DataMember]
        public virtual Guid CategoryFK { get; set; }



        /// <summary>
        /// The Category of this Task.
        /// </summary>
        [DataMember]
        public virtual TaskProjectCategory Category { get; set; }



        /// <summary>
        /// Gets the optional tags associated to this task
        /// </summary>
        /// <value>
        /// The tags.
        /// </value>
        [IgnoreDataMember]
        public virtual ICollection<Tag> Tags
        {
            get { return _tags ?? (_tags = new Collection<Tag>()); }
            //private set { _tags = value; }
        }
        [DataMember]
        private ICollection<Tag> _tags;



        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProject"/> class.
        /// </summary>
        public TaskProject()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }
    }
}