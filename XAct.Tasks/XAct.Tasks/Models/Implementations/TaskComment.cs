﻿namespace XAct.Tasks.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Comments associate to a <see cref="Task"/>
    /// </summary>
    [DataContract]
    public class TaskComment : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasUserIdentifier, IHasText, IHasDateTimeModifiedOnUtc
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the FK to the <see cref="Task"/>
        /// to which this <see cref="TaskComment"/> is associated.
        /// </summary>
        /// <value>
        /// The task fk.
        /// </value>
        [DataMember]
        public virtual Guid TaskFK { get; set; }


        /// <summary>
        /// Gets a reference to the user who wrote the comment.
        /// </summary>
        [DataMember]
        public virtual string UserIdentifier { get; set; }


        /// <summary>
        /// Gets or sets the comment's text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [DataMember]
        public virtual string Text { get; set; }



        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// <para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? LastModifiedOnUtc { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="TaskComment"/> class.
        /// </summary>
        public TaskComment()
        {
            this.GenerateDistributedId();
            //this.Enabled = true;
        }
    }
}