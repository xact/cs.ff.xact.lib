﻿namespace XAct.Tasks.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration of the types
    /// of <see cref="Task"/>
    /// available.
    /// </summary>
    [DataContract]
    public enum TaskType
    {
        /// <summary>
        /// The Task is undefined.
        /// <para>
        /// This is an error condition.
        /// </para>
        /// </summary>
        Undefined=0,
        /// <summary>
        /// The <see cref="Task"/>
        /// is a Project.
        /// </summary>
        Project=1,
        /// <summary>
        /// The <see cref="Task"/>
        /// is a Task within a <see cref="Project"/>
        /// </summary>
        Task=2
    }
}
