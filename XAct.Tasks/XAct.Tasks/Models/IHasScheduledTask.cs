namespace XAct.Tasks.Models
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IHasScheduledTask<TId> : IHasTask<TId>, IHasNullableStartDateTimeEndDateTimeUtc, IHasNullableProjectedStartDateTimeEndDateTimeUtc
        where TId:struct
    {
        
    }
}