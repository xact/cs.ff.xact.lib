namespace XAct.Tasks.Models
{
    using System;

    /// <summary>
    /// Contract for the basis of a Task.
    /// </summary>
    public interface IHasTaskWithDistributedGuidId : IHasDistributedGuidIdAndTimestamp, IHasTask<Guid>
    {

    }

}