//namespace XAct.Tasks.Models
//{
//    using System;

//    /// <summary>
//    /// Contract for Tasks that have a Projected Start and End date.
//    /// </summary>
//    public interface IHasTaskScheduledProjected
//    {
//        /// <summary>
//        /// Gets or sets the projected started date.
//        /// </summary>
//        DateTime? ProjectedStartDateTimeUtc { get; set; }

//        /// <summary>
//        /// Gets or sets the projected end date.
//        /// </summary>
//        DateTime? ProjectedTerminationDateTimeUtc { get; set; }
//    }
//}