namespace XAct.Tasks.Models
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TTask">The type of the task.</typeparam>
    public interface IHasHierarchicalTaskWithDistributedGuidId<TTask> : 
        
        IHasHierarchyCollectionReadOnly<TTask>,
        IHasTaskWithDistributedGuidId
        where TTask : IHasTaskWithDistributedGuidId
    {

    }
}