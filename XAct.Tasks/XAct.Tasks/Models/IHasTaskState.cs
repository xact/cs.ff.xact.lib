namespace XAct.Tasks.Models
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface IHasTaskState
    {
        /// <summary>
        /// Gets or sets the Project category fk.
        /// </summary>
        /// <value>
        /// The Project category fk.
        /// </value>
        Guid StateFK { get; set; }

        /// <summary>
        /// Gets or sets the Project category.
        /// </summary>
        TaskState State { get; set; }
    }
}