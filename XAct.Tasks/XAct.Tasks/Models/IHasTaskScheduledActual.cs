//namespace XAct.Tasks.Models
//{
//    using System;

//    /// <summary>
//    /// Contract for Tasks that have an Actual Start and End date.
//    /// </summary>
//    public interface IHasTaskScheduledActual
//    {
//        /// <summary>
//        /// Gets or sets the actual started date.
//        /// </summary>
//        DateTime? ActualStartDateTimeUtc { get; set; }

//        /// <summary>
//        /// Gets or sets the actual end date.
//        /// </summary>
//        DateTime? ActualTerminationDateTimeUtc { get; set; }
//    }
//}