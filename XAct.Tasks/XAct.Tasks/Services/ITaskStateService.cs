﻿namespace XAct.Tasks
{
    using System;
    using XAct.Domain.Repositories;
    using XAct.Tasks.Models;

    /// <summary>
    /// Contract for a service to manage <see cref="TaskState"/> elements.
    /// </summary>
    public interface ITaskStateService : IApplicationTennantIdSpecificReferenceDataRepository<TaskState, Guid>, IHasXActLibService
    {

    }
}