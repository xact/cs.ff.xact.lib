﻿namespace XAct.Tasks
{
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public interface ICurrentApplicationTennantProjectManagementService :IHasXActLibService
    {
        /// <summary>
        /// Persists the new or updated project.
        /// </summary>
        /// <param name="project">The project.</param>
        void PersistOnCommit(TaskProject project);



        /// <summary>
        /// Deletes the specified project.
        /// </summary>
        /// <param name="project">The project.</param>
        void DeleteOnCommit(TaskProject project);



    }

    /// <summary>
    /// Contract for managing the <see cref="TaskState"/>
    /// within the current ApplicationTennant.
    /// </summary>
    public interface ICurrentApplicationTennantTaskStateManagementService :IHasXActLibService
    {
        
    }


}