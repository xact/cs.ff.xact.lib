﻿namespace XAct.Tasks
{
    using System;
    using XAct.Domain.Repositories;
    using XAct.Tasks.Models;

    /// <summary>
    /// Contract for a service to manage <see cref="TaskCategory"/> elements.
    /// </summary>
    public interface ITaskCategoryService : IApplicationTennantIdSpecificReferenceDataRepository<TaskCategory, Guid>, IHasXActLibService
    {
        
    }
}