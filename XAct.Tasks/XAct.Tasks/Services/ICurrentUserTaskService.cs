﻿using System;
using System.Linq;

namespace XAct.Tasks
{
    using XAct.Messages;
    using XAct.Tasks.Models;


    /// <summary>
    /// A service to manage <see cref="Task"/>
    /// objects.
    /// </summary>
    public interface ICurrentUserTaskService : IHasXActLibService
    {

        /// <summary>
        /// Gets the current user's tasks.
        /// </summary>
        IQueryable<Task> GetProjectTasks(Guid projectId,
                                                Guid? optionalCategoryId,
                                                IPagedQuerySpecification pagedQuerySpecification,
                                                bool includeDisabled = true,
                                                bool includeCompleted = true,
                                                bool includeCategoryAndTags = false,
                                                bool includeChildTasks = false
            );

        /// <summary>
        /// Gets the current user task.
        /// </summary>
        /// <param name="distributedId">The distributed identifier.</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        Task GetTask(Guid distributedId, bool includeCategory = false, bool includeTags = true, bool includeChildTasks = false);

    }
}
