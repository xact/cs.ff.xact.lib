﻿namespace XAct.Tasks
{
    using System;
    using XAct.Domain.Repositories;
    using XAct.Tasks.Models;

    /// <summary>
    /// Contract for a service to manage <see cref="TaskProjectCategory"/> elements.
    /// </summary>
    public interface ITaskProjectCategoryService :  IApplicationTennantIdSpecificReferenceDataRepository<TaskProjectCategory, Guid>, IHasXActLibService
    {


    }
}