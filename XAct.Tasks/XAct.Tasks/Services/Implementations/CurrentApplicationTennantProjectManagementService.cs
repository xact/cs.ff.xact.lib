﻿namespace XAct.Tasks.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Expressions;
    using XAct.Messages;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class CurrentApplicationTennantProjectManagementService : ICurrentApplicationTennantProjectManagementService
    {
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Is the current application tennant project management service.
        /// </summary>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public CurrentApplicationTennantProjectManagementService(IApplicationTennantService applicationTennantService , IRepositoryService repositoryService)
        {
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Persists the new or updated project.
        /// </summary>
        /// <param name="project">The project.</param>
        public void PersistOnCommit(TaskProject project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project");
            }
            _repositoryService.PersistOnCommit<TaskProject,Guid>(project,true);
        }

        /// <summary>
        /// Deletes the specified project.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <exception cref="System.ArgumentNullException">project</exception>
        public void DeleteOnCommit(TaskProject project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project");
            }
            _repositoryService.DeleteOnCommit(project);
        }


        private IQueryable<TaskProject> InternalGetProjects(Guid[] projectIds, IPagedQuerySpecification pagedQuerySpecification, 
            bool includeDisabled, 
            bool includeCompleted, 
            bool includeCategory,
            bool includeTags,
            bool includeChildTasks)
        {
            var applicationTennantId = _applicationTennantService.Get();



            var predicate = PredicateBuilder.True<TaskProject>();

            predicate = predicate.And<TaskProject>(x => (x.ApplicationTennantId == applicationTennantId));



            //if (tags!=null)&& (tags.Length>0)
            //{
            //    predicate.And<DistributedTask>(x => ((x.Tags.Any(tags() == categoryId);
            //}


            //I think we always want to return disabled -- just have to watch out for Sum() conditions.
            if (!includeDisabled)
            {
                predicate = predicate.And<TaskProject>(x => x.Enabled == true);
            }

            //if (!includeCompleted)
            //{
            //    predicate = predicate.And<TaskProject>(x => !x.EndDateTimeUtc.HasValue);
            //}



            var includeSpecification = CreateIncludeSpecification(
                includeCategory: includeCategory, 
                includeTags: includeTags, 
                includeChildTasks:includeChildTasks
                );


            var result =
                _repositoryService.GetByFilter<TaskProject>(
                        predicate,
                        includeSpecification,
                        pagedQuerySpecification
                        );


            return result;
        }


        private static IncludeSpecification<TaskProject> CreateIncludeSpecification(
    bool includeCategory,
    bool includeTags,
    bool includeChildTasks)
        {

            var includesList = new List<Expression<Func<TaskProject, object>>>();

            if (includeCategory)
            {
                includesList.Add(x => x.Category);
            }
            

            if (includeTags)
            {
                includesList.Add(x => x.Tags);
            }

            //if (includeChildTasks)
            //{
            //    includesList.Add(x => x.Children);
            //}

            var includeSpecification = new IncludeSpecification<TaskProject>(includesList.ToArray());

            return includeSpecification;
        }

 
    }
}