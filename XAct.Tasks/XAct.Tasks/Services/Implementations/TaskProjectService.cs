﻿namespace XAct.Tasks.Implementations
{
    using System;
    using System.Linq;
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Services;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ITaskProjectService"/>
    /// to manage 
    /// <see cref="Task"/>
    /// entities whose Type is <see cref="TaskType.Project"/>
    /// </summary>
    public class TaskProjectService : ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase<TaskProject>, ITaskProjectService
    {

        /// <summary>
        /// Gets the application tennant identifier.
        /// </summary>
        /// <value>
        /// The application tennant identifier.
        /// </value>
        private Guid ApplicationTennantId
        {
            get { return _applicationTennantService.Get(); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProjectService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public TaskProjectService(ITracingService tracingService, IApplicationTennantService applicationTennantService,
                              IRepositoryService repositoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {
        }


    }
}