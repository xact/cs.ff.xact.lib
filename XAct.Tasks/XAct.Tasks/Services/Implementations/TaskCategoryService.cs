﻿namespace XAct.Tasks.Services.Implementations
{
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ITaskCategoryService"/>
    /// to retrieve <see cref="TaskCategory"/> elements.
    /// </summary>
    public class TaskCategoryService :
        ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase<TaskCategory>,
        ITaskCategoryService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskCategoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public TaskCategoryService(ITracingService tracingService, IApplicationTennantService applicationTennantService,
                                   IRepositoryService repositoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {

        }

    }
}