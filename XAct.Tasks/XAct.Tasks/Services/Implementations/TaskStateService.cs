﻿namespace XAct.Tasks.Services.Implementations
{
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Tasks.Models;

    /// <summary>
    /// 
    /// </summary>
    public class TaskStateService :
        ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase<TaskState>,
        ITaskStateService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskStateService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public TaskStateService(ITracingService tracingService, IApplicationTennantService applicationTennantService,
                                IRepositoryService repositoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {

        }

    }
}