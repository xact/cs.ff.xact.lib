﻿namespace XAct.Tasks.Implementations
{
    using System;
    using System.Linq;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Services;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ICurrentUserTaskService"/>
    /// contract to manage a single person's Tasks.
    /// </summary>
    public class CurrentUserTaskService : ICurrentUserTaskService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly ICurrentApplicationTennantTaskService _currentApplicationTennantTaskService;
        private readonly IPrincipalService _principalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentUserTaskService" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="currentApplicationTennantTaskService">The current application tennant task service.</param>
        /// <param name="principalService">The principal service.</param>
        public CurrentUserTaskService(
            IDateTimeService dateTimeService,
            ICurrentApplicationTennantTaskService currentApplicationTennantTaskService,
            IPrincipalService principalService
            )
        {
            _dateTimeService = dateTimeService;
            _currentApplicationTennantTaskService = currentApplicationTennantTaskService;
            _principalService = principalService;
        }


        /// <summary>
        /// Gets the project tasks.
        /// </summary>
        /// <param name="projectId">The project identifier.</param>
        /// <param name="optionalCategoryId">The optional category identifier.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeCompleted">if set to <c>true</c> [include completed].</param>
        /// <param name="includeCategoryAndTags">if set to <c>true</c> [include category and tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        public IQueryable<Task> GetProjectTasks(Guid projectId,
                                                Guid? optionalCategoryId,
                                                IPagedQuerySpecification pagedQuerySpecification,
                                                bool includeDisabled = true,
                                                bool includeCompleted = true,
                                                bool includeCategoryAndTags = false,
                                                bool includeChildTasks = false
            )
        {
            var results = 
                _currentApplicationTennantTaskService.GetTasksByUser(
                    _principalService.CurrentIdentityIdentifier,
                    projectId,
                    optionalCategoryId, pagedQuerySpecification,
                    includeDisabled, includeCompleted, includeCategoryAndTags,
                    includeChildTasks);

            return results;
        }


        /// <summary>
        /// Gets the task.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        public Task GetTask(Guid id, bool includeCategory=false, bool includeTags=true,bool includeChildTasks=false)
        {
            return _currentApplicationTennantTaskService.GetTask(id,
                                                                 includeCategory: includeCategory,
                                                                 includeTags:includeTags,
                                                                 includeChildTasks: includeChildTasks);
        }
    }
}