﻿namespace XAct.Tasks.Services.Implementations
{
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ITaskProjectCategoryService"/>
    /// to retrieve <see cref="TaskProjectCategory"/> elements.
    /// </summary>
    public class TaskProjectCategoryService : 
        ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase<TaskProjectCategory>, 
        ITaskProjectCategoryService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProjectCategoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public TaskProjectCategoryService(ITracingService tracingService, IApplicationTennantService applicationTennantService, IRepositoryService repositoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {
            
        }

    }
}