﻿namespace XAct.Tasks.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Expressions;
    using XAct.Messages;
    using XAct.Tasks.Models;

    /// <summary>
    /// An implementation of the <see cref="ICurrentApplicationTennantTaskService"/>
    /// to manage Tasks associated to current Application Tennant.
    /// </summary>
    public class CurrentApplicationTennantTaskService : ICurrentApplicationTennantTaskService
    {
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentApplicationTennantTaskService" /> class.
        /// </summary>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public CurrentApplicationTennantTaskService(IApplicationTennantService applicationTennantService,
                                                    IRepositoryService repositoryService)
        {
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
        }



        /// <summary>
        /// Gets the task.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        public Task GetTask(Guid id, bool includeCategory = false, bool includeTags = true, bool includeChildTasks = false)
        {
            var applicationTennantId = _applicationTennantService.Get();

            var result =
                _repositoryService.GetSingle<Task>(
                    x => x.ApplicationTennantId == applicationTennantId && x.Id == id,
                    CreateIncludeSpecification(
                    includeCategory:includeCategory,
                    includeTags:includeTags,
                    includeChildTasks:includeChildTasks)
                    );

            return result;
        }

        /// <summary>
        /// Persists the specified <see cref="Task" />.
        /// </summary>
        /// <param name="task"></param>
        public void PersistOnCommit(Task task)
        {
            _repositoryService.PersistOnCommit<Task, Guid>(task, true);
        }

        /// <summary>
        /// Deletes the specified <see cref="Task" />.
        /// </summary>
        /// <param name="task"></param>
        public void DeleteOnCommit(Task task)
        {
            _repositoryService.DeleteOnCommit(task);
        }







        /// <summary>
        /// Gets the Tasks specific to a user in the current Application Tennant.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="projectId">The project identifier.</param>
        /// <param name="optionalCategoryId">The category identifier.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeCompleted">if set to <c>true</c> [include completed].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        public IQueryable<Task> GetTasksByUser(string userIdentifier,
                                             Guid? projectId,
                                             Guid? optionalCategoryId,
                                             IPagedQuerySpecification pagedQuerySpecification,
                                             bool includeDisabled = true,
                                             bool includeCompleted = true,
                                             bool includeCategory = false,
                                             bool includeTags = true,
                                             bool includeChildTasks = false)
        {
            userIdentifier.ValidateIsNotNullOrEmpty("userIdentifier");

            Guid[] projectIds = projectId.HasValue ? new Guid[] {projectId.Value} : null;

            return InternalGetTasks(
                TaskType.Task,
                projectIds,
                new[] {userIdentifier},
                optionalCategoryId,
                pagedQuerySpecification,
                includeDisabled: includeDisabled, 
                includeCompleted: includeCompleted,
                includeCategory: includeCategory,
                includeTags: includeTags,
                includeChildTasks: includeChildTasks);
                

        }


        /// <summary>
        /// Gets the Tasks specific to a user in the current Application Tennant.
        /// </summary>
        /// <param name="projectId">The project identifier.</param>
        /// <param name="userIdentifiers">The user identifiers.</param>
        /// <param name="optionalCategoryId">The category identifier.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeCompleted">if set to <c>true</c> [include completed].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        public IQueryable<Task> GetTasksByProject(Guid projectId,
                                              string[] userIdentifiers,
                                              Guid? optionalCategoryId,
                                              IPagedQuerySpecification pagedQuerySpecification,
                                              bool includeDisabled = true,
                                              bool includeCompleted = true,
                                              bool includeCategory = false,
                                              bool includeTags = true,
                                              bool includeChildTasks = false)
        {

            projectId.ValidateIsNotDefault("projectId");
            userIdentifiers.ValidateIsNotDefaultOrNotInitialized("userIdentifier");


            Guid[] projectIds = new Guid[]{projectId};

            return InternalGetTasks(
                TaskType.Task,
                projectIds,
                userIdentifiers,
                optionalCategoryId,
                pagedQuerySpecification,
                includeDisabled: includeDisabled,
                includeCompleted: includeCompleted,
                includeCategory: includeCategory,
                includeTags: includeTags,
                includeChildTasks: includeChildTasks);

        }








        private IQueryable<Task> InternalGetTasks(
            TaskType? taskType,
            Guid[] projectIds,
            string[] userIdentifier,
            Guid? optionalCategoryId,
            IPagedQuerySpecification pagedQuerySpecification,
            bool includeDisabled = true,
            bool includeCompleted = true,
            bool includeCategory = false,
            bool includeTags = true,
            bool includeChildTasks = false)
        {
            var applicationTennantId = _applicationTennantService.Get();



            var predicate = PredicateBuilder.True<Task>();

            predicate = predicate.And<Task>(x => (x.ApplicationTennantId == applicationTennantId));



            if ((projectIds != null) && (projectIds.Length > 0))
            {
                if (projectIds.Length == 0)
                {
                    Guid tmp = projectIds[0];

                    predicate = predicate.And<Task>(x => x.ProjectFK == tmp);
                }
                else
                {
                    predicate = predicate.And<Task>(x => projectIds.Contains(x.ProjectFK));
                }
            }




            if ((userIdentifier != null) && (userIdentifier.Length > 0))
            {
                if (userIdentifier.Length == 1)
                {
                    //faster than an IN statement:
                    string tmp = userIdentifier[0];
                    predicate = predicate.And<Task>(x => x.UserIdentifier == tmp);
                }
                else
                {
                    //Use an IN statement:
                    predicate = predicate.And<Task>(x => (userIdentifier.Contains(x.UserIdentifier)));
                }
            }

            if (optionalCategoryId.HasValue)
            {
                predicate = predicate.And<Task>(x => (x.CategoryFK == optionalCategoryId));
            }

            //if (tags!=null)&& (tags.Length>0)
            //{
            //    predicate.And<DistributedTask>(x => ((x.Tags.Any(tags() == categoryId);
            //}


            //I think we always want to return disabled -- just have to watch out for Sum() conditions.
            if (!includeDisabled)
            {
                predicate = predicate.And<Task>(x => x.Enabled == true);
            }

            if (!includeCompleted)
            {
                predicate = predicate.And<Task>(x => !x.EndDateTimeUtc.HasValue);
            }

            if (taskType.HasValue)
            {
                predicate = predicate.And<Task>(x => x.Type == taskType.Value);
            }


            var includeSpecification = CreateIncludeSpecification(
                includeCategory: includeCategory,
                includeTags: includeTags,
                includeChildTasks: includeChildTasks);


            var result =
                _repositoryService.GetByFilter<Task>(
                    predicate,
                    includeSpecification,
                    pagedQuerySpecification
                    );


            return result;
        }

        private static IncludeSpecification<Task> CreateIncludeSpecification(
            bool includeCategory,
            bool includeTags,
            bool includeChildTasks)
        {

            var includesList = new List<Expression<Func<Task, object>>>();

            if (includeCategory)
            {
                includesList.Add(x => x.Category);
            }

            if (includeTags)
            {
                includesList.Add(x => x.Tags);
            }

            if (includeChildTasks)
            {
                includesList.Add(x => x.Children);
            }

            var includeSpecification = new IncludeSpecification<Task>(includesList.ToArray());

            return includeSpecification;
        }
    }
}