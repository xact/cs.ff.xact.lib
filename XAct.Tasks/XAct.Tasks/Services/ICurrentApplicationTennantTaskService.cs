﻿namespace XAct.Tasks
{
    using System;
    using System.Linq;
    using XAct.Messages;
    using XAct.Tasks.Models;


    /// <summary>
    /// Service to manage Tasks in a sinle application.
    /// <para>
    /// Basis of <see cref="ICurrentUserTaskService" />
    /// </para>
    /// </summary>
    public interface ICurrentApplicationTennantTaskService : IHasXActLibService
    {
        /// <summary>
        /// Persists the specified <see cref="Task"/>.
        /// </summary>
        void PersistOnCommit(Task task);

        /// <summary>
        /// Deletes the specified <see cref="Task"/>.
        /// </summary>
        void DeleteOnCommit(Task task);

        /// <summary>
        /// Gets the specified <see cref="Task" />.
        /// (as long as its ApplicationHostId matches the current IApplicationHostService's Current Id).
        /// </summary>
        /// <param name="distributedId">The distributed identifier.</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category and tags].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        Task GetTask(Guid distributedId, bool includeCategory=false, bool includeTags=true, bool includeChildTasks = false);



        /// <summary>
        /// Gets the Tasks specific to a user, across all Projects (or just one Project) in the current Application Tennant.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="projectId">The project identifier.</param>
        /// <param name="optionalCategoryId">The category identifier.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeCompleted">if set to <c>true</c> [include completed].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        IQueryable<Task> GetTasksByUser(string userIdentifier,
                                      Guid? projectId,
                                      Guid? optionalCategoryId,
                                      IPagedQuerySpecification pagedQuerySpecification,
                                      bool includeDisabled = true,
                                      bool includeCompleted = true,
                                      bool includeCategory = false, 
                                      bool includeTags = true, 
                                      bool includeChildTasks = false);


        /// <summary>
        /// Gets the Tasks specific to a project, for all users, or a specific user.
        /// </summary>
        /// <param name="userIdentifiers">The user identifiers.</param>
        /// <param name="projectId">The project identifier.</param>
        /// <param name="optionalCategoryId">The category identifier.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
        /// <param name="includeCompleted">if set to <c>true</c> [include completed].</param>
        /// <param name="includeCategory">if set to <c>true</c> [include category].</param>
        /// <param name="includeTags">if set to <c>true</c> [include tags].</param>
        /// <param name="includeChildTasks">if set to <c>true</c> [include child tasks].</param>
        /// <returns></returns>
        IQueryable<Task> GetTasksByProject(Guid projectId,
                                       string[] userIdentifiers,
                                       Guid? optionalCategoryId,
                                       IPagedQuerySpecification pagedQuerySpecification,
                                       bool includeDisabled = true,
                                       bool includeCompleted = true,
                                       bool includeCategory = false,
                                       bool includeTags = false,
                                       bool includeChildTasks = false);

    }
}