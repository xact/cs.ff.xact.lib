﻿namespace XAct.Tasks
{
    using System;
    using System.Linq;
    using XAct.Domain.Repositories;
    using XAct.Tasks.Models;

    /// <summary>
    /// Contract for managing 
    /// <see cref="TaskProject"/>
    /// </summary>
    public interface ITaskProjectService : IApplicationTennantIdSpecificRepository<TaskProject, Guid>, IHasXActLibService
    {
    }
}