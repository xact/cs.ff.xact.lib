namespace XAct.Workflows.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Diagnostics;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IServerConnectionConfigurationManagerServiceTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Can_Create_And_Set_A_TestServerConnectionConfiguration()
        {

            TestServerConnectionConfiguration connectionConfiguration = 
                new TestServerConnectionConfiguration(DependencyResolver.Current.GetInstance<ITracingService>());


            connectionConfiguration.ServerName = "MyServerName";
            connectionConfiguration.PortNumber = 123;


            Assert.AreEqual("DEMO:MyServerName:123", connectionConfiguration.ConnectionString);
        }


        [Test]
        public void Can_Create_And_Set_A_TestServerConnectionConfiguration_And_Use_It_To_Configure_IServerConnectionConfigurationManagerService()
        {


                        TestServerConnectionConfiguration connectionConfiguration = 
                new TestServerConnectionConfiguration(DependencyResolver.Current.GetInstance<ITracingService>());


            connectionConfiguration.ServerName = "MyServerName";
            connectionConfiguration.PortNumber = 123;



            IServerConnectionConfigurationManagerService serverConnectionConfigurationManager =
                DependencyResolver.Current.GetInstance<IServerConnectionConfigurationManagerService>();


            serverConnectionConfigurationManager.Current = connectionConfiguration;

            Assert.AreEqual(connectionConfiguration, serverConnectionConfigurationManager.Current);



        }

        [Test]
        public void Can_Create_And_Set_A_TestServerConnectionConfiguration_Set_Config_Then_Clear_It()
        {


            TestServerConnectionConfiguration connectionConfiguration =
    new TestServerConnectionConfiguration(DependencyResolver.Current.GetInstance<ITracingService>());


            connectionConfiguration.ServerName = "MyServerName";
            connectionConfiguration.PortNumber = 123;

            IServerConnectionConfigurationManagerService serverConnectionConfigurationManager =
                DependencyResolver.Current.GetInstance<IServerConnectionConfigurationManagerService>();


            serverConnectionConfigurationManager.Current = connectionConfiguration;

            Assert.AreEqual(connectionConfiguration, serverConnectionConfigurationManager.Current);


            //And clearable:
            serverConnectionConfigurationManager.Current = null;

            Assert.AreEqual(null, serverConnectionConfigurationManager.Current);

        }




        [Test]
        public void Can_Create_And_Set_A_TestServerConnectionConfiguration_And_Configure_Service()
        {


            TestServerConnectionConfiguration connectionConfiguration =
    new TestServerConnectionConfiguration(DependencyResolver.Current.GetInstance<ITracingService>());


            connectionConfiguration.ServerName = "MyServerName";
            connectionConfiguration.PortNumber = 123;

            IServerConnectionConfigurationManagerService serverConnectionConfigurationManager =
                DependencyResolver.Current.GetInstance<IServerConnectionConfigurationManagerService>();


            serverConnectionConfigurationManager.Current = connectionConfiguration;

            Assert.AreEqual(connectionConfiguration, serverConnectionConfigurationManager.Current);


            //And same even if different instance of Manager:
            var serverConnectionConfigurationManager2 =
                DependencyResolver.Current.GetInstance<IServerConnectionConfigurationManagerService>();


            Assert.AreEqual(connectionConfiguration, serverConnectionConfigurationManager2.Current);
        }




    }

}
