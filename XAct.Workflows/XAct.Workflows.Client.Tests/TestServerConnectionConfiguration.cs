﻿namespace XAct.Workflows.Tests
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IServerConnectionConfiguration), BindingLifetimeType.TransientScope, Priority.Low /*OK:Secondary Binding*/)]
    public class TestServerConnectionConfiguration : ServerConnectionConfigurationBase,IHasTransientBindingScope
    {
        public TestServerConnectionConfiguration(ITracingService tracingService) : base(tracingService)
        {

        }

        protected override string RebuildConnectionString()
        {
            return "DEMO:{0}:{1}".FormatStringInvariantCulture(this.ServerName, this.PortNumber);
        }
    }
}
