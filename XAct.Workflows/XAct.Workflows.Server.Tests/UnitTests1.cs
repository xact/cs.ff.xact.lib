namespace XAct.Workflows.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class UnitTests1
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
                        Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest01()
        {


            IWorkflowContextService workflowContextService =
                DependencyResolver.Current.GetInstance<IWorkflowContextService>();


            Assert.IsNotNull(workflowContextService);
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest02()
        {


            IWorkflowContextManagementService workflowContextManagementService =
                DependencyResolver.Current.GetInstance<IWorkflowContextManagementService>();

            Assert.IsNotNull(workflowContextManagementService);

            WorkflowContext context = new WorkflowContext("FAKECONTEXT");
            
            workflowContextManagementService.Context = context;

            Assert.AreEqual(context, workflowContextManagementService.Context);
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest03()
        {


            IWorkflowContextManagementService workflowContextManagementService =
                DependencyResolver.Current.GetInstance<IWorkflowContextManagementService>();

            Assert.IsNotNull(workflowContextManagementService);

            WorkflowContext context = new WorkflowContext("FAKECONTEXT");

            workflowContextManagementService.Context = context;


            IWorkflowContextService workflowContextService =
    DependencyResolver.Current.GetInstance<IWorkflowContextService>();

            Assert.AreEqual(context, workflowContextService.Context);
        }

    }


}


