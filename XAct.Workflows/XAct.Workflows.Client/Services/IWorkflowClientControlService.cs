// ReSharper disable CheckNamespace
namespace XAct.Workflows
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a Service to manage Workflow Process Instances
    /// from outside of the Workflow events (either initiating Workflows
    /// or returning the Outcome of a Client Event).
    /// </summary>
    public interface IWorkflowClientControlService : IHasXActLibService
    {

        /// <summary>
        /// Pings this instance.
        /// </summary>
        void Ping(IServerConnectionConfiguration serverConnectionConfiguration);




        /// <summary>
        /// Create, Initialize, and Start a new Instance of a Workflow Process.
        /// <para>
        /// <para>
        /// An example of usage would be from a Drop folder Filewatcher event
        /// that needs to create a new file (in a Db or in SharePoint), and
        /// start a new workflow to process it.
        /// </para>
        /// Code Usage:
        /// <![CDATA[
        /// void FileWatcherEventHandler (object o, EventArgs e){
        /// 
        ///   //Move the file to a destination (eg db)...
        ///   //var loanRequest = ....saved in Db....
        ///   //Create an list of key/values to pass to the workflow right 
        ///   //after it has been started:
        ///   var processVariables = new List<KeyValuePair<string, object>>();
        ///   //namely, the Id of the record set.
        ///   processVariables.Add(new KeyValuePair<string, object>(REQUEST_ID_WF_VAR, loanRequest.Id));
        /// 
        ///   //Start a new workflow, and pass the vars it will need, all in one go:
        ///   ServiceLocatorService.Current
        ///     .GetInstance<IWorkflowClientVariablesService>().
        ///       .CreateAndStartNewProcessInstance(
        ///         "258SPIKES", 
        ///         "LoanRequest_{0} ({1})".FormatStringCurrentCulture(loanRequest.Id);DateTime.Now.ToString(),
        ///         "Datacom.Demos.D07.K2WF08\DemoBank2", //Path to WF as is visible on K2 Server.
        ///         processVariables);
        ///         
        ///   //At this point, WF is now being processed by K2 WF server, 
        ///   //and it will proceed as programmed (for example, the next place 
        ///   //you might hear of this WF, is as an an email received, 
        ///   //alerting of a Client event to handle at a specific website).
        /// }
        /// ]]>
        /// </para>
        /// </summary>
        /// <param name="workflowInstanceTag">A tag/string identifier to give to the workflow instance being started up.</param>
        /// <param name="workflowIdentifier">The unique name of the workflow definition.</param>
        /// <param name="dataFields">An optional set of variables to initialize the workflow with before starting it.</param>
        void CreateAndStartNewProcessInstance(string workflowIdentifier, string workflowInstanceTag,  IEnumerable<KeyValuePair<string, object>> dataFields = null);


        /// <summary>
        /// Invokes the specific Action reponse of a Client Workflow Event.
        /// <para>
        /// For example, response with "Accept" or "Cancel" (the names of the outgoing lines from a Client Event).
        /// </para>
        /// </summary>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).</param>
        /// <param name="action">The action.</param>
        void InvokeProcessInstanceEventOutgoingAction(string serialNumber, string action);


        //void StopProcess();


        //WorkflowInstance GetProcessInstance(string identifier);
    }
}