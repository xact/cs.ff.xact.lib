namespace XAct.Workflows
{
    using XAct.Services;

    /// <summary>
    /// A default implementation of <see cref="IServerConnectionConfigurationManagerService"/>
    /// to keep a reference to the current <see cref="IServerConnectionConfiguration"/>
    /// so that its settings can be centrally accessed.
    /// <para>
    /// Note how the BindingLifetypeType is SingletonPerWebRequestScope.
    /// </para>
    /// </summary>
    public class ServerConnectionConfigurationManagerService : IServerConnectionConfigurationManagerService,IHasWebThreadBindingScope
    {
        /// <summary>
        /// Gets or sets the current <see cref="IServerConnectionConfiguration"/>
        /// from which the connection string can be extracted to create a connection
        /// to the server.
        /// </summary>
        /// <value>
        /// The current <see cref="IServerConnectionConfiguration"/>
        /// </value>
        public IServerConnectionConfiguration Current { get; set; }
    }
}