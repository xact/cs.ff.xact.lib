﻿namespace XAct.Workflows
{
    using XAct.Diagnostics;

    /// <summary>
        /// An implementation of
        /// <see cref="IServerConnectionConfiguration"/>
        /// to contain the connection string information required to connect to a Server 
        /// server.
        /// </summary>
        public abstract class ServerConnectionConfigurationBase : IServerConnectionConfiguration
        {
            /// <summary>
            /// A flag indicating whether the connection string needs to be recalculated.
            /// </summary>
            protected bool Initialized;


            /// <summary>
            /// Gets the tracing service.
            /// </summary>
            protected ITracingService TracingService { get; private set; }

            /// <summary>
            /// Gets the name of the server.
            /// <para>
            /// Example: '258SPIKES'
            /// </para>
            /// </summary>
            /// <value>
            /// The name of the server.
            /// </value>
            public string ServerName { get { return _serverName; } set{_serverName = value; Initialized = false;}}
            string _serverName; 

            /// <summary>
            /// Gets the port number.
            /// <para>
            /// Example: '5555' for K2
            /// </para>
            /// </summary>
            public uint PortNumber { get { return _portNumber; } set { _portNumber = value; Initialized = false; } }
            uint _portNumber; 


            /// <summary>
            /// Gets the connection string built from the 
            /// other variables (<see cref="ServerName"/>, etc.).
            /// </summary>
            public virtual string ConnectionString { get
            {
                if (!Initialized)
                {
                    _connectionString = RebuildConnectionString();
                    TracingService.Trace(TraceLevel.Verbose,
                                          "ServerConnectionConfigurationBase: Rebuilding ConnectionString as '{0}'.",
                                          _connectionString);
                    Initialized = true; 
                }
                return _connectionString;

            } set { _connectionString = value; Initialized = true; } }
            string _connectionString; 

            /// <summary>
            /// Initializes a new instance of the <see cref="ServerConnectionConfigurationBase"/> class.
            /// </summary>
            /// <param name="tracingService">The tracing service.</param>
            public ServerConnectionConfigurationBase(ITracingService tracingService)
            {
                tracingService.ValidateIsNotDefault("tracingService");

                TracingService = tracingService;

                //Build ConnectionString early:

// ReSharper disable DoNotCallOverridableMethodsInConstructor
                this.ConnectionString = RebuildConnectionString();
// ReSharper restore DoNotCallOverridableMethodsInConstructor

            }

            /// <summary>
            /// Returns a <see cref="System.String"/> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String"/> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return ConnectionString;
            }


            /// <summary>
            /// Invoked by <see cref="ConnectionString"/> according to <see cref="Initialized"/> 
            /// in order to rebuilds the connection string.
            /// </summary>
            protected virtual string RebuildConnectionString()
            {
                return "[ConnectionString Not Set]";
            }
        }
}
