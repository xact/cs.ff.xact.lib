﻿

namespace XAct.Workflows
{

     /// <summary>
    /// Contract for a configuration package used to hold the settings
    /// required to connect to a server.
    /// </summary>
    public interface IServerConnectionConfiguration
    {
        /// <summary>
        /// Gets or sets the name of the server.
        /// <para>
        /// Example: '258SPIKES.local'
        /// </para>
        /// </summary>
        /// <value>
        /// The name of the server.
        /// </value>
        string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the port number.
        /// <para>
        /// Example: '5555' for K2
        /// </para>
        /// </summary>
        uint PortNumber { get; set; }


        /// <summary>
        /// Gets the connection string built from the other variables (<see cref="ServerName"/>, etc.).
        /// <para>
        /// NOTE: also available via <c>ToString</c>
        /// </para>
        /// </summary>
        string ConnectionString { get; }

    }
}
