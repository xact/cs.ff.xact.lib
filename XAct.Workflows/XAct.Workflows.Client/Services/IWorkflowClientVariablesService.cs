﻿namespace XAct.Workflows
{
    using System.Collections.Generic;

    /// <summary>
    /// A Contract for getting Server Instance Field values over the wire (without 
    /// having a Context object).
    /// </summary>
    public interface IWorkflowClientVariablesService : IHasXActLibService
    {

        /// <summary>
        /// Gets the DataFields of a Workflow Process Instance.
        /// <para>
        /// Retrieves the list of all datafields for the specific process.
        /// <code>
        /// <![CDATA[
        /// var serialNumberOfCurrentEventItem =
        ///   ((SourceCode.KO.ServerEventContext)k2workflowContext).SerialNumber;
        /// 
        /// Dictionary<string,object> values = 
        ///   ServiceLocatorService.Current
        ///     .GetInstance<IWorkflowClientVariablesService>().
        ///       .GetProcessInstanceVariables(
        ///         "258SPIKES", 
        ///         
        ///         "Datacom.Demos.D07.K2WF08\DemoBank2" //Path to WF as is visible on K2 Server.
        ///       );
        ///   //Prove that one has a value returned (eg: a db record id):
        ///   Assert.IsNotNull(values[REQUEST_ID_WF_VAR]);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).</param>
        /// <param name="variableKeys">The variable keys.</param>
        /// <returns></returns>
        Dictionary<string, object> GetProcessInstanceVariables(string serialNumber, IEnumerable<string> variableKeys = null);


        /// <summary>
        /// Sets the process instance variables.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="dataFields">The data fields.</param>
        void SetProcessInstanceVariables(string serialNumber, IEnumerable<KeyValuePair<string, object>> dataFields = null);

    }
}
