﻿namespace XAct.Workflows
{
    using System.Collections.Generic;

    /// <summary>
    /// A Contract for getting Workflow Server Host Settings values over the wire (without 
    /// having a Context object).
    /// </summary>
    /// <internal>
    /// Note that it does not implement IHostSettingsService contract
    /// in order to promote chunky communication.
    /// </internal>
    public interface IWorkflowClientHostSettingsService : IHasXActLibService
    {

        /// <summary>
        /// Gets the specified host settings.
        /// </summary>
        /// <param name="serverId">The server id.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="variableKeys">The variable keys.</param>
        /// <returns></returns>
        Dictionary<string, object> Get(string serverId, string serialNumber, IEnumerable<string> variableKeys = null);

    }
}
