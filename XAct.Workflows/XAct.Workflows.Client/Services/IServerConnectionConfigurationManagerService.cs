﻿
namespace XAct.Workflows
{
     /// <summary>
    /// Contract for a Manager to keep a reference of 
    /// the current <see cref="IServerConnectionConfiguration"/>
    /// so that its settings can be centrally accessed.
    /// </summary>    
    public interface IServerConnectionConfigurationManagerService : IHasXActLibService
    {
        /// <summary>
        /// Gets or sets the current <see cref="IServerConnectionConfiguration"/>
        /// from which the connection string can be extracted to create a connection
        /// to the server.
        /// </summary>
        /// <value>
        /// The current <see cref="IServerConnectionConfiguration"/>
        /// </value>
        IServerConnectionConfiguration Current { get; set; }
    }
}
