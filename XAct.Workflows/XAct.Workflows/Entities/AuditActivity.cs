﻿
namespace XAct.Workflows
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class AuditActivity



    {
        /// <summary>
        /// 
        /// </summary>
        public int ActivityInstanceDestinationId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ActivityName { get; set; }


        /// <summary>
        /// Gets or sets the activity instance id.
        /// </summary>
        /// <value>
        /// The activity instance id.
        /// </value>
        public int ActivityInstanceId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int ProcessInstanceId { get; set; }



        /// <summary>
        /// 
        /// </summary>
        public string Destination { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime FinishDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Folio { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ExpectedDuration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ActivityId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FinalAction { get; set; }
    }
}
