﻿namespace XAct.Workflows
{
    using System;

    /// <summary>
    /// <para>
    /// Used to report on tasks for a user
    /// </para>
    /// </summary>
    public class TaskSummary 
    {

        /// <summary>
        /// 
        /// </summary>
        public string Title;

        /// <summary>
        /// 
        /// </summary>
        public DateTime DueDate;

        /// <summary>
        /// <para>
        /// Name of user Task is for (blank if not yet assigned).
        /// </para>
        /// </summary>
        public string UserName;
    }
}
