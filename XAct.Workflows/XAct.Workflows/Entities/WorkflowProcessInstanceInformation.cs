﻿namespace XAct.Workflows
{
    using System;

    /// <summary>
    /// Information about a single Workflow Process Instance.
    /// </summary>
    public class WorkflowProcessInstanceInformation
    {
        /// <summary>
        /// The unique identifier for this Process
        /// </summary>
        public int ProcessId { get; set; }

        /// <summary>
        /// Gets or sets the process instance id.
        /// </summary>
        /// <value>
        /// The process instance id.
        /// </value>
        public int ProcessInstanceId { get; set; }

        /// <summary>
        /// Gets or sets the Process Name/Type that this is an instance of.
        /// </summary>
        /// <value>
        /// The name of the process.
        /// </value>
        public string ProcessName { get; set; }

        /// <summary>
        /// Gets or sets the unique Tag/Folio of the Process Instance.
        /// </summary>
        /// <value>
        /// The folio.
        /// </value>
        public string Folio { get; set; }

        /// <summary>
        /// Gets or sets the start date of the process.
        /// </summary>
        /// <value>
        /// The start date.
        /// </value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date of the process instance (if set).
        /// </summary>
        /// <value>
        /// The end date.
        /// </value>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the Workflow status.
        /// </summary>
        /// <remarks>
        /// Expressed in a non-vendor specific way.
        /// </remarks>
        /// <value>
        /// The status.
        /// </value>
        public WorkflowStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the duration of the process.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        /// <internal>
        /// TODO: Define meaning of Duration
        /// </internal>
        public int Duration { get; set; }

        /// <summary>
        /// Gets or sets the priority of the workflow.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        /// <internal>
        /// TODO: Define meaning of Priority, exactly.
        /// </internal>
        public int Priority { get; set; }
    }
}
