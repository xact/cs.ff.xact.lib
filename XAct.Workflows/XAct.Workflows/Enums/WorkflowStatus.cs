﻿

namespace XAct.Workflows
{

    /// <summary>
    /// Process Status Enumeration.
    /// </summary>
    /// <internal>
    /// Valus are equal to K2.
    /// </internal>
    public enum WorkflowStatus
    {

        /// <summary>
        /// <para>Value= -100</para>
        /// </summary>
        Error = -100,


        /// <summary>
        /// Havn't queried 
        /// <para>
        /// Value = -1
        /// </para>
        /// </summary>
        Unknown = -1,


        /// <summary>
        /// Error State
        /// <para>
        /// Value = 0
        /// </para>
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// <para>Value=1</para>
        /// </summary>
        Running = 1,

        /// <summary>
        /// <para>Value=2</para>
        /// </summary>
        Active = 2,

        /// <summary>
        /// <para>Value=3</para>
        /// </summary>
        Completed = 3,

        /// <summary>
        /// <para>Value=4</para>
        /// </summary>
        Stopped = 4,

        /// <summary>
        /// <para>Value=5</para>
        /// </summary>
        Deleted = 5
    }
}


