﻿//using SourceCode.Hosting.Client.BaseAPI;
//using SourceCode.Workflow.Management;
//using SourceCode.Workflow.Management.Criteria;

//namespace XAct
//{
//    /// <summary>
//    /// Extensions to the <see cref="WorkflowManagementServer"/> object.
//    /// </summary>
//    public static class WorkflowManagementServerExtensions
//    {
//        /// <summary>
//        /// Creates a new Connection to the server, and opens it for further commands.
//        /// </summary>
//        /// <param name="workflowManagementServer">The workflow management server.</param>
//        /// <param name="connectionString">The connection string.</param>
//        public static BaseAPIConnection CreateAndOpenConnection(this WorkflowManagementServer workflowManagementServer, string connectionString)
//        {
//            BaseAPIConnection connection = workflowManagementServer.CreateConnection();
//            connection.Open(connectionString);
//            return connection;
//        }

//        /// <summary>
//        /// Stops the process instance by by process id.
//        /// </summary>
//        /// <param name="workflowManagementServer">The workflow management server.</param>
//        /// <param name="proccessInstanceId">The proccess instance id.</param>
//        public static void StopProcessInstanceByByProcessId (this WorkflowManagementServer workflowManagementServer, int proccessInstanceId)
//        {
//            workflowManagementServer.StopProcessInstances(proccessInstanceId);

//        }
//        /// <summary>
//        /// Stops the process instance matchign the given tag/folio.
//        /// </summary>
//        /// <param name="workflowManagementServer">The workflow management server.</param>
//        /// <param name="folio">The folio.</param>
//        public static void StopProcessInstanceByProcessByFolioName(this WorkflowManagementServer workflowManagementServer, string folio)
//        {
//            ProcessInstances procInst = workflowManagementServer.GetProcessInstancesAll(string.Empty, folio, string.Empty);
        
//            foreach(ProcessInstance processInstance in procInst)
//            {
//                workflowManagementServer.StopProcessInstances(processInstance.ID);
//            }
//       }
//    }
//}
