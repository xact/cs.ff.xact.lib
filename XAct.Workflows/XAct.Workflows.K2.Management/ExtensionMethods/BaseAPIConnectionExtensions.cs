﻿//using SourceCode.Hosting.Client.BaseAPI;

namespace XAct.Workflows.ExtensionMethods
{
    /// <summary>
    /// </summary>
    public static class BaseAPIConnectionExtensions 
    {
        ///// <summary>
        ///// <para>
        ///// An XActLib Extension.
        ///// </para>
        ///// Closes and Disposes of the connection.
        ///// <para>
        ///// Note that using {...} invokes Dispose, but K2's implementation of Dispose doesn't invoke Close :-(
        ///// </para>
        ///// </summary>
        ///// <param name="baseApiConnection">The base API connection.</param>
        //public static void CloseAndDispose(this BaseAPIConnection baseApiConnection)
        //{
        //    baseApiConnection.Close();
        //    baseApiConnection.Dispose();
        //}
    }
}
