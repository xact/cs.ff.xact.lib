﻿namespace XAct.Workflows
{
    /// <summary>
    /// A contract for managing K2 Workflows
    /// </summary>
    public interface IK2WorkflowManagementService : IHasXActLibService
    {
    }
}
