﻿namespace XAct
{
    using System;
    using XAct.Diagnostics;
    using XAct.Workflows;

    /// <summary>
    /// Extensions to the <see cref="WorkflowManagementServerDisposableContainer"/>
    /// object.
    /// </summary>
    public static class WorkflowManagementServerDisposableContainerExtensions
    {


                /// <summary>
        /// Gets the tracing service.
        /// </summary>
        private static ITracingService TracingService
        {
            get
            {
                return DependencyResolver.Current.GetInstance<ITracingService>();
            }
        }





        /// <summary>
        /// Stops the workflow instances that match the given fullProcessPath
        /// </summary>
        /// <param name="workflowManagementServerDisposableContainer">The workflow management server disposable container.</param>
        /// <param name="fullProcessPath">The full process path.</param>
        public static void StopWorkflowInstances(this WorkflowManagementServerDisposableContainer workflowManagementServerDisposableContainer, string fullProcessPath)
        {

            TracingService.Trace(TraceLevel.Verbose, "StopWorkflowInstances: Stopping process Type:{0} Begin...", fullProcessPath);

            //As opening connection is expensive, refactor as much to ExtensionMethods
            //that can be chained together during one connection:
            workflowManagementServerDisposableContainer.WorkflowManagementServer.StopWorkflowInstances(fullProcessPath);

            TracingService.Trace(TraceLevel.Verbose, "StopWorkflowInstances: Stopping process Type:{0}  Complete.", fullProcessPath);

        }

        /// <summary>
        /// Stops the workflow instance.
        /// </summary>
        /// <param name="workflowManagementServerDisposableContainer">The workflow management server disposable container.</param>
        /// <param name="processInstanceId">The process instance id.</param>
        public static void StopWorkflowInstance(this WorkflowManagementServerDisposableContainer workflowManagementServerDisposableContainer, int processInstanceId)
        {
            TracingService.Trace(TraceLevel.Verbose, "StopWorkflowInstance: Stopping process Type:{0} Begin...", processInstanceId);
            bool result = workflowManagementServerDisposableContainer.WorkflowManagementServer.StopProcessInstances(processInstanceId);

            if (!result)
            {
                throw new Exception("StopWorkflowInstance failed for {0} failed.".FormatStringInvariantCulture(processInstanceId));
            }
        }
    }
}
