﻿namespace XAct
{
    using System.Collections.Generic;
    using System.Linq;
    using SourceCode.Workflow.Client;
    using XAct.Diagnostics;

    /// <summary>
    /// Extensions to the K2 Client ProcessInstance
    /// </summary>
    public static class ProcessInstanceExtensions
    {
        /// <summary>
        /// Gets the tracing service.
        /// </summary>
        private static ITracingService TracingService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<ITracingService>(); }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Sets the Workflow Process Instance's data fields.
        /// </summary>
        /// <param name="processInstance">The new process.</param>
        /// <param name="dataFields">The initial data fields.</param>
        public static void SetDataFields(this ProcessInstance processInstance, IEnumerable<KeyValuePair<string, object>> dataFields)
        {
            processInstance.ValidateIsNotDefault("processInstance");

            if (dataFields == null)
            {
                return;
            }

            foreach (KeyValuePair<string, object> keyValuePair in dataFields)
            {
                TracingService.Trace(TraceLevel.Verbose,
                                     "CreateAndStartNewProcessInstance: setting datafield {0} to {1}",
                                     keyValuePair.Key, keyValuePair.Value);

                processInstance.DataFields[keyValuePair.Key].Value = keyValuePair.Value;
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the Workflow Process Instance's data fields.
        /// </summary>
        /// <param name="processInstance">The process instance.</param>
        /// <param name="variableKeys">The variable keys.</param>
        /// <returns></returns>
        public static Dictionary<string,object> GetDataFields(this ProcessInstance processInstance, IEnumerable<string> variableKeys=null)
        {
            Dictionary<string, object> variableResults = new Dictionary<string, object>();

            if (variableKeys == null)
            {
                foreach (DataField dataField in processInstance.DataFields)
                {
                    variableResults.Add(dataField.Name, dataField.Value);
                }
            }
            else
            {
                foreach (string variable in variableKeys)
                {
                    object variableValue = processInstance.DataFields[variable].Value;

                    variableResults.Add(variable, variableValue);
                }
            }
            return variableResults;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Does the DataField exist?
        /// </summary>
        /// <param name="processInstance">The process instance.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static bool DataFieldExist(this ProcessInstance processInstance, string key)
        {

            return processInstance.DataFields.Cast<DataField>().Any(x => x.Name == key);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Tries to get the DataField value, it it exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="processInstance">The process instance.</param>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        public static bool TryGetDataField<T>(this ProcessInstance processInstance, string key, out T result)
        {
            foreach(DataField dataField in processInstance.DataFields)
            {
                if (dataField.Name != key)
                {
                    continue;
                }

                result = dataField.Value.ConvertTo<T>();
                return true;
            }
            result = default(T);
            return false;
        }
        

    }
}
