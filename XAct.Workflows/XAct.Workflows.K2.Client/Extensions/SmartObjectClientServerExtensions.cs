﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SourceCode.SmartObjects.Client;
    using SourceCode.SmartObjects.Client.Filters;
    using XAct.Diagnostics;
    using XAct.Workflows;

    /// <summary>
    /// Extension method to the K2 <see cref="SmartObjectClientServer"/> object.
    /// </summary>
    public static class SmartObjectClientServerExtensions
    {
        /// <summary>
        /// Gets the tracing service.
        /// </summary>
        private static ITracingService TracingService
        {
            get
            {
                return DependencyResolver.Current.GetInstance<ITracingService>();
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// returns a status for a workflow instance
        /// </summary>
        /// <param name="smartObjectClientServer"></param>
        /// <param name="processIdentifier"></param>
        /// <returns></returns>
        public static WorkflowStatus GetWorkflowStatus(this SmartObjectClientServer smartObjectClientServer, int processIdentifier)
        {

            smartObjectClientServer.ValidateIsNotDefault("smartObjectClientServer");

            // setup the search
            TracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatus: Setting up search");
            SmartObject smartObject = smartObjectClientServer.GetSmartObject("Process_Instance");
            SmartListMethod list = smartObject.ListMethods["List"];
            smartObject.MethodToExecute = list.Name;

            // add a filter for the given process id
            Equals idFilter = new Equals(new PropertyExpression("ProcessInstanceID", PropertyType.Number),
                                         new ValueExpression(processIdentifier, PropertyType.Number));
            list.Filter = idFilter;

            // do the search
            TracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatus: Starting search");
            SmartObjectList smoList = smartObjectClientServer.ExecuteList(smartObject);

            if (smoList.SmartObjectsList.Count < 1)
                throw new Exception(String.Format("GetWorkflowStatus: Unable to find process with id {0}",
                                                  processIdentifier));

            // get the status from our search results
            TracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatus: Parsing results");
            string status = smoList.SmartObjectsList[0].Properties["Status"].Value;


            // parse the status to our enum


            WorkflowStatus currentStatus;
            try
            {
                currentStatus = (WorkflowStatus)Enum.Parse(typeof(WorkflowStatus), status);
            }
            catch
            {
                throw new Exception(
                    "GetProcessInstanceInformation: Unable to parse status of {0} to workflow status".
                        FormatStringCurrentCulture(status));
            }


            return currentStatus;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// gets a list of activity names for a given process
        /// </summary>
        /// <param name="smartObjectClientServer"></param>
        /// <param name="processId"></param>
        /// <returns></returns>
        public static List<string> GetActivitiesForProcess(this SmartObjectClientServer smartObjectClientServer, int processId)
        {
            smartObjectClientServer.ValidateIsNotDefault("smartObjectClientServer");

            // setup the search
            TracingService.Trace(TraceLevel.Verbose, "GetActivitiesForProcess: Setting up search");
            SmartObject smartObject = smartObjectClientServer.GetSmartObject("Process_Activities");
            SmartListMethod list = smartObject.ListMethods["List"];
            smartObject.MethodToExecute = list.Name;
            smartObject.Properties["Process_ID"].Value = processId.ToString();

            // do the search
            TracingService.Trace(TraceLevel.Verbose, "GetActivitiesForProcess: Starting search for {0}", processId);
            SmartObjectList smoList = smartObjectClientServer.ExecuteList(smartObject);

            if (smoList.SmartObjectsList.Count == 0)
                throw new Exception(String.Format("GetActivitiesForProcess: Unable to find activities for process with id {0}", processId));

            // get the results
            TracingService.Trace(TraceLevel.Verbose, "GetActivitiesForProcess: Parsing {0} results", smoList.SmartObjectsList.Count);
            List<string> activityNames = new List<string>();
            foreach (SmartObject smartObject2 in smoList.SmartObjectsList)
            {
                activityNames.Add(smartObject2.Properties["Activity_Name"].Value);
            }
            return activityNames;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the process instance information.
        /// </summary>
        /// <param name="smartObjectClientServer">The smart object client server.</param>
        /// <param name="processInstanceId">The process instance id.</param>
        /// <returns></returns>
        public static WorkflowProcessInstanceInformation GetProcessInstanceInformation(this SmartObjectClientServer smartObjectClientServer, int processInstanceId)
        {
            smartObjectClientServer.ValidateIsNotDefault("smartObjectClientServer");

            // setup the search
            TracingService.Trace(TraceLevel.Verbose, "GetProcessInstanceInformation: Setting up search");

            SmartObject smartObject = smartObjectClientServer.GetSmartObject("Process_Instance");
            SmartListMethod list = smartObject.ListMethods["List"];
            smartObject.MethodToExecute = list.Name;

            // add a filter for the given process id
            TracingService.Trace(TraceLevel.Verbose, "GetProcessInstanceInformation: creating filter for {0}", processInstanceId);

            Equals idFilter = new Equals(new PropertyExpression("ProcessInstanceID", PropertyType.Number), new ValueExpression(processInstanceId, PropertyType.Number));

            list.Filter = idFilter;

            // do the search
            TracingService.Trace(TraceLevel.Verbose, "GetProcessInstanceInformation: Starting search");
            SmartObjectList smoList = smartObjectClientServer.ExecuteList(smartObject);

            if (smoList.SmartObjectsList.Count < 1)
                throw new Exception(String.Format("GetWorkflowStatus: Unable to find process with id {0}", processInstanceId));

            // get the status from our search results
            TracingService.Trace(TraceLevel.Verbose, "GetProcessInstanceInformation: parsing {0} results", smoList.SmartObjectsList.Count);
            string status = smoList.SmartObjectsList[0].Properties["Status"].Value;

            // parse the status to our enum
            WorkflowStatus currentStatus = GetWorkflowStatusFromString(status);

            // do an incredibly error prone mapping
            WorkflowProcessInstanceInformation info = new WorkflowProcessInstanceInformation();
            info.Duration = int.Parse(smoList.SmartObjectsList[0].Properties["Duration"].Value);
            info.EndDate = DateTime.Parse(smoList.SmartObjectsList[0].Properties["EndDate"].Value);
            info.Folio = smoList.SmartObjectsList[0].Properties["Folio"].Value;
            info.Priority = int.Parse(smoList.SmartObjectsList[0].Properties["Priority"].Value);
            info.ProcessId = int.Parse(smoList.SmartObjectsList[0].Properties["ProcessId"].Value);
            info.ProcessInstanceId = int.Parse(smoList.SmartObjectsList[0].Properties["ProcessInstanceId"].Value);
            info.ProcessName = smoList.SmartObjectsList[0].Properties["ProcessName"].Value;
            info.StartDate = DateTime.Parse(smoList.SmartObjectsList[0].Properties["ProcessName"].Value);
            info.Status = currentStatus;

            return info;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the activity audit for process.
        /// </summary>
        /// <param name="smartObjectClientServer">The smart object client server.</param>
        /// <param name="processInstanceId">The process instance id.</param>
        /// <returns></returns>
        public static List<AuditActivity> GetActivityAuditForProcess(this SmartObjectClientServer smartObjectClientServer, int processInstanceId)
        {
            smartObjectClientServer.ValidateIsNotDefault("smartObjectClientServer");

            // setup the search
            TracingService.Trace(TraceLevel.Verbose, "GetActivityAuditForProcess: Setting up search for {0}", processInstanceId);
            SmartObject smartObject = smartObjectClientServer.GetSmartObject("Activity_Instance_Destination");
            SmartListMethod list = smartObject.ListMethods["List"];
            smartObject.MethodToExecute = list.Name;

            smartObject.Properties["ProcessInstanceID"].Value = processInstanceId.ToString();

            // do the search
            TracingService.Trace(TraceLevel.Verbose, "GetActivityAuditForProcess: Starting search");
            SmartObjectList smoList = smartObjectClientServer.ExecuteList(smartObject);

            TracingService.Trace(TraceLevel.Verbose, "GetActivityAuditForProcess: Parsing {0} results", smoList.SmartObjectsList.Count);
            List<AuditActivity> activities = new List<AuditActivity>();
            foreach (SmartObject smartObject2 in smoList.SmartObjectsList)
            {
                // i'm sure this will be fine.
                activities.Add(new AuditActivity
                {
                    ActivityId = int.Parse(smartObject2.Properties["ActivityID"].Value),
                    ActivityInstanceDestinationId = int.Parse(smartObject2.Properties["ActivityInstanceDestinationID"].Value),
                    ActivityInstanceId = int.Parse(smartObject2.Properties["ActivityInstanceID"].Value),
                    ActivityName = smartObject2.Properties["ActivityName"].Value,
                    Destination = smartObject2.Properties["Destination"].Value,
                    Duration = int.Parse(smartObject2.Properties["Duration"].Value),
                    ExpectedDuration = int.Parse(smartObject2.Properties["ExpectedDuration"].Value),
                    FinalAction = smartObject2.Properties["Final_Action"].Value,
                    FinishDate = DateTime.Parse(smartObject2.Properties["FinishDate"].Value),
                    Folio = smartObject2.Properties["Folio"].Value,
                    ProcessInstanceId = int.Parse(smartObject2.Properties["ProcessInstanceID"].Value),
                    ProcessName = smartObject2.Properties["ProcessName"].Value,
                    StartDate = DateTime.Parse(smartObject2.Properties["StartDate"].Value),
                    Status = smartObject2.Properties["Status"].Value
                });
            }
            return activities;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// gets a list of actions that have occurred for a process along with the user who did the action
        /// </summary>
        /// <param name="smartObjectClientServer"></param>
        /// <param name="processInstanceId"></param>
        /// <returns></returns>
        public static List<AuditAction> GetActionAuditForProcess(this SmartObjectClientServer smartObjectClientServer, int processInstanceId)
        {
            smartObjectClientServer.ValidateIsNotDefault("smartObjectClientServer");

            // setup the search
            TracingService.Trace(TraceLevel.Verbose, "GetActionAuditForProcess: Setting up search for {0}", processInstanceId);
            SmartObject smartObject = smartObjectClientServer.GetSmartObject("Activity_Slot_Data");
            SmartListMethod list = smartObject.ListMethods["List"];
            smartObject.MethodToExecute = list.Name;
            smartObject.Properties["ProcessInstanceID"].Value = processInstanceId.ToString();

            // do the search
            TracingService.Trace(TraceLevel.Verbose, "GetActionAuditForProcess: Starting search");
            SmartObjectList smoList = smartObjectClientServer.ExecuteList(smartObject);

            TracingService.Trace(TraceLevel.Verbose, "GetActionAuditForProcess: Parsing {0} results", smoList.SmartObjectsList.Count);
            List<AuditAction> actions = new List<AuditAction>();
            foreach (SmartObject smartObject2 in smoList.SmartObjectsList)
            {
                if (smartObject2.Properties["DataName"].Value != "Action Result") continue;
                actions.Add(new AuditAction
                {
                    UserName = smartObject2.Properties["Destination"].Value,
                    DataName = smartObject2.Properties["DataName"].Value,
                    DataValue = smartObject2.Properties["DataValue"].Value
                });
            }
            return actions;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// takes a heap of workflow identifiers and returns their statuses
        /// </summary>
        /// <param name="smartObjectClientServer"></param>
        /// <param name="workflowIndentifiers"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<int, WorkflowStatus>> WorkflowStatuses(this SmartObjectClientServer smartObjectClientServer, IEnumerable<int> workflowIndentifiers)
        {
            smartObjectClientServer.ValidateIsNotDefault("smartObjectClientServer");

            // setup the search
            TracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatuses: Setting up search");

            SmartObject smartObject = smartObjectClientServer.GetSmartObject("Process_Instance");
            SmartListMethod list = smartObject.ListMethods["List"];

            smartObject.MethodToExecute = list.Name;

            // get all the filters
            TracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatuses: Creating filters for workflow identifiers");


            List<Equals> equalsFilters = new List<Equals>();
            foreach (int processIdentifier in workflowIndentifiers)
            {
                Equals newFilter = new Equals(new PropertyExpression("ProcessInstanceID", PropertyType.Number),
                                              new ValueExpression(processIdentifier, PropertyType.Number));

                equalsFilters.Add(newFilter);
            }


            // turn our filters into something K2 (hur dur) can deal with
            TracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatuses: Combining filters into gigantic tree of doom");

            if (equalsFilters.Count == 1)
            {
                list.Filter = equalsFilters.First();
            }
            else
            {
                list.Filter = MakeAStupidFilter(equalsFilters);
            }

            // do the search
            TracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatuses: Starting search");
            SmartObjectList smoList = smartObjectClientServer.ExecuteList(smartObject);


            TracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatuses: Parsing search results for return");

            // get the status from our search results
            //Dictionary<string, Process.ProcessStatus> results = new Dictionary<string, Process.ProcessStatus>();

            foreach (SmartObject smartObject2 in smoList.SmartObjectsList)
            {
                int processId = int.Parse(smartObject2.Properties["ProcessInstanceID"].Value);

                yield return new KeyValuePair<int, WorkflowStatus>
                                 (
                                     processId,
                                     GetWorkflowStatusFromString(smartObject2.Properties["Status"].Value)
                                 );
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// builds up a big ol' tree of OR filters based on a list of equal filters
        /// </summary>
        /// <param name="remainingList"></param>
        /// <returns>an Or filter</returns>
        private static Or MakeAStupidFilter(IList<Equals> remainingList)
        {
            if (remainingList.Count == 1)
                throw new Exception("Can't make an Or filter if only one filter item.");

            if (remainingList.Count == 2)
            {
                Or newFilter = new Or(remainingList[0], remainingList[1]);
                return newFilter;
            }

            // more than 2 items
            // take one, remove from list, pass rest on
            Or weeFilter = new Or();
            weeFilter.Left = remainingList[0];
            remainingList.RemoveAt(0);
            weeFilter.Right = MakeAStupidFilter(remainingList);
            return weeFilter;
        }



        // todo move me and make accessible to other stuff
        private static WorkflowStatus GetWorkflowStatusFromString(string status)
        {
            // parse the status to our enum
            WorkflowStatus currentStatus;
            try
            {
                currentStatus = (WorkflowStatus)Enum.Parse(typeof(WorkflowStatus), status);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetWorkflowStatusFromString: Unable to parse status of {0} to workflow status", status), ex);
            }
            return currentStatus;
        }

    }
}


