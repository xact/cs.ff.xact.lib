﻿namespace XAct
{
    using System.Collections.Generic;
    using SourceCode.Workflow.Client;

    /// <summary>
    /// Extensions to the K2 WorkList object
    /// </summary>
    public static class WorkListExtensions
    {
        /// <summary>
        /// Gets an array of <see cref="WorkListSummary"/>
        /// entities from the given <see cref="Worklist"/>
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static WorkListSummary[] GetWorkListSummary(Worklist list)
        {
            List<WorkListSummary> results = new List<WorkListSummary>();


            if (list == null || list.Count == 0)
            {
                return new WorkListSummary[0];
            }
            foreach (WorklistItem item in list)
            {
                WorkListSummary x = new WorkListSummary
                            {
                                Tag = item.ProcessInstance.Folio,
                                ProcessId = item.ProcessInstance.ID,
                                StartDate = item.ProcessInstance.StartDate,
                                                                    SerialNumber =item.SerialNumber,
                                Id = item.ID,
                            };




                results.Add(x);
            }
            return results.ToArray();
        }

    }
}
