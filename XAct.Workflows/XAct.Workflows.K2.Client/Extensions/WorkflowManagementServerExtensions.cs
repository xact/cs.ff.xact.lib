﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SourceCode.Workflow.Management;
    using XAct.Diagnostics;
    using XAct.Messages;
    using XAct.Workflows;

    /// <summary>
    /// Extension Methods to the K2 <see cref="WorkflowManagementServer"/> object.
    /// </summary>
    public static class WorkflowManagementServerExtensions
    {
        /// <summary>
        /// Gets the tracing service.
        /// </summary>
        private static ITracingService TracingService
        {
            get { return DependencyResolver.Current.GetInstance<ITracingService>(); }
        }




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Creates and opens a connection to the workflow server.
        /// <para>
        /// IMPORTANT: Opening a new connection is *expensive* as it involves authentication, etc.
        /// </para>
        /// </summary>
        /// <param name="workflowManagementServer">The workflow management server.</param>
        /// <param name="connectionString">The connection string.</param>
        public static void CreateAndOpenConnection(this WorkflowManagementServer workflowManagementServer,
                                                   string connectionString)
        {
            workflowManagementServer.CreateConnection();
            workflowManagementServer.Connection.Open(connectionString);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Tasks the summaries.
        /// </summary>
        /// <param name="workflowManagementServer">The workflow management server.</param>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        [Obsolete("The method is often highly unefficient as it returns *all* Task summaries for the user.")]
        public static IEnumerable<TaskSummary> GetTaskSummaries(this WorkflowManagementServer workflowManagementServer, string userName)
        {
            WorklistItems worklistItems = workflowManagementServer.GetWorklistItems(
                userName
                ,"" //ProcessName
                ,"" //Destination
                ,""//eventName 
                ,"" //folio
                ,"" //fromDate
                ,"" //toDate
                );


            return from WorklistItem workListItem in worklistItems
                   select new TaskSummary
                              {
                                  Title = workListItem.ActivityName,
                                  UserName = userName
                              };
        }

        /// <summary>
        /// Gets a paged list of Task Summaries for the given user
        /// </summary>
        /// <param name="workflowManagementServer">The workflow management server.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        public static IEnumerable<TaskSummary> GetTaskSummaries(this WorkflowManagementServer workflowManagementServer, string userName, IPagedQuerySpecification pagedQuerySpecification)
        {

            workflowManagementServer.ValidateIsNotDefault("workflowManagementServer");
            userName.ValidateIsNotNullOrEmpty("userName");
            pagedQuerySpecification.ValidateIsNotDefault("pagedQuerySpecification");


            //int recordCount;

            throw new NotImplementedException("TODO");

            //WorklistCriteriaFilter worklistCriteriaFilter = new WorklistCriteriaFilter();
            //worklistCriteriaFilter.AddRegularFilter(new WorklistFields(), Comparison.Equals, userName);
            //worklistCriteriaFilter.PageIndex = pagedQuerySpecification.PageIndex;
            //worklistCriteriaFilter.PageSize = worklistCriteriaFilter.PageSize;


            //WorklistItems results = workflowManagementServer.GetWorklistItems(worklistCriteriaFilter);


            //return from WorklistItem stupid in results
            //       select new TaskSummary
            //       {
            //           Title = stupid.ActivityName,
            //           UserName = userName
            //       };
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the process instance information.
        /// </summary>
        /// <param name="workflowManagementServer">The workflow management server.</param>
        /// <param name="processInstanceId">The process instance id.</param>
        /// <returns></returns>
        public static WorkflowProcessInstanceInformation GetProcessInstanceInformation(
            this WorkflowManagementServer workflowManagementServer, int processInstanceId)
        {
            workflowManagementServer.ValidateIsNotDefault("workflowManagementServer");


            ProcessInstances whatevers = workflowManagementServer.GetProcessInstances();
            ProcessInstance what = whatevers[0];

            WorkflowStatus currentStatus;

            try
            {
                currentStatus = (WorkflowStatus)Enum.Parse(typeof(WorkflowStatus), what.Status);
            }
            catch
            {
                throw new Exception(
                    "GetProcessInstanceInformation: Unable to parse status of {0} to workflow status".
                        FormatStringCurrentCulture(what.Status));
            }

            WorkflowProcessInstanceInformation processInstanceInformation = new WorkflowProcessInstanceInformation
                                                  {
                                                      Duration = what.ExpectedDuration,
                                                      EndDate = what.FinishDate,
                                                      Folio = what.Folio,
                                                      Priority = what.Priority,
                                                      ProcessId = what.ProcID,
                                                      ProcessInstanceId = what.ID,
                                                      ProcessName = what.ProcSetFullName,
                                                      StartDate = what.StartDate,
                                                      Status = currentStatus
                                                  };

            return processInstanceInformation;
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Stops the workflow instances.
        /// </summary>
        /// <param name="workflowManagementServer">The workflow management server.</param>
        /// <param name="processType">Type of the process.</param>
        public static void StopWorkflowInstances(this WorkflowManagementServer workflowManagementServer,
                                                 string processType)
        {
            workflowManagementServer.ValidateIsNotDefault("workflowManagementServer");
            processType.ValidateIsNotNullOrEmpty("processType");

            TracingService.Trace(TraceLevel.Verbose,
                                 "StopWorkflowInstances: Getting process instances of type {0}", processType);

            ProcessInstances processInstances =
                workflowManagementServer
                    .GetProcessInstancesAll(processType, string.Empty, string.Empty);

            StopProcessInstances(workflowManagementServer, processInstances);
        }


        




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Stops the process instances that match the specified workflow status.
        /// </summary>
        /// <param name="workflowManagementServer">The workflow management server.</param>
        /// <param name="procId">The proc id.</param>
        public static void StopProcessInstances(this WorkflowManagementServer workflowManagementServer, int procId)
        {
            workflowManagementServer.ValidateIsNotDefault("workflowManagementServer");

            TracingService.Trace(TraceLevel.Verbose, "StopProcessInstances: Stopping process with id {0}", procId);

            ProcessInstances processInstances =
                workflowManagementServer.GetProcessInstances(procId);

            StopProcessInstances(workflowManagementServer, processInstances);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Stops the process instances that match the specified workflow status.
        /// </summary>
        /// <param name="workflowManagementServer">The workflow management server.</param>
        /// <param name="processInstances">The process instances.</param>
        private static bool StopProcessInstances(WorkflowManagementServer workflowManagementServer, ProcessInstances processInstances)
        {
            workflowManagementServer.ValidateIsNotDefault("workflowManagementServer");

            bool totalResult = false;
            foreach (ProcessInstance processInstance in processInstances)
            {
                TracingService.Trace(TraceLevel.Verbose, "StopProcessInstances: Stopping process with id {0}",
                                     processInstance.ID);

                WorkflowStatus processWorkflowStatus = (WorkflowStatus) int.Parse(processInstance.Status);
                bool result;
                switch (processWorkflowStatus)
                {
                    case WorkflowStatus.Active:
                    case WorkflowStatus.Running:
                    case WorkflowStatus.Unknown:
                        try
                        {
                            result = workflowManagementServer.StopProcessInstances(processInstance.ID);
                            totalResult &= result;
                        }
                        catch
                        {

                        }
                        break;
                    case WorkflowStatus.Completed:
                    case WorkflowStatus.Deleted:
                    case WorkflowStatus.Stopped:
                    case WorkflowStatus.Undefined:
                    default:
                        break;
                }
            }
            return totalResult;
        }


    }
}


