﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using SourceCode.Workflow.Client;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Workflows;

    /// <summary>
    /// 
    /// </summary>
    public static class IK2ClientConnectionDisposableContainerExtensions
    {

        /// <summary>
        /// Gets the tracing service.
        /// </summary>
        private static ITracingService TracingService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<ITracingService>(); }
        }

        private static IDateTimeService DateTimeService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IDateTimeService>(); }
        }
        private static IEnvironmentService EnvironmentService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>(); }
        }


        /// <summary>
        /// Starts the workflow instance.
        /// </summary>
        /// <param name="k2WorkflowDisposableClientConnection">The k2 workflow disposable client connection.</param>
        /// <param name="fullProcessPath">The process path.</param>
        /// <returns>Process Id</returns>
        public  static ProcessInstance CreateWorkflowProcessInstance(this IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection, 
            string fullProcessPath)
        {
            k2WorkflowDisposableClientConnection.ValidateIsNotDefault("k2WorkflowDisposableClientConnection");
            fullProcessPath.ValidateIsNotNullOrEmpty("fullProcessPath");


            //As opening connection is expensive, refactor as much to ExtensionMethods
            //that can be chained together during one connection:

            ProcessInstance newProcess =
                k2WorkflowDisposableClientConnection.Connection.CreateWorkflowProcessInstance(fullProcessPath);


            return newProcess;
        }




        /// <summary>
        /// Starts the existing workflow process instance.
        /// </summary>
        /// <param name="k2WorkflowDisposableClientConnection">The k2 workflow disposable client connection.</param>
        /// <param name="processInstance">The process instance.</param>
        /// <param name="workflowTag">The workflow tag.</param>
        /// <param name="dataFields">The data fields.</param>
        /// <returns></returns>
        public static int StartExistingWorkflowProcessInstance(this IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection, int processInstance, string workflowTag, IEnumerable<KeyValuePair<string, object>> dataFields)
        {
            k2WorkflowDisposableClientConnection.ValidateIsNotDefault("k2WorkflowDisposableClientConnection");

            throw new NotImplementedException();

            //ProcessInstance processInstance = null;

            //k2WorkflowDisposableClientConnection.StartExistingWorkflowProcessInstance(processInstance, dataFields);

        }



        /// <summary>
        /// Starts the existing workflow process instance.
        /// </summary>
        /// <param name="k2WorkflowDisposableClientConnection">The k2 workflow disposable client connection.</param>
        /// <param name="processInstance">The process instance.</param>
        /// <param name="workflowTag">The workflow tag.</param>
        /// <param name="dataFields">The data fields.</param>
        /// <returns></returns>
        public static void StartExistingWorkflowProcessInstance(this IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection, ProcessInstance processInstance, string workflowTag, IEnumerable<KeyValuePair<string, object>> dataFields)
        {
            k2WorkflowDisposableClientConnection.ValidateIsNotDefault("k2WorkflowDisposableClientConnection");

            processInstance.SetDataFields(dataFields);

            k2WorkflowDisposableClientConnection.Connection.StartExistingWorkflowProcessInstance(processInstance,workflowTag, dataFields);

        }



        

        /// <summary>
        /// Start a process given a path and any data variables
        /// </summary>
        /// <param name="k2WorkflowDisposableClientConnection">The k2 workflow disposable client connection.</param>
        /// <param name="fullProcessPath">The full path to the Process.</param>
        /// <param name="processTag">The (optional) process path (ie Folio, when dealing with K2).</param>
        /// <param name="dataFields">The data fields.</param>
        /// <returns>
        /// The Process Instance Id.
        /// </returns>
        public static ProcessInstance CreateAndStartWorkflowProcessInstance(this IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection,
            string fullProcessPath,
            string processTag = null,
            IEnumerable<KeyValuePair<string, object>> dataFields = null)
        {


            processTag = (!processTag.IsNullOrEmpty())
                             ? processTag
                             : "{0}:{1}".FormatStringCurrentCulture(fullProcessPath, DateTimeService.NowUTC);



            //As opening connection is expensive, refactor as much to ExtensionMethods
            //that can be chained together during one connection:
            ProcessInstance newProcess =
                k2WorkflowDisposableClientConnection.Connection.CreateAndStartWorkflowProcessInstance(fullProcessPath,
                                                                                                 processTag, dataFields);

            return newProcess;

        }



        /// <summary>
        /// Invokes the workflow event outgoing action.
        /// </summary>
        /// <param name="k2WorkflowDisposableClientConnection">The k2 workflow disposable client connection.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <returns></returns>
        public static SourceCode.Workflow.Client.Action InvokeWorkflowEventOutgoingAction(this IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection, string serialNumber, string actionName)
        {

            return k2WorkflowDisposableClientConnection.Connection.InvokeWorkflowEventOutgoingAction(serialNumber, actionName);
        }

        /// <summary>
        /// Gets the workflow process instance variables.
        /// </summary>
        /// <param name="k2WorkflowDisposableClientConnection">The k2 workflow disposable client connection.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="variableKeys">The variable keys.</param>
        /// <returns></returns>
        public static Dictionary<string, object> GetWorkflowProcessInstanceVariables(this IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection, string serialNumber,
                                                                             IEnumerable<string> variableKeys = null)
        {
            return k2WorkflowDisposableClientConnection.Connection.GetWorkflowProcessInstanceVariables(serialNumber,
                                                                                                       variableKeys);
        }



        /// <summary>
        /// Sets the workflow process instance variables.
        /// </summary>
        /// <param name="k2WorkflowDisposableClientConnection">The k2 workflow disposable client connection.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="initialDataFields">The initial data fields.</param>
        public static void SetWorkflowProcessInstanceVariables(this IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection, 
            string serialNumber, IEnumerable<KeyValuePair<string, object>> initialDataFields = null)
       {
           k2WorkflowDisposableClientConnection.Connection.SetWorkflowProcessInstanceVariables(serialNumber, initialDataFields);
       }
    }
}
