﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using SourceCode.Workflow.Client;
    using XAct.Diagnostics;

    /// <summary>
    /// Extensions to the K2 Connection object,
    /// the means by which a Client website/other 
    /// can connect to a server
    /// </summary>
    public static class ConnectionExtensions
    {

        /// <summary>
        /// Gets the tracing service.
        /// </summary>
        private static ITracingService TracingService
        {
            get
            {
                return DependencyResolver.Current.GetInstance<ITracingService>();
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Creates and starts a new Instance of a Workflow Process.
        /// </summary>
        /// <param name="connection">The *open* K2 connection object.</param>
        /// <param name="fullPprocessPath">The K2 WF process path (eg: "Datacom.Demos.D07.K2WF08\DemoBank2").</param>
        /// <param name="workflowTag">The workflow display label/tag (eg: "A LoanRequest (id: 132)").</param>
        /// <param name="initialDataFields">An optional set of variables to initialize the workflow with before starting it.</param>
        public static ProcessInstance CreateAndStartWorkflowProcessInstance(this Connection connection, string fullPprocessPath, string workflowTag,
                                                         IEnumerable<KeyValuePair<string, object>> initialDataFields = null)
        {
            TracingService.Trace(TraceLevel.Verbose, "CreateAndStartNewProcessInstance: starting process, folio={0}", workflowTag);

            ProcessInstance newProcess = connection.CreateWorkflowProcessInstance(fullPprocessPath);

            connection.StartExistingWorkflowProcessInstance(newProcess, workflowTag, initialDataFields);


            return newProcess;
        }




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Creates and starts a new Instance of a Workflow Process.
        /// </summary>
        /// <param name="connection">The *open* K2 connection object.</param>
        /// <param name="fullPprocessPath">The K2 WF process path (eg: "Datacom.Demos.D07.K2WF08\DemoBank2").</param>
        public static ProcessInstance CreateWorkflowProcessInstance(this Connection connection, string fullPprocessPath)
        {
            TracingService.Trace(TraceLevel.Verbose, "CreateWorkflowProcessInstance: Creating process of {0}. Tag:{1}", fullPprocessPath);

            connection.ValidateIsNotDefault("connection");
            fullPprocessPath.ValidateIsNotNullOrEmpty("fullProcessPath");

            ProcessInstance newProcess = connection.CreateProcessInstance(fullPprocessPath);


            return newProcess;
        }




        /// <summary>
        /// Starts the existing workflow process instance.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="processInstance">The process instance.</param>
        /// <param name="workflowTag">The workflow tag.</param>
        /// <param name="dataFields">The data fields.</param>
        /// <returns></returns>
        public static void StartExistingWorkflowProcessInstance(this Connection connection, ProcessInstance processInstance, string workflowTag = null, IEnumerable<KeyValuePair<string, object>> dataFields = null)
        {
            connection.ValidateIsNotDefault("connection");
            processInstance.ValidateIsNotDefault("processInstance");

            TracingService.Trace(TraceLevel.Verbose, "StartExistingWorkflowProcessInstance: starting process, folio={0}", workflowTag);

            processInstance.Folio = workflowTag;

            //NOTE: As per http://bit.ly/Mrm5C0
            //If you are setting the data fields after calling CreateProcessInstance 
            //then you don't need process admin; you only need 'Start' permissions. 

            processInstance.SetDataFields(dataFields);

            //As per http://bit.ly/NMrnsZ requires Admin rights:
            //processInstance.Update();
            //As per http://bit.ly/Oe0D1x one can create an Action
            //Use instead:
            //workItem.Actions["UpdateWorkItem"].Execute();

            //But in create mode, does not: http://www.k2underground.com/forums/t/9282.aspx

            connection.StartProcessInstance(processInstance);

        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the a Workflow Process Instances current DataField values.
        /// </summary>
        /// <param name="connection">The *open* K2 connection object.</param>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).Format is {ProcInstID}_{ActInstDestID}.</param>
        /// <param name="variableKeys">The variable keys.</param>
        /// <returns></returns>
        public static Dictionary<string, object> GetWorkflowProcessInstanceVariables(this Connection connection,
                                                                             string serialNumber,
                                                                             IEnumerable<string> variableKeys=null)
        {

            WorklistItem workItem = connection.OpenWorklistItem(serialNumber);

            ProcessInstance processInstance = workItem.ProcessInstance;

            return processInstance.GetDataFields(variableKeys);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Sets the process instance variables.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).Format is {ProcInstID}_{ActInstDestID}.</param>
        /// <param name="initialDataFields">The initial data fields.</param>
        public static void SetWorkflowProcessInstanceVariables(this Connection connection, string serialNumber, IEnumerable<KeyValuePair<string, object>> initialDataFields = null)
        {
            initialDataFields.ValidateIsNotDefault("initialDataFields");

            WorklistItem workItem = connection.OpenWorklistItem(serialNumber);

            ProcessInstance processInstance = workItem.ProcessInstance;

            processInstance.SetDataFields(initialDataFields);
            

            //As per http://bit.ly/NMrnsZ requires Admin rights:
            //processInstance.Update();
            //Use instead:
            workItem.Actions["UpdateWorkItem"].Execute();
                
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).Format is {ProcInstID}_{ActInstDestID}.</param>
        /// <param name="key"></param>
        /// <internal>
        /// Src: http://conqueringk2.wordpress.com/category/k2-blackpearl/
        /// </internal>
        public static bool DoesProcessInstanceVariableExist(this Connection connection, string serialNumber, string key)
        {

            WorklistItem workItem = connection.OpenWorklistItem(serialNumber);

            ProcessInstance processInstance = workItem.ProcessInstance;

            return processInstance.DataFieldExist(key);

        }
        

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Open the workflow in the current Connection/specified K2 Server
        /// and invoke a specific outgoing Action from a Client Event.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).Format is {ProcInstID}_{ActInstDestID}.</param>
        /// <param name="actionName">The action.</param>
        public static Action InvokeWorkflowEventOutgoingAction(this Connection connection, string serialNumber, string actionName)
        {

            WorklistItem workItem = connection.OpenWorklistItem(serialNumber);

            Action workItemAction = workItem.Actions[actionName];

            workItemAction.Execute();

            return workItemAction;
        }





        //public static Connection Test(this Connection connection, string serialNumber)
        //{
        //http://jeylabsblog.com/2008/07/14/k2-accessing-environment-fields-from-code/
        //    //SourceCode.EnvironmentSettings.Clien
        //    var workItem = connection.OpenWorklistItem(serialNumber);
        //    var processInstance = workItem.ProcessInstance;


        //    var x = processInstance.XmlFields;
        //}







        /// <summary>
        /// Gets the W ork list item by serial number.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="k2SN">The k2 SN.</param>
        /// <returns></returns>
        public static WorklistItem GetWOrkListItemBySerialNumber(this Connection connection, string k2SN)
        {
            return connection.OpenWorklistItem(k2SN);
        }


        /// <summary>
        /// Gets the server item by serial number.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="K2SN">The k2 SN.</param>
        /// <returns></returns>
        public static ServerItem GetServerItemBySerialNumber(this Connection connection, string K2SN)
        {
            ServerItem serverItem = connection.OpenServerItem(K2SN);
            return serverItem;
        }
    }

  
}
