namespace XAct.Workflows
{
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IK2ServerConnectionConfigurationManager"/>
    /// to keep a copy of the current <see cref="IK2ServerConnectionConfiguration"/>
    /// so that its settings can be centrally accessed.
    /// </summary>
    [DefaultBindingImplementation(typeof(IServerConnectionConfigurationManagerService), BindingLifetimeType.SingletonPerWebRequestScope, Priority.Normal /*OK: An Override Priority*/)]
    [DefaultBindingImplementation(typeof(IK2ServerConnectionConfigurationManager), BindingLifetimeType.SingletonPerWebRequestScope,Priority.Low)]
    public class K2ServerConnectionConfigurationManager : IK2ServerConnectionConfigurationManager
    {
        /// <summary>
        /// Gets or sets the current <see cref="IK2ServerConnectionConfiguration"/>
        /// from which the connection string can be extracted to create a connection
        /// to the server.
        /// </summary>
        /// <value>
        /// The current <see cref="IK2ServerConnectionConfiguration"/>
        /// </value>
        public IK2ServerConnectionConfiguration Current { get; set; }

        IServerConnectionConfiguration IServerConnectionConfigurationManagerService.Current
        {
            get { return Current; }
            set { Current = (IK2ServerConnectionConfiguration) value; }
        }
    }
}