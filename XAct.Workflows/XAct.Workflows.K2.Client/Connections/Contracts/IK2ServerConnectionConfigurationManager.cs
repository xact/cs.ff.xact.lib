﻿
namespace XAct.Workflows
{
     /// <summary>
    /// Contract for a Manager to keep a copy of 
    /// the current <see cref="IK2ServerConnectionConfiguration"/>
    /// so that its settings can be centrally accessed.
    /// </summary>    
    public interface IK2ServerConnectionConfigurationManager : IServerConnectionConfigurationManagerService
    {
        /// <summary>
        /// Gets or sets the current <see cref="IK2ServerConnectionConfiguration"/>
        /// from which the connection string can be extracted to create a connection
        /// to the server.
        /// </summary>
        /// <value>
        /// The current <see cref="IK2ServerConnectionConfiguration"/>
        /// </value>
        new IK2ServerConnectionConfiguration Current { get; set; }
    }
}
