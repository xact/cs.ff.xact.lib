﻿
namespace XAct.Workflows
{
    /// <summary>
    ///  Contract for a service to create and return the Current K2 Client 
    /// Connection used by some operations.
    /// </summary>
    public interface IK2WorkflowClientConnectionManager
    {

        /// <summary>
        /// Creates a new K2 Client Connection to the current server.
        /// </summary>
        /// <returns></returns>
        IK2ClientConnectionDisposableContainer Create();
        /// <summary>
        /// Gets or sets the current K2 Client Connection.
        /// </summary>
        /// <value>
        /// The current.
        /// </value>
        IK2ClientConnectionDisposableContainer Current { get; set; }


        /// <summary>
        /// Gets or sets the k2 process connection connection manager.
        /// </summary>
        /// <value>
        /// The k2 process connection connection manager.
        /// </value>
        IK2ServerConnectionConfigurationManager K2ProcessConnectionConnectionManager { get; }

    }
}
