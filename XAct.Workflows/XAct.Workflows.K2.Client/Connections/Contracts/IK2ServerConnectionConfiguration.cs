﻿namespace XAct.Workflows
{
    /// <summary>
    /// Contract for a configuration package used to hold the settings
    /// required to connect to a K2 server.
    /// </summary>
    public interface IK2ServerConnectionConfiguration : IServerConnectionConfiguration
    {

        /// <summary>
        /// Gets or sets the name of the security label.
        /// </summary>
        /// <value>
        /// The name of the security label.
        /// </value>
        string SecurityLabelName { get; set; }

        /// <summary>
        /// Gets or sets the process base path (the *.kprj filename + any project folders).
        /// </summary>
        string ProcessBasePath { get; set; }


        /// <summary>
        /// Gets the full process path.
        /// </summary>
        /// <param name="processPath">The process path.</param>
        /// <returns></returns>
        string GetFullProcessPath(string processPath);
    }
}
