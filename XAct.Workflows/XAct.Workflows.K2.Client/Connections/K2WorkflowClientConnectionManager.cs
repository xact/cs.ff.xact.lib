namespace XAct.Workflows
{
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IK2WorkflowClientConnectionManager"/>
    /// contract
    /// </summary>
    [DefaultBindingImplementation(typeof(IK2WorkflowClientConnectionManager), BindingLifetimeType.Undefined, Priority.Low)]
    public class K2WorkflowClientConnectionManager : IK2WorkflowClientConnectionManager
    {
        /// <summary>
        /// Gets the k2 process connection connection manager.
        /// </summary>
        public IK2ServerConnectionConfigurationManager K2ProcessConnectionConnectionManager { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="K2WorkflowClientConnectionManager"/> class.
        /// </summary>
        /// <param name="k2ProcessConnectionConnectionManager">The k2 process connection connection manager.</param>
        public K2WorkflowClientConnectionManager(IK2ServerConnectionConfigurationManager k2ProcessConnectionConnectionManager)
        {
            K2ProcessConnectionConnectionManager = k2ProcessConnectionConnectionManager;
        }

        

        /// <summary>
        /// Creates a new K2 Client Connection to the current server.
        /// </summary>
        /// <returns></returns>
        public IK2ClientConnectionDisposableContainer Create()
        {
            return new K2ClientConnectionDisposableContainer(K2ProcessConnectionConnectionManager);
        }




        /// <summary>
        /// Gets or sets the current <see cref="IK2ClientConnectionDisposableContainer"/>
        /// </summary>
        /// <value>
        /// The current.
        /// </value>
        public IK2ClientConnectionDisposableContainer Current {
            get { return _current ?? (_current = Create()); }
            set { _current = value; }
        }

        private IK2ClientConnectionDisposableContainer _current;
    }
}