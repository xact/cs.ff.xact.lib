﻿namespace XAct.Workflows
{
    using SourceCode.Hosting.Client.BaseAPI;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of
    /// <see cref="IK2ServerConnectionConfiguration"/>
    /// to contain the connection string information required to connect to a K2
    /// server.
    /// </summary>
    [DefaultBindingImplementation(typeof(IServerConnectionConfiguration), BindingLifetimeType.TransientScope, Priority.Normal /*OK: An Override Priority*/)]
    [DefaultBindingImplementation(typeof(IK2ServerConnectionConfiguration), BindingLifetimeType.TransientScope,Priority.Low)]
    public class K2ServerConnectionConfiguration : ServerConnectionConfigurationBase, IK2ServerConnectionConfiguration
    {
        //private readonly IAppSettingsService _appSettingsService;

        /// <summary>
        /// Gets or sets the name of the security label.
        /// </summary>
        /// <value>
        /// The name of the security label.
        /// </value>
        public string SecurityLabelName { get; set; }


        /// <summary>
        /// Gets or sets the process base path (the *.kprj filename + any project folders).
        /// </summary>
        public string ProcessBasePath { get; set; }

        /// <summary>
        /// Gets the full process path.
        /// </summary>
        /// <param name="processPath">The process path.</param>
        /// <returns></returns>
        public string GetFullProcessPath (string processPath)
        {
            return "{0}\\{1}".FormatStringInvariantCulture(ProcessBasePath, processPath);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="K2ServerConnectionConfiguration"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public K2ServerConnectionConfiguration(ITracingService tracingService)
            : base(tracingService)
        {
            //_hostSettingsService = hostSettingsService;


                                                          
        }



        /// <summary>
        /// Invoked by <see cref="ServerConnectionConfigurationBase.ConnectionString"/> according to 
        /// <see cref="ServerConnectionConfigurationBase.Initialized"/>
        /// in order to rebuilds the connection string.
        /// </summary>
        /// <returns></returns>
        protected override string RebuildConnectionString()
        {
            SCConnectionStringBuilder builder = new SCConnectionStringBuilder
            {
                Authenticate = true,
                Host = string.IsNullOrEmpty(ServerName)?"[NOT SET]":ServerName,
                Port = (PortNumber==0)?5555:PortNumber,
                Integrated = true,
                IsPrimaryLogin = true,
                SecurityLabelName = string.IsNullOrEmpty(SecurityLabelName)?"K2":SecurityLabelName,
                
            };


            string k2ConnectionString = builder.ConnectionString;

            TracingService.Trace(TraceLevel.Verbose, "GetConnectionStringToK2: Got connection string for K2: {0}",
                                  k2ConnectionString);

            return k2ConnectionString;
        }
    
    }
}
