﻿namespace XAct
{
    using SourceCode.Workflow.Management;
    using XAct.Workflows;

    /// <summary>
    /// Extensions to the K2 <see cref="WorkflowStatus"/>.
    /// </summary>
    public static class WorkflowStatusExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Maps a K2 <see cref="WorkflowStatus"/> to a generic <see cref="Process.ProcessStatus"/>
        /// </summary>
        /// <param name="workflowStatus">The workflow status.</param>
        /// <returns></returns>
        public static Process.ProcessStatus MapTo(this WorkflowStatus workflowStatus)
        {
            int val = (int)workflowStatus;
            return ((Process.ProcessStatus)val + 1);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Maps the <see cref="Process.ProcessStatus"/> to a K2 <see cref="WorkflowStatus"/> value.
        /// </summary>
        /// <param name="workflowStatus">The workflow status.</param>
        /// <returns></returns>
        public static WorkflowStatus MapTo(this Process.ProcessStatus workflowStatus)
        {
            int val = (int)workflowStatus;
            return ((WorkflowStatus)val - 1);
        }

    }
}
