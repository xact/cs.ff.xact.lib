﻿namespace XAct.Workflows
    {
        using System;
        using SourceCode.Hosting.Client.BaseAPI;
        using SourceCode.Workflow.Management;

        /// <summary>
        /// A helper object to wrap an <see cref="WorkflowManagementServer"/>
        /// in order to provide a means to ensure that its <see cref="BaseAPIConnection"/> is correctly 
        /// closd and disposed of when finished.  
        /// </summary>
        public class WorkflowManagementServerDisposableContainer : IDisposable
        {
            /// <summary>
            /// Gets the wrapped <c>WorkflowManagementServer</c>.
            /// </summary>
            public WorkflowManagementServer WorkflowManagementServer { get; private set; }


            /// <summary>
            /// Initializes a new instance of the <see cref="WorkflowManagementServerDisposableContainer"/> class.
            /// </summary>
            /// <param name="serverConnectionConfiguration">The server connection configuration.</param>
            public WorkflowManagementServerDisposableContainer(IK2ServerConnectionConfiguration serverConnectionConfiguration)
                : this(new WorkflowManagementServer(), serverConnectionConfiguration.ConnectionString)
            {
                
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="WorkflowManagementServerDisposableContainer"/> class.
            /// </summary>
            /// <param name="connectionString">The connection string.</param>
            public WorkflowManagementServerDisposableContainer(string connectionString)
                : this(new WorkflowManagementServer(), connectionString)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="WorkflowManagementServerDisposableContainer"/> class.
            /// </summary>
            /// <param name="workflowManagementServer">The workflow management server.</param>
            /// <param name="connectionString">The connection string.</param>
            protected WorkflowManagementServerDisposableContainer(WorkflowManagementServer workflowManagementServer, string connectionString)
            {
                workflowManagementServer.ValidateIsNotDefault("workflowManagementServer");
                connectionString.ValidateIsNotDefault("connectionString");

                WorkflowManagementServer = workflowManagementServer;

                WorkflowManagementServer.CreateConnection();
                WorkflowManagementServer.Connection.Open(connectionString);

            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                //Close WCF basd connection explictely, or you can get unexpected behaviour
                //as it uses up threads...keping them open till they time out.
                WorkflowManagementServer.Connection.Close();
                WorkflowManagementServer.Connection.Dispose();
            }
        }
    }

