namespace XAct.Workflows
{
    using System;
    using SourceCode.Workflow.Client;

    /// <summary>
    /// 
    /// </summary>
    public interface IK2ClientConnectionDisposableContainer : IDisposable
    {
        /// <summary>
        /// Gets the K2 Client Connection.
        /// </summary>
        Connection Connection { get; }
    }
}