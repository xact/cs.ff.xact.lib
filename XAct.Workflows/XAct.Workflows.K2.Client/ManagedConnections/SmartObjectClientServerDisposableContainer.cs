﻿namespace XAct.Workflows
{
    using System;
    using SourceCode.Hosting.Client.BaseAPI;
    using SourceCode.SmartObjects.Client;

    /// <summary>
        /// A helper object to wrap an <see cref="SmartObjectClientServer"/>
        /// in order to provide a means to ensure that its <see cref="BaseAPIConnection"/> is correctly 
        /// closd and disposed of when finished.  
        /// </summary>
        public class SmartObjectClientServerDisposableContainer : IDisposable
        {
            /// <summary>
            /// Gets the wrapped <c>SmartObjectClientServer</c>.
            /// </summary>
            public SmartObjectClientServer SmartObjectClientServer { get; private set; }



                        /// <summary>
            /// Initializes a new instance of the <see cref="SmartObjectClientServerDisposableContainer"/> class.
            /// </summary>
            /// <param name="serverConnectionConfiguration">The server connection configuration.</param>
            public SmartObjectClientServerDisposableContainer(IK2ServerConnectionConfiguration serverConnectionConfiguration)
                : this(new SmartObjectClientServer(), serverConnectionConfiguration.ConnectionString)
            {
                
            }


            /// <summary>
            /// Initializes a new instance of the <see cref="SmartObjectClientServerDisposableContainer"/> class.
            /// </summary>
            public SmartObjectClientServerDisposableContainer(string connectionString)
                : this(new SmartObjectClientServer(), connectionString)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="SmartObjectClientServerDisposableContainer"/> class.
            /// </summary>
            /// <param name="workflowManagementServer">The workflow management server.</param>
            /// <param name="connectionString">The connection string.</param>
            protected SmartObjectClientServerDisposableContainer(SmartObjectClientServer workflowManagementServer, string connectionString)
            {
                workflowManagementServer.ValidateIsNotDefault("workflowManagementServer");
                connectionString.ValidateIsNotDefault("connectionString");

                SmartObjectClientServer = workflowManagementServer;

                SmartObjectClientServer.CreateConnection();
                SmartObjectClientServer.Connection.Open(connectionString);

            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                //Close WCF basd connection explictely, or you can get unexpected behaviour
                //as it uses up threads...keping them open till they time out.
                SmartObjectClientServer.Connection.Close();
                SmartObjectClientServer.Connection.Dispose();
            }
        }
    }

