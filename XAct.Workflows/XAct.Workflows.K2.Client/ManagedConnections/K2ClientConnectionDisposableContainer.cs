﻿namespace XAct.Workflows
{
    using SourceCode.Workflow.Client;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IK2ClientConnectionDisposableContainer), BindingLifetimeType.TransientScope, Priority.Low)]
    public class K2ClientConnectionDisposableContainer: IK2ClientConnectionDisposableContainer 
    {
        /// <summary>
        /// Gets the connection.
        /// </summary>
        public Connection Connection { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="K2ClientConnectionDisposableContainer"/> class.
        /// </summary>
        /// <param name="k2ProcessConnectionConnectionManager">The k2 process connection connection manager.</param>
        public K2ClientConnectionDisposableContainer(IK2ServerConnectionConfigurationManager k2ProcessConnectionConnectionManager)
        {

            //Client connections are notopened with connectionstring, but
            //ConnectionStrings

             Connection = new Connection();

            ConnectionSetup connectionSetup = new ConnectionSetup();
                connectionSetup.ConnectionParameters["Authenticate"] = "true";

connectionSetup.ConnectionParameters["Host"] = k2ProcessConnectionConnectionManager.Current.ServerName;

connectionSetup.ConnectionParameters["Integrated"] = "true";

connectionSetup.ConnectionParameters["IsPrimaryLogin"] = "true";

//connectionSetup.ConnectionParameters["Originator"] = "false";

            //Note that it's not 5555
            connectionSetup.ConnectionParameters["Port"] = 5252.ToString();
//connectionSetup.ConnectionParameters["Port"] = k2ProcessConnectionConnectionManager.Current.PortNumber.ToString();

//connectionSetup.ConnectionParameters["SecurityLabelName"] =k2ProcessConnectionConnectionManager.Current.SecurityLabelName;

//connectionSetup.ConnectionParameters["SecurityPackage"] = "Kerberos,NTLM";

//connectionSetup.ConnectionParameters["UserID"] = "{YourUserName}";

//connectionSetup.ConnectionParameters["WindowsDomain"] = "{YourDomain}";

//connectionSetup.ConnectionParameters["Password"] = "{YourPassword}";

            Connection.Open(connectionSetup); //(k2ProcessConnectionConnectionManager.Current.ConnectionString


        }




        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Connection.Close();
            Connection.Dispose();
            Connection = null;
        }



    }
}
