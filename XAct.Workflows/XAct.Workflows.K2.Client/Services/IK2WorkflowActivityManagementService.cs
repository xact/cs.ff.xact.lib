﻿// ReSharper disable CheckNamespace
namespace XAct.Workflows
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using SourceCode.Workflow.Management;
    using XAct.Messages;

    /// <summary>
    /// Contract for an interface to return information about the
    /// Activities (individual steps) within a Workflow Process Instance.
    /// </summary>
    public interface IK2WorkflowActivityManagementService : IHasXActLibService
    {
        /// <summary>
        /// Moves a workflow identity directly to a specific named Activity.
        /// <para>
        /// Note thate Goto does not guarantee that the workflow's values
        /// can handle a disruption in the original intended flow of the workflow.
        /// </para>
        /// </summary>
        /// <param name="processIdentifier">The process identifier.</param>
        /// <param name="activityIdentifier">The activity identifier/name.</param>
        void GotoActivity(int processIdentifier, string activityIdentifier);

        /// <summary>
        /// TODO: Describe what this does exactly (same as Goto?) ???
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="activityIdentifier">The activity identifier/name.</param>
        void PerformAction(string serialNumber, string activityIdentifier);

        /// <summary>
        /// Redirects a current Task (ie Activity, assigned to a User), to another User.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="userName">Name of the user.</param>
        void RedirectTask(string serialNumber, string userName);

        /// <summary>
        /// Get the list of Client Activies for the User.
        /// TODO: Picked up by? Or *potentially* for?
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        IEnumerable<TaskSummary> GetTasksForUser(string userName, IPagedQuerySpecification pagedQuerySpecification);

        /// <summary>
        /// Gets the K2 ProcessStatus's for the given workflow identifiers.
        /// </summary>
        /// <param name="workflowIndentifiers">The workflow indentifiers.</param>
        /// <returns></returns>
        IEnumerable<KeyValuePair<int, Process.ProcessStatus>> GetWorkflowStatuses(IEnumerable<int> workflowIndentifiers);

        /// <summary>
        /// Gets all activity names for a specific process instance.
        /// </summary>
        /// <param name="processInstanceId">The process instance id.</param>
        /// <returns></returns>
        IEnumerable<string> GetAllActivityNames(int processInstanceId);

    }
}
