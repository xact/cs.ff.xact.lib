﻿namespace XAct.Workflows.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using SourceCode.SmartObjects.Client;
    using SourceCode.Workflow.Client;
    using SourceCode.Workflow.Management;
    using XAct.Diagnostics;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IK2WorkflowActivityManagementService"/>
    /// to return information about the
    /// Activities (individual steps) within a Workflow Process Instance.
    /// </summary>
    public class K2WorkflowActivityManagementService : IK2WorkflowActivityManagementService
    {
        private readonly ITracingService _tracingService;
        private readonly IK2ServerConnectionConfigurationManager _serverConnectionConnectionManager;




        private IK2ServerConnectionConfiguration CurrentServerConnectionConfiguration
        {
            get { return _serverConnectionConnectionManager.Current ; }
        }




        /// <summary>
        /// Initializes a new instance of the <see cref="K2WorkflowActivityManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="serverConnectionConnectionManager">The k2 process connection connection manager.</param>
        public K2WorkflowActivityManagementService(ITracingService tracingService,
            IK2ServerConnectionConfigurationManager serverConnectionConnectionManager)
        {
            _tracingService = tracingService;
            _serverConnectionConnectionManager = serverConnectionConnectionManager;
        }

        /// <summary>
        /// Moves a workflow instance directly to a specifically named Activity.
        /// </summary>
        /// <param name="processIdentifier">The process identifier.</param>
        /// <param name="activityIdentifier">The activity identifier/name.</param>
        public void GotoActivity(int processIdentifier, string activityIdentifier)
        {

            _tracingService.Trace(TraceLevel.Verbose, "GotoActivity: Opening connection to K2");

            

            using (WorkflowManagementServerDisposableContainer workflowManagementServerContainer =
                new WorkflowManagementServerDisposableContainer(CurrentServerConnectionConfiguration)
                )
            {

                _tracingService.Trace(TraceLevel.Verbose, "GotoActivity: Attempting to move process {0} to activity {1}",
                                      processIdentifier, activityIdentifier);

                //Use the K2 WorkflowManagementServer:
                workflowManagementServerContainer.WorkflowManagementServer.GotoActivity(processIdentifier,
                                                                                        activityIdentifier);

            }
        }

        /// <summary>
        /// TODO: Describe what this does exactly (same as Goto?) ???
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="activityIdentifier">The activity identifier/name.</param>
        public void PerformAction(string serialNumber, string activityIdentifier)
        {

            _tracingService.Trace(TraceLevel.Verbose, "PerformAction: Opening connection to K2");

            using (K2ClientConnectionDisposableContainer k2ClientConnectionDisposableContainer 
                = new K2ClientConnectionDisposableContainer(_serverConnectionConnectionManager))
            {

                Connection connection = k2ClientConnectionDisposableContainer.Connection;


                _tracingService.Trace(TraceLevel.Verbose,
                                      "PerformAction: Retrieving work list item with serial number {0}",
                                      serialNumber);

                // ReSharper disable RedundantNameQualifier
                SourceCode.Workflow.Client.WorklistItem worklistItem = connection.OpenWorklistItem(serialNumber);
                // ReSharper restore RedundantNameQualifier

                if (worklistItem == null)
                    throw new Exception(string.Format("Could not get work list item from K2 for serial number {0}",
                                                      serialNumber));

                _tracingService.Trace(TraceLevel.Verbose, "PerformAction: Retrieving action {0} from work list item.",
                                      activityIdentifier);
                SourceCode.Workflow.Client.Action action = worklistItem.Actions[activityIdentifier]; 
                if (action == null)
                    throw new Exception(
                        string.Format("PerformAction: Could not get action '{0}' to execute on serial number {1}",
                                      activityIdentifier, serialNumber));

                _tracingService.Trace(TraceLevel.Verbose, "PerformAction: Executing action {0}.", serialNumber);
                action.Execute();

            }
        }

        /// <summary>
        /// Redirects a current Task (ie Activity, assigned to a User), to another User.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="userName">Name of the user.</param>
        public void RedirectTask(string serialNumber, string userName)
        {
            _tracingService.Trace(TraceLevel.Verbose, "RedirectTask: Opening connection to K2");
            

            using (K2ClientConnectionDisposableContainer k2ClientConnectionDisposableContainer =
                new K2ClientConnectionDisposableContainer(_serverConnectionConnectionManager))
            {
                


            _tracingService.Trace(TraceLevel.Verbose, "RedirectTask: Retrieving work list item with serial number {0}",
                                  serialNumber);


            Connection connection = k2ClientConnectionDisposableContainer.Connection;

            // ReSharper disable RedundantNameQualifier
            SourceCode.Workflow.Client.WorklistItem worklistItem = 
                connection.OpenWorklistItem(serialNumber);
            // ReSharper restore RedundantNameQualifier

            if (worklistItem == null)
            {
                throw new Exception(string.Format("Could not get work list item from K2 for serial number {0}",
                                                  serialNumber));
            }

            _tracingService.Trace(TraceLevel.Verbose, "RedirectTask: Redirecting task to username {0}.", userName);
            worklistItem.Redirect(userName);

            }
        }

        /// <summary>
        /// Get the list of Client Activies for the User.
        /// TODO: Picked up by? Or *potentially* for?
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        public IEnumerable<TaskSummary> GetTasksForUser(string userName,
                                                        IPagedQuerySpecification pagedQuerySpecification)
        {
            userName.ValidateIsNotDefault("userName");
            pagedQuerySpecification.ValidateIsNotDefault("pagedQuerySpecification");

            // todo does service account have to be admin for this to work?
            _tracingService.Trace(TraceLevel.Verbose, "GetTasksForUser: Opening connection to K2");

            using (WorkflowManagementServerDisposableContainer workflowManagementServerContainer =
                new WorkflowManagementServerDisposableContainer(CurrentServerConnectionConfiguration)
                )
            {

                _tracingService.Trace(TraceLevel.Verbose, "GetTasksForUser: Getting worklist items for user {0}",
                                      userName);

                IEnumerable<TaskSummary> taskSummaries = workflowManagementServerContainer.WorkflowManagementServer.GetTaskSummaries(
                    userName, pagedQuerySpecification);

                return taskSummaries;

            }
        }


        /// <summary>
        /// get workflow statuses for a given collection of identifiers
        /// </summary>
        /// <param name="workflowIndentifiers"></param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<int, Process.ProcessStatus>> GetWorkflowStatuses(IEnumerable<int> workflowIndentifiers)
        {
            _tracingService.Trace(TraceLevel.Verbose, "GetWorkflowStatuses: Openning connection to K2");


            
            using (SmartObjectClientServerDisposableContainer smartObjectClientServerDisposableContainer
                = new SmartObjectClientServerDisposableContainer(CurrentServerConnectionConfiguration)
                )
            {
                SmartObjectClientServer smartObjectClientServerContainer =
                    smartObjectClientServerDisposableContainer.SmartObjectClientServer;

                foreach (
                    KeyValuePair<int, WorkflowStatus> set in
                        smartObjectClientServerContainer.WorkflowStatuses(workflowIndentifiers))
                {
                    //Convert from K2 specific values to generic Process values:
                    yield return new KeyValuePair<int, Process.ProcessStatus>(set.Key, set.Value.MapTo());
                }

            }
        }

        /// <summary>
        /// Gets all activity names for a specific process instance.
        /// </summary>
        /// <param name="processInstanceId">The process instance id.</param>
        /// <returns></returns>
        public IEnumerable<string> GetAllActivityNames(int processInstanceId)
        {
            _tracingService.Trace(TraceLevel.Verbose, "GetAllActivityNames: Openning connection to K2");

            int processId;
            // get the process id given the process instance id, this is really stupid
            using (WorkflowManagementServerDisposableContainer workflowManagementServerContainer =
                new WorkflowManagementServerDisposableContainer(CurrentServerConnectionConfiguration)
                )
            {


                WorkflowProcessInstanceInformation info = workflowManagementServerContainer.WorkflowManagementServer.GetProcessInstanceInformation(processInstanceId);

                processId = info.ProcessId;
            }
            // get the activities from the process id
            using (SmartObjectClientServerDisposableContainer smartObjectClientServerDisposableContainer
                = new SmartObjectClientServerDisposableContainer(CurrentServerConnectionConfiguration)
                )
            {

                SmartObjectClientServer smartObjectClientServerContainer =
                    smartObjectClientServerDisposableContainer.SmartObjectClientServer;

                List<string> activityList = smartObjectClientServerContainer.GetActivitiesForProcess(processId);

                return activityList;
            }

        }











//        //Src:http://www.k2underground.com/forums/t/2689.aspx
//        public string DeleteWorkflowByFolio(string folio)
//        {


//// Create the K2MNG object (add reference to k2mng.dll (...\program files\k2.net\bin\k2mng.dll)
//SourceCode.K2Mng.K2Manager k2m = new SourceCode.K2Mng.K2Manager();

//// Login to the K2 Server (user with K2 admin rights)
//k2m.Login([K2 Server Name], 5252, [(optionally) a K2 Connection String]);

//// Return all processes with specified folio parameter
//SourceCode.K2Mng.ProcessInstances processCollection = k2m.GetProcessInstances(folio);

//// Loop through collection, returning process ID's
//foreach(SourceCode.K2Mng.ProcessInstance k2process in processCollection)
//{
//long processID = k2process.ID;

//// Delete relevant process instance, specifying true or false to delete log entries
//bool isDeleted = k2m.DeleteProcessInstances(processID, true);
//}
        //}

    }
}


