﻿namespace XAct.Workflows.Services.Implementations
{
    using System.Collections.Generic;
    using XAct.Diagnostics;

    /// <summary>
    /// An implementation of <see cref="IWorkflowClientVariablesService"/>
    /// for a Client (eg, an MVC app) to get/set variables specific to the K2 workflow instance.
    /// </summary>
    public class K2WorkflowClientVariablesService : IWorkflowClientVariablesService
    {
        private readonly ITracingService _tracingService;


        private readonly IK2WorkflowClientConnectionManager _workflowClientConnectionManager;


        private IK2ServerConnectionConfiguration CurrentServerConnectionConfiguration
        {
            get { return _workflowClientConnectionManager.Current as IK2ServerConnectionConfiguration; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="K2WorkflowClientVariablesService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="workflowClientConnectionManager">The workflow client connection manager.</param>
        public K2WorkflowClientVariablesService(ITracingService tracingService, IK2WorkflowClientConnectionManager workflowClientConnectionManager)
        {
            _tracingService = tracingService;
            _workflowClientConnectionManager = workflowClientConnectionManager;
        }

        /// <summary>
        /// Gets the DataFields of a Workflow Process Instance.
        /// <para>
        /// Retrieves the list of all datafields for the specific process.
        /// <code>
        /// <![CDATA[
        /// var serialNumberOfCurrentEventItem =
        ///   ((SourceCode.KO.ServerEventContext)k2workflowContext).SerialNumber;
        /// 
        /// Dictionary<string,object> values = 
        ///   ServiceLocatorService.Current
        ///     .GetInstance<IWorkflowClientVariablesService>().
        ///       .GetProcessInstanceVariables(
        ///         "258SPIKES", 
        ///         
        ///         "Datacom.Demos.D07.K2WF08\DemoBank2" //Path to WF as is visible on K2 Server.
        ///       );
        ///   //Prove that one has a value returned (eg: a db record id):
        ///   Assert.IsNotNull(values[REQUEST_ID_WF_VAR]);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).Format is {ProcInstID}_{ActInstDestID}.</param>
        /// <param name="variableKeys">The variable keys.</param>
        /// <returns></returns>
        public Dictionary<string, object> GetProcessInstanceVariables(string serialNumber, IEnumerable<string> variableKeys)
        {
            using (IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection =
                        new K2ClientConnectionDisposableContainer(_workflowClientConnectionManager.K2ProcessConnectionConnectionManager))
            {                //Use our ExtensionMethods:
                return k2WorkflowDisposableClientConnection.GetWorkflowProcessInstanceVariables(serialNumber, variableKeys);
            }
        }


        /// <summary>
        /// Sets the process instance variables.
        /// </summary>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).Format is {ProcInstID}_{ActInstDestID}.</param>
        /// <param name="dataFields">The data fields.</param>
        public void SetProcessInstanceVariables(string serialNumber, IEnumerable<KeyValuePair<string, object>> dataFields = null)
        {
            using (IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection =
                        new K2ClientConnectionDisposableContainer(_workflowClientConnectionManager.K2ProcessConnectionConnectionManager))
            {                //Use our ExtensionMethods:
                k2WorkflowDisposableClientConnection.SetWorkflowProcessInstanceVariables(serialNumber, dataFields);
            }
        }
    }
}
