﻿// ReSharper disable CheckNamespace
namespace XAct.Workflows.Services.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Implementation of the
    /// <see cref="IWorkflowClientControlService"/>
    /// contract for a Client app (eg, MVC) to 
    /// initiate and control the flow of a Workflow.
    /// </summary>
    public class K2WorkflowClientControlService : IWorkflowClientControlService
    {

        private readonly ITracingService _tracingService;
        private readonly IK2WorkflowClientConnectionManager _workflowClientConnectionManager;


        private IK2ServerConnectionConfiguration CurrentServerConnectionConfiguration
        {
            get { return _workflowClientConnectionManager.Current as IK2ServerConnectionConfiguration; }
        }



        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="K2WorkflowClientControlService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="workflowClientConnectionManager">The workflow client connection manager.</param>
        public K2WorkflowClientControlService(ITracingService tracingService, IK2WorkflowClientConnectionManager workflowClientConnectionManager)
        {
            _tracingService = tracingService;
            _workflowClientConnectionManager = workflowClientConnectionManager;
        }


        /// <summary>
        /// Pings this instance.
        /// </summary>
        /// <param name="serverConnectionConfiguration"></param>
        public void Ping(IServerConnectionConfiguration serverConnectionConfiguration)
        {
            throw new NotImplementedException();
        }





        /// <summary>
        /// Create, Initialize, and Start a new Instance of a Workflow Process.
        /// <para>
        /// <para>
        /// An example of usage would be from a Drop folder Filewatcher event
        /// that needs to create a new file (in a Db or in SharePoint), and
        /// start a new workflow to process it.
        /// </para>
        /// Code Usage:
        /// <![CDATA[
        /// void FileWatcherEventHandler (object o, EventArgs e){
        /// 
        ///   //Move the file to a destination (eg db)...
        ///   //var loanRequest = ....saved in Db....
        ///   //Create an list of key/values to pass to the workflow right 
        ///   //after it has been started:
        ///   var processVariables = new List<KeyValuePair<string, object>>();
        ///   //namely, the Id of the record set.
        ///   processVariables.Add(new KeyValuePair<string, object>(REQUEST_ID_WF_VAR, loanRequest.Id));
        /// 
        ///   //Start a new workflow, and pass the vars it will need, all in one go:
        ///   ServiceLocatorService.Current
        ///     .GetInstance<IWorkflowClientControlService>().
        ///       .CreateAndStartNewProcessInstance(
        ///         "258SPIKES", 
        ///         "LoanRequest_{0} ({1})".FormatStringCurrentCulture(loanRequest.Id);DateTime.Now.ToString(),
        ///         "Datacom.Demos.D07.K2WF08\DemoBank2", //Path to WF as is visible on K2 Server.
        ///         processVariables);
        ///         
        ///   //At this point, WF is now being processed by K2 WF server, 
        ///   //and it will proceed as programmed (for example, the next place 
        ///   //you might hear of this WF, is as an an email received, 
        ///   //alerting of a Client event to handle at a specific website).
        /// }
        /// ]]>
        /// </para>
        /// </summary>
        /// <param name="processPath">A tag/string identifier to give to the workflow instance being started up.</param>
        /// <param name="workflowTag">The unique name of the workflow definition.</param>
        /// <param name="dataFields">An optional set of variables to initialize the workflow with before starting it.</param>
        public void CreateAndStartNewProcessInstance(string processPath, string workflowTag, IEnumerable<KeyValuePair<string, object>> dataFields)
		{
            _tracingService.Trace(TraceLevel.Verbose,"Starting Workflow '{0}' ('{1}')".FormatStringCurrentUICulture(workflowTag,processPath));

            using (IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection =
                new K2ClientConnectionDisposableContainer(_workflowClientConnectionManager.K2ProcessConnectionConnectionManager))
            {
                //Use our ExtensionMethods:
                k2WorkflowDisposableClientConnection.CreateAndStartWorkflowProcessInstance(processPath, workflowTag, dataFields);
            }
		}

        /// <summary>
        /// Invokes the specific Action reponse of a Client Workflow Event.
        /// <para>
        /// For example, response with "Accept" or "Cancel" (the names of the outgoing lines from a Client Event).
        /// </para>
        /// </summary>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).Format is {ProcInstID}_{ActInstDestID}.</param>
        /// <param name="action">The action.</param>
        public void InvokeProcessInstanceEventOutgoingAction(string serialNumber, string action)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Responding to Workflow Client Event '{0}' with Outcome '{1}'".FormatStringCurrentUICulture(serialNumber, action));


            using (IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection =
                new K2ClientConnectionDisposableContainer(_workflowClientConnectionManager.K2ProcessConnectionConnectionManager))
            {
                //Use our ExtensionMethods:
                k2WorkflowDisposableClientConnection.InvokeWorkflowEventOutgoingAction(serialNumber, action);
            }

        }


        //public void Stop ()
        //{
        //    using (Connection connection = new Connection())
        //    {
                
        //       // workflowManagementServerContainer.WorkflowManagementServer.StopProcessInstances(processIdentifier);
        //    }
        //}



        /*
         * 
         * 
using System;
using System.Collections.Generic;
using System.Configuration;
using Nab.Wdms.Service.Data;
using Nab.Wdms.Service.Logging;
using SourceCode.Workflow.Client;
using SourceCode.Hosting.Client.BaseAPI;
using SourceCode.Workflow.Management;

//using ProcessInstance = SourceCode.Workflow.Client.ProcessInstance;

namespace Nab.Wdms.Service.WorkflowManagement.K2
{
    public class K2Services : IWorkflowManagementServices
    {
        private readonly ILogger _logger;

        public K2Services(ILogger logger)
        {
            _logger = logger;
        }

        public WorkflowInstance GetProcessInstance(string identifier)
        {
            return new WorkflowInstance() { Identifier = "HI THERE"};
        }

        public bool StartProcess(ProcessType processType, List<KeyValuePair<string, object>> dataFields)
        {
            string processPathBase = ConfigurationManager.AppSettings["processPathBase"];
            string fullProcessPath = processPathBase + "\\" + processType.ToString();
            string folio = "WDMS " + DateTime.Now;

            _logger.Log(string.Format("StartProcess: Opening connection to K2"));
            var connection = new Connection();
            connection.Open(ConfigurationManager.AppSettings["k2HostServerName"]);
            
            _logger.Log(string.Format("StartProcess: Creating process of {0}",fullProcessPath));
            SourceCode.Workflow.Client.ProcessInstance newProcess = connection.CreateProcessInstance(fullProcessPath);
            newProcess.Folio = folio;
            foreach (var keyValuePair in dataFields)
            {
                _logger.Log(string.Format("StartProcess: setting datafield {0} to {1}",keyValuePair.Key, keyValuePair.Value));
                newProcess.DataFields[keyValuePair.Key].Value = keyValuePair.Value;
            }
            
            _logger.Log(string.Format("StartProcess: starting process, folio={0}",folio));
            connection.StartProcessInstance(newProcess);

            connection.Close();

            return true;
        }

        private string GetConnectionStringToK2()
        {
            string k2HostServerName = ConfigurationManager.AppSettings["k2HostServerName"] ?? "localhost";
            string k2ManagementPort = ConfigurationManager.AppSettings["k2ManagementPort"] ?? "5555";
            
            SCConnectionStringBuilder builder = new SCConnectionStringBuilder();

            builder.Authenticate = true;
            builder.Host = k2HostServerName;
            builder.Port = uint.Parse(k2ManagementPort);
            builder.Integrated = true;
            builder.IsPrimaryLogin = true;
            builder.SecurityLabelName = "K2";

            var k2ConnectionString = builder.ConnectionString;

            _logger.Log(string.Format("GetConnectionStringToK2: Got connection string for K2: {0}", k2ConnectionString));
            
            return k2ConnectionString;
        }

        public bool MoveToActivity(int processIdentifier, string activityName)
        {
            WorkflowManagementServer wfmServer = new WorkflowManagementServer();

            _logger.Log(string.Format("MoveToActivity: Opening connection to K2"));
            wfmServer.CreateConnection();
            wfmServer.Connection.Open(GetConnectionStringToK2());

            _logger.Log(string.Format("MoveToActivity: Attempting to move process {0} to activity {1}",processIdentifier,activityName));
            bool result = wfmServer.GotoActivity(processIdentifier, activityName);

            wfmServer.Connection.Close();

            return result;
        }

        public bool PerformAction(string serialNumber, string actionName)
        {
            _logger.Log(string.Format("PerformAction: Opening connection to K2"));
            Connection connection = new Connection();
            connection.Open(ConfigurationManager.AppSettings["k2HostServerName"]);

            _logger.Log(string.Format("PerformAction: Retrieving work list item with serial number {0}",serialNumber));
            SourceCode.Workflow.Client.WorklistItem worklistItem = connection.OpenWorklistItem(serialNumber);
            if (worklistItem==null) throw new Exception(string.Format("Could not get work list item from K2 for serial number {0}",serialNumber));

            _logger.Log(string.Format("PerformAction: Retrieving action {0} from work list item.",actionName));
            var action = worklistItem.Actions[actionName];
            if (action == null) throw new Exception(string.Format("PerformAction: Could not get action '{0}' to execute on serial number {1}",actionName,serialNumber));
            
            _logger.Log(string.Format("PerformAction: Executing action {0}.",serialNumber));
            action.Execute();

            connection.Close();

            return true;
        }
    }
}

         */



    }
}
