﻿namespace XAct.Workflows.Services.Implementations
{
    using System.Collections.Generic;
    using System.Linq;
    using SourceCode.Workflow.Management;
    using XAct.Diagnostics;
    using XAct.Environment;

    /// <summary>
    /// Implementation of 
    /// <see cref="IK2WorkflowClientWorkflowManagementService"/>
    /// </summary>
    public class K2WorkflowWorkflowManagementService : IK2WorkflowClientWorkflowManagementService

    {
        private readonly ITracingService _tracingService;
        private readonly IK2WorkflowClientConnectionManager _workflowClientConnectionManager;
        private readonly IK2ServerConnectionConfigurationManager _workflowConnectionManager;




        private IK2ServerConnectionConfiguration CurrentServerConnectionConfiguration
        {
            get { return _workflowConnectionManager.Current as IK2ServerConnectionConfiguration; }
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="K2WorkflowWorkflowManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="workflowClientConnectionManager">The workflow client connection manager.</param>
        /// <param name="workflowConnectionManager">The workflow connection manager.</param>
        public K2WorkflowWorkflowManagementService(
            ITracingService tracingService, 
            IEnvironmentService environmentService,
            IK2WorkflowClientConnectionManager workflowClientConnectionManager, 
            IK2ServerConnectionConfigurationManager workflowConnectionManager)
        {
            _workflowClientConnectionManager = workflowClientConnectionManager;
            _workflowConnectionManager = workflowConnectionManager;
            _tracingService = tracingService;
        }



        //public int CreateWorkflowInstance()
        //{
        //    processPath.ValidateIsNotNullOrEmpty("processPath");



        //    string processPathBase = CurrentWorkflowManagementServiceConfiguration.ProcessBasePath;
        //    string fullProcessPath = "{0}\\{1}".FormatStringInvariantCulture(processPathBase, processPath);

        //    string folio = (!processTag.IsNullOrEmpty())
        //                       ? processTag
        //                       : "WDMS {0}".FormatStringCurrentCulture(_environmentService.Now);

        //    _tracingService.Trace(TraceLevel.Verbose, "StartProcess: Opening connection to K2");

        //    ProcessInstance newProcess;

        //    using (IK2WorkflowDisposableClientConnection k2WorkflowDisposableClientConnection =
        //        new K2WorkflowDisposableClientConnection(_workflowConnectionManager))
        //    {

        //        //As opening connection is expensive, refactor as much to ExtensionMethods
        //        //that can be chained together during one connection:
        //        newProcess =
        //            k2WorkflowDisposableClientConnection.Connection.CreateProcessInstance(fullProcessPath, dataFields);

        //        k2WorkflowDisposableClientConnection.Connection.
        //        return newProcess.ID;
        //    }
        //}
        //}

        /// <summary>
        /// Creates a new Workflow instance from the given Process Path.
        /// <para>
        /// Returns the Process Id.
        /// </para>
        /// 	<para>
        /// IMPORTANT: In K2 If you do not use the returned process immediately, to start the process,
        /// you will need Admin rights to set DataFields.
        /// </para>
        /// </summary>
        /// <param name="processPath">The process path.</param>
        /// <returns></returns>
        public int CreateWorkflowInstance(string processPath)
        {

            //Create and then dispose of conection:
            using (IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection =
                new K2ClientConnectionDisposableContainer(_workflowConnectionManager))
            {
                string fullProcessPath = this.CurrentServerConnectionConfiguration.GetFullProcessPath(processPath);
                
                //Note: the connection is open at this stage.

                //Use Extension Method:
                SourceCode.Workflow.Client.ProcessInstance processInstance =
                    k2WorkflowDisposableClientConnection.CreateWorkflowProcessInstance(fullProcessPath);

                return processInstance.ID;
            }
        }



        /// <summary>
        /// Creates the Workflow instance from the given Process Path, and starts it, after setting it's tag/folio name and datafields.
        /// </summary>
        /// <param name="processPath">The process path.</param>
        /// <param name="processTag">The process tag.</param>
        /// <param name="dataFields">The data fields.</param>
        /// <returns></returns>
        public int CreateAndStartWorkflowInstance(string processPath, string processTag =null,  IEnumerable<KeyValuePair<string, object>> dataFields = null)
        {

            //Create and then dispose of conection:
            using (IK2ClientConnectionDisposableContainer k2WorkflowDisposableClientConnection =
                new K2ClientConnectionDisposableContainer(_workflowClientConnectionManager.K2ProcessConnectionConnectionManager))
            {
                //Note: the connection is open at this stage.
                string fullProcessPath = this.CurrentServerConnectionConfiguration.GetFullProcessPath(processPath);

                //Use Extension Method:
                SourceCode.Workflow.Client.ProcessInstance processInstance =
                    k2WorkflowDisposableClientConnection.CreateAndStartWorkflowProcessInstance(fullProcessPath,processTag,dataFields);

                return processInstance.ID;
            }
        }




        /// <summary>
        /// for a certain process type, locates and stops all running instances
        /// </summary>
        /// <param name="processPath">The process path in the current namespace</param>
        public void StopWorkflowInstances(string processPath)
        {
            _tracingService.Trace(TraceLevel.Verbose, "StopAllProcessesOfType: Opening connection to K2");

            using (WorkflowManagementServerDisposableContainer workflowManagementServerContainer =
                new WorkflowManagementServerDisposableContainer(this.CurrentServerConnectionConfiguration))
            {

                string fullProcessPath = this.CurrentServerConnectionConfiguration.GetFullProcessPath(processPath);

                workflowManagementServerContainer.StopWorkflowInstances(fullProcessPath);
            }
        }

        /// <summary>
        /// Stop the specified workflow instance.
        /// </summary>
        /// <param name="processInstanceId"></param>
        public void StopWorkflow(int processInstanceId)
        {

            _tracingService.Trace(TraceLevel.Verbose, "StopProcess: Opening connection to K2");

            using (WorkflowManagementServerDisposableContainer workflowManagementServerContainer =
                new WorkflowManagementServerDisposableContainer(this.CurrentServerConnectionConfiguration))
            {
                _tracingService.Trace(TraceLevel.Verbose, "StopProcess: Stopping process Instance Id:{0}", processInstanceId);

                //As opening connection is expensive, refactor as much to ExtensionMethods
                //that can be chained together during one connection:
                workflowManagementServerContainer.StopWorkflowInstance(processInstanceId);
            }
        }



        /// <summary>
        /// Get the <see cref="Process.ProcessStatus"/> (a generic Process status enum)
        /// of a given workflow instance/process.
        /// </summary>
        /// <param name="processIdentifier">The workflow instance/process identifier.</param>
        /// <returns>
        /// A <see cref="Process.ProcessStatus"/> (a generic Process status enum).
        /// </returns>
        public Process.ProcessStatus GetWorkflowStatus(int processIdentifier)
        {

            using (SmartObjectClientServerDisposableContainer smartObjectClientServerContainer =
                new SmartObjectClientServerDisposableContainer(CurrentServerConnectionConfiguration.ConnectionString))
            {
                return smartObjectClientServerContainer.SmartObjectClientServer.GetWorkflowStatus(processIdentifier).MapTo();
            }
        }

        /// <summary>
        /// gets all the actions that have occurred and destination users and whatever
        /// use this to get the last activity the wf is on
        /// </summary>
        /// <param name="processIdentifier"></param>
        /// <returns></returns>
        public List<AuditActivity> GetWorkflowInstanceActivityAudit(int processIdentifier)
        {
            using (SmartObjectClientServerDisposableContainer smartObjectClientServerContainer =
                new SmartObjectClientServerDisposableContainer(CurrentServerConnectionConfiguration.ConnectionString))
            {
                return smartObjectClientServerContainer.SmartObjectClientServer.GetActivityAuditForProcess(processIdentifier);
            }
        }
        
        /// <summary>
        /// gets actions preformed on process instance and user who completed them
        /// </summary>
        /// <param name="processIdentifier"></param>
        /// <returns></returns>
        public List<AuditAction> GetWorkflowInstanceActionAudit(int processIdentifier)
        {
            using (SmartObjectClientServerDisposableContainer smartObjectClientServerContainer =
                new SmartObjectClientServerDisposableContainer(CurrentServerConnectionConfiguration.ConnectionString))
            {
                return smartObjectClientServerContainer.SmartObjectClientServer.GetActionAuditForProcess(processIdentifier);
            }
        }

        /// <summary>
        /// gets the serial number for the latest activity on a process instance
        /// </summary>
        /// <param name="processIdentifier"></param>
        /// <returns></returns>
        public string GetSerialNumberForWorkflowInstance(int processIdentifier)
        {
            // using the process instance id get the current activity instance id 
            // using activity instance id get the act inst destination id
            using (SmartObjectClientServerDisposableContainer smartObjectClientServerContainer =
                new SmartObjectClientServerDisposableContainer(CurrentServerConnectionConfiguration.ConnectionString))
            {
                List<AuditActivity> activities =
                    smartObjectClientServerContainer.SmartObjectClientServer.GetActivityAuditForProcess(
                        processIdentifier);
                AuditActivity currentActivity = activities.LastOrDefault();
                if (currentActivity == null) return null;
                return processIdentifier + "_" + currentActivity.ActivityInstanceDestinationId;

            }

        }
    }
}


