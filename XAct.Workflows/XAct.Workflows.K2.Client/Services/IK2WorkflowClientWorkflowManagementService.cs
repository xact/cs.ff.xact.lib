namespace XAct.Workflows
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a service to manage K2 workflows remotely 
    /// from a client app (ie without access to Context).
    /// </summary>
    public interface IK2WorkflowClientWorkflowManagementService : IHasXActLibService
    {


        /// <summary>
        /// Creates a new Workflow instance from the given Process Path.
        /// <para>
        /// Returns the Process Id.
        /// </para>
        /// <para>
        /// IMPORTANT: In K2 If you do not use the returned process immediately, to start the process, 
        /// you will need Admin rights to set DataFields.
        /// </para>
        /// </summary>
        /// <param name="processPath">The process path.</param>
        /// <returns></returns>
        int CreateWorkflowInstance(string processPath);



        /// <summary>
        /// Creates the Workflow instance from the given Process Path, and starts it, after setting it's tag/folio name and datafields.
        /// </summary>
        /// <param name="processPath">The process path.</param>
        /// <param name="processTag">The process tag.</param>
        /// <param name="dataFields">The data fields.</param>
        /// <returns></returns>
        int CreateAndStartWorkflowInstance(string processPath, string processTag = null,
                                           IEnumerable<KeyValuePair<string, object>> dataFields = null);



    }
}