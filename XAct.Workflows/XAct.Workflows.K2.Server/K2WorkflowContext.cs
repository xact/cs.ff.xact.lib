﻿//using XAct.Worflows;

//// ReSharper disable CheckNamespace
//namespace XAct.Workflows
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// An implementation of <see cref="IK2WorkflowContext"/>,
//    /// which is a K2 specific implementation of the more generic 
//    /// <see cref="IContext"/>.
//    /// <para>
//    /// It wraps, and returns a K2 <c>ServerEventContext</c>
//    /// without causing a dependency on a K2 assembly (less tight coupling,
//    /// always a good thing).
//    /// </para>
//    /// </summary>
//    public class K2WorkflowContext : WorkflowContext, IK2WorkflowContext
//    {
//        /// <summary>
//        /// Initializes a new instance of the 
//        /// <see cref="K2WorkflowContext"/> class.
//        /// </summary>
//        /// <param name="serverEventContext">The server event context.</param>
//        public K2WorkflowContext(object serverEventContext)
//            :base(typeof(SourceCode.KO.ServerEventContext),serverEventContext)
//        {
            
//        }
        
//    }
//}
