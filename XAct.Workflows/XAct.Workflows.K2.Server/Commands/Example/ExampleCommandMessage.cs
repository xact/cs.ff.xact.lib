﻿
//// ReSharper disable CheckNamespace
//using XAct.Commands;

//namespace XAct.Workflows.Examples
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// Command Message, to be
//    /// handled by an associated CommandMessageHandler
//    /// (see <see cref="ExampleCommandMessageHandler"/>)
//    /// </summary>
//    /// <internal>
//    /// Note: No Interface needed.
//    /// </internal>
//    public class ExampleCommandMessage : ICommandMessage
//    {
//        /// <summary>
//        /// An example variable
//        /// </summary>
//        public int SomeVar { get; set; }
//        /// <summary>
//        /// An example variable
//        /// </summary>
//        public string SomeOtherVar { get; set; }
//    }
//}
