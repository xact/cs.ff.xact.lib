﻿//Example of creating an application specific 
//subclass of WorkflowCommandMessageHandlerBase

// ReSharper disable CheckNamespace
namespace MyApp.Comands
// ReSharper restore CheckNamespace
{
    using XAct;
    using XAct.Workflows;


    /// <summary>A class to provide typed access to variables. </summary>
    /// <remarks>
    /// Much less risk than deploying, and bug hunting strings. Let the compiler do checking
    /// before deployment...
    /// </remarks>
    public class MyWorkflowContextHostSettings : IMyWorkflowContextHostSettings
    {
        /// <summary>Gets the settings service backing the Properties</summary>
        protected IWorkflowContextHostSettingsService SettingsService { get; private set; }

        /// <summary>Initializes a new instance of the <see cref="MyWorkflowContextHostSettings"/> class.</summary>
        /// <param name="settingsService">The settings service.</param>
        public MyWorkflowContextHostSettings(IWorkflowContextHostSettingsService settingsService)
        {
            settingsService.ValidateIsNotDefault("variablesService");

            this.SettingsService = settingsService;
        }

        /// <summary>A typed setting</summary>
        public string SmtpServer
        {
            get
            {
                var result = this.SettingsService.Get<string>(Constants.Fields.SmtpServer);
                return result;
            }
        }

    }


    /// <summary>A class to provide typed access to variables. </summary>
    /// <remarks>
    /// Much less risk than deploying, and bug hunting strings. Let the compiler do checking
    /// before deployment...
    /// </remarks>
    public class ProcessEventVariables : IMyWorkflowContextVariables
    {
        /// <summary>Gets the variable service backing the Properties</summary>
        protected IWorkflowContextVariablesService VariableService { get; private set; }

        /// <summary>Initializes a new instance of the <see cref="ProcessEventVariables"/> class.</summary>
        /// <param name="variablesService">The variables service.</param>
        public ProcessEventVariables(IWorkflowContextVariablesService variablesService)
        {
            variablesService.ValidateIsNotDefault("variablesService");
            this.VariableService = variablesService;
        }

        /// <summary>A typed variable</summary>
        public int RequestId
        {
            get
            {
                var result = this.VariableService.Get<int>(Constants.Fields.RequestId);
                return result;
            }
            set { this.VariableService.Set(Constants.Fields.RequestId, value); }
        }
    }
    /// <summary>Contract for typed settings.</summary>
    public interface IMyWorkflowContextHostSettings
    {
        /// <summary></summary>
        string SmtpServer { get; }
    }
    /// <summary>Contract for typed variables.</summary>
    public interface IMyWorkflowContextVariables
    {
        /// <summary></summary>
        int RequestId { get; }
    }
    /// <summary>Constants for names of variables on Workflow server...
    /// less errors can happen with Constants.</summary>
    public static class Constants
    {
        /// <summary></summary>
        public static class Fields
        {
            /// <summary></summary>
            public static string RequestId = "Id";
            /// <summary></summary>
            public static string SmtpServer = "Smtp";
        }
    }

}
