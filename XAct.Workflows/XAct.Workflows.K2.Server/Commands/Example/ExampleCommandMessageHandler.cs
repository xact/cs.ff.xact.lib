﻿//using System;
//using XAct.Commands;
//using XAct.Services;


//// ReSharper disable CheckNamespace
//namespace XAct.Workflows.Examples
//// ReSharper restore CheckNamespace
//{


//    /// <summary>
//    /// An implementation of 
//    /// <see cref="ICommandMessageHandler{TCommandMessage,TCommandResponse}"/>
//    /// to process <see cref="ExampleCommandMessage"/>
//    /// when passed to <see cref="CommandMessageDispatcher"/> to be executed.
//    /// </summary>
//    /// <internal>
//    /// Note: No Interface needed as only one using this class is Dispatcher.
//    /// MUST: But it must be of type ICommandMessageHandler for dispatcher to find.
//    /// </internal>
//    [DefaultBindingImplementation(typeof(ICommandMessageHandler<ExampleCommandMessage, ICommandResponse>))]
//    public class ExampleCommandMessageHandler : 
//        WorkflowCommandMessageHandlerBase<ExampleCommandMessage, ICommandResponse>
//    {

//        /// <summary>
//        /// Initializes a new instance of the <see cref="ExampleCommandMessageHandler"/> class.
//        /// </summary>
//        /// <param name="workflowCommandServices">package of workflow services that this command handler may use.</param>
//        public ExampleCommandMessageHandler(IWorkflowCommandServices workflowCommandServices)
//            : base(workflowCommandServices)
//        {
//            //Use base class    
//        }
        

//        /// <summary>
//        /// Validate the 
//        /// <see cref="ICommandMessage"/>
//        /// properties and required services, 
//        /// before Execution.
//        /// </summary>
//        /// <returns></returns>
//        public override ICommandResponse ValidateForExecution(ExampleCommandMessage bootstrap)
//        {
//            //Create a response, based on some logic (not implemented in this example):
//            return new CommandResponse {Success = true };
//        }


//        /// <summary>
//        /// Executes the specified command message.
//        /// </summary>
//        /// <param name="commandMessage">The command message.</param>
//        /// <returns></returns>
//        public override ICommandResponse Execute(ExampleCommandMessage commandMessage)
//        {
//            throw new NotImplementedException("Unexecute not implemented.");
//        }

//    }
//}