﻿namespace XAct.Workflows
{
    using System;
    using XAct.Commands;
    using XAct.Diagnostics;

    /// <summary>
    /// Abstract base class for custom Workflow CommandMessageHandlers
    /// to handle custom implementations of 
    /// <see cref="ICommandMessage"/>
    /// </summary>
    public abstract class WorkflowCommandMessageHandlerBase<TCommandMessage> : 
        CommandMessageHandlerBase<TCommandMessage>
        where TCommandMessage : ICommandMessage
    {
        /// <summary>
        /// Gets the workflow services object used to 
        /// initialize the services of this WokflowCommandMessageHandler.
        /// </summary>
        protected IWorkflowCommandServices WorkflowCommandServices { get; private set; }

        /// <summary>
        /// Gets the default library tracing service.
        /// </summary>
        protected ITracingService TracingService { get { return WorkflowCommandServices.TracingService; }}

        /// <summary>
        /// Gets the workflow context service (same context that is used by the other workflow specific services).
        /// </summary>
        protected IWorkflowContextService WorkflowContextService { get { return WorkflowCommandServices.WorkflowContextService; }}

        /// <summary>
        /// Gets the workflow process host settings service (ie equivalent to AppSettings).
        /// </summary>
        protected IWorkflowContextHostSettingsService WorkflowContextHostSettingsService { get { return WorkflowCommandServices.WorkflowContextHostSettingsService; }}

        /// <summary>
        /// Gets the workflow process instance variables service in order to get/set 
        /// variables managed by the workflow server for this singular workflow instance.
        /// </summary>
        protected IWorkflowContextVariablesService WorkflowContextVariablesService { get { return WorkflowCommandServices.WorkflowContextVariablesService; }}


        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="WorkflowCommandMessageHandlerBase{TCommandMessage}"/> class.
        /// </summary>
        protected WorkflowCommandMessageHandlerBase(
            ITracingService tracingService,
            IWorkflowCommandServices workflowCommandServices,
            Action<TCommandMessage> execute
            ):base(execute)
        {

            workflowCommandServices.ValidateIsNotDefault("workflowCommandServices");

            WorkflowCommandServices = workflowCommandServices;


        }


        //public abstract override TCommandResponse Execute(TCommandMessage commandMessage);
    }
}
