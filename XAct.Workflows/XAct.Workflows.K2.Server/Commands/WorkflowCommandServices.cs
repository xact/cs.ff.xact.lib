﻿namespace XAct.Workflows
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An argument package of services.
    /// <para>
    /// Used by <see cref="WorkflowCommandMessageHandlerBase{TCommandMessage}"/>
    /// to diminish the amount of wiring (ie Constructor arguments)
    /// sub classed commands need to specify
    /// in their constructors.
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowCommandServices), BindingLifetimeType.SingletonPerWebRequestScope, Priority.Low)]
    public class WorkflowCommandServices : IWorkflowCommandServices
    {
        /// <summary>
        /// Gets the default library tracing service.
        /// </summary>
        public ITracingService TracingService { get; private set; }

        /// <summary>
        /// Gets the workflow Context service.
        /// </summary>
        public IWorkflowContextService WorkflowContextService { get; private set; }



        /// <summary>
        /// Service to retrieve from the Workflow server immutable host settings.
        /// (Think: appSettings).
        /// </summary>
        public IWorkflowContextHostSettingsService WorkflowContextHostSettingsService { get; private set; }

        /// <summary>
        /// Service to get/set settings specific to this workflow instance.
        /// </summary>
        public IWorkflowContextVariablesService WorkflowContextVariablesService { get; private set; }

        /// <summary>
        /// Service to trace messages into the Workflow server's tracing system.
        /// </summary>
        public IWorkflowContextTracingService WorkflowContextTraceService { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowCommandServices"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="workflowContextService">The workflow context service.</param>
        /// <param name="workflowProcessHostSettingsService">The workflow process host settings service.</param>
        /// <param name="workflowProcessInstanceVariablesService">The workflow process instance variables service.</param>
        /// <param name="workflowProcessTraceService">The workflow process trace service.</param>
        public WorkflowCommandServices(ITracingService tracingService,
            IWorkflowContextService workflowContextService,
            IWorkflowContextHostSettingsService workflowProcessHostSettingsService,
            IWorkflowContextVariablesService workflowProcessInstanceVariablesService,
            IWorkflowContextTracingService workflowProcessTraceService
            )
        {
            tracingService.ValidateIsNotDefault("tracingService");
            workflowContextService.ValidateIsNotDefault("workflowContextService");
            workflowProcessHostSettingsService.ValidateIsNotDefault("workflowProcessHostSettingsService");
            workflowProcessInstanceVariablesService.ValidateIsNotDefault("workflowProcessInstanceVariablesService");
            workflowProcessTraceService.ValidateIsNotDefault("workflowProcessTraceService ");
 
            TracingService = tracingService;
            WorkflowContextService = workflowContextService;
            WorkflowContextHostSettingsService = workflowProcessHostSettingsService;
            WorkflowContextVariablesService = workflowProcessInstanceVariablesService;
            WorkflowContextTraceService = workflowProcessTraceService;
        }
    }
}
