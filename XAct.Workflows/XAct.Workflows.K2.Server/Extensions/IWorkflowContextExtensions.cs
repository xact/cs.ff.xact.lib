﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using SourceCode.KO;
    using XAct.Workflows;
    using XAct.Workflows.Implementations;

    /// <summary>
    /// K2 specific extensions to the generic 
    /// <see cref="IWorkflowContext"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IWorkflowContextExtensions
// ReSharper restore InconsistentNaming
    {


        //serverContext.GotoActivity();
        //serverContext.ProcessInstance;
        //serverContext.SerialNumber;
        //string stringTableValue = serverContext.StringTable[key];
        //serverContext.VerifyCredentials;
        //serverContext.ActivityInstanceDestination;
        //Event e = serverContext.Event;
        //EventInstance eventInstance = serverContext.EventInstance;
        ////serverContext.ExpireActivity(name);



        /// <summary>
        /// Extracts the k2 specific <see cref="ServerEventContext"/>
        /// from the given vendor-agnostic 
        /// <see cref="IWorkflowContext"/>.
        /// <para>
        /// Used by <see cref="K2WorkflowContextServiceBase"/>
        /// to provide a K2 specific context to its subclasses
        /// (see <see cref="K2WorkflowContextHostSettingsService"/>,
        /// <see cref="K2WorkflowContextVariablesService"/>,
        /// etc.)
        /// </para>
        /// </summary>
        /// <param name="workflowContext">The workflow context.</param>
        /// <returns></returns>
        public static ServerEventContext ServerEventContext(this IWorkflowContext workflowContext)
        {
          workflowContext.ValidateIsNotDefault("workflowContext");

          //from there, we can get the typed K2 specific object:
          ServerEventContext serverEventContext = workflowContext.GetContext<ServerEventContext>();

          return serverEventContext;

        }

        /// <summary>
        /// Get the current processes' SN.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
// ReSharper disable InconsistentNaming
        public static string SN(this IWorkflowContext context)
// ReSharper restore InconsistentNaming
        {
            ServerEventContext serverEventContext = context.GetContext<ServerEventContext>();

            return serverEventContext.SerialNumber;
        }

        /// <summary>
        /// Gets the name of the curre the name.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string EventName(this IWorkflowContext context)
        {
            ServerEventContext serverEventContext = context.GetContext<ServerEventContext>();
            return serverEventContext.Event.Name;
        }
        /// <summary>
        /// Gets the name of the curre the name.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string EventDescription(this IWorkflowContext context)
        {
            ServerEventContext serverEventContext = context.GetContext<ServerEventContext>();
            return serverEventContext.Event.Description;
        }

        /// <summary>
        /// Gets the Current Process' Folio Name.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string ProcessFolioName(this IWorkflowContext context)
        {
            ServerEventContext serverEventContext = context.GetContext<ServerEventContext>();
            return serverEventContext.ProcessInstance.Folio;
        }



    }
}
