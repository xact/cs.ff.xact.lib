﻿
namespace XAct.Diagnostics
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// The default Category Values for K2 Logging purposes.
    /// </remarks>
    public enum LoggingCategory
    {
        /// <summary>
        /// 
        /// </summary>
        General=0,
        /// <summary>
        /// 
        /// </summary>
        System=1,
        /// <summary>
        /// 
        /// </summary>
        Client=2,
        /// <summary>
        /// 
        /// </summary>
        Security=3,
        /// <summary>
        /// 
        /// </summary>
        Communication=3,
        /// <summary>
        /// 
        /// </summary>
        SmartFunctions=6,
        /// <summary>
        /// 
        /// </summary>
        AuthorizationProvider=7,
        /// <summary>
        /// 
        /// </summary>
        SmartObjects=8,
        /// <summary>
        /// 
        /// </summary>
        DependencyService=9,
        /// <summary>
        /// 
        /// </summary>
        UserRoleManager=10,
        /// <summary>
        /// 
        /// </summary>
        CategoryServer=11,
        /// <summary>
        /// 
        /// </summary>
        WoerflowServer=12,
        /// <summary>
        /// 
        /// </summary>
        LiveCommServer=13,
        /// <summary>
        /// 
        /// </summary>
        EVentBut=14,
        /// <summary>
        /// 
        /// </summary>
        EnvironmentServer=15,
        /// <summary>
        /// 
        /// </summary>
        SystemAudit=16,
        /// <summary>
        /// 
        /// </summary>
        WorkActivity=17



    }
}
