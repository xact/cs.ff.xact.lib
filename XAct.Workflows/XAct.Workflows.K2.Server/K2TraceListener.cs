﻿//using SourceCode.Logging;

// ReSharper disable CheckNamespace
namespace XAct.Diagnostics  
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using SourceCode.KO;
    using XAct.Workflows;

    /// <summary>
    /// K2 based TraceListener that pumps the message back to the K2 Logging service.
    /// </summary>
    /// <summary>
    /// Trace listener that will write all trace messages to K2's logging system. 
    /// This listener makes sure not to break existing trace functionality.
    /// </summary>
    /// <remarks>
    /// <para>
    /// An example of the configuration required is as follows:
    /// <code>
    /// <![CDATA[
    /// <system.diagnostics>
    ///   <trace autoflush="true">
    ///     <listeners>
    ///       <add name="myLog4NetTraceListener" />
    ///     </listeners>
    ///   </trace>
    ///   <sources>
    ///     <!-- http://bit.ly/rpys5M -->
    ///     <!-- http://bit.ly/oYbgKG -->
    ///     <!-- http://bit.ly/mUr0cs -->
    ///     <source name="System.ServiceModel" switchValue="Information,ActivityTracing"
    ///             propagateActivity="true">
    ///       <listeners>
    ///         <add name="xml" />
    ///       </listeners>
    ///     </source>    
    ///     <source name="System.ServiceModel.MessageLogging">
    ///       <listeners>
    ///         <add name="myLog4NetTraceListener" />
    ///         <add name="messages"
    ///              type="System.Diagnostics.XmlWriterTraceListener"
    ///              initializeData="c:\logs\messages.svclog" />
    ///       </listeners>
    ///     </source>
    ///   </sources>
    ///   <sharedListeners>
    ///     <add name="myK2TraceListener" 
    ///          source="myMessageSourceTag"
    ///          type="XAct.Diagnostics.K2TraceListener,XAct.Workflows.Client"/>
    ///   </sharedListeners>
    /// </system.diagnostics>
    /// <system.serviceModel>
    ///   <diagnostics>
    ///     <messageLogging 
    ///       logEntireMessage="true" 
    ///       logMalformedMessages="false"
    ///       logMessagesAtServiceLevel="true" 
    ///       logMessagesAtTransportLevel="true"
    ///       maxSizeOfMessageToLog="2000" 
    ///     />
    ///   </diagnostics>
    /// </system.serviceModel>
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
// ReSharper disable RedundantNameQualifier
    public class K2TraceListener : TraceListener
// ReSharper restore RedundantNameQualifier
    {
        private static bool _constructing;
        /// <summary>
        /// Gets the IWorkflowContextService.
        /// </summary>
        protected IWorkflowContextService WorkflowContextService
        {
            get
            {
                return DependencyResolver.Current.GetInstance<IWorkflowContextService>();
            }
        }


        /// <summary>
        /// Gets the current K2 Instance Cntext.
        /// </summary>
        protected ServerEventContext Context
        {
            get {

                var result = this.WorkflowContextService.Context.GetContext<ServerEventContext>();
                return result;
            }
        }


        /// <summary>
        /// Gets or sets the Message Source. Set via attributes on xml fragrment in web.config
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public string Source
        {
            get
            {
                var result = this.Attributes["source"];
                return result;
            }
            set { this.Attributes["Source"] = value; }
        }


        /// <summary>
        /// Gets the message source (if set, otherwise falls back to Name of Listener).
        /// </summary>
        protected string MessageSource
        {
            get
            {
                return _MessageSource ?? (_MessageSource = (!this.Source.IsNullOrEmpty()) ? this.Source : this.Name);
            }
        }
        private string _MessageSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="K2TraceListener"/> class.
        /// </summary>
        public K2TraceListener()
        {
            if (_constructing)
            {
                return;
            }
            _constructing = true;

            //_log = InitializeLogger(this.GetType());


            //log4net.Appender.FileAppender f;
            //f.
            //log4net.Appender.RollingFileAppender r;
            //log4net.Appender.ColoredConsoleAppender cr;
            //log4net.Appender.SmtpAppender s;
            //s.Evaluator
            //log4net.Appender.ConsoleAppender c;


// ReSharper disable DoNotCallOverridableMethodsInConstructor
            Initialize();
// ReSharper restore DoNotCallOverridableMethodsInConstructor

            _constructing = false;

        }


        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected virtual void Initialize()
        {
            InitializeInternal();
        }

        private void InitializeInternal()
        {
            InitializePropertiesFromConfigAttributes();
        }

        /// <summary>
        /// Initializes Properties from the Attributes.
        /// </summary>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected virtual void InitializePropertiesFromConfigAttributes()
        {
            //base.InitializePropertiesFromConfigAttributes();

            //_autoFlush = GetAttributeValue("autoFlush", true);
            //_messageFormat = GetAttributeValue("messageFormat", "{DateTime}: {ProcessName}: {Level}: {Message}");
        }



        #region Protected Helper Methods: Config Attributes

        /// <summary>
        /// Helper method to get the typed value from the config attributes.
        /// </summary>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected TProperty GetAttributeValue<TProperty>(string key, TProperty defaultValue)
        {
            var result = this.Attributes.GetValue(key, defaultValue);
            return result;
        }

        #endregion

        #region MethodOverrides

        /// <summary>
        /// Gets the custom attributes supported by the trace listener.
        /// </summary>
        /// <returns>
        /// A string array naming the custom attributes supported by the trace listener, 
        /// or null if there are no custom attributes.
        /// </returns>
        /// <internal><para>6/9/2011: Sky</para></internal>
        protected override string[] GetSupportedAttributes()
        {
            if (_supportedAttributes != null)
            {
                return _supportedAttributes;
            }
            List<string> tmp = new List<string>();

            string[] baseValues = base.GetSupportedAttributes();
            if (baseValues != null)
            {
                tmp.AddRange(baseValues);
            }
// ReSharper disable RedundantExplicitArrayCreation
            tmp.AddRange(new string[]
// ReSharper restore RedundantExplicitArrayCreation
                             {
                                 "source"
                             });
            _supportedAttributes = tmp.ToArray();

            return _supportedAttributes;
        }

        private string[] _supportedAttributes;

        #endregion




        #region Methods

        /// <summary>
        /// Writes trace information, a formatted array of objects and event information to the listener specific output.
        /// </summary>
        /// <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"/> object that contains the current process ID, thread ID, and stack trace information.</param>
        /// <param name="source">A name used to identify the output, typically the name of the application that generated the trace event.</param>
        /// <param name="traceEventType">One of the <see cref="T:System.Diagnostics.TraceEventType"/> values specifying the type of event that has caused the trace.</param>
        /// <param name="id">A numeric identifier for the event.</param>
        /// <param name="format">A format string that contains zero or more format items, which correspond to objects in the <paramref name="args"/> array.</param>
        /// <param name="args">An object array containing zero or more objects to format.</param>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode"/>
        /// </PermissionSet>
        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType traceEventType, int id,
                                        string format, params object[] args)
        {
            // Call overload
            TraceEvent(eventCache, source, traceEventType, id, format.FormatStringCurrentCulture(args));
        }

        /// <summary>
        /// Writes trace information, a message, and event information to the listener specific output.
        /// </summary>
        /// <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"/> object that contains the current process ID, thread ID, and stack trace information.</param>
        /// <param name="source">A name used to identify the output, typically the name of the application that generated the trace event.</param>
        /// <param name="traceEventType">One of the <see cref="T:System.Diagnostics.TraceEventType"/> values specifying the type of event that has caused the trace.</param>
        /// <param name="id">A numeric identifier for the event.</param>
        /// <param name="message">A message to write.</param>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode"/>
        /// </PermissionSet>
        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType traceEventType, int id,
                                        string message)
        {
            if (!ShouldTrace(eventCache, source, traceEventType, id, message, null))
            {
                return;
            }

            // Check the type
            switch (traceEventType)
            {
                case TraceEventType.Critical:
                    this.Context.ProcessInstance.Logger.LogErrorMessage(this.MessageSource, message);
                    break;
                case TraceEventType.Error:
                    this.Context.ProcessInstance.Logger.LogErrorMessage(this.MessageSource, message);
                    break;
                case TraceEventType.Warning:
                    this.Context.ProcessInstance.Logger.LogWarningMessage(this.MessageSource, message);
                    break;
                case TraceEventType.Information:
                    this.Context.ProcessInstance.Logger.LogInfoMessage(this.MessageSource, message);
                    break;
                default:
                    this.Context.ProcessInstance.Logger.LogDebugMessage(this.MessageSource, message);
                    break;
            }
        }

        /// <summary>
        /// Writes text to the output window.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public override void Write(string message)
        {
            if (!ShouldTrace(null, null, TraceEventType.Verbose, 0, message, null))
            {
                return;
            }
            this.Context.ProcessInstance.Logger.LogDebugMessage(this.MessageSource, message);
        }

        /// <summary>
        /// Writes a line of text to the output window.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public override void WriteLine(string message)
        {
            if (_constructing)
            {
                return;
            }
            if (!ShouldTrace(null, null, TraceEventType.Verbose, 0, message, null))
            {
                return;
            }
            this.Context.ProcessInstance.Logger.LogDebugMessage(this.MessageSource, message);
        }

        #endregion


        #region Protected Helper Methods: TraceFilter

        /// <summary>
        /// Shoulds the trace.
        /// </summary>
        /// <param name="traceEventCache">The trace event cache.</param>
        /// <param name="source">The source.</param>
        /// <param name="traceEventType">Type of the trace event.</param>
        /// <param name="id">The id.</param>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected virtual bool ShouldTrace(TraceEventCache traceEventCache, string source, TraceEventType traceEventType,
                                           int id, string format, params object[] args)
        {
            return ShouldTrace(this.Filter, traceEventCache, source, traceEventType, id, format, args);
        }

        /// <summary>
        /// Shoulds the trace.
        /// </summary>
        /// <param name="traceFilter">The trace filter.</param>
        /// <param name="traceEventCache">The trace event cache.</param>
        /// <param name="source">The source.</param>
        /// <param name="traceEventType">Type of the trace event.</param>
        /// <param name="id">The id.</param>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected virtual bool ShouldTrace(TraceFilter traceFilter, TraceEventCache traceEventCache, string source,
                                           TraceEventType traceEventType, int id, string format, params object[] args)
        {
            return
                (
                    (this.Filter == null)
                    ||
                    (this.Filter.ShouldTrace(traceEventCache, source, traceEventType, id, format, args, null, null))
                );
        }

        #endregion


    }
}
