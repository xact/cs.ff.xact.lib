﻿
// ReSharper disable CheckNamespace

namespace XAct.Workflows.Implementations
// ReSharper restore CheckNamespace
{
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IWorkflowContextVariablesService"/>
    /// to store and retrieve variables scoped to a single workflow instance.
    /// <para>
    /// By using this service, rather than referencing directly the underlying
    /// K2 ProcessInstance DataFields, you will keep your code modular,
    /// mockable, as well as Vendor agnostic, in case anything better than 
    /// K2 ever comes along.
    /// </para>
    /// <para>
    /// Depends on an instance of <see cref="IWorkflowContextService"/>
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowContextVariablesService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class K2WorkflowContextVariablesService : K2WorkflowContextServiceBase, IWorkflowContextVariablesService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="K2WorkflowContextHostSettingsService"/> class.
        /// <para>
        /// Constructor is intended to be DI'ed by an DependencyInjectionContainer.
        /// </para>
        /// </summary>
        /// <param name="contextService">The context service.</param>
        public K2WorkflowContextVariablesService(IWorkflowContextService contextService)
            : base(contextService)
        {
            //by using a base class (K2WorkflowContextedServiceBase) 
            //makes it trivial to get a handle on the current K2 context
            //and use it for the methods of this service.
        }


        /// <summary>
        /// Sets the Instance scoped variable.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Example of usage:
        /// <code>
        /// <![CDATA[
        /// XAct.DependencyResolver.Current
        ///   .GetInstance<IWorkflowContextVariablesService>().
        ///   .Set<int>("SomeNumber") = 101;
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Set<TValue>(string key, TValue value)
        {
            //Serialize the Type to a string?
            base.ServerEventContext.ProcessInstance.DataFields[key].Value = value.ConvertTo<string>();
        }

        /// <summary>
        /// Gets the Instances scoped variable value.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Example of usage:
        /// <code>
        /// <![CDATA[
        /// int someNumber = 
        ///   XAct.DependencyResolver.Current
        ///     .GetInstance<IWorkflowContextVariablesService>().
        ///     .Get("SomeNumber");
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key)
        {
            //Deserialize the string back to an object Type:
            return base.ServerEventContext.ProcessInstance.DataFields[key].Value.ConvertTo<TValue>();
        }

        /// <summary>
        /// Gets the Instances scoped variable value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        /// <remarks>
        /// Example of usage:
        /// <code>
        /// 		<![CDATA[
        /// int someNumber =
        /// XAct.DependencyResolver.Current
        /// .GetInstance<IWorkflowContextVariablesService>().
        /// .Get("SomeNumber");
        /// ]]>
        /// 	</code>
        /// </remarks>
        public TValue Get<TValue>(string key, TValue defaultValue)
                {
                    if (base.ServerEventContext.ProcessInstance.DataFields.Contains(key))
                    {
                        return base.ServerEventContext.ProcessInstance.DataFields[key].Value.ConvertTo<TValue>();
                    }else
                    {
                        return defaultValue;
                    }
                }
    

    }
}
