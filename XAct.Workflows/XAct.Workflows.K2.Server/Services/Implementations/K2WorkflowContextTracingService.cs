﻿// ReSharper disable CheckNamespace
namespace XAct.Workflows.Implementations
// ReSharper restore CheckNamespace
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IWorkflowContextTracingService"/> contract.
    /// <para>
    /// It is highly recommended to *not* use it directly, but instead
    /// use the a more decoupled approach -- such as using the general 
    /// System.Diagnostics.Trace mechanism, with a backing
    /// TraceListener to pump the messages to the Workflow engine. 
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowContextTracingService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class K2WorkflowContextTracingService : K2WorkflowContextServiceBase, IWorkflowContextTracingService
    {
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="K2WorkflowContextTracingService"/> class,
        /// to send messages to the K2 workflow Server's trace solution.
        /// <para>
        /// It is highly recommended to *not* use it directly, but instead
        /// use the a more decoupled approach -- such as using the general
        /// System.Diagnostics.Trace mechanism, with a backing
        /// TraceListener to pump the messages to the Workflow engine.
        /// </para>
        /// </summary>
        /// <param name="contextService">The context service.</param>
        /// <param name="tracingService">The logging service.</param>
        public K2WorkflowContextTracingService(IWorkflowContextService contextService, ITracingService tracingService)
            : base(contextService)
        {
            //by using a base class (K2WorkflowContextedServiceBase) 
            //makes it trivial to get a handle on the current K2 context
            //and use it for the methods of this service.

            _tracingService = tracingService;
        }

        /// <summary>
        /// Gets or sets the trace source.
        /// </summary>
        /// <value>
        /// The trace source.
        /// </value>
        public string TraceSource { get; set; }

        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="message">The message.</param>
        /// <param name="messageArgs">The message args.</param>
        public void Trace(TraceLevel traceLevel,  string message, params object[] messageArgs)
        {
            if (messageArgs != null)
            {
                try
                {
                    message = string.Format(message, messageArgs);
                }
// ReSharper disable EmptyGeneralCatchClause
                catch
// ReSharper restore EmptyGeneralCatchClause
                {
                }
            }
            if (this.TraceSource.IsNullOrEmpty())
            {
                this.TraceSource = "CUSTOM";
            }
            switch (traceLevel)
            {
                case TraceLevel.Off:
                    break;
                case TraceLevel.Verbose:
                    //Use the tracing built into K2:
                    base.ServerEventContext.ProcessInstance.Logger.LogDebugMessage(this.TraceSource, message);
                    return;
                case TraceLevel.Info:
                    //Use the tracing built into K2:
                    base.ServerEventContext.ProcessInstance.Logger.LogInfoMessage(this.TraceSource, message);
                    return;
                case TraceLevel.Warning:
                    //Use the tracing built into K2:
                    base.ServerEventContext.ProcessInstance.Logger.LogWarningMessage(this.TraceSource, message);
                    return;
                case TraceLevel.Error:
                    //Use the tracing built into K2:
                    base.ServerEventContext.ProcessInstance.Logger.LogErrorMessage(this.TraceSource, message);
                    return;
            }

        }
    }
}
