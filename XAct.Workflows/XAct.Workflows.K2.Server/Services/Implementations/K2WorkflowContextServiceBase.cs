﻿// ReSharper disable CheckNamespace
namespace XAct.Workflows.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using SourceCode.KO;

    /// <summary>
    /// Base abstract class for services that reference 
    /// the current K2 Context.
    /// <para>
    /// Simplifies 
    /// </para>
    /// <para>
    /// Implemented by 
    /// <see cref="K2WorkflowContextHostSettingsService"/>,
    /// <see cref="K2WorkflowContextVariablesService"/>
    /// and
    /// <see cref="K2WorkflowContextTracingService"/>,
    /// </para>
    /// </summary>
    public abstract class K2WorkflowContextServiceBase
    {
        /// <summary>
        /// Gets the IWorkflowContextService.
        /// </summary>
        protected readonly IWorkflowContextService WorkflowContextService;


        /// <summary>
        /// Initializes a new instance of the <see cref="K2WorkflowContextServiceBase"/> class.
        /// </summary>
        /// <param name="workflowContextService">The workflow context service.</param>
        protected K2WorkflowContextServiceBase(IWorkflowContextService workflowContextService)
        {
            if (workflowContextService == null) { throw new ArgumentNullException("workflowContextService"); }

            WorkflowContextService = workflowContextService;
        }

        /// <summary>
        /// Gets the current K2 <see cref="ServerEventContext"/>.
        /// </summary>
        protected ServerEventContext ServerEventContext
        {
            get
            {
                //Do not cached it at this level:
                return WorkflowContextService.Context.ServerEventContext();
            }
        }

    }
}
