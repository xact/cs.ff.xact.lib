﻿
// ReSharper disable CheckNamespace

namespace XAct.Workflows.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Services;
    using XAct.Settings;

    /// <summary>
    /// An implementation of <see cref="IWorkflowContextHostSettingsService"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// By using this service, rather than referencing directly the underlying
    /// K2 ProcessInstance StringTable, you will keep your code modular,
    /// mockable, as well as Vendor agnostic, in case anything better than 
    /// K2 ever comes along.
    /// </para>
    /// <para>
    /// Depends on an instance of <see cref="IWorkflowContextService"/>
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(IWorkflowContextHostSettingsService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class K2WorkflowContextHostSettingsService : K2WorkflowContextServiceBase, IWorkflowContextHostSettingsService
    {

        /// <summary>
        /// Gets the common singleton settings shared between
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public IHostSettingsServiceConfiguration Configuration
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="K2WorkflowContextHostSettingsService"/> class.
        /// </summary>
        /// <param name="contextService">The context service.</param>
        public K2WorkflowContextHostSettingsService (IWorkflowContextService contextService):base(contextService)
        {
            //by using a base class (K2WorkflowContextedServiceBase) 
            //makes it trivial to get a handle on the current K2 context
            //and use it for the methods of this service.
        }
        /// <summary>
        /// Gets the current Settings.
        /// </summary>
        /// <value>
        /// The settings.
        /// </value>
        public XAct.Settings.Settings Settings { get; set; }

        /// <summary>
        /// Gets the specified (readonly) Host Setting (in K2, this a value in the  Server's StringTable).
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key, bool throwExceptionOnConversionException = true)
        {

            string result = base.ServerEventContext.StringTable[key];


            if (throwExceptionOnConversionException)
            {
                return result.ConvertTo<TValue>();
            }
            try
            {
                return result.ConvertTo<TValue>();
            }
            catch
            {
                return default(TValue);
            }

        }




        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key, TValue defaultValue, bool throwExceptionOnConversionException = true)
        {
            string result = base.ServerEventContext.StringTable[key];
            
            if (result == null)
            {
                return defaultValue;
            } 
            if (throwExceptionOnConversionException)
            {
                return result.ConvertTo<TValue>();
            }
            try {
                return result.ConvertTo<TValue>();
            }
            catch
            {
                return defaultValue;
            }
        }




        /// <summary>
        /// Retrieves this instance.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public TSettings Current<TSettings>()
            where TSettings : Settings
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Persists this instance.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Persist()
        {
            throw new System.NotImplementedException();
        }
    }
}
