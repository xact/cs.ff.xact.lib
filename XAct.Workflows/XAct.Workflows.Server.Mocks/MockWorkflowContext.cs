﻿namespace XAct.Workflows
{
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public interface IMockWorkflowContext : IWorkflowContext {}

    /// <summary>
    /// Mock implementation of a Work Context.
    /// </summary>
    [DefaultBindingImplementation(typeof(IMockWorkflowContext), BindingLifetimeType.SingletonScope, Priority.Low /*OK: overridden*/)]
    [DefaultBindingImplementation(typeof(IWorkflowContext), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class MockWorkflowContext : IMockWorkflowContext
    {
        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <typeparam name="TContextType">The type of the context type.</typeparam>
        /// <returns></returns>
        public TContextType GetContext<TContextType>()
        {
            return default(TContextType);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "[FAKE CONTEXT]";
        }
    }
}
