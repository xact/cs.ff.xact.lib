﻿namespace XAct.Workflows.Implementations
{
    using XAct.Diagnostics;
    using XAct.Services;
    using XAct.Settings;

    /// <summary>
    /// A mock implementation of <see cref="IWorkflowCommandServices"/>
    /// for testing purposes.
    /// <para>
    /// In a test case, register the service early (in the Bootstrapper).
    /// You do not need to register any of the other Mock Workflow Services
    /// unless you are invoking Workflow services anywhere else than Commands
    /// (eg, an MVC test might need to register the Mocks).
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowCommandServices), BindingLifetimeType.SingletonScope, Priority.Low /*OK:SecondaryBinding*/)]
    public class MockWorkflowCommandServices : IWorkflowCommandServices
    {
        private readonly ITracingService _tracingService;
        private readonly IApplicationSettingsService _appSettingsService;
        private readonly IWorkflowContextService _workflowContextService;

        private readonly IWorkflowContextHostSettingsService _workflowProcessHostSettingsService;
        private readonly IWorkflowContextVariablesService _workflowProcessInstanceVariablesService;
        private readonly IWorkflowContextTracingService _workflowProcessTraceService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockWorkflowCommandServices"/> class.
        /// </summary>
        public MockWorkflowCommandServices()
        {
            _tracingService = DependencyResolver.Current.GetInstance<ITracingService>();
            _appSettingsService = DependencyResolver.Current.GetInstance<IApplicationSettingsService>();

            _workflowContextService =
                DependencyResolver.Current.GetInstance<IWorkflowContextService>(false) as IMockWorkflowContextService ?? DependencyResolver.Current.GetInstance<IMockWorkflowContextService>();
            _workflowProcessHostSettingsService = DependencyResolver.Current.GetInstance<IWorkflowContextHostSettingsService>(false) as IMockWorkflowContextHostSettingsService??DependencyResolver.Current.GetInstance<IMockWorkflowContextHostSettingsService>();
            _workflowProcessInstanceVariablesService = DependencyResolver.Current.GetInstance <IWorkflowContextVariablesService>(false) as IMockWorkflowContextVariablesService??DependencyResolver.Current.GetInstance<IMockWorkflowContextVariablesService>();
            _workflowProcessTraceService = DependencyResolver.Current.GetInstance <IWorkflowContextTracingService>(false) as IMockWorkflowContextTracingService??DependencyResolver.Current.GetInstance <IMockWorkflowContextTracingService>();

        }

        /// <summary>
        /// Gets the default library tracing service.
        /// </summary>
        public ITracingService TracingService 
        {
            get { return _tracingService; }
        }

        /// <summary>
        /// Gets the workflow Context service.
        /// </summary>
        public IWorkflowContextService WorkflowContextService
        {
            get { return _workflowContextService; }
        }

        /// <summary>
        /// Service to retrieve from the Workflow server immutable host settings.
        /// (Think: appSettings).
        /// </summary>
        public IWorkflowContextHostSettingsService WorkflowContextHostSettingsService
        {
            get { return _workflowProcessHostSettingsService; }
        }

        /// <summary>
        /// Service to get/set settings specific to this workflow instance.
        /// </summary>
        public IWorkflowContextVariablesService WorkflowContextVariablesService
        {
            get { return _workflowProcessInstanceVariablesService; }
        }

        /// <summary>
        /// Service to trace messages into the Workflow server's tracing system.
        /// </summary>
        public IWorkflowContextTracingService WorkflowContextTraceService
        {
            get { return _workflowProcessTraceService; }
        }
    }
}
