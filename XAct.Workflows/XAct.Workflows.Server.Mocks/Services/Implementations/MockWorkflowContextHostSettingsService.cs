﻿namespace XAct.Workflows.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;
    using XAct.Settings;

    /// <summary>
    /// 
    /// </summary>
    public interface IMockWorkflowContextHostSettingsService : IWorkflowContextHostSettingsService, IHasXActLibService, IHasGetSet{}

    /// <summary>
    /// A mock implementation of <see cref="IWorkflowContextHostSettingsService"/>
    /// for testing purposes.
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowContextHostSettingsService), BindingLifetimeType.SingletonScope, Priority.Low /*OK:SecondaryBinding*/)]
    public class MockWorkflowContextHostSettingsService : IMockWorkflowContextHostSettingsService 
    {
        private readonly Dictionary<string,object> _settings 
            = new Dictionary<string, object>();

        private readonly IHostSettingsService _hostSettingsService;



        /// <summary>
        /// Gets the common singleton settings shared between
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IHostSettingsServiceConfiguration Configuration
        {
            get
            {

                return _hostSettingsService.Configuration;
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="MockWorkflowContextHostSettingsService"/> class.
        /// </summary>
        /// <param name="hostSettingsService">The host settings service.</param>
        public MockWorkflowContextHostSettingsService(IHostSettingsService hostSettingsService)
        {
            hostSettingsService.ValidateIsNotDefault("appSettingsService");
            _hostSettingsService = hostSettingsService;
        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key)
        {
            object result;
            if (_settings.TryGetValue(key, out result))
            {
                return result.ConvertTo<TValue>();
            }
            //fall back to AppSettings:
            return _hostSettingsService.Get<TValue>(key);
        }


        /// <summary>
        /// Gets the specified (readonly) Host Setting.
        /// <para>
        /// Note that in K2, this a value in the  Server's StringTable.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key, bool throwExceptionOnConversionException = true)
        {
            object result;
            if (_settings.TryGetValue(key, out result))
            {
                return result.ConvertTo<TValue>();
            }
            //fall back to AppSettings:
            return _hostSettingsService.Get<TValue>(key);
        }

        /// <summary>
        /// Gets the specified (readonly) Host Setting.
        /// <para>
        /// Note that in K2, this a value in the  Server's StringTable.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key, TValue defaultValue, bool throwExceptionOnConversionException = true)
        {
            object result;
            if (_settings.TryGetValue(key, out result))
            {
                if (result == null)
                {
                    return defaultValue;
                }
                return result.ConvertTo<TValue>();
            }
            return _hostSettingsService.Get(key, defaultValue);
        }

        /// <summary>
        /// Sets the specified key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void Set<TValue>(string key, TValue value)
        {
            _settings[key] = value;
        }

        /// <summary>
        /// Retrieves this instance.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public TSettings Current<TSettings>()
            where TSettings : XAct.Settings.Settings
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Persists this instance.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Persist()
        {
            throw new System.NotImplementedException();
        }
    }
}