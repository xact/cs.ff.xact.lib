﻿namespace XAct.Workflows.Implementations
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public interface IMockWorkflowContextTracingService : IWorkflowContextTracingService, IHasXActLibService {}

    /// <summary>
    /// A mock implementation of <see cref="IWorkflowContextTracingService"/>
    /// that sends output to the current <see cref="ITracingService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowContextTracingService), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class MockWorkflowContextTracingService : IMockWorkflowContextTracingService
    {
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockWorkflowContextTracingService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public MockWorkflowContextTracingService(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }

        /// <summary>
        /// Gets or sets the trace source (added to Trace output).
        /// </summary>
        /// <value>
        /// The trace source.
        /// </value>
        public string TraceSource { get; set; }

        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="message">The message.</param>
        /// <param name="messageArgs">The message args.</param>
        public void Trace(TraceLevel traceLevel, string message, params object[] messageArgs)
        {
            string source = (!TraceSource.IsNullOrEmpty()) ? TraceSource : "CUSTOM";

            message = "{0}: {1}".FormatStringCurrentUICulture(source, message);
            
            _tracingService.Trace(traceLevel,message,messageArgs);
        }
    }
}