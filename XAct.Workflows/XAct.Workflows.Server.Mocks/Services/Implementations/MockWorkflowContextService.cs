﻿namespace XAct.Workflows.Implementations
{
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public interface IMockWorkflowContextService: IWorkflowContextService, IHasXActLibService
    {}

    /// <summary>
    /// A mock implementation of <see cref="IWorkflowContextService"/>
    /// for testing purposes.
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowContextService), BindingLifetimeType.SingletonScope, Priority.Low /*OK:SecondaryBinding*/)]
    public class MockWorkflowContextService : IMockWorkflowContextService
    {
        /// <summary>
        /// Gets the workflow's current event instance context.
        /// <para>
        /// To *set* the Context -- which should only be done only when initializaing
        /// a Workflow <c>Event Item</c>, use
        /// <see cref="IWorkflowContextManagementService"/> (keeps separation of concerns).
        /// </para>
        /// 	<para>
        /// Defined in <see cref="IWorkflowContextService"/>.
        /// </para>
        /// </summary>
        public IWorkflowContext Context
        {
            get { return new MockWorkflowContext(); }
        }
    }
}
