﻿namespace XAct.Workflows.Implementations
{
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public interface IMockWorkflowContextManagementService: IWorkflowContextManagementService, IHasXActLibService {}
    /// <summary>
    /// A mock implementation of <see cref="IWorkflowContextManagementService"/>
    /// for testing purposes.
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowContextManagementService), BindingLifetimeType.SingletonScope, Priority.Low /*OK:SecondaryBinding*/)]
    public class MockWorkflowContextManagementService : IMockWorkflowContextManagementService
    {

        /// <summary>
        /// Initializes the <see cref="IWorkflowContext"/>
        /// that the other services workflow services
        /// (
        /// <see cref="IWorkflowContextTracingService"/>,
        /// <see cref="IWorkflowContextHostSettingsService"/>,
        /// <see cref="IWorkflowContextVariablesService"/>,
        /// )
        /// will need.
        /// <para>
        /// This should be invoked only during the initialization
        /// phase of a Workflow Event Item. From then on,
        /// use <see cref="IWorkflowContextService"/> to
        /// retrieve the Context.
        /// </para>
        /// </summary>
        public IWorkflowContext Context { get; set; }
    }
}
