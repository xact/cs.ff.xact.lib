﻿namespace XAct.Workflows.Implementations
{
    using System.Collections.Generic;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public interface IMockWorkflowContextVariablesService : IWorkflowContextVariablesService, IHasXActLibService {}

    /// <summary>
    /// A mock implementation of <see cref="IWorkflowContextVariablesService"/>
    /// for testing purposes.
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowContextVariablesService), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class MockWorkflowContextVariablesService : IMockWorkflowContextVariablesService
    {
        internal readonly Dictionary<string, object> Settings
            = new Dictionary<string, object>();

        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockWorkflowContextVariablesService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public MockWorkflowContextVariablesService(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }

        /// <summary>
        /// Gets the Instances scoped variable value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key)
        {
            return Settings[key].ConvertTo<TValue>();

        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public TValue Get<TValue>(string key, TValue defaultValue)
        {
            object result;
            if (Settings.TryGetValue(key, out result))
            {
                return Settings[key].ConvertTo<TValue>();
            }
            return defaultValue;

        }

        /// <summary>
        /// Sets the Instance scoped variable.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Set<TValue>(string key, TValue value)
        {
            Settings[key] = value;
        }
    }
}