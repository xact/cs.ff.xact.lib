﻿namespace XAct.Workflows.Tests
{
    using System;
    using System.Diagnostics;
    using NUnit.Framework;
    using XAct.Settings;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class UnitTests1
    {

        protected string TestServerName
        {
            get { 
                
                return DependencyResolver.Current.GetInstance<IHostSettingsService>().Get<string>("K2TestServerName");
            }
        }

        protected uint TestServerPort
        {
            get {
                return DependencyResolver.Current.GetInstance<IHostSettingsService>().Get<uint>("K2TestServerPort");
            }
        }
        protected string TestServerBasePath
        {
            get {
                return DependencyResolver.Current.GetInstance<IHostSettingsService>().Get<string>("K2TestServerBasePath");
            }
        }

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Test01()
        {
            IServerConnectionConfigurationManagerService serverConnectionConfigurationManager =
                DependencyResolver.Current.GetInstance<IServerConnectionConfigurationManagerService>();

            Assert.AreEqual(typeof(K2ServerConnectionConfigurationManager),serverConnectionConfigurationManager.GetType());

        }

        [Test]
        public void Test02()
        {

            IServerConnectionConfiguration serverConnectionConfiguration =
                DependencyResolver.Current.GetInstance<IServerConnectionConfiguration>();

            Assert.AreEqual(typeof(K2ServerConnectionConfiguration), serverConnectionConfiguration.GetType());

        }

        [Test]
        public void Test03()
        {

            IServerConnectionConfigurationManagerService serverConnectionConfigurationManager =
                DependencyResolver.Current.GetInstance<IServerConnectionConfigurationManagerService>();

            IServerConnectionConfiguration serverConnectionConfiguration =
                DependencyResolver.Current.GetInstance<IServerConnectionConfiguration>();

            serverConnectionConfigurationManager.Current = serverConnectionConfiguration;
            serverConnectionConfiguration.ServerName = this.TestServerName;


        }

        [Test]
        public void Test04()
        {

            IServerConnectionConfigurationManagerService serverConnectionConfigurationManager =
                DependencyResolver.Current.GetInstance<IServerConnectionConfigurationManagerService>();

            IServerConnectionConfiguration serverConnectionConfiguration =
                DependencyResolver.Current.GetInstance<IServerConnectionConfiguration>();

            serverConnectionConfigurationManager.Current = serverConnectionConfiguration;
            serverConnectionConfiguration.ServerName = this.TestServerName;


#pragma warning disable 168
            string check = serverConnectionConfiguration.ConnectionString;
#pragma warning restore 168


        }



        [Test]
        public void Test05()
        {

            IServerConnectionConfigurationManagerService serverConnectionConfigurationManager =
                DependencyResolver.Current.GetInstance<IServerConnectionConfigurationManagerService>();

            IK2ServerConnectionConfiguration serverConnectionConfiguration =
                DependencyResolver.Current.GetInstance<IK2ServerConnectionConfiguration>();

            serverConnectionConfigurationManager.Current = serverConnectionConfiguration;
            serverConnectionConfiguration.ServerName = this.TestServerName;
            serverConnectionConfiguration.PortNumber= this.TestServerPort;
            serverConnectionConfiguration.ProcessBasePath = this.TestServerBasePath;

#pragma warning disable 168
            string check = serverConnectionConfiguration.ConnectionString;
#pragma warning restore 168
            Debug.WriteLine(check);

            //IK2ServerConnectionConfigurationManager serverConnectionConfigurationManager
            //    = DependencyResolver.Current.GetInstance<IK2ServerConnectionConfigurationManager>();

#pragma warning disable 168
            using (IK2ClientConnectionDisposableContainer clientConnectionDisposableContainer
#pragma warning restore 168
                = DependencyResolver.Current.GetInstance<IK2ClientConnectionDisposableContainer>())
            {
                
            }

        }

    }

}
