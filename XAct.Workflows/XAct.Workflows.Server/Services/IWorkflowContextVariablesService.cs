﻿
// ReSharper disable CheckNamespace
namespace XAct.Workflows
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A service to save variables scoped to a specific Workflow Instance.
    /// </summary>
    /// <remarks>
    /// <para>
    /// By using this service, rather than referencing directly the underlying
    /// K2 ProcessInstance DataFields, you will keep your code modular,
    /// mockable, as well as Vendor agnostic.
    /// </para>
    /// </remarks>
    public interface IWorkflowContextVariablesService : IHasXActLibService
    {

        /// <summary>
        /// Sets the Instance scoped variable.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        void Set<TValue>(string key, TValue value);


        /// <summary>
        /// Gets the Instances scoped variable value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        TValue Get<TValue>(string key);


        /// <summary>
        /// Gets the Instances scoped variable value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        TValue Get<TValue>(string key, TValue defaultValue);

    }
}
