﻿
// ReSharper disable CheckNamespace
namespace XAct.Workflows
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// The contract for a Service to set the current 
    /// <see cref="IWorkflowContext"/> (done only during the init
    /// phase of a Workflow Item). After that, prefer 
    /// <see cref="IWorkflowContextService"/> to retrieve the 
    /// <see cref="IWorkflowContext"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// The functionality to set and get the current
    /// <see cref="IWorkflowContext"/> is split across two services
    /// for the sake of <c>Separation of Concerns</c>.
    /// </para>
    /// </remarks>
    public interface IWorkflowContextManagementService : IWorkflowContextService, IHasXActLibService
    {
        /// <summary>
        /// Initializes the <see cref="IWorkflowContext"/>
        /// that the other services workflow services
        /// (
        /// <see cref="IWorkflowContextTracingService"/>,
        /// <see cref="IWorkflowContextHostSettingsService"/>,
        /// <see cref="IWorkflowContextVariablesService"/>,
        /// )
        /// will need.
        /// <para>
        /// This should be invoked only during the initialization 
        /// phase of a Workflow Event Item. From then on, 
        /// use <see cref="IWorkflowContextService"/> to 
        /// retrieve the Context.
        /// </para>
        /// </summary>
        new IWorkflowContext Context { get; set; }


    }
}
