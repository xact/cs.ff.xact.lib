// ReSharper disable CheckNamespace
namespace XAct.Workflows
// ReSharper restore CheckNamespace
{
    using XAct.Diagnostics;

    /// <summary>
    /// Contract for a service that logs messages to the current Workflow engine.
    /// <para>
    /// It is highly recommended to *not* use it directly, but instead
    /// use the a more decoupled approach -- such as using the general 
    /// System.Diagnostics.Trace mechanism, with a backing
    /// TraceListener to pump the messages to the Workflow engine. 
    /// </para>
    /// </summary>
    public interface IWorkflowContextTracingService : IHasXActLibService
    {

        /// <summary>
        /// Gets or sets the trace source (added to Trace output).
        /// </summary>
        /// <value>
        /// The trace source.
        /// </value>
        string TraceSource { get; set; }

        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="message">The message.</param>
        /// <param name="messageArgs">The message args.</param>
        void Trace(TraceLevel traceLevel, string message, params object[] messageArgs);
    }
}