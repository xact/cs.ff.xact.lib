
// ReSharper disable CheckNamespace
namespace XAct.Workflows
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Service to *get* the current Workflow Process Instance's Context.
    /// <para>
    /// Use <see cref="IWorkflowContextManagementService"/> to set it.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// The functionality to set and get the current
    /// <see cref="IWorkflowContext"/> is split across two services
    /// for the sake of <c>Separation of Concerns</c>.
    /// </para>
    /// </remarks>
    public interface IWorkflowContextService : IHasXActLibService
    {
        /// <summary>
        /// Gets the workflow's current event instance context.
        /// <para>
        /// To *set* the Context -- which should only be done only when initializaing
        /// a Workflow <c>Event Item</c>, use 
        /// <see cref="IWorkflowContextManagementService"/> (keeps separation of concerns).
        /// </para>
        /// <para>
        /// Defined in <see cref="IWorkflowContextService"/>.
        /// </para>
        /// </summary>
        IWorkflowContext Context { get; }

    }
}