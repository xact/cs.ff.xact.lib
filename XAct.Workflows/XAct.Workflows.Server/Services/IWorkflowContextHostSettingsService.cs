﻿
// ReSharper disable CheckNamespace

namespace XAct.Workflows
// ReSharper restore CheckNamespace
{
    using XAct.Settings;

    /// <summary>
    /// Service to retrieve readonly Settings scoped to a workflow (not a running instance).
    /// <para>
    /// Think of them as Workflow specific <c>AppSettings</c>.
    /// </para>
    /// </summary>
    public interface IWorkflowContextHostSettingsService : IHostSettingsService, IHasXActLibService
    {
        /*
        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        TValue Get<TValue>(string key);

        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// <para>
        /// If the result is null, <paramref name="defaultValue"/>.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value if no value was found.</param>
        /// <returns></returns>
        TValue Get<TValue>(string key, TValue defaultValue);
         */
    }
}
