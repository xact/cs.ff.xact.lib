﻿namespace XAct.Workflows
{
    using XAct.Diagnostics;

    /// <summary>
    /// Contract for a constructor argument package
    /// used by <c>WorkflowCommandMessageHandlerBase{TCommandMessage, TCommandResponse}</c>
    /// </summary>
    public interface IWorkflowCommandServices
    {
        /// <summary>
        /// Gets the default library tracing service.
        /// </summary>
        ITracingService TracingService { get; }

        /// <summary>
        /// Gets the workflow Context service.
        /// </summary>
        IWorkflowContextService WorkflowContextService { get; }

        /// <summary>
        /// Service to retrieve from the Workflow server immutable host settings.
        /// (Think: appSettings).
        /// </summary>
        IWorkflowContextHostSettingsService WorkflowContextHostSettingsService { get; }

        /// <summary>
        /// Service to get/set settings specific to this workflow instance.
        /// </summary>
        IWorkflowContextVariablesService WorkflowContextVariablesService { get; }

        /// <summary>
        /// Service to trace messages into the Workflow server's tracing system.
        /// </summary>
        IWorkflowContextTracingService WorkflowContextTraceService { get; }
    }
}