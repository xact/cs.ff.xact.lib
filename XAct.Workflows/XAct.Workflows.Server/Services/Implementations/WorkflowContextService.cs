﻿// ReSharper disable CheckNamespace
namespace XAct.Workflows.Implementations
// ReSharper restore CheckNamespace
{
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IWorkflowContextService"/>
    /// to return Context of current Workflow Instance Event.
    /// </summary>
    /// <internal>
    /// Has to be <c>SingletonPerWebRequest</c>
    /// as <see cref="IWorkflowContextManagementService"/>
    /// is <c>SingletonPerWebRequest</c>
    /// </internal>
    public class WorkflowContextService : IWorkflowContextService, IHasWebThreadBindingScope
    {
        private readonly IWorkflowContextManagementService _workflowContextManagementService;

        /// <summary>
        /// Gets the workflow's current event instance context.
        /// <para>
        /// To *set* the Context -- which should only be done only when initializaing
        /// a Workflow <c>Event Item</c>, use
        /// <see cref="IWorkflowContextManagementService"/> (keeps separation of concerns).
        /// </para>
        /// 	<para>
        /// Defined in <see cref="IWorkflowContextService"/>.
        /// </para>
        /// </summary>
        public IWorkflowContext Context
        {
            get { return _workflowContextManagementService.Context; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowContextService"/> class.
        /// </summary>
        /// <param name="workflowContextManagementService">The workflow context management service.</param>
        public WorkflowContextService(IWorkflowContextManagementService workflowContextManagementService)
        {
            workflowContextManagementService.ValidateIsNotDefault("workflowContextManagementService");

            _workflowContextManagementService = workflowContextManagementService;
        }

    }
}
