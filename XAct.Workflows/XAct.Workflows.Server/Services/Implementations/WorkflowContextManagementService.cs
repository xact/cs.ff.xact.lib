﻿
// ReSharper disable CheckNamespace
namespace XAct.Workflows.Implementations
// ReSharper restore CheckNamespace
{
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IWorkflowContextManagementService"/>
    /// used to set the <see cref="IWorkflowContext"/> required by 
    /// <para>
    /// Usage example:
    /// <![CDATA[
    /// public partial class EventItem_dc9bf : ICodeExtender<hostContext> {
    ///   public void Main(Project_7a14.EventItemContext_dc9b K2) {
    ///      //Init the DependencyInjectionContainer
    ///      ...
    ///      //Init the Context:
    ///      DependencyResolver.Current.GetInstance<IWorkflowContentManagementService>().Context =
    ///        new 
    ///   }
    /// }
    /// ]]>
    /// </para>
    /// </summary>
    public class WorkflowContextManagementService : IWorkflowContextManagementService,IHasWebThreadBindingScope
    {

        /// <summary>
        /// Gets or sets the workflow's current event instance context.
        /// <para>
        /// Note that *setting* the Context should only be only when initializing
        /// a Workflow <c>Event Item</c>.
        /// <para>
        /// For Separation of Concerns reasons, 
        /// subsequent retrieval of the context shouldbe done via 
        /// <see cref="IWorkflowContextService"/>.
        /// </para>
        /// </para>
        /// 	<para>
        /// Defined in <see cref="IWorkflowContextService"/>.
        /// </para>
        /// </summary>
        public IWorkflowContext Context { get; set; }


    }
}
