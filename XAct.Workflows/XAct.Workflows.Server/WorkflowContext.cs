﻿
namespace XAct.Workflows
{
    /// <summary>
    /// An implementation of <see cref="IWorkflowContext"/>,
    /// to provide a good starting point for making a vendor specific
    /// Worflow context.
    /// <para>
    /// For example, can be quickly make specific to K2, as follows:
    /// <code>
    /// <![CDATA[
    /// public K2WorkflowContext : WorkflowContext : IK2WorkflowContext {
    ///   public K2WorkflowContext(SourceCode.KO.ServerEventContext serverEventContext)
    ///     :base(typeof(SourceCode.KO.ServerEventContext), serverEventContext){
    ///     //That's all that's needed...
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// It wraps, and returns a <c>ServerEventContext</c>
    /// </para>
    /// </summary>
    public class WorkflowContext : Context , IWorkflowContext
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowContext"/> class.
        /// </summary>
        /// <param name="serverEventContext">The server event context.</param>
        public WorkflowContext(object serverEventContext) : base(serverEventContext)
        {
            
        }
    }
}
