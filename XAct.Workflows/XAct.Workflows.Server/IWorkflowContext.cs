﻿
// ReSharper disable CheckNamespace
namespace XAct.Workflows
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A specialization of <see cref="IHasGetContext"/>,
    /// to encapsulate in a vendor agnostic way  the workflow context.
    /// <para>
    /// Returned by <see cref="IWorkflowContextService"/>
    /// once it has been set via <see cref="IWorkflowContextManagementService"/>
    /// </para>
    /// <para>
    /// Note: adds no functionality -- only used for scoping ExtensionMethods, etc.
    /// </para>
    /// </summary>
    public interface IWorkflowContext : IHasGetContext
    {
    }
}
