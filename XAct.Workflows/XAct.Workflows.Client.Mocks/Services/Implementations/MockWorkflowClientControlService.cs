﻿namespace XAct.Workflows.Services.Implementations
{
    using System.Collections.Generic;
    using System.Text;
    using XAct.Diagnostics;
    using XAct.Services;
    using XAct.Workflows.Mocks;

    /// <summary>
    /// A mock implementation of <see cref="IWorkflowClientControlService"/>
    /// for testing purposes.
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowClientControlService), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class MockWorkflowClientService : IMockWorkflowClientService
    {
        private readonly ITracingService _tracingService;
        private readonly IWorkflowClientVariablesService _workflowClientVariablesService;


        

        /// <summary>
        /// Initializes a new instance of the <see cref="MockWorkflowClientService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="workflowClientVariablesService">The workflow process instance variables service.</param>
        public MockWorkflowClientService(ITracingService tracingService, IWorkflowClientVariablesService workflowClientVariablesService)
        {
            _tracingService = tracingService;
            _workflowClientVariablesService = workflowClientVariablesService;
        }

        /// <summary>
        /// Pings this instance.
        /// </summary>
        /// <param name="serverConnectionConfiguration"></param>
        public void Ping(IServerConnectionConfiguration serverConnectionConfiguration)
        {
        }

        /// <summary>
        /// Create, Initialize, and Start a new Instance of a Workflow Process.
        /// <para>
        /// 		<para>
        /// An example of usage would be from a Drop folder Filewatcher event
        /// that needs to create a new file (in a Db or in SharePoint), and
        /// start a new workflow to process it.
        /// </para>
        /// Code Usage:
        /// <![CDATA[
        /// void FileWatcherEventHandler (object o, EventArgs e){
        /// //Move the file to a destination (eg db)...
        /// //var loanRequest = ....saved in Db....
        /// //Create an list of key/values to pass to the workflow right
        /// //after it has been started:
        /// var processVariables = new List<KeyValuePair<string, string>>();
        /// //namely, the Id of the record set.
        /// processVariables.Add(new KeyValuePair<string, string>(REQUEST_ID_WF_VAR, loanRequest.Id.ToString()));
        /// //Start a new workflow, and pass the vars it will need, all in one go:
        /// ServiceLocatorService.Current
        /// .GetInstance<IWorkflowClientInstanceService>().
        /// .CreateAndStartNewProcessInstance(
        /// "258SPIKES",
        /// "LoanRequest_{0} ({1})".FormatStringCurrentCulture(loanRequest.Id);DateTime.Now.ToString(),
        /// "Datacom.Demos.D07.K2WF08\DemoBank2", //Path to WF as is visible on K2 Server.
        /// processVariables);
        /// //At this point, WF is now being processed by K2 WF server,
        /// //and it will proceed as programmed (for example, the next place
        /// //you might hear of this WF, is as an an email received,
        /// //alerting of a Client event to handle at a specific website).
        /// }
        /// ]]>
        /// 	</para>
        /// </summary>
        /// <param name="workflowInstanceTag"></param>
        /// <param name="processPath"></param>
        /// <param name="dataFields">An optional set of variables to initialize the workflow with before starting it.</param>
        public void CreateAndStartNewProcessInstance(string processPath, string workflowInstanceTag, IEnumerable<KeyValuePair<string, object>> dataFields = null)
        {
            StringBuilder sb = new StringBuilder();
            if (dataFields != null)
            {
                foreach (KeyValuePair<string, object> pair in dataFields)
                {
                    sb.Append("{0}={1},".FormatStringCurrentUICulture(pair.Key, pair.Value));

                    _workflowClientVariablesService.SetProcessInstanceVariables(0.ToString(),dataFields);
                }
                sb.Remove(sb.Length - 1, 1);
            }

            _tracingService.Trace(TraceLevel.Info,
                                  "IWorkflowClientService.CreateAndStartNewProcessInstance(Folio:{1}, Values:{2}"
                                      .FormatStringCurrentUICulture("XXX", processPath, sb.ToString()));

        }

        /// <summary>
        /// Invokes the specific Action reponse of a Client Workflow Event.
        /// <para>
        /// For example, response with "Accept" or "Cancel" (the names of the outgoing lines from a Client Event).
        /// </para>
        /// </summary>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).</param>
        /// <param name="action">The action.</param>
        public void InvokeProcessInstanceEventOutgoingAction(string serialNumber, string action)
        {
            _tracingService.Trace(TraceLevel.Info,
                                  "IWorkflowClientService.CreateAndStartNewProcessInstance(Server:{0}, serialNumber:{1}, action:{2}"
                                      .FormatStringCurrentUICulture("XXX", serialNumber, action));
        }


        /// <summary>
        /// Gets the DataFields of a Workflow Process Instance.
        /// <para>
        /// Retrieves the list of all datafields for the specific process.
        /// <code>
        /// 			<![CDATA[
        /// var serialNumberOfCurrentEventItem =
        /// ((SourceCode.KO.ServerEventContext)k2workflowContext).SerialNumber;
        /// Dictionary<string,object> values = ServiceLocatorService.Current
        ///          .GetInstance<IWorkflowClientService>().
        ///          .GetProcessInstanceVariables(
        ///            "258SPIKES",
        ///            "Datacom.Demos.D07.K2WF08\DemoBank2" //Path to WF as is visible on K2 Server.
        /// );
        /// //Prove that one has a value returned (eg: a db record id):
        /// Assert.IsNotNull(values[REQUEST_ID_WF_VAR]);
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </summary>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).</param>
        /// <param name="variableKeys">The variable keys.</param>
        /// <returns></returns>
        public Dictionary<string, object> GetProcessInstanceVariables(string serialNumber, IEnumerable<string> variableKeys = null)
        {
            Dictionary<string, object> results = new Dictionary<string, object>();

            if (variableKeys == null)
            {
                MockWorkflowClientVariablesService tmp =
                    _workflowClientVariablesService as MockWorkflowClientVariablesService;

                if (tmp != null)
                {
                    foreach (KeyValuePair<string, object> pair in tmp.Settings)
                    {
                        results[pair.Key] = pair.Value;
                    }
                }
            }
            else
            {
                foreach (string key in variableKeys)
                {
                    results[key] = _workflowClientVariablesService.GetMemberValue(key);
                }
            }
            return results;
        }

        /// <summary>
        /// Sets the process instance variables.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="dataFields">The data fields.</param>
        public void SetProcessInstanceVariables(string serialNumber, IEnumerable<KeyValuePair<string, object>> dataFields = null)
        {
            if (dataFields != null)
            {
                foreach (KeyValuePair<string, object> pair in dataFields)
                {
                    _workflowClientVariablesService.SetMemberValue(pair.Key, pair.Value);
                }
            }
        }
    }


}