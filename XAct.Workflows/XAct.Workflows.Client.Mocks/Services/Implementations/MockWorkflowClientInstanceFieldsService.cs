﻿namespace XAct.Workflows.Services.Implementations
{
    using System.Collections;
    using System.Collections.Generic;
    using XAct.Services;
    using XAct.State;

    /// <summary>
    /// Mock implementation of <see cref="IWorkflowClientVariablesService"/>
    /// that stores values in the underlying <see cref="IContextStateService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IWorkflowClientVariablesService), BindingLifetimeType.Undefined, Priority.Low /*OK:SecondaryBinding*/)]
    public class MockWorkflowClientVariablesService : IWorkflowClientVariablesService
    {
        private readonly IContextStateService _contextStateService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockWorkflowClientVariablesService"/> class.
        /// </summary>
        /// <param name="contextStateService">The context state service.</param>
        public MockWorkflowClientVariablesService(IContextStateService contextStateService)
        {
            _contextStateService = contextStateService;
        }

        /// <summary>
        /// Gets the settings.
        /// </summary>
        public IDictionary Settings
        {
            get { return _contextStateService.Items; }
        }

        /// <summary>
        /// Gets the DataFields of a Workflow Process Instance.
        /// <para>
        /// Retrieves the list of all datafields for the specific process.
        /// <code>
        /// 			<![CDATA[
        /// var serialNumberOfCurrentEventItem =
        /// ((SourceCode.KO.ServerEventContext)k2workflowContext).SerialNumber;
        /// Dictionary<string,object> values =
        /// ServiceLocatorService.Current
        /// .GetInstance<IWorkflowClientVariablesService>().
        /// .GetProcessInstanceVariables(
        /// "258SPIKES",
        /// "Datacom.Demos.D07.K2WF08\DemoBank2" //Path to WF as is visible on K2 Server.
        /// );
        /// //Prove that one has a value returned (eg: a db record id):
        /// Assert.IsNotNull(values[REQUEST_ID_WF_VAR]);
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </summary>
        /// <param name="serialNumber">The serial number (received from the Workflow via a Client Event).</param>
        /// <param name="variableKeys">The variable keys.</param>
        /// <returns></returns>
        public Dictionary<string, object> GetProcessInstanceVariables(string serialNumber, IEnumerable<string> variableKeys = null)
        {
            Dictionary<string,object> results = new Dictionary<string, object>();

            if (variableKeys != null)
            {
                    return results;
            }
            foreach(string key in variableKeys)
            {
                results[key] = _contextStateService.Items[key];
            }
            return results;
        }

        /// <summary>
        /// Sets the process instance variables.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="dataFields">The data fields.</param>
        public void SetProcessInstanceVariables(string serialNumber, IEnumerable<KeyValuePair<string, object>> dataFields = null)
        {
            if (dataFields == null)
            {
                return;
            }
            foreach(KeyValuePair<string,object> kvp in dataFields)
            {
                _contextStateService.Items[kvp.Key] = kvp.Value;
            }
        }
    }
}
