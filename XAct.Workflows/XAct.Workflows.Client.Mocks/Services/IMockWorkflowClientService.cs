namespace XAct.Workflows.Mocks
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMockWorkflowClientService : IWorkflowClientControlService, IHasXActLibService{}
}