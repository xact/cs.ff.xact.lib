namespace XAct.Languages.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Languages.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IJurassicJavaScriptScriptHostServiceTests
    {


        public class T
        {
            public string Prop { get; set; }
            public string DoSomething()
            {
                return "Hi...";
            }
            public string DoSomething(string name)
            {
                return "Hi " + name;
            }
        }



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

            ////run once before any tests in this testfixture have been run...
            ////...setup vars common to all the upcoming tests...
            //IoCBootStrapper.Initialize(
            //    true
            ////    new XAct.Services.BindingDescriptor<IJurassicJavaScriptScriptHostService,JurassicJavaScriptScriptHostService>(),
            ////    new XAct.Services.BindingDescriptor<IScriptHostService,JurassicJavaScriptScriptHostService>()

            //    );

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanInstantiateScriptHostService()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();


            Assert.IsNotNull(scriptHostService);
        }

        [Test]
        public void CanInstantiateScriptHostServiceOfExpectedType()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();


            Assert.AreEqual(typeof(ScriptHostService), scriptHostService.GetType());
        }

        [Test]
        public void CanInstantiateScriptHost()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.JavaScript);

            Assert.IsNotNull(scriptHost);
        }

        [Test]
        public void CanInstantiateScriptHostOfExpectedType()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.JavaScript);

            Assert.AreEqual(typeof(JurassicJavaScriptScriptHost), scriptHost.GetType());
        }
        

        [Test]
        public void CanExecuteSettingUpAVarAndEvaluateItBackToHost()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.JavaScript);

            scriptHost.Evaluate("var i=1;");
            scriptHost.Evaluate<int>("i++;");
            int result = scriptHost.Evaluate<int>("i;");

            Assert.AreEqual(result,2);
        }





        [Test]
        [ExpectedException]
        public void CanInstantiaetScriptHostAndCreateAGlobalVariableWithoutException_BUT_FAIL_GETTING_VALUE()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IHostInteractiveScriptHost scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.JavaScript);

            var t = new T();
            scriptHost.RegisterHostObject("T", t);

            //The following will always fail because for Jurassic to be able to 'see'
            //a C# object, the C# object has to derive from using Jurassic.Library.ObjectInstance
            scriptHost.Execute("T.Prop = '123';");
            string result = scriptHost.Evaluate<string>("t.DoSomething('John')");
            Assert.AreEqual(result, "Hi John");
        }




    
    
    }


}


