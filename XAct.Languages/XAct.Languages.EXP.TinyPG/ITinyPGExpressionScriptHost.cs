﻿namespace XAct.Languages
{
    /// <summary>
    /// Contract for an implementation of <see cref="IScriptHostService"/>,
    /// using Jurassic
    /// 
    /// </summary>
    public interface ITinyPGExpressionScriptHost : IExpressionScriptHost { }
}
