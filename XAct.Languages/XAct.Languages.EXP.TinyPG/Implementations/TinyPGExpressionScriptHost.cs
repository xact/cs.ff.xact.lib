// ReSharper disable CheckNamespace
namespace XAct.Languages
// ReSharper restore CheckNamespace
{
    using System;
    using System.IO;
    using System.Reflection;
    using XAct.Diagnostics;
    using XAct.IO;
    using XAct.Languages.Exp;
    using XAct.Services;

    /// <summary>
    ///   An XAct.Languages.JS.Jurassic class.
    /// </summary>
    [DefaultBindingImplementation(typeof(IScriptHost), BindingLifetimeType.TransientScope, Priority.Normal /*OK: An Override Priority*/, "Expression")]
    [DefaultBindingImplementation(typeof(IExpressionScriptHost),BindingLifetimeType.TransientScope,Priority.Low)]
    [DefaultBindingImplementation(typeof(ITinyPGExpressionScriptHost),BindingLifetimeType.TransientScope,Priority.Low)]
    public class TinyPGExpressionScriptHost : ITinyPGExpressionScriptHost
    {
        private readonly ITracingService _tracingService;
        private readonly IIOService _ioService;


        private Exp.Context _scriptEngine;



        /// <summary>
        /// Gets the current engine.
        /// </summary>
        public T GetInnerItem<T>()
        {
            return _scriptEngine.ConvertTo<T>();
        }

        /// <summary>
        /// Gets the latest error message.
        /// </summary>
        public string LatestErrorMessage
        {
            get
            {
                return string.Empty;
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="TinyPGExpressionScriptHost"/> class.
        /// </summary>
        public TinyPGExpressionScriptHost(ITracingService tracingService,IIOService ioService)
        {
            _tracingService = tracingService;
            _ioService = ioService;

            //Create a new Script Engine:
            Reset();
        }


        /// <summary>
        /// Resets the memory space.
        /// </summary>
        public void Reset()
        {
            _scriptEngine = new Exp.Context();


        }

        /// <summary>
        /// Registers the assembly in which the Types are defined.
        /// </summary>
        /// <param name="types">The types.</param>
        public void RegisterAssemblies(params Type[] types)
        {

            _tracingService.Trace(TraceLevel.Verbose,
                                    "JurassicJavaScriptScriptHost.RegisterAssembly(): Ignored as .NET Assemblies cannot be registered in Javascript.");
        }


        /// <summary>
        /// Not Implemented in JavaScript Script Host.
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        public void RegisterAssemblies(params Assembly[] assemblies)
        {
            _tracingService.Trace(TraceLevel.Verbose, "JurassicJavaScriptScriptHost.RegisterAssemblies(): Ignored as .NET Assemblies cannot be registered in Javascript.");
        }

        /// <summary>
        /// Registers the namespaces in the script engine's memory space.
        /// <para>
        /// Not supported in all languages.
        /// </para>
        /// </summary>
        /// <param name="namespaces">The namespaces.</param>
        public void RegisterNamespaces(params string[] namespaces)
        {
            _tracingService.Trace(TraceLevel.Verbose,
                                  "JurassicJavaScriptScriptHost.RegisterNamespaces(): Ignored as .NET Namespaces cannot be registered in Javascript.");
        }


        /// <summary>
        /// Sets the variable.
        /// <para>
        /// <para>
        /// <example>
        /// <![CDATA[
        /// A a = new A(); a.B.C.Name = "Test";
        /// _service.SetVariable("target",a);
        /// 
        /// //Prefere retrieving values using Evaluate:
        /// _service.Evaluate("target",  "target.B.C.Name");
        /// //output:"Test";
        /// //as 
        /// var result = _service.GetVariable("target");
        /// //output: a jurassic object
        /// var result = _service.GetVariable("targetB.C.Name");
        /// //output: null
        /// ]]>
        /// </example>
        /// </para>
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void SetGlobalVariable<TValue>(string key, TValue value)
        {

            //_scriptEngine.CurrentScope[key] = value;

            Execute("{0}={1}".FormatStringInvariantCulture(key, value));

        }


        /// <summary>
        /// Gets a global variable.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>A untyped object.</returns>
        public object GetGlobalVariable(string key)
        {
            //return _scriptEngine.CurrentScope[key];
            
            return Evaluate(key);
        }

        /// <summary>
        /// Gets a global variable.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns>A typed value.</returns>
        public TValue GetGlobalVariable<TValue>(string key)
        { 
            return Evaluate(key).ConvertTo<TValue>();
            
        }


        /// <summary>
        /// Executes the specified statement, returning nothing.
        /// </summary>
        /// <param name="statement">The statement.</param>
        public void Execute(string statement)
        {
            try
            {
                Exp.Expression e = new Expression(_scriptEngine);

                if (e.Errors.Count > 0)
                {
                    throw new Exception("Parse Error:" + e.Errors[0].Message);
                }
                
                //Execute:
                e.Eval<object>(statement);

                if (e.Errors.Count > 0)
                {
                    throw new Exception("Execution Error:" + e.Errors[0].Message);
                }
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }

        /// <summary>
        /// Executes the script found in the given file.
        /// </summary>
        /// <param name="fileFullName">Full name of the file.</param>
        /// <exception cref="System.Exception"></exception>
        public void ExecuteFile(string fileFullName)
        {
            try
            {
                string statement = _ioService.FileOpenReadAsync(fileFullName).WaitAndGetResult().ReadToEnd();

                Execute(statement);
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }

        /// <summary>
        /// Evaluates the specified statement returning an untyped value.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <returns></returns>
        public object Evaluate(string statement)
        {
            try
            {
                Exp.Expression e = new Expression(_scriptEngine);

                if (e.Errors.Count > 0)
                {
                    throw new Exception("Parse Error:" + e.Errors[0].Message);
                }
                
                //Execute:

                // Note that Eval() always returns an object that can be of various types
                // you will need to check for the type yourself
                object result = e.Eval<object>(statement);

                if (e.Errors.Count > 0)
                {
                    throw new Exception("Execution Error:" + e.Errors[0].Message);
                }

                return result;

            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }


        /// <summary>
        /// Evaluates the specified statement, returning a typed value.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <returns></returns>
        public TResult Evaluate<TResult>(string statement)
        {
            try
            {
                return Evaluate(statement).ConvertTo<TResult>();
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }















    }
}