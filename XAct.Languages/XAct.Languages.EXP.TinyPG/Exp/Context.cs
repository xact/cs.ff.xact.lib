﻿namespace XAct.Languages.Exp
{
    using System.Collections.Generic;

    public class Context
    {
        // contains a list of stackVariables that is in scope. Scope is used only for DynamicFunctions (for now)
        private List<Variables> _stackVariables;


        public static Context Default
        {
            get { return _defaultContext ?? (_defaultContext = new Context()); }
        }
        private static Context _defaultContext;

        // list of functions currently in scope during an evaluation
        // note that this typically is NOT thread safe.

        public Functions Functions { get; private set; }
        public Variables Globals { get; private set; }

        //public CommandPrompt Console { get; set; }
        /// <summary>
        /// check current stacksize
        /// is used for debugging purposes and error handling
        /// to prevent stackoverflows
        /// </summary>
        public int CurrentStackSize
        {
            get
            {
                return _stackVariables.Count;
            }
        }

        public Variables CurrentScope
        {
            get
            {
                return _stackVariables.Count <= 0 ? null : _stackVariables[_stackVariables.Count-1];
            }
        }





        public Context()
        {
            Reset();
        }



        public void PushScope(Variables stackVariables)
        {
            _stackVariables.Add(stackVariables);
        }

        public Variables PopScope()
        {
            if (_stackVariables.Count <= 0)
            {
                return null;
            }

            Variables vars = _stackVariables[_stackVariables.Count-1];
            _stackVariables.RemoveAt(_stackVariables.Count - 1);

            return vars;
        }


        /// <summary>
        /// resets the context to its defaults
        /// </summary> 
        public void Reset()
        {
            _stackVariables = new List<Variables>();
            Globals = new Variables();
            Functions = new Functions();

            Functions.InitDefaults();
            
            Globals["Pi"] = 3.1415926535897932384626433832795; // Math.Pi is not very precise
            Globals["E"] = 2.7182818284590452353602874713527;  // Math.E is not very precise either
        }
    }
}
