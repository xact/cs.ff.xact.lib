﻿namespace XAct.Languages.Exp
{
    using System;

    public class Expression
    {
        private readonly string _expression;
        private readonly Context _context;
        readonly Parser _parser;
        private ParseTreeEvaluator _parseTreeEvaluator;



        ///
        public ParseErrors Errors
        {
            get
            {
                return _parseTreeEvaluator != null ? _parseTreeEvaluator.Errors : new ParseErrors();
            }
        }


        public static object Evaluate(string expression)
        {
            return Expression.Evaluate<object>(expression);
        }

        public static T Evaluate<T>(string expression)
        {
            object result = null;
            try
            {
                Expression exp = new Expression(expression);

                result = exp._parseTreeEvaluator.Errors.Count > 0 ? exp._parseTreeEvaluator.Errors[0].Message : exp.Eval();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result != null ? ((T)(result)) : default(T);
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="Expression" /> class.
        /// </summary>
        /// <param name="exp">The exp.</param>
        /// <param name="context">The context.</param>
        private Expression(string exp, Context context = null)
        {
            if (context == null)
            {
                context = Context.Default;
            }

            _context = context;
            _expression = exp;

            Scanner scanner = new Scanner();
            _parser = new Parser(scanner);

        }

        public Expression(Context context = null)
        {
            if (context == null)
            {
                context = Context.Default;
            }

            _context = context;

            Scanner scanner = new Scanner();
            _parser = new Parser(scanner);

        }

        public object Eval()
        {
            return Eval<object>(_expression);
        }


        public T Eval<T>(string expression)
        {
            if (_context.CurrentStackSize > 0)
            {
                Errors.Add(new ParseError("Stacksize is not empty", 0, null));
            }

            _parseTreeEvaluator = new ParseTreeEvaluator(_context);
            _parseTreeEvaluator = _parser.Parse(expression, _parseTreeEvaluator) as ParseTreeEvaluator;


            object result = _parseTreeEvaluator.Eval(null);

            return result != null ? ((T)(result)) : default(T);

        }

    }
}
