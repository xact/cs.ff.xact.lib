﻿namespace XAct.Languages
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration of known Script engines.
    /// </summary>
    [DataContract]
    public enum ScriptHostType
    {
        /// <summary>
        /// <para>
        /// Value = 0
        /// </para>
        /// <para>
        /// An error state.
        /// </para>
        /// </summary>
        [EnumMember]
        Unspecified = 0,

        /// <summary>
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        Unrecognized = -1,

        /// <summary>
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        JavaScript = 1,
        /// <summary>
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        [EnumMember]
        CSharp = 2,

        /// <summary>
        /// Simple Expressions
        /// <value>
        /// Value = 3
        /// </value>
        /// </summary>
        [EnumMember]
        Expressions = 3,
    }
}
