namespace XAct.Languages
{
    /// <summary>
    /// A specialization of <see cref="IScriptHostService"/>
    /// </summary>
    public interface ICSharpScriptHost : IScriptHost
    {
        
    }
}