﻿

namespace XAct.Languages
{
    /// <summary>
    /// 
    /// </summary>
    public interface IJavaScriptHost : IScriptHost, IHasTransientBindingScope
    {
    }
}
