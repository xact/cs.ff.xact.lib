namespace XAct.Languages
{
    using System;
    using System.Reflection;


    /// <summary>
    /// Methods for the Host application (eg, the compiled .NET application)
    /// to interact with the script (eg: share common objects).
    /// </summary>
    public interface IHostInteractiveScriptHost : IScriptHost
    {
        /// <summary>
        /// Registers the given assemblies in the script engines memory space.
        /// <para>
        /// IMPORTANT: Not supported in all languages (more of a language .NET concern).
        /// </para>
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        void RegisterAssemblies(params Assembly[] assemblies);

        /// <summary>
        /// Registers the assemblies in which the <see cref="System.Type"/>s are defined.
        /// <para>
        /// IMPORTANT: Not supported in all languages (more of a language .NET concern).
        /// </para>
        /// </summary>
        /// <param name="types">The types.</param>
        void RegisterAssemblies(params Type[] types);



        /// <summary>
        /// Registers the namespaces (ie, 'using') in the script engine's memory space
        /// <para>
        /// With C# scripts, this is the equivalent of prepending scripts with 'using SomeNamespace;'
        /// in order Types referred in subsequent execution statements do not have to be declared with FullName or FQN. 
        /// <code>
        /// <![CDATA[
        /// using SomeNamespace;
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// IMPORTANT: Not supported in all languages (more of a .NET language concern).
        /// </para>
        /// </summary>
        /// <param name="namespaces">The namespaces.</param>
        void RegisterNamespaces(params string[] namespaces);




        /// <summary>
        /// Sets a script environment global (root) variable referencing a Host environment object shared between the two environments.
        /// <para>
        /// Not supported in all languages.
        /// </para>
        /// <para>
        /// C# example:
        /// <code>
        /// <![CDATA[
        /// var foo = new Foo();
        /// service.RegisterHostObject("foo", foo);
        /// service.Execute("foo.SomeProp = 123;");
        /// foo.SomeProp.Dump(); // displays '123'...
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// var foo = new Foo();
        /// service.RegisterHostObject("foo", foo);
        /// service.Execute("foo.SomeProp = 123;");
        /// foo.SomeProp.Dump(); // displays '123'...
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the host environment object.</typeparam>
        /// <param name="key">The variable name to use in the script environment.</param>
        /// <param name="value">The host environment object.</param>
        void RegisterHostObject<TValue>(string key, TValue value);
        
    }

    /// <summary>
    /// Contract for a ScriptHost to process a language.
    /// </summary>
    public interface IScriptHost: XAct.IHasInnerItemReadOnly
    {
//new object InnerItem { get; }


        /// <summary>
        /// Gets the latest error message.
        /// </summary>
        string LatestErrorMessage { get; }

        /// <summary>
        /// Resets the memory space.
        /// </summary>
        void Reset();



        /// <summary>
        /// Executes the specified statement, returning nothing.
        /// <para>
        /// C# Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// // Before commencing execution, remember to Include required namespaces:
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// ...
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// scriptHost.Execute("var bar = new TestObject{Name=\"Joe\"};");
        /// ...
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// TestObject r1 = scriptHost.Evaluate<TestObject>("bar");
        /// string r3 = scriptHost.Evaluate<string>("bar.Name");        
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// ...
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="statement">The statement.</param>
        void Execute(string statement);

        
        /// <summary>
        /// Evaluates the specified statement returning an untyped value.
        /// <para>
        /// C# Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// // Before commencing execution, remember to Include required namespaces:
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// 
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// scriptHost.Execute("var bar = new TestObject{Name=\"Joe\"};");
        /// 
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// TestObject r1 = scriptHost.Evaluate<TestObject>("bar");
        /// string r3 = scriptHost.Evaluate<string>("bar.Name");        
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// ...
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <returns>An object.</returns>
        object Evaluate(string statement);




        /// <summary>
        /// Evaluates the specified statement, returning a typed value.
        /// <para>
        /// C# Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// // Before commencing execution, remember to Include required namespaces:
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// 
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// scriptHost.Execute("var bar = new TestObject{Name=\"Joe\"};");
        /// 
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// TestObject r1 = scriptHost.Evaluate<TestObject>("bar");
        /// string r3 = scriptHost.Evaluate<string>("bar.Name");        
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// ...
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <returns>A typed object.</returns>
        TResult Evaluate<TResult>(string statement);


        /// <summary>
        /// Evaluates the script found in the given file.
        /// <para>
        /// C# Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// // Before commencing execution, remember to Include required namespaces:
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// 
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// scriptHost.Execute("var bar = new TestObject{Name=\"Joe\"};");
        /// 
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// TestObject r1 = scriptHost.Evaluate<TestObject>("bar");
        /// string r3 = scriptHost.Evaluate<string>("bar.Name");        
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// </summary>
        /// <param name="fileFullName">The file info.</param>
        /// <returns>A typed object.</returns>
        void ExecuteFile(string fileFullName);




    }
}