namespace XAct.Languages
{
    /// <summary>
    /// A Service to create a Host for a scripting language.
    /// </summary>
    public interface IScriptHostService : IHasXActLibService
    {
        /// <summary>
        /// Creates the specified script host.
        /// </summary>
        /// <param name="scriptHostType">Type of the script host.</param>
        /// <param name="reusePreviousHostIfAvailable">if set to <c>true</c> return the same <see cref="IScriptHost"/> as previously issued (in same thread).</param>
        /// <returns></returns>
        IScriptHost CreateScriptHost(ScriptHostType scriptHostType, bool reusePreviousHostIfAvailable=false);

        /// <summary>
        /// Creates the specified script host, as a host capable of 
        /// registering Host assemblies and types. Not all script hosts can, hence the two means of instantiating script hosts.
        /// </summary>
        /// <param name="scriptHostType">Type of the script host.</param>
        /// <param name="reusePreviousHostIfAvailable">if set to <c>true</c> return the same <see cref="IScriptHost"/> as previously issued (in same thread).</param>
        /// <returns></returns>
        IHostInteractiveScriptHost CreateHostInteractiveScriptHost(ScriptHostType scriptHostType, bool reusePreviousHostIfAvailable = false);

    }
}