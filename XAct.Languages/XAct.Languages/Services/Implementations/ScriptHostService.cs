namespace XAct.Languages.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IScriptHostService"/>
    /// </summary>
    public class ScriptHostService : IScriptHostService, IHasWebThreadBindingScope
    {
        private readonly ITracingService _tracingService;

        private IScriptHost _scriptHost;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScriptHostService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ScriptHostService(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }


        /// <summary>
        /// Creates the specified script host.
        /// </summary>
        /// <param name="scriptHostType">Type of the script host.</param>
        /// <param name="reusePreviousHostIfAvailable">if set to <c>true</c> return the same <see cref="IScriptHost"/> as previously issued (in same thread).</param>
        /// <returns></returns>
        public IScriptHost CreateScriptHost(ScriptHostType scriptHostType, bool reusePreviousHostIfAvailable=false)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Creating a ScriptHost of type '{0}'",scriptHostType);

            if ((reusePreviousHostIfAvailable)&&(_scriptHost!=null))
            {
                return _scriptHost;
            }


            switch (scriptHostType)
            {
                case ScriptHostType.JavaScript:
                    _scriptHost = DependencyResolver.Current.GetInstance<IJavaScriptHost>();
                    break;
                case ScriptHostType.CSharp:
                    _scriptHost = DependencyResolver.Current.GetInstance<ICSharpScriptHost>();
                    break;
                case ScriptHostType.Expressions:
                    _scriptHost = DependencyResolver.Current.GetInstance<IExpressionScriptHost>();
                    break;
                default:
// ReSharper disable LocalizableElement
                    throw new ArgumentOutOfRangeException("scriptHostType", "Unrecognized ScriptHostType");
// ReSharper restore LocalizableElement
            }
            return _scriptHost;
        }


        /// <summary>
        /// Creates the specified script host.
        /// </summary>
        /// <param name="scriptHostType">Type of the script host.</param>
        /// <param name="reusePreviousHostIfAvailable">if set to <c>true</c> return the same <see cref="IScriptHost"/> as previously issued (in same thread).</param>
        /// <returns></returns>
        public IHostInteractiveScriptHost CreateHostInteractiveScriptHost(ScriptHostType scriptHostType,
                                                                   bool reusePreviousHostIfAvailable = false)
        {
            IScriptHost result = CreateScriptHost(scriptHostType, reusePreviousHostIfAvailable);


            return (IHostInteractiveScriptHost) result;
        }

    }
}