﻿namespace XAct.Languages
{
    using Irony.Interpreter;
    using Irony.Interpreter.Evaluator;
    using Irony.Parsing;

    /// <summary>
    /// 
    /// </summary>
    [Language("ExtendedExpressionEvaluatorGrammar", "1.1", "Multi-line expression evaluator")]
    public class ExtendedExpressionEvaluatorGrammar : ExpressionEvaluatorGrammar
    {

        /// <summary>
        /// Creates the runtime.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public override LanguageRuntime CreateRuntime(LanguageData language)
        {
            return new ExtendedExpressionEvaluatorRuntime(language);
        }
    }
}

