namespace XAct.Languages
{
    using System;
    using Irony.Interpreter;
    using Irony.Interpreter.Evaluator;
    using Irony.Parsing;

    /// <summary>
    /// 
    /// </summary>
    public class ExtendedExpressionEvaluatorRuntime : ExpressionEvaluatorRuntime
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedExpressionEvaluatorRuntime" /> class.
        /// </summary>
        /// <param name="language">The language.</param>
        public ExtendedExpressionEvaluatorRuntime(LanguageData language)
            : base(language)
        {
        }



        /// <summary>
        /// Inits this instance.
        /// </summary>
        public override void Init()
        {
            base.Init();

            //add built-in methods:
            BuiltIns.AddMethod(BuiltInContains, "contains");

        }


        #region Function implementations
        private object BuiltInContains(ScriptThread thread, object[] args)
        {


            if (args == null || args.Length < 2)
            {
                throw new Exception("Contains method requires atleast 2 arguments.");
            }
            
            //Prtty piggish...

            //I don't know what would be expected best practice to handle routing based on type of var
            //ie, contains in array, or contains in string....

            string text = args[0] as string;
            string criteria = args[1] as string;


            return (text.IndexOf(criteria, StringComparison.CurrentCultureIgnoreCase) > -1);
        }
        #endregion


    }
}