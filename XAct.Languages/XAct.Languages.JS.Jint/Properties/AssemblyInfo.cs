using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
// associated with an assembly.

[assembly: AssemblyTitle("XAct.Languages.JS.Jint")]
[assembly: AssemblyDescription("An XActLib assembly: OBSOLETE: Prefer using XAct.Languages.JS.ClearScript")]
#if DEBUG
[assembly: AssemblyConfiguration("DEBUG")]
#else 
[assembly: AssemblyConfiguration("")]
#endif

[assembly: AssemblyCompany("XAct Software Solutions, Inc.")]
[assembly: AssemblyProduct("XActLib")]
[assembly: AssemblyCopyright("Copyright © XAct Software Solutions, Inc. 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

//PLC: [assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

//PLC: [assembly: Guid("ecc6676d-471f-426c-b349-a27804b0ef47")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

//Begin:SIGNED
#if SIGNED
//Allow this signed assembly access from Partially trusted: http://bit.ly/vyFFTu
[assembly: System.Security.AllowPartiallyTrustedCallers] //Only If Signed
#if NET40
//When Ninject3 running in a .NET40 environment starts causing errors due to demanding
//more precise security. Can't do it, so added this:
[assembly: System.Security.SecurityRules(System.Security.SecurityRuleSet.Level1)] //Only If Signed
//See note in NinjectMvcBootstrapper regarding Security.Critical use of IKernel
#endif
#endif
//End:SIGNED
