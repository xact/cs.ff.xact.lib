namespace XAct.Languages
{
    using System;
    using System.IO;
    using System.Reflection;
    using Mono.CSharp;
    using XAct.Diagnostics;
    using XAct.IO;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="ICSharpScriptHost"/>
    /// to create a CSharp script processing context.
    /// </summary>
    [DefaultBindingImplementation(typeof (IScriptHost), BindingLifetimeType.TransientScope, Priority.VeryLow, "CSharp")]
    [DefaultBindingImplementation(typeof (ICSharpScriptHost), BindingLifetimeType.TransientScope, Priority.Low)]
    [DefaultBindingImplementation(typeof(IMonoCSharpScriptHost), BindingLifetimeType.TransientScope, Priority.Low)]
    public class MonoCSharpScriptHost : IMonoCSharpScriptHost, IHasTransientBindingScope
    {
        private readonly ITracingService _tracingService;
        private readonly IIOService _ioService;

        private StringWriter _reportWriter;
        private CompilerContext _compilerContext;


        /// <summary>
        /// Gets the compiler settings.
        /// </summary>
        public static CompilerSettings CompilerSettings
        {
            get { return _compilerSettings; }
        }

        private static CompilerSettings _compilerSettings;

        /// <summary>
        /// Gets the report.
        /// </summary>
        public Report Report
        {
            get { return _report; }
        }

        private Report _report;

        /*
         * CompilerSettings compilerSettings = new CompilerSettings();
            compilerSettings.LoadDefaultReferences = true;
            Report report = new Report(new Mono.CSharp.ConsoleReportPrinter());

            Mono.CSharp.Evaluator e;

            e= new Evaluator(compilerSettings, report);

            //IMPORTANT:This has to be put before you include references to any assemblies
            //our you;ll get a stream of errors:
            e.Run("using System;");
            //IMPORTANT:You have to reference the assemblies your code references...
            //...including this one:
            e.Run("using XAct.Spikes.Duo;");

            //Go crazy -- although that takes time:
            //foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            //{
            //    e.ReferenceAssembly(assembly);
            //}
            //More appropriate in most cases:
            e.ReferenceAssembly((typeof(A).Assembly));

            //Exception due to no semicolon
            //e.Run("var a = 1+3");
            //Doesn't set anything:
            //e.Run("a = 1+3;");
            //Works:
            //e.ReferenceAssembly(typeof(A).Assembly);
            e.Run("var a = 1+3;");
            e.Run("A x = new A{Name=\"Joe\"};");

            var a  = e.Evaluate("a;");
            var x = e.Evaluate("x;");

            //Not extremely useful:
            string check = e.GetVars();

            //Note that you have to type it:
            Console.WriteLine(((A) x).Name);

            e = new Evaluator(compilerSettings, report);
            var b = e.Evaluate("a;");
         */

        /// <summary>
        /// Initializes a new instance of the <see cref="MonoCSharpScriptHost" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ioService">The io service.</param>
        public MonoCSharpScriptHost(ITracingService tracingService, IIOService ioService)
        {
            _tracingService = tracingService;
            _ioService = ioService;

            if (_compilerSettings == null)
            {
                _compilerSettings = new CompilerSettings();
                _compilerSettings.LoadDefaultReferences = true;


                //if (_environmentService.HttpContext == null)
                //{
                //    _report = 
                //        new Report(new Mono.CSharp.ConsoleReportPrinter());
                //}else
                //{
                //    _report = 
                //        new Report(new Mono.CSharp.ConsoleReportPrinter());                    
                //}

            
            }
            
            Reset();

        }

        /// <summary>
        /// Gets the current engine.
        /// </summary>
        public T GetInnerItem<T>()
        {
            return _evaluator.ConvertTo<T>();
        }

        /// <summary>
        /// Gets the latest error message.
        /// </summary>
        public string LatestErrorMessage
        {
            get
            {
                string result = _reportWriter.ToString();
                return result;
            }
        }

// ReSharper disable RedundantNameQualifier
        private Mono.CSharp.Evaluator _evaluator;
// ReSharper restore RedundantNameQualifier


        /// <summary>
        /// Resets the memory space.
        /// </summary>
        public void Reset()
        {
            _tracingService.Trace(TraceLevel.Verbose, "Creating a new ScriptEngine Evaluator");
            _reportWriter = new StringWriter();
            _compilerContext = new CompilerContext(CompilerSettings, new ConsoleReportPrinter());
            _report = new Report(_compilerContext, new StreamReportPrinter(_reportWriter));
            _evaluator = new Evaluator(_compilerContext);

            
            RegisterAssemblies(
                typeof(IScriptHost),
                this.GetType());

        }

        /// <summary>
        /// Registers the assembly in which the Types are defined.
        /// <para>
        /// Not supprted in all languages.
        /// </para>
        /// </summary>
        /// <param name="types">The types.</param>
        public void RegisterAssemblies(params Type[] types)
        {
            foreach(Type type in types)
            {
                Assembly assembly = type.Assembly;
                _tracingService.Trace(TraceLevel.Verbose, "Registering an Assembly: {0}", assembly.FullName);
                _evaluator.ReferenceAssembly(assembly);
            }
        }


        /// <summary>
        /// Registers the given assemblies in the script engines memory space.
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        public void RegisterAssemblies(params Assembly[] assemblies)
        {
            foreach (Assembly assembly in assemblies)
            {
                _tracingService.Trace(TraceLevel.Verbose, "Registering an Assembly: {0}", assembly.FullName);
                _evaluator.ReferenceAssembly(assembly);
            }
        }


        /// <summary>
        /// Registers the namespaces in the script engine's memory space.
        /// <para>
        /// Not supported in all languages.
        /// </para>
        /// </summary>
        /// <param name="namespaces">The namespaces.</param>
        public void RegisterNamespaces(params string[] namespaces)
        {


            if (namespaces == null)
            {
                return;
            }

            foreach (string usingNamespace in namespaces)
            {
                string statement = "using {0};".FormatStringInvariantCulture(usingNamespace);
                _evaluator.Run(statement);
            }
        }

        /// <summary>
        /// Registers the host object.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void RegisterHostObject<TValue>(string key, TValue value)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Setting a global variable: {0}={1}", key,value);
            try
            {
                string statement;
               
                //Check if argument is there:


                Type type = typeof (TValue);

                string typeFullName = type.FullNameHack();

                try
                {
                    //Create new var:
                    statement = "{1} {0}=default({1});".FormatStringInvariantCulture(
                        key, 
                        typeFullName);

                    _evaluator.Run(statement);
                }catch(System.ArgumentException argumentException)
                {
                    _tracingService.Trace(TraceLevel.Verbose, "First chance caught: " + argumentException.Message);
                    //Clear existing:
                    statement = "{0}=default({1});".FormatStringInvariantCulture(key, typeFullName);
                    _evaluator.Run(statement);
                }
                catch(Exception e)
                {
                    _tracingService.TraceException(TraceLevel.Warning, e,"Exception occurred when Setting global variable");
                    throw;
                }

                //Now that variable exists,
                //pass the value into it:
                //eg: assuming args are "ABC" and 123, then 
                //statement = "new Action<XAct.Languages.MonoCSharpScriptHostVariableBag>(b => ABC = (System.Int32)b.Item);"
                statement = "new Action<{2}>(b => {0} = ({1})b.Item);".FormatStringInvariantCulture(key, typeFullName, typeof(MonoCSharpScriptHostVariableBag).FullNameHack());

                Action<MonoCSharpScriptHostVariableBag> action = 
                    (Action<MonoCSharpScriptHostVariableBag>)_evaluator.Evaluate(statement);

                action(new MonoCSharpScriptHostVariableBag { Item = value });

                //At this point, the target property is set. Yahoo!
                //var check = _evaluator.Evaluate("{0};".FormatStringInvariantCulture(key));

            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                _tracingService.TraceException(TraceLevel.Warning, e,"A Error occurred within IScriptHost:{0}",e.Message);
                throw;
            }
        }

        /// <summary>
        /// Gets a global variable.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        /// A untyped object.
        /// </returns>
        public object GetGlobalVariable(string key)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Getting a global variable: {0}",key);
            try
            {
                return _evaluator.GetMemberValue(key);
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                _tracingService.TraceException(TraceLevel.Warning, e,"A Error occurred within IScriptHost:{0}",e.Message);
                throw;
            }
        }

        /// <summary>
        /// Gets the global variable.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public TValue GetGlobalVariable<TValue>(string key)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Getting a global variable: {0}", key);
            try
            {
                return _evaluator.GetMemberValue(key).ConvertTo<TValue>();
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                _tracingService.TraceException(TraceLevel.Warning, e,"A Error occurred within IScriptHost:{0}",e.Message);
                throw;
            }
        }

        /// <summary>
        /// Executes the specified statement, returning nothing.
        /// </summary>
        /// <param name="statement">The statement.</param>
        public void Execute(string statement)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Executing a script...");
            try
            {
                _evaluator.Run(statement);
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                _tracingService.TraceException(TraceLevel.Warning, e,"A Error occurred within IScriptHost:{0}",e.Message);
                throw;
            }
        }

        /// <summary>
        /// Evaluates the specified statement returning an untyped value.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <returns>
        /// An object.
        /// </returns>
        public object Evaluate(string statement)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Evaluationg a script...");
            try
            {
                return _evaluator.Evaluate(statement);
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                _tracingService.TraceException(TraceLevel.Warning, e,"A Error occurred within IScriptHost:{0}",e.Message);
                throw;
            }
        }

        /// <summary>
        /// Evaluates the specified statement.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <returns></returns>
        public TResult Evaluate<TResult>(string statement)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Evaluating a script...");
            try
            {
                var r = _evaluator.Evaluate(statement);

                TResult r2= r.ConvertTo<TResult>();
                return r2;
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                _tracingService.TraceException(TraceLevel.Warning, e,"A Error occurred within IScriptHost:{0}",e.Message);
                throw;
            }
        }


        /// <summary>
        /// Evaluates the specified file info.
        /// </summary>
        /// <param name="fileFullName">Full name of the file.</param>
        /// <returns></returns>
        public void ExecuteFile(string fileFullName)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Evaluting a script...from {0}", fileFullName);
            try
            {
                string statement = _ioService.FileOpenReadAsync(fileFullName).WaitAndGetResult().ReadToEnd();
 
                _evaluator.Run(statement);
                //.ConvertTo<TResult>();
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                _tracingService.TraceException(TraceLevel.Warning, e,"A Error occurred within IScriptHost:{0}",e.Message);
                throw;
            }
        }

    }
}