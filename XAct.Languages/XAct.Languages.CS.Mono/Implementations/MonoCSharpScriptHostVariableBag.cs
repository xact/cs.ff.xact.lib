namespace XAct.Languages
{
    /// <summary>
    /// FOR INTERNAL USE.
    /// </summary>
    /// <remarks>
    /// Cannot be made Internal, or ScriptHost can't see it.
    /// And T throws off script...
    /// </remarks>
    public class MonoCSharpScriptHostVariableBag
    {

        /// <summary>
        /// Gets or sets the item to transfer into ScriptHost.
        /// </summary>
        /// <value>
        /// The item.
        /// </value>
        public object Item { get; set; }
    }
}