namespace XAct.Languages
{
    /// <summary>
    /// Specialization of <see cref="ICSharpScriptHost"/>
    /// to provide a 
    /// </summary>
    public interface IMonoCSharpScriptHost : ICSharpScriptHost, IHostInteractiveScriptHost
    {

    }
}