namespace XAct.Languages.Tests
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Languages.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class ClearScriptJavaScriptScriptHostTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        
        [Test]
        public void CanInstantiateScriptService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            Assert.IsNotNull(service);
        }

        [Test]
        public void CanInstantiateScriptServiceOfTypeScriptHostService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            Assert.AreEqual(typeof(ScriptHostService), service.GetType());
        }

        [Test]
        public void CanInstantiateJScriptScriptHost()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = service.CreateScriptHost(ScriptHostType.JavaScript);

            Assert.AreEqual(typeof(ClearScriptJavaScriptScriptHost), scriptHost.GetType());
        }

        [Test]
        public void ScriptHostCanExecuteSimpleStatement()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = service.CreateScriptHost(ScriptHostType.JavaScript);

            scriptHost.Execute("var foo = 123;");


            Assert.IsTrue(true);
        }

        [Test]
        public void ScriptHostCanEvaluateSimpleStatement()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = service.CreateScriptHost(ScriptHostType.JavaScript);

            scriptHost.Execute("var foo = 123;");

            object result = scriptHost.Evaluate("foo");
            Assert.AreEqual(123,result);
        }

        [Test]
        public void ScriptHostCanEvaluateSimpleTypedStatement()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = service.CreateScriptHost(ScriptHostType.JavaScript);

            scriptHost.Execute("var foo = 123;");

            int result = scriptHost.Evaluate<int>("foo");
        
            Assert.AreEqual(123, result);
        }

        [Test]
        public void CanRegisterAssembly()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = service.CreateHostInteractiveScriptHost(ScriptHostType.JavaScript);

            scriptHost.RegisterAssemblies(typeof(System.Guid).Assembly);
            
            scriptHost.Execute("var d = new assemblies.System.DateTime();");

            object result = scriptHost.Evaluate("d");

            Assert.IsNotNull(result);
        }
        [Test]
        public void CanRegisterAssembliesMoreThanOnce()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = service.CreateHostInteractiveScriptHost(ScriptHostType.JavaScript);

            scriptHost.RegisterAssemblies(typeof(System.Guid).Assembly);
            scriptHost.RegisterAssemblies(typeof(System.Guid).Assembly,typeof(NUnit.Framework.Assert).Assembly);

            scriptHost.Execute("var d = new assemblies.System.DateTime();");

            object result = scriptHost.Evaluate("d");

            Assert.IsNotNull(result);
        }

    }
}


