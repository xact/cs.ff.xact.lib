namespace XAct.Languages
{
    using System;
    using System.IO;
    using XAct.Diagnostics;
    using XAct.IO;
    using XAct.Services;

    /// <summary>
    ///   An XAct.Languages.JS.Jurassic class.
    /// </summary>
    [DefaultBindingImplementation(typeof(IScriptHost),BindingLifetimeType.TransientScope,Priority.Normal /*OK: An Override Priority*/, "JavaScript")]
    [DefaultBindingImplementation(typeof(IJavaScriptHost),BindingLifetimeType.TransientScope,Priority.Normal /*OK: An Override Priority*/)]
    public class JurassicJavaScriptScriptHost : IJurassicJavaScriptScriptHost
    {
        private readonly ITracingService _tracingService;
        private readonly IIOService _ioService;

        /// <summary>
        /// Gets the current engine.
        /// </summary>
        public T GetInnerItem<T>()
        {
            return _scriptEngine.ConvertTo<T>();
        }

        /// <summary>
        /// Gets the latest error message.
        /// </summary>
        public string LatestErrorMessage
        {
            get
            {
                return string.Empty;
            }
        }

        private Jurassic.ScriptEngine _scriptEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="JurassicJavaScriptScriptHost"/> class.
        /// </summary>
        public JurassicJavaScriptScriptHost(ITracingService tracingService,IIOService ioService)
        {
            _tracingService = tracingService;
            _ioService = ioService;
            //Create a new Script Engine:
            Reset();
        }


        /// <summary>
        /// Resets the memory space.
        /// </summary>
        public void Reset()
        {
            //Study options here: 
            _scriptEngine = new Jurassic.ScriptEngine();

        }


        /// <summary>
        /// Sets a script environment global (root) variable referencing a Host environment object shared between the two environments.
        /// <para>
        /// Not supported in all languages.
        /// </para>
        /// <para>
        /// <example>
        /// <![CDATA[
        /// //Create a C# Object:
        /// A a = new A(); 
        /// //Set some (deeply nested) property within it:
        /// a.B.C.Name = "Test";
        /// 
        /// //Set a ref to it in the script host:
        /// _service.SetVariable("target",a);
        /// 
        /// //You can retrieve
        /// //Prefere retrieving values using Evaluate:
        /// var r = _service.Evaluate("target",  "target.B.C.Name");
        /// r.Dump();
        /// //output:"Test";
        ///
        /// 
        /// var result = _service.GetVariable("target");
        /// //output: a jurassic object
        /// var result = _service.GetVariable("targetB.C.Name");
        /// //output: null
        /// ]]>
        /// </example>
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the host environment object.</typeparam>
        /// <param name="key">The variable name to use in the script environment.</param>
        /// <param name="value">The host environment object.</param>
        public void SetGlobalVariable<TValue>(string key, TValue value)
        {
            //Using the objec raw:
            // Can't pull back sub properties with "target.X" (just target)
            //Using the serializer built into Jurassic
            // Same problem.
            //Also, the object it puts into the JS space is not exactly JS...it's Jurassic.

            //Whereas Json.NET sends across a good serialization of the object
            //therefore from the JS script, it's accessible with correct JS syntax
            //string jsonSerialization = Jurassic.Library.JSONObject.Parse(c, sss));

            string jsonSerialization = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            object jurassicJsObject = Jurassic.Library.JSONObject.Parse(_scriptEngine, jsonSerialization);

            _scriptEngine.SetGlobalValue(key, jurassicJsObject);
        }


        /// <summary>
        /// Gets a global variable.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>A untyped object.</returns>
        public object GetGlobalVariable(string key)
        {
            //If put in as a JSON object, returned as a JSON object:
            return _scriptEngine.GetGlobalValue(key);
        }

        /// <summary>
        /// Gets a global variable.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns>A typed value.</returns>
        public TValue GetGlobalVariable<TValue>(string key)
        {
            //If put in as a JSON object, returned as a JSON object:
            var result = GetGlobalVariable(key).ConvertTo<TValue>();
            return result;
        }


        /// <summary>
        /// Executes the specified statement, returning nothing.
        /// </summary>
        /// <param name="statement">The statement.</param>
        public void Execute(string statement)
        {
            try {
            _scriptEngine.Execute(statement);
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }

        /// <summary>
        /// Executes the script found in the given file.
        /// </summary>
        /// <param name="fileInfo">The file info.</param>
        public void Execute(FileInfo fileInfo)
        {
            try {
            _scriptEngine.ExecuteFile(fileInfo.FullName);
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }

        /// <summary>
        /// Evaluates the specified statement returning an untyped value.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <returns></returns>
        public object Evaluate(string statement)
        {
            try {
            return _scriptEngine.Evaluate(statement);
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }


        /// <summary>
        /// Evaluates the specified statement, returning a typed value.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <returns></returns>
        public TResult Evaluate<TResult>(string statement)
        {
            try {
            return _scriptEngine.Evaluate<TResult>(statement);
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }


        /// <summary>
        /// Evaluates the specified file info.
        /// </summary>
        /// <param name="fileFullName">The file info.</param>
        /// <returns></returns>
        public void ExecuteFile(string fileFullName)
        {
            try {
            string statement = _ioService.FileOpenReadAsync(fileFullName).WaitAndGetResult().ReadToEnd();
            Evaluate(statement);
                //.ConvertTo<TResult>();
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            {
                string message = this.LatestErrorMessage;
                _tracingService.Trace(TraceLevel.Warning, message);
                throw new Exception(message);
                //throw;
            }
        }

    }
}