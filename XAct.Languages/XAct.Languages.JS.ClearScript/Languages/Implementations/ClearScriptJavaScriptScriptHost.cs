﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XAct.Languages.Implementations
{
    using System.Reflection;
    using Microsoft.ClearScript;
    using Microsoft.ClearScript.V8;
    using XAct.Services;


    /// <summary>
    /// An implementation of the <see cref="IClearScriptJavaScriptScriptHost" />
    /// contract.
    /// </summary>

    [DefaultBindingImplementation(typeof(IScriptHost), BindingLifetimeType.TransientScope, Priority.VeryLow /*OK: An Override Priority*/, "JavaScript")]
    [DefaultBindingImplementation(typeof(IJavaScriptHost), BindingLifetimeType.TransientScope, Priority.Normal /*OK: An Override Priority*/)]
    public class ClearScriptJavaScriptScriptHost : IClearScriptJavaScriptScriptHost
    {
        /// <summary>
        /// Script environment variable name to use for referencing external assemblies.
        /// <para>
        /// The value is 'assemblies'
        /// </para>
        /// <code>
        /// <![CDATA[
        /// //Therefore, from JS:
        /// var x = new assemblies.System.Core.Guid.NewGuid();
        /// ]]>
        /// </code>
        /// </summary>
        public const string ExternalAssembliesKey = "assemblies";


        private V8ScriptEngine _engine;

        //List of assemblies to register under 'ExternalAssembliesKey':
        private List<Assembly> _registeredAssemblies = new List<Assembly>();

        //Flag set so we can ensure assemblies are registered prior to host objects being shared.
        private bool _hostObjectsRegistered;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="ClearScriptJavaScriptScriptHost"/> class.
        /// </summary>
        public ClearScriptJavaScriptScriptHost()
        {
            Reset();
        }



        /// <summary>
        /// Gets the latest error message.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public string LatestErrorMessage
        {
            get { return _latestErrorMessage; }
        }

        private string _latestErrorMessage;

        /// <summary>
        /// Resets the memory space.
        /// </summary>
        public void Reset()
        {
            _hostObjectsRegistered = false;
            _latestErrorMessage = null;
            _engine = new V8ScriptEngine();
            
        }

        /// <summary>
        /// Registers the given assemblies in the script engines memory space.
        /// <para>
        /// IMPORTANT: Not supported in all languages (more of a language .NET concern).
        /// </para>
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void RegisterAssemblies(params System.Reflection.Assembly[] assemblies)
        {
            _latestErrorMessage =
                "RegisterHostObject has already been invoked at least once. Ensure all neccessary Assemblies are registered prior to invoking RegisterHostObject.";
            if (_hostObjectsRegistered)
            {
                throw new Exception(LatestErrorMessage);
            }

            //Ensure we only mention assembly one:
            foreach (Assembly assembly in assemblies.Where(assembly => !_registeredAssemblies.Contains(assembly)))
            {
                _registeredAssemblies.Add(assembly);
            }

            //Register it again under the same script reference 'host'
            HostTypeCollection assembliesSet = new HostTypeCollection(_registeredAssemblies.ToArray());

            _engine.AddHostObject(ExternalAssembliesKey, assembliesSet);
        }

        /// <summary>
        /// Registers the assemblies in which the <see cref="Type" />s are defined.
        /// <para>
        /// IMPORTANT: Not supported in all languages (more of a language .NET concern).
        /// </para>
        /// </summary>
        /// <param name="types">The types.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void RegisterAssemblies(params Type[] types)
        {
            if (_hostObjectsRegistered)
            {
                _latestErrorMessage =
                    "RegisterHostObject has already been invoked at least once. Ensure all neccessary Assemblies are registered prior to invoking RegisterHostObject.";
                throw new Exception(_latestErrorMessage);
            }
            foreach (Assembly assembly in types.Select(type => type.Assembly).Where(assembly => !_registeredAssemblies.Contains(assembly)))
            {
                _registeredAssemblies.Add(assembly);
            }

            HostTypeCollection assembliesSet = new HostTypeCollection(_registeredAssemblies.ToArray());

            _engine.AddHostObject(ExternalAssembliesKey, assembliesSet);
        }

        /// <summary>
        /// Registers the namespaces (ie, 'using') in the script engine's memory space
        /// <para>
        /// With C# scripts, this is the equivalent of prepending scripts with 'using SomeNamespace;'
        /// in order Types referred in subsequent execution statements do not have to be declared with FullName or FQN.
        /// <code>
        /// <![CDATA[
        /// using SomeNamespace;
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// IMPORTANT: Not supported in all languages (more of a .NET language concern).
        /// </para>
        /// </summary>
        /// <param name="namespaces">The namespaces.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void RegisterNamespaces(params string[] namespaces)
        {
            _latestErrorMessage =
                "ClearScript does not implement an equivalent to the 'using SomeNamespace;' statement.";
            throw new NotImplementedException(_latestErrorMessage);
        }


        /// <summary>
        /// Sets a script environment global (root) variable referencing a Host environment object shared between the two environments.
        /// <para>
        /// Not supported in all languages.
        /// </para>
        /// <para>
        /// C# example:
        /// <code>
        /// <![CDATA[
        /// var foo = new Foo();
        /// service.RegisterHostObject("foo", foo);
        /// service.Execute("foo.SomeProp = 123;");
        /// foo.SomeProp.Dump(); // displays '123'...
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// var foo = new Foo();
        /// service.RegisterHostObject("foo", foo);
        /// service.Execute("foo.SomeProp = 123;");
        /// foo.SomeProp.Dump(); // displays '123'...
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the host environment object.</typeparam>
        /// <param name="key">The variable name to use in the script environment.</param>
        /// <param name="value">The host environment object.</param>
        public void RegisterHostObject<TValue>(string key, TValue value)
        {
            try
            {
                _hostObjectsRegistered = true;
                _engine.AddHostObject(key, value);
            }
            catch (Exception e)
            {
                _latestErrorMessage = e.Message;
                throw;
            }

        }


        /// <summary>
        /// Executes the specified statement, returning nothing.
        /// <para>
        /// C# Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// // Before commencing execution, remember to Include required namespaces:
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// ...
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// scriptHost.Execute("var bar = new TestObject{Name=\"Joe\"};");
        /// ...
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");
        /// TestObject r1 = scriptHost.Evaluate<TestObject>("bar");
        /// string r3 = scriptHost.Evaluate<string>("bar.Name");
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// ...
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="statement">The statement.</param>
        public void Execute(string statement)
        {
            try {
            _engine.Execute(statement);
            }
            catch (Exception e)
            {
                _latestErrorMessage = e.Message;
                throw;
            }

        }

        /// <summary>
        /// Evaluates the specified statement returning an untyped value.
        /// <para>
        /// C# Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// // Before commencing execution, remember to Include required namespaces:
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// ..
        /// //  Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// scriptHost.Execute("var bar = new TestObject{Name=\"Joe\"};");
        /// ..
        /// //  Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");
        /// TestObject r1 = scriptHost.Evaluate<TestObject>("bar");
        /// string r3 = scriptHost.Evaluate<string>("bar.Name");
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// ...
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="statement">The statement.</param>
        public object Evaluate(string statement)
        {
            try {
            return _engine.Evaluate(statement);
            }
            catch (Exception e)
            {
                _latestErrorMessage = e.Message;
                throw;
            }

        }

        /// <summary>
        /// Evaluates the specified statement returning an untyped value.
        /// <para>
        /// C# Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// // Before commencing execution, remember to Include required namespaces:
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// ..
        /// //  Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// scriptHost.Execute("var bar = new TestObject{Name=\"Joe\"};");
        /// ..
        /// //  Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");
        /// TestObject r1 = scriptHost.Evaluate<TestObject>("bar");
        /// string r3 = scriptHost.Evaluate<string>("bar.Name");
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// // Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// ...
        /// // Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");        
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="statement">The statement.</param>
        public TResult Evaluate<TResult>(string statement)
        {
            try {
            object o = _engine.Evaluate(statement);

            return o.ConvertTo<TResult>();
            }
            catch (Exception e)
            {
                _latestErrorMessage = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Evaluates the script found in the given file.
        /// <para>
        /// C# Example:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// // Before commencing execution, remember to Include required namespaces:
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// ..
        /// //  Execute statements must be ended in ';'
        /// scriptHost.Execute("var foo = 123;");
        /// scriptHost.Execute("var bar = new TestObject{Name=\"Joe\"};");
        /// ..
        /// //  Evaluate statements are not ended with ';'
        /// int r1 = scriptHost.Evaluate<int>("foo");
        /// TestObject r1 = scriptHost.Evaluate<TestObject>("bar");
        /// string r3 = scriptHost.Evaluate<string>("bar.Name");
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// </summary>
        /// <param name="fileFullName">The file info.</param>
        public void ExecuteFile(string fileFullName)
        {
            try {
            _engine.Execute(fileFullName, true,null);
            }
            catch (Exception e)
            {
                _latestErrorMessage = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Gets the inner item.
        /// </summary>
        /// <typeparam name="TItem">The type of the item.</typeparam>
        /// <returns></returns>
        public TItem GetInnerItem<TItem>()
        {
            try {
            return _engine.ConvertTo<TItem>();
            }
            catch (Exception e)
            {
                _latestErrorMessage = e.Message;
                throw;
            }
        }
    }
}
