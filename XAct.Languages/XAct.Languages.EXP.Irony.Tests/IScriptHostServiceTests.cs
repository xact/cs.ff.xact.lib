namespace XAct.Languages.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IScriptHostServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

            ////run once before any tests in this testfixture have been run...
            ////...setup vars common to all the upcoming tests...

            //IoCBootStrapper.Initialize(
            //    true
            //    //    new XAct.Services.BindingDescriptor<IJurassicJavaScriptScriptHostService,JurassicJavaScriptScriptHostService>(),
            //    //    new XAct.Services.BindingDescriptor<IScriptHostService,JurassicJavaScriptScriptHostService>()

            //    );

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest01()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.Expressions);

            scriptHost.Execute("i=1");
            scriptHost.Execute("i=i+1");
            int result = scriptHost.Evaluate<int>("i");

            Assert.AreEqual(result, 2);
        }


        [Test]
        public void UnitTest02()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.Expressions);

            scriptHost.Execute("LO=\"SI\"");
            bool result = scriptHost.Evaluate<bool>("contains(\"Noon\",\"No\")");

            Assert.AreEqual(result, true);
        }
    }


}


