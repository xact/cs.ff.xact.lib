namespace XAct.Languages
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Roslyn.Compilers;
    using Roslyn.Scripting;
    using Roslyn.Scripting.CSharp;
    using XAct.Diagnostics;
    using XAct.Languages.Implementations;
    using XAct.Services;

    /// <summary>
    /// An XAct.Languages.CS.Roslyn class.
    /// </summary>
    /// 
    [DefaultBindingImplementation(typeof(IScriptHost),BindingLifetimeType.TransientScope,Priority.VeryLow, "CSharp")]
    [DefaultBindingImplementation(typeof(ICSharpScriptHost), BindingLifetimeType.TransientScope, Priority.Low)]
    [DefaultBindingImplementation(typeof(IRoslynCSharpScriptHost), BindingLifetimeType.TransientScope, Priority.Low)]
    public class RoslynCSharpScriptHost :IRoslynCSharpScriptHost
    {
        private readonly ITracingService _tracingService;

        private ScriptEngine _evaluator;
        private RoslynContext _roslynContext;
        private Session _session;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoslynCSharpScriptHost"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public RoslynCSharpScriptHost(ITracingService tracingService)
        {
            _tracingService = tracingService;

            Reset();

        }

        /// <summary>
        /// Gets the latest error message.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public string LatestErrorMessage { get; set; }

        /// <summary>
        /// Resets the memory space.
        /// </summary>
        public void Reset()
        {
            _tracingService.Trace(TraceLevel.Verbose, "Creating a new ScriptEngine Evaluator");



            _evaluator = new ScriptEngine(null,new AssemblyLoader());

            _roslynContext = new RoslynContext();


            _session = _evaluator.CreateSession(_roslynContext, typeof(RoslynContext));

            RegisterAssemblies(
                typeof(System.String).Assembly,
                typeof(System.Linq.EnumerableQuery).Assembly,
                typeof(IScriptHost).Assembly,
                _roslynContext.GetType().Assembly,
                this.GetType().Assembly
                );
            

        }

        /// <summary>
        /// Registers the given assemblies in the script engines memory space.
        /// <para>
        /// Not Supported in all languages.
        /// </para>
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        public void RegisterAssemblies(params System.Reflection.Assembly[] assemblies)
        {
            foreach (Assembly assembly in assemblies)
            {
                _session.AddReference(assembly);
            }
        }

        /// <summary>
        /// Registers the assembly in which the Types are defined.
        /// <para>
        /// Not supprted in all languages.
        /// </para>
        /// </summary>
        /// <param name="types">The types.</param>
        public void RegisterAssemblies(params System.Type[] types)
        {
            foreach (Type type in types)
            {
                _session.AddReference(type.Assembly);
            }
        }

        /// <summary>
        /// Registers the namespaces in the script engine's memory space.
        /// <para>
        /// Not supported in all languages.
        /// </para>
        /// </summary>
        /// <param name="namespaces">The namespaces.</param>
        public void RegisterNamespaces(params string[] namespaces)
        {
            try
            {
                foreach (string @namespace in namespaces)
                {
                    _session.ImportNamespace(@namespace);
                }
            }
            catch (System.Exception e)
            {
                LatestErrorMessage = e.Message;
                throw;
            }
        }


        /// <summary>
        /// Sets a script environment global (root) variable referencing a Host environment object shared between the two environments.
        /// <para>
        /// Not supported in all languages.
        /// </para>
        /// <para>
        /// C# example:
        /// <code>
        /// <![CDATA[
        /// var foo = new Foo();
        /// service.RegisterHostObject("foo", foo);
        /// service.Execute("foo.SomeProp = 123;");
        /// foo.SomeProp.Dump(); // displays '123'...
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// JS example:
        /// <code>
        /// <![CDATA[
        /// var foo = new Foo();
        /// service.RegisterHostObject("foo", foo);
        /// service.Execute("foo.SomeProp = 123;");
        /// foo.SomeProp.Dump(); // displays '123'...
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the host environment object.</typeparam>
        /// <param name="key">The variable name to use in the script environment.</param>
        /// <param name="value">The host environment object.</param>
        public void RegisterHostObject<TValue>(string key, TValue value)
        {
            _roslynContext[key] = value;

        }

        /// <summary>
        /// Sets the global variable.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void SetGlobalVariable<TValue>(string key, TValue value)
        {

            _roslynContext[key] = value;

        }


        /// <summary>
        /// Executes the specified statement, returning nothing.
        /// <para>
        /// IMPORTANT: With both JavaScript and CSharp, remember to complete *all* statements with semi-colons.
        /// </para>
        /// <para>
        /// IMPORTANT: With CSharp, remember to Include namespaces.
        /// </para>
        /// <para>
        /// C# Example:
        /// </para>
        /// <para>
        /// <example>
        /// <![CDATA[
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// scriptHost.Execute("var x = new TestObject{Name=\"Joe\"};");
        /// TestObject to = scriptHost.Evaluate<TestObject>("x;");
        /// string s = scriptHost.Evaluate<string>("x.Name;");
        /// ]]>
        /// </example>
        /// </para>
        /// </summary>
        /// <param name="statement">The statement.</param>
        public void Execute(string statement)
        {
            if (statement.IsNullOrEmpty())
            {
                return;
            }
            statement = statement.Trim();
            if (statement.IsNullOrEmpty())
            {
                return;
            }

            //if (!statement.EndsWith(";"))
            //{
            //    statement += ";";
            //}

            try
            {
                _session.Execute(statement);
            }
            catch(System.Exception e)
            {
                LatestErrorMessage = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Evaluates the specified statement returning an untyped value.
        /// <para>
        /// IMPORTANT: With both JavaScript and CSharp, remember to complete *all* statements with semi-colons.
        /// </para>
        /// <para>
        /// IMPORTANT: With CSharp, remember to Include namespaces.
        /// </para>
        /// <para>
        /// C# Example:
        /// </para>
        /// <para>
        /// <example>
        /// <![CDATA[
        /// scriptHost.Execute("using " + this.GetType().Namespace + ";");
        /// scriptHost.Execute("var x = new TestObject{Name=\"Joe\"};");
        /// TestObject to = scriptHost.Evaluate<TestObject>("x;");
        /// string s = scriptHost.Evaluate<string>("x.Name;");
        /// ]]>
        /// </example>
        /// </para>
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <returns>
        /// An object.
        /// </returns>
        public object Evaluate(string statement)
        {
            if (statement.IsNullOrEmpty())
            {
                return null;
            }
            statement = statement.Trim();
            if (statement.IsNullOrEmpty())
            {
                return null;
            }
            if (statement.EndsWith(";"))
            {
                statement = statement.TrimEnd(new[] { ';' });
            }

            try
            {
                return _session.Execute<object>(statement);
            }
            catch (System.Exception e)
            {
                LatestErrorMessage = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Evaluates the specified statement.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="statement">The statement.</param>
        /// <returns></returns>
        public TResult Evaluate<TResult>(string statement)
        {
            try
            {
                return (TResult) _session.Execute(statement);
            }
            catch (System.Exception e)
            {
                LatestErrorMessage = e.Message;
                throw;
            }
        }


        /// <summary>
        /// Evaluates the file.
        /// </summary>
        /// <param name="fullFileName">Full name of the file.</param>
        public void ExecuteFile(string fullFileName)
        {
            try
            {
                //string statement = _ioService.FileOpenReadAsync(fileFullName).WaitAndGetResult().ReadToEnd();
                _session.ExecuteFile(fullFileName);
            }
            catch (System.Exception e)
            {
                LatestErrorMessage = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Gets the inner item.
        /// </summary>
        /// <typeparam name="TItem">The type of the item.</typeparam>
        /// <returns></returns>
        public TItem GetInnerItem<TItem>()
        {
            return _session.ConvertTo<TItem>();
        }

    }
}