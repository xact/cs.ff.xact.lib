﻿

namespace XAct.Languages.Implementations
{
    using System.Collections.Generic;
    using System.Dynamic;

#pragma warning disable 1591
    public class RoslynContext : DynamicObject 
    {
        //public T GetContext<T>() { return (T) this[] ; }
        //public void SetContext<T>(T value) { _context = value; }
        //private object _context;


        // The inner dictionary.
        Dictionary<string, object> _dictionary
            = new Dictionary<string, object>();


        public RoslynContext Context {
            get { return this; }
        }

        public object this[string key ]
        {
            get { return _dictionary[key]; }
            set { _dictionary[key] = value; }
        }


        public int Count
        {
            get
            {
                return _dictionary.Count;
            }
        }


        // If you try to get a value of a property  
        // not defined in the class, this method is called. 
        public override bool TryGetMember(
            GetMemberBinder binder, out object result)
        {
            // Converting the property name to lowercase 
            // so that property names become case-insensitive. 
            string name = binder.Name.ToLower();

            // If the property name is found in a dictionary, 
            // set the result parameter to the property value and return true. 
            // Otherwise, return false. 
            return _dictionary.TryGetValue(name, out result);
        }

        // If you try to set a value of a property that is 
        // not defined in the class, this method is called. 
        public override bool TrySetMember(
            SetMemberBinder binder, object value)
        {
            // Converting the property name to lowercase 
            // so that property names become case-insensitive.
            _dictionary[binder.Name.ToLower()] = value;

            // You can always add a value to a dictionary, 
            // so this method always returns true. 
            return true;
        }
    }
#pragma warning restore 1591
}
