namespace XAct.Languages.Tests
{
    using System;
    using NUnit.Framework;
    using Roslyn.Scripting.CSharp;
    using XAct;
    using XAct.Languages.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class RoslynBasedScriptHostServiceTests
    {
        public class HostObject
        {
            public object Result { get; set; }
        }

        
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        ///// <summary>
        /////   An Example Test.
        ///// </summary>
        //[Test]
        //public void UnitTest01()
        //{
        //    var engine = new ScriptEngine();

        //    var session = engine.CreateSession();
        //    session.Execute()
        //    engine.Execute(@"var a = 42;");
        //    engine.Execute(@"System.Console.WriteLine(a);");
        //}


        [Test]
        public void CanGetScriptHostService()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();


            Assert.IsNotNull(scriptHostService);
        }

        [Test]
        public void CanGetScriptHostServiceOfExpectedType_ScriptHostService()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();


            Assert.AreEqual(typeof(ScriptHostService), scriptHostService.GetType());
        }


        [Test]
        public void CanCreateScriptHost()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            Assert.IsNotNull(scriptHost);
        }


        [Test]
        public void CanCreateScriptHostOfType_RoslynCSharpScriptHost()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            Assert.AreEqual(typeof(RoslynCSharpScriptHost), scriptHost.GetType());
        }



        
        [Test]
        [ExpectedException]
        public void DontTryToExecuteEvaluations()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //THis fails because it's really an evaluation, not an execution.
            scriptHost.Execute("2+5;");

            Assert.IsTrue(true);

        }


        [Test]
        public void CanExecuteNumbersAndSaveVar()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //But whereas it failed in the previous test, it now passes
            //as an Execute, because it is setting a var (an operation, not an evaluation)
            scriptHost.Execute("var x = 2+5;");

            Assert.IsTrue(true);
        }

        [Test]
        //FIXED:[ExpectedException]
        public void ExceptionRaisedIfEvaluateNumbersWithSemiColon()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            // Do NOT evaluate with final semi colon:
            var result = scriptHost.Evaluate("2+5;");

            Assert.AreEqual(7, result);
        }

        [Test]
        public void CanEvaluateNumbers()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            var result = scriptHost.Evaluate("2+5");

            Assert.AreEqual(7,result);
        }

        [Test]
        public void CanExecuteNumbersAndEvaluate()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            scriptHost.Execute("var y= 2+5;");
            
            
            object result = scriptHost.Evaluate("y");


            Assert.AreEqual(7, result);

            
        }
        [Test]
        //FIXED:[ExpectedException]
        public void ExceptionRaisedIfEvalHassemiColon()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            scriptHost.Execute("var y= 2+5;");


            object result = scriptHost.Evaluate("y;");


            Assert.AreEqual(7, result);


        }



        [Test]
        //[ExpectedException]
        public void CanEvaluateNumbersUsingNoSemicolon()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //Will raise exception due to no semi-colon:
#pragma warning disable 168
            int result = scriptHost.Evaluate<int>("2+5");
#pragma warning restore 168
            //No Exception thrown...
            //Unlike CS.Mono
            Assert.AreEqual(7,result);
        }

        [Test]
        [ExpectedException]
        public void RaisesExpectedExceptionIfRequestingResultOfAction()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //Return nothing...and throw an exception (hence ExpectedException):
#pragma warning disable 168
            object result = scriptHost.Evaluate<int>("var x = 2+5;");
#pragma warning restore 168

Assert.AreEqual(0,result);

        }

        [Test]
        public void SetGlobalScalar()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();



            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);


            scriptHost.RegisterHostObject("A", 123);


        }

        [Test]
        public void SetGlobalScalarAndRetrieveIt()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            int x = 123;

            scriptHost.RegisterHostObject("A", x);

            var o = scriptHost.Evaluate("Context[\"A\"]");

            Assert.AreEqual(123,o);
        }

        [Test]
        [ExpectedException]
        public void SetGlobalScalarAndRetrieveItUsingDynamicProperty()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            int x = 123;

            scriptHost.RegisterHostObject("A", x);

            var o = scriptHost.Evaluate("Context.A");

            Assert.AreEqual(123, o);
        }

        [Test]
        public void SetGlobalScalarAndRetrieveIt2()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            int x = 123;

            scriptHost.RegisterHostObject("A", x);

            var o = scriptHost.Evaluate<int>("Context[\"A\"]");


            Assert.AreEqual(123, o);
        }

        [Test]
        [ExpectedException]
        public void SetGlobalScalarAndRetrieveIt3()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            int x = 123;

            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);


            scriptHost.RegisterHostObject("A", x);

            //Change on the host side:
            x = 456;

            var o = scriptHost.Evaluate<int>("Context[\"A\"]");


            //Whereas object vars are shared (see below)
            //scalar ones are not shared between host and script environment
            Assert.AreEqual(345, o);
        }

        [Test]
        public void SetGlobalObjectAndRetrieveIt()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();


            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            var x = new XAct.KeyValue<string, object> { Key = "X", Value = 123 };
            
            scriptHost.RegisterHostObject("A", x);

            var o = scriptHost.Evaluate<XAct.KeyValue<string, object>>("Context[\"A\"]");




            Assert.AreEqual(123, o.Value);
        }

        [Test]
        public void SetGlobalObjectAndRetrieveIt2()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            

            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            var x = new XAct.KeyValue<string, object> { Key = "X", Value = 123 };

            scriptHost.RegisterAssemblies(typeof(XAct.KeyValue<string, object>).Assembly, typeof(System.String).Assembly);
            scriptHost.RegisterAssemblies(typeof(XAct.KeyValue<string, object>).Assembly, typeof(System.String).Assembly);

            scriptHost.RegisterHostObject("A", x);

            x.Value = 456;
            var o = scriptHost.Evaluate<XAct.KeyValue<string, object>>("Context[\"A\"]");




            //Provies vars are shared:
            Assert.AreEqual(456, o.Value);
        }


        //[Test]
        //public void PasInNameSpaceAndEntityAndRetrieveResult()
        //{

        //    IScriptHostService scriptHostService
        //        = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

        //    IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

        //    scriptHost.RegisterAssemblies(this.GetType().Assembly);

        //    //IMPORTANT:!!!!
        //    //Do not forget *any* SemiColons!
        //    //Do not forget assemblies
        //    //Do not forget includes
        //    //Rarer but still, do not forget any Nested Types (classes within types are not just ClassName)

        //    scriptHost.Execute("using " + this.GetType().Namespace + ";");
        //    scriptHost.Execute("var x = new TestObject{Name=\"Joe\"};");
        //    TestObject to = scriptHost.Evaluate<TestObject>("x;");

        //    Assert.IsNotNull(to);

        //    string s = scriptHost.Evaluate<string>("x.Name;");

        //    Assert.AreEqual("Joe", s);
        //}

//        [Test]
//        public void RunAScript()
//        {
//            IScriptHostService scriptHostService
//                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

//            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

//            scriptHost.RegisterAssemblies(this.GetType().Assembly);

//            string statement = "using " + typeof(ATestBusinessDomainEntity).Namespace + ";";
//            scriptHost.Execute(statement);
//            Trace.WriteLine("OK: {0}".FormatStringInvariantCulture(statement));

//            statement = "var item = new ATestBusinessDomainEntity{Name=\"Joe\", Amount=12.34};";
//            scriptHost.Execute(statement);
//            Trace.WriteLine("OK: {0}".FormatStringInvariantCulture(statement));

//            string script =
//                @"
//if (item.Name == ""John"")
//{
//    item.Amount = 1000.12m;
//}
//else 
//{
//    item.Amount = -999.89m;
//    item.Name = ""Was!John"";
//}
//";

//            scriptHost.Execute(script);
//            Trace.WriteLine("OK: {0}".FormatStringInvariantCulture("'script'"));

//            statement = "item;";
//            //ATestBusinessDomainEntity itemRef2 = scriptHost.Evaluate<ATestBusinessDomainEntity>("item;");
//            //Trace.WriteLine("OK: {0}".FormatStringInvariantCulture(statement));

//            //Assert.IsNotNull(itemRef2);

//            statement = "item.Amount;";
//            decimal s = scriptHost.Evaluate<decimal>(statement);
//            Trace.WriteLine("OK: {0}".FormatStringInvariantCulture(statement));

//            Assert.AreNotEqual(12.34, s);

//        }

    }
}


