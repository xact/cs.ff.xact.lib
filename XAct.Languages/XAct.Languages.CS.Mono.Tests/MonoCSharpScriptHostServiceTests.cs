namespace XAct.Languages.Tests
{
    using System.Diagnostics;
    using System.Threading;
    using NUnit.Framework;
    using XAct.IO;
    using XAct.IO.Implementations;
    using XAct.Languages.Implementations;
    using XAct.LanguagesTests.Entities;
    using XAct.Tests;


    public class TestObject
    {
        public string Name { get; set; }
    }


    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class MonoBasedScriptHostServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

            ////run once before any tests in this testfixture have been run...
            ////...setup vars common to all the upcoming tests...


            //IoCBootStrapper.Initialize(
            //    true
            //    //new XAct.Services.BindingDescriptor<ICSharpScriptHostService, CSharpScriptHostService>(),
            //    //new XAct.Services.BindingDescriptor<IScriptHostService, CSharpScriptHostService>()
            //    );

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }


        [Test]
        public void CanGetIScriptHostService()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();


            Assert.IsNotNull(scriptHostService);
        }

        [Test]
        public void CanGetIScriptHostServiceOfExpectedType()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();


            Assert.AreEqual(typeof(ScriptHostService), scriptHostService.GetType());
        }


        [Test]
        public void CanCreateScriptHost()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            Assert.IsNotNull(scriptHost);
        }


        [Test]
        public void CanCreateScriptHostOfExpectedType()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            Assert.AreEqual(typeof(MonoCSharpScriptHost), scriptHost.GetType());
        }

        [Test]
        public void CanGetIOService()
        {
            IIOService service
                = XAct.DependencyResolver.Current.GetInstance<IIOService>();


            Assert.IsNotNull(service);
        }
        [Test]
        public void CanGetFSIOService()
        {
            IFSIOService service
                = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();


            Assert.IsNotNull(service);
        }


        [Test]
        public void CanGetIOServiceOfExpectedType()
        {
            IIOService ioService
                = XAct.DependencyResolver.Current.GetInstance<IIOService>();


            Assert.AreEqual(typeof(FSIOService), ioService.GetType());
        }



        [Test]
        public void SetGlobalScalar()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();



            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);


            scriptHost.RegisterHostObject("A", 123);


        }

        [Test]
        public void SetGlobalScalarAndRetrieveIt()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            int x = 123;

            scriptHost.RegisterHostObject("A", x);

            var o = scriptHost.Evaluate("A");

            Assert.AreEqual(123, o);
        }

        [Test]
        public void SetGlobalScalarAndRetrieveIt2()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            int x = 123;

            scriptHost.RegisterHostObject("A", x);

            var o = scriptHost.Evaluate<int>("A");


            Assert.AreEqual(123, o);
        }

        [Test]
        [ExpectedException]
        public void SetGlobalScalarAndRetrieveIt3()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            int x = 123;

            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);


            scriptHost.RegisterHostObject("A", x);

            //Change on the host side:
            x = 456;

            var o = scriptHost.Evaluate<int>("A");


            //Whereas object vars are shared (see below)
            //scalar ones are not shared between host and script environment
            Assert.AreEqual(456, o);
        }

        [Test]
        public void SetGlobalObjectAndRetrieveIt()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();


            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            var x = new XAct.KeyValue<string, object> { Key = "X", Value = 123 };

            scriptHost.RegisterAssemblies(typeof(XAct.KeyValue<string, object>).Assembly, typeof(System.String).Assembly);

            scriptHost.RegisterHostObject("A", x);

            var o = scriptHost.Evaluate("A");

            var o2 = o as XAct.KeyValue<string, object>;

            Assert.AreEqual(123, o2.Value);
        }

        [Test]
        public void SetGlobalObjectAndRetrieveIt2()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();



            var scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            var x = new XAct.KeyValue<string, object> { Key = "X", Value = 123 };

            scriptHost.RegisterHostObject("A", x);

            x.Value = 456;
            var o = scriptHost.Evaluate<XAct.KeyValue<string, object>>("A");




            //Provies vars are shared:
            Assert.AreEqual(456, o.Value);
        }


        [Test]
        public void CanExecuteAdditionOperationOnTwoNumbers()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //fails:scriptHost.Evaluate<int>("2+5;");
            scriptHost.Execute("2+5;");

            Assert.IsTrue(true);
        }

        [Test]
        public void CanExecuteAdditionOperationOnTwoNumbersAndSaveResultsAsVariable()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //fails:scriptHost.Evaluate<int>("var x= 2+5;");
            scriptHost.Execute("var x = 2+5;");

            Assert.IsTrue(true);
        }

        [Test]
        public void CanExecuteAdditionOperationOnTwoNumbersAndSaveResultsAsVarAndRetrieveVar()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            scriptHost.Execute("var y= 2+5;");
            Thread.Sleep(100);
            object result = scriptHost.Evaluate("y;");

            //INTERESTING (SCARY): Works, manually, but in testrig, fails.

            Assert.AreEqual(7, result);
        }


        [Test]
        public void CanExecuteAdditionOperationOnTwoNumbersAndSaveResultsAsVarAndRetrieveVar2()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //fails:scriptHost.Evaluate<int>("var x= 2+5;");
            scriptHost.Execute("var x= 2+5;");
            Thread.Sleep(100);
            int result = scriptHost.Evaluate<int>("x;");


            //INTERESTING (SCARY): Works, manually, but in testrig, fails.

            Assert.AreEqual(7, result);
        }
        [Test]
        public void CanExecuteAdditionOperationOnTwoNumbersAndSaveResultsAsVarAndRetrieveVar3()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            scriptHost.Execute("var z= 2+5;");
            Thread.Sleep(100);
            //Missing ; should no longer be a problem
            int result = scriptHost.Evaluate<int>("z");


            //INTERESTING (SCARY): Works, manually, but in testrig, fails.

            Assert.AreEqual(7, result);
        }

        [Test]
        [ExpectedException]
        public void RaisExceptionIfCommandMissingSemicolon()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //Will raise exception due to no semi-colon:
#pragma warning disable 168
            int result = scriptHost.Evaluate<int>("2+5");
#pragma warning restore 168
            //Exception thrown...
            //...not anymore...
            //so...
            Assert.AreEqual(7, result);

        }


        [Test]
        [ExpectedException]
        public void RaisesExpectedExceptionIfRequestingResultOfAction()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IScriptHost scriptHost = scriptHostService.CreateScriptHost(ScriptHostType.CSharp);

            //Return nothing...and throw an exception (hence ExpectedException):
#pragma warning disable 168
            object result = scriptHost.Evaluate<int>("var e = 2+5;");
#pragma warning restore 168

        }


        [Test]
        public void PassInNameSpaceAndEntityAndRetrieveResult()
        {

            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IHostInteractiveScriptHost scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            scriptHost.RegisterAssemblies(this.GetType().Assembly);

            //IMPORTANT:!!!!
            //Do not forget *any* SemiColons!
            //Do not forget assemblies
            //Do not forget includes
            //Rarer but still, do not forget any Nested Types (classes within types are not just ClassName)

            scriptHost.Execute("using " + this.GetType().Namespace + ";");
            scriptHost.Execute("var x = new TestObject{Name=\"Joe\"};");
            TestObject to = scriptHost.Evaluate<TestObject>("x;");

            Assert.IsNotNull(to);

            string s = scriptHost.Evaluate<string>("x.Name;");

            //Excellent.
            Assert.AreEqual("Joe", s);
        }

        [Test]
        public void RunAScript()
        {
            IScriptHostService scriptHostService
                = XAct.DependencyResolver.Current.GetInstance<IScriptHostService>();

            IHostInteractiveScriptHost scriptHost = scriptHostService.CreateHostInteractiveScriptHost(ScriptHostType.CSharp);

            scriptHost.RegisterAssemblies(this.GetType().Assembly);

            string statement = "using " + typeof(ATestBusinessDomainEntity).Namespace + ";";
            scriptHost.Execute(statement);
            Trace.WriteLine("OK: {0}".FormatStringInvariantCulture(statement));

            statement = "var item = new ATestBusinessDomainEntity{Name=\"Joe\", Amount=12.34};";
            scriptHost.Execute(statement);
            Trace.WriteLine("OK: {0}".FormatStringInvariantCulture(statement));

            string script =
                @"
if (item.Name == ""John"")
{
    item.Amount = 1000.12m;
}
else 
{
    item.Amount = -999.89m;
    item.Name = ""Was!John"";
}
";

            scriptHost.Execute(script);
            Trace.WriteLine("OK: {0}".FormatStringInvariantCulture("'script'"));

            statement = "item;";
            //ATestBusinessDomainEntity itemRef2 = scriptHost.Evaluate<ATestBusinessDomainEntity>("item;");
            //Trace.WriteLine("OK: {0}".FormatStringInvariantCulture(statement));

            //Assert.IsNotNull(itemRef2);

            statement = "item.Amount;";
            decimal s = scriptHost.Evaluate<decimal>(statement);
            Trace.WriteLine("OK: {0}".FormatStringInvariantCulture(statement));

            Assert.AreNotEqual(12.34, s);

        }

    }
}


