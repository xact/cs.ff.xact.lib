using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using XAct.Environment;

namespace XAct.VisualStudio.Tests
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class UnitTests1
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Find_ProjectFile()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();


            string csProjFile = environmentService.MapPath(@"..\..\XAct.VisualStudio.Tools.Tests.csproj");

            Console.WriteLine(csProjFile);
            Assert.IsTrue(System.IO.File.Exists(csProjFile));
        }

        [Test]
        public void Read_ProjectFile()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();


            string csProjFile = environmentService.MapPath(@"..\..\XAct.VisualStudio.Tools.Tests.csproj");

            Assert.IsTrue(System.IO.File.Exists(csProjFile));
            string lines = System.IO.File.ReadAllText(csProjFile);
            Console.WriteLine(lines);
            Assert.That(lines, Is.Not.Null);
        }

        [Test]
        public void Read_ProjectFile_As_Xml()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();


            string csProjFile = environmentService.MapPath(@"..\..\XAct.VisualStudio.Tools.Tests.csproj");

            Assert.IsTrue(System.IO.File.Exists(csProjFile));
            string lines = System.IO.File.ReadAllText(csProjFile);
            Console.WriteLine(lines);
            XDocument x = XDocument.Load(csProjFile);

            Assert.That(x, Is.Not.Null);
            Assert.That(x.Root, Is.Not.Null);
            Assert.That(x.Root.Name, Is.Not.Null);
        }

        [Test]
        public void Read_ProjectFile_As_XDocument_And_Find_Project_References()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();


            string csProjFile = environmentService.MapPath(@"..\..\XAct.VisualStudio.Tools.Tests.csproj");

            Assert.IsTrue(System.IO.File.Exists(csProjFile));
            string lines = System.IO.File.ReadAllText(csProjFile);
            Console.WriteLine(lines);
            XDocument x = XDocument.Load(csProjFile);


            var namespaceManager = new XmlNamespaceManager(new NameTable());
            namespaceManager.AddNamespace("empty", "http://schemas.microsoft.com/developer/msbuild/2003");
            var projectReferenceNodes = x.XPathSelectElements("/empty:Project/empty:ItemGroup/empty:ProjectReference",
                namespaceManager);

            Assert.That(x, Is.Not.Null);
            Assert.That(x.Root, Is.Not.Null);
            Assert.That(x.Root.Name, Is.Not.Null);

            Assert.That(projectReferenceNodes, Is.Not.Null);
            Assert.That(projectReferenceNodes.Count(), Is.AtLeast(1));

        }


        [Test]
        public void Read_ProjectFile_As_XDocument_And_Find_Project_References_As_FilePaths()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IVisualStudioProjectFileService visualStudioProjectFileService =
                DependencyResolver.Current.GetInstance<IVisualStudioProjectFileService>();


            string csProjFile = environmentService.MapPath(@"..\..\XAct.VisualStudio.Tools.Tests.csproj");
            string projectDirectory = System.IO.Path.GetDirectoryName(csProjFile);


            var x = visualStudioProjectFileService.GetReferencedProjects(csProjFile);

            Assert.That(x, Is.Not.Null);
            Assert.That(x.Count(), Is.AtLeast(1));
            foreach (string path in x)
            {
                Assert.That(System.IO.File.Exists(path));
            }

        }


        [Test]
        public void Ensure_Solution_File_Exists()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IVisualStudioProjectFileService visualStudioProjectFileService =
                DependencyResolver.Current.GetInstance<IVisualStudioProjectFileService>();


            string csProjFile = environmentService.MapPath(@"..\..\..\..\..\CS.FF.XAct.Lib.sln");

            Console.WriteLine(csProjFile);
            Assert.IsTrue(System.IO.File.Exists(csProjFile));
        }

        [Test]
        public void Read_Solution_File()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IVisualStudioProjectFileService visualStudioProjectFileService =
                DependencyResolver.Current.GetInstance<IVisualStudioProjectFileService>();


            string csProjFile = environmentService.MapPath(@"..\..\..\..\..\CS.FF.XAct.Lib.sln");

            string lines = System.IO.File.ReadAllText(csProjFile);

            Assert.That(lines, Is.Not.Null);
        }

        [Test]
        public void Get_Solutions_Projects()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IVisualStudioProjectFileService visualStudioProjectFileService =
                DependencyResolver.Current.GetInstance<IVisualStudioProjectFileService>();


            string csProjFile = environmentService.MapPath(@"..\..\..\..\..\CS.FF.XAct.Lib.sln");

            string projectDirectory = System.IO.Path.GetDirectoryName(csProjFile);

            var Content = File.ReadAllText(csProjFile);
            Regex projReg = new Regex("Project\\(\"\\{[\\w-]*\\}\"\\) = \"([\\w _]*.*)\", \"(.*\\.(cs|vcx|vb)proj)\"",
                RegexOptions.Compiled);

            var matches = projReg.Matches(Content).Cast<Match>();
            var Projects = matches.Select(x => x.Groups[2].Value).ToList();
            for (int i = 0; i < Projects.Count; ++i)
            {
                if (!Path.IsPathRooted(Projects[i]))
                    Projects[i] = Path.Combine(Path.GetDirectoryName(csProjFile),
                        Projects[i]);
                Projects[i] = Path.GetFullPath(Projects[i]);
            }
            Assert.That(Projects.Count(), Is.AtLeast(1));
            foreach (string path in Projects)
            {
                Console.WriteLine(path);
                Assert.That(System.IO.File.Exists(path));
            }

        }

        [Test]
        public void Get_Solutions_Projects_Using_Service2()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IVisualStudioSolutionFileService visualStudioSolutionFileService =
                DependencyResolver.Current.GetInstance<IVisualStudioSolutionFileService>();


            string slnProjFile = environmentService.MapPath(@"..\..\..\..\..\CS.FF.XAct.Lib.sln");

            var projects = visualStudioSolutionFileService.GetSolutionProjectFileNames(slnProjFile);

            Assert.That(projects.Count(), Is.AtLeast(1));
            Console.WriteLine(projects.Count());
            foreach (string path in projects)
            {
                Console.WriteLine(path);
                Assert.That(System.IO.File.Exists(path));
            }
        }

        [Test]
        public void Get_Solutions_Projects_Using_Service()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IVisualStudioSolutionFileService visualStudioSolutionFileService =
                DependencyResolver.Current.GetInstance<IVisualStudioSolutionFileService>();


            string slnProjFile = environmentService.MapPath(@"..\..\..\..\..\CS.FF.XAct.Lib.sln");

            var graph = visualStudioSolutionFileService.BuildProjectGraph(slnProjFile);

            Assert.That(graph.Count, Is.AtLeast(1));


        }



        [Test]
        public void Get_A_Core_File()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            string csFile = environmentService.MapPath(@"..\..\..\..\..\XAct.Core\XAct.Core.PCL\Difficulty.cs");

            csFile = System.IO.Path.GetFullPath(csFile);
            Console.WriteLine(csFile);
            Assert.That(System.IO.File.Exists(csFile), Is.True);
        }



        [Test]
        public void Execute_A_Git_Command_By_ProcessStartInfo()
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo();
            processStartInfo.CreateNoWindow = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.RedirectStandardOutput = true;
            //gitInfo.FileName = YOUR_GIT_INSTALLED_DIRECTORY + @"\bin\git.exe";
            processStartInfo.FileName = "git.exe";
            processStartInfo.Arguments = "log -1 --pretty=%B"; // such as "fetch orign"

            Process process = new Process();
            //gitInfo.WorkingDirectory = YOUR_GIT_REPOSITORY_PATH;
            processStartInfo.UseShellExecute = false;

            process.StartInfo = processStartInfo;
            process.Start();

            string stderr_str = process.StandardError.ReadToEnd();  // pick up STDERR
            string stdout_str = process.StandardOutput.ReadToEnd(); // pick up STDOUT

            process.WaitForExit();
            process.Close();

            Console.WriteLine(stdout_str);

            Assert.That(stdout_str,Is.Not.Null);
        }


        [Test]
        public void Execute_A_Git_Command_Using_LibraryHelper()
        {

            var result = ProcessManager.InvokeProcess(@"C:\Program Files (x86)\Git\bin\git.exe", "log -1 --pretty=%B").StandardOut;
            Console.WriteLine(result);

            Assert.That(result, Is.Not.Null);
        }



        [Test]
        public void Execute_A_Git_Command_Using_LibraryHelper_Without_Path()
        {

            var result = ProcessManager.InvokeProcess(@"git.exe", "log -1 --pretty=%B").StandardOut;
            Console.WriteLine(result);

            Assert.That(result, Is.Not.Null);
        }



    }//~class
}


