using System;
using NUnit.Framework;
using XAct.Environment;
using XAct.RemoteServices.Git;
using XAct.Tests;

namespace XAct.VisualStudio.Tests
{

    [TestFixture]
    public class IGitCommandServiceTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Get_Git_Hashes()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IGitCommandService gitCommandService =
                DependencyResolver.Current.GetInstance<IGitCommandService>();


            var result = gitCommandService.GetHashes();
            
            Console.WriteLine(string.Join(System.Environment.NewLine,result));

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Not.StartsWith("fatal:"));
        }


        [Test]
        public void Get_Files_Within_Commit_Directly()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IGitCommandService gitCommandService =
                DependencyResolver.Current.GetInstance<IGitCommandService>();
            //  

            var result = XAct.ProcessManager.InvokeProcess("git.exe", "show  --name-only --pretty=\"format:\"");


            Console.WriteLine(string.Join(System.Environment.NewLine, result.StandardError));
            Console.WriteLine(string.Join(System.Environment.NewLine, result.StandardOut));

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Not.StartsWith("fatal:"));
        }


        [Test]
        public void Get_Files_WIthin_Last_Commit()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IGitCommandService gitCommandService =
                DependencyResolver.Current.GetInstance<IGitCommandService>();


            var result = gitCommandService.GetLatestCommitFiles();

            Console.WriteLine(string.Join(System.Environment.NewLine, result));

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result, Is.Not.StartsWith("fatal:"));
        }


        [Test]
        public void Get_Files_WIthin_Two_Commits()
        {

            IEnvironmentService environmentService =
                DependencyResolver.Current.GetInstance<IEnvironmentService>();

            IGitCommandService gitCommandService =
                DependencyResolver.Current.GetInstance<IGitCommandService>();


            var result = gitCommandService.GetFilesWithinCommits();

            Console.WriteLine(string.Join(System.Environment.NewLine, result));

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result, Is.Not.StartsWith("fatal:"));
        }


    }
}