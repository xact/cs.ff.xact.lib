namespace XAct.VisualStudio
{
    public interface IVisualStudioSolutionFileService : IHasService
    {
        string[] GetSolutionProjectFileNames(string solutionFileName);
        VisualStudioProjectGraph BuildProjectGraph(string solutionFileName);
    }
}