using System.Collections.Generic;

namespace XAct.VisualStudio
{
    public class VisualStudioProjectGraph
    {
        private Dictionary<string, VisualStudioProjectFileInfo> vertices =
            new Dictionary<string, VisualStudioProjectFileInfo>();

        public int Count
        {
            get { return vertices.Count; }
        }

        public VisualStudioProjectFileInfo Register(string projectFileName)
        {
            var projectKey = System.IO.Path.GetFileName(projectFileName);

            VisualStudioProjectFileInfo projectInfo;
            if (!vertices.TryGetValue(projectKey, out projectInfo))
            {
                vertices[projectKey] =
                    projectInfo = 
                        new VisualStudioProjectFileInfo(this) { Name = projectKey, Path = projectFileName };
            }

            //var dependencyKey = System.IO.Path.GetFileName(dependencyFileName);

            //VisualStudioProjectFileInfo dependencyInfo;
            //if (!vertices.TryGetValue(dependencyKey, out dependencyInfo))
            //{
            //    vertices[dependencyKey] = 
            //        dependencyInfo = 
            //            new VisualStudioProjectFileInfo {Name = dependencyKey, Path = dependencyFileName};
            //}

            ////Register 
            //projectInfo.Dependencies[dependencyKey] = dependencyInfo;
            //dependencyInfo.DependedOnBy[projectKey] = projectInfo;

            return projectInfo;
        }

        public void Register(string projectFileName, string dependencyFileName)
        {
            var projectKey = System.IO.Path.GetFileName(projectFileName);

            VisualStudioProjectFileInfo projectInfo;
            if (!vertices.TryGetValue(projectKey, out projectInfo))
            {
                vertices[projectKey] =
                    projectInfo =
                        new VisualStudioProjectFileInfo(this) { Name = projectKey, Path = projectFileName };
            }

            var dependencyKey = System.IO.Path.GetFileName(dependencyFileName);

            VisualStudioProjectFileInfo dependencyInfo;
            if (!vertices.TryGetValue(dependencyKey, out dependencyInfo))
            {
                vertices[dependencyKey] =
                    dependencyInfo =
                        new VisualStudioProjectFileInfo(this) { Name = dependencyKey, Path = dependencyFileName };
            }

            //Register 
            projectInfo.Dependencies[dependencyKey] = dependencyInfo;
            dependencyInfo.DependedOnBy[projectKey] = projectInfo;

            //return projectInfo;
        }


        public IEnumerable<VisualStudioProjectFileInfo> GetDependents(string name)
        {
            name = System.IO.Path.GetFileName(name);

            VisualStudioProjectFileInfo tmp;
            if (!vertices.TryGetValue(out tmp))
            {
                yield return null;
            }
            foreach (var x in tmp.DependedOnBy.Values)
            {
                yield return x;
                foreach (var y in GetDependents(x.Path))
                {
                    yield return y;
                }
            }
        }
    }
}