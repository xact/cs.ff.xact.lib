using System.Collections.Generic;

namespace XAct.VisualStudio
{
    public class VisualStudioProjectFileInfo
    {
        private readonly VisualStudioProjectGraph _graph;
        private readonly Dictionary<string, VisualStudioProjectFileInfo> _dependencies = new Dictionary<string, VisualStudioProjectFileInfo>();
        private readonly Dictionary<string, VisualStudioProjectFileInfo> _dependedOnBy = new Dictionary<string, VisualStudioProjectFileInfo>();
        public string Name { get; set; }
        public string Path { get; set; }

        public VisualStudioProjectFileInfo(VisualStudioProjectGraph graph)
        {
            _graph = graph;
        }


        public VisualStudioProjectFileInfo RegisterDependency(string projectFileName)
        {
            return Register(Dependencies, projectFileName);
        }

        public VisualStudioProjectFileInfo RegisterClient(string projectFileName)
        {
            return Register(DependedOnBy, projectFileName);
        }


        public Dictionary<string, VisualStudioProjectFileInfo> Dependencies
        {
            get { return _dependencies; }
        }

        public Dictionary<string, VisualStudioProjectFileInfo> DependedOnBy
        {
            get { return _dependedOnBy; }
        }


        private VisualStudioProjectFileInfo Register(IDictionary<string, VisualStudioProjectFileInfo> dictionary, string projectFileName)
        {
            string key = System.IO.Path.GetFileName(projectFileName);

            VisualStudioProjectFileInfo dependencyInfo;
            if (!dictionary.TryGetValue(key, out dependencyInfo))
            {
                dictionary[key] = _graph.Register(projectFileName);
            }
            return dependencyInfo;



        }
    }
}