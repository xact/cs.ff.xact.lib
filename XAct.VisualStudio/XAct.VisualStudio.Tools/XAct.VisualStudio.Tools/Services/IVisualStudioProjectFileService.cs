namespace XAct.VisualStudio
{
    public interface IVisualStudioProjectFileService :IHasService
    {
        string[] GetReferencedProjects(string projectFileName);
    }
}