using System;
using XAct.Environment;
using System.Linq;

namespace XAct.RemoteServices.Git.Services.Implementations
{
    public class GitCommandService:IGitCommandService
    {
        private readonly IEnvironmentService _environmentService;
        public GitCommandService(IEnvironmentService environmentService)
        {
            _environmentService = environmentService;
        }


        public string GetCurrentBranch()
        {
            return ExecuteGitCommand("branch");
        }

        public string GetRemoteBranches()
        {
            //Note that this only reads local cache of what it knows of remote.
            //use git ls-remote for better:
            return ExecuteGitCommand("branch -r");
        }

        public string AddUpdatedFiles()
        {
            //Commit/Add only updated tracked files:
            return ExecuteGitCommand("add -u");
        }

        public string AddUpdatedAndNewFiles()
        {
            return ExecuteGitCommand("add --A");
        }

        public string Pull()
        {
            return ExecuteGitCommand("pull -v");
        }

        public string Push()
        {
            return ExecuteGitCommand("push");
        }

        public string GetLocalAndRemoteBranches()
        {
            return ExecuteGitCommand("branch -a");
        }

        public string GetLatestCommitMessage() {
            return ExecuteGitCommand("log -1 --pretty=%B");
        }

        public string[] GetHashes()
        {
            string[] results = ExecuteGitCommand("rev-list HEAD").Split(
                new char[] {'\n'},
                StringSplitOptions.RemoveEmptyEntries);
            return results;
        }

        public string GetLatestCommitHash()
        {
            string result = ExecuteGitCommand("rev-parse HEAD").Trim();//.Split(new string[] {System.Environment.NewLine},StringSplitOptions.RemoveEmptyEntries).FirstOrDefault().Trim();
            return result;
        }

        public string[] GetLatestCommitFiles()
        {
            string[] results = ExecuteGitCommand("show --name-only --pretty=\"format:\"").Split(
                new char[] { '\n' },
                StringSplitOptions.RemoveEmptyEntries).OrderBy(x=>x, StringComparer.InvariantCultureIgnoreCase).ToArray();

            return results;

        }

        public string[] GetFilesWithinCommits(string beginHash = null, string endHash = null)
        {
            if (endHash.IsNullOrEmpty())
            {
                endHash = GetLatestCommitHash();
            }
            if (!beginHash.IsNullOrEmpty())
            {
                beginHash = beginHash + "..";
            }
            string[] results = ExecuteGitCommand("show {0}{1} --name-only --pretty=\"format:\"".FormatStringInvariantCulture(beginHash,endHash)).Split(
                new char[] { '\n' },
                StringSplitOptions.RemoveEmptyEntries).OrderBy(x => x, StringComparer.InvariantCultureIgnoreCase).ToArray();
            return results;
        }


        public string ExecuteGitCommand(string arguments, int expectedErrorCode=0) {
            var r = XAct.ProcessManager.InvokeProcess("git.exe", arguments);

            if (r.ExitCode==0)
            {
                return r.StandardOut;
            }
            System.Diagnostics.Trace.TraceError(r.StandardError);
            return string.Empty;
        }


    }
}