using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace XAct.VisualStudio.Services.Implementations
{
    public class VisualStudioSolutionFileService : IVisualStudioSolutionFileService
    {
        private readonly IVisualStudioProjectFileService _visualStudioProjectFileService;

        public VisualStudioSolutionFileService(IVisualStudioProjectFileService visualStudioProjectFileService)
        {
            _visualStudioProjectFileService = visualStudioProjectFileService;
        }

        public string[] GetSolutionProjectFileNames(string solutionFileName)
        {
            string projectDirectory = System.IO.Path.GetDirectoryName(solutionFileName);

            var Content = File.ReadAllText(solutionFileName);
            Regex projReg = new Regex("Project\\(\"\\{[\\w-]*\\}\"\\) = \"([\\w _]*.*)\", \"(.*\\.(cs|vcx|vb)proj)\"",
                RegexOptions.Compiled);

            var matches = projReg.Matches(Content).Cast<Match>();

            var projects = matches.Select(x => x.Groups[2].Value).ToList();

            for (int i = 0; i < projects.Count; ++i)
            {
                if (!Path.IsPathRooted(projects[i]))
                    projects[i] = Path.Combine(Path.GetDirectoryName(solutionFileName),
                        projects[i]);
                projects[i] = Path.GetFullPath(projects[i]);
            }

            return projects.ToArray();
        }

        public VisualStudioProjectGraph BuildProjectGraph(string solutionFileName)
        {
            var graph = new VisualStudioProjectGraph();
            foreach (string projectFileName in GetSolutionProjectFileNames(solutionFileName))
            {
                foreach (var dependencyFileName in _visualStudioProjectFileService.GetReferencedProjects(projectFileName))
                {
                    graph.Register(dependencyFileName,projectFileName);
                }
            }
            return graph;
        }
    }
}