﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using XAct;
using XAct.Environment;

namespace XAct.RemoteServices.Git
{
}

namespace XAct.RemoteServices.Git.Services.Implementations
{
}

namespace XAct.VisualStudio.Services.Implementations
{
    

    public class VisualStudioProjectFileService : IVisualStudioProjectFileService
    {
        private readonly IEnvironmentService _environmentService;

        public VisualStudioProjectFileService(IEnvironmentService environmentService)
        {
            _environmentService = environmentService;
        }


        public string[] GetReferencedProjects(string projectFileName)
        {


            string projectDirectory = System.IO.Path.GetDirectoryName(projectFileName);
            XDocument xDocument = XDocument.Load(projectFileName);


            var namespaceManager = new XmlNamespaceManager(new NameTable());
            namespaceManager.AddNamespace("empty", "http://schemas.microsoft.com/developer/msbuild/2003");
            string[] projectReferenceNodes = xDocument.XPathSelectElements("/empty:Project/empty:ItemGroup/empty:ProjectReference", namespaceManager).Select(y => y.Attribute("Include").Value).ToArray();

            var results = new List<string>();

           results.AddRange(projectReferenceNodes.Map(
                    x=> 
                    
                    Path.IsPathRooted(x)
                    ? x
                    : System.IO.Path.GetFullPath(System.IO.Path.Combine(projectDirectory, x))));

            return results.ToArray();
        }





    


    //FileInfo projectFileInfo = FindFileRecurseUp(fileInfo.Directory, "*.csproj", 3);


    public FileInfo FindFileRecurseUp(DirectoryInfo directoryInfo, string searchPattern,
                                         int maxRecursion = int.MaxValue, int recursion = 0)
        {
            if (recursion >= maxRecursion)
            {
                return null;
            }
            if (directoryInfo == null)
            {
                return null;
            }
            if (!directoryInfo.Exists)
            {
                return null;
            }

            FileInfo[] fileInfos = directoryInfo.GetFiles(searchPattern, SearchOption.TopDirectoryOnly);
            if (fileInfos.Length == 0)
            {
                return FindFileRecurseUp(directoryInfo.Parent, searchPattern, maxRecursion, recursion + 1);
            }
            return fileInfos[0];
        }


    }
}
