namespace XAct.RemoteServices.Git
{
    /// <summary>
    /// Contract for a Service to manage Git.
    /// </summary>
    public interface IGitCommandService:IHasService
    {



        /// <summary>
        /// Gets the local cache of known remote branches, along with any newly created local branches.
        /// </summary>
        /// <returns></returns>
        string GetCurrentBranch();

        /// <summary>
        /// Gets the local cache of known remote branches.
        /// </summary>
        /// <returns></returns>
        string GetRemoteBranches();

        
        string GetLocalAndRemoteBranches();

        /// <summary>
        /// Get the last local Commit Message.
        /// </summary>
        /// <returns></returns>
        string GetLatestCommitMessage();

        /// <summary>
        /// Gets *all* commit hashes.
        /// <para>
        /// You probably want instead <see cref="GetLatestCommitHash"/>
        /// </para>
        /// </summary>
        /// <returns></returns>
        string[] GetHashes();

        /// <summary>
        /// Gets the hash for the latest Commit.
        /// </summary>
        /// <returns></returns>
        string GetLatestCommitHash();

        /// <summary>
        /// Gets list of files within latest Commit.
        /// <para>
        /// Useful in order to get list of projects that are affected by latest commit.
        /// </para>
        /// </summary>
        /// <returns></returns>
        string[] GetLatestCommitFiles();

        /// <summary>
        /// Gets list of files between first commit hash (if null, right back to beginning!)
        /// and latest commit hash.
        /// </summary>
        /// <param name="beginHash"></param>
        /// <param name="endHash"></param>
        /// <returns></returns>
        string[] GetFilesWithinCommits(string beginHash = null, string endHash = null);



        /// <summary>
        /// Commit/Add only updated tracked files
        /// </summary>
        /// <returns></returns>
        string AddUpdatedFiles();


        /// <summary>
        /// Commit/Add new untracked and updated tracked files
        /// </summary>
        /// <returns></returns>
        string AddUpdatedAndNewFiles();


        /// <summary>
        /// Pull changes from the remote server.
        /// </summary>
        /// <returns></returns>
        string Pull();

        /// <summary>
        /// Push changes to the remote source.
        /// </summary>
        /// <returns></returns>
        string Push();

    }
}