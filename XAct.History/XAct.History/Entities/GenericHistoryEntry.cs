﻿namespace XAct.History
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Data.DataSources;
    using XAct.Diagnostics;

    /// <summary>
    /// History Entity
    /// <para>
    /// TODO:Not at all happy that it can't be used for both List/Item at present.
    /// Still thinking about how to uniquely identify operations per View, not just
    /// items.
    /// </para>
    /// </summary>
    [DataContract]
    public class GenericHistoryEntry : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasDataSourceIdentifier, IHasApplicationTennantId, IHasTraceLevel, XAct.IHasDateTimeCreatedOnUtc, IHasTag, IHasSerializedTypeValueAndMethod
    {




        /// <summary>
        /// The unique Id of the record on that machine.
        /// <para>Member defined in<see cref="XAct.IHasDistributedIdentities"/></para>
        /// </summary>
        /// <value></value>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }

        /// <summary>
        /// The DataSource record associated to this <see cref="GenericHistoryEntry"/>.
        /// </summary>
        public virtual DataSource DataSource { get; set; }
        
        
        /// <summary>
        /// A unique identifier (FK) for the <c>IDataSource</c>
        /// (eg: the XXX Table in YYY Db in ZZZ ConnectionString)
        /// <para>Member defined in<see cref="IHasDataSourceIdentifier" /></para>
        /// </summary>
        /// <remarks>
        /// The Guid (rather than a shorter Int) allows developers to develop offline,
        /// rather than having to first reserve a DataSource Id, centrally.
        /// </remarks>
        [DataMember]
        public virtual Guid DataSourceId { get; set; }

        /// <summary>
        /// The unique identifiers for the data object in the source XXX table.
        /// <para>
        /// It is up to the Connector to know how to deserialize the value
        /// into a string, guid, etc. depending on the Connector's knowledge
        /// of the source catalog.
        /// </para>
        /// <para>Member defined in<see cref="IHasDataSourceIdentifier" /></para>
        /// </summary>
        /// <remarks>
        /// I agonized a long time on whether this should be a dictionary,
        /// but
        /// a) makes it hard to impossible to use ORMs effectively,
        /// b) saves time serialize/deserialize as the most common use is in the serialized state.
        /// </remarks>
        [DataMember]
        public virtual string DataSourceSerializedIdentities { get; set; }

        
        
        /// <summary>
        /// Not all records need to be made visible to anybody else but support staff.
        /// </summary>
        [DataMember]
        public virtual bool VisibleToUsers { get; set; }

        /// <summary>
        /// A history might be showing an error that happened (eg: email not delivered)
        /// <para>Critical, High, Normal, Low, VeryLow</para>
        /// </summary>
        [DataMember]
        public virtual TraceLevel TraceLevel { get; set; }

        /// <summary>
        /// The Utc time the history record was added.
        /// </summary>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }



        /// <summary>
        /// Gets or sets the serialization method.
        /// </summary>
        /// <value>
        /// The serialization method.
        /// </value>
        [DataMember]
        public virtual SerializationMethod SerializationMethod { get; set; }

        /// <summary>
        /// Gets or sets the type of the serialized value.
        /// </summary>
        /// <value>
        /// The type of the serialized value.
        /// </value>
        [DataMember]
        public virtual string SerializedValueType { get; set; }
        
        /// <summary>
        /// Gets or sets the serialized value.
        /// </summary>
        /// <value>
        /// The serialized value.
        /// </value>
        [DataMember]
        public virtual string SerializedValue { get; set; }



        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        [DataMember]
        public virtual string Tag { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="GenericHistoryEntry"/> class.
        /// </summary>
        public GenericHistoryEntry()
        {
            this.GenerateDistributedId();
        }
    }
}
