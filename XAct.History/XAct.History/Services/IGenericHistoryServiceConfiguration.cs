namespace XAct.History
{
    /// <summary>
    /// Contract for a Configuratino package for the 
    /// <see cref="IGenericHistoryService"/>
    /// </summary>
    public interface IGenericHistoryServiceConfiguration : IHasXActLibServiceConfiguration
    {
        
    }
}