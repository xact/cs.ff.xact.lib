namespace XAct.History
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Data.DataSources;
    using XAct.Messages;


    /// <summary>
    ///   Service to attach arbitrary History to a record, view, user, etc.
    ///  <para>
    /// Example usage is displaying under an record every update to it, 
    /// showing change type, icon, by user, description, date...or anything 
    /// else. Another use case would be under a User table, displaying 
    /// every login, namechange, or password reset.
    /// <para>
    /// </para>
    /// The format is non-prescribed, and can be of your choosing 
    /// (string, JSON, XML) depending on your UX needs. 
    /// </para>
    /// <para>
    /// The records do not contain a relational FK to any record,
    /// but contains a serialized Id ( <see cref="DataSourceIdentifier"/>)
    /// in order to be attachable to more than one table. 
    /// </para>
    /// </summary>
    /// <summary>
    ///   Interface for writting and retrieving 
    ///   <see cref = "GenericHistoryEntry" /> records.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     The difference between a Trace and History entry is that 
    ///     the Trace is for the whole app, and is not kept.
    ///     The History on the other hand is permanently associated to the record.
    ///   </para>
    /// </remarks>
    public interface IGenericHistoryService : IHasXActLibService
    {

        /// <summary>
        /// Saves the given history entry.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <param name="visibleToUsers">if set to <c>true</c> [visible to users].</param>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="serializedTypeValueAndMethod">The serialized type value and method.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="tag">The tag.</param>
        void Record(IHasDataSourceIdentifier dataSourceIdentifier, bool visibleToUsers, XAct.Diagnostics.TraceLevel traceLevel,
                                                       IHasSerializedTypeValueAndMethod serializedTypeValueAndMethod, Guid? applicationTennantId = null, string tag=null);


        /// <summary>
        /// Gets the specified history entry type.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="includeApplicationEntries">if set to <c>true</c> [include application entries].</param>
        /// <returns></returns>
        IQueryable<GenericHistoryEntry> Get(IHasDataSourceIdentifierReadOnly dataSourceIdentifier, IPagedQuerySpecification pagedQuerySpecification, Guid? applicationTennantId = null, bool includeApplicationEntries = true);
    }
}