﻿namespace XAct.History.Implementations
{
    using System;
    using System.Linq;
    using XAct;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IGenericHistoryService" />
    /// </summary>
    public class GenericHistoryService : IGenericHistoryService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IDistributedIdService _distributedIdService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;
        private readonly IGenericHistoryServiceConfiguration _genericHistoryServiceConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericHistoryService" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="distributedIdService">The distributed identifier service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="genericHistoryServiceConfiguration">The generic history service configuration.</param>
        public GenericHistoryService(
            IDateTimeService dateTimeService,
            IDistributedIdService distributedIdService,
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService,
            IGenericHistoryServiceConfiguration genericHistoryServiceConfiguration)
        {
            _dateTimeService = dateTimeService;
            _distributedIdService = distributedIdService;
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
            _genericHistoryServiceConfiguration = genericHistoryServiceConfiguration;
        }

        /// <summary>
        /// Saves the specified history entry.
        /// </summary>
        public void Record(IHasDataSourceIdentifier dataSourceIdentifier, 
                            bool visibleToUsers,
                            XAct.Diagnostics.TraceLevel traceLevel,
                            IHasSerializedTypeValueAndMethod serializedTypeValueAndMethod,
                            Guid? applicationTennantId = null, 
            string tag=null)
        {
            if (!applicationTennantId.HasValue)
            {
                applicationTennantId = _applicationTennantService.Get();
            }


            GenericHistoryEntry historyEntry = new GenericHistoryEntry
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = applicationTennantId.Value,
                    DataSourceId = dataSourceIdentifier.DataSourceId,
                    DataSourceSerializedIdentities = dataSourceIdentifier.DataSourceSerializedIdentities,
                    TraceLevel = traceLevel,
                    VisibleToUsers = visibleToUsers,
                    SerializationMethod = serializedTypeValueAndMethod.SerializationMethod,
                    SerializedValueType = serializedTypeValueAndMethod.SerializedValueType,
                    SerializedValue = serializedTypeValueAndMethod.SerializedValue,
                    Tag = tag,
                    CreatedOnUtc = _dateTimeService.NowUTC
                };


            //Would need ref to XAct.Core -- and therefore FF.
            //Needs more work.
            //SerializationMethod serializationMethod =
            //    historyEntry.SerializeValue<TValue>(valueToRecord);

            _repositoryService.AddOnCommit(historyEntry);
        }


        /// <summary>
        /// Gets the specified history entries.
        /// </summary>
        /// <param name="dataSourceIdentifier">The data source identifier.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="includeApplicationEntries">if set to <c>true</c> [include application entries].</param>
        /// <returns></returns>
        public IQueryable<GenericHistoryEntry> Get(IHasDataSourceIdentifierReadOnly dataSourceIdentifier,
                                                   IPagedQuerySpecification pagedQuerySpecification,
                                                   Guid? applicationTennantId =null, bool includeApplicationEntries=true)
        {

            //If no applicationId, it's because we want to know everything:
            if (!applicationTennantId.HasValue)
            {
                return _repositoryService.GetByFilter<GenericHistoryEntry>(
                    x => 
                         (x.DataSourceId == dataSourceIdentifier.DataSourceId)
                         &&
                         (x.DataSourceSerializedIdentities == dataSourceIdentifier.DataSourceSerializedIdentities)
                    ,
                    null,
                    pagedQuerySpecification
                    );
            }

            if (!includeApplicationEntries)
            {
                return _repositoryService.GetByFilter<GenericHistoryEntry>(
                    x => (
                        (x.ApplicationTennantId == applicationTennantId)
                        )
                         &&
                         (x.DataSourceId == dataSourceIdentifier.DataSourceId)
                         &&
                         (x.DataSourceSerializedIdentities == dataSourceIdentifier.DataSourceSerializedIdentities)
                    ,
                    null,
                    pagedQuerySpecification
                    );
                
            }

            return _repositoryService.GetByFilter<GenericHistoryEntry>(
                x => (
                    (x.ApplicationTennantId == applicationTennantId)
                    ||
                    (x.ApplicationTennantId == Guid.Empty)
                    )
                     &&
                     (x.DataSourceId == dataSourceIdentifier.DataSourceId)
                     &&
                     (x.DataSourceSerializedIdentities == dataSourceIdentifier.DataSourceSerializedIdentities)
                ,
                null,
                pagedQuerySpecification
                );
        }
    }
}
