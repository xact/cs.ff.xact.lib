namespace XAct.History
{
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IGenericHistoryServiceConfiguration"/>
    /// to configure the <see cref="IGenericHistoryService"/>
    /// </summary>
    public class GenericHistoryServiceConfiguration : IHasXActLibServiceConfiguration, IGenericHistoryServiceConfiguration
    {
        
    }
}