﻿
using System.Globalization;
using XAct.Data;
using XAct.Services;
using XAct.Settings;

namespace XAct.History.Tests
{
    /// <summary>
    /// An implementation of the <see cref="IHistoryDatabaseSeedSettings"/>
    /// contract in order to provide a means to set -- in a DI/IoC way -- 
    /// settings needed by <c>HistoryRepositoryDbBootstrapper</c>
    /// to 
    /// </summary>
    [DefaultBindingImplementation(typeof(IHistoryDatabaseSeedSettings),BindingLifetimeType.SingletonScope)]
    public class HistoryDatabaseSeedSettings : DbRepositorySettings, IHistoryDatabaseSeedSettings
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="HistoryDatabaseSeedSettings"/> class.
        /// </summary>

        public HistoryDatabaseSeedSettings()
        {
            ConnectionStringSettingsName = ServiceLocatorService.Current.GetInstance<IHostSettingsService>().Get<string>(
                    "XActLibConnectionStringName", XAct.Library.Settings.Db.DefaultXActLibConnectionStringSettingsName);


            ParameterPrefix = XAct.Library.Settings.Db.DbParameterPrefix;
        }
    }

}
