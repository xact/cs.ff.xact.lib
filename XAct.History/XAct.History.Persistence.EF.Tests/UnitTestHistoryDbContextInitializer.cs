﻿using System.Data.Entity;
using System.Text;
using XAct.History;

namespace XAct.History.Tests
{
    /// <summary>
    /// The resourceDbInitializer invoked from the <see cref="UnitTestHistoryDbContext"/>.
    /// <para>
    /// Used by <see cref="HistoryEFDatabaseBootstrapper"/>
    /// to force creation of a Db.
    /// </para>
    /// </summary>
    public class UnitTestHistoryDbContextInitializer : DropCreateDatabaseIfModelChanges<UnitTestHistoryDbContext>
    {
        private readonly IHistoryDatabaseSeedSettings _repositorySettings;
        private readonly IHistoryDbContextSeeder _historyDbContextSeeder;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTestHistoryDbContextInitializer"/> class.
        /// </summary>
        /// <param name="repositorySettings">The repository settings.</param>
        /// <param name="historyDbContextSeeder">The history db context seeder.</param>
        public UnitTestHistoryDbContextInitializer(IHistoryDatabaseSeedSettings repositorySettings, IHistoryDbContextSeeder historyDbContextSeeder )
        {
            _repositorySettings = repositorySettings;

            _historyDbContextSeeder = historyDbContextSeeder;
        }

        /// <summary>
        /// Seeds the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        protected override void Seed(UnitTestHistoryDbContext context)
        {
            _historyDbContextSeeder.Seed(context);

            base.Seed(context);

        }
    }
}