using System.Data.Entity;
using XAct.Bootstrapper.Tests;
using XAct.Resources;
using XAct.Services;
using System.Linq;

namespace XAct.History.Tests
{
    /// <summary>
    /// Ensures a db exists for working with History entries.
    /// Implementation of <see cref="IUnitTestDatabaseBootstrapper"/>.
    /// </summary>
    [DefaultBindingImplementation(typeof(IUnitTestDatabaseBootstrapper),BindingLifetimeType.SingletonScope)]
    public class UnitTestDatabaseBootstrapper : IUnitTestDatabaseBootstrapper
    {
        private readonly IUnitTestDatabaseInitializer _unitTestDatabaseInitializer;

        public UnitTestDatabaseBootstrapper(IUnitTestDatabaseInitializer unitTestDatabaseInitializer)
        {
            _unitTestDatabaseInitializer = unitTestDatabaseInitializer;
        }

        public void Initialize()
        {

            //Set the Initialzer that will define the Db Model:
            Database.SetInitializer(_unitTestDatabaseInitializer);

            UnitTestHistoryDbContext dbContext = new UnitTestHistoryDbContext();

            DbSet<HistoryEntry> historyEntries = dbContext.Set<HistoryEntry>();

            //Force a query, which in turn forces creation of Db:
            historyEntries.FirstOrDefault();
        }
    }
}