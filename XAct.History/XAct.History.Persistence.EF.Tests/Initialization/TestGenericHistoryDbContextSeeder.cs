﻿namespace XAct.History
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct;
    using XAct.Data.DataSources;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.History.Initialization.DbContextSeeders;

    public class GenericHistoryDataSourceDbContextSeeder : XActLibDbContextSeederBase<DataSource>, IGenericHistoryDataSourceDbContextSeeder, IHasMediumBindingPriority
    {
        public GenericHistoryDataSourceDbContextSeeder(ITracingService tracingService) : base(tracingService)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.Name);
        } 

        public override void CreateEntities()
        {
            this.InternalEntities = new List<DataSource>();

            var ds = new DataSource
            {
                Id = 1.ToGuid(),
                Name = "Contacts",
                IsRemote = false,
                ConnectorClassification = "Db",
                ConnectionProviderType = null,
                ConnectionString = null,
                ConnectionMetaData = null,
                DataStoreCatalogName = null,
                DataStoreTableName = "Contacts",
                DisplayMetaData = null,
                DataStoreSerializedIdentifierNames = "123"
            };

            ds.Validate();



            this.InternalEntities.Add(ds);


        }
    }
}
