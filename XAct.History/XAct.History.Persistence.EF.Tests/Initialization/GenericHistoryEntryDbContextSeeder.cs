﻿namespace XAct.History
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.History.Initialization.DbContextSeeders;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class GenericHistoryEntryDbContextSeeder : XActLibDbContextSeederBase<GenericHistoryEntry>,
                                                      IGenericHistoryEntryDbContextSeeder
    {
        private readonly IDeviceInformationService _deviceInformationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericHistoryEntryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="deviceInformationService">The device information service.</param>
        /// <param name="genericHistoryDataSourceDbContextSeeder">The generic history data source database context seeder.</param>
        public GenericHistoryEntryDbContextSeeder(IDeviceInformationService deviceInformationService,
                                                  IGenericHistoryDataSourceDbContextSeeder
                                                      genericHistoryDataSourceDbContextSeeder) :
                                                          base(genericHistoryDataSourceDbContextSeeder)
        {

            _deviceInformationService = deviceInformationService;
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        /// <param name="dbContext">The context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.Id);
        }



        public override void CreateEntities()
        {
            this.InternalEntities = new List<GenericHistoryEntry>();

            //For DataSource1+123,Org=None:
            this.InternalEntities.Add(new GenericHistoryEntry
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = Guid.Empty, //General
                    TraceLevel = TraceLevel.Info,
                    DataSourceId = 1.ToGuid(),
                    DataSourceSerializedIdentities = "123",
                    SerializationMethod = SerializationMethod.String,
                    SerializedValue = "General System message (no OrgId, so Techies can read it)",
                    SerializedValueType = "System.String",
                    Tag = "SomeTag",
                    CreatedOnUtc = DateTime.UtcNow
                });


            //For DataSource1+123,Org=1:
            this.InternalEntities.Add(new GenericHistoryEntry
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = 1.ToGuid(), //Specific Org 
                    TraceLevel = TraceLevel.Info,
                    DataSourceId = 1.ToGuid(),
                    DataSourceSerializedIdentities = "123",
                    SerializationMethod = SerializationMethod.String,
                    SerializedValue = "Info specific to Org Id",
                    SerializedValueType = "System.String",
                    Tag = "SomeTag",
                    CreatedOnUtc = DateTime.UtcNow
                });


            //For DataSource1+123,Org=1
            this.InternalEntities.Add(new GenericHistoryEntry
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = 1.ToGuid(), //Specific Org
                    TraceLevel = TraceLevel.Info,
                    DataSourceId = 1.ToGuid(),
                    DataSourceSerializedIdentities = "123",
                    SerializationMethod = SerializationMethod.String,
                    SerializedValue = "Info specific to Org Id, again",
                    SerializedValueType = "System.String",
                    Tag = "SomeTag",
                    CreatedOnUtc = DateTime.UtcNow
                });



            //For DataSource1+123,Org=2:
            this.InternalEntities.Add(new GenericHistoryEntry
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = 2.ToGuid(), //Specific Org
                    TraceLevel = TraceLevel.Info,
                    DataSourceId = 1.ToGuid(),
                    DataSourceSerializedIdentities = "123",
                    SerializationMethod = SerializationMethod.String,
                    SerializedValue = "Info specific to Org Id, again",
                    SerializedValueType = "System.String",
                    Tag = "SomeTag",
                    CreatedOnUtc = DateTime.UtcNow
                });











            //Entity two:


            this.InternalEntities.Add(new GenericHistoryEntry
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = Guid.Empty,
                    TraceLevel = TraceLevel.Info,
                    DataSourceId = 1.ToGuid(),
                    DataSourceSerializedIdentities = "12345",
                    SerializationMethod = SerializationMethod.String,
                    SerializedValue = "General System Message",
                    SerializedValueType = "System.String",
                    Tag = "SomeTag",
                    CreatedOnUtc = DateTime.UtcNow
                });

            this.InternalEntities.Add(new GenericHistoryEntry
                {
                    Id = Guid.NewGuid(),
                    ApplicationTennantId = 2.ToGuid(),
                    TraceLevel = TraceLevel.Info,
                    DataSourceId = 1.ToGuid(),
                    DataSourceSerializedIdentities = "12345",
                    SerializationMethod = SerializationMethod.String,
                    SerializedValue = "private message for Org",
                    SerializedValueType = "System.String",
                    Tag = "SomeTag",
                    CreatedOnUtc = DateTime.UtcNow
                });


        }

    }
}