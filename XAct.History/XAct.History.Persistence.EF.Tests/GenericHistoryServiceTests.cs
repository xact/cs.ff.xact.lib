//using NUnit.Framework;

namespace XAct.History.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Data.DataSources;
    using XAct.History.Implementations;
    using XAct.Messages;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class GenericHistoryServiceTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();



            //DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>().FactoryDelegate = 
            //    () => new EntityDbContext(new UnitTestDbContext());

            ////Initialize the db using the common initializer passing it one entity from this class
            ////so that it can perform a query (therefore create db as necessary):
            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<HistoryEntry>();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetGenericHistoryServiceConfiguration()
        {
            IGenericHistoryServiceConfiguration genericHistoryServiceConfiguration =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryServiceConfiguration>();


            Assert.IsNotNull(genericHistoryServiceConfiguration);
        }

        [Test]
        public void GenericHistoryServiceIsAvailable()
        {

            IGenericHistoryService genericHistoryService =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryService>();

            Assert.IsTrue(genericHistoryService != null);
        }

        [Test]
        public void GenericHistoryServiceIsOfExpectedService()
        {
            IGenericHistoryService genericHistoryService =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryService>();

            Assert.AreEqual(typeof (GenericHistoryService), genericHistoryService.GetType());
        }



        [Test]
        public void CanGetEntryIfNoOrgSpecified()
        {
            IGenericHistoryService genericHistoryService =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryService>();

            //No AppHostId means, get everything:
            var historyEntry = genericHistoryService.Get(
                new DataSourceIdentifier(1.ToGuid(), "123"), 
                new PagedQuerySpecification(), null).ToArray();

            Assert.IsTrue(historyEntry.Count()>0);

            //As No org specified, get's 1+2+1:
            Assert.AreEqual(4, historyEntry.Count());
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == Guid.Empty));
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == 1.ToGuid()));
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == 2.ToGuid()));
        }

        [Test]
        public void CanGetEntriesSpecificToOrg()
        {
            IGenericHistoryService genericHistoryService =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryService>();


            var historyEntry = genericHistoryService.Get(new DataSourceIdentifier(1.ToGuid(), "123"), new PagedQuerySpecification(), 1.ToGuid()).ToArray();

            Assert.IsTrue(historyEntry.Count() > 0);
            //get's 1+2:
            Assert.AreEqual(3, historyEntry.Count());
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == Guid.Empty));
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == 1.ToGuid()));
            Assert.IsFalse(historyEntry.Any(x => x.ApplicationTennantId == 2.ToGuid()));
        }



        [Test]
        public void CanNotGetEntriesIfDifferentOrg()
        {
            IGenericHistoryService genericHistoryService =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryService>();


            var historyEntry = genericHistoryService.Get(new DataSourceIdentifier(1.ToGuid(), "123"), new PagedQuerySpecification(), 2.ToGuid()).ToArray();

            Assert.IsTrue(historyEntry.Count() > 0);
            //As different org, can only get 1+1:
            Assert.AreEqual(2,historyEntry.Count());
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == Guid.Empty));
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == 2.ToGuid()));
            Assert.IsFalse(historyEntry.Any(x => x.ApplicationTennantId == 1.ToGuid()));
        }














        [Test]
        public void CanGetEntriesForSecondaryElement()
        {
            IGenericHistoryService genericHistoryService =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryService>();

            var historyEntry = genericHistoryService.Get(new DataSourceIdentifier(1.ToGuid(), "12345"), new PagedQuerySpecification()).ToArray();

            Assert.IsTrue(historyEntry.Count() > 0);
            //As no org specified, gets 1+2:
            Assert.AreEqual(2, historyEntry.Count());
            Assert.IsTrue(historyEntry.Any(x=>x.ApplicationTennantId==Guid.Empty));
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == 2.ToGuid()));
            Assert.IsFalse(historyEntry.Any(x => x.ApplicationTennantId == 1.ToGuid()));
        }


        [Test]
        public void CanGetEntriesForSecondaryElementSpecificOrg()
        {
            IGenericHistoryService genericHistoryService =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryService>();


            var historyEntry = genericHistoryService.Get(new DataSourceIdentifier(1.ToGuid(), "12345"), new PagedQuerySpecification(), 2.ToGuid()).ToArray();


            Assert.IsTrue(historyEntry.Count() > 0);
            //As no org specified, gets 1+1:
            Assert.AreEqual(2, historyEntry.Count());
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == Guid.Empty));
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == 2.ToGuid()));
            Assert.IsFalse(historyEntry.Any(x => x.ApplicationTennantId == 1.ToGuid()));
        }


        [Test]
        public void CanNotGetEntriesForSecondaryElementwithDiffOrg()
        {
            IGenericHistoryService genericHistoryService =
                XAct.DependencyResolver.Current.GetInstance<IGenericHistoryService>();


            var historyEntry = genericHistoryService.Get(new DataSourceIdentifier(1.ToGuid(), "12345"), new PagedQuerySpecification(), 1.ToGuid()).ToArray();


            Assert.IsTrue(historyEntry.Count() > 0);
            //As no org specified, gets 1+0:
            Assert.AreEqual(1,historyEntry.Count());
            Assert.IsTrue(historyEntry.Any(x => x.ApplicationTennantId == Guid.Empty));
            Assert.IsFalse(historyEntry.Any(x => x.ApplicationTennantId == 2.ToGuid()));
            Assert.IsFalse(historyEntry.Any(x => x.ApplicationTennantId == 1.ToGuid()));
        }


    }
}
