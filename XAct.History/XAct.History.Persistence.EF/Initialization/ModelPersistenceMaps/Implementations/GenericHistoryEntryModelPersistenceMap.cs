﻿namespace XAct.History.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class GenericHistoryEntryModelPersistenceMap : EntityTypeConfiguration<GenericHistoryEntry>,
                                                                         IGenericHistoryEntryModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericHistoryEntryModelPersistenceMap"/> class.
        /// </summary>
        public GenericHistoryEntryModelPersistenceMap()
        {

            this.ToXActLibTable("GenericHistoryEntry");

            //The properties expression 'h => new <>f__AnonymousType0`2(TargetMachineId = h.MachineId, Id = h.Id)' is not valid. 
            //The expression should represent a property: C#: 't => t.MyProperty'  VB.Net: 'Function(t) t.MyProperty'. When specifying multiple properties use an anonymous type: C#: 't => new { t.MyProperty1, t.MyProperty2 }'  VB.Net: 'Function(t) New With { t.MyProperty1, t.MyProperty2 }'.
            this
                .HasKey(h => h.Id);



            int colOrder = 0;

            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++)
                ;
            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);
                ;

            this
                .Property(m => m.ApplicationTennantId)
                .DefineRequiredApplicationTennantId(colOrder++)
                ;

            //this
            //    .Property(m => m.DataSourceId)
            //    .IsRequired()
            //    .HasColumnOrder(colOrder++)
            //    ;
            this
                .Property(m => m.DataSourceSerializedIdentities)
                .IsRequired()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.TraceLevel)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.VisibleToUsers)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);

            this
                .Property(m => m.SerializationMethod)
                .DefineRequiredSerializationMethod(colOrder++);
                ;

            this
                .Property(m => m.SerializedValueType)
                .DefineRequired1024CharSerializationValueType(colOrder++);
                ;

            this
                .Property(m => m.SerializedValue)
                .DefineOptional4000CharSerializationValue(colOrder++);
            
            this
                .Property(m => m.Tag)
                .DefineOptional256CharTag(colOrder++)
                ;

            //A GenericHistory
            //references a DataSource.
            this
                .HasRequired(m => m.DataSource)
                .WithMany()
                .HasForeignKey(m => m.DataSourceId);

        }

    }


}