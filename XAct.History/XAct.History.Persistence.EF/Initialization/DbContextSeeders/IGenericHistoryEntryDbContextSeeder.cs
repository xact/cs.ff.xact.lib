namespace XAct.History.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{GenericHistoryEntry}"/>
    /// to seed the History tables with default data.
    /// </summary>
        //[Initializer("XActLib", "",InitializationStage.S03_Integration)]
    public interface IGenericHistoryEntryDbContextSeeder : IHasXActLibDbContextSeeder<GenericHistoryEntry>
    {
        
    }
}