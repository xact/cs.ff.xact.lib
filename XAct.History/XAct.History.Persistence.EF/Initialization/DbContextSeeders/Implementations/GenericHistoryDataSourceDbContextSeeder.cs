﻿namespace XAct.History.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.DataSources;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class GenericHistoryDataSourceDbContextSeeder : XActLibDbContextSeederBase<DataSource>,
                                                           IGenericHistoryDataSourceDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericHistoryEntryDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public GenericHistoryDataSourceDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
            
        }
    }
}