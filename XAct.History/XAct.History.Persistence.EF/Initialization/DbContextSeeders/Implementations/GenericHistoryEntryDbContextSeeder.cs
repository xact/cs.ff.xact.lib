﻿namespace XAct.History.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="IGenericHistoryEntryDbContextSeeder"/> contract
    /// to seed the History tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class GenericHistoryEntryDbContextSeeder : XActLibDbContextSeederBase<GenericHistoryEntry>, IGenericHistoryEntryDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericHistoryEntryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="genericHistoryDataSourceDbContextSeeder">The generic history data source database context seeder.</param>
        public GenericHistoryEntryDbContextSeeder(IGenericHistoryDataSourceDbContextSeeder
                                                      genericHistoryDataSourceDbContextSeeder) :
                                                          base(genericHistoryDataSourceDbContextSeeder)
        {
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
            
        }
    }
}
