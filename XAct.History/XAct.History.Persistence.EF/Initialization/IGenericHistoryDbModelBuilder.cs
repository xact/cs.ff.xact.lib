namespace XAct.History.Initialization
{
    using XAct.Initialization;

    /// <summary>
    /// 
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface IGenericHistoryDbModelBuilder : XAct.Data.EF.CodeFirst.IHasXActLibDbModelBuilder
    {

    }
}