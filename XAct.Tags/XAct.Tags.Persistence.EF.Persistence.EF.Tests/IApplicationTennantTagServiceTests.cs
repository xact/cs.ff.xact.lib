namespace XAct.Tags.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Tags.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IApplicationTennantTagServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetIApplicationTennantTagService()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantTagService>();
            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetIApplicationTennantTagServiceOfExpectedType()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantTagService>();
            Assert.AreEqual(typeof(ApplicationTennantTagService),service.GetType());
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTagsForCurrentApplicationTennant()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantTagService>();

            var results = service.GetTags().ToArray();

            Assert.IsTrue(results.Length>0);
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTagInCurrentApplicationTennantsTagCollection()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantTagService>();

            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(1.ToGuid());
            var result = service.GetTag("Tag1A");
            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(Guid.Empty);

            Assert.IsNotNull(result);
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanAddATagToCurrentApplicationTennantsTagCollection()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTagService>();

            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(1.ToGuid());
            var result = service.GetTag( "Tag1New");

            Assert.IsNull(result);

            result = service.AddTag("Tag1New");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            result = service.GetTag( "Tag1New");
            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(Guid.Empty);
            Assert.IsNotNull(result);
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanRemoveTagFromCurrentApplicationTennantsTagCollection()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTagService>();

            var result = service.GetTag("Tag1ToDelete");

            Assert.IsNull(result);

            result = service.AddTag("Tag1ToDelete");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            result = service.GetTag("Tag1ToDelete");
            Assert.IsNotNull(result);


            service.RemoveTag("Tag1ToDelete");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();


            result = service.GetTag("Tag1ToDelete");
            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(1.ToGuid());

            Assert.IsNull(result);

        }


    }
}


