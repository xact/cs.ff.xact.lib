namespace XAct.Tags.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Tags.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class TagManagementServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetITagService()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ITagManagementService>();
            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetITagServiceOfExpectedType()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagManagementService>();
            Assert.AreEqual(typeof(TagManagementService),service.GetType());
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTagsForAnApplicationTennant()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagManagementService>();

            var results = service.GetTags(1.ToGuid()).ToArray();

            Assert.IsTrue(results.Length>0);
            Assert.IsTrue(results.Contains(x => x.Title == "Tag1A"));
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTagInCurrentApplicationTennantsTagCollection()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagManagementService>();

            var result = service.GetTag(1.ToGuid(),"Tag1A");

            Assert.IsNotNull(result);
        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanAddATagToCurrentApplicationTennantsTagCollection()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagManagementService>();

            var result = service.GetTag(1.ToGuid(), "Tag1New");

            Assert.IsNull(result);

            result = service.AddTag(1.ToGuid(), "Tag1New");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            result = service.GetTag(1.ToGuid(), "Tag1New");
            Assert.IsNotNull(result);
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanRemoveTagFromCurrentApplicationTennantsTagCollection()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagManagementService>();

            var result = service.GetTag(1.ToGuid(), "Tag1New2");

            Assert.IsNull(result);

            result = service.AddTag(1.ToGuid(), "Tag1New2");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            result = service.GetTag(1.ToGuid(), "Tag1New2");
            Assert.IsNotNull(result);


            service.RemoveTag(1.ToGuid(),"Tag1New2");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();


            result = service.GetTag(1.ToGuid(), "Tag1New2");
            Assert.IsNull(result);

        }


    }
}


