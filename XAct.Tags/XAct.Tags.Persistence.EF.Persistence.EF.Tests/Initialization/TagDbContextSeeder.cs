﻿namespace XAct.Tags.Tests.Initialization
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct.Categorization;
    using XAct;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Library.Settings;
    using XAct.Tags.Initialization.DbContextSeeders;
    using global::XAct.Services;

    public class TagDbContextSeeder : XActLibDbContextSeederBase<Tag>, ITagDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IDistributedIdService _distributedIdService;
        private readonly IDateTimeService _dateTimeService;
        private readonly ITagCategoryDbContextSeeder _tagCategoryDbContextSeeder;


        public TagDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService, 
            IDistributedIdService distributedIdService,
            IDateTimeService dateTimeService,
            ITagCategoryDbContextSeeder tagCategoryDbContextSeeder
            )
            : base(tracingService)
        {

            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
            _distributedIdService = distributedIdService;
            _dateTimeService = dateTimeService;
            _tagCategoryDbContextSeeder = tagCategoryDbContextSeeder;
        }

        public override void SeedInternal(DbContext dbContext) 
        {
            SeedInternalHelper(dbContext,true,x=>x.Title);

        }

        public override void CreateEntities()
        {


            this.InternalEntities = new List<Tag>();

            var category = _tagCategoryDbContextSeeder.Entities.Single(x => x.Text == "CatA");

            this.InternalEntities.Add(new Tag
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = _applicationTennantService.Get(),
                    Enabled = true,
                    Title = "Tag0A",
                    Category = category,
                    CreatedBy = "foo",
                    CreatedOnUtc = _dateTimeService.NowUTC
                });

            this.InternalEntities.Add(new Tag
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = _applicationTennantService.Get(),
                    Enabled = true,
                    Title = "Tag0B",
                    Category = category,
                    CreatedBy = "foo",
                    CreatedOnUtc = _dateTimeService.NowUTC
                });



            this.InternalEntities.Add(new Tag
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = 1.ToGuid(),
                    Enabled = true,
                    Title = "Tag1A",
                    Category = category,
                    CreatedBy = "foo",
                    CreatedOnUtc = _dateTimeService.NowUTC
                });

            this.InternalEntities.Add(new Tag
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = 1.ToGuid(),
                    Enabled = true,
                    Title = "Tag1B",
                    Category = category,
                    CreatedBy = "foo",
                    CreatedOnUtc = _dateTimeService.NowUTC
                });




            this.InternalEntities.Add(new Tag
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = 1.ToGuid(),
                    Enabled = true,
                    Title = "Tag1C",
                    Category = category,
                    CreatedBy = "foo",
                    CreatedOnUtc = _dateTimeService.NowUTC
                });





            this.InternalEntities.Add(new Tag
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = 2.ToGuid(),
                    Enabled = true,
                    Title = "Tag2A",
                    Category = category,
                    CreatedBy = "foo",
                    CreatedOnUtc = _dateTimeService.NowUTC
                });

            this.InternalEntities.Add(new Tag
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = 2.ToGuid(),
                    Enabled = true,
                    Title = "Tag2B",
                    Category = category,
                    CreatedBy = "foo",
                    CreatedOnUtc = _dateTimeService.NowUTC
                });














            this.InternalEntities.Add(new Tag
                {
                    Id = _distributedIdService.NewGuid(),
                    ApplicationTennantId = 3.ToGuid(),
                    Enabled = true,
                    Title = "Tag3A",
                    Category = category,
                    CreatedBy = "foo",
                    CreatedOnUtc = _dateTimeService.NowUTC
                });

        }

    }

}
