﻿namespace XAct.Tags.Tests.Initialization
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Categorization;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Tags.Initialization.DbContextSeeders;

    public class TagCategoryDbContextSeeder : XActLibDbContextSeederBase<TagCategory>, ITagCategoryDbContextSeeder,
                                              IHasMediumBindingPriority
    {

        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IDistributedIdService _distributedIdService;
        private readonly IDateTimeService _dateTimeService;

        public TagCategoryDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
                                          IApplicationTennantService applicationTennantService, 
                                          IDistributedIdService distributedIdService,
                                          IDateTimeService dateTimeService
            )
            : base(tracingService)
        {

            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
            _distributedIdService = distributedIdService;
            _dateTimeService = dateTimeService;
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.Id);
        }



        public override void CreateEntities()
        {
            this.InternalEntities = new List<TagCategory>();

            var cat = new TagCategory
                {
                    Id = 1.ToGuid(),
                    ApplicationTennantId = _applicationTennantService.Get(),
                    Enabled = true,
                    Text = "CatA",
                };
            this.InternalEntities.Add(cat);

            var cat2 = new TagCategory
                {
                    Id = 2.ToGuid(),
                    ApplicationTennantId = _applicationTennantService.Get(),
                    Enabled = true,
                    Text = "CatB",
                };
            this.InternalEntities.Add(cat2);



        }



    }
}