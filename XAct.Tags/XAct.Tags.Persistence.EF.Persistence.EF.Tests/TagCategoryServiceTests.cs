namespace XAct.Tags.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Categorization;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Tags.Services;
    using XAct.Tags.Services.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class TagCategoryServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetTagCategoryService()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<ITagCategoryManagementService>();
            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetTagCategoryServiceOfExpectedType()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagCategoryManagementService>();
            Assert.AreEqual(typeof(TagCategoryManagementService),service.GetType());
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTagsForCurrentApplicationTennant()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagCategoryManagementService>();

            var results = service.Retrieve(new PagedQuerySpecification(0,20));

            Assert.IsTrue(results.Any());
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetACategoryById()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagCategoryManagementService>();

            var result = service.GetById(1.ToGuid());

            Assert.IsNotNull(result);
            Assert.AreEqual("CatA", result.Text);
        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetACategoryByText()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagCategoryManagementService>();

            var result = service.GetByText("CatA");

            Assert.IsNotNull(result);
            Assert.AreEqual("CatA", result.Text);
        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanAddANewTag()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagCategoryManagementService>();
            var cat = new TagCategory
                {
                    ApplicationTennantId = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>().Get(),
                    Text = "Miao"
                };
            Console.WriteLine(cat.ApplicationTennantId);
            service.PersistOnCommit(cat);
            IUnitOfWorkService unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
            unitOfWorkService.GetCurrent().Commit();

            var result = service.GetByText("Miao");
            Assert.IsNotNull(result);
            Assert.AreEqual("Miao", result.Text);

        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanRemoveATagCategory()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ITagCategoryManagementService>();
            var cat = new TagCategory
            {
                ApplicationTennantId = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>().Get(),
                Text = "Miao2"
            };
            service.PersistOnCommit(cat);
            IUnitOfWorkService unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
            unitOfWorkService.GetCurrent().Commit();

            TagCategory result = service.GetByText("Miao2");
            Assert.IsNotNull(result);
            Assert.AreEqual("Miao2", result.Text);


            service.DeleteOnCommit(cat);
            unitOfWorkService.GetCurrent().Commit();

            result = service.GetByText("Miao2");
            Assert.IsNull(result);


        }


    }
}


