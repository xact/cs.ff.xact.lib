namespace XAct.Tags.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Tags.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class ApplicationTagServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetITagService()
        {

            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTagService>();
            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetITagServiceOfExpectedType()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTagService>();
            Assert.AreEqual(typeof(ApplicationTagService),service.GetType());
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTagsForAnApplicationTennant()
        {
            
            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTagService>();

            //Set the Application Id = Empty:
            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(Guid.Empty);
            //And get Tags:
            var results = service.GetTags().ToArray();
            //Should be at least one:
            Assert.IsTrue(results.Length>0);
            Assert.IsTrue(results.Contains(x => x.Title == "Tag0A"));
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanGetTagInCurrentApplicationTennantsTagCollection()
        {
            
            //Set current App to 1:
            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(1.ToGuid());


            //Application Tag alwasy gets Tags where ApplciationTennantId = Guid.Empty,
            //no matter what ApplicationHost is set to (ie, it's Tags for the whole Application)
            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTagService>();
            var result = service.GetTag("Tag0A");
            
            //Reset:
            try
            {
                Assert.IsNotNull(result);
            }
            catch
            {
                throw;
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(Guid.Empty);
            }
        }

        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanAddATagToCurrentApplicationTennantsTagCollection()
        {

            //Application Tag alwasy gets Tags where ApplciationTennantId = Guid.Empty,
            //no matter what ApplicationHost is set to (ie, it's Tags for the whole Application)
            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(1.ToGuid());

            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTagService>();
            var result = service.GetTag("Tag0New");
            //As we are using ApplicationTagService, the following will not exist yet:
            try
            {
                Assert.IsNull(result);
            }
            catch
            {
                throw;
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(Guid.Empty);
            }
            //Add it, using ApplciationTag, which therefore means we are adding it to with a AppHostId = Guid.Empty
            result = service.AddTag("Tag0New");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            //Get it:
            result = service.GetTag( "Tag0New");
            //Reset it back for next test:
            try
            {
                Assert.IsNotNull(result);
            }
            catch
            {
                throw;
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(Guid.Empty);
            }
        }


        [Test]
        [InitializeUnitTestDbIoCContextAttribute(true)]
        public void CanRemoveTagFromCurrentApplicationTennantsTagCollection()
        {
            XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(1.ToGuid());

            var service = XAct.DependencyResolver.Current.GetInstance<IApplicationTagService>();

            var result = service.GetTag("Tag0ToDelete");
            //Doesn't yest yet:
            Assert.IsNull(result);

            //So Add it first:
            result = service.AddTag("Tag0ToDelete");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            //Get it again:
            result = service.GetTag("Tag0ToDelete");
            //Thsi time it does exist:
            try
            {
                Assert.IsNotNull(result);
            }
            catch
            {
                throw;
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(Guid.Empty);
            }

            //Lets delete it:
            service.RemoveTag("Tag0ToDelete");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();


            result = service.GetTag("Tag0ToDelete");

            try
            {
                Assert.IsNull(result);
            }
            catch
            {
                throw;
            }
            finally
            {
                XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>().Set(Guid.Empty);

            }

        }


    }
}


