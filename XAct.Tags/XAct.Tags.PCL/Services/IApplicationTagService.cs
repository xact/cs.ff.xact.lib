﻿namespace XAct.Tags
{
    using System.Linq;
    using XAct.Categorization;

    /// <summary>
    /// Contract for a service to manage <see cref="Tag"/> models
    /// for the current <c>ApplicationHost</c>.
    /// </summary>
    public interface IApplicationTagService : IHasXActLibService
    {
        /// <summary>
        /// Gets all the tags for the application.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        IQueryable<Tag> GetTags(bool enabled = true);


        /// <summary>
        /// Adds the new <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        Tag AddTag(string tag, bool enabled = true);

        /// <summary>
        /// Get a single <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        Tag GetTag(string tag);

        /// <summary>
        /// Removes tags from the current <c>ApplicationTennant</c>
        /// with the given tag title.
        /// </summary>
        /// <param name="tag">The tag.</param>
        void RemoveTag(string tag);


        /// <summary>
        /// Removes all tags from the specified application tennant.
        /// </summary>
        /// <returns></returns>
        void RemoveAllTags();
    }
}
