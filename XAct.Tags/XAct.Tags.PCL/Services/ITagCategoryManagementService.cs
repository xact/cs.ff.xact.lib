﻿namespace XAct.Tags.Services
{
    using System;
    using XAct.Categorization;
    using XAct.Domain.Repositories;

    /// <summary>
    /// 
    /// </summary>
    public interface ITagCategoryManagementService :  IReferenceDataRepository<TagCategory,Guid>, IHasXActLibService
    {
    }
}