﻿namespace XAct.Tags.Implementations
{
    using System;
    using System.Linq;
    using XAct.Categorization;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="ITagManagementService"/>
    /// to manage <see cref="Tag"/> models.
    /// </summary>
    public class ApplicationTagService : XActLibServiceBase, IApplicationTagService
    {
        private readonly TagManagementService _tagService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationTagService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="tagService">The tag service.</param>
        public ApplicationTagService(
            ITracingService tracingService,
            TagManagementService tagService):base(tracingService)
        {
            _tagService = tagService;
        }

        /// <summary>
        /// Gets the tags.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        public IQueryable<Tag> GetTags(bool enabled = true)
        {
            Guid applicationTennantId = Guid.Empty;

            return _tagService.GetTags(applicationTennantId, enabled);
                
        }

        /// <summary>
        /// Adds the new <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        public Tag AddTag(string tag, bool enabled=true)
        {
            Guid applicationTennantId = Guid.Empty;

            return _tagService.AddTag(applicationTennantId, tag, enabled);
        }


        /// <summary>
        /// Get a single <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        public Tag GetTag(string tag)
        {
            Guid applicationTennantId = Guid.Empty;

            return _tagService.GetTag(applicationTennantId,tag);

        }


        /// <summary>
        /// Removes tags from the current <c>ApplicationTennant</c>
        /// with the given tag title.
        /// </summary>
        /// <param name="tag">The tag.</param>
        public void RemoveTag(string tag)
        {
            Guid applicationTennantId = Guid.Empty;

            _tagService.RemoveTag(applicationTennantId,tag);
        }

        /// <summary>
        /// Removes all tags from the specified application tennant.
        /// </summary>
        /// <returns></returns>
        public void RemoveAllTags()
        {

            Guid applicationTennantId = Guid.Empty;

            _tagService.RemoveAllTags(applicationTennantId);

        }

    }
}