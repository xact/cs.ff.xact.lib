﻿namespace XAct.Tags.Services.Implementations
{
    using XAct.Categorization;
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;

    /// <summary>
    /// Contract to manage <see cref="TagCategory"/> elements
    /// used by <see cref="ITagManagementService"/>
    /// </summary>
    public class TagCategoryManagementService :
        ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase<TagCategory>,
        ITagCategoryManagementService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TagCategoryManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public TagCategoryManagementService(ITracingService tracingService,
                                            IApplicationTennantService applicationTennantService,
                                            IRepositoryService repositoryService) :
                                                base(tracingService, applicationTennantService, repositoryService)
        {

        }

    }
}
