﻿namespace XAct.Tags.Implementations
{
    using System;
    using System.Linq;
    using XAct.Categorization;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="ITagManagementService"/>
    /// to manage <see cref="Tag"/> models.
    /// </summary>
    public class ApplicationTennantTagService : XActLibServiceBase, IApplicationTennantTagService
    {
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly TagManagementService _tagService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationTagService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant tag service.</param>
        /// <param name="tagService">The tag service.</param>
        public ApplicationTennantTagService(
            ITracingService tracingService,
            IApplicationTennantService applicationTennantService,
            TagManagementService tagService)
            : base(tracingService)
        {
            _applicationTennantService = applicationTennantService;
            _tagService = tagService;
        }

        /// <summary>
        /// Gets the tags.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        /// <returns></returns>
        public IQueryable<Tag> GetTags(bool enabled = true, bool includeApplicationTags = false)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            return _tagService.GetTags(applicationTennantId, enabled);

        }

        /// <summary>
        /// Adds the new <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="isApplicationShared">if set to <c>true</c> [is application shared].</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        public Tag AddTag(string tag, bool isApplicationShared=false, bool enabled = true)
        {
            Guid applicationTennantId = (isApplicationShared) ? Guid.Empty : _applicationTennantService.Get();

            return _tagService.AddTag(applicationTennantId, tag, enabled);
        }


        /// <summary>
        /// Get a single <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        /// <returns></returns>
        public Tag GetTag(string tag, bool includeApplicationTags = false)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            return _tagService.GetTag(applicationTennantId, tag, includeApplicationTags);

        }


        /// <summary>
        /// Removes tags from the current <c>ApplicationTennant</c>
        /// with the given tag title.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        public void RemoveTag(string tag, bool includeApplicationTags = false)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            _tagService.RemoveTag(applicationTennantId, tag, includeApplicationTags);
        }

        /// <summary>
        /// Removes all tags from the specified application tennant.
        /// </summary>
        /// <param name="includeApplicationTags"></param>
        public void RemoveAllTags(bool includeApplicationTags = false)
        {

            Guid applicationTennantId = _applicationTennantService.Get();

            _tagService.RemoveAllTags(applicationTennantId, includeApplicationTags);

        }

    }
}