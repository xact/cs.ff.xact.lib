﻿namespace XAct.Tags.Implementations
{
    using System;
    using System.Linq;
    using XAct.Categorization;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="ITagManagementService"/>
    /// to manage <see cref="Tag"/> models.
    /// </summary>
    public class TagManagementService : ITagManagementService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IPrincipalService _principalService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagManagementService" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public TagManagementService(IDateTimeService dateTimeService,
                          IPrincipalService principalService,
                          IRepositoryService repositoryService)
        {
            _dateTimeService = dateTimeService;
            _principalService = principalService;
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Gets the tags for the current ApplicationTennantId.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        /// <returns></returns>
        public IQueryable<Tag> GetTags(Guid applicationTennantId, bool enabled = true,
                                                  bool includeApplicationTags = false)
        {
            if (includeApplicationTags)
            {
                return _repositoryService.GetByFilter<Tag>(
                    x => (
                             (
                                 x.ApplicationTennantId == applicationTennantId
                                 ||
                                 x.ApplicationTennantId == Guid.Empty
                             )
                             &&

                             (x.Enabled == enabled)
                         ),
                    null,
                    null,
                    x => x.OrderBy(y => y.Order));
            }
            else
            {
                return _repositoryService.GetByFilter<Tag>(
                    x => (
                             (
                                 x.ApplicationTennantId == applicationTennantId
                             )
                             &&

                             (x.Enabled == enabled)
                         ),
                    null,
                    null,
                    x => x.OrderBy(y => y.Order));
            }

        }

        /// <summary>
        /// Adds the new <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <returns></returns>
        public Tag AddTag(Guid applicationTennantId, string tag, bool enabled = true)
        {


            Tag distributedTag = new Tag
                {
                    ApplicationTennantId = applicationTennantId,
                    Title = tag,
                    Enabled = enabled,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    CreatedBy = _principalService.CurrentIdentityIdentifier,
                };

            _repositoryService.PersistOnCommit<Tag, Guid>(distributedTag, true);

            return distributedTag;
        }


        /// <summary>
        /// Get a single <see cref="Tag"/>.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        /// <returns></returns>
        public Tag GetTag(Guid applicationTennantId, string tag, bool includeApplicationTags = false)
        {
            if (includeApplicationTags)
            {
                return _repositoryService.GetSingle<Tag>(
                    x => (
                             x.ApplicationTennantId == applicationTennantId
                             ||
                             x.ApplicationTennantId == Guid.Empty
                         )
                         &&
                         x.Title == tag);
            }
            else
            {
                return _repositoryService.GetSingle<Tag>(
                    x => (
                             x.ApplicationTennantId == applicationTennantId
                         )
                         &&
                         x.Title == tag);

            }
        }

        /// <summary>
        /// Removes tags from the current <c>ApplicationTennant</c>
        /// with the given tag title.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        public
            void RemoveTag(Guid applicationTennantId, string tag, bool includeApplicationTags = false)
        {
            if (includeApplicationTags)
            {
                _repositoryService.DeleteOnCommit<Tag>(
                    x =>
                    (
                        x.ApplicationTennantId == applicationTennantId
                        ||
                        x.ApplicationTennantId == Guid.Empty
                    )
                    &&
                    x.Title == tag);
            }
            else
            {
                _repositoryService.DeleteOnCommit<Tag>(
                    x =>
                    (
                        x.ApplicationTennantId == applicationTennantId
                    )
                    &&
                    x.Title == tag);
            }
        }

        /// <summary>
        /// Removes all tags from the specified application tennant.
        /// </summary>
        /// <param name="applicationTennantId">The application identifier.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        public
            void RemoveAllTags(Guid applicationTennantId, bool includeApplicationTags = false)
        {
            if (includeApplicationTags)
            {
                _repositoryService.DeleteOnCommit<Tag>(
                    x => (
                             x.ApplicationTennantId == applicationTennantId
                             ||
                             x.ApplicationTennantId == Guid.Empty
                         )
                    );

            }
            else
            {
                _repositoryService.DeleteOnCommit<Tag>(
                    x => x.ApplicationTennantId == applicationTennantId);
            }
        }
    }

}
