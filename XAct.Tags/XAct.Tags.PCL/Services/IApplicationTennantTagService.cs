﻿namespace XAct.Tags
{
    using System.Linq;
    using XAct.Categorization;

    /// <summary>
    /// Contract for a service to manage <see cref="Tag"/> models
    /// for the current <c>ApplicationHost</c>.
    /// </summary>
    public interface IApplicationTennantTagService : IHasXActLibService
    {
        /// <summary>
        /// Gets all the tags for the application.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        /// <returns></returns>
        IQueryable<Tag> GetTags(bool enabled = true, bool includeApplicationTags = false);


        /// <summary>
        /// Adds the new <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="isApplicationShared">if set to <c>true</c> [is application shared].</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        Tag AddTag(string tag, bool isApplicationShared=false, bool enabled = true);

        /// <summary>
        /// Get a single <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        /// <returns></returns>
        Tag GetTag(string tag, bool includeApplicationTags = false);

        /// <summary>
        /// Removes tags from the current <c>ApplicationTennant</c>
        /// with the given tag title.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        void RemoveTag(string tag, bool includeApplicationTags = false);


        /// <summary>
        /// Removes all tags from the specified application tennant.
        /// </summary>
        /// <returns></returns>
        void RemoveAllTags(bool includeApplicationTags=false);
    }
}
