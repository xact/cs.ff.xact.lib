﻿namespace XAct.Tags
{
    using System;
    using System.Linq;
    using XAct.Categorization;

    /// <summary>
    /// Contract for a service to manage <see cref="Tag"/> models.
    /// </summary>
    public interface ITagManagementService : IHasXActLibService
    {
        /// <summary>
        /// Gets all the tags for the application.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        /// <returns></returns>
        IQueryable<Tag> GetTags(Guid applicationTennantId, bool enabled = true, bool includeApplicationTags = false);


        /// <summary>
        /// Adds the new <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <returns></returns>
        Tag AddTag(Guid applicationTennantId, string tag, bool enabled = true);

        /// <summary>
        /// Get a single <see cref="Tag" />.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        /// <returns></returns>
        Tag GetTag(Guid applicationTennantId, string tag, bool includeApplicationTags = false);

        /// <summary>
        /// Removes tags from the current <c>ApplicationTennant</c>
        /// with the given tag title.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        void RemoveTag(Guid applicationTennantId, string tag, bool includeApplicationTags = false);


        /// <summary>
        /// Removes all tags from the specified application tennant.
        /// </summary>
        /// <param name="applicationTennantIdentifier">The application identifier.</param>
        /// <param name="includeApplicationTags">if set to <c>true</c> [include application tags].</param>
        void RemoveAllTags(Guid applicationTennantIdentifier, bool includeApplicationTags = false);
    }
}
