﻿namespace XAct.Tags.Initialization.DbContextSeeders.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Categorization;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;


    /// <summary>
    /// 
    /// </summary>
    public class TagCategoryDbContextSeeder : XActLibDbContextSeederBase<TagCategory>, ITagCategoryDbContextSeeder
    {
          /// <summary>
        /// Initializes a new instance of the <see cref="TagDbContextSeeder"/> class.
        /// </summary>
        public TagCategoryDbContextSeeder()
            : base()
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        /// <param name="dbContext">The context.</param>
        public override void SeedInternal(DbContext dbContext) 
        {

            SeedInternalHelper(dbContext,true,x=>x.Id);

        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
            this.InternalEntities = new List<TagCategory>();

            this.InternalEntities.Add(new TagCategory { Id = Guid.Empty, Text = "Default" });

        }
    }

    /// <summary>
    /// A default implementation of the <see cref="ITagDbContextSeeder"/> contract
    /// to seed the HostSettings tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class TagDbContextSeeder : XActLibDbContextSeederBase<Tag>, ITagDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TagDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tagCategoryDbContextSeeder">The tag category database context seeder.</param>
        public TagDbContextSeeder(ITagCategoryDbContextSeeder tagCategoryDbContextSeeder)
            : base(tagCategoryDbContextSeeder)
        {
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }

}
