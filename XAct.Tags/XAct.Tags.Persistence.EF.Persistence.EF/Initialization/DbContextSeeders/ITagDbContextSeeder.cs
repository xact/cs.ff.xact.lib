namespace XAct.Tags.Initialization.DbContextSeeders
{
    using XAct.Categorization;
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Tag}"/>
    /// to seed the HostSettigns tables with default data.
    /// </summary>
        //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface ITagDbContextSeeder : IHasUnitTestXActLibDbContextSeeder<Tag>
    {

    }
}