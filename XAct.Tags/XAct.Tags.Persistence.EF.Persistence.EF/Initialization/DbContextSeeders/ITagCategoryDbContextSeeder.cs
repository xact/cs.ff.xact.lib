namespace XAct.Tags.Initialization.DbContextSeeders
{
    using XAct.Categorization;
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// 
    /// </summary>
    public interface ITagCategoryDbContextSeeder : IHasUnitTestXActLibDbContextSeeder<TagCategory>
    {

    }
}