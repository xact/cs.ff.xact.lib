﻿namespace XAct.Tags.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Categorization;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    /// 
    public class TagModelPersistenceMap : EntityTypeConfiguration<Tag>, ITagModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TagModelPersistenceMap"/> class.
        /// </summary>
        public TagModelPersistenceMap()
        {
            this.ToXActLibTable("Tag");
            


            this
                .HasKey(m => m.Id);

            int colOrder = 0;
            int indexMember = 1; //Indexs of db's are 1 based.


            this.Property(x => x.ApplicationTennantId)
                .IsRequired()
                //.HasMaxLength(32)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Tag", indexMember++) {IsUnique = true}))
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Title", 1) {IsUnique = true}))
                ;

            this.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Tag", indexMember++) {IsUnique = true}))
                ;

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);



            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.Order)
                .DefineRequiredOrder(colOrder++);


            //RESET:
            indexMember = 1;

            this.Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(32)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Title", 2) {IsUnique = true}))
                ;



            this.Property(x => x.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);

            this.Property(x => x.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++);


            //Will make it a bit difficult to create a first tag,
            //but once there's a Category, one can start happily
            //adding new ones. Suggest adding a Cat with Guid.Empty
            //to start with.
            this.HasOptional(x => x.Category)
                .WithMany();
        }

    }
}