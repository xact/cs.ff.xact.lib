﻿namespace XAct.Tags.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Categorization;
    using XAct.Library.Settings;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    //[DefaultBindingImplementation(typeof(ITagCategoryModelPersistenceMap), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class TagCategoryModelPersistenceMap : EntityTypeConfiguration<TagCategory>,
        ITagCategoryModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TagModelPersistenceMap"/> class.
        /// </summary>
        public TagCategoryModelPersistenceMap()
        {
            this.ToXActLibTable("Category");

            int colOrder = 0;
            int indexMember = 1; //Indexs of db's are 1 based.

            this
                .HasKey(m => m.Id);

            this.Property(x => x.ApplicationTennantId)
                .IsRequired()
                //.HasMaxLength(32)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Name", indexMember++) { IsUnique = true }))
                ;

            this.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.Order)
                .DefineRequiredOrder(colOrder++);

            this.Property(x => x.Text)
                .IsRequired()
                .HasMaxLength(32)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Name", indexMember++) { IsUnique = true }))
                ;

        }

    }
}