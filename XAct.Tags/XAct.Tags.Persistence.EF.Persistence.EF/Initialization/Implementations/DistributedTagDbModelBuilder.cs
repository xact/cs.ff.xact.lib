﻿// ReSharper disable CheckNamespace

namespace XAct.Tags.Initialization.Implementations
// ReSharper restore CheckNamespace
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Categorization;
    using XAct.Diagnostics;
    using XAct.Tags.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// Implementation of <see cref="IDistributedTagDbModelBuilder"/>
    /// in order to create HostSettings capabilities in a CodeFirst managed Db.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class DistributedTagDbModelBuilder : IDistributedTagDbModelBuilder
    {
        private readonly ITracingService _tracingService;
        private readonly string _typeName;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributedTagDbModelBuilder"/> class.
        /// </summary>
        public DistributedTagDbModelBuilder(ITracingService tracingService)
        {
            _tracingService = tracingService;
            _typeName = this.GetType().Name;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<Tag>)XAct.DependencyResolver.Current.GetInstance<ITagModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<TagCategory>)XAct.DependencyResolver.Current.GetInstance<ITagCategoryModelPersistenceMap>());
            
        }
    }
}

