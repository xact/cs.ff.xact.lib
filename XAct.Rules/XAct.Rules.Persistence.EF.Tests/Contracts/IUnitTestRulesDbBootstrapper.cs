﻿using XAct.Initialization;

namespace XAct.Rules.Tests
{
    /// <summary>
    /// Contract for a class to ensure the creation of 
    /// a db and required Table.
    /// </summary>
    public interface IUnitTestRulesDbBootstrapper :IInitializable
    {
    }
}