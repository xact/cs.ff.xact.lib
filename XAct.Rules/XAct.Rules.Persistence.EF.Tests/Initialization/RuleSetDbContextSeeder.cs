namespace XAct.Rules.Tests
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Languages;
    using XAct.Rules.Initialization.DbContextSeeders;

    public class RuleSetDbContextSeeder : XActLibDbContextSeederBase<RuleSet>, IRuleSetDbContextSeeder,
                                          IHasMediumBindingPriority
    {
        private readonly IApplicationTennantService _applicationTennantService;

        public RuleSetDbContextSeeder(ITracingService tracingService, IApplicationTennantService applicationTennantService)
            : base(tracingService)
        {
            _applicationTennantService = applicationTennantService;
        }

        public override void CreateEntities()
        {
            Create_RuleSet_One_CSharpBased_For_ATestBusinessDomainEntity();

            Create_RuleSet_Two_CSharpBased_For_ATestBusinessDomainEntity();
        }

        private void Create_RuleSet_One_CSharpBased_For_ATestBusinessDomainEntity()
        {
            //Create a RuleSet, and add Rules to it:

            RuleSet ruleSet = new RuleSet();


            ruleSet.Id = 1.ToGuid();

            //Define that the RuleSet contains CSharp rules, 
            //where each rule is intended to be used against 
            //a domain of type 'ATestBusinessDomainEntity'
            ruleSet.ApplicationTennantId = _applicationTennantService.Get();

            ruleSet.ScriptHostType = ScriptHostType.CSharp;

            //Some minor stuff:
            ruleSet.Enabled = true;
            ruleSet.Name = "RuleUno";
            ruleSet.Description = "...someDescription...";

            ruleSet.TargetIsRequired = true;
            //The rule is meant to check the following assembly.
            ruleSet.TargetType = typeof (ATestBusinessDomainEntity);
            //and it will need the followind dependencies:
            ruleSet.Assemblies.Add(new RuleSetAssembly(ruleSet.TargetType.Assembly));


            this.InternalEntities.Add(ruleSet);
        }


                //CreateViewModeTestEntry
        private void Create_RuleSet_Two_CSharpBased_For_ATestBusinessDomainEntity()
        {
            //Create a RuleSet, and Add Rules to it:

            RuleSet ruleSet = new RuleSet();

            ruleSet.Id = 2.ToGuid();

            ruleSet.ScriptHostType = ScriptHostType.CSharp;
            ruleSet.TargetIsRequired = true;
            ruleSet.TargetType = typeof (ATestBusinessDomainEntity);
            ruleSet.Assemblies.Add(new RuleSetAssembly(ruleSet.TargetType.Assembly));
            ruleSet.Enabled = true;
            ruleSet.Name = "ViewRuleUno";
            ruleSet.Description = "...someDescription...";



            this.InternalEntities.Add(ruleSet);
        }


    }
}