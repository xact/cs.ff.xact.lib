namespace XAct.Rules.Tests
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Languages;
    using XAct.Rules.Initialization.DbContextSeeders;
    using XAct.Services;


    /// <summary>
    /// 
    /// </summary>
    public class RuleDbContextSeeder : XActLibDbContextSeederBase<Rule>, IRuleDbContextSeeder, IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="ruleSetDbContextSeeder">The rule set database context seeder.</param>
        public RuleDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
                                   IApplicationTennantService applicationTennantService, IRuleSetDbContextSeeder ruleSetDbContextSeeder)
            : base(tracingService, ruleSetDbContextSeeder)
        {
        }


        /// <summary>
        /// Builds the resource entries.
        /// </summary>
        public override void CreateEntities()
        {
            this.InternalEntities = new List<Rule>();

            Create_Rules_For_RuleSet_One_CSharpBased_For_ATestBusinessDomainEntity();
            Create_Rules_For_RuleSet_Two_CSharpBased_For_ATestBusinessDomainEntity();

        }

        private void Create_Rules_For_RuleSet_One_CSharpBased_For_ATestBusinessDomainEntity()
        {
            //Add a couple of rules, that contain some C#,
            //that test the given domain entity (we specified is ATestBusinessDomainEntity) 
            this.InternalEntities.Add(
                new Rule
                    {
                        Enabled = true,
                        Order = 0,
                        RuleSetFK = 1.ToGuid(),
                        Name = "Rule",
                        Description = null,
                        Script =
                            @"
RuleSetTarget Target = Context[""Target""] as RuleSetTarget;
ATestBusinessDomainEntity item = Target.Item as ATestBusinessDomainEntity;

if (item.Name == ""John"")
{
    item.Amount = 1000.12m;
    Target.Matched();
}
else 
{
    item.Amount = -999.89m;
    item.Name = ""Was!John"";
    //Ensure that both if and else are marked as Matched if you want it to qualify as Last.
    //Or Use Target.Exit();
    Target.Matched();
}
"
                    }
                );


        }



























        //CreateViewModeTestEntry
        private void Create_Rules_For_RuleSet_Two_CSharpBased_For_ATestBusinessDomainEntity()
        {

            this.InternalEntities.Add(
                new Rule
                    {
                        Enabled = true,
                        Order = 0,
                        RuleSetFK = 2.ToGuid(),
                        Name = "A Rule",
                        Description = null,
                        Script =
                            @"
//The following has to be done every time:
RuleSetTarget Target = Context[""Target""] as RuleSetTarget;
ViewModeCacheItem item = Target.Item as ViewModeCacheItem;

if (item.Key == ""TestCase:TestClassDef:TestClassProperty:"")
{
    if (Target.ItemData['ViewMode']==""Edit""){
      Target.Result = ViewMode.ReadOnly;
      Target.Matched();
    }
}
"
                    }
                );

        }

    }
}