namespace XAct.Rules.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Rules.Implementations;
    using XAct.Tests;


    [TestFixture]
    public class RuleSetServiceTests
    {


        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void Can_Get_RuleSetService()
        {

            IRuleSetService ruleSetService = XAct.DependencyResolver.Current.GetInstance<IRuleSetService>();

            Assert.IsNotNull(ruleSetService);
        }



        [Test]
        public void Can_Get_RuleSetService_Of_Expected_Type()
        {

            IRuleSetService ruleSetService = XAct.DependencyResolver.Current.GetInstance<IRuleSetService>();

            Assert.AreEqual(typeof(RuleSetService), ruleSetService.GetType());
        }


























        [Test]
        public void Apply_RuleSetOne_On_A_BusinessEntity_And_Ensure_The_RuleSetProcessedTheEntity()
        {

            IRuleSetService ruleSetService = XAct.DependencyResolver.Current.GetInstance<IRuleSetService>();

            //Create a domain Entity of some kind, so we can test it
            ATestBusinessDomainEntity ruleTestEntity = new ATestBusinessDomainEntity { Name = "Joe", Amount = 123.45m };

            //WRap the business domain entity as a RuleSetTarget
            //Which is our way of passing it across the Host/Script layer: 
            RuleSetTarget<ATestBusinessDomainEntity> target = new 
                RuleSetTarget<ATestBusinessDomainEntity>(ruleTestEntity);

            Assert.IsFalse(target.Processed, "target.Processed should start off as false");

            //Apply ruleset (with id=1) on the given business domain entity.
            ruleSetService.ApplyRuleSetOn(1.ToGuid(), target);


            //Proof that script was created in Db, picked back up as a ruleset,
            //and that Rule was found...
            Console.WriteLine(target.Error);
            Assert.IsNullOrEmpty(target.Error,"Should be no error.");

            /*
             Rule 1.1 is the following:
                if (item.Name == ""John"")
                {
                    item.Amount = 1000.12m;
                    Target.Matched();
                }
                else 
                {
                    item.Amount = -999.89m;
                    item.Name = ""Was!John"";
                    //Ensure that both if and else are marked as Matched if you want it to qualify as Last.
                    //Or Use Target.Exit();
                    Target.Matched();
                }
             */

            //So since Joe != John, rule should not have processed:
            Assert.True(target.Processed, "target.Processed should end off as true.");

        }


        [Test]
        public void Apply_RuleSetOne_On_A_BusinessEntity_And_Ensure_The_RuleSet_ProcessedEntity_Till_Marked_As_Final()
        {

            IRuleSetService ruleSetService = XAct.DependencyResolver.Current.GetInstance<IRuleSetService>();

            //Create a domain Entity of some kind, so we can test it
            ATestBusinessDomainEntity ruleTestEntity = new ATestBusinessDomainEntity { Name = "Joe", Amount = 123.45m };

            //WRap the business domain entity as a RuleSetTarget:
            RuleSetTarget<ATestBusinessDomainEntity> target = new RuleSetTarget<ATestBusinessDomainEntity>(ruleTestEntity);

            Assert.IsFalse(target.Processed, "target.Processed should start off as false");

            //Apply ruleset (with id=1) on the given business domain entity.
            ruleSetService.ApplyRuleSetOn(1.ToGuid(), target);

            //Proof that script was created in Db, picked back up as a ruleset,
            //and that Rule was found...


            Console.WriteLine(target.Error);
            Assert.IsNullOrEmpty(target.Error, "Should be no error.");


            /*
             Rule 1.1 is the following:
                if (item.Name == ""John"")
                {
                    item.Amount = 1000.12m;
                    Target.Matched();
                }
                else 
                {
                    item.Amount = -999.89m;
                    item.Name = ""Was!John"";
                    //Ensure that both if and else are marked as Matched if you want it to qualify as Last.
                    //Or Use Target.Exit();
                    Target.Matched();
                }
             */


            Assert.IsTrue(target.IsMatched, "In both cases, should have been set to matched == true");

        }


        [Test]
        public void Apply_RuleSetOne_On_A_BusinessEntity_And_Ensure_The_RuleSet_Processed_The_Entity_Yes_Match()
        {

            IRuleSetService ruleSetService = XAct.DependencyResolver.Current.GetInstance<IRuleSetService>();

            //Create a domain Entity of some kind, so we can test it
            ATestBusinessDomainEntity ruleTestEntity = new ATestBusinessDomainEntity { Name = "John", Amount = 123.45m };

            //WRap the business domain entity as a RuleSetTarget:
            RuleSetTarget<ATestBusinessDomainEntity> target = new RuleSetTarget<ATestBusinessDomainEntity>(ruleTestEntity);

            //Apply ruleset (with id=1) on the given business domain entity.
            ruleSetService.ApplyRuleSetOn(1.ToGuid(), target);

            //Proof that script was created in Db, picked back up as a ruleset,
            //and that Rule was found...

            Assert.AreEqual(1000.12m, target.Item.Amount);
            Assert.AreEqual("John", target.Item.Name);

            //Get our entity:
#pragma warning disable 168
            var item = target.Item;
#pragma warning restore 168
        }
        
        [Test]
        public void Apply_RuleSetOne_On_A_BusinessEntity_Whose_Properties_Are_Modified_By_RuleSet()
        {

            IRuleSetService ruleSetService = XAct.DependencyResolver.Current.GetInstance<IRuleSetService>();


            //Create a domain Entity of some kind, so we can test it
            ATestBusinessDomainEntity ruleTestEntity = new ATestBusinessDomainEntity { Name = "Joe", Amount = 123.45m };

            //WRap the business domain entity as a RuleSetTarget:
            RuleSetTarget<ATestBusinessDomainEntity> target = new RuleSetTarget<ATestBusinessDomainEntity>(ruleTestEntity);


            //Apply ruleset (with id=1) on the given business domain entity.
            ruleSetService.ApplyRuleSetOn(1.ToGuid(), target);

            //Proof that script was created in Db, picked back up as a ruleset,
            //and that value of target was modified...

            //Was 123.45m
            //and now is larger number.

            Assert.AreEqual(-999.89m, target.Item.Amount);
            Assert.AreEqual("Was!John", target.Item.Name);
        }



    
    }


}


