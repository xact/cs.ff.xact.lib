namespace XAct.Rules.Tests
{
    /// <summary>
    /// A class neded for seeding.
    /// </summary>
    public class ATestBusinessDomainEntity
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public decimal Amount { get; set; }
    }
}