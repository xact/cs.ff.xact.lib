﻿namespace XAct.Rules.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Rules.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// An implementation of <see cref="IRulesDbModelBuilder"/>
    /// to define the db Model in a CodeFirst environment.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class RulesDbModelBuilder : IRulesDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RulesDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public RulesDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<RuleSet>)XAct.DependencyResolver.Current.GetInstance<IRuleSetModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<RuleSetAssembly>)XAct.DependencyResolver.Current.GetInstance<IRuleSetAssemblyModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<Rule>)XAct.DependencyResolver.Current.GetInstance<IRuleModelPersistenceMap>());
        }
    }
}
