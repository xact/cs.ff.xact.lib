namespace XAct.Rules.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for the <see cref="IHasXActLibDbModelBuilder"/>
    /// specific to setting up XActLib Rules capabilities
    /// in an EF CodeFirst scenario.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface IRulesDbModelBuilder : IHasXActLibDbModelBuilder
    {

    }
}