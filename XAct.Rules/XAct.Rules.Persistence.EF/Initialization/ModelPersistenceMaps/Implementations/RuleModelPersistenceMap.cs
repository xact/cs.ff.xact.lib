﻿namespace XAct.Rules.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class RuleModelPersistenceMap : EntityTypeConfiguration<Rule>, IRuleModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RuleModelPersistenceMap"/> class.
        /// </summary>
        public RuleModelPersistenceMap()
        {
            this.ToXActLibTable("Rule");
            
            //----------------------------------------

            this.HasKey(x => x.Id);

            int colOrder = 0;
            this
                .Property(m => m.Id)
                //.IsRequired()
                .HasColumnOrder(colOrder++);
            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);
            this
                .Property(m => m.Order)
                .DefineRequiredOrder(colOrder++);
            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++);
            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;
            this
                .Property(m => m.Script)
                .IsRequired()
                .HasMaxLength(4000) // 4000 (50lx80c) should be enough for a script (that should be invoking app code for any heavy lifting).
                .HasColumnOrder(colOrder++);

        }
    }
}