﻿
namespace XAct.Rules.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class RuleSetModelPersistenceMap : EntityTypeConfiguration<RuleSet>, IRuleSetModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSetModelPersistenceMap"/> class.
        /// </summary>
        public RuleSetModelPersistenceMap()
        {
            this.ToXActLibTable("RuleSet");
            



            this.HasKey(x => x.Id);

            int colOrder = 0;


            this
                .Property(m => m.Id)
                .IsRequired()
                .HasColumnOrder(colOrder++)
            ;
            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);
            this
                .Ignore(m => m.ProcessingType)
                ;
            this
                .Property(m => m.ProcessingTypeRawValue)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Ignore(m => m.ScriptHostType)
                ;
            this
                .Property(m => m.ScriptHostTypeName)
                .IsRequired()
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.TargetIsRequired)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Ignore(m => m.TargetType)
                ;

            this
                .Property(m => m.TargetTypeSerializedValue)
                .IsOptional() // Nullable
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;
            //----------------------------------------
            this
                .HasMany(bcwi => bcwi.Rules)
                .WithRequired()
                .HasForeignKey(bc => bc.RuleSetFK);

            this
                .HasMany(bcwi => bcwi.Assemblies)
                .WithRequired()
                .HasForeignKey(bc => bc.RuleSetFK);

        }
    }
}
