﻿namespace XAct.Rules.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class RuleSetAssemblyModelPersistenceMap : EntityTypeConfiguration<RuleSetAssembly>, IRuleSetAssemblyModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSetAssemblyModelPersistenceMap"/> class.
        /// </summary>
        public RuleSetAssemblyModelPersistenceMap()
        {
            this.ToXActLibTable("RuleSetAssembly");
            
            //----------------------------------------
            this.HasKey(x => x.Id);

            int colOrder = 0;

            this
                .Property(m => m.Id)
                //.IsRequired()
                .HasColumnOrder(colOrder++);
            this
                .Ignore(m => m.Assembly)
                ;
            this
                .Property(m => m.AssemblyFullName)
                .IsRequired()
                .HasColumnOrder(colOrder++);

            //----------------------------------------

   
        }
    }
}