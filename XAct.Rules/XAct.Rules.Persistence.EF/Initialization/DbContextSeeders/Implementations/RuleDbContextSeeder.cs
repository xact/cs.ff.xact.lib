﻿namespace XAct.Rules.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="IRuleDbContextSeeder"/> contract
    /// to seed the Rules tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class RuleDbContextSeeder : XActLibDbContextSeederBase<Rule>, IRuleDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public RuleDbContextSeeder(ITracingService tracingService):base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}
