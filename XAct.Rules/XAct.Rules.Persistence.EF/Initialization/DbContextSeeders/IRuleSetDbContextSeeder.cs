namespace XAct.Rules.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// 
    /// </summary>
    public interface IRuleSetDbContextSeeder : IHasXActLibDbContextSeeder<RuleSet>
    {

    }
}