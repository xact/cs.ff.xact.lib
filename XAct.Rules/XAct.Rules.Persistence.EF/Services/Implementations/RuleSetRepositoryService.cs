namespace XAct.Rules.Services.Implementations
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;

    /// <summary>
 /// A Repository to manage <see cref="RuleSet"/>s.
 /// </summary>
    public class RuleSetRepositoryService:  XActLibServiceBase, IRuleSetRepositoryService
 {
     private readonly IRepositoryService _repositoryService;

     /// <summary>
     /// Initializes a new instance of the <see cref="RuleSetRepositoryService"/> class.
     /// </summary>
     public RuleSetRepositoryService(ITracingService tracingService, IRepositoryService repositoryService) : base(tracingService)
     {
         _repositoryService = repositoryService;
     }


     /// <summary>
     /// Persists the specified rule sets.
     /// </summary>
     /// <param name="ruleSets">The rule sets.</param>
     public void Persist(params RuleSet[] ruleSets)
     {
         foreach (RuleSet ruleSet in ruleSets)
         {
// ReSharper disable RedundantTypeArgumentsOfMethod
             _repositoryService.PersistOnCommit<RuleSet,Guid>(ruleSet,true);
// ReSharper restore RedundantTypeArgumentsOfMethod
         }

     }




     /// <summary>
     /// Gets the specified <see cref="RuleSet"/>
     /// </summary>
     /// <param name="id">The id.</param>
     /// <returns></returns>
     public RuleSet GetRuleSet(Guid id)
     {
         return _repositoryService.GetSingle<RuleSet>(
             rs => rs.Id == id
             , new IncludeSpecification<RuleSet>(rs => rs.Rules)
             //,null
             //null//,rs => rs.OrderBy(rs.Rules)
             );
     }

         /// <summary>
         /// Gets all rule sets.
         /// </summary>
         /// <returns></returns>
     public RuleSet[] GetFilteredRuleSets(Expression<Func<RuleSet,bool>> filter = null )
         {
// ReSharper disable RedundantTypeArgumentsOfMethod
             return _repositoryService.GetByFilter<RuleSet>(
// ReSharper restore RedundantTypeArgumentsOfMethod
                     filter
                 , new IncludeSpecification<RuleSet>(rs => rs.Rules)
                 //,null
                 //null//,rs => rs.OrderBy(rs.Rules)
                 ).ToArray();
         }

     /// <summary>
     /// Gets the <see cref="RuleSet"/>s that match the given target type.
     /// </summary>
     /// <param name="type">The type.</param>
     /// <returns></returns>
     public RuleSet[] GetRuleSets(Type type)
     {
         return _repositoryService.GetByFilter<RuleSet>(
         rs => rs.TargetType == type
             , new IncludeSpecification<RuleSet>(rs => rs.Rules)
             //,null
             //null//,rs => rs.OrderBy(rs.Rules)
             ).ToArray();
     }

     /// <summary>
     /// Gets the named <see cref="RuleSet"/> for the given target Type.
     /// </summary>
     /// <param name="type">The type.</param>
     /// <param name="name">The name.</param>
     /// <returns></returns>
     public RuleSet GetRuleSet(Type type, string name)
     {
         return _repositoryService.GetSingle<RuleSet>(
         rs => (rs.TargetType == type) && (rs.Name == name)
             , new IncludeSpecification<RuleSet>(rs => rs.Rules)
             //,null
             //null//,rs => rs.OrderBy(rs.Rules)
             );
     }
 }
}