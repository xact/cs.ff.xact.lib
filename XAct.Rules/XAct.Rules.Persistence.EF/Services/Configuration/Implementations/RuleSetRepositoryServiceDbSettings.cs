﻿namespace XAct.Rules.Services.Configuration.Implementations
{
    using System.Globalization;
    using XAct.Rules.Services.Implementations;
    using XAct.Services;
    using XAct.Settings;

    /// <summary>
    /// An implementation of RepositorySettings, 
    /// for an DependencyInjectionContainer 
    /// to pass to to
    /// <see cref="RuleSetRepositoryService"/> 
    /// </summary>
    public class RuleSetRepositoryServiceDbSettings :  IRuleSetRepositoryServiceDbSettings
    {
        /// <summary>
        /// Gets or sets the name of the connection string settings.
        /// <para>Default value = 'Rules'</para>
        /// </summary>
        /// <value>
        /// The name of the connection string settings.
        /// </value>
        public string ConnectionStringSettingsName { get; set; }

        /// <summary>
        /// Gets or sets the default culture.
        /// <para>Default value = 'en'</para>
        /// </summary>
        /// <value>
        /// The default culture.
        /// </value>
        public CultureInfo DefaultCulture { get; set; }


        /// <summary>
        /// Gets or sets the parameter prefix.
        /// <para>Default value = '@'</para>
        /// </summary>
        /// <value>
        /// The parameter prefix.
        /// </value>
        public string ParameterPrefix { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSetRepositoryServiceDbSettings"/> class.
        /// </summary>
        public RuleSetRepositoryServiceDbSettings()
        {
            DefaultCulture = new CultureInfo("en");

            ConnectionStringSettingsName = DependencyResolver.Current.GetInstance<IHostSettingsService>().Get<string>(
                    "XActLibConnectionStringName", XAct.Library.Settings.Db.DefaultXActLibConnectionStringSettingsName);
            
            ParameterPrefix = XAct.Library.Settings.Db.DbParameterPrefix;
        }
    }
}
