﻿
namespace XAct.Rules.Services.Configuration
{
    using XAct.Configuration;
    using XAct.Data;

    /// <summary>
    /// Contract for an argument package that can be passed by a
    /// DependencyInjectionContainer to <see cref="IRuleSetRepositoryService"/>
    /// </summary>
    [ConfigurationSettings]
    public interface IRuleSetRepositoryServiceDbSettings : IHasXActLibServiceConfiguration, IDbConnectionSettings
    {
    }
}