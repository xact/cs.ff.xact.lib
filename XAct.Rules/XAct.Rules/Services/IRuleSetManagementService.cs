﻿namespace XAct.Rules
{
    using System;

    /// <summary>
    /// Contract for a service to manage services.
    /// </summary>
    public interface IRuleSetManagementService : IHasXActLibService
    {
        /// <summary>
        /// Persists the specified rule set (new or replacement of existing).
        /// </summary>
        /// <param name="ruleSet">The rule set.</param>
        void Persist(RuleSet ruleSet);




        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the specified <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        RuleSet GetRuleSet(Guid id);

        /// <summary>
        /// Gets the an array <see cref="RuleSet"/>s matching the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        RuleSet[] GetRuleSet(Type type);

        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the specified <paramref name="type"/> and <paramref name="name"/>.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        RuleSet GetRuleSet(Type type, string name);


    }
}
