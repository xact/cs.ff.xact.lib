﻿namespace XAct.Rules.Implementations
{
    using System;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IRuleSetManagementService), BindingLifetimeType.Undefined, Priority.Low /*OK: Overruled by Db soluion*/)]
    public class InMemRuleSetManagementService : XActLibServiceBase, IInMemRuleSetManagementService
    {

        /// <summary>
        /// Gets the rule set cache.
        /// </summary>
        protected IRuleSetManagementServiceState RuleSetCache { get { return _ruleSetCache; } }
        private readonly IRuleSetManagementServiceState _ruleSetCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="InMemRuleSetManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ruleSetCache">The rule set cache.</param>
        public InMemRuleSetManagementService(ITracingService tracingService, IRuleSetManagementServiceState ruleSetCache):base(tracingService)
        {
            _ruleSetCache = ruleSetCache;
        }

        /// <summary>
        /// Persists the specified rule set (new or replacement of existing).
        /// </summary>
        /// <param name="ruleSet">The rule set.</param>
        public virtual void Persist(RuleSet ruleSet)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Persist {0}", ruleSet);
            //Insert at front to 'replace' older ones:
            _ruleSetCache[ruleSet.Id] = ruleSet;
        }

        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the specified <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public virtual RuleSet GetRuleSet(Guid id)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Get RuleSet Id:{0}", id);
            RuleSet ruleSet = _ruleSetCache[id]; //FirstOrDefault(rs => rs.Id == id);

            return ruleSet;
        }

        /// <summary>
        /// Gets the an array <see cref="RuleSet"/>s matching the specified <paramref name="targetType"/>.
        /// </summary>
        /// <param name="targetType">The type.</param>
        /// <returns></returns>
        public virtual RuleSet[] GetRuleSet(Type targetType)
        {
            if (targetType == null) throw new ArgumentNullException("targetType");
            _tracingService.Trace(TraceLevel.Verbose, "Get RuleSet Type:{0}", targetType);

            RuleSet[] ruleSets = _ruleSetCache.Values.Where(rs => rs.TargetType == targetType).ToArray();

            return ruleSets;
        }
        

        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the specified <paramref name="type"/> and <paramref name="name"/>.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public virtual RuleSet GetRuleSet(Type type, string name)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Get RuleSet Type:{0}, Name:{1}", type,name);
            RuleSet ruleSet = _ruleSetCache.Values.FirstOrDefault(rs => ((rs.TargetType == type) && (rs.Name == name)));

            return ruleSet;
        }
    }
}
