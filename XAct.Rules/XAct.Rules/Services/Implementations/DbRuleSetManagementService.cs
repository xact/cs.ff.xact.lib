﻿namespace XAct.Rules.Implementations
{
    using System;
    using XAct.Caching;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public interface IDbRuleSetManagementService : IInMemRuleSetManagementService, IHasXActLibService
    {
        
    }

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof (IRuleSetManagementService), BindingLifetimeType.Undefined, Priority.Normal /*OK: An Override Priority*/)]
    public class DbRuleSetManagementService :  InMemRuleSetManagementService, IDbRuleSetManagementService
    {
        private readonly ICachingService _hostBasedCachingService;
        private readonly IRuleSetRepositoryService _ruleSetRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbRuleSetManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ruleSetCache">The rule set cache.</param>
        /// <param name="hostBasedCachingService">The caching service.</param>
        /// <param name="ruleSetRepository">The rule set repository.</param>
        public DbRuleSetManagementService(ITracingService tracingService, IRuleSetManagementServiceState ruleSetCache, ICachingService hostBasedCachingService, IRuleSetRepositoryService ruleSetRepository)
            : base(tracingService, ruleSetCache)
        {
            _hostBasedCachingService = hostBasedCachingService;
            _ruleSetRepository = ruleSetRepository;
        }

        /// <summary>
        /// Persists the specified rule set (new or replacement of existing).
        /// </summary>
        /// <param name="ruleSet">The rule set.</param>
        public override void Persist(RuleSet ruleSet)
        {

            _ruleSetRepository.Persist(ruleSet);
            refreshFunc();

        }

        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the specified <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public override RuleSet GetRuleSet(Guid id)
        {
            refresh();
            return base.GetRuleSet(id);
        }

        /// <summary>
        /// Gets the an array <see cref="RuleSet"/>s matching the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public override RuleSet[] GetRuleSet(Type type)
        {
            refresh();
            return base.GetRuleSet(type);
        }

        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the specified <paramref name="type"/> and <paramref name="name"/>.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public override RuleSet GetRuleSet(Type type, string name)
        {
            refresh();
            return base.GetRuleSet(type, name);
        }

        private void refresh()
        {
            //Messy way to update cache:
            bool fleshFlag;
            _hostBasedCachingService.TryGet(this.GetType().FullName, out fleshFlag, refreshFunc, TimeSpan.FromMinutes(5), true);


        }

        private bool refreshFunc()
        {

            lock(base.RuleSetCache)
            {
                base.RuleSetCache.Clear();

                foreach(RuleSet ruleSet in _ruleSetRepository.GetFilteredRuleSets())
                {
                    base.RuleSetCache.Add(ruleSet.Id,ruleSet);
                }
            }
            return true;

        }

    }

}
