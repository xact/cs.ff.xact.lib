﻿namespace XAct.Rules.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using XAct.Diagnostics;
    using XAct.Languages;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class RuleSetService : IRuleSetService
    {
        private readonly ITracingService _tracingService;
        private readonly IRuleSetManagementService _ruleSetManagementService;
        private readonly IScriptHostService _scriptHostService;

        List<Assembly> _assemblies = new List<Assembly>();
        List<string> _namespaces = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSetService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ruleSetManagementService">The rule set management service.</param>
        /// <param name="scriptHostService">The script host service.</param>
        public RuleSetService(ITracingService tracingService, IRuleSetManagementService ruleSetManagementService, IScriptHostService scriptHostService )
        {
            _tracingService = tracingService;
            _tracingService.Trace(TraceLevel.Verbose,"RuleSetService.Constructor()");
            _ruleSetManagementService = ruleSetManagementService;
            _scriptHostService = scriptHostService;
        }

        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the specified <paramref name="id"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The id.</param>
        /// <param name="ruleSetTarget">The target.</param>
        /// <param name="reuseScriptHost">if set to <c>true</c> [reuse script host].</param>
        /// <returns></returns>
        public void ApplyRuleSetOn<T>(Guid id, RuleSetTarget<T> ruleSetTarget, bool reuseScriptHost = false)
        {


            _tracingService.Trace(TraceLevel.Verbose, "RuleSetService.ApplyRuleSetOn(id={0}, ruleSetTarget={1}, reuseScriptHost={3})", id, ruleSetTarget, reuseScriptHost);

            ruleSetTarget.ValidateIsNotDefault("ruleSetTarget");

            RuleSet ruleSet = _ruleSetManagementService.GetRuleSet(id);


            if (ruleSet.TargetIsRequired)
            {
                ruleSetTarget.Item.ValidateIsNotDefault("target");
            }

            ApplyRuleSetOn(ruleSet, ruleSetTarget, reuseScriptHost);
        }


        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the <c>type</c> and <paramref name="name"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <param name="ruleSetTarget">The target.</param>
        /// <param name="reuseScriptHost">if set to <c>true</c> [reuse script host].</param>
        /// <returns></returns>
        public void ApplyRuleSetOn<T>(RuleSetTarget<T> ruleSetTarget, string name, bool reuseScriptHost = false)
         {
             _tracingService.Trace(TraceLevel.Verbose, "RuleSetService.ApplyRuleSetOn(id={0}, ruleSetTarget={1}, name={2}, reuseScriptHost={3})", ruleSetTarget, name, reuseScriptHost);

            RuleSet ruleSet = _ruleSetManagementService.GetRuleSet(typeof(T), name);

             ruleSetTarget.ValidateIsNotDefault("ruleSetTarget");

             if (ruleSet.TargetIsRequired)
             {
                 ruleSetTarget.ValidateIsNotDefault("target");
             }
             
            ApplyRuleSetOn(ruleSet, ruleSetTarget, reuseScriptHost);
         }






        private void ApplyRuleSetOn<T>(RuleSet ruleSet, RuleSetTarget<T> ruleSetTarget, bool reuseScriptHost)
        {
            ScriptHostType scriptHostType =
                (ScriptHostType) System.Enum.Parse(typeof (ScriptHostType), ruleSet.ScriptHostTypeName, true);

            IHostInteractiveScriptHost scriptHost = _scriptHostService.CreateHostInteractiveScriptHost(scriptHostType,
                                                                                                       reuseScriptHost);

            //Don't clear the Result. Never know..might carry a payload in transit. ruleSetTarget.Result = null;

            if (!reuseScriptHost)
            {
                _assemblies.Clear();

                //Ensure XAct.Core, XAct.Rules are registered:
                _assemblies.Add(typeof (string).Assembly);
                _assemblies.Add(typeof (System.Linq.IQueryable).Assembly);
                _assemblies.Add(typeof (System.Collections.Generic.IList<>).Assembly);
                _assemblies.Add(typeof (System.Diagnostics.Debug).Assembly);
                _assemblies.Add(typeof (System.Text.Encoder).Assembly);
                _assemblies.Add(typeof (System.Text.RegularExpressions.Match).Assembly);
                _assemblies.Add(typeof (System.Threading.Thread).Assembly);
                _assemblies.Add(typeof (System.Reflection.Assembly).Assembly);
                _assemblies.Add(typeof (System.Xml.XmlException).Assembly);
                _assemblies.Add(typeof (XAct.StringExtensions).Assembly);
                _assemblies.Add(XAct.DependencyResolver.Current.GetType().Assembly);
                _assemblies.Add(this.GetType().Assembly);

                scriptHost.RegisterAssemblies(_assemblies.ToArray());
            }

            if (ruleSet.Assemblies != null)
            {
                foreach (var assembly in ruleSet.Assemblies)
                {
                    if (_assemblies.Contains(assembly.Assembly))
                    {
                        continue;
                    }
                    _assemblies.Add(assembly.Assembly);
                    scriptHost.RegisterAssemblies(assembly.Assembly);
                }
            }



            if (!reuseScriptHost)
            {
                _namespaces.Clear();

                _namespaces.AddRange(
                    new[]
                        {
                            "System",
                            "System.Collections",
                            "System.Collections.Generic",
                            "System.Diagnostics",
                            "System.Linq",
                            "System.Linq.Expressions",
                            "System.Text",
                            "System.Text.RegularExpressions",
                            "System.Threading",
                            "System.Reflection",
                            "System.Xml",
                            "XAct.Rules",
                        });
                scriptHost.RegisterNamespaces(_namespaces.ToArray());
            }


            if (!_namespaces.Contains(ruleSet.TargetType.Namespace))
            {
                _namespaces.Add(ruleSet.TargetType.Namespace);
                scriptHost.RegisterNamespaces(ruleSet.TargetType.Namespace);
            }


            
            scriptHost.RegisterHostObject("Target", ruleSetTarget);

#pragma warning disable 168
            var check = scriptHost.Evaluate("Context[\"Target\"]");
            var check2 = scriptHost.Evaluate("Context[\"Target\"]") as RuleSetTarget;
            var check3 = check2.Item;
#pragma warning restore 168

            //string sss = Newtonsoft.Json.JsonConvert.SerializeObject(s);
            //c.SetGlobalValue("Y", Jurassic.Library.JSONObject.Parse(c, sss));

            bool matchedCombined=false;
            bool matched=false;

            foreach (Rule rule in ruleSet.Rules)
            {
                bool continueFlag;

                try
                {
                    continueFlag = ApplyRule<T>(scriptHost, ruleSet, rule, ruleSetTarget, out matched);
                }
                catch
                {
                    continueFlag = false;
                    ruleSetTarget.Error = scriptHost.LatestErrorMessage;
                }
                matchedCombined = matchedCombined || matched;
                if (!continueFlag)
                {
                    break;
                }
            }

            ruleSetTarget.Processed = true;
         }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------
        //-----------------------------------------------------------------
        //-----------------------------------------------------------------
        //-----------------------------------------------------------------


        //-----------------------------------------------------------------

        private bool ApplyRule<T>(IScriptHost scriptHost, RuleSet ruleSet, Rule rule, RuleSetTarget<T> ruleSetTarget, out bool matched)
        {
            _tracingService.Trace(TraceLevel.Verbose,
                                  "RuleSetService.ApplyRule(id={0}, name={1})",
                                  rule.Id, rule.Name);

            matched = false;
            
            try
            {
                if (rule.ScriptReturnsMatchedBoolean)
                {
                    matched = scriptHost.Evaluate<bool>(rule.Script);
                }
                else
                {
                    scriptHost.Execute(rule.Script);

                    matched = ruleSetTarget.IsMatched;

                }


                bool exitEarly = ruleSetTarget.IsExitEarly;


                if ((exitEarly) || (matched) && (ruleSet.ProcessingType == RuleSetProcessingType.ExitOnFirstMatch))
                {
                    //Do not continue 
                    return false;
                }
                
                return true;
            }
            catch (System.Exception e)
            {

                _tracingService.TraceException(TraceLevel.Error, e, "Exception raised when parsing rule.");
            }

            //string s2 = scriptHost.Evaluate<string>("JSON.stringify(Target)");

            return false;
        }

        //-----------------------------------------------------------------

    }
}
