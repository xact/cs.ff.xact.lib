﻿namespace XAct.Rules
{
    using System;

    /// <summary>
    /// Contract for a service to retrieve RuleSets,
    /// and process them against a target object.
    /// </summary>
    public interface IRuleSetService : IHasXActLibService
    {
        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the specified <paramref name="id"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The id.</param>
        /// <param name="target">The target.</param>
        /// <param name="reuseScriptHost">if set to <c>true</c> [reuse script host].</param>
        /// <returns></returns>
        void ApplyRuleSetOn<T>(Guid id, RuleSetTarget<T> target, bool reuseScriptHost=false);


        /// <summary>
        /// Gets the <see cref="RuleSet"/> matching the <c>type</c> and <paramref name="name"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <param name="target">The target.</param>
        /// <param name="reuseScriptHost">if set to <c>true</c> [reuse script host].</param>
        /// <returns></returns>
        void ApplyRuleSetOn<T>(RuleSetTarget<T> target, string name, bool reuseScriptHost = false);

        ///// <summary>
        ///// Applies the <see cref="RuleSet"/> on the given target.
        ///// </summary>
        ///// <param name="ruleSet">The rule set.</param>
        ///// <param name="target">The target.</param>
        ///// <returns></returns>
        //RuleSetResult ApplyRuleSetOn<T>(RuleSet ruleSet, T target);

    }
}
