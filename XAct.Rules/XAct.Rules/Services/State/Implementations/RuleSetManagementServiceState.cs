namespace XAct.Rules.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class RuleSetManagementServiceState : Dictionary<Guid, RuleSet>, IRuleSetManagementServiceState { }
}
