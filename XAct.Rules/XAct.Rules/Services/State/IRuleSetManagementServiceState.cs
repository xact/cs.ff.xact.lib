﻿namespace XAct.Rules
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public interface IRuleSetManagementServiceState : IDictionary<Guid, RuleSet>, IHasXActLibServiceState
       {
           
       }
}
