namespace XAct.Rules
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A seriaiized Rule.
    /// </summary>
    [DataContract]
    public class Rule : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasNameAndDescription, IHasEnabled
    {
        /// <summary>
        /// Gets or sets the datastore id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; /*protected*/ set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the Id of the parent <see cref="RuleSet"/>.
        /// </summary>
        /// <value>
        /// The rule set FK.
        /// </value>
        [DataMember]
        public virtual Guid RuleSetFK { get; set; }


        /// <summary>
        /// Gets the optional name/identifier of the rule.
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }
        /// <summary>
        /// Gets or sets the optional description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Rule"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool Enabled { get; set; }
        
        /// <summary>
        /// Gets or sets the processing order of this <see cref="Rule"/> 
        /// within its <see cref="RuleSet"/>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember]
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets the script to run.
        /// </summary>
        /// <value>
        /// The script.
        /// </value>
        [DataMember]
        public string Script { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether 
        /// the script is going to set the <see cref="RuleSetTarget.Matched()"/>
        /// property, or return a boolean value to that effect.
        /// <para>
        /// The expected behaviour is <see cref="RuleSetTarget.Matched()"/>
        /// should be set.
        /// </para>
        /// <para>
        /// <example>
        /// <![CDATA[
        /// if (Target.Item.Value >100){Target.Item.Msg =".x...";Target.Matched=true;}
        /// ]]>
        /// </example>
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [script sets matched property]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool ScriptReturnsMatchedBoolean { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="Rule"/> class.
        /// </summary>
        public Rule()
        {
            this.GenerateDistributedId();
        }
    }
}