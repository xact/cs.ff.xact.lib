﻿namespace XAct.Rules
{
    /// <summary>
    /// NOT USED AT PRESENT.
    /// Extended version of Rule that separates When from Then. 
    /// </summary>
    public class RuleEx : Rule
    {
        /// <summary>
        /// Gets or sets the serialized condition.
        /// </summary>
        /// <value>
        /// The serialized condition.
        /// </value>
        public virtual string Condition { get; set; }

        /// <summary>
        /// Gets or sets the serialized Action to perform on the <see cref="RuleSet"/> Target
        /// if the <see cref="Condition"/> is met.
        /// </summary>
        /// <value>
        /// The serialized rule.
        /// </value>
        public virtual string Action { get; set; }

        /// <summary>
        /// Gets or sets the (optional) serialized alternate Action to perform on the <see cref="RuleSet"/> Target
        /// if the <see cref="Condition"/> is not met.
        /// </summary>
        /// <value>
        /// The serialized else.
        /// </value>
        public virtual string AlternateAction { get; set; }
    }
}
