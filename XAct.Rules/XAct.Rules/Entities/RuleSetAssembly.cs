﻿namespace XAct.Rules
{
    using System;
    using System.Reflection;
    using System.Runtime.Serialization;
    using XAct.Languages;

    /// <summary>
    /// Reference to an Assembly that has to be mounted into the 
    /// <see cref="IScriptHost"/> before the <see cref="Rule.Script"/>
    /// can be processed.
    /// </summary>
    [DataContract]
    public class RuleSetAssembly : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
    {

   

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; /*protected*/ set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the Id of the parent <see cref="RuleSet"/>.
        /// </summary>
        /// <value>
        /// The rule set FK.
        /// </value>
        [DataMember]
        public virtual Guid RuleSetFK { get; set; }


        /// <summary>
        /// Gets or sets the assembly FullName.
        /// </summary>
        /// <value>
        /// The assembly FQN.
        /// </value>
        public virtual string AssemblyFullName
        {
            get { return _assemblyFullName; }
            set { _assemblyFullName = value;
                //_assembly = System.Reflection.Assembly.Load(new AssemblyName(value));
                _assembly = System.Reflection.Assembly.Load(value);
            }
        }
        [DataMember]
        private string _assemblyFullName;

        /// <summary>
        /// Gets or sets the assembly <see cref="System.Type"/> to 
        /// register in the <see cref="IScriptHost"/> before the <see cref="Rule.Script"/>
        /// is processed.
        /// </summary>
        /// <value>
        /// The assembly.
        /// </value>
        public virtual Assembly Assembly
        {
            get { return _assembly; } 
            set { 

                if (value == null)
                {
                    throw new  ArgumentNullException("Assembly value");
                }
                _assembly = value;

                _assemblyFullName = value.FullName;
            }
        }
        private Assembly _assembly;



        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSetAssembly"/> class.
        /// </summary>
        public RuleSetAssembly()
        {
            this.GenerateDistributedId();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSetAssembly"/> class.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        public RuleSetAssembly(Assembly assembly):this()
        {
            assembly.ValidateIsNotDefault("assembly");
            this.Assembly = assembly;
        }
    }
}
