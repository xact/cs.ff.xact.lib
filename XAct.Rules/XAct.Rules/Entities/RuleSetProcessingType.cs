﻿

namespace XAct.Rules
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The way to process the <see cref="RuleSet"/>'s <see cref="Rules"/>
    /// (exit on first match, or continue to end).
    /// </summary>
    [DataContract]
    public enum RuleSetProcessingType
    {
        /// <summary>
        /// <para>Value = 0.</para>
        /// <para>
        /// An Error State.
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined =0,
        
        /// <summary>
        /// <para>Value = 1.</para>
        /// </summary>
        [EnumMember]
        ExitOnFirstMatch = 1,

        /// <summary>
        /// <para>Value = 2.</para>
        /// </summary>
        [EnumMember]
        CompleteAll = 2

    }
}
