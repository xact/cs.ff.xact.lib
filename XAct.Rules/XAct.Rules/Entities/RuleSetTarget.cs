﻿namespace XAct.Rules
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using XAct.Languages;

    /// <summary>
    /// A package to submit a target object to a rule.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RuleSetTarget<T> : RuleSetTarget
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSetTarget{T}"/> class.
        /// </summary>
        /// <param name="target">The target.</param>
        public RuleSetTarget(T target) : base(target)
        {
        }

        /// <summary>
        /// Gets or sets the target that is to be validated.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public new T Item
        {
            get { return (T)_item; }
            private set { _item = value; }
        }
    }


    /// <summary>
    /// A package to submit a target object to a rule.
    /// </summary>
    public class RuleSetTarget
    {

    /// <summary>
        /// A Bag to embed any information the target Rule script needs to process the Item.
        /// </summary>
        public Dictionary<string, object> ItemData { get { return _data; } }
        private readonly Dictionary<string, object> _data = new Dictionary<string, object>();

// ReSharper disable InconsistentNaming
        /// <summary>
        /// </summary>
        protected object _item;
// ReSharper restore InconsistentNaming


        /// <summary>
        /// Gets or sets the target that is to be validated.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public virtual object Item
        {
            get { return _item; }
            private set { _item = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the script returns a boolean, 
        /// or sets the Matched property.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [script returns matched boolean]; otherwise, <c>false</c>.
        /// </value>
        public bool ScriptSetsMatchedProperty { get; set; }


        //public T2 Result { get; set; }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="RuleSetTarget&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="target">The target.</param>
        public RuleSetTarget(object target)
        {
            Item = target;
        }

        /// <summary>
        /// Gets or sets whether the RuleSet was proccessed.
        /// </summary>
        /// <value>
        /// The processed.
        /// </value>
        public bool Processed { get; set; }

        /// <summary>
        /// Sets the target matched 
        /// </summary>
        public void Matched()
        {
            IsMatched = true;
        }

        /// <summary>
        /// Sets whether to exit after this rule, and not prcess any others.
        /// </summary>
        public void Exit()
        {
            IsExitEarly = true;
        }





        /// <summary>
        /// Gets value indicating whether this <see cref="RuleSetTarget&lt;T&gt;"/> was matched by the <see cref="Rule"/>
        /// condition.
        /// </summary>
        /// <value>
        ///   <c>true</c> if valid; otherwise, <c>false</c>.
        /// </value>
        public bool IsMatched { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to exit 
        /// the processing of RuleSet even if matched is not met, 
        /// and/or the RuleSet was suppossed to be all processed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if exit; otherwise, <c>false</c>.
        /// </value>
        public bool IsExitEarly { get; private set; }

        /// <summary>
        /// Some Rule processing script languages (JS) 
        /// cannot affect the target item, and only return
        /// a proxy result that it is up to the invoker to know
        /// what to do with.
        /// </summary>
        /// <value>
        /// The rule set proxy.
        /// </value>
        public object Result { get; set; }


        /// <summary>
        /// Any error message raised by the <see cref="IScriptHost"/>
        /// while processing the item.
        /// </summary>
        public string Error { get; set; }
    }

}
