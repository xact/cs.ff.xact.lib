//namespace XAct.Rules
//{
//    /// <summary>
//    /// The Type of parser used to process the RuleSet.
//    /// </summary>
//    public class RuleSetFormatType: IHasIdReadOnly<int>, IHasName
//    {
//        /// <summary>
//        /// Gets or sets the id.
//        /// </summary>
//        /// <value>
//        /// The id.
//        /// </value>
//        public virtual int Id { get; /*protected*/ set; }


//        /// <summary>
//        /// Gets the name of the RuleSet Type (indicating whether Parser is Javascript, or other).
//        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
//        /// </summary>
//        /// <value>
//        /// The name.
//        /// </value>
//        /// <internal>8/13/2011: Sky</internal>
//        public virtual string Name { get; set; }
//    }
//}