namespace XAct.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using XAct.Languages;

    /// <summary>
    /// The RuleSet (collection of Rules)
    /// </summary>
    [DataContract]
    public class RuleSet : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantIdReadOnly, IHasNameAndDescription, IHasEnabled
    {

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        /// <value>
        /// The organisation id.
        /// </value>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; /*protected*/ set; }
        




        /// <summary>
        /// Gets the immutable name of the object.
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets a description of the RuleSet.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="RuleSet"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }




        /// <summary>
        /// Gets or sets how to process the <see cref="Rules"/>
        /// of this <see cref="RuleSet"/> 
        /// (ie, exit on first match, or continue processing through all rules).
        /// </summary>
        /// <value>
        /// The type of the processing.
        /// </value>
        public virtual RuleSetProcessingType ProcessingType
        {
            get { return _processingType; }
            set
            {
                _processingTypeRawValue = (int)value;
                _processingType = value;
            }
        }
        [DataMember]
        private RuleSetProcessingType _processingType;

        /// <summary>
        /// Gets or sets the raw value of the enum indicating
        /// how to process the <see cref="Rules"/>
        /// of this <see cref="RuleSet"/> 
        /// (ie, exit on first match, or continue processing through all rules).
        /// </summary>
        /// <value>
        /// The processing type raw.
        /// </value>
        public virtual int ProcessingTypeRawValue
        {
            get { return _processingTypeRawValue; }
            set
            {
                _processingTypeRawValue = value;
                ProcessingType = (RuleSetProcessingType)value;
            }
        }
        [DataMember]
        private int _processingTypeRawValue;


        /// <summary>
        /// Gets or sets the type of the <see cref="IScriptHost"/>
        /// used to process the Scripts within the <see cref="Rules"/>
        /// </summary>
        /// <value>
        /// The type of the script host.
        /// </value>
        public virtual ScriptHostType ScriptHostType
        {
            get { return _scriptHostType; }
            set
            {
                _scriptHostType = value; _scriptHostTypeName = value.ToString();
            }
        }

        [DataMember]
        private XAct.Languages.ScriptHostType _scriptHostType;


        /// <summary>
        /// Gets or sets the string name of the <see cref="IScriptHost"/>
        /// used to process the <see cref="Rules"/>.
        /// <para>
        /// Note that this is the value usede to search for the right script host -- 
        /// the enum is provided only to make it easier to set default values (you can 
        /// offer alternate languages/script hosts if you register them first).
        /// </para>
        /// </summary>
        /// <value>
        /// The name of the script host type.
        /// </value>
        public virtual string ScriptHostTypeName
        {
            get
            {
                return _scriptHostTypeName;
            }
            set
            {
                _scriptHostTypeName = value;
                try
                {
                    _scriptHostType = (ScriptHostType) Enum.Parse(typeof(ScriptHostType), value,false);
                }
                catch
                {
                    _scriptHostType = ScriptHostType.Unrecognized;
                }
            }
        }
        [DataMember]
        string _scriptHostTypeName;


        /// <summary>
        /// Gets or sets a value indicating whether the target is required, or Rules
        /// can still succeed if the Rule target is null.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [target is required]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool TargetIsRequired { get; set; }


        /// <summary>
        /// Gets or sets the (optional) type of the <c>Target</c>
        /// if <c><see cref="TargetIsRequired"/>=true</c>
        /// </summary>
        /// <value>
        /// The type of the target.
        /// </value>
        public virtual Type TargetType
        {
            get
            {
                return
                    _targetType ??
                    (_targetType = TargetTypeSerializedValue.IsNullOrEmpty() ? null : System.Type.GetType(TargetTypeSerializedValue));
            }
            set
            {
                _targetType = value;
                _targetTypeSerializedValue = _targetType == null ? null : _targetType.AssemblyQualifiedName;
            }

        }
        private Type _targetType;

        /// <summary>
        /// Gets or sets the type of the serialized FQN of the target Type.
        /// </summary>
        /// <value>
        /// The type of the serialized target.
        /// </value>
        public virtual string TargetTypeSerializedValue
        {
            get { return _targetTypeSerializedValue; }
            set { 
                _targetTypeSerializedValue = value;
                _targetType = (value.IsNullOrEmpty()) ? null : Type.GetType(value);
            }
        }
        [DataMember]
        private string _targetTypeSerializedValue;
        



        /// <summary>
        /// Gets the rules that belong to this <see cref="RuleSet"/>.
        /// </summary>
        public virtual ICollection<Rule> Rules { get { return _rules ?? (_rules = new Collection<Rule>()); } }
        [DataMember]
        private ICollection<Rule> _rules;



        /// <summary>
        /// Gets a collection of references to Assemblies that need to be loaded up into the <see cref="IScriptHost"/>
        /// before the script can be processed.
        /// </summary>
        public virtual ICollection<RuleSetAssembly> Assemblies { get { return _assemblies ?? (_assemblies = new Collection<RuleSetAssembly>()); } }
        [DataMember]
        private ICollection<RuleSetAssembly> _assemblies;


        /// <summary>
        /// Initializes a new instance of the <see cref="RuleSet"/> class.
        /// </summary>
        public RuleSet()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }
    }
}