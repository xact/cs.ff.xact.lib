﻿namespace XAct.Rules
{
    using System;
    using System.Linq.Expressions;

    /// <summary>
    /// Contract for a Repository to manage RuleSets.
    /// </summary>
    public interface IRuleSetRepositoryService:IHasXActLibService
    {

        /// <summary>
        /// Persists the specified rule sets.
        /// </summary>
        /// <param name="ruleSets">The rule sets.</param>
        void Persist(params RuleSet[] ruleSets);


        /// <summary>
        /// Gets the specified <see cref="RuleSet"/>
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        RuleSet GetRuleSet(Guid id);

        /// <summary>
        /// Gets the <see cref="RuleSet"/>s that match the given target type.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        RuleSet[] GetFilteredRuleSets(Expression<Func<RuleSet, bool>> filter = null);

        /// <summary>
        /// Gets the <see cref="RuleSet"/>s that match the given target type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        RuleSet[] GetRuleSets(Type type);

        /// <summary>
        /// Gets the named <see cref="RuleSet"/> for the given target Type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        RuleSet GetRuleSet(Type type, string name);
    }
}
