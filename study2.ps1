﻿$dir = Split-Path $MyInvocation.MyCommand.Path
invoke-expression -Command   $dir\myScript1.ps1

cls

Write-Host("Script Start...");
$affectedProjects = GetAffectedNugetProjects ("d:\bb\cs.ff.xact.lib\cs.ff.xact.lib.sln");



foreach ($affectedProject in $affectedProjects){
  Write-Host("...`"$($affectedProject.Name)`"") -ForegroundColor "gray" -b DarkRed;
}



Write-Host("Script Complete.");
