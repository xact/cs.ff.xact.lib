﻿namespace XAct.Net.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Net.Messaging;

    /// <summary>
    /// A default implementation of the <see cref="IMessageDbContextSeeder"/> contract
    /// to seed the Messaging tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class MessageDbContextSeeder : XActLibDbContextSeederBase<Message>, IMessageDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDbContextSeeder"/> class.
        /// </summary>
        public MessageDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities() 
        {
        }
    }
}
