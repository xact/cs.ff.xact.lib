﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
    /// Defines a EF Map on how to persist a MessageAddress into the database.
    /// </summary>
    public class MessageAddressModelPersistenceMap : EntityTypeConfiguration<MessageAddress>, IMessageAddressModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageAddressModelPersistenceMap" /> class.
        /// </summary>
        public MessageAddressModelPersistenceMap()
        {

            this.ToXActLibTable("MessageAddress");


            this.HasKey(t => t.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.MessageId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Type)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            //length based on:http://stackoverflow.com/a/574698/1052767
            this.Property(m => m.Name)
                .IsOptional()
                .HasMaxLength(254)
                .HasColumnOrder(colOrder++)
                ;


            //length based on:http://stackoverflow.com/a/574698/1052767
            this.Property(m => m.Value)
                .IsRequired()
                .HasMaxLength(254)
                .HasColumnOrder(colOrder++)
                ;





        }
    }
}