﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
    /// Defines a EF Map on how to persist a MessageStatus into the database.
    /// </summary>
    public class MessageStatusModelPersistenceMap : EntityTypeConfiguration<MessageStatus>,
                                             IMessageStatusModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageStatusModelPersistenceMap" /> class.
        /// </summary>
        public MessageStatusModelPersistenceMap()
        {
            this.ToXActLibTable("MessageStatus");


            this.HasKey(t => t.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.MessageId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Source)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Status)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Note)
                .IsOptional().
                 HasMaxLength(2048)
                .HasColumnOrder(colOrder++)
                ;

        }
    }
}