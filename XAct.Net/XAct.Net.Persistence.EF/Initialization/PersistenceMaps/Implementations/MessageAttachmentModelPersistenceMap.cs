﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
    /// Defines a EF Map on how to persist a MessageHeader into the database.
    /// </summary>
    public class MessageAttachmentModelPersistenceMap : EntityTypeConfiguration<MessageAttachment>, IMessageAttachmentModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageHeaderModelPersistenceMap" /> class.
        /// </summary>
        public MessageAttachmentModelPersistenceMap()
        {
            this.ToXActLibTable("MessageAttachment");


            //this.Ignore(m => m.IsStream);

            this.HasKey(t => t.Id);


            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.MessageId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Type)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.ContentType)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);
                ;


            this.Property(m => m.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++)
                ;


            this.Property(m => m.DeletedOnUtc)
                .DefineOptionalDeletedOnUtc(colOrder++)
                ;


            this.Property(m => m.Size)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Value)
                .IsRequired()
                .IsMaxLength()//OK to be MaxLength. 4000 (50lx80c) would be too small.
                .HasColumnOrder(colOrder++)
                ;


        }
    }
}