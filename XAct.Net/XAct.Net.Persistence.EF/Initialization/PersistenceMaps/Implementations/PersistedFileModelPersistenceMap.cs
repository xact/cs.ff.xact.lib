﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
    /// Defines a EF Map on how to persist a MessageHeader into the database.
    /// </summary>
    public class PersistedFileModelPersistenceMap : EntityTypeConfiguration<PersistedFile>, IPersistedFileModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageHeaderModelPersistenceMap" /> class.
        /// </summary>
        public PersistedFileModelPersistenceMap()
        {
            this.ToXActLibTable("PersistedFile");


            //If we are using a relationship, the number of values in both keys 
            //must be identical (ie, can't have composite).
            this.HasKey(t => t.Id);

            int colOrder = 0;
            int indexMember = 1; //1-based.

            this.Property(m => m.ApplicationTennantId)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id_Key", indexMember++) { IsUnique = true }))
                        .DefineRequiredApplicationTennantId(colOrder++)
    ;

            this.Property(m => m.Id)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id_Key", indexMember++) { IsUnique = true }))
                .DefineRequiredGuidId(colOrder++);
            ;

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;
            this.Ignore(m => m.Size)
                ;
            this.Property(m => m.Value) //byte[] array
                .IsOptional()
                .IsMaxLength() //OK to be MaxLength. 4000 (50lx80c) would be too small.
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.ContentId)
                .IsOptional()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(m => m.ContentType)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(m => m.CreatedOnUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++)
                ;

            this.Property(m => m.DeletedOnUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.Tag)
                .DefineOptional256CharTag(colOrder++)
                ;
            this.Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;


            this.HasMany(m => m.Metadata)
                .WithRequired() //A metadata cannot be added if we don't know which file it belongs to.
                //cannot add a Part without an Message. NOTE: It does NOT mean that you have to have atleast 1 Part. 
                .HasForeignKey(m2 => m2.OwnerFK)
                ;


        }
    }
}