﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class ProgressStatusModelPersistenceMap : EntityTypeConfiguration<XAct.Net.Status.ProgressStatus>, IProgressStatusModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressStatusModelPersistenceMap" /> class.
        /// </summary>
        public ProgressStatusModelPersistenceMap()
        {
            this.ToXActLibTable("ProgressStatus");

            this.HasKey(t => t.Id);

            int colOrder = 0;
            int indexMember = 1; //1-based

            this.Property(m => m.ApplicationTennantId)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id_Key", indexMember++) { IsUnique = true }))
            .DefineRequiredApplicationTennantId(colOrder++)
                        ;
            this.Property(m => m.Id)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id_Key", indexMember++) { IsUnique = true }))
                .DefineRequiredGuidId(colOrder++);
            ;

            //this
            //    .Property(m => m.Timestamp)
                //.DefineTimestamp(colOrder++);

            this.Property(m => m.Progress) //byte[] array
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.Value) //byte[] array
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.Title)
                .IsOptional()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.Text)
                .IsOptional()
                .HasMaxLength(2048)
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.CreatedOnUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.LastModifiedOnUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

        }
    }
}