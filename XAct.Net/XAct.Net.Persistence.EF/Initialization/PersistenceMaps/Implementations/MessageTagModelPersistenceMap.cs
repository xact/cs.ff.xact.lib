﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
    /// Defines a EF Map on how to persist a MessageTag into the database.
    /// </summary>
    public class MessageTagModelPersistenceMap : EntityTypeConfiguration<MessageTag>, IMessageTagModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageTagModelPersistenceMap" /> class.
        /// </summary>
        public MessageTagModelPersistenceMap()
        {
            this.ToXActLibTable("MessageTag");


            this.HasKey(m => m.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);


            this.Property(m => m.Order)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Name)
                .HasMaxLength(64)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

        }
    }
}