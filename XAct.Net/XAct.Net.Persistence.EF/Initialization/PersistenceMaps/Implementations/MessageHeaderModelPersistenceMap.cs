﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
    /// Defines a EF Map on how to persist a MessageHeader into the database.
    /// </summary>
    /// 
    public class MessageHeaderModelPersistenceMap : EntityTypeConfiguration<MessageHeader>, IMessageHeaderModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageHeaderModelPersistenceMap" /> class.
        /// </summary>
        public MessageHeaderModelPersistenceMap()
        {
            this.ToXActLibTable("MessageHeader");

            this.HasKey(t => t.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.MessageId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            //length: http://stackoverflow.com/a/2721849/1052767
            this.Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;



            //length: http://stackoverflow.com/a/2721849/1052767
            //but it's not really practical, so going to limit to 4000 for now. Or less...
            this.Property(m => m.Value)
                .IsRequired()
                .HasMaxLength(2048)
                .HasColumnOrder(colOrder++)
                ;

        }
    }
}