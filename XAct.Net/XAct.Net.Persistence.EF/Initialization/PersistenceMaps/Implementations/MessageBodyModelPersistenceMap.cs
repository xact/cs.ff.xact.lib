﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
    /// Defines a EF Map on how to persist a MessageBody into the database.
    /// </summary>
    public class MessageBodyModelPersistenceMap : EntityTypeConfiguration<MessageBody>, IMessageBodyModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBodyModelPersistenceMap" /> class.
        /// </summary>
        public MessageBodyModelPersistenceMap()
        {
            this.ToXActLibTable("MessageBody");


            this.HasKey(t => t.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.MessageId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            
            this.Property(m => m.Type)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Name)
                .IsOptional()//Name is not required for body.
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Value)
                .IsRequired()
                .IsMaxLength()//OK to be MaxLength. 4000 (50lx80c) would be too small.
                .HasColumnOrder(colOrder++)
                ;

        }
    }
}