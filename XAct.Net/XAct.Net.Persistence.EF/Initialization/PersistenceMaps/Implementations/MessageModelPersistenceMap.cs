﻿namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
/// Defines a EF Map on how to persist a Message into the database.
/// </summary>
    public class MessageModelPersistenceMap : EntityTypeConfiguration<Message>, IMessageModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageModelPersistenceMap" /> class.
        /// </summary>
        public MessageModelPersistenceMap()
        {

            this.ToXActLibTable("Message");


            this.HasKey(t => t.MessageId);

            int colOrder = 0;

            this.Property(m => m.MessageId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None) //Need to get this working
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(m => m.ApplicationTennantId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;



            this.Property(m => m.IsDraft)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Status)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Importance)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Priority)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.CreatedOnUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.SentOn)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.DeliveryProtocols)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;


            this.Property(m => m.Subject)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;



            this.Ignore(m => m.From);
            this.Ignore(m => m.ReplyTo);



            //Many to one, Required, Collection
            ////1..*-1
            //this
            //    .HasMany(m => m.Statuses)
            //    .WithRequired() //cannot add a Part without an Message. NOTE: It does NOT mean that you have to have atleast 1 Part. 
            //    .HasForeignKey(m2 => m2.MessageId) 
            //    ;

            this
                .HasMany(m => m.Headers)
                .WithOptional()
                //cannot add a Part without an Message. NOTE: It does NOT mean that you have to have atleast 1 Part. 
                .HasForeignKey(m2 => m2.MessageId)
                ;

            this
                .HasMany(m => m.Addresses)
                .WithOptional()
                //cannot add a Part without an Message. NOTE: It does NOT mean that you have to have atleast 1 Part. 
                .HasForeignKey(m2 => m2.MessageId)
                ;

            this
                .HasMany(m => m.Bodies)
                .WithOptional()
                //cannot add a Part without an Message. NOTE: It does NOT mean that you have to have atleast 1 Part. 
                .HasForeignKey(m2 => m2.MessageId)
                ;

            this
                .HasMany(m => m.Attachments)
                .WithOptional()
                //cannot add a Part without an Message. NOTE: It does NOT mean that you have to have atleast 1 Part. 
                .HasForeignKey(m2 => m2.MessageId)
                ;

            //Create ManyToMany Link Table:

            this
                .HasMany(p => p.Tags)
                .WithMany(t => t.Messages)
                .Map(x =>
                         {
                             x.ToXActLibTable("Message_MessageTag");
                             x.MapLeftKey("MessageId");
                             x.MapRightKey("MessageTagId");
                         });




        }
    }
}