namespace XAct.Net.Initialization.PersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Messaging;

    /// <summary>
    /// 
    /// </summary>
    public class PersistedFileMetadataModelPersistenceMap : EntityTypeConfiguration<PersistedFileMetadata>, IPersistedFileMetadataModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersistedFileMetadataModelPersistenceMap"/> class.
        /// </summary>
        public PersistedFileMetadataModelPersistenceMap()
        {
            this.ToXActLibTable("PersistedFileMetadata");


            this.HasKey(t => t.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.Key)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(m => m.Value)
                .IsOptional()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.OwnerFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this.Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);
            this.Property(m => m.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++);
            this.Property(m => m.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++);
            this.Property(m => m.LastModifiedBy)
                .DefineRequired64CharLastModifiedBy(colOrder++);
                ;
        }
    }
}