﻿namespace XAct.Net.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Initialization.PersistenceMaps;
    using XAct.Net.Messaging;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class PersistedFileDbModelBuilder : IPersistedFileDbModelBuilder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingDbModelBuilder"/> class.
        /// </summary>
        public PersistedFileDbModelBuilder()
        {
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add((EntityTypeConfiguration<PersistedFile>)XAct.DependencyResolver.Current.GetInstance<IPersistedFileModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<PersistedFileMetadata>)XAct.DependencyResolver.Current.GetInstance<IPersistedFileMetadataModelPersistenceMap>());



        }
    }
}
