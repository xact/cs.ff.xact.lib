﻿namespace XAct.Net.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Initialization.PersistenceMaps;
    using XAct.Net.Messaging;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class MessagingDbModelBuilder : IMessagingDbModelBuilder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingDbModelBuilder"/> class.
        /// </summary>
        public MessagingDbModelBuilder()
        {
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add((EntityTypeConfiguration<MessageStatus>)XAct.DependencyResolver.Current.GetInstance<IMessageStatusModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<MessageHeader>)XAct.DependencyResolver.Current.GetInstance<IMessageHeaderModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<MessageAddress>)XAct.DependencyResolver.Current.GetInstance<IMessageAddressModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<MessageBody>)XAct.DependencyResolver.Current.GetInstance<IMessageBodyModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<MessageAttachment>)XAct.DependencyResolver.Current.GetInstance<IMessageAttachmentModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<MessageTag>)XAct.DependencyResolver.Current.GetInstance<IMessageTagModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<Message>)XAct.DependencyResolver.Current.GetInstance<IMessageModelPersistenceMap>());


            //TODO: NEED BETTER RELATIONSHIPS IN MAPPINGS


        }
    }
}
