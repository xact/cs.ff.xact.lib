﻿namespace XAct.Net.Initialization.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Net.Initialization.PersistenceMaps;
    using XAct.Net.Status;

    /// <summary>
    /// 
    /// </summary>
    public class ProgressStatusDbModelBuilder : IProgressStatusDbModelBuilder
    {

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add((EntityTypeConfiguration<ProgressStatus>)XAct.DependencyResolver.Current.GetInstance<IProgressStatusModelPersistenceMap>());
        }
    }
}