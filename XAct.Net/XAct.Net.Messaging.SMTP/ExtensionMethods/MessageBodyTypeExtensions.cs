﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.Net.Mail;
    using System.Net.Mime;
    using XAct.Net.Messaging;

    /// <summary>
    /// Extension Methods to the 
    /// <see cref="MessageBodyType"/>
    /// enumeration
    /// </summary>
    public static class MessageBodyTypeExtensions
    {
        /// <summary>
        /// Converts the <see cref="MessageBodyType"/> 
        /// the the type required by the 
        /// <see cref="AlternateView"/> of 
        /// the System <see cref="MailMessage"/>.
        /// </summary>
        /// <param name="messageBodyType">Type of the message body.</param>
        /// <returns>A string value</returns>
        public static string MapToMailMessageBodyType(this MessageBodyType messageBodyType)
        {
            switch (messageBodyType)
            {
                case MessageBodyType.Html:
                    return MediaTypeNames.Text.Html;
                default:
                    return MediaTypeNames.Text.Plain;
            }
        }
    }
}