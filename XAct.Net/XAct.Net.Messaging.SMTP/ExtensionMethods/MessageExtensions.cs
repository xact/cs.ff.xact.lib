﻿namespace XAct
{
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using XAct.Net.Messaging;

    /// <summary>
    /// Extensions to the <see cref="XAct.Net.Messaging.Message"/>
    /// object, in the context of <see cref="MailMessage"/>
    /// </summary>
    public static class MessageExtensions
    {
        /// <summary>
        /// Maps this <see cref="XAct.Net.Messaging.Message"/>
        /// to a 
        /// <see cref="System.Net.Mail.MailMessage"/>
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        /// <remarks>
        /// Internally, throughout the application,
        /// and framework, use the Message, which is intended
        /// to be a cross protocol (SMTP, Twitter, whatever) entity.
        /// But when it's time to use SMTP, we have to map it to
        /// a MailMessage -- using this extension method.
        /// </remarks>
        public static MailMessage MapToMailMessage(this Message message)
        {
            MailMessage mailMessage = new MailMessage();

            //Map Headers:
            message.MapHeaders(mailMessage);

            //Map From:
            message.MapFrom(mailMessage);

            //ReplyTo:
            message.MapReplyTo(mailMessage);

            message.MapOutgoingAddresses(mailMessage);

            //Subject:
            mailMessage.Subject = message.Subject;

            message.MapBodies(mailMessage);


            message.MapPriorityTo(mailMessage);


            message.MapImportanceTo(mailMessage);

            //LinkedResource linkedResource =
            // new LinkedResource(
            //   "screw_64_64.png",
            //   "image/png"); //MediaTypeNames.Image.Png not available.

            // //Give it an id/name that the html can refer to:
            // linkedResource.ContentId = "pic1";

            // //Now that we have an embedded resource, let's update
            // //our html message to refer to it:

            // string newHtml =
            // "" +
            // "A Test HTML Message" +
            // "" +
            // "";

            // //Link the resource to the AlternateView, 
            // //(and not the Message itself):
            // htmlAlternateView.LinkedResources.Add(linkedResource);


            message.MapAttachments(mailMessage);

            //Right...on to next attachment, or you're done.
            return mailMessage;
        }




        private static void MapHeaders(this Message message, MailMessage mailMessage)
        {
            foreach (MessageHeader messageHeader in message.Headers)
            {
                //There is no such thing as a System.Net.Mail.MessageHeader,
                //it's just a NameValueCollection, no Extensionmethod required:
                mailMessage.Headers.Add(messageHeader.Name, messageHeader.Value);
            }
        }

        private static void MapPriorityTo(this Message message, MailMessage mailMessage)
        {
            switch (message.Priority)
            {
                case MessagePriority.Urgent:
                    mailMessage.Priority = MailPriority.High;
                    break;
                case MessagePriority.Normal:
                    mailMessage.Priority = MailPriority.Normal;
                    break;
                case MessagePriority.Low:
                    mailMessage.Priority = MailPriority.Low;
                    break;
                case MessagePriority.VeryLow:
                    mailMessage.Priority = MailPriority.Low;
                    break;
                default:
                    mailMessage.Priority = MailPriority.Normal;
                    break;
            }
        }


        private static void MapImportanceTo(this Message message, MailMessage mailMessage)
        {

            return;
        }

        /// <summary>
        /// Maps the From Address.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="mailMessage">The mail message.</param>
        private static void MapFrom(this Message message, MailMessage mailMessage)
        {
            mailMessage.From = message.From.ToMailAddress();

        }


        private static void MapReplyTo(this Message message, MailMessage mailMessage)
        {
            
            //ReplyTo is important when sending email on behalf of end users.
            //But I don't think we should be setting it if not there. Too magic.
            if (message.ReplyTo == null)
            {
                return;


                //WATCH OUT:
                //http://blackwidows.co.uk/blog/2006/10/02/reply-to-header-can-create-bounces/

//#if (NET40 || NET45)
//                mailMessage.ReplyToList.Add(message.From.ToMailAddress());
//#else
//            mailMessage.ReplyTo = message.From.ToMailAddress();
//#endif
            }
            mailMessage.ReplyToList.Add(message.ReplyTo.ToMailAddress());

        }


        private static void MapOutgoingAddresses(this Message message, MailMessage mailMessage)
        {
            foreach (MessageAddress mailAddress in message.To)
            {
                mailMessage.To.Add(mailAddress.ToMailAddress());
            }
            foreach (MessageAddress mailAddress in message.Cc)
            {
                mailMessage.CC.Add(mailAddress.ToMailAddress());
            }
            foreach (MessageAddress mailAddress in message.Bcc)
            {
                mailMessage.Bcc.Add(mailAddress.ToMailAddress());
            }
        }





        private static void MapBodies(this Message message, MailMessage mailMessage)
        {
            //IMPORTANT:
            //When working with AlternateView 
            //Make sure mailMessage.Body is empty
            SetMailMessageAlternateView(message, mailMessage, MessageBodyType.Text);

            SetMailMessageAlternateView(message, mailMessage, MessageBodyType.Html);
        }


        /// <summary>
        /// Sets the mail message AlternateView if there is an equivalent MessageBody in the given Message.
        /// <para>
        /// Adds nothing if there are no bodies.
        /// </para>
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="mailMessage">The mail message.</param>
        /// <param name="messageBodyType">Type of the message body.</param>
        /// <returns></returns>
        private static AlternateView SetMailMessageAlternateView(this Message message, MailMessage mailMessage,
                                                                 MessageBodyType messageBodyType)
        {
            //Get the Body that matches the given specification:
            MessageBody textBody = message.Bodies.FirstOrDefault(b => b.Type == messageBodyType);

            if (textBody == null)
            {
                return null;
            }


            //Make a new AlternateView:
            AlternateView alternateView
                = AlternateView.CreateAlternateViewFromString
                    (textBody.Value, //string
                     null, //ContentType
                     messageBodyType.MapToMailMessageBodyType() //Text, Html, or Rich
                    );


            //HACK:
            if (messageBodyType == MessageBodyType.Html)
            {
                //See if there are any Attachments marked as inline:
                foreach (
                    MessageAttachment messageAttachment in
                        message.Attachments.Where(a => a.Type == MessageAttachmentType.Inline))
                {
                    Stream stream = new MemoryStream(messageAttachment.Value);

                    LinkedResource linkedResource = new LinkedResource(stream, messageAttachment.ContentType);
                    linkedResource.ContentId = messageAttachment.ContentId??(messageAttachment.ContentId = messageAttachment.Name);

                    alternateView.LinkedResources.Add(linkedResource);
                }
            }


            mailMessage.AlternateViews.Add(alternateView);

            return alternateView;
        }


        /// <summary>
        /// Attaches an attachment to the given <see cref="XAct.Net.Messaging.Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="mailMessage">The mail message.</param>
        private static void MapAttachments(this Message message, MailMessage mailMessage)
        {
            foreach (MessageAttachment messageAttachment in message.Attachments)
            {
                Attachment attachment = messageAttachment.MapToAttachment();
                if (messageAttachment.Type == MessageAttachmentType.Inline)
                {
                    continue;
                }
                mailMessage.Attachments.Add(attachment);
            }

        }
    }
}
