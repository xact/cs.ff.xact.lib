
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif




// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Net.Mail;
    using XAct.Net.Messaging;

    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="MessageAddress"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class MessageAddressExtensions
// ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// Maps the values from a given 
        /// <see cref="MailMessage"/>
        /// to this entity.
        /// </summary>
        /// <param name="messageAddress">The address.</param>
        /// <param name="mailAddress">The mail address.</param>
        /// <param name="addressType">Type of the address.</param>
        public static void MapFrom(this MessageAddress messageAddress, MailAddress mailAddress,
                                   MessageAddressType addressType)
        {
            if (addressType == MessageAddressType.Undefined)
            {
                throw new ArgumentException("addressType");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            messageAddress.Type = addressType;
            messageAddress.Value = mailAddress.Address;
            messageAddress.Name = mailAddress.DisplayName;
        }


        /// <summary>
        /// Maps this <see cref="MessageAttachment"/>
        /// based entity to a 
        /// <see cref="MailAddress"/>
        /// </summary>
        /// <param name="messageAddress">The message address.</param>
        /// <returns></returns>
        public static MailAddress ToMailAddress(this MessageAddress messageAddress)
        {
            MailAddress mailAddress =
                (string.IsNullOrEmpty(messageAddress.Name))
                    ? new MailAddress(messageAddress.Value)
                    : new MailAddress(messageAddress.Value, messageAddress.Name)
                ;

            return mailAddress;
        }


        /// <summary>
        /// Maps this <see cref="MessageAddress"/>
        /// to a
        /// <see cref="MailAddress"/>
        /// <para>
        /// Note that the <see cref="MessageAddress.Type"/>
        /// is lost in the mapping.
        /// </para>
        /// </summary>
        /// <param name="messageAddress">The this address.</param>
        /// <returns></returns>
        public static MailAddress MapTo(this MessageAddress messageAddress)
        {
            return new MailAddress(messageAddress.Value, messageAddress.Name);
        }

    }
}