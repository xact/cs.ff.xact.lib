﻿namespace XAct
{
    using System.IO;
    using System.Net.Mail;
    using System.Net.Mime;
    using XAct.Net.Messaging;

    /// <summary>
    /// 
    /// </summary>
    public static class MessageAttachmentExtensions
    {

        /// <summary>
        /// Maps to.
        /// </summary>
        /// <param name="messageAttachment">The message attachment.</param>
        /// <returns></returns>
        public static Attachment MapToAttachment(this MessageAttachment messageAttachment)
        {

            Attachment attachment;

            //SUPER IMPORTANT: Ensure the MemoryStream is left open
            //Or it will be closed by the time it tries to send the message in a sec
            MemoryStream stream = new MemoryStream(messageAttachment.Value);

            //Attachment attachment;
               
                attachment = new Attachment(stream, MediaTypeNames.Application.Octet);
                //MediaTypeNames.Application.Octet

                attachment.Name = messageAttachment.Name;


                attachment.ContentId = messageAttachment.ContentId;

                if (messageAttachment.Type == MessageAttachmentType.Inline)
                {
                    attachment.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                }
                else
                {
                    attachment.ContentDisposition.DispositionType = DispositionTypeNames.Attachment;
                }


                if (messageAttachment.CreatedOnUtc.HasValue)
                {
                    attachment.ContentDisposition.CreationDate = messageAttachment.CreatedOnUtc.Value;
                }

                if (messageAttachment.LastModifiedOnUtc.HasValue)
                {
                    attachment.ContentDisposition.ModificationDate = messageAttachment.LastModifiedOnUtc.Value;
                }

                attachment.ContentDisposition.Size = messageAttachment.Size;
                attachment.ContentDisposition.FileName = messageAttachment.Name;

 


            

            
            return attachment;
        }
    }
}
