﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging.Implementations
// ReSharper restore CheckNamespace
{
    using System.ComponentModel;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Performance;
    using XAct.Net.Messaging.Services.Configuration;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IMessageDeliveryService"/>
    /// to send <see cref="Messaging.Message"/>s using SMTP.
    /// <para>
    /// Note that one could call <see cref="SmtpMessageDeliveryService"/> 
    /// directly -- but it is meant to be invoked
    /// via <see cref="IMessagingService"/>, routing to the correct
    /// <see cref="IMessageDeliveryService"/>
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IMessageDeliveryService),BindingLifetimeType.Undefined,Priority.Low,"SMTP")]
    public class SmtpMessageDeliveryService : ISmtpMessageDeliveryService
    {
        private readonly ITracingService _tracingService;
        private readonly IPerformanceCounterService _performanceCounterService;
        private readonly ISmtpMessageDeliveryServiceConfiguration _smtpMessagingServiceConfiguration;


        /// <summary>
        /// Gets the configuration object
        /// used to configure this 
        /// message delivery service
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public ISmtpMessageDeliveryServiceConfiguration Configuration { get { return _smtpMessagingServiceConfiguration; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="SmtpMessageDeliveryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="performanceCounterService">The performance counter service.</param>
        /// <param name="smptpMessagingConfiguration">The SMPTP messaging configuration.</param>
        public SmtpMessageDeliveryService(ITracingService tracingService,
            IPerformanceCounterService performanceCounterService,
            ISmtpMessageDeliveryServiceConfiguration smptpMessagingConfiguration)
        {
            _tracingService = tracingService;
            _performanceCounterService = performanceCounterService;
            _smtpMessagingServiceConfiguration = smptpMessagingConfiguration;

        }



        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void SendMessage(Message message)
        {
            if ((message.From == null)&& (Configuration.AutoFillFromAddressIfEmpty))
            {
                message.From = Configuration.FromAddress;
            }

            SendMailMessage(message.MapToMailMessage());
        }



        ///// <summary>
        /////   Sends the message.
        ///// </summary>
        ///// <param name = "toAddress">To address.</param>
        ///// <param name = "subject"></param>
        ///// <param name = "body">The message.</param>
        //public void SendMessage(string toAddress, string subject, string body)
        //{
        //    _tracingService.Log(TraceLevel.Verbose, "Sending Messsage (toAddress={0}, subject={1})", toAddress,
        //                             subject);


        //    MailAddress fromFullAddress = _smtpMessagingServiceConfiguration.DefaultMessageFrom.ToMailAddress();



        //    SendMailMessage(message);

        //}

        private void SendMailMessage(MailMessage mailMessage)
        {



            SmtpClient mailClient = new SmtpClient
                                        {
                                            //The config file in .NET 3.5 doesn't have an SSL
                                            //flag -- so can only be set by code. Well known defect.
                                            //Think it's fixed in later versions of .NET
                                            //if (_smtpMessagingServiceConfiguration.Ssl.HasValue(){
                                            //  EnableSsl = _smtpMessagingServiceConfiguration.Ssl.Value
                                            //}
                                        }; 


            if (_smtpMessagingServiceConfiguration.Enabled)
            {
                /*
                 * <mailSettings>
                 *   <smtp deliveryMethod="Network" from="skysigal@xact-solutions.com">
                 *   <network defaultCredentials="false" 
                 host="smtp.gmail.com" 
                 port="587" 
                 userName="skysigal@xact-solutions.com" 
                 password="...." />
      </smtp>
    </mailSettings>
                 */

                if (_smtpMessagingServiceConfiguration.ServerName != null)
                {
                    mailClient.Host = _smtpMessagingServiceConfiguration.ServerName;
                }
                if (!_smtpMessagingServiceConfiguration.Port.HasValue)
                {
                    mailClient.Port = _smtpMessagingServiceConfiguration.Port.Value;
                }
                if (_smtpMessagingServiceConfiguration.Ssl.HasValue)
                {
                    mailClient.EnableSsl = _smtpMessagingServiceConfiguration.Ssl.Value;
                }
                if ((!_smtpMessagingServiceConfiguration.UserName.IsNullOrEmpty()) &&
                    (!_smtpMessagingServiceConfiguration.Password.IsNullOrEmpty()))
                {
                    mailClient.UseDefaultCredentials = false;
                    mailClient.Credentials = new NetworkCredential(_smtpMessagingServiceConfiguration.UserName, _smtpMessagingServiceConfiguration.Password);
                }
                if (_smtpMessagingServiceConfiguration.TimeoutSeconds != null)
                {
                    mailClient.Timeout = _smtpMessagingServiceConfiguration.TimeoutSeconds.Value;
                }

            }


            //STARTTLS
            //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("skysigal@xact-solutions.com", "");
            //mailClient.UseDefaultCredentials = false;
            //mailClient.Credentials = credentials;
            //mailClient.EnableSsl = true;

            try
            {

                _performanceCounterService.Offset("XActLib", "SmtpMessageDeliveryService.MessageSent.Attempt", instanceName: null, amount: 1, raiseExceptionIfNotFound: false);
                mailClient.Send(mailMessage);
                _performanceCounterService.Offset("XActLib", "SmtpMessageDeliveryService.MessageSent.Succeeded", instanceName: null, amount: 1, raiseExceptionIfNotFound: false);

            }
            catch
            {
                _performanceCounterService.Offset("XActLib", "SmtpMessageDeliveryService.MessageSent.Failed", instanceName: null, amount: 1, raiseExceptionIfNotFound: false);
                throw;
            }
            //foreach (Attachment attachment in mailMessage.Attachments)
            //{
            //    Stream stream = attachment.GetDynamicAttributes()["Stream"] as Stream;
            //    if (stream!=null){stream.Close();}
            //}
            //mailClient.SendCompleted += MailClient_SendCompleted;
            //mailClient.SendAsync(message, userToken: message);
        }


        #region Event Handlers

        private void MailClient_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
// ReSharper disable SuggestUseVarKeywordEvident
            MailMessage mailMessage = e.UserState as MailMessage;
// ReSharper restore SuggestUseVarKeywordEvident


            if (e.Error != null)
            {
                _tracingService.TraceException(
                    TraceLevel.Error,
                    e.Error,
                    "An exception was raised while sending e-mail message to {0}",
                    (mailMessage == null ? "<unknown>" : mailMessage.To.First().Address)
                    );
            }
            _tracingService.Trace(TraceLevel.Verbose, "Email on its way to {0}",
                                     (mailMessage == null ? "<unknown>" : mailMessage.To.First().Address)
                );
        }

        #endregion

    }
}
