﻿namespace XAct.Net.Messaging
{
    using XAct.Net.Messaging.Services.Configuration;

    /// <summary>
    /// Contract for a service to deliver <see cref="Message"/>s
    /// using Smtp (email).
    /// <para>
    /// Note that one could call <see cref="ISmtpMessageDeliveryService"/> 
    /// directly -- but it is meant to be invoked
    /// via <see cref="IMessagingService"/>, routing to the correct
    /// <see cref="IMessageDeliveryService"/>
    /// </para>
    /// <para>
    /// Adds no functionality: only used by an IoC to differentiate
    /// between contracts.
    /// </para>
    /// </summary>
    public interface ISmtpMessageDeliveryService : IMessageDeliveryService, IHasXActLibService
    {


        /// <summary>
        /// Gets the configuration object
        /// used to configure this  
        /// message delivery service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        ISmtpMessageDeliveryServiceConfiguration Configuration { get; }
    }
}