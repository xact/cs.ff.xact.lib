﻿namespace XAct.Net.Messaging.Services.Configuration
{
    /// <summary>
    /// Contract for the configuration of an 
    /// implementation of <see cref="ISmtpMessageDeliveryService"/>
    /// </summary>
    public interface ISmtpMessageDeliveryServiceConfiguration : IHasXActLibServiceConfiguration, IHasEnabled
    {
        /// <summary>
        /// If a message is sent that has no sender, 
        /// it will be deemed a system message,
        /// and filled with <see cref="FromAddress"/>.
        /// </summary>
        bool AutoFillFromAddressIfEmpty { get; set; }

        /// <summary>
        /// Gets the SMTP Default System 'From' email address.
        /// </summary>
        /// <value>The SMTP from email address.</value>
        MessageAddress FromAddress { get; set; }

        /// <summary>
        /// Gets the SMTP relay server.
        /// <para>
        /// eg: "smtp.gmail.com"
        /// </para>
        /// </summary>
        /// <value>The SMTP relay server.</value>
        string ServerName { get; set; }

        /// <summary>
        /// Gets the smtp relay server's port.
        /// <para>
        /// eg: 
        /// </para>
        /// </summary>
        /// <value>The port.</value>
        int? Port { get; set; }

        /// <summary>
        /// Gets a value indicating whether 
        /// the server requires SSL to be enabled.
        /// </summary>
        /// <value><c>true</c> if SSL needs to be enabled; otherwise, <c>false</c>.</value>
        bool? Ssl { get; set; }


        /// <summary>
        /// Gets or sets the SMTP Account user name.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        string UserName { get; set; }


        /// <summary>
        /// Gets or sets the SMTP Account password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        string Password { get; set; }

        /// <summary>
        /// The connection timeout. Default SMTP timeout is 100000 seconds (a long time...like 27 minutes).
        /// </summary>
        int? TimeoutSeconds { get; set; }
    }
}