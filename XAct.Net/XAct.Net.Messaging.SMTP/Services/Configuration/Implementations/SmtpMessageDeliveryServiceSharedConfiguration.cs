﻿namespace XAct.Net.Messaging.Services.Configuration.Implementations
{
    using XAct.Net.Messaging.Implementations;
    using XAct.Services;

    /// <summary>
    /// A configuration package used by an IoC to DI
    /// into <see cref="MessagingService"/> during it's
    /// Construction.
    /// </summary>
    /// <summary>
    /// An interface of settings that an app will
    /// need in order to send emails.
    /// <para>
    /// Most apps have emails that are sent from a specific
    /// address, via a specific route. 
    /// This allows this kind of configuration, 
    /// while allowing for some indirection.
    /// </para>
    /// </summary>
    public class SmtpMessageDeliveryServiceConfiguration : ISmtpMessageDeliveryServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// If a message is sent that has no messaeg, 
        /// it will be deemed a system messaeg,
        /// and filled with <see cref="FromAddress"/>.
        /// </summary>
        public bool AutoFillFromIfEmpty { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="T:XAct.IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; set; }



        /// <summary>
        /// If a message is sent that has no sender,
        /// it will be deemed a system message,
        /// and filled with <see cref="FromAddress" />.
        /// </summary>
        public bool AutoFillFromAddressIfEmpty { get; set; }

        /// <summary>
        /// Gets the SMTP Default System 'From' email address.
        /// </summary>
        /// <value>The SMTP from email address.</value>
        public MessageAddress FromAddress { get; set; }

        
                /// <summary>
        /// Gets the SMTP relay server.
        /// <para>
        /// eg: "smtp.gmail.com"
        /// </para>
        /// </summary>
        /// <value>The SMTP relay server.</value>
        public string ServerName { get; set; }

        /// <summary>
        /// Gets the smtp relay server's port.
        /// <para>
        /// eg: 
        /// </para>
        /// </summary>
        /// <value>The port.</value>
        public int? Port { get; set; }

        /// <summary>
        /// Gets a value indicating whether 
        /// the server requires SSL to be enabled.
        /// </summary>
        /// <value><c>true</c> if SSL needs to be enabled; otherwise, <c>false</c>.</value>
        public bool? Ssl { get; set; }



        /// <summary>
        /// Gets or sets the SMTP Account user name.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }


        /// <summary>
        /// Gets or sets the SMTP Account password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }


        /// <summary>
        /// The connection timeout. Default SMTP timeout is 100000.
        /// Source code says that's seconds...but it strikes me as a probable
        /// typo. cause that'sa long time (...like 27 minutes).
        /// </summary>
        public int? TimeoutSeconds { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SmtpMessageDeliveryServiceConfiguration" /> class.
        /// </summary>
        public SmtpMessageDeliveryServiceConfiguration()
        {
            AutoFillFromIfEmpty = true;
        }

    }
}


