using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
// associated with an assembly.

[assembly: AssemblyTitle("XAct.Net.Messaging.SMTP")]
[assembly: AssemblyDescription("An XActLib assembly: Service for delivering Messages via SMTP")]
#if DEBUG
[assembly: AssemblyConfiguration("DEBUG")]
#else 
[assembly: AssemblyConfiguration("")]
#endif

[assembly: AssemblyCompany("XAct Software Solutions, Inc.")]
[assembly: AssemblyProduct("XActLib")]
[assembly: AssemblyCopyright("Copyright © XAct Software Solutions, Inc. 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

//PLC: [assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

//PLC: [assembly: Guid("ecc6676d-471f-426c-b349-a27804b0ef47")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

//Allow this signed assembly access from Partially trusted: http://bit.ly/vyFFTu
//[assembly: AllowPartiallyTrustedCallers()]
[assembly: ComVisibleAttribute(false)]
