namespace XAct.Tests
{
    using System;
    using System.Linq;
    using System.Net.Mail;
    using System.Net.Mime;
    using NUnit.Framework;
    using XAct.Net.Messaging;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class XActMessagePartsToSmtpMailMessagePartsConversionTests
    {

        private bool _initialized = false;
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();
            _initialized = true;
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void TestClassInitializedCorrectly()
        {
            //2.1.505
            //Current:2.6.2.12296
            Assert.IsTrue(_initialized);
        }

        [Test]
        public void CanMapMailAddressToMessageAddress()
        {
            System.Net.Mail.MailAddress mailAddress = new MailAddress("bob@somewhere.com");

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.MapFrom(mailAddress,MessageAddressType.From);

            Assert.AreEqual(MessageAddressType.From, messageAddress.Type);
            Assert.AreEqual("bob@somewhere.com", messageAddress.Value);

        }

        [Test]
        public void CanMapMailAddressToMessageAddress2()
        {
            System.Net.Mail.MailAddress mailAddress = new MailAddress("bob@somewhere.com","Bobby.Q");
#pragma warning disable 168
            string host = mailAddress.Host;
            string check = mailAddress.ToString();
#pragma warning restore 168

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.MapFrom(mailAddress, MessageAddressType.From);

            Assert.AreEqual(MessageAddressType.From, messageAddress.Type);
            Assert.AreEqual("bob@somewhere.com", messageAddress.Value);
            Assert.AreEqual(check, messageAddress.ToString());

        }


        [Test]
        public void CanMapMessageToMailMessage()
        {
            Message message = TestMessageFactory.FactoryMessage0();


            MailMessage mailMessage = message.MapToMailMessage();

            Assert.IsNotNull(mailMessage);
        }


        [Test]
        public void CanMapMessageToMailMessagePriority()
        {
            Message message = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            Assert.AreEqual(MailPriority.High, mailMessage.Priority);
        }

        [Test]
        public void CanMapMessageToMailMessageHeaders()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            Assert.IsTrue(mailMessage.Headers.Count  == 2);
        }

        [Test]
        public void CanMapMessageToMailMessageHeaders2()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            string value = mailMessage.Headers["SomeHeader"];

            Assert.IsNotNullOrEmpty(value);
        }

        [Test]
        public void CanMapMessageToMailMessageToAddresses()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            
            Assert.IsTrue(mailMessage.To.Count == 2);
        }


        [Test]
        public void CanMapMessageToMailMessageToAddresses2()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            Assert.AreEqual("\"Abbot C.\" <a@b.com>", mailMessage.To[0].ToString());
            Assert.AreEqual("\"Abbot C. #2\" <a@b2.com>",  mailMessage.To[1].ToString());
        }



        [Test]
        public void CanMapMessageToMailMessageCcAddresses()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            Assert.IsTrue(mailMessage.CC.Count == 2);

        }


        [Test]
        public void CanMapMessageToMailMessageCcAddresses2()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            Assert.AreEqual("\"CC Guy\" <a@cc.com>", mailMessage.CC[0].ToString());
            Assert.AreEqual("\"CC Guy #2\" <a@cc2.com>", mailMessage.CC[1].ToString());
        }

        [Test]
        public void CanMapMessageToMailMessageBccAddresses()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            Assert.IsTrue(mailMessage.Bcc.Count == 2);

        }


        [Test]
        public void CanMapMessageToMailMessageBccAddresses2()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            Assert.AreEqual("\"BCC Guy\" <a@bcc.com>", mailMessage.Bcc[0].ToString());
            Assert.AreEqual("\"BCC Guy #2\" <a@bcc2.com>", mailMessage.Bcc[1].ToString());
        }



        [Test]
        public void CanMapMessageToMailMessageSubject()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();


            Assert.AreEqual("wada wada wada", mailMessage.Subject);
        }


        [Test]
        public void CanMapMessageToMailMessageBody()
        {
            Message message  = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            Assert.IsTrue(mailMessage.AlternateViews.Count == 2);
            Assert.IsNullOrEmpty(mailMessage.Body);

        }

        [Test]
        public void CanMapMessageToMailMessageBody_Comparison()
        {
            MailMessage mailMessage = new MailMessage();


            
            AlternateView textAlternateView
  = AlternateView.CreateAlternateViewFromString
    ("Some Text Body...", null, MediaTypeNames.Text.Plain);

            AlternateView htmlAlternateView
= AlternateView.CreateAlternateViewFromString
("Some <b>HTML</b> Body...", null, MediaTypeNames.Text.RichText); 

            mailMessage.AlternateViews.Add(textAlternateView);
            mailMessage.AlternateViews.Add(htmlAlternateView);

            Assert.IsTrue(mailMessage.AlternateViews.Count == 2);
            Assert.IsNullOrEmpty(mailMessage.Body);
        }



        [Test]
        public void CanMapMessageToMailMessageBody2()
        {
            Message message = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            mailMessage.IsBodyHtml = true;

            Assert.IsTrue(mailMessage.AlternateViews.Count == 2);
            Assert.IsNullOrEmpty(mailMessage.Body);

        }

        [Test]
        public void CanMapMessageToMailMessageAttachments()
        {
            Message message = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();

            //One is attachment, one is inline.
            //Count is 1:
            Assert.AreEqual(1,mailMessage.Attachments.Count);

            //Note that the inline attachments should have been converted to LinkedResource elements
            //attached to Html view:
            AlternateView alternateView = mailMessage.AlternateViews.FirstOrDefault(x => x.ContentType.MediaType == "text/html");

            Assert.IsTrue(alternateView.LinkedResources.Count == 1);

        }


        [Test]
        public void CanMapMessageToMailMessageAttachments2()
        {
            Message message = TestMessageFactory.FactoryMessage0();
            MailMessage mailMessage = message.MapToMailMessage();


            Attachment attachment = mailMessage.Attachments[0];

            Assert.IsTrue(attachment.Name.Length>0);
            Assert.IsTrue(attachment.ContentDisposition.FileName.Length > 0);
            Assert.IsTrue(attachment.ContentDisposition.CreationDate > DateTime.UtcNow.Subtract(new TimeSpan(0,0,10)));

        }


    }

//Class:End
}

//Namespace:End