namespace XAct.Tests
{
    using System;
    using System.Net.Mail;
    using NUnit.Framework;
    using XAct.Net.Messaging;
    using XAct.Net.Messaging.Implementations;
    using XAct.Net.Messaging.Services.Configuration;
    using XAct.Net.Messaging.Tests;
    using XAct.Tests;

    [TestFixture]
    public class ISmtpMessageDeliveryServiceUnitTests
    {


        private bool _initialized = false;

        private bool _hasEnoughInfoToContactMTA = false;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

            _initialized = true;

           TestSmtpMessageDeliveryServiceConfigurationHelper.InitializeSmtpConfigurationFromEnvironmentVariable();

                        ISmtpMessageDeliveryServiceConfiguration smtpMessageDeliveryServiceConfiguration =
                XAct.DependencyResolver.Current.GetInstance<ISmtpMessageDeliveryServiceConfiguration>();


            _hasEnoughInfoToContactMTA = smtpMessageDeliveryServiceConfiguration.Password.IsNullOrEmpty() ==false;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void TestClassInitializedCorrectly()
        {
            //2.1.505
            //Current:2.6.2.12296
            Assert.IsTrue(_initialized);
        }

        [Test]
        public void CanInstantiatedSmtpMessageDeliveryService()
        {
            SmtpMessageDeliveryService smtpService = new SmtpMessageDeliveryService( null, null, null);

            Assert.IsNotNull(smtpService);
        }

        [Test]
        public void CanGetISmtpMessageDeliveryService()
        {
            XAct.Net.Messaging.ISmtpMessageDeliveryService smtpService =
                XAct.DependencyResolver.Current.GetInstance<ISmtpMessageDeliveryService>();

            Assert.IsNotNull(smtpService);
        }




        [Test]
        public void ConversionToNETMailMessageDoesNotGenerateId()
        {
            //XAct.Net.Messaging.IMessagingService smtpService = XAct.DependencyResolver.Current.GetInstance<IMessagingService>();


            Message message = TestMessageFactory.CreateTestMessageWithReplyToAndDualBodies();


            //At ths point, the message.MessageId is Guid.Empty

            //if we convert it to a .NET object, the .NET Object should have a Guid.

#pragma warning disable 168
            MailMessage x = message.MapToMailMessage();
#pragma warning restore 168


            Assert.IsTrue(true);

        }

        [Test]
        public void SendEnglishBodyMessageWithReplyToViaISmtpMessagingService()
        {
            if (!_hasEnoughInfoToContactMTA)
            {
                Assert.Ignore("Config or Env settings not configured enough to test smtp connection to mta.");
                return;
            }

            XAct.Net.Messaging.ISmtpMessageDeliveryService smtpService =
                XAct.DependencyResolver.Current.GetInstance<ISmtpMessageDeliveryService>();


            Message message = TestMessageFactory.CreateTestMessageWithReplyToAndDualBodies();



            smtpService.SendMessage(message);

            Assert.IsTrue(true);
        }





        [Test]
        public void SendEnglishBodyMessageWithReplyToViaIMessagingService()
        {
            if (!_hasEnoughInfoToContactMTA)
            {
                Assert.Ignore("Config or Env settings not configured enough to test smtp connection to mta.");
                return;
            }
            
            XAct.Net.Messaging.IMessagingService smtpService = XAct.DependencyResolver.Current.GetInstance<IMessagingService>();


            Message message = TestMessageFactory.CreateTestMessageWithReplyToAndDualBodies();

                smtpService.SendMessage(message);

            Assert.IsTrue(true);
        }

        
        [Test]
        public void SendEnglishBodyMessageWithNoReplyToViaIMessagingService()
        {
            if (!_hasEnoughInfoToContactMTA)
            {
                Assert.Ignore("Config or Env settings not configured enough to test smtp connection to mta.");
                return;
            }
            
            XAct.Net.Messaging.IMessagingService smtpService = XAct.DependencyResolver.Current.GetInstance<IMessagingService>();


            Message message = TestMessageFactory.CreateTestMessageWithReplyToAndDualBodiesButNoReplyTo();

                smtpService.SendMessage(message);

            Assert.IsTrue(true);
        }





        [Test]
        public void SendFrenchEmailAsText()
        {
            if (!_hasEnoughInfoToContactMTA)
            {
                Assert.Ignore("Config or Env settings not configured enough to test smtp connection to mta.");
                return;
            }

            Message message = TestMessageFactory.CreateTestMessageWithReplyToAndDualFrenchBodies();


                XAct.Net.Messaging.IMessagingService smtpService = XAct.DependencyResolver.Current.GetInstance<IMessagingService>();
                smtpService.SendMessage(message);

            Assert.IsTrue(true);
        }


        [Test]
        public void SendFrenchEmailAsHtml()
        {
            if (!_hasEnoughInfoToContactMTA)
            {
                Assert.Ignore("Config or Env settings not configured enough to test smtp connection to mta.");
                return;
            }

            Message message = TestMessageFactory.CreateMessage3();

            XAct.Net.Messaging.IMessagingService smtpService =
                XAct.DependencyResolver.Current.GetInstance<IMessagingService>();
            smtpService.SendMessage(message);



        }


        [Test]
        public void SendHtmlMessageWithInlays()
        {
            if (!_hasEnoughInfoToContactMTA)
            {
                Assert.Ignore("Config or Env settings not configured enough to test smtp connection to mta.");
                return;
            }

            Message message = TestMessageFactory.CreateTestMessageWithReplyToAndDualFrenchBodiesAndInlineImage();

                XAct.Net.Messaging.IMessagingService smtpService = XAct.DependencyResolver.Current.GetInstance<IMessagingService>();
                smtpService.SendMessage(message);

                Assert.IsTrue(_initialized);
        }




    }

}

