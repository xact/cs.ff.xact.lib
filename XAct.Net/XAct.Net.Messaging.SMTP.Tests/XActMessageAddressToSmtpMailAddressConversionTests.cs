namespace XAct.Tests
{
    using System;
    using System.Net.Mail;
    using NUnit.Framework;
    using XAct.Net.Messaging;
    using XAct.Tests;


    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class XActMessageAddressToSmtpMailAddressConversionTests
    {

        private bool _initialized = false;

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();
            
            _initialized = true;

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void TestClassInitializedCorrectly()
        {
            //2.1.505
            //Current:2.6.2.12296
            Assert.IsTrue(_initialized);
        }

        
        [Test]
        public void CanConvertMailAddress()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "Paul");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress1()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From,"a@b.com","A");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(),messageAddress.ToString());
        }
        [Test]
        public void CanConvertMailAddress2()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "Andy Smith");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }
        [Test]
        public void CanConvertMailAddress3()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "Andy O'Neil");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress4()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "Andy O. Neil");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }
        [Test]
        public void CanConvertMailAddress5()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "Smith, Andy");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress6()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "John Smith");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress7()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "John Smith, Esq.");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress8()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "John:Smith");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress9()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "John Mounsy-Smith");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress10()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "ABC (Incorporated)");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress11()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "ABC;Inc.");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }

        [Test]
        public void CanConvertMailAddress12()
        {
            MessageAddress messageAddress = new MessageAddress(MessageAddressType.From, "a@b.com", "@ttack");
            MailAddress mailAddress = messageAddress.ToMailAddress();
            Assert.AreEqual(mailAddress.ToString(), messageAddress.ToString());
        }
    }
}
