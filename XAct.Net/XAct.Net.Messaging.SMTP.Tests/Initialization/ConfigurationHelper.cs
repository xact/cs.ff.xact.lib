﻿namespace XAct.Net.Messaging.Tests
{
    using System;
    using System.Collections.Generic;
    using XAct.Net.Messaging.Services.Configuration;

    public static class TestSmtpMessageDeliveryServiceConfigurationHelper
    {


        public static void InitializeSmtpConfigurationFromEnvironmentVariable()
        {
            ISmtpMessageDeliveryServiceConfiguration smtpMessageDeliveryServiceConfiguration =
                XAct.DependencyResolver.Current.GetInstance<ISmtpMessageDeliveryServiceConfiguration>();

            LoadConfigFromAppSettings(smtpMessageDeliveryServiceConfiguration);

            LoadSettingsFromEnvVar(smtpMessageDeliveryServiceConfiguration);

        }

        private static void LoadConfigFromAppSettings(
            ISmtpMessageDeliveryServiceConfiguration smtpMessageDeliveryServiceConfiguration)
        {
            string tmp;
            tmp = System.Configuration.ConfigurationManager.AppSettings["ForSmtpTests_Enabled"];
            if (tmp != null)
            {
                smtpMessageDeliveryServiceConfiguration.Enabled = tmp.ToBool();
            }
            tmp = System.Configuration.ConfigurationManager.AppSettings["ForSmtpTests_ServerName"];
            if (tmp != null)
            {
                smtpMessageDeliveryServiceConfiguration.ServerName = tmp;
            }
            tmp = System.Configuration.ConfigurationManager.AppSettings["ForSmtpTests_Port"];
            if (tmp != null)
            {
                smtpMessageDeliveryServiceConfiguration.Port = System.Int32.Parse(tmp);
            }
            tmp = System.Configuration.ConfigurationManager.AppSettings["ForSmtpTests_FromAddress"];
            if (tmp != null)
            {
                smtpMessageDeliveryServiceConfiguration.FromAddress = new MessageAddress(tmp);
            }
            tmp = System.Configuration.ConfigurationManager.AppSettings["ForSmtpTests_Ssl"];
            if (tmp != null)
            {
                smtpMessageDeliveryServiceConfiguration.Ssl = tmp.ToBool();
            }
            tmp = System.Configuration.ConfigurationManager.AppSettings["ForSmtpTests_UserName"];
            if (tmp != null)
            {
                smtpMessageDeliveryServiceConfiguration.UserName = tmp;
            }
            tmp = System.Configuration.ConfigurationManager.AppSettings["ForSmtpTests_Password"];
            if (tmp != null)
            {
                smtpMessageDeliveryServiceConfiguration.Password = tmp;
            }
        }


        public static bool LoadSettingsFromEnvVar(
            ISmtpMessageDeliveryServiceConfiguration smtpMessageDeliveryServiceConfiguration)
        {

            //I didn't want to leave a password in my unit tests, so am using an EnvironmentVariable to set them up
            //in the Continuous Environment.
            //Format of the EnvironmentVariable is: 

            //"Enabled:true;ServerName:smtp.gmail.com;Port:465;FromAddress:skysigal@xact-solutions.com;Ssl:true;UserName:skysigal@xact-solutions.com;Password:*******"

            string variablesFromEnvironment = System.Environment.GetEnvironmentVariable("XACTLIB.SMTP",
                                                                                        EnvironmentVariableTarget.User);
            if (variablesFromEnvironment.IsNullOrEmpty())
            {
                variablesFromEnvironment = System.Environment.GetEnvironmentVariable("XACTLIB.SMTP",
                                                                                     EnvironmentVariableTarget.Machine);
            }

            //ServerName:
            if (variablesFromEnvironment.IsNullOrEmpty())
            {
                return false;
            }

            Dictionary<string, string> dictionary = variablesFromEnvironment.SplitIntoKeyValues(true);

            string key;

            key = "Enabled";
            if (dictionary.ContainsKey(key))
            {
                smtpMessageDeliveryServiceConfiguration.Enabled = dictionary[key].ToBool();
            }

            key = "ServerName";
            if (dictionary.ContainsKey(key))
            {
                smtpMessageDeliveryServiceConfiguration.ServerName = dictionary[key];
            }

            key = "Port";
            if (dictionary.ContainsKey(key))
            {
                smtpMessageDeliveryServiceConfiguration.Port = dictionary[key].ConvertTo<int>();
            }

            key = "FromAddress";
            if (dictionary.ContainsKey(key))
            {
                smtpMessageDeliveryServiceConfiguration.FromAddress = new MessageAddress(dictionary[key]);
            }

            key = "Ssl";
            if (dictionary.ContainsKey(key))
            {
                smtpMessageDeliveryServiceConfiguration.Ssl = dictionary[key].ToBool();
            }

            key = "Username";
            if (dictionary.ContainsKey(key))
            {
                smtpMessageDeliveryServiceConfiguration.UserName = dictionary[key];
            }

            key = "Password";
            if (dictionary.ContainsKey(key))
            {
                smtpMessageDeliveryServiceConfiguration.Password = dictionary[key];
            }



            return true;

        }

    }

}
