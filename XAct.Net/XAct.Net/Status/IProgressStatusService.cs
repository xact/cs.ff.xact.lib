﻿
namespace XAct.Net.Status
{
    /// <summary>
    /// Contract for keeping track of the <see cref="Progress"/> status of long running processes.
    /// </summary>
    public interface IProgressStatusService : IHasXActLibService
    {

        /// <summary>
        /// Gets the status
        /// or null if it does not exist.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ProgressStatus GetStatus(int id);

        /// <summary>
        /// Creates a new Status record.
        /// that can later be <see cref="Update" />d.
        /// </summary>
        /// <param name="progress">The progress.</param>
        /// <param name="value">The value.</param>
        /// <param name="title">The message.</param>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        int Create(Progress progress, int? value = null, string title = null, string text=null);


        /// <summary>
        /// Updates a status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="progress">The progress.</param>
        /// <param name="value">The value.</param>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        void Update(int id, Progress progress, int? value = null, string title = null, string text = null);

        /// <summary>
        /// Removes the specified <see cref="ProgressStatus"/>
        /// record on next Commit.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Remove(int id);
    }
}
