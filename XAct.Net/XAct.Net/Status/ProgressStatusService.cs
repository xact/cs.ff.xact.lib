﻿namespace XAct.Net.Status
{
    using System;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IProgressStatusService"/>
    /// </summary>
    public class ProgressStatusService :IProgressStatusService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Is the status service.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public ProgressStatusService(IDateTimeService dateTimeService, 
            IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService)
        {
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Gets the status
        /// or null if it does not exist.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public ProgressStatus GetStatus(int id)
        {
            ProgressStatus status = _repositoryService.GetSingle<ProgressStatus>(x => x.Id == id);
            return status;
        }

        /// <summary>
        /// Creates a new Status record.
        /// that can later be <see cref="Update" />d.
        /// </summary>
        /// <param name="progress">The progress.</param>
        /// <param name="value">The value.</param>
        /// <param name="tite">The message.</param>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public int Create(Progress progress, int? value = null, string tite = null, string text=null)
        {
            ProgressStatus status = new ProgressStatus();
            
            status.Progress = progress;

            status.ApplicationTennantId = _applicationTennantService.Get();

            if (value.HasValue)
            {
                status.Value = value.Value;
            }

            if (tite != null)
            {
                if (tite == string.Empty)
                {
                    tite = null;
                }
                status.Title = tite;
            }
            if (text != null)
            {
                if (text == string.Empty)
                {
                    text = null;
                }
                status.Text = text;
            }

            status.CreatedOnUtc = _dateTimeService.NowUTC;
            status.LastModifiedOnUtc = status.CreatedOnUtc;
            
            _repositoryService.PersistOnCommit<ProgressStatus>(status, x => true);

            return status.Id;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="progress">The progress.</param>
        /// <param name="value">The value.</param>
        /// <param name="title">The message.</param>
        /// <param name="text">The text.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Status.id not found.</exception>
        public void Update(int id, Progress progress, int? value = null, string title = null, string text = null)
        {



            ProgressStatus status = GetStatus(id);
            
            if (status == null)
            {
                throw new ArgumentOutOfRangeException("Status.id not found.");
            }


            status.Progress = progress;
            if (value.HasValue)
            {
                status.Value = value.Value;
            }
            
            if (title != null)
            {
                if (title == string.Empty)
                {
                    title = null;
                }
                status.Title = title;
            }
            if (text != null)
            {
                if (text == string.Empty)
                {
                    text = null;
                }
                status.Text = text;
            }
            status.LastModifiedOnUtc = _dateTimeService.NowUTC;

            UpdateStatus(status);
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="status">The status.</param>
        public void UpdateStatus(ProgressStatus status)
        {
            _repositoryService.PersistOnCommit<ProgressStatus>(status,x=>x.Id==0);
        }

        /// <summary>
        /// Removes the specified <see cref="ProgressStatus" />
        /// record on next Commit.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Remove(int id)
        {
            ProgressStatus status = GetStatus(id);
            if (status == null)
            {
                return;
            }
            _repositoryService.DeleteOnCommit(status);
        }

    }
}