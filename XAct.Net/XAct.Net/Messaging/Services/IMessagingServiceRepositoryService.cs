﻿namespace XAct.Net.Messaging
{
    using System;

    /// <summary>
    /// Contract for a Repository to 
    /// persist and retrieve
    /// Messaging objects.
    /// </summary>
    public interface IMessagingServiceRepositoryService : IHasXActLibService
    {

        /// <summary>
        /// Determines whether this repository is functional or not. 
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance can persist; otherwise, <c>false</c>.
        /// </returns>
        bool CanPersist();


        /// <summary>
        /// Gets the specified <see cref="Message"/>.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        Message GetMessage(Guid messageId);


        /// <summary>
        /// Gets Messages by priority.
        /// <para>
        /// Method used to thread dispatch messages that have been persisted.
        /// </para>
        /// <para>
        /// TODO: Add MultiTennancy(OrganisationName).
        /// </para>
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="pageIndex">0 based Index of the page.</param>
        /// <param name="maxRecordsToReturn">The max records to return.</param>
        /// <returns></returns>
        Message[] GetMessagesByPriority(MessagePriority priority, int pageIndex, int maxRecordsToReturn);

        /// <summary>
        /// Persists the Message on Commit.
        /// </summary>
        /// <param name="message">The message.</param>
        void PersistOnCommit(Message message);

        /// <summary>
        /// Updates the delivery status of the message, without retrieving the message.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="messageDeliveryStatus">The message delivery status.</param>
        /// <param name="source">The source.</param>
        /// <param name="note">The note.</param>
        void UpdateStatus(Guid messageId, MessageDeliveryStatus messageDeliveryStatus,string source=null, string note=null);

        /// <summary>
        /// Commits Changes.
        /// </summary>
        void Commit();
    }
}
