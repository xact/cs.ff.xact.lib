﻿namespace XAct.Net.Messaging
{
    /// <summary>
    ///   Contract for the notification service.
    /// </summary>
    public interface IMessagingService : IHasXActLibService
    {

        /// <summary>
        /// Gets or sets the configuration used by this messaging service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IMessagingServiceConfiguration Configuration { get; }

        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="protocolIdentifier">The protocol identifier (eg: 'SMTP').</param>
        /// <param name="replyTo">The reply to.</param>
        /// <param name="toAddress">To address.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The message.</param>
        void SendMessage(string protocolIdentifier, MessageAddress replyTo, MessageAddress toAddress, string subject, string body);


        /// <summary>
        /// Sends the <see cref="Message" />.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="protocolIdentifier">The protocol identifier (eg: 'SMTP').</param>
        /// <param name="forceDeliveryNow">if set to <c>true</c> [force delivery now].</param>
        void SendMessage(Message message, string protocolIdentifier = "SMTP", bool forceDeliveryNow = false);


    }
}