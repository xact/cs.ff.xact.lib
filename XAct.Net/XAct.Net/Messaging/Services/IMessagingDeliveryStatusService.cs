﻿namespace XAct.Net.Messaging
{
    using XAct.Services.Comm.ServiceModel;

    /// <summary>
    /// Contract for a service to track delivery status of a message.
    /// <para>
    /// Used to enquire against an MTA or other server how things are progressing.
    /// </para>
    /// </summary>
    public interface IMessagingDeliveryStatusService : IHasXActLibService, IHasPing
    {


                /// <summary>
        /// Message has been sent successfully.
        /// </summary>
        /// <param name="mailAddress"></param>
        void Sent(MessageAddress mailAddress);


        /// <summary>
        /// Message has been sent, but the receiving server has indicated mail 
        /// is being delivered too quickly and Mandrill should slow down sending temporarily
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        void DeliveryDeferred(MessageAddress mailAddress);

        /// <summary>
        /// Message has soft bounced.
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        void SoftBounced(MessageAddress mailAddress);

        /// <summary>
        /// Message has hard bounced.
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        void HardBounced(MessageAddress mailAddress);

        /// <summary>
        /// Recipient has opened the email.
        /// <para>
        /// Will only occur when open tracking is enabled
        /// </para>
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <param name="ip">The recipient's ip.</param>
        /// <param name="location">The recipient's location.</param>
        void Opened(MessageAddress mailAddress, string ip, string location);

        /// <summary>
        /// Recipient clicked a link in a message.
        /// <para>
        /// Will only occur when click tracking is enabled
        /// </para>
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <param name="urlClicked">The URL clicked.</param>
        /// <param name="ip">The recipient's ip.</param>
        /// <param name="location">The recipient's location.</param>
        void Clicked(MessageAddress mailAddress, string urlClicked, string ip, string location);

        /// <summary>
        /// Recipient marked message as Spam.
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <param name="messageType">Type of the message.</param>
        void MarkedAsSpam(MessageAddress mailAddress, string messageType);

        /// <summary>
        /// Recipient has requested to be unsubscribed.
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <param name="messageType">Type of the message.</param>
        void Unsubscribed(MessageAddress mailAddress, string messageType);

        /// <summary>
        /// Message was rejected (by ?)
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <param name="messageType"></param>
        void Rejected(MessageAddress mailAddress, string messageType);






    }
}
