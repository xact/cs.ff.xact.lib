﻿namespace XAct.Net.Messaging.Implementations
{
    using XAct.Services;

    /// <summary>
    /// An implementation of 
    /// <see cref="IMessagingServiceConfiguration"/>
    /// to configure how 
    /// an implementation of
    ///  <see cref="IMessagingService"/>
    /// would work
    /// </summary>
    public class MessagingServiceConfiguration : IMessagingServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether to spool messages for later delivery,
        /// or deliver them immediately (not recommended in a high volume website as it locks
        /// the response until the remote Mail Server accepts the message).
        /// </summary>
        /// <value>
        /// <c>true</c> if [spool messages for later delivery]; otherwise, <c>false</c>.
        /// </value>
        public bool SpoolMessagesForLaterDelivery { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingServiceConfiguration" /> class.
        /// </summary>
        public MessagingServiceConfiguration()
        {
            //TODO: Until we figure out the concurrency threading business 
            //of the repo, this has to remain false....damn.
            SpoolMessagesForLaterDelivery = false;
        }
    }
}