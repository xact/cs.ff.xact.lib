﻿

namespace XAct.Net.Messaging
{
    /// <summary>
    /// Contract for the configuration of
    /// an implementation of
    ///  <see cref="IMessagingService"/>
    /// </summary>
    public interface IMessagingServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether to spool messages for later delivery,
        /// or deliver them immediately (not recommended in a high volume website as it locks
        /// the response until the remote Mail Server accepts the message).
        /// <para>
        /// Default value is <c>True</c>
        /// </para>
        /// </summary>
        /// <value>
        /// <c>true</c> if [spool messages for later delivery]; otherwise, <c>false</c>.
        /// </value>
        bool SpoolMessagesForLaterDelivery { get; set; }
    }
}
