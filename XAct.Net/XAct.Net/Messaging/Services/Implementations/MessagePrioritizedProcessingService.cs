﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging.Implementations
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;
    using XAct.State;
    using XAct.Threading;


    /// <summary>
    /// An implementation of the <see cref="IMessagePrioritizedProcessingService" />
    /// to process spooled messages:
    /// </summary>
    public class MessagePrioritizedProcessingService : IMessagePrioritizedProcessingService
    {
        private readonly ITracingService _tracingService;
        private readonly IMessagingServiceRepositoryService _messagingServiceRepositoryService;
        private readonly IMessagingService _messagingService;

        private readonly PrioritizedSpoolProcessor<Message> _prioritizedSpoolProcessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagePrioritizedProcessingService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationStateService">The application state service.</param>
        /// <param name="messagingServiceRepositoryService">The messaging service repository service.</param>
        /// <param name="messagingService">The messaging service.</param>
        public MessagePrioritizedProcessingService(ITracingService tracingService,
            IApplicationStateService applicationStateService,
                                                   IMessagingServiceRepositoryService messagingServiceRepositoryService, IMessagingService messagingService)
        {
            _tracingService = tracingService;
            _messagingServiceRepositoryService = messagingServiceRepositoryService;
            _messagingService = messagingService;


            Dictionary<string,object> xactState = 
               applicationStateService.GetXActLibStateDictionary();

            object tmp;
            if (xactState.TryGetValue("PrioritizedSpoolProcessor", out tmp))
            {
                _prioritizedSpoolProcessor = tmp as PrioritizedSpoolProcessor<Message>;
            }
            if (_prioritizedSpoolProcessor == null)
            {
                xactState["PrioritizedSpoolProcessor"] =
                    _prioritizedSpoolProcessor = new PrioritizedSpoolProcessor<Message>(tracingService,
                                                                                        RetrievePrioritizedMessages,
                                                                                        ProcessMessages,
                                                                                        null,
                                                                                        RegularCleanup,
                                                                                        1,
                                                                                        1,
                                                                                        1000,
                                                                                        5,
                                                                                        50);
            }
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="Start"/>
        /// has been invoked;
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is started; otherwise, <c>false</c>.
        /// </value>
        public bool IsPolling { get; private set; }

        /// <summary>
        /// Gets the number of times polled since <see cref="Start" /> was invoked.
        /// <para>
        /// <see cref="Stop" /> resets the counter.
        /// </para>
        /// </summary>
        /// <value>
        /// The polled.
        /// </value>
        public long Polled { get; private set; }




        /// <summary>
        /// Initializes this instance.
        /// <para>
        /// Invoked by <see cref="IMessageDeliveryService"/>
        /// to ensure the service is available for 
        /// handling messages later.
        /// </para>
        /// </summary>
        public bool IsAvailable()
        {
            return _messagingServiceRepositoryService.CanPersist();
        }

        /// <summary>
        /// Starts processing Messages.
        /// <para>
        /// Intended to be invoked from the Application's bootstrapper.
        /// </para>
        /// <para>
        /// Can be called several times without adverse effects.
        /// </para>
        /// </summary>
        public void Start()
        {
            
            _prioritizedSpoolProcessor.Start();

            IsPolling = true;
        }

        /// <summary>
        /// Stop processing Messages.
        /// <para>
        /// Call when the application is about to be shut down.
        /// </para>
        /// </summary>
        public void Stop()
        {
            _prioritizedSpoolProcessor.Stop();
            IsPolling = false;
        }


        //Delegate to retrieve messsages from the repository:
        Message[] RetrievePrioritizedMessages(int maxRecordsToRetrieve, object priority)
        {
            //Have to convert the general Priority to MailPriority:
            MessagePriority messagePriority;

            switch ((Priority) priority)
            {
                case Priority.High:
                case Priority.Urgent:
                case Priority.Critical:
                    messagePriority = MessagePriority.Urgent; //Value is 1
                    break;
                case Priority.Normal:
                    messagePriority = MessagePriority.Normal;
                    break;
                case Priority.Low:
                case Priority.VeryLow:
                default:
                    messagePriority = MessagePriority.VeryLow; //Value is now 4
                    break;

            }

            Message[] results = _messagingServiceRepositoryService.GetMessagesByPriority(messagePriority, 0, maxRecordsToRetrieve);

            Polled += 1;

            //Have messages to process.
            return results;
        }

        /// <summary>
        /// Delegate to process message by worker thread (when it gets around to it).
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool ProcessMessages(Message message)
        {

            _messagingService.SendMessage(message, message.DeliveryProtocols, true);


#pragma warning disable 168
            using (UnitOfWorkThreadScope serviceLocatorThreadScope = new UnitOfWorkThreadScope())
#pragma warning restore 168
            {
                _messagingServiceRepositoryService.UpdateStatus(message.MessageId, MessageDeliveryStatus.Sent);
                _messagingServiceRepositoryService.Commit();

                return true;
            }
        }

        void RegularCleanup()
        {
            //_messagingServiceRepositoryService.ArchiveMessages(){}
        }


    }
}