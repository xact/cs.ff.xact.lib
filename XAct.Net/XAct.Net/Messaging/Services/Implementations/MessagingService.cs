﻿namespace XAct.Net.Messaging.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    ///   Implementation of the notification service.
    /// </summary>
    public class MessagingService : IMessagingService
    {
        private readonly ITracingService _tracingService;
        private readonly IMessagingServiceRepositoryService _messagingServiceRepositoryService;
        private readonly IMessagingServiceConfiguration _messagingServiceConfiguration;


        /// <summary>
        /// Gets or sets the configuration used by this messaging service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IMessagingServiceConfiguration Configuration { get { return _messagingServiceConfiguration; }}


        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingService" /> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <param name="messagingServiceRepositoryService">The messaging service repository service.</param>
        /// <param name="messagingServiceConfiguration">The messaging service configuration.</param>
        public MessagingService(ITracingService tracingService, IMessagingServiceRepositoryService messagingServiceRepositoryService,IMessagingServiceConfiguration messagingServiceConfiguration)
        {
            _tracingService = tracingService;
            _messagingServiceRepositoryService = messagingServiceRepositoryService;
            _messagingServiceConfiguration = messagingServiceConfiguration;
        }

        private bool Initialize()
        {

            //Ensure it's wired up -- even if it's not started:
            return XAct.DependencyResolver.Current.GetInstance<IMessagePrioritizedProcessingService>().IsAvailable() ;


        }


        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="protocolIdentifier">The protocol identifier (eg: 'SMTP').</param>
        /// <param name="replyTo">The reply to.</param>
        /// <param name="toAddress">To address.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The message.</param>
        public void SendMessage(string protocolIdentifier, MessageAddress replyTo, MessageAddress toAddress,
                                string subject, string body)
        {

            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Sends the <see cref="Message" />.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="protocolIdentifier">The protocol identifier (eg: 'SMTP').</param>
        /// <param name="forceDeliveryNow">if set to <c>true</c> [force delivery now].</param>
        public void SendMessage(Message message, string protocolIdentifier = "SMTP", bool forceDeliveryNow = false)
        {
            //As we are persisting, we need to embed in the message the protocol information:
            if (message.DeliveryProtocols.IsNullOrEmpty())
            {
                message.DeliveryProtocols = protocolIdentifier ?? (protocolIdentifier = "SMTP");
            }


            if (!forceDeliveryNow && _messagingServiceConfiguration.SpoolMessagesForLaterDelivery)
            {
                bool persistenceAvailable = Initialize();

                //Ensure it's wired up -- even if it's not started:
                if (persistenceAvailable)
                {
                  //Save it to the database
                    //for later publishing, assuming someone has remembered to invoke
                    //Start in the bootstrapper.

                    //Must mark as logged first:

                    //That's all we've done so far:

                    message.ChangeStatus(MessageDeliveryStatus.Logged);
                    

                    _messagingServiceRepositoryService.PersistOnCommit(message);

                    return;
                }
            }

            TransmitMessage(message);
        }


        //Message that the Unspooler should call.
        internal static void TransmitMessage(Message message)
        {
            IMessageDeliveryService messageDeliveryService;

            try
            {
                messageDeliveryService =
                    XAct.DependencyResolver.Current.GetInstance<IMessageDeliveryService>(
                        message.DeliveryProtocols,
                        true);
            }
            catch
            {
                throw new ArgumentOutOfRangeException("DeliveryProtocols",
                                                      "Delivery protocolIdentifier ('{0}') not recognized by ServiceLocator."
                                                          .FormatStringInvariantCulture(message.DeliveryProtocols));
            }

            messageDeliveryService.SendMessage(message);
        }
    }
}