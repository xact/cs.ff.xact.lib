﻿namespace XAct.Net.Messaging.Implementations
{
    using System;
    using XAct.Domain.Repositories;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IMessagingDeliveryStatusService"/>
    /// to track delivery status of a <see cref="Message"/>
    /// </summary>
    public class MessagingDeliveryStatusService  : IMessagingDeliveryStatusService
    {

        private readonly IRepositoryService _repositoryService;



        /// <summary>
        /// Pings this instance.
        /// <para>
        /// By Convention, returns DateTime.UtCNow.ToString();
        /// </para>
        /// </summary>
        /// <returns></returns>
        public string Ping()
        {
            return string.Format("Ping: UTC='{0}'.", DateTime.UtcNow);
        }


        
        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingDeliveryStatusService"/> class.
        /// </summary>
        /// <param name="repositoryService">The repository service.</param>
        public MessagingDeliveryStatusService(IRepositoryService repositoryService)
        {
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Message has been sent successfully.
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Sent(MessageAddress mailAddress)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Message has been sent, but the receiving server has indicated mail
        /// is being delivered too quickly and Mandrill should slow down sending temporarily
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void DeliveryDeferred(MessageAddress mailAddress)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Message has soft bounced.
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void SoftBounced(MessageAddress mailAddress)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Message has hard bounced.
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void HardBounced(MessageAddress mailAddress)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Recipient has opened the email.
        /// <para>
        /// Will only occur when open tracking is enabled
        /// </para>
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <param name="ip">The recipient's ip.</param>
        /// <param name="location">The recipient's location.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Opened(MessageAddress mailAddress, string ip, string location)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Recipient clicked a link in a message.
        /// <para>
        /// Will only occur when click tracking is enabled
        /// </para>
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <param name="urlClicked">The URL clicked.</param>
        /// <param name="ip">The recipient's ip.</param>
        /// <param name="location">The recipient's location.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Clicked(MessageAddress mailAddress, string urlClicked, string ip, string location)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Recipient marked message as Spam.
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <param name="messageType">Type of the message.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void MarkedAsSpam(MessageAddress mailAddress, string messageType)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Recipient has requested to be unsubscribed.
        /// </summary>
        /// <param name="mailAddress">The email address.</param>
        /// <param name="messageType">Type of the message.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Unsubscribed(MessageAddress mailAddress, string messageType)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Message was rejected (by ?)
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <param name="messageType"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Rejected(MessageAddress mailAddress, string messageType)
        {
            throw new System.NotImplementedException();
        }
    }
}
