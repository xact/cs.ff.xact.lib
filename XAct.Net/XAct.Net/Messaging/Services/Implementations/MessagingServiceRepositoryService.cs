﻿namespace XAct.Net.Messaging.Implementations
{
    using System;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// Implementation of 
    /// <see cref="IMessagingServiceRepositoryService"/>
    /// persist and retrieve
    /// Messaging objects.
    /// </summary>
    [DefaultBindingImplementation(typeof(IMessagingServiceRepositoryService), BindingLifetimeType.Undefined, Priority.Normal /*OK:Higher than Null*/)]
    public class MessagingServiceRepositoryService : IMessagingServiceRepositoryService
    {
        private readonly ITracingService _tracingService;
        private readonly IRepositoryService _repositoryService;
        private readonly IDistributedIdService _distributedIdService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingServiceRepositoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="distributedIdService">The distributed id service.</param>
        public MessagingServiceRepositoryService(ITracingService tracingService, IRepositoryService repositoryService, IDistributedIdService distributedIdService)
        {
            _tracingService = tracingService;
            _repositoryService = repositoryService;
            _distributedIdService = distributedIdService;
        }

        /// <summary>
        /// Determines whether this repository is functional or not.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance can persist; otherwise, <c>false</c>.
        /// </returns>
        public bool CanPersist()
        {
            return true;
        }

        /// <summary>
        /// Gets the specified <see cref="Messaging.Message" />.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        public Messaging.Message GetMessage(Guid messageId)
        {
            return _repositoryService.GetSingle<Messaging.Message>(
                m => m.MessageId == messageId,
                new IncludeSpecification<Messaging.Message>(m => m.Statuses, m => m.Headers, m => m.Addresses, m => m.Bodies, m => m.Attachments));
        }


        /// <summary>
        /// Gets Messages by priority.
        /// <para>
        /// Method used to thread dispatch messages that have been persisted.
        /// </para>
        /// <para>
        /// TODO: Add MultiTennancy(OrganisationName).
        /// </para>
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="pageIndex">0 based Index of the page.</param>
        /// <param name="maxRecordsToReturn">The max records to return.</param>
        /// <returns></returns>
        public Messaging.Message[] GetMessagesByPriority(MessagePriority priority, int pageIndex, int maxRecordsToReturn)
        {
            //Remember that mail priority goes from Low(3) to High (1)

            Messaging.Message[] results = _repositoryService.GetByFilter<Messaging.Message>(
                m => ((!m.IsDraft) && (m.Status == MessageDeliveryStatus.Logged) && (m.Priority <= priority)), //As Low =3, we want all 3's and above (Normal=2, High=1) but sorted by 3's first.
                new IncludeSpecification<Messaging.Message>(m => m.Statuses, m => m.Headers, m => m.Addresses, m => m.Bodies,
                                                  m => m.Attachments),
                new PagedQuerySpecification(pageIndex, maxRecordsToReturn, false),
                m => m.OrderByDescending(x => x.Priority)) //Order Priorities from 3 (Low) down to (Higher).
                .ToArray();

            return results;
        }

        /// <summary>
        /// Persists the Message on Commit.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void PersistOnCommit(Messaging.Message message)
        {
            //I think this is the only place it needs to be set -- 
            //until it is actually sent, nobody really cares what the guid is.
            //Ensure the Message has a proper Id:
            message.SetIds(_distributedIdService.NewGuid());

            _repositoryService.PersistOnCommit<Messaging.Message>(message,m=>m.MessageId == Guid.Empty);
        }

        /// <summary>
        /// Updates the delivery status of the message, without retrieving the message.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="messageDeliveryStatus">The message delivery status.</param>
        /// <param name="source">The source.</param>
        /// <param name="note">The note.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void UpdateStatus(Guid messageId, MessageDeliveryStatus messageDeliveryStatus,string source= null, string note=null)
        {
            MessageStatus messageStatus = new MessageStatus();
            
            messageStatus.Id = _distributedIdService.NewGuid();
            messageStatus.MessageId = messageId;
            messageStatus.Status = messageDeliveryStatus;
            messageStatus.Source = source;
            messageStatus.Note = note;

           _repositoryService.PersistOnCommit(messageStatus,m=>(true));

        }
        /// <summary>
        /// Commits Changes.
        /// </summary>
        public void Commit()
        {
            _repositoryService.GetContext().Commit(CommitType.Default);
        }

    }
}