﻿namespace XAct.Net.Messaging.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IMessagingServiceRepositoryService" />
    /// contract, that does nothing.
    /// </summary>
    [DefaultBindingImplementation(typeof(IMessagingServiceRepositoryService), BindingLifetimeType.Undefined, Priority.Low /*OK:SecondaryBinding*/)]
    public class NullMessagingServiceRepositoryService : IMessagingServiceRepositoryService
    {
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NullMessagingServiceRepositoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public NullMessagingServiceRepositoryService(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }


        /// <summary>
        /// Determines whether this repository is functional or not.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance can persist; otherwise, <c>false</c>.
        /// </returns>
        public bool CanPersist()
        {
            return false;
        }

        /// <summary>
        /// Gets the specified <see cref="Message"/>.
        /// </summary>
        /// <param name="id">The id.</param>
        public Message GetMessage(Guid id)
        {
            _tracingService.Trace(TraceLevel.Warning, "");
            _tracingService.Trace(TraceLevel.Warning, "********************************************************************************");
            _tracingService.Trace(TraceLevel.Warning, "IMessagingService is configured to persist messages -- but only NullMessagingServiceRepositoryService was found. No persistance possible.");
            _tracingService.Trace(TraceLevel.Warning, "********************************************************************************");
            return null;
        }

        /// <summary>
        /// Gets Messages by priority.
        /// <para>
        /// Method used to thread dispatch messages that have been persisted.
        /// </para>
        /// <para>
        /// TODO: Add MultiTennancy(OrganisationName).
        /// </para>
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="pageIndex">0 based Index of the page.</param>
        /// <param name="maxRecordsToReturn">The max records to return.</param>
        /// <returns></returns>
        public Message[] GetMessagesByPriority(MessagePriority priority, int pageIndex, int maxRecordsToReturn)
        {
            _tracingService.Trace(TraceLevel.Warning, "");
            _tracingService.Trace(TraceLevel.Warning, "********************************************************************************");
            _tracingService.Trace(TraceLevel.Warning, "IMessagingService is configured to persist messages -- but only NullMessagingServiceRepositoryService was found. No persistance possible.");
            _tracingService.Trace(TraceLevel.Warning, "********************************************************************************");

            return null;
        }


        /// <summary>
        /// Persists the Message on Commit.
        /// </summary>
        /// <param name="message">The message.</param>
        public void PersistOnCommit(Message message)
        {
            _tracingService.Trace(TraceLevel.Warning, "");
            _tracingService.Trace(TraceLevel.Warning, "********************************************************************************");
            _tracingService.Trace(TraceLevel.Warning, "IMessagingService is configured to persist messages -- but only NullMessagingServiceRepositoryService was found. No persistance possible.");
            _tracingService.Trace(TraceLevel.Warning, "********************************************************************************");
        }

        /// <summary>
        /// Updates the delivery status of the message, without retrieving the message.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="messageDeliveryStatus">The message delivery status.</param>
        /// <param name="source">The source.</param>
        /// <param name="note">The note.</param>
        public void UpdateStatus(Guid messageId, MessageDeliveryStatus messageDeliveryStatus,string source=null,string note=null)
        {
            _tracingService.Trace(TraceLevel.Warning, "");
            _tracingService.Trace(TraceLevel.Warning, "********************************************************************************");
            _tracingService.Trace(TraceLevel.Warning, "IMessagingService is configured to persist messages -- but only NullMessagingServiceRepositoryService was found. No persistance possible.");
            _tracingService.Trace(TraceLevel.Warning, "********************************************************************************");
        }


        /// <summary>
        /// Commits Changes.
        /// </summary>
        public void Commit()
        {
        }

    }
}