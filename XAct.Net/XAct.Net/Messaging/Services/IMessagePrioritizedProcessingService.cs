﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for a service to process persisted messages.
    /// </summary>
    public interface IMessagePrioritizedProcessingService : IHasXActLibService
    {


        /// <summary>
        /// Gets a value indicating whether <see cref="Start"/>
        /// has been invoked;
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is started; otherwise, <c>false</c>.
        /// </value>
        bool IsPolling { get; }

        /// <summary>
        /// Gets the number of times polled since <see cref="Start"/> was invoked.
        /// </summary>
        /// <value>
        /// The polled.
        /// </value>
        long Polled { get; }

        /// <summary>
        /// Initializes this instance.
        /// <para>
        /// Invoked by <see cref="IMessageDeliveryService"/>
        /// to ensure the service is available for 
        /// handling messages later.
        /// </para>
        /// </summary>
        bool IsAvailable();

        /// <summary>
        /// Starts processing Messages.
        /// <para>
        /// Intended to be invoked from the Application's bootstrapper.
        /// </para>
        /// <para>
        /// Can be called several times without adverse effects.
        /// </para>
        /// </summary>
        void Start();

        /// <summary>
        /// Stop processing Messages.
        /// <para>
        /// Call when the application is about to be shut down.
        /// </para>
        /// </summary>
        void Stop();
    }
}