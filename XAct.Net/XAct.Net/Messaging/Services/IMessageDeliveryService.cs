﻿namespace XAct.Net.Messaging
{
    /// <summary>
    /// A service invoked by <see cref="IMessagingService"/>
    /// <para>
    /// Do not use directly -- go through <see cref="IMessagingService"/>
    /// for the most future proofing.
    /// </para>
    /// </summary>
    public interface IMessageDeliveryService : IHasXActLibService
    {
        /// <summary>
        /// Sends the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        void SendMessage(Message message);
    }
}
