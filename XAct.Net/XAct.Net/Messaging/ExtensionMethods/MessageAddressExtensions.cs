// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Net.Messaging;

    /// <summary>
    /// Extensions to the <see cref="MessageAddress"/>
    /// object.
    /// </summary>
    public static class MessageAddressExtensions
    {
        /// <summary>
        /// Initializes this 
        /// <see cref="MessageAddress"/>
        /// based entity.
        /// </summary>
        /// <param name="messageAddress">The message address.</param>
        /// <param name="addressType">Type of the address.</param>
        /// <param name="address">The address.</param>
        public static void Set(this MessageAddress messageAddress, MessageAddressType addressType, string address)
        {
            if (addressType == MessageAddressType.Undefined)
            {
                throw new ArgumentException("addressType");
            }
            if (string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException("address");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            messageAddress.Type = addressType;
            messageAddress.Value = address;
        }

        /// <summary>
        /// Initializes this 
        /// <see cref="MessageAddress"/>
        /// based entity.
        /// </summary>
        /// <param name="messageAddress">The message address.</param>
        /// <param name="addressType">Type of the address.</param>
        /// <param name="address">The address.</param>
        /// <param name="name">The name.</param>
        public static void Set(this MessageAddress messageAddress, MessageAddressType addressType, string address,
                               string name)
        {
            if (addressType == MessageAddressType.Undefined)
            {
                throw new ArgumentException("addressType");
            }
            if (string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException("address");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            messageAddress.Type = addressType;
            messageAddress.Value = address;
            messageAddress.Name = name;
        }


        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <param name="thisAddress">The this address.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(this MessageAddress thisAddress)
        {
            return thisAddress.ToString(false);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <param name="thisAddress">The this address.</param>
        /// <param name="quoteNames">if set to <c>true</c> [quote names].</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(this MessageAddress thisAddress, bool quoteNames)
        {
            if (string.IsNullOrEmpty(thisAddress.Name))
            {
                return thisAddress.Value;
            }

            
            if (thisAddress.Name.IndexOfAny(",.;:'\"|(){}[]\\/<>?@#%^&*-_+=!`~ ".ToCharArray())>-1)
            {
                quoteNames = true;
            }

            quoteNames = true;

            string format = quoteNames ? "\"{0}\" <{1}>" : "{0} <{1}>";
            return format.FormatStringInvariantCulture(thisAddress.Name, thisAddress.Value);
        }
  
    }
}