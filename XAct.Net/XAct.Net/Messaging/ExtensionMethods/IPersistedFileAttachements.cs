namespace XAct
{
    using System;
    using System.IO;
    using System.Text;
    using XAct.Diagnostics;
    using XAct.IO;
    using XAct.Net;

    /// <summary>
    /// Extensions to objects that implement 
    /// the <see cref="IPersistedFile"/>
    /// contract (such as <c>PersistedFile</c>).
    /// </summary>
    public static class IPersistedFileAttachements
    {
        static IPathService PathService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IPathService>(); }
        }

        private static ITracingService TracingService
        {
            get { return DependencyResolver.Current.GetInstance<ITracingService>(); }
        }

        private static IMimeTypeService MimeTypeService
        {
            get
            {
                return DependencyResolver.Current.GetInstance<IMimeTypeService>();
            }
        }

        /// <summary>
        /// Initializes the values of this 
        /// <see cref="Net.Messaging.PersistedFile"/>
        /// </summary>
        /// <param name="persistedFile">The message attachment.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        // ReSharper disable RedundantNameQualifier
        public static void Set(this IPersistedFile persistedFile, string name, byte[] value)
            // ReSharper restore RedundantNameQualifier
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            persistedFile.Name = name;
            persistedFile.Value = value;

        }


        //public static void MapFrom(this PersistedFile thisPersistedFile, ClientPersistedFile clientPersistedFile)
        //{
        //    thisPersistedFile.Type = (PersistedFileType) clientPersistedFile.Type;
        //    thisPersistedFile.Name = clientPersistedFile.Name;
        //    thisPersistedFile.ContentType = clientPersistedFile.ContentType;
        //    thisPersistedFile.DateCreated = clientPersistedFile.DateCreated;
        //    thisPersistedFile.DateModified = clientPersistedFile.DateModified;
        //    thisPersistedFile.Value = clientPersistedFile.Value;
        //}


        /// <summary>
        /// Loads the attachment from a file.
        /// </summary>
        /// <param name="persistedFile">The message attachment.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="ioService">An implemention of the <see cref="IIOService"/>.</param>
        /// <exception cref="System.ArgumentNullException">fileName</exception>
        /// <exception cref="System.ArgumentException">fileName does not have an extension.</exception>
        /// <exception cref="System.IO.FileNotFoundException">ClientPersistedFile FileName '{0}' not found.</exception>
        public static void LoadFromFile(this IPersistedFile persistedFile, string fileName, IIOService ioService)
        {
            if (fileName.IsNullOrEmpty())
            {
                throw new ArgumentNullException("fileName");
            }

            if (!PathService.HasExtension(fileName))
            {
                throw new ArgumentException("fileName does not have an extension.");
            }


            if (!ioService.FileExistsAsync(fileName).WaitAndGetResult())
            {
                throw new FileNotFoundException("ClientPersistedFile FileName '{0}' not found.");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            persistedFile.Name = PathService.GetFileName(fileName);
            persistedFile.ContentType = GetMimeTypeFromFileExtension(PathService.GetExtension(fileName));

            FileInfo info = new FileInfo(fileName);
            persistedFile.CreatedOnUtc = new DateTime?(info.CreationTimeUtc);
            //persistedFile. = new DateTime?(info.LastAccessTime);
            persistedFile.LastModifiedOnUtc = new DateTime?(info.LastWriteTimeUtc);
            persistedFile.Value = ioService.FileOpenReadAsync(fileName).WaitAndGetResult().ReadAllBytes();
            //persistedFile.IsStream = false;
        }

        /// <summary>
        /// Loads the attachment from a stream.
        /// </summary>
        /// <param name="persistedFile">The message attachment.</param>
        /// <param name="stream">The stream.</param>
        public static void LoadFromStream(this IPersistedFile persistedFile, Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            if (stream.Position != 0L)
            {
                throw new ArgumentException("stream Position is not 0.");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            byte[] buffer = new byte[stream.Length];

            stream.Read(buffer, 0, (int)stream.Length);

            persistedFile.Value = buffer;
            persistedFile.CreatedOnUtc = null;
            persistedFile.LastModifiedOnUtc = null;

            persistedFile.ContentType = GetMimeTypeFromBuffer(ref buffer);
            //persistedFile.IsStream = true;
        }




        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the <see cref="IPersistedFile"/>.
        /// </summary>
        /// <param name="persistedFile">The message attachment.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(this IPersistedFile persistedFile)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(typeof(IPersistedFile).ToString());
            builder.Append("{");
            builder.Append("ContentType:{0}, Name:{1}, Value.Length:{2}".FormatStringInvariantCulture(
                new object[]
                    {
                        persistedFile.ContentType, 
                        persistedFile.Name,
                        persistedFile.Value.Length
                    }));
            builder.Append("DateCreatedUtc:{0}, ".FormatStringInvariantCulture(new object[] { persistedFile.CreatedOnUtc }));
            builder.Append("DateModifiedUtc:{0}".FormatStringInvariantCulture(new object[] { persistedFile.LastModifiedOnUtc }));
            builder.Append("}");
            return builder.ToString();
        }

        /// <summary>
        /// Gets the MIME type from the filename's extension (with prefix dot).
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        private static string GetMimeTypeFromFileExtension(string fileName)
        {

            string fileNameExtension = PathService.GetExtension(fileName);
            return MimeTypeService.GetMimeTypeFromFileExtension(fileNameExtension);
        }

        // ReSharper disable UnusedParameter.Local
        private static string GetMimeTypeFromBuffer(ref byte[] byteBuffer)
        // ReSharper restore UnusedParameter.Local
        {
            TracingService.Trace(TraceLevel.Warning, "Havn't finished 'GetMimeTypeFromBuffer'");
            return "application/unknown";
        }


    }
}