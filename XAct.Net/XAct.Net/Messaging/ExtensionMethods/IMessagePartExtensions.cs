﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Net.Messaging;

    /// <summary>
    /// Extension methods to 
    /// elements that implement
    /// <see cref="IMessagePart"/>
    /// See 
    /// <see cref="MessageHeader"/>, 
    /// <see cref="MessageAddress"/>, 
    /// <see cref="MessageBody"/>, 
    /// and <see cref="MessageAttachment"/>.
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IMessagePartExtensions
// ReSharper restore InconsistentNaming 
    {
        /// <summary>
        /// Sets the DatastoreId and parent Message datastoreId.
        /// </summary>
        /// <param name="messagePart">The message part.</param>
        /// <param name="message">The message.</param>
        /// <param name="partId">The part id.</param>
        /// <exception cref="System.ArgumentException">id</exception>
        public static void SetId(this IMessagePart messagePart, Message message, Guid partId)
        {
            if (message == null)
            {
                throw new ArgumentException("id");
            }
            if (partId == Guid.Empty)
            {
                throw new ArgumentException("partId");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            messagePart.SetId(message.MessageId, partId);
        }


        /// <summary>
        /// Sets the DatastoreId and parent Message datastoreId.
        /// </summary>
        /// <param name="messagePart">The message part.</param>
        /// <param name="messageId">The message id.</param>
        /// <param name="partId">The part id.</param>
        /// <exception cref="System.ArgumentException">messageId</exception>
        public static void SetId(this IMessagePart messagePart, Guid messageId, Guid partId)
        {
            if (messageId == Guid.Empty)
            {
                throw new ArgumentException("messageId");
            }
            if (partId == Guid.Empty)
            {
                throw new ArgumentException("partId");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            //Give it its own unique id:
            Guid existingPartId = ((IHasDistributedGuidId) messagePart).Id;

            if ((existingPartId != Guid.Empty) && (existingPartId != partId))
            {
                throw new ArgumentException(
                    "Given part has Id that is already set, but not for this message. Did you intend to clone the MessagePart first?");
            }

            //Avoid triggering Changing Tracking when avoidable:
            if (existingPartId != messageId)
            {
                ((IHasDistributedGuidId)messagePart).Id = partId;
            }



            //Put on the FK value to its parent Message:

// ReSharper disable InconsistentNaming
            Guid existingFK = messagePart.MessageId;
// ReSharper restore InconsistentNaming

            if ((existingFK != Guid.Empty) && (existingFK != messageId))
            {
                throw new ArgumentException(
                    "Given part has MessageId that is already set, but not for this message. Did you intend to clone the MessagePart first?");
            }
            //Avoid triggering Changing Tracking when avoidable:
            if (existingFK != messageId)
            {
                messagePart.MessageId = messageId;
            }
        }
    }
}