﻿//using System;
//using XAct.Net.Messaging;

//// ReSharper disable CheckNamespace
//namespace XAct
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// Extension methods to entities
//    /// that implement 
//    /// <see cref="IMessageIdentified"/>,
//    /// which are
//    /// <see cref="Message"/>
//    /// and any element
//    /// that implements
//    /// <see cref="IMessagePart"/>
//    /// </summary>
//// ReSharper disable InconsistentNaming
//    public static class MessageDataStoreIdentifierExtensions
//// ReSharper restore InconsistentNaming
//    {
//        /// <summary>
//        /// Sets the datastore Id of the entity.
//        /// </summary>
//        /// <param name="messageDataStoreIdentifier">The message data store identifier.</param>
//        /// <param name="id">The id.</param>
//        public static void SetId(this IMessageIdentified messageDataStoreIdentifier, Guid id)
//        {
//            messageDataStoreIdentifier.MessageId = id;
//        }
//    }
//}