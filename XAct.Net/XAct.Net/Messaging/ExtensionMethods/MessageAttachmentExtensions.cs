// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.Text;
    using XAct.Net.Messaging;


    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="Net.Messaging.MessageAttachment"/>
    /// </summary>
    // ReSharper disable InconsistentNaming
    public static class MessageAttachmentExtensions
        // ReSharper restore InconsistentNaming
    {

    
        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the <see cref="MessageAttachment"/>.
        /// </summary>
        /// <param name="messageAttachment">The message attachment.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(this MessageAttachment messageAttachment)
        {
            StringBuilder builder = new StringBuilder();
// ReSharper disable RedundantToStringCall
            builder.Append(messageAttachment.ToString());
// ReSharper restore RedundantToStringCall
            builder.Append(" {");
            builder.Append("Type:{0}, ContentType:{1}, Name:{2}, Value.Length:{3}".FormatStringInvariantCulture(
                new object[]
                    {
                        messageAttachment.Type, messageAttachment.ContentType, messageAttachment.Name,
                        messageAttachment.Value.Length
                    }));
            builder.Append("DateCreated:{0}, ".FormatStringInvariantCulture(new object[] {messageAttachment.CreatedOnUtc}));
            builder.Append("DateModified:{0}".FormatStringInvariantCulture(new object[] {messageAttachment.LastModifiedOnUtc}));
            builder.Append("}");
            return builder.ToString();
        }

     

        
    }
}