// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Net.Messaging;

    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="MessageHeader"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class MessageHeaderExtensions
// ReSharper restore InconsistentNaming
    {
        //public static void MapFrom(this MessageHeader thisMessageHeader, ClientMessageHeader clientMessageHeader)
        //{
        //    ArgumentValidation.IsInitialized(clientMessageHeader, "clientMessageHeader");

        //    thisMessageHeader.Name = clientMessageHeader.Name;
        //    thisMessageHeader.Value = clientMessageHeader.Value;
        //}

        /// <summary>
        /// Initializes the values of this
        /// <see cref="MessageHeader"/>
        /// </summary>
        /// <param name="thisMessageHeader">The this message header.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void Set(this MessageHeader thisMessageHeader, string name, string value)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            ((IMessageNamedValue<string>) thisMessageHeader).Set(name, value);
        }


        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <param name="messageHeader">The message header.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(this MessageHeader messageHeader)
        {
            return "{0}={1};".FormatStringInvariantCulture(messageHeader.Name, messageHeader.Name);
        }


    }
}