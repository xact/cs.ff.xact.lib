﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Net.Messaging;

    /// <summary>
    /// Extensions to the <see cref="Message"/>
    ///  object
    /// </summary>
    public static class MessageExtensions
    {

        /// <summary>
        /// Sets the ids of the message, as well as it's parts.
        /// <para>
        /// Generate the <paramref name="messageId"/>
        /// using <see cref="XAct.IDistributedIdService"/>
        /// </para>
        /// <para>
        /// Invoked by the repositories.
        /// </para>
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageId">The message id.</param>
        public static void SetIds(this Message message, Guid messageId)
        {
            Guid existingMessageId = message.MessageId;

            if (existingMessageId == Guid.Empty)
            {
                message.MessageId = messageId;
            }
            //use old or new one for next parts:

            foreach (IMessagePart messagePart in message.Statuses)
            {
                messagePart.SetId(message.MessageId, Guid.NewGuid());
            }
            foreach (IMessagePart messagePart in message.Headers)
            {
                messagePart.SetId(message.MessageId, Guid.NewGuid());
            }
            foreach (IMessagePart messagePart in message.Addresses)
            {
                messagePart.SetId(message.MessageId, Guid.NewGuid());
            }
            foreach (IMessagePart messagePart in message.Bodies)
            {
                messagePart.SetId(message.MessageId, Guid.NewGuid());
            }
            foreach (IMessagePart messagePart in message.Attachments)
            {
                messagePart.SetId(message.MessageId, Guid.NewGuid());
            }
        }





        /// <summary>
        /// Add a Status to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageStatus">The message header.</param>
        public static void AddStatus(this Message message, MessageStatus messageStatus)
        {
            message.Statuses.Add(messageStatus);

            //secondly, it has to be of right MessageId:
            if ((messageStatus.MessageId != Guid.Empty) && (messageStatus.MessageId != message.MessageId))
            {
                throw new ArgumentException("MessageStatus has a Guid -- but it is not the same as the Message it is being added to.");
            }

        }


        /// <summary>
        /// Add a Header to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageHeader">The message header.</param>
        public static void AddHeader(this Message message, MessageHeader messageHeader)
        {
            message.Headers.Add(messageHeader);


            //secondly, it has to be of right MessageId:
            if ((messageHeader.MessageId != Guid.Empty) && (messageHeader.MessageId != message.MessageId))
            {
                throw new ArgumentException("MessageHeader has a Guid -- but it is not the same as the Message it is being added to.");
            }

        }





        /// <summary>
        /// Add a primary recipient to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAddress">The message address.</param>
        public static void AddTo(this Message message, MessageAddress messageAddress)
        {
            message.AddAddress(MessageAddressType.To, messageAddress);
        }


        /// <summary>
        /// Add a CC recipient to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAddress">The message address.</param>
        public static void AddCc(this Message message, MessageAddress messageAddress)
        {
            message.AddAddress(MessageAddressType.Cc, messageAddress);
        }

        /// <summary>
        /// Add a BCC recipient to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAddress">The message address.</param>
        public static void AddBcc(this Message message, MessageAddress messageAddress)
        {
            message.AddAddress(MessageAddressType.Bcc, messageAddress);
        }

        /// <summary>
        /// Add a recipient of the specified type to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAddressType">Type of the <see cref="Message"/> address.</param>
        /// <param name="messageAddress">The message address.</param>
        public static void AddAddress(this Message message, MessageAddressType messageAddressType,
                                      MessageAddress messageAddress)
        {

            //It's ok to be undefined, but if it is set to anything, it has to be right type:
            if (messageAddress.Type == MessageAddressType.Undefined)
            {
                messageAddress.Type = messageAddressType;
            }
            else if (messageAddress.Type != messageAddressType)
            {
                throw new ArgumentException(
                    "messageAddress.Type must be '{0}'".FormatStringExceptionCulture(messageAddressType));
            }

            //secondly, it has to be of right MessageId:
            if ((messageAddress.MessageId != Guid.Empty) && (messageAddress.MessageId != message.MessageId))
            {
                throw new ArgumentException("MessageAddress has a Guid -- but it is not the same as the Message it is being added to.");
            }


            message.Addresses.Add(messageAddress);
        }


        /// <summary>
        /// Adds a Text body to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBody">The message body.</param>
        public static void AddATextBody(this Message message, MessageBody messageBody)
        {
            message.AddBody(MessageBodyType.Text,  messageBody);
        }

        /// <summary>
        /// Adds an HTML body to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBody">The message body.</param>
        public static void AddAnHtmlBody(this Message message, MessageBody messageBody)
        {
            message.AddBody(MessageBodyType.Html, messageBody);
        }


        /// <summary>
        /// Add a Body to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageBodyType">Type of the <see cref="Message"/> body.</param>
        /// <param name="messageBody">The message body.</param>
        /// <exception cref="System.ArgumentException">messageBody.Type must be '{0}'.FormatStringExceptionCulture(messageBodyType)</exception>
        public static void AddBody(this Message message, MessageBodyType messageBodyType, MessageBody messageBody)
        {
            if (messageBody.Type == MessageBodyType.Undefined)
            {
                messageBody.Type = messageBodyType;
            }
            else if (messageBody.Type != messageBodyType)
            {
                throw new ArgumentException(
                    "messageBody.Type must be '{0}'".FormatStringExceptionCulture(messageBodyType));
            }

            //secondly, it has to be of right MessageId:
            if ((messageBody.MessageId != Guid.Empty) && (messageBody.MessageId != message.MessageId))
            {
                throw new ArgumentException("MessageBody has a Guid -- but it is not the same as the Message it is being added to.");
            }


            message.Bodies.Add(messageBody);
        }

        /// <summary>
        /// Adds the attachment to the <see cref="Message"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAttachmentType">Type of the <see cref="Message"/> attachment.</param>
        /// <param name="messageAttachment">The message attachment.</param>
        /// <exception cref="System.ArgumentException">messageAttachment.Type must be '{0}'.FormatStringExceptionCulture(messageAttachmentType)</exception>
        public static void AddAttachment(this Message message, MessageAttachmentType messageAttachmentType,
                                         MessageAttachment messageAttachment)
        {
            if (messageAttachment.Type == MessageAttachmentType.Undefined)
            {
                messageAttachment.Type = messageAttachmentType;
            }
            else if (messageAttachment.Type != messageAttachmentType)
            {
                throw new ArgumentException(
                    "messageAttachment.Type must be '{0}'".FormatStringExceptionCulture(messageAttachmentType));
            }



            //secondly, it has to be of right MessageId:
            if ((messageAttachment.MessageId != Guid.Empty) && (messageAttachment.MessageId != message.MessageId))
            {
                throw new ArgumentException("MessageAddress has a Guid -- but it is not the same as the Message it is being added to.");
            }


            message.Attachments.Add(messageAttachment);
            
        }


        /// <summary>
        /// Changes the <see cref="MessageDeliveryStatus" /> of the <see cref="Message" />.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="status">The status.</param>
        /// <param name="source">The source.</param>
        /// <param name="note">The note.</param>
        public static void ChangeStatus(this Message message, MessageDeliveryStatus status,string source=null, string note=null)
        {
            message.Status = status;

            MessageStatus messageStatus = new MessageStatus {Status = status};

            if (!source.IsNullOrEmpty())
            {
                messageStatus.Source = source;
            }

            if (!note.IsNullOrEmpty())
            {
                messageStatus.Note = note;
            }

            message.Statuses.Add(messageStatus);

        }
    }
}