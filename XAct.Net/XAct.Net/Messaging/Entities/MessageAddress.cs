﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;
    using System.Text.RegularExpressions;
    using XAct.Net.Properties;
    using XAct.Text.RegularExpressions;

    /// <summary>
    /// An item within the 
    /// <see cref="Message.Addresses"/> collection.
    /// </summary>
    /// <internal>
    /// Intentionally an Anemic Entity for easier serialization. 
    /// <para>
    /// Behavior provided by Extension Methods.
    /// </para>
    /// </internal>
    [DataContract(Name = "MessageAddress")]
    public class MessageAddress : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IMessageIdentified, IMessagePart, IMessageNamedValue<string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageAddress"/> class.
        /// </summary>
        public MessageAddress()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageAddress" /> class.
        /// </summary>
        /// <param name="address">The address.</param>
        public MessageAddress(string address)
        {
            this.Set(address);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageAddress" /> class.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="displayName">The display name.</param>
        public MessageAddress(string address, string displayName)
        {
            this.Set(displayName, address);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageAddress" /> class.
        /// </summary>
        /// <param name="addressType">Type of the address.</param>
        /// <param name="address">The address.</param>
        /// <param name="displayName">The display name.</param>
        public MessageAddress(MessageAddressType addressType, string address, string displayName)
        {
            this.Set(addressType, address, displayName);
        }


        /// <summary>
        /// Gets or sets the datastore id of this address element.
        /// <para>Member defined in <see cref="IMessageIdentified"/>.</para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "Id", Order = 1, IsRequired = true)]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the parent/owner <see cref="Message.MessageId"/> (the datastore FK).
        /// <para>Member defined in <see cref="IMessagePart"/>.</para>
        /// </summary>
        /// <value>The message id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "MessageId", Order = 2, IsRequired = true)]
        public virtual Guid MessageId { get; set; }



        /// <summary>
        /// Gets or sets the address type (To, From, etc).
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "Type", Order = 3, IsRequired = true)]
        public virtual MessageAddressType Type { get; set; }



        /// <summary>
        /// Gets or sets the (optional) name alias (Display Name) for the address (eg: 'John Smith')
        /// <para>
        /// Implementation of <see cref="IMessageNamedValue{TValue}"/>
        /// </para>
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "Name", Order = 4, IsRequired = false, EmitDefaultValue = true)]
        public virtual string Name { get; set; }


        /// <summary>
        /// Gets or sets the address value.
        /// </summary>
        /// <value>The value.</value>
        public virtual string Value
        {
            get { return this._value; }
            set
            {
                value.ValidateIsNotNullOrEmpty("value", "Value cannot be null.");
                this._value = value;
            }
        }

        [DataMember(Name = "Value", Order = 5, EmitDefaultValue = true)] private string _value;






        #region Methods

        /// <summary>
        /// Parses a given address into its constituent
        /// address and optional display name.
        /// </summary>
        /// <param name="fullAddress">The full address.</param>
        /// <param name="parsedAddress">The parsed address.</param>
        /// <param name="parsedDisplayName">Display name of the parsed.</param>
        /// <returns></returns>
        public static bool ParseAddress(string fullAddress, out string parsedAddress, out string parsedDisplayName)
        {
            int num2;
            parsedAddress = string.Empty;
            parsedDisplayName = string.Empty;
            if (string.IsNullOrEmpty(fullAddress))
            {
                return false;
            }

            string str = fullAddress.Trim();
            int index = str.IndexOf('"');

            if (index != -1)
            {
                if (index > 0)
                {
                    return false;
                }
                num2 = str.IndexOf('"', index + 1);
                if (num2 == -1)
                {
                    throw new Exception(fullAddress);
                }
                index++;
                parsedDisplayName = str.Substring(index, num2 - index);
                str = str.Substring(num2 + 1);
            }
            index = str.IndexOf('<');
            if (index == -1)
            {
                parsedAddress = str;
            }
            else
            {
                num2 = str.IndexOf('>');
                if (num2 < index)
                {
                    throw new Exception(fullAddress);
                }
                if (string.IsNullOrEmpty(parsedDisplayName))
                {
                    parsedDisplayName = fullAddress.Substring(0, index - 1).Trim();
                }
                index++;
                parsedAddress = str.Substring(index, num2 - index);
            }
            return true;
        }


        /// <summary>
        /// Sets the address (accepts Address, or Address+Name).
        /// </summary>
        /// <param name="address">The address.</param>
        public void Set(string address)
        {
            string parsedAddress;
            string parsedDisplayName;
            if (address.IsNullOrEmpty())
            {
                throw new ArgumentNullException("address");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            ParseAddress(address, out parsedAddress, out parsedDisplayName);

            if (!ValidateEmailAddress(parsedAddress))
            {
                throw new Exception(
                    Resources.InvalidMessageAddress.FormatStringCurrentUICulture(parsedAddress)
                    );
            }
            this.Value = parsedAddress;
            this.Name = parsedDisplayName;
        }


        /// <summary>
        /// Validates a given email address, 
        /// using Regex.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>True if the email passed.</returns>
        public static bool ValidateEmailAddress(string emailAddress)
        {
            return (
                       (
                           //emailAddress == "MAILER-DAEMON") ||
                           //((emailAddress == "MAILER_DAEMON") || 
                           Regex.IsMatch(
                               emailAddress,ValidationRegex ,
                               RegexOptions.Singleline | RegexOptions.Compiled))
                   );
        }

        #endregion


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            //Use Extension method
            return this.ToString(false);
        }

        /// <summary>
        /// Gets the validation regex.
        /// </summary>
        /// <value>
        /// The validation regex.
        /// </value>
        public static string ValidationRegex
        {
            get
            {
                return _validationRegex ??
                       (_validationRegex =
                        CommonRegularExpressionPatterns.Validation.EmailAddressStrict);
            }

        }
        private static string _validationRegex;

    }

}