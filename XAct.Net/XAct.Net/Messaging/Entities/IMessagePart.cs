﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Interface for a part of a Message
    /// (eg: <see cref="MessageAddress"/>, 
    /// <see cref="MessageBody"/>, etc.)
    /// </summary>
    public interface IMessagePart
    {
        /// <summary>
        /// Gets or sets the owner message's Id.
        /// <para>Member defined in <see cref="XAct.Net.Messaging.IMessagePart"/>.</para>
        /// </summary>
        /// <value>
        /// The owner message Id.
        /// </value>
        Guid MessageId { get; set; }
    }
}
