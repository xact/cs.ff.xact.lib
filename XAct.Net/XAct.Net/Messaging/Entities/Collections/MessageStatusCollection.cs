namespace XAct.Net.Messaging
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// A collection of 
    /// <see cref="MessageStatus"/>
    /// elements.
    /// </summary>
    [Serializable, CollectionDataContract]
    public class MessageStatusCollection : List<MessageStatus>
    {
    }
}