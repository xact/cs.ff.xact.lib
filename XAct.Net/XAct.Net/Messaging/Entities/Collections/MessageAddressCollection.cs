namespace XAct.Net.Messaging
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// A collection of 
    /// <see cref="MessageAddress"/>
    /// elements.
    /// </summary>
    [Serializable, CollectionDataContract]
    public class MessageAddressCollection :  List<MessageAddress> 
    {
    }
}