﻿namespace XAct.Net.Messaging
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A summary of the distribution Status entries for the delivery 
    /// of a message.
    /// <para>
    /// Messages are sent along a chain of MTA's until delivery. 
    /// At various stages, reports of progress can be sent back to the source
    /// MTA, which can record those changes against the original message
    /// as <see cref="MessageStatus"/> records.
    /// </para>
    /// <remarks>
    /// <para>
    /// The MessageStatus is the reason the Message table currently requires
    /// a secondary index (by MessageId). But for high speed systems not that this 
    /// is not strictly necessary, if the MessageStatus tables made to be 
    /// not strictly Referential, and more loosely coupled.
    /// </para>
    /// Note the following scenario:
    /// Alice sends Message to Bob (Id=123, MessageId=abc)
    /// Alice Clones email to Eve
    /// Statuses come back for abc
    /// System must update by finding message according to abc.
    /// </remarks>
    /// </summary>
    [DataContract(Name = "MessageStatus")]
    public class MessageStatus : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IMessageIdentified, IMessagePart, IHasNote
    {
        /// <summary>
        /// Gets or sets the distributabe datastore id of this address element.
        /// <para>Member defined in <see cref="IHasDistributedGuidId"/>.</para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "Id", Order = 1, EmitDefaultValue = false, IsRequired = true)]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the parent/owner <see cref="Message.MessageId"/> (the datastore FK).
        /// <para>Member defined in <see cref="IMessagePart"/>.</para>
        /// </summary>
        /// <value>The message id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "MessageId", Order=2,IsRequired=true, EmitDefaultValue=false)]
        public virtual Guid MessageId { get; set; }

        /// <summary>
        /// Gets or sets the message statuses (Processing, Processed, Bounced, etc.).
        /// </summary>
        /// <value>The status.</value>
        [DataMember(Name = "Status",Order=3,IsRequired = true,EmitDefaultValue=false)]
        public virtual MessageDeliveryStatus Status { get; set; }


        /// <summary>
        /// Gets or sets any information regarding the source of the message.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        [DataMember(Name = "Source", Order = 4, IsRequired = true, EmitDefaultValue = false)]
        public virtual string Source { get; set; }

        /// <summary>
        /// Gets or sets an optional note, possibly returned information from remote MTA.
        /// <para>
        /// Member defined in the <see cref="IHasNote" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The note.
        /// </value>
        [DataMember(Name = "Note",Order=4,IsRequired=false,EmitDefaultValue = false)]
        public virtual string Note { get; set; }
    }
}