﻿namespace XAct.Net.Messaging
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// An item within the 
    /// <see cref="Message.Attachments"/> collection.
    /// </summary>
    /// <internal>
    /// Intentionally an Anemic Entity for easier serialization. 
    /// <para>
    /// Behavior provided by Extension Methods.
    /// </para>
    /// </internal>
    [DataContract(Name = "MessageHeader")]
    public class MessageHeader : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IMessageIdentified, IMessagePart, IMessageNamedValue<string> 
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageHeader"/> class.
        /// </summary>
        public MessageHeader()
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageHeader" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public MessageHeader(string name, string value)
        {
            this.Set(name,value);
        }

        /// <summary>
        /// Gets or sets the datastore id of this address element.
        /// <para>Member defined in <see cref="IHasDistributedGuidId"/>.</para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "Id", Order=1, IsRequired=true)]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the parent/owner <see cref="Message.MessageId"/> (the datastore FK).
        /// <para>Member defined in <see cref="IMessagePart"/>.</para>
        /// </summary>
        /// <value>The message id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "MessageId", Order = 2, IsRequired = true)]
        public virtual Guid MessageId { get; set; }


        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="T:XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        public virtual string Name
        {
            get { return this._name; }
            set
            {
                value.ValidateIsNotNullOrEmpty("value", XAct.Properties.Resources.ErrMsgValueCannotBeNull.FormatStringCurrentCulture("Name"));

#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
                if (string.Compare(value, _name, StringComparison.InvariantCulture) != 0)
                {
                    this._name = value;
                }
            }
        }

        [DataMember(Name = "Name", Order = 3, IsRequired = true)]
        private string _name;


        /// <summary>
        /// Gets or sets the value
        /// of this header.
        /// <para>Member defined in <see cref="XAct.IHasNamedValue{TValue}"/>.</para>
        /// </summary>
        /// <internal>
        /// Was going to make it so that 
        /// </internal>
        /// <value>The value.</value>
        public virtual string Value
        {
            get { return this._value; }
            set
            {
                value.ValidateIsNotDefault("value", XAct.Properties.Resources.ErrMsgValueCannotBeNull);

                  this._value = value;

#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            }
        }

        [DataMember(Name = "Value", Order = 4, EmitDefaultValue = false, IsRequired = true)]
        private string _value;



    }
}