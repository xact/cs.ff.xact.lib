﻿namespace XAct.Net.Messaging
{
    using System;
    using XAct.Implementations;

    /// <summary>
    /// The contract for a message.
    /// <para>Implemented by <see cref="Message"/>.</para>
    /// </summary>
    public interface IMessageIdentified 
    {

        /// <summary>
        /// Gets or sets the unique Id of this message.
        /// <para>
        /// A <see cref="Guid"/> is used for the datastore
        /// identity, rather than an Int,
        /// due to the very nature of messages -- which are
        /// distributed.
        /// </para>
        /// <para>
        /// The use of <see cref="DistributedIdService"/>
        /// to generate Guids may improve retrieval 
        /// performance slightly.
        /// </para>
        /// </summary>
        /// <value>
        /// The message id.
        /// </value>
        Guid MessageId { get; set; }
    }
}
