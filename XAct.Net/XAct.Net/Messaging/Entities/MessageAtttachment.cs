﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;
    using XAct.IO;

    /// <summary>
    /// An item within the 
    /// <see cref="Message.Attachments"/> collection.
    /// </summary>
    /// <internal>
    /// Intentionally an Anemic Entity for easier serialization. 
    /// <para>
    /// Behavior provided by Extension Methods.
    /// </para>
    /// </internal>
    [DataContract(Name = "MessageAttachment")]
    public class MessageAttachment : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IMessageIdentified, IMessagePart,
        IMessageNamedValue<byte[]>,
    IPersistedFile
    {

        /// <summary>
        /// Froms the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="ioService">The <see cref="IIOService"/> to use (can be from FileSystem in full desktop or 
        /// IsolatedStorage based system.
        /// </param>
        /// <para>
        /// If <paramref name="ioService"/> is left blank, will use 
        /// <see cref="XAct.DependencyResolver"/> to retrieve an implementation
        /// of <see cref="IFSIOService"/>
        /// </para>
        /// <returns></returns>
        public static MessageAttachment FromFile(string fileName, IIOService ioService=null)
        {

            if (ioService == null)
            {
                ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();
            }
            MessageAttachment messageAttachment = new MessageAttachment();

            messageAttachment.LoadFromFile(fileName,ioService);
            
            return messageAttachment;
        }

        /// <summary>
        /// Froms the stream.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static MessageAttachment FromStream(Stream stream)
        {
            MessageAttachment messageAttachment = new MessageAttachment();
            messageAttachment.LoadFromStream(stream);
            return messageAttachment;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageAttachment"/> class.
        /// </summary>
        public MessageAttachment()
        {
        }

        /// <summary>
        /// Gets or sets the datastore id of this address element.
        /// <para>Member defined in <see cref="IMessageIdentified"/>.</para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "Id",Order=1, IsRequired = true)]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the parent/owner <see cref="Message.MessageId"/> (the datastore FK).
        /// <para>Member defined in <see cref="IMessagePart"/>.</para>
        /// </summary>
        /// <value>The message id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
// ReSharper disable RedundantNameQualifier
        [DataMember(Name = "MessageId", Order=2, IsRequired = true)]
        public virtual System.Guid MessageId { get; set; }
// ReSharper restore RedundantNameQualifier


        /// <summary>
        /// Gets the Attachment Type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "Type", Order=3, IsRequired=true)]
        public virtual MessageAttachmentType Type { get; set; }

        ///// <summary>
        ///// Gets or sets a value indicating whether this file was loaded from a Stream versus a File.
        ///// </summary>
        ///// <value>
        /////   <c>true</c> if this instance is stream; otherwise, <c>false</c>.
        ///// </value>
        //[DataMember(Name = "IsStream", Order = 4, IsRequired = true)]
        //public virtual bool IsStream { get; set; }


        /// <summary>
        /// Gets the size of the attachment (<see cref="Value"/> Length).
        /// <para>
        /// Value is a calculated field, and is not serialized.
        /// </para>
        /// </summary>
        /// <value>The size.</value>
        //[NonSerialized]
        public virtual int Size
        {
            get
            {
                _size= ((this.Value != null) ? this.Value.Length : 0);
                return _size;
            }
            protected set { _size = value; }
        }
        [DataMember(Name = "Size", Order=5)]
        private int _size;

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        [DataMember(Name = "ContentType", Order = 6)]
        public virtual string ContentType { get; set; }


        /// <summary>
        /// Gets the date the attachment was last modified.
        /// <para>Member defined in <see cref="IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        /// <value>The date modified.</value>
        [DataMember(Name = "LastModifiedOn", Order = 7)]
        public virtual DateTime? LastModifiedOnUtc { get; set; }

        /// <summary>
        /// Gets the date the attachment was created.
        /// <para>Member defined in <see cref="IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        /// <value>The date created.</value>
        [DataMember(Name = "CreatedOn", Order = 8)]
        public virtual DateTime? CreatedOnUtc { get; set; }

        /// <summary>
        /// Not Used.
        /// <para>Member defined in <see cref="IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        [DataMember(Name = "DeletedOn", Order = 9)]
        public virtual DateTime? DeletedOnUtc { get; set; }




        /// <summary>
        /// Gets or sets the name
        /// of this attachment.
        /// <para>Member defined in <see cref="IHasNamedValue{TValue}"/>.</para>
        /// </summary>
        /// <value>The name.</value>
        public virtual string Name
        {
            get { return this._name; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value",
                        // ReSharper disable LocalizableElement
                                                    "Value cannot be null when setting ClientMessageAttachment.Name.");
                    // ReSharper restore LocalizableElement
                }
                if (value.Length == 0)
                {
                    // ReSharper disable LocalizableElement
                    throw new ArgumentException("String is Empty (Length == 0).", "value");
                    // ReSharper restore LocalizableElement
                }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
                // ReSharper disable RedundantCheckBeforeAssignment
                if (value != this._name)
                // ReSharper restore RedundantCheckBeforeAssignment
                {
                    this._name = value;
                }
            }
        }
        [DataMember(Name = "Name", Order = 10, IsRequired = true)]
        private string _name;



        /// <summary>
        /// Gets or sets the content id that needs to be set if you want to 
        /// embed the image in an html message.
        /// </summary>
        /// <value>
        /// The content id.
        /// </value>
        [DataMember(Name = "ContentId", Order = 11, IsRequired = false)]
        public string ContentId { get; set; }

        /// <summary>
        /// Gets or sets the value
        /// of this attachment.
        /// <para>Member defined in <see cref="IHasNamedValue{TValue}"/>.</para>
        /// </summary>
        /// <value>The value.</value>
        [DataMember(Name = "Value", Order = 12, EmitDefaultValue = false, IsRequired = true)]
        public virtual byte[] Value { get; set; }



        ///// <summary>
        ///// An internal variable, not serialised, used
        ///// to temporarily associate to the MessageAttachment 
        ///// the stream used to read it's value.
        ///// </summary>
        //[IgnoreDataMember]
        //public Stream Stream { get; set; }


    }
}