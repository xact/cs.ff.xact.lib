﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A non persisted (DTO) object to report on the
    /// status of a message's delivery.
    /// </summary>
    [DataContract(Name="MessageSummary")]
    public class MessageSummary: IMessageIdentified, IHasDateTimeCreatedOnUtc
    {
        /// <summary>
        /// Gets or sets the MessageId of the message being reported on with this summary.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember(Name = "MessageId", Order = 1)]
        public virtual Guid MessageId { get; set; }

        /// <summary>
        /// Gets or sets the message statuses 
        /// (Processing, Processed, Bounced, etc.).
        /// </summary>
        /// <value>The status.</value>
        [DataMember(Name = "Status", Order=2)]
        public virtual MessageDeliveryStatus Status { get; set; }


        /// <summary>
        /// The importance of this message 
        /// (Normal, Important).
        /// <para>
        /// This does not affect delivery -- only how 
        /// it is displayed (in outlook, it is done with a red flag)
        /// </para>
        /// </summary>
        /// <value></value>
        [DataMember(Name = "Importance", Order=3, IsRequired = false)]
        public virtual MessageImportance Importance { get; set; }

        /// <summary>
        /// Gets or sets the priority of this message.
        /// <para>
        /// Affects delivery queueing (High Priority messages are allocated more threads).
        /// </para>
        /// </summary>
        /// <value>The priority.</value>
        [DataMember(Name = "Priority", Order=4, EmitDefaultValue = false, IsRequired = false)]
        public virtual MessagePriority Priority { get; set; }

        /// <summary>
        /// Gets or sets the date the message was created.
        /// <para>
        /// Although nullable, never will be null, as set by Constructor.
        /// </para>
        /// </summary>
        /// <value>The date created.</value>
        [DataMember(Name = "CreatedOn", Order=5, IsRequired = false)]
        public virtual DateTime? CreatedOnUtc { get; set; }


        /// <summary>
        /// Gets or sets the date the message was sent.
        /// <para>
        /// Null if not yet sent.
        /// </para>
        /// </summary>
        /// <value>The date sent.</value>
        [DataMember(Name = "SentOn", Order=6, IsRequired = false)]
        public virtual DateTime? SentOnUtc { get; set; }


    }
}
