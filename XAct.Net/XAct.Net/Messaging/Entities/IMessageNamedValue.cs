﻿
// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for elements that have named values.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IMessageNamedValue<TValue> : IHasNamedValue<TValue>
    {
    }
}
