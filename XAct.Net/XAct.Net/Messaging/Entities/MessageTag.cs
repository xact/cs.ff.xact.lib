﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// A Tag that can be Many-To-Many associated to 
    /// persisted Messages.
    /// </summary>
    /// 
    [DataContract(Name="MessageTag")]
    public class MessageTag : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasTag<Guid>
    {
        /// <summary>
        /// Gets or sets the distributable <see cref="Guid"/> id of the Tag.
        /// <para>
        /// The Identifier is a <see cref="Guid"/> due to the very nature of distributed
        /// messaging systems.
        /// </para>
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember(Name="Id",Order=1)]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "Enabled", Order = 2)]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember(Name = "Order", Order = 3)]
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember(Name = "Name", Order = 4)]
        public virtual string Name { get; set; }




        /// <summary>
        /// Gets or sets the <see cref="Message"/>s this Tag is associated to.
        /// </summary>
        /// <value>
        /// The help entries.
        /// </value>
        public virtual ICollection<Message> Messages { get; set; }


    }
}
