﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.Serialization;
    using XAct.Environment;

    /// <summary>
    /// A message that can be sent by one or more delivery services.
    /// </summary>
    /// <remarks>
    /// <para>
    /// A design question that comes up regularly is why take the trouble
    /// of redesigning a set of Message entities, that already exist in the
    /// Framework as the <c>MailMessage</c> et al.
    /// </para>
    /// <para>
    /// Part of the reason is that MailMessage is a server technology, 
    /// not available in Silverlight. 
    /// </para>
    /// <para>
    /// Secondly, it was designed with only
    /// Smtp in mind. In a era of Twitter, etc. a message should be able
    /// to be delivered many different ways.
    /// </para>
    /// <para>
    /// Thirdly, and probably most importantly, the MailMessage entity
    /// is not WCF serializable as is.
    /// </para>
    /// <para>
    /// Requires serialisation to Db as either a whole object (blob)
    /// or searchable, by being put into persistence as separate objects.
    /// </para>
    /// </remarks>
    /// <internal>
    /// Intentionally an Anemic Entity for easier serialization. 
    /// <para>
    /// Behavior provided by Extension Methods.
    /// </para>
    /// <para>
    /// Does not inherit from MessageStatus even though they share properties
    /// because SOAP (therefore WCF) does not do Polymorphism/Inheritence.
    /// </para>
    /// </internal>
    //[Serializable]
    [DataContract(Name="Message")]
    public class Message : IHasXActLibEntity, IMessageIdentified, IHasDateTimeCreatedOnUtc, IHasApplicationTennantId
    {
        IDateTimeService DateTimeService
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        public Message()
        {
            //Do NOT set the MessageId, until persisted, or sent:
            //this.MessageId = Guid.NewGuid();
            //This is so that 
            //can distinguish better whether messages were persisted/sent, or are
            //still being built up.
            //the send method can take advantage of XAct.IDistributedIdService

            //But created can be set.
            //as it's not the same as Sent
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.CreatedOnUtc =  DateTimeService.NowUTC;
// ReSharper restore DoNotCallOverridableMethodsInConstructor
            
        }




        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Messages should be persisted and accessed per Org, per User.
        /// </para>
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        /// <value>
        /// The organisation id.
        /// </value>
        [DataMember(Name = "ApplicationTennantId", Order = 1, IsRequired = true)]
        public virtual Guid ApplicationTennantId { get; set; }


        /// <summary>
        /// Gets or sets the MTA specific Guid to track this record.
        /// <para>
        /// The MessageId is the Datastore Id.
        /// </para>
        /// <para>
        /// If the message is cloned, it gets a new Datatore Id. 
        /// This ensures that <see cref="MessageStatus"/> reports return to 
        /// the original sender, and not to any other users who may have been
        /// given a Cloned record.
        /// </para>
        /// <para>
        /// In the large scope of things, cloning saves not enough space to warrant
        /// the risks associated to shared records.
        /// </para>
        /// </summary>
        /// <value>The message id.</value>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "MessageId", Order = 2, IsRequired = true)]
        public virtual Guid MessageId { get; set; }





        /// <summary>
        /// Gets or sets a value indicating whether this message is to be persisted
        /// as a Draft (so that it doesn't get sent now).
        /// <para>
        /// This parameter is only taken into consideration if the 
        /// <see cref="IMessagingService"/> is configured to Spool messages for later
        /// delivery.
        /// </para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is draft; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "IsDraft", Order = 3, IsRequired = false)]
        public virtual bool IsDraft { get; set; }

        /// <summary>
        /// Gets or sets the message statuses (Processing, Processed, Bounced, etc.).
        /// </summary>
        /// <value>The status.</value>
        [DataMember(Name = "Status", Order = 4, IsRequired = true)]
        public virtual MessageDeliveryStatus Status { get; set; }


        /// <summary>
        /// The importance of this message (Normal, Important).
        /// <para>
        /// This does not affect delivery -- only how it is displayed (in outlook, it is done with a red flag)
        /// </para>
        /// </summary>
        /// <value></value>
        [DataMember(Name = "Importance", Order = 5, IsRequired = false)]
        public virtual MessageImportance Importance { get; set; }

        /// <summary>
        /// Gets or sets the priority of this message.
        /// <para>
        /// Affects delivery queueing (High Priority messages are allocated more threads).
        /// </para>
        /// </summary>
        /// <value>The priority.</value>
        [DataMember(Name = "Priority", Order=6, EmitDefaultValue=false, IsRequired = false)]
        public virtual MessagePriority Priority { get; set; }




        /// <summary>
        /// Gets or sets the date the message was created.
        /// <para>
        /// Although nullable, never will be null, as set by Constructor.
        /// </para>
        /// </summary>
        /// <value>The date created.</value>
        [DataMember(Name = "CreatedOn", Order = 7, IsRequired = false)]
        public virtual DateTime? CreatedOnUtc { get; set; }


        /// <summary>
        /// Gets or sets the date the message was sent.
        /// <para>
        /// Null if not yet sent.
        /// </para>
        /// </summary>
        /// <value>The date sent.</value>
        [DataMember(Name = "SentOn", Order = 8, IsRequired = false)]
        public virtual DateTime? SentOn { get; set; }



        /// <summary>
        /// Gets or sets the subject of this message.
        /// </summary>
        /// <value>The subject.</value>
        [DataMember(Name = "Subject", Order = 9, EmitDefaultValue = false, IsRequired = true)]
        public virtual string Subject { get; set; }
        


        /// <summary>
        /// The address the message is from.
        /// </summary>
        /// <value></value>
        [DataMember(Name = "From", Order = 10, EmitDefaultValue = false, IsRequired = true)]
        public virtual MessageAddress From
        {
            get { return Addresses.SingleOrDefault(a => a.Type == MessageAddressType.From); }
            set { 
                //There can only be one address here, so we remove and replace any that are there before:
                MessageAddress existing = Addresses.SingleOrDefault(a => a.Type == MessageAddressType.From);
                if (existing != null)
                {
                    Addresses.Remove(existing);
                }
                if (value.Type == MessageAddressType.Undefined)
                {
                    value.Type = MessageAddressType.From;
                }else if (value.Type != MessageAddressType.From)
                {
                    throw new ArgumentException("From.Type has to be either Undefined, or From");
                }
                Addresses.Add(value);
            }

        }


        /// <summary>
        /// The address the recipient should reply to.
        /// <para>
        /// As the message will be sent through a MTA, 
        /// a value is always required -- even if made to be same as <see cref="From"/>
        /// </para>
        /// </summary>
        /// <value></value>
        [DataMember(Name = "ReplyTo", Order = 11, EmitDefaultValue = false, IsRequired = true)]
        public virtual MessageAddress ReplyTo
        {
            get { return Addresses.SingleOrDefault(a => a.Type == MessageAddressType.ReplyTo); }
            set
            {
                MessageAddress existing = Addresses.SingleOrDefault(a => a.Type == MessageAddressType.ReplyTo);
                if (existing != null)
                {
                    Addresses.Remove(existing);
                }
                if (value.Type == MessageAddressType.Undefined)
                {
                    value.Type = MessageAddressType.ReplyTo;
                }
                else if (value.Type != MessageAddressType.ReplyTo)
                {
                    throw new ArgumentException("From.Type has to be either Undefined, or ReplyTo");
                }
                Addresses.Add(value);
            }
        }


        /// <summary>
        /// An enumeration of the 'To' addresses.
        /// <para>
        /// Use the <c>AddTo(...)</c> method to add an AddressMethod to the Addresss Collection.
        /// </para>
        /// </summary>
        /// <value></value>
        //[NonSerialized]
        [DataMember(Name = "To", Order = 12, EmitDefaultValue = false, IsRequired = true)]
        public virtual IEnumerable<MessageAddress> To
        {
            get { return Addresses.Where(a => a.Type == MessageAddressType.To); }
        }

        /// <summary>
        /// An enumeration of the 'To' addresses.
        /// <para>
        /// Use the <c>AddCc(...)</c> method to add an AddressMethod to the Addresss Collection.
        /// </para>
        /// </summary>
        /// <value></value>
        //[NonSerialized]
        [DataMember(Name = "Cc", Order = 13, EmitDefaultValue = false, IsRequired = false)]
        public virtual IEnumerable<MessageAddress> Cc
        {
            get { return Addresses.Where(a => a.Type == MessageAddressType.Cc); }
        }

        /// <summary>
        /// The collection of addresses the message is blind copied to.
        /// <para>
        /// Use the <c>AddBcc(...)</c> method to add an AddressMethod to the Addresss Collection.
        /// </para>
        /// </summary>
        /// <value></value>
        //[NonSerialized]
        [DataMember(Name = "Bcc", Order = 14, EmitDefaultValue = false, IsRequired = false)]
        public virtual IEnumerable<MessageAddress> Bcc
        {
            get { return Addresses.Where(a => a.Type == MessageAddressType.Bcc); }
        }







        /// <summary>
        /// Gets or sets the delivery protocols.
        /// </summary>
        /// <value>
        /// The delivery protocols.
        /// </value>
        [DataMember(Name = "DeliveryProtocols", Order = 15, EmitDefaultValue = false, IsRequired = false)]
        public virtual string DeliveryProtocols { get; set; }







        /// <summary>
        /// Gets the collection of <see cref="MessageStatus"/> elements.
        /// </summary>
        /// <value>The headers.</value>
        public virtual ICollection<MessageStatus> Statuses { get { return _statuses ?? (_statuses = new Collection<MessageStatus>()); } }
        [DataMember(Name = "Statuses", EmitDefaultValue = false, IsRequired = false)]
        ICollection<MessageStatus> _statuses;
    
        /// <summary>
        /// Gets the collection of <see cref="MessageHeader"/> elements.
        /// </summary>
        /// <value>The headers.</value>
        public virtual ICollection<MessageHeader> Headers { get { return _headers ?? (_headers = new Collection<MessageHeader>()); } }
        [DataMember(Name = "Headers", EmitDefaultValue = false, IsRequired = false)]
        ICollection<MessageHeader> _headers;

        /// <summary>
        /// Gets the collection of <see cref="MessageAddress"/> elements.
        /// </summary>
        /// <value>The addresses.</value>
        //[NonSerialized]
        public virtual ICollection<MessageAddress> Addresses { get { return _addresses ?? (_addresses = new Collection<MessageAddress>()); } }
        [DataMember(Name = "Addresses", EmitDefaultValue = false, IsRequired = false)]
        ICollection<MessageAddress> _addresses;

        
        /// <summary>
        /// Gets the collection of <see cref="MessageBody"/> elements.
        /// </summary>
        /// <value>The bodies.</value>
        public virtual ICollection<MessageBody> Bodies { get { return _bodies ?? (_bodies = new Collection<MessageBody>()); } }
        [DataMember(Name = "Bodies", EmitDefaultValue = false, IsRequired = false)]
        ICollection<MessageBody> _bodies;
        

        /// <summary>
        /// Gets the collection of <see cref="MessageAttachment"/> elements.
        /// </summary>
        /// <value>The attachments.</value>
        public virtual ICollection<MessageAttachment> Attachments { get { return _attachments ?? (_attachments = new Collection<MessageAttachment>()); } }
        [DataMember(Name = "Atachments", EmitDefaultValue = false, IsRequired = false)]
        ICollection<MessageAttachment> _attachments;


        /// <summary>
        /// Gets or sets the optional list of tags 
        /// associated to this <see cref="MessageTag"/>.
        /// </summary>
        /// <value>
        /// The tags.
        /// </value>
        [DataMember]
        public virtual ICollection<MessageTag> Tags { get { return _tags ?? (_tags = new Collection<MessageTag>()); } }
        private ICollection<MessageTag> _tags;

        

    }
}