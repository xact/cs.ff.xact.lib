﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// An item within the 
    /// <see cref="Message.Bodies"/> collection.
    /// </summary>
    /// <internal>
    /// Intentionally an Anemic Entity for easier serialization. 
    /// <para>
    /// Behavior provided by Extension Methods.
    /// </para>
    /// </internal>
    [DataContract(Name="MessageBody")]
    public class MessageBody : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IMessageIdentified, IMessagePart, IMessageNamedValue<string>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBody"/> class.
        /// </summary>
        public MessageBody()
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBody" /> class.
        /// </summary>
        /// <param name="messageBodyType">Type of the message body.</param>
        /// <param name="bodyTextValue">The body text.</param>
        public MessageBody(MessageBodyType messageBodyType, string bodyTextValue)
        {
            this.Type = messageBodyType;
            this.Set(null, bodyTextValue);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBody" /> class.
        /// </summary>
        /// <param name="messageBodyType">Type of the message body.</param>
        /// <param name="bodyTextValue">The body text value.</param>
        /// <param name="name">The name.</param>
        public MessageBody(MessageBodyType messageBodyType, string bodyTextValue, string name)
        {
            this.Type = messageBodyType;
            this.Set(name, bodyTextValue);
        }

        /// <summary>
        /// Gets or sets the datastore id 
        /// of this address element.
        /// <para>Member defined in <see cref="IMessageIdentified"/>.</para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "Id", Order = 1, IsRequired = true)]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the parent/owner <see cref="Message.MessageId"/> (the datastore FK).
        /// </summary>
        /// <value>The message id.</value>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "MessageId", Order = 2, IsRequired = true)]
        public virtual Guid MessageId { get; set; }


        /// <summary>
        /// Gets or sets the Message Body Type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "Type", Order = 3, IsRequired=true, EmitDefaultValue = false)]
        public virtual MessageBodyType Type { get; set; }


        /// <summary>
        /// Gets or sets the name
        /// of this body.
        /// <para>Member defined in <see cref="XAct.IHasNamedValue{TValue}"/>.</para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember(Name = "Name", Order=4, IsRequired=false, EmitDefaultValue = true)]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the value
        /// of this body.
        /// <para>Member defined in <see cref="XAct.IHasNamedValue{TValue}"/>.</para>
        /// </summary>
        /// <value>The value.</value>
        /// <internal>
        /// It is acceptable to have an email with no body (EOM).
        /// </internal>
        [DataMember(Name = "Value", Order=5,IsRequired=false, EmitDefaultValue = true)]
        public virtual string Value { get; set; }






    }
}