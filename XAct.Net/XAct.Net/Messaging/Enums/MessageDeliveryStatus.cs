﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "MessageDeliveryStatus", Namespace = "http://xact-solutions.com/Services/Messaging/2011/01/01")
    ]
    public enum MessageDeliveryStatus
    {
        /// <summary>
        /// 
        /// <para>
        /// Note: this is an error state
        /// to ensure the Client software has considered/chosen a value.
        /// </para>
        /// <para>
        /// Value is '0'
        /// </para>
        /// </summary>
        [EnumMember] Undefined = 0,

            
            
            
        /// <summary>
        /// 
        /// <para>
        /// Value is '30'
        /// </para>
        /// </summary>
        [EnumMember] Bounced = 30,
        /// <summary>
        /// 
        /// <para>
        /// Value is '20'
        /// </para>
        /// </summary>
        [EnumMember] Defer = 20,
        /// <summary>
        /// 
        /// <para>
        /// Value is '0x15'
        /// </para>
        /// </summary>
        [EnumMember] Failed = 0x15,
        /// <summary>
        /// 
        /// <para>
        /// Value is '1'
        /// </para>
        /// </summary>
        [EnumMember] Logged = 1,
        /// <summary>
        /// 
        /// <para>
        /// Value is '2'
        /// </para>
        /// </summary>
        [EnumMember] Processing = 2,
        /// <summary>
        /// 
        /// <para>
        /// Value is '0x16'
        /// </para>
        /// </summary>
        [EnumMember] Sent = 0x16,
        /// <summary>
        /// 
        /// <para>
        /// Value is '12'
        /// </para>
        /// </summary>
        [EnumMember] ServiceAccepted = 12,
        /// <summary>
        /// 
        /// <para>
        /// Value is '10'
        /// </para>
        /// </summary>
        [EnumMember] ServiceError = 11,
        /// <summary>
        /// 
        /// <para>
        /// Value is '10'
        /// </para>
        /// </summary>
        [EnumMember] ServiceRejected = 10,

    }
}