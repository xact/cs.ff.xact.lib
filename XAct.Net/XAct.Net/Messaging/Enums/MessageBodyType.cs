﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration of the types of Bodies.
    /// <para>
    /// Used by <see cref="MessageBody.Type"/>
    /// </para>
    /// </summary>
    [DataContract(Name = "MessageBodyType")]
    public enum MessageBodyType
    {
        /// <summary>
        /// An Error state.
        /// <para>
        /// Value is '0'
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// The body is a Text body.
        /// <para>
        /// Value is '1'
        /// </para>
        /// </summary>
        [EnumMember]
        Text = 1,

        /// <summary>
        /// The body is a rich Html body.
        /// <para>
        /// Value is '2'
        /// </para>
        /// </summary>
        [EnumMember]
        Html = 2,

        /// <summary>
        /// The body is a type specific to the transmission protocol.
        /// <para>
        /// Value is '3'
        /// </para>
        /// </summary>
        [EnumMember]
        ProtocolSpecific = 3
    }
}