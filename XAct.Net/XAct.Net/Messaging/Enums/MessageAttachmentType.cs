// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration of the various types
    /// a 
    /// <see cref="MessageAttachment"/> can be.
    /// </summary>
    [DataContract(Name = "MessageAttachmentType", Namespace = "http://xact-solutions.com/Services/Messaging/2011/01/01")
    ]
    public enum MessageAttachmentType
    {
        /// <summary>
        /// Undefined.
        /// <para>
        /// Note: this is an error state
        /// to ensure the Client software has considered/chosen a value.
        /// </para>
        /// <para>
        /// Value is '0'
        /// </para>
        /// </summary>
        [EnumMember] Undefined = 0,

        /// <summary>
        /// The attachment is a normal message attachment.
        /// <para>
        /// Value is '1'
        /// </para>
        /// </summary>
        [EnumMember] Attachment = 1,

        /// <summary>
        /// The attachment is referred to inline.
        /// <para>
        /// Value is '2'
        /// </para>
        /// </summary>
        [EnumMember] Inline = 2,
    }
}