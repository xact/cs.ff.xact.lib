﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration of the types of Priorities for an
    /// <see cref="Message"/>.
    /// <para>
    /// Used by <see cref="Message.Priority"/>
    /// </para>
    /// </summary>
    /// <internal>
    /// Privority values are ascending in order to easily
    /// Linq.Sort ascending
    /// </internal>
    [DataContract(Name = "MessagePriority", Namespace = "http://xact-solutions.com/Services/Messaging/2011/01/01")]
    public enum MessagePriority
    {
        /// <summary>
        /// <para>
        /// Note: this is an error state
        /// to ensure the Client software has considered/chosen a value.
        /// </para>
        /// <para>
        /// Value is '0'
        /// </para>
        /// </summary>
        [EnumMember] Undefined = 0,

        /// <summary>
        /// The message is of Urgent priority.
        /// <para>Value = '1'</para>
        /// </summary>
        [EnumMember] Urgent = 1,

        /// <summary>
        /// The message is of Normal priority.
        /// <para>Value = '2'</para>
        /// </summary>
        [EnumMember]
        Normal = 2,

        /// <summary>
        /// The message is of Low priority.
        /// <para>Value = '3'</para>
        /// </summary>
        [EnumMember]
        Low = 3,

        /// <summary>
        /// The message is of Very Low priority.
        /// <para>Value = '4'</para>
        /// </summary>
        [EnumMember]
        VeryLow = 4
    }
}