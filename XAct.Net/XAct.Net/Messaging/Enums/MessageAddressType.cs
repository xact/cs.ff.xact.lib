﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The Type pf <see cref="Message"/>
    /// </summary>
    [DataContract(Name = "MessageAttachmentType")]
    public enum MessageAddressType
    {
        /// <summary>
        /// The MessageAddress Type is undefined.
        /// <para>
        /// Note: this is an error state
        /// to ensure the Client software has considered/chosen a value.
        /// </para>
        /// <para>
        /// Value is '0'
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined = 0,
        /// <summary>
        /// The MessageAddress is a From address.
        /// <para>
        /// Value is '1'
        /// </para>
        /// </summary>
        [EnumMember]
        From = 1,



        /// <summary>
        /// The MessageAddress is a ReplyTo address.
        /// <para>
        /// Value is '2'
        /// </para>
        /// </summary>
        [EnumMember]
        ReplyTo = 2,
        
        /// <summary>
        /// The MessageAddress is a To address.
        /// <para>
        /// Value is '3'
        /// </para>
        /// </summary>
        [EnumMember]
        To = 3,


        /// <summary>
        /// The MessageAddress is a Cc address.
        /// <para>
        /// Value is '4'
        /// </para>
        /// </summary>
        [EnumMember]
        Cc = 4,
        /// <summary>
        /// The MessageAddress is a Bcc address.
        /// <para>
        /// Value is '5'
        /// </para>
        /// </summary>
        [EnumMember]
        Bcc = 5,
    }
}