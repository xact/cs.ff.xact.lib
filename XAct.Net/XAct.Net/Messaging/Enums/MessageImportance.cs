﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration of the types of Importance.
    /// <para>
    /// Used by <see cref="Message.Importance"/>
    /// </para>
    /// </summary>
    [DataContract(Name = "MessageImportance")]
    public enum MessageImportance
    {
        /// <summary>
        /// The importance is undefined.
        /// <para>
        /// Note: this is an error state
        /// to ensure the Client software has considered/chosen a value.
        /// </para>
        /// <para>
        /// Value is '0'
        /// </para>
        /// </summary>
        [EnumMember] Undefined = 0,

        /// <summary>
        /// The message subject/body is of normal importance.
        /// <para>
        /// Value is '1'
        /// </para>
        /// </summary>
        [EnumMember] Normal = 1,

        /// <summary>
        /// The message is important.
        /// <para>
        /// Value is '1'
        /// </para>
        /// </summary>
        [EnumMember] Important = 2,
    }
}