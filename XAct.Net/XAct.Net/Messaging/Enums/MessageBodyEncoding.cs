﻿// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    /// <summary>
    /// TODO: Document
    /// </summary>
    [DataContract(Name = "MessageBodyEncoding")]
    public enum MessageBodyEncoding
    {/// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '0'
        /// </para>
        /// <para>
        /// Note: this is an error state
        /// to ensure the Client software has considered/chosen a value.
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined = 0,


        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '1'
        /// </para>
        /// </summary>
        [EnumMember] Base64 = 1,
        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '5'
        /// </para>
        /// </summary>
        [EnumMember] Binary = 5,
        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '2'
        /// </para>
        /// </summary>
        [EnumMember] Bit7 = 2,
        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '3'
        /// </para>
        /// </summary>
        [EnumMember] Bit8 = 3,
        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '7'
        /// </para>
        /// </summary>
        [EnumMember] NullEncoding = 7,
        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '4'
        /// </para>
        /// </summary>
        [EnumMember] QuotedPrintable = 4,
        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '0x10'
        /// </para>
        /// </summary>
        [EnumMember] QuotedPrintableMinimal = 0x10,
        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '8'
        /// </para>
        /// </summary>
        [EnumMember] QuotedPrintableRelaxed = 8,
        
        /// <summary>
        /// TODO: Document
        /// <para>
        /// Value is '6'
        /// </para>
        /// </summary>
        [EnumMember] XToken = 6
    }
}