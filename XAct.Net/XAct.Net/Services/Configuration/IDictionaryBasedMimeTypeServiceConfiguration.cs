﻿namespace XAct.Net.Services.Configuration
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for the configuration package 
    /// for the <see cref="IMimeTypeService"/>
    /// </summary>
    public interface IDictionaryBasedMimeTypeServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets the 
        /// case insensitive 
        /// cache of file name extensions (with prefix '.') to mime type.
        /// <para>
        /// The dictionary is not static 
        /// and therefore is subsequently adjustable during application
        /// initialization, as needed.
        /// </para>
        /// </summary>
        /// <value>
        /// The cache.
        /// </value>
        Dictionary<string, string> Cache { get; }
    }
}