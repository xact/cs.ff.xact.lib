﻿namespace XAct.Net.Messaging.Services
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class MessageDeliverySimpleMessageService : XActLibServiceBase, ISimpleMessageService, IHasLowBindingPriority
    {
        private readonly IMessageDeliveryService _smtpMessageDeliveryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDeliverySimpleMessageService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="smtpMessageDeliveryService">The SMTP message delivery service.</param>
        public MessageDeliverySimpleMessageService(ITracingService tracingService, IMessageDeliveryService smtpMessageDeliveryService)
            : base(tracingService)
        {
            _smtpMessageDeliveryService = smtpMessageDeliveryService;
        }

        /// <summary>
        /// Sends the specified simple message.
        /// </summary>
        /// <param name="simpleMessage">The simple message.</param>
        public void Send(SimpleMessage simpleMessage)
        {
            var message = new Message();

            message.AddTo(new MessageAddress(simpleMessage.Destination));
            message.Subject = simpleMessage.Subject;
            message.Bodies.Add(new MessageBody(MessageBodyType.Text,  simpleMessage.Body));

            _smtpMessageDeliveryService.SendMessage(message);
        }

    }

}
