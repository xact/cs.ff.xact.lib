﻿namespace XAct.Net.Implementations
{
    using System;
    using System.IO;
    using System.Net;
    using XAct.Diagnostics;
    using XAct.IO;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IRequestService"/>
    /// to download files from the Web.
    /// </summary>
    public class RequestService : IRequestService
    {
        private readonly ITracingService _tracingService;
        private readonly IIOService _ioService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="ioService">The io service.</param>
        public RequestService(ITracingService tracingService, IIOService ioService)
        {
            _tracingService = tracingService;
            _ioService = ioService;
        }

        /// <summary>
        /// Downloads the remote file to a temp file.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns>The temp filename, or null if the file could not be downloaded</returns>
        public string DownloadRemoteFile(string uri)
        {
            string tmpFileName = Path.Combine(Path.GetTempPath(), string.Format("SmptTestFile_{0}.png", Guid.NewGuid().ToString("D")));
            return DownloadRemoteFile(uri, tmpFileName);
        }


        /// <summary>
        /// Downloads the remote file to the given filename.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// The filename, or null if the file could not be downloaded
        /// </returns>
        public string DownloadRemoteFile(string uri, string fileName)
        {

            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);
            HttpWebResponse response;
                

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception)
            {
                return null;
            }

            // Check that the remote file was found. The ContentType
            // check is performed since a request for a non-existent
            // image file might be redirected to a 404-page, which would
            // yield the StatusCode "OK", even though the image was not
            // found.
            if (response.StatusCode == HttpStatusCode.OK)
            {
                // if the remote file was found, download oit
                using (Stream inputStream = response.GetResponseStream())
                {
                    inputStream.WriteStreamToFile(_ioService, fileName, true);
                }
                return fileName;
            }
            return null;
        }
    }
}