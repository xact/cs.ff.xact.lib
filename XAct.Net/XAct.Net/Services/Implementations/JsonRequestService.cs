using System;
using System.Net;
using System.Runtime.Serialization.Json;

namespace XAct.Net.Services.Implementations
{
    /// <summary>
    /// Implementation of <see cref="IJsonRequestService"/>
    /// to make requests.
    /// </summary>
    public class JsonRequestService : IJsonRequestService
    {

        //using using System.Net;
        //SYstem.Runtime.Serialization

        /// <summary>
        /// Makes the request for a JSON response.
        /// </summary>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="requestUrl">The request URL.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public TResponse MakeRequest<TResponse>(string requestUrl)

            where TResponse : class
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception(String.Format(
                            "Server error (HTTP {0}: {1}).",
                            response.StatusCode,
                            response.StatusDescription));

                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(TResponse));

                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());

                    TResponse jsonResponse = objResponse as TResponse;
                    return jsonResponse;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}