﻿using XAct.Net.Services.Configuration;

namespace XAct.Net.Implementations
{
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="IDictionaryBasedMimeTypeService"/>
    /// contract 
    /// </summary>
    /// <internal>
    /// See: http://stackoverflow.com/questions/1029740/get-mime-type-from-filename-extension
    /// </internal>
    [DefaultBindingImplementation(typeof(IMimeTypeService), BindingLifetimeType.Undefined, Priority.Low /*OK:SecondaryBinding*/)]
    public class DictionaryBasedMimeTypeService : IDictionaryBasedMimeTypeService
    {

        /// <summary>
        /// Gets or sets the configuration used by this service.
        /// <para>
        /// The Configuration object is shared between instances of
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IDictionaryBasedMimeTypeServiceConfiguration Configuration { get { return _dictionaryBasedMimeTypeServiceConfiguration; } }
        private readonly IDictionaryBasedMimeTypeServiceConfiguration _dictionaryBasedMimeTypeServiceConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryBasedMimeTypeService"/> class.
        /// </summary>
        /// <param name="dictionaryBasedMimeTypeServiceConfiguration">The dictionary based MIME type service configuration.</param>
        public DictionaryBasedMimeTypeService(IDictionaryBasedMimeTypeServiceConfiguration dictionaryBasedMimeTypeServiceConfiguration)
        {
            _dictionaryBasedMimeTypeServiceConfiguration = dictionaryBasedMimeTypeServiceConfiguration;
        }


        /// <summary>
        /// Gets the MIME type from the given file extension.
        /// </summary>
        /// <param name="fileNameExtensionWithPrefixDot">The file name extension (with prefix dot) as is returned with Path.GetExtension().</param>
        /// <returns></returns>
        public string GetMimeTypeFromFileExtension(string fileNameExtensionWithPrefixDot)
        {
            string mimeType;

            if (_dictionaryBasedMimeTypeServiceConfiguration.Cache.TryGetValue(fileNameExtensionWithPrefixDot, out mimeType))
            {
                return mimeType;
            }
            return null;
        }
    }
}