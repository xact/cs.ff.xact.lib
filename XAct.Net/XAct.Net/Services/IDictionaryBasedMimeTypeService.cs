﻿using XAct.Net.Services.Configuration;

namespace XAct.Net
{
    /// <summary>
    /// Contract for a dictionary based (ie, not requiring access to Registry or IIS metadata)
    /// implementation of <see cref="IMimeTypeService"/>
    /// to provide Mime related operations.
    /// </summary>
    public interface IDictionaryBasedMimeTypeService  : IHasXActLibService, IMimeTypeService
    {

        /// <summary>
        /// Gets or sets the configuration used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IDictionaryBasedMimeTypeServiceConfiguration Configuration { get; }
    }
}