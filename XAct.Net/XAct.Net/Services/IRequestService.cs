﻿
namespace XAct.Net
{
    /// <summary>
    /// Contract for a service to download resources from the web.
    /// </summary>
    public interface IRequestService : IHasXActLibService
    {
        /// <summary>
        /// Downloads the remote file to a temp file location.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns>The temp filename, or null if the file could not be downloaded</returns>
        string DownloadRemoteFile(string uri);

        /// <summary>
        /// Downloads the remote file to the given filename.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// The filename, or null if the file could not be downloaded
        /// </returns>
        string DownloadRemoteFile(string uri, string fileName);
    }
}
