﻿namespace XAct.Net
{
    /// <summary>
    /// The contract for a service 
    /// to provide Mime related operations.
    /// </summary>
    public interface IMimeTypeService : IHasXActLibService
    {

        /// <summary>
        /// Gets the MIME type from the given file extension.
        /// </summary>
        /// <param name="fileNameExtensionWithPrefixDot">The file name extension (with prefix dot) as is returned with Path.GetExtension().</param>
        /// <returns></returns>
        string GetMimeTypeFromFileExtension(string fileNameExtensionWithPrefixDot);
    }
}