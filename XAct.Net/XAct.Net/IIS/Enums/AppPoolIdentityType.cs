﻿

namespace XAct.Net.IIS
{
	public enum AppPoolIdentityType {
		LocalSystem = 0,
		LocalService = 1,
		NetworkService = 2,
		SpecifiedUserAccount = 3,		
	}

}
