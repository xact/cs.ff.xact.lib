﻿using System;
using System.DirectoryServices;
using XAct.Net;
using XAct.Net.IIS;

namespace XAct.DirectoryServices
{
    public interface IAppPool
    {

        /// <summary>
        /// Gets the name of the machine.
        /// <para>
        /// eg: 'localhost'
        /// </para>
        /// </summary>
        /// <value>The name of the machine.</value>
        string MachineName { get; }
        
        /// <summary>
        /// Gets the name of the AppPool.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; }



        AppPoolIdentityType AppPoolIdentityType { get;  }

        string AppPoolState { get;  }
        
        string AppPoolCommand { get;  }
        string KeyType { get;  }
        object Win32Error { get;  }
        string ManagedRuntimeVersion { get;  }




        /// <summary>
        /// Gets a value indicating whether this <see cref="IAppPool"/> 
        /// is currently being recycled.
        /// </summary>
        /// <value><c>true</c> if recycling; otherwise, <c>false</c>.</value>
        bool Recycling { get; }

        /// <summary>
        /// Recyles the AppPool.
        /// </summary>
        void Recyle();
        /// <summary>
        /// Starts this AppPool.
        /// </summary>
        void Start();
        /// <summary>
        /// Stops this AppPool.
        /// </summary>
        void Stop();
    }

    public interface IAppPoolHealth
    {
        
    }

    public interface IAppPoolIdentity
    {
        
    }
    public interface IAppPoolPerformance
    {
        
    }

    public class AppPool : IAppPool
    {

        #region Implementation of IAppPool Properties
        /// <summary>
        /// Gets the name of the machine.
        /// <para>
        /// eg: 'localhost'
        /// </para>
        /// </summary>
        /// <value>The name of the machine.</value>
        public string MachineName { get; private set; }

        /// <summary>
        /// Gets the name of the AppPool.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; private set; }

        public AppPoolIdentityType AppPoolIdentityType { get; private set; }
        public string AppPoolState { get; private set; }
        public string AppPoolCommand { get; private set; }
        public string KeyType { get; private set; }
        public object Win32Error { get; private set; }
        public string ManagedRuntimeVersion { get; private set; }

        #endregion


        public string Path { get; private set; }



        public bool Recycling
        {
            get { throw new NotImplementedException(); }
        }



        #region Constructors
        public AppPool(string appPoolName)
        {
            LoadAppPool("localhost", appPoolName);
        }
        public AppPool(string machineName, string appPoolName)
        {
            LoadAppPool(machineName,appPoolName);
        }

        public AppPool(string machineName, DirectoryEntry appPoolDirectoryEntry)
        {

            LoadAppPool(machineName, appPoolDirectoryEntry);
        }
        #endregion

        #region Implementation of IAppPool 

        /// <summary>
        /// Recyles the AppPool.
        /// </summary>
        public void Recyle()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Starts this AppPool.
        /// </summary>
        public void Start()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stops this AppPool.
        /// </summary>
        public void Stop()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Private Methods
        private void LoadAppPool(string machineName, string appPoolName)
        {
            DirectoryEntry directoryEntry = 
                new DirectoryEntry(
                    string.Format("IIS://{0}/W3SVC/AppPools/{1}",machineName, appPoolName));  

            LoadAppPool(machineName, directoryEntry);
        }

        private void LoadAppPool(string machineName, DirectoryEntry appPoolDirectoryEntry)
        {
            this.Name = appPoolDirectoryEntry.Name;
            this.Path = appPoolDirectoryEntry.Path;


            //this.AppPoolIdentityType = appPoolDirectoryEntry.Properties.GetPropertyValue("AppPoolIdentityType");
            //this.AppPoolState = appPoolDirectoryEntry.Properties.GetPropertyValue("AppPoolState");
            //this.Win32Error = appPoolDirectoryEntry.Properties.GetPropertyValue("Win32Error");
            //this.AppPoolCommand = appPoolDirectoryEntry.Properties.GetPropertyValue("AppPoolCommand");
            //this.KeyType = appPoolDirectoryEntry.Properties.GetPropertyValue("KeyType");
            //this.ManagedRuntimeVersion = appPoolDirectoryEntry.Properties.GetPropertyValue("ManagedRuntimeVersion");


            //Recycling.RestartTime = Convert.ToInt32(appPool.GetProperty("PeriodicRestartTime"));
            //Recycling.RestartRequests = Convert.ToInt32(appPool.GetProperty("PeriodicRestartRequests"));
            //Recycling.SetRestartScheduleArray((Array)appPool.GetProperty("PeriodicRestartSchedule"));
            //Recycling.MaximumVirtualMemory = Convert.ToInt32(appPool.GetProperty("PeriodicRestartMemory")) / 1024;
            //Recycling.MaximumUsedMemory = Convert.ToInt32(appPool.GetProperty("PeriodicRestartPrivateMemory")) / 1024;

            //Performance.IdleTimeout = Convert.ToInt32(appPool.GetProperty("IdleTimeout"));
            //Performance.RequestQueueLimit = Convert.ToInt32(appPool.GetProperty("AppPoolQueueLength"));
            //Performance.CpuMaximumUse = Convert.ToInt32(appPool.GetProperty("CPULimit")) / 1000;
            //Performance.CpuRefreshInterval = Convert.ToInt32(appPool.GetProperty("CPUResetInterval"));
            //Performance.CpuAction = (CpuAction)Enum.Parse(typeof(CpuAction), appPool.GetProperty("CPUAction").ToString());
            //Performance.WorkerProcesses = Convert.ToInt32(appPool.GetProperty("MaxProcesses"));

            //Health.PingEnabled = Convert.ToBoolean(appPool.GetProperty("PingingEnabled"));
            //Health.PingInterval = Convert.ToInt32(appPool.GetProperty("PingInterval"));
            //Health.RapidFailProtectionEnabled = Convert.ToBoolean(appPool.GetProperty("RapidFailProtection"));
            //Health.RapidFailProtectionMaximumFailures = Convert.ToInt32(appPool.GetProperty("RapidFailProtectionMaxCrashes"));
            //Health.RapidFailProtectionInterval = Convert.ToInt32(appPool.GetProperty("RapidFailProtectionInterval"));
            //Health.StartupTimeLimit = Convert.ToInt32(appPool.GetProperty("StartupTimeLimit"));
            //Health.ShutdownTimeLimit = Convert.ToInt32(appPool.GetProperty("ShutdownTimeLimit"));
        }
        #endregion


    }
}
