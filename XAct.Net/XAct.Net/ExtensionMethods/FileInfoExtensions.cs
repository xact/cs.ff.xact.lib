﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.IO;
    using XAct.IO;
    using XAct.Net;

    /// <summary>
    /// Extension Methods to the 
    /// <see cref="FileInfo"/>
    /// class.
    /// </summary>
    /// <remarks>
    /// Dependencies: requires <see cref="IMimeTypeService"/>
    /// </remarks>
    public static class IFileInfoExtensions
    {
        /// <summary>
        /// Gets the MIME type appropriate for the file, based on the filename's extension.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This is...ok. Not great.
        /// MIME type should be determined 
        /// by the content -- not the extension.
        /// But this is a known method in windows.
        /// </para>
        /// </remarks>
        /// <param name="fileInfo">The file info.</param>
        /// <returns></returns>
        public static string GetMimeType(this IFileInfo fileInfo)
        {
            IMimeTypeService mimeService = DependencyResolver.Current.GetInstance<IMimeTypeService>();

            return mimeService.GetMimeTypeFromFileExtension(Path.GetExtension(fileInfo.Name));
        }
    }
}