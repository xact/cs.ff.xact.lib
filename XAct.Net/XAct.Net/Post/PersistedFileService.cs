﻿namespace XAct.Net.Post
{
    using System;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Net.Messaging;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IPersistedFileService"/>
    /// contract to
    /// persist File Streams
    /// to a persistent store mechanism.
    /// </summary>
    public class PersistedFileService : IPersistedFileService
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;
        private readonly IDistributedIdService _distributedIdService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersistedFileService" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="distributedIdService">The distributed identifier service.</param>
        public PersistedFileService(IEnvironmentService environmentService, 
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService,
            IDistributedIdService distributedIdService
            )
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
            _distributedIdService = distributedIdService;
        }

        /// <summary>
        /// Persists the specified persisted file.
        /// </summary>
        /// <param name="persistedFile">The persisted file.</param>
        public void Persist(PersistedFile persistedFile)
        {


            // ReSharper disable RedundantTypeArgumentsOfMethod
            _repositoryService.PersistOnCommit<PersistedFile, Guid>(persistedFile, true);

            foreach (var persistedFileMetadata in persistedFile.Metadata)
            {
                // ReSharper disable RedundantTypeArgumentsOfMethod
                _repositoryService.PersistOnCommit<PersistedFileMetadata, Guid>(persistedFileMetadata, true);
            }

            // ReSharper restore RedundantTypeArgumentsOfMethod
        }




        /// <summary>
        /// Retrieves the specified organisation identifier.
        /// </summary>
        /// <param name="applicationTennantId">The organisation identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="includeMetadata">if set to <c>true</c>, search for any metadata to include.</param>
        /// <returns></returns>
        public PersistedFile Retrieve(Guid id, bool includeMetadata = false, Guid? applicationTennantId = null)
        {
            PersistedFile result;

            if (!applicationTennantId.HasValue)
            {

                if (!includeMetadata)
                {
                    result = _repositoryService.GetSingle<PersistedFile>(
                        f => (f.Id == id)
                        );
                    return result;
                }

                result = _repositoryService.GetSingle<PersistedFile>(
                    f => (f.Id == id),
                    new IncludeSpecification<PersistedFile>(p => p.Metadata)
                    );

                return result;
            }


            if (!includeMetadata)
            {
                result =
                    _repositoryService.GetSingle<PersistedFile>(
                        f => (f.ApplicationTennantId == applicationTennantId)
                             &&
                             (f.Id == id)
                        );
                return result;
            }
            result =
                _repositoryService.GetSingle<PersistedFile>(
                    f => (f.ApplicationTennantId == applicationTennantId)
                         &&
                         (f.Id == id),
                    new IncludeSpecification<PersistedFile>(p => p.Metadata)
                    );

            return result;
        }



        /// <summary>
        /// Updates the specified  <see cref="PersistedFile" />.
        /// </summary>
        /// <param name="persistedFile">The persisted file.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Update(PersistedFile persistedFile)
        {

            // ReSharper disable RedundantTypeArgumentsOfMethod
            _repositoryService.UpdateOnCommit<PersistedFile>(persistedFile);
            // ReSharper restore RedundantTypeArgumentsOfMethod
        }


        /// <summary>
        /// Deletes the specified <see cref="PersistedFile"/> upon the next commit.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        public void Delete(Guid id, Guid? applicationTennantId = null)
        {
            if (!applicationTennantId.HasValue)
            {
                _repositoryService.DeleteOnCommit<PersistedFile>(
                    f => (f.Id == id)
                    );
                return;
            }
            _repositoryService.DeleteOnCommit<PersistedFile>(
                f => (f.ApplicationTennantId == applicationTennantId)
                     &&
                     (f.Id == id)
                );
        }
    }
}