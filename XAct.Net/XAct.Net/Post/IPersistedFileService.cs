﻿
namespace XAct.Net.Post
{
    using System;
    using XAct.Net.Messaging;

    /// <summary>
    /// Contract for a service to 
    /// persist File Streams
    /// to a persistent store mechanism.
    /// </summary>
    public interface IPersistedFileService : IHasXActLibService
    {
        /// <summary>
        /// Persists (saves or Updates) the <see cref="PersistedFile" /> object.
        /// <para>
        /// If an Id is not provided, a new Guid will be
        /// generated using <see cref="IDistributedIdService"/>.
        /// </para>
        /// </summary>
        /// <param name="persistedFile">The persisted file.</param>
        void Persist(PersistedFile persistedFile);


        /// <summary>
        /// Updates the specified  <see cref="PersistedFile"/>.
        /// </summary>
        /// <param name="persistedFile">The persisted file.</param>
        void Update(PersistedFile persistedFile);


        /// <summary>
        /// Retrieves the <see cref="PersistedFile"/>
        /// </summary>
        /// <param name="applicationTennantId">The organisation identifier.</param>
        /// <param name="id">The owner identifier.</param>
        /// <param name="includeMetadata">if set to <c>true</c>, search for any metadata to include.</param>
        PersistedFile Retrieve(Guid id, bool includeMetadata = false,Guid? applicationTennantId = null);


        

        
        /// <summary>
        /// Deletes the specified <see cref="PersistedFile"/> upon the next commit.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        void Delete(Guid id, Guid? applicationTennantId=null);

    }
}
