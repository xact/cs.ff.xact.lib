using System.Threading;

namespace XAct.Net.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Net.Messaging;
    using XAct.Net.Messaging.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class MessageDeliveryRepositoryUnitTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Monitor.Enter(ThreadLock.Lock);

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            ////Initialize the db using the common initializer passing it one entity from this class
            ////so that it can perform a query (therefore create db as necessary):
            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<Message>();

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();


        }
        [SetUp]
        public void TestSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            ////Initialize the db using the common initializer passing it one entity from this class
            ////so that it can perform a query (therefore create db as necessary):
            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<Message>();


        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            Monitor.Exit(ThreadLock.Lock);
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetRepositoryService()
        {
            IMessagingServiceRepositoryService  messagingServiceRepositoryService = XAct.DependencyResolver.Current.GetInstance<IMessagingServiceRepositoryService>();



            Assert.IsNotNull(messagingServiceRepositoryService);
        }

        [Test]
        public void CanGetRightRepositoryService()
        {
            IMessagingServiceRepositoryService messagingServiceRepositoryService = XAct.DependencyResolver.Current.GetInstance<IMessagingServiceRepositoryService>();



            Assert.IsTrue(messagingServiceRepositoryService is MessagingServiceRepositoryService);
        }



        [Test]
        public void CanGetMessage()
        {
            IMessagingServiceRepositoryService messagingServiceRepositoryService = XAct.DependencyResolver.Current.GetInstance<IMessagingServiceRepositoryService>();


            Guid newGuid = GetGuidOfMessage1CreatedByTestSeedData(1);

            Message message = messagingServiceRepositoryService.GetMessage(newGuid);

            Assert.IsNotNull(message);
        }

        private static Guid GetGuidOfMessage1CreatedByTestSeedData(int id)
        {
            string key = "MessageId_" + id;
            string s = System.Environment.GetEnvironmentVariable(key);
            if (s.IsNullOrEmpty())
            {
                s = Guid.NewGuid().ToString("D");
                System.Environment.SetEnvironmentVariable(key, s);
            }
            Guid newGuid = Guid.Parse(s);
            return newGuid;
        }
    }


}


