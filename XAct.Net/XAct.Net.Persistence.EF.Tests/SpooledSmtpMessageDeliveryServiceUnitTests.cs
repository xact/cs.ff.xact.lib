using System.Threading;
using XAct.Net.Tests;

namespace XAct.Net.Messaging.Tests
{
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Net.Messaging.Implementations;
    using XAct.Net.Messaging.Services.Configuration;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SpooledSmtpMessageDeliveryServiceUnitTests
    { 


        private bool _initialized = false;

        private bool _hasEnoughInfoFromRegistry = false;



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Monitor.Enter(ThreadLock.Lock);

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();



            _initialized = true;

            ISmtpMessageDeliveryServiceConfiguration smtpMessageDeliveryServiceConfiguration = 
                XAct.DependencyResolver.Current.GetInstance<ISmtpMessageDeliveryServiceConfiguration>();

            IMessagingServiceConfiguration messagingServiceConfiguration =
                XAct.DependencyResolver.Current.GetInstance<IMessagingServiceConfiguration>();

            messagingServiceConfiguration.SpoolMessagesForLaterDelivery=true;

            


            //I didn't want to leave a password in my unit tests, so am using an EnvironmentVariable to set them up
            //in the Continuous Environment.
            //Format of the EnvironmentVariable is: 
            
            //"Enabled:true;ServerName:smtp.gmail.com;Port:465;FromAddress:skysigal@xact-solutions.com;Ssl:true;UserName:skysigal@xact-solutions.com;Password:*******"

            string variablesFromEnvironment = System.Environment.GetEnvironmentVariable("XACTLIB.SMTP",
                                                                                     EnvironmentVariableTarget.User);
            if (variablesFromEnvironment.IsNullOrEmpty())
            {
                variablesFromEnvironment = System.Environment.GetEnvironmentVariable("XACTLIB.SMTP",
                                                                                     EnvironmentVariableTarget.Machine);
            }


            //ServerName:
            if (!variablesFromEnvironment.IsNullOrEmpty())
            {
                _hasEnoughInfoFromRegistry = true;

                Dictionary<string, string> dictionary = variablesFromEnvironment.SplitIntoKeyValues(true);

                string key;

                key = "Enabled";
                if (dictionary.ContainsKey(key))
                {
                    smtpMessageDeliveryServiceConfiguration.Enabled = dictionary[key].ToBool();
                }

                key = "ServerName";
                if (dictionary.ContainsKey(key))
                {
                    smtpMessageDeliveryServiceConfiguration.ServerName = dictionary[key];
                }

                key = "Port";
                if (dictionary.ContainsKey(key))
                {
                    smtpMessageDeliveryServiceConfiguration.Port = dictionary[key].ConvertTo<int>();
                }

                key = "FromAddress";
                if (dictionary.ContainsKey(key))
                {
                    smtpMessageDeliveryServiceConfiguration.FromAddress = new MessageAddress(dictionary[key]);
                }

                key = "Ssl";
                if (dictionary.ContainsKey(key))
                {
                    smtpMessageDeliveryServiceConfiguration.Ssl = dictionary[key].ToBool();
                }

                key = "Username";
                if (dictionary.ContainsKey(key))
                {
                    smtpMessageDeliveryServiceConfiguration.UserName = dictionary[key];
                }

                key = "Password";
                if (dictionary.ContainsKey(key))
                {
                    smtpMessageDeliveryServiceConfiguration.Password = dictionary[key];
                }

                //If using SMTP Fake
                //smtpMessageDeliveryServiceConfiguration.ServerName = "localhost";
                //smtpMessageDeliveryServiceConfiguration.Port = 25;

            }
        }

        [SetUp]
        public void TestSetUp()
        {
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            Monitor.Exit(ThreadLock.Lock);

        }

        [Test]
        public void TestClassInitializedCorrectly()
        {
            //2.1.505
            //Current:2.6.2.12296
            Assert.IsTrue(_initialized);
        }
        [Test]
        public void CanGetSmtpService()
        {
            SmtpMessageDeliveryService smtpService = new SmtpMessageDeliveryService(null, null, null);
            
            Assert.IsNotNull(smtpService);
        }

        [Test]
        public void CanGetSmtpServiceByServiceLocator()
        {
            XAct.Net.Messaging.ISmtpMessageDeliveryService smtpService = XAct.DependencyResolver.Current.GetInstance<ISmtpMessageDeliveryService>();

            Assert.IsNotNull(smtpService);
        }


        [Test]
        public void CanGetMessagingService()
        {
            XAct.Net.Messaging.IMessagingService messagingService = XAct.DependencyResolver.Current.GetInstance<IMessagingService>();

            Assert.IsNotNull(messagingService);
        }


        [Test]
        public void CanGetMessagePrioritizedProcessingService()
        {
            XAct.Net.Messaging.IMessagePrioritizedProcessingService messagePrioritizedProcessingService
                = XAct.DependencyResolver.Current.GetInstance<IMessagePrioritizedProcessingService>();

            Assert.IsNotNull(messagePrioritizedProcessingService);
        }


        [Test]
        public void CanRetrievePrioritizedMessages()
        {
            XAct.Net.Messaging.IMessagingServiceRepositoryService messagingServiceRepositoryService
                = XAct.DependencyResolver.Current.GetInstance<IMessagingServiceRepositoryService>();

            Message[] messages = messagingServiceRepositoryService.GetMessagesByPriority(MessagePriority.Low, 0,20);


            //Ensure the service has polled Repository at least once during this time:
            Assert.IsTrue(messages.Length > 0);
        }

        
        
        [Test]
        public void CanSeeMessagePrioritizedProcessingService()
        {
            XAct.Net.Messaging.IMessagePrioritizedProcessingService messagePrioritizedProcessingService
                = XAct.DependencyResolver.Current.GetInstance<IMessagePrioritizedProcessingService>();


            System.Threading.Thread.Sleep(3000);

            if (messagePrioritizedProcessingService.IsPolling)
            {
                messagePrioritizedProcessingService.Stop();
            }


            //As it is not running, it is not polling, so it should be 0 (although it is possible some late running threads may update it).
            Assert.IsTrue(messagePrioritizedProcessingService.Polled == 0);
        }




        [Test]
        public void CanSeeMessagePrioritizedProcessingService2()
        {
            XAct.Net.Messaging.IMessagePrioritizedProcessingService messagePrioritizedProcessingService
                = XAct.DependencyResolver.Current.GetInstance<IMessagePrioritizedProcessingService>();

            XAct.Net.Messaging.IMessagingServiceRepositoryService repositoryService
    = XAct.DependencyResolver.Current.GetInstance<IMessagingServiceRepositoryService>();

            //Ensure repo is fully built before putting things to sleep:
#pragma warning disable 168
            Message m = repositoryService.GetMessage(Guid.Empty);
#pragma warning restore 168


            messagePrioritizedProcessingService.Start();

            System.Threading.Thread.Sleep(3000);

            //Ensure the service has polled Repository at least once during this time:
            Assert.IsTrue(messagePrioritizedProcessingService.Polled > 0);
        }


        [Test]
        public void CanSeeMessagePrioritizedProcessingService3()
        {
            XAct.Net.Messaging.IMessagePrioritizedProcessingService messagePrioritizedProcessingService
                = XAct.DependencyResolver.Current.GetInstance<IMessagePrioritizedProcessingService>();

            XAct.Net.Messaging.IMessagingServiceRepositoryService repositoryService
    = XAct.DependencyResolver.Current.GetInstance<IMessagingServiceRepositoryService>();

            //Ensure repo is fully built before putting things to sleep:
#pragma warning disable 168
            Message m = repositoryService.GetMessage(Guid.Empty);
#pragma warning restore 168


            messagePrioritizedProcessingService.Start();

            System.Threading.Thread.Sleep(5000);

            //Ensure the service has polled Repository at least once during this time:
            Assert.IsTrue(messagePrioritizedProcessingService.Polled > 0);

            

        }


        [Test]
        public void SendMessage()
        {
            XAct.Net.Messaging.ISmtpMessageDeliveryService smtpService = XAct.DependencyResolver.Current.GetInstance<ISmtpMessageDeliveryService>();


            Message message = MessageFactory.CreateMessage1();

            try
            {
                smtpService.SendMessage(message);
            }
            catch
            {
                if (_hasEnoughInfoFromRegistry)
                {

                    throw;
                }
            }
        }


        [Test]
        public void SendMessageViaIMessagingService()
        {
            XAct.Net.Messaging.IMessagingService smtpService = XAct.DependencyResolver.Current.GetInstance<IMessagingService>();


            Message message = MessageFactory.CreateMessage1();

            try
            {
                smtpService.SendMessage(message);
            }
            catch
            {
                if (_hasEnoughInfoFromRegistry)
                {

                    throw;
                }
            }
        }





        private static Guid GetGuidOfMessage1CreatedByTestSeedData()
        {
            string s = System.Environment.GetEnvironmentVariable("Message1Id");
            if (s.IsNullOrEmpty())
            {
                s = Guid.NewGuid().ToString("D");
                System.Environment.SetEnvironmentVariable("Message1Id", s);
            }
            Guid newGuid = Guid.Parse(s);
            return newGuid;
        }

    }

//Class:End
}

//Namespace:End