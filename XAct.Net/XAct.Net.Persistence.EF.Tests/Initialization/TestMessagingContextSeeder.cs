﻿namespace XAct.Net.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Net.Initialization.DbContextSeeders;
    using XAct.Net.Messaging;
    using XAct.Net.Messaging.Tests;
    using XAct.Services;

    /// <summary>
    /// A default implementation of the <see cref="IMessageDbContextSeeder"/> contract
    /// to seed the Messaging tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class MessageDbContextSeeder : XActLibDbContextSeederBase<Message>, IMessageDbContextSeeder,
                                          IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public MessageDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.MessageId);
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {
            this.InternalEntities = new List<Message>();

            Message message;

            message = MessageFactory.CreateMessage4();
            message.SetIds(GetGuidOfMessage1CreatedByTestSeedData(1));
            this.InternalEntities.Add(message);

            message = MessageFactory.CreateMessage4();
            message.SetIds(GetGuidOfMessage1CreatedByTestSeedData(2));
            this.InternalEntities.Add(message);


        }



        private static Guid GetGuidOfMessage1CreatedByTestSeedData(int id)
        {
            string key = "MessageId_" + id;
            string s = System.Environment.GetEnvironmentVariable(key);
            if (s.IsNullOrEmpty())
            {
                s = Guid.NewGuid().ToString("D");
                System.Environment.SetEnvironmentVariable(key, s);
            }
            Guid newGuid = Guid.Parse(s);
            return newGuid;
        }


    }
}
