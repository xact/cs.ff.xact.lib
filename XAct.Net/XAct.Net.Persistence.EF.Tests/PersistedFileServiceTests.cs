﻿using System.Threading;
using XAct.Environment;

namespace XAct.Net.Tests.UnitTests
{
    using System;
    using System.IO;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.IO;
    using XAct.Net.Messaging;
    using XAct.Net.Post;
    using XAct.Tests;
    //using XAct.Extensions;

    [TestFixture]
    class PersistedFileServiceTests
    {
        private readonly string[] _testfileRelativePaths = new string[]
            {
                "TestMaterial\\SomeTextFile.txt",
                "TestMaterial\\skull.png",
                "TestMaterial\\skull.jpg"
            };


        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Monitor.Enter(ThreadLock.Lock);

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        [SetUp]
        public void TestSetUp()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            Monitor.Exit(ThreadLock.Lock);

        }
        
        [Test]
        public void CanGetPersistedFileService()
        {
            IPersistedFileService persistedFileService =
                XAct.DependencyResolver.Current.GetInstance<IPersistedFileService>();

            Assert.IsNotNull(persistedFileService);
        }



        [Test]
        public void CanGetPersistedFileServiceOfExpectedType()
        {
            IPersistedFileService persistedFileService =
                XAct.DependencyResolver.Current.GetInstance<IPersistedFileService>();

            Assert.AreEqual(typeof(PersistedFileService), persistedFileService.GetType());
        }


        [Test]
        public void CanFindTestFile()
        {
            //The following tests rely on finding 3 or files in a directory
            //under the basedirectory ('/TestMaterial'):
            Assert.IsTrue(File.Exists(_testfileRelativePaths[0]));
        }

        [Test]
        public void CanGetInstanceOfRequiredFileSystemIOService()
        {
            //The service relies on the use of file system:
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            Assert.IsNotNull(fsioService);
        }


        [Test]
        public void LoadASingleTestTextFile()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            //Load the file from the hard drive into the Persisted file:
            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);

            //For the following tests, to have something to compare against, 
            //we're going to go and get the file directly.
            var fileInfo =
                fsioService.GetFileInfoAsync(_testfileRelativePaths[0]).WaitAndGetResult();
            //Also need some path service:
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            //Compare the two:
            Assert.IsTrue(persistedFile.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), persistedFile.Name, "Name Test");
            Assert.AreEqual("text/plain", persistedFile.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile.Description, "Description Test");
            Assert.IsTrue(persistedFile.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile.Value, "Value Test");
            Assert.IsTrue(persistedFile.Value.Length > 0, "Value Test");
        }


    //    [Test]
    //    public void PersistASingleTestTextFileWithoutCommitting()
    //    {
    //        IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

    //        //Load one file:
    //        PersistedFile persistedFile = new PersistedFile();
    //        persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);


    //        IPersistedFileService persistedFileService =
    //XAct.DependencyResolver.Current.GetInstance<IPersistedFileService>();

    //        //Persist, but don't save it to hard drive:
    //        persistedFileService.Persist(persistedFile);

    //        //Did we survive?
    //        Assert.IsTrue(true);
    //    }



        [Test]
        public void PersistASingleTestTextFileAndCommit()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);

            IPersistedFileService persistedFileService =
                XAct.DependencyResolver.Current.GetInstance<IPersistedFileService>();


            persistedFileService.Persist(persistedFile);

            //Same as previous test, but this time, commit it to hard drive:
            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            repositoryService.GetContext().Commit(CommitType.Default);

            //Did we survive?
            Assert.IsTrue(true);
        }


        [Test]
        public void PersistASingleTestTextFileAndCommitThenRetrieve()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);

            
            IFileInfo fileInfo = fsioService.GetFileInfoAsync(_testfileRelativePaths[0]).WaitAndGetResult();

            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            IPersistedFileService persistedFileService =
    XAct.DependencyResolver.Current.GetInstance<IPersistedFileService>();


            persistedFileService.Persist(persistedFile);

            //IUnitOfWorkService unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
            //unitOfWorkService.GetCurrent();


            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            repositoryService.GetContext().Commit(CommitType.Default);

            //persistedFile.Id

            PersistedFile roundTripped = persistedFileService.Retrieve(persistedFile.Id);


            Assert.IsTrue(roundTripped.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), roundTripped.Name, "Name Test");
            Assert.AreEqual("text/plain", roundTripped.ContentType, "ContentType Test");
            Assert.IsFalse(roundTripped.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(roundTripped.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, roundTripped.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, roundTripped.Description, "Description Test");
            Assert.IsTrue(roundTripped.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, roundTripped.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, roundTripped.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, roundTripped.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, roundTripped.Tag, "Tag Test");
            Assert.IsNotNull(roundTripped.Value, "Value Test");
            Assert.IsTrue(roundTripped.Value.Length > 0, "Value Test");


            //Did we survive?
            Assert.IsTrue(true);
        }

        [Test]
        public void PersistASingleTestTextFileWithMetaAndCommitThenRetrieve()
        {
            //Same test as previous, but attaching some information to the file:

            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);


            IFileInfo fileInfo = fsioService.GetFileInfoAsync(_testfileRelativePaths[0]).WaitAndGetResult();

            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            IPersistedFileService persistedFileService =
    XAct.DependencyResolver.Current.GetInstance<IPersistedFileService>();

            var now = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC;
            persistedFile.Metadata.Add(new PersistedFileMetadata{ Key="ABA",Value="BABBA", CreatedOnUtc =now,CreatedBy = "sls", LastModifiedOnUtc = now,LastModifiedBy = "sls"});
            persistedFile.Metadata.Add(new PersistedFileMetadata { Key = "BABA", Value = "BABBA2", CreatedOnUtc = now, CreatedBy = "sls", LastModifiedOnUtc = now, LastModifiedBy = "sls" });

            persistedFileService.Persist(persistedFile);

            IRepositoryService repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            repositoryService.GetContext().Commit(CommitType.Default);


            //Retrieve the file again:
            PersistedFile roundTripped = persistedFileService.Retrieve(persistedFile.Id);

            //And compare the two:
            Assert.IsTrue(roundTripped.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), roundTripped.Name, "Name Test");
            Assert.AreEqual("text/plain", roundTripped.ContentType, "ContentType Test");
            Assert.IsFalse(roundTripped.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(roundTripped.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, roundTripped.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, roundTripped.Description, "Description Test");
            Assert.IsTrue(roundTripped.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, roundTripped.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, roundTripped.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, roundTripped.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, roundTripped.Tag, "Tag Test");
            Assert.IsNotNull(roundTripped.Value, "Value Test");
            Assert.IsTrue(roundTripped.Value.Length > 0, "Value Test");


            Assert.IsTrue(roundTripped.Metadata.Count>1,"Has Metadata");
            Assert.AreEqual("ABA", roundTripped.Metadata.First().Key, "Has Metadata");
            Assert.AreEqual("BABBA", roundTripped.Metadata.First().Value, "Has Metadata");

        }


        //[Test]
        //public void TestThePersistedPersistedFileMetadataObjectCanBeSerializableAcrossWCFWire()
        //{
        //    PersistedFileMetadata persistedFileMetadata = new PersistedFileMetadata();
        //    persistedFileMetadata.Id = Guid.NewGuid();
        //    persistedFileMetadata.CreatedBy = "skys";
        //    persistedFileMetadata.CreatedOn = DateTime.Now;
        //    persistedFileMetadata.Key = "ABA";
        //    persistedFileMetadata.LastModifiedBy = "skys";
        //    persistedFileMetadata.LastModifiedOn = DateTime.Now;
        //    persistedFileMetadata.OwnerFK = Guid.NewGuid();
        //    persistedFileMetadata.Tag = "blah";


        //    string serializeddata = 
        //        XAct.Extensions.
        //        persistedFileMetadata.ToDataContractString<PersistedFileMetadata>();

        //    PersistedFileMetadata persistedFileMetadata2 =
        //        serializeddata.DeserializeFromDataContractSerializedString<PersistedFileMetadata>();

        //    Assert.AreEqual(persistedFileMetadata.Id, persistedFileMetadata2.Id);
        //    Assert.AreEqual(persistedFileMetadata.CreatedBy, persistedFileMetadata2.CreatedBy);
        //    Assert.AreEqual(persistedFileMetadata.CreatedOn, persistedFileMetadata2.CreatedOn);
        //    Assert.AreEqual(persistedFileMetadata.Key, persistedFileMetadata2.Key);
        //    Assert.AreEqual(persistedFileMetadata.LastModifiedBy, persistedFileMetadata2.LastModifiedBy);
        //    Assert.AreEqual(persistedFileMetadata.LastModifiedOn, persistedFileMetadata2.LastModifiedOn);
        //    Assert.AreEqual(persistedFileMetadata.OwnerFK, persistedFileMetadata2.OwnerFK);
        //    Assert.AreEqual(persistedFileMetadata.Tag, persistedFileMetadata2.Tag);
        //}

        //[Test]
        //public void TestThePersistedPersistedFileObjectCanBeSerializableAcrossWCFWire()
        //{
        //    var g = Guid.NewGuid();
        //    var i = Guid.NewGuid();
        //    PersistedFile persistedFile = new PersistedFile();
        //    persistedFile.ApplicationTennantId = g;
        //    persistedFile.ContentId = "abc";
        //    persistedFile.ContentType = "test/test";
        //    persistedFile.CreatedOn = DateTime.Now;
        //    persistedFile.DeletedOn = DateTime.Now;
        //    persistedFile.Description = "...";
        //    persistedFile.Id = i;
        //    persistedFile.LastModifiedOn = DateTime.Now;
        //    persistedFile.Name = "abc";
        //    //persistedFile.OwnerFK = g;
        //    persistedFile.Tag = "abc";
        //    persistedFile.Value = new byte[0] {};

        //    string serializeddata = persistedFile.ToDataContractString<PersistedFile>();

        //    PersistedFile  persistedFile2 =
        //        serializeddata.DeserializeFromDataContractSerializedString<PersistedFile>();

        //    Assert.AreEqual(persistedFile.ApplicationTennantId, persistedFile2.ApplicationTennantId);
        //    Assert.AreEqual(persistedFile.ContentId, persistedFile2.ContentId);
        //    Assert.AreEqual(persistedFile.ContentType, persistedFile2.ContentType);
        //    Assert.AreEqual(persistedFile.CreatedOn, persistedFile2.CreatedOn);
        //    Assert.AreEqual(persistedFile.DeletedOn, persistedFile2.DeletedOn);
        //    Assert.AreEqual(persistedFile.Description, persistedFile2.Description);
        //    Assert.AreEqual(persistedFile.Id, persistedFile2.Id);
        //    Assert.AreEqual(persistedFile.LastModifiedOn, persistedFile2.LastModifiedOn);
        //    Assert.AreEqual(persistedFile.Name, persistedFile2.Name);
        //    //Assert.AreEqual(persistedFile.OwnerFK, persistedFile2.OwnerFK);
        //    Assert.AreEqual(persistedFile.Tag, persistedFile2.Tag);
        //    Assert.AreEqual(persistedFile.Value, persistedFile2.Value);
        //}
    }
}
