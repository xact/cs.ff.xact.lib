﻿namespace XAct.Net.Messaging.Tests
{
    using System;
    using System.IO;
    using System.Linq;
    using XAct.IO;

    public static class MessageFactory
    {
        private static string testTargetEmail = "skysigal@xact-solutions.com";




        public static Message FactoryMessage0()
        {
            Message message = new Message();

            message.AddHeader(new MessageHeader("SomeHeader", "SomeValue"));
            message.AddHeader(new MessageHeader("AnotherHeader", "SomeOtherValue"));

            message.AddTo(new MessageAddress("a@b.com", "Abbot C."));
            message.AddTo(new MessageAddress("a@b2.com", "Abbot C. #2"));

            message.AddCc(new MessageAddress("a@cc.com", "CC Guy"));
            message.AddCc(new MessageAddress("a@cc2.com", "CC Guy #2"));

            message.AddBcc(new MessageAddress("a@bcc.com", "BCC Guy"));
            message.AddBcc(new MessageAddress("a@bcc2.com", "BCC Guy #2"));


            //Logged is required to show that it came through repo.Save method.
            message.Status = MessageDeliveryStatus.Logged;


            message.Importance = MessageImportance.Important;
            message.Priority = MessagePriority.Urgent;

            message.From = new MessageAddress("abc@from.com");
            message.ReplyTo = new MessageAddress("abc@replyto.com");
            message.Subject = "wada wada wada";

            message.AddATextBody(new MessageBody(MessageBodyType.Text, "This is a text body."));
            message.AddAnHtmlBody(new MessageBody(MessageBodyType.Html, "This is an html body."));


            string tmpFileName = Path.Combine(Path.GetTempPath(), string.Format("SmptTestFile_{0}.png", Guid.NewGuid().ToString("D")));
            System.IO.File.WriteAllText(tmpFileName, "This is a test file...");

            MessageAttachment messageAttachment = new MessageAttachment();

            //Use Extension Method
            messageAttachment.LoadFromFile(tmpFileName, XAct.DependencyResolver.Current.GetInstance<IFSIOService>());


            message.AddAttachment(MessageAttachmentType.Inline, messageAttachment);


            tmpFileName = Path.Combine(Path.GetTempPath(), string.Format("SmptTestFile_{0}.png", Guid.NewGuid().ToString("D")));
            System.IO.File.WriteAllText(tmpFileName, "This is a test file...");

            messageAttachment = new MessageAttachment();

            //Use Extension Method
            messageAttachment.LoadFromFile(tmpFileName,XAct.DependencyResolver.Current.GetInstance<IFSIOService>());


            message.AddAttachment(MessageAttachmentType.Attachment, messageAttachment);

            return message;
        }


        public static Message CreateMessage1()
        {
            Message message = new Message();

            message.AddHeader(new MessageHeader("X-SomeHeader", "SomeValue"));
            message.AddHeader(new MessageHeader("X-AnotherHeader", "SomeOtherValue"));

            message.AddTo(new MessageAddress(testTargetEmail, "Sky Boy"));

            message.AddCc(new MessageAddress(testTargetEmail, "Sky Boy CC"));
            message.AddBcc(new MessageAddress(testTargetEmail, "Sky Boy BCC"));

            //Logged is required to show that it came through repo.Save method.
            message.Status = MessageDeliveryStatus.Logged;

            message.Importance = MessageImportance.Important;
            message.Priority = MessagePriority.Urgent;

            message.From = new MessageAddress(testTargetEmail);
            message.ReplyTo = new MessageAddress(testTargetEmail);

            message.Subject = "wada wada wada";

            message.AddATextBody(new MessageBody(MessageBodyType.Text, "This is a text body."));
            message.AddAnHtmlBody(new MessageBody(MessageBodyType.Html, "This is an <b>html</b> body."));



            return message;
            
        }

        public static Message CreateMessage2()
        {
            Message message = MessageFactory.CreateMessage1();

            
            string text = @"Il parle très bien français. - “He speaks French very well.”
Elle a fait beaucoup d'efforts pour améliorer son français. - “She made a lot of effort to improve her French.”";
            message.Bodies.Single(s => s.Type == MessageBodyType.Text).Value = text;

            return message;
        }

        public static Message CreateMessage3()
        {
            Message message = MessageFactory.CreateMessage1();


            string text = @"Il parle très bien français. - “He speaks French very well.”
Elle a fait beaucoup d'efforts pour améliorer son français. - “She made a lot of effort to improve her French.”";
            message.Bodies.Single(s => s.Type == MessageBodyType.Html).Value = text;

            return message;
        }


        public static Message CreateMessage4()
        {


            Message message = MessageFactory.CreateMessage1();


            string tmpFileName =
                XAct.DependencyResolver.Current.GetInstance<IRequestService>()
                    .DownloadRemoteFile("https://dl.dropboxusercontent.com/u/11851202/skys/Posted/Resources/separator.png");


            message.Subject = "French Html Msg w/ Inline Image";

            string text = @"
<h4>Some Non-English examples</h4>
Il parle <b>très</b> bien français...<br/>
加入营销计划‎隐私权和使用条款‎<br/>
Arabic (العربية al-ʻarabīyah [alʕaraˈbijja] ( listen) or عربي/عربى<br/>

<h4>Some Embedded Images</h4>
Image:
<img src=""cid:xyz""/>
<br/>

<h4>Some Attachments Images</h4>
";
            message.Bodies.Single(s => s.Type == MessageBodyType.Html).Value = text;

            MessageAttachment messageAttachment = MessageAttachment.FromFile(tmpFileName);
            messageAttachment.ContentId = "xyz";
            messageAttachment.Type = MessageAttachmentType.Inline;
            message.Attachments.Add(messageAttachment);

            MessageAttachment messageAttachment2 = MessageAttachment.FromFile(tmpFileName);
            message.Attachments.Add(messageAttachment2);

            return message;
        }



    }
}
