namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Net;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class MimeServiceUnitTests
    {
        private bool _initialized=false;
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();
            _initialized = true;
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void TestClassInitializedCorrectly()
        
        {
            //2.1.505
            //Current:2.6.2.12296
            Assert.IsTrue(_initialized);
        }

        [Test]
        public void CanGetMimeService()
        {
            XAct.Net.IMimeTypeService mimeService = XAct.DependencyResolver.Current.GetInstance<IMimeTypeService>();

            Assert.IsNotNull(mimeService);
        }

        
        [Test]
        public void MimeServiceReturnsTextPlainForTextFile()
        {
            XAct.Net.IMimeTypeService mimeService = XAct.DependencyResolver.Current.GetInstance<IMimeTypeService>();

            string mimeTime = mimeService.GetMimeTypeFromFileExtension(".txt");

            Assert.AreEqual("text/plain", mimeTime);
        }

        [Test]
        public void MimeServiceReturnsImgGifForGifFile()
        {
            XAct.Net.IMimeTypeService mimeService = XAct.DependencyResolver.Current.GetInstance<IMimeTypeService>();

            string mimeTime = mimeService.GetMimeTypeFromFileExtension(".gif");

            Assert.AreEqual("image/gif", mimeTime);
        }
        [Test]
        public void MimeServiceReturnsImgGifForPngFile()
        {
            XAct.Net.IMimeTypeService mimeService = XAct.DependencyResolver.Current.GetInstance<IMimeTypeService>();

            string mimeTime = mimeService.GetMimeTypeFromFileExtension(".png");

            Assert.AreEqual("image/png", mimeTime);
        }
        [Test]
        public void MimeServiceReturnsImgJpegForJpgFile()
        {
            XAct.Net.IMimeTypeService mimeService = XAct.DependencyResolver.Current.GetInstance<IMimeTypeService>();

            string mimeTime = mimeService.GetMimeTypeFromFileExtension(".jpg");

            Assert.AreEqual("image/jpeg", mimeTime);
        }

        [Test]
        public void MimeServiceReturnsLongFormatForDocxFile()
        {
            XAct.Net.IMimeTypeService mimeService = XAct.DependencyResolver.Current.GetInstance<IMimeTypeService>();

            string mimeTime = mimeService.GetMimeTypeFromFileExtension(".docx");

            Assert.AreEqual("application/vnd.openxmlformats-officedocument.wordprocessingml.document", mimeTime);
        }

        
    }


}


