﻿namespace XAct.Net.Tests.UnitTests
{
    using System;
    using NUnit.Framework;
    using XAct.Net.Post;
    using XAct.Tests;

    [TestFixture]
    class PersistedFileServiceTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIMessagingDeliveryService()
        {
            IPersistedFileService persistedFileService =
                XAct.DependencyResolver.Current.GetInstance<IPersistedFileService>();

            Assert.IsNotNull(persistedFileService);
        }

        [Test]
        public void ServiceIsOfExpectedType()
        {
            IPersistedFileService persistedFileService =
                XAct.DependencyResolver.Current.GetInstance<IPersistedFileService>();

            Assert.AreEqual(typeof(PersistedFileService),persistedFileService.GetType());
        }

        //Can't go any further than this, as the tests would need the Db to be initialized.

    }
}
