﻿namespace XAct.Net.Tests.UnitTests
{
    using System;
    using System.IO;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Extensions;
    using XAct.IO;
    using XAct.Net.Messaging;
    using XAct.Tests;

    [TestFixture]
    class PersistedFileTests
    {
        private readonly string[] _testfileRelativePaths = new string[]
            {
                "TestMaterial\\SomeTextFile.txt",
                "TestMaterial\\skull.png",
                "TestMaterial\\skull.jpg"
            };


        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanFindTestFile()
        {


            Assert.IsTrue(File.Exists(_testfileRelativePaths[0]));
        }
        [Test]
        public void CanGetInstanceOfRequiredFileSystemIOService()
        {

            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();


            Assert.IsNotNull(fsioService);
        }

        [Test]
        public void LoadASingleTestTextFile()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);

            var fileInfo =
                fsioService.GetFileInfoAsync(_testfileRelativePaths[0]).WaitAndGetResult();
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            Assert.IsTrue(persistedFile.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), persistedFile.Name, "Name Test");
            Assert.AreEqual("text/plain", persistedFile.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile.Description, "Description Test");
            Assert.IsTrue(persistedFile.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile.Value, "Value Test");
            Assert.IsTrue(persistedFile.Value.Length > 0, "Value Test");
        }

        [Test]
        public void LoadASingleTestImagePngFile()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[1], fsioService);

            var fileInfo =
                fsioService.GetFileInfoAsync(_testfileRelativePaths[1]).WaitAndGetResult();
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            Assert.IsTrue(persistedFile.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[1]), persistedFile.Name, "Name Test");
            Assert.AreEqual("image/png", persistedFile.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile.Description, "Description Test");
            Assert.IsTrue(persistedFile.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile.Value, "Value Test");
            Assert.IsTrue(persistedFile.Value.Length > 0, "Value Test");
        }


        [Test]
        public void LoadASingleTestFileAndSerializeAsAcrossWCFTier()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);

            var fileInfo =
                fsioService.GetFileInfoAsync(_testfileRelativePaths[0]).WaitAndGetResult();
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            Assert.IsTrue(persistedFile.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), persistedFile.Name, "Name Test");
            Assert.AreEqual("text/plain", persistedFile.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile.Description, "Description Test");
            Assert.IsTrue(persistedFile.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile.Value, "Value Test");
            Assert.IsTrue(persistedFile.Value.Length > 0, "Value Test");


            //Let's try roundtripping it across WCF

            string serializedText = persistedFile.ToDataContractString<PersistedFile>();

            PersistedFile persistedFile2 = serializedText.DeserializeFromDataContractSerializedString<PersistedFile>();

            Assert.IsTrue(persistedFile2.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), persistedFile2.Name, "Name Test");
            Assert.AreEqual("text/plain", persistedFile2.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile2.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile2.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile2.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile2.Description, "Description Test");
            Assert.IsTrue(persistedFile2.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile2.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile2.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile2.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile2.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile2.Value, "Value Test");
            Assert.IsTrue(persistedFile2.Value.Length > 0, "Value Test");


        }



        [Test]
        public void LoadASingleTestTextFileWithMetaDataAndSerializeAsAcrossWCFTier()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);

            var fileInfo =
                fsioService.GetFileInfoAsync(_testfileRelativePaths[0]).WaitAndGetResult();
            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            Assert.IsTrue(persistedFile.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), persistedFile.Name, "Name Test");
            Assert.AreEqual("text/plain", persistedFile.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile.Description, "Description Test");
            Assert.IsTrue(persistedFile.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile.Value, "Value Test");
            Assert.IsTrue(persistedFile.Value.Length > 0, "Value Test");


            var meta = new PersistedFileMetadata
                {
                    Id=Guid.NewGuid(),
                    OwnerFK = persistedFile.Id,
                    Key = "SomeKeyA",
                    Value="abba",
                    CreatedOnUtc = DateTime.UtcNow,
                    CreatedBy = "John",
                    LastModifiedBy = "john",
                    LastModifiedOnUtc = DateTime.UtcNow
                };

            persistedFile.Metadata.Add(meta);
            persistedFile.Metadata.Add(meta);
            persistedFile.Metadata.Add(meta);

            //Let's try roundtripping it across WCF

            string serializedText = persistedFile.ToDataContractString<PersistedFile>();

            PersistedFile persistedFile2 = serializedText.DeserializeFromDataContractSerializedString<PersistedFile>();

            Assert.IsTrue(persistedFile2.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), persistedFile2.Name, "Name Test");
            Assert.AreEqual("text/plain", persistedFile2.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile2.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile2.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile2.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile2.Description, "Description Test");
            Assert.IsTrue(persistedFile2.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile2.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile2.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile2.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile2.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile2.Value, "Value Test");
            Assert.IsTrue(persistedFile2.Value.Length > 0, "Value Test");


            Assert.IsTrue(persistedFile2.Metadata.Count>0);
            var meta2 = persistedFile2.Metadata.First();

            Assert.AreEqual(meta.Id, meta2.Id);
            Assert.AreEqual(meta.OwnerFK, meta2.OwnerFK);
            Assert.AreEqual(meta.Key, meta2.Key);
            Assert.AreEqual(meta.Value, meta2.Value);
            Assert.AreEqual(meta.CreatedBy, meta2.CreatedBy);
            Assert.AreEqual(meta.CreatedOnUtc, meta2.CreatedOnUtc);
            Assert.AreEqual(meta.LastModifiedBy, meta2.LastModifiedBy);
            Assert.AreEqual(meta.LastModifiedOnUtc, meta2.LastModifiedOnUtc);
            

        }



        [Test]
        public void LoadASingleTestTextFileWithMetaDataAndSerializeAsAcrossWCFTierAndReadContent()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();

            PersistedFile persistedFile = new PersistedFile();
            persistedFile.LoadFromFile(_testfileRelativePaths[0], fsioService);

            var fileInfo =
                fsioService.GetFileInfoAsync(_testfileRelativePaths[0]).WaitAndGetResult();

            string fileContent =
                fsioService.FileOpenAsync(
                    fileInfo.FullName,
                    IO.FileMode.Open,
                    IO.FileAccess.Read,
                    IO.FileShare.Read)
                           .WaitAndGetResult().ReadToEnd();


            IPathService pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

            Assert.IsTrue(persistedFile.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), persistedFile.Name, "Name Test");
            Assert.AreEqual("text/plain", persistedFile.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile.Description, "Description Test");
            Assert.IsTrue(persistedFile.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile.Value, "Value Test");
            Assert.IsTrue(persistedFile.Value.Length > 0, "Value Test");


            var meta = new PersistedFileMetadata
            {
                Id = Guid.NewGuid(),
                OwnerFK = persistedFile.Id,
                Key = "SomeKeyA",
                Value = "abba",
                CreatedOnUtc = DateTime.UtcNow,
                CreatedBy = "John",
                LastModifiedBy = "john",
                LastModifiedOnUtc = DateTime.UtcNow
            };
            persistedFile.Metadata.Add(meta);
            persistedFile.Metadata.Add(meta);
            persistedFile.Metadata.Add(meta);

            //Let's try roundtripping it across WCF

            string serializedText = persistedFile.ToDataContractString<PersistedFile>();

            PersistedFile persistedFile2 = serializedText.DeserializeFromDataContractSerializedString<PersistedFile>();

            Assert.IsTrue(persistedFile2.Size > 0, "Size Test");
            Assert.AreEqual(pathService.GetFileName(_testfileRelativePaths[0]), persistedFile2.Name, "Name Test");
            Assert.AreEqual("text/plain", persistedFile2.ContentType, "ContentType Test");
            Assert.IsFalse(persistedFile2.DeletedOnUtc.HasValue, "DeletedOn Test");
            Assert.IsTrue(persistedFile2.CreatedOnUtc.HasValue, "CreatedOn Test");
            Assert.AreEqual(fileInfo.CreatedOnUtc, persistedFile2.CreatedOnUtc, "CreatedOn Test");
            Assert.AreEqual(null, persistedFile2.Description, "Description Test");
            Assert.IsTrue(persistedFile2.LastModifiedOnUtc.HasValue, "LastModifiedOn Test");
            Assert.AreEqual(fileInfo.LastModifiedOnUtc, persistedFile2.LastModifiedOnUtc, "ModifiedOn Test");
            //Assert.AreEqual(Guid.Empty, persistedFile2.OwnerFK, "OwnerFK Test");
            Assert.AreEqual(Guid.Empty, persistedFile2.ApplicationTennantId, "OrganisationId Test");
            Assert.AreEqual(null, persistedFile2.Tag, "Tag Test");
            Assert.IsNotNull(persistedFile2.Value, "Value Test");
            Assert.IsTrue(persistedFile2.Value.Length > 0, "Value Test");


            Assert.IsTrue(persistedFile2.Metadata.Count > 0);
            var meta2 = persistedFile2.Metadata.First();

            Assert.AreEqual(meta.Id, meta2.Id);
            Assert.AreEqual(meta.OwnerFK, meta2.OwnerFK);
            Assert.AreEqual(meta.Key, meta2.Key);
            Assert.AreEqual(meta.Value, meta2.Value);
            Assert.AreEqual(meta.CreatedBy, meta2.CreatedBy);
            Assert.AreEqual(meta.CreatedOnUtc, meta2.CreatedOnUtc);
            Assert.AreEqual(meta.LastModifiedBy, meta2.LastModifiedBy);
            Assert.AreEqual(meta.LastModifiedOnUtc, meta2.LastModifiedOnUtc);




            var originalContent = System.Text.Encoding.UTF8.GetString(persistedFile.Value);
            var roundTrippedFileContent = System.Text.Encoding.UTF8.GetString(persistedFile2.Value);

            Assert.IsTrue(fileContent.Contains("File Marked as CopyAlways"));
            Assert.AreEqual(originalContent, roundTrippedFileContent);
            Assert.IsTrue(roundTrippedFileContent.Contains("File Marked as CopyAlways"));


        }



    }
}
