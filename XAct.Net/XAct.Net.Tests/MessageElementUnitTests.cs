﻿namespace XAct.Tests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using XAct.Extensions;
    using XAct.IO;
    using XAct.Net.Messaging;

    [TestFixture]
    public class MessagingElementUnitTests
    {
        private bool _initialized = false;
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();
            _initialized = true;
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void TestClassInitializedCorrectly()
        {
            //2.1.505
            //Current:2.6.2.12296
            Assert.IsTrue(_initialized);
        }

        [Test]
        public void MessageHeaderIsSettableUsingExtensionMethod()
        {

            MessageHeader messageHeader = new MessageHeader();

            messageHeader.Set("X-Whatever", "SomeValue");

            Assert.IsNotNullOrEmpty(messageHeader.Name);
            Assert.IsNotNullOrEmpty(messageHeader.Value);
        }



        [Test]
        public void MessageHeaderIsSettable()
        {

            MessageHeader messageHeader = new MessageHeader();

            messageHeader.Name = "X-Whatever";
            messageHeader.Value = "SomeValue";

            Assert.IsNotNullOrEmpty(messageHeader.Name);
            Assert.IsNotNullOrEmpty(messageHeader.Value);
        }


        [Test]
        public void MessageHeaderWCFIsSerializable()
        {

            MessageHeader messageHeader = new MessageHeader();

            messageHeader.Name = "X-Whatever";
            messageHeader.Value = "SomeValue";

            MessageHeader messageHeaderCopy = messageHeader.TestWCFSerialization();


            Assert.IsNotNullOrEmpty(messageHeaderCopy.Name);
            Assert.IsNotNullOrEmpty(messageHeaderCopy.Value);
        }


        [Test]
        public void MessageAddressIsSettable()
        {

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.Id = Guid.NewGuid();
            messageAddress.MessageId = Guid.NewGuid();
            messageAddress.Type = MessageAddressType.From;
            messageAddress.Name = "Jahn Bloggie";
            messageAddress.Value = "j.bloggie@snutfu.com";

            Assert.AreNotEqual(Guid.Empty, messageAddress.Id);
            Assert.AreNotEqual(Guid.Empty, messageAddress.MessageId);
            Assert.AreEqual(MessageAddressType.From, messageAddress.Type);
            Assert.IsNotNullOrEmpty(messageAddress.Name);
            Assert.IsNotNullOrEmpty(messageAddress.Value);
        }



        [Test]
        public void MessageAddressIsSettableUsingExtensionMethod()
        {

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.Id = Guid.NewGuid();
            messageAddress.MessageId = Guid.NewGuid();
            messageAddress.Type = MessageAddressType.From;
            messageAddress.Set("joe.blow@nowhere.com");

            Assert.AreNotEqual(Guid.Empty, messageAddress.Id);
            Assert.AreNotEqual(Guid.Empty, messageAddress.MessageId);
            Assert.AreEqual(MessageAddressType.From, messageAddress.Type);
            Assert.IsNullOrEmpty(messageAddress.Name);
            Assert.IsNotNullOrEmpty(messageAddress.Value);
        }


        [Test]
        public void MessageAddressIsSettableUsingExtensionMethod2()
        {

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.Id = Guid.NewGuid();
            messageAddress.MessageId = Guid.NewGuid();
            messageAddress.Type = MessageAddressType.From;
            messageAddress.Set("Joey B","joe.blow@nowhere.com");

            Assert.AreNotEqual(Guid.Empty, messageAddress.Id);
            Assert.AreNotEqual(Guid.Empty, messageAddress.MessageId);
            Assert.AreEqual(MessageAddressType.From, messageAddress.Type);
            Assert.IsNotNullOrEmpty(messageAddress.Name);
            Assert.IsNotNullOrEmpty(messageAddress.Value);
        }


        [Test]
        public void MessageAddressIsSettableUsingExtensionMethod3()
        {

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.Id = Guid.NewGuid();
            messageAddress.MessageId = Guid.NewGuid();
            messageAddress.Type = MessageAddressType.From;
            messageAddress.Set(MessageAddressType.From,"joe.blow@nowhere.com");

            Assert.AreNotEqual(Guid.Empty, messageAddress.Id);
            Assert.AreNotEqual(Guid.Empty, messageAddress.MessageId);
            Assert.AreEqual(MessageAddressType.From, messageAddress.Type);
            Assert.IsNullOrEmpty(messageAddress.Name);
            Assert.IsNotNullOrEmpty(messageAddress.Value);
        }



        [Test]
        public void MessageAddressIsSettableUsingExtensionMethod4()
        {

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.Id = Guid.NewGuid();
            messageAddress.MessageId = Guid.NewGuid();
            messageAddress.Type = MessageAddressType.From;
            messageAddress.Set(MessageAddressType.From, "joe.blow@nowhere.com", "Joey B.");

            Assert.AreNotEqual(Guid.Empty, messageAddress.Id);
            Assert.AreNotEqual(Guid.Empty, messageAddress.MessageId);
            Assert.AreEqual(MessageAddressType.From, messageAddress.Type);
            Assert.IsNotNullOrEmpty(messageAddress.Name);
            Assert.IsNotNullOrEmpty(messageAddress.Value);
        }



        [Test]
        public void MessageAddressIsWCFSerializable()
        {

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.Id = Guid.NewGuid();
            messageAddress.MessageId = Guid.NewGuid();
            messageAddress.Type = MessageAddressType.From;
            messageAddress.Name = "Jahn Bloggie";
            messageAddress.Value = "j.bloggie@snutfu.com";

            MessageAddress messageAddressCopy = messageAddress.TestWCFSerialization();


            Assert.AreNotEqual(Guid.Empty, messageAddressCopy.Id);
            Assert.AreNotEqual(Guid.Empty, messageAddressCopy.MessageId);
            Assert.AreEqual(MessageAddressType.From, messageAddressCopy.Type);
            Assert.IsNotNullOrEmpty(messageAddressCopy.Name);
            Assert.IsNotNullOrEmpty(messageAddressCopy.Value);
        }



        [Test]
        public void MessageAddressToStringExtensionMethod4()
        {

            MessageAddress messageAddress = new MessageAddress();

            messageAddress.Id = Guid.NewGuid();
            messageAddress.MessageId = Guid.NewGuid();
            messageAddress.Type = MessageAddressType.From;
            messageAddress.Set(MessageAddressType.From, "joe.blow@nowhere.com", "Joey B.");

            string result = messageAddress.ToString();

            Assert.AreNotEqual(Guid.Empty, messageAddress.Id);
            Assert.AreNotEqual(Guid.Empty, messageAddress.MessageId);
            Assert.AreEqual(MessageAddressType.From, messageAddress.Type);
            Assert.IsNotNullOrEmpty(messageAddress.Name);
            Assert.IsNotNullOrEmpty(messageAddress.Value);

            //TODO: Verify that it has a space between name and <
            Assert.AreEqual("\"Joey B.\" <joe.blow@nowhere.com>",result);
        }










        [Test]
        public void MessageBodyIsSettable()
        {

            MessageBody messageBody = new MessageBody();

            messageBody.Id = Guid.NewGuid();
            messageBody.MessageId = Guid.NewGuid();
            messageBody.Type = MessageBodyType.Text;
            messageBody.Name = "SomeBody";
            messageBody.Value = "Blah blah blah";

            Assert.AreNotEqual(Guid.Empty, messageBody.Id);
            Assert.AreNotEqual(Guid.Empty, messageBody.MessageId);
            Assert.AreEqual(MessageBodyType.Text, messageBody.Type);
            Assert.IsNotNullOrEmpty(messageBody.Name);
            Assert.IsNotNullOrEmpty(messageBody.Value);
        }

        [Test]
        public void MessageBodyIsSettableByExtensionMethod()
        {

            MessageBody messageBody = new MessageBody();

            messageBody.Id = Guid.NewGuid();
            messageBody.MessageId = Guid.NewGuid();
            messageBody.Type = MessageBodyType.Text;
            messageBody.Set("SomeBody", "Blah...");

            Assert.AreNotEqual(Guid.Empty, messageBody.Id);
            Assert.AreNotEqual(Guid.Empty, messageBody.MessageId);
            Assert.AreEqual(MessageBodyType.Text, messageBody.Type);
            Assert.IsNotNullOrEmpty(messageBody.Name);
            Assert.IsNotNullOrEmpty(messageBody.Value);
        }


        [Test]
        public void MessageBodyIsSettableByExtensionMethod2()
        {

            MessageBody messageBody = new MessageBody();

            messageBody.Id = Guid.NewGuid();
            messageBody.MessageId = Guid.NewGuid();
            messageBody.Set(MessageBodyType.Text,"Blah...");

            Assert.AreNotEqual(Guid.Empty, messageBody.Id);
            Assert.AreNotEqual(Guid.Empty, messageBody.MessageId);
            Assert.AreEqual(MessageBodyType.Text, messageBody.Type);

            Assert.IsNotNullOrEmpty(messageBody.Value);

            //Note:
            Assert.IsNullOrEmpty(messageBody.Name);
        }



        [Test]
        public void MessageBodyIsWCFSerializable()
        {

            MessageBody messageBody = new MessageBody();

            messageBody.Id = Guid.NewGuid();
            messageBody.MessageId = Guid.NewGuid();
            messageBody.Type = MessageBodyType.Text;
            messageBody.Name = "SomeBody";
            messageBody.Value = "Blah blah blah";

            MessageBody messageBodyCopy = messageBody.TestWCFSerialization();


            Assert.AreNotEqual(Guid.Empty, messageBodyCopy.Id);
            Assert.AreNotEqual(Guid.Empty, messageBodyCopy.MessageId);
            Assert.AreEqual(MessageBodyType.Text, messageBodyCopy.Type);
            Assert.IsNotNullOrEmpty(messageBodyCopy.Name);
            Assert.IsNotNullOrEmpty(messageBodyCopy.Value);
        }



        [Test]
        public void MessageAttachmentIsSettable()
        {
            MessageAttachment messageAttachment = new MessageAttachment();

            messageAttachment.Id = Guid.NewGuid();
            messageAttachment.MessageId = Guid.NewGuid();
            messageAttachment.Type = MessageAttachmentType.Attachment;
            messageAttachment.Name = "SomeFile.txt";
            messageAttachment.Value = "xyz....content...of...file....".ToByteArray();
            //messageBody.Size = messageBody.Value.Length;
            messageAttachment.ContentType = "text/plain";
            messageAttachment.CreatedOnUtc = DateTime.Now;
            messageAttachment.DeletedOnUtc = DateTime.Now;
            messageAttachment.LastModifiedOnUtc = DateTime.Now;


            Assert.AreNotEqual(Guid.Empty, messageAttachment.Id);
            Assert.AreNotEqual(Guid.Empty, messageAttachment.MessageId);
            Assert.AreEqual(MessageAttachmentType.Attachment, messageAttachment.Type);
            Assert.IsNotNullOrEmpty(messageAttachment.Name);
            Assert.IsNotNull(messageAttachment.Value);
            Assert.IsTrue(messageAttachment.Value.Length > 5);
            Assert.AreNotEqual(DateTime.MinValue, messageAttachment.CreatedOnUtc);
            Assert.AreNotEqual(DateTime.MinValue, messageAttachment.DeletedOnUtc);
            Assert.AreNotEqual(DateTime.MinValue, messageAttachment.LastModifiedOnUtc);


        }

        [Test]
        public void MessageAttachmentIsSettableUsingFileExtensionMethod()
        {
            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();


            string tmpFileName = Path.Combine(Path.GetTempPath(), string.Format("SmptTestFile_{0}.png",Guid.NewGuid().ToString("D")));
            System.IO.File.WriteAllText(tmpFileName,"This is a test file...");

            MessageAttachment messageAttachment = new MessageAttachment();
            messageAttachment.Id = Guid.NewGuid();
            messageAttachment.MessageId = Guid.NewGuid();

            //Use Extension Method
            messageAttachment.LoadFromFile(tmpFileName, fsioService);

            messageAttachment.Type = MessageAttachmentType.Attachment;

            Assert.AreNotEqual(Guid.Empty, messageAttachment.Id);
            Assert.AreNotEqual(Guid.Empty, messageAttachment.MessageId);
            Assert.AreEqual(MessageAttachmentType.Attachment, messageAttachment.Type);
            Assert.IsNotNullOrEmpty(messageAttachment.Name);
            Assert.IsNotNull(messageAttachment.Value);
            Assert.IsTrue(messageAttachment.Value.Length > 5);
            Assert.AreNotEqual(DateTime.MinValue, messageAttachment.CreatedOnUtc);
            Assert.AreNotEqual(DateTime.MinValue, messageAttachment.LastModifiedOnUtc);

            Assert.AreEqual(null, messageAttachment.DeletedOnUtc);


        }



        [Test]
        public void SetIdWorks()
        {
            Message message = FactoryMessage();
            Guid newGuid = Guid.NewGuid();
            message.SetIds(newGuid);

            Assert.AreEqual(newGuid,message.MessageId);

            foreach (MessageStatus messageStatus in message.Statuses)
            {
                Assert.AreEqual(newGuid, messageStatus.MessageId);
            }
            foreach (MessageHeader messageHeader in message.Headers)
            {
                Assert.AreEqual(newGuid, messageHeader.MessageId);
            }
            foreach (MessageAddress messageAddress in message.Addresses)
            {
                Assert.AreEqual(newGuid, messageAddress.MessageId);
            }
            foreach (MessageBody messageBody in message.Bodies)
            {
                Assert.AreEqual(newGuid, messageBody.MessageId);
            }
            foreach (MessageAttachment messageAttachment in message.Attachments)
            {
                Assert.AreEqual(newGuid, messageAttachment.MessageId);
            }

        }


                private static Message FactoryMessage()
        {

            IFSIOService fsioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();
                    
                    Message message = new Message();

            message.AddHeader(new MessageHeader("SomeHeader","SomeValue"));
            message.AddHeader(new MessageHeader("AnotherHeader","SomeOtherValue"));

            message.AddTo(new MessageAddress("a@b.com", "Abbot C."));
            message.AddTo(new MessageAddress("a@b2.com", "Abbot C. #2"));
            
            message.AddCc(new MessageAddress("a@cc.com", "CC Guy"));
            message.AddCc(new MessageAddress("a@cc2.com", "CC Guy #2"));

            message.AddBcc(new MessageAddress("a@bcc.com", "BCC Guy"));
            message.AddBcc(new MessageAddress("a@bcc2.com", "BCC Guy #2"));




            message.Importance = MessageImportance.Important;
            message.Priority = MessagePriority.Urgent;
            
            message.From = new MessageAddress("abc@from.com");
            message.ReplyTo = new MessageAddress("abc@replyto.com");
            message.Subject = "wada wada wada";

            message.AddATextBody(new MessageBody(MessageBodyType.Text, "This is a text body."));
            message.AddAnHtmlBody(new MessageBody(MessageBodyType.Html, "This is an html body."));


            string tmpFileName = Path.Combine(Path.GetTempPath(), string.Format("SmptTestFile_{0}.png", Guid.NewGuid().ToString("D")));
            System.IO.File.WriteAllText(tmpFileName, "This is a test file...");

            MessageAttachment messageAttachment = new MessageAttachment();

            //Use Extension Method
            messageAttachment.LoadFromFile(tmpFileName,fsioService);


            message.AddAttachment(MessageAttachmentType.Inline,  messageAttachment);


            tmpFileName = Path.Combine(Path.GetTempPath(), string.Format("SmptTestFile_{0}.png", Guid.NewGuid().ToString("D")));
            System.IO.File.WriteAllText(tmpFileName, "This is a test file...");

             messageAttachment = new MessageAttachment();

            //Use Extension Method
            messageAttachment.LoadFromFile(tmpFileName, fsioService);


            message.AddAttachment(MessageAttachmentType.Attachment, messageAttachment); 
            
            return message;
        }
    



    }
}
