﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Net.Messaging;

    [TestFixture]
    public class MessagingDeliveryServiceUnitTests
    {
        private bool _initialized = false;
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();
            _initialized = true;
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void TestClassInitializedCorrectly()
        {
            //2.1.505
            //Current:2.6.2.12296
            Assert.IsTrue(_initialized);
        }

        [Test]
        public void CanGetIMessagingDeliveryService()
        {
            IMessagingService messagingDeliveryService =
                XAct.DependencyResolver.Current.GetInstance<IMessagingService>();

            Assert.IsNotNull(messagingDeliveryService);
        }
    }
}
