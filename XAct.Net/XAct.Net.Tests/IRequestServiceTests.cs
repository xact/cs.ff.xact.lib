﻿
namespace XAct.Net.Tests.UnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using XAct.Environment;
    using XAct.Net.Implementations;
    using XAct.Tests;

    [TestFixture]
    public class IRequestServiceTests
    {






        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();



        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void CanGetIRequestService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IRequestService>();

            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetIRequestServiceOfExpectedType()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IRequestService>();

            Assert.AreEqual(typeof(RequestService), service.GetType());
        }

        [Test]
        public void CanDownloadAFile()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IRequestService>();

            string folderPath =
                XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().MapPath("TestData/Results/");
            string filePath = Path.Combine(folderPath, "downloaded.png");

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            service.DownloadRemoteFile("http://cdn.sstatic.net/stackoverflow/img/sprites.png", filePath);

            Assert.IsTrue(File.Exists(filePath));
        }

    }
}
