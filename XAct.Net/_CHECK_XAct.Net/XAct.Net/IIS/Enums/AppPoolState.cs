﻿

namespace XAct.Net.IIS
{
    public enum AppPoolState
    {
        Unknown = 0,
        Starting = 1,
        Started = 2,
        Stopping = 3,
        Stopped = 4,
    }
}
