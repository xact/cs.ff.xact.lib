﻿using System;

//http://www.koders.com/csharp/fidC744AA29DCAE1F2620D19D83618294DE953512B7.aspx?s=mdef%3afile
namespace XAct.Net.IIS.Enums
{
    [Flags]
    public enum AuthFlags
    {
        Anonymous = 1,
        Basic = 2,
        NTLM = 4,
        Digest = 16
    }

}
