﻿
using System;

namespace XAct.Net.Messaging.Elements
{
    /// <summary>
    /// An implementation of
    /// <see cref="IMessageBody"/>
    /// </summary>
    public class MessageBody : IMessageBody
    {
        #region IMplementation of IMessageDataStoreIndentifier
        /// <summary>
        /// Gets or sets the datastore id of this address element.
        /// </summary>
        /// <value>The id.</value>
        public Guid Id { get; set; }
        #endregion

        #region Implementation of IMessagePart
        /// <summary>
        /// Gets or sets the parent/owner message id.
        /// </summary>
        /// <value>The message id.</value>
        public System.Guid MessageId { get; set; }
        #endregion

        #region Implementation of IMessageNamedValue
        /// <summary>
        /// Gets or sets the name
        /// of this body.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// of this body.
        /// </summary>
        /// <value>The value.</value>
        public string Value { get; set; }
        #endregion

        #region IMplementation of IMessageBody
        /// <summary>
        /// Gets or sets the Message Body Type.
        /// </summary>
        /// <value>The type.</value>
        public MessageBodyType Type { get; set; }
        #endregion
    }
}
