﻿
using System;
using System.Collections.Generic;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// An implementation of 
    /// <see cref="IMessage"/>
    /// </summary>
    [Serializable]
    public class Message : IMessage
    {

        #region IMplementation of IMessageDataStoreIndentifier
        /// <summary>
        /// Gets or sets the datastore id of this element.
        /// </summary>
        /// <value>The id.</value>
        public Guid Id { get; set; }
        #endregion


        /// <summary>
        /// Gets or sets the date the message was created.
        /// </summary>
        /// <value>The date created.</value>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Gets or sets the date the message was sent.
        /// </summary>
        /// <value>The date sent.</value>
        public DateTime DateSent { get; set; }

        /// <summary>
        /// Gets or sets the message statuses.
        /// </summary>
        /// <value>The status.</value>
        public IEnumerable<MessageDeliveryStatus> Status { get; set; }



        /// <summary>
        /// The importance of this message.
        /// </summary>
        /// <value></value>
        public MessageImportance Importance { get; set; }

        /// <summary>
        /// Gets or sets the priority of this message.
        /// </summary>
        /// <value>The priority.</value>
        public MessagePriority Priority { get; set; }


        /// <summary>
        /// Gets the collection of <see cref="IMessageHeader"/> elements.
        /// </summary>
        /// <value>The headers.</value>
        public IMessageHeaderCollection Headers { get; set; }
        private readonly IMessageHeaderCollection _headers = new MessageHeaderCollection();



        /// <summary>
        /// The address the message is from.
        /// </summary>
        /// <value></value>
        public IMessageAddress From { get; set; }

        /// <summary>
        /// The collection of addresses the message is to.
        /// </summary>
        /// <value></value>
        public IEnumerable<IMessageAddress> To { get { return _addresses.Where(a => a.Type == MessageAddressType.To);} }

        /// <summary>
        /// The collection of addresses the message is copied to.
        /// </summary>
        /// <value></value>
        public IEnumerable<IMessageAddress> Cc { get { return _addresses.Where(a => a.Type == MessageAddressType.Cc); } }

        /// <summary>
        /// The collection of addresses the message is blind copied to.
        /// </summary>
        /// <value></value>
        public IEnumerable<IMessageAddress> Bcc { get { return _addresses.Where(a => a.Type == MessageAddressType.Bcc); } }

        /// <summary>
        /// Gets the collection of <see cref="IMessageAddress"/> elements.
        /// </summary>
        /// <value>The addresses.</value>
        public IMessageAddressCollection Addresses { get { return _addresses; } }
        private readonly IMessageAddressCollection _addresses = new MessageAddressCollection();


        /// <summary>
        /// Gets or sets the subject of this message.
        /// </summary>
        /// <value>The subject.</value>
        public string Subject { get; set; }


        /// <summary>
        /// Gets the collection of <see cref="IMessageBody"/> elements.
        /// </summary>
        /// <value>The bodies.</value>
        public IMessageBodyCollection Bodies { get { return _bodies; } }
        private readonly IMessageBodyCollection _bodies = new MessageBodyCollection();


        /// <summary>
        /// Gets the collection of <see cref="IMessageAttachment"/> elements.
        /// </summary>
        /// <value>The attachments.</value>
        public IMessageAttachmentCollection Attachments { get { return _attachments; } }
        private readonly IMessageAttachmentCollection _attachments = new MessageAttachmentCollection();

    }
}
