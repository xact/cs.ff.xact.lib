﻿using System;
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif
using System.IO;
using System.Text.RegularExpressions;

#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

namespace XAct.Net.Messaging.Entities
{
    /// <summary>
    /// An implementation of
    /// <see cref="IMessageAttachment"/>
    /// </summary>
    public class MessageAtttachment : IMessageAttachment
    {

        #region IMplementation of IMessageDataStoreIndentifier
        /// <summary>
        /// Gets or sets the datastore id of this address element.
        /// </summary>
        /// <value>The id.</value>
        public Guid Id { get; set; }
        #endregion

        #region Implementation of IMessagePart
        /// <summary>
        /// Gets or sets the parent/owner message id.
        /// </summary>
        /// <value>The message id.</value>
        public System.Guid MessageId { get; set; }
        #endregion

        
        #region Implementation of IMessageNamedValue
        /// <summary>
        /// Gets or sets the name
        /// of this attachment.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value", "Value cannot be null when setting ClientMessageAttachment.Name.");
                }
                if (value.Length == 0)
                {
                    throw new ArgumentException("String is Empty (Length == 0).", "value");
                }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
                if (value != this._name)
                {
                    this._name = value;
                }
            }
        }
        private string _name;

        /// <summary>
        /// Gets or sets the value
        /// of this attachment.
        /// </summary>
        /// <value>The value.</value>
        public byte[] Value { get; set; }
        #endregion


        #region Implementation of IMessageAttachment
        /// <summary>
        /// Gets the size of the attachment.
        /// </summary>
        /// <value>The size.</value>
        public int Size
        {
            get
            {
                return ((this.Value != null) ? this.Value.Length : 0);
            }
        }

        /// <summary>
        /// Gets the Attachment Type.
        /// </summary>
        /// <value>The type.</value>
        public MessageAttachmentType Type { get; set; }

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        public string ContentType { get; set; }


        /// <summary>
        /// Gets the date the attachment was modified.
        /// </summary>
        /// <value>The date modified.</value>
        public DateTime? DateModified { get; set; }

        /// <summary>
        /// Gets the date the attachment was created.
        /// </summary>
        /// <value>The date created.</value>
        public DateTime? DateCreated { get; set; }
        #endregion


    }
}
