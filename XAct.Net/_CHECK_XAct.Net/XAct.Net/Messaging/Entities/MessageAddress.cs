﻿
using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace XAct.Net.Messaging.Elements
{
    /// <summary>
    /// An implementation of 
    /// <see cref="IMessageAddress"/>
    /// </summary>
    public class MessageAddress : IMessageAddress
    {

        #region Resources
        public static class Resources
        {
            public static string EmailValidationRegexPattern = Properties.Resources.EmailValidationRegexPattern;
            public static string InvalidMessageAddress = "Invalid ClientMessage Address '{0}'.";
        }
        #endregion


        #region IMplementation of IMessageDataStoreIndentifier
        /// <summary>
        /// Gets or sets the datastore id of this address element.
        /// </summary>
        /// <value>The id.</value>
        public Guid Id { get; set; }
        #endregion

        #region Implementation of IMessagePart
        /// <summary>
        /// Gets or sets the parent/owner message id.
        /// </summary>
        /// <value>The message id.</value>
        public System.Guid MessageId { get; set; }
        #endregion


        #region Implementation of IMessageNamedValue
        /// <summary>
        /// Gets or sets the optional name alias for the address (eg: 'John Smith')
        /// </summary>
        /// <value>The name.</value>
        public string Name { 
            get { return _name; } 
            set
            {
                //Can be null.
                _name = value;
            }
        }

        private string _name;

        /// <summary>
        /// Gets or sets the address value.
        /// </summary>
        /// <value>The value.</value>
        [DataMember(EmitDefaultValue = true)]
        public string Value
        {
            get
            {
                return this._value;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value", "Value cannot be null.");
                }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
                if (string.Compare(value, _value, StringComparison.InvariantCulture) == 0)
                {
                    this._value = value;
                }
            }
        }

        private string _value;
        #endregion

        #region Implementation of IMessageAddress
        /// <summary>
        /// Gets or sets the address type (To, From, etc).
        /// </summary>
        /// <value>The type.</value>
        public MessageAddressType Type { get; set; }
        #endregion








        #region Methods
        /// <summary>
        /// Parses a given address into its constituent
        /// address and optional display name.
        /// </summary>
        /// <param name="fullAddress">The full address.</param>
        /// <param name="parsedAddress">The parsed address.</param>
        /// <param name="parsedDisplayName">Display name of the parsed.</param>
        /// <returns></returns>
        public static bool ParseAddress(string fullAddress, out string parsedAddress, out string parsedDisplayName)
        {
            int num2;
            parsedAddress = string.Empty;
            parsedDisplayName = string.Empty;
            if (string.IsNullOrEmpty(fullAddress))
            {
                return false;
            }

            string str = fullAddress.Trim();
            int index = str.IndexOf('"');
            if (index != -1)
            {
                if (index > 0)
                {
                    return false;
                }
                num2 = str.IndexOf('"', index + 1);
                if (num2 == -1)
                {
                    throw new Exception(fullAddress);
                }
                index++;
                parsedDisplayName = str.Substring(index, num2 - index);
                str = str.Substring(num2 + 1);
            }
            index = str.IndexOf('<');
            if (index == -1)
            {
                parsedAddress = str;
            }
            else
            {
                num2 = str.IndexOf('>');
                if (num2 < index)
                {
                    throw new Exception(fullAddress);
                }
                if (string.IsNullOrEmpty(parsedDisplayName))
                {
                    parsedDisplayName = fullAddress.Substring(0, index - 1).Trim();
                }
                index++;
                parsedAddress = str.Substring(index, num2 - index);
            }
            return true;
        }


        public void Set(string address)
        {
            string parsedAddress;
            string parsedDisplayName;
            if (address.IsNullOrEmpty()) { throw new ArgumentNullException("address"); }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            ParseAddress(address, out parsedAddress, out parsedDisplayName);

            if (!ValidateEmailAddress(parsedAddress))
            {

                throw new Exception(
                    Resources.InvalidMessageAddress.FormatStringUICulture(parsedAddress)
                    );
            }
            this.Value = parsedAddress;
            this.Name = parsedDisplayName;
        }



        /// <summary>
        /// Validates a given email address, 
        /// using Regex.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>True if the email passed.</returns>
        public static bool ValidateEmailAddress(string emailAddress)
        {
            return (
                (
                //emailAddress == "MAILER-DAEMON") ||
                //((emailAddress == "MAILER_DAEMON") || 
                Regex.IsMatch(
                    emailAddress, Resources.EmailValidationRegexPattern,
                    RegexOptions.Singleline | RegexOptions.Compiled))
                );
        }

        #endregion


    }
}
