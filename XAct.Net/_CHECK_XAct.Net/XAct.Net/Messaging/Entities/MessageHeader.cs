﻿
using System;
using System.Runtime.Serialization;

namespace XAct.Net.Messaging.Elements
{
    /// <summary>
    /// An implementation of
    /// <see cref="IMessageHeader"/>
    /// </summary>
    public class MessageHeader : IMessageHeader
    {

        #region IMplementation of IMessageDataStoreIndentifier
        /// <summary>
        /// Gets or sets the datastore id of this address element.
        /// </summary>
        /// <value>The id.</value>
        public Guid Id { get; set; }
        #endregion

        #region Implementation of IMessagePart
        /// <summary>
        /// Gets or sets the parent/owner message id.
        /// </summary>
        /// <value>The message id.</value>
        public System.Guid MessageId { get; set; }
        #endregion

        #region Implementation of IMessageNamedValue
  public string Name
    {
        get
        {
            return this._name;
        }
        set
        {
            if (value == null)
            {
                throw new ArgumentNullException("value", "Value cannot be null when setting ClientMessageHeader.Name.");
            }
            if (value.Length == 0)
            {
                throw new ArgumentException("Value cannot be empty when setting ClientMessageHeader.Name value.", "value");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            if (string.Compare(value, _name, StringComparison.InvariantCulture) == 0)
            {
                this._name = value;
            }
        }
    }
  [DataMember(IsRequired=true)]
    private string _name;


    public string Value
    {
        get
        {
            return this._value;
        }
        set
        {
            if (value == null)
            {
                throw new ArgumentNullException("value", "Value cannot be null.");
            }
            if (string.Compare(value,_value,StringComparison.InvariantCulture)==0)
            {
                this._value = value;
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
        }
    }
    [DataMember(EmitDefaultValue = true)]
    private string _value;


        #endregion
    }
}
