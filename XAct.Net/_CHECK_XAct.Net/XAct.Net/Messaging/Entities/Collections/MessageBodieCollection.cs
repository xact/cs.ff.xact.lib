using System;
using System.Collections;
using System.Runtime.Serialization;
using XAct.Collections;

namespace XAct.Net.Messaging
{

    /// <summary>
    /// A collection of 
    /// <see cref="IMessageBody"/>
    /// elements.
    /// </summary>
    [Serializable, CollectionDataContract]
    public class MessageBodyCollection : WrappedList<IMessageBody>, IMessageBodyCollection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBodyCollection"/> class.
        /// </summary>
        public MessageBodyCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBodyCollection"/> class.
        /// </summary>
        /// <param name="innerList">The inner wrapped list.</param>
        public MessageBodyCollection(IList innerList) : base(innerList)
        {
        }
    }
}

