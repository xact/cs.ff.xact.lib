using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using XAct.Collections;

namespace XAct.Net.Messaging
{



    /// <summary>
    /// A collection of 
    /// <see cref="IMessageAddress"/>
    /// elements.
    /// </summary>
    [Serializable, CollectionDataContract]
    public class MessageAddressCollection : Wintellect.PowerCollections.CollectionBase<IMessageAddress> , IMessageAddressCollection
    {
        private IList<IMessageAddress> wrappedList = new List<IMessageAddress>();
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageAddressCollection"/> class.
        /// </summary>
        public MessageAddressCollection()
        {
        }

        public override void Clear()
        {
            wrappedList.Clear();
        }

        public override int Count
        {
            get { return wrappedList.Count; }
        }

        public override IEnumerator<IMessageAddress> GetEnumerator()
        {
            return wrappedList.GetEnumerator();
        }

        public override bool Remove(IMessageAddress item)
        {
            return wrappedList.Remove(item);
        }
    }
}
