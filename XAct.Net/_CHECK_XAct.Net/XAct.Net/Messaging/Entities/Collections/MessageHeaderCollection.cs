using System;
using System.Collections;
using System.Runtime.Serialization;
using XAct.Collections;

namespace XAct.Net.Messaging
{

    /// <summary>
    /// A collection of 
    /// <see cref="IMessageHeader"/>
    /// elements.
    /// </summary>
    [Serializable, CollectionDataContract]
    public class MessageHeaderCollection : WrappedList<IMessageHeader>, IMessageHeaderCollection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageHeaderCollection"/> class.
        /// </summary>
        public MessageHeaderCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageHeaderCollection"/> class.
        /// </summary>
        /// <param name="innerList">The inner wrapped list.</param>
        public MessageHeaderCollection(IList innerList)
            : base(innerList)
        {
        }
    }
}

