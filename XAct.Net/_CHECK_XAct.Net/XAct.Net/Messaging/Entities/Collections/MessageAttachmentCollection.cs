using System;
using System.Runtime.Serialization;

namespace XAct.Net.Messaging
{

    /// <summary>
    /// A collection of 
    /// <see cref="IMessageAttachment"/>
    /// elements.
    /// </summary>
    [Serializable, CollectionDataContract]
    public class MessageAttachmentCollection : Wintellect.PowerCollections.CollectionBase<IMessageAttachment>, IMessageAttachmentCollection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageAttachmentCollection"/> class.
        /// </summary>
        public MessageAttachmentCollection()
        {
        }

    }
}

