using System.Globalization;
using System.Text;

namespace XAct.Net.Messaging
{
    using System;


    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="IMessageBody"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IMessageBodyExtensions
// ReSharper restore InconsistentNaming
    {
        //public static void MapFrom(this IMessageBody thisMessageBody, ClientMessageBody clientBody)
        //{
        //    if (clientBody == null)
        //    {
        //        throw new ArgumentNullException(string.Format(CultureInfo.CurrentCulture, Resources.ErrMsgArgumentCannotBeNull, new object[] { "clientBody" }));
        //    }
        //    thisMessageBody.Type = (MessageBodyType) clientBody.Type;
        //    thisMessageBody.Body = clientBody.Body;
        //}

        /// <summary>
        /// Sets the Message with the given message body, and type (Text, Html, etc).
        /// </summary>
        /// <param name="thisMessageBody">The this message body.</param>
        /// <param name="type">The type.</param>
        /// <param name="body">The body.</param>
        public static void Set(this IMessageBody thisMessageBody, MessageBodyType type, string body)
        {
            thisMessageBody.Type = type;
            thisMessageBody.Value = body;
        }

        /// <summary>
        /// Sets the Id
        /// </summary>
        /// <param name="thisMessageBody">The this message body.</param>
        /// <param name="id">The id.</param>
        /// <param name="messageId">The message id.</param>
        public static void SetId(this IMessageBody thisMessageBody, Guid id, Guid messageId)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException("id");
            }
            if (messageId == Guid.Empty)
            {
                throw new ArgumentException("messageId");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            thisMessageBody.Id = id;
            thisMessageBody.MessageId = messageId;
        }


        public static string ToString(IMessageBody thisBody)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(thisBody.ToString());
            builder.Append("{");
            builder.Append(string.Format(CultureInfo.InvariantCulture, "Type:[0], ", new object[] { thisBody.Type }));
            string str = string.IsNullOrEmpty(thisBody.Value) ? string.Empty : thisBody.Value.Substring(0, Math.Min(0x40, thisBody.Value.Length));
            builder.Append(string.Format(CultureInfo.InvariantCulture, "Body:[{0}] {1}", new object[] { thisBody.Value.Length, str }));
            if (thisBody.Value.Length > 0x40)
            {
                builder.Append("...");
            }
            builder.Append("}");
            return builder.ToString();
        }

    }
}

