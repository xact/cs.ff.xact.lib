﻿using System;

namespace XAct.Net.Messaging.ExtensionMethods
{
    /// <summary>
    /// Extension methods to entities
    /// that implement 
    /// <see cref="IMessageDataStoreIdentifier"/>,
    /// which are
    /// <see cref="IMessage"/>
    /// and any element
    /// that implements
    /// <see cref="IMessagePart"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IMessageDataStoreIdentifierExtensions
// ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// Sets the datastore Id of the entity.
        /// </summary>
        /// <param name="messageDataStoreIdentifier">The message data store identifier.</param>
        /// <param name="id">The id.</param>
        public static void SetId(this IMessageDataStoreIdentifier messageDataStoreIdentifier, Guid id)
        {
            messageDataStoreIdentifier.Id = id;
        }
    }
}
