using System.Globalization;

namespace XAct.Net.Messaging
{
    using System;


    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="IMessageHeader"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IMessageHeaderExtensions
// ReSharper restore InconsistentNaming
    {
        //public static void MapFrom(this IMessageHeader thisMessageHeader, ClientMessageHeader clientMessageHeader)
        //{
        //    ArgumentValidation.IsInitialized(clientMessageHeader, "clientMessageHeader");

        //    thisMessageHeader.Name = clientMessageHeader.Name;
        //    thisMessageHeader.Value = clientMessageHeader.Value;
        //}

        /// <summary>
        /// Initializes the values of this
        /// <see cref="IMessageHeader"/>
        /// </summary>
        /// <param name="thisMessageHeader">The this message header.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void Set(this IMessageHeader thisMessageHeader, string name, string value)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            ((IMessageNamedValue<string>)thisMessageHeader).Set(name, value);
        }


        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <param name="messageHeader">The message header.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(this IMessageHeader messageHeader)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}={1};",  messageHeader.Name, messageHeader.Name);
        }

    }
}

