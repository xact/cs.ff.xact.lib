using System;
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif
using System.Net.Mail;



namespace XAct.Net.Messaging
{

    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="IMessageAddress"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IMessageAddressExtensions
// ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// Maps the values from a given 
        /// <see cref="MailMessage"/>
        /// to this entity.
        /// </summary>
        /// <param name="messageAddress">The address.</param>
        /// <param name="mailAddress">The mail address.</param>
        /// <param name="addressType">Type of the address.</param>
        public static void MapFrom(this IMessageAddress messageAddress, MailAddress mailAddress, MessageAddressType addressType)
        {
            if (addressType == MessageAddressType.Undefined)
            {
                throw new ArgumentException("addressType");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            messageAddress.Type = addressType;
            messageAddress.Value = mailAddress.Address;
            messageAddress.Name = mailAddress.DisplayName;
        }


        /// <summary>
        /// Maps this <see cref="IMessageAttachment"/>
        /// based entity to a 
        /// <see cref="MailAddress"/>
        /// </summary>
        /// <param name="messageAddress">The message address.</param>
        /// <returns></returns>
        public static MailAddress ToMailAddress(this IMessageAddress messageAddress)
        {

            MailAddress mailAddress =
                (string.IsNullOrEmpty(messageAddress.Name))
                ?
                new MailAddress(messageAddress.Value)
                :
                new MailAddress(messageAddress.Value, messageAddress.Name)
                ;

            return mailAddress;
        }


        /// <summary>
        /// Maps this <see cref="IMessageAddress"/>
        /// to a
        /// <see cref="MailAddress"/>
        /// <para>
        /// Note that the <see cref="IMessageAddress.Type"/>
        /// is lost in the mapping.
        /// </para>
        /// </summary>
        /// <param name="messageAddress">The this address.</param>
        /// <returns></returns>
        public static MailAddress MapTo(this IMessageAddress messageAddress)
        {
            return new MailAddress(messageAddress.Value, messageAddress.Name);
         }

        /// <summary>
        /// Initializes this 
        /// <see cref="IMessageAddress"/>
        /// based entity.
        /// </summary>
        /// <param name="messageAddress">The message address.</param>
        /// <param name="addressType">Type of the address.</param>
        /// <param name="address">The address.</param>
        public static void Set(this IMessageAddress messageAddress, MessageAddressType addressType, string address)
        {
            if (addressType == MessageAddressType.Undefined)
            {
                throw new ArgumentException("addressType");
            }
            if (string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException("address");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            messageAddress.Type = addressType;
            messageAddress.Value = address;
        }

        /// <summary>
        /// Initializes this 
        /// <see cref="IMessageAddress"/>
        /// based entity.
        /// </summary>
        /// <param name="messageAddress">The message address.</param>
        /// <param name="addressType">Type of the address.</param>
        /// <param name="address">The address.</param>
        /// <param name="name">The name.</param>
        public static void Set(this IMessageAddress messageAddress, MessageAddressType addressType, string address, string name)
        {
            if (addressType == MessageAddressType.Undefined)
            {
                throw new ArgumentException("addressType");
            }
            if (string.IsNullOrEmpty(address))
            {
                throw new ArgumentNullException("address");
            }
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            messageAddress.Type = addressType;
            messageAddress.Value = address;
            messageAddress.Name = name;
        }


        public static string ToString(this IMessageAddress thisAddress)
        {
            return (string.IsNullOrEmpty(thisAddress.Name) ? thisAddress.Value : string.Format("{0} <{1}>", thisAddress.Name, thisAddress.Value));
        }

        public static string ToString(this IMessageAddress thisAddress, bool quoteNames)
        {
            if (string.IsNullOrEmpty(thisAddress.Name))
            {
                return thisAddress.Value;
            }
            string format = quoteNames ? "\"{0}\" <{1}>" : "{0} <{1}>";
            return string.Format(format, thisAddress.Name, thisAddress.Value);
        }





    }
}

