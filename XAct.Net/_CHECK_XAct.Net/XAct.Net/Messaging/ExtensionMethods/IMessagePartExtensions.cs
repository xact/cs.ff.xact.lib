﻿using System;

namespace XAct.Net.Messaging.ExtensionMethods
{
    /// <summary>
    /// Extension methods to 
    /// elements that implement
    /// <see cref="IMessagePart"/>
    /// See 
    /// <see cref="IMessageHeader"/>, 
    /// <see cref="IMessageAddress"/>, 
    /// <see cref="IMessageBody"/>, 
    /// and <see cref="IMessageAttachment"/>.
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IMessagePartExtensions
// ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// Sets the DatastoreId and parent Message datastoreId.
        /// </summary>
        /// <param name="messagePart">The message part.</param>
        /// <param name="id">The id.</param>
        /// <param name="messageId">The message id.</param>
        public static void SetId(this IMessagePart messagePart, Guid id, Guid messageId)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException("id");
            }
            if (messageId == Guid.Empty)
            {
                throw new ArgumentException("messageId");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            ((IMessageDataStoreIdentifier)messagePart).SetId(id);

            messagePart.MessageId = messageId;
        }
    }
}
