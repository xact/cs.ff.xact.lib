﻿using System;
using System.Net.Mail;
using System.Linq;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="IMessage"/>
    /// </summary>
    // ReSharper disable InconsistentNaming
    public static class IMessageExtensions
// ReSharper restore InconsistentNaming
    {

        /// <summary>
        /// Maps this <see cref="IMessage"/>
        /// to a 
        /// <see cref="MailMessage"/>
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        /// <remarks>
        /// Internally, throughout the application,
        /// and framework, use the IMessage, which is intended
        /// to be a cross protocol (SMTP, Twitter, whatever) entity.
        /// But when it's time to use SMTP, we have to map it to
        /// a MailMessage -- using this extension method.
        /// </remarks>
        public static MailMessage MapToMailMessage(this IMessage message)
        {

            MailMessage mailMessage = new MailMessage();


            mailMessage.From = message.From.ToMailAddress();
            

            mailMessage.Subject = message.Subject;

            //IMPORTANT:
            //When working with AlternateView 
            //Make sure mailMessage.Body is empty
            SetMailMessageAlternateView(message, mailMessage, MessageBodyType.Text);
            SetMailMessageAlternateView(message, mailMessage, MessageBodyType.Html);


            //LinkedResource linkedResource =
            // new LinkedResource(
            //   "screw_64_64.png",
            //   "image/png"); //MediaTypeNames.Image.Png not available.

            // //Give it an id/name that the html can refer to:
            // linkedResource.ContentId = "pic1";

            // //Now that we have an embedded resource, let's update
            // //our html message to refer to it:

            // string newHtml =
            // "" +
            // "A Test HTML Message" +
            // "" +
            // "";

            // //Link the resource to the AlternateView, 
            // //(and not the Message itself):
            // htmlAlternateView.LinkedResources.Add(linkedResource);


            message.AttachAttachments(mailMessage);

            //Right...on to next attachment, or you're done.
            return mailMessage;
        }


        private static AlternateView SetMailMessageAlternateView(this IMessage message, MailMessage mailMessage, MessageBodyType messageBodyType)
        {
            IMessageBody textBody = message.Bodies.Where(b => b.Type == messageBodyType).FirstOrDefault();

            if (textBody == null)
            {
                return null;
            }


            //Make a new AlternateView:
            AlternateView alternateView
                = AlternateView.CreateAlternateViewFromString
                    (textBody.Value, //string
                     null, //ContentType
                     messageBodyType.MapToMailMessageBodyType() //Text, Html, or Rich
                    );


            mailMessage.AlternateViews.Add(alternateView);

            return alternateView;
        }

        private static void AttachAttachments(this IMessage message, MailMessage mailMessage)
        {
            foreach(IMessageAttachment messageAttachment in message.Attachments)
            {
                mailMessage.AddAttachment(messageAttachment.Name);
            }
        }


        /// <summary>
        /// Add a primary recipient to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAddress">The message address.</param>
        public static void AddTo(this IMessage message, IMessageAddress messageAddress)
        {
            message.AddAddress(MessageAddressType.To, messageAddress);
        }


        /// <summary>
        /// Add a CC recipient to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAddress">The message address.</param>
        public static void AddCc(this IMessage message, IMessageAddress messageAddress)
        {
            message.AddAddress(MessageAddressType.Cc, messageAddress);
        }

        /// <summary>
        /// Add a BCC recipient to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAddress">The message address.</param>
        public static void AddBcc(this IMessage message, IMessageAddress messageAddress)
        {
            message.AddAddress(MessageAddressType.Bcc, messageAddress);
        }

        /// <summary>
        /// Add a recipient of the specified type to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageAddressType">Type of the message address.</param>
        /// <param name="messageAddress">The message address.</param>
        public static void AddAddress(this IMessage message, MessageAddressType messageAddressType, IMessageAddress messageAddress)
        {
            if (messageAddress.Type != messageAddressType)
            {
                throw new ArgumentException(string.Format("messageAddress.Type must be '{0}'", messageAddressType));
            }
            message.Addresses.Add(messageAddress);
        }

    }
}