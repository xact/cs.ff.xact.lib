﻿using System;
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif



namespace XAct.Net.Messaging
{
    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="IMessageNamedValue{T}"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IMessageNamedValueExtensions 
// ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// Sets the specified element's name and value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nameValueElement">The this message header.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void Set<T>(this IMessageNamedValue<T> nameValueElement, string name, T value)
        {
            //NO: Because IMessageAddress doesn't always have a name.
            nameValueElement.Name = name;
            nameValueElement.Value = value;
        }
    }
}
