using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace XAct.Net.Messaging
{

    /// <summary>
    /// Extension methods to elements
    /// that implement
    /// <see cref="IMessageAttachment"/>
    /// </summary>
    // ReSharper disable InconsistentNaming
    public static class IMessageAttachmentExtensions
    // ReSharper restore InconsistentNaming
    {

        /// <summary>
        /// Initializes the values of this 
        /// <see cref="IMessageAttachment"/>
        /// </summary>
        /// <param name="messageAttachment">The message attachment.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public static void Set(this IMessageAttachment messageAttachment, string name, object value)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            ((IMessageNamedValue<object>)messageAttachment).Set(name, value);
        }


        //public static void MapFrom(this IMessageAttachment thisMessageAttachment, ClientMessageAttachment clientMessageAttachment)
        //{
        //    thisMessageAttachment.Type = (MessageAttachmentType) clientMessageAttachment.Type;
        //    thisMessageAttachment.Name = clientMessageAttachment.Name;
        //    thisMessageAttachment.ContentType = clientMessageAttachment.ContentType;
        //    thisMessageAttachment.DateCreated = clientMessageAttachment.DateCreated;
        //    thisMessageAttachment.DateModified = clientMessageAttachment.DateModified;
        //    thisMessageAttachment.Value = clientMessageAttachment.Value;
        //}




        #region Methods
        public static void LoadFromFile(this IMessageAttachment messageAttachment, string fileName)
        {
            if (fileName.IsNullOrEmpty()) { throw new ArgumentNullException("fileName"); }

            if (!Path.HasExtension(fileName))
            {
                throw new ArgumentException("fileName does not have an extension.");
            }
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("ClientMessageAttachment FileName '{0}' not found.");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            messageAttachment.Name = fileName;
            messageAttachment.ContentType = GetMimeTypeFromFileExtension(fileName);
            FileInfo info = new FileInfo(fileName);
            messageAttachment.DateCreated = new DateTime?(info.CreationTime);
            messageAttachment.DateModified = new DateTime?(info.LastWriteTime);
            messageAttachment.Value = File.ReadAllBytes(fileName);
        }

        public static void LoadFromStream(this IMessageAttachment messageAttachment, Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            if (stream.Position != 0L)
            {
                throw new ArgumentException("stream Position is not 0.");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            byte[] buffer = new byte[stream.Length];

            stream.Read(buffer, 0, (int)stream.Length);

            messageAttachment.Value = buffer;
            messageAttachment.DateCreated = null;
            messageAttachment.DateModified = null;

            messageAttachment.ContentType = GetMimeTypeFromBuffer(ref buffer);
        }




        /// <summary>
        /// Returns a <see cref="System.String"/> that represents the <see cref="IMessageAttachment"/>.
        /// </summary>
        /// <param name="MessageAttachment">The message attachment.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString(this IMessageAttachment MessageAttachment)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(MessageAttachment.ToString());
            builder.Append("{");
            builder.Append(string.Format(CultureInfo.InvariantCulture, "Type:{0}, ContentType:{1}, Name:{2}, Value.Length:{3}",
                new object[] { MessageAttachment.Type, MessageAttachment.ContentType, MessageAttachment.Name, MessageAttachment.Value.Length }));
            builder.Append(string.Format(CultureInfo.InvariantCulture, "DateCreated:{0}, ", new object[] { MessageAttachment.DateCreated }));
            builder.Append(string.Format(CultureInfo.InvariantCulture, "DateModified:{0}", new object[] { MessageAttachment.DateModified }));
            builder.Append("}");
            return builder.ToString();
        }
        #endregion

        #region Private Helpers
        /// <summary>
        /// Gets the MIME type from the Registry after
        /// analysing the file extension.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        private static string GetMimeTypeFromFileExtension(string fileName)
        {
            return FileNameUtil.GetMimeTypeFromFileExtension(fileName);
        }

        private static string GetMimeTypeFromBuffer(ref byte[] byteBuffer)
        {
            XAct.Diagnostics.DefaultLoggingService.Instance.Trace(0, TraceLevel.Warning, "Havn't finished 'GetMimeTypeFromBuffer'");
            return "application/unknown";
        }
        #endregion
    }
}

