﻿//AWEFUL CODE --
//Needs to be turned into a non static Service.


//using System;
//using System.IO;
//using System.Net.Mail;
//using XAct.Net;

//namespace Datacom.Framework.Email
//{
//    /// <summary>
//    /// SmtpMailer -- send Emails via SMTP (as opposed to say IMAPI)
//    /// .NET2: Uses System.Net.Mail rather than the older System.Web.Mail of v1.1
//    /// </summary>
//    public static class SmtpMailer
//    {
//        /// <summary>
//        /// Send an Email Message
//        /// </summary>
//        /// <param name="message"></param>
//        static public void SendEmailMessage(MailMessage message)
//        {
//            SmtpClient client = new SmtpClient(ISmtpConfiguration.SmtpRelayServer);
//            client.Send(message);
//        }

//        /// <summary>
//        /// Send a plain-text email
//        /// </summary>
//        /// <param name="smtpServer"></param>
//        /// <param name="fromEmailAddress"></param>
//        /// <param name="toEmailAddress"></param>
//        /// <param name="emailSubject"></param>
//        /// <param name="emailBody"></param>
//        static public void SendEmailMessage(string smtpServer, string fromEmailAddress, string toEmailAddress,
//                                            string emailSubject, string emailBody)
//        {
//            SmtpClient client = new SmtpClient(smtpServer);
//            client.Send(fromEmailAddress, toEmailAddress, emailSubject, emailBody);
//        }

//        /// <summary>
//        /// Send an email message, using the SMTP server defined in the framework configuration file, and
//        /// from the user specified in the framework config file
//        /// </summary>
//        /// <param name="toEmailAddress"></param>
//        /// <param name="emailSubject"></param>
//        /// <param name="emailBody"></param>
//        static public void SendEmailMessage(ISmtpConfiguration smptpConfiguration, string toEmailAddress, string emailSubject, string emailBody)
//        {
//            SendEmailMessage(
//                ISmtpConfiguration.SmtpRelayServer,
//                ISmtpConfiguration.SmtpFromEmailAddress,
//                toEmailAddress, emailSubject, emailBody);
//        }

//        /// <summary>
//        /// Send an email message, using the SMTP server defined in the framework configuration file, and
//        /// from the user specified in the framework config file
//        /// </summary>
//        /// <param name="toEmailAddress"></param>
//        /// <param name="emailSubject"></param>
//        /// <param name="emailBody"></param>
//        /// <param name="isBodyHtml"></param>
//        static public void SendEmailMessage(string toEmailAddress, string emailSubject, string emailBody, bool isBodyHtml, bool blindCopy)
//        {
//            MailAddress from = new MailAddress(ISmtpConfiguration.SmtpFromEmailAddress);
//            MailAddress to = new MailAddress(toEmailAddress);
//            MailMessage mailMessage = new MailMessage(from, to);
//            mailMessage.Subject = emailSubject;
//            mailMessage.Body = emailBody;
//            mailMessage.IsBodyHtml = isBodyHtml;

//            if (blindCopy)
//            {
//                string bccEmail = ISmtpConfiguration.SmtpBccEmailAddress;
//                if (!string.IsNullOrEmpty(bccEmail))
//                    mailMessage.Bcc.Add(new MailAddress(bccEmail));

//            }

//            SendEmailMessage(mailMessage);
//        }

//        /// <summary>
//        /// Send an email message, using the SMTP server defined in the framework configuration file, and
//        /// from the user specified in the framework config file
//        /// <param name="fromEmailAddress"></param>
//        /// <param name="toEmailAddress"></param>
//        /// <param name="emailSubject"></param>
//        /// <param name="emailBody"></param>
//        /// <param name="isFromName">indicate whether 'fromEmailAddress' is the Name displayed for 'From'</param>
//        static public void SendEmailMessage(string fromEmailAddress, string toEmailAddress, string emailSubject, string emailBody, bool isFromName)
//        {
//            if (!isFromName)
//            {
//                SendEmailMessage(fromEmailAddress, toEmailAddress, emailSubject, emailBody);
//            }
//            else
//            {
//                MailAddress from = new MailAddress(ISmtpConfiguration.SmtpFromEmailAddress, fromEmailAddress);
//                MailAddress to = new MailAddress(toEmailAddress);
//                MailMessage mailMessage = new MailMessage(from, to);
//                mailMessage.Subject = emailSubject;
//                mailMessage.Body = emailBody;
//                SendEmailMessage(mailMessage);
//            }
//        }

//        /// <summary>
//        /// Send an email message with attachments, which are a list <B>OF SERVER-SIDE</B> filenames to attach
//        /// </summary>
//        /// <param name="toEmailAddress"></param>
//        /// <param name="emailSubject"></param>
//        /// <param name="emailBody"></param>
//        /// <param name="filenames">List of server-side filenames</param>
//        static public void SendEmailMessageWithAttachments(string toEmailAddress, string emailSubject, string emailBody, string[] filenames)
//        {
//            MailMessage message = new MailMessage(ISmtpConfiguration.SmtpFromEmailAddress,
//                                                  toEmailAddress,
//                                                  emailSubject,
//                                                  emailBody);
//            foreach (string filename in filenames)
//            {
//                if (File.Exists(filename))
//                    message.Attachments.Add(new Attachment(filename));
//                else
//                    throw new ApplicationException(String.Format("Request to attach non-existant file (0} in email message", filename));
//            }

//            SendEmailMessage(message);
//        }


//        /// <summary>
//        /// Send an email message, using the SMTP server defined in the framework config file
//        /// </summary>
//        /// <param name="fromEmailAddress"></param>
//        /// <param name="toEmailAddress"></param>
//        /// <param name="emailSubject"></param>
//        /// <param name="emailBody"></param>
//        static public void SendEmailMessage(string fromEmailAddress, string toEmailAddress, string emailSubject, string emailBody)
//        {
//            SendEmailMessage(
//                    ISmtpConfiguration.SmtpRelayServer,
//                    fromEmailAddress,
//                    toEmailAddress, emailSubject, emailBody);
//        }
//    }
//}

