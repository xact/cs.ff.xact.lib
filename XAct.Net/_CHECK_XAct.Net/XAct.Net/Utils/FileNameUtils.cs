﻿using System;
using System.IO;
using Microsoft.Win32;

// ReSharper disable CheckNamespace
namespace XAct.Net
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Methods to work with 
    /// FileNames
    /// </summary>
    public static class FileNameUtil
    {
        /// <summary>
        /// Gets the MIME type from the given file extension.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        /// <internal>
        /// As I do not know of a way to do this without using the Registry
        /// AND
        /// I certainly do NOT want to put this in XAct.Core,
        /// so that every app requires higher privileges
        /// (in order to reach registry)
        /// I'm putting it out here on the edge of things, in XAct.Net.
        /// </internal>
//        public static string GetMimeTypeFromFileExtension(string fileName)
//        {

//            if (fileName.IsNullOrEmpty()) { throw new ArgumentNullException("fileName"); }
//#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
//            Contract.EndContractBlock();
//#endif

//            string mimeType = "application/unknown";
//            string ext = System.IO.Path.GetExtension(fileName).ToLower();

//            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            
//            if (regKey != null && regKey.GetValue("Content Type") != null)
//            {
//                mimeType = regKey.GetValue("Content Type").ToString();
//            }
//            return mimeType;
//        }
        public static string GetMimeTypeFromFileExtension(string fileName)
        {
            if (fileName.IsNullOrEmpty() ){throw new ArgumentNullException("fileName"); }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            string fileNameExtension;

            if (Path.HasExtension(fileName))
            {
                fileNameExtension = Path.GetExtension(fileName);
            }
            else
            {
                if (fileName.IndexOf(Path.DirectorySeparatorChar) > -1)
                {
                    throw new ArgumentException("::path contains no extension.");
                }
                fileNameExtension = "." + fileName;
            }
            string result = null;

            using (RegistryKey key = Registry.ClassesRoot)
            {
                using (RegistryKey key2 = key.OpenSubKey(fileNameExtension))
                {
                    if (key2 == null)
                    {
                        result = null;
                    }
                    else
                    {
                        result = key2.GetValue("Content Type") as string;
                    }
                }
            }
            if (string.IsNullOrEmpty(result))
            {
                result = "application/octet-stream";
            }
            return result;
        }


    }
}
