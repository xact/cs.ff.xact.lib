﻿using System;
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net.Mime;


//For extension methods, which are compiler syntatic sugar
//use the root namespace (see notes in Core explaining why).
namespace XAct
{

    /// <summary>
    /// Extension Methods to the
    /// System
    /// <see cref="MailMessage"/>
    /// </summary>
    public static class MailMessageExtensions
    {

        /// <summary>
        /// Adds the linked resource to the 
        /// so that it can be referenced
        /// in an Html Body
        /// </summary>
        /// <remarks>
        /// <para>
        /// Once embedded, once can do as follows:
        /// <code>
        /// <![CDATA[
        /// //Now that we have an embedded resource, let's update
        /// //our html message to refer to it:
        /// string newHtml ="...<img src=\"pic1\"/>...";
        /// //Make a new AlternateView:
        /// htmlAlternateView
        /// = AlternateView.CreateAlternateViewFromString
        /// (newHtml, null, MediaTypeNames.Text.Html);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="mailMessage">The mail message.</param>
        /// <param name="fileInfo">The file info.</param>
        /// <param name="uniqueContentId">The unique content id.</param>
        /// <returns></returns>
        public static LinkedResource AddLinkedResource(this MailMessage mailMessage, FileInfo fileInfo, string uniqueContentId)
        {
            if (mailMessage == null) { throw new ArgumentNullException("mailMessage"); }
            if (fileInfo == null) { throw new ArgumentNullException("fileInfo"); }
            if (uniqueContentId.IsNullOrEmpty()) { throw new ArgumentNullException("uniqueContentId"); }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            
            //Lets make our html actually a little
//more complicated, and add an embedded resource
//such as a picture.
//To do that, we need a LinkedResource:

            LinkedResource linkedResource =
                new LinkedResource(fileInfo.GetMimeType()); //MediaTypeNames.Image.Png not available.

            if (uniqueContentId.IsNullOrEmpty())
            {
                uniqueContentId = "linkedResource_" + Guid.NewGuid().ToShortGuidString();
            }
//Give it an id/name that the html can refer to:
            linkedResource.ContentId = uniqueContentId;

            return linkedResource;

        }



        /// <summary>
        /// Adds the given file as an attachment.
        /// </summary>
        /// <param name="mailMessage">The mail message.</param>
        /// <param name="fileInfo">The file info.</param>
        public static Attachment AddAttachment(this MailMessage mailMessage, FileInfo fileInfo)
        {
            if (mailMessage == null) { throw new ArgumentNullException("mailMessage"); }
            if (fileInfo == null) { throw new ArgumentNullException("fileInfo"); }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            return mailMessage.AddAttachment(fileInfo, System.Net.Mime.MediaTypeNames.Application.Octet);
        }


        /// <summary>
        /// Adds the given file as an attachment.
        /// </summary>
        /// <param name="mailMessage">The mail message.</param>
        /// <param name="fileInfo">The file info.</param>
        /// <param name="contentType">Type of the content.</param>
        public static Attachment AddAttachment(this MailMessage mailMessage, FileInfo fileInfo, string contentType)
        {
            if (mailMessage == null) { throw new ArgumentNullException("mailMessage"); }
            if (fileInfo == null) { throw new ArgumentNullException("fileInfo"); }
            if (contentType.IsNullOrEmpty()) { throw new ArgumentNullException("contentType"); }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            // Create the file attachment for this e-mail message.
            string fileName = fileInfo.Name;

            Attachment attachment = mailMessage.AddAttachment(fileName, contentType);

            //For extra points...
            //Get a ref to the content disposition:
            ContentDisposition disposition = attachment.ContentDisposition;

            //So that you can actually set the dates of the 
            //resource from what's on the file...
            disposition.CreationDate = fileInfo.CreationTime;
            disposition.ModificationDate = fileInfo.LastWriteTime;
            disposition.ReadDate = fileInfo.LastAccessTime;

            //Right...on to next attachment, or you're done.
            return attachment;
        }

        /// <summary>
        /// Adds the given file as an attachment.
        /// </summary>
        /// <param name="mailMessage">The mail message.</param>
        /// <param name="fileName">The file info.</param>
        public static Attachment AddAttachment(this MailMessage mailMessage, string fileName)
        {
            if (mailMessage == null)
            {
                throw new ArgumentNullException("mailMessage");
            }
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            return mailMessage.AddAttachment(fileName, System.Net.Mime.MediaTypeNames.Application.Octet);
        }

        /// <summary>
        /// Adds the given file as an attachment.
        /// </summary>
        /// <param name="mailMessage">The mail message.</param>
        /// <param name="fileName">The file info.</param>
        /// <param name="contentType">Type of the content.</param>
        public static Attachment AddAttachment(this MailMessage mailMessage, string fileName, string contentType)
        {
            if (mailMessage == null)
            {
                throw new ArgumentNullException("mailMessage");
            }
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }
            if (contentType == null)
            {
                throw new ArgumentNullException("contentType");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            Attachment attachment =
                new Attachment(
                    fileName,
                    contentType);


            //Attach the attachment:
            // Add the attachment to mailMessage:
            mailMessage.Attachments.Add(attachment);

            return attachment;
        }
        /// <summary>
        /// 
        /// <para>
        /// Method uses settings defined in web.config.
        /// See Remarks
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Configuration is as follows:
        /// </para>
        /// <para>
        /// <code>
        /// <![CDATA[
        ///     <system.net>
        ///       <mailSettings>
        ///         <smtp from ="noreply@thisapp.com">
        ///           <network host ="smtp.gmail.com" 
        ///                    port ="587"
        ///                    enableSsl ="true"
        ///                    userName ="unnamed@gmail.com" 
        ///                    password ="passw0rd" 
        ///                    />
        ///                    </smtp>
        ///       </mailSettings>
        ///     </system.net>
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// 
        /// <param name="mailMessage"></param>
        /// <returns></returns>
        public static bool Send(this MailMessage mailMessage)
        {
            if (mailMessage == null) { throw new ArgumentNullException("mailMessage"); }
            //Messages sent out will always have the From
            //set to the account name (ie, unnamed@gmail.com -- not noreply@thisapp.com).
            //That's ok for system emails. But for end users, they therefore have to have a replyTo
            //attached so that receivers can respond to the enduser -- and not the system email address...
            if ((mailMessage.ReplyTo == null) || (mailMessage.ReplyTo.Address.IsNullOrEmpty()))
            {
                XAct.Diagnostics.DefaultLoggingService.Instance.Trace(0, TraceLevel.Warning, "Sending Mailmessage via SmtpClient, but it does not have a ReplyTo value.");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            //Create an SmtpClient that will use the system.Net/mailSettings/smtp config settings:
            SmtpClient smtpClient = new SmtpClient();

            //HACK:
            //.NET prior to 4 did not allow for configuring the SSL from the config file.
            //So the workaround is to get the config section, 
            //look at the port, and decide from that.
            //As well as look at appSettings
            if (!smtpClient.EnableSsl  && IsSmtpSSLRequired)
            {
                smtpClient.EnableSsl = true;
            }

            try
            {
                smtpClient.Send(mailMessage);

                return true;
            }
            catch
            {
                //Don't make it an Error -- just Warning -- 
                //as too many exceptions occur due to remote servers on which
                //we have no code control, and therefore cannot fix.
                XAct.Diagnostics.DefaultLoggingService.Instance.Trace(0, TraceLevel.Warning, "Mail Sent via SmtpClient failed.");

            }
            return false;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is SMTP SSL required.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is SMTP SSL required; otherwise, <c>false</c>.
        /// </value>
        /// <remarks>
        /// Prior to .NET 4.0 one could not set enableSSL from the config file.
        /// This is a workaround to a stupid stupid bug...
        /// </remarks>
        private static bool IsSmtpSSLRequired
        {
            get
            {
                if (_isSSLRequiredChecked)
                {
                    return _isSSLRequired;
                }
                //Check for AppSetting override first:
                bool useSsl = bool.TryParse(ConfigurationManager.AppSettings["smtpEnableSsl"], out useSsl);

                if (useSsl)
                {
                    _isSSLRequired = true;
                    _isSSLRequiredChecked = true;
                    return _isSSLRequired;
                }
                //Check 2:
                SmtpSection smtpConfigSection =
                    ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;
                if (smtpConfigSection != null)
                {

                    _isSSLRequired = (smtpConfigSection.Network.Port == 587) || //Imap
                                     (smtpConfigSection.Network.Port == 465);

                }
                _isSSLRequiredChecked = true;

                return _isSSLRequired;
            }
        }
        private static bool _isSSLRequired;
        private static bool _isSSLRequiredChecked;

        public static bool Send(this MailMessage mailMessage, string smtpRelayServerHost, int smtpRelayServerPort, bool enableSSL, string relayServerUserName, string relayServerUserPwd)
        {
            if (mailMessage == null){throw new ArgumentNullException("mailMessage");}
            if (smtpRelayServerHost.IsNullOrEmpty()){throw new ArgumentNullException("smtpRelayServerHost");}
            if (smtpRelayServerPort == 0) { throw new ArgumentNullException("smtpRelayServerPort"); }
            if (relayServerUserName.IsNullOrEmpty()) { throw new ArgumentNullException("relayServerUserName"); }
            if (relayServerUserPwd.IsNullOrEmpty()) { throw new ArgumentNullException("relayServerUserPwd"); }
            //Messages sent out will always have the From
            //set to the account name (ie, unnamed@gmail.com -- not noreply@thisapp.com).
            //That's ok for system emails. But for end users, they therefore have to have a replyTo
            //attached so that receivers can respond to the enduser -- and not the system email address...
            if ((mailMessage.ReplyTo == null)||(mailMessage.ReplyTo.Address.IsNullOrEmpty()))
            {
                XAct.Diagnostics.DefaultLoggingService.Instance.Trace(0, TraceLevel.Warning, "Sending Mailmessage via SmtpClient, but it does not have a ReplyTo value.");
            }

#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            //The default method is to use
            //Anonymous authentication...
            //In our case, we want to use Basic Authentication
            //so we need to turn the default behavior  off, 
            //so that we can...hence UseDefaultCredentials=false...

            SmtpClient smtpClient = new SmtpClient
                                        {
                                            Host = smtpRelayServerHost,
                                            Port = smtpRelayServerPort,
                                            DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                                            EnableSsl = enableSSL,
                                            UseDefaultCredentials = false,
                                            Credentials = new System.Net.NetworkCredential(
                                                relayServerUserName,
                                                relayServerUserPwd
                                                )
                                        };





        }

    }
}
