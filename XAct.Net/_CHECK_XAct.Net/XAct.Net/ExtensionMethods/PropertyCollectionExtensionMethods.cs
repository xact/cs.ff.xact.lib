﻿using System;
using System.DirectoryServices;

namespace XAct.Net
{
    public static class PropertyCollectionExtensionMethods
    {
        /// <summary>
        /// Gets the property's primary value.
        /// <para>
        /// Note: no exception is thrown if propertyName is not found.
        /// </para>
        /// </summary>
        /// <param name="propertyCollection">The property collection.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>An untyped value.</returns>
        public static object GetPropertyValue(this PropertyCollection propertyCollection, string propertyName)
        {
            return propertyCollection[propertyName].Value;
            //same as:
            //return propertyCollection[propertyName][0];
        }

        /// <summary>
        /// Gets the property's primary value.
        /// <para>
        /// Note: no exception is thrown if propertyName is not found.
        /// </para>
        /// </summary>
        /// <typeparam name="T">The result Type</typeparam>
        /// <param name="propertyCollection">The property collection.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>A typed value.</returns>
        public static T GetPropertyValue<T>(this PropertyCollection propertyCollection, string propertyName)
        {
            object result = propertyCollection[propertyName].Value;
            //same as:
            //return propertyCollection[propertyName][0];
            return (T)Convert.ChangeType(result, typeof(T));
        }
    }
}
