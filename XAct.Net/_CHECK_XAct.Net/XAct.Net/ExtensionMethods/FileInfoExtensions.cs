﻿using System.IO;
using XAct.Net;

namespace XAct
{
    /// <summary>
    /// Extension Methods to the 
    /// <see cref="FileInfo"/>
    /// class.
    /// </summary>
    public static class FileInfoExtensions
    {
        /// <summary>
        /// Gets the MIME type appropriate for the file, based on the filename's extension.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This is...ok. Not great.
        /// MIME type should be determined 
        /// by the content -- not the extension.
        /// But this is a known method in windows.
        /// </para>
        /// </remarks>
        /// <param name="fileInfo">The file info.</param>
        /// <returns></returns>
        public static string GetMimeType(this FileInfo fileInfo)
        {
            return FileNameUtil.GetMimeTypeFromFileExtension(Path.GetExtension(fileInfo.Name));
        }


    }
}
