﻿using System;
using System.Runtime.Serialization;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// An enumeration of the types of Priorities for an
    /// <see cref="IMessage"/>.
    /// <para>
    /// Used by <see cref="IMessageSummary.Priority"/>
    /// </para>
    /// </summary>
    [DataContract(Name = "MessagePriority", Namespace = "http://xact-solutions.com/Services/Messaging/2011/01/01")]
    public enum MessagePriority
    {
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Undefined = 0,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Urgent = 1,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Normal = 2,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Low = 3,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        VeryLow = 4
    }
}