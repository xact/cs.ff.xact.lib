﻿using System.Runtime.Serialization;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// An enumeration of the types of Importance.
    /// <para>
    /// Used by <see cref="IMessageSummary.Importance"/>
    /// </para>
    /// </summary>
    [DataContract(Name = "MessageImportance")]
    public enum MessageImportance
    {

        /// <summary>
        /// The importance is undefined.
        /// <para>
        /// This is an error state.
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// The message subject/body is of normal importance.
        /// </summary>
        [EnumMember]
        Normal = 1,

            /// <summary>
        /// The message is important.
        /// </summary>
        [EnumMember]
        Important = 2,

}

 

}