using System.Runtime.Serialization;

namespace XAct.Net.Messaging
{


    /// <summary>
    /// An enumeration of the various types
    /// a 
    /// <see cref="IMessageAttachment"/> can be.
    /// </summary>
    [DataContract(Name="MessageAttachmentType", Namespace="http://xact-solutions.com/Services/Messaging/2011/01/01")]
    public enum MessageAttachmentType
    {
        /// <summary>
        /// Undefined.
        /// <para>An error condition.</para>
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// The attachment is a normal message attachment.
        /// </summary>
        [EnumMember]
        Attachment = 1,
        
        /// <summary>
        /// The attachment is referred to inline.
        /// </summary>
        [EnumMember]
        Inline = 2,
    }
}

