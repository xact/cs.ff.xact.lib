﻿
using System.Runtime.Serialization;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// An enumeration of the types of Bodies.
    /// <para>
    /// Used by <see cref="IMessageBody.Type"/>
    /// </para>
    /// </summary>
    [DataContract(Name = "MessageBodyType")]
    public enum MessageBodyType
    {
        /// <summary>
        /// The body is a Text body.
        /// </summary>
        Text=0,
        /// <summary>
        /// The body is a rich Html body.
        /// </summary>
        Html=1,
        /// <summary>
        /// The body is a type specific to the transmission protocol.
        /// </summary>
        ProtocolSpecific=2
    }
}
