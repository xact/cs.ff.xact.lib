﻿using System.Runtime.Serialization;

namespace XAct.Net.Messaging
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "MessageDeliveryStatus", Namespace = "http://xact-solutions.com/Services/Messaging/2011/01/01")]
    public enum MessageDeliveryStatus
    {
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Bounced = 30,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Defer = 20,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Failed = 0x15,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Logged = 1,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Processing = 2,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Sent = 0x16,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        ServiceAccepted = 12,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        ServiceError = 11,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        ServiceRejected = 10,
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Undefined = 0
    }
}