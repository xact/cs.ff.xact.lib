﻿

using System.Runtime.Serialization;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// TODO: Document
    /// </summary>
    [DataContract(Name = "MessageBodyEncoding")]
public enum MessageBodyEncoding
{
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    Base64 = 1,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    Binary = 5,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    Bit7 = 2,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    Bit8 = 3,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    NullEncoding = 7,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    QuotedPrintable = 4,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    QuotedPrintableMinimal = 0x10,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    QuotedPrintableRelaxed = 8,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    Undefined = 0,
    /// <summary>
    /// TODO: Document
    /// </summary>
    [EnumMember]
    XToken = 6
}

 
}
