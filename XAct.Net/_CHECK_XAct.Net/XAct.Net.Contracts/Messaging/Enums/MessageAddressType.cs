﻿
using System.Runtime.Serialization;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// The Type pf <see cref="IMessage"/>
    /// </summary>
    [DataContract(Name = "MessageAttachmentType")]
    public enum MessageAddressType
    {
        /// <summary>
        /// The MessageAddress Type is undefined.
        /// <para>
        /// An Error Condition.
        /// </para>
        /// </summary>
        Undefined=0,
        /// <summary>
        /// The MessageAddress is a From address.
        /// </summary>
        From=1,
        /// <summary>
        /// The MessageAddress is a To address.
        /// </summary>
        To = 2,
        /// <summary>
        /// The MessageAddress is a Cc address.
        /// </summary>
        Cc = 3,
        /// <summary>
        /// The MessageAddress is a Bcc address.
        /// </summary>
        Bcc = 4,

    }
}
