﻿using System;
using System.Collections.Generic;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// The contract for a distributable Message.
    /// </summary>
    public interface IMessage : IMessageSummary, IMessageDataStoreIdentifier
    {


        /// <summary>
        /// Gets the collection of <see cref="IMessageHeader"/> elements.
        /// </summary>
        /// <value>The headers.</value>
        IMessageHeaderCollection Headers { get;  }
        /// <summary>
        /// Gets the collection of <see cref="IMessageAddress"/> elements.
        /// </summary>
        /// <value>The addresses.</value>
        IMessageAddressCollection Addresses { get; }
        
        /// <summary>
        /// Gets the collection of <see cref="IMessageBody"/> elements.
        /// </summary>
        /// <value>The bodies.</value>
        IMessageBodyCollection Bodies { get; }

        /// <summary>
        /// Gets the collection of <see cref="IMessageAttachment"/> elements.
        /// </summary>
        /// <value>The attachments.</value>
        IMessageAttachmentCollection Attachments { get; }
    }
}
