﻿namespace XAct.Net.Messaging
{
    /// <summary>
    /// A Message Address.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Not suitable as an Event argument (see 
    /// <see cref="IReadOnlyMessageAddress"/>)
    /// </para>
    /// </remarks>
    public interface IMessageAddress : IReadOnlyMessageAddress, IMessageNamedValue<string>, IMessagePart    {

        //Inherits Name, Value

        /// <summary>
        /// Gets or sets the address type (To, From, etc).
        /// </summary>
        /// <value>The type.</value>
        new MessageAddressType Type { get; set; }

    }
}
