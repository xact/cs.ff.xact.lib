﻿using System;
using System.Collections.Generic;

namespace XAct.Net.Messaging
{
    /// <summary>
    ///   Interface for <c>MessageSummary</c>
    /// </summary>
    public interface IMessageSummary
    {
        /// <summary>
        /// Gets or sets the date the message was created.
        /// </summary>
        /// <value>The date created.</value>
        DateTime DateCreated { get; set; }

        /// <summary>
        /// Gets or sets the date the message was sent.
        /// </summary>
        /// <value>The date sent.</value>
        DateTime DateSent { get; set; }


        /// <summary>
        /// Gets or sets the message statuses.
        /// </summary>
        /// <value>The status.</value>
        IEnumerable<MessageDeliveryStatus> Status { get; set; }


        /// <summary>
        /// The importance of this message.
        /// </summary>
        MessageImportance Importance { get; set; }

        /// <summary>
        /// Gets or sets the priority of this message.
        /// </summary>
        /// <value>The priority.</value>
        MessagePriority Priority { get; set; }


        /// <summary>
        /// The address the message is from.
        /// </summary>
        IMessageAddress From { get; }

        /// <summary>
        /// The collection of addresses the message is to.
        /// </summary>
        IEnumerable<IMessageAddress> To { get; }


        /// <summary>
        /// The collection of addresses the message is copied to.
        /// </summary>
        IEnumerable<IMessageAddress> Cc { get; }

        /// <summary>
        /// The collection of addresses the message is blind copied to.
        /// </summary>
        IEnumerable<IMessageAddress> Bcc { get; }

        /// <summary>
        /// Gets or sets the subject of this message.
        /// </summary>
        /// <value>The subject.</value>
        string Subject { get; set; }
    }

    /// <summary>
    ///   Interface for <c>MessageDeliverySummary</c>
    /// </summary>
    internal interface IMessageDeliverySummary : IMessageSummary
    {
        MessageDeliveryStatus DeliveryStatus { get; set; }
    }
}