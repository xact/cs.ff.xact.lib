﻿using System;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// A Message Attachment.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Not suitable as an Event argument (see 
    /// <see cref="IReadOnlyMessageAttachment"/>)
    /// </para>
    /// </remarks>
    public interface IMessageAttachment : IReadOnlyMessageAttachment, IMessageNamedValue<byte[]>, IMessagePart
    {
        //Inherits name and value


        /// <summary>
        /// Gets the Attachment Type.
        /// </summary>
        /// <value>The type.</value>
        new MessageAttachmentType Type { get; set; }


        // no need to override as both are just get;
        //new int Size { get; }

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        new string ContentType { get; set; }


        /// <summary>
        /// Gets the date the attachment was modified.
        /// </summary>
        /// <value>The date modified.</value>
        new DateTime? DateModified { get; set; }

        /// <summary>
        /// Gets the date the attachment was created.
        /// </summary>
        /// <value>The date created.</value>
        new DateTime? DateCreated { get; set; }


    }
}
