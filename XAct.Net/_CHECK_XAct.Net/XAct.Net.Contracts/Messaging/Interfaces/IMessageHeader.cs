﻿
namespace XAct.Net.Messaging
{
    /// <summary>
    /// A Message Header.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Not suitable as an Event argument (see 
    /// <see cref="IReadOnlyMessageAddress"/>)
    /// </para>
    /// </remarks>
    public interface IMessageHeader : IReadOnlyMessageHeader, IMessageNamedValue<string>, IMessagePart
    {

        //Inherits Name, Value
        
    }
}
