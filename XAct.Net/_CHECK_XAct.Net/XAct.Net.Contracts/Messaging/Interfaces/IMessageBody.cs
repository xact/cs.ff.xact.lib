﻿namespace XAct.Net.Messaging
{
    /// <summary>
    /// A Message Body.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Not suitable as an Event argument (see 
    /// <see cref="IReadOnlyMessageBody"/>)
    /// </para>
    /// </remarks>
    public interface IMessageBody : IReadOnlyMessageBody, IMessageNamedValue<string>, IMessagePart
    {
        //Inherits Name, Value
        
        /// <summary>
        /// Gets or sets the Message Body Type.
        /// </summary>
        /// <value>The type.</value>
        new MessageBodyType Type { get; set; }


    }
}
