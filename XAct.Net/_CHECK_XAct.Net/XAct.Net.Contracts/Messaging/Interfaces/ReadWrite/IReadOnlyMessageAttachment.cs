﻿using System;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// A readonly Message Attachment,
    /// suitable for delivery via events to 3rd party listeners,
    /// while remaining confident that they can't modify the message along the way.
    /// </summary>
    public interface IReadOnlyMessageAttachment : IReadOnlyMessageNamedValue<byte[]>
    {
        //inherits Name and Value

        /// <summary>
        /// Gets the Attachment Type.
        /// </summary>
        /// <value>The type.</value>
        MessageAttachmentType Type { get; }


        /// <summary>
        /// Gets the size of the attachment.
        /// </summary>
        /// <value>The size.</value>
        int Size { get; }

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        string ContentType { get; }

        
        /// <summary>
        /// Gets the date the attachment was modified.
        /// </summary>
        /// <value>The date modified.</value>
        DateTime? DateModified { get;  }

        /// <summary>
        /// Gets the date the attachment was created.
        /// </summary>
        /// <value>The date created.</value>
        DateTime? DateCreated { get; }




    }
}
