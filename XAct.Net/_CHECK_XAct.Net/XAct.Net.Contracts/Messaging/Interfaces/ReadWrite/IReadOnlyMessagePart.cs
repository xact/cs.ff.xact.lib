﻿using System;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// A readonly interface to the value of the Message that owns this part
    /// (Header, Address, Body, Attachment). 
    /// </summary>
    public interface IReadOnlyMessagePart 
    {
        /// <summary>
        /// Gets the parent/owner message id.
        /// </summary>
        /// <value>The message id.</value>
        Guid MessageId {get;}
    }
}
