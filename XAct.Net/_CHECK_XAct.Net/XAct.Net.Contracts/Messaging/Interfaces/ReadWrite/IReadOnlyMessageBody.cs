﻿namespace XAct.Net.Messaging
{
    /// <summary>
    /// A readonly Message Body part, 
    /// suitable for delivery via events to 3rd party listeners,
    /// while remaining confident that they can't modify the message along the way.
    /// </summary>
    public interface IReadOnlyMessageBody : IReadOnlyMessageNamedValue<string>
    {
        //inherits Name and Value

        /// <summary>
        /// The type of the Body (Html, Text, etc.)
        /// </summary>
        MessageBodyType Type { get; set; }

    }
}
