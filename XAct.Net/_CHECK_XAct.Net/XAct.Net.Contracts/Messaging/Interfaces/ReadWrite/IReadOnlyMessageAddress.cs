﻿namespace XAct.Net.Messaging
{
    /// <summary>
    /// A readonly Message Address, 
    /// suitable for delivery via events to 3rd party listeners,
    /// while remaining confident that they can't modify the message along the way.
    /// </summary>
    public interface IReadOnlyMessageAddress : IReadOnlyMessageNamedValue<string>
    {

        //inherits Name and Value

        /// <summary>
        /// Gets the Address Type.
        /// </summary>
        /// <value>The type.</value>
        MessageAddressType Type { get; }

        #region Implementation of IReadOnlyNamedValue

        #endregion

    }
}
