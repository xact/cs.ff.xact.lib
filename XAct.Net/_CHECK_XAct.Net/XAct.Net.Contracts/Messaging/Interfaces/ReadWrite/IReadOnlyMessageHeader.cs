﻿
namespace XAct.Net.Messaging
{
    /// <summary>
    /// <summary>
    /// A readonly Message Header, 
    /// suitable for delivery via events to 3rd party listeners,
    /// while remaining confident that they can't modify the message along the way.
    /// </summary>
    /// </summary>
    public interface IReadOnlyMessageHeader: IReadOnlyMessageNamedValue<string>
    {
        //inherits Name and Value
    }
}
