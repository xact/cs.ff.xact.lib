﻿using System;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// A contract for any 
    /// message entity
    /// saved to a datastore (db).
    /// </summary>
    public interface IReadOnlyMessageDataStoreIdentifier
    {
        /// <summary>
        /// Gets the unique Id of this entity in the datastore.
        /// </summary>
        /// <value>The id.</value>
        Guid Id { get; }
    }
}
