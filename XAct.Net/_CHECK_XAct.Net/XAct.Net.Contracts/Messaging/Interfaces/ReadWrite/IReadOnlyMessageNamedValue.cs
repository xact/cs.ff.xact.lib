﻿
namespace XAct.Net.Messaging
{
    /// <summary>
    /// Interface for all Message elements. 
    /// Provides access to the owning Message Id.
    /// </summary>
    /// <internal>
    /// Used as the basis of 
    /// <see cref="IReadOnlyMessageHeader"/>
    /// <see cref="IReadOnlyMessageAddress"/>
    /// <see cref="IReadOnlyMessageAttachment"/>
    /// but not
    /// <see cref="IReadOnlyMessageBody"/>
    /// </internal>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IReadOnlyMessageNamedValue<TValue> 
    {
        /// <summary>
        /// Gets the key name of the entity.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; }

        /// <summary>
        /// Gets the typed value of the entity.
        /// </summary>
        TValue Value { get; }
    }
}
