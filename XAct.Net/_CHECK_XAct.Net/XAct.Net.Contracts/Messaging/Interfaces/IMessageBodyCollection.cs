﻿
using System.Collections.Generic;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// An interface for a collection of
    /// <see cref="IMessageBody"/>
    /// entities.
    /// </summary>
    public interface IMessageBodyCollection :ICollection<IMessageBody>
    {
    }
}
