﻿using System;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// Contract for all elements
    /// that are part of an
    /// <see cref="IMessage"/>.
    /// </summary>
    public interface IMessagePart : IReadOnlyMessagePart, IMessageDataStoreIdentifier
    {
        /// <summary>
        /// Gets or sets the parent/owner message id.
        /// </summary>
        /// <value>The message id.</value>
        new Guid MessageId { get; set; }
    }
}
