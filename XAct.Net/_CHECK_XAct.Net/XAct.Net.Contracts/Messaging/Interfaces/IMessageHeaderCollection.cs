﻿
namespace XAct.Net.Messaging
{
    /// <summary>
    /// An interface for a collection of
    /// <see cref="IMessageHeader"/>
    /// entities.
    /// </summary>
    public interface IMessageHeaderCollection
    {
    }
}
