﻿using System.Collections.Generic;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// An interface for a collection of
    /// <see cref="IMessageAddress"/>
    /// entities.
    /// </summary>
    public interface IMessageAddressCollection : ICollection<IMessageAddress>    
    {

    }
}
