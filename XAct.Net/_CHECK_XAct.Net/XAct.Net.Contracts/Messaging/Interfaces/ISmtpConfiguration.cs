﻿
using System;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// An interface of settings that an app will
    /// need in order to send emails.
    /// <para>
    /// Most apps have emails that are sent from a specific
    /// address, via a specific route. 
    /// This allows this kind of configuration, 
    /// while allowing for some indirection.
    /// </para>
    /// </summary>
    public interface ISmtpSystemConfiguration
    {
        /// <summary>
        /// Gets the SMTP Default System 'From' email address.
        /// </summary>
        /// <value>The SMTP from email address.</value>
        IMessageAddress SmtpFromEmailAddress { get; }


        ISmtpServerConfigurationSettings ServerSettings { get; }
    }

    public interface ISmtpServerConfigurationSettings
    {
        /// <summary>
        /// Gets the SMTP relay server.
        /// <para>
        /// eg: "smtp.gmail.com"
        /// </para>
        /// </summary>
        /// <value>The SMTP relay server.</value>
        string ServerName { get; }

        /// <summary>
        /// Gets the smtp relay server's port.
        /// <para>
        /// eg: 
        /// </para>
        /// </summary>
        /// <value>The port.</value>
        int Port { get; }

        /// <summary>
        /// Gets a value indicating whether 
        /// the server requires SSL to be enabled.
        /// </summary>
        /// <value><c>true</c> if SSL needs to be enabled; otherwise, <c>false</c>.</value>
        bool Ssl { get; }

    }
}
