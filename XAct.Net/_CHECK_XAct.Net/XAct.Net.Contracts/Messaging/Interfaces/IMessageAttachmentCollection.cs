﻿
using System.Collections.Generic;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// An interface for a collection of
    /// <see cref="IMessageAttachment"/>
    /// entities.
    /// </summary>
    public interface IMessageAttachmentCollection : ICollection<IMessageAttachment>
    {
    }
}
