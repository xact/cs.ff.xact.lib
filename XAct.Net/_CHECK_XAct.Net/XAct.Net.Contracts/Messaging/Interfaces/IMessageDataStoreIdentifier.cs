﻿using System;

namespace XAct.Net.Messaging
{
    /// <summary>
    /// A contract for any 
    /// message entity
    /// saved to a datastore (db).
    /// </summary>
    public interface IMessageDataStoreIdentifier : IReadOnlyMessageDataStoreIdentifier
    {
        /// <summary>
        /// Gets or sets the datastore id of this element.
        /// </summary>
        /// <value>The id.</value>
        new Guid Id { get; set; }
    }
}
