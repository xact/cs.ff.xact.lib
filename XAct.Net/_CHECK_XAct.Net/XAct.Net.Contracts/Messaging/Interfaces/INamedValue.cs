﻿
namespace XAct.Net.Messaging
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IMessageNamedValue<TValue> : IReadOnlyMessageNamedValue <TValue>
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        new string Name { get; set; }



        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        new TValue Value { get; set; }
    }
}
