namespace XAct.Imaging.Vision
{
    using System.Drawing;
    using XAct.Drawing;

    /// <summary>
    /// Contract for a service to analyse images.
    /// </summary>
    public interface IVisionService : IHasXActLibService
    {
        /// <summary>
        /// Compares two images.
        /// </summary>
        /// <param name="image1">The image1.</param>
        /// <param name="image2">The image2.</param>
        /// <param name="colorType">Type of the color.</param>
        /// <returns></returns>
        float CompareTwoImages(Image image1, Image image2, ColorType colorType);
    }
}