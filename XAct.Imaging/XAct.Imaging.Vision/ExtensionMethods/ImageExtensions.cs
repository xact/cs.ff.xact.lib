﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct
{
    using System.Drawing;

#endif

    /// <summary>
    /// Extension Methods for Image ojects
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        /// Converts an image into an 8-bit grayscale bitmap
        /// </summary>
        public static Bitmap ToGrayscale(this Image sourceImage)
        {
            return new Bitmap(sourceImage).ToGrayscale();
        }


        ///// <summary>
        ///// Images to byte array.
        ///// </summary>
        ///// <param name="imageIn">The image in.</param>
        ///// <returns></returns>
        //public static byte[] imageToByteArray(this System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
        //    return ms.ToArray();
        //}

        ///// <summary>
        ///// Bytes the array to image.
        ///// </summary>
        ///// <param name="byteArrayIn">The byte array in.</param>
        ///// <returns></returns>
        //public static Image byteArrayToImage(this byte[] byteArrayIn)
        //{
        //    MemoryStream ms = new MemoryStream(byteArrayIn);
        //    Image returnImage = Image.FromStream(ms);
        //    return returnImage;

        //}

        ///// <summary>
        ///// Converts the specified oldbmp.
        ///// </summary>
        ///// <param name="oldbmp">The oldbmp.</param>
        ///// <returns></returns>
        //public static Image Convert(this Bitmap oldbmp)
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        oldbmp.Save(ms, ImageFormat.Gif);
        //        ms.Position = 0;
        //        return Image.FromStream(ms);
        //    }
        //}


    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif

