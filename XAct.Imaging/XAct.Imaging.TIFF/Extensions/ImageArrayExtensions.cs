﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using XAct.IO;

#endif

    /// <summary>
    /// Extension Methods for working with Image Arrays
    /// </summary>
    public static class ImageArrayExtensions
    {
        /// <summary>
        /// WRites the array of images to the hard drive.
        /// </summary>
        /// <param name="images">The images.</param>
        /// <param name="targetFilePath">The target file path.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <param name="ioService">The specific (isolated or hd) io service.</param>
        /// <param name="replaceExistingFilesIfAny">if set to <c>true</c> replace existing files if any found.</param>
        /// <param name="startNumber">The start number.</param>
        public static void ToFiles(this IEnumerable<Image> images, string targetFilePath, ImageFormat imageFormat, IIOService ioService, bool replaceExistingFilesIfAny=true, int startNumber=0)
        {
            int pageNumber = startNumber+1;

            foreach (Image singlePageImage in images)
            {
                string dir = Path.GetDirectoryName(targetFilePath);
                string fileName = Path.GetFileNameWithoutExtension(targetFilePath);
                string ext = Path.GetExtension(targetFilePath);
                string rebuiltFileName =
                    "{0}{1}_pg{2}{3}".FormatStringCurrentCulture(
                        dir,
                        fileName,
                        pageNumber,
                        ext);

                using (Stream stream = ioService.FileOpenWriteAsync(rebuiltFileName, replaceExistingFilesIfAny,false).WaitAndGetResult())
                {
                    //Avoid using File access directly, as it can't be detoured
                    //when testing...
                    singlePageImage.Save(stream, imageFormat);
                }

                pageNumber++;
            }
            //int frameCount = sourceTiffImage.GetFrameCount(FrameDimension.Page);

        } 
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
