namespace XAct.Imaging.TIFF.Implementations
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using XAct.Diagnostics;
    using XAct.IO;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="ITiffManagementService"/>
    /// to work with tiffs.
    /// </summary>
    public class TiffManagementService : XActLibServiceBase, ITiffManagementService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TiffManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public TiffManagementService(ITracingService tracingService) : base(tracingService)
        {
        }

        /// <summary>
        /// Converts the multi page tiff to images saved to hard drive.
        /// </summary>
        /// <param name="sourceTiffImage">The source tiff image.</param>
        /// <param name="targetFilePath">The target file path.</param>
        /// <param name="targetImageFormat">The target image format.</param>
        /// <param name="ioService">The specific io  (isolated or hd) service to use when writing the files..</param>
        public void ConvertMultiPageTiffToImages(Bitmap sourceTiffImage, string targetFilePath,
                                                 ImageFormat targetImageFormat, IIOService ioService)
        {

            ConvertMultiPageTiffToImages(sourceTiffImage).ToFiles(targetFilePath, targetImageFormat,ioService/*,0*/);


        }


        /// <summary>
        /// Converts the multi page tiff to images an enumerable set of images.
        /// </summary>
        /// <param name="sourceTiffImage">The source tiff image.</param>
        /// <param name="startPage">The start page.</param>
        /// <param name="lastPage">The last page.</param>
        /// <returns></returns>
        public IEnumerable<Image> ConvertMultiPageTiffToImages(
            Bitmap sourceTiffImage, int startPage=0, int lastPage=int.MaxValue)
        {
            int frameCount = sourceTiffImage.GetFrameCount(FrameDimension.Page);

            
            for (int i = startPage; i < frameCount; i++)
            {
                sourceTiffImage.SelectActiveFrame(FrameDimension.Page, i);

                //Took me a sec to track this down...
                //Faxes come in at res 200. 
                //Bitmaps by default are built at 96...
                //So you can either do it this way:
                //Bitmap temp = new Bitmap((int)(fax.Width * (96/fax.HorizontalResolution) ), (int)(fax.Height * (96/fax.VerticalResolution)));
                Bitmap singlePageImage = new Bitmap(sourceTiffImage.Width, sourceTiffImage.Height);
                //Or set the resolution before you paint on it.
                singlePageImage.SetResolution(sourceTiffImage.HorizontalResolution,
                                              sourceTiffImage.VerticalResolution);
                using (Graphics g = Graphics.FromImage(singlePageImage))
                {
                    g.InterpolationMode = InterpolationMode.NearestNeighbor;
                    g.DrawImageUnscaled(sourceTiffImage, 0, 0);
                    g.Dispose();
                }

                yield return sourceTiffImage;
            }
        }


    }


    //See:http://www.bobpowell.net/addframes.htm
}

