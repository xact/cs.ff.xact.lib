namespace XAct.Imaging.TIFF
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using XAct.IO;

    /// <summary>
    /// Contract for a service to work with Tiff files (ie, faxes).
    /// </summary>
    public interface ITiffManagementService : IHasXActLibService
    {
        /// <summary>
        /// Converts the multi page tiff to images saved to hard drive.
        /// </summary>
        /// <param name="sourceTiffImage">The source tiff image.</param>
        /// <param name="targetFilePath">The target file path.</param>
        /// <param name="targetImageFormat">The target image format.</param>
        /// <param name="ioService">The specific io service  (isolated or hd) to use.</param>
        void ConvertMultiPageTiffToImages(Bitmap sourceTiffImage, string targetFilePath, ImageFormat targetImageFormat, IIOService ioService);

        /// <summary>
        /// Converts the multi page tiff to images an enumerable set of images.
        /// </summary>
        /// <param name="sourceTiffImage">The source tiff image.</param>
        /// <param name="startPage">The start page (zero based).</param>
        /// <param name="endPage">The end page.</param>
        /// <returns></returns>
        IEnumerable<Image> ConvertMultiPageTiffToImages(Bitmap sourceTiffImage, int startPage=1, int endPage = int.MaxValue);
    }
}