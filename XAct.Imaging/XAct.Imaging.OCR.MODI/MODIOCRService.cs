using System;
using System.Collections.Generic;
using MODI;

namespace XAct.Imaging.OCR
{
    /// <summary>
    /// An implementation of <see cref="IMODIOCRService"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage:
    /// <code>
    /// <![CDATA[        
    /// static void Main(string[] args)
    /// {
    ///   string imagePath = @"C:\Temp\personalfinancesituation.png";
    ///   MODIOCRService ocrService = new MODIOCRService();
    ///   string[] results = ocrService.OCR(imagePath);
    ///   results.ForEach(Console.Write);
    ///   Console.ReadKey();
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Requirements:
    /// Office 2007 Microsoft Office Document Imaging is required (MODI was removed from Office 2010)
    /// Add a Reference to the Microsoft Office Document Imaging 11.0 Type Library (located in MDIVWCTL.DLL)
    /// C:\Program Files (x86)\Common Files\microsoft shared\MODI\12.0\MDIVWCTL.DLL
    /// </para>
    /// <para>
    /// http://support.microsoft.com/kb/982760
    /// </para>
    /// </remarks>
    public class MODIOCRService : IMODIOCRService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MODIOCRService"/> class.
        /// </summary>
        public MODIOCRService() { }


        /// <summary>
        /// OCRs the specified source image file name.
        /// </summary>
        /// <param name="sourceImageFileName">Name of the source image file.</param>
        /// <returns></returns>
        public string[] OCR(string sourceImageFileName)
        {
            MODI.Document modiDocument = null;

            try
            {
                modiDocument = new MODI.Document();

                //Hook up event to watch progress:
                modiDocument.OnOCRProgress += new _IDocumentEvents_OnOCRProgressEventHandler(modiDocument_OnOCRProgress);

                
                //Create MODI doc around source:
                modiDocument.Create(sourceImageFileName);

                bool orientImage = true;
                bool straightenImage = true;

                //OCR all pages of a muli-page tiff file:
                modiDocument.OCR(MODI.MiLANGUAGES.miLANG_ENGLISH, orientImage, straightenImage);

                //Save deskewed/re-oriented images, and the OCR text, back to the input file.
                //modiDocument.SaveAs(outputFileName, MiFILE_FORMAT.miFILE_FORMAT_DEFAULTVALUE, MiCOMP_LEVEL.miCOMP_LEVEL_HIGH);

                List<string> results = new List<string>();

                foreach (Image image in modiDocument.Images)
                {
                    results.Add(image.Layout.Text);

                    //All these don't work:
                    //http://stackoverflow.com/questions/468972/how-to-convert-ipicturedisp-to-system-drawing-image
                    //http://blogs.msdn.com/b/andreww/archive/2007/07/30/converting-between-ipicturedisp-and-system-drawing-image.aspx
                //var o = image.Thumbnail;
                //System.Drawing.Image thumbNail = 
                //AxHostConverter.PictureDispToImage(o);
                }
                
                //AnalyseDoc(modiDocument);

                return results.ToArray();
            }
            catch 
            {
                throw;
            }
            finally
            {
                bool saveChanges = false;
                modiDocument.Close(saveChanges);
                modiDocument.OnOCRProgress -= modiDocument_OnOCRProgress;
                modiDocument = null;
            }
        }

        void modiDocument_OnOCRProgress(int Progress, ref bool Cancel)
        {
            //throw new NotImplementedException();
        }

        //void AnalyseDoc(MODI.Document modiDocument)
        //{
        //    //Other stuff that could be tried at some point:
        //    //     for (int j= 0; j< layout.Words.Count; j++)
        //    //{
        //    //    MODI.Word word = (MODI.Word) layout.Words[j];
        //    //    // getting the word's characters

        //    //    for (int k = 0; k < word.Rects.Count; k++)
        //    //    {
        //    //        MODI.MiRect rect = (MODI.MiRect) word.Rects[k];
        //    //        charactersHeights  += rect.Bottom-rect.Top;
        //    //        numOfCharacters++;                        
        //    //    }
        //    //}
        //    //float avHeight = (float )charactersHeights/numOfCharacters;
        //    //statistic += "Page "+i+ ": Avarage character height is: "+
        //    //                 "avHeight.ToString("0.00") +" pixel!"+ "\r\n";

            
        //}


        //internal class AxHostConverter : AxHost
        //{
        //    private AxHostConverter() : base("") { }

        //    static public stdole.IPictureDisp ImageToPictureDisp(System.Drawing.Image image)
        //    {
        //        return (stdole.IPictureDisp)GetIPictureDispFromPicture(image);
        //    }

        //    static public System.Drawing.Image PictureDispToImage(stdole.IPictureDisp pictureDisp)
        //    {
        //        return GetPictureFromIPicture(pictureDisp);
        //    }
        //}

    }
}