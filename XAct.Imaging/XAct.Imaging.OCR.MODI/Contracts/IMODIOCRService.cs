namespace XAct.Imaging.OCR
{
    /// <summary>
    /// A Contract for a Service that manages the 
    /// Microsoft Office Document Imaging (MODI) library
    /// that came with Office 2007.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Requirements:
    /// Office 2007 Microsoft Office Document Imaging is required (MODI was removed from Office 2010)
    /// Add a Reference to the Microsoft Office Document Imaging 11.0 Type Library (located in MDIVWCTL.DLL)
    /// C:\Program Files (x86)\Common Files\microsoft shared\MODI\12.0\MDIVWCTL.DLL
    /// </para>
    /// <para>
    /// http://support.microsoft.com/kb/982760
    /// </para>
    /// </remarks>
    public interface IMODIOCRService : IOCRService
    {

    }
}