using System.Collections.Generic;

namespace XAct.Imaging.OCR
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOCRService
    {
        /// <summary>
        /// OCRs the specified source image file name.
        /// </summary>
        /// <param name="sourceImageFileName">Name of the source image file.</param>
        /// <returns></returns>
        string[] OCR(string sourceImageFileName);
    }
}