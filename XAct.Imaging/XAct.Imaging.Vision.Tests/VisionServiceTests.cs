namespace NAMESPACENAME.Tests
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using NUnit.Framework;
    using XAct;
    using XAct.Drawing;
    using XAct.Environment;
    using XAct.Imaging.Vision;
    using XAct.Imaging.Vision.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class VisionServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

                //TEST:FAILING
        [Test]
        public void CanGetIVisionService()
        {
            //ARRANGE:
            IVisionService visionService = XAct.DependencyResolver.Current.GetInstance<IVisionService>();
            
            //ASSERT:
            Assert.IsNotNull(visionService);
        }

        [Test]
        public void CanGetIVisionServiceOfExpectedType()
        {
            //ARRANGE:
            IVisionService visionService = XAct.DependencyResolver.Current.GetInstance<IVisionService>();

            //ASSERT:
            Assert.AreEqual(typeof(VisionService),visionService.GetType());
        }

        [Test]
        public void CanFindTestDataFile()
        {
            //ARRANGE:
            IEnvironmentService environmentService = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ACT:
            string path = "TestData/Files/nmrEOKw.jpg";
            path = environmentService.MapPath(path);

            //ASSERT:
            Assert.IsTrue(File.Exists(path));

        }

        [Test]
        public void CanSpotSimilarityOfTwoImages()
        {
            //ARRANGE:
            IEnvironmentService environmentService = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>();
            IVisionService visionService = XAct.DependencyResolver.Current.GetInstance<IVisionService>();


            //ACT:
            string path = "TestData/Files/nmrEOKw.jpg";
            path = environmentService.MapPath(path);

            string path2 = "TestData/Files/Copy of nmrEOKw.jpg";
            path2 = environmentService.MapPath(path2);

            double result = visionService.CompareTwoImages(Image.FromFile(path),Image.FromFile(path2), ColorType.Color);
            Trace.WriteLine("Level:" + result);
            Assert.IsTrue(result > 0.9);
        }
    }
}


