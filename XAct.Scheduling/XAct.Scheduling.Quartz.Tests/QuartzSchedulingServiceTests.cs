namespace XAct.Scheduling.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Scheduling.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class QuartzSchedulingServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                        Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetSchedulerController()
        {
            ISchedulerController controller =
                XAct.DependencyResolver.Current.GetInstance<ISchedulerController>();

            Assert.IsNotNull(controller);
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetQuartzSchedulerController()
        {
            IQuartzSchedulerController controller =
                XAct.DependencyResolver.Current.GetInstance<IQuartzSchedulerController>();

            Assert.IsNotNull(controller);
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetQuartzSchedulerControllerOfExpectedType()
        {
            IQuartzSchedulerController controller =
                XAct.DependencyResolver.Current.GetInstance<IQuartzSchedulerController>();

            Assert.AreEqual(typeof(QuartzSchedulerController), controller.GetType());
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetManagementService()
        {
            ISchedulingManagementService schedulingManagementService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingManagementService>();

            Assert.IsNotNull(schedulingManagementService);
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetManagementServiceOfExpectedType()
        {

            ISchedulingManagementService schedulingManagementService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingManagementService>();

            Assert.AreEqual(typeof(SchedulingManagementService), schedulingManagementService.GetType());
        }



    }


}


