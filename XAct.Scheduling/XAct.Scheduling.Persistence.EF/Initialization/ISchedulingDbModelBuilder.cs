namespace XAct.Scheduling.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for the <see cref="IHasXActLibDbModelBuilder"/>
    /// specific to setting up XActLib Resources capabilities.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface ISchedulingDbModelBuilder : IHasXActLibDbModelBuilder
    {

    }
}