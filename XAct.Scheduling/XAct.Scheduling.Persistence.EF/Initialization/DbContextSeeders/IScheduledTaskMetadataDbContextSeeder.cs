namespace XAct.Scheduling.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// 
    /// </summary>
    public interface IScheduledTaskMetadataDbContextSeeder : IHasXActLibDbContextSeeder<ScheduledTaskMetadata>
    {


    }
}