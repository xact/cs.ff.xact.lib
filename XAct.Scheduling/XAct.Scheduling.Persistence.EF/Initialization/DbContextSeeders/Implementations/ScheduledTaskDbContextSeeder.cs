﻿namespace XAct.Scheduling.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;


    /// <summary>
    /// A default implementation of the <see cref="IScheduledTaskDbContextSeeder"/> contract
    /// to seed the Scheduling tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class ScheduledTaskDbContextSeeder : XActLibDbContextSeederBase<ScheduledTask>, IScheduledTaskDbContextSeeder
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledTaskDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ScheduledTaskDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}
