﻿namespace XAct.Scheduling.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class ScheduledTaskMetadataDbContextSeeder : XActLibDbContextSeederBase<ScheduledTaskMetadata>, IScheduledTaskMetadataDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledTaskMetadataDbContextSeeder"/> class.
        /// </summary>
        /// <param name="scheduledTaskDbContextSeeder">The scheduled task database context seeder.</param>
        public ScheduledTaskMetadataDbContextSeeder(IScheduledTaskDbContextSeeder scheduledTaskDbContextSeeder)
            : base(scheduledTaskDbContextSeeder)
        {
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}