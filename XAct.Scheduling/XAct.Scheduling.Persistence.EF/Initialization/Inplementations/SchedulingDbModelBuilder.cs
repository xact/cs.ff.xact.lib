﻿namespace XAct.Scheduling.Initialization.Inplementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Scheduling.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class SchedulingDbModelBuilder : ISchedulingDbModelBuilder
    {

        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SchedulingDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public SchedulingDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;
            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            //modelBuilder.Configurations.Add(....new ScheduledTaskTriggerMap.ScheduledTaskMap());

            modelBuilder.Configurations.Add((EntityTypeConfiguration<ScheduledTask>) XAct.DependencyResolver.Current.GetInstance<IScheduledTaskModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<ScheduledTaskMetadata>)XAct.DependencyResolver.Current.GetInstance<IScheduledTaskMetadataModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<ScheduledTaskExecutionLog>)XAct.DependencyResolver.Current.GetInstance<IScheduledTaskExecutionLogModelPersistenceMap>());

        }
    }
}

