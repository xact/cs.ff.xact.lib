﻿namespace XAct.Scheduling.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    

    /// <summary>
    /// 
    /// </summary>
    /// 
    public class ScheduledTaskMetadataModelPersistenceMap : EntityTypeConfiguration<ScheduledTaskMetadata>, IScheduledTaskMetadataModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledTaskMetadataModelPersistenceMap"/> class.
        /// </summary>
        public ScheduledTaskMetadataModelPersistenceMap()
        {
            this.ToXActLibTable("ScheduledTaskMetadata");
            



            this.HasKey(x => x.Id);

            int colOrder = 0;

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.ScheduledTaskFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            //this.Property(x => x.ScheduledTask)
            //    .hasIsRequired()
            //    .HasColumnOrder(1);


            this.Property(x => x.IsAMethodArgument)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.Order)
                .DefineRequiredOrder(colOrder++);

            this.Property(x => x.Key)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.SerializationMethod)
                .DefineRequiredSerializationMethod(colOrder++);

            this.Property(x => x.SerializedValueType)
                .DefineRequired1024CharSerializationValueType(colOrder++);

            this.Property(x => x.SerializedValue)
                .DefineOptional4000CharSerializationValue(colOrder++);

            this.Property(x => x.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;


            //this.HasRequired(b => b.ScheduledTask)
            //    .WithMany(i => i.Metadata)
            //    .HasForeignKey(b => b.ScheduledTaskFK);

        }
    }
}