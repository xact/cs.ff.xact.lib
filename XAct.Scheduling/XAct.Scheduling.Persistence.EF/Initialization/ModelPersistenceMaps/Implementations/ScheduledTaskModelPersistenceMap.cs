﻿namespace XAct.Scheduling.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class ScheduledTaskModelPersistenceMap : EntityTypeConfiguration<ScheduledTask>, IScheduledTaskModelPersistenceMap
    {
        /// <summary>
        /// Scheduleds the serialized task.
        /// </summary>
        public ScheduledTaskModelPersistenceMap()
        {
            this.ToXActLibTable("ScheduledTask");
            


            this.HasKey(x => x.Id);

            int colOrder = 0;
            int indexMember = 1;//1 based.

            this.Property(x=>x.ApplicationTennantId)
                .DefineRequiredApplicationTennantId(colOrder++)
            //this.Property(x => x.ApplicationTennantId)
            //    .IsRequired()
            //    .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id", indexMember++) { IsUnique = true }))
                ;

            

            this.Property(x => x.Id)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationTennantId_Id", indexMember++) { IsUnique = true }))
                ;

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.GroupName)
                .IsOptional()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;

            //EF doesn't work with TimeSpans.
            this.Ignore(x => x.StartDelay);

            this.Property(x => x.StartDelayTicks)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this.Property(x => x.ChronSchedule)
                .IsRequired()
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.IsExecutable)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.ExecutablePath)
                .IsOptional()
                .HasMaxLength(512)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.ExecutableArguments)
                .IsOptional()
                .HasMaxLength(4000)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.AssemblyName)
                .IsOptional()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.TypeFullName)
                .IsOptional()
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.MethodName)
                .IsOptional()
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;

            this.Property(x => x.DeletedOnUtc)
                .DefineOptionalDeletedOnUtc(colOrder++);

            
            this.HasMany(m => m.Metadata)
                .WithRequired(x => x.ScheduledTask)
                .HasForeignKey(m => m.ScheduledTaskFK);


        }
    }
}