﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Scheduling.Initialization.ModelPersistenceMaps.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public class ScheduledTaskExecutionLogModelPersistenceMap : EntityTypeConfiguration<ScheduledTaskExecutionLog>,
        IScheduledTaskExecutionLogModelPersistenceMap
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ScheduledTaskExecutionLogModelPersistenceMap()
        {
            this.ToXActLibTable("ScheduledTaskExecutionLog");
            



            this.HasKey(x => x.Id);

            int colOrder = 0;

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.ScheduledTaskFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.CreatedOnUtc)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Ignore(x => x.Duration)
                ;

            this.Property(x => x.DurationTicks)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;



            this.Property(x => x.ResultStatus)
                .DefineRequiredSerializationMethod(colOrder++);


            this.Property(x => x.Summary)
                .DefineOptional4000CharDescription(colOrder++)
                ;


        }
    }
}