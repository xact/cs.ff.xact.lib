﻿
namespace XAct.Scheduling
{
    /// <summary>
    /// A task that can be executed by the <see cref="ISchedulingManagementService"/>.
    /// </summary>
    public interface IScheduledTask :XAct.Commands.ICommandMessage, IHasNameAndDescription
    {

    }
}
