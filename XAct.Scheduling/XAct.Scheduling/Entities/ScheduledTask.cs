﻿//using System;
//namespace XAct.Scheduling.Entities
//{
//    using System.Collections.ObjectModel;
//    using System.Runtime.Serialization;

//    [DataContract]
//    class ScheduledTask: IHasId<int>, IHasEnabled, IHasNameAndDescription
//    {


//        /// <summary>
//        /// Gets or sets the identifier.
//        /// </summary>
//        /// <value>
//        /// The identifier.
//        /// </value>
//        [DataMember]
//        public virtual int Id { get; set; }

//        /// <summary>
//        /// Gets or sets a value indicating whether this object is enabled.
//        /// <para>Member defined in<see cref="IHasEnabled" /></para>
//        /// </summary>
//        /// <value>
//        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
//        /// </value>
//        [DataMember]
//        public virtual bool Enabled { get; set; }


//        /// <summary>
//        /// Gets or sets the name of the Group to which the Task belongs.
//        /// <para>
//        /// Optional.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The name of the group.
//        /// </value>
//        [DataMember]
//        public virtual string GroupName { get; set; }

//        /// <summary>
//        /// Gets the name of the Task.
//        /// <para>
//        /// Required.
//        /// </para>
//        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
//        /// </summary>
//        /// <value>
//        /// The name.
//        /// </value>
//        /// <internal>8/13/2011: Sky</internal>
//        [DataMember]
//        public virtual string Name { get; set; }

//        /// <summary>
//        /// Gets or sets the description of the task.
//        /// <para>Member defined in <see cref="XAct.IHasNameAndDescription" /></para>
//        /// </summary>
//        /// <value>
//        /// The description.
//        /// </value>
//        [DataMember]
//        public virtual string Description { get; set; }


//        /// <summary>
//        /// Gets or sets the amount of time to delay the start.
//        /// </summary>
//        /// <value>
//        /// The start delay.
//        /// </value>
//        public virtual TimeSpan StartDelay { get; set; }

//        /// <summary>
//        /// Gets or sets the start delay ticks.
//        /// </summary>
//        /// <value>
//        /// The start delay ticks.
//        /// </value>
//        /// <internal>
//        /// TimeSpan is not marked with DataMember, hence the underlying long value.
//        /// </internal>
//        public virtual long StartDelayTicks
//        {
//            get { return StartDelay.Ticks; }
//            set
//            {
//                _ticks = value;
//                StartDelay = new TimeSpan(_ticks); }
//        }
//        [DataMember] private long _ticks;

//        /// <summary>
//        /// Gets or sets the chron schedule on which to trigger the Job.
//        /// </summary>
//        /// <value>
//        /// The chron schedule.
//        /// </value>
//        [DataMember]
//        public virtual string ChronSchedule { get; set; }


//        /// <summary>
//        /// Gets or sets the Assembly qualified name of the Type that is serialized.
//        /// </summary>
//        /// <value>
//        /// The type.
//        /// </value>
//        /// <internal>8/16/2011: Sky</internal>
//        public virtual string TypeName
//        {
//            get { return _typeName; }
//            set
//            {
//                    //X(value);
//                    _typeName = value;
//            }
//        }
//        [DataMember]
//        private string _typeName;




//        /// <summary>
//        /// Gets the optional metadata associated to this task.
//        /// </summary>
//        /// <value>
//        /// The metadata.
//        /// </value>
//        [DataMember]
//        public virtual Collection<ScheduledSerializedTaskMetadata> Metadata
//        {
//            get { return _metadata ?? (_metadata = new Collection<ScheduledSerializedTaskMetadata>()); }
//        }
//        private Collection<ScheduledSerializedTaskMetadata> _metadata;



//    }
//}
