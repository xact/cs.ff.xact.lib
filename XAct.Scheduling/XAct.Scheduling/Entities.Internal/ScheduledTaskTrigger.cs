﻿//// ReSharper disable CheckNamespace
//namespace XAct.Scheduling
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Runtime.Serialization;

//    /// <summary>
//    /// Entity to contain the information required
//    /// to describe a scheduled Trigger for 
//    /// <see cref="ScheduledTask"/>.
//    /// <para>
//    /// A <see cref="ScheduledTask"/> can have more than one
//    /// intervals (eg,one every 10 minutes, one at midnight).
//    /// </para>
//    /// </summary>
//    [DataContract]
//    public class ScheduledTaskTrigger
//    {
//        /// <summary>
//        /// Gets or sets the Id of the
//        /// <see cref="ScheduledTask"/>
//        /// this <see cref="ScheduledTaskTrigger"/> is associated to.
//        /// </summary>
//        /// <value>
//        /// The scheduled task Id.
//        /// </value>
//        [DataMember]
//        public virtual Guid ScheduledTaskFK { get; set; }

//        /// <summary>
//        /// Gets or sets the chron schedule used to trigger the 
//        /// <see cref="ScheduledTask"/>
//        /// </summary>
//        /// <value>
//        /// The chron schedule.
//        /// </value>
//        [DataMember]
//        public string ChronSchedule { get; set; }

//    }
//}
