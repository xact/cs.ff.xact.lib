﻿// ReSharper disable CheckNamespace
namespace XAct.Scheduling
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// Entity to describe a Task
    /// </summary>
    [DataContract]
    public class ScheduledTask : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, IHasEnabled, IHasNameAndDescription, IHasAssemblyAndTypeAndMethodNames, IHasDateTimeDeletedOnUtc
    {




        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the Concurrency timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets the name of the Group to which the Task belongs.
        /// <para>
        /// Optional.
        /// </para>
        /// </summary>
        /// <value>
        /// The name of the group.
        /// </value>
        [DataMember]
        public virtual string GroupName { get; set; }

        /// <summary>
        /// Gets the name of the Task.
        /// <para>
        /// Required.
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the description of the task.
        /// <para>Member defined in <see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Gets or sets the amount of time to delay the start.
        /// </summary>
        /// <value>
        /// The start delay.
        /// </value>
        //NO:[DATAMEMBER]
        public virtual TimeSpan StartDelay { get; set; }

        /// <summary>
        /// Gets or sets the start delay ticks.
        /// </summary>
        /// <value>
        /// The start delay ticks.
        /// </value>
        [DataMember]
        public virtual long StartDelayTicks
        {
            get { return StartDelay.Ticks; }
            set { StartDelay = new TimeSpan(value); }
        }

        /// <summary>
        /// Gets or sets the chron schedule on which to trigger the Job.
        /// </summary>
        /// <value>
        /// The chron schedule.
        /// </value>
        [DataMember]
        public virtual string ChronSchedule { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether the task
        /// will be initializing an external executable, 
        /// or internal class.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is executable]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool IsExecutable { get; set; }


        /// <summary>
        /// If <see cref="IsExecutable"/> is true,
        /// gets or sets the executable path.
        /// </summary>
        /// <value>
        /// The executable path.
        /// </value>
        [DataMember]
        public virtual string ExecutablePath { get; set; }

        /// <summary>
        /// If <see cref="IsExecutable"/> is true,
        /// gets or sets the command line arguments to invoke
        /// <see cref="ExecutablePath"/> with.
        /// </summary>
        /// <value>
        /// The command line arguments.
        /// </value>
        [DataMember]
        public virtual string ExecutableArguments { get; set; }


        /// <summary>
        /// Gets or sets the name of the assembly within which 
        /// to find <see cref="TypeFullName"/>.
        /// </summary>
        /// <value>
        /// The name of the assembly.
        /// </value>
        [DataMember]
        public virtual string AssemblyName { get; set; }


        /// <summary>
        /// Gets or sets the Assembly qualified name of the Type that is serialized.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string TypeFullName { get; set; }


        /// <summary>
        /// Gets or sets the name of the method to invoke within
        /// <see cref="TypeFullName"/>.
        /// </summary>
        /// <value>
        /// The name of the method.
        /// </value>
        [DataMember]
        public virtual string MethodName { get; set; }



        /// <summary>
        /// Gets the optional metadata associated to this task.
        /// </summary>
        /// <value>
        /// The metadata.
        /// </value>
        [DataMember]
        public virtual Collection<ScheduledTaskMetadata> Metadata
        {
            get { return _metadata ?? (_metadata = new Collection<ScheduledTaskMetadata>()); }
            private set { _metadata = value; }
        }
        private Collection<ScheduledTaskMetadata> _metadata;


        /// <summary>
        /// Gets or sets the datetime the item was deleted, expressed in UTC.
        /// <para>
        /// Defined in the <see cref="XAct.IHasDateTimeDeletedOnUtc" /> contract.
        /// </para>
        /// <para>
        /// Note: Generally updated by a db trigger, or saved first, then deleted.
        /// </para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual DateTime? DeletedOnUtc { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledTask"/> class.
        /// </summary>
        public ScheduledTask()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{0} [Id:{1}] [{2}/{3}] [{4}] [{5}] [{6}/{7}] [{8}/{9}]".FormatStringInvariantCulture(
                base.ToString(),
                this.Id,
                this.GroupName, this.Name,
                this.ChronSchedule,
                IsExecutable,
                ExecutablePath, ExecutableArguments,
                TypeFullName, MethodName
                );
        }
    }
}