﻿// ReSharper disable CheckNamespace
namespace XAct.Scheduling
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class ScheduledTaskSummary
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual bool Scheduled { get; set; }



        /// <summary>
        /// Gets or sets the last time run.
        /// </summary>
        /// <para>
        /// Not sure how I can record this, as we don't know when the scheduler invokes the job...
        /// </para>
        public DateTime LastTimeRun { get; set; }
    }

}
