﻿// ReSharper disable CheckNamespace
namespace XAct.Scheduling
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Optional values to set into the context
    /// for the <see cref="ScheduledTask"/> to use.
    /// </summary>
    [DataContract]
    public class ScheduledTaskMetadata : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasEnabled, IHasKey, IHasOrder, IHasSerializedTypeValueAndMethod, IHasDescription
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the Id of the
        /// <see cref="ScheduledTask"/>
        /// this <see cref="ScheduledTaskMetadata"/> is associated to.
        /// </summary>
        [DataMember]
        public virtual Guid ScheduledTaskFK { get; set; }


        /// <summary>
        /// Gets or sets the scheduled task 
        /// this metadata element belongs to.
        /// </summary>
        /// <value>
        /// The scheduled task.
        /// </value>
        [DataMember]
        public virtual ScheduledTask ScheduledTask { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }





        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// metadata is to be treated as an argument
        /// intended when invoking the ScheduledTask's
        /// MethodInfo.
        /// <para>
        /// If so, ensure the arguments are in the right <see cref="Order"/>
        /// </para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is method argument]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool IsAMethodArgument { get; set; }


        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember]
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets or sets the serialization method.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The serialization method.
        /// </value>
        [DataMember]
        public virtual SerializationMethod SerializationMethod { get; set; }

        /// <summary>
        /// Gets or sets the Assembly qualified name of the Value that is serialized.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValueType { get; set; }

        /// <summary>
        /// Gets or sets the serialized value.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValue { get; set; }


        /// <summary>
        /// Gets or sets the description of this trigger.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduledTaskMetadata"/> class.
        /// </summary>
        public ScheduledTaskMetadata()
        {
            this.GenerateDistributedId();
        }
    }
}