using System;
using System.Runtime.Serialization;
using XAct.Diagnostics;

namespace XAct.Scheduling
{
    /// <summary>
    /// An entity to record the execution of tasks.
    /// </summary>
    [DataContract]
    public class ScheduledTaskExecutionLog : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, 
        IHasDateTimeCreatedOnUtc

    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the Id of the <see cref="ScheduledTask"/> this record describes.
        /// </summary>
        /// <value>
        /// The scheduled task fk.
        /// </value>
        public virtual Guid ScheduledTaskFK { get; set; }

        /// <summary>
        /// Gets or sets the Concurrency timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the Duration of the Task.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        public virtual long DurationTicks { get; set; }

        /// <summary>
        /// Gets the duration.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        [DataMember]
        public virtual TimeSpan Duration {
            get
            {
                return  TimeSpan.FromTicks(DurationTicks);
            } 
        }
        /// <summary>
        /// Gets or sets the trace level.
        /// (1=Error, 2=Warning, 3=Info, 4=Verbose.
        /// </summary>
        [DataMember]
        public virtual ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets an optional (no need to write anything if everything was fine).
        /// </summary>
        [DataMember]
        public string Summary { get; set; }


        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        /// </internal>
        /// <internal>
        /// As to why its Nullable: sometimes the contract is applied to items
        /// that are not Entities themselves, but pointers to objects that are not known
        /// if they are
        /// </internal>
        /// <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        /// </internal>
        [DataMember]
        public DateTime? CreatedOnUtc { get; set; }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="ScheduledTaskExecutionLog"/> class.
        /// </summary>
        public ScheduledTaskExecutionLog()
        {
            this.GenerateDistributedId();
        }

    }
}