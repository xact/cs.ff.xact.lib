﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XAct.Diagnostics;

namespace XAct.Scheduling.Services
{
    /// <summary>
    /// A contract for service with which to 
    /// record information about the invocation of a scheduled job.
    /// <para>
    /// Note: Invoked by AbstractQuartzJob.
    /// </para>
    /// </summary>
    public interface ISchedulingReportingService :IHasXActLibService
    {
        /// <summary>
        /// Create a message to associate to the job.
        /// </summary>
        /// <param name="jobIdentifier">The job identifier.</param>
        /// <param name="timeSpan">The time span.</param>
        /// <param name="resultStatus">The result status.</param>
        /// <param name="message">The message.</param>
        void Trace(Guid jobIdentifier, TimeSpan timeSpan, ResultStatus resultStatus, string message);
    }
}
