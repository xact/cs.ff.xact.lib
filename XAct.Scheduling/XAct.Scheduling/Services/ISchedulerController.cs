// ReSharper disable CheckNamespace
namespace XAct.Scheduling
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Contract for an abstract schedule engine controller.
    /// <para>
    /// The contract the scheduler does not concern itself
    /// with how the scheduler does the work.
    /// </para>
    /// <para>
    /// See also:IQuartzSChedulerController.
    /// </para>
    /// </summary>
    public interface ISchedulerController : IHasInnerItemReadOnly
    {

        /// <summary>
        /// Start the scheduler.
        /// <para>
        /// If the first time, will initialize the scheduler with a list of task definitions.
        /// </para>
        /// </summary>
        void Start();

        /// <summary>
        /// Pause the scheduler, and therefore the triggering
        /// of all <see cref="ScheduledTask"/>s.
        /// </summary>
        void Standby();


        /// <summary>
        /// Resumes the scheduler, and therefore the triggering
        /// of all <see cref="ScheduledTask"/>.
        /// </summary>
        void Resume();

        /// <summary>
        /// Stop the scheduler, and therefore the triggering
        /// of all <see cref="ScheduledTask" />s.
        /// </summary>
        /// <param name="waitForJobsToComplete">if set to <c>true</c> [wait for jobs to complete].</param>
        void Stop(bool waitForJobsToComplete = false);


        /// <summary>
        /// Registers new <see cref="ScheduledTask"/>s
        /// into the current internal
        ///  <c>Scheduler</c>.
        /// <para>
        /// Ensure that <see cref="Start"/>
        /// is invoked at some point as just 
        /// Registering the <see cref="ScheduledTask"/>
        /// does not automatically mean that they are being
        /// triggered, yet.
        /// </para>
        /// </summary>
        /// <param name="scheduledTasks">The scheduled tasks.</param>
        void RegisterTask(params ScheduledTask[] scheduledTasks);



        /// <summary>
        /// Pauses a registered task if it is found.
        /// <para>
        /// No exception is raised if not found (it's up to the invokee to get it right).
        /// </para>
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        void PauseTask(ScheduledTask scheduledTask);

        /// <summary>
        /// Resumes a paused task if it is found.
        /// <para>
        /// No exception is raised if not found (it's up to the invokee to get it right).
        /// </para>
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        void ResumeTask(ScheduledTask scheduledTask);

        /// <summary>
        /// Removes the task if it is found.
        /// <para>
        /// No exception is raised if not found (it's up to the invokee to get it right).
        /// </para>
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        void RemoveTask(ScheduledTask scheduledTask);

        /// <summary>
        /// Removes the task if it is found.
        /// <para>
        /// No exception is raised if not found (it's up to the invokee to get it right).
        /// </para>
        /// </summary>
        /// <param name="scheduledTaskId">The scheduled task identifier.</param>
        void RemoveTask(Guid scheduledTaskId);

        /// <summary>
        /// Removes the task if it is found.
        /// <para>
        /// No exception is raised if not found (it's up to the invokee to get it right).
        /// </para>
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="group">The group.</param>
        void RemoveTask(string name, string group=null);


        /// <summary>
        /// Checks whether a task has been registered.
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        bool CheckExists(ScheduledTask scheduledTask);



        /// <summary>
        /// Gets a current snapshot of the 
        /// metadata passed between scheduler and job instances.
        /// <para>
        /// CAUTION:
        /// Scalar variables (eg: counters) will be fixed and at
        /// time of retrieval, and therefore safe to work with, 
        /// but reference types may continue to be updated.
        /// </para>
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        /// <returns></returns>
        Dictionary<string, object> GetJobDetails(ScheduledTask scheduledTask);


        /// <summary>
        /// Gets a current snapshot of the 
        /// metadata passed between scheduler and job instances.
        /// <para>
        /// CAUTION:
        /// Scalar variables (eg: counters) will be fixed at
        /// time of retrieval, and therefore safe to work with, 
        /// but reference types may continue to be updated 
        /// while they are being looked at (caution...)
        /// </para>
        /// </summary>
        /// <param name="scheduledTaskId">The scheduled task's Id.</param>
        /// <returns></returns>
        Dictionary<string, object> GetJobMetadata(Guid scheduledTaskId);


        /// <summary>
        /// Gets a current snapshot of the
        /// metadata passed between scheduler and job instances.
        /// <para>
        /// CAUTION:
        /// Scalar variables (eg: counters) will be fixed at
        /// time of retrieval, and therefore safe to work with,
        /// but reference types may continue to be updated 
        /// while they are being looked at (caution...)
        /// </para>
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        Dictionary<string, object> GetJobDetails(string name, string group = null);
    }
}