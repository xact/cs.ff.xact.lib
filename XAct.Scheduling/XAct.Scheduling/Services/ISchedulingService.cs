﻿namespace XAct.Scheduling
{
    using System;


    /// <summary>
    /// Contract for a service to manage <see cref="ScheduledTask"/>s.
    /// </summary>
    public interface ISchedulingService : IHasXActLibService, IHasInitialize
    {

        /// <summary>
        /// Gets the singleton configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        ISchedulingServiceConfiguration Configuration { get; }



        /// <summary>
        /// Gets the schedules within the group, or all if no group specified.
        /// </summary>
        /// <param name="group">The group.</param>
        ScheduledTask[] GetSchedules(string group = null);



        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ScheduledTask GetSchedule(Guid id);

        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        /// <returns></returns>
        ScheduledTask GetSchedule(string name, string group = null);



        /// <summary>
        /// Adds the scheduled task.
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        void AddScheduledTask(ScheduledTask scheduledTask);

        /// <summary>
        /// Deletes the scheduled task.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteScheduledTask(Guid id);

        /// <summary>
        /// Deletes the scheduled task.
        /// </summary>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        void DeleteScheduledTask(string name,string group=null);



    }
}