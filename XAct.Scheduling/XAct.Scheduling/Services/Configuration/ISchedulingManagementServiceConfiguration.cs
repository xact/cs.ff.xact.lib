﻿namespace XAct.Scheduling
{
    /// <summary>
    /// Configuration for the configuration element
    /// injected into an instance of
    /// <see cref="ISchedulingService"/>
    /// </summary>
    public interface ISchedulingManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {
    }
}
