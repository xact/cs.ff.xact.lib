﻿namespace XAct.Scheduling
{
    using System;

    /// <summary>
    /// Contract for a service to manage <see cref="ScheduledTask"/>s.
    /// </summary>
    public interface ISchedulingManagementService: IHasInitializedReadOnly, IHasXActLibService
    {

        /// <summary>
        /// Gets the singleton scheduler that this service manages.
        /// </summary>
        /// <value>
        /// The scheduler.
        /// </value>
        ISchedulerController Scheduler { get; }


        /// <summary>
        /// Initializes the specified application tennant identifier.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        void Initialize(Guid applicationTennantId);

        /// <summary>
        /// Gets the singleton configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        ISchedulingManagementServiceConfiguration Configuration { get; }

        /// <summary>
        /// Gets the schedules within the group, or all if no group specified.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="group">The group.</param>
        ScheduledTask[] GetSchedules(Guid applicationTennantId, string group = null);

        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        ScheduledTask GetSchedule(Guid applicationTennantId, Guid id);


        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        ScheduledTask GetSchedule(Guid applicationTennantId, string name, string group = null);



        /// <summary>
        /// Adds the scheduled task, and starts it.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="scheduledTask">The scheduled task.</param>
        void AddScheduledTask(Guid applicationTennantId, ScheduledTask scheduledTask);



        /// <summary>
        /// Deletes the scheduled task, and removes it from the scheduler.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="id">The identifier.</param>
        void DeleteScheduledTask(Guid applicationTennantId, Guid id);




        /// <summary>
        /// Deletes the scheduled task, and removes it from the scheduler.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        void DeleteScheduledTask(Guid applicationTennantId, string name, string group = null);





    }
}
