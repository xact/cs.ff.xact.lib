﻿
namespace XAct.Scheduling.Implementations
{
    using System;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="ISchedulingService"/>
    /// to manage <see cref="ScheduledTask"/>s
    /// using <c>.NET</c>
    /// </summary>
    public class SchedulingService : ISchedulingService
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly ISchedulingServiceConfiguration _schedulingServiceConfiguration;
        private readonly ISchedulingManagementService _schedulingManagementService;


        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get; private set; }


        ISchedulingServiceConfiguration ISchedulingService.Configuration
        {
            get
            {
                return _schedulingServiceConfiguration;
            }
        }

        /// <summary>
        /// Gets the singleton configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public ISchedulingServiceConfiguration Configuration
        {
            get
            {
                return _schedulingServiceConfiguration;
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="SchedulingService" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="quartzSchedulingServiceConfiguration">The quartz scheduling service configuration.</param>
        /// <param name="schedulingManagementService">The quartz scheduling management service.</param>
        public SchedulingService(
            IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService,
            ISchedulingServiceConfiguration quartzSchedulingServiceConfiguration,
            ISchedulingManagementService schedulingManagementService)
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
            _schedulingServiceConfiguration = quartzSchedulingServiceConfiguration;
            _schedulingManagementService = schedulingManagementService;
        }


        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void Initialize()
        {
            _schedulingManagementService.Initialize(_applicationTennantService.Get());
        }

        /// <summary>
        /// Adds the scheduled task.
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        public void AddScheduledTask(ScheduledTask scheduledTask)
        {
            _schedulingManagementService.AddScheduledTask(_applicationTennantService.Get(),scheduledTask);
        }

        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ScheduledTask GetSchedule(Guid id)
        {
            return _schedulingManagementService.GetSchedule(_applicationTennantService.Get(), id);
        }

        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        /// <returns></returns>
        public ScheduledTask GetSchedule(string name, string @group = null)
        {
            return _schedulingManagementService.GetSchedule(_applicationTennantService.Get(), name, group);
        }

        /// <summary>
        /// Gets the schedules within the group, or all if no group specified.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        public ScheduledTask[] GetSchedules(string group = null)
        {
            return _schedulingManagementService.GetSchedules(_applicationTennantService.Get(), group);
        }


        /// <summary>
        /// Deletes the scheduled task.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteScheduledTask(Guid id)
        {

            _schedulingManagementService.DeleteScheduledTask(_applicationTennantService.Get(), id);
        }

        /// <summary>
        /// Deletes the scheduled task.
        /// </summary>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        public void DeleteScheduledTask(string name, string group = null)
        {
            _schedulingManagementService.DeleteScheduledTask(_applicationTennantService.Get(), name, group);
        }



    }
}
