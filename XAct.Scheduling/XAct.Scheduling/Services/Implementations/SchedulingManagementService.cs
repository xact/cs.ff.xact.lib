namespace XAct.Scheduling.Implementations
{
    using System;
    using System.Linq;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;


    /// <summary>
    /// Implementation of the 
    /// <see cref="ISchedulingManagementService"/>
    /// to manage <see cref="ScheduledTask"/>s
    /// </summary>
    public class SchedulingManagementService : ISchedulingManagementService
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;
        private readonly ISchedulingManagementServiceConfiguration _quartzSchedulingManagementServiceConfiguration;
        private readonly ISchedulerController _schedulerController;



        /// <summary>
        /// Gets the application tennant identifier.
        /// </summary>
        /// <value>
        /// The application tennant identifier.
        /// </value>
        private Guid ApplicationTennantId
        {
            get { return _applicationTennantService.Get(); }
        }


        /// <summary>
        /// Gets the singleton scheduler that this service manages.
        /// </summary>
        /// <value>
        /// The scheduler.
        /// </value>
        public ISchedulerController Scheduler
        {
            get { return _schedulerController; }
        }



        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get; private set; }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void Initialize(Guid applicationTennantId)
        {
            foreach (ScheduledTask scheduledTask in this.GetSchedules(applicationTennantId))
            {
                _schedulerController.RegisterTask(scheduledTask);
            }
            Initialized = true;
        }



        ISchedulingManagementServiceConfiguration ISchedulingManagementService.Configuration
        {
            get { return _quartzSchedulingManagementServiceConfiguration; }
        }

        /// <summary>
        /// Gets the singleton configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public ISchedulingManagementServiceConfiguration Configuration
        {
            get { return _quartzSchedulingManagementServiceConfiguration; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="SchedulingManagementService" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="quartzSchedulingManagementServiceConfiguration">The quartz scheduling management service configuration.</param>
        /// <param name="schedulerController">The scheduler controller.</param>
        public SchedulingManagementService(
            IEnvironmentService environmentService,
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService,
            ISchedulingManagementServiceConfiguration quartzSchedulingManagementServiceConfiguration,
            ISchedulerController schedulerController)
        {
            _environmentService = environmentService;
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
            _quartzSchedulingManagementServiceConfiguration = quartzSchedulingManagementServiceConfiguration;
            _schedulerController = schedulerController;
        }



        /// <summary>
        /// Gets the schedules within the group, or all if no group specified.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        public ScheduledTask[] GetSchedules(Guid applicationTennantId, string @group = null)
        {
            if (group == null)
            {
                return _repositoryService.GetByFilter<ScheduledTask>(
                    x =>
                    (
                        (x.ApplicationTennantId == applicationTennantId)
                    ),
                    new IncludeSpecification<ScheduledTask>(x => x.Metadata)
                    ).ToArray();
            }
                // ReSharper disable RedundantIfElseBlock
            else //clearer:
                // ReSharper restore RedundantIfElseBlock
            {
                return _repositoryService.GetByFilter<ScheduledTask>(
                    x =>
                    (
                        (x.ApplicationTennantId == applicationTennantId)
                        &&
                        (group == null ? x.GroupName == null : x.GroupName == group)
                    ),
                    new IncludeSpecification<ScheduledTask>(x => x.Metadata)
                    ).ToArray();
            }
        }



        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ScheduledTask GetSchedule(Guid applicationTennantId, Guid id)
        {
            return _repositoryService.GetSingle<ScheduledTask>(
                x =>
                (
                    (x.ApplicationTennantId == applicationTennantId)
                    &&
                    (x.Id == id)
                ),
                new IncludeSpecification<ScheduledTask>(x => x.Metadata)

                );
        }

        /// <summary>
        /// Gets the specified schedule.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        /// <returns></returns>
        public ScheduledTask GetSchedule(Guid applicationTennantId, string name, string @group = null)
        {
            return _repositoryService.GetSingle<ScheduledTask>(
                x =>
                (
                    (x.ApplicationTennantId == applicationTennantId)
                    &&
                    (group == null ? x.GroupName == null : x.GroupName == group)
                    &&
                    (x.Name == name)
                ),
                new IncludeSpecification<ScheduledTask>(x => x.Metadata)
                );

        }


        /// <summary>
        /// Adds the scheduled task.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="scheduledTask">The scheduled task.</param>
        public void AddScheduledTask(Guid applicationTennantId, ScheduledTask scheduledTask)
        {
            //Update scheduler:
            _schedulerController.RegisterTask(scheduledTask);

            //Update datastore:
            //bool isNew = _repositoryService.IsNew<ScheduledTask,Guid>(scheduledTask,true);

            _repositoryService.PersistOnCommit<ScheduledTask, Guid>(scheduledTask, 
                _repositoryService.IsNew<ScheduledTask, Guid>(scheduledTask, true));

        }

        /// <summary>
        /// Deletes the scheduled task.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="id">The identifier.</param>
        public void DeleteScheduledTask(Guid applicationTennantId, Guid id)
        {
            //Update scheduler:
            _schedulerController.RemoveTask(id);

            //Update datastore:
            _repositoryService.DeleteOnCommit<ScheduledTask>(x =>  x.Id == id);
        }

        /// <summary>
        /// Deletes the scheduled task.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="name">The task's required name.</param>
        /// <param name="group">The task's optional grouping name.</param>
        public void DeleteScheduledTask(Guid applicationTennantId, string name, string group = null)
        {
            //Update scheduler:
            _schedulerController.RemoveTask(name, group);

            //Update datastore:
            _repositoryService.DeleteOnCommit<ScheduledTask>(
                x =>
                (
                    (x.ApplicationTennantId == applicationTennantId)
                    &&
                    (group == null ? x.GroupName == null : x.GroupName == group)
                    &&
                    (x.Name == name)
                )
                );

        }

    }
}