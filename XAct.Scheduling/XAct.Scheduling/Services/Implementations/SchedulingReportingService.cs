using System;
using XAct.Diagnostics;
using XAct.Domain.Repositories;
using XAct.Environment;
// ReSharper disable RedundantCatchClause

namespace XAct.Scheduling.Services.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ISchedulingReportingService"/>
    /// to record information about the invocation of a scheduled job.
    /// </summary>
    public class SchedulingReportingService : ISchedulingReportingService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SchedulingReportingService" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public SchedulingReportingService(IDateTimeService dateTimeService, IRepositoryService repositoryService)
        {
            _dateTimeService = dateTimeService;
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Create a message to associate to the job.
        /// </summary>
        /// <param name="jobIdentifier">The job identifier.</param>
        /// <param name="duration">The duration.</param>
        /// <param name="resultStatus">The result status.</param>
        /// <param name="message">The message.</param>
        public void Trace(Guid jobIdentifier, TimeSpan duration, ResultStatus resultStatus, string message)
        {

            try
            {

                ScheduledTaskExecutionLog scheduledTaskExecutionLog = new ScheduledTaskExecutionLog();

                    scheduledTaskExecutionLog.CreatedOnUtc = _dateTimeService.NowUTC;
                    scheduledTaskExecutionLog.ScheduledTaskFK = jobIdentifier;
                    scheduledTaskExecutionLog.ResultStatus = resultStatus;
                    scheduledTaskExecutionLog.DurationTicks = duration.Ticks;
                    scheduledTaskExecutionLog.Summary = message;

                    _repositoryService.PersistOnCommit<ScheduledTaskExecutionLog, Guid>(scheduledTaskExecutionLog, true);

            }
                // ReSharper disable once UnusedVariable
            catch (System.Exception e)
            {
                var x = e;
                throw;
            }

        }
    }
}