﻿namespace XAct.Scheduling.Entities
{
    using System;
    using System.Reflection;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class AbstractQuarzJobInvocationInformation
    {
        /// <summary>
        /// Gets or sets the original <see cref="ScheduledTask"/>'s Id.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="ScheduledTask"/>'s  of the group.
        /// </summary>
        /// <value>
        /// The name of the group.
        /// </value>
        [DataMember]
        public string GroupName { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="ScheduledTask"/>'s name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets InstanceType.
        /// </summary>
        [DataMember]
        public Type InstanceType { get; set; }
        /// <summary>
        /// Gets or sets the instanciation of <see cref="InstanceType"/>.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        [DataMember]
        public object Instance { get; set; }

        /// <summary>
        /// Gets or sets the method to invoke on <see cref="Instance"/>.
        /// </summary>
        /// <value>
        /// The method information.
        /// </value>
        [DataMember]
        public MethodInfo MethodInfo { get; set; }

        /// <summary>
        /// Gets or sets optional methods arguments to invoke <see cref="MethodInfo"/> with.
        /// </summary>
        /// <value>
        /// The methods arguments.
        /// </value>
        [DataMember]
        public object[] MethodsArguments { get; set; }
    }
}