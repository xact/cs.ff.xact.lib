﻿
using XAct.Domain.Repositories;
using XAct.Scheduling.Services;
using XAct.Settings;

namespace XAct.Scheduling.Entities
{
    using System;
    using System.Reflection;
    using Quartz;
    using XAct;
    using XAct.Diagnostics;


    /// <summary>
    /// 
    /// </summary>
    [PersistJobDataAfterExecution]
    public class AbstractQuartzJob : Quartz.IJob
    {

        /// <summary>
        /// Key to use when transfering parameters.
        /// </summary>
        public const string C_DATAMAP_INVOCATIONCOUNTER_KEY = "InvocationCounter";

        /// <summary>
        /// Key to use when transfering parameters.
        /// </summary>
        public const string C_DATAMAP_INVOCATIONLASTDATETIMEUTCINVOKED_KEY = "InvocationLastDateTime";

        /// <summary>
        /// Key to use when transfering parameters.
        /// </summary>
        public const string C_DATAMAP_INVOCATION_INFO_KEY = "InvocationInfo";
         
        /// <summary>
        /// Key to use when transfering parameters.
        /// </summary>
        public const string C_DATAMAP_LASTEXCEPTION_KEY = "Exception";

        //Unfortunately, this is new every time triggered:
        private object _targetInstance;
        private MethodInfo _targetMethodInfo;
        private object[] _targetMethodArguments;


        /// <summary>
        /// Gets the reporting service to record exceptions when trying to invoke
        /// the specified job.
        /// </summary>
        /// <value>
        /// The scheduling reporting service.
        /// </value>
        protected ISchedulingReportingService SchedulingReportingService
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<ISchedulingReportingService>(false);
            }
        }

        private ITracingService TracingService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<ITracingService>(false); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractQuartzJob"/> class.
        /// </summary>
        public AbstractQuartzJob()
        {
        }




        /// <summary>
        /// Called by the <see cref="T:Quartz.IScheduler" /> when a <see cref="T:Quartz.ITrigger" />
        /// fires that is associated with the <see cref="T:Quartz.IJob" />.
        /// </summary>
        /// <param name="context">The execution context.</param>
        /// <remarks>
        /// The implementation may wish to set a  result object on the
        /// JobExecutionContext before this method exits.  The result itself
        /// is meaningless to Quartz, but may be informative to
        /// <see cref="T:Quartz.IJobListener" />s or
        /// <see cref="T:Quartz.ITriggerListener" />s that are watching the job's
        /// execution.
        /// </remarks>
        public void Execute(IJobExecutionContext context)
        {
            using (TimedScope timedScope = new TimedScope())
            {
                ITracingService tracingService = TracingService;

                //Pick up variables.
                //There are two dictionaries. Merged and non-merged.
                //One is specific to this instance/time/moment, and the other is
                //shared across all instances.

                //Pick up merged one -- which is shared by all instances of this job:
                JobDataMap mergedJobDataMap = context.MergedJobDataMap;
                int counter = mergedJobDataMap.GetIntValue(C_DATAMAP_INVOCATIONCOUNTER_KEY);

                //For the non merged -- ie, specific to this job only:
                JobDataMap nonMergedDataMap = context.JobDetail.JobDataMap;
                nonMergedDataMap.Put(C_DATAMAP_INVOCATIONCOUNTER_KEY, counter + 1);
                nonMergedDataMap.Put(C_DATAMAP_INVOCATIONLASTDATETIMEUTCINVOKED_KEY, DateTime.UtcNow);



                //As contructor invoked every time, this approach won't work:
                //if (_target == null){_target = System.Activator.CreateInstance((Type)mergedJobDataMap["Type"]);}
                //Instead:

                AbstractQuarzJobInvocationInformation abstractQuarzJobInvocationInformation =
                    mergedJobDataMap[C_DATAMAP_INVOCATION_INFO_KEY] as AbstractQuarzJobInvocationInformation;

                string groupName = abstractQuarzJobInvocationInformation.GroupName;
                string name = abstractQuarzJobInvocationInformation.Name;

                _targetInstance = abstractQuarzJobInvocationInformation.Instance;
                _targetMethodInfo = abstractQuarzJobInvocationInformation.MethodInfo;
                _targetMethodArguments = abstractQuarzJobInvocationInformation.MethodsArguments;

                try
                {
                    //Clear out the exception:
                    nonMergedDataMap.Put(C_DATAMAP_LASTEXCEPTION_KEY, null);

                    //Try to invoke the method.
                    //Hopefully it's an ICommand:
                    if (_targetMethodInfo == null)
                    {
                        ((XAct.IHasExecutableAction) _targetInstance).Execute();
                    }
                    else
                    {
                        //Otherwise, no problem, just use Generic invocation:
                        if ((_targetMethodArguments != null) && (_targetMethodArguments.Length > 0))
                        {
                            _targetMethodInfo.Invoke(_targetInstance, _targetMethodArguments);
                        }
                        else
                        {
                            _targetMethodInfo.Invoke(_targetInstance, null);
                        }
                    }
                    int threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;

                    System.Diagnostics.Trace.WriteLine(TraceLevel.Verbose, "-----------");
                    System.Diagnostics.Trace.WriteLine(TraceLevel.Verbose,
                        "[]Thread:{0}".FormatStringInvariantCulture(threadId));

                    using (var x = new UnitOfWorkThreadScope())
                    {

                        var rs = XAct.DependencyResolver.Current.GetInstance<ISchedulingReportingService>(false);

                        if (rs != null)
                        {
                            rs.Trace(
                                abstractQuarzJobInvocationInformation.Id,
                                timedScope.Elapsed,
                                ResultStatus.Success,
                                "Invoked.");
                        }
                    }
                    System.Diagnostics.Trace.WriteLine(TraceLevel.Verbose, "-----------");
                }
                    //catch (Quartz.JobExecutionException jee)
                    //{
                    //    jee.RefireImmediately = false;
                    //    jee.UnscheduleAllTriggers = false;
                    //    jee.UnscheduleFiringTrigger = false;
                    //    //@@@
                    //}
                catch (System.Exception e)
                {
                    string typeName = (_targetMethodInfo != null) ? _targetMethodInfo.Name : "NULL";
                    string msg =
                        "Error while Executing ScheduledTask (Id:'{0}' [{1}/{2}]), invoking targetMethodInfo: {3}"
                            .FormatStringInvariantCulture(
                                abstractQuarzJobInvocationInformation.Id,
                                abstractQuarzJobInvocationInformation.GroupName,
                                abstractQuarzJobInvocationInformation.Name,
                                typeName
                            );

                    if (tracingService != null)
                    {
                        tracingService.Trace(TraceLevel.Error, "----");
                        tracingService.TraceException(
                            TraceLevel.Error,
                            e,
                            msg);
                    }
                    using (var x = new UnitOfWorkThreadScope())
                    {

                        var rs = XAct.DependencyResolver.Current.GetInstance<ISchedulingReportingService>(false);

                        if (rs != null)
                        {
                            rs.Trace(
                                abstractQuarzJobInvocationInformation.Id,
                                timedScope.Elapsed,
                                ResultStatus.Fail,
                                e.ToLogString(msg));

                            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();
                        }
                    }
                    nonMergedDataMap.Put(C_DATAMAP_LASTEXCEPTION_KEY, e);


                }
            }
            //And good to know that it doesn't fall apart if calling non existent var:
            //object o2 = mergedJobDataMap["SomeNonExistentVar"];
            
            //Console.WriteLine(context.JobDetail.Key);
            //Console.WriteLine(context.JobDetail.Description);


            //If you want to use the same object every time:
            //Console.WriteLine(context.JobDetail.PersistJobDataAfterExecution);
        }
    }
}



