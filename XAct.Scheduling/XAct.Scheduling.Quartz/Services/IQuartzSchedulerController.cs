namespace XAct.Scheduling
{
    /// <summary>
    /// A <c>Quartz.NET</c> specific 
    /// implementation of the 
    /// <see cref="ISchedulerController"/>
    /// contract.
    /// </summary>
    public interface IQuartzSchedulerController :ISchedulerController 
    {
        
    }
}