﻿//namespace XAct.Scheduling
//{
//    using System;

//    public class SchedulingManagementService : ISchedulingManagementService
//    {
//        public bool Initialized { get; private set; }
//        public void Initialize()
//        {
//            throw new NotImplementedException();
//        }

//        public void AddScheduledTask(ScheduledTask scheduledTask)
//        {
//            throw new NotImplementedException();
//        }

//        public void DeleteScheduledTask(Guid id)
//        {
//            throw new NotImplementedException();
//        }

//        public void DeleteScheduledTask(string @group, string name)
//        {
//            throw new NotImplementedException();
//        }

//        void SetExample()
//        {
//using System;
//    using Quartz;
//    using Quartz.Impl;
 
//    class Program
//    {
//        static void Main(string[] args)
//        {
//  // construct a scheduler factory
//            ISchedulerFactory schedFact = new StdSchedulerFactory();
 
//            // get a scheduler
//            IScheduler sched = schedFact.GetScheduler();
//            sched.Start();
 
//            // construct job info
//            IJobDetail jobDetail = JobBuilder.Create<AbstractJob>()
//                .WithIdentity("myJob")
//                .Build();
 
//            jobDetail.JobDataMap["SomeVar"] = "Hi!";
//            jobDetail.JobDataMap["Type"] = typeof(SomeOperation);
//            jobDetail.JobDataMap["Instance"] = System.Activator.CreateInstance(typeof(SomeOperation));
 
//            //Create a Trigger:
//            ITrigger trigger = 
//                TriggerBuilder.Create()
//                //.ForJob(jobDetail)
//                .WithCronSchedule("0/1 * * * * ?").StartNow().Build();
 
 
 
 
 
//            sched.ScheduleJob(jobDetail, trigger);
//            Console.ReadLine();
//        }
//    }
 
//    [PersistJobDataAfterExecution]
//    class AbstractJob : Quartz.IJob
//    {
//        //Unfortunately, this is new every time:
//        private IHasCommand _target;
 
//        public AbstractJob()
//        {
//            Console.WriteLine("Note that new instance is created every time Trigger goes off.");
//        }
 
//        public void Execute(IJobExecutionContext context)
//        {
//            //Pick up variables:
//            JobDataMap mergedJobDataMap = context.MergedJobDataMap;
 
//            int counter = mergedJobDataMap.GetIntValue("SomeCounter");
//            context.JobDetail.JobDataMap.Put("SomeCounter", counter + 1);
//            Console.WriteLine(counter);
 
 
//            //As contructor invoked every time, this approach won't work:
//            //if (_target == null){_target = System.Activator.CreateInstance((Type)mergedJobDataMap["Type"]);}
//            //Instead:
//            ((IHasCommand)mergedJobDataMap["Instance"]).Execute();
 
//            //And good to knwo that it doesn't fall apart if calling non existent var:
//            object o2 = mergedJobDataMap["SomeNonExistentVar"];
//            Console.WriteLine(context.JobDetail.Key);
//            Console.WriteLine(context.JobDetail.Description);
 
 
//            //If you want to use the same object every time:
//            //Console.WriteLine(context.JobDetail.PersistJobDataAfterExecution);
//        }
//    }
 
//    class SomeOperation :IHasCommand
//    {
//        public  void Execute()
//        {
//            Console.WriteLine("Booya!");
//        }
//    }
 
//    public interface IHasCommand
//    {
//        void Execute();
//    }            
//        }
//    }
//}