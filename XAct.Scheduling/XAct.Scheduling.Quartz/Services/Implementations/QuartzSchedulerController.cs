// ReSharper disable CheckNamespace

using System.Runtime.InteropServices;

namespace XAct.Scheduling
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Linq;
    using Quartz;
    using Quartz.Impl;
    using XAct;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Scheduling.Entities;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof (ISchedulerController), BindingLifetimeType.SingletonScope, Priority.Low /*OK: No preference to technology*/)]
    [DefaultBindingImplementation(typeof(IQuartzSchedulerController), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class QuartzSchedulerController : IQuartzSchedulerController
    {
        /// <summary>
        /// Name of the type. Used for logging purposes
        /// </summary>
        protected string _typeName;

        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;
        private readonly ITracingService _tracingService;
        private IScheduler _quartzScheduler;

        /// <summary>
        /// Static lock across instances:
        /// </summary>
        protected object _lock = new object();


        private readonly Dictionary<Guid, ScheduledTask> _registeredScheduledTask =
            new Dictionary<Guid, ScheduledTask>();

        /// <summary>
        /// Gets the inner item.
        /// </summary>
        /// <typeparam name="TItem">The type of the item.</typeparam>
        /// <returns></returns>
        public TItem GetInnerItem<TItem>()
        {
            return _quartzScheduler.ConvertTo<TItem>();
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="QuartzSchedulerController" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="tracingService">The tracing service.</param>
        public QuartzSchedulerController(IDateTimeService dateTimeService, IEnvironmentService environmentService, ITracingService tracingService)
        {
            _typeName = this.GetType().FullName;

            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
            _tracingService = tracingService;

            //Invoke Start, which invokes InitializeScheduler
            //using the info within _registeredScheduledTask
            Start();
        }

        /// <summary>
        /// Registers a <see cref="ScheduledTask" />
        /// into the current internal
        /// <c>Scheduler</c>.
        /// <para>
        /// Ensure that <see cref="Start" />
        /// is invoked at some point as just
        /// Registering the <see cref="ScheduledTask" />
        /// does not automatically mean that they are being
        /// triggered, yet.
        /// </para>
        /// </summary>
        /// <param name="scheduledTasks">The scheduled tasks.</param>
        public void RegisterTask(params ScheduledTask[] scheduledTasks)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Registering task:array of tasks: {0}",((scheduledTasks != null)?scheduledTasks.Count():-1));

            if (scheduledTasks == null)
            {
                return;
            }

            lock (_lock)
            {
                foreach (ScheduledTask scheduledTask in scheduledTasks)
                {
                    _tracingService.Trace(TraceLevel.Verbose, "{0},Registering task: {1}", _typeName, scheduledTask);
                    _registeredScheduledTask[scheduledTask.Id] = scheduledTask;

                    ScheduleJobAndStartIt(scheduledTask);
                }
            }
        }

        /// <summary>
        /// Pauses a registered task if it is found.
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        public void PauseTask(ScheduledTask scheduledTask)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.PauseTask: {1}", _typeName, scheduledTask);
            _quartzScheduler.PauseJob(new JobKey(scheduledTask.Name, scheduledTask.GroupName));
        }

        /// <summary>
        /// Resumes a paused task if it is found.
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        public void ResumeTask(ScheduledTask scheduledTask)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.ResumeTask: {1}", _typeName, scheduledTask);
            _quartzScheduler.PauseJob(new JobKey(scheduledTask.Name, scheduledTask.GroupName));
        }

        /// <summary>
        /// Removes the task if it is found.
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        public void RemoveTask(ScheduledTask scheduledTask)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.RemoveTask: {1}", _typeName, scheduledTask);
            lock (_lock)
            {
                //May throw exception if not found:
                try
                {
                    _quartzScheduler.DeleteJob(new JobKey(scheduledTask.Name, scheduledTask.GroupName));
                }
                catch
                {

                }
                //Either way, continue as if it was in the scheduler:
                if (_registeredScheduledTask.ContainsKey(scheduledTask.Id))
                {
                    //Remove from cache, or it will get put back in if you stop/start scheduler.
                    _registeredScheduledTask.Remove(scheduledTask.Id);
                }
            }
        }


        /// <summary>
        /// Removes the task if it is found.
        /// </summary>
        /// <param name="scheduledTaskId">The scheduled task identifier.</param>
        public void RemoveTask(Guid scheduledTaskId)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.RemoveTask: {1}", _typeName, scheduledTaskId);
            lock (_lock)
            {
                //We use Id to get to the task description, 
                //in order to get to the name that was used to register
                //it in scheduler:
                ScheduledTask scheduledTask;
                if (!_registeredScheduledTask.TryGetValue(scheduledTaskId, out scheduledTask))
                {
                    return;
                }

                _quartzScheduler.DeleteJob(new JobKey(scheduledTask.Name, scheduledTask.GroupName));

                //Remove from cache, or it will get put back in if you stop/start scheduler.
                _registeredScheduledTask.Remove(scheduledTaskId);
            }
        }

        /// <summary>
        /// Removes the task if it is found.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="group">The group.</param>
        public void RemoveTask(string name, string group = null)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.RemoveTask: {1}/{2}", _typeName, group,name);
            lock (_lock)
            {
                //Find the cached job description:
                ScheduledTask scheduledTask =
                    _registeredScheduledTask.Values.FirstOrDefault(x => x.Name == name && x.GroupName == @group);

                if (scheduledTask == null)
                {
                    return;
                }
                //We use Id to get to the task description, 
                //in order to get to the name that was used to register
                //it in scheduler:

                //Remove it from the scheduler:
                _quartzScheduler.DeleteJob(new JobKey(scheduledTask.Name, scheduledTask.GroupName));

                //Remove from cache, or it will get put back in if you stop/start scheduler.
                _registeredScheduledTask.Remove(scheduledTask.Id);
            }
        }


        /// <summary>
        /// Checks whether a task has been registered.
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        public bool CheckExists(ScheduledTask scheduledTask)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.CheckTask: {1}", _typeName, scheduledTask);
            return _quartzScheduler.CheckExists(new JobKey(scheduledTask.Name, scheduledTask.GroupName));
        }

        /// <summary>
        /// Gets a current snapshot of the
        /// metadata passed between scheduler and job instances.
        /// <para>
        /// CAUTION:
        /// Scalar variables (eg: counters) will be fixed and at
        /// time of retrieval, and therefore safe to work with,
        /// but reference types may continue to be updated.
        /// </para>
        /// </summary>
        /// <param name="scheduledTask">The scheduled task.</param>
        /// <returns></returns>
        public Dictionary<string,object> GetJobDetails(ScheduledTask scheduledTask)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.GetJobDetails: {1}", _typeName, scheduledTask);

            var jobDetail = _quartzScheduler.GetJobDetail(new JobKey(scheduledTask.Name, scheduledTask.GroupName));

            //Convert from vendor specific oject to standard dictionary:
            Dictionary<string, object> results = new Dictionary<string, object>();
            lock (jobDetail)
            {
                foreach (var kvp in jobDetail.JobDataMap)
                {
                    results[kvp.Key] = kvp.Value;
                }
            }
            return results;
        }


        /// <summary>
        /// Gets a current snapshot of the
        /// metadata passed between scheduler and job instances.
        /// <para>
        /// CAUTION:
        /// Scalar variables (eg: counters) will be fixed at
        /// time of retrieval, and therefore safe to work with,
        /// but reference types may continue to be updated.
        /// </para>
        /// </summary>
        /// <param name="scheduledTaskId">The scheduled task's Id.</param>
        /// <returns></returns>
        public Dictionary<string, object> GetJobMetadata(Guid scheduledTaskId)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.GetJobDetails: {1}", _typeName, scheduledTaskId);

            lock (_lock)
            {
                ScheduledTask scheduledTask;
                if (!_registeredScheduledTask.TryGetValue(scheduledTaskId, out scheduledTask))
                {
                    return null;
                }

                Dictionary<string,object> result = GetJobDetails(scheduledTask);

                return result;
            }
        }


        /// <summary>
        /// Gets a current snapshot of the
        /// metadata passed between scheduler and job instances.
        /// <para>
        /// CAUTION:
        /// Scalar variables (eg: counters) will be fixed and at
        /// time of retrieval, and therefore safe to work with,
        /// but reference types may continue to be updated.
        /// </para>
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        public Dictionary<string, object> GetJobDetails(string name, string group=null)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.GetJobDetails: {1}/{2}", _typeName, group,name);

            ScheduledTask scheduledTask =
                _registeredScheduledTask.Values.FirstOrDefault(x => x.Name == name && x.GroupName == @group);

            if (scheduledTask == null)
            {
                return null;
            }
            
            var result = GetJobDetails(scheduledTask);
            return result;
        }



        /// <summary>
        /// Starts this scheduler.
        /// <para>
        /// Note that it invokes <see cref="InitializeScheduler"/>
        /// </para>
        /// </summary>
        public void Start()
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.Start", _typeName);

            ISchedulerFactory schedFact = new StdSchedulerFactory();

            if (
                (_quartzScheduler == null)
                ||
                (_quartzScheduler.IsShutdown)
                )
            {
                //Rebuild a new scheduler:
                _quartzScheduler = schedFact.GetScheduler();

                InitializeScheduler();
            }
            //else
            //{
            //    //was in shutdown mode, or already started.
            //}


            if (!_quartzScheduler.IsStarted)
            {
                _quartzScheduler.Start();
            }
        }

        /// <summary>
        /// Pause the scheduler, and therefore the triggering
        /// of all <see cref="ScheduledTask" />s.
        /// </summary>
        public void Standby()
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.StandBy", _typeName);
            _quartzScheduler.Standby();
        }


        /// <summary>
        /// Resumes the scheduler, and therefore the triggering
        /// of all <see cref="ScheduledTask"/>.
        /// </summary>
        public void Resume()
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.Resume", _typeName);
            _quartzScheduler.ResumeAll();
        }

        /// <summary>
        /// Stop the scheduler, and therefore the triggering
        /// of all <see cref="ScheduledTask" />s.
        /// </summary>
        public void Stop(bool waitForJobsToComplete = false)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.Stop", _typeName);
            _quartzScheduler.Shutdown();
        }





        private void ScheduleJobAndStartIt(ScheduledTask scheduledTask)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.ScheduleJobAndStartIt: {1}", _typeName, scheduledTask);
            // construct job info

            IJobDetail jobDetail = CreateJobDetailFromScheduledTask(scheduledTask);

            Quartz.Collection.ISet<ITrigger> triggers = CreateTriggersFromScheduledTask(scheduledTask, jobDetail);

            var jobKey = new JobKey(scheduledTask.Name, scheduledTask.GroupName);

            //Whatever it was last time, we are replacing it...
            if (_quartzScheduler.CheckExists(jobKey))
            {
                _quartzScheduler.DeleteJob(jobKey);
            }
            //All clear to register it now:
            _quartzScheduler.ScheduleJob(jobDetail, triggers, true);
        }



















        /// <summary>
        /// Rehydrates a new scheduler with the schedules that were
        /// in the previous scheduler.
        /// <para>
        /// Invoked only after Stopping and Restarting the Scheduler
        /// to transfer jobs.
        /// </para>
        /// </summary>
        private void InitializeScheduler()
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.InitializeScheduler", _typeName);
            lock (_lock)
            {
                foreach (ScheduledTask scheduledTask in _registeredScheduledTask.Values)
                {
                    ScheduleJobAndStartIt(scheduledTask);
                }
            }
        }


        private IJobDetail CreateJobDetailFromScheduledTask(ScheduledTask scheduledTask)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.CreateJobDetailFromScheduledTask: {1}", _typeName, scheduledTask);

            IJobDetail jobDetail = JobBuilder.Create<AbstractQuartzJob>()
                                             .WithIdentity(scheduledTask.Name, scheduledTask.GroupName)
                                             .Build();
            

            AbstractQuarzJobInvocationInformation abstractQuarzJobInvocationInforation =
                new AbstractQuarzJobInvocationInformation();

            abstractQuarzJobInvocationInforation.Id = scheduledTask.Id;
            abstractQuarzJobInvocationInforation.GroupName = scheduledTask.GroupName;
            abstractQuarzJobInvocationInforation.Name = scheduledTask.Name;


            EmbedInTheDataMapTheTypeInstance(jobDetail, scheduledTask, abstractQuarzJobInvocationInforation);
            EmbedInTheDataMapTheMethodArguments(jobDetail, scheduledTask, abstractQuarzJobInvocationInforation);

            return jobDetail;
        }

        private void EmbedInTheDataMapTheTypeInstance(IJobDetail jobDetail, ScheduledTask scheduledTask, AbstractQuarzJobInvocationInformation abstractQuarzJobInvocationInforation)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.EmbedInTheDataMapTheTypeInstance: {1}, {2} {3}", _typeName, jobDetail, scheduledTask, abstractQuarzJobInvocationInforation);

            //Create instance, pass it via the common data buffer:
            Type typeToInstantiate = AppDomain.CurrentDomain.GetTypeFromTypeFullName(scheduledTask); //IHasAssemblyAndTypeAndMethodNames

            object instance =
                XAct.DependencyResolver.Current.GetInstance(typeToInstantiate, false);

            //System.Activator.CreateInstance(scheduledTask.Type);
            jobDetail.JobDataMap[AbstractQuartzJob.C_DATAMAP_INVOCATION_INFO_KEY] = abstractQuarzJobInvocationInforation;

            if (instance != null)
            {
                abstractQuarzJobInvocationInforation.Instance = instance;

                try
                {
                    MethodInfo methodInfo =
                        typeToInstantiate.GetMethod(scheduledTask.MethodName);

                    abstractQuarzJobInvocationInforation.MethodInfo = methodInfo;
                }
                catch
                {
                   //Damn...
                }
            }
        }


        private void EmbedInTheDataMapTheMethodArguments(IJobDetail jobDetail, ScheduledTask scheduledTask, AbstractQuarzJobInvocationInformation abstractQuarzJobInvocationInforation)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.EmbedInTheDataMapTheMethodArguments: {1}, {2} {3}", _typeName, jobDetail, scheduledTask, abstractQuarzJobInvocationInforation);

            //Get any arguments (in the right order) to pass to 
            List<object> methodArguments = new List<object>();
            foreach (
                ScheduledTaskMetadata metadata in
                    scheduledTask.Metadata.Where(x => x.Enabled && x.IsAMethodArgument).OrderBy(x => x.Order))
            {
                methodArguments.Add(metadata.DeserializeValue());
            }
            if (methodArguments.Count > 0)
            {
                abstractQuarzJobInvocationInforation.MethodsArguments = methodArguments.ToArray();
            }


        }

        private Quartz.Collection.ISet<ITrigger> CreateTriggersFromScheduledTask(ScheduledTask scheduledTask,
                                                                                 IJobDetail jobDetail)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.CreateTriggersFromScheduledTask: {1}, {2}", _typeName, jobDetail, scheduledTask);

            Quartz.Collection.ISet<ITrigger> triggers = new Quartz.Collection.HashSet<ITrigger>();

            ITrigger trigger =
                TriggerBuilder.Create()
                    //.ForJob(jobDetail)
                              .WithCronSchedule(scheduledTask.ChronSchedule)
                              .StartAt(_dateTimeService.NowUTC)
                              //.ForJob(jobDetail)
                              //.StartNow()
                              .Build();

            triggers.Add(trigger);


            //ON HOLD AS NOT REALLY SURE WHAT THIS ADDS CONSIDERING THERES A CHRON SCHEDULE 
            //IN THE FIRST PLACE.
            //foreach (ScheduledTaskTrigger scheduledTaskTrigger in scheduledTask.Triggers)
            //{
            //    trigger =
            //        TriggerBuilder.Create()
            //            //.ForJob(jobDetail)
            //                      .WithCronSchedule(scheduledTaskTrigger.ChronSchedule)
            //                      .StartAt(_environmentService.NowUTC)
            //                      .StartNow()
            //                      .Build();

            //    triggers.Add(trigger);
            //}
            return triggers;
        }
    }
}