using XAct.Domain.Repositories;
using XAct.Scheduling.Entities;

namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using NUnit.Framework;
    using XAct.Scheduling;
    using XAct.Scheduling.Implementations;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SchedulingManagementServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }



        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetQuartzSchedulerController()
        {
            IQuartzSchedulerController quartzSchedulerController =
                XAct.DependencyResolver.Current.GetInstance<IQuartzSchedulerController>();

            Assert.IsNotNull(quartzSchedulerController);
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetQuartzSchedulerControllerOfExpectedType()
        {

            IQuartzSchedulerController quartzSchedulerController =
                XAct.DependencyResolver.Current.GetInstance<IQuartzSchedulerController>();

            Assert.AreEqual(typeof(QuartzSchedulerController), quartzSchedulerController.GetType());
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetSchedulerController()
        {
            ISchedulerController quartzSchedulerController =
                XAct.DependencyResolver.Current.GetInstance<ISchedulerController>();

            Assert.IsNotNull(quartzSchedulerController);
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetSchedulerControllerOfExpectedType()
        {

            ISchedulerController quartzSchedulerController =
                XAct.DependencyResolver.Current.GetInstance<IQuartzSchedulerController>();

            Assert.AreEqual(typeof(QuartzSchedulerController), quartzSchedulerController.GetType());
        }



        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetManagementService()
        {
            ISchedulingManagementService schedulingManagementService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingManagementService>();

            Assert.IsNotNull(schedulingManagementService);
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]       
        public void CanGetManagementServiceOfExpectedType()
        {

            ISchedulingManagementService schedulingManagementService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingManagementService>();

            Assert.AreEqual(typeof(SchedulingManagementService), schedulingManagementService.GetType());
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetISchedulingService()
        {
            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();

            Assert.IsNotNull(schedulingService);
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetISchedulingServiceOfExpectedType()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();

            Assert.AreEqual(typeof(SchedulingService), schedulingService.GetType());
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetJobAById()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();

            ScheduledTask tmp = schedulingService.GetSchedule("JobA");


            //Get First Job
            ScheduledTask scheduledTask = schedulingService.GetSchedule(tmp.Id);

            var targetJobType = scheduledTask.GetTypeFromTypeName();

            Assert.AreEqual(typeof(TestJobA), targetJobType);
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetJobAByName()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();

            //Get First Job
            ScheduledTask scheduledTask = schedulingService.GetSchedule("JobA");

            Assert.IsNotNull(scheduledTask,"have a task");

            var targetJobType = scheduledTask.GetTypeFromTypeName();

            Assert.AreEqual(typeof(TestJobA), targetJobType);
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetJobBByName()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();

            //Get First Job
            ScheduledTask scheduledTask = schedulingService.GetSchedule("JobB");

            Assert.IsNotNull(scheduledTask, "have a task");

            var targetJobType = scheduledTask.GetTypeFromTypeName();

            Assert.AreEqual(typeof(TestJobB), targetJobType);
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetJobBByNameAndHasArguments()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();

            //Get First Job
            ScheduledTask scheduledTask = schedulingService.GetSchedule("JobB");

            Assert.IsNotNull(scheduledTask, "have a task");

            var targetJobType = scheduledTask.GetTypeFromTypeName();

            Assert.AreEqual(typeof(TestJobB), targetJobType);


            Assert.IsTrue(scheduledTask.Metadata.Count>0);
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanGetSchedules()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();


            var result = schedulingService.GetSchedules();

            //Appears we survived...
            Assert.IsTrue(result.Length>0);
        }


        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanInvokeInitialize()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();


            schedulingService.Initialize();

            //Appears we survived...
            Assert.IsTrue(true);
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanInvokeInitializeAndWait12SecondsToSeeIfSomethingInvoked()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();


            schedulingService.Initialize();

            //Should hit A twice and twice once, and C once
            System.Threading.Thread.Sleep(12000);

            //Appears we survived...
            //Now that we have done some work, let's get counters:
            var counter = ExtractMetadata(schedulingService, "JobA");

            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            //Appears we survived...
            Assert.That(counter.IsLargerThan(3));
        }

        [Test]
        [Category("SchedulingService")]
        [Category("SchedulingManagementService")]
        [Category("Service")]
        public void CanInvokeInitializeAndWait3SecondsAndPickupModifiedData()
        {

            ISchedulingService schedulingService =
                XAct.DependencyResolver.Current.GetInstance<ISchedulingService>();

            schedulingService.Initialize();

            //Should hit A twice and B once, and C once
            System.Threading.Thread.Sleep(3000);


            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            //Now that we have done some work, let's get counters:
            var counter = ExtractMetadata(schedulingService, "JobA");


            //Appears we survived...
            Assert.That(counter.IsLargerThan(0));
        }

        private static int ExtractMetadata(ISchedulingService schedulingService,
            string jobName)
        {
            ISchedulingManagementService schedulingManagementService =
    XAct.DependencyResolver.Current.GetInstance<ISchedulingManagementService>();

            ScheduledTask tmp = schedulingService.GetSchedule(jobName);

            Dictionary<string, object> jobMetadata = schedulingManagementService.Scheduler.GetJobMetadata(tmp.Id);

            foreach (KeyValuePair<string, object> kvp in jobMetadata)
            {
                Trace.WriteLine(string.Format("{0}:{1}", kvp.Key, kvp.Value));
            }

            object o;
            jobMetadata.TryGetValue(AbstractQuartzJob.C_DATAMAP_INVOCATION_INFO_KEY, out o);
            AbstractQuarzJobInvocationInformation abstractQuarzJobInvocationInformation = o as AbstractQuarzJobInvocationInformation;

            jobMetadata.TryGetValue(AbstractQuartzJob.C_DATAMAP_INVOCATIONCOUNTER_KEY, out o);
            int counter = o.ConvertTo<int>();

            return counter;
        }
    }



}


