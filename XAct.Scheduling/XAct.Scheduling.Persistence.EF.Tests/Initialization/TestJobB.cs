﻿namespace XAct.Tests
{
    using System;

    public class TestJobB
    {
        public void Execute(int someArg)
        {
            Console.WriteLine("{0}:{1}: someArg:{2}", this.GetType().Name, DateTime.Now, someArg);
        }
    }
}