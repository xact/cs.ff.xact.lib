﻿namespace XAct.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Scheduling;
    using XAct.Scheduling.Initialization.DbContextSeeders;

    public class ScheduledTaskMetadataDbContextSeeder : UnitTestXActLibDbContextSeederBase<ScheduledTaskMetadata>,
                                                        IScheduledTaskMetadataDbContextSeeder
    {
        private readonly IScheduledTaskDbContextSeeder _scheduledTaskDbContextSeeder;

        public ScheduledTaskMetadataDbContextSeeder(ITracingService tracingService,
                                                    IScheduledTaskDbContextSeeder scheduledTaskDbContextSeeder)
            : base(tracingService, scheduledTaskDbContextSeeder)
        {
            _scheduledTaskDbContextSeeder = scheduledTaskDbContextSeeder;
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<ScheduledTaskMetadata>();

            CreateTaskBMetadata();
            CreateTaskCMetadata();
        }

        private void CreateTaskBMetadata()
        {

            var st = _scheduledTaskDbContextSeeder.Entities.Single(x => x.Id == 3.ToGuid());

            var stm = new ScheduledTaskMetadata
                {
                    Enabled = true,
                    IsAMethodArgument = false,
                    Order = 0,
                    Key = "SMT 3.1",
                    Description = "..."
                };
            stm.SerializeValue("Monkey", SerializationMethod.String);
            //There are two ways of doing this: Either we add it to this collection, 
            //but only setting the FK...but the fk is not yet created.
            //Or we add it to the parent entity, and do not add it to this collection of entities
            st.Metadata.Add(stm);
            //We do not add: this.InternalEntities.Add(stm);

            stm = new ScheduledTaskMetadata
                {
                    Enabled = true,
                    IsAMethodArgument = true,
                    Order = 1,
                    Key = "SMT 3.2",
                    Description = "..."
                };
            stm.SerializeValue(456789, SerializationMethod.String, typeof (int));
            //There are two ways of doing this: Either we add it to this collection, 
            //but only setting the FK...but the fk is not yet created.
            //Or we add it to the parent entity, and do not add it to this collection of entities
            st.Metadata.Add(stm);
            //We do not add: this.InternalEntities.Add(stm);

        }

        private void CreateTaskCMetadata()
        {
            var st = _scheduledTaskDbContextSeeder.Entities.Single(x => x.Id == 4.ToGuid());

            //A dud (non-Argument):
            var stm = new ScheduledTaskMetadata
                {
                    Enabled = true,
                    IsAMethodArgument = false,
                    Order = 0,
                    Key = "SMT 4.1",
                    Description = "..."
                };

            stm.SerializeValue("Monkey", SerializationMethod.String);
            //There are two ways of doing this: Either we add it to this collection, 
            //but only setting the FK...but the fk is not yet created.
            //Or we add it to the parent entity, and do not add it to this collection of entities
            st.Metadata.Add(stm);
            //We do not add: this.InternalEntities.Add(stm);

            //Two arguments, out of ourder (the Execute method wanted int,string, whereas this is being added string, int, except for the Order
            //argument.

            stm = new ScheduledTaskMetadata
                {
                    //ScheduledTask = st,
                    Enabled = true,
                    IsAMethodArgument = true,
                    Order = 2,
                    Key = "SMT 4.2",
                    Description = "..."
                };
            stm.SerializeValue("Second Var", SerializationMethod.String);
            //There are two ways of doing this: Either we add it to this collection, 
            //but only setting the FK...but the fk is not yet created.
            //Or we add it to the parent entity, and do not add it to this collection of entities
            st.Metadata.Add(stm);
            //We do not add: this.InternalEntities.Add(stm);

            stm = new ScheduledTaskMetadata
                {
                    //ScheduledTask = st,
                    //ScheduledTaskFK = st.Id;
                    Enabled = true,
                    IsAMethodArgument = true,
                    Order = 1,
                    Key = "SMT 4.3",
                    Description = "..."
                    
                };
            stm.SerializeValue(9876543, SerializationMethod.String, typeof (int));
            //There are two ways of doing this: Either we add it to this collection, 
            //but only setting the FK...but the fk is not yet created.
            //Or we add it to the parent entity, and do not add it to this collection of entities
            st.Metadata.Add(stm);
            //We do not add: this.InternalEntities.Add(stm);
        }

    }
}