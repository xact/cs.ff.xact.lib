﻿
namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Library.Settings;
    using XAct.Scheduling;
    using XAct.Scheduling.Initialization.DbContextSeeders;
    using XAct.Services;


    public class ScheduledTaskDbContextSeeder : UnitTestXActLibDbContextSeederBase<ScheduledTask>, IScheduledTaskDbContextSeeder 
    {
        public string ApplicationName
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().ApplicationName; }
        }
        public Guid ApplicationTennantId
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IApplicationTennantService>().Get(); }
        }

        public ScheduledTaskDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
            
        }


        public override void CreateEntities()
        {
            this.InternalEntities = new List<ScheduledTask>();

            CreateTaskBad();
            CreateTaskA();
            CreateTaskB();
            CreateTaskC();

        }


        private void CreateTaskBad() 
        {
            this.InternalEntities.Add(
                new ScheduledTask
                {
                    Id = 1.ToGuid(),
                    ApplicationTennantId = ApplicationTennantId,
                    Enabled = true,
                    GroupName = null,
                    Name = "Job_NotFound",
                    IsExecutable = false,
                    ExecutablePath = null,
                    ExecutableArguments = null,
                    AssemblyName = typeof(TestJobA).Assembly.FullName,
                    //Make sure this aligns with Properties/AssemblyName
                    TypeFullName = typeof(TestJobA).FullName + "WILLNOTFIND",
                    MethodName = "Execute",
                    ChronSchedule = "/2 * * * * ?",
                    Description = "Some description"
                });
        }

        private void CreateTaskA() 
        {
            var st =
                new ScheduledTask
                {
                    Id = 2.ToGuid(),
                    ApplicationTennantId = ApplicationTennantId,
                    Enabled = true,
                    GroupName = null,
                    Name = "JobA",
                    IsExecutable =false,
                    ExecutablePath = null,
                    ExecutableArguments = null,
                    AssemblyName = typeof(TestJobA).Assembly.FullName,
                    //Make sure this aligns with Properties/AssemblyName
                    TypeFullName = typeof(TestJobA).FullName,
                    MethodName = "Execute",
                    ChronSchedule = "/2 * * * * ?",
                    Description = "Some description"
                };
            this.InternalEntities.Add(st);
        }








        private void CreateTaskB()
        {
            var st = 
                new ScheduledTask
                {
                    Id = 3.ToGuid(),
                    ApplicationTennantId = ApplicationTennantId,
                    Enabled = true,
                    GroupName = null,
                    Name = "JobB",
                    IsExecutable = false,
                    ExecutablePath = null,
                    ExecutableArguments = null,
                    AssemblyName = typeof(TestJobB).Assembly.FullName,
                    //Make sure this aligns with Properties/AssemblyName
                    TypeFullName = typeof (TestJobB).FullName,
                    MethodName = "Execute",
                    ChronSchedule = "/4 * * * * ?",
                    Description = "Some description"
                };
            this.InternalEntities.Add(st);


        }

        private void CreateTaskC() 
        {
            var st = new ScheduledTask
            {
                Id = 4.ToGuid(),
                ApplicationTennantId = ApplicationTennantId,
                Enabled = true,
                GroupName = null,
                Name = "JobC",
                IsExecutable = false,
                ExecutablePath = null,
                ExecutableArguments = null,
                AssemblyName = typeof(TestJobC).Assembly.FullName,
                //Make sure this aligns with Properties/AssemblyName
                TypeFullName = typeof(TestJobC).FullName,
                MethodName = "Execute",
                ChronSchedule = "/6 * * * * ?",
                Description = "Some description"
            };

            this.InternalEntities.Add(st);
        }

    }
}
