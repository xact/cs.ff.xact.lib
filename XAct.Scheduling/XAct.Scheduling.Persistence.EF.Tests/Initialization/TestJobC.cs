﻿namespace XAct.Tests
{
    using System;

    public class TestJobC
    {
        public void Execute(int someArg, string someSecondArg)
        {
            Console.WriteLine("{0}:{1}: someArg:{2}, someSecondArg:{3}", this.GetType().Name, DateTime.Now, someArg, someSecondArg);
        }
    }
}