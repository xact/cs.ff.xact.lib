namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.CommandLine;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Unit Tests of the CommandLine Switch Parser.")]
    public class CONSOLEAPPLICATION_UnitTests2
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                                    Singleton<IocContext>.Instance.ResetIoC();

        }


        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }
        private const string C_APPPATH = "c:\\Program Files\\SomeDir\\AnApp.exe";


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        // Console.WriteLine("CommandLine:{0}", Environment.CommandLine);
        // Console.WriteLine("ApplicationName:{0}", parser.ApplicationName);
        // Console.WriteLine("Template:{0}", parser["Template"]);


        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser App")]
        public void Correctly_Parses_AppName_From_Beginning_Of_String()
        {
            CommandLineParser commandLineParser = new CommandLineParser("AnApp.exe");

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            Assert.IsTrue(commandLineParser.ApplicationName == "AnApp.exe");
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test()]
        public void Fails_To_Sets_ApplicationName_From_UnQuoted_Spaced_FileFullName()
        {
            //Default path has Space between "Program" and "Files":
            CommandLineParser commandLineParser = new CommandLineParser(C_APPPATH);

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            //Because it had a space, should not be equal. But should not be blank either
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.IsTrue(commandLineParser.ApplicationName != C_APPPATH);
        }


        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser App")]
        public void Correctly_Sets_ApplicationName_From_Quoted_Spaced_FileFullName()
        {
            //Default path has Space between "Program" and "Files":
            CommandLineParser commandLineParser =
                new CommandLineParser("\"{0}\"".FormatStringInvariantCulture(C_APPPATH));

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));

            Assert.IsTrue(commandLineParser.ApplicationName == C_APPPATH);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser App")]
        public void Correctly_Sets_ApplicationName_From_Quoted_Spaced_FileFullName_But_Does_Not_Change_Direction_Of_Slashes()
        {
            //Default path has Space between "Program" and "Files":
            CommandLineParser commandLineParser = new CommandLineParser("\"c:/Program Files/SomeDir/AnApp.exe\"");

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            Assert.IsTrue(commandLineParser.ApplicationName == "c:/Program Files/SomeDir/AnApp.exe");
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser App")]
        public void Correctly_Sets_ApplicationName_From_Quoted_Spaced_FileFullName_And_Switch()
        {
            //Default path has Space between "Program" and "Files":
            CommandLineParser commandLineParser =
                new CommandLineParser("\"{0}\" -h".FormatStringInvariantCulture(C_APPPATH));

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            Assert.IsTrue(commandLineParser.ApplicationName == C_APPPATH);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser App")]
        public void Correctly_Sets_ApplicationName_From_Quoted_Spaced_FileFullName_And_Several_Switches()
        {
            CommandLineParser commandLineParser =
                new CommandLineParser(string.Format("\"{0}\"    -a:one --b=two /c:three", C_APPPATH));

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            Assert.IsTrue(commandLineParser.ApplicationName == C_APPPATH);
        }


        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser -h")]
        public void Correctly_Sets_UnhandledSwitches_From_UnhandledSwitch_With_No_Value()
        {
            
            CommandLineParser commandLineParser = new CommandLineParser("AnApp.exe -h");
            
            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(1, commandLineParser.UnhandledSwitches.Length);
        }


        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: variable switch divider char")]
        public void Correctly_Sets_UnhandledSwitches_From_Various_Switch_Divider_Chars()
        {
            CommandLineParser commandLineParser = new CommandLineParser("AnApp.exe -a:one -b=two -c:three");


            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(3, commandLineParser.UnhandledSwitches.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: variable switch prefix char")]
        public void Correctly_Sets_UnhandledSwitches_From_Various_Switch_Prefix_Chars_And_Divider_Chars()
        {
            CommandLineParser commandLineParser = new CommandLineParser("AnApp.exe -a:one --b=two /c:three");

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(3, commandLineParser.UnhandledSwitches.Length);
        }


        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: quoted and unquoted variables")]
        public void Correctly_Sets_UnhandledSwitches_From_Quoted_Switch_Values()
        {
            CommandLineParser commandLineParser =
                new CommandLineParser("AnApp.exe -a:one -b:\"two\" -c:\"three's company\"");

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(3, commandLineParser.UnhandledSwitches.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: quoted and unquoted variables that contain escaped quotes...")]
        public void Correctly_Sets_Unhandled_Switches_From_Strings_Containing_Escaped_Quote_Chars()
        {
            string line = "AnApp.exe -a:one -b:\"two\" -c:\"I said \\\"enough!\\\", I did...\"";

            Console.WriteLine(line);
            CommandLineParser commandLineParser =
                new CommandLineParser(line);


            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(3, commandLineParser.UnhandledSwitches.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: quoted and unquoted variables that contain escaped quotes...")]
        public void Correctly_Sets_Unhandled_Switches_From_Strings_Containing_Escaped_Quote_Chars3()
        {
            string line = "AnApp.exe -a:one -b:\"two\"";

            Console.WriteLine(line);
            
            CommandLineParser commandLineParser =
                new CommandLineParser(line);


            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(2, commandLineParser.UnhandledSwitches.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: quoted and unquoted variables that contain escaped quotes...")]
        public void Correctly_Sets_Unhandled_Switches_From_Strings_Containing_Escaped_Quote_Chars4()
        {
            string line = "AnApp.exe -a:one";

            Console.WriteLine(line);

            CommandLineParser commandLineParser =
                new CommandLineParser(line);


            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(1, commandLineParser.UnhandledSwitches.Length);
        }


        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: quoted and unquoted variables that contain escaped quotes...")]
        public void Correctly_Sets_Unhandled_Switches_From_Strings_Containing_Escaped_Quote_Chars5()
        {
            string line = "AnApp.exe -b:\"two\"";

            Console.WriteLine(line);

            CommandLineParser commandLineParser =
                new CommandLineParser(line);


            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(1, commandLineParser.UnhandledSwitches.Length);
        }



        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: quoted and unquoted variables that contain escaped quotes...")]
        public void Correctly_Sets_Unhandled_Switches_From_Strings_Containing_Escaped_Quote_Chars2()
        {
            string line = "AnApp.exe -a:one -b:\"two\" -c:\"I said \\\"enough!\\\", I did...\" -d:\"again\"";

            Console.WriteLine(line);

            CommandLineParser commandLineParser =
                new CommandLineParser(line);


            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(4, commandLineParser.UnhandledSwitches.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: quoted and unquoted variables that contain escaped quotes...")]
        public void UnitTest17()
        {
            CommandLineParser commandLineParser =
                new CommandLineParser(
                    "AnApp.exe -a:one -b:\"two\" -c:\"I said \\\"enough!\\\", I did...\" /d:four --e:five");

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(5, commandLineParser.UnhandledSwitches.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: variable switch prefix char")]
        public void UnitTest18()
        {
            CommandLineParser commandLineParser = new CommandLineParser("AnApp.exe -a:one -b=two -c:three /d /e");

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(5, commandLineParser.UnhandledSwitches.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: variable switch prefix char")]
        public void UnitTest19()
        {
            CommandLineParser commandLineParser = new CommandLineParser("AnApp.exe -a:one --b=two /c:three /d /e");

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(5, commandLineParser.UnhandledSwitches.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: variable switch prefix char")]
        public void UnitTest20()
        {
            CommandLineParser commandLineParser = new CommandLineParser("AnApp.exe x y z -a:one --b=two /c:three /d /e ");
            Console.WriteLine(commandLineParser.UnhandledSwitches.Length);

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.UnhandledSwitches)
            {
                Console.WriteLine("{0}={1}".FormatStringInvariantCulture(x.Name, x.Value));
            }


            foreach (var x in commandLineParser.Parameters)
            {
                Console.WriteLine("{0}".FormatStringInvariantCulture(x));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(5, commandLineParser.UnhandledSwitches.Length);
            Assert.AreEqual(3, commandLineParser.Parameters.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: variable switch prefix char")]
        public void UnitTest21()
        {
            CommandLineParser commandLineParser =
                new CommandLineParser("AnApp.exe -a:one --b=two /c:three /d /e val x y z");
            Console.WriteLine(commandLineParser.UnhandledSwitches.Length);

            foreach (var x in commandLineParser.Parameters)
            {
                Console.WriteLine("{0}".FormatStringInvariantCulture(x));
            }


            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(5, commandLineParser.UnhandledSwitches.Length);
            Assert.AreEqual(3, commandLineParser.Parameters.Length);
        }


        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: variable switch prefix char")]
        public void UnitTest22()
        {
            CommandLineParser commandLineParser = new CommandLineParser("AnApp.exe -a:one --b=two /c:three /d /e x y z");
            Console.WriteLine(commandLineParser.UnhandledSwitches.Length);


            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.Parameters)
            {
                Console.WriteLine("{0}".FormatStringInvariantCulture(x));
            }

            //Because switch was not defined in CommandLineParser, it's considered unhandled:
            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(5, commandLineParser.UnhandledSwitches.Length);
            Assert.AreEqual(3, commandLineParser.Parameters.Length);
        }

        /// <summary>
        ///   A Test.
        /// </summary>
        [Test(Description = "Testing CommandLineParser: variable switch prefix char")]
        public void UnitTest23()
        {
            CommandLineParser commandLineParser =
                new CommandLineParser("AnApp.exe x1 -a:one x2 --b=two x3 /c:three x4 /d /e:val x y z");
            Console.WriteLine(commandLineParser.UnhandledSwitches.Length);

            //Because switch was not defined in CommandLineParser, it's considered unhandled:

            Console.WriteLine("{0}={1}".FormatStringInvariantCulture("ApplicationName", commandLineParser.ApplicationName));
            foreach (var x in commandLineParser.Parameters)
            {
                Console.WriteLine("{0}".FormatStringInvariantCulture(x));
            }

            Assert.IsNotNullOrEmpty(commandLineParser.ApplicationName);
            Assert.AreEqual(5, commandLineParser.UnhandledSwitches.Length);
            Assert.AreEqual(7, commandLineParser.Parameters.Length);
        }
    }


}


