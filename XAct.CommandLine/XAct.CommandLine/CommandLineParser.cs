#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

namespace XAct.CommandLine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;



    ///<summary>
    ///  A CommandLine parser that can handle all typical switch formats.
    ///</summary>
    ///<remarks>
    ///  <para>
    ///    Switches can start with '/','-','--'.
    ///  </para>
    ///  <para>
    ///    Switches keys can be separated from their value by ':', '=', or one or more spaces (' ').
    ///  </para>
    ///  <para>
    ///    Switch values can be either a set of contiguous non-whitespace characters, or enclosed in quotes to allow whitespaces.
    ///    Quoted strings support nested escaped strings.
    ///    <br />
    ///    Note:<br /> Enclosing Single quote symbols are not supported (although they of course can be within double quotes).
    ///  </para>
    ///  <para>
    ///    Therefore, all of the following examples are supported:
    ///    <list type = "bullet">
    ///      <item>-key value</item>
    ///      <item>-key "value"</item>
    ///      <item>--key "some longer value"</item>
    ///      <item>--key "some longer value with \"escaped\" values"</item>
    ///      <item>--key "O'Brien"</item>
    ///    </list>
    ///  </para>
    ///</remarks>
    ///<internal>
    ///  Started with http://www.codeproject.com/KB/recipes/commandlineparser.aspx
    ///  and went on from there...notably simplifications of logic and handling of escapable qoutes 
    ///  in values. Removed ability to add CommandLine as Constructor argument, and ability to add
    ///  Arguments programmatically, which muddies the clarity of the logic.
    ///</internal>
    public class CommandLineParser
    {
        #region Constants

        private readonly CultureInfo _invariantCulture = CultureInfo.InvariantCulture;

        /// <summary>
        ///   Constants used by CommandLineParser
        /// </summary>
        public static class Constants
        {
            /// <summary>
            ///   All Switches found on the <see cref = "_workingString" /> 
            ///   start at the beginning of the line, 
            ///   or after a one or more blank spaces.
            ///   <para>
            ///     NOTE: It is a Capturing Group.
            ///   </para>
            /// </summary>
            public const string SwitchKeyValuePrefixPatternConstant = @"(\s+|^)";

            /// <summary>
            ///   Switch keys start with '-' '--', or '/':
            ///   <para>
            ///     NOTE: It is a Non-Capturing Group (so can be within match).
            ///   </para>
            /// </summary>
            public const string SwitchKeyPrefixCharacterPatternPrefix = "(?<switchPrefix>(?:/|-{1,2}))";

            /// <summary>
            ///   The key is separated from a value by ':', '=', or one or more spaces.
            ///   <para>
            ///     NOTE: It is a Non-Capturing Group (so can be within match).
            ///   </para>
            /// </summary>
            public const string SwitchKeyValueDividerPatternConstant = @"(?:[:=]|\s+)";

            /// <summary>
            ///   Switches are followed by either one or more spaces, or the end of the line.
            ///   <para>
            ///     NOTE: It is a Non-Capturing Group (so can be within match).
            ///   </para>
            /// </summary>
            public const string SwitchKeyValueSuffixPatternConstant = @"(?=(\s|$))";


            /// <summary>
            ///   Regex pattern to match a boolean '+' or '-'.
            ///   <para>
            ///     TODO: Extend to allow '0' and '1'.
            ///   </para>
            ///   <para>
            ///     NOTE: It is a Capturing Group.
            ///   </para>
            /// </summary>
            public const string BooleanArgumentPatternConstant = @"(?<value>(\+|-){0,1})";

            /// <summary>
            ///   Regex pattern to match an integer.
            ///   <para>
            ///     NOTE: It is a Capturing Group.
            ///   </para>
            /// </summary>
            public const string IntegerArgumentPatternConstant = @"(?<value>(-/\+)?[0-9]+)";

            //ORIG:"(-|\+)[0-9]+)|(?<value>[0-9]+)";

            /// <summary>
            ///   Regex pattern to match an number (not just an int).
            ///   <para>
            ///     NOTE: It is a Capturing Group.
            ///   </para>
            /// </summary>
            public const string NumberArgumentPatternConstant = @"(?<value>(-/\+)?([0-9]+)(\.[0-9]+))?";

            /// <summary>
            ///   Regex pattern to match either a quoted string, or a 
            ///   <para>
            ///     NOTE: It is a Capturing Group.
            ///   </para>
            /// </summary>
            /// <remarks>
            ///   <para>
            ///     Notice the use of the \k switch (NET Regex only?) to avoid using numbers
            ///     as its just about impossible to know the number once the pattern is assembled
            ///     into larger units.
            ///   </para>
            ///   <para>
            ///     Notice the keeping of the bracket counts balanced on either side.
            ///   </para>
            /// </remarks>
            /// <internal>
            ///   Tip of the hat: http://blog.stevenlevithan.com/archives/match-quoted-string
            /// </internal>
            public const string StringArgumentPatternConstant =
                "(" +
                "((?<quote>[\"'])(?<value>(?:\\\\\\k<quote>|.)*?)\\k<quote>)" +
                "  |  " +
                "((?<value>\\S+))" +
                ")";


            /// <summary>
            /// </summary>
            public const string ParameterPatternConstant = @"((\s*(""(?<param>.+?)""|(?<param>\S+))))";


            /// <summary>
            ///   The Regex pattern used to extract the ApplicationName from the start of the given CommandLine.
            /// </summary>
            /// <remarks>
            ///   Explanation: Search for OneOrMore NonSpaces OR Non-Greedy OneOrMoreChars within QuotationMarks
            /// </remarks>
            //string altappNamePattern = "\\A(?:^(?\"[^/*?\"<>|\\r\\n]*\"|(?:\\S)+)(? .+))\\z";
            //orig @"^(?<commandLine>("".+""|(\S)+))(?<remainder>.+)";
            ////@"^(\S+|("".+""))";
            public const string ApplicationNamePatternConstant = StringArgumentPatternConstant;

            /// <summary>
            ///   Pattern to extract key+value for a switch not previously 
            ///   accounted for.
            /// </summary>
            public const string NonNamedSwitchPatternConstant =
                SwitchKeyValuePrefixPatternConstant +
                "(?<switch>" + SwitchKeyPrefixCharacterPatternPrefix + "(?<key>\\w+)" +
                SwitchKeyValueDividerPatternConstant +
                StringArgumentPatternConstant + ")" +
                SwitchKeyValueSuffixPatternConstant;

            //ORIG:@"(\s|^)(?<switch>(-{1,2}|/)(.+?))(?=(\s|$))";
        }

        #endregion

        #region Private Variables

        /// <summary>
        ///   Remainder of <see cref = "CommandLine" /> as it is being parsed
        ///   and stripped of switches, etc.
        /// </summary>
        private string _workingString;

        #endregion

        #region Public Static Properties

        private static CommandLineParser _instance;

        /// <summary>
        ///   Returns a singleton instance of <see cref = "CommandLineParser" />
        ///   which is useful, but not especially clean coding, when in a hurry and not wishing
        ///   to setup a CommandLineInstance.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Example usage would be:
        ///     <code>
        ///       <![CDATA[
        /// CommandLineParser parser = CommandLineParser.Instance;
        /// 
        /// Console.WriteLine(string.Format("CommandLine:{0}", Environment.CommandLine));
        /// Console.WriteLine(string.Format("ApplicationName:{0}", parser.ApplicationName));
        /// Console.WriteLine(string.Format("Template:{0}", parser["Template"]));
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        public static CommandLineParser Instance
        {
            get { return _instance ?? (_instance = new CommandLineParser(null)); }
        }

        #endregion

        #region Public Properties

        private readonly string _commandLine;

        private readonly IList<CommandLineSwitch> _switches = new List<CommandLineSwitch>();

        private string _applicationName;
        private string[] _parameters;
        private CommandLineSwitch[] _unhandledSwitches;

        /// <summary>
        ///   Gets the raw command line as set in the Constructor.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     <code>
        ///       <![CDATA[
        /// void Main(){
        ///   Parser p = new Parser();
        /// }
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <value>The CommandLine.</value>
        public string CommandLine
        {
            get { return _commandLine; }
        }

        /// <summary>
        ///   Gets the name of the application.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Value available after <see cref = "Parse" /> has invoked <see cref = "ParseApplicationName" />.
        ///   </para>
        /// </remarks>
        /// <value>The name of the application.</value>
        public string ApplicationName
        {
            get { return _applicationName; }
        }


        /// <summary>
        ///   Indexer to Switch values.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Values are available after <see cref = "Parse" /> has invoked <see cref = "ParseSwitches" />.
        ///   </para>
        /// </remarks>
        /// <param name = "name"></param>
        /// <returns></returns>
        public object this[string name]
        {
            get
            {
                foreach (CommandLineSwitch commandLineSwitch in _switches)
                {
                    if (string.Compare(commandLineSwitch.Name, name, true, _invariantCulture) == 0)
                    {
                        return commandLineSwitch.Value;
                    }
                }
                foreach (CommandLineSwitch commandLineSwitch in _unhandledSwitches)
                {
                    if (string.Compare(commandLineSwitch.Name, name, true, _invariantCulture) == 0)
                    {
                        return commandLineSwitch.Value;
                    }
                }
                throw new ArgumentException("Switch by that name ('{0}') not found.".FormatStringCurrentUICulture(name));
            }
        }


        /// <summary>
        ///   Collection of Switches, and their respective properties,
        ///   parsed from the given <see cref = "CommandLine" />.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Values are available after <see cref = "Parse" /> has invoked <see cref = "ParseSwitches" />.
        ///   </para>
        /// </remarks>
        protected CommandLineSwitch[] Switches
        {
            get { return _switches.ToArray(); }
        }


        /// <summary>
        ///   Gets an array of Parameters that were
        ///   parsed from the given <see cref = "CommandLine" />,
        ///   that were not found to be <see cref = "Switches" />.
        ///   <para>
        ///     Note that the parser respects quotes.
        ///   </para>
        /// </summary>
        public string[] Parameters
        {
            get { return _parameters; }
        }


        /// <summary>
        ///   This function returns a list of the unhandled switches
        ///   that the parser has seen, but not processed.
        /// </summary>
        /// <remark>
        ///   <para>
        ///     Values are available after <see cref = "Parse" /> has invoked <see cref = "ParseSwitches" />.
        ///   </para>
        ///   <para>
        ///     TODO:
        ///     The unhandled switches are *not* removed from the 
        ///     remainder of the <see cref = "CommandLine" />.
        ///   </para>
        /// </remark>
        public CommandLineSwitch[] UnhandledSwitches
        {
            get { return _unhandledSwitches; }
        }

        #endregion

        #region Constructors

        
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLineParser"/> class.
        /// </summary>
        /// <param name="commandLine">The command line.</param>
        public CommandLineParser(string commandLine=null)
        {
            _commandLine = commandLine ?? System.Environment.CommandLine;

            //Kick in to filling these switches:
            Parse();

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLineParser" /> class.
        /// </summary>
        /// <param name="classForAutoAttributes">The class for auto attributes.</param>
        /// <param name="commandLine">The command line.</param>
        /// <exception cref="System.ArgumentException">classForAutoAttributes Cannot be of type string.</exception>
        public CommandLineParser(object classForAutoAttributes, string commandLine=null)
        {
            _commandLine = commandLine?? System.Environment.CommandLine;

            if (classForAutoAttributes == null)
            {
            }
            else
            {
                if (classForAutoAttributes is string)
                {
                    throw new ArgumentException("classForAutoAttributes Cannot be of type string.");
                }
                CreateEmptySwitches(classForAutoAttributes);
            }

            //Kick in to filling these switches:
            Parse();
        }

        #endregion

        /// <summary>
        ///   Fills the classForAutoAttributes Properties/Methods with the switch values
        ///   found in the <see cref = "CommandLine" />.
        ///   <para>
        ///     Invoked by the Constructor.
        ///   </para>
        /// </summary>
        /// <param name = "classForAutoAttributes"></param>
        protected void CreateEmptySwitches<T>(T classForAutoAttributes)
            where T : class
        {
            classForAutoAttributes.ValidateIsNotDefault("classForAutoAttributes");
#if CONTRACTS_FULL || NET40 || NET45
            // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Get Type so we can reflect on its members (properties + fields):
            Type objectType = classForAutoAttributes.GetType();
            MemberInfo[] memberInfos = objectType.GetMembers();

            //Loop through members:
            for (int i = 0; i < memberInfos.Length; i++)
            {
                //Most will be for stuff other than we want:
                if ((memberInfos[i].MemberType != MemberTypes.Property) &&
                    (memberInfos[i].MemberType != MemberTypes.Field))
                {
                    continue;
                }

                //Get attributes for this member (property/field):
                object[] customAttributes = memberInfos[i].GetCustomAttributes(false);
                //If there are none, loop to next valid member:
                if (customAttributes.Length == 0)
                {
                    continue;
                }
                //Right...there are attributes...but the right kind?
                CommandLineSwitch switchRecord = null;

                //Loop through attributes:
                foreach (Attribute customAttribute in customAttributes)
                {
                    CommandLineSwitchAttribute argumentAttribute = customAttribute as CommandLineSwitchAttribute;
                    if (argumentAttribute == null)
                    {
                        continue;
                    }
                    //Lovely: we have the right kind of attribute:
                    // Get the property information.  

                    if (memberInfos[i] is PropertyInfo)
                    {
                        switchRecord = new CommandLineSwitch(argumentAttribute.Name,
                                                             argumentAttribute.Description,
                                                             classForAutoAttributes,
                                                             memberInfos[i]);

                        // Can only handle a single switch for each property
                        // (otherwise the parsing of aliases gets silly...)
                        break;
                    }
                } //~loop:customAttributes

                // See if any aliases are required.  We can only do this after
                // a switch has been registered and the framework doesn't make
                // any guarantees about the order of attributes, so we have to
                // walk the collection a second time.
                if (switchRecord != null)
                {
                    foreach (Attribute customAttribute in customAttributes)
                    {
                        CommandLineAliasAttribute aliasAttribute = customAttribute as CommandLineAliasAttribute;
                        if (aliasAttribute == null)
                        {
                            continue;
                        }
                        switchRecord.AddAlias(aliasAttribute.Alias);
                    }
                }


                // Assuming we have a switch record (that may or may not have
                // aliases), add it to the collection of switches.
                if (switchRecord != null)
                {
                    _switches.Add(switchRecord);
                }
            } //~end:memberInfos loop
        }

        #region Private Utility Functions

        /// <summary>
        ///   Parses the given <see cref = "CommandLine" />.
        /// </summary>
        /// <para>
        ///   Only call this after adding all the parameters to the switch.
        /// </para>
        /// <returns></returns>
        protected virtual bool Parse()
        {
            //Start again:
            _workingString = _commandLine;

            ParseApplicationName();

            // Remove switches and associated info.
            ParseSwitches();

            ParseUnhandledSwitches();

            // Split parameters.
            ParseParameters();

            return true;
        }

        /// <summary>
        ///   Extracts the first part of <see cref = "_workingString" />, which is the application name itself.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Note: modifies <see cref = "_workingString" /> in the process.
        ///   </para>
        ///   <para>
        ///     Invoked by <see cref = "Parse" />.
        ///   </para>
        /// </remarks>
        protected virtual void ParseApplicationName()
        {
            //string pattern = "(((?<quote>[\"'])(?<value>(?:\\\\\\k<quote>|.)*?)\\k<quote>)  |  ((?<value>\\S+)))";
            string pattern = "^\\s*(((?<quote>[\"'])(?<value>(?:\\\\\\k<quote>|.)*?)\\k<quote>))";
            Regex r = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
            Match match = r.Match(_commandLine);
            if (match.Success)
            {
                _applicationName = match.Groups["value"].Value;
                _workingString = _workingString.Replace(match.Value, string.Empty);
                return;
            }

            pattern = "^\\s*(?<value>\\S+)";
            r = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
            match = r.Match(_commandLine);
            if (match.Success)
            {
                _applicationName = match.Groups["value"].Value;
                _workingString = _workingString.Replace(match.Value, string.Empty);
            }
        }


        /// <summary>
        ///   Parses the <see cref = "_workingString" /> looking for information to create each defined Switch.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Invoked by <see cref = "Parse" />.
        ///   </para>
        ///   <para>
        ///     Note: modifies <see cref = "_workingString" /> in the process.
        ///   </para>
        /// </remarks>
        protected virtual void ParseSwitches()
        {
            foreach (CommandLineSwitch s in _switches)
            {
                Regex r = new Regex(s.RegexPattern, RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);
                //We use a collection because the same key may be used more than once.
                //And although it may look simpler to Regex from the right to get rid of it
                //you cannot use this shortcut, because we need to not only *find* the part, 
                //but *remove* it (each time) from the _WorkingString...hence a need to 
                //use r.Matches:
                MatchCollection matchCollection = r.Matches(_workingString);

                /*
					if (matchCollection.Count == 0) {
						string check = _CommandLine;
						System.Diagnostics.Debug.WriteLine("What?");
					}
					*/

                foreach (Match match in matchCollection)
                {
                    //string switchName = s.Name;
                    string value = match.Groups["value"].Value;

                    if (s.SwitchValueType == typeof (bool))
                    {
                        bool state = true;
                        // The value string may indicate what value we want.
                        switch (value)
                        {
                            case "+":
                                //state = true;
                                break;
                            case "-":
                                state = false;
                                break;
                            case "":
                                if (s.Value != null)
                                {
                                    state = !(bool) s.Value;
                                }
                                break;
                            default:
                                break;
                        }
                        s.Value = state;

                        break;
                    }
                    else if (s.SwitchValueType == typeof (string))
                    {
                        s.Value = value;
                    }
                    else if (s.SwitchValueType == typeof (Int16))
                    {
                        s.Value = Int16.Parse(value, _invariantCulture);
                    }
                    else if (s.SwitchValueType == typeof (Int32))
                    {
                        s.Value = Int32.Parse(value, _invariantCulture);
                    }
                    else if (s.SwitchValueType == typeof (Int32))
                    {
                        s.Value = Int64.Parse(value, _invariantCulture);
                    }
                    else if (s.SwitchValueType == typeof (Single))
                    {
                        s.Value = Single.Parse(value, _invariantCulture);
                    }
                    else if (s.SwitchValueType == typeof (Double))
                    {
                        s.Value = Double.Parse(value, _invariantCulture);
                    }
                    else if (s.SwitchValueType.IsEnum)
                    {
                        s.Value = (Enum.Parse(s.SwitchValueType, value, true));
                    }

                    //string toRemove = match.Value;

                    //Remove the whole found fragrment from workingString:
                    //_WorkingString = _WorkingString.Replace(toRemove, string.Empty);//
                    //r.Replace(_WorkingString, string.Empty);
                    _workingString = _workingString.Replace(match.Value, string.Empty);
                    //_WorkingString = _WorkingString.Trim();
                }
            }
        }


        /// <summary>
        ///   Parses any unspecified/unhandled switches.
        /// </summary>
        protected virtual void ParseUnhandledSwitches()
        {
            //string check = _commandLine;
            Regex r = new Regex(Constants.NonNamedSwitchPatternConstant, RegexOptions.IgnorePatternWhitespace);
            MatchCollection matchCollection = r.Matches(_workingString);

            List<CommandLineSwitch> tmp =
                new List<CommandLineSwitch>();

            foreach (Match match in matchCollection)
            {
                string switchName = match.Groups["key"].Value;
                CommandLineSwitch switchInfo = new CommandLineSwitch(switchName, string.Empty, typeof (string));
                switchInfo.Value = match.Groups["value"].Value;
                tmp.Add(switchInfo);
                _workingString = _workingString.Replace(match.Value, string.Empty);
                //_WorkingString = _WorkingString.Trim();
            }
            _unhandledSwitches = tmp.ToArray();
        }

        ///<summary>
        ///  Handles left-over parameters that were not extracted by
        ///  <see cref = "ParseSwitches" /> and used as part of switches.
        ///</summary>
        ///<remarks>
        ///  <para>
        ///    Populate the split parameters array with the remaining parameters.
        ///    Note that if quotes are used, the quotes are removed.
        ///    <code>
        ///      <![CDATA[
        /// e.g.   one two three "four five six"
        ///						0 - one
        ///						1 - two
        ///						2 - three
        ///						3 - four five six
        /// (e.g. 3 is not in quotes).
        /// ]]> 
        ///    </code>
        ///  </para>
        ///  <para>
        ///    Invoked by <see cref = "Parse" />.
        ///  </para>
        ///  <para>
        ///    Note: TODO: DOES NOT modifies <see cref = "_workingString" /> in the process.
        ///  </para>
        ///</remarks>
        protected virtual void ParseParameters()
        {
            Regex r = new Regex(Constants.ParameterPatternConstant, RegexOptions.IgnorePatternWhitespace);
            MatchCollection matchCollection = r.Matches(_workingString);

            _parameters = new string[matchCollection.Count];
            for (int i = 0; i < matchCollection.Count; i++)
            {
                _parameters[i] = matchCollection[i].Groups["param"].Value;
            }
        }

        #endregion

        #region Nested Classes

        /// <summary>
        ///   The CommandLineSwitch is created by the <see cref = "CommandLineParser" />
        ///   to hold the information required to describe the schema and value of a Switch
        ///   on the CommandLine.
        /// </summary>
        public class CommandLineSwitch
        {
            #region Fields

            /// <summary>
            ///   The (optional) MemberInfo that is filled by the constructor.
            /// </summary>
            private readonly MemberInfo _memberInfo;

            #endregion

            #region Public Properties

            private readonly IList<string> _aliases =
                new List<string>();

            private string _description;

            /// <summary>
            ///   The <see cref = "Value" />'s Cached value, used only if the Switch does not have an <c>Owner</c> and <c>MemberInfo</c> to <c>set;get;</c>.
            /// </summary>
            /// <value>The internal value.</value>
            private object _internalValue;

            private string _name;
            private object _owner;


            private string _regexPattern;
            private Type _switchValueType;

            /// <summary>
            ///   Gets the Switch's Name.
            /// </summary>
            /// <value>The name.</value>
            public string Name
            {
                get { return _name; }
            }

            /// <summary>
            ///   Gets the list of aliases for this switch.
            /// </summary>
            /// <value>The aliases.</value>
            public string[] Aliases
            {
                get { return (_aliases != null) ? _aliases.ToArray() : null; }
            }


            /// <summary>
            ///   Gets the Switch's Description.
            /// </summary>
            /// <value>The description.</value>
            public string Description
            {
                get { return _description; }
            }


            /// <summary>
            ///   Gets the current (mutable) value of the property.
            /// </summary>
            /// <remarks>
            ///   The value is not setable as it is intended to reference values
            ///   that came in on the CommandLine. Even if these CommandLine settings
            ///   set an eg: Settings class, it is not up to the CommandLineParser to allow
            ///   access to it -- this doesn't stop the Settings instance from allowing access though...
            /// </remarks>
            /// <value>The value.</value>
            public object Value
            {
                get
                {
                    if (_owner != null)
                    {
                        if (_memberInfo.MemberType == MemberTypes.Field)
                        {
                            return ((FieldInfo) _memberInfo).GetValue(_owner);
                        }
                        if (_memberInfo.MemberType == MemberTypes.Property)
                        {
                            return ((PropertyInfo) _memberInfo).GetValue(_owner, null);
                        }
                    }

                    return _internalValue;
                }
                protected internal set
                {
                    if (_owner != null)
                    {
                        if (_memberInfo.MemberType == MemberTypes.Field)
                        {
                            ((FieldInfo) _memberInfo).SetValue(_owner, value);
                        }
                        else if (_memberInfo.MemberType == MemberTypes.Property)
                        {
                            ((PropertyInfo) _memberInfo).SetValue(_owner, value, null);
                        }
                    }
                    else
                    {
                        _internalValue = value;
                    }
                }
            }

            /// <summary>
            ///   Gets the type of the Switch's value.
            /// </summary>
            /// <value>The type.</value>
            public Type SwitchValueType
            {
                get { return _switchValueType; }
            }

            /// <summary>
            ///   The pattern used to find the Switch Key + Divider + Value in the <see cref = "CommandLine" />.
            ///   <para>
            ///     Used by <see cref = "CommandLineParser.ParseSwitches" />.
            ///   </para>
            /// </summary>
            public string RegexPattern
            {
                get { return _regexPattern; }
            }

            /// <summary>
            ///   The instance that this Property/Field belongs to.
            /// </summary>
            /// <value>The owner.</value>
            public void SetOwner(object owner)
            {
                _owner = owner;
            }

            #endregion

            #region Constructors

            /// <summary>
            ///   Initializes a new instance of the <see cref = "CommandLineSwitch" /> class.
            /// </summary>
            /// <param name = "name">The name.</param>
            /// <param name = "description">The description.</param>
            public CommandLineSwitch(string name, string description) : this(name, description, typeof (bool))
            {
            }


            /// <summary>
            ///   Initializes a new instance of the <see cref = "CommandLineSwitch" /> class.
            /// </summary>
            /// <param name = "name">The name.</param>
            /// <param name = "description">The description.</param>
            /// <param name = "propertyOwner">The instance that this property belongs to.</param>
            /// <param name = "memberInfo">The schema of the property or field this switch refers to.</param>
            [SuppressMessage("Microsoft.Naming", "CA2204", Justification = "Spelling ('memberInfo') is ok.")]
            public CommandLineSwitch(string name, string description, object propertyOwner, MemberInfo memberInfo)
            {
                //Check Args:
                propertyOwner.ValidateIsNotDefault("propertyOwner");
                memberInfo.ValidateIsNotDefault("memberInfo");

                Type type;
                if (memberInfo.MemberType == MemberTypes.Property)
                {
                    type = ((PropertyInfo) memberInfo).PropertyType;
                }
                else if (memberInfo.MemberType == MemberTypes.Field)
                {
                    type = ((FieldInfo) memberInfo).FieldType;
                }
                else
                {
                    throw new ArgumentException("memberInfo must be of type Property or Field");
                }
                _owner = propertyOwner;
                _memberInfo = memberInfo;

                Initialize(name, description, type);
            }

            /// <summary>
            ///   Initializes a new instance of the <see cref = "CommandLineSwitch" /> class.
            /// </summary>
            /// <param name = "name">The name.</param>
            /// <param name = "description">The description.</param>
            /// <param name = "type">The type.</param>
            public CommandLineSwitch(string name, string description, Type type)
            {
                Initialize(name, description, type);
            }

            #endregion

            #region Public Methods

            /// <summary>
            ///   Adds the alias name for the Switch.
            ///   <para>
            ///     Invoked by <c>CommandLineParser.CreateEmptySwitches</c> when it is invoked by the <see cref = "CommandLineParser" /> constructor.
            ///   </para>
            /// </summary>
            /// <param name = "alias">The alias.</param>
            public void AddAlias(string alias)
            {
                alias.ValidateIsNotNullOrEmpty("alias");
                _aliases.Add(alias);
                //Rebuild the pattern:
                BuildSwitchKeyValueRegexPattern();
            }

            #endregion

            #region Private Utility Functions

            private void Initialize(string name, string description, Type type)
            {
                name.ValidateIsNotNullOrEmpty("name");
                if (description == null)
                {
                    description = string.Empty;
                }
                type.ValidateIsNotDefault("type");

                if (type == typeof (bool) ||
                    type == typeof (string) ||
                    type == typeof (Int16) ||
                    type == typeof (Int32) ||
                    type == typeof (Int64) ||
                    type == typeof (float) ||
                    type == typeof (double) ||
                    type.IsEnum)
                {
                    _switchValueType = type;
                    _name = name;
                    _description = description;

                    BuildSwitchKeyValueRegexPattern();
                }
                else
                {
                    throw new ArgumentException("Currently only Ints, Bool and Strings are supported");
                }
            }

            /// <summary>
            ///   Build the Regex pattern needed to scan the given _WorkingLine
            ///   for the Switch's Key + Divider + Value.
            /// </summary>
            /// <para>
            ///   Invoked by Constructor and <see cref = "AddAlias" />.
            /// </para>
            private void BuildSwitchKeyValueRegexPattern()
            {
                //We are looking for something like:
                //'/MySwitch:MyValue'

                //So We start building the pattern with 'MySwitch':
                string strSwitchKeyPattern = Name;

                //And add any aliases in order to get:
                //'MySwitch|MyAlias':
                foreach (string s in Aliases)
                {
                    strSwitchKeyPattern += "|" + s;
                }

                string strSwitchKeyValueDividerPattern = Constants.SwitchKeyValueDividerPatternConstant;

                //The switch key is done, now for the value.
                string strSwitchValuePattern;
                if (_switchValueType == typeof (bool))
                {
                    //Unique: there is no divider between key and value if boolean:
                    strSwitchKeyValueDividerPattern = string.Empty;
                    //If the type of the argument is a boolean
                    //we are looking for '+', '-':
                    strSwitchValuePattern = Constants.BooleanArgumentPatternConstant;
                }
                else if (_switchValueType == typeof (string))
                {
                    //If a string, we are looking for:
                    strSwitchValuePattern = Constants.StringArgumentPatternConstant;
                }
                else if (
                    (_switchValueType == typeof (Int16)) ||
                    (_switchValueType == typeof (Int32)) ||
                    (_switchValueType == typeof (Int64)))
                {
                    strSwitchValuePattern = Constants.IntegerArgumentPatternConstant;
                }
                else if (
                    (_switchValueType == typeof (double)) ||
                    (_switchValueType == typeof (float)))
                {
                    strSwitchValuePattern = Constants.NumberArgumentPatternConstant;
                }
                else if (_switchValueType.IsEnum)
                {
                    //If the type is an enum, we are looking for something like:
                    //(Monday|TuesDay|...etc)
                    string[] enumNames = Enum.GetNames(_switchValueType);
                    string eStr = enumNames[0];
                    for (int e = 1; e < enumNames.Length; e++)
                    {
                        eStr += "|" + enumNames[e];
                    }
                    strSwitchValuePattern = @"(?<value>" + eStr + ")";
                }
                else
                {
                    //Argument Type not recognized.
                    throw new ArgumentException("Argument Type not handled at present.");
                }


                //Update the Key part to add non-capturing divider and close it:
                strSwitchKeyPattern = "(?<key>" + strSwitchKeyPattern + ")";

                // Set the internal regular expression pattern.
                //Test:_RegexPattern = strSwitchKeyPattern;
                //Test:_RegexPattern = SwitchKeyValuePrefixPatternConstant + "(?<switch>" + strSwitchKeyPattern + strSwitchValuePattern + ")" + SwitchKeyValueSuffixPatternConstant;
                _regexPattern =
                    "(" +
                    "   " +
                    Constants.SwitchKeyValuePrefixPatternConstant +
                    "   " +
                    "(?<switch>" +
                    "   " +
                    Constants.SwitchKeyPrefixCharacterPatternPrefix +
                    strSwitchKeyPattern +
                    "   " +
                    strSwitchKeyValueDividerPattern +
                    "   " +
                    strSwitchValuePattern +
                    ")" +
                    "   " +
                    //SwitchKeyValueSuffixPatternConstant +
                    "   " +
                    ")" +
                    "";
            }

            #endregion
        }



        #endregion
    }




    /// <summary>
    ///   Attribute to attach to any switch definition.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class CommandLineSwitchAttribute : Attribute
    {
        #region Public Properties

        private readonly string _description;
        private readonly string _name;

        /// <summary>
        ///   Gets the name of the Switch.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        ///   Gets the description of the switch.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return _description; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "CommandLineSwitchAttribute" /> class.
        /// </summary>
        /// <param name = "name">The name.</param>
        public CommandLineSwitchAttribute(string name) : this(name, null)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "CommandLineSwitchAttribute" /> class.
        /// </summary>
        /// <param name = "name">The name.</param>
        /// <param name = "description">The description.</param>
        public CommandLineSwitchAttribute(string name, string description)
        {
            name.ValidateIsNotNullOrEmpty("name");
            if (description == null)
            {
                description = string.Empty;
            }

            _name = name;
            _description = description;
        }

        #endregion
    }

    /// <summary>
    ///   This class implements an alias attribute to work in conjunction
    ///   with the <see cref = "CommandLineSwitchAttribute">CommandLineSwitchAttribute</see>
    ///   attribute.  If the CommandLineSwitchAttribute exists, then this attribute
    ///   defines an alias for it.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CommandLineAliasAttribute : Attribute
    {
        #region Public Properties

        private readonly string _alias;

        /// <summary>
        ///   Gets the alias/alternate name to use for the command switch.
        /// </summary>
        /// <value>The alias.</value>
        public string Alias
        {
            get { return _alias; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "CommandLineAliasAttribute" /> class.
        /// </summary>
        /// <param name = "alias">The alias.</param>
        public CommandLineAliasAttribute(string alias)
        {
            alias.ValidateIsNotNullOrEmpty("alias");
            _alias = alias;
        }

        #endregion
    }


}


