﻿
namespace XAct.Quality
{
    /// <summary>
    /// The type of <see cref="SoftwareTaskAttachment"/> (MSInfo32 report, 
    /// </summary>
    public class SoftwareTaskAttachmentType : IHasXActLibEntity, IHasId<int>, IHasEnabled, IHasOrder, IHasKeyValue<string>
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder"/>.
        /// </para>
        /// </summary>
        /// <value>The order.</value>
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        public virtual string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public virtual string Value { get; set; }

    }
}
