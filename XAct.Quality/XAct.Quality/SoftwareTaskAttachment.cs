﻿
namespace XAct.Quality
{
    using XAct.Environment.Implementations;
    using XAct.Environment.Services.Implementations;

    /// <summary>
    /// An entity for adding Attachments (MSInfo32, ScreenGrabs, etc.)
    /// to <see cref="SoftwareTask"/>s.
    /// </summary>
    public class SoftwareTaskAttachment : IHasXActLibEntity, IHasDistributedIdentities, IHasSerializedTypeValueAndMethod, IHasDescription
    {
        /// <summary>
        /// Gets or sets the machine id (a hash built using the <see cref="PCLEnvironmentService.ApplicationNameHash"/>).
        /// </summary>
        /// <value>The machine id.</value>
        public virtual string MachineId { get; set; }

        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasId{TId}"/>.
        /// </para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal><para>8/9/2011: Sky</para></internal>
        public virtual int Id { get; set; }


        /// <summary>
        /// Gets or sets the id of the parent attachment.
        /// </summary>
        /// <value>The attachment id.</value>
        public virtual int DefectReportFK { get; set; }

        /// <summary>
        /// Gets or sets the defect report attachment type FK.
        /// </summary>
        /// <value>The defect report attachment type FK.</value>
        public virtual int TypeFK { get; set; }

        /// <summary>
        /// Gets or sets the type of attachment (MSInfo32 report, Screen grab, etc).
        /// </summary>
        /// <value>The type.</value>
        public virtual SoftwareTaskAttachmentType Type { get; set; }


        /// <summary>
        /// Gets or sets the description of the attachment.
        /// <para>
        /// Note that there is no Key --
        /// </para>
        /// </summary>
        /// <value>The description.</value>
        public virtual string Description { get; set; }

        /// <summary>
        /// Gets or sets the serialization method.
        /// </summary>
        /// <value>The serialization method.</value>
        public virtual SerializationMethod SerializationMethod { get; set; }


        /// <summary>
        /// Gets or sets the serialized <see cref="IHasSerializedTypeValueAndMethod.SerializedValue"/> <see cref="IHasSerializedTypeValueAndMethod.SerializedValueType"/>.
        /// </summary>
        /// <value>The type.</value>
        /// <internal><para>8/16/2011: Sky</para></internal>
        public virtual string SerializedValueType { get; set; }

        /// <summary>
        /// Gets or sets the serialized value.
        /// </summary>
        /// <value>The value.</value>
        /// <internal>8/16/2011: Sky</internal>
        public virtual string SerializedValue { get; set; }
    }
}
