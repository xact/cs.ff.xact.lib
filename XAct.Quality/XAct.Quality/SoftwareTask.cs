namespace XAct.Quality
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using XAct;
    using XAct.Environment.Implementations;
    using XAct.Environment.Services.Implementations;

    /// <summary>
    /// An entity for a Software Task (Bug Report, Suggestion, Enhancement, etc.).
    /// </summary>
    /// <remarks>
    /// The entity implements <see cref="IHasDistributedIdentities"/> to allow for 
    /// Clients to report BugReports to be filled offline.
    /// </remarks>
    public class SoftwareTask : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasSubjectAndBody, XAct.IHasDateTimeCreatedBy
    {

        /// <summary>
        /// 
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the FK for the <see cref="SoftwareTaskType"/> (Bug Defect, Suggestion, Enhancement, etc.)
        /// </summary>
        /// <value>The type FK.</value>
        public virtual int TypeFK { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="SoftwareTaskType"/> 
        /// (can be defined per project to be something like
        /// Bug Defect, Suggestion, Enhancement, etc.).
        /// </summary>
        /// <value>The type.</value>
        public virtual SoftwareTaskType Type { get; set; }



        /// <summary>
        /// Gets or sets the optional vote value.
        /// <para>
        /// The rating range is specific to an app.
        /// Some apps might like 1-3, others 1-5, others 0-100.
        /// </para>
        /// </summary>
        /// <value>The vote value.</value>
        public int? FeedbackRating { get; set; }

        /// <summary>
        /// Gets or sets the FK for the optional <see cref="SoftwareTaskFeature"/>.
        /// </summary>
        /// <value>The feedback feature FK.</value>
        public virtual Guid? FeedbackFeatureFK { get; set; }

        /// <summary>
        /// Gets or sets the feature being referred to.
        /// </summary>
        /// <value>The feature.</value>
        public virtual SoftwareTaskFeature Feature { get; set; }

        /// <summary>
        /// Gets or sets the message subject.
        /// </summary>
        /// <value>The subject.</value>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body.
        /// </summary>
        /// <value>The body.</value>
        public virtual string Body { get; set; }


        /// <summary>
        /// Gets the Attachments that belong to this <see cref="SoftwareTask"/>.
        /// </summary>
        public virtual ICollection<SoftwareTaskAttachment> Attachments { get { return _attachments ?? (_attachments = new Collection<SoftwareTaskAttachment>()); } }
        private ICollection<SoftwareTaskAttachment> _attachments;


        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        /// <value></value>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        /// </internal>
        public System.DateTime? CreatedOnUtc { get; set; }



        /// <summary>
        /// Gets or sets the who created the document.
        /// </summary>
        /// <value>The created by.</value>
        public string CreatedBy { get; set; }



        /// <summary>
        /// Constructor.
        /// </summary>
        public SoftwareTask()
        {
            this.GenerateDistributedId();
        }
    }
}