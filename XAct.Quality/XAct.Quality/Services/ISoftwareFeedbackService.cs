﻿namespace XAct.Quality
{
    /// <summary>
    /// Contract for a service to vote on software features.
    /// <para>
    /// Generally it will be a a -1,0,1 (Dislike, Ambivalent, Like)
    /// or 5 star vote (although that is less clear as to what people
    /// think of anything)
    /// </para>
    /// </summary>
    public interface ISoftwareFeedbackService : IHasXActLibService
    {

        /// <summary>
        /// Gets the record describing the Software Feature.
        /// <para>
        /// Most features will not be defined ahead of time.
        /// </para>
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        SoftwareTaskFeature GetSoftwareFeedbackFeature(string key);

        /// <summary>
        /// Give feedback on a specific feature.
        /// <para>
        /// Ensure that <see cref="SoftwareTask.Type"/>
        /// is set to 'Feedback', Ratings is set, etc.
        /// </para>
        /// </summary>
        /// <param name="feedback">The feedback.</param>
        void SaveFeedback(SoftwareTask feedback);

    }
}