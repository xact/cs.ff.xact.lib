// ReSharper disable CheckNamespace
namespace XAct.Quality.Implementations
// ReSharper restore CheckNamespace
{
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="ISoftwareFeedbackManagementService"/>
    /// to manage <see cref="SoftwareTask"/> records.
    /// </summary>
    public class SoftwareFeedbackManagementService : ISoftwareFeedbackManagementService
    {
        private readonly ITracingService _tracingService;
        private readonly ISoftwareTaskRepositoryService _softwareTaskRepositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareFeedbackManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="softwareTaskRepository">The software task repository.</param>
        public SoftwareFeedbackManagementService(ITracingService tracingService, ISoftwareTaskRepositoryService softwareTaskRepository)
        {
            _tracingService = tracingService;
            _softwareTaskRepositoryService = softwareTaskRepository;
        }

        /// <summary>
        /// Create or update the specified feedback feature.
        /// </summary>
        /// <param name="feedbackFeature">The feedback feature.</param>
        public void Persist(SoftwareTaskFeature feedbackFeature)
        {
            _softwareTaskRepositoryService.Persist(feedbackFeature);
        }

        /// <summary>
        /// Deletes the specified feedback feature.
        /// </summary>
        /// <param name="feedbackFeature">The feedback feature.</param>
        public void Delete(SoftwareTaskFeature feedbackFeature)
        {
            _softwareTaskRepositoryService.Delete(feedbackFeature);
        }


        /// <summary>
        /// Gets the feedback for feature.
        /// </summary>
        /// <param name="featureId">The feature id.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        public IQueryable<SoftwareTask> GetFeedbackForFeature(string featureId, IPagedSearchTermQuerySpecification pagedQuerySpecification)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Retrieve Features for {0}", featureId);

            return _softwareTaskRepositoryService.GetFeedbackForFeature(featureId, pagedQuerySpecification);

        }
    }
}