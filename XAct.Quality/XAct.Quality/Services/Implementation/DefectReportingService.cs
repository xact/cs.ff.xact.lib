//Not so happy about these dependencies...are they in WinRT?

// ReSharper disable CheckNamespace
namespace XAct.Quality.Implementations
// ReSharper restore CheckNamespace
{
    using System.Diagnostics;
    //using System.Drawing.Imaging;
    using System.IO;
    //using System.Windows.Forms;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IDefectReportingService"/>
    /// for reporting software Defects with more information
    /// than just a client's impression of what's going on.
    /// </summary>
    public class DefectReportingService : IDefectReportingService
    {

        private readonly string _typeName;
        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly ISoftwareTaskRepositoryService _defectReportRepositoryService;

        //SEE:  http://www.hanselman.com/blog/
        //http://www.codeproject.com/Articles/1760/How-to-start-the-Microsoft-System-Information-dial
        //Seems this is best solution:
        //http://support.microsoft.com/kb/300887
        //http://www.c-sharpcorner.com/Forums/Thread/99921/



        /// <summary>
        /// Initializes a new instance of the <see cref="DefectReportingService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="defectReportRepository">The defect report repository.</param>
        public DefectReportingService(ITracingService tracingService, IEnvironmentService environmentService, ISoftwareTaskRepositoryService defectReportRepository)
        {
            _typeName = this.GetType().Name;
            _tracingService = tracingService;
            _environmentService = environmentService;
            _defectReportRepositoryService = defectReportRepository;
        }


        //And for widnow
        //http://stackoverflow.com/questions/1163761/c-sharp-capture-screenshot-of-active-window
        
        /// <summary>
        /// Reports the defect.
        /// </summary>
        /// <param name="defectReport">The defect report.</param>
        public void ReportDefect(SoftwareTask defectReport)
        {
            _tracingService.Trace(Diagnostics.TraceLevel.Verbose, "{0}.{1}({2})", _typeName, "ReportDefect", defectReport);

            _defectReportRepositoryService.SaveReport(defectReport);
        }

        ///// <summary>
        ///// Generates a report of the client computer's setup (invokes MSINFO32.exe)
        ///// </summary>
        ///// <param name="outputReportFilePath">The output report file path.</param>
        ///// <param name="delay">The delay.</param>
        ///// <returns></returns>
        //public string GenerateReport(string outputReportFilePath = null, int delay=240000)
        //{
        //    //string OutputFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
        //    string tmpDirectory = Path.GetFullPath(System.IO.Path.GetTempPath());

        //    if (outputReportFilePath.IsNullOrEmpty())
        //    {
        //        outputReportFilePath =
        //            Path.GetFullPath(System.IO.Path.Combine(tmpDirectory, _environmentService.MachineName + ".txt"));
        //    }
        //    // string arg = string.Format(@"/nfo /categories +all-SWEnvWindowsError ""{0}"" ",fullPath); 
        //    string arg = string.Format(@"/report ""{0}"" ", outputReportFilePath);

        //    Process infoProcess = new Process();
        //    infoProcess.StartInfo.FileName = @"msinfo32.exe";
        //    infoProcess.StartInfo.Arguments = arg;
        //    infoProcess.Start();
        //    infoProcess.WaitForExit(240000);


        //    // Define other methods and classes here
        //    return outputReportFilePath;

        //}

        ///// <summary>
        ///// Create a screen grab of the currently selected window.
        ///// </summary>
        ///// <param name="outputReportFilePath">The output report file path.</param>
        ///// <returns></returns>
        //public string CaptureScreen(string outputReportFilePath)
        //{
        //    string tmpDirectory = Path.GetFullPath(System.IO.Path.GetTempPath());

        //    if (outputReportFilePath.IsNullOrEmpty())
        //    {
        //        outputReportFilePath =
        //            Path.GetFullPath(System.IO.Path.Combine(tmpDirectory, _environmentService.MachineName + ".png"));
        //    }

        //    // get the bounding area of the screen containing (0,0)
        //    // remember in a multidisplay environment you don't know which display holds this point
        //    Rectangle bounds = Screen.GetBounds(Point.Empty);

        //    // create the bitmap to copy the screen shot to
        //    Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height);

        //    // now copy the screen image to the graphics device from the bitmap
        //    using (Graphics gr = Graphics.FromImage(bitmap))
        //    {
        //        gr.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
        //    }
        //    bitmap.Save(outputReportFilePath, ImageFormat.Png);

        //    return outputReportFilePath;
        //}

    }
}