// ReSharper disable CheckNamespace
namespace XAct.Quality.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of 
    /// <see cref="ISoftwareFeedbackService"/>
    /// to persist <c>Feedback</c> (in the form of a <see cref="SoftwareTask"/>).
    /// on a specific feature or the software in general.
    /// </summary>
    public class SoftwareFeedbackService : ISoftwareFeedbackService
    {

        private readonly ITracingService _tracingService;
        private readonly ISoftwareTaskRepositoryService _softwareTaskRepositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareFeedbackService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="softwareTaskRepository">The feedback repository.</param>
        public SoftwareFeedbackService(ITracingService tracingService, ISoftwareTaskRepositoryService softwareTaskRepository)
        {
            _tracingService = tracingService;
            _softwareTaskRepositoryService = softwareTaskRepository;
        }


        /// <summary>
        /// Give feedback on a specific feature.
        /// </summary>
        /// <param name="feedback">The feedback.</param>
        public void SaveFeedback(SoftwareTask feedback)
        {
            _tracingService.Trace(TraceLevel.Verbose,"Post feedback for Feature {0}",feedback.Feature);

            if (!feedback.FeedbackFeatureFK.HasValue)
            {
// ReSharper disable LocalizableElement
                throw new ArgumentNullException("feedback", "feedback.FeedbackFeatureFK is null.");
// ReSharper restore LocalizableElement
            }

            if (!feedback.FeedbackRating.HasValue)
            {
// ReSharper disable LocalizableElement
                throw new ArgumentNullException("feedback","feedback.FeedbackRating is null.");
// ReSharper restore LocalizableElement
            }

            _softwareTaskRepositoryService.SaveReport(feedback);
        }


        /// <summary>
        /// Gets the record describing the Software Feature.
        /// <para>
        /// Most features will not be defined ahead of time.
        /// </para>
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public SoftwareTaskFeature GetSoftwareFeedbackFeature(string key)
        {
            SoftwareTaskFeature result = _softwareTaskRepositoryService.GetSoftwareFeedbackFeature(key);

            return result;
        }
    }
}