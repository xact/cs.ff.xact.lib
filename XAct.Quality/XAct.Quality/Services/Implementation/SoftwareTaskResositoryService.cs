// ReSharper disable CheckNamespace
namespace XAct.Quality.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="ISoftwareTaskRepositoryService"/>
    /// to persist <see cref="SoftwareTask"/> objects
    /// for the 
    /// </summary>
    public class SoftwareTaskResositoryService : ISoftwareTaskRepositoryService
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareTaskResositoryService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public SoftwareTaskResositoryService(ITracingService tracingService, IEnvironmentService environmentService, IRepositoryService repositoryService)
        {
            _typeName = this.GetType().Name;
            _tracingService = tracingService;
            _environmentService = environmentService;
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Gets the software feedback feature.
        /// <para>
        /// If non found,return null.
        /// </para>
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="applicationIdentifier">The application identifier.</param>
        /// <returns></returns>
        public SoftwareTaskFeature GetSoftwareFeedbackFeature(string key, string applicationIdentifier=null)
        {
            if (applicationIdentifier.IsNullOrEmpty())
            {
                applicationIdentifier = _environmentService.ApplicationNameHash;
            }

            return _repositoryService.GetSingle<SoftwareTaskFeature>(f => f.ApplicationIdentifier == applicationIdentifier && f.Key == key);
        }


        /// <summary>
        /// Gets the feedback/tasks/defects for the specified feature.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        public IQueryable<SoftwareTask> GetFeedbackForFeature(string key, IPagedQuerySpecification pagedQuerySpecification)
        {
            return _repositoryService.GetByFilter<SoftwareTask>(f => f.Feature.Key == key,null,pagedQuerySpecification);
        }


        /// <summary>
        /// Saves the <see cref="SoftwareTask"/>.
        /// </summary>
        /// <param name="defectReport">The defect report.</param>
        public void SaveReport(SoftwareTask defectReport)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}({2})",_typeName,"SaveReport", defectReport);

            //Persist or Update:
// ReSharper disable RedundantLambdaSignatureParentheses
            _repositoryService.PersistOnCommit<SoftwareTask,Guid>(defectReport,true);
// ReSharper restore RedundantLambdaSignatureParentheses

        }


        /// <summary>
        /// Persists the specified feedback feature.
        /// </summary>
        /// <param name="feedbackFeature">The feedback feature.</param>
        public void Persist(SoftwareTaskFeature feedbackFeature)
        {
            _repositoryService.PersistOnCommit(feedbackFeature, f=>f.Id == Guid.Empty);
        }


        /// <summary>
        /// Deletes the specified feedback feature.
        /// </summary>
        /// <param name="feedbackFeature">The feedback feature.</param>
        public void Delete(SoftwareTaskFeature feedbackFeature)
        {
            _repositoryService.DeleteOnCommit(feedbackFeature);

        }

    }
}