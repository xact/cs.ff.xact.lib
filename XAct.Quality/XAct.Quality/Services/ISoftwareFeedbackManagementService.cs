﻿namespace XAct.Quality
{
    using System.Linq;
    using XAct.Messages;

    /// <summary>
    /// Contract for managing Feature comments.
    /// </summary>
    public interface ISoftwareFeedbackManagementService : IHasXActLibService
    {

        /// <summary>
        /// Create or update the specified feedback feature.
        /// </summary>
        /// <param name="feedbackFeature">The feedback feature.</param>
        void Persist(SoftwareTaskFeature feedbackFeature);

        /// <summary>
        /// Deletes the specified feedback feature.
        /// </summary>
        /// <param name="feedbackFeature">The feedback feature.</param>
        void Delete(SoftwareTaskFeature feedbackFeature);

        /// <summary>
        /// Gets the feedback for feature.
        /// </summary>
        /// <param name="featureId">The feature id.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        IQueryable<SoftwareTask> GetFeedbackForFeature(string featureId,
                                                   IPagedSearchTermQuerySpecification pagedQuerySpecification);


    }
}
