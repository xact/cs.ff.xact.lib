﻿
namespace XAct.Quality
{
    /// <summary>
    /// The contract for a Service to 
    /// report software defects with more information
    /// than just a client's impression of what's going on.
    /// </summary>
    public interface IDefectReportingService : IHasXActLibService
    {
        ///// <summary>
        ///// Generates a report of the client computer's setup (invokes MSINFO32.exe)
        ///// </summary>
        ///// <param name="outputReportFilePath">The output report file path.</param>
        ///// <param name="delay">The delay.</param>
        ///// <returns></returns>
        //string GenerateReport(string outputReportFilePath = null, int delay = 240000);


        ///// <summary>
        ///// Create a screen grab of the currently selected window.
        ///// </summary>
        ///// <param name="outputReportFilePath">The output report file path.</param>
        ///// <returns></returns>
        //string CaptureScreen(string outputReportFilePath);

            
            /// <summary>
        /// Reports the defect.
        /// </summary>
        /// <param name="defectReport">The defect report.</param>
        void ReportDefect(SoftwareTask defectReport);
    }
}
