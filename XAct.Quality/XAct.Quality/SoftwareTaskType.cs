﻿
namespace XAct.Quality
{
    using System.Runtime.Serialization;
    using XAct.Messages;

    /// <summary>
    /// The type of <see cref="SoftwareTask"/>
    /// (DefectReport, Suggestion, Enhancement, etc.)
    /// </summary>
    [DataContract]
    public class SoftwareTaskType : ReferenceDataBase<int>
    {
    }
}
