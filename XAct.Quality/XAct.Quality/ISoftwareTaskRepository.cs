namespace XAct.Quality
{
    using System.Linq;
    using XAct.Messages;

    /// <summary>
    /// Contract for a Repository service used
    /// to persist a <see cref="SoftwareTask"/>
    /// </summary>
    public interface ISoftwareTaskRepositoryService : IHasXActLibService
    {

        /// <summary>
        /// Gets the software feedback feature.
        /// <para>
        /// If non found,return null.
        /// </para>
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="applicationIdentifier">The application identifier.</param>
        /// <returns></returns>
        SoftwareTaskFeature GetSoftwareFeedbackFeature(string key, string applicationIdentifier = null);


        /// <summary>
        /// Gets the feedback/tasks/defects for the specified feature.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        IQueryable<SoftwareTask> GetFeedbackForFeature(string key, IPagedQuerySpecification pagedQuerySpecification);



        /// <summary>
        /// Saves the <see cref="SoftwareTask"/>.
        /// </summary>
        /// <param name="defectReport">The defect report/enhancement/feedback/etc.</param>
        void SaveReport(SoftwareTask defectReport);




        /// <summary>
        /// Persists the specified feedback feature.
        /// </summary>
        /// <param name="feedbackFeature">The feedback feature.</param>
        void Persist(SoftwareTaskFeature feedbackFeature);


        /// <summary>
        /// Deletes the specified feedback feature.
        /// </summary>
        /// <param name="feedbackFeature">The feedback feature.</param>
        void Delete(SoftwareTaskFeature feedbackFeature);

    }
}