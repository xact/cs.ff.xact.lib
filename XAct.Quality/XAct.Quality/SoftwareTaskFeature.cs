﻿
namespace XAct.Quality
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SoftwareTaskFeature : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasKey, IHasDescription
    {

        /// <summary>
        /// Gets or sets a string identifier (name, guid, other) specifying the Application this Entity belong to.
        /// </summary>
        [DataMember]
        public string ApplicationIdentifier { get; set; }

        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasId{TId}"/>.
        /// </para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal><para>8/9/2011: Sky</para></internal>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// Gets or sets the unique key that identifies this Feature.
        /// </summary>
        /// <value>The key.</value>
        [DataMember]
        public virtual string Key { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareTaskFeature"/> class.
        /// </summary>
        public SoftwareTaskFeature()
        {
            this.GenerateDistributedId();
        }
    }
}
