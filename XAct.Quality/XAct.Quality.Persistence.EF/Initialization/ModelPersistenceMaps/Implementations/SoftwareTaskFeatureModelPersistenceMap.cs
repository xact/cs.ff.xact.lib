﻿namespace XAct.Quality.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class SoftwareTaskFeatureModelPersistenceMap : EntityTypeConfiguration<SoftwareTaskFeature>, ISoftwareTaskFeatureModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareTaskModelPersistenceMap"/> class.
        /// </summary>
        public SoftwareTaskFeatureModelPersistenceMap()
        {
            this.ToXActLibTable("SoftwareTaskFeature");


            this
    .HasKey(dr => dr.Id);

            //No way of defining Index:
            //modelBuilder.Entity<SoftwareTaskFeature>()
            //    .Property(m=>m.ApplicationIdentifier).


        }


    }
}
