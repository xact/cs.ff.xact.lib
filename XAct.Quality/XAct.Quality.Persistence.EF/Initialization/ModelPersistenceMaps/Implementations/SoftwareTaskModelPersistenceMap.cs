﻿namespace XAct.Quality.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class SoftwareTaskModelPersistenceMap : EntityTypeConfiguration<SoftwareTask>, ISoftwareTaskModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareTaskModelPersistenceMap"/> class.
        /// </summary>
        public SoftwareTaskModelPersistenceMap()
        {

            this.ToXActLibTable("SoftwareTask");



            this
                .HasKey(x => x.Id);

            int colOrder = 0;

            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this
                .Property(m => m.TypeFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


            this
                .Property(m => m.FeedbackRating)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.FeedbackFeatureFK)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;


            //Even automated defect reports should summarize what the defect is about.
            this
                .Property(m => m.Subject)
                .DefineRequired1024CharSubject(colOrder);

            this
                .Property(m => m.Body)
                .DefineRequired4000Char(colOrder);

            this
                .Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);

            this
                .Property(m => m.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++);
                






            this
                .HasOptional(m => m.Feature);

            this
                .HasMany(dr => dr.Attachments)
                .WithRequired()
                .HasForeignKey(bc => bc.DefectReportFK);

        }



    }
}