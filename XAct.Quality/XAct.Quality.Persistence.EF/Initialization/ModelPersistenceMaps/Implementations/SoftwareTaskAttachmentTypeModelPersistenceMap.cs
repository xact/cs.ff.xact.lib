﻿namespace XAct.Quality.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class SoftwareTaskAttachmentTypeModelPersistenceMap : EntityTypeConfiguration<SoftwareTaskAttachmentType>, ISoftwareTaskAttachmentTypeModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareTaskAttachmentTypeModelPersistenceMap"/> class.
        /// </summary>
        public SoftwareTaskAttachmentTypeModelPersistenceMap()
        {

            this.ToXActLibTable("SoftwareTaskAttachmentType");


            this
    .HasKey(drat => drat.Id);

            this
                .Property(m => m.Key)
                .HasMaxLength(64)
                .IsVariableLength()
                .IsRequired();

            this
                .Property(m => m.Value)
                .HasMaxLength(50)
                .IsVariableLength()
                .IsRequired();

        }


    }
}
