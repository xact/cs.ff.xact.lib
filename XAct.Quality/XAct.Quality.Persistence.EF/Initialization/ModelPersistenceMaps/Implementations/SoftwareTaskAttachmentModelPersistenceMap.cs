﻿namespace XAct.Quality.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Library.Settings;

    /// <summary>
    /// 
    /// </summary>
    public class SoftwareTaskAttachmentModelPersistenceMap : EntityTypeConfiguration<SoftwareTaskAttachment>, ISoftwareTaskAttachmentModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareTaskAttachmentModelPersistenceMap"/> class.
        /// </summary>
        public SoftwareTaskAttachmentModelPersistenceMap()
        {

            this.ToXActLibTable("SoftwareTaskAttachment");


            this
                .HasKey(dr => new { TargetMachineId = dr.MachineId, dr.Id });

            //Is the Attachment an MSInfo32 or Screengrab?

            int colOrder = 0;

            this
                .HasRequired(drat => drat.Type);

            this
                .Property(m => m.Id)
                .DefineRequiredIntId(colOrder++)
                ;



            this
                .Property(m => m.SerializationMethod)
                .DefineRequiredSerializationMethod(colOrder++)
                ;

            this
                .Property(m => m.SerializedValueType)
                .DefineRequired1024CharSerializationValueType(colOrder++)
                ;

            this
                .Property(m => m.SerializedValue)
                .DefineOptionalMaxLengthCharSerializationValue(colOrder++)//OK to be MaxLength. 4000 (50lx80c) would be too small.
                ;


            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;

        }


    }
}