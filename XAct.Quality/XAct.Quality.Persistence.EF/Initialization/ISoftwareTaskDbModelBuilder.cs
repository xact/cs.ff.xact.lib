namespace XAct.Quality.Initialization
{
    using XAct.Initialization;

    /// <summary>
    /// 
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface ISoftwareTaskDbModelBuilder : XAct.Data.EF.CodeFirst.IHasXActLibDbModelBuilder
    {

    }
}