﻿// ReSharper disable CheckNamespace
namespace XAct.Quality.Initialization.DbContextSeeders.Implementations
// ReSharper restore CheckNamespace
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="ISoftwareTaskDbContextSeeder"/> contract
    /// to seed the DefectReport tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class SoftwareTaskDbContextSeeder : XActLibDbContextSeederBase<SoftwareTask>, ISoftwareTaskDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareTaskDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public SoftwareTaskDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities() 
        {
        }
    }

}
