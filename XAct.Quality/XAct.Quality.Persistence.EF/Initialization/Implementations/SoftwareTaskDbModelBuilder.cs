﻿// ReSharper disable CheckNamespace
namespace XAct.Quality.Initialization.Implementations
// 
// ReSharper restore CheckNamespace
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Quality.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
// ReSharper disable RedundantArgumentDefaultValue
// ReSharper restore RedundantArgumentDefaultValue
    public class SoftwareTaskDbModelBuilder : ISoftwareTaskDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;


        /// <summary>
        /// Initializes a new instance of the <see cref="SoftwareTaskDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public SoftwareTaskDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<SoftwareTask>)XAct.DependencyResolver.Current.GetInstance<ISoftwareTaskModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<SoftwareTaskFeature>)XAct.DependencyResolver.Current.GetInstance<ISoftwareTaskFeatureModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<SoftwareTaskAttachment>)XAct.DependencyResolver.Current.GetInstance<ISoftwareTaskAttachmentModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<SoftwareTaskAttachmentType>)XAct.DependencyResolver.Current.GetInstance<ISoftwareTaskAttachmentTypeModelPersistenceMap>());


        }




    }
}
