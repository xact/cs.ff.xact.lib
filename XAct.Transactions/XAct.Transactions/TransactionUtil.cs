namespace XAct.Transactions
{
    using System;
    using System.Transactions;

    /// <summary>
    /// Helper methods for creating Transactions, 
    /// that could not be created as 
    /// Extension Methods
    /// </summary>
    public static class TransactionsUtils
    {
        /// <summary>
        /// Creates a non-locking TransactionScope
        /// <para>
        /// See why this Helper method is needed to produce a more performant 
        /// TransactionScope: http://blogs.msdn.com/b/dbrowne/archive/2010/06/03/using-new-transactionscope-considered-harmful.aspx
        /// </para>
        /// </summary>
        /// <returns></returns>
        public static System.Transactions.TransactionScope CreateTransactionScope()
        {
            return CreateTransactionScope(TimeSpan.MaxValue);
        }

        /// <summary>
        /// Creates a non-locking TransactionScope
        /// <para>
        /// See why this Helper method is needed to produce a more performant 
        /// TransactionScope: http://blogs.msdn.com/b/dbrowne/archive/2010/06/03/using-new-transactionscope-considered-harmful.aspx
        /// </para>
        /// </summary>
        /// <returns></returns>
        public static System.Transactions.TransactionScope CreateTransactionScope(TimeSpan timeout)
        {
            TransactionOptions transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            transactionOptions.Timeout = timeout;
            return new System.Transactions.TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

    }
}
