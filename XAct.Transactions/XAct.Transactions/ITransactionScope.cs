﻿// ReSharper disable CheckNamespace
namespace XAct.Transactions
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for a TransactionScope object
    /// that won't cause a dependency on its wrapped
    /// <c>System.Transactions.TransactionScope</c>
    /// object.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage is exactly the same as the framework one:
    /// <code>
    /// <![CDATA[
    /// using (TransactionScope transactionScope = _transactionService.CreateTransactionScope()){
    ///     //do something
    ///     transactionScope.Commit();
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public interface ITransactionScope : IHasInnerItemReadOnly //<System.Transactions.TransactionScope> 
    {
        
    }
}