﻿namespace XAct.Transactions
{
    using System;

    /// <summary>
    /// An implementation of the 
    /// <see cref="ITransactionScope"/>
    /// that won't cause a dependency on its wrapped
    /// <c>System.Transactions.TransactionObject</c>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage is exactly the same as the framework one:
    /// <code>
    /// <![CDATA[
    /// using (ITransactionScope transactionScope = _transactionService.CreateTransactionScope()){
    ///     //do something
    ///     transactionScope.Commit();
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public class TransactionScope : ITransactionScope, IDisposable
    {
        /// <summary>
        /// Commits this TransactionScope.
        /// </summary>
        public void Commit()
        {
            _innerObject.Complete();
        }

               /// <summary>
        /// Gets the inner item X.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetInnerItem<T>()
        {
            return _innerObject.ConvertTo<T>();
        }

        private readonly System.Transactions.TransactionScope _innerObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionScope"/> class.
        /// </summary>
        /// <param name="transactionScope">The transaction scope.</param>
        public TransactionScope(object transactionScope)
        {
            _innerObject = transactionScope as System.Transactions.TransactionScope;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _innerObject.Dispose();
        }
    }
}