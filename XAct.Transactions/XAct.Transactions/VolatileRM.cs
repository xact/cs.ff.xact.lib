﻿namespace XAct.Transactions
{
    using System.Transactions;
    using XAct.Diagnostics;

    /// <summary>
    /// An example of a Transacted Command.
    /// </summary>
    /// <internal>
    /// See: http://bit.ly/tGmN2v and http://bit.ly/vawwg2
    /// </internal>
    internal class VolatileRM : IEnlistmentNotification
    {
        private int _exampleValue;
        private int _oldMemberValue;

        public int ExapleValue
        {
            get { return _exampleValue; }
        }

        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="VolatileRM"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public VolatileRM(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }


        /// <summary>
        /// Executes the specified command.
        /// </summary>
        /// <param name="newMemberValue">The new member value.</param>
        public void Execute(int newMemberValue)
        {
            Transaction currentTx = Transaction.Current;

            if (currentTx != null)
            {
                _tracingService.Trace(0, TraceLevel.Verbose, "VolatileRM: Execute - EnlistVolatile");
                //As scope exists, enlist:
                currentTx.EnlistVolatile(this, EnlistmentOptions.None);
            }

            //Save the value:
            _oldMemberValue = _exampleValue;
            //Set the new one:
            _exampleValue = newMemberValue;
        }






        #region IEnlistmentNotification Members

        /// <summary>
        /// Notifies an enlisted object that a transaction is being committed.
        /// </summary>
        /// <param name="enlistment">An <see cref="T:System.Transactions.Enlistment"/> object used to send a response to the transaction manager.</param>
        public void Commit(Enlistment enlistment)
        {
            _tracingService.Trace(0, TraceLevel.Verbose, "VolatileRM: Commit");
            // Clear out oldMemberValue
            _oldMemberValue = 0;
        }
        /// <summary>
        /// Notifies an enlisted object that the status of a transaction is in doubt.
        /// </summary>
        /// <param name="enlistment">An <see cref="T:System.Transactions.Enlistment"/> object used to send a response to the transaction manager.</param>
        public void InDoubt(Enlistment enlistment)
        {
            _tracingService.Trace(0, TraceLevel.Verbose, "VolatileRM: InDoubt");
        }
        /// <summary>
        /// Notifies an enlisted object that a transaction is being prepared for commitment.
        /// </summary>
        /// <param name="preparingEnlistment">A <see cref="T:System.Transactions.PreparingEnlistment"/> object used to send a response to the transaction manager.</param>
        public void Prepare(PreparingEnlistment preparingEnlistment)
        {
            _tracingService.Trace(0, TraceLevel.Verbose, "VolatileRM: Prepare");

            preparingEnlistment.Prepared();
        }

        /// <summary>
        /// Notifies an enlisted object that a transaction is being rolled back (aborted).
        /// </summary>
        /// <param name="enlistment">A <see cref="T:System.Transactions.Enlistment"/> object used to send a response to the transaction manager.</param>
        public void Rollback(Enlistment enlistment)
        {
            _tracingService.Trace(0, TraceLevel.Verbose, "VolatileRM: Rollback");
            // Restore previous state
            _exampleValue = _oldMemberValue;

            //And clear out old value.
            _oldMemberValue = 0;
        }

        #endregion
    }
}
