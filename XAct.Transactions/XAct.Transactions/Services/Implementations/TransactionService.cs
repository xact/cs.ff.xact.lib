﻿namespace XAct.Transactions.Implementations
{
    using System;
    using System.Transactions;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of the
    /// <see cref="ITransactionService" /> contract to create
    /// a service that returns a TransactionScope that
    /// does not cause a dependency on the
    /// <c>System.Transactions</c> asssembly.
    /// </summary>
    public class TransactionService : XActLibServiceBase, ITransactionService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public TransactionService(ITracingService tracingService) : base(tracingService)
        {
        }

        /// <summary>
        /// Creates a portable TransactionScope
        /// that won't cause a dependency on System.Transactions.
        /// <para>
        /// The default timespan is 30 seconds.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public ITransactionScope CreateTransactionScope()
        {
            return CreateTransactionScope(TimeSpan.FromSeconds(30));
        }

        /// <summary>
        /// Creates a portable TransactionScope
        /// that won't cause a dependency on System.Transactions.
        /// </summary>
        /// <param name="timeSpan">The time span.</param>
        /// <returns></returns>
        public ITransactionScope CreateTransactionScope(TimeSpan timeSpan)
        {
            System.Transactions.TransactionScope transactionScope = 
                TransactionsUtils.CreateTransactionScope(timeSpan);

            return new Transactions.TransactionScope(transactionScope);
        }

        /// <summary>
        /// Gets the current 
        /// <see cref="ITransactionScope"/> if any.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This can be used by <c>IUnitOfWork</c>
        /// to determine whether to join an existing transaction, and
        /// how to Commit.
        /// </para>
        /// <para>
        /// Note that there's a non safe way of getting TransactionScope:
        /// http://stackoverflow.com/a/980358
        /// </para>
        /// </remarks>
        public Transaction Current
        {
            get
            {
                return Transaction.Current;


            }
        }
    }
}
