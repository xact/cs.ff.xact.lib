﻿// ReSharper disable CheckNamespace
namespace XAct.Transactions
// ReSharper restore CheckNamespace
{
    using System;
    using System.Transactions;

    /// <summary>
    /// The contract for a service that returns
    /// TransactionScope objects that do not cause
    /// a dependency on the 
    /// <c>System.Transactions</c> assembly.
    /// </summary>
    public interface ITransactionService : IHasXActLibService
    {

        /// <summary>
        /// Gets the current 
        /// <see cref="ITransactionScope"/> if any.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that there's a non safe way of getting TransactionScope:
        /// http://stackoverflow.com/a/980358
        /// </para>
        /// </remarks>
        Transaction Current { get; }

        /// <summary>
        /// Creates a portable TransactionScope
        /// that won't cause a dependency on System.Transactions.
        /// <para>
        /// The default timespan is 30 seconds.
        /// </para>
        /// </summary>
        /// <returns></returns>
        ITransactionScope CreateTransactionScope();
        /// <summary>
        /// Creates a portable TransactionScope
        /// that won't cause a dependency on System.Transactions.
        /// </summary>
        /// <param name="timeSpan">The time span.</param>
        /// <returns></returns>
        ITransactionScope CreateTransactionScope(TimeSpan timeSpan);
        
        
    }
}