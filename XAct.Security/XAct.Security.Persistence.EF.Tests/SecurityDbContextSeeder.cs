﻿namespace XAct.Tests
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Library.Settings;
    using XAct.Security.Authorization;
    using XAct.Security.Authorization.Initialization.DbContextSeeders;
    using XAct.Services;

    public class OperationDbContextSeeder : XActLibDbContextSeederBase<Operation>, IOperationDbContextSeeder
    {
        private readonly IEnvironmentService _environmentService;

        public OperationDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _environmentService = environmentService;
        }

        public override void SeedInternal(DbContext dbContext) 
        {
            this.SeedInternalHelper(dbContext,true,x=>x.Name);



        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<Operation>();

            this.InternalEntities.Add(
                new Operation
                    {
                        Enabled = true,
                        Name = "Op1"
                    });

            this.InternalEntities.Add(
                new Operation
                    {
                        Enabled = true,
                        Name = "Op2"
                    });

        }

    }
}
