namespace XAct.Tests
{
    using System;
    using System.Security.Principal;
    using NUnit.Framework;
    using XAct.Security.Authorization;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IOperationBasedAuthorizationManagementServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Get_IOperationBasedAuthorizationManagementService()
        {
            IOperationBasedAuthorizationManagementService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationManagementService>();

            Assert.IsTrue(service!=null);
        }


        [Test]
        public void GetAuthenticationForUnRegisteredMethod()
        {
            IOperationBasedAuthorizationManagementService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationManagementService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "P1" });

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized("Op_Unknown", out authorisationResult, genericPrincipal);

            //This will only be true if config values are manually changed.
            Assert.IsFalse(check);
        }

        [Test]
        public void GetAuthenticationForRegisteredMethodButNotInUserRoles()
        {
            IOperationBasedAuthorizationManagementService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationManagementService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "P8" });

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);

            Assert.IsFalse(check);
        }

        
        [Test]
        public void GetAuthenticationForRegisteredMethodWithUserPermission()
        {
            IOperationBasedAuthorizationManagementService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationManagementService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"),new string[]{"P1"});

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);

            Assert.IsTrue(check);
        }


        [Test]
        public void TogglePermission()
        {
            IOperationBasedAuthorizationManagementService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationManagementService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "P1" });


            service.DisableOperationPermission("Op1","P1");

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);

            Assert.IsFalse(check);

            service.EnableOperationPermission("Op1", "P1");

            check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);

            Assert.IsTrue(check);
        }
        [Test]
        public void AddNewPermission()
        {
            IOperationBasedAuthorizationManagementService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationManagementService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "PNEW" });

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);

            Assert.IsFalse(check);

            service.SetOperationPermission("Op1", "PNEW");

            check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);

            Assert.IsTrue(check);

            service.RemoveOperationPermission("Op1","PNEW");
            check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);
            Assert.IsFalse(check);
        }

        [Test]
        public void MovePermissionUP()
        {
            IOperationBasedAuthorizationManagementService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationManagementService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "P1","P3" });


            for (int i = 0; i < 10; i++)
            {
                service.MoveOperationPermissionUp("Op1", "P1");
            }

            bool check;
            XAct.Security.AuthorizationResult authorisationResult;
            check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);
            Assert.IsTrue(check);


            service.MoveOperationPermissionUp("Op1", "P3");
            check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);
            Assert.IsTrue(check);

            for (int i = 0; i < 5; i++)
            {
                service.MoveOperationPermissionUp("Op1", "P3");
            }
            check = service.IsAuthorized("Op1", out authorisationResult, genericPrincipal);
            Assert.IsFalse(check);

        }
    }


}


