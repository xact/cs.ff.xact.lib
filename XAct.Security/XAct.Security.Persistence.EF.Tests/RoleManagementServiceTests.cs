﻿
namespace XAct.Security.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Security.Authorization.RBAC;
    using XAct.Security.Authorization.RBAC.Implementations;
    using XAct.Security.Entities;
    using XAct.Tests;
    using XAct.Tests.Services.Implementations;

    [TestFixture]
    public class RoleManagementServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Get_IRoleManagementService()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();

            Assert.IsTrue(service != null);
        }

        [Test]
        public void Get_IRoleManagementService_Of_Expected_Type()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();

            Assert.That(service.GetType(), Is.EqualTo(typeof(RoleManagementService)));
        }


        [Test]
        public void Get_Check_If_NonExistent_Role_Exists()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();

            var result = service.Exists(x => x.Name == "NonExistentRole");

            Assert.That(result,Is.False);
        }

        [Test]
        public void Create_Enabled_Role()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var roleName = "NewRole1";
            service.CreateRole(roleName);
            uow.GetCurrent().Commit();

            Role result = service.GetByKey(roleName);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Name, Is.EqualTo(roleName));
        }

        [Test]
        public void Create_Disabled_Role()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var roleName = "NewRole2";
            service.CreateRole(roleName,false);
            uow.GetCurrent().Commit();

            Role result = service.GetByKey(roleName);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Name, Is.EqualTo(roleName));
            Assert.That(result.Enabled, Is.False);
        }


        [Test]
        public void List_All_Roles_For_Application()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();

            var result = service.GetRoles();
            foreach (var r in result)
            {
                Console.WriteLine(r.Name);
            }
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.GreaterThan(0));
            Assert.That(result.Any(x=>x.Enabled), Is.True);
            Assert.That(result.Any(x => x.Enabled==false), Is.True);
        }

        [Test]
        public void List_All_Disabled_Roles_For_Application()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var result = service.GetRoles(false);
            foreach (var r in result)
            {
                Console.WriteLine(r.Name);
            }

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.GreaterThan(0));
            Assert.That(result.Any(x => x.Enabled), Is.False);
            Assert.That(result.All(x => !x.Enabled), Is.True);
        }
        [Test]
        public void List_All_Enabled_Roles_For_Application()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var result = service.GetRoles(true);
            foreach (var r in result)
            {
                Console.WriteLine(r.Name);
            }

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.GreaterThan(0));
            Assert.That(result.All(x => x.Enabled), Is.True);
        }


        [Test]
        public void Check_For_Role_Exists()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var result1 = service.Exists("RoleABC");
            service.CreateRole("RoleABC");
            uow.GetCurrent().Commit();
            var result2 = service.Exists("RoleABC");

            Assert.That(result1, Is.False,"Should not exist before role is created.");
            Assert.That(result2, Is.Not.False,"Should have been created.");
        }

        [Test]
        public void Rename_Role()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var roleName = "NewRoleDEF";
            var result1 = service.Exists(roleName);
            service.CreateRole(roleName);
            uow.GetCurrent().Commit();
            var result2 = service.Exists(roleName);
            service.RenameRole(roleName, roleName+"_X");
            uow.GetCurrent().Commit();
            var result3 = service.Exists(roleName);
            var result4 = service.Exists(roleName+"_X");

            Assert.That(result1, Is.False,"1");
            Assert.That(result2, Is.True, "2");
            Assert.That(result3, Is.False, "3");
            Assert.That(result4, Is.True, "4");
        }

        [Test]
        public void Delete_Role()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var roleName = "NewRoleGHI";
            var result1 = service.Exists(roleName);
            service.CreateRole(roleName);
            uow.GetCurrent().Commit();
            var result2 = service.Exists(roleName);
            service.DeleteRole(roleName);
            uow.GetCurrent().Commit();
            var result3 = service.Exists(roleName);

            Assert.That(result1, Is.False,"1");
            Assert.That(result2, Is.Not.False, "2");
            Assert.That(result3, Is.False, "3");

        }

        [Test]
        public void Change_EnabledState()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var roleName = "NewRoleJKL";

            var result1 = service.Exists(roleName);
            service.CreateRole(roleName);
            uow.GetCurrent().Commit();
            var result2 = service.Exists(roleName);

            var result3 = service.GetEnabledState(roleName);
            service.ChangeEnabledState(roleName, false);
            uow.GetCurrent().Commit();
            var result4 = service.GetEnabledState(roleName);
            service.ChangeEnabledState(roleName, true);
            uow.GetCurrent().Commit();
            var result5 = service.GetEnabledState(roleName);

            Assert.That(result1, Is.False,"1");
            Assert.That(result2, Is.Not.False, "2");
            Assert.That(result3, Is.True, "3");
            Assert.That(result4, Is.False, "4");
            Assert.That(result5, Is.True, "5");
        }
        [Test]
        public void Check_NonExistent_Role_State()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var roleName = "NewRole_Does_Not_Exist";

            var result1 = service.GetEnabledState(roleName);
            
            Assert.That(result1, Is.Null, "1");
        }

        [Test]
        public void Is_User_In_Role()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var roleName = "Role_SomeRole";
            var userA = "userA";

            if (!service.Exists(roleName))
            {
                service.CreateRole(roleName);
                uow.GetCurrent().Commit();
            }

            var result = service.IsUserInRole(userA,roleName);

            Assert.That(result, Is.False);
        }

        [Test]
        public void Add_User_To_Role()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var roleName = "Role_SomeRole";
            var userA = "userA123";

            if (!service.Exists(roleName))
            {
                service.CreateRole(roleName);
                uow.GetCurrent().Commit();
            }


            var roleExists = service.Exists(roleName);

            var result1 = service.IsUserInRole(userA, roleName);
            service.AddUserToRole(userA,roleName);
            uow.GetCurrent().Commit();

            var result2 = service.IsUserInRole(userA, roleName);

            Assert.That(roleExists,Is.True,"Role should exist");
            Assert.That(result1, Is.False, "User should not be member of role yet.");
            Assert.That(result2, Is.True,"User Should now be member of Role");
        }

        [Test]
        public void Remove_User_From_Role()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var roleName = "Role_SomeRole";
            var userA = "userA12345";

            if (!service.Exists(roleName))
            {
                service.CreateRole(roleName);
                uow.GetCurrent().Commit();
            }


            var result1 = service.IsUserInRole(userA,roleName);
            service.AddUserToRole(userA,roleName);
            uow.GetCurrent().Commit();

            var result2 = service.IsUserInRole(userA, roleName);

            service.RemoveUserFromRoleRole(userA, roleName);
            uow.GetCurrent().Commit();
            var result3 = service.IsUserInRole(userA, roleName);
            

            Assert.That(result1, Is.False);
            Assert.That(result2, Is.True);
            Assert.That(result3, Is.False);
        }

        [Test]
        public void Can_Delete_Relationship_Without_Deleting_Role()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var roleName = "ANewRole_QWE123";
            var roleName2 = "ANewRole_QWE1234";
            var roleName3 = "ANewRole_QWE12345";
            var userName = "BinkyBoo";

            service.CreateRole(roleName);
            service.CreateRole(roleName2);
            service.CreateRole(roleName3);
            uow.GetCurrent().Commit();

            service.AddUserToRole(userName, roleName);
            service.AddUserToRole(userName, roleName2);
            uow.GetCurrent().Commit();

            var roles1=service.GetUsersRoles(userName);

            service.AddUserToRole(userName, roleName3);
            uow.GetCurrent().Commit();

            var roles2 = service.GetUsersRoles(userName);

            service.RemoveUserFromRoleRole(userName, roleName);
            uow.GetCurrent().Commit();

            var roles3 = service.GetUsersRoles(userName);

            Assert.That(roles1.Count(), Is.EqualTo(2));
            Assert.That(roles2.Count(), Is.EqualTo(3));
            Assert.That(roles3.Count(), Is.EqualTo(2));
        }

        [Test]
        public void Can_Get_List_Of_Users_Roles()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var roleName = "ANewRole_QWE";
            var userName = "Binky";

            service.CreateRole(roleName);
            uow.GetCurrent().Commit();
            service.AddUserToRole(userName, roleName);
            uow.GetCurrent().Commit();

            bool isInRoleBefore = service.IsUserInRole(userName,roleName);

            var roleBeforeDeletingOnlyRelationship = service.Exists(roleName);
            service.RemoveUserFromRoleRole(userName, roleName);
            uow.GetCurrent().Commit();
            var roleAfterDeletingOnlyRelationship = service.Exists(roleName);

            bool isInRoleAfter = service.IsUserInRole(userName, roleName);
            
            Assert.That(roleBeforeDeletingOnlyRelationship,Is.Not.Null,"Role exists before deleting relationship");
            Assert.That(roleAfterDeletingOnlyRelationship, Is.Not.Null, "Role no longer should before deleting relationship");


            Assert.That(isInRoleBefore, Is.True,"Is in role before deleting relationship");
            Assert.That(isInRoleAfter, Is.False,"Is no longer in role after deleting relationship");
        }

        [Test]
        public void Can_Make_A_Role_A_Parent_Of_Another()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRoleManagementService>();
#pragma warning disable 168
            var uow = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
#pragma warning restore 168

            var roleName = "ANewRole_QWE123456";

            service.CreateRole(roleName);
            uow.GetCurrent().Commit();

            var roleName2 = "ANewRole_QWE123456231";

            service.CreateRole(roleName2);
            uow.GetCurrent().Commit();


            var role1= service.GetByKey(roleName);
            var role2 = service.GetByKey(roleName2);


            role2.Parent = role1;
            uow.GetCurrent().Commit();

            var role3 = service.GetByKey(roleName2, true);

            Assert.That(role3,Is.Not.Null);
            Assert.That(role3.Parent.Name, Is.EqualTo(role1.Name));
        }

    }
}
