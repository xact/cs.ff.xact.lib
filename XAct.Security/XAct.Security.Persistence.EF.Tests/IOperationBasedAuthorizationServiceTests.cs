namespace XAct.Tests
{
    using System;
    using System.Security.Principal;
    using NUnit.Framework;
    using XAct.Security;
    using XAct.Security.Authorization;
    using XAct.Security.Tests.Data;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IOperationBasedAuthorizationServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void GetRightService()
        {
            IOperationBasedAuthorizationService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationService>();

            Assert.IsTrue(service!=null);
        }


        [Test]
        public void GetAuthenticationForMethodWhenNoProvider()
        {
            IOperationBasedAuthorizationService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationService>();

            GenericPrincipal genericPrincipal = null;
            
            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized(typeof(TestTarget).GetMethod("Foo1"), out authorisationResult, genericPrincipal);

            Assert.IsFalse(check);
            Assert.AreEqual(AuthorizationResult.NotAuthenticated, authorisationResult);
        }


        [Test]
        public void GetAuthenticationForMethodMarkedAnonymous()
        {
            IOperationBasedAuthorizationService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "R1" });

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized(typeof(TestTarget).GetMethod("Foo2"), out authorisationResult, genericPrincipal);

            Assert.IsTrue(check);
            Assert.AreEqual(AuthorizationResult.AnonymousPermitted, authorisationResult);
        }

        [Test]
        public void GetAuthenticationForMethodWhenNotInRoles()
        {
            IOperationBasedAuthorizationService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "R1" });

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized(typeof(TestTarget).GetMethod("Foo3"), out authorisationResult, genericPrincipal);

            Assert.IsFalse(check);
            Assert.AreEqual(AuthorizationResult.NotAuthorized, authorisationResult);
        }



        [Test]
        public void GetAuthenticationForMethodWhenInRoles()
        {
            IOperationBasedAuthorizationService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "R2" });

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized(typeof(TestTarget).GetMethod("Foo4"), out authorisationResult, genericPrincipal);

            Assert.IsTrue(check);
            Assert.AreEqual(AuthorizationResult.Authorized, authorisationResult);
        }



        [Test]
        public void GetAuthenticationForMethodWhenInRolesButDeniedInAnother()
        {
            IOperationBasedAuthorizationService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "R2", "R3" });

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized(typeof(TestTarget).GetMethod("Foo5"), out authorisationResult, genericPrincipal);

            Assert.IsFalse(check);
            Assert.AreEqual(AuthorizationResult.NotAuthorized, authorisationResult);
        }

        [Test]
        public void GetAuthenticationForMethodWhenInRolesButDeniedInAnother_2()
        {
            IOperationBasedAuthorizationService service =
                XAct.DependencyResolver.Current.GetInstance<IOperationBasedAuthorizationService>();

            GenericPrincipal genericPrincipal = new GenericPrincipal(new GenericIdentity("Paul"), new string[] { "R2", "R3" });

            XAct.Security.AuthorizationResult authorisationResult;
            bool check = service.IsAuthorized(typeof(TestTarget).GetMethod("Foo6"), out authorisationResult, genericPrincipal);

            //It depends on the order...since R2 is spotted before R3, it's authorized.
            Assert.IsTrue(check);
            Assert.AreEqual(AuthorizationResult.Authorized, authorisationResult);
        }

        

    }


}


