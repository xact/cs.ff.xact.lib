﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Security.Tests.Data
{
    public class TestTarget
    {
        [Authorization(Roles = "A,B,C,R2")]
        public void Foo1() { }

        [AllowAnonymous]
        [Authorization(Roles = "A,B,C,R2")]
        public void Foo2() { }

        [Authorization(Roles = "A,B,C,R2")]
        public void Foo3() { }

        [Authorization(Roles = "A,B,C,R2")]
        public void Foo4() { }

        [Authorization(Roles = "A,B,C,R3:D,R2")]
        public void Foo5() { }

        [Authorization(Roles = "A,B,C,R2,R3:D")]
        public void Foo6() { }

    }
}
