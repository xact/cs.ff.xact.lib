﻿namespace XAct.Tests
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Security.Authorization;
    using XAct.Security.Authorization.Initialization.DbContextSeeders;

    public class OperationPermissionDbContextSeeder : XActLibDbContextSeederBase<OperationPermission>,
                                                      IOperationPermissionDbContextSeeder
    {
        private readonly IOperationDbContextSeeder _operationDbContextSeeder;

        public OperationPermissionDbContextSeeder(ITracingService tracingService, IOperationDbContextSeeder operationDbContextSeeder )
            : base(tracingService, operationDbContextSeeder)
        {
            _operationDbContextSeeder = operationDbContextSeeder;
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true);
        }

        public override void CreateEntities()
        {

            this.InternalEntities = new List<OperationPermission>();

            Operation operation = _operationDbContextSeeder.Entities.Single(m => m.Name == "Op1");
            //Use Extension method to create OperationPermission objects:
            operation.SetPermissions("P1,P2,!P3,^P4,P5");
            this.InternalEntities.Add(operation.Permissions);
            
            operation = _operationDbContextSeeder.Entities.Single(m => m.Name == "Op2");
            //Use Extension method to create OperationPermission objects:
            operation.SetPermissions("P1,P2,!P3,^P4,P5");
            this.InternalEntities.Add(operation.Permissions);

            

        }

    }
}