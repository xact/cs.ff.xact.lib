﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Security.Cryptography.X509Certificates;

#endif

    /// <summary>
    /// Extension methods to certificate Store.
    /// </summary>
    public static class X509StoreExtensions
    {

        /// <summary>
        /// Gets fromt store the named certificate.
        /// </summary>
        /// <param name="x509Store">The X509 store.</param>
        /// <param name="certificateSubjectName">Name of the certificate subject.</param>
        /// <returns></returns>
        public static X509Certificate2 GetCertificate(this X509Store x509Store, string certificateSubjectName)
        {
            X509Certificate2 result = null;

            X509Certificate2Collection certificates = null;

            try
            {

                //
                // Every time we call store.Certificates property, a new collection will be returned.
                //

                certificates = x509Store.Certificates;


                foreach (X509Certificate2 cert in certificates)
                {
                    if (
                        string.Compare(cert.SubjectName.Name, certificateSubjectName, StringComparison.OrdinalIgnoreCase) !=
                        0)
                    {
                        continue;
                    }

                    if (result != null)
                    {
                        throw new ApplicationException(
                            "There are more than one certificate found for subject Name {0}".
                                FormatStringExceptionCulture(certificateSubjectName));
                    }
                    result = new X509Certificate2(cert);
                }

                if (result == null)
                {
                    throw new ApplicationException(
                        "No certificate was found for subject Name {0}".FormatStringExceptionCulture(
                            certificateSubjectName));
                }

                return result;
            }
            finally
            {
                if (certificates != null)
                {
                    foreach (X509Certificate2 cert in certificates)
                    {
                        cert.Reset();
                    }
                }
            }
            //return result;

        }
    }



#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
