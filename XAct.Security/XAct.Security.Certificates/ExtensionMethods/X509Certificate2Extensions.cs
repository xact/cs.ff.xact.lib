﻿namespace XAct
{
    using System;
    using System.Security.Cryptography.X509Certificates;
    using XAct.Environment;

    /// <summary>
    /// Extensions to objects of type <see cref="X509Certificate2"/>
    /// </summary>
    public static class X509Certificate2Extensions
    {
        /// <summary>
        /// Gets the expiration date.
        /// </summary>
        /// <param name="x509Certificate2">The X509 certificate2.</param>
        /// <returns></returns>
        public static DateTime GetExpirationDate(this X509Certificate2 x509Certificate2)
        {
            DateTime dateTime = Convert.ToDateTime(x509Certificate2.GetExpirationDateString());

            return dateTime;
        }

        /// <summary>
        /// Determines whether the specified X509 certificate's date (only) is expired.
        /// </summary>
        /// <param name="x509Certificate2">The X509 certificate2.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <returns></returns>
        public static bool IsExpired(this X509Certificate2 x509Certificate2, IDateTimeService dateTimeService = null)
        {
            if (dateTimeService == null)
            {
                dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            }

            DateTime dateTime = x509Certificate2.GetExpirationDate();

            return (dateTime < dateTimeService.NowUTC);
        }
    }
}