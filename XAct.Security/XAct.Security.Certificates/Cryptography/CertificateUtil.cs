namespace XAct.Security.Cryptography
{
    using System.Security.Cryptography.X509Certificates;

    /// <summary>
    ///   A utility class which helps to retrieve an x509 certificate
    /// </summary>
    public class CertificateUtil
    {
        /// <summary>
        ///   Gets an X.509 certificate given the name, 
        ///   store location and the subject distinguished name of the X.509 certificate.
        /// </summary>
        /// <param name = "storeName">Specifies the name of the X.509 certificate to open.</param>
        /// <param name = "storeLocation">Specifies the location of the X.509 certificate store.</param>
        /// <param name = "subjectName">Subject distinguished name of the certificate to return.</param>
        /// <returns>The specific X.509 certificate.</returns>
        public X509Certificate2 GetCertificate(StoreName storeName, StoreLocation storeLocation, string subjectName)
        {
            subjectName.ValidateIsNotNullOrEmpty("subjectName");

            X509Store store = new X509Store(storeName, storeLocation);

            store.Open(OpenFlags.ReadOnly);
            return store.GetCertificate(subjectName);

        }
    }
}