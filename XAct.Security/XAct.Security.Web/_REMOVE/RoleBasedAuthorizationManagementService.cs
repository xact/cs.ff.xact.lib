﻿//namespace XAct.Security.Authorization.RBAC.Implementations
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Web.Security;
//    using XAct.Diagnostics;
//    using XAct.Services;

//    /// <summary>
//    /// An <see cref="System.Web.Security.RoleProvider"/> based 
//    /// implementation of <see cref="IRoleBasedAuthorizationManagementService"/>
//    /// to add/remove/etc. Roles from users.
//    /// <para>
//    /// Do not confuse with <see cref="RoleProviderAuthorizationService"/>
//    /// </para>
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// As per the SOC and ISP priciples, this class is for managing the roles, 
//    /// disctinct from authentication methods as expressed in 
//    /// <see cref="IRoleBasedAuthorizationService"/>
//    /// </para>
//    /// 	<para>
//    /// This AuthorizationService relies on the ASP.NET Membership provider being configured.
//    /// </para>
//    /// 	<para>
//    /// 		<code>
//    /// 			<![CDATA[
//    /// <configuration>
//    /// <connectionStrings>
//    /// <add name="MySqlConnection" connectionString="Data
//    /// Source=MySqlServer;Initial Catalog=aspnetdb;Integrated Security=SSPI;" />
//    /// </connectionStrings>
//    /// <system.web>
//    /// <authorization>
//    /// <deny users="?" />
//    /// </authorization>
//    /// <authentication mode="Forms" >
//    /// <forms loginUrl="login.aspx"
//    /// name=".ASPXFORMSAUTH" />
//    /// </authentication>
//    /// <!-- default values (see http://bit.ly/iLDnr3) are shown -->
//    /// <membership defaultProvider="AspNetSqlProvider" 
//    ///             userIsOnlineTimeWindow="10" 
//    ///             hashAlgorithmType="SHA1">
//    /// <providers>
//    /// <clear />
//    /// <add name="SqlProvider"
//    /// type="System.Web.Security.SqlMembershipProvider"
//    /// connectionStringName="MySqlConnection"
//    /// applicationName="MyApplication"
//    /// requiresUniqueEmail="false"
//    /// />
//    /// </providers>
//    /// </membership>
//    /// </system.web>
//    /// </configuration>
//    /// ]]>
//    /// 		</code>
//    /// 	</para>
//    /// 	<para>
//    /// Note that the default Values are as follows (see http://bit.ly/juT8Y8)
//    /// and found in the machine.config are:
//    /// <code>
//    /// 			<![CDATA[
//    /// <membership>
//    /// <providers>
//    /// <add name="AspNetSqlMembershipProvider"
//    /// type="System.Web.Security.SqlMembershipProvider, ..."
//    /// connectionStringName="LocalSqlServer"
//    /// enablePasswordRetrieval="false"
//    /// enablePasswordReset="true"
//    /// requiresQuestionAndAnswer="true"
//    /// applicationName="/"
//    /// requiresUniqueEmail="false"
//    /// passwordFormat="Hashed"
//    /// maxInvalidPasswordAttempts="5"
//    /// minRequiredPasswordLength="7"
//    /// minRequiredNonalphanumericCharacters="1"
//    /// passwordAttemptWindow="10"
//    /// passwordStrengthRegularExpression=""
//    /// />
//    /// </providers>
//    /// </membership>
//    /// ]]>
//    /// 		</code>
//    /// 		<para>
//    /// Note that passwordFormat="Hashed" means password retrieval is not permitted.
//    /// Note that passwordFormat can't be changed to "Encrypted" without a MachineKey defined in web.config
//    /// </para>
//    /// 	</para>
//    /// </remarks>
//    public class RoleBasedAuthorizationManagementService : XActLibServiceBase, IRoleBasedAuthorizationManagementService
//    {
//        #region Services



//        #endregion

//        #region Constructor

//        /// <summary>
//        /// Initializes a new instance of the <see cref="RoleBasedAuthorizationManagementService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The logging service.</param>
//        /// <internal>5/15/2011: Sky</internal>
//        public RoleBasedAuthorizationManagementService(ITracingService tracingService):base(tracingService)
//        {
//        }

//        #endregion

//        #region Implementation of IRoleBasedAuthorizationManagementService

//        /// <summary>
//        /// Doeses the role exist.
//        /// </summary>
//        /// <param name="roleName">Name of the role.</param>
//        /// <returns></returns>
//        /// <internal>5/11/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public bool DoesRoleExist(string roleName)
//        {
//            _tracingService.QuickTrace("roleName:{0}".FormatStringCurrentCulture(roleName));

//            //Use underlying System.Web.Security
//            return Roles.RoleExists(roleName);
//        }

//        /// <summary>
//        /// Renames the role and users.
//        /// <para>
//        /// Warning: this method, is using ASP.NET's RoleProvider,
//        /// is highly unoptimized.
//        /// Consider doing the changes more directly.
//        /// </para>
//        /// </summary>
//        /// <param name="currentRoleName">Name of the current role.</param>
//        /// <param name="newRoleName">New name of the role.</param>
//        /// <internal>8/14/2011: Sky</internal>
//        /// <internal><para>8/14/2011: Sky</para></internal>
//        public void RenameRoleAndUsers(string currentRoleName, string newRoleName)
//        {
//            string[] users = Roles.GetUsersInRole(currentRoleName);
//            Roles.CreateRole(newRoleName);
//            Roles.AddUsersToRole(users, newRoleName);
//            Roles.RemoveUsersFromRole(users, currentRoleName);
//            Roles.DeleteRole(currentRoleName);
//        }

//        /// <summary>
//        /// Gets all role names for the current application.
//        /// </summary>
//        /// <returns></returns>
//        /// <internal>5/11/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public string[] GetAllRoleKeys()
//        {
//            _tracingService.QuickTrace();

//            return Roles.GetAllRoles();
//        }



//        /// <summary>
//        /// Adds the user to specified roles.
//        /// </summary>
//        /// <param name="userName">Name of the user.</param>
//        /// <param name="roleNames">The role names.</param>
//        /// <internal>5/11/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void AddUserToRoles(string userName, params string[] roleNames)
//        {
//            _tracingService.QuickTrace("userName:{0}, roleNames:{1}".FormatStringCurrentCulture(userName, roleNames.JoinSafely(",")));

//            Roles.AddUserToRoles(userName, roleNames);
//        }

//        /// <summary>
//        /// Adds the users to roles.
//        /// </summary>
//        /// <param name="userNames">The user names.</param>
//        /// <param name="roleNames">The role names.</param>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void AddUsersToRoles(string[] userNames, params string[] roleNames)
//        {
//            _tracingService.QuickTrace("userNames:{0}, roleNames:{1}".FormatStringCurrentCulture(userNames.JoinSafely(","), roleNames.JoinSafely(",")));
//            Roles.AddUsersToRoles(userNames, roleNames);
//        }

//        /// <summary>
//        /// Removes the user from the specified roles.
//        /// </summary>
//        /// <param name="userName">Name of the user.</param>
//        /// <param name="roleName">Name of the role.</param>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void RemoveUserFromRoles(string userName, params string[] roleName)
//        {
//            _tracingService.QuickTrace("userName:{0}, roleNames:{1}".FormatStringCurrentCulture(userName, roleName));
//            Roles.RemoveUserFromRoles(userName, roleName);
//        }

//        /// <summary>
//        /// Removes the user from the specified roles.
//        /// </summary>
//        /// <param name="userNames">The user names.</param>
//        /// <param name="roleNames">The role names.</param>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void RemoveUserFromRoles(string[] userNames, params string[] roleNames)
//        {
//            _tracingService.QuickTrace("userNames:{0}, roleNames:{1}".FormatStringCurrentCulture(userNames.JoinSafely(","), roleNames.JoinSafely(",")));
//            Roles.RemoveUsersFromRoles(userNames, roleNames);
//        }


//        /// <summary>
//        /// Gets the count of all users in the roles.
//        /// <para>
//        /// Note: be careful when this is invoking the .NET 2.0 ASP MembershipProvider:
//        /// it isn't fine grain enough (it pulls back *all* users before counting them).
//        /// </para>
//        /// </summary>
//        /// <param name="roleName">Name of the role.</param>
//        /// <returns></returns>
//        /// <internal>5/11/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public int GetCountOfUsersInRole(params string[] roleName)
//        {
//            _tracingService.QuickTrace("roleName:{1}".FormatStringCurrentCulture(roleName.JoinSafely(",")));
//            return GetUsersInRole(Int32.MaxValue, 0, roleName).Length;
//        }

//        /// <summary>
//        /// Gets a list of the users in given roles.
//        /// <para>
//        /// Note: not optimized, as RolesProvider method returns all users before paging the list server side.
//        /// </para>
//        /// </summary>
//        /// <param name="pageSize">Size of the page.</param>
//        /// <param name="pageIndex">0 based Index of the page.</param>
//        /// <param name="roleNames">The role names.</param>
//        /// <returns></returns>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public string[] GetUsersInRole(int pageSize, int pageIndex, params string[] roleNames)
//        {
//            _tracingService.QuickTrace("pageSize:{0}, pageIndex:{1}, roleNames:{2}".FormatStringCurrentCulture(pageSize,pageIndex,roleNames.JoinSafely(",")));

//            //Rather annoying...
//            List<string> results = new List<string>();
//            foreach (string roleName in roleNames)
//            {
//                Roles.GetUsersInRole(roleName);
//            }

//            //Use Extension method to return subset:
//            results.GetPage(pageSize, pageIndex);

//            return results.ToArray();
//        }

//        /// <summary>
//        /// Gets the roles the specified user is in.
//        /// </summary>
//        /// <param name="userName">Name of the user.</param>
//        /// <returns></returns>
//        /// <internal>5/11/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public string[] GetRoleKeysForUser(string userName)
//        {
//            _tracingService.QuickTrace("userName:{0}".FormatStringCurrentCulture(userName));
//            return Roles.GetRolesForUser(userName);
//        }

//        /// <summary>
//        /// Determines whether the user is in any of the given roles.
//        /// </summary>
//        /// <param name="roleNames">The role names.</param>
//        /// <returns>
//        /// 	<c>true</c> if [is in role] [the specified role names]; otherwise, <c>false</c>.
//        /// </returns>
//        /// <internal>5/9/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public bool IsInRole(params string[] roleNames)
//        {
//            _tracingService.QuickTrace("roleNames:{0}".FormatStringCurrentCulture(roleNames.JoinSafely(",")));

//            return roleNames.Any(roleName => Roles.IsUserInRole(roleName));
//        }


//        /// <summary>
//        /// Determines whether the current user is in one of the roles
//        /// defined in the csv string (';|,').
//        /// </summary>
//        /// <param name="roleNames">The role names.</param>
//        /// <param name="interpretEmptyStringAsAllowed">if set to <c>true</c> returns true if the roleNames string is empty.</param>
//        /// <returns>
//        /// 	<c>true</c> if [is in role] [the specified role names]; otherwise, <c>false</c>.
//        /// </returns>
//        /// <internal><para>8/15/2011: Sky</para></internal>
//        public bool IsInRole(string roleNames, bool interpretEmptyStringAsAllowed = false)
//        {
//            if (string.IsNullOrEmpty(roleNames))
//            {
//                return interpretEmptyStringAsAllowed;
//            }

//            string[] roleNamesList = roleNames.Split(new[] {';', '|', ','}, StringSplitOptions.RemoveEmptyEntries);

//            return IsInRole(roleNamesList);
//        }
//        #endregion


//        ///// <summary>
//        ///// Gets an array of the roles that are parents to this role.
//        ///// </summary>
//        ///// <param name="roleName">Name of the role.</param>
//        ///// <returns></returns>
//        ///// <internal>8/14/2011: Sky</internal>
//        ///// <internal><para>8/14/2011: Sky</para></internal>
//        //public string[] GetAncestorRoles(string roleName)
//        //{
//        //    return roleName.SplitPath('/');
//        //}

//        ///// <summary>
//        ///// In a RoleProvider implementation that supports hierarchisation,
//        ///// adds the role to the specified roles.
//        ///// </summary>
//        ///// <param name="roleName">Name of the role.</param>
//        ///// <param name="roleNames">The role names.</param>
//        ///// <exception cref="System.NotImplementedException">RoleProviderAuthorizationManagementService:AspNetRoleProvider cannot nest Roles.</exception>
//        //public void AddRoleToRoles(string roleName, params string[] roleNames)
//        //{
//        //    throw new NotImplementedException("RoleProviderAuthorizationManagementService:AspNetRoleProvider cannot nest Roles.");
//        //}

//        ///// <summary>
//        ///// In a RoleProvider implementation that supports hierarchisation,
//        ///// removes the role from roles.
//        ///// </summary>
//        ///// <param name="roleName">Name of the role.</param>
//        ///// <param name="roleNames">The role names.</param>
//        ///// <exception cref="System.NotImplementedException">RoleProviderAuthorizationManagementService:AspNetRoleProvider cannot nest Roles.</exception>
//        //public void RemoveRoleFromRoles(string roleName, params string[] roleNames)
//        //{
//        //    throw new NotImplementedException("RoleProviderAuthorizationManagementService:AspNetRoleProvider cannot nest Roles.");
//        //}
//    }
//}