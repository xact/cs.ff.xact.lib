﻿//namespace XAct.Security.Authentication.Implementations
//{
//    using System;
//    using System.Web.Security;
//    using XAct.Diagnostics;
//    using XAct.Services;

//    /// <summary>
//    /// An implementation of the 
//    /// <see cref="IAuthenticationManagementService"/>
//    /// that wraps functionality of the
//    /// <see cref="System.Web.Security.MembershipProvider"/>
//    /// <para>
//    /// Note that one must keep Authentication separate from 
//    /// Authorization, as there are several different systems
//    /// one can hot swap for that Service (Roles, Operations,etc.)
//    /// </para>
//    /// </summary>
//    /// <remarks>
//    /// When MS was designing the MembershipProvider in .NET20, 
//    /// there was no Mobile. Or WIF, or much of anything that is
//    /// on the market today. It wasn't really well designed, 
//    /// and it hasn't aged gracefully.
//    /// <para>
//    /// These days, it's cheap to start developing with it,
//    /// but you shouldn't really end up going to production
//    /// with it.
//    /// </para>
//    /// <para>
//    /// At the very least, in a corp environment, you would
//    /// have to use the AD provider -- and really consider
//    /// factoring that out to a SSO/WIF based solution.
//    /// </para>
//    /// <para>
//    /// Hence why I *strongly* suggest you don't code your 
//    /// authentication service directly wrapping the System.Web
//    /// based classes -- write to a portable interface, that
//    /// you can later swap for a more robust solution (WIF).
//    /// 
//    /// </para>
//    /// </remarks>
//    /// <internal>
//    /// <para>
//    /// The default SQL based Membership provider saves the password
//    /// depending on the value (plain, Hashed, Encrypted)
//    /// given to the <c>passwordFormat</c> attribute.
//    /// <code>
//    /// <![CDATA[
//    /// <membership defaultProvider="SqlProvider" userIsOnlineTimeWindow="20">
//    ///   <providers>
//    ///     <add name="SqlProvider"
//    ///          type="System.Web.Security.SqlMembershipProvider"
//    ///          connectionStringName="SqlServices"
//    ///          enablePasswordRetrieval="false"
//    ///          enablePasswordReset="true"
//    ///          requiresQuestionAndAnswer="true"
//    ///          passwordFormat="Hashed"
//    ///          applicationName="MyApplication" />
//    ///   </providers>
//    /// </membership>    
//    /// ]]>
//    /// </code>
//    /// Hash is the safest, as -- internally -- it uses SHA1
//    /// the safest *General Purpose* Algorithm:
//    /// <code>
//    /// <![CDATA[
//    /// public string EncodePassword(string pass, string salt) {
//    ///   byte[] bytes = Encoding.Unicode.GetBytes(pass);
//    ///   byte[] src = Encoding.Unicode.GetBytes(salt);
//    ///   byte[] dst = new byte[src.Length + bytes.Length];
//    ///   Buffer.BlockCopy(src, 0, dst, 0, src.Length);
//    ///   Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
//    ///   HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
//    ///   byte[] inArray = algorithm.ComputeHash(dst);
//    ///   return Convert.ToBase64String(inArray);
//    ///   }
//    /// ]]>
//    /// </code>
//    /// The first line of defence is to use the 
//    /// MaxInvalidPasswordAttempts and PasswordAttemptWindow  settings
//    /// but if you want to get more serious about it,
//    /// for a safer algorithm -- or better understanding on how SHA1
//    /// is good, but not perfect -- look at:
//    /// http://bit.ly/oZtY4z and http://bit.ly/pl2bgX
//    /// </para>
//    /// <para>
//    /// Better yet -- do NOT use MembershipProvider and get into the business
//    /// of keeping usernames/passwords on your own server -- use SSO, and delegate
//    /// in to another service (think SRP. Your app is probably not in the business
//    /// domain of passwords. It just uses them. Which is an external service.
//    /// </para>
//    /// <para>
//    /// If you are thinking of writing your own solution: http://bit.ly/qo2hMd
//    /// </para>
//    /// </internal>
//    [DefaultBindingImplementation(typeof(IAuthenticationManagementService), BindingLifetimeType.Undefined, Priority.Low /*OK: No preferrence between technologies */)]
//    public class MembershipProviderAuthenticationManagementService : XActLibServiceBase, IMembershipProviderAuthenticationManagementService
//    {

//        #region Fields

//        #region Private Members
//        //Private so that other assemblies who reference 
//        //this assembly don't have to import System.Web, System.Configuration, etc.
//        private MembershipProvider _MembershipProvider
//        {
//            get { return Membership.Provider; }
//        }

//        #endregion


//        //Generate one time so that it's internal Random is generated only once (more random).
//        private readonly StrongPasswordGenerator _strongPasswordGenerator = new StrongPasswordGenerator();

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Initializes a new instance of the <see cref="MembershipProviderAuthenticationManagementService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The logging service.</param>
//        /// <internal>5/15/2011: Sky</internal>
//        public MembershipProviderAuthenticationManagementService(ITracingService tracingService):base(tracingService)
//        {
//            _tracingService.ValidateIsNotDefault("tracingService");


//        }

//        #endregion

//        /// <summary>
//        /// Generates the new password.
//        /// </summary>
//        /// <returns></returns>
//        /// <internal>5/13/2011: Sky</internal>
//        /// <internal><para>5/14/2011: Sky</para></internal>
//        public string GeneratePassword()
//        {
//            return _strongPasswordGenerator.GeneratePassword(
//                _MembershipProvider.MinRequiredPasswordLength,
//                Math.Max(_MembershipProvider.MinRequiredPasswordLength, 8),
//                (_MembershipProvider.MinRequiredNonAlphanumericCharacters > 0),
//                null);

//            //_MembershipProvider.MinRequiredNonAlphanumericCharacters;
//            //_MembershipProvider.MinRequiredPasswordLength;
//            //_MembershipProvider.PasswordStrengthRegularExpression;
//        }

//        /// <summary>
//        /// Resets password to an automatically generated password
//        /// </summary>
//        /// <param name="identityName"></param>
//        /// <param name="answerToChallengeQuestion"></param>
//        /// <internal>
//        /// Should invoke <see cref="GeneratePassword"/>.
//        /// </internal>
//        /// <internal><para>5/14/2011: Sky</para></internal>
//        public void ResetPassword(string identityName, string answerToChallengeQuestion)
//        {
//            _tracingService.Trace(TraceLevel.Info, "Password Reset for {0} requested.".FormatStringCurrentCulture(identityName));

//            _MembershipProvider.ResetPassword(identityName, answerToChallengeQuestion);
//        }

//        /// <summary>
//        /// Changes the password.
//        /// </summary>
//        /// <param name="identityName">Name of the member.</param>
//        /// <param name="oldPassword">The old password.</param>
//        /// <param name="newPassword">The new password.</param>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void ChangePassword(string identityName, string oldPassword, string newPassword)
//        {
//            identityName.ValidateIsNotNullOrEmpty("identityName");
//            newPassword.ValidateIsNotNullOrEmpty("newPassword");

//            _tracingService.Trace(TraceLevel.Info, "Password Change for {0} requested.".FormatStringCurrentCulture(identityName));

//            _MembershipProvider.ChangePassword(identityName, oldPassword, newPassword);
//        }

//        /// <summary>
//        /// Changes the challenge question and answer.
//        /// </summary>
//        /// <param name="identityName">Name of the member/user.</param>
//        /// <param name="password">The password.</param>
//        /// <param name="newChallengeQuestion">The new challenge question.</param>
//        /// <param name="newChallengeAnswer">The new challenge answer.</param>
//        /// <internal><para>5/14/2011: Sky</para></internal>
//        public void ChangeChallengeQuestionAndAnswer(string identityName, string password, string newChallengeQuestion,
//                                                     string newChallengeAnswer)
//        {
//            identityName.ValidateIsNotNullOrEmpty("identityName");
//            password.ValidateIsNotNullOrEmpty("password");
//            newChallengeQuestion.ValidateIsNotNullOrEmpty("newChallengeQuestion");
//            newChallengeAnswer.ValidateIsNotNullOrEmpty("newChallengeAnswer");

//            _tracingService.Trace(TraceLevel.Info, "Change of Password Challenge Question and Answer for {0} requested.".FormatStringCurrentCulture(identityName));

//            //string password = _membershipProvider.GetPassword(identityName, oldChallengeAnswer);
//            _MembershipProvider.ChangePasswordQuestionAndAnswer(identityName, password, newChallengeQuestion,
//                                                                newChallengeAnswer);
//        }

//        /// <summary>
//        /// Unlocks the member.
//        /// </summary>
//        /// <typeparam name="TId">The type of the id.</typeparam>
//        /// <param name="identityInfo">The authentication user info.</param>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void UnlockMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
//        {
//            _tracingService.Trace(TraceLevel.Info, "Unlock Member {0} requested.".FormatStringCurrentCulture(identityInfo.Name));

//            if (!_MembershipProvider.UnlockUser(identityInfo.Name))
//            {
//                throw new ArgumentException("Could not unlock the specified User.");
//            }
//        }

//        /// <summary>
//        /// Unlocks a user who has locked up their account
//        /// (probably by entering too many wrong passwords
//        /// within a given time).
//        /// </summary>
//        /// <param name="identityName"></param>
//        /// <returns></returns>
//        /// <internal><para>5/14/2011: Sky</para></internal>
//        public void UnlockMember(string identityName)
//        {
//            _tracingService.Trace(TraceLevel.Info, "Unlock Member {0} requested.".FormatStringCurrentCulture(identityName));

//            identityName.ValidateIsNotNullOrEmpty("identityName");

//            if (_MembershipProvider.UnlockUser(identityName))
//            {
//                throw new ArgumentException("Could not unlock the specified User.");
//            }
//        }

//        ///// <summary>
//        ///// Retrieves the password.
//        ///// <para>
//        ///// Do NOT implement this method as it is 
//        ///// a security breach waiting to happen.
//        ///// </para>
//        ///// </summary>
//        ///// <param name="identityName">Name of the member/user.</param>
//        ///// <returns></returns>
//        ///// <internal><para>5/13/2011: Sky</para></internal>
//        //string RetrievePassword(string identityName)
//        //{
//        //    return _MembershipProvider.GetPassword();
//        //}

//        /// <summary>
//        /// Creates the member/user.
//        /// <para>
//        /// Note that if <see cref="IUserIdentity.IsApproved"/> is not set, it defaults to <c>false</c>.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TId">The type of the DataStore Id (eg: Guid or Int).</typeparam>
//        /// <param name="identityInfo">The authentication identity info.</param>
//        /// <internal><para>5/14/2011: Sky</para></internal>
//        public void CreateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
//        {
//            _tracingService.Trace(TraceLevel.Info, "Create Member {0} requested.".FormatStringCurrentCulture(identityInfo.Name));

//            MembershipCreateStatus membershipCreateStatus;

//            MembershipUser aspNetMembershipUser
//                = _MembershipProvider.CreateUser(
//                    identityInfo.Name,
//                    identityInfo.Password,
//                    identityInfo.EmailAddress,
//                    identityInfo.ChallengeQuestion,
//                    identityInfo.ChallengeAnswer,
//                    (identityInfo.IsApproved ?? false), //If not specifically mentioned, let's just say it's false.
//                    identityInfo.Id,
//                    out membershipCreateStatus);


//            if (membershipCreateStatus != MembershipCreateStatus.Success)
//            {
//                //throw new ArgumentException(
//                //    "Could not create Member with given MembershipInfo. Cause: {0}".FormatStringCurrentUICulture(
//                //        membershipCreateStatus.ToString()));


//                throw new ArgumentException(
//                    "Could not create Member. {0}".FormatStringCurrentUICulture(ErrorCodeToString(membershipCreateStatus)));

                
//            }

//            aspNetMembershipUser.MapTo(identityInfo);

            
//        }

//        /// <summary>
//        /// Updates the member.
//        /// </summary>
//        /// <typeparam name="TId">The type of the id.</typeparam>
//        /// <param name="identityInfo">The authentication identity info.</param>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void UpdateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
//        {
//            _tracingService.Trace(TraceLevel.Info, "Update Member {0} requested.".FormatStringCurrentCulture(identityInfo.Name));

            
//            //SLightly more complicated when dealing with an interface smaller than what exists in db:
//            //What we have to do is pick up existing record to populate an asp net specific record
//            //and overwrite it with new values, before sending it back:
//            MembershipUser membershipUser = _MembershipProvider.GetUser(identityInfo.Id, false);

//            if (membershipUser == null)
//            {
//                throw new ArgumentException("Could not find a member matching the given identityInfo");
//            }

//            //Using Resharper, can see that the default sqlMembershipProvider updates
//            //command.Parameters.Add(this.CreateInputParam("@ApplicationName", SqlDbType.NVarChar, this.ApplicationName));
//            //command.Parameters.Add(this.CreateInputParam("@UserName", SqlDbType.NVarChar, user.UserName));
//            //command.Parameters.Add(this.CreateInputParam("@Email", SqlDbType.NVarChar, user.Email));
//            //command.Parameters.Add(this.CreateInputParam("@Comment", SqlDbType.NText, user.Comment));
//            //command.Parameters.Add(this.CreateInputParam("@IsApproved", SqlDbType.Bit, user.IsApproved ? 1 : 0));
//            //command.Parameters.Add(this.CreateInputParam("@LastLoginDate", SqlDbType.DateTime, user.LastLoginDate.ToUniversalTime()));
//            //command.Parameters.Add(this.CreateInputParam("@LastActivityDate", SqlDbType.DateTime, user.LastActivityDate.ToUniversalTime()));
//            //command.Parameters.Add(this.CreateInputParam("@UniqueEmail", SqlDbType.Int, this.RequiresUniqueEmail ? 1 : 0));
//            //command.Parameters.Add(this.CreateInputParam("@CurrentTimeUtc", SqlDbType.DateTime, DateTime.UtcNow));


//            //Update it:
//            //Don't think one should update audit and traceable fields, 
//            //so will only be updating Email, Comment, IsApproved);
//            membershipUser.MapFrop(identityInfo);

//            _MembershipProvider.UpdateUser(membershipUser);
//        }

//        /// <summary>
//        /// Deletes the member.
//        /// </summary>
//        /// <typeparam name="TId">The type of the id.</typeparam>
//        /// <param name="membershipInfo">The membership info.</param>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void DeleteMember<TId>(IAuthenticationIdentityInfo<TId> membershipInfo)
//        {
//            _tracingService.Trace(TraceLevel.Info, "Delete Member {0} requested.".FormatStringCurrentCulture(membershipInfo.Name));

//            DeleteMember(membershipInfo.Name);
//        }


//        /// <summary>
//        /// Deletes the specified member/user.
//        /// </summary>
//        /// <param name="identityName">Name of the member/user.</param>
//        /// <internal>5/13/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public void DeleteMember(string identityName)
//        {
//            _tracingService.Trace(TraceLevel.Info, "Delete Member {0} requested.".FormatStringCurrentCulture(identityName));

//            if (!_MembershipProvider.DeleteUser(identityName, true))
//            {
//                throw new ArgumentException("Could not delete the specified user.");
//            }
//        }


//        /// <summary>
//        /// Validates that the member/user's name/pwd combination is in the datastore.
//        /// <para>
//        /// Note: This can be useful for signing in one person as another, but think carefully about doing things 
//        /// that way...
//        /// </para>
//        /// </summary>
//        /// <param name="identityName">Name of the user.</param>
//        /// <param name="password">The password.</param>
//        /// <returns></returns>
//        /// <internal>5/13/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public bool ValidateMember(string identityName, string password)
//        {
//            _tracingService.Trace(TraceLevel.Verbose, "Validate Member {0} requested.".FormatStringCurrentCulture(identityName));

//            identityName.ValidateIsNotNullOrEmpty("identityName");
//            password.ValidateIsNotNullOrEmpty("password");


//            return _MembershipProvider.ValidateUser(identityName, password);
//        }


//                private static string ErrorCodeToString(MembershipCreateStatus createStatus)
//                {
//                    // See http://go.microsoft.com/fwlink/?LinkID=177550 for
//                    // a full list of status codes.
//                    switch (createStatus)
//                    {
//                        case MembershipCreateStatus.DuplicateUserName:
//                            return "User name already exists. Please enter a different user name.";

//                        case MembershipCreateStatus.DuplicateEmail:
//                            return
//                                "A user name for that e-mail address already exists. Please enter a different e-mail address.";

//                        case MembershipCreateStatus.InvalidPassword:
//                            return "The password provided is invalid. Please enter a valid password value.";

//                        case MembershipCreateStatus.InvalidEmail:
//                            return "The e-mail address provided is invalid. Please check the value and try again.";

//                        case MembershipCreateStatus.InvalidAnswer:
//                            return
//                                "The password retrieval answer provided is invalid. Please check the value and try again.";

//                        case MembershipCreateStatus.InvalidQuestion:
//                            return
//                                "The password retrieval question provided is invalid. Please check the value and try again.";

//                        case MembershipCreateStatus.InvalidUserName:
//                            return "The user name provided is invalid. Please check the value and try again.";

//                        case MembershipCreateStatus.ProviderError:
//                            return
//                                "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

//                        case MembershipCreateStatus.UserRejected:
//                            return
//                                "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

//                        default:
//                            return
//                                "An unknown error occurred ('{0}'). Please verify your entry and try again. If the problem persists, please contact your system administrator.".FormatStringCurrentUICulture(createStatus);
//                    }
//                }

//    }
//}