﻿
//#if (!REMOVENS4EXTENSIONS)
////See: http://bit.ly/snE0xY
//namespace XAct {
//    using System.Web.Security;
//    using XAct.Security.Authentication;

//#endif

//    /// <summary>
//    /// Extensions to the <see cref="System.Web.Security.MembershipUser"/> object.
//    /// </summary>
//    public static class MembershipUserExtensions
//    {
//        /// <summary>
//        /// Maps an ASP.NET MembershipProvider specific MembershipUser object 
//        /// (returned by MembershipProvider.Create) to 
//        /// to a tech agnostic <see cref="IAuthenticationIdentityInfo{TId}"/>.
//        /// <para>
//        /// Note that the target Passport field will always be left null.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TId">The type of the id data.</typeparam>
//        /// <param name="membershipUser">The membership user.</param>
//        /// <returns></returns>
//        /// <internal><para>5/14/2011: Sky</para></internal>
//        public static IAuthenticationIdentityInfo<TId> MapTo<TId>(this MembershipUser membershipUser)
//        {
//            //Create new one to fill and return:
//            IAuthenticationIdentityInfo<TId> result =
//                new AuthenticationIdentityInfo<TId>();

//            membershipUser.MapTo(result);

//            return result;
//        }

//        /// <summary>
//        /// Maps an ASP.NET MembershipProvider specific MembershipUser object 
//        /// (returned by MembershipProvider.Create) to 
//        /// to a tech agnostic <see cref="IAuthenticationIdentityInfo{TId}"/>.
//        /// <para>
//        /// Note that the target Passport field will always be left null.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TId">The type of the id data.</typeparam>
//        /// <param name="membershipUser">The membership user.</param>
//        /// <param name="toMapTo">To map to.</param>
//        /// <internal><para>5/14/2011: Sky</para></internal>
//        public static void MapTo<TId>(this MembershipUser membershipUser, IAuthenticationIdentityInfo<TId> toMapTo)
//        {
//            toMapTo.Id = (TId) membershipUser.ProviderUserKey;
//            (toMapTo).Name = membershipUser.UserName;
//            toMapTo.Password = null; // Never pass a password around -- only used to create a new user.
//            toMapTo.EmailAddress = membershipUser.Email;
//            toMapTo.ChallengeQuestion = membershipUser.PasswordQuestion;
//            toMapTo.ChallengeAnswer = membershipUser.PasswordQuestion;
//        }

//        /// <summary>
//        /// Maps the given <paramref name="authenticationUser"/>.
//        /// </summary>
//        /// <typeparam name="TId">The type of the id.</typeparam>
//        /// <param name="membershipUser">The membership user.</param>
//        /// <param name="authenticationUser">The member info.</param>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public static void MapFrop<TId>(this MembershipUser membershipUser,
//                                        IAuthenticationIdentityInfo<TId> authenticationUser)
//        {
//            //Using Resharper, can see that the default sqlMembershipProvider.Update method updates
//            //command.Parameters.Add(this.CreateInputParam("@ApplicationName", SqlDbType.NVarChar, this.ApplicationName));
//            //command.Parameters.Add(this.CreateInputParam("@UserName", SqlDbType.NVarChar, user.UserName));
//            //command.Parameters.Add(this.CreateInputParam("@Email", SqlDbType.NVarChar, user.Email));
//            //command.Parameters.Add(this.CreateInputParam("@Comment", SqlDbType.NText, user.Comment));
//            //command.Parameters.Add(this.CreateInputParam("@IsApproved", SqlDbType.Bit, user.IsApproved ? 1 : 0));
//            //command.Parameters.Add(this.CreateInputParam("@LastLoginDate", SqlDbType.DateTime, user.LastLoginDate.ToUniversalTime()));
//            //command.Parameters.Add(this.CreateInputParam("@LastActivityDate", SqlDbType.DateTime, user.LastActivityDate.ToUniversalTime()));
//            //command.Parameters.Add(this.CreateInputParam("@UniqueEmail", SqlDbType.Int, this.RequiresUniqueEmail ? 1 : 0));
//            //command.Parameters.Add(this.CreateInputParam("@CurrentTimeUtc", SqlDbType.DateTime, DateTime.UtcNow));

//            //membershipUser.UserName = authenticationUser.Name;
//            membershipUser.Comment = authenticationUser.Comment;
//            membershipUser.Email = authenticationUser.EmailAddress;
//            membershipUser.IsApproved = authenticationUser.IsApproved ?? false;
//        }
//    }

//#if (!REMOVENS4EXTENSIONS)
//    //See: http://bit.ly/snE0xY
//}
//#endif
