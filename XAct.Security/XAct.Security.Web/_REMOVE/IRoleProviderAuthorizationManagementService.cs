﻿
//// ReSharper disable CheckNamespace


//namespace XAct.Security.Authorization.RBAC
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// A specialization of <see cref="IRoleBasedAuthorizationManagementService"/>
//    /// </summary>
//    public interface IRoleProviderAuthorizationManagementService : IRoleBasedAuthorizationManagementService, IHasXActLibService { }
//}