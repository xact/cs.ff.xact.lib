﻿
//// ReSharper disable CheckNamespace
//namespace XAct.Security.Authorization.RBAC
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// A specialization of <see cref="IRoleBasedAuthorizationService"/>
//    /// </summary>
//    public interface IRoleProviderAuthorizationService: IRoleBasedAuthorizationService, IHasXActLibService {}
//}