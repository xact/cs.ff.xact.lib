﻿//namespace XAct.Security.Authorization.RBAC.Implementations
//{
//    using System;
//    using System.Linq;
//    using System.Web.Security;
//    using XAct.Diagnostics;
//    using XAct.Services;

//    /// <summary>
//    /// An implementation of <see cref="IRoleProviderAuthorizationService"/>
//    /// that wraps the System.Web Role Provider.
//    /// <para>
//    /// Never write directly to the System.Web.RoleProvider -- it's anemic.
//    /// It's cheap enough to get started, but there are far better solutions
//    /// to work with, such as OperationsBasedAuthorizationServices.
//    /// </para>
//    /// </summary>
//    /// <internal><para>5/15/2011: Sky</para></internal>
//    [DefaultBindingImplementation(typeof(IRoleBasedAuthorizationService), BindingLifetimeType.Undefined, Priority.Low/*OK:Secondary Binding*/)]
//    public class RoleProviderAuthorizationService : XActLibServiceBase, IRoleProviderAuthorizationService
//    {
        
//        #region Constructors

//        /// <summary>
//        /// Initializes a new instance of the <see cref="RoleProviderAuthorizationService"/> class.
//        /// </summary>
//        /// <param name="tracingService">The logging service.</param>
//        /// <internal>5/15/2011: Sky</internal>
//        public RoleProviderAuthorizationService(ITracingService tracingService):base(tracingService)
//        {

//        }

//        #endregion

//        /// <summary>
//        /// Determines whether the user is in the specified roles.
//        /// </summary>
//        /// <param name="roleNames">The role names.</param>
//        /// <returns>
//        /// 	<c>true</c> if [is in role] [the specified role names]; otherwise, <c>false</c>.
//        /// </returns>
//        /// <internal>5/9/2011: Sky</internal>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        public bool IsInRole(params string[] roleNames)
//        {
//            _tracingService.QuickTrace(roleNames.JoinSafely(","));

//            //Note that from reflector, it just passes it to current identity if authenticated.
//            bool result = roleNames.Any(Roles.IsUserInRole);

//            return result;
//        }

//        /// <summary>
//        /// Determines whether the current user is in one of the roles
//        /// defined in the csv string (';|,').
//        /// </summary>
//        /// <param name="roleNames">The role names.</param>
//        /// <param name="interpretEmptyStringAsAllowed">if set to <c>true</c> returns true if the roleNames string is empty.</param>
//        /// <returns>
//        /// 	<c>true</c> if [is in role] [the specified role names]; otherwise, <c>false</c>.
//        /// </returns>
//        /// <internal><para>8/15/2011: Sky</para></internal>
//        public bool IsInRole(string roleNames, bool interpretEmptyStringAsAllowed = false)
//        {
//            if (string.IsNullOrEmpty(roleNames))
//            {
//                return interpretEmptyStringAsAllowed;
//            }

//            string[] roleNamesList = roleNames.Split(new[] { ';', '|', ',' }, StringSplitOptions.RemoveEmptyEntries);

//            return IsInRole(roleNamesList);
//        }

//    }
//}