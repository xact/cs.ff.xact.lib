//namespace XAct.Security.Authentication.Implementations
//{
//    using System;
//    using System.Security.Principal;
//    using System.Web.Security;
//    using XAct.Diagnostics;
//    using XAct.Environment;
//    using XAct.Services;

//    /// <summary>
//    /// An implementation of the 
//    /// <see cref="IAuthenticationService"/>
//    /// that wraps functionality of the
//    /// <see cref="System.Web.Security.MembershipProvider"/>
//    /// </summary>
//    /// <internal>
//    /// <para>
//    /// The default SQL based provider saves the password
//    /// as depending on the value (plain, Hashed, Encrypted)
//    /// given to the <c>passwordFormat</c> attribute.
//    /// <code>
//    /// <![CDATA[
//    /// <membership defaultProvider="SqlProvider" userIsOnlineTimeWindow="20">
//    ///   <providers>
//    ///     <add name="SqlProvider"
//    ///          type="System.Web.Security.SqlMembershipProvider"
//    ///          connectionStringName="SqlServices"
//    ///          enablePasswordRetrieval="false"
//    ///          enablePasswordReset="true"
//    ///          requiresQuestionAndAnswer="true"
//    ///          passwordFormat="Hashed"
//    ///          applicationName="MyApplication" />
//    ///   </providers>
//    /// </membership>    
//    /// ]]>
//    /// </code>
//    /// Hash is the safest, as -- internally -- it uses SHA1
//    /// the safest *General Purpose* Algorithm:
//    /// <code>
//    /// <![CDATA[
//    /// public string EncodePassword(string pass, string salt) {
//    ///   byte[] bytes = Encoding.Unicode.GetBytes(pass);
//    ///   byte[] src = Encoding.Unicode.GetBytes(salt);
//    ///   byte[] dst = new byte[src.Length + bytes.Length];
//    ///   Buffer.BlockCopy(src, 0, dst, 0, src.Length);
//    ///   Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
//    ///   HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
//    ///   byte[] inArray = algorithm.ComputeHash(dst);
//    ///   return Convert.ToBase64String(inArray);
//    ///   }
//    /// ]]>
//    /// </code>
//    /// The first line of defence is to use the 
//    /// MaxInvalidPasswordAttempts and PasswordAttemptWindow  settings
//    /// but if you want to get more serious about it,
//    /// for a safer algorithm -- or better understanding on how SHA1
//    /// is good, but not perfect -- look at:
//    /// http://bit.ly/oZtY4z and http://bit.ly/pl2bgX
//    /// </para>
//    /// <para>
//    /// Better yet -- do NOT use MembershipProvider and get into the business
//    /// of keeping usernames/passwords on your own server -- use SSO, and delegate
//    /// in to another service (think SRP. Your app is probably not in the business
//    /// domain of passwords. It just uses them. Which is an external service.
//    /// </para>
//    /// <para>
//    /// If you are thinking of writing your own solution: http://bit.ly/qo2hMd
//    /// </para>
//    /// </internal>
//    /// <internal><para>5/15/2011: Sky</para></internal>
//    [DefaultBindingImplementation(typeof(IAuthenticationService), BindingLifetimeType.Undefined, Priority.Low /*OK: No preferrence between technologies */)]
//    public class MembershipProviderAuthenticationService : XActLibServiceBase, IMembershipProviderAuthenticationService
//    {

//        private readonly IPrincipalService _principalService;


//        /// <summary>
//        /// Initializes a new instance of the <see cref="MembershipProviderAuthenticationService" /> class.
//        /// </summary>
//        /// <param name="tracingService">The logging service.</param>
//        /// <param name="principalService">The principal service.</param>
//        /// <internal>5/15/2011: Sky</internal>
//        public MembershipProviderAuthenticationService(
//            ITracingService tracingService, 
//            IPrincipalService principalService):base(tracingService)
//        {
//            _principalService = principalService;
//        }


//        /// <summary>
//        /// Gets the current Thread Principal, 
//        /// from which one can derive the Identity as needed.
//        /// </summary>
//        /// <value>The current.</value>
//        /// <internal><para>8/10/2011: Sky</para></internal>
//        public IPrincipal CurrentPrincipal
//        {
//            get
//            {
//                return _principalService.Principal;
//            }
//        }

//        /// <summary>
//        /// Logins the specified username.
//        /// </summary>
//        /// <param name="username">The username.</param>
//        /// <param name="password">The password.</param>
//        /// <returns></returns>
//        public bool Login(string username, string password)
//        {
//            //Note how we do NOT log password. Only that there was one:
//            _tracingService.QuickTrace("UserName:{0}, Password:{1}".FormatStringCurrentCulture(username,password.SafeString()));

//            bool result;

//            try
//            {
//                //check against the local db:
//                result = Membership.Provider.ValidateUser(username, password);
//            }catch
//            {
//                throw new NotImplementedException();
//                //Check against the local config file.
//                //result = FormsAuthentication.Authenticate(username, password);
//            }


//            if (result){
//                //Set a non-persistent cookie:
//                FormsAuthentication.SetAuthCookie(username, false);

//                return true;
//            }

//            return false;
//        }

//        /// <summary>
//        /// Logouts this instance.
//        /// </summary>
//        public void Logout()
//        {
//            _tracingService.QuickTrace("UserName:{0}".FormatStringCurrentCulture(
                
//                _principalService.Principal.Identity.Name));
            
//            FormsAuthentication.SignOut();
//        }
//    }
//}