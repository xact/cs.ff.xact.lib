﻿namespace XAct.Security.Session.Services
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface ISessionServiceConfiguration : IHasXActLibServiceConfiguration
    {



        /// <summary>
        /// Gets or sets the time span to cache <see cref="Session"/>.
        /// <para>
        /// Update this at startup, and when the application settings changes 
        /// the timeout period.
        /// </para>
        /// <para>
        /// The default time is 20 minutes.
        /// </para>
        /// </summary>
        TimeSpan CacheTimeSpan { get; set; }


    }
}