﻿namespace XAct.Security.Session.Services
{
    using System.Security.Principal;
    using XAct.Caching;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="ISessionService"/>
    /// in order to retrieve information about the current
    /// <see cref="IPrincipal"/>'s Session.
    /// </summary>
    public class SessionService:ISessionService
    {
        private readonly ISessionServiceConfiguration _sessionServiceConfiguration;
        private readonly ICachingService _hostBasedCachingService;
        private readonly IPrincipalService _principalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionService" /> class.
        /// </summary>
        /// <param name="sessionServiceConfiguration">The session service configuration.</param>
        /// <param name="hostBasedCachingService">The caching service.</param>
        /// <param name="principalService">The principal service.</param>
        public SessionService(ISessionServiceConfiguration sessionServiceConfiguration, ICachingService hostBasedCachingService, IPrincipalService principalService)
        {
            _sessionServiceConfiguration = sessionServiceConfiguration;
            _hostBasedCachingService = hostBasedCachingService;
            _principalService = principalService;
        }

        /// <summary>
        /// Gets the current <see cref="IPrincipal"/>'s <see cref="Session"/>,
        /// or null if the <see cref="IPrincipal"/> is anonymous.
        /// </summary>
        /// <returns></returns>
        public Session GetSession()
        {
            IPrincipal principal = _principalService.Principal;
            if (principal == null)
            {
                return null;
            }
            if (!principal.Identity.IsAuthenticated)
            {
                return null;
            }
            var key = MakeCacheKey(principal.Identity);
            Session session;

            if (_hostBasedCachingService.TryGet<Session>(key, out session))
            {

                _hostBasedCachingService.Set(key, session, _sessionServiceConfiguration.CacheTimeSpan);
            }
            
            return session;
        }

        private string MakeCacheKey(IIdentity identity)
        {
            string key = "Session" + ":" + identity.Name;

            return key;
        }

        /// <summary>
        /// Caches the <see cref="Session"/>
        /// for retrieval by <see cref="GetSession"/>
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="identity">The identity.</param>
        public void SetSession(Session session, IIdentity identity = null)
        {
            if (identity == null)
            {
                identity = _principalService.Principal.Identity;
            }


            string key = MakeCacheKey(identity);

            _hostBasedCachingService.Set(key, session, _sessionServiceConfiguration.CacheTimeSpan);
        }

    }
}