﻿namespace XAct.Security.Session.Services.Implementations
{
    using System;
    using System.Linq;
    using System.Security.Principal;
    using XAct.Caching;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Security.Session.Services.Configuration;

    /// <summary>
    /// An implementation of the 
    /// <see cref="ISessionService"/>
    /// </summary>
    public class SessionManagementService : ISessionManagementService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly ICachingService _hostBasedCachingService;
        private readonly IRepositoryService _repositoryService;
        //private readonly IICachedGeoIPService _cachedGeoIpService;

        /// <summary>
        /// The singleton configuration for this service.
        /// </summary>
        public ISessionManagementServiceConfiguration Configuration { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ISessionService"/> interface.
        /// </summary>
        public SessionManagementService(
            IDateTimeService dateTimeService,
            ICachingService hostBasedCachingService,
            IRepositoryService repositoryService, 
            //ICachedGeoIPService cachedGeoIPService,
            ISessionManagementServiceConfiguration sessionServiceConfiguration)
        {
            _dateTimeService = dateTimeService;
            _hostBasedCachingService = hostBasedCachingService;
            _repositoryService = repositoryService;
            //_cachedGeoIpService = cachedGeoIPService;
            Configuration = sessionServiceConfiguration;
        }


        /// <summary>
        /// Given an Identity (or the info that describes it), and sign in info, create  the session.
        /// <para>
        /// Does not create an Identity, nor cache it on the thread -- that's up to the invoker.
        /// </para>
        /// <para>
        /// Although an <see cref="IIdentity" /> is serializable on FF, using BinarySerializer,
        /// BinarySerializer is not available on PCL. And there is no equivalent (Json.NET, UniversalSerializer, SharpSerializer, all failed).
        /// </para>
        /// <para>
        /// Serializing the information that *describes* the Identity is more combersome than simply passing the Identity to be serialized -- but portable.
        /// </para>
        /// </summary>
        /// <param name="sessionSignInRequest">The session sign in information.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">sessionSignInInfo
        /// or
        /// identityEncodedAsBase64</exception>
        public SessionResponse CreateSession(SessionCreationRequest sessionSignInRequest)
        {

            if (sessionSignInRequest == null) throw new ArgumentNullException("sessionSignInRequest");
            var result = new SessionResponse();


            //Get the custom Session record type:
            //Convert message to entity, filling in the missing bits:
            Session session = MapTo(sessionSignInRequest, _dateTimeService);

#pragma warning disable 168
            using (UnitOfWorkThreadScope unitOfWorkThreadScope = new UnitOfWorkThreadScope())
#pragma warning restore 168
            {
                //We're going to save it in all cases, so that we have a record of the status.
                _repositoryService.PersistOnCommit<Session, Guid>(session, true);

                //Need to persist now, so that it is later available via the CacheService
                //below -- but we don't want to commit anything else either. Hence the 
                //UnitOfWorkThreadScope
                _repositoryService.GetContext().Commit();

            }

            //Notice how this method can be used to fill in the ApplicationUserFK and ApplicationOrganisationFK:
            result.Status = session.Status = Configuration.ValidateAndCompleteSessionCreationInfo(session);

            if (result.Status != SessionResponseStatus.Success)
            {
                return result;
            }


            result.SessionSummary = MapToCache(session);


            //We use max length, because we don't want it to disappear at 20.
            //But we will delete it if not used...
            _hostBasedCachingService.Set<SessionCacheableSummary>(
                CreateCacheIdentifier(session.Id),
                ()=>
                    //NO: result.SessionSummary
                    //Hit the db:
                    MapToCache(_repositoryService.GetByFilter<Session>(x => x.Id == session.Id).FirstOrDefault()??session)
                ,
                Configuration.SessionMaxLength);


            return result;

        }



        /// <summary>
        /// Updates the session last activity date.
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <param name="returnSerializedIdentityInfo">if set to <c>true</c> [return serialized identity information].</param>
        /// <param name="forceCommit">if set to <c>true</c> force commit now.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">sessionToken</exception>
        public SessionResponse UpdateSessionLastAccessed(Guid sessionId, bool returnSerializedIdentityInfo = false, bool forceCommit = true)
        {

            if (sessionId == Guid.Empty)
            {
                throw new ArgumentNullException("sessionId");
            }

            var result = new SessionResponse();

            //Update Session's LastAccessedDateTime, but only if not expired.
            Session session;
            result.Status = UpdateSessionLastAccessedDateTime(sessionId, forceCommit, out session);


            if (result.Status != SessionResponseStatus.Success)
            {
                //record the reason we're not going to return a Session:
                session.Status = result.Status;
            }
            else{
    
                //If updated, not expired, so depending on argument received,
                //go back and optionally get the Session.

                if (returnSerializedIdentityInfo)
                {
                    SessionCacheableSummary sessionCacheableSummary;
                    _hostBasedCachingService.TryGet<SessionCacheableSummary>(CreateCacheIdentifier(sessionId),
                                                                    out sessionCacheableSummary);
                    result.SessionSummary = sessionCacheableSummary;
                }
            }

            return result;
        }



        private SessionResponseStatus UpdateSessionLastAccessedDateTime(Guid sessionId, bool forceCommit, out Session session)
        {
            //TODO : this needs to be optimized with conditional update query

            //the expiry value has to precalculated so that EF can translate it to SQL query
            //for the session to be NOT expired the following has to apply

            //lastAccessTime + TimeOUt > NOW
            //wich is the same as:
            //lastAccessTime > NOW - Timeout

            Session[] sessions =
                _repositoryService
                    .GetByFilter<Session>(
                        s => (s.Id == sessionId)).ToArray();


            if (sessions.Count() != 1)
            {
                throw new Exception(string.Format("Either none, or two or moer sessions with the same token {0}", sessionId));
                //this shouldn't happen
            }

            session = sessions.First();

            if (session.Status != SessionResponseStatus.Success)
            {
                //It's closed in a previous pass, so the status will no longer be success.
                //Whatever it is (Closed, Expired, etc.) return it.
                return session.Status;
            }
            if (session.LogoutDateTimeUtc.HasValue)
            {
                //Should never happen, but worth checking:
                return SessionResponseStatus.SessionClosed;
            }

            var result = Configuration.ValidateSessionUpdateInfo(session);
            
            if (result != SessionResponseStatus.Success)
            {
                return result;
            }

            DateTime nowUtc = _dateTimeService.NowUTC;
            DateTime lastallowedAccessedDateTimeUtc = nowUtc.Subtract(Configuration.SessionInactivityTimeout);
            DateTime lastallowedLoginDateTimeUtc = nowUtc.Subtract(Configuration.SessionMaxLength);

            if (session.LoginDateTimeUtc < lastallowedLoginDateTimeUtc)
            {
                //Session started too long ago.
                session.LogoutDateTimeUtc = nowUtc;
                return SessionResponseStatus.SessionExpiredDueToDurationTooLong;
            }

            if (session.LastActivityDateTimedUtc < lastallowedAccessedDateTimeUtc)
            {
                //Last accessed too long ago.
                session.LogoutDateTimeUtc = nowUtc;
                return SessionResponseStatus.SessionExpiredDueToInactivity;
            }

            //Seems ok to update:
            session.LastActivityDateTimedUtc = nowUtc;
            //@@@

            _repositoryService.UpdateOnCommit(session);

            //Commit immediately so that we have updated logs:
            if (forceCommit)
            {
                _repositoryService.GetContext().Commit();
            }

            return SessionResponseStatus.Success;
        }


        /// <summary>
        /// Does the session exist.
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <returns></returns>
        public SessionResponseStatus DoesSessionExist(Guid sessionId, bool isActive)
        {
            if (!isActive)
            {
                if (_repositoryService.GetByFilter<Session>(s => (s.Id == sessionId)).Any())
                {
                    return SessionResponseStatus.Success;
                }
                return SessionResponseStatus.SessionInvalid;
            }
            //Do a deaper check:
            DateTime nowUtc = _dateTimeService.NowUTC;
            DateTime lastallowedAccessedDateTimeUtc =
                nowUtc.Subtract(Configuration.SessionInactivityTimeout);
            DateTime lastallowedLoginDateTimeUtc = nowUtc.Subtract(Configuration.SessionMaxLength);

            var x = _repositoryService.GetByFilter<Session>(
                s => (
                         (s.Id == sessionId)
                         &&
                         //User has accessed after the cutoff in the pase:
                         (s.LastActivityDateTimedUtc > lastallowedAccessedDateTimeUtc)
                         &&
                         //User has log'ed in after the cutoff in the past:
                         (s.LoginDateTimeUtc > lastallowedLoginDateTimeUtc)
                         &&
                    //User has log'ed in after the cutoff in the past:
                         (s.LogoutDateTimeUtc == null)
                     )
                ).Any();

            if (x)
            {
                return SessionResponseStatus.Success;
            }
            return SessionResponseStatus.SessionInvalid;
        }



        /// <summary>
        /// Updates the <see cref="Session" />'s last accessed date (if not already set)
        /// and deletes the <see cref="Session" />
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <param name="forceCommit">if set to <c>true</c> [force commit].</param>
        public void CloseSession(Guid sessionId, bool forceCommit = false)
        {

            Session session =
                _repositoryService
                    .GetByFilter<Session>(
                        s => (s.Id == sessionId)).FirstOrDefault();

            if (session != null)
            {
                session.LogoutDateTimeUtc = _dateTimeService.NowUTC;
                session.Status = SessionResponseStatus.SessionClosed;
            }

            _repositoryService.DeleteOnCommit<Session>(s => s.Id == sessionId);

            if (forceCommit)
            {
                _repositoryService.GetContext().Commit();
            }
        }

        private string CreateCacheIdentifier(Guid sessionId)
        {
            return Configuration.CachePrefix + sessionId;
        }


        private Session MapTo(SessionCreationRequest sessionSignInInfo, IDateTimeService dateTimeService=null)
        {
            if (dateTimeService == null)
            {
                dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            }

            Session sessionLog = new Session();

            sessionLog.LoginIP = sessionSignInInfo.ClientIP;

            DateTime nowUtc = dateTimeService.NowUTC;
            sessionLog.LoginDateTimeUtc = nowUtc;
            sessionLog.LastActivityDateTimedUtc = nowUtc;

            sessionLog.SessionProviderKey = sessionSignInInfo.SessionProviderKey;

            sessionLog.SessionProviderUserIdentifier = sessionSignInInfo.SessionProviderUserIdentifier;
            sessionLog.SessionProviderUserOrganisationIdentifier = sessionSignInInfo.SessionProviderUserOrganisationIdentifier;

            sessionLog.ApplicationUserIdentifier = sessionSignInInfo.ApplicationUserFK;
            sessionLog.ApplicationOrganisationIdentifier = sessionSignInInfo.ApplicationUserOrganisationFK;

            return sessionLog;
        }


        private SessionCacheableSummary MapToCache(Session session)
        {
            if (session == null)
            {
                return null;
            }

            SessionCacheableSummary sessionCacheableSummary = new SessionCacheableSummary
                {
                    SessionId = session.Id,
                    SessionProviderKey = session.SessionProviderKey,
                    SessionProviderUserIdentifier = session.SessionProviderUserIdentifier,
                    SessionProviderUserOrganisationIdentifier = session.SessionProviderUserOrganisationIdentifier,
                    ApplicationUserIdentifier = session.ApplicationUserIdentifier,
                    ApplicationOrganisationIdentifier = session.ApplicationOrganisationIdentifier
                };

            return sessionCacheableSummary;
        }


    }

    
}

