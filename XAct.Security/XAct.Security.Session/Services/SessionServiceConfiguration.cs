﻿namespace XAct.Security.Session.Services
{
    using System;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class SessionServiceConfiguration :ISessionServiceConfiguration, IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets or sets the time span to cache <see cref="Session"/>.
        /// <para>
        /// Update this at startup, and when the application settings changes 
        /// the timeout period.
        /// </para>
        /// <para>
        /// The default time is 20 minutes.
        /// </para>
        /// </summary>
        public TimeSpan CacheTimeSpan
        {
            get { return _cacheTimeSpan; }
            set { _cacheTimeSpan = value; }
        }
        private TimeSpan _cacheTimeSpan = TimeSpan.FromMinutes(20);
    }
}