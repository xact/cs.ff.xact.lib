namespace XAct.Security.Session
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Optional Parameter to configure a <see cref="SessionProvider"/>.
    /// </summary>
    [DataContract]
    public class SessionProviderParameter : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasSerializedTypeValueAndMethod
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }
        /// <summary>
        /// FK to the owner <see cref="SessionProvider"/>
        /// </summary>
        [DataMember]
        public virtual Guid SessionProviderFK { get; set; }
        /// <summary>
        /// Gets or sets the serialization method.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The serialization method.
        /// </value>
        [DataMember]
        public virtual SerializationMethod SerializationMethod { get; set; }
        /// <summary>
        /// Gets or sets the Assembly qualified name of the Value that is serialized.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValueType { get; set; }
        /// <summary>
        /// Gets or sets the serialized value.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValue { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionProviderParameter"/> class.
        /// </summary>
        public SessionProviderParameter()
        {
            this.GenerateDistributedId();
        }
    }
}