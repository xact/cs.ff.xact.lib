namespace XAct.Security.Session
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// The SSO Service used (Local, Facebook, etc.)
    /// </summary>
    [DataContract]
    public class SessionProvider : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasReferenceDataReadOnly<Guid>
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual int Order { get; set; }


        /// <summary>
        /// Gets or sets the text.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Text { get; set; }

        /// <summary>
        /// Gets the description.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Gets the Filter for the Resource in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        [DataMember]
        public virtual string ResourceFilter { get; set; }

        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        [DataMember]
        public virtual string Tag { get; set; }

        /// <summary>
        /// Gets the string that can be used as filter.
        /// <para>
        /// By convention the syntax is similar to CSV,
        /// but with ! for NOT clauses, &amp; for AND, etc:
        /// <example>
        /// <![CDATA[
        /// AA;!BB;CC&DD;CC&!DD
        /// ]]>
        /// </example>
        /// </para>
        /// <para>Member defined in<see cref="IHasFilter" /></para>
        /// </summary>
        [DataMember]
        public virtual string Filter { get; set; }



        /// <summary>
        /// Gets or sets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        [DataMember]
        public virtual ICollection<SessionProviderParameter> Parameters
        {
            get { return _parameters ?? (_parameters = new Collection<SessionProviderParameter>()); }
        }
        private ICollection<SessionProviderParameter> _parameters;


        /// <summary>
        /// Initializes a new instance of the <see cref="SessionProvider"/> class.
        /// </summary>
        public SessionProvider()
        {
            this.GenerateDistributedId();
        }
    }
}