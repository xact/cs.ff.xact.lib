namespace XAct.Security.Session
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A log of <see cref="Session"/> objects.
    /// This table -- unlike the <see cref="Session"/> -- 
    /// is not truncated. Or if is, it is Archived 
    /// for forensic reasons.
    /// </summary>
    [DataContract]
    public class Session : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasTag 
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// The status of the Session.
        /// </summary>
        [DataMember]
        public virtual SessionResponseStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the Text representation of <see cref="Status"/>.
        /// <para>
        /// Purely for readability of the database and reports.
        /// </para>
        /// </summary>
        /// <value>
        /// The status text.
        /// </value>
        [DataMember]
        public virtual string StatusText
        {
            get { return _statusString = Status.ToString(); }
// ReSharper disable ValueParameterNotUsed
            set { _statusString = value; }
// ReSharper restore ValueParameterNotUsed
        }

        private string _statusString;

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        
        /// <summary>
        /// Gets or sets the login date time (UTC).
        /// </summary>
        [DataMember]
        public virtual DateTime LoginDateTimeUtc { get; set; }
        
        /// <summary>
        /// Gets or sets the datetime of the last activity (UTC).
        /// </summary>
        [DataMember]
        public virtual DateTime LastActivityDateTimedUtc { get; set; }

        /// <summary>
        /// Gets or sets the datetime of when the session was terminated.
        /// </summary>
        [DataMember]
        public virtual DateTime? LogoutDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets the unique <c>Key</c> 
        /// identifying the SessionProvider (Facebook, etc.)
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that using a Key is simpler than allows not having to cache a list of ProviderFK's.
        /// </para>
        /// </remarks>
        [DataMember]
        public virtual string SessionProviderKey { get; set; }


        /// <summary>
        /// The User Id as assigned by the SSO.
        /// <para>
        /// Each SSO might have a different identifier. 
        /// Each SSO might have a different identifier for the same person
        /// across different applications the user is allowed access to 
        /// (keeping applications distinct from each other. See Government concerns
        /// about limiting cross department/application access to data)
        /// </para> 
        /// </summary>
        [DataMember]
        public virtual string SessionProviderUserIdentifier { get; set; }



        /// <summary>
        /// The Organisation Id as assigned by the SSO.
        /// <para>
        /// Each SSO might have a different identifier. 
        /// Each SSO might have a different identifier for the same person
        /// across different applications the user is allowed access to 
        /// (keeping applications distinct from each other. See Government concerns
        /// about limiting cross department/application access to data)
        /// </para> 
        /// </summary>
        [DataMember]
        public virtual string SessionProviderUserOrganisationIdentifier { get; set; }

        /// <summary>
        /// Optional additional information returned by the SessionProvider (roles,etc.)
        /// </summary>
        [DataMember]
        public virtual string SessionProviderSerializedInformation { get; set; }
        
        /// <summary>
        /// This is a hack to get around where UserFK is neither an int or Guid
        /// or dealin with applications that won't have a User Table...
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationUserIdentifier { get; set; }

        /// <summary>
        /// The Id of the Organisation to which the User belonged to
        /// when signed in (a User can belong to 1+ Organisations).
        /// </summary>
        [DataMember]
        public virtual Guid? ApplicationOrganisationIdentifier { get; set; }


        /// <summary>
        /// Gets or sets the IP of the user-agent.
        /// </summary>
        [DataMember]
        public virtual string LoginIP { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's Latitude.
        /// </summary>
        [DataMember]
        public virtual string LoginIPLatitude { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's Latitude.
        /// </summary>
        [DataMember]
        public virtual string LoginIPLongitude { get; set; }


        /// <summary>
        /// Gets or sets the Login IP's TimeZone.
        /// </summary>
        [DataMember]
        public virtual string LoginIPTimeZone { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's City Name.
        /// </summary>
        [DataMember]
        public virtual string LoginIPCityName { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's City Name.
        /// </summary>
        [DataMember]
        public virtual string LoginIPRegion { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's Postal Code.
        /// </summary>
        [DataMember]
        public virtual string LoginIPPostalCode { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's Postal Code.
        /// </summary>
        [DataMember]
        public virtual string LoginIPCountry { get; set; }




        /// <summary>
        /// Gets or sets the IP of the user-agent.
        /// </summary>
        [DataMember]
        public virtual string LastActivityIP { get; set; }


        /// <summary>
        /// Gets or sets the Login IP's Latitude.
        /// </summary>
        [DataMember]
        public virtual string LastActivityIPLatitude { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's Latitude.
        /// </summary>
        [DataMember]
        public virtual string LastActivityIPLongitude { get; set; }


        /// <summary>
        /// Gets or sets the Login IP's TimeZone.
        /// </summary>
        [DataMember]
        public virtual string LastActivityIPTimeZone { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's City Name.
        /// </summary>
        [DataMember]
        public virtual string LastActivityIPCityName { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's City Name.
        /// </summary>
        [DataMember]
        public virtual string LastActivityIPRegion { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's Postal Code.
        /// </summary>
        [DataMember]
        public virtual string LastActivityIPPostalCode { get; set; }

        /// <summary>
        /// Gets or sets the Login IP's Postal Code.
        /// </summary>
        [DataMember]
        public virtual string LastActivityIPCountry { get; set; }




        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        [DataMember]
        public virtual string Tag { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="Session"/> class.
        /// </summary>
        public Session()
        {
            this.GenerateDistributedId();
        }
    }
}