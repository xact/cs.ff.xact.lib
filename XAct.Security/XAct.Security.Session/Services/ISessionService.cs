﻿namespace XAct.Security.Session.Services
{
    using System.Security.Principal;

    /// <summary>
    /// Contract for a service to retrieve a cached <see cref="Session"/> message
    /// about the current <see cref="IPrincipal"/>.
    /// <para>
    /// Use <see cref="ISessionManagementService"/> if you want to create a <see cref="Session"/>.
    /// </para>
    /// </summary>
    public interface ISessionService : IHasXActLibService
    {
        /// <summary>
        /// Gets the current <see cref="IPrincipal"/>'s <see cref="Session"/>,
        /// or null if the <see cref="IPrincipal"/> is anonymous.
        /// </summary>
        /// <returns></returns>
        Session GetSession();

    }
}
