namespace XAct.Security.Session.Services.Configuration
{
    using System;

    /// <summary>
    /// Contract for a singleton configuration package
    /// of <see cref="ISessionManagementService"/>
    /// </summary>
    public interface ISessionManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// The prefix that is put before the <see cref="Session"/>'s Id
        /// when it is cached.
        /// </summary>
        string CachePrefix { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Type"/> of the <c>Session</c> object.
        /// <para>
        /// The default is the type of <see cref="Session"/>.
        /// </para>
        /// </summary>
        Type SessionType { get; set; }



        /// Gets or sets the <see cref="Type"/> of the <c>SessionLog</c> object.
        /// <para>
        /// The default is the type of <see cref="Session"/>.
        /// </para>
        Type SessionLogType { get; set; }


        /// <summary>
        /// Gets or sets the maximum amount of time of inactivity.
        /// </summary>
        TimeSpan SessionInactivityTimeout { get; set; }

        /// <summary>
        /// Gets or sets the maximum length a session can be active.
        /// </summary>
        TimeSpan SessionMaxLength { get; set; }


        /// <summary>
        /// A customizable Func callback to validate requests for the creation of Sessions.
        /// <para>
        /// The default is <c>()=>true</c>, but can be replaced with a func to check 
        /// the given Org info, User info, etc.
        /// </para>
        /// </summary>
        Func<Session, SessionResponseStatus> ValidateAndCompleteSessionCreationInfo { get; set; }

        /// <summary>
        /// A customizable Func callback to validate requests for the extension of Sessions.
        /// <para>
        /// The default is <c>()=>true</c>, but can be replaced with a func to check 
        /// the given Org info, User info, etc.
        /// </para>
        /// </summary>
        Func<Session, SessionResponseStatus> ValidateSessionUpdateInfo { get; set; }
    }
}