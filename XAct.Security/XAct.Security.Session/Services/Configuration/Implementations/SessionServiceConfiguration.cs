namespace XAct.Security.Session.Services.Configuration.Implementations
{
    using System;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="ISessionManagementServiceConfiguration" />
    /// singleton configuration package for the <see cref="ISessionManagementService" />.
    /// </summary>
    public class SessionManagementServiceConfiguration : ISessionManagementServiceConfiguration, IHasXActLibServiceConfiguration
    {
        private Type _sessionLogType;
        private Type _sessionType;


        /// <summary>
        /// The prefix that is put before the <see cref="Session" />'s Id
        /// when it is cached.
        /// </summary>
        public string CachePrefix { get; set; }
        /// <summary>
        /// Gets or sets the <see cref="Type" /> of the <c>Session</c> object.
        /// <para>
        /// The default is the type of <see cref="Session" />.
        /// </para>
        /// </summary>
        public Type SessionType
        {
            get { return _sessionType; }
            set { _sessionType = value; }
        }

        /// <summary>
        /// </summary>
        /// Gets or sets the <see cref="Type" /> of the <c>SessionLog</c> object.
        /// <para>
        /// The default is the type of <see cref="Session" />.
        /// </para>
        public Type SessionLogType
        {
            get { return _sessionLogType; }
            set { _sessionLogType = value; }
        }

        /// <summary>
        /// Gets or sets the maximum amount of time of inactivity.
        /// </summary>
        public TimeSpan SessionInactivityTimeout { get; set; }
        
        /// <summary>
        /// Gets or sets the maximum length a session can be active.
        /// </summary>
        public TimeSpan SessionMaxLength { get; set; }

        /// <summary>
        /// A customizable Func callback to validate requests for the creation of Sessions.
        /// <para>
        /// The default is <c>(session)=>SessionResponseStatus.Success</c>, but can be replaced with a func to check 
        /// the given Org info, User info, etc.
        /// </para>
        /// </summary>
        public Func<Session,SessionResponseStatus> ValidateAndCompleteSessionCreationInfo { get; set; }

        /// <summary>
        /// A customizable Func callback to validate requests for the extension of Sessions.
        /// <para>
        /// The default is <c>(session)=>SessionResponseStatus.Success</c>, but can be replaced with a func to check 
        /// the given Org info, User info, etc.
        /// </para>
        /// </summary>
        public Func<Session,SessionResponseStatus> ValidateSessionUpdateInfo { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="SessionManagementServiceConfiguration"/> class.
        /// </summary>
        public SessionManagementServiceConfiguration()
        {
            CachePrefix = "SessionToken:";

            SessionType = typeof (Session);
            SessionLogType = typeof (Session);
            SessionInactivityTimeout  = TimeSpan.FromMinutes(20);
            SessionMaxLength = TimeSpan.FromHours(8);

            ValidateAndCompleteSessionCreationInfo = (session) => SessionResponseStatus.Success;
            ValidateSessionUpdateInfo = (session) => SessionResponseStatus.Success;

        }
    }
}