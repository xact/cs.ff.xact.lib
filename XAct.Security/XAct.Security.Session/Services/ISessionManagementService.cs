﻿
namespace XAct.Security.Session.Services
{
    using System;
    using XAct.Security.Session.Services.Configuration;

    /// <summary>
    /// 
    /// </summary>
    public interface ISessionManagementService : IHasXActLibService
    {

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        ISessionManagementServiceConfiguration Configuration { get; }

        /// <summary>
        /// Given an Identity (or the info that describes it), and sign in info, create  the session.
        /// <para>
        /// Does not create an Identity, nor cache it on the thread -- that's up to the invoker.
        /// </para>
        /// <para>
        /// Invoked after a Saml or OAuth response processor has parsed the returned information
        /// and determined that the response states that the User is valid. 
        /// </para>
        /// <para>
        /// The Saml object will be responding with extra information, the OAuth token was used to retrieve more info.
        /// Either way, this extra information could be roles, OrgId, etc. All this should be serialized and passed 
        /// along with the User identifier.
        /// </para>
        /// <para>
        /// At which point, a Session record needs to be created.
        /// </para>
        /// </summary>
        /// <param name="sessionSignInInfo">The session sign in information.</param>
        /// <returns></returns>
        SessionResponse CreateSession(SessionCreationRequest sessionSignInInfo);


        /// <summary>
        /// Updates the session last activity date.
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <param name="returnSerializedIdentityInfo">if set to <c>true</c> [return serialized identity information].</param>
        /// <param name="forceCommit">if set to <c>true</c> force commit now.</param>
        /// <returns></returns>
        SessionResponse UpdateSessionLastAccessed(Guid sessionId, bool returnSerializedIdentityInfo = false, bool forceCommit = true);

        /// <summary>
        /// Does the session exist.
        /// </summary>
        SessionResponseStatus DoesSessionExist(Guid sessionId, bool isActive);

        /// <summary>
        /// Updates the <see cref="Session" />'s last accessed date (if not already set)
        /// and deletes the <see cref="Session" />
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <param name="forceCommit">if set to <c>true</c> [force commit].</param>
        void CloseSession(Guid sessionId, bool forceCommit=true);

    }
}
