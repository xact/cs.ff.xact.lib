﻿namespace XAct.Security.Session
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration of either the success or denied reason 
    /// from requesting a session to be created or renewed.
    /// </summary>
    [DataContract]
    public enum SessionResponseStatus
    {
        /// <summary>
        /// The value has not been set.
        /// <para>
        /// This is an error condition (value should have been set).
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined =0,

        /// <summary>
        /// The Session was created or renewed.
        /// </summary>
        [EnumMember]
        Success = 1,


        /// <summary>
        /// The user was not found, and a new session was not able to be started.
        /// </summary>
        [EnumMember]
        UserInvalid = 11,

        /// <summary>
        /// The user was disabled, and a new session was not able to be started.
        /// </summary>
        [EnumMember]
        UserDisabled = 12,


        /// <summary>
        /// The organisation was not found, and a new session was not able to be started.
        /// </summary>
        [EnumMember]
        OrganisationInvalid = 21,

        /// <summary>
        /// The organisation was disabled, and a new session was not able to be started.
        /// </summary>
        [EnumMember]
        OrganisationDisabled = 22,

        /// <summary>
        /// Session was not found, so could not be extended.
        /// </summary>
        [EnumMember]
        SessionInvalid = 31,


        /// <summary>
        /// Session was disabled, so could not be extended.
        /// <para>
        /// Only done if suspicious activity was spotted, and 
        /// both the User and Session were disabled.
        /// </para>
        /// </summary>
        [EnumMember]
        SessionDisabled = 32,


        /// <summary>
        /// The session was expired due to user inactivity
        /// (eg: after 20 minutes), so could not be extended.
        /// </summary>
        [EnumMember]
        SessionExpiredDueToInactivity = 33,

        /// <summary>
        /// The expired due to the session duration being too long
        /// (eg: after 8 hours), so could not be extended.
        /// </summary>
        [EnumMember]
        SessionExpiredDueToDurationTooLong = 34,


        /// <summary>
        /// The session was closed/logged out.
        /// </summary>
        [EnumMember]
        SessionClosed = 35
    }
}
