namespace XAct.Security.Session
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Cached Session information optionall returned 
    /// within a <see cref="SessionResponse"/> (always when successfully
    /// creating a new Session, only if requested when updating the LastAccessedDateTime.
    /// <para>
    /// The difference between <see cref="Session"/>
    /// and this <see cref="SessionCacheableSummary"/> object
    /// is that the <see cref="Session"/>
    /// is a Db Record -- whereas this is a Message.
    /// </para>
    /// <para>
    /// The advantage is that there is no mistaking that this
    /// <see cref="SessionCacheableSummary"/> can be updated, 
    /// and persisted (try that with a cached <see cref="Session"/>
    /// and you'll have to first re-attach the object to the current
    /// UoW Context, etc.)
    /// </para>
    /// </summary>
    [DataContract]
    public class SessionCacheableSummary
    {

        /// <summary>
        /// The Id of the <see cref="Session"/> record.
        /// </summary>
        [DataMember]
        public virtual Guid SessionId { get; set; }

        /// <summary>
        /// Gets or sets the unique <c>Key</c> 
        /// identifying the SessionProvider (Facebook, etc.)
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that using a Key is simpler than allows not having to cache a list of ProviderFK's.
        /// </para>
        /// </remarks>
        [DataMember]
        public virtual string SessionProviderKey { get; set; }


        /// <summary>
        /// The User Id as assigned by the SSO.
        /// <para>
        /// Each SSO might have a different identifier. 
        /// Each SSO might have a different identifier for the same person
        /// across different applications the user is allowed access to 
        /// (keeping applications distinct from each other. See Government concerns
        /// about limiting cross department/application access to data)
        /// </para> 
        /// </summary>
        [DataMember]
        public virtual string SessionProviderUserIdentifier { get; set; }



        /// <summary>
        /// The Organisation Id as assigned by the SSO.
        /// <para>
        /// Each SSO might have a different identifier. 
        /// Each SSO might have a different identifier for the same person
        /// across different applications the user is allowed access to 
        /// (keeping applications distinct from each other. See Government concerns
        /// about limiting cross department/application access to data)
        /// </para> 
        /// </summary>
        [DataMember]
        public virtual string SessionProviderUserOrganisationIdentifier { get; set; }


        /// <summary>
        /// This is a hack to get around where UserFK is neither an int or Guid
        /// or dealin with applications that won't have a User Table...
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationUserIdentifier { get; set; }

        /// <summary>
        /// The Id of the Organisation to which the User belonged to
        /// when signed in (a User can belong to 1+ Organisations).
        /// </summary>
        [DataMember]
        public virtual Guid? ApplicationOrganisationIdentifier { get; set; }



    }
}