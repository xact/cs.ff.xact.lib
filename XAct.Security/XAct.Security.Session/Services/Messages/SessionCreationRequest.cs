﻿
namespace XAct.Security.Session
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A Message to better describe 
    /// the means by which a new <see cref="Session"/>
    /// was initiated (where it came from, what interface
    /// is being used, etc).
    /// <para>
    /// The Message's properties are used to populate
    /// a DataStore <see cref="Session"/> entity.
    /// </para>
    /// </summary>
    [DataContract]
    public class SessionCreationRequest
    {


        /// <summary>
        /// Gets or sets the unique identity identifier.
        /// </summary>
        [DataMember]
        public virtual string IdentityIdentifier { get; set; } 


        /// <summary>
        /// Gets or sets the IP of the client's user agent.
        /// </summary>
        [DataMember]
        public virtual string ClientIP { get; set; }


        /// <summary>
        /// Gets or sets the unique <c>Key</c> 
        /// identifying the SessionProvider (Facebook, etc.)
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that using a Key is simpler than allows not having to cache a list of ProviderFK's.
        /// </para>
        /// </remarks>
        [DataMember]
        public virtual string SessionProviderKey { get; set; }


        /// <summary>
        /// Gets or sets the SSO's identifier for the user,
        /// as returned from the remote SSO.
        /// <para>
        /// This will be different that the Application's identifier.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string SessionProviderUserIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the SSO's identifier for the user's organisation,
        /// as returned from the remote SSO.
        /// <para>
        /// This will be different that the Application's identifier.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string SessionProviderUserOrganisationIdentifier { get; set; }


        /// <summary>
        /// Gets or sets any other information returned by the  SSO regarding the user.
        /// <para>
        /// This could be Roles, Claims information, etc.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string SerializedSessionProviderAdditionalInformation { get; set; }

        /// <summary>
        /// Gets or sets the Application's identifier for the user.
        /// <para>
        /// This will be different that the Application's identifier.
        /// </para>
        /// <para>
        /// Depending on the Datastore Mappings, this will be the FK to 
        /// application's User table.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationUserFK { get; set; }

        /// <summary>
        /// Gets or sets the Application's identifier for the user's organisation.
        /// <para>
        /// This will be different that the Application's identifier.
        /// </para>
        /// <para>
        /// Depending on the Datastore Mappings, this will be the FK to 
        /// application's Organisation table.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationUserOrganisationFK { get; set; }




    }

}
