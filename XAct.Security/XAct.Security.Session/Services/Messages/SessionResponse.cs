﻿namespace XAct.Security.Session
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The message containing the created Session record.
    /// <para>
    /// This is the response from Creating a new Session,
    /// as well as Updating Last Accessed.
    /// </para>
    /// </summary>
    [DataContract]
    public class SessionResponse
    {


        /// <summary>
        /// Gets or sets a flag indicating whether the session was created or not.
        /// </summary>
        /// <value>
        /// The reason.
        /// </value>
        [DataMember]
        public virtual SessionResponseStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="SessionSummary"/> record,
        /// </summary>
        /// <value>
        /// The session.
        /// </value>
        [DataMember]
        public virtual SessionCacheableSummary SessionSummary { get; set; }

    }


}
