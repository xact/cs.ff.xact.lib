﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.Initialization.Implementations
// ReSharper restore CheckNamespace
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Security.Authorization.Initialization.ModelPersistenceMaps;
    using XAct.Services;

    /// <summary>
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class SecurityDbModelBuilder : XActLibServiceBase, ISecurityDbModelBuilder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public SecurityDbModelBuilder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<Operation>)XAct.DependencyResolver.Current.GetInstance<IOperationModelPersistenceMap>());

            modelBuilder.Configurations.Add((EntityTypeConfiguration<OperationPermission>)XAct.DependencyResolver.Current.GetInstance<IOperationPermissionModelPersistenceMap>());
        }
    }
}
