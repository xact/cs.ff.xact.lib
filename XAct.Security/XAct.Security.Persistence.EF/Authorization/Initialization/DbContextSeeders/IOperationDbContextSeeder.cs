namespace XAct.Security.Authorization.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Operation}"/>
    /// to seed the <c>MethodAuthorization</c> tables with default data.
    /// </summary>
        //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IOperationDbContextSeeder : IHasXActLibDbContextSeeder<Operation>
    {

    }
    /// <summary>
    /// 
    /// </summary>
    public interface IOperationPermissionDbContextSeeder : IHasXActLibDbContextSeeder<OperationPermission>
    {

    }
}