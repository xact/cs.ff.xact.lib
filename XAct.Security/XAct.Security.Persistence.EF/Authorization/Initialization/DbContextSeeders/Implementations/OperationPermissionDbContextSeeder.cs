﻿namespace XAct.Security.Authorization.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class OperationPermissionDbContextSeeder : XActLibDbContextSeederBase<OperationPermission>, IOperationPermissionDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public OperationPermissionDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}