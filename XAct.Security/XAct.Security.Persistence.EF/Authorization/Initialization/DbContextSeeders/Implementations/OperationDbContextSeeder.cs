﻿namespace XAct.Security.Authorization.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="IOperationDbContextSeeder"/> contract
    /// to seed the <c>MethodAuthorization</c> tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class OperationDbContextSeeder : XActLibDbContextSeederBase<Operation>, IOperationDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public OperationDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}
