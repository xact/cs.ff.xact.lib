﻿namespace XAct.Security.Authorization.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class OperationPermissionModelPersistenceMap : EntityTypeConfiguration<OperationPermission>, IOperationPermissionModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationPermissionModelPersistenceMap"/> class.
        /// </summary>
        public OperationPermissionModelPersistenceMap()
        {

            this.ToXActLibTable("OperationPermission");


            this
                .HasKey(m => m.Id);

            int colOrder = 0;

            this
                .Property(m => m.Id)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;



            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;


            //this.HasRequired(wi => wi.Operation)
            //    .WithRequiredPrincipal();
            ////.WithRequiredDependent();

        }


    }
}