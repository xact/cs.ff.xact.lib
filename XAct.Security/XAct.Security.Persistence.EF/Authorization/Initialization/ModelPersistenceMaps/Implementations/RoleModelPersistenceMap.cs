﻿namespace XAct.Security.Authorization.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Security.Entities;

    /// <summary>
    /// 
    /// </summary>
    public class RoleModelPersistenceMap : EntityTypeConfiguration<Role>, IRoleModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationModelPersistenceMap"/> class.
        /// </summary>
        public RoleModelPersistenceMap()
        {

            this.ToXActLibTable("Role");


            this
                .HasKey(m => m.Id);

            int colOrder = 0;

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.ApplicationTennantId)
                .DefineRequiredApplicationTennantId(colOrder++);

            this.Ignore(x => x.ParentFK);

            //this.Property(x => x.ParentFK)
            //    .DefineOptionalParentFK(colOrder++);

            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.Name)
                .DefineRequired64CharCode(colOrder++);

            this
            .HasOptional<Role>(x=>x.Parent)
            .WithOptionalDependent()
            .Map(p => p.MapKey("ParentFK"));


        }
    }
}