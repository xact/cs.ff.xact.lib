﻿namespace XAct.Security.Authorization.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;
    using XAct.Security.Entities;

    /// <summary>
    /// 
    /// </summary>
    public class UserToRoleModelPersistenceMap : EntityTypeConfiguration<UserToRole>, IUserToRoleModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationModelPersistenceMap"/> class.
        /// </summary>
        public UserToRoleModelPersistenceMap()
        {

            this.ToXActLibTable("UserToRole");

            this
                .HasKey(m => m.Id);


            int colOrder = 0;

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.ApplicationTennantId)
                .DefineRequiredApplicationTennantId(colOrder++);

            this.Property(x => x.UserIdentifier)
                .DefineRequired64CharUserIdentifier(colOrder++);

            this.Property(x => x.RoleFK)
                .IsRequired()
                .HasColumnOrder(colOrder++);


            //Relationships:
            this.HasRequired(x => x.Role)
                .WithMany()
                .HasForeignKey(x=>x.RoleFK)
                .WillCascadeOnDelete(false)//Don't want to delete role if relationship is deleted.
                ;

        }
    }
}