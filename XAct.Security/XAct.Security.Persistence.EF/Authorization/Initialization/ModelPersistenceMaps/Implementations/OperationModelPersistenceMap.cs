﻿namespace XAct.Security.Authorization.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class OperationModelPersistenceMap : EntityTypeConfiguration<Operation>, IOperationModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationModelPersistenceMap"/> class.
        /// </summary>
        public OperationModelPersistenceMap()
        {

            this.ToXActLibTable("Operation");


            this
                .HasKey(m => m.Id);

            int colOrder = 0;

            this
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this
                .Property(m => m.Note)
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;

            this
                .HasMany(wi => wi.Permissions)
                .WithRequired()
                .HasForeignKey(a => a.OperationFK)
                ;

        }
    }
}