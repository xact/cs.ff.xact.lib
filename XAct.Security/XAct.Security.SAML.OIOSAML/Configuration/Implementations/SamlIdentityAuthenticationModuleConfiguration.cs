﻿namespace App.Core.AppHost.HttpModules.Configuration.Implementations
{
    using System;
    using XAct.Messages;

    public class SamlIdentityAuthenticationModuleConfiguration : ISamlIdentityAuthenticationModuleConfiguration
    {
        //Calls this.AccountService.UpdateSessionAndSetOrClearThreadPrincipal(sessionId);

        public Func<string, Response<bool>> UpdateSessionAndSetOrClearThreadPrincipal { get; private set; } 
    }
}