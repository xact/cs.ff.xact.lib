﻿namespace App.Core.AppHost.HttpModules
{
    using System;
    using XAct.Messages;

    public interface ISamlIdentityAuthenticationModuleConfiguration : XAct.IHasSingletonBindingScope
    {
        Func<string, Response<bool>> UpdateSessionAndSetOrClearThreadPrincipal { get; }
    }
}