﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using XAct.Web.HttpModules;

//namespace XAct.Security.SAML
//{
//    using System.Diagnostics;
//    using System.Security.Principal;
//    using System.Web;
//    using System.Web.Security;
//    using XAct.Messages;
//    using XAct.Settings;
//    using dk.nita.saml20;
//    using dk.nita.saml20.Schema.Core;
//    using dk.nita.saml20.config;
//    using dk.nita.saml20.identity;
//    using dk.nita.saml20.protocol;


//    /// <summary>
//    /// A custom implementation of the default OIOSAML Action used to
//    /// create an identity upon return from the sigle sign on authority.
//    /// <para>
//    /// It is only invoked upon return from SSO (not on every subsequent 
//    /// method).
//    /// </para>
//    /// <para>
//    /// This is a custom implementation that creates a record
//    /// in the Db -- a SessionLog -- instantiating a Session, and 
//    /// an AppIdentity.
//    /// </para>
//    /// <para>
//    /// Later in the pipeline, in every request, 
//    /// the <see cref="SamlIdentityAuthenticationModule"/>
//    /// processes the incoming requests.
//    /// </para>
//    /// </summary>
//    public class SetAppSamlPrincipal : dk.nita.saml20.Actions.IAction
//    {
//        public const string ACTION_NAME = "SetAppSamlPrincipal";
//        private string _name;

//        //protected IClientEnvironmentService ClientEnvironmentService
//        //{
//        //    get { return XAct.DependencyResolver.Current.GetInstance<IClientEnvironmentService>(); }
//        //}

//        protected IApplicationSettingsService ApplicationSettingsService
//        {
//            get { return XAct.DependencyResolver.Current.GetInstance<IApplicationSettingsService>(); }
//        }

//        private AppSettings AppSettings
//        {
//            get { return ApplicationSettingsService.AppSettings; }
//        }

//        /// <summary>
//        /// Gets or sets the public name of this action.
//        /// </summary>
//        /// <value>
//        /// The name.
//        /// </value>
//        public string Name
//        {
//            get
//            {
//                if (!string.IsNullOrEmpty(this._name))
//                    return this._name;
//                else
//                    return ACTION_NAME;
//            }
//            set { this._name = value; }
//        }

//        /// <summary>
//        /// Invoked by <see cref="dk.nita.saml20.protocol.Saml20SignonHandler.DoLogin"/>
//        /// </summary>
//        /// <param name="handler"></param>
//        /// <param name="context"></param>
//        /// <param name="assertion"></param>
//        public void LoginAction(AbstractEndpointHandler handler, HttpContext context, Saml20Assertion assertion)
//        {
//            //Get a reference to the HttpHandler that the SSO returned the information to.
//            Saml20SignonHandler saml20SignonHandler = (Saml20SignonHandler) handler;

//            //The original Action used a static internal method within Saml20Identity.
//            //We can't access it (it's internal), but it's been copied below to make use of the same logic.
//            //
//            //IPrincipal principal = Saml20Identity.InitSaml20Identity(assertion, saml20SignonHandler.RetrieveIDPConfiguration((string)context.Session["TempIDPId"]));

//            //Get config:
//            IDPEndPoint idpEndPointConfiguration = saml20SignonHandler.RetrieveIDPConfiguration(
//                (string) context.Session["TempIDPId"]);





//            //*** Build the SamlIdentity + GenericPrincipal
//            UserDetails userDetails = new UserDetails();

//            string[] privileges;
//            IPrincipal principal = InitSaml20Identity(
//                assertion,
//                idpEndPointConfiguration,
//                ref userDetails,
//                out privileges);


//            //IMPORTANT: Clear if it exists
//            if (context.Session["MessageCode"] != null)
//            {
//                context.Session.Remove("MessageCode");

//            }

//            //At this point we have a Generic principal, containing a specialize saml identity that contains claims.
//            //The claims have been parsed to extract
//            //* FullName
//            //* Roles
//            //* TODO: More variables could be gleaned in the future. Try to find a generic (read library) way of doing it too.


//            //We need our service to create an AppIdentity, passing it our Identity, but only 
//            IAccountService accountService =
//                XAct.DependencyResolver.Current.GetInstance<IAccountService>();

//            //Do not pass Principal -- as it only has IsInRoles() method, and not a Roles[] property, which we need.
//            SignInInfo signInInfo = new SignInInfo();

//            signInInfo.UserName = userDetails.UserId;
//            signInInfo.RemoteIp = HttpContext.Current.Request.UserHostAddress;
//            signInInfo.SSOProtocol = "SAML";
//            signInInfo.SSOIdentifier = principal.Identity.Name;
//            signInInfo.InterfaceTypeFK = InterfaceType.Web;

//            Response<Guid?> sessionIdResponse =
//                accountService.CreateSessionAndCacheIdentity(signInInfo, principal.Identity, userDetails, privileges);

//            //*** Regarding Persistence:
//            //The original Action at this point saved the identity into the Saml20PrincipalCache,
//            //which in turn saved it to HttpSession. A poor design that would require sticky sessions
//            //in a load balanced scenario.
//            //NO: Saml20PrincipalCache.AddPrincipal(principal);
//            //The solution is to do nothing at this point, 
//            //and leave it to the AccountsService to manage this persistence in a load balanced manner.


//            //*** Regarding Cookie:
//            //The original Action at this point created an FormsAuthentication Cookie.
//            //but the original solution was to embed the user's Name in it.
//            //we don't want bleed the name, and we want to allow signing in multiple times
//            //using the same name. So Session can't be off of name.
//            //Instead, save the session Id:


//            Guid? sessionId = sessionIdResponse.Data;

//            if (!sessionId.HasValue)
//            {
//                //Possible errors seen so far:
//                MessageCode[] knownErrorCodes = new MessageCode[]
//                    {
//                        //(Incorrect OrgId, which can be due to incorrect setup)
//                        MessageCodes.Security_InvalidOrganisationForUser_515
//                    };

//                Debug.Assert(sessionIdResponse.Messages.All(m => knownErrorCodes.Contains(m.MessageCode)));

//                //The downside is that there really isn't much I can do with the Error messages explaining
//                //what is going on.

//                //Options:
//                //* Throw an Exception?
//                //* Eat it?

//                //If I throw it, it ends up brutally on the screen.
//                //If I catch it, just means that nobody knows 
//                //why session could not be created.
//                //Internal error
//                //OrgId was incorrect
//                //Timeout

//                //string url = System.Configuration.ConfigurationManager.AppSettings["VDir"];
//                //url = (url.IsNullOrEmpty())
//                //          ? "/"
//                //          : "/" + url + "/" + "?MessageCode=" + sessionIdResponse.Messages.First().MessageCode.Id;

//                long messageCode = sessionIdResponse.Messages.First().MessageCode.Id;
//                string url = "/NSI/HttpHandlers/SignOff.ashx"; //"?ReturnUrl=/&#64;MessageCODE=" + messageCode;

//                context.Session["MessageCode"] = messageCode;

//                System.Web.HttpContext.Current.Response.Redirect(url);

//            }
//            else
//            {
//                IFormsAuthenticationCookieService _formsAuthenticationCookieService =
//                    XAct.DependencyResolver.Current.GetInstance<IFormsAuthenticationCookieService>();

//                _formsAuthenticationCookieService.SetSessionCookie(sessionId.Value.ToString("D"),
//                                                                   sessionId.Value.ToString("D"));
//            }
//            ;


//            //Must not remove:
//            //Turns out that oiosaml really is not production ready
//            //and Saml20SignonHandler sets it in session, and
//            //Saml20SignoffHandler needs it again to sign out.
//            //context.Session.RemoveSafely(Saml20AbstractEndpointHandler.IDPTempSessionKey);
//            //context.Session.RemoveSafely(Saml20AbstractEndpointHandler.IDPLoginSessionKey);
//            //context.Session.RemoveSafely(Saml20AbstractEndpointHandler.IDPSessionIdKey);
//            //context.Session.RemoveSafely(Saml20AbstractEndpointHandler.IDPNameIdFormat);
//            //context.Session.RemoveSafely(Saml20AbstractEndpointHandler.IDPNameId);




//        }

//        public void LogoutAction(AbstractEndpointHandler handler, HttpContext context, bool IdPInitiated)
//        {
//            FormsAuthentication.SignOut();

//            //Saml20PrincipalCache.Clear();
//        }


//        /// <summary>
//        /// Generate the SamlIdentity + Generic Principal
//        /// </summary>
//        /// <param name="assertion"></param>
//        /// <param name="point"></param>
//        /// <param name="userDetails"></param>
//        /// <param name="privileges"></param>
//        /// <returns></returns>
//        private static IPrincipal InitSaml20Identity(
//            Saml20Assertion assertion,
//            IDPEndPoint point,
//            ref UserDetails userDetails,
//            out string[] privileges)
//        {
//            bool flag = assertion.Subject.Format == "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent";

//            string name = assertion.Subject.Value;

//            if (flag && point.PersistentPseudonym != null)
//                name = point.PersistentPseudonym.GetMapper().MapIdentity(assertion.Subject);

//            IIdentity identity = new Saml20Identity(name,
//                                                    (ICollection<SamlAttribute>)
//                                                    assertion.Attributes,
//                                                    flag ? assertion.Subject.Value : (string) null);

//            // Parse basic assertions
//            userDetails.UserId = GetAssertionAsString(assertion, "uid"); //"uid"
//            userDetails.Email = GetAssertionAsString(assertion, "mail");

//            userDetails.OrgName = GetAssertionAsString(assertion, "o");
//            userDetails.OrgId = GetAssertionAsString(assertion, "eduPersonOrgDN");

//            userDetails.FirstName = GetAssertionAsString(assertion, "givenName");
//            userDetails.LastName = GetAssertionAsString(assertion, "sn");

//            // Parse security privileges
//            SamlAttribute privAttribute =
//                assertion.Attributes.FirstOrDefault(a => a.Name == "eduPersonScopedAffiliation");

//            if (privAttribute != null)
//            {
//                privileges = privAttribute.AttributeValue;

//                // As the privileges list is almost always given as a comma separated list, parse it:
//                if (privileges != null && privileges.Length == 1)
//                {
//                    privileges = privileges[0].Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
//                }
//            }
//            else
//            {
//                privileges = null;
//            }


//            return new GenericPrincipal(identity, privileges);
//        }

//        private static string GetAssertionAsString(Saml20Assertion assertion, string assertionKey)
//        {
//            SamlAttribute attribute = assertion.Attributes.FirstOrDefault(a => a.Name == assertionKey);

//            string result = attribute != null
//                                ? ((string[]) attribute.AttributeValue).First()
//                                : string.Empty;

//            // Saml/LSI passes back the *string* "null" for empty fields
//            if (result.Equals("null", StringComparison.InvariantCultureIgnoreCase))
//            {
//                result = "";
//            }

//            return result;
//        }
//    }
//}