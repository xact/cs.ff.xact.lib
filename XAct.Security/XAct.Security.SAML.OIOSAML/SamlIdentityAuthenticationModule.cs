﻿namespace XAct.Web.HttpModules
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Security;
    using App.Core.AppHost.HttpModules.Configuration.Implementations;
    using XAct;
    using XAct.Diagnostics;
    using XAct.Messages;

    /// <summary>
    /// This Module handles *all* incoming requests (no matter what Handler ends up processing the request)
    /// It is processed *AFTER* the Forms Authentication Module has converted the Cookie into a FormsIdentity.
    /// If there no FormsIdentity means there was no cookie, or it was timed out.
    /// If there is a FormsIdentity, it converts it to an AppIdentity.
    /// <para>
    /// Notice that in both cases we are not talking about the SAML based identity.
    /// This was done earlier -- only once -- when the Response came back from the SAML based IdP
    /// and was processed by the SignOn HttpHandler.
    /// </para>
    /// <para>
    /// Configuration:
    /// In web.config:
    /// <code>
    /// <![CDATA[
    /// <httpModules>
    ///   <add name="SamlIdentityConversion" type="XAct.Web.HttpModules.SamlIdentityAuthenticationModule" />
    /// </httpModules>
    /// ...
    /// <modules>
    ///   <remove name="SamlIdentityConversion" />
    ///   <add name="SamlIdentityConversion" type="XAct.Web.HttpModules.SamlIdentityAuthenticationModule" preCondition="managedHandler" />
    /// </modules>
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public class SamlIdentityAuthenticationModule : IHttpModule
    {
        
        private readonly ITracingService _tracingService;

        public SamlIdentityAuthenticationModuleConfiguration Configuration { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="SamlIdentityAuthenticationModule"/> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        public SamlIdentityAuthenticationModule(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }


        /// <summary>
        /// You will need to configure this module in the Web.config file of your
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        public void Dispose()
        {
            //clean-up code here.
        }

        /// <summary>
        /// Initializes a module and prepares it to handle requests.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpApplication" /> that provides access to the methods, properties, and events common to all application objects within an ASP.NET application</param>
        public void Init(HttpApplication context)
        {
            // Below is an example of how you can handle LogRequest event and provide 
            // custom logging implementation for it
            context.LogRequest += new EventHandler(OnLogRequest);

            //Wire up event handlers.

            //Notice a neat fact. One of the questions that comes up when dealing
            //with modules is "How does one insert a Module in between two other Modules
            //already defined in the machine's web.config, without removing and re-adding
            //every after the next insert?" Turns out the question is a red-herring. 
            //Security Modules are handling AUthenticateRequest events which -- even if the 
            //module is installed after an UrlAuthenticationModule -- is handled *before* the 
            //OnAuthoriseRequest event... neat trick....
            context.AuthenticateRequest += context_AuthenticateRequest;
            context.EndRequest += context_EndRequest;
        }

        /// <summary>
        /// Method called on every request.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void context_AuthenticateRequest(object sender, EventArgs e)
        {
            // Check it's running as a web application
            HttpApplication httpApplication = sender as HttpApplication;
            if (httpApplication == null)
            {
                _tracingService.Trace(TraceLevel.Warning, this.GetType() + ":" + "Unexpected. No httpApplication.");
                return;
            }

            HttpContext httpContext = httpApplication.Context;
            if (httpContext == null)
            {
                _tracingService.Trace(TraceLevel.Warning, this.GetType() + ":" + "Unexpected. No httpContext.");
                return;
            }

            //We get here because nobody else earlier in the pipeline
            //has set Context.SkipAuthorization

            if (httpContext.User == null)
            {
                //Means the FormsAuthenticationModule found no cookie, 
                //or the cookie was no longer valid enough to make a FormsIdentity
                //from it.
                return;
            }

            //Someone has authorised it. That's really good.
            //It's most probably either WindowsAuthentication or FormsAuthentication
            //(usually they are mutually exclusive, but there is a way)

            //Time to switch it with the AppIdentity.

            //So we instead have to get it out of the auth mechanism.


            System.Web.Security.FormsIdentity formsIdentity
                = httpContext.User.Identity as System.Web.Security.FormsIdentity;



            if (formsIdentity == null)
            {
                _tracingService.Trace(TraceLevel.Warning, this.GetType() + ":" + "Unexpected. If we have a User it stands to reason we have an identity as well.");
                return;
            }

            //As the Cookie was Non-Persistent, 
            //it has an Expiration of: {1/01/0001 12:00:00 a.m.}
            //So it's not a good value to check as to whether one has timed out.
            //HttpCookie httpCookie = HttpContext.Current.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
            //Have to guess for now that  things are still ok for now, but when we get 
            //news from the Db that the Session has expired
            //we have to clear the cookie.
            //TODO: But do we redirect?
            //No -- let a later module (UrlAuthorisationModule throw the 404).
            //or when returning from WCF back tier, which will have thrown a
            //XAct.NotAuthorizedException wrapped in a FaultException



            var sessionId = GetSessionTokenFromFormsAuthenticationIdentity(formsIdentity);

            SetOrClearPrincipalInWebContext(httpApplication, sessionId, httpContext);
        }

        private static string GetSessionTokenFromFormsAuthenticationIdentity(FormsIdentity formsIdentity)
        {
            //The formsIdentity gives us 'formsIdentity.Name' 
            //and 'formsIdentity.Ticket' from which we can
            //extract the SessionGuid.


            //One place the Serialized SessionId can be is in the custom UserData.
            //I'm not often going to recommend that, as I prefer
            //taking over the formIdentity.Name for the Guid.
            //This is so that the cookie contains absolutely nothing 
            //that can be used to sign in (The Guid is not the user's LoginName,
            //which is 50% of the puzzle of a (single factor signon) sorted out...)
            string sessionId = ((formsIdentity.Ticket != null) && (!formsIdentity.Ticket.UserData.IsNullOrEmpty()))
                            ? formsIdentity.Ticket.UserData
                            : formsIdentity.Name;
            return sessionId;
        }

        private void SetOrClearPrincipalInWebContext(HttpApplication application, string sessionId, HttpContext httpContext)
        {



            //Invoke our callback:
            Response<bool> sessionUpdated = Configuration.UpdateSessionAndSetOrClearThreadPrincipal(sessionId);


            //It's possible that it didn't go well:
            if (!sessionUpdated.Data)
            {

                //Session has expired -- either due to being over 20 mins or 8 hours.
                //Do we raise an Exception?
                //No.
                //Because it's not a crime to be expired. 
                //It just means no identity.
                //and AuthZ later on is going to raise an AuthorisationException
                //and that is going to end up being a Http Error...which is going to end up 
                //being a 302 redirection to the SSO.

                //But there is a cost to doing it that way.
                //We won't know if we expired the Session due to 20mins or 8hours.
                //That's fine, really. Expired, is Expired, from the users point of view.
                //Correction: turns out that Xml Interface want's to know.
                httpContext.Items["SessionClosedDueTo"] = sessionUpdated.Messages.First().MessageCode;

                //Clear the FormAuthenticationCookie that got us into this......
                httpContext.Response.ClearFormsAuthenticationCookie();



                //Cannot remove, as oiosaml lib is very much session bound 
                //(ie, it's setin SAML20SignonHandler, and refetched in SAML20SignoffHandler
                //session.RemoveSafely(Saml20AbstractEndpointHandler.IDPTempSessionKey);
                //session.RemoveSafely(Saml20AbstractEndpointHandler.IDPLoginSessionKey);
                //session.RemoveSafely(Saml20AbstractEndpointHandler.IDPSessionIdKey);
                //session.RemoveSafely(Saml20AbstractEndpointHandler.IDPNameIdFormat);
                //session.RemoveSafely(Saml20AbstractEndpointHandler.IDPNameId);

            }
        }



        private void context_EndRequest(object sender, EventArgs e)
        {
            //We get here because nobody else has set Context.SkipAuthorization = false

            HttpApplication httpApplication = sender as HttpApplication;

            if (httpApplication == null)
            {
                return;
            }
            HttpContext httpContext = httpApplication.Context; //System.Web.HttpContext.Current

            if (httpContext == null)
            {
                return;
            }

            int statusCode = httpContext.Response.StatusCode;
            string destination = httpContext.Request.RawUrl;
            string redirectLocation = httpContext.Response.RedirectLocation;

            _tracingService.Trace(TraceLevel.Verbose, "{0}:EndRequest [StatusCode:{1}, RawUrl:{2}, RedirectLocation:{3}]".FormatStringInvariantCulture(this.GetType(),statusCode,destination,redirectLocation));

            if (!new[] { 200 }.Contains(httpContext.Response.StatusCode))
            {
                //Things are not going so well after all...

                if (statusCode == 401) // UnAuthenticated
                {
                    string headerValue = httpContext.Request.Headers["X-Requested-With"];

                    //Because it is a JSON request:
                    if (headerValue == "XMLHttpRequest")
                    {
                        //By default, forms authentication converts HTTP 401 status codes 
                        //to 302 in order to redirect to the login page. 
                        //This isn't appropriate for certain classes of errors, such as 
                        //when authentication succeeds but authorization fails, or when 
                        //the current request is an AJAX or web service request. 
                        //This property provides a way to suppress the redirect behavior 
                        //and send the original status code to the client.
                        httpContext.Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }

                if (statusCode == 403) // UnAuthenticated
                {
                    string headerValue = httpContext.Request.Headers["X-Requested-With"];

                    if (headerValue == "XMLHttpRequest")
                    {
                        //By default, forms authentication converts HTTP 401 status codes 
                        //to 302 in order to redirect to the login page. 
                        //This isn't appropriate for certain classes of errors, such as 
                        //when authentication succeeds but authorization fails, or when 
                        //the current request is an AJAX or web service request. 
                        //This property provides a way to suppress the redirect behavior 
                        //and send the original status code to the client.
                        httpContext.Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }
                if (statusCode == 301) //Permanent redirect.
                {
                    //Do nothing.
                }
                if (statusCode == 302) //temp redirect 
                {
                    //(most probably coming from the UrlAuthorisationService, coming up next
                    //in the chain of Modules processing requests,
                    //or from a Security Service.

                    //Do nothing.
                }

            }
            else
            {
                //Ignore, things going fine  (code = 200)  
            }
        }



        public void OnLogRequest(Object source, EventArgs e)
        {

            //custom logging logic can go here
        }
    }

    /*
     *   public class SamlPrincipalAction : IAction
  {
     ...
 
    public void LoginAction(AbstractEndpointHandler handler, HttpContext context, Saml20Assertion assertion)
    {
      Saml20SignonHandler saml20SignonHandler = (Saml20SignonHandler) handler;
 
//********************************************************      
      //Make a principal...that's ok 
      IPrincipal principal = Saml20Identity.InitSaml20Identity(assertion, saml20SignonHandler.RetrieveIDPConfiguration((string) context.Session["TempIDPId"]));
 
      //OUCH! That's using a Session object...TOTALLY NOT ACCEPTABLE.
      Saml20PrincipalCache.AddPrincipal(principal);
 
      //Interesting...got to look this up to understand exactly what's going on here.
      FormsAuthentication.SetAuthCookie(principal.Identity.Name, false);
//********************************************************      
 
    }
    public void LogoutAction(AbstractEndpointHandler handler, HttpContext context, bool IdPInitiated)
    {
      FormsAuthentication.SignOut();
      Saml20PrincipalCache.Clear();
    }
  }
}
     */
}

