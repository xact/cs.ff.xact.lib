﻿namespace XAct.Security.Session.Services.Initialization.Maps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Security.Session;
    using XAct.Services;

    /// <summary>
    /// Defines a EF Map on how to persist a <see cref="Session"/> into the database.
    /// </summary>
    public class SessionModelPersistenceMap : EntityTypeConfiguration<Session>, ISessionModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SessionModelPersistenceMap"/> class.
        /// </summary>
        public SessionModelPersistenceMap()
        {

            this.ToXActLibTable("Session");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;
            
            
            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);
            
            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this
                .Property(m => m.Status)
                .IsRequired()
                .HasColumnOrder(colOrder++);

            this
                .Property(m => m.StatusText)
                .DefineRequired64Char(colOrder++);


        
        this
                .Property(m => m.LoginDateTimeUtc)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.LastActivityDateTimedUtc)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.LogoutDateTimeUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;








            this
                .Property(m => m.SessionProviderKey)
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_SessionProviderKey") { IsUnique = true }))
                    ;

            this
                .Property(m => m.SessionProviderUserIdentifier)
                .DefineRequired64CharUserIdentifier(colOrder++)
                ;
            this
                .Property(m => m.SessionProviderUserOrganisationIdentifier)
                .DefineOptional64Char(colOrder++)
                ;
            this
                .Property(m => m.SessionProviderSerializedInformation)
                .DefineOptional4000CharSerializationValue(colOrder++)
                ;

            this
                .Property(x => x.ApplicationUserIdentifier)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.ApplicationOrganisationIdentifier)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;



            this
                .Property(m => m.LoginIP)
                .DefineOptional64Char(colOrder++)
                ;

            this
            .Property(m => m.LoginIPLatitude)
                .DefineOptional64Char(colOrder++)
            ;

            this
                    .Property(m => m.LoginIPLongitude)
                .DefineOptional64Char(colOrder++)
                    ;

            this
                    .Property(m => m.LoginIPTimeZone)
                .DefineOptional64Char(colOrder++)
                    ;

            this
                    .Property(m => m.LoginIPCityName)
                .DefineOptional64Char(colOrder++)
                    ;

            this
                    .Property(m => m.LoginIPRegion)
                .DefineOptional64Char(colOrder++)
                    ;

            this
                    .Property(m => m.LoginIPPostalCode)
                .DefineOptional64Char(colOrder++)
                    ;


            this
                    .Property(m => m.LoginIPCountry)
                .DefineOptional64Char(colOrder++)
                    ;







            this
                .Property(m => m.LastActivityIP)
                .DefineOptional64Char(colOrder++)
                ;

            this
            .Property(m => m.LastActivityIPLatitude)
                .DefineOptional64Char(colOrder++)
            ;

            this
                    .Property(m => m.LastActivityIPLongitude)
                .DefineOptional64Char(colOrder++)
                    ;

            this
                    .Property(m => m.LastActivityIPTimeZone)
                .DefineOptional64Char(colOrder++)
                    ;

            this
                    .Property(m => m.LastActivityIPCityName)
                .DefineOptional64Char(colOrder++)
                    ;

            this
                    .Property(m => m.LastActivityIPRegion)
                .DefineOptional64Char(colOrder++)
                    ;

            this
                    .Property(m => m.LastActivityIPPostalCode)
                .DefineOptional64Char(colOrder++)
                    ;


            this
                    .Property(m => m.LastActivityIPCountry)
                .DefineOptional64Char(colOrder++)
                    ;





            this
                .Property(m => m.Tag)
                .DefineOptional256CharTag(colOrder++)
                ;


        }
    }
}