namespace XAct.Security.Session.Services.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Session}"/>
    /// to seed the Tip tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface ISessionDbContextSeeder : IHasXActLibDbContextSeeder<Session>
    {

    }
}