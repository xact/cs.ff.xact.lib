namespace XAct.Security.Session.Services.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class SessionDbContextSeeder : XActLibDbContextSeederBase<Session>, ISessionDbContextSeeder
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="SessionDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public SessionDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}