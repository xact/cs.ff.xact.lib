﻿
namespace XAct.Security.Session.Services.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Security.Session.Services.Initialization.Maps;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class SessionDbModelBuilder : ISessionDbModelBuilder
    {
        private readonly string _typeName;

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionDbModelBuilder" /> class.
        /// </summary>
        public SessionDbModelBuilder()
        {
            _typeName = this.GetType().Name;

        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add((EntityTypeConfiguration<Session>) XAct.DependencyResolver.Current.GetInstance<ISessionModelPersistenceMap>());

        }
    }
}