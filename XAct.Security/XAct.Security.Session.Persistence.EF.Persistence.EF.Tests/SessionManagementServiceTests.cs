namespace KW_NAMESPACENAME.Tests
{
    using System;
    using System.Diagnostics;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Security.Session;
    using XAct.Security.Session.Services;
    using XAct.Security.Session.Services.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SessionManagementServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //Singleton<IocContext>.Instance.ResetIoC();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Can_Get_SessionManagementService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISessionManagementService>();
            Assert.That(service,Is.Not.Null);
        }
        [Test]
        public void Can_Get_SessionManagementService_Of_Expected_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISessionManagementService>();
            Assert.That(service.GetType(), Is.EqualTo(typeof(SessionManagementService)));
        }

        [Test]
        public void Can_Create_Session()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISessionManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var sessionCreationRequest = new SessionCreationRequest();

            sessionCreationRequest.SessionProviderKey = "foo123";
            sessionCreationRequest.SessionProviderUserIdentifier = "user123";
            sessionCreationRequest.ClientIP = "";
            //sessionCreationRequest.SessionProviderUserOrganisationIdentifier = "-1";

            sessionCreationRequest.ApplicationUserFK = 1.ToGuid();
            //sessionCreationRequest.ApplicationUserOrganisationFK = 11.ToGuid();

            var sessionResponse = service.CreateSession(sessionCreationRequest);
            uowService.GetCurrent().Commit();


            Assert.That(sessionResponse, Is.Not.Null);
            Assert.That(sessionResponse.Status, Is.EqualTo(SessionResponseStatus.Success),"Status should be success");
        }


        [Test]
        public void Does_Session_Exist()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISessionManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var sessionCreationRequest = new SessionCreationRequest();

            sessionCreationRequest.SessionProviderKey = "foo123a43";
            sessionCreationRequest.SessionProviderUserIdentifier = "user123";
            sessionCreationRequest.ClientIP = "";
            //sessionCreationRequest.SessionProviderUserOrganisationIdentifier = "-1";

            sessionCreationRequest.ApplicationUserFK = 1.ToGuid();
            //sessionCreationRequest.ApplicationUserOrganisationFK = 11.ToGuid();

            var sessionResponse = service.CreateSession(sessionCreationRequest);
            uowService.GetCurrent().Commit();


            var check = service.DoesSessionExist(sessionResponse.SessionSummary.SessionId,true);

            Assert.That(sessionResponse, Is.Not.Null);
            Assert.That(sessionResponse.Status, Is.EqualTo(SessionResponseStatus.Success), "Status should be success");
            Assert.That(check, Is.EqualTo(SessionResponseStatus.Success), "Check Status should be success");
        }


        [Test]
        public void Update_Session_LastAccessed_When_Valid()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISessionManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var sessionCreationRequest = new SessionCreationRequest();

            sessionCreationRequest.SessionProviderKey = "foo1x983a43";
            sessionCreationRequest.SessionProviderUserIdentifier = "user123";
            sessionCreationRequest.ClientIP = "";
            //sessionCreationRequest.SessionProviderUserOrganisationIdentifier = "-1";

            sessionCreationRequest.ApplicationUserFK = 1.ToGuid();
            //sessionCreationRequest.ApplicationUserOrganisationFK = 11.ToGuid();

            var sessionResponse = service.CreateSession(sessionCreationRequest);
            uowService.GetCurrent().Commit();



            var check = service.DoesSessionExist(sessionResponse.SessionSummary.SessionId, true);

            var updateSessionResponse = service.UpdateSessionLastAccessed(sessionResponse.SessionSummary.SessionId, true);

            Trace.WriteLine("Status: "+ updateSessionResponse.Status);

            Assert.That(sessionResponse, Is.Not.Null);
            Assert.That(sessionResponse.Status, Is.EqualTo(SessionResponseStatus.Success), "Status should be success");
            Assert.That(check, Is.EqualTo(SessionResponseStatus.Success), "Check Status should be success");

            Assert.That(updateSessionResponse, Is.Not.Null,"Update response should not be null.");
            Assert.That(updateSessionResponse.Status, Is.EqualTo(SessionResponseStatus.Success), "Update should be Successful");
            Assert.That(updateSessionResponse.SessionSummary, Is.Not.Null,"Update should return a SessionSummary");

        }


        [Test]
        public void Update_Session_LastAccessed_When_Timeout()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISessionManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            dateTimeService.ResetUTCOffset();

            Trace.WriteLine("Service.NowUtc: " + dateTimeService.NowUTC);

            TimeSpan offset = service.Configuration.SessionInactivityTimeout.Add(TimeSpan.FromMinutes(2));

            Trace.WriteLine("Offset: " + offset);

            dateTimeService.SetUTCOffset(offset);

            Trace.WriteLine("Service.NowUtc: " + dateTimeService.NowUTC);
            Trace.WriteLine("DateTime.NowUtc: " + DateTime.UtcNow);
            Trace.WriteLine("DateTime.NowUtc#2: " + DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(10)));

            //Assert that we did the math right and the service's notion of time is in the past by some minutes
            Assert.That(dateTimeService.NowUTC, Is.LessThan(DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(10))),"DateTimeService should now be in the past.");

            var sessionCreationRequest = new SessionCreationRequest();

            sessionCreationRequest.SessionProviderKey = "fowdflg1x983a43";
            sessionCreationRequest.SessionProviderUserIdentifier = "user123";
            sessionCreationRequest.ClientIP = "";
            //sessionCreationRequest.SessionProviderUserOrganisationIdentifier = "-1";

            sessionCreationRequest.ApplicationUserFK = 1.ToGuid();
            //sessionCreationRequest.ApplicationUserOrganisationFK = 11.ToGuid();

            var sessionCreationResponse = service.CreateSession(sessionCreationRequest);
            uowService.GetCurrent().Commit();

            //Reset time offset:
            dateTimeService.ResetUTCOffset();


            //But as its in the past, it should not be able to updated:

            var check = service.DoesSessionExist(sessionCreationResponse.SessionSummary.SessionId, true);

            //And now update it:
            var updateSessionResponse = service.UpdateSessionLastAccessed(sessionCreationResponse.SessionSummary.SessionId, true);


            Assert.That(sessionCreationResponse, Is.Not.Null);
            Assert.That(sessionCreationResponse.Status, Is.EqualTo(SessionResponseStatus.Success), "Creation Status should be success as Offset not yet applied.");

            //The result is not deeply checked (ie don't get fine grain explanation of why)
            Assert.That(check, Is.EqualTo(SessionResponseStatus.SessionInvalid), "Check Status should not be success as it's expired");

            Assert.That(updateSessionResponse, Is.Not.Null, "Update response should not be null.");
            Assert.That(updateSessionResponse.Status, Is.EqualTo(SessionResponseStatus.SessionExpiredDueToInactivity), "Update Status should be Session Expired Due To Inactivity");
            Assert.That(updateSessionResponse.SessionSummary, Is.Null, "Update should return a null SessionSummary");

        }

        [Test]
        public void Update_Session_LastAccessed_When_TooLong()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISessionManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            var dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            dateTimeService.ResetUTCOffset();

            Trace.WriteLine("Service.NowUtc: " + dateTimeService.NowUTC);

            TimeSpan offset = service.Configuration.SessionMaxLength.Add(TimeSpan.FromMinutes(2));

            Trace.WriteLine("Offset: " + offset);

            dateTimeService.SetUTCOffset(offset);

            Trace.WriteLine("Service.NowUtc: " + dateTimeService.NowUTC);
            Trace.WriteLine("DateTime.NowUtc: " + DateTime.UtcNow);
            Trace.WriteLine("DateTime.NowUtc#2: " + DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(10)));

            //Assert that we did the math right and the service's notion of time is in the past by some minutes
            Assert.That(dateTimeService.NowUTC, Is.LessThan(DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(10))), "DateTimeService should now be in the past.");

            var sessionCreationRequest = new SessionCreationRequest();

            sessionCreationRequest.SessionProviderKey = "fowsiu1x983a43";
            sessionCreationRequest.SessionProviderUserIdentifier = "user123";
            sessionCreationRequest.ClientIP = "";
            //sessionCreationRequest.SessionProviderUserOrganisationIdentifier = "-1";

            sessionCreationRequest.ApplicationUserFK = 1.ToGuid();
            //sessionCreationRequest.ApplicationUserOrganisationFK = 11.ToGuid();

            var sessionCreationResponse = service.CreateSession(sessionCreationRequest);
            uowService.GetCurrent().Commit();

            //Reset time offset:
            dateTimeService.ResetUTCOffset();


            //But as its in the past, it should not be able to updated:

            var check = service.DoesSessionExist(sessionCreationResponse.SessionSummary.SessionId, true);

            //And now update it:
            var updateSessionResponse = service.UpdateSessionLastAccessed(sessionCreationResponse.SessionSummary.SessionId, true);


            Assert.That(sessionCreationResponse, Is.Not.Null);
            Assert.That(sessionCreationResponse.Status, Is.EqualTo(SessionResponseStatus.Success), "Creation Status should be success as Offset not yet applied.");

            //The result is not deeply checked (ie don't get fine grain explanation of why)
            Assert.That(check, Is.EqualTo(SessionResponseStatus.SessionInvalid), "Check Status should not be success as it's expired");

            Assert.That(updateSessionResponse, Is.Not.Null, "Update response should not be null.");
            Assert.That(updateSessionResponse.Status, Is.EqualTo(SessionResponseStatus.SessionExpiredDueToDurationTooLong), "Update Status should be Session Expired Due To Being too long");
            Assert.That(updateSessionResponse.SessionSummary, Is.Null, "Update should return a null SessionSummary");

        }


    }
}


