namespace XAct.Security.Session.Services.Initialization
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class UnitTestSessionDbContextSeeder : UnitTestXActLibDbContextSeederBase<Session>, ISessionDbContextSeeder
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="SessionDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public UnitTestSessionDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        public override void CreateEntities()
        {
        }
    }
}