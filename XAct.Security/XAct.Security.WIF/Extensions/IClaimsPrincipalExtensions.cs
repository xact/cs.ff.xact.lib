﻿
#if NET35  
using System;
#endif

using Microsoft.IdentityModel.Claims;
#if CONTRACTS_FULL || NET40 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

    /// <summary>
    /// Extensions to the Windows Identity Foundation <see cref="IClaimsPrincipal"/> object.
    /// </summary>
    /// <remarks>
    /// 
    /// </remarks>
    /// <internal>
    /// The new Identities property (plural) is need because a principal can be associated with several identities, e. g., 
    /// on delegation scenarios.
    /// </internal>
    public static class IClaimsPrincipalExtensions
    {

        /// <summary>
        /// Nothings special.
        /// </summary>
        /// <param name="claimsPrincipal">The claims principal.</param>
        public static void Nothing(IClaimsPrincipal claimsPrincipal)
        {
            //claimsPrincipal.Identities[]
        }
    }
