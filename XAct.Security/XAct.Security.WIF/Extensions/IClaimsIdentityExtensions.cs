﻿using System;
using System.Linq;
using Microsoft.IdentityModel.Claims;

    /// <summary>
    /// Extensions to the Windows Identity Foundation <see cref="IClaimsIdentity"/> object.
    /// </summary>
  public static class IClaimsIdentityExtensions
    {

        /// <summary>
        /// Gets the named claim, or null if it doesn't exist.
        /// </summary>
        /// <param name="claimsIdentity">The claims identity.</param>
        /// <param name="claimName">Name of the claim.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        public static Claim GetClaim(this IClaimsIdentity claimsIdentity, string claimName, bool raiseExceptionIfNotFound=true)
        {
            Claim result = claimsIdentity.Claims.Where(c => c.ClaimType.EndsWith(claimName)).FirstOrDefault();
            if ((result == null) && raiseExceptionIfNotFound)
            {
                throw new ArgumentOutOfRangeException("claimName");
            }
            return result;
        }

      /// <summary>
      /// Gets the claim string string value (you still would have to convert it to the appropriate type).
      /// <para>
      /// returns null if not found.
      /// </para>
      /// </summary>
      /// <param name="claimsIdentity">The claims identity.</param>
      /// <param name="claimName">Name of the claim.</param>
      /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
      /// <returns></returns>
        public static string GetClaimValue(this IClaimsIdentity claimsIdentity, string claimName, bool raiseExceptionIfNotFound=true)
        {
            Claim claim = claimsIdentity.GetClaim(claimName,raiseExceptionIfNotFound);

            return (claim!=null)?claim.Value:null;

            //Note that the Claim ValueType will be an Xml string describing the type of the value,
            //and will look like 'http://www.w3.org/2001/XMLSchema#string' (for 2011-08-20T09:58:15.774Z) or 
            // 'http://www.w3.org/2001/XMLSchema#dateTime'
         }
    }

