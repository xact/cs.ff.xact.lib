﻿
using System;
using System.Diagnostics;
using XAct.Diagnostics;

namespace XAct.Security.NetSqlAzMan
{
    public class NetSqlAuthorizationService : IOperationBasedAuthorizationService, IRoleBasedAuthorizationService
    {

        #region Services

        private readonly ILoggingService _loggingService;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="NetSqlAuthorizationService"/> class.
        /// </summary>
        /// <param name="loggingService">The logging service.</param>
        public NetSqlAuthorizationService (ILoggingService loggingService)
        {
            if (loggingService == null){throw new ArgumentNullException("loggingService");}
            _loggingService = loggingService;
        }
        #endregion

        #region Implementation of IOperationBasedAuthorizationService
        /// <summary>
        /// Is the current identity allowed to perform the following operation?
        /// <para>
        /// Note: will work against current thread's IIdentity.
        /// </para>
        /// </summary>
        /// <param name="operationName"></param>
        /// <returns></returns>
        /// <internal>
        /// See AzMan operation.
        /// </internal>
        public bool IsOperationAllowed(string operationName)
        {
            return IsOperationAllowed(System.Threading.Thread.CurrentPrincipal.Identity, operationName);

        }

        /// <summary>
        /// Is the current identity allowed to perform the following operation?
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="operationName"></param>
        /// <returns></returns>
        /// <internal>
        /// See AzMan operation.
        /// </internal>
        public bool IsOperationAllowed(System.Security.Principal.IIdentity identity, string operationName)
        {
            _loggingService.Trace(TraceLevel.Verbose, "Checking whether '{0}' has '{1}' Operation rights".FormatStringCurrentCulture(identity.Name,operationName));

            throw new System.NotImplementedException();
        }
        #endregion
    }
}
