namespace XAct.Security
{
    using System;
    using System.Runtime.InteropServices;
    using System.Security.Principal;
    using XAct.Diagnostics;

    /// <summary>
    /// A means of impersonating a user context.
    /// <para>
    /// Note that not only is Impersonation a very expensive (CPU wise)
    /// operation, and these days is a frowned upon practices, it also
    /// happens to be non-portable code, as it invokes Win32 API.
    /// </para>
    /// <para>
    /// That said, sometimes there is a use for it...
    /// </para>
    /// </summary>
    public class ImpersonationContext : IDisposable
    {
        private readonly WindowsImpersonationContext _idContext;
        private readonly IntPtr _idToken = IntPtr.Zero;
        private bool _disposed;



        private readonly ITracingService _tracingService;


        /// <summary>
        /// Initializes a new instance of the <see cref="ImpersonationContext"/> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <param name="name">The name.</param>
        /// <param name="domain">The domain.</param>
        /// <param name="password">The password.</param>
        /// <internal>
        /// Declare signatures for Win32 LogonUser and CloseHandle APIs
        /// </internal>
        public ImpersonationContext(ITracingService tracingService, string name, string domain, string password)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            if (name.IsNullOrEmpty())
            {
                throw new ArgumentNullException("name");
            }
            if (domain.IsNullOrEmpty())
            {
                throw new ArgumentNullException("domain");
            }
            if (password.IsNullOrEmpty())
            {
                throw new ArgumentNullException("password");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            _tracingService = tracingService;

            try
            {
                    _tracingService.Trace(TraceLevel.Verbose,
                                        "IdentityContext::ctor() : Attempting to impersonate name=[{0}] domain=[{1}]",
                                        name, domain);

                // Create a token for DomainName\Bob            
                bool result = API.LogonUser(name, domain, password,
                                            API.LogonSessionType.Network,
                                            API.LogonProvider.Default,
                                            out _idToken);
                if (!result)
                {
                    throw new Exception(
                        "Failed to authenticate for impersonation name=[{0}] domain=[{1}]".FormatStringExceptionCulture(
                            new object[] {name, domain}));
                }

                WindowsIdentity windowsIdentity = new WindowsIdentity(_idToken);

                _idContext = windowsIdentity.Impersonate();

                    _tracingService.Trace(TraceLevel.Verbose,
                                        "IdentityContext::ctor() : Now impersonating name=[{0}] domain=[{1}]", name,
                                        domain);
            }
            catch (Exception exception)
            {
                _tracingService.TraceException(TraceLevel.Error, exception,
                                             "Failed to impersonate name=[{0}] domain=[{1}]", name, domain);

                throw new Exception(
                    "Failed to impersonate name=[{0}] domain=[{1}]".FormatStringExceptionCulture(new object[]
                                                                                                     {name, domain}),
                    exception);
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Member defined in <see cref="IDisposable"/>.
        /// </para>
        /// </remarks>
        public void Dispose()
        {
            if (!this._disposed)
            {
                _disposed = true;
                // Stop impersonation and revert to the process identity
                if (_idContext != null)
                {
                    if (_tracingService != null)
                    {
                        _tracingService.Trace(TraceLevel.Verbose,
                                            "IdentityContext::ctor() : Undoing impersonation.");
                    }

                    _idContext.Undo();
                }
                // Free the identity token
                if (_idToken != IntPtr.Zero)
                {
                    API.CloseHandle(_idToken);
                }
            }
        }


        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="ImpersonationContext"/> is reclaimed by garbage collection.
        /// </summary>
        ~ImpersonationContext()
        {
            Dispose();
        }


        /// <summary>
        /// 
        /// </summary>
// ReSharper disable InconsistentNaming
        protected class API
// ReSharper restore InconsistentNaming
        {
            #region Nested type: LogonSessionType

            /// <summary>
            /// 
            /// </summary>
            public enum LogonSessionType : uint
            {
                /// <summary>
                /// 
                /// </summary>
                Interactive = 2,
                /// <summary>
                /// 
                /// </summary>
                Network,
                /// <summary>
                /// 
                /// </summary>
                Batch,
                /// <summary>
                /// 
                /// </summary>
                Service,
                /// <summary>
                /// 
                /// </summary>
                NetworkCleartext = 8,
                /// <summary>
                /// 
                /// </summary>
                NewCredentials
            }

            #endregion

            #region Nested type: LogonProvider

            /// <summary>
            /// 
            /// </summary>
            public enum LogonProvider : uint
            {
                /// <summary>
                /// 
                /// </summary>
                Default = 0, // default for platform (use this!)
                /// <summary>
                /// 
                /// </summary>
// ReSharper disable InconsistentNaming
                WinNT35, // sends smoke signals to authority
// ReSharper restore InconsistentNaming
                /// <summary>
                /// 
                /// </summary>
// ReSharper disable InconsistentNaming
                WinNT40, // uses NTLM
// ReSharper restore InconsistentNaming
                /// <summary>
                /// 
                /// </summary>
// ReSharper disable InconsistentNaming
                WinNT50 // negotiates Kerb or NTLM
// ReSharper restore InconsistentNaming
            }

            #endregion

            /// <summary>
            /// 
            /// </summary>
            /// <param name="principal"></param>
            /// <param name="authority"></param>
            /// <param name="password"></param>
            /// <param name="logonType"></param>
            /// <param name="logonProvider"></param>
            /// <param name="token"></param>
            /// <returns></returns>
            [DllImport("advapi32.dll", SetLastError = true)]
            public static extern bool LogonUser(
                string principal,
                string authority,
                string password,
                LogonSessionType logonType,
                LogonProvider logonProvider,
                out IntPtr token);


            /// <summary>
            /// Closes the handle.
            /// </summary>
            /// <param name="handle">The handle.</param>
            /// <returns></returns>
            [DllImport("kernel32.dll", SetLastError = true)]
            public static extern bool CloseHandle(IntPtr handle);
        }
    }
}