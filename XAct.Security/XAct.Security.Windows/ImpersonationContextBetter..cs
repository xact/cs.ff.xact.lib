﻿namespace XAct.Security
{
    using System.Security.Principal;
    using XAct.Environment;

    /// <summary>
    /// MUST READ: http://visualstudiomagazine.com/articles/2004/05/01/activate-windows-impersonation-selectively.aspx
    /// </summary>
    public class ImpersonationContextBetter
    {

        IPrincipalService PrincipalService
        {
            get { return DependencyResolver.Current.GetInstance<IPrincipalService>(); }
        }

        void Example()
        {
            //Get an Identity....
            WindowsIdentity wi = (WindowsIdentity) PrincipalService.Principal.Identity;

            using (WindowsImpersonationContext wc = wi.Impersonate())
            {


                // this code accesses file system under 
                // the caller identity
                //...
                wc.Undo();
                // the code below accesses a SQL Server
                // database under ASP.NET process 
                // identity
            }
        }
    }
    
}
