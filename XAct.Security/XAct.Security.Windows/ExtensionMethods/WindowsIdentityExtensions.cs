﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;
    using XAct.Security.Authorization.Services.Implementations;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// Extension Methods to the <see cref="WindowsIdentity"/> object.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Mostly methods that are then used by 
    /// <see cref="WindowsAuthorizationService"/>, such as ways of 
    /// cheching in a User is a member of an AD group (eg: <see cref="IsInADGroups"/>),
    /// or getting the list of AD groups a User is in (eg: <see cref="GetADGroupNames(WindowsIdentity)"/>).
    /// </para>
    /// </remarks>
    public static class IdentityExtensions
    {
        /// <summary>
        /// Determines whether the windows identity is a member of the named AD groups.
        /// </summary>
        /// <param name="windowsIdentity">The windows identity.</param>
        /// <param name="ignoreDomain">if set to <c>true</c> [ignore domain].</param>
        /// <param name="adGroupNamesToMatch">The ad group names to match.</param>
        /// <returns>
        /// 	<c>true</c> if [is in AD groups] [the specified windows identity]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInADGroups(this WindowsIdentity windowsIdentity, bool ignoreDomain,
                                        params string[] adGroupNamesToMatch)
        {
            windowsIdentity.ValidateIsNotDefault("windowsIdentity");
            if ((adGroupNamesToMatch == null) || (adGroupNamesToMatch.Length == 0))
            {
                //Might turn out to be a little too tight?
                throw new ArgumentNullException("adGroupNamesToMatch");
            }

            IEnumerable<string> truncatedADGroupNamesToMatch =
                !ignoreDomain
// ReSharper disable RedundantEnumerableCastCall
                    ? adGroupNamesToMatch.Cast<string>()
// ReSharper restore RedundantEnumerableCastCall
                    : adGroupNamesToMatch.Select(adGroupName => RemoveDomain(adGroupName)).ToList();

            //GetADGroupNames will take more resources, so put that in outer loop.
            foreach (string existingGroupName in windowsIdentity.GetADGroupNames(ignoreDomain))
            {
                foreach (string adGroupName in truncatedADGroupNamesToMatch)
                {
                    if (!ignoreDomain)
                    {
                        if (String.CompareOrdinal(existingGroupName, adGroupName) == 0)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Gets an array of the names of the AD group the identity belongs to.
        /// <para>
        /// Can be returned without domain part for easier matching.
        /// </para>
        /// </summary>
        /// <param name="windowsIdentity">The windows identity.</param>
        /// <param name="ignoreDomain">if set to <c>true</c> [ignore domain].</param>
        /// <returns></returns>
// ReSharper disable InconsistentNaming
        public static string[] GetADGroupNames(this WindowsIdentity windowsIdentity, bool ignoreDomain)
// ReSharper restore InconsistentNaming
        {
            windowsIdentity.ValidateIsNotDefault("windowsIdentity");

            IList<string> results = new List<string>();

            foreach (string groupName in windowsIdentity.GetADGroupNames())
            {
                if (!ignoreDomain)
                {
                    results.Add(groupName);
                }
                else
                {
                    string truncatedGroupName = RemoveDomain(groupName);
                    results.AddIfNotAlreadyAdded(truncatedGroupName);
                }
            }

            return results.ToArray();
        }

        /// <summary>
        /// Gets the names of the AD group the identity belongs to.
        /// </summary>
        /// <param name="windowsIdentity">The windows identity.</param>
        /// <returns></returns>
        public static IEnumerable<string> GetADGroupNames(this WindowsIdentity windowsIdentity)
        {
            windowsIdentity.ValidateIsNotDefault("windowsIdentity");

            if (windowsIdentity.Groups != null)
            {
                foreach (IdentityReference reference in windowsIdentity.Groups)
                {
                    IdentityReference groupReference = reference.TranslateSIDToGroup();

                    if (groupReference == null)
                    {
                        continue;
                    }
                    //return name of group:
                    yield return groupReference.Value;
                }
            }
        }


        /// <summary>
        /// Removes the domain part of the returned group name.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <returns></returns>
        private static string RemoveDomain(string groupName)
        {
            return groupName.LastPart('\\');
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
