﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Security.Principal;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// Extensions to the IdentityReference object.
    /// </summary>
    public static class IdentityReferenceExtensions
    {
        /// <summary>
        /// Translates a SID to the AD Group that it represents
        /// </summary>
        /// <param name="identityReference"></param>
        /// <returns></returns>
        public static IdentityReference TranslateSIDToGroup(this IdentityReference identityReference)
        {
            identityReference.ValidateIsNotDefault("identityReference");

            return identityReference.Translate(typeof (NTAccount));
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
