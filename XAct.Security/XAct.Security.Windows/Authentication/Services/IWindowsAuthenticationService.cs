﻿namespace XAct.Security.Authentication
{
    /// <summary>
    /// The contract for a Windows Identity based authentication Service.
    /// </summary>
    public interface IWindowsAuthenticationService : IAuthenticationService, IHasXActLibService
    {
        //public AuthenticateUser(string userName, string password);
    }
}