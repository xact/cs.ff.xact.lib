﻿namespace XAct.Security.Authentication.Services.Implementations
{
    using System;
    using System.Security.Principal;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// A WindowsIndentity based implementation of
    /// <see cref="IWindowsAuthenticationService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IAuthenticationService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class WindowsAuthenticationService : XActLibServiceBase, IWindowsAuthenticationService
    {
        #region Services

        private readonly IPrincipalService _principalService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsAuthenticationService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <internal>7/16/2011: Sky</internal>
        public WindowsAuthenticationService(ITracingService tracingService, IPrincipalService principalService):base(tracingService)
        {
            _principalService = principalService;
        }

        #endregion

        /// <summary>
        /// Logins the specified username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <internal>5/14/2011: Sky</internal>
        /// <internal><para>7/16/2011: Sky</para></internal>
        public bool Login(string username, string password)
        {
            throw new NotImplementedException();
        }


        //public void ValidateUser(string userName, string password)
        //{
        //    //PrincipalContext pc = new PrincipalContext(ContextType.Domain);
        //    //bool isCredentialValid = pc.ValidateCredentials(username, password);
        //}

        /// <summary>
        /// Logouts the current identity.
        /// </summary>
        /// <internal>5/14/2011: Sky</internal>
        /// <internal><para>7/16/2011: Sky</para></internal>
        public void Logout()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the current Thread Principal,
        /// from which one can derive the Identity as needed.
        /// </summary>
        /// <value>The current.</value>
        /// <internal>8/10/2011: Sky</internal>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public IPrincipal CurrentPrincipal
        {
            get { 
                //Note that WindowsIdentity is *NOT* available on Ag, or CF
                //return System.Security.Principal.WindowsIdentity.GetCurrent();
                return _principalService.Principal;
            }
        }
    }
}