﻿namespace XAct.Security.Authorization.Services.Implementations
{
    using System.Security.Principal;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// A windows/Integrated Security bases Authorization Service.
    /// </summary>
    public class WindowsAuthorizationService : XActLibServiceBase, IWindowsAuthorizationService
    {

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsAuthorizationService"/> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <internal><para>7/16/2011: Sky</para></internal>
        public WindowsAuthorizationService(ITracingService tracingService):base(tracingService)
        {
        }

        #endregion

        /// <summary>
        /// Determines whether this identity is in the specified AD group.
        /// </summary>
        /// <param name="windowsIdentity">The windows identity.</param>
        /// <param name="ignoreDomains">Ignore matching against Domain part.</param>
        /// <param name="adGroupNames">One or more AD group names.</param>
        /// <returns>
        /// 	<c>true</c> if [is in role] [the specified windows identity]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsInADGroups(WindowsIdentity windowsIdentity, bool ignoreDomains, params string[] adGroupNames)
        {
            return windowsIdentity.IsInADGroups(ignoreDomains, adGroupNames);
        }
    }
}