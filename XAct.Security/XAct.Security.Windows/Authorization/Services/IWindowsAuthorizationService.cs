﻿namespace XAct.Security.Authorization
{
    using System.Security.Principal;

    /// <summary>
    /// Contract for a Windows based Authorization Service.
    /// </summary>
    public interface IWindowsAuthorizationService : IHasXActLibService
    {
        /// <summary>
        /// Determines whether this identity is in the specified AD group.
        /// </summary>
        /// <param name="windowsIdentity">The windows identity.</param>
        /// <param name="ignoreDomain">Ignore matching against Domain part.</param>
        /// <param name="adGroupNames">One or more AD group names.</param>
        /// <returns>
        /// 	<c>true</c> if [is in role] [the specified windows identity]; otherwise, <c>false</c>.
        /// </returns>
        bool IsInADGroups(WindowsIdentity windowsIdentity, bool ignoreDomain, string[] adGroupNames);
    }
}