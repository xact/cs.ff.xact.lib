﻿using System;
using System.Runtime.Serialization;

namespace XAct.Security.Entities
{
    using XAct.Security.Authentication;


    [DataContract]
    public class UserMembershipInfo : IIMembershipInfo, IHasApplicationTennantId, IHasUserIdentifier
        //, IAuthenticationIdentityInfo<Guid>
    {
        [DataMember]
        public virtual Guid Id { get; set; }
        [DataMember]
        public virtual byte[] Timestamp { get; set; }
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }

        [DataMember]
        public virtual string UserIdentifier { get; set; }
        
        [DataMember]
        public virtual bool Enabled { get; set; }

        [DataMember]
        public virtual string ChallengeQuestion { get; set; }
        [DataMember]
        public virtual string ChallengeAnswer { get; set; }

        //[DataMember]
        //public string UserName { get; set; }

        [DataMember]
        public virtual string EmailAddress { get; set; }
        [DataMember]
        public virtual bool EmailConfirmed { get; set; }

        [DataMember]
        public virtual string SecurityStamp { get; set; }
        [DataMember]
        public virtual string TwoFactorInformation { get; set; }
        [DataMember]
        public virtual bool TwoFactorInformationConfirmed { get; set; }

        //Only relevant to local u/p storage.
        [DataMember]
        public virtual DateTime? LockoutEndedDateTimeUtc { get; set; }
        [DataMember]
        public virtual int LoginFailedAttemptsCount { get; set; }
        [DataMember]
        public virtual string PasswordHash { get; set; }
    }
}
