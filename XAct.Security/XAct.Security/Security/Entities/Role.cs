﻿using System;

namespace XAct.Security.Entities
{
    using System.Runtime.Serialization;


    [DataContract]
    public class Role : IHasXActLibEntity, IHasApplicationTennantIdSpecificDistributedGuidId, 
        IHasDeleted, 
        IHasEnabled, 
        //IHasCode, 
        //IHasResourceFilter, 
        //IHasNameAndDescription,
        IHasName,
        IHasDescription
        
    {
        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }

        [DataMember]
        public virtual Guid? ParentFK { get; set; }

        [DataMember]
        public virtual Role Parent { get; set; }

        [DataMember]
        public virtual bool Deleted { get; set; }

        [DataMember]
        public virtual bool Enabled { get; set; }

        ///// <summary>
        ///// The unique textual code (eg: 'X123') for this object.
        ///// </summary>
        //[DataMember]
        //public virtual string Code { get; set; }

        ///// <summary>
        ///// Gets or sets the resource filter.
        ///// </summary>
        ///// <value>
        ///// The resource filter.
        ///// </value>
        //[DataMember]
        //public virtual string ResourceFilter { get; set; }

        /// <summary>
        /// Gets or sets the name,.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the role's description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Role"/> class.
        /// </summary>
        public Role()
        {
            this.GenerateDistributedId();
            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            this.Enabled = true;
        }

    }
}
