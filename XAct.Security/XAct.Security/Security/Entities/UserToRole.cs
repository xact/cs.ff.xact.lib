﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Security.Entities
{
    using System.Runtime.Serialization;


    [DataContract]
    public class UserToRole : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, IHasUserIdentifier
    {
        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }

        [DataMember]
        public virtual string UserIdentifier { get; set; }

        [DataMember]
        public virtual Role Role { get; set; }

        [DataMember]
        public virtual Guid RoleFK { get; set; }

        public UserToRole()
        {
            this.GenerateDistributedId();
        }

    }

}
