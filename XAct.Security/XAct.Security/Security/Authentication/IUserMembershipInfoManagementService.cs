﻿namespace XAct.Security.Authentication
{
    using XAct.Security.Entities;

    public interface IUserMembershipInfoManagementService : IHasXActLibService
    {
        /// <summary>
        /// Generates the new password.
        /// </summary>
        /// <internal><para>5/13/2011: Sky</para></internal>
        string GeneratePassword();



        /// <summary>
        /// Creates the member/user from the given minimal information on the user.
        /// <para>
        /// Note that if <see cref="IAuthenticationIdentityInfo{TId}.IsApproved" /> is not set, it defaults to <c>false</c>.
        /// </para>
        /// </summary>
        /// <typeparam name="TId">The type of the data.</typeparam>
        /// <param name="userMembershipInfo">The user membership information.</param>
        /// <internal>5/14/2011: Sky</internal>
        void CreateMember<TId>(UserMembershipInfo userMembershipInfo);


        
        /// <summary>
        /// Changes the password question and answer.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="newChallengeQuestion">The new challenge question.</param>
        /// <param name="newChallengeAnswer">The new challenge answer.</param>
        /// <internal><para>5/13/2011: Sky</para></internal>
        void ChangeChallengeQuestionAndAnswer(string userName, string newChallengeQuestion,
                                              string newChallengeAnswer);


        /// <summary>
        /// Resets password to an automatically generated password
        /// </summary>
        /// <internal>
        /// Should invoke <see cref="GeneratePassword"/>.
        /// </internal>
        /// <param name="userName"></param>
        /// <param name="answerToChallengeQuestion"></param>
        void ResetPassword(string userName, string answerToChallengeQuestion);

        /// <summary>
        /// Changes the user's password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <internal><para>5/13/2011: Sky</para></internal>
        void ChangePassword(string userName, string oldPassword, string newPassword);


        /// <summary>
        /// Validates that the member/user's name/pwd combination is in the datastore.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <internal><para>5/13/2011: Sky</para></internal>
        bool ValidateMember(string userName, string password);



        /// <summary>
        /// Unlocks a the member/user who has locked up their account
        /// (probably by entering too many wrong passwords 
        /// within a given time).
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        void UnlockMember(string userName);


        
        
    }
}