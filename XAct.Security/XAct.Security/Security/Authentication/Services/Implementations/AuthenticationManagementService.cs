﻿using System;
using System.Diagnostics;
using XAct.Diagnostics;
using XAct.Services;

#if CONTRACTS_FULL || NET40 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace XAct.Security.Authentication.Services.Implementations
{
    /// <summary>
    /// An implementation of the 
    /// <see cref="IAuthenticationManagementServiceProvider"/>
    /// contract.
    /// </summary>
    /// <remarks>
    /// <para>
    /// There are a couple of reasons to use this class rather than a more tech specific one:
    /// </para>
    /// <para>
    /// The first is that it is tech agnostic.
    /// </para>
    /// <para>
    /// The second is that it adds logging, so that any implementation will always have some logging.
    /// </para>
    /// </remarks>
    /// <internal><para>5/15/2011: Sky</para></internal>
    [DefaultBindingImplementation(typeof(IAuthenticationManagementService))]
    public class NOMOREAuthenticationManagementService : IAuthenticationManagementService
    {
        #region Services

        private readonly IAuthenticationManagementServiceProvider _authenticationManagementService;
        private readonly ITracingService _tracingService;

        #endregion

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="AuthenticationManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <param name="authenticationManagementService">The authentication management service.</param>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public NOMOREAuthenticationManagementService(ITracingService tracingService,
                                               IAuthenticationManagementServiceProvider authenticationManagementService)
        {
            if (tracingService == null)
            {
                throw new ArgumentNullException("tracingService");
            }
            if (authenticationManagementService == null)
            {
                throw new ArgumentNullException("authenticationManagementService");
            }

            _tracingService = tracingService;
            _authenticationManagementService = authenticationManagementService;
        }

        #region IAuthenticationManagementService Members

        /// <summary>
        /// Generates the new password.
        /// </summary>
        /// <returns></returns>
        /// <internal>5/13/2011: Sky</internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public string GeneratePassword()
        {
            return _authenticationManagementService.GeneratePassword();
        }

        /// <summary>
        /// Changes the password question and answer.
        /// </summary>
        /// <param name="identityName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <param name="newChallengeQuestion">The new challenge question.</param>
        /// <param name="newChallengeAnswer">The new challenge answer.</param>
        /// <internal>5/13/2011: Sky</internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public void ChangeChallengeQuestionAndAnswer(string identityName, string password, string newChallengeQuestion,
                                                     string newChallengeAnswer)
        {
            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "Updating Question/Answer for {0}".FormatStringCurrentUICulture(identityName));

            _authenticationManagementService.ChangeChallengeQuestionAndAnswer(identityName, password,
                                                                              newChallengeQuestion, newChallengeAnswer);
        }

        /// <summary>
        /// Resets password to an automatically generated password
        /// </summary>
        /// <param name="identityName"></param>
        /// <param name="answerToChallengeQuestion"></param>
        /// <internal>
        /// Should invoke <see cref="GeneratePassword"/>.
        /// </internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public void ResetPassword(string identityName, string answerToChallengeQuestion)
        {
            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "ResetPassword for {0}".FormatStringCurrentUICulture(identityName));

            _authenticationManagementService.ResetPassword(identityName, answerToChallengeQuestion);
        }

        /// <summary>
        /// Changes the user's password.
        /// </summary>
        /// <param name="identityName">Name of the user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <internal>5/13/2011: Sky</internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public void ChangePassword(string identityName, string oldPassword, string newPassword)
        {
            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "Change Password for {0} to {1}".FormatStringCurrentUICulture(identityName),
                                  newPassword.SafeString());

            _authenticationManagementService.ChangePassword(identityName, oldPassword, newPassword);
        }

        /// <summary>
        /// Validates that the member/user's name/pwd combination is in the datastore.
        /// </summary>
        /// <param name="identityName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <internal>5/13/2011: Sky</internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public bool ValidateMember(string identityName, string password)
        {
            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "ValidatingMember {0}/{1}".FormatStringCurrentUICulture(identityName),
                                  password.SafeString());

            return _authenticationManagementService.ValidateMember(identityName, password);
        }

        /// <summary>
        /// Unlocks a the member/user who has locked up their account
        /// (probably by entering too many wrong passwords
        /// within a given time).
        /// </summary>
        /// <typeparam name="TId">The type of the id.</typeparam>
        /// <param name="identityInfo">The membership info.</param>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public void UnlockMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
        {
            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "Unlocking User {0}".FormatStringCurrentUICulture(identityInfo.Name));

            _authenticationManagementService.UnlockMember(identityInfo);
        }

        /// <summary>
        /// Unlocks a the member/user who has locked up their account
        /// (probably by entering too many wrong passwords
        /// within a given time).
        /// </summary>
        /// <param name="identityName"></param>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public void UnlockMember(string identityName)
        {
            _tracingService.Trace(0, TraceLevel.Verbose, "Unlocking User {0}".FormatStringCurrentUICulture(identityName));

            _authenticationManagementService.UnlockMember(identityName);
        }

        /// <summary>
        /// Creates the member/user from the given minimal information on the user.
        /// <para>
        /// Note that if <see cref="IAuthenticationIdentityInfo{TId}.IsApproved"/> is not set, it defaults to <c>false</c>.
        /// </para>
        /// </summary>
        /// <typeparam name="TId">The type of the data.</typeparam>
        /// <param name="identityInfo">The membership info.</param>
        /// <internal>5/14/2011: Sky</internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public void CreateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
        {
            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "Creating new User {0}".FormatStringCurrentUICulture(identityInfo.Name));

            _authenticationManagementService.UnlockMember(identityInfo);
        }

        /// <summary>
        /// Updates the given member/user.
        /// <para>
        /// Updates
        /// <see cref="IAuthenticationIdentityInfo{TId}.Name"/>,
        /// <see cref="IAuthenticationIdentityInfo{TId}.EmailAddress"/>,
        /// <see cref="IAuthenticationIdentityInfo{TId}.Comment"/>,
        /// <see cref="IAuthenticationIdentityInfo{TId}.IsApproved"/>.
        /// </para>
        /// 	<para>
        /// One should not be updating any traceability fields (CreatedOn, LastEditedOn, etc.)
        /// </para>
        /// 	<para>
        /// Use <see cref="UnlockMember{TId}"/> to unlock a user.
        /// </para>
        /// 	<para>
        /// Use <see cref="ChangeChallengeQuestionAndAnswer"/> to change challenge Question and Answer.
        /// </para>
        /// 	<para>
        /// Use <see cref="ChangePassword"/> to change the user's password.
        /// </para>
        /// </summary>
        /// <typeparam name="TId">The type of the id.</typeparam>
        /// <param name="identityInfo">The membership info.</param>
        /// <internal>5/15/2011: Sky</internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public void UpdateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
        {
            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "Updating User {0}".FormatStringCurrentUICulture(identityInfo.Name));

            _authenticationManagementService.UpdateMember(identityInfo);
        }

        /// <summary>
        /// Deletes the member/user.
        /// </summary>
        /// <typeparam name="TId">The type of the id.</typeparam>
        /// <param name="identityInfo">The membership info.</param>
        /// <internal>5/15/2011: Sky</internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public void DeleteMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
        {
            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "Deleting User {0}".FormatStringCurrentUICulture(identityInfo.Name));

            _authenticationManagementService.DeleteMember(identityInfo);
        }

        /// <summary>
        /// Deletes the specified member/user.
        /// </summary>
        /// <param name="identityName">Name of the member/user.</param>
        /// <internal>5/13/2011: Sky</internal>
        /// <internal>5/15/2011: Sky</internal>
        public void DeleteMember(string identityName)
        {
            _tracingService.Trace(0, TraceLevel.Verbose, "Deleting User {0}".FormatStringCurrentUICulture(identityName));

            _authenticationManagementService.DeleteMember(identityName);
        }

        #endregion
    }
}