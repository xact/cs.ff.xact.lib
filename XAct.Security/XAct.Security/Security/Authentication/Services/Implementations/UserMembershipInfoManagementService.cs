﻿namespace XAct.Security.Authentication.Services.Implementations

{
    using System;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Net.Messaging;
    using XAct.Security;
    using XAct.Security.Authentication;
    using XAct.Security.Entities;
    using XAct.Services;
    using XAct.Tests.Services.Implementations;
    using XAct.Users;

    public class UserMembershipInfoManagementService:XActLibServiceBase, IUserMembershipInfoManagementService
    {
        private readonly IUserManagementService _userManagementService;
        private readonly ISimpleMessageService _simpleMessagingService;
        private readonly IRepositoryService _repositoryService;

        public IUserMembershipInfoManagementServiceConfiguration Configuration { get; private set; }

        //Generate one time so that it's internal Random is generated only once (more random).
        private readonly StrongPasswordGenerator _strongPasswordGenerator = new StrongPasswordGenerator();

        /// <summary>
        /// Initializes a new instance of the <see cref="UserMembershipInfoManagementService" /> class.
        /// </summary>
        /// <param name="userManagementService">The user management service.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="userMembershipInfoManagementServiceConfiguration">The user membership information management service configuration.</param>
        public UserMembershipInfoManagementService(
            ITracingService tracingService,
            IRepositoryService repositoryService,
            IUserManagementService userManagementService,
            ISimpleMessageService simpleMessagingService,
            IUserMembershipInfoManagementServiceConfiguration userMembershipInfoManagementServiceConfiguration)
            : base(tracingService)
        {
            _userManagementService = userManagementService;
            _simpleMessagingService = simpleMessagingService;
            _repositoryService = repositoryService;
            Configuration = userMembershipInfoManagementServiceConfiguration;
        }

        public string GeneratePassword()
        {
            var configuration = this.Configuration;
            return _strongPasswordGenerator.GeneratePassword(configuration.MinRequiredPasswordLength,
                                                             Math.Max(configuration.MinRequiredPasswordLength, 8),
                                                             configuration.MinRequiredNonAlphanumericCharacters > 0
                                                                 ? true
                                                                 : false,
                                                             null);
        }

        public void ChangeChallengeQuestionAndAnswer(string identityName, string newChallengeQuestion,
                                                     string newChallengeAnswer)
        {
            User user = _userManagementService.GetByName(identityName);

            if (user == null)
            {
                return;
            }

            UserMembershipInfo userMembership = _repositoryService.GetSingle<UserMembershipInfo>(x => x.Id == user.Id);

            if (userMembership == null)
            {
                return;
            }

            userMembership.ChallengeQuestion = newChallengeQuestion;
            userMembership.ChallengeAnswer = newChallengeAnswer;
            _repositoryService.UpdateOnCommit(user);
        }

        public void ResetPassword(string identityName, string answerToChallengeQuestion)
        {
            var userMembershipInfo = GetUserMembershipInfo(identityName);
            if (userMembershipInfo == null)
            {
                return;
            }
            if (
                System.String.Compare(userMembershipInfo.ChallengeAnswer, answerToChallengeQuestion,
                                      System.StringComparison.OrdinalIgnoreCase) != 0)
            {
                return;
            }
            string newPassword = GeneratePassword();

            _simpleMessagingService.Send(new EmailResetMessage(userMembershipInfo.EmailAddress, newPassword));
        }

        public void ChangePassword(string identityName, string oldPassword, string newPassword)
        {
            var userMembershipInfo = GetUserMembershipInfo(identityName, oldPassword);

            if (userMembershipInfo == null)
            {
                return;
            }
            userMembershipInfo.PasswordHash = newPassword.CalculateHash();

            _repositoryService.UpdateOnCommit(userMembershipInfo);
        }


        public bool ValidateMember(string identityName, string password)
        {
            var result = GetUserMembershipInfo(identityName, password) != null;
            return result;
        }


        public void UnlockMember(string identityName)
        {
            var userMembershipInfo = GetUserMembershipInfo(identityName);
            if (userMembershipInfo == null)
            {
                return;
            }
            userMembershipInfo.LockoutEndedDateTimeUtc = null;
            userMembershipInfo.LoginFailedAttemptsCount = 0;
            _repositoryService.UpdateOnCommit(userMembershipInfo);
        }

        public void CreateMember<TId>(UserMembershipInfo identityInfo)
        {
            _repositoryService.PersistOnCommit<UserMembershipInfo, Guid>(identityInfo, true);
        }

        public void Delete<TId>(string userIdentifier)
        {
            _repositoryService.DeleteOnCommit<UserMembershipInfo>(x=>x.UserIdentifier==userIdentifier);

        }

        public void DeleteMember(Guid id)
        {
            throw new NotImplementedException();
        }


        private UserMembershipInfo GetUserMembershipInfo(string identityName)
        {
            User user = _userManagementService.GetByName(identityName);

            if (user == null)
            {
                return null;
            }

            UserMembershipInfo userMembership = _repositoryService.GetSingle<UserMembershipInfo>(x => x.Id == user.Id);

            if (userMembership == null)
            {
                return null;
            }
            return userMembership;
        }
        private UserMembershipInfo GetUserMembershipInfo(string identityName, string password)
        {
            User user = _userManagementService.GetByName(identityName);

            if (user == null)
            {
                return null;
            }
            string oldPasswordHash = HashPassword(identityName, password);
            UserMembershipInfo userMembership = _repositoryService.GetSingle<UserMembershipInfo>(x => ((x.Id == user.Id) && (x.PasswordHash == oldPasswordHash)));

            if (userMembership == null)
            {
                return null;
            }
            return userMembership;
        }

        private string HashPassword(string name, string password)
        {
            password = password + Configuration.Salt;

            string passwordHash = password.CalculateHash();
            return passwordHash;
        }

    }
}