﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

namespace XAct.Security.Authentication.Implementations
{
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;
    using XAct.Tests.Services.Implementations;

    /// <summary>
    /// An implementation of 
    /// <see cref="IAuthenticationSelfManagementService"/>
    /// to provide a service to end users to manage their own authentication needs
    /// without exposing too many methods that give access to updating other users.
    /// <para>
    /// The Separation of Concerns ensures a more robust - securitywise - 
    /// service, as this service only allows updates by the current user,
    /// to the current user.
    /// </para>
    /// <para>
    /// Requires an implementation of <see cref="IAuthenticationManagementService"/>
    /// (eg: <c>MembershipProviderAuthenticationManagementService</c>, 
    /// <c>ActiveDirectoryAuthenticationService"/></c>, etc) to be available.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// An Example of usage would be:
    /// <code>
    /// <![CDATA[
    /// IAuthenticationSelfManagementService selfServiceAuthenticationManagementService 
    /// = ServiceLocatorService.Current.Get<IAuthenticationSelfManagementService>();
    /// 
    /// selfServiceAuthenticationManagementService.ChangePassword(x,y);
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal><para>5/14/2011: Sky</para></internal>
    public class AuthenticationSelfManagementService : XActLibServiceBase, IAuthenticationSelfManagementService
    {
        private readonly IPrincipalService _principalService;
        private readonly IUserMembershipInfoManagementService _userMembershipInfoManagementService;


        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationSelfManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="userMembershipInfoManagementService">The user membership information management service.</param>
        /// <internal>5/14/2011: Sky</internal>
        public AuthenticationSelfManagementService(ITracingService tracingService,
            IPrincipalService principalService,
            IUserMembershipInfoManagementService userMembershipInfoManagementService):base(tracingService)
        {
            tracingService.ValidateIsNotDefault("tracingService");


            _principalService = principalService;
            _userMembershipInfoManagementService = userMembershipInfoManagementService;
        }


        /// <summary>
        /// Changes the currently logged in user's password.
        /// </summary>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <internal>5/13/2011: Sky</internal>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public void ChangePassword(string oldPassword, string newPassword)
        {
            _tracingService.Trace(TraceLevel.Info,"{0} is changing their own password.".FormatStringCurrentCulture(this.GetIdentityIdentifier()));

            _userMembershipInfoManagementService.ChangePassword(this.GetIdentityIdentifier(), oldPassword, newPassword);
        }

        /// <summary>
        /// Changes the currently logged in user's challenge question and answer.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="newChallengeQuestion">The new challenge question.</param>
        /// <param name="newChallengeAnswer">The new challenge answer.</param>
        /// <internal>5/13/2011: Sky</internal>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public void ChangePasswordQuestionAndAnswer(string password, string newChallengeQuestion,
                                                    string newChallengeAnswer)
        {
            _tracingService.Trace(TraceLevel.Info, "{0} is changing their own password challenge question and answer.".FormatStringCurrentCulture(this.GetIdentityIdentifier()));

            _userMembershipInfoManagementService.ChangeChallengeQuestionAndAnswer(this.GetIdentityIdentifier(), newChallengeQuestion, newChallengeAnswer);
        }

        
        /// <summary>
        /// Gets the thread's IIdentity name.
        /// <para>
        /// In other words: <c>Thread.CurrentPrincipal.Identity.Name</c>
        /// </para>
        /// </summary>
        /// <value>The name of the user.</value>
        /// <internal><para>5/14/2011: Sky</para></internal>
        private string GetIdentityIdentifier()
        {
            return _principalService.CurrentIdentityIdentifier; 
        }
    }
}