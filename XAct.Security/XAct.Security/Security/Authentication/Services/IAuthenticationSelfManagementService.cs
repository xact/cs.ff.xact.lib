﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authentication
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// The contract for a reduced surface (ie lower risk) service 
    /// that an end user can use to change his own password 
    /// or challenge question/answer,  without exposing too many 
    /// methods that give access to updating other users.
    /// </summary>
    /// <internal><para>5/14/2011: Sky</para></internal>
    public interface IAuthenticationSelfManagementService : IHasXActLibService
    {
        /// <summary>
        /// Changes the currently logged in user's password.
        /// </summary>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <internal><para>5/13/2011: Sky</para></internal>
        void ChangePassword(string oldPassword, string newPassword);

        /// <summary>
        /// Changes the password question and answer.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="newChallengeQuestion">The new challenge question.</param>
        /// <param name="newChallengeAnswer">The new challenge answer.</param>
        /// <internal><para>5/13/2011: Sky</para></internal>
        void ChangePasswordQuestionAndAnswer(string password, string newChallengeQuestion,
                                             string newChallengeAnswer);
    }
}