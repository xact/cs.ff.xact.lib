﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authentication
// ReSharper restore CheckNamespace
{
    using System.Security.Principal;

    /// <summary>
    /// An contract for an authentication service.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Notice how this contract exposes only methos required to authenticate/login a user,
    /// and log them back out. 
    /// </para>
    /// <para>
    /// For a contract for a service to manage users, see
    /// <see cref="IAuthenticationSelfManagementService"/>,
    /// and 
    /// <see cref="IAuthenticationManagementService"/>.
    /// </para>
    /// </remarks>
    /// <internal><para>5/14/2011: Sky</para></internal>
    public interface IAuthenticationService : IHasXActLibService
    {
        /// <summary>
        /// Gets the current Thread Principal, 
        /// from which one can derive the Identity as needed.
        /// </summary>
        /// <value>The current.</value>
        /// <internal><para>8/10/2011: Sky</para></internal>
        IPrincipal CurrentPrincipal { get; }

        /// <summary>
        /// Logins the specified username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// <internal><para>5/14/2011: Sky</para></internal>
        bool Login(string username, string password);


        /// <summary>
        /// Logouts the current identity.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Implementations should log which username is logging out.
        /// </para>
        /// </remarks>
        /// <internal><para>5/14/2011: Sky</para></internal>
        void Logout();

        //Same already available from Principle.User
        //bool IsAuthenticated();
    }
}