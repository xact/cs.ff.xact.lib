﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authentication
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for a Service to manage an Authentication store.
    /// <para>
    /// Implemented by <c>MembershipProviderAuthenticationService</c> (in
    /// <c>XAct.Security.Web</c>).  Start with a service that wraps
    /// an authentication store you know -- and then move to WIF or other
    /// more robust solution.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Not to be confused with <see cref="IAuthenticationService"/>
    /// which is used to do the actual authentication -- this just
    /// manages users.
    /// </para>
    /// </remarks>
    public interface IAuthenticationManagementService : IHasXActLibService
    {


        /// <summary>
        /// Creates the member/user from the given minimal information on the user.
        /// <para>
        /// Note that if <see cref="IAuthenticationIdentityInfo{TId}.IsApproved"/> is not set, it defaults to <c>false</c>.
        /// </para>
        /// </summary>
        /// <typeparam name="TId">The type of the data.</typeparam>
        /// <param name="identityInfo">The membership info.</param>
        /// <returns></returns>
        /// <internal><para>5/14/2011: Sky</para></internal>
        void CreateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo);


        /// <summary>
        /// Updates the given member/user.
        /// <para>
        /// Updates 
        /// <see cref="IAuthenticationIdentityInfo{TId}.Name"/>,
        /// <see cref="IAuthenticationIdentityInfo{TId}.EmailAddress"/>,
        /// <see cref="IAuthenticationIdentityInfo{TId}.Comment"/>,
        /// <see cref="IAuthenticationIdentityInfo{TId}.IsApproved"/>.
        /// </para>
        /// <para>
        /// One should not be updating any traceability fields (CreatedOn, LastEditedOn, etc.)
        /// </para>
        /// <para>
        /// Use <see cref="UnlockMember{TId}"/> to unlock a user.
        /// </para>
        /// <para>
        /// Use <see cref="ChangeChallengeQuestionAndAnswer"/> to change challenge Question and Answer.
        /// </para>
        /// <para>
        /// Use <see cref="ChangePassword"/> to change the user's password.
        /// </para>
        /// </summary>
        /// <typeparam name="TId">The type of the id.</typeparam>
        /// <param name="identityInfo">The membership info.</param>
        /// <internal><para>5/15/2011: Sky</para></internal>
        void UpdateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo);


        
        /// <summary>
        /// Deletes the specified member/user.
        /// </summary>
        /// <param name="identityName">Name of the member/user.</param>
        /// <returns></returns>
        /// <internal><para>5/13/2011: Sky</para></internal>
        void DeleteMember(string identityName);

    }
}