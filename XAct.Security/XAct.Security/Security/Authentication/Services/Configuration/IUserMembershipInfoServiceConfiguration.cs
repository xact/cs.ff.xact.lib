﻿namespace XAct.Security.Authentication
{
    using System;

    public interface IUserMembershipInfoManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// If true, will enable user lockout when users are created
        /// </summary>
        bool UserLockoutEnabledByDefault { get; set; }

        /// <summary>
        /// Number of access attempts allowed before a user is locked out (if lockout is enabled)
        /// </summary>
        int MaxFailedAccessAttemptsBeforeLockout { get; set; }


        /// <summary>
        /// Default amount of time that a user is locked out for after 
        /// <see cref="MaxFailedAccessAttemptsBeforeLockout"/> is reached.
        /// <para>
        /// The Default time should be something short (eg: 5 minutes)
        /// </para>
        /// <para>
        /// Permanet lockout just raises operational costs for no specific 
        /// gain that a timeout (and auditing) does not achieve.
        /// </para>
        /// </summary>
        TimeSpan DefaultAccountLockoutTimeSpan { get; set; }



        int MinRequiredPasswordLength { get; set; }

        int MinRequiredNonAlphanumericCharacters { get; set; }
        
        /// <summary>
        /// Salt used to hash passwords.
        /// </summary>
        string Salt { get; set; }

    }
}