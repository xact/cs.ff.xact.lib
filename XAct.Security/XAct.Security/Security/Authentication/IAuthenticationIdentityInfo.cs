﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authentication
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Authentication;

    /// <summary>
    /// The contract for the encapsulation of a Membership User.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The ASP.NET Membership provider's MembershipInfo has a good schema
    /// for the minimal description of a User. 
    /// </para>
    /// <para>
    /// Unfortunately, it's tied to the
    /// ASP.NET mechanism.
    /// </para>
    /// <para>
    /// Indirection from it, protects oneself from TechRot.
    /// </para>
    /// <para>
    /// This vendor neutral object is returned by 
    /// <see cref="IAuthenticationManagementService"/>
    /// </para>
    /// </remarks>
    /// <internal><para>5/14/2011: Sky</para></internal>
    public interface IAuthenticationIdentityInfo<TId> : IHasId<TId>,
                                                        IUserIdentity,
                                                        IHasUserNameAndPassword,
                                                        IAuthenticationChallengeInformation,
                                                        IHasDateTimeCreatedOnUtcReadOnly
    {
    }


    public interface IAuthenticationChallengeInformation
    {
        /// <summary>
        /// The question used to verify the user is whom he says he is.
        /// </summary>
        string ChallengeQuestion { get; set; }

        /// <summary>
        /// The answer to the challenge question.
        /// </summary>
        string ChallengeAnswer { get; set; }

    }

    public interface IUserIdentity : IHasDateTimeCreatedOnUtc    {
        /// <summary>
        /// Gets the date the User was created on.
        /// <para>
        /// NOte: will be null if the user has never been persisted.
        /// </para>
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        /// <internal>
        /// Note: this property name matches exactly the IAuditable property.
        /// </internal>
        /// <value>The created on.</value>
        /// <internal><para>5/15/2011: Sky</para></internal>
        new DateTime? CreatedOnUtc { get; set; }

        /// <summary>
        /// The user is approved.
        /// <para>
        /// Note: value will be null if the user has never been persisted.
        /// </para>
        /// </summary>
        bool? IsApproved { get; set; }

        
        /// <summary>
        /// The email address for the user.
        /// </summary>
        string EmailAddress { get; set; }





        /// <summary>
        /// Gets or sets the comment/note about the user.
        /// </summary>
        /// <value>The comment.</value>
        /// <internal><para>5/15/2011: Sky</para></internal>
        string Comment { get; set; }
        
    }

    public interface IIMembershipInfo : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasEnabled, 
        IHasUserIdentifier,
        IAuthenticationChallengeInformation    
    {
        /// <summary>
        /// 
        /// </summary>
        //string UserName { get; set; }
        
        string EmailAddress { get; set; }
        bool EmailConfirmed { get; set; }

        string SecurityStamp { get; set; }

        /// <summary>
        /// Commonly will be cellphone where a text message is sent.
        /// </summary>
        string TwoFactorInformation { get; set; }
        bool TwoFactorInformationConfirmed { get; set; }


        DateTime? LockoutEndedDateTimeUtc { get; set; }
        int LoginFailedAttemptsCount { get; set; }

        /// <summary>
        /// Hash of User's pasword.
        /// </summary>
        string PasswordHash { get; set; }
    }
}