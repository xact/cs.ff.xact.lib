﻿namespace XAct.Tests.Services.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Security.Authentication;
    using XAct.Users;



    public class RepositoryBasedAuthenticationManagementService : IAuthenticationManagementService
    {
        private readonly ITracingService _tracingService;
        private readonly IRepositoryService _repositoryService;
        private readonly IUserManagementService _userManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBasedAuthenticationManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="userManagementService">The user management service.</param>
        public RepositoryBasedAuthenticationManagementService(
            ITracingService tracingService, 
            IRepositoryService repositoryService,
            IUserManagementService userManagementService)
        {
            _tracingService = tracingService;
            _repositoryService = repositoryService;
            _userManagementService = userManagementService;
        }


        public void CreateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
        {
            throw new Exception("TODO");
            //User user = new User();
            //_userManagementService.PersistOnCommit(user);
        }

        public void UpdateMember<TId>(IAuthenticationIdentityInfo<TId> identityInfo)
        {
            throw new System.NotImplementedException();
        }


        public void DeleteMember(string identityName)
        {
            _userManagementService.DeleteOnCommit(identityName);

        }


        private User GetUserByName(string name)
        {
            return _userManagementService.GetByName(name);
        }

    }
}