﻿//// ReSharper disable CheckNamespace
//namespace XAct.Security.Authentication
//// ReSharper restore CheckNamespace
//{
//    using System;



//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="TId"></typeparam>
//    public interface IAuthenticationMemberStatusInfo<TId> : IAuthenticationIdentityInfo<TId>, IHasAuditability
//        where TId:struct
//    {
//        /// <summary>
//        /// Gets a value indicating whether this member is locked out.
//        /// </summary>
//        /// <value>
//        /// 	<c>true</c> if this instance is locked out; otherwise, <c>false</c>.
//        /// </value>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        bool IsLockedOut { get; }

//        /// <summary>
//        /// Gets the last lockout date.
//        /// <para>
//        /// This field is ignored by 
//        /// <see cref="IAuthenticationManagementService.UpdateMember"/>
//        /// </para>
//        /// </summary>
//        /// <value>The last lockout date.</value>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        DateTime LastLockoutDate { get; set; }

//        /// <summary>
//        /// Gets or sets the date user last loged in .
//        /// </summary>
//        /// <value>The last login date.</value>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        DateTime LastLoginDate { get; set; }

//        /// <summary>
//        /// Gets or sets the last activity date.
//        /// </summary>
//        /// <value>The last activity date.</value>
//        /// <internal><para>5/15/2011: Sky</para></internal>
//        DateTime LastActivityDate { get; set; }

//        //public bool IsOnline { get; }
//    }
//}