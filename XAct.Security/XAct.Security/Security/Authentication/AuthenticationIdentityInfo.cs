﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

// ReSharper disable CheckNamespace
namespace XAct.Security.Authentication
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Authentication;

    /// <summary>
    /// An implementation of <see cref="IAuthenticationIdentityInfo{TId}"/>.
    /// <para>
    /// Use this with an implementation of <see cref="IAuthenticationManagementService"/>
    /// to create or update users.
    /// </para>
    /// </summary>
    /// <internal><para>5/14/2011: Sky</para></internal>
    public class AuthenticationIdentityInfo<TId> : IAuthenticationIdentityInfo<TId>, IHasUserNameAndPassword
    {

        /// <summary>
        /// Gets or sets the id.
        /// <para>
        /// This property is *not* updatable by the <see cref="IAuthenticationManagementService"/>.
        /// </para>
        /// </summary>
        /// <value>The id.</value>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public virtual TId Id { get; set; }


        /// <summary>
        /// Gets the date the User was created on.
        /// <para>
        /// NOte: will be null if the user has never been persisted.
        /// </para>
        /// <para>
        /// This property is *not* updatable by the <see cref="IAuthenticationManagementService"/>.
        /// </para>
        /// </summary>
        /// <internal>
        /// Note: this property name matches exactly the IAuditable property.
        /// </internal>
        /// <value>The created on.</value>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public virtual DateTime? CreatedOnUtc { get; set; }


        /// <summary>
        /// The user is approved.
        /// <para>
        /// NOte: will be null if the user has never been persisted.
        /// </para>
        /// <para>
        /// This property is updatable via <see cref="IAuthenticationManagementService.UpdateMember{TId}"/>
        /// </para>
        /// </summary>
        /// <value></value>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public virtual bool? IsApproved { get; set; }

        /// <summary>
        /// The name of the user (the login name).
        /// <para>
        /// This property is *not* updatable by the <see cref="IAuthenticationManagementService"/>.
        /// </para>
        /// </summary>
        /// <value></value>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public virtual string Name { get; set; }

        /// <summary>
        /// The login password of the user.
        /// <para>
        /// Note that this is only used to create a new user -- it is always null when returned from the datastore.
        /// </para>
        /// <para>
        /// This property is updatable via <see cref="IAuthenticationManagementService.ChangePassword"/>
        /// </para>
        /// </summary>
        public virtual string Password { get; set; }

        /// <summary>
        /// The email address for the user.
        /// <para>
        /// This property is updatable via <see cref="IAuthenticationManagementService.UpdateMember{TId}"/>
        /// </para>
        /// </summary>
        /// <value></value>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public virtual string EmailAddress { get; set; }

        /// <summary>
        /// The question used to verify the user is whom he says he is.
        /// <para>
        /// Note that this is only used to create a new user -- it is always null when returned from the datastore.
        /// </para>
        /// <para>
        /// This property is updatable via <see cref="IAuthenticationManagementService.ChangeChallengeQuestionAndAnswer"/>
        /// </para>
        /// </summary>
        /// <value></value>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public virtual string ChallengeQuestion { get; set; }

        /// <summary>
        /// The answer to the challenge question.
        /// <para>
        /// Note that this is only used to create a new user -- it is always null when returned from the datastore.
        /// </para>
        /// <para>
        /// This property is updatable via <see cref="IAuthenticationManagementService.ChangeChallengeQuestionAndAnswer"/>
        /// </para>
        /// </summary>
        /// <value></value>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public virtual string ChallengeAnswer { get; set; }


        /// <summary>
        /// Gets or sets the comment.
        /// <para>
        /// This property is updatable via <see cref="IAuthenticationManagementService.UpdateMember{TId}"/>
        /// </para>
        /// </summary>
        /// <value>The comment.</value>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public virtual string Comment { get; set; }

    }
}