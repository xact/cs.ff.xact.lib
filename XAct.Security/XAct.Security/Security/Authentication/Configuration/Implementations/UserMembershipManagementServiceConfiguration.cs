namespace XAct.Users.Services.Configuration.Implementations
{
    using System;

    /// <summary>
    /// A singleton implementation of 
    /// <see cref="IUserMembershipManagementServiceConfiguration"/>
    /// to configure <see cref="IUserManagementService{TUser}"/>
    /// </summary>
    public class UserManagementServiceConfiguration : IUserMembershipManagementServiceConfiguration
    {
        /// <summary>
        /// If true, will enable user lockout when users are created
        /// </summary>
        public bool UserLockoutEnabledByDefault { get; set; }
        /// <summary>
        /// Number of access attempts allowed before a user is locked out (if lockout is enabled)
        /// </summary>
        public int MaxFailedAccessAttemptsBeforeLockout { get; set; }
        /// <summary>
        /// Default amount of time that a user is locked out for after
        /// <see cref="MaxFailedAccessAttemptsBeforeLockout" /> is reached.
        /// <para>
        /// The Default time should be something short (eg: 5 minutes)
        /// </para>
        /// <para>
        /// Permanet lockout just raises operational costs for no specific
        /// gain that a timeout (and auditing) does not achieve.
        /// </para>
        /// </summary>
        public TimeSpan DefaultAccountLockoutTimeSpan { get; set; }
    }
}