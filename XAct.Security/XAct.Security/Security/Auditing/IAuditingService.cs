﻿namespace XAct.Security
{
    using System.Diagnostics;

    /// <summary>
    /// 
    /// <para>
    /// One difintion of the difference between Auditing and Logging.
    /// Auditing is used to answer the question "Who did what?" and possibly why. 
    /// Logging is more focussed on what's happening.
    /// </para>
    /// </summary>
    public interface IAuditingService : IHasXActLibService
    {
        void Log(TraceLevel traceLevel, string operationIdentifier, string message);
    }
}
