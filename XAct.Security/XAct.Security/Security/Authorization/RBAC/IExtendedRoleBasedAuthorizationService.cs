﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.RBAC
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using XAct.Settings;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TMemberIds">The type of the member ids.</typeparam>
    internal interface IExtendedRoleBasedAuthorizationService<TMemberIds> : IHasXActLibService
    {
        /// <summary>
        /// Gets the Ids of the Members who are owners of the Role.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns></returns>
        List<TMemberIds> GetRoleOwners(string roleName);


        /// <summary>
        /// Defines the member as a role owner.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <param name="memberName">Name of the member.</param>
        void DefineMemberAsRoleSettingsAdministrator(string roleName, string memberName);


        /// <summary>
        /// Removes the member from the list of role administrators.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <param name="memberName">Name of the member.</param>
        void UndefineMemberAsRoleSettingsAdministrator(string roleName, string memberName);


        /// <summary>
        /// Creates or updates a Setting for a role.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <param name="setting">The setting.</param>
        void SetRoleSetting<TValue>(string roleName, ISetting<TValue> setting);

        /// <summary>
        /// Clears the Setting value for the specified Role.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <param name="setting">The setting.</param>
        void ClearRoleSetting<TValue>(string roleName, ISetting<TValue> setting);


        /// <summary>
        /// Gets a list of the Roles whome have this Setting name defined,
        /// in the order of precedence.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        List<string> GetSettingRolesNames(ISetting setting);
    }
}