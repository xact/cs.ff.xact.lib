﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.RBAC
// ReSharper restore CheckNamespace
{

    /*
    /// <internal>
    /// <para>
    /// IRoleBasedAuthorizationService is part of XAct.Core 
    /// as it is needed for <c>SettingBase{TValue}</c>,
    /// but <c>IRoleBasedAuthorizationManagementService</c>
    /// is left in XAct.Security as it isn't needed for most apps.
    /// </para>
    /// </internal>
     */

    /// <summary>
    /// Contract for a Role based Authentication Service.
    /// <para>
    /// Implemented by 
    /// <c>RoleProviderBasedAuthorizationService</c>
    /// </para>
    /// </summary>
    /// <internal><para>5/9/2011: Sky</para></internal>
    public interface IRoleBasedAuthorizationService : IHasXActLibService
    {
        /// <summary>
        /// Determines whether the user is in the specified roles.
        /// </summary>
        /// <param name="roleNames">The role names.</param>
        /// <returns>
        /// 	<c>true</c> if [is in role] [the specified role names]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal>5/9/2011: Sky</internal>
        bool IsInRole(params string[] roleNames);

        /// <summary>
        /// Determines whether the current user is in one of the roles
        /// defined in the csv string (';|,').
        /// </summary>
        /// <param name="roleNames">The role names.</param>
        /// <param name="interpretEmptyStringAsAllowed">if set to <c>true</c> returns true if the roleNames string is empty.</param>
        /// <returns>
        /// 	<c>true</c> if [is in role] [the specified role names]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        bool IsInRole(string roleNames, bool interpretEmptyStringAsAllowed = false);
    }
}