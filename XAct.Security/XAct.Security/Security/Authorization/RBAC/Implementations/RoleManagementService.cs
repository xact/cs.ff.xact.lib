﻿namespace XAct.Security.Authorization.RBAC.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Expressions;
    using XAct.Security.Authorization.RBAC;
    using XAct.Security.Entities;

    public class RoleManagementService : ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase<Role>,
                                         IRoleManagementService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public RoleManagementService(
            ITracingService tracingService,
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {
        }

        public bool Exists(string code)
        {
            var applicationTennantId = _applicationTennantService.Get();

            var result =
                base.Exists(x => x.ApplicationTennantId == applicationTennantId && x.Name == code && x.Deleted == false);

            return result;
        }

        public Role GetById(Guid id, bool withParent = false, bool withChildren = false)
        {
            var applicationTennantId = _applicationTennantService.Get();


            List<Expression<Func<Role,object>>> includes = 
                new List<Expression<Func<Role, object>>>();
            
            if (withParent)
            {
                includes.Add(x=>x.Parent);
            }
            if (withChildren)
            {
                //includes.Add(x=>x.Children);
            }
            
            Role result;

            if (includes.Count>0)
            {
                result =
                    _repositoryService.GetSingle<Role>(
                        x => x.ApplicationTennantId == applicationTennantId
                             && x.Id == id
                             && x.Deleted==false,
                        new IncludeSpecification<Role>(includes.ToArray())
                        );
            }
            else
            {
                result =
                    _repositoryService.GetSingle<Role>(
                        x => x.ApplicationTennantId == applicationTennantId
                             && x.Id == id
                             && x.Deleted == false);
            }

            return result;
        }


        public Role GetByKey(string code, bool withParent = false, bool withChildren = false)
        {
            var applicationTennantId = _applicationTennantService.Get();

            List<Expression<Func<Role, object>>> includes =
                new List<Expression<Func<Role, object>>>();

            if (withParent)
            {
                includes.Add(x => x.Parent);
            }
            if (withChildren)
            {
                //includes.Add(x=>x.Children);
            }

            Role result;

            if (includes.Count > 0)
            {
                result =
                    _repositoryService.GetSingle<Role>(
                        x => x.ApplicationTennantId == applicationTennantId 
                        && x.Name == code
                        && x.Deleted == false,
                        new IncludeSpecification<Role>(includes.ToArray())
                        );
            }
            else
            {
                result =
                    base.Get(x => x.ApplicationTennantId == applicationTennantId && x.Name == code && x.Deleted == false);
            }
            return result;
        }


        public IQueryable<Role> GetRoles(bool? enabled = null)
        {
            var applicationTennantId = _applicationTennantService.Get();


            var result =
                (!enabled.HasValue)
                    ? base.Retrieve(x => x.ApplicationTennantId == applicationTennantId && x.Deleted == false)
                    : base.Retrieve(x => x.ApplicationTennantId == applicationTennantId && x.Enabled == enabled.Value && x.Deleted == false);
            return result;
        }


        public bool CreateRole(string roleUniqueName, bool enabled = true)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            if (Exists(roleUniqueName))
            {
                return false;
            }

            var role = new Role
            {
                ApplicationTennantId = applicationTennantId,
                Enabled = enabled,
                Name = roleUniqueName
            };
            _repositoryService.AddOnCommit(role);
            return true;
        }

        public bool CreateRole(string roleUniqueName, bool enabled, out Role role)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            role = Get(x => x.Name == roleUniqueName);

            if (role != null)
            {
                return false;
            }

            role = new Role
                {
                    ApplicationTennantId = applicationTennantId,
                    Enabled = enabled,
                    Name = roleUniqueName
                };
            _repositoryService.AddOnCommit(role);
            return true;
        }

        public void DeleteRole(string roleUniqueName)
        {


            var role = GetByKey(roleUniqueName);
            if (role == null)
            {
                return;
            }
            role.Deleted = true;
            _repositoryService.PersistOnCommit<Role,Guid>(role);

            //Guid applicationTennantId = _applicationTennantService.Get();
            //_repositoryService.DeleteOnCommit<Role>(x => x.ApplicationTennantId == applicationTennantId
            //    &&
            //    x.Name == roleUniqueName);
        }

        public bool? GetEnabledState(string roleUniqueName)
        {
            Guid applicationTennantId = _applicationTennantService.Get();
            var role = _repositoryService.GetSingle<Role>(
                x => x.ApplicationTennantId == applicationTennantId
                        && x.Deleted == false
                        && x.Name == roleUniqueName);
            if (role == null)
            {
                return null;
            }
            return role.Enabled;
        }


        public void ChangeEnabledState(string roleUniqueName, bool enabled)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            var role = _repositoryService.GetSingle<Role>(x => 
                x.ApplicationTennantId == applicationTennantId
                && x.Deleted == false
                && x.Name == roleUniqueName);
            if (role == null)
            {
                return;
            }
            role.Enabled = enabled;
            _repositoryService.UpdateOnCommit(role);
        }

        public void RenameRole(string roleUniqueName, string newroleUniqueName)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            var role = _repositoryService.GetSingle<Role>(x => x.ApplicationTennantId == applicationTennantId && x.Deleted == false &&  x.Name == roleUniqueName);
            if (role == null)
            {
                return;
            }
            role.Name = newroleUniqueName;
            _repositoryService.UpdateOnCommit(role);
        }


        public bool IsUserInRole(string name, string roleUniqueName)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            var result = _repositoryService.GetSingle<UserToRole>(
                x=>x.ApplicationTennantId == applicationTennantId
                && x.UserIdentifier == name
                && x.Role.Deleted==false
               && x.Role.Name == roleUniqueName
                , new IncludeSpecification<UserToRole>(y=>y.Role)
                );

            return result!=null;
        }

        public bool AddUserToRole(string userIdentifier, string roleUniqueName)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            if (IsUserInRole(userIdentifier, roleUniqueName))
            {
                return false;
            }
            Role role = GetByKey(roleUniqueName);

            if (role == null)
            {
                throw new ArgumentException("Cannot Add user to a role that does not exist.");
            }
            var result = new UserToRole
                {
                    ApplicationTennantId = applicationTennantId,
                    RoleFK = role.Id,
                    UserIdentifier = userIdentifier
                };

            _repositoryService.AddOnCommit(result);
            return true;
        }

        public bool RemoveUserFromRoleRole(string userIdentifier, string roleUniqueName)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            if (!IsUserInRole(userIdentifier, roleUniqueName))
            {
                return false;
            }

            Role role = GetByKey(roleUniqueName);

            _repositoryService.DeleteOnCommit<UserToRole>(
                x =>
                x.ApplicationTennantId == applicationTennantId
                && x.UserIdentifier == userIdentifier
                && x.Role.Deleted == false
                && x.RoleFK==role.Id);

            return true;
        }

        public string[] GetUsersRoles(string userIdentifier, bool? enabled = true)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            Expression<Func<UserToRole, bool>> predicate =
                x => x.ApplicationTennantId == applicationTennantId && x.Role.Deleted == false
&& x.UserIdentifier == userIdentifier;

            if (enabled.HasValue)
            {
                predicate = PredicateBuilder.And<UserToRole>(predicate,
                                                             x => x.Role.Enabled == enabled.Value);
            }

            string[] result = _repositoryService.GetByFilter<UserToRole>(predicate,
                                                                         new IncludeSpecification<UserToRole>(x => x.Role))
                                                .Select(x => x.Role.Name)
                                                .ToArray();

            return result;

        }

        public int GetCountOfUsersInRole(string roleUniqueName)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            var result = _repositoryService.Count<UserToRole>(x => 
                x.ApplicationTennantId == applicationTennantId
                &&  x.Role.Name == roleUniqueName
                && x.Role.Deleted == false
                );

            return result;
        }

        public string[] GetUserNamesInRole(params string[] roleUniqueNames)
        {
            Guid applicationTennantId = _applicationTennantService.Get();

            string[] result;
            if (roleUniqueNames.Length == 1)
            {
                string roleUniqueName = roleUniqueNames[0];

                result = _repositoryService.GetByFilter<UserToRole>(x =>
                                                                    x.ApplicationTennantId == applicationTennantId
                                                                    && x.Role.Deleted == false
                                                                    && x.Role.Name == roleUniqueName)
                                           .Select(x => x.UserIdentifier)
                                           .ToArray();
            }
            else
            {
                result = _repositoryService.GetByFilter<UserToRole>(x =>
                                                                    x.ApplicationTennantId == applicationTennantId
                                                                    && x.Role.Deleted == false
                                                                    && roleUniqueNames.Contains(x.Role.Name))
                                           .Select(x => x.UserIdentifier)
                                           .ToArray();
            }
            return result;
        }

    }
}