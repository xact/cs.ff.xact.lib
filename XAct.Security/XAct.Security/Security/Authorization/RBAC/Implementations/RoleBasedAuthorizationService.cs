﻿namespace XAct.Security.Authorization.RBAC
{
    using System;
    using System.Linq;
    using XAct.Environment;

    public class RoleBasedAuthorizationService : IRoleBasedAuthorizationService
    {
        private readonly IPrincipalService _principalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleBasedAuthorizationService" /> class.
        /// </summary>
        /// <param name="principalService">The principal service.</param>
        public RoleBasedAuthorizationService(
            IPrincipalService principalService
            )
        {
            _principalService = principalService;
        }

        public bool IsInRole(params string[] roleNames)
        {
            return roleNames.Any(x => _principalService.Principal.IsInRole(x));
        }

        /// <summary>
        /// Determines whether the current user is in one of the roles
        /// defined in the csv string (';|,').
        /// </summary>
        /// <param name="roleNames">The role names.</param>
        /// <param name="interpretEmptyStringAsAllowed">if set to <c>true</c> returns true if the roleNames string is empty.</param>
        /// <returns>
        ///   <c>true</c> if [is in role] [the specified role names]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal>8/15/2011: Sky</internal>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool IsInRole(string roleNames, bool interpretEmptyStringAsAllowed = false)
        {
            if (roleNames.IsNullOrEmpty() && interpretEmptyStringAsAllowed)
            {
                return true;
            }
            string[] roles = roleNames.Split(new char[] { ';', '|', ',' }, StringSplitOptions.RemoveEmptyEntries);

            return IsInRole(roles);
        }
    }
}