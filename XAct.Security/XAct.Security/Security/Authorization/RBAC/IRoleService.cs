//namespace XAct.Services
//{
//    public interface IRoleService :IHasXActLibService
//    {
        
//    }



//    // Type: Microsoft.AspNet.Identity.UserManager`2
//// Assembly: Microsoft.AspNet.Identity.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
//// Assembly location: Z:\Dropbox\Code\Spikes\XAct.Spikes.AspIdentity2_2F\XAct.Spikes.AspIdentity2_2F\bin\Microsoft.AspNet.Identity.Core.dll

//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using System.Runtime.CompilerServices;
//using System.Security.Claims;
//using System.Threading.Tasks;

//namespace Microsoft.AspNet.Identity
//{
//  /// <summary>
//  /// Exposes user related api which will automatically save changes to the UserStore
//  /// 
//  /// </summary>
//  /// <typeparam name="TUser"/><typeparam name="TKey"/>
//  public class UserManager<TUser, TKey> : IDisposable where TUser : class, IUser<TKey> where TKey : IEquatable<TKey>
//  {
//    private readonly Dictionary<string, IUserTokenProvider<TUser, TKey>> _factors = new Dictionary<string, IUserTokenProvider<TUser, TKey>>();
//    private TimeSpan _defaultLockout = TimeSpan.Zero;
//    private IClaimsIdentityFactory<TUser, TKey> _claimsFactory;
//    private bool _disposed;
//    private IPasswordHasher _passwordHasher;
//    private IIdentityValidator<string> _passwordValidator;
//    private IIdentityValidator<TUser> _userValidator;

//    /// <summary>
//    /// Persistence abstraction that the UserManager operates against
//    /// 
//    /// </summary>
//    protected internal IUserStore<TUser, TKey> Store { get; set; }

//    /// <summary>
//    /// Used to hash/verify passwords
//    /// 
//    /// </summary>
//    public IPasswordHasher PasswordHasher
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this._passwordHasher;
//      }
//      set
//      {
//        this.ThrowIfDisposed();
//        if (value == null)
//          throw new ArgumentNullException("value");
//        this._passwordHasher = value;
//      }
//    }

//    /// <summary>
//    /// Used to validate users before changes are saved
//    /// 
//    /// </summary>
//    public IIdentityValidator<TUser> UserValidator
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this._userValidator;
//      }
//      set
//      {
//        this.ThrowIfDisposed();
//        if (value == null)
//          throw new ArgumentNullException("value");
//        this._userValidator = value;
//      }
//    }

//    /// <summary>
//    /// Used to validate passwords before persisting changes
//    /// 
//    /// </summary>
//    public IIdentityValidator<string> PasswordValidator
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this._passwordValidator;
//      }
//      set
//      {
//        this.ThrowIfDisposed();
//        if (value == null)
//          throw new ArgumentNullException("value");
//        this._passwordValidator = value;
//      }
//    }

//    /// <summary>
//    /// Used to create claims identities from users
//    /// 
//    /// </summary>
//    public IClaimsIdentityFactory<TUser, TKey> ClaimsIdentityFactory
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this._claimsFactory;
//      }
//      set
//      {
//        this.ThrowIfDisposed();
//        if (value == null)
//          throw new ArgumentNullException("value");
//        this._claimsFactory = value;
//      }
//    }

//    /// <summary>
//    /// Used to send email
//    /// 
//    /// </summary>
//    public IUserManagementMessageService EmailService { get; set; }

//    /// <summary>
//    /// Used to send a sms message
//    /// 
//    /// </summary>
//    public IUserManagementMessageService SmsService { get; set; }

//    /// <summary>
//    /// Used for generating reset password and confirmation tokens
//    /// 
//    /// </summary>
//    public IUserTokenProvider<TUser, TKey> UserTokenProvider { get; set; }

//    /// <summary>
//    /// If true, will enable user lockout when users are created
//    /// 
//    /// </summary>
//    public bool UserLockoutEnabledByDefault { get; set; }

//    /// <summary>
//    /// Number of access attempts allowed before a user is locked out (if lockout is enabled)
//    /// 
//    /// </summary>
//    public int MaxFailedAccessAttemptsBeforeLockout { get; set; }

//    /// <summary>
//    /// Default amount of time that a user is locked out for after MaxFailedAccessAttemptsBeforeLockout is reached
//    /// 
//    /// </summary>
//    public TimeSpan DefaultAccountLockoutTimeSpan
//    {
//      get
//      {
//        return this._defaultLockout;
//      }
//      set
//      {
//        this._defaultLockout = value;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserTwoFactorStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserTwoFactor
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserTwoFactorStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserPasswordStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserPassword
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserPasswordStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserSecurityStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserSecurityStamp
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserSecurityStampStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserRoleStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserRole
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserRoleStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserLoginStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserLogin
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserLoginStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserEmailStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserEmail
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserEmailStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserPhoneNumberStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserPhoneNumber
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserPhoneNumberStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserClaimStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserClaim
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserClaimStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IUserLockoutStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsUserLockout
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IUserLockoutStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns true if the store is an IQueryableUserStore
//    /// 
//    /// </summary>
//    public virtual bool SupportsQueryableUsers
//    {
//      get
//      {
//        this.ThrowIfDisposed();
//        return this.Store is IQueryableUserStore<TUser, TKey>;
//      }
//    }

//    /// <summary>
//    /// Returns an IQueryable of users if the store is an IQueryableUserStore
//    /// 
//    /// </summary>
//    public virtual IQueryable<TUser> Users
//    {
//      get
//      {
//        IQueryableUserStore<TUser, TKey> queryableUserStore = this.Store as IQueryableUserStore<TUser, TKey>;
//        if (queryableUserStore == null)
//          throw new NotSupportedException(Resources.StoreNotIQueryableUserStore);
//        else
//          return queryableUserStore.Users;
//      }
//    }

//    /// <summary>
//    /// Maps the registered two-factor authentication providers for users by their id
//    /// 
//    /// </summary>
//    public IDictionary<string, IUserTokenProvider<TUser, TKey>> TwoFactorProviders
//    {
//      get
//      {
//        return (IDictionary<string, IUserTokenProvider<TUser, TKey>>) this._factors;
//      }
//    }

//    /// <summary>
//    /// Constructor
//    /// 
//    /// </summary>
//    /// <param name="store">The IUserStore is responsible for commiting changes via the UpdateAsync/CreateAsync methods</param>
//    public UserManager(IUserStore<TUser, TKey> store)
//    {
//      if (store == null)
//        throw new ArgumentNullException("store");
//      this.Store = store;
//      this.UserValidator = (IIdentityValidator<TUser>) new UserValidator<TUser, TKey>(this);
//      this.PasswordValidator = (IIdentityValidator<string>) new MinimumLengthValidator(6);
//      this.PasswordHasher = (IPasswordHasher) new PasswordHasher();
//      this.ClaimsIdentityFactory = (IClaimsIdentityFactory<TUser, TKey>) new ClaimsIdentityFactory<TUser, TKey>();
//    }

//    /// <summary>
//    /// Dispose this object
//    /// 
//    /// </summary>
//    public void Dispose()
//    {
//      this.Dispose(true);
//      GC.SuppressFinalize((object) this);
//    }

//    /// <summary>
//    /// Creates a ClaimsIdentity representing the user
//    /// 
//    /// </summary>
//    /// <param name="user"/><param name="authenticationType"/>
//    /// <returns/>
//    public virtual Task<ClaimsIdentity> CreateIdentityAsync(TUser user, string authenticationType)
//    {
//      this.ThrowIfDisposed();
//      if ((object) user == null)
//        throw new ArgumentNullException("user");
//      else
//        return this.ClaimsIdentityFactory.CreateAsync(this, user, authenticationType);
//    }

//    /// <summary>
//    /// Create a user with no password
//    /// 
//    /// </summary>
//    /// <param name="user"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CCreateAsync\u003Ed__0))]
//    public virtual Task<IdentityResult> CreateAsync(TUser user)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CCreateAsync\u003Ed__0 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.user = user;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CCreateAsync\u003Ed__0>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Update a user
//    /// 
//    /// </summary>
//    /// <param name="user"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CUpdateAsync\u003Ed__5))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> UpdateAsync(TUser user)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CUpdateAsync\u003Ed__5 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.user = user;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CUpdateAsync\u003Ed__5>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Delete a user
//    /// 
//    /// </summary>
//    /// <param name="user"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CDeleteAsync\u003Ed__a))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> DeleteAsync(TUser user)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CDeleteAsync\u003Ed__a stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.user = user;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CDeleteAsync\u003Ed__a>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Find a user by id
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    public virtual Task<TUser> FindByIdAsync(TKey userId)
//    {
//      this.ThrowIfDisposed();
//      return this.Store.FindByIdAsync(userId);
//    }

//    /// <summary>
//    /// Find a user by user name
//    /// 
//    /// </summary>
//    /// <param name="userName"/>
//    /// <returns/>
//    public virtual Task<TUser> FindByNameAsync(string userName)
//    {
//      this.ThrowIfDisposed();
//      if (userName == null)
//        throw new ArgumentNullException("userName");
//      else
//        return this.Store.FindByNameAsync(userName);
//    }

//    private IUserPasswordStore<TUser, TKey> GetPasswordStore()
//    {
//      IUserPasswordStore<TUser, TKey> userPasswordStore = this.Store as IUserPasswordStore<TUser, TKey>;
//      if (userPasswordStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserPasswordStore);
//      else
//        return userPasswordStore;
//    }

//    /// <summary>
//    /// Create a user with the given password
//    /// 
//    /// </summary>
//    /// <param name="user"/><param name="password"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CCreateAsync\u003Ed__d))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> CreateAsync(TUser user, string password)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CCreateAsync\u003Ed__d stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.user = user;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.password = password;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CCreateAsync\u003Ed__d>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Return a user with the specified username and password or null if there is no match.
//    /// 
//    /// </summary>
//    /// <param name="userName"/><param name="password"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CFindAsync\u003Ed__12))]
//    public virtual Task<TUser> FindAsync(string userName, string password)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CFindAsync\u003Ed__12 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userName = userName;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.password = password;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<TUser>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CFindAsync\u003Ed__12>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns true if the password is valid for the user
//    /// 
//    /// </summary>
//    /// <param name="user"/><param name="password"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CCheckPasswordAsync\u003Ed__17))]
//    [DebuggerStepThrough]
//    public virtual Task<bool> CheckPasswordAsync(TUser user, string password)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CCheckPasswordAsync\u003Ed__17 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.user = user;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.password = password;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CCheckPasswordAsync\u003Ed__17>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns true if the user has a password
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CHasPasswordAsync\u003Ed__1b))]
//    public virtual Task<bool> HasPasswordAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CHasPasswordAsync\u003Ed__1b stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CHasPasswordAsync\u003Ed__1b>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Add a user password only if one does not already exist
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="password"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CAddPasswordAsync\u003Ed__21))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> AddPasswordAsync(TKey userId, string password)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CAddPasswordAsync\u003Ed__21 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.password = password;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CAddPasswordAsync\u003Ed__21>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Change a user password
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="currentPassword"/><param name="newPassword"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CChangePasswordAsync\u003Ed__2a))]
//    public virtual Task<IdentityResult> ChangePasswordAsync(TKey userId, string currentPassword, string newPassword)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CChangePasswordAsync\u003Ed__2a stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.currentPassword = currentPassword;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.newPassword = newPassword;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CChangePasswordAsync\u003Ed__2a>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Remove a user's password
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CRemovePasswordAsync\u003Ed__32))]
//    public virtual Task<IdentityResult> RemovePasswordAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CRemovePasswordAsync\u003Ed__32 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CRemovePasswordAsync\u003Ed__32>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    [AsyncStateMachine(typeof (UserManager<,>.\u003CUpdatePasswordInternal\u003Ed__39))]
//    [DebuggerStepThrough]
//    internal Task<IdentityResult> UpdatePasswordInternal(IUserPasswordStore<TUser, TKey> passwordStore, TUser user, string newPassword)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CUpdatePasswordInternal\u003Ed__39 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.passwordStore = passwordStore;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.user = user;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.newPassword = newPassword;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CUpdatePasswordInternal\u003Ed__39>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// By default, retrieves the hashed password from the user store and calls PasswordHasher.VerifyHashPassword
//    /// 
//    /// </summary>
//    /// <param name="store"/><param name="user"/><param name="password"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CVerifyPasswordAsync\u003Ed__3e))]
//    [DebuggerStepThrough]
//    protected virtual Task<bool> VerifyPasswordAsync(IUserPasswordStore<TUser, TKey> store, TUser user, string password)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CVerifyPasswordAsync\u003Ed__3e stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.store = store;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.user = user;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.password = password;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CVerifyPasswordAsync\u003Ed__3e>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    private IUserSecurityStampStore<TUser, TKey> GetSecurityStore()
//    {
//      IUserSecurityStampStore<TUser, TKey> securityStampStore = this.Store as IUserSecurityStampStore<TUser, TKey>;
//      if (securityStampStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserSecurityStampStore);
//      else
//        return securityStampStore;
//    }

//    /// <summary>
//    /// Returns the current security stamp for a user
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetSecurityStampAsync\u003Ed__42))]
//    [DebuggerStepThrough]
//    public virtual Task<string> GetSecurityStampAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetSecurityStampAsync\u003Ed__42 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<string>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetSecurityStampAsync\u003Ed__42>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Generate a new security stamp for a user, used for SignOutEverywhere functionality
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CUpdateSecurityStampAsync\u003Ed__48))]
//    public virtual Task<IdentityResult> UpdateSecurityStampAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CUpdateSecurityStampAsync\u003Ed__48 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CUpdateSecurityStampAsync\u003Ed__48>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Generate a password reset token for the user using the UserTokenProvider
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    public virtual Task<string> GeneratePasswordResetTokenAsync(TKey userId)
//    {
//      this.ThrowIfDisposed();
//      return this.GenerateUserTokenAsync("ResetPassword", userId);
//    }

//    /// <summary>
//    /// Reset a user's password using a reset password token
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="token"/><param name="newPassword"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CResetPasswordAsync\u003Ed__4f))]
//    public virtual Task<IdentityResult> ResetPasswordAsync(TKey userId, string token, string newPassword)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CResetPasswordAsync\u003Ed__4f stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.token = token;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.newPassword = newPassword;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CResetPasswordAsync\u003Ed__4f>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    [AsyncStateMachine(typeof (UserManager<,>.\u003CUpdateSecurityStampInternal\u003Ed__57))]
//    [DebuggerStepThrough]
//    internal Task UpdateSecurityStampInternal(TUser user)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CUpdateSecurityStampInternal\u003Ed__57 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.user = user;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CUpdateSecurityStampInternal\u003Ed__57>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    private static string NewSecurityStamp()
//    {
//      return Guid.NewGuid().ToString();
//    }

//    private IUserLoginStore<TUser, TKey> GetLoginStore()
//    {
//      IUserLoginStore<TUser, TKey> userLoginStore = this.Store as IUserLoginStore<TUser, TKey>;
//      if (userLoginStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserLoginStore);
//      else
//        return userLoginStore;
//    }

//    /// <summary>
//    /// Returns the user associated with this login
//    /// 
//    /// </summary>
//    /// 
//    /// <returns/>
//    public virtual Task<TUser> FindAsync(UserLoginInfo login)
//    {
//      this.ThrowIfDisposed();
//      return this.GetLoginStore().FindAsync(login);
//    }

//    /// <summary>
//    /// Remove a user login
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="login"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CRemoveLoginAsync\u003Ed__5a))]
//    public virtual Task<IdentityResult> RemoveLoginAsync(TKey userId, UserLoginInfo login)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CRemoveLoginAsync\u003Ed__5a stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.login = login;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CRemoveLoginAsync\u003Ed__5a>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Associate a login with a user
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="login"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CAddLoginAsync\u003Ed__61))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> AddLoginAsync(TKey userId, UserLoginInfo login)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CAddLoginAsync\u003Ed__61 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.login = login;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CAddLoginAsync\u003Ed__61>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Gets the logins for a user.
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetLoginsAsync\u003Ed__69))]
//    public virtual Task<IList<UserLoginInfo>> GetLoginsAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetLoginsAsync\u003Ed__69 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IList<UserLoginInfo>>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetLoginsAsync\u003Ed__69>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    private IUserClaimStore<TUser, TKey> GetClaimStore()
//    {
//      IUserClaimStore<TUser, TKey> userClaimStore = this.Store as IUserClaimStore<TUser, TKey>;
//      if (userClaimStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserClaimStore);
//      else
//        return userClaimStore;
//    }

//    /// <summary>
//    /// Add a user claim
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="claim"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CAddClaimAsync\u003Ed__6f))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> AddClaimAsync(TKey userId, Claim claim)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CAddClaimAsync\u003Ed__6f stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.claim = claim;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CAddClaimAsync\u003Ed__6f>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Remove a user claim
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="claim"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CRemoveClaimAsync\u003Ed__76))]
//    public virtual Task<IdentityResult> RemoveClaimAsync(TKey userId, Claim claim)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CRemoveClaimAsync\u003Ed__76 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.claim = claim;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CRemoveClaimAsync\u003Ed__76>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Get a users's claims
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetClaimsAsync\u003Ed__7d))]
//    [DebuggerStepThrough]
//    public virtual Task<IList<Claim>> GetClaimsAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetClaimsAsync\u003Ed__7d stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IList<Claim>>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetClaimsAsync\u003Ed__7d>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    private IUserRoleStore<TUser, TKey> GetUserRoleStore()
//    {
//      IUserRoleStore<TUser, TKey> userRoleStore = this.Store as IUserRoleStore<TUser, TKey>;
//      if (userRoleStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserRoleStore);
//      else
//        return userRoleStore;
//    }

//    /// <summary>
//    /// Add a user to a role
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="role"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CAddToRoleAsync\u003Ed__83))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> AddToRoleAsync(TKey userId, string role)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CAddToRoleAsync\u003Ed__83 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.role = role;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CAddToRoleAsync\u003Ed__83>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Method to add user to multiple roles
//    /// 
//    /// </summary>
//    /// <param name="userId">user id</param><param name="roles">list of role names</param>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CAddToRolesAsync\u003Ed__8c))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> AddToRolesAsync(TKey userId, params string[] roles)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CAddToRolesAsync\u003Ed__8c stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.roles = roles;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CAddToRolesAsync\u003Ed__8c>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Remove user from multiple roles
//    /// 
//    /// </summary>
//    /// <param name="userId">user id</param><param name="roles">list of role names</param>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CRemoveFromRolesAsync\u003Ed__98))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> RemoveFromRolesAsync(TKey userId, params string[] roles)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CRemoveFromRolesAsync\u003Ed__98 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.roles = roles;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CRemoveFromRolesAsync\u003Ed__98>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Remove a user from a role.
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="role"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CRemoveFromRoleAsync\u003Ed__a4))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> RemoveFromRoleAsync(TKey userId, string role)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CRemoveFromRoleAsync\u003Ed__a4 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.role = role;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CRemoveFromRoleAsync\u003Ed__a4>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns the roles for the user
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetRolesAsync\u003Ed__ac))]
//    [DebuggerStepThrough]
//    public virtual Task<IList<string>> GetRolesAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetRolesAsync\u003Ed__ac stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IList<string>>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetRolesAsync\u003Ed__ac>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns true if the user is in the specified role
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="role"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CIsInRoleAsync\u003Ed__b2))]
//    public virtual Task<bool> IsInRoleAsync(TKey userId, string role)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CIsInRoleAsync\u003Ed__b2 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.role = role;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CIsInRoleAsync\u003Ed__b2>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    internal IUserEmailStore<TUser, TKey> GetEmailStore()
//    {
//      IUserEmailStore<TUser, TKey> userEmailStore = this.Store as IUserEmailStore<TUser, TKey>;
//      if (userEmailStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserEmailStore);
//      else
//        return userEmailStore;
//    }

//    /// <summary>
//    /// Get a user's email
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetEmailAsync\u003Ed__b8))]
//    [DebuggerStepThrough]
//    public virtual Task<string> GetEmailAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetEmailAsync\u003Ed__b8 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<string>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetEmailAsync\u003Ed__b8>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Set a user's email
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="email"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CSetEmailAsync\u003Ed__be))]
//    public virtual Task<IdentityResult> SetEmailAsync(TKey userId, string email)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CSetEmailAsync\u003Ed__be stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.email = email;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CSetEmailAsync\u003Ed__be>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Find a user by his email
//    /// 
//    /// </summary>
//    /// <param name="email"/>
//    /// <returns/>
//    public virtual Task<TUser> FindByEmailAsync(string email)
//    {
//      this.ThrowIfDisposed();
//      IUserEmailStore<TUser, TKey> emailStore = this.GetEmailStore();
//      if (email == null)
//        throw new ArgumentNullException("email");
//      else
//        return emailStore.FindByEmailAsync(email);
//    }

//    /// <summary>
//    /// Get the email confirmation token for the user
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    public virtual Task<string> GenerateEmailConfirmationTokenAsync(TKey userId)
//    {
//      this.ThrowIfDisposed();
//      return this.GenerateUserTokenAsync("Confirmation", userId);
//    }

//    /// <summary>
//    /// Confirm the user's email with confirmation token
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="token"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CConfirmEmailAsync\u003Ed__c5))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> ConfirmEmailAsync(TKey userId, string token)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CConfirmEmailAsync\u003Ed__c5 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.token = token;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CConfirmEmailAsync\u003Ed__c5>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns true if the user's email has been confirmed
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CIsEmailConfirmedAsync\u003Ed__cd))]
//    [DebuggerStepThrough]
//    public virtual Task<bool> IsEmailConfirmedAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CIsEmailConfirmedAsync\u003Ed__cd stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CIsEmailConfirmedAsync\u003Ed__cd>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    internal IUserPhoneNumberStore<TUser, TKey> GetPhoneNumberStore()
//    {
//      IUserPhoneNumberStore<TUser, TKey> phoneNumberStore = this.Store as IUserPhoneNumberStore<TUser, TKey>;
//      if (phoneNumberStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserPhoneNumberStore);
//      else
//        return phoneNumberStore;
//    }

//    /// <summary>
//    /// Get a user's phoneNumber
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetPhoneNumberAsync\u003Ed__d3))]
//    [DebuggerStepThrough]
//    public virtual Task<string> GetPhoneNumberAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetPhoneNumberAsync\u003Ed__d3 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<string>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetPhoneNumberAsync\u003Ed__d3>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Set a user's phoneNumber
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="phoneNumber"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CSetPhoneNumberAsync\u003Ed__d9))]
//    public virtual Task<IdentityResult> SetPhoneNumberAsync(TKey userId, string phoneNumber)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CSetPhoneNumberAsync\u003Ed__d9 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.phoneNumber = phoneNumber;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CSetPhoneNumberAsync\u003Ed__d9>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Set a user's phoneNumber with the verification token
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="phoneNumber"/><param name="token"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CChangePhoneNumberAsync\u003Ed__e0))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> ChangePhoneNumberAsync(TKey userId, string phoneNumber, string token)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CChangePhoneNumberAsync\u003Ed__e0 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.phoneNumber = phoneNumber;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.token = token;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CChangePhoneNumberAsync\u003Ed__e0>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns true if the user's phone number has been confirmed
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CIsPhoneNumberConfirmedAsync\u003Ed__e8))]
//    public virtual Task<bool> IsPhoneNumberConfirmedAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CIsPhoneNumberConfirmedAsync\u003Ed__e8 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CIsPhoneNumberConfirmedAsync\u003Ed__e8>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CCreateSecurityTokenAsync\u003Ed__ee))]
//    internal Task<SecurityToken> CreateSecurityTokenAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CCreateSecurityTokenAsync\u003Ed__ee stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<SecurityToken>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CCreateSecurityTokenAsync\u003Ed__ee>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Generate a code that the user can use to change their phone number to a specific number
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="phoneNumber"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGenerateChangePhoneNumberTokenAsync\u003Ed__f1))]
//    [DebuggerStepThrough]
//    public virtual Task<string> GenerateChangePhoneNumberTokenAsync(TKey userId, string phoneNumber)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGenerateChangePhoneNumberTokenAsync\u003Ed__f1 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.phoneNumber = phoneNumber;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<string>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGenerateChangePhoneNumberTokenAsync\u003Ed__f1>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Verify the code is valid for a specific user and for a specific phone number
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="token"/><param name="phoneNumber"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CVerifyChangePhoneNumberTokenAsync\u003Ed__f4))]
//    public virtual Task<bool> VerifyChangePhoneNumberTokenAsync(TKey userId, string token, string phoneNumber)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CVerifyChangePhoneNumberTokenAsync\u003Ed__f4 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.token = token;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.phoneNumber = phoneNumber;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CVerifyChangePhoneNumberTokenAsync\u003Ed__f4>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Verify a user token with the specified purpose
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="purpose"/><param name="token"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CVerifyUserTokenAsync\u003Ed__f9))]
//    [DebuggerStepThrough]
//    public virtual Task<bool> VerifyUserTokenAsync(TKey userId, string purpose, string token)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CVerifyUserTokenAsync\u003Ed__f9 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.purpose = purpose;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.token = token;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CVerifyUserTokenAsync\u003Ed__f9>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Get a user token for a specific purpose
//    /// 
//    /// </summary>
//    /// <param name="purpose"/><param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGenerateUserTokenAsync\u003Ed__fe))]
//    public virtual Task<string> GenerateUserTokenAsync(string purpose, TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGenerateUserTokenAsync\u003Ed__fe stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.purpose = purpose;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<string>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGenerateUserTokenAsync\u003Ed__fe>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Register a two factor authentication provider with the TwoFactorProviders mapping
//    /// 
//    /// </summary>
//    /// <param name="twoFactorProvider"/><param name="provider"/>
//    public virtual void RegisterTwoFactorProvider(string twoFactorProvider, IUserTokenProvider<TUser, TKey> provider)
//    {
//      this.ThrowIfDisposed();
//      if (twoFactorProvider == null)
//        throw new ArgumentNullException("twoFactorProvider");
//      if (provider == null)
//        throw new ArgumentNullException("provider");
//      this.TwoFactorProviders[twoFactorProvider] = provider;
//    }

//    /// <summary>
//    /// Returns a list of valid two factor providers for a user
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetValidTwoFactorProvidersAsync\u003Ed__103))]
//    public virtual Task<IList<string>> GetValidTwoFactorProvidersAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetValidTwoFactorProvidersAsync\u003Ed__103 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IList<string>>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetValidTwoFactorProvidersAsync\u003Ed__103>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Verify a two factor token with the specified provider
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="twoFactorProvider"/><param name="token"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CVerifyTwoFactorTokenAsync\u003Ed__10b))]
//    [DebuggerStepThrough]
//    public virtual Task<bool> VerifyTwoFactorTokenAsync(TKey userId, string twoFactorProvider, string token)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CVerifyTwoFactorTokenAsync\u003Ed__10b stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.twoFactorProvider = twoFactorProvider;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.token = token;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CVerifyTwoFactorTokenAsync\u003Ed__10b>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Get a token for a specific two factor provider
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="twoFactorProvider"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGenerateTwoFactorTokenAsync\u003Ed__111))]
//    public virtual Task<string> GenerateTwoFactorTokenAsync(TKey userId, string twoFactorProvider)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGenerateTwoFactorTokenAsync\u003Ed__111 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.twoFactorProvider = twoFactorProvider;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<string>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGenerateTwoFactorTokenAsync\u003Ed__111>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Notify a user with a token using a specific two-factor authentication provider's Notify method
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="twoFactorProvider"/><param name="token"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CNotifyTwoFactorTokenAsync\u003Ed__116))]
//    public virtual Task<IdentityResult> NotifyTwoFactorTokenAsync(TKey userId, string twoFactorProvider, string token)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CNotifyTwoFactorTokenAsync\u003Ed__116 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.twoFactorProvider = twoFactorProvider;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.token = token;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CNotifyTwoFactorTokenAsync\u003Ed__116>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    internal IUserTwoFactorStore<TUser, TKey> GetUserTwoFactorStore()
//    {
//      IUserTwoFactorStore<TUser, TKey> userTwoFactorStore = this.Store as IUserTwoFactorStore<TUser, TKey>;
//      if (userTwoFactorStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserTwoFactorStore);
//      else
//        return userTwoFactorStore;
//    }

//    /// <summary>
//    /// Get whether two factor authentication is enabled for a user
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetTwoFactorEnabledAsync\u003Ed__11b))]
//    [DebuggerStepThrough]
//    public virtual Task<bool> GetTwoFactorEnabledAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetTwoFactorEnabledAsync\u003Ed__11b stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetTwoFactorEnabledAsync\u003Ed__11b>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Set whether a user has two factor authentication enabled
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="enabled"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CSetTwoFactorEnabledAsync\u003Ed__121))]
//    public virtual Task<IdentityResult> SetTwoFactorEnabledAsync(TKey userId, bool enabled)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CSetTwoFactorEnabledAsync\u003Ed__121 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.enabled = enabled;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CSetTwoFactorEnabledAsync\u003Ed__121>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Send an email to the user
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="subject"/><param name="body"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CSendEmailAsync\u003Ed__129))]
//    [DebuggerStepThrough]
//    public virtual Task SendEmailAsync(TKey userId, string subject, string body)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CSendEmailAsync\u003Ed__129 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.subject = subject;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.body = body;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CSendEmailAsync\u003Ed__129>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Send a user a sms message
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="message"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CSendSmsAsync\u003Ed__12f))]
//    [DebuggerStepThrough]
//    public virtual Task SendSmsAsync(TKey userId, string message)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CSendSmsAsync\u003Ed__12f stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.message = message;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CSendSmsAsync\u003Ed__12f>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    internal IUserLockoutStore<TUser, TKey> GetUserLockoutStore()
//    {
//      IUserLockoutStore<TUser, TKey> userLockoutStore = this.Store as IUserLockoutStore<TUser, TKey>;
//      if (userLockoutStore == null)
//        throw new NotSupportedException(Resources.StoreNotIUserLockoutStore);
//      else
//        return userLockoutStore;
//    }

//    /// <summary>
//    /// Returns true if the user is locked out
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CIsLockedOutAsync\u003Ed__134))]
//    public virtual Task<bool> IsLockedOutAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CIsLockedOutAsync\u003Ed__134 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CIsLockedOutAsync\u003Ed__134>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Sets whether lockout is enabled for this user
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="enabled"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CSetLockoutEnabledAsync\u003Ed__13c))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> SetLockoutEnabledAsync(TKey userId, bool enabled)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CSetLockoutEnabledAsync\u003Ed__13c stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.enabled = enabled;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CSetLockoutEnabledAsync\u003Ed__13c>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns whether lockout is enabled for the user
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetLockoutEnabledAsync\u003Ed__143))]
//    public virtual Task<bool> GetLockoutEnabledAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetLockoutEnabledAsync\u003Ed__143 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<bool>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetLockoutEnabledAsync\u003Ed__143>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns when the user is no longer locked out, dates in the past are considered as not being locked out
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetLockoutEndDateAsync\u003Ed__149))]
//    [DebuggerStepThrough]
//    public virtual Task<DateTimeOffset> GetLockoutEndDateAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetLockoutEndDateAsync\u003Ed__149 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<DateTimeOffset>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetLockoutEndDateAsync\u003Ed__149>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Sets the when a user lockout ends
//    /// 
//    /// </summary>
//    /// <param name="userId"/><param name="lockoutEnd"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CSetLockoutEndDateAsync\u003Ed__14f))]
//    public virtual Task<IdentityResult> SetLockoutEndDateAsync(TKey userId, DateTimeOffset lockoutEnd)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CSetLockoutEndDateAsync\u003Ed__14f stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.lockoutEnd = lockoutEnd;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CSetLockoutEndDateAsync\u003Ed__14f>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Increments the access failed count for the user and if the failed access account is greater than or equal
//    ///             to the MaxFailedAccessAttempsBeforeLockout, the user will be locked out for the next DefaultAccountLockoutTimeSpan
//    ///             and the AccessFailedCount will be reset to 0. This is used for locking out the user account.
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CAccessFailedAsync\u003Ed__157))]
//    public virtual Task<IdentityResult> AccessFailedAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CAccessFailedAsync\u003Ed__157 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CAccessFailedAsync\u003Ed__157>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Resets the access failed count for the user to 0
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CResetAccessFailedCountAsync\u003Ed__160))]
//    [DebuggerStepThrough]
//    public virtual Task<IdentityResult> ResetAccessFailedCountAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CResetAccessFailedCountAsync\u003Ed__160 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<IdentityResult>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CResetAccessFailedCountAsync\u003Ed__160>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    /// <summary>
//    /// Returns the number of failed access attempts for the user
//    /// 
//    /// </summary>
//    /// <param name="userId"/>
//    /// <returns/>
//    [DebuggerStepThrough]
//    [AsyncStateMachine(typeof (UserManager<,>.\u003CGetAccessFailedCountAsync\u003Ed__167))]
//    public virtual Task<int> GetAccessFailedCountAsync(TKey userId)
//    {
//      // ISSUE: variable of a compiler-generated type
//      UserManager<TUser, TKey>.\u003CGetAccessFailedCountAsync\u003Ed__167 stateMachine;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E4__this = this;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.userId = userId;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder = AsyncTaskMethodBuilder<int>.Create();
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003E1__state = -1;
//      // ISSUE: reference to a compiler-generated field
//      stateMachine.\u003C\u003Et__builder.Start<UserManager<TUser, TKey>.\u003CGetAccessFailedCountAsync\u003Ed__167>(ref stateMachine);
//      // ISSUE: reference to a compiler-generated field
//      return stateMachine.\u003C\u003Et__builder.Task;
//    }

//    private void ThrowIfDisposed()
//    {
//      if (this._disposed)
//        throw new ObjectDisposedException(this.GetType().Name);
//    }

//    /// <summary>
//    /// When disposing, actually dipose the store
//    /// 
//    /// </summary>
//    /// <param name="disposing"/>
//    protected virtual void Dispose(bool disposing)
//    {
//      if (!disposing || this._disposed)
//        return;
//      this.Store.Dispose();
//      this._disposed = true;
//    }
//  }
//}

//}