﻿namespace XAct.Security.Authorization.RBAC
{
    using System;
    using System.Linq;
    using XAct.Domain.Repositories;
    using XAct.Security.Entities;

    public interface IRoleManagementService : ISimpleRepository<Role, Guid>, IHasXActLibService
    {
        bool Exists(string roleUniqueName);
        IQueryable<Role> GetRoles(bool? enabled = null);
        Role GetById(Guid id, bool withParent = false, bool withChildren = false);
        Role GetByKey(string roleUniqueName, bool withParent=false, bool withChildren=false);
        bool CreateRole(string roleUniqueName, bool enabled = true);
        bool CreateRole(string roleUniqueName, bool enabled, out Role role);
        bool? GetEnabledState(string roleUniqueName);
        void ChangeEnabledState(string roleUniqueName, bool enabled);
        void RenameRole(string roleUniqueName, string newroleUniqueName);
        void DeleteRole(string roleUniqueName);

        bool IsUserInRole(string userIdentifier, string roleUniqueName);
        bool AddUserToRole(string userIdentifier, string roleUniqueName);
        bool RemoveUserFromRoleRole(string userIdentifier, string roleUniqueName);
        string[] GetUsersRoles(string userIdentifier, bool? enabled = true);
        int GetCountOfUsersInRole(string roleUniqueName);
        string[] GetUserNamesInRole(params string[] roleName);




    }
}