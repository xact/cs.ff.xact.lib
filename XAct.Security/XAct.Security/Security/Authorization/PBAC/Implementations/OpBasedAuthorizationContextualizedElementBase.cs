﻿using System;

namespace XAct.Security.Authorization.PBAC
{
    public abstract class OpBasedAuthorizationContextualizedElementBase <TElementService, TContextElement, TVSElement>
        :
        OpBasedAuthorizationElementBase<TElementService, TVSElement>
        where TElementService : class
        where TContextElement : class 
        where TVSElement : class
    {

        /// <summary>
        /// Gets the context element 
        /// (eg: <see cref="IOpBasedAuthorizationStore"/>,
        /// or 
        /// <see cref="IOpBasedAuthorizationStoreApplication"/>)
        /// </summary>
        /// <value>The context element.</value>
        /// <internal><para>7/16/2011: Sky</para></internal>
        /// <internal>
        /// Reminder: 
        /// Protected (therefore no contract)
        /// as it's only needed in order to provide it
        /// as an arg whne invoking ServiceMethods
        /// </internal>
        protected TContextElement ContextElement
        {
            get { return _contextElement; }
        }
        private readonly TContextElement _contextElement;


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OpBasedAuthorizationContextualizedElementBase&lt;TElementService, TContextElement, TVSElement&gt;"/> class.
        /// </summary>
        /// <param name="elementService">The element service.</param>
        /// <param name="contextElement">The context element.</param>
        /// <param name="wrappedVendorSpecificElement">The wrapped vendor specific element.</param>
        /// <internal><para>7/16/2011: Sky</para></internal>
        protected OpBasedAuthorizationContextualizedElementBase( TElementService elementService, TContextElement contextElement, TVSElement wrappedVendorSpecificElement) : base(elementService,wrappedVendorSpecificElement)
        {
            if (contextElement == null) { throw new ArgumentNullException("contextElement");}

            _contextElement = contextElement;
        }
        #endregion

    }
}
