
namespace XAct.Security.Authorization.PBAC
{
    namespace XAct.Security.Authorization
    {
        public class OpBasedAuthorizationAppOperationBase<TVSElement> :
            OpBasedAuthorizationContextualizedElementBase
                <
                IOpBasedAuthorizationApplicationOperationService,
                IOpBasedAuthorizationStoreApplication,
                TVSElement
                >,
            IOpBasedAuthorizationAppOperation<TVSElement>
            where TVSElement : class
        {

            #region Constructors
            /// <summary>
            /// Initializes a new instance of the <see cref="OpBasedAuthorizationAppOperationBase&lt;TVSElement&gt;"/> class.
            /// <para>
            /// IMPORTANT: The vendor specific sub-class has to
            /// extract the Name and Description properties from the passed <paramref name="wrappedVendorSpecificOperationElement"/>.
            /// </para>
            /// </summary>
            /// <param name="operationService">The operation service.</param>
            /// <param name="application">The application.</param>
            /// <param name="wrappedVendorSpecificOperationElement">The wrapped vendor specific element (eg: <c>IAzOperation</c>).</param>
            /// <internal>7/15/2011: Sky</internal>
            protected OpBasedAuthorizationAppOperationBase(
                IOpBasedAuthorizationApplicationOperationService operationService,
                IOpBasedAuthorizationStoreApplication application,
                TVSElement wrappedVendorSpecificOperationElement)
                : base(operationService, application, wrappedVendorSpecificOperationElement)
            {
                //IMPORTANT: SubClass must extract and set Name/Description from passed in element.
            }
            #endregion



        }
    }
}