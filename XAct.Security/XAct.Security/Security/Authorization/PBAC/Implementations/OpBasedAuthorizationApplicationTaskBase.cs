﻿
namespace XAct.Security.Authorization.PBAC
{
    public class OpBasedAuthorizationApplicationTaskBase<TVSElement> :
        OpBasedAuthorizationContextualizedElementBase
            <
            IOpBasedAuthorizationApplicationTaskService,
            IOpBasedAuthorizationStoreApplication,
            TVSElement
            >,
        IOpBasedAuthorizationAppTask<TVSElement>
        where TVSElement : class
    {

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OpBasedAuthorizationApplicationTaskBase&lt;TVSElement&gt;"/> class.
        /// <para>
        /// IMPORTANT: The vendor specific sub-class has to 
        /// extract the Name and Description properties from the passed <paramref name="wrappedVendorSpecificTaskElement"/>.
        /// </para>
        /// </summary>
        /// <param name="wrappedVendorSpecificTaskElement">The wrapped vendor specific element (eg: <c>IAzTask</c>).</param>
        /// <internal><para>7/15/2011: Sky</para></internal>
        protected OpBasedAuthorizationApplicationTaskBase(
            IOpBasedAuthorizationApplicationTaskService roleService,
            IOpBasedAuthorizationStoreApplication application,
            TVSElement wrappedVendorSpecificTaskElement)
            : base(roleService, application, wrappedVendorSpecificTaskElement)
        {
            //IMPORTANT: SubClass must extract and set Name/Description from passed in element.
        }
        #endregion



        public string[] ListNames(OpBasedAuthorizationElementType nestedElementType)
        {
            return this.ElementService.ListNestedElementNames(this.ContextElement, this, nestedElementType);
        }

        public System.Collections.Generic.List<TNestedElementType> List<TNestedElementType>()
        {
            return this.ElementService.ListNestedElements<TNestedElementType>(this.ContextElement, this);
        }

        public bool Contains(OpBasedAuthorizationElementType nestedElementType, string nestedElementName)
        {
            return this.ElementService.ContainsNestedElement(this.ContextElement, this, nestedElementType, nestedElementName);
        }

        public bool Contains<TNestedElement>(TNestedElement nestedElement)
        {
            return this.ElementService.ContainsNestedElement(this.ContextElement, this, nestedElement);
        }

        public void AddExisting(OpBasedAuthorizationElementType nestedElementType, string nestedElementName)
        {
            this.ElementService.AddNestedElement(this.ContextElement, this, nestedElementType, nestedElementName);
        }

        public void AddExisting<TNestedElement>(TNestedElement nestedElement)
        {
            this.ElementService.AddNestedElement<TNestedElement>(this.ContextElement, this, nestedElement);
        }

        public void Remove(OpBasedAuthorizationElementType nestedElementType, string nestedElementName)
        {
            this.ElementService.RemoveNestedElement(this.ContextElement, this, nestedElementType, nestedElementName);
        }

        public void Remove<TNestedElement>(TNestedElement nestedElement)
        {
            this.ElementService.RemoveNestedElement(this.ContextElement, this, nestedElement);
        }
    }
}

