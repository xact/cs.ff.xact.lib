﻿
namespace XAct.Security.Authorization.PBAC
{
    public abstract class OpBasedAuthorizationContextualizedParentElementBase<TElementService, TContextElement, TVSElement, TNestedElementTypeEnum> :
        OpBasedAuthorizationContextualizedElementBase
            <
            TElementService,
            TContextElement,
            TVSElement
            >,
        IOpBasedAuthorizationAppRole
        where TElementService : class, IOpBasedAuthorizationParentElementService<TContextElement, TVSElement, TNestedElementTypeEnum> 
        where TContextElement : class
        where TVSElement : class
    {

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OpBasedAuthorizationApplicationRoleBase&lt;TVSElement&gt;"/> class.
        /// <para>
        /// IMPORTANT: The vendor specific sub-class has to
        /// extract the Name and Description properties from the passed <paramref name="wrappedVendorSpecificRoleElement"/>.
        /// </para>
        /// </summary>
        /// <param name="roleService">The role service.</param>
        /// <param name="application">The application.</param>
        /// <param name="wrappedVendorSpecificRoleElement">The wrapped vendor specific element (eg: <c>IAzRole</c>).</param>
        /// <internal>7/15/2011: Sky</internal>
        protected OpBasedAuthorizationContextualizedParentElementBase(
            TElementService roleService,
            TContextElement application,
            TVSElement wrappedVendorSpecificRoleElement)
            : base(roleService, application, wrappedVendorSpecificRoleElement)
        {
            //IMPORTANT: SubClass must extract and set Name/Description from passed in element.
        }
        #endregion



        public string[] ListNames(OpBasedAuthorizationElementType nestedElementType)
        {
            return this.ElementService.ListNestedElementNames(this.ContextElement, this, nestedElementType);
        }

        public System.Collections.Generic.List<TNestedElementType> List<TNestedElementType>()
        {
            return this.ElementService.ListNestedElements<TNestedElementType>(this.ContextElement, this);
        }

        public bool Contains(OpBasedAuthorizationElementType nestedElementType, string nestedElementName)
        {
            return this.ElementService.ContainsNestedElement(this.ContextElement, this, nestedElementType, nestedElementName);
        }

        public bool Contains<TNestedElement>(TNestedElement nestedElement)
        {
            return this.ElementService.ContainsNestedElement(this.ContextElement, this, nestedElement);
        }

        public void AddExisting(OpBasedAuthorizationElementType nestedElementType, string nestedElementName)
        {
            this.ElementService.AddNestedElement(this.ContextElement, this, nestedElementType, nestedElementName);
        }

        public void AddExisting<TNestedElement>(TNestedElement nestedElement)
        {
            this.ElementService.AddNestedElement(this.ContextElement, this, nestedElement);
        }

        public void Remove(OpBasedAuthorizationElementType nestedElementType, string nestedElementName)
        {
            this.ElementService.RemoveNestedElement(this.ContextElement, this, nestedElementType, nestedElementName);
        }

        public void Remove<TNestedElement>(TNestedElement nestedElement)
        {
            this.ElementService.RemoveNestedElement(this.ContextElement, this, nestedElement);
        }
    }
}

