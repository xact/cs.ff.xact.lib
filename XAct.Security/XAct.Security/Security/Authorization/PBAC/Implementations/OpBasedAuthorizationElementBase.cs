﻿
using System;

namespace XAct.Security.Authorization.PBAC
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TElementService">The type of the element service.</typeparam>
    /// <typeparam name="TVSElement">The type of the VS element.</typeparam>
    /// <internal>
    /// 	<para>
    /// Base class for
    /// <c>AzManAuthorizationApplicationRole</c>,
    /// <c>AzManAuthorizationManApplciation</c>,
    /// etc.
    /// </para>
    /// 	<para>
    /// The idea is to make it simple to write
    /// the <c>AzMan</c> specific classes as thin and easy as possible,
    /// and leverage this class, which in turn
    /// </para>
    /// </internal>

    public abstract class OpBasedAuthorizationElementBase<TElementService, TVSElement> 
        : 
        IOpBasedAuthorizationElement<TVSElement>
        where TElementService : class
        where TVSElement : class
    {
        /// <summary>
        /// Gets or sets the name of the element.
        /// </summary>
        /// <value>The name.</value>
        /// <internal><para>7/15/2011: Sky</para></internal>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the (optional) description of the element.
        /// </summary>
        /// <value>The description.</value>
        /// <internal><para>7/15/2011: Sky</para></internal>
        public string Description{ get; set; }


        /// <summary>
        /// Gets the internal element.
        /// </summary>
        /// <value>The internal element.</value>
        /// <internal><para>7/15/2011: Sky</para></internal>
        public TVSElement InternalElement
        {
            get { return _internalElement; }
        }
        private readonly TVSElement _internalElement;


        /// <summary>
        /// Gets the Service used to manage this element.
        /// </summary>
        /// <value>The element service.</value>
        /// <internal><para>7/16/2011: Sky</para></internal>
        protected TElementService ElementService
        {
            get { return _elementService; }
        }
        private readonly TElementService _elementService;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OpBasedAuthorizationElementBase&lt;TElementService,TVSElement&gt;"/> class.
        /// <para>
        /// IMPORTANT: The vendor specific sub-class has to
        /// extract the Name and Description properties from the passed <paramref name="wrappedVendorSpecificElement"/>.
        /// </para>
        /// </summary>
        /// <param name="elementService">The element service.</param>
        /// <param name="wrappedVendorSpecificElement">The element.</param>
        /// <internal>7/15/2011: Sky</internal>
        protected OpBasedAuthorizationElementBase(TElementService elementService, TVSElement wrappedVendorSpecificElement) 
        {
            if (wrappedVendorSpecificElement == null){throw new ArgumentNullException("wrappedVendorSpecificElement");}

            _internalElement = wrappedVendorSpecificElement;
            _elementService = elementService;
        }
        #endregion
    }

}
