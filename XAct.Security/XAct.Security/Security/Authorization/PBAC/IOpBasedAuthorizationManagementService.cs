﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// As per ISP principal, break the interface down into 
    /// smaller units.
    /// </internal>
    public interface IOpBasedAuthorizationManagementService : IHasXActLibService

    {
        /// <summary>
        /// 
        /// </summary>
        IOpBasedAuthorizationStoreManagementService Stores { get; }

    }
}
