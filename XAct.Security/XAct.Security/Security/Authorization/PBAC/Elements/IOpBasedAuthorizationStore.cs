﻿
namespace XAct.Security.Authorization.PBAC
{
    /// <summary>
    /// A specialization of the 
    /// <see cref="IOpBasedAuthorizationElementBase"/>
    /// (Name,Description,InnerElement,Application)
    /// contract.
    /// </summary>
    public interface IOpBasedAuthorizationStore :
        IOpBasedAuthorizationParentElement
            <
            OpBasedAuthorizationStoreElementType
            >
    {
    }

}
