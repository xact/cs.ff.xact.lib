﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for an Application Role
    /// </summary>
    public interface IOpBasedAuthorizationAppRole
        :
        IOpBasedAuthorizationParentElement
        <
            OpBasedAuthorizationRoleElementType
        >
    {
    }
}