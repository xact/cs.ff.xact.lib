﻿

namespace XAct.Security.Authorization.PBAC
{
    /// <summary>
    /// A specialization of the 
    /// <see cref="IOpBasedAuthorizationElementBase"/>
    /// that adds a <see cref="Store"/> property
    /// so that Applications and Store Groups can refer back
    /// to the Store in which they were created.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <internal>
    /// Base interface for both 
    /// <see cref="IOpBasedAuthorizationStoreApplication"/>
    /// and
    /// <see cref="IOpBasedAuthorizationStoreGroup"/>
    /// </internal>
    public interface IOpBasedAuthorizationStoreElementBase :
        IOpBasedAuthorizationElementBase
    {
        /// <summary>
        /// The owning <see cref="IOpBasedAuthorizationStore"/>.
        /// </summary>
        /// <remarks>
        /// Set via the Constructor.
        /// </remarks>
        IOpBasedAuthorizationStore Store { get; }

    }
}
