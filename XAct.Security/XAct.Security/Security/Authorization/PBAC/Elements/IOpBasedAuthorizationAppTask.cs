﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A specialization of
    ///  <see cref="IOpBasedAuthorizationParentElement{OpBasedAuthorizationElementType}"/>
    /// </summary>
    public interface IOpBasedAuthorizationAppTask
        :
        IOpBasedAuthorizationParentElement<OpBasedAuthorizationTaskElementType> 
    {
    }
}