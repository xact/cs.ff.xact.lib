﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A base contract for any object that wraps a product specific object
    /// allowing the vendor-specific system to be manipulated from 
    /// other assemblies, without forcing a dependency on the vendor code/assembly.
    /// </summary>
    public interface IOpBasedAuthorizationElementBase    : IHasNameAndDescription, IHasInnerItemReadOnly
    {
    }
}
