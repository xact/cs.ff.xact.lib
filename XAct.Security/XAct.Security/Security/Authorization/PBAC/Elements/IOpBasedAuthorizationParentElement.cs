﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    /// Base contract for 
    /// <see cref="IOpBasedAuthorizationAppTask"/>, and
    /// <see cref="IOpBasedAuthorizationAppRole"/>,
    /// but not <see cref="IOpBasedAuthorizationAppOperation"/>, 
    /// as Operations cannot contain
    /// child <see cref="IOpBasedAuthorizationAppOperation"/>.
    /// </summary>
    /// <typeparam name="TNestedElementTypeEnum">The type of the nested element type enum.</typeparam>
    public interface IOpBasedAuthorizationParentElement<in TNestedElementTypeEnum> :
               IOpBasedAuthorizationElementBase
 
    {

        /// <summary>
        /// list the names of the child elements within this element.
        /// </summary>
        /// <param name="nestedElementType">The Type of element (Element or Element).</param>
        /// <returns>array of Element names</returns>
        /// <internal>5/17/2011: Sky</internal>
        string[] ListNames(TNestedElementTypeEnum nestedElementType);

        /// <summary>
        /// list the child elements within this element.
        /// </summary>
        /// <typeparam name="TNestedElementType">The type of the nested element type.</typeparam>
        /// <returns></returns>
        /// <internal><para>7/14/2011: Sky</para></internal>
        List<TNestedElementType> List<TNestedElementType>(TNestedElementTypeEnum type);

        /// <summary>
        /// Determines whether the given child element is a member of/nested under the parent role.
        /// </summary>
        /// <param name="nestedElementType">The Type of nested child element.</param>
        /// <param name="nestedElementName">Name of the nested child element.</param>
        /// <returns>
        /// 	<c>true</c> if child element is nested under the parent element; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>5/17/2011: Sky</para></internal>
        bool Contains(TNestedElementTypeEnum nestedElementType, string nestedElementName);

        /// <summary>
        /// Determines whether [contains] [the specified context element].
        /// </summary>
        /// <typeparam name="TNestedElement">The type of the nested element.</typeparam>
        /// <param name="nestedElement">The nested element.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified context element]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>7/14/2011: Sky</para></internal>
        bool Contains<TNestedElement>(TNestedElement nestedElement);


        /// <summary>
        /// Adds the given child element as a nested child to the parent element.
        /// </summary>
        /// <param name="nestedElementType">The Type of nested child element.</param>
        /// <param name="nestedElementName">Name of the nested child element.</param>
        /// <internal><para>5/17/2011: Sky</para></internal>
        void AddExisting(TNestedElementTypeEnum nestedElementType, string nestedElementName);


        /// <summary>
        /// Adds the specified context element.
        /// </summary>
        /// <typeparam name="TNestedElement">The type of the nested element.</typeparam>
        /// <param name="nestedElement">The nested element.</param>
        /// <internal><para>7/14/2011: Sky</para></internal>
        void AddExisting<TNestedElement>(TNestedElement nestedElement);

        /// <summary>
        /// Removes the given child element from the parent element.
        /// </summary>
        /// <param name="nestedElementType">The Type of nested child element.</param>
        /// <param name="nestedElementName">Name of the nested child element.</param>
        /// <internal><para>5/17/2011: Sky</para></internal>
        void Remove(TNestedElementTypeEnum nestedElementType, string nestedElementName);

        /// <summary>
        /// Removes the specified context element.
        /// </summary>
        /// <typeparam name="TNestedElement">The type of the nested element.</typeparam>
        /// <param name="nestedElement">The nested element.</param>
        /// <internal><para>7/14/2011: Sky</para></internal>
        void Remove<TNestedElement>(TNestedElement nestedElement);
    }
}
