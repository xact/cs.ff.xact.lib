﻿
//namespace XAct.Security.Authorization.PBAC
//{
//    /// <summary>
//    /// A specialization of 
//    /// <see cref="IOpBasedAuthorizationStoreElementBase"/>
//    /// (Name, Description, InnerElement, Application) 
//    /// that adds Access to general Administrator classes to manage
//    /// Groups, Roles, Tasks, and Operations.
//    /// </summary>
//    public interface IOpBasedAuthorizationStoreApplication : 
//        IOpBasedAuthorizationStoreElementBase,
//        IOpBasedAuthorizationContextualizedElementBase<IOpBasedAuthorizationStore>
//    {

//        /// <summary>
//        /// Gets the <see cref="IOpBasedAuthorizationApplicationGroupAdministrator"/>
//        /// used to manage the Application's Groups.
//        /// <para>
//        /// Reminder: A Group contains one or more child Groups, and/or Members.
//        /// </para>
//        /// </summary>
//        IOpBasedAuthorizationApplicationGroupAdministrator Groups { get; }

//        /// <summary>
//        /// Gets the <see cref="IOpBasedAuthorizationApplicationRoleAdministrator"/>
//        /// used to manage the Application's Roles.
//        /// <para>
//        /// Reminder: A Role contains one or more child Roles, Tasks and/or Operations.
//        /// </para>
//        /// </summary>
//        IOpBasedAuthorizationApplicationRoleAdministrator Roles { get; }

//        /// <summary>
//        /// Gets the <see cref="IOpBasedAuthorizationApplicationTaskAdministrator"/>
//        /// used to manage the Application's Tasks.
//        /// <para>
//        /// Reminder: A Task contains one or more child Tasks and/or Operations.
//        /// </para>
//        /// </summary>
//        IOpBasedAuthorizationApplicationTaskAdministrator Tasks { get; }

//        /// <summary>
//        /// Gets the <see cref="IOpBasedAuthorizationApplicationOperationAdministrator"/>
//        /// used to manage the Application's Operations.
//        /// </summary>
//        IOpBasedAuthorizationApplicationOperationAdministrator Operations { get; }
//    }
//}
