﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{

    /// <summary>
    /// Contract for an Operation.
    /// An Operation can be assigned to a Task (<see cref="IOpBasedAuthorizationAppTask"/>,
    /// that can in turn be assigned to a Role (<see cref="IOpBasedAuthorizationAppRole"/>.
    /// </summary>
    public interface IOpBasedAuthorizationAppOperation
        :
        IOpBasedAuthorizationElementBase
    {
    }
}