﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{

    /// <summary>
    /// Interface for a Group, that can contain other Groups, and/or Users.
    /// <para>
    /// A group is a element within a store.
    /// </para>
    /// <para>
    /// It is a specialization of IOpBasedAuthorizationParentElement because it can contain child elements.
    /// </para>
    /// </summary>
    public interface IOpBasedAuthorizationAppGroup
        :
        IOpBasedAuthorizationParentElement<OpBasedAuthorizationElementType>
    {
        //A Group is a Collection/ classification of users based on their rights

        //Not Possible with AzMan: void AddGroupToGroup(string applicationGroupName, string nestedGroupName);
        //Not Possible with AzMan: void RemoveGroupFromGroup(string applicationGroupName, string nestedGroupName);

        /// <summary>
        /// Get the list of Members/Users in the Group.
        /// </summary>
        /// <param name="getSIDs">Get a list of Sids rather than names.</param>
        /// <returns>An array of Member names.</returns>
// ReSharper disable InconsistentNaming
        string[] GetGroupMembers(bool getSIDs);
// ReSharper restore InconsistentNaming

        /// <summary>
        /// Determines if the member/user is a member of the Application specific Group.
        /// </summary>
        /// <param name="applicationMemberName">Name of the member.</param>
        /// <param name="nameIsSID">The given name is a SID</param>
        bool IsMemberOfGroup(string applicationMemberName, bool nameIsSID);

        /// <summary>
        /// Adds the member/user to the Application specific Group
        /// </summary>
        /// <param name="applicationMemberName">Name of the member.</param>
        /// <param name="nameIsSID">The given name is a SID</param>
        void AddMemberToGroup(string applicationMemberName, bool nameIsSID);

        /// <summary>
        /// Removes the member/user from the Application specific Group
        /// </summary>
        /// <param name="applicationMemberName">Name of the member.</param>
        /// <param name="nameIsSID">The given name is a SID</param>
        void RemoveUserFromGroup(string applicationMemberName, bool nameIsSID);


        /// <summary>
        /// Adds the member/user to the Application specific Group DenyList
        /// </summary>
        /// <param name="applicationMemberName">Name of the member.</param>
        /// <param name="nameIsSID">The given name is a SID</param>
        void AddMemberToGroupDeniedList(string applicationMemberName, bool nameIsSID);

        /// <summary>
        /// Removes the member/user from the Application specific Group DenyList
        /// </summary>
        /// <param name="applicationMemberName">Name of the member.</param>
        /// <param name="nameIsSID">The given name is a SID</param>
        void RemoveUserFromGroupDeniedList(string applicationMemberName, bool nameIsSID);

    }
}