﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// An enumeration of the types of elements that can be nested 
    /// within a <see cref=""/>.
    /// </summary>
    public enum OpBasedAuthorizationGroupMemberType
    {
        /// <summary>
        /// <para>
        /// This is an error state.
        /// </para>
        /// </summary>
        Undefined=0,
        /// <summary>
        /// A Member/User
        /// </summary>
        Member=1,

        /// <summary>
        /// A Group
        /// </summary>
        Group=2,
    }
}
