﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Enumeration used to describe t
    /// <para>
    /// Valid values are: Role, Task, Operation
    /// </para>
    /// </summary>
    public enum OpBasedAuthorizationElementType 
    {
        /// <summary>
        /// This is an error state.
        /// </summary>
        Undefined=0,


        /// <summary>
        /// The element is an Role.
        /// </summary>
        Role = 4,

        /// <summary>
        /// The element is an Task.
        /// </summary>
        Task = 3,

        /// <summary>
        /// The element is an Operation.
        /// </summary>
        Operation=2
        
    }

    /// <summary>
    /// Enumeration used to describe t
    /// <para>
    /// Valid values are: Role, Task, Operation
    /// </para>
    /// </summary>
    public enum OpBasedAuthorizationRoleElementType
    {
        /// <summary>
        /// This is an error state.
        /// </summary>
        Undefined = 0,


        /// <summary>
        /// The element is an Role.
        /// </summary>
        Role = 4,

        /// <summary>
        /// The element is an Task.
        /// </summary>
        Task = 3,

        /// <summary>
        /// The element is an Operation.
        /// </summary>
        Operation = 2

    }


    public enum OpBasedAuthorizationTaskElementType
    {
        /// <summary>
        /// This is an error state.
        /// </summary>
        Undefined = 0,


        /// <summary>
        /// The element is an Task.
        /// </summary>
        Task = 3,

        /// <summary>
        /// The element is an Operation.
        /// </summary>
        Operation = 2

    }
    public enum OpBasedAuthorizationOperationElementType
    {
        /// <summary>
        /// This is an error state.
        /// </summary>
        Undefined = 0,


        /// <summary>
        /// The element is an Operation.
        /// </summary>
        Operation = 2

    }

}