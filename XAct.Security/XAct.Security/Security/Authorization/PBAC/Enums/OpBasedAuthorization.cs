﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// The response of an Operations based Authentication query..
    /// </summary>
    /// <remarks>
    /// <para>
    /// Used by <c>IOpBasedAuthorizationService.IsOperationAllowed(string)</c>
    /// </para>
    /// </remarks>
    public enum OpBasedAuthorization : byte
    {
        /// <summary>
        /// 
        /// </summary>
        Neutral,
        /// <summary>
        /// 
        /// </summary>
        Allow,
        /// <summary>
        /// 
        /// </summary>
        Deny,
        /// <summary>
        /// 
        /// </summary>
        AllowWithDelegation,
    }
}
