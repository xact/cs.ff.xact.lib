﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    public enum OpBasedAuthorizationStoreElementType
    {
        Undefined=0,
        Group=1,
        Application=2
    }
}
