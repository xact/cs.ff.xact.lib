﻿
namespace XAct.Security.Authorization.PBAC
{
    public interface IOpBasedAuthorizationStoreGroupService
        :
        IOpBasedAuthorizationElementService
            <
            IOpBasedAuthorizationStore,
            IOpBasedAuthorizationStoreGroup
            >,
        IOpBasedAuthorizationParentElementService
            <
            IOpBasedAuthorizationStore,
            IOpBasedAuthorizationStoreGroup,
            OpBasedAuthorizationGroupMemberType
            >
    {
    }

}
