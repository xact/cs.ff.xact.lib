﻿
namespace XAct.Security.Authorization.PBAC
{
    public interface IOpBasedAuthorizationApplicationTaskService
        :
        IOpBasedAuthorizationElementService
            <
            IOpBasedAuthorizationStoreApplication,
            IOpBasedAuthorizationAppTask
            >,
        IOpBasedAuthorizationParentElementService
            <
            IOpBasedAuthorizationStoreApplication,
            IOpBasedAuthorizationAppTask,
            OpBasedAuthorizationElementType
            >
    {
    }
}
