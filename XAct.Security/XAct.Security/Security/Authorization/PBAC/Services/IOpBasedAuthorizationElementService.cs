﻿
namespace XAct.Security.Authorization.PBAC
{
    /// <summary>
    /// Base interface for any service that manages Elements
    /// (Groups, Roles, Tasks, Operations)
    /// within an Application.
    /// </summary>
    /// <typeparam name="TContextElementWrapper">
    /// The type of the context element wrapper
    /// (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>).
    /// </typeparam>
    /// <typeparam name="TElementWrapper">
    /// An 
    /// <see cref="IOpBasedAuthorizationAppGroup"/>,
    /// <see cref="IOpBasedAuthorizationAppRole"/>,
    /// <see cref="IOpBasedAuthorizationAppTask"/>,
    /// or
    /// <see cref="IOpBasedAuthorizationAppOperation"/>
    /// </typeparam>
    public interface IOpBasedAuthorizationElementService<TContextElementWrapper,  TElementWrapper>
    {
        /// <summary>
        /// Does the element exist within the context (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>)?
        /// </summary>
        /// <param name="contextElement">The contextElement.</param>
        /// <param name="elementName">The element's name.</param>
        /// <returns></returns>
        /// <internal>5/17/2011: Sky</internal>
        bool Exists(TContextElementWrapper contextElement, string elementName);

        /// <summary>
        /// Creates the element within the context (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>).
        /// </summary>
        /// <param name="contextElement">The contextElement.</param>
        /// <param name="elementName">The element's name.</param>
        /// <param name="description"></param>
        /// <internal><para>5/17/2011: Sky</para></internal>
        void Create(TContextElementWrapper contextElement, string elementName, string description);

        /// <summary>
        /// Gets a list of the elements within the context (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>).
        /// </summary>
        /// <returns>
        /// Returns a list of 
        /// <see cref="IOpBasedAuthorizationAppGroup"/>,
        /// <see cref="IOpBasedAuthorizationAppRole"/>,
        /// <see cref="IOpBasedAuthorizationAppTask"/>,
        /// or
        /// <see cref="IOpBasedAuthorizationAppOperation"/>
        /// </returns>
        /// <internal><para>5/18/2011: Sky</para></internal>
        /// <param name="contextElement">The contextElement.</param>
        TElementWrapper[] List(TContextElementWrapper contextElement);

        /// <summary>
        /// Gets a list of the names of the elements
        /// (eg: the names of the relevant
        /// <see cref="IOpBasedAuthorizationAppGroup"/>,
        /// <see cref="IOpBasedAuthorizationAppRole"/>,
        /// <see cref="IOpBasedAuthorizationAppTask"/>,
        /// or
        /// <see cref="IOpBasedAuthorizationAppOperation"/>
        /// )
        /// within the current context (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>).
        /// </summary>
        /// <returns></returns>
        /// <internal><para>5/18/2011: Sky</para></internal>
        /// <param name="contextElement">The contextElement.</param>
        string[] ListNames(TContextElementWrapper contextElement);


        /// <summary>
        /// Gets the specified wrapped element within the current context (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>).
        /// </summary>
        /// <param name="contextElement">The contextElement.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <returns></returns>
        /// <internal><para>7/14/2011: Sky</para></internal>
        TElementWrapper Get(TContextElementWrapper contextElement, string elementName);


        /// <summary>
        /// Update the definition of the Element.
        /// </summary>
        /// <param name="contextElement">The contextElement.</param>
        /// <param name="existingElementName"></param>
        /// <param name="newElementSettings"></param>
        void Update(TContextElementWrapper contextElement, string existingElementName, TElementWrapper newElementSettings);

        /// <summary>
        /// Deletes the element with the given name from the current context (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>).
        /// </summary>
        /// <param name="contextElement">The contextElement.</param>
        /// <param name="elementName">Name of the element to delete from the current context.</param>
        /// <internal><para>5/17/2011: Sky</para></internal>
        void Delete(TContextElementWrapper contextElement, string elementName);

        /// <summary>
        /// Deletes the element from the current context (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>).
        /// </summary>
        /// <param name="contextElement">The contextElement.</param>
        /// <param name="element">The element to delete from the current context.</param>
        void Delete(TContextElementWrapper contextElement, TElementWrapper element);
    }
}
