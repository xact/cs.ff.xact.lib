﻿
using System.Collections.Generic;

namespace XAct.Security.Authorization.PBAC
{
    /// <summary>
    /// Base interface for any service that manages Elements
    /// (Groups, Roles, Tasks, Operations)
    /// within an Application.
    /// </summary>
    /// <typeparam name="TContextElementWrapper">
    /// The type of the parent element wrapper
    /// (eg: <see cref="IOpBasedAuthorizationStoreApplication"/>).
    /// </typeparam>
    /// <typeparam name="TElementWrapper">An
    /// <see cref="IOpBasedAuthorizationAppGroup"/>,
    /// <see cref="IOpBasedAuthorizationAppRole"/>,
    /// <see cref="IOpBasedAuthorizationAppTask"/>,
    /// or
    /// <see cref="IOpBasedAuthorizationAppOperation"/></typeparam>
    /// <typeparam name="TNestedElementTypeEnum">
    /// The type of the child element type (eg: <see cref="OpBasedAuthorizationElementType"/>.
    /// </typeparam>
    public interface IOpBasedAuthorizationParentElementService<TContextElementWrapper, TElementWrapper, TNestedElementTypeEnum>
    {
        /// <summary>
        /// Gets the list child element within the parent element.
        /// </summary>
        /// <param name="contextElement">The parent element (eg: an <see cref="IOpBasedAuthorizationStoreApplication"/>).</param>
        /// <param name="element">The parent Element.</param>
        /// <param name="nestedElementType">The Type of element (Element or Element).</param>
        /// <returns>array of Element names</returns>
        /// <internal>5/17/2011: Sky</internal>
        string[] ListNestedElementNames(TContextElementWrapper contextElement, TElementWrapper element, TNestedElementTypeEnum nestedElementType);

        /// <summary>
        /// Lists the specified context element.
        /// </summary>
        /// <typeparam name="TNestedElementType">The type of the nested element type.</typeparam>
        /// <param name="contextElement">The context element.</param>
        /// <param name="element">The parent Element.</param>
        /// <returns></returns>
        /// <internal><para>7/14/2011: Sky</para></internal>
        List<TNestedElementType> ListNestedElements<TNestedElementType>(TContextElementWrapper contextElement, TElementWrapper element);

        /// <summary>
        /// Determines whether the given child element is a member of/nested under the parent role.
        /// </summary>
        /// <param name="contextElement">The parent element (eg: an <see cref="IOpBasedAuthorizationStoreApplication"/>).</param>
        /// <param name="element">The parent Element.</param>
        /// <param name="nestedElementType">The Type of nested child element.</param>
        /// <param name="nestedElementName">Name of the nested child element.</param>
        /// <returns>
        /// 	<c>true</c> if child element is nested under the parent element; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>5/17/2011: Sky</para></internal>
        bool ContainsNestedElement(TContextElementWrapper contextElement, TElementWrapper element, TNestedElementTypeEnum nestedElementType, string nestedElementName);

        /// <summary>
        /// Determines whether [contains] [the specified context element].
        /// </summary>
        /// <typeparam name="TNestedElement">The type of the nested element.</typeparam>
        /// <param name="element">The parent Element.</param>
        /// <param name="contextElement">The context element.</param>
        /// <param name="nestedElement">The nested element.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified context element]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>7/14/2011: Sky</para></internal>
        bool ContainsNestedElement<TNestedElement>(TContextElementWrapper contextElement, TElementWrapper element, TNestedElement nestedElement);


        /// <summary>
        /// Adds the given child element as a nested child to the parent element.
        /// </summary>
        /// <param name="contextElement">The parent element (eg: an <see cref="IOpBasedAuthorizationStoreApplication"/>).</param>
        /// <param name="element">The parent Element.</param>
        /// <param name="nestedElementType">The Type of nested child element.</param>
        /// <param name="nestedElementName">Name of the nested child element.</param>
        /// <internal><para>5/17/2011: Sky</para></internal>
        void AddNestedElement(TContextElementWrapper contextElement, TElementWrapper element, TNestedElementTypeEnum nestedElementType, string nestedElementName);


        /// <summary>
        /// Adds the specified context element.
        /// </summary>
        /// <typeparam name="TNestedElement">The type of the nested element.</typeparam>
        /// <param name="contextElement">The context element.</param>
        /// <param name="element">The parent Element.</param>
        /// <param name="nestedElement">The nested element.</param>
        /// <internal><para>7/14/2011: Sky</para></internal>
        void AddNestedElement<TNestedElement>(TContextElementWrapper contextElement, TElementWrapper element, TNestedElement nestedElement);
         

        /// <summary>
        /// Removes the given child element from the parent element.
        /// </summary>
        /// <param name="contextElement">The parent element (eg: an <see cref="IOpBasedAuthorizationStoreApplication"/>).</param>
        /// <param name="element">The parent Element.</param>
        /// <param name="nestedElementType">The Type of nested child element.</param>
        /// <param name="nestedElementName">Name of the nested child element.</param>
        /// <internal><para>5/17/2011: Sky</para></internal>
        void RemoveNestedElement(TContextElementWrapper contextElement, TElementWrapper element, TNestedElementTypeEnum nestedElementType, string nestedElementName);

        /// <summary>
        /// Removes the specified context element.
        /// </summary>
        /// <typeparam name="TNestedElement">The type of the nested element.</typeparam>
        /// <param name="contextElement">The context element.</param>
        /// <param name="element">The parent Element.</param>
        /// <param name="nestedElement">The nested element.</param>
        /// <internal><para>7/14/2011: Sky</para></internal>
        void RemoveNestedElement<TNestedElement>(TContextElementWrapper contextElement, TElementWrapper element, TNestedElement nestedElement);
    }
}
