﻿namespace XAct.Security.Authorization.PBAC
{
    public interface IOpBasedAuthorizationApplicationGroupService<TVSElement>
        : 
        IOpBasedAuthorizationElementService
            <
            IOpBasedAuthorizationStoreApplication, //Context
            IOpBasedAuthorizationAppGroup<TVSElement> //Element Type
            >,
        IOpBasedAuthorizationParentElementService
            <
            IOpBasedAuthorizationStoreApplication, //Context
            IOpBasedAuthorizationAppGroup<TVSElement>, //Element Type
            OpBasedAuthorizationGroupMemberType                  //Type of child elements
            >
    {
    }
}
