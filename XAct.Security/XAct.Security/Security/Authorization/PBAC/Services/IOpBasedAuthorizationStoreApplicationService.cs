﻿
namespace XAct.Security.Authorization.PBAC
{
    public interface IOpBasedAuthorizationStoreApplicationService
        :
        IOpBasedAuthorizationElementService
            <
            IOpBasedAuthorizationStore,
            IOpBasedAuthorizationStoreApplication
            >,
        IOpBasedAuthorizationParentElementService
            <
            IOpBasedAuthorizationStore,
            IOpBasedAuthorizationStoreApplication,
            OpBasedAuthorizationElementType
            >
    {
    }
}
