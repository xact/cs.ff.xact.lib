﻿using System;

namespace XAct.Security.Authorization.PBAC
{
    public interface IOpBasedAuthorizationApplicationRoleService
        :
        IOpBasedAuthorizationElementService
            <
            IOpBasedAuthorizationStoreApplication,
            IOpBasedAuthorizationAppRole
            >,
        IOpBasedAuthorizationParentElementService
            <
            IOpBasedAuthorizationStoreApplication,
            IOpBasedAuthorizationAppRole,
            OpBasedAuthorizationElementType
            >
    {
    }
}
