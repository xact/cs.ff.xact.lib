﻿
namespace XAct.Security.Authorization.PBAC
{
    public interface IOpBasedAuthorizationApplicationOperationService 
        : 
        IOpBasedAuthorizationElementService
            <
            IOpBasedAuthorizationStoreApplication, 
            IOpBasedAuthorizationAppOperation
            >
    {
    }
}
