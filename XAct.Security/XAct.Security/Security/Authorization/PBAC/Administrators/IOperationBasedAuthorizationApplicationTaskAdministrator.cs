﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// An Operations based Authorization Task Administrator for a specific Application 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Implementations take as a Constructor argument the 
    /// owner <see cref="IOpBasedAuthorizationStoreApplication"/>
    /// </para>
    /// </remarks>
    /// <internal>
    /// <para>
    /// Questions have been raised as to why I did not go for a more object
    /// oriented approach to the problem domain: the reason is that it's not a
    /// as simple as it looks trying to wrap 'n' systems: An Operation in one
    /// system will have different properties, and methods, than an Opertion
    /// in another. 
    /// </para>
    /// </internal>
    public interface IOpBasedAuthorizationApplicationTaskAdministrator :
        IOpBasedAuthorizationElementAdministrator
            <EntityDescription, IOpBasedAuthorizationAppTask>
    {


    }
}
