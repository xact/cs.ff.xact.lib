﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// An  Administrator  of Application Level Operations. 
    /// <para>
    /// Note that unlike Groups, there are no Scope level Opererations.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Implementations take as constructor argument the 
    /// owner <see cref="IOpBasedAuthorizationStoreApplication"/>
    /// </para>
    /// </remarks>
    /// <internal>
    /// <para>
    /// Questions have been raised as to why I did not go for a more object
    /// oriented approach to the problem domain: the reason is that it's not a
    /// as simple as it looks trying to wrap 'n' systems: An Operation in one
    /// system will have different properties, and methods, than an Opertion
    /// in another. 
    /// </para>
    /// </internal>
    public interface IOpBasedAuthorizationApplicationOperationAdministrator : 
        IOpBasedAuthorizationElementAdministrator
            <EntityDescription, IOpBasedAuthorizationAppOperation>
    {
    }
}
