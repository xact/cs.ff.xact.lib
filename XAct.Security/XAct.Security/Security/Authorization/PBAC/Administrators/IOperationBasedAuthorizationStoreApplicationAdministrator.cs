﻿
namespace XAct.Security.Authorization.PBAC
{
    interface IOpBasedAuthorizationStoreApplicationAdministrator
    {
        /// <summary>
        /// Gets a list of the Applications registered in the store.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Reminder:
        /// </para>
        /// <para>
        /// A Store can contain n Applications and n Store Groups.
        /// An Application can contain n Application Groups, and Roles.
        /// A Group can contain other Groups, or Members.
        /// (Group 1-* [Group|Members])
        /// A Role can contain other Roles and Tasks, 
        ///           a Task can contain other Tasks and Operations.
        /// In other words: Role 1-* [Role|Task] 
        ///                 Task 1-* [Task|Operation]
        /// </para>
        /// </remarks>
        /// <returns></returns>
        string[] ListNames();


        /// <summary>
        /// Is the application definition registered?
        /// </summary>
        /// <remarks>
        /// <para>
        /// Reminder: a Role can contain other Roles and Tasks, 
        ///           a Task can contain other Tasks and Operations.
        /// In other words: Role 1-* [Role|Task] 
        ///                 Task 1-* [Task|Operation]
        /// </para>
        /// </remarks>
        /// <param name="name">Name of the application.</param>
        /// <returns></returns>
        /// <internal><para>5/17/2011: Sky</para></internal>
        bool Exists(string name);


        /// <summary>
        /// Register the Application in the AzMan store.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Reminder: a Role can contain other Roles and Tasks, 
        ///           a Task can contain other Tasks and Operations.
        /// In other words: Role 1-* [Role|Task] 
        ///                 Task 1-* [Task|Operation]
        /// </para>
        /// </remarks>
        /// <param name="name">Name of the application.</param>
        /// <param name="description">The application description.</param>
        /// <internal><para>5/15/2011: Sky</para></internal>
        void Create(string name, string description);


        /// <summary>
        /// Gets the application.
        /// </summary>
        /// <summary>
        /// Get/Retrieve the element.
        /// </summary>
        /// <typeparam name="TApplication"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        TApplication InternalGet<TApplication>(string name);


        /// <summary>
        /// Updates the Application in the AzMan store.
        /// </summary>
        /// <param name="newName">Name of the application.</param>
        /// <param name="newDescription">The application description.</param>
        /// <internal>5/15/2011: Sky</internal>
        /// <internal><para>5/15/2011: Sky</para></internal>
        void Update(string name, string newName, string newDescription);


        /// <summary>
        /// Deletes the Application from the Store.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="name"></param>
        /// <internal><para>5/15/2011: Sky</para></internal>
        void Delete(string name);

    }
}
