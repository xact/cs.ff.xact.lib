﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Base Contract for all Administrator interfaces
    /// <para>
    /// Implemented by
    /// <see cref="IOpBasedAuthorizationApplicationRoleAdministrator" />,
    /// <see cref="IOpBasedAuthorizationApplicationTaskAdministrator" />,
    /// <see cref="IOpBasedAuthorizationApplicationOperationAdministrator" />
    /// </para>
    /// <para>
    /// Provides CRUD mechanisms to any service
    /// (Exists, Create, List, ListNames, Get, Update, Delete).
    /// </para>
    /// </summary>
    /// <typeparam name="TElementDescription">The type of the element description.</typeparam>
    /// <typeparam name="TElementWrapper">The type of the element wrapper.</typeparam>
    public interface IOpBasedAuthorizationElementAdministrator<in TElementDescription, out TElementWrapper> : IHasXActLibService
    {

        /// <summary>
        /// Does the element exist?
        /// </summary>
        /// <param name="elementName">The element's name.</param>
        /// <returns></returns>
        /// <internal><para>5/17/2011: Sky</para></internal>
        bool Exists(string elementName);

        /// <summary>
        /// Creates the element.
        /// </summary>
        /// <param name="newElementSchema"></param>
        /// <internal><para>5/17/2011: Sky</para></internal>
        void Create(TElementDescription newElementSchema);

        /// <summary>
        /// Gets a list of the elements.
        /// </summary>
        /// <returns></returns>
        /// <internal><para>5/18/2011: Sky</para></internal>
        TElementWrapper[] List();

        /// <summary>
        /// Gets a list of the names of the elements.
        /// </summary>
        /// <returns></returns>
        /// <internal><para>5/18/2011: Sky</para></internal>
        string[] ListNames();



        /// <summary>
        /// Gets the specified element by its name.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        /// <returns></returns>
        TElementWrapper Get(string elementName);

        /// <summary>
        /// Update the definition of the Element.
        /// </summary>
        /// <param name="elementName"></param>
        /// <param name="newElementSchema"></param>
        void Update(string elementName, TElementDescription newElementSchema);


        /// <summary>
        /// Deletes the element with the given name.
        /// </summary>
        /// <param name="elementName">Name of the element.</param>
        /// <internal><para>5/17/2011: Sky</para></internal>
        void Delete(string elementName);

    }
}
