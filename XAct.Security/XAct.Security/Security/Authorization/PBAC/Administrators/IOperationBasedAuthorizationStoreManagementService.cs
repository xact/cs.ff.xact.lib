﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    public interface IOpBasedAuthorizationStoreManagementService :
        IOpBasedAuthorizationElementAdministrator<EntityDescription, IOpBasedAuthorizationStore>, IHasXActLibService
    {
    }
}
