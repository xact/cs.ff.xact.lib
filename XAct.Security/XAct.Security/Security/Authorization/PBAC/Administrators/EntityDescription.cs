﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    
    public class EntityDescription
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }

}