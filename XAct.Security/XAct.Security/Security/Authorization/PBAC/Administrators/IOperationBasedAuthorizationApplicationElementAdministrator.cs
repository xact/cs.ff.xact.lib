﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A specialization of 
    /// <see cref="IOpBasedAuthorizationElementAdministrator{TElementDescription,TElementWrapper}"/>
    /// (which provides CRUD operations),
    /// adding an Application property.
    /// <para>
    /// The base contract for an Operations based Authorization Element Administrator for a specific Application. 
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Implementations take as a constructor argument the 
    /// owner <see cref="IOpBasedAuthorizationStoreApplication"/>
    /// </para>
    /// </remarks>
    /// <internal>
    /// <para>
    /// Questions have been raised as to why I did not go for a more object
    /// oriented approach to the problem domain: the reason is that it's not a
    /// as simple as it looks trying to wrap 'n' systems: An Operation in one
    /// system will have different properties, and methods, than an Opertion
    /// in another. 
    /// </para>
    /// </internal>
    public interface IOpBasedAuthorizationApplicationElementAdministrator
    {
        ///// <summary>
        ///// The owner <see cref="IOpBasedAuthorizationStoreApplication"/>
        ///// </summary>
        //IOpBasedAuthorizationStoreApplication Application { get; }


        /// <summary>
        /// Get/Retrieve the element.
        /// </summary>
        /// <typeparam name="TVSElement">The type of the VS element.</typeparam>
        /// <param name="elementName">Name of the element.</param>
        /// <returns></returns>
        TVSElement InternalGet<TVSElement>(string elementName);

    }
}
