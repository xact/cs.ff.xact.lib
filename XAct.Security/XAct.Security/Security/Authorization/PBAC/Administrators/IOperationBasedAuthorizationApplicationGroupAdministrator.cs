﻿
namespace XAct.Security.Authorization.PBAC
{
    /// <summary>
    /// The contract for an Administrator of Application level Groups.
    /// (as opposed to a Store Level Groups).
    /// <para>
    /// Note that unlike Groups, there are no Scope level Opererations.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Implementations take as a Constructor argument the 
    /// owner <see cref="IOpBasedAuthorizationStoreApplication"/>
    /// </para>
    /// </remarks>
    /// <internal>
    /// <para>
    /// Questions have been raised as to why I did not go for a more object
    /// oriented approach to the problem domain: the reason is that it's not a
    /// as simple as it looks trying to wrap 'n' systems: An Operation in one
    /// system will have different properties, and methods, than an Opertion
    /// in another. 
    /// </para>
    /// </internal>
    /// <internal>
    /// Contract for a Manager to manage the groups within an Application
    /// (A group is collection of Groups and or Members)
    /// </internal>
    public interface IOpBasedAuthorizationApplicationGroupAdministrator : 
        IOpBasedAuthorizationElementAdministrator
            <EntityDescription, IOpBasedAuthorizationAppGroup>
    {


    }
}
