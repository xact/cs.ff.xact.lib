﻿
// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// An Operations based Authorization Role Administrator for a specific Application
    /// </summary>
    /// <remarks>
    /// <para>
    /// Implementations take as a Constructor argument the 
    /// owner <see cref="IOpBasedAuthorizationStoreApplication"/>
    /// </para>
    /// </remarks>
    /// <internal>
    /// <para>
    /// Questions have been raised as to why I did not go for a more object
    /// oriented approach to the problem domain: the reason is that it's not a
    /// as simple as it looks trying to wrap 'n' systems: An Operation in one
    /// system will have different properties, and methods, than an Opertion
    /// in another. 
    /// </para>
    /// </internal>
    public interface IOpBasedAuthorizationApplicationRoleAdministrator :
        IOpBasedAuthorizationElementAdministrator<EntityDescription, IOpBasedAuthorizationAppRole>
    {

        /// <summary>
        /// Gets the members of the Groups associated to the Application specific Role.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns></returns>
        /// <internal><para>5/17/2011: Sky</para></internal>
        string[] GetRoleMembers(string roleName);
    }
}
