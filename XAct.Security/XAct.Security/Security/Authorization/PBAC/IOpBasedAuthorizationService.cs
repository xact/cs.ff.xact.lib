﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.PBAC
// ReSharper restore CheckNamespace
{
    using System.Security.Principal;

    /// <summary>
    /// Contract for checking authorization 
    /// against an Operations/Permission based store.
    /// <para>
    /// Note that this interface is just for checking Permissions.
    /// </para>
    /// <para>
    /// Use <see cref="IOpBasedAuthorizationManagementService"/>
    /// to change the store itself.
    /// </para>
    /// </summary>
    /// <internal>
    /// I originally put this in XAct.Security -- 
    /// but it turned out to be just about the only 
    /// thing in the dll. Not really neded.
    /// </internal>
    /// <internal>
    /// Note: do not implement GetRoles,
    /// as that that sort of defeats the point 
    /// of granularity by Operations.
    /// </internal>
    public interface IOpBasedAuthorizationService : IHasXActLibService
    {


        /// <summary>
        /// Is the current identity allowed to perform the following operation?
        /// <para>
        /// Note: will work against current thread's IIdentity.
        /// </para>
        /// </summary>
        /// <internal>
        /// See AzMan operation.
        /// </internal>
        /// <param name="operationName"></param>
        /// <returns></returns>
        OpBasedAuthorization IsOperationAllowed(string operationName);

        /// <summary>
        /// Is the current identity allowed to perform the following operation?
        /// </summary>
        /// <internal>
        /// See AzMan operation.
        /// </internal>
        /// <param name="identity"></param>
        /// <param name="operationName"></param>
        /// <returns></returns>
        OpBasedAuthorization IsOperationAllowed(IIdentity identity, string operationName);

    }
}
