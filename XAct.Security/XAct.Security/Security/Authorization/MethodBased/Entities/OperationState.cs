﻿namespace XAct.Security.Authorization.MethodBased.Entities
{
    public enum OperationState
    {
        AllowAnonymous,
        AllowAuthenticated,
        AllowAuthorized,
    }
}
