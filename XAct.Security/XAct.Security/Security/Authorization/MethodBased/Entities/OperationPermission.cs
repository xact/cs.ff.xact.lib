﻿
namespace XAct.Security.Authorization
{
    /// <summary>
    /// An entity to describe a Permission related to an Operation
    /// in a many to many join.
    /// </summary>
    public class OperationPermission : IHasXActLibEntity, IHasId<int>, IHasEnabled, IHasOrder, IHasName, IHasDescription
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public virtual int Id { get; set; }


        /// <summary>
        /// Gets or sets the FK to the <see cref="Operation"/> that manages this <see cref="OperationPermission"/>.
        /// </summary>
        /// <value>
        /// The operation fk.
        /// </value>
        public virtual int OperationFK { get; set; }

        
        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this the operation
        /// is denied to Identities with the current Permission.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [deny]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Deny { get; set; }

        /// <summary>
        /// Gets the name of the Permission.
        /// <para>Member defined in<see cref="XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the optional description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public virtual string Description { get; set; }





    }
}
