﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Security.Principal;

    /// <summary>
    /// Entity to record the Roles required
    /// by the current <see cref="IPrincipal" />
    /// to access the specified Method/Operation.
    /// </summary>
    public class Operation : IHasXActLibEntity, IHasId<int>, IHasEnabled, IHasName, IHasNote 
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public virtual int Id { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Operation"/> is
        /// allowed to be accessed by Anonymous as well as Authenticated users.
        /// <para>
        /// Evaluated before <see cref="Enabled"/>
        /// </para>
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public virtual bool AllowAnonymous { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Operation"/> rule is enabled.
        /// <para>
        /// Attribute is checked after <paramref name="AllowAnonymous"/>.
        /// In other words, as <paramref name="AllowAnonymous"/> is false (or else
        /// we would not be evaluating this Attribute)
        /// if Enabled = false, it allows through users who are Authenticated
        /// without checking for Roles.
        /// if Enabled = true, it will expect an Authenticated user, 
        /// and will check against stated roles. 
        /// </para>
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Enabled { get; set; }



        /// <summary>
        /// Gets or sets the *case-sentitive* full name of the <see cref="Operation"/>
        /// (eg: 'MyNamespace.MyClass.MyMethod')
        /// <para>
        /// Eg: `App.Module.IService.MyMethod`
        /// </para>
        /// </summary>
        /// <value>
        /// The full name of the method.
        /// </value>
        public virtual string Name { get; set; }


        /// <summary>
        /// Gets or sets the optional note that helps describe the current permission condition.
        /// <para>
        /// Member defined in the <see cref="IHasNote" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The note.
        /// </value>
        public virtual string Note { get; set; }

        /// <summary>
        /// Gets the the collection of <see cref="OperationPermission"/>s.
        /// associated to this <see cref="Operation"/>.
        /// </summary>
        /// <value>
        /// The permissions.
        /// </value>
        public ICollection<OperationPermission> Permissions { get { return _permissions ?? (_permissions = new Collection<OperationPermission>()); } }
        private ICollection<OperationPermission> _permissions;

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {

            List<string> tmp = new List<string>();

            foreach (OperationPermission permission in this.Permissions)
            {
                if (permission.Enabled)
                {
                    tmp.Add(permission.Deny ? "!" + permission.Name : permission.Name);
                }
                else
                {
                    tmp.Add("^" + (permission.Deny ? "!" + permission.Name : permission.Name));
                }
            }

            var result = this.Name + ": [Roles:" + tmp.JoinSafely(",") + "]";
            return result;
        }
    }
}
