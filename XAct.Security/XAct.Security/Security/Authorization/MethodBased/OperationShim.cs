﻿namespace XAct.Security.Authorization
{
    using System.Linq;

    /// <summary>
    /// A wrapper around 
    /// offline (out of DbContext) instances of
    /// <see cref="Operation"/>.
    /// <para>
    /// NOTE:
    /// 
    /// The primary purpose is to return the <see cref="OperationPermission"/>s
    /// associated to the <see cref="Operation"/> in a specific Order (
    /// the Db only fills the <see cref="OperationPermission"/> ICollection
    /// in an indescriminate order.
    /// 
    /// </para>
    /// </summary>
    public class OperationShim 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OperationShim"/> class.
        /// </summary>
        /// <param name="operation">The operation.</param>
        public OperationShim(Operation operation)
        {
            Operation = operation;
        }
        public Operation Operation { get; private set; }

        /// <summary>
        /// Gets or sets the array of <see cref="OperationPermission"/>
        /// definitions defined for this <see cref="Operation"/>,
        /// ordered by the Permission's order.
        /// </summary>
        /// <value>
        /// The ordered permissions.
        /// </value>
        public OperationPermission[] OrderedPermissions
        {
            get
            {
                if (_orderedPermissions == null)
                {
                    lock (this)
                    {
                        _orderedPermissions = Operation.Permissions.OrderBy(p => p.Order).ToArray();
                    }
                }
                return _orderedPermissions;
            }
            set { _orderedPermissions = value; }
        }

        private OperationPermission[] _orderedPermissions;

    }
}