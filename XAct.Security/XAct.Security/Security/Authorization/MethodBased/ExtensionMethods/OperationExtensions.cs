// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Security.Authorization;

    public static class OperationExtensions
    {
        public static void SetPermissions(this Operation operation, string csvSeparatedPermissions)
        {

            //Split the string into individual permission names
            //that we will look to see if they already exist,
            //if they do, update, if they don't add.
            //Finally, remove those not mentioned in this string.
            string[] permissionTokens = (csvSeparatedPermissions != null) ? csvSeparatedPermissions.Split(new[] {';', '|', ','}) : new string[0];

            int position = 0;
            List<OperationPermission> mentioned = new List<OperationPermission>();
            List<OperationPermission> newRecords = new List<OperationPermission>();


            //For each token, go through existing permission set
            //and see if it can be found. 
            //If found, update it.
            //If not found, create it.
            foreach (string permissionToken in permissionTokens)
            {
                bool isNew;

                //Call private method, which returns the isNew
                //flag that we need in order to know if it existed already or not.
                OperationPermission operationPermission =
                    operation.SetPermission(permissionToken, out isNew, true, false, position);

                mentioned.Add(operationPermission);
                if (!isNew)
                {
                    newRecords.Add(operationPermission);
                }

                position += 1;
            }

            //Get list of OperationPermission objects that were in original list
            //that were not mentioned.

            
            /// Returns items missing from list1, found in list2 (original).

            List<OperationPermission> operationPermissionsToRemove =
                operation.Permissions.Except(
                    mentioned,
                    x => x.Name).ToList();

            //And remove them:
            foreach (OperationPermission operationPermission in operationPermissionsToRemove)
            {
                operation.Permissions.Remove(operationPermission);
            }
        }

        /// <summary>
        /// Sets the permission.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        /// <param name="isEnabled">if set to <c>true</c> [is enabled].</param>
        /// <param name="isDeny">if set to <c>true</c> [is deny].</param>
        /// <returns></returns>
        public static OperationPermission SetPermission(this Operation operation, string caseSensitivePermissionName,
                                                        bool isEnabled = true,
                                                        bool isDeny = false)
        {
            bool isNew;
            //Call private method, which returns the isNew:
            return operation.SetPermission(caseSensitivePermissionName,out isNew, isEnabled, isDeny);
        }


        public static void SetPermissionState(this Operation operation, string caseSensitivePermissionName,
                                              bool enabledState)
        {
            OperationPermission result =
                operation.Permissions.SingleOrDefault(p => p.Name == caseSensitivePermissionName);

            if (result == null)
            {
                return;
            }

            result.Enabled = enabledState;
        }


        public static void MovePermissionUp(this Operation operation, string caseSensitivePermissionName)
        {
            OperationPermission[] orderedPermissions;
            orderedPermissions = operation.Permissions.OrderBy(p => p.Order).ToArray();

            OperationPermission result = orderedPermissions.SingleOrDefault(p => p.Name == caseSensitivePermissionName);

            if (result == null)
            {
                return;
            }

            int indexOf = orderedPermissions.IndexOf(result);

            if (orderedPermissions.IndexOf(result) == 0)
            {
                //already at top,nowhere to move it:
                return;
            }

            //Get one higher up:
            OperationPermission result2 = orderedPermissions[indexOf - 1];

            int heldOrder = result.Order;
            result.Order = result2.Order;
            result2.Order = heldOrder;


            //Order has been set...but array is not in right order...watch out.

        }

        public static void MovePermissionDown(this Operation operation, string caseSensitivePermissionName)
        {
            OperationPermission[] orderedPermissions = operation.Permissions.OrderBy(p => p.Order).ToArray();

            OperationPermission result = orderedPermissions.SingleOrDefault(p => p.Name == caseSensitivePermissionName);

            if (result == null)
            {
                return;
            }

            int indexOf = orderedPermissions.IndexOf(result);
            if (orderedPermissions.IndexOf(result) == orderedPermissions.Length-1)
            {
                //already at bottom,nowhere to move it:
                return;
            }

            //Get one higher up:
            OperationPermission result2 = orderedPermissions[indexOf + 1];

            int heldOrder = result.Order;
            result.Order = result2.Order;
            result2.Order = heldOrder;

            //Order has been set...but array is not in right order...watch out.
        }

        public static OperationPermission[] RemovePermission(this Operation operation, string caseSensitivePermissionName)
        {
            OperationPermission[] removed =
                operation.Permissions.RemoveAll(p => p.Name == caseSensitivePermissionName);

            return removed;
        }











        //Internal method invoked by both 
        //SetPermissions and SetPermission above:
        private static OperationPermission SetPermission(this Operation operation, string caseSensitivePermissionName,
                                                        out bool isNew, bool isEnabled = true,
                                                        bool isDeny = false, int position = 0)
        {

            char firstChar = caseSensitivePermissionName[0];
            if (new[] { '^', '!' }.Contains(firstChar))
            {
                isDeny = true;
            }
            string name = (isDeny) ? caseSensitivePermissionName.Substring(1) : caseSensitivePermissionName;


            OperationPermission result =
                operation.Permissions.SingleOrDefault(p => p.Name == caseSensitivePermissionName);

            isNew = (result == null);

            result = new OperationPermission();

            if (isNew)
            {
                operation.Permissions.Add(result);
            }
            result.Enabled = isEnabled;
            result.Deny = isDeny;
            result.Name = name;
            if (position > -1)
            {
                result.Order = position;
            }


            return result;
        }


    }
}