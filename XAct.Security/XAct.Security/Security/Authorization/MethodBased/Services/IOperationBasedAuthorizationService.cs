﻿namespace XAct.Security.Authorization
{
    using System.Reflection;
    using System.Security.Principal;
    using XAct.Security.Authorization.MethodBased;

    public interface IOperationBasedAuthorizationService : IHasXActLibService
    {
        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Operation/Method, 
        /// </para>
        /// <para>
        /// * the method is decorated with <see cref="AllowAnonymousAttribute"/>
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.SetOperationAllowAnonymous"/> was invoked on that method,
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.AllowAccessWhenOperationNotMentionedInDataStore"/> has been set to true.
        /// </para>
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="authorisationResult"></param>
        /// <param name="principal"></param>
        bool IsAuthorized(MethodInfo methodInfo, out AuthorizationResult authorisationResult, IPrincipal principal = null);


        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Operation/Method, 
        /// </para>
        /// <para>
        /// * the method is decorated with <see cref="AllowAnonymousAttribute"/>
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementService.SetOperationAllowAnonymous"/> was invoked on that method,
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.AllowAccessWhenOperationNotMentionedInDataStore"/> has been set to true.
        /// </para>
        /// </summary>
        bool IsAuthorized(string caseSensitiveOperationFullName, out AuthorizationResult authorisationResult, bool allowAnonymous = false, IPrincipal principal = null);


        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Operation/Method, 
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementService.SetOperationAllowAnonymous"/> was invoked on that method,
        /// </para>
        /// <para>
        /// IMPORTANT: 
        /// with this override, 
        /// even if the method is decorated with <see cref="AllowAnonymousAttribute"/>, 
        /// if the user does not have sufficient permissions, access will be denied.
        /// </para>
        /// </summary>
        bool IsAuthorized(string caseSensitiveOperationFullName, out AuthorizationResult authorisationResult, IPrincipal principal = null);
    }
}