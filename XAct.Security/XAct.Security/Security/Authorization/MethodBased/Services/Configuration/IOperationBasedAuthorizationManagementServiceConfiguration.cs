﻿
namespace XAct.Security.Authorization.MethodBased
{
    /// <summary>
    /// Singleton configuration package for the
    /// <see cref="IOperationBasedAuthorizationManagementService"/>
    /// service
    /// </summary>
    public interface IOperationBasedAuthorizationManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// <para>
        /// By default, an authorisation service should not allow
        /// access to any facade method, unless is decorated with
        /// an attribute explictly allowing Anonymous access.
        /// </para>
        /// This should be always be false...
        /// but there are sometimes cases that developers 
        /// need a temporary work around, until security is sorted out. 
        /// </summary>
        bool AllowAccessWhenOperationNotMentionedInDataStore { get; set; }
    }
}
