﻿namespace XAct.Security.Authorization.MethodBased.Implementations
{
    using XAct.Services;

    /// <summary>
    /// A singleton implementation of the 
    /// <see cref="IOperationBasedAuthorizationManagementServiceConfiguration"/>
    /// contract for means to configure
    /// implementations of <see cref="IOperationBasedAuthorizationManagementService"/>
    /// </summary>
    public class OperationBasedAuthorizationManagementServiceConfiguration : IOperationBasedAuthorizationManagementServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        ///   <para>
        /// By default, an authorisation service should not allow
        /// access to any facade method, unless is decorated with
        /// an attribute explictly allowing Anonymous access.
        /// </para>
        /// This should be always be false...
        /// but there are sometimes cases that developers
        /// need a temporary work around, until security is sorted out.
        /// </summary>
        public bool AllowAccessWhenOperationNotMentionedInDataStore { get; set; }

    }
}