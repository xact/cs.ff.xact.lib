﻿namespace XAct.Security.Authorization
{
    using System.Reflection;
    using System.Security.Principal;
    using XAct.Security.Authorization.MethodBased;

    public interface IOperationBasedAuthorizationManagementService : IHasXActLibService
    {

        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IOperationBasedAuthorizationManagementServiceConfiguration Configuration { get; }

        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Method, 
        /// </para>
        /// <para>
        /// * the method is decorated with <see cref="AllowAnonymousAttribute"/>
        /// </para>
        /// <para>
        /// * <see cref="SetOperationAllowAnonymous"/> was invoked on that method,
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.AllowAccessWhenOperationNotMentionedInDataStore"/> has been set to true.
        /// </para>
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="authorisationResult"></param>
        /// <param name="principal"></param>
        bool IsAuthorized(MethodInfo methodInfo, out AuthorizationResult authorisationResult, IPrincipal principal = null);
        
        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Method, 
        /// </para>
        /// <para>
        /// * the method is decorated with <see cref="AllowAnonymousAttribute"/>
        /// </para>
        /// <para>
        /// * <see cref="SetOperationAllowAnonymous"/> was invoked on that method,
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.AllowAccessWhenOperationNotMentionedInDataStore"/> has been set to true.
        /// </para>
        /// </summary>
        bool IsAuthorized(string caseSensitiveOperationFullName, out AuthorizationResult authorisationResult, bool allowAnonymous = false, IPrincipal principal = null);

        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Method, 
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.AllowAccessWhenOperationNotMentionedInDataStore"/> has been set to true.
        /// </para>
        /// <para>
        /// IMPORTANT: 
        /// with this override, 
        /// even if the method is decorated with <see cref="AllowAnonymousAttribute"/>, 
        /// if the user does not have sufficient permissions, access will be denied.
        /// </para>
        /// </summary>
        bool IsAuthorized(string caseSensitiveOperationFullName, out AuthorizationResult authorisationResult, IPrincipal principal = null);


        /// <summary>
        /// Remove a method name from the permission matrix.
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        void RemoveOperation(string caseSensitiveOperationFullName);

        /// <summary>
        /// Gives anonumous access to a specific method
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        /// <param name="allowAnonymous">if set to <c>true</c> [allow anonymous].</param>
        void SetOperationAllowAnonymous(string caseSensitiveOperationFullName, bool allowAnonymous);

        /// <summary>
        /// Sets the (enabled) 'Allow' permissions required to access a specific Operation/Method.
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="csvCaseSensitivePermissionNames">The CSV case sensitive permission names.</param>
        void SetOperationPermissions(string caseSensitiveOperationFullName, string csvCaseSensitivePermissionNames);


        /// <summary>
        /// Associates an (enabled) 'Disallow' permissions to the Operation/Method.
        /// <para>
        /// REMINDER: 
        /// </para>
        /// <para>
        /// When checking Authorization, the system iterates through the Permissions associated to the Operation. 
        /// The first Allow permission that the Principal is a member of, is deemed sufficient.
        /// </para>
        /// <para>
        /// The exception to this is if it *first* encounters a Deny permission that the user meets, in which 
        /// case it is immediately deemed as not allowed access.
        /// </para>
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        /// <param name="isEnabled">if set to <c>true</c> [is enabled].</param>
        /// <param name="isDeny">if set to <c>true</c> [is deny].</param>
        void SetOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName, bool isEnabled = true, bool isDeny = false);


        /// <summary>
        /// Removes a single Permission from the Operation/Method.
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        void RemoveOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName);

        /// <summary>
        /// Enables a single Permission from the Operation/Method.
        /// <para>
        /// See <see cref="DisableOperationPermission"/>
        /// </para>
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        void EnableOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName);


        /// <summary>
        /// Disables a single Permission from the Operation/Method.
        /// <para>
        /// See <see cref="EnableOperationPermission"/>
        /// </para>
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        void DisableOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName);

        /// <summary>
        /// Removes an operation associated to a method.
        /// </summary>
        /// <param name="caseSensitiveOperationFullName"></param>
        /// <param name="caseSensitivePermissionName"></param>
        void DeleteOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName);

        /// <summary>
        /// Moves the permission for a specific operation, up.
        /// <para>
        /// Use to place `Deny` Permissions before `Allow` Permissions.
        /// </para>
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        void MoveOperationPermissionUp(string caseSensitiveOperationFullName, string caseSensitivePermissionName);

        /// <summary>
        /// Moves the permission for a specific operation, down.
        /// <para>
        /// Use to place `Deny` Permissions before `Allow` Permissions.
        /// </para>
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        void MoveOperationPermissionDown(string caseSensitiveOperationFullName, string caseSensitivePermissionName);
        



    }
}
