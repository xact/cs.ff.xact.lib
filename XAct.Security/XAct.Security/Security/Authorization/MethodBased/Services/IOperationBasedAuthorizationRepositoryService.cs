﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization
// ReSharper restore CheckNamespace
{
    using XAct.Environment;

    /// <summary>
    /// Repository for retrieving <see cref="Operation"/>
    /// entities.
    /// <para>
    /// Invoked by <see cref="IOperationBasedAuthorizationManagementService"/>
    /// </para>
    /// </summary>
    public interface IOperationBasedAuthorizationRepositoryService : IHasXActLibService
    {


        /// <summary>
        /// Gets all the rules that can apply to this Application
        /// (see <see cref="IEnvironmentService.ApplicationName"/>
        /// <para>
        /// The rules are intended to be cached in the infrastructure layer
        /// in order to not have to hit the database on every method.
        /// </para>
        /// </summary>
        /// <returns></returns>
        Operation[] GetAllEnabled();


        /// <summary>
        /// Gets all the rules that can apply to this given Application.
        /// <para>
        /// The rules are intended to be cached in the infrastructure layer
        /// in order to not have to hit the database on every method.
        /// </para>
        /// </summary>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        Operation[] GetAllEnabled(string applicationName);


        Operation GetByName(string caseSensitiveOperationFullName);


        /// <summary>
        /// Persists the given <see cref="Operation" />
        /// </summary>
        /// <param name="operationRoleBasedAuthorization">The operation role based authorisation.</param>
        /// <param name="commitImmediately">if set to <c>true</c> [commit immediately].</param>
        void Persist(Operation operationRoleBasedAuthorization, bool commitImmediately = true);


        void Delete(string caseSensitiveOperationFullName, bool commitImmediately = true);


        void DeletePermission(OperationPermission operationPermission, bool commitImmediately = true);

    }
}
