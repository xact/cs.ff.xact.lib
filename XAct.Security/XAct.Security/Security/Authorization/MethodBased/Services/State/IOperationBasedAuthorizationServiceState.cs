﻿namespace XAct.Security.Authorization
{
    using System.Collections.Generic;

    /// <summary>
    /// Cache of 
    /// operation names to <see cref="OperationShim"/>, which provides
    /// information about the <see cref="Operation"/>, and its 
    /// ordered <see cref="OperationPermission"/> array.
    /// </summary>
    public interface IOperationBasedAuthorizationServiceState : IDictionary<string, OperationShim>, IHasXActLibServiceState
    {

    }
}