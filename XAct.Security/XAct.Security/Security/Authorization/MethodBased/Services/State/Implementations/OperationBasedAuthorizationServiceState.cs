﻿namespace XAct.Security.Authorization.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Services;


    /// <summary>
    /// An implementation of the 
    /// <see cref="IOperationBasedAuthorizationServiceState"/>
    /// to persist (offline/DbCOntext disconnected) information about each 
    /// <see cref="Operation"/> and it's associated - ordered - <see cref="OperationPermission"/> array.
    /// </summary>
    public class OperationBasedAuthorizationServiceState : Dictionary<string, OperationShim>, IOperationBasedAuthorizationServiceState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OperationBasedAuthorizationServiceState"/> class.
        /// </summary>
        public OperationBasedAuthorizationServiceState():base(StringComparer.InvariantCultureIgnoreCase)
        {
            
        }
    }
}
