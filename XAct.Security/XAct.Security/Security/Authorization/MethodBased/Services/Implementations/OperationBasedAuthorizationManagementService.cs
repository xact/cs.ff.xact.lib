// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.Implementations
// ReSharper restore CheckNamespace
{
    using System.Reflection;
    using System.Security.Principal;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Security.Authorization.MethodBased;
    using XAct.Services;


    /// <summary>
    /// 
    /// </summary>
    public class OperationBasedAuthorizationManagementService : XActLibServiceBase, IOperationBasedAuthorizationManagementService
    {
        private readonly object _lock =new object();

        private readonly IPrincipalService _principalService;
        private readonly IEnvironmentService _environmentService;
        private readonly IOperationBasedAuthorizationManagementServiceConfiguration _operationBasedAuthorizationManagementServiceConfiguration;
        private readonly IOperationBasedAuthorizationServiceState _operationBasedAuthorizationServiceState;
        private readonly IOperationBasedAuthorizationRepositoryService _authorisationRepositoryService;


        public readonly bool DefaultValueIfMethodNotFound=true;
        public readonly bool DefaultValueIfPermissionNotFound = false;


        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IOperationBasedAuthorizationManagementServiceConfiguration Configuration { get { return _operationBasedAuthorizationManagementServiceConfiguration; } }


        /// <summary>
        /// Initializes a new instance of the <see cref="OperationBasedAuthorizationManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="operationBasedAuthorizationManagementServiceConfiguration">The operation based authorization management service configuration.</param>
        /// <param name="operationBasedAuthorizationCache">The operation based authorisation cache.</param>
        /// <param name="authorisationRepositoryService">The authorisation repository service.</param>
        public OperationBasedAuthorizationManagementService(ITracingService tracingService, IPrincipalService principalService, IEnvironmentService environmentService,
            IOperationBasedAuthorizationManagementServiceConfiguration operationBasedAuthorizationManagementServiceConfiguration,
            IOperationBasedAuthorizationServiceState operationBasedAuthorizationCache,
            IOperationBasedAuthorizationRepositoryService authorisationRepositoryService)
            : base(tracingService)
        {
            _principalService = principalService;
            _environmentService = environmentService;
            _operationBasedAuthorizationManagementServiceConfiguration = operationBasedAuthorizationManagementServiceConfiguration;
            _operationBasedAuthorizationServiceState = operationBasedAuthorizationCache;
            _authorisationRepositoryService = authorisationRepositoryService;

            LoadCache();

        }

        public bool IsAuthorized(MethodInfo methodInfo, out AuthorizationResult authorisationResult, IPrincipal principal = null)
        {
            //Note that we are using our GetAttributeRecursively, 
            //because the default .NET solution doesn't work as well, 
            //and won't see Attributes on base interfaces...which we normally need.
            bool allowAnonymous = (methodInfo.GetAttributeRecursively<AllowAnonymousAttribute>(true) !=null);


            //If it allows anonymous, we don't have to look up anything in any database.
            if (allowAnonymous)
            {
                authorisationResult = AuthorizationResult.AnonymousPermitted;
                return true;
            }

            if (principal == null)
            {
                principal = _principalService.Principal ;
            }

            string operationName = methodInfo.FullName();

            //TODO: Optimise using a cache:
            AuthorizationAttribute authorisationByAopAttribute =
                methodInfo.GetAttributeRecursively<AuthorizationAttribute>(true);

            if (authorisationByAopAttribute != null)
            {

                if (!authorisationByAopAttribute.Name.IsNullOrEmpty())
                {
                    operationName = authorisationByAopAttribute.Name;
                }

                bool result;

                if (authorisationByAopAttribute.TryCheck(principal, out result, out authorisationResult))
                {
                    return result;
                }
            }



            return IsAuthorized(operationName, out authorisationResult, allowAnonymous, principal);
        }

        public bool IsAuthorized(string caseSensitiveOperationFullName, out AuthorizationResult authorisationResult, bool allowAnonymous=false, IPrincipal principal = null)
        {
            //If it allows anonymous, we don't have to look up anything in any database.
            if (allowAnonymous)
            {
                authorisationResult = AuthorizationResult.Authorized;
                return true;
            }

            //Everything past this point means that the author has to be authenticated.

            return IsAuthorized(caseSensitiveOperationFullName,out authorisationResult, principal);
        }

        public bool IsAuthorized(string caseSensitiveOperationFullName, out AuthorizationResult authorisationResult, IPrincipal principal = null)
        {
            if (principal == null)
            {
                principal = _principalService.Principal ;
            }

            //Get the roles specified for the Method.
            OperationShim operationShim;
            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                //No OperationShim (ie Operation) found, 
                //so determine whether wwe should by default allow, or not allow.

                bool result = _operationBasedAuthorizationManagementServiceConfiguration
                    .AllowAccessWhenOperationNotMentionedInDataStore;

                if (result)
                {
                    //Whether he is also authenticated and has permissions, we don't care.
                    //we just report that he has access:
                    authorisationResult = AuthorizationResult.AnonymousPermitted;
                }
                else
                {
                    authorisationResult = 
                        !principal.Identity.IsAuthenticated
                        ? AuthorizationResult.NotAuthenticated
                        : AuthorizationResult.NotAuthorized;
                }

                //That's it, since we have no other information to work on...
                return result;
            }

            //Alternatively, we have an Operation entry to work with:
            Operation operation = operationShim.Operation;


            //A db entry can also be marked as anoymous (ie doesn't have to always be via Attributes).
            //Again, we don't need ot bother with checking permissions if the Operation
            //is marked in the db as allowing anonymous:
            if (operation.AllowAnonymous)
            {
                authorisationResult = AuthorizationResult.AnonymousPermitted;
                return true;
            }


            //As we are not saying that the entry allows anonymous,
            //Everything past this point means that the author has to be authenticated.
            if (!principal.Identity.IsAuthenticated)
            {
                authorisationResult = AuthorizationResult.NotAuthenticated;
                return false;
            }

            if (!operation.Enabled)
            {
                //Enabled: Can't check this before AllowAnonymous if it's not the same answer as AllowAnonymous.
                //If not same as Anonymous, must be after check for Authenticated

                //We are saying it must be Authenticated, but that's all, no role checking:
                authorisationResult = AuthorizationResult.Authorized;
                return true;
            }



            //Iterate through each OperationPermission associated to the Operation:
            foreach (OperationPermission permission in operationShim.OrderedPermissions)
            {
                if (!permission.Enabled)
                {
                    //We don't take into account the permissions that are disabled:
                    continue;
                }

                //First check for exclusions:
                if (permission.Deny)
                {
                    if (principal.IsInRole(permission.Name))
                    {
                        //Straight out of the game...
                        authorisationResult = AuthorizationResult.NotAuthorized;
                        return false;
                    }
                }

                //Then inclusions:
                if (principal.IsInRole(permission.Name))
                {
                    authorisationResult = AuthorizationResult.Authorized;
                    return true;
                }
                //Loop to next permission.
            }


            //as the list was zero lengthed.
            //or all OperationPermission were Enabled=false (ie zero-lengthed)
            //or was not zero lengthed, but not Accepted or Denied.
            //so it's not allowed.

            authorisationResult = AuthorizationResult.NotAuthorized;
            return false;
        }



        /// <summary>
        /// Gives anonumous access to a specific method
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// state cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="allowAnonymous">if set to <c>true</c> [allow anonymous].</param>
        public void SetOperationAllowAnonymous(string caseSensitiveOperationFullName, bool allowAnonymous)
        {
            OperationShim operationShim;
            Operation operation;

            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                operation = new Operation
                {
                    Enabled = true,
                    Name = caseSensitiveOperationFullName
                };

                operationShim = new OperationShim(operation);
                lock (_lock)
                {
                    _operationBasedAuthorizationServiceState[caseSensitiveOperationFullName] = operationShim;
                }
            }
            operation = operationShim.Operation;

            operation.AllowAnonymous = allowAnonymous;

            operationShim.OrderedPermissions = null;
            _authorisationRepositoryService.Persist(operation);
        }



        /// <summary>
        /// Sets the (enabled) 'Allow' permissions required to access a specific Operation/Method.
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="csvCaseSensitivePermissionNames">The CSV case sensitive permission names.</param>
        public void SetOperationPermissions(string caseSensitiveOperationFullName, string permissions)
        {
            OperationShim operationShim;
                Operation operation;

            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                operation = new Operation
                    {
                        Enabled = true,
                        Name = caseSensitiveOperationFullName
                    };

                operationShim = new OperationShim(operation);
                lock (_lock)
                {
                    _operationBasedAuthorizationServiceState[caseSensitiveOperationFullName] = operationShim;
                }
            }
            operation = operationShim.Operation;

            operation.SetPermissions(permissions);

            operationShim.OrderedPermissions = null;
            _authorisationRepositoryService.Persist(operation);
        }

        /// <summary>
        /// Associates an (enabled) 'Disallow' permissions to the Operation/Method.
        /// <para>
        /// REMINDER: 
        /// </para>
        /// <para>
        /// When checking Authorization, the system iterates through the Permissions associated to the Operation. 
        /// The first Allow permission that the Principal is a member of, is deemed sufficient.
        /// </para>
        /// <para>
        /// The exception to this is if it *first* encounters a Deny permission that the user meets, in which 
        /// case it is immediately deemed as not allowed access.
        /// </para>
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        /// <param name="isEnabled">if set to <c>true</c> [is enabled].</param>
        /// <param name="isDeny">if set to <c>true</c> [is deny].</param>
        public void SetOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName, 
                                    bool isEnabled = true, bool isDeny = false)
        {
            OperationShim operationShim;

            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                return;
            }
            lock (_lock)
            {
                //Lock is required, as could be adding new item to array:
                operationShim.Operation.SetPermission(caseSensitivePermissionName, isEnabled, isDeny);
            }
            operationShim.OrderedPermissions = null;
            _authorisationRepositoryService.Persist(operationShim.Operation);


        }

        /// <summary>
        /// Removes a single Permission from the Operation/Method.
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// state cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        public void RemoveOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName)
        {
            OperationShim operationShim;

            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                return;
            }

            Operation operation = operationShim.Operation;

            lock (_lock)
            {
                //Lock is required, as removing new item to array:
                
               foreach(OperationPermission operationPermission in operation.RemovePermission(caseSensitivePermissionName))
                {
                    _authorisationRepositoryService.DeletePermission(operationPermission,false);
                }
            }

            operationShim.OrderedPermissions = null;
            _authorisationRepositoryService.Persist(operation);
        }



        /// <summary>
        /// Remove a method name from the permission matrix.
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// state cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        public void RemoveOperation(string caseSensitiveOperationFullName)
        {
            lock (_lock)
            {
                _authorisationRepositoryService.Delete(caseSensitiveOperationFullName);
                _operationBasedAuthorizationServiceState.Remove(caseSensitiveOperationFullName);
            }
        }



        /// <summary>
        /// Enables a single Permission from the Operation/Method.
        /// <para>
        /// See <see cref="DisableOperationPermission"/>
        /// </para>
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// state cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        public void EnableOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName)
        {
            SetOperationPermissionState(caseSensitiveOperationFullName, caseSensitivePermissionName, true);
        }
        /// <summary>
        /// Disable a single Permission from the Operation/Method.
        /// <para>
        /// See <see cref="DisableOperationPermission"/>
        /// </para>
        /// <para>
        /// Updates both the in-memory
        /// <see cref="IOperationBasedAuthorizationServiceState"/>
        /// state cache, as well as the database from which it was loaded.
        /// </para> 
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Case sensitive Full name of the operation/method.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        public void DisableOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName)
        {

            SetOperationPermissionState(caseSensitiveOperationFullName, caseSensitivePermissionName, false);
        }

        /// <summary>
        /// Sets the state of the operation permission.
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        /// <param name="enabledState">if set to <c>true</c> [enabled state].</param>
        public void SetOperationPermissionState(string caseSensitiveOperationFullName,
                                                string caseSensitivePermissionName, bool enabledState)
        {

            OperationShim operationShim;
            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                return;
            }

            Operation operation = operationShim.Operation;

            operation.SetPermissionState(caseSensitivePermissionName, enabledState);

            _authorisationRepositoryService.Persist(operation);
        }


        /// <summary>
        /// Moves the permission for a specific operation, up.
        /// <para>
        /// Use to place `Deny` Permissions before `Allow` Permissions.
        /// </para>
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        public void MoveOperationPermissionUp(string caseSensitiveOperationFullName, string caseSensitivePermissionName)
        {

            OperationShim operationShim;
            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                return;
            }

            Operation operation = operationShim.Operation;
            
            lock (_lock)
            {
                operation.MovePermissionUp(caseSensitivePermissionName);
                operationShim.OrderedPermissions = null;
            }

            _authorisationRepositoryService.Persist(operation);
        }

        /// <summary>
        /// Moves the permission for a specific operation, down.
        /// <para>
        /// Use to place `Deny` Permissions before `Allow` Permissions.
        /// </para>
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the case sensitive operation.</param>
        /// <param name="caseSensitivePermissionName">Name of the case sensitive permission.</param>
        public void MoveOperationPermissionDown(string caseSensitiveOperationFullName, string caseSensitivePermissionName)
        {

            OperationShim operationShim;
            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                return;
            }

            Operation operation = operationShim.Operation;

            lock (_lock)
            {
                operation.MovePermissionDown(caseSensitivePermissionName);
                operationShim.OrderedPermissions = null;
            }
            _authorisationRepositoryService.Persist(operation);
        }


        /// <summary>
        /// Removes an operation associated to a method.
        /// </summary>
        /// <param name="caseSensitiveOperationFullName"></param>
        /// <param name="caseSensitivePermissionName"></param>
        public void DeleteOperationPermission(string caseSensitiveOperationFullName, string caseSensitivePermissionName)
        {

            OperationShim operationShim;
            if (!_operationBasedAuthorizationServiceState.TryGetValue(caseSensitiveOperationFullName, out operationShim))
            {
                return;
            }
            
            Operation operation = operationShim.Operation;

            lock (_lock)
            {
                operation.RemovePermission(caseSensitivePermissionName);
            }
            _authorisationRepositoryService.Persist(operation);
        }





        /// <summary>
        /// Invoked from constructor:
        /// </summary>
        private void LoadCache()
        {
            lock (_lock)
            {
                _operationBasedAuthorizationServiceState.Clear();

                foreach (Operation operation in _authorisationRepositoryService.GetAllEnabled())
                {
                    _operationBasedAuthorizationServiceState.Add(operation.Name, new OperationShim(operation));
                }
            }
        }





    }
}