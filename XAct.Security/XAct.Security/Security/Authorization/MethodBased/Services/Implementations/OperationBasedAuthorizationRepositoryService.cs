﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.Implementations
// ReSharper restore CheckNamespace
{
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IOperationBasedAuthorizationRepositoryService"/>
    /// to retrieve <see cref="Operation"/> entities.
    /// </summary>
    public class OperationBasedAuthorizationRepositoryService : XActLibServiceBase, IOperationBasedAuthorizationRepositoryService
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationBasedAuthorizationRepositoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public OperationBasedAuthorizationRepositoryService(
            ITracingService tracingService, 
            IEnvironmentService environmentService, 
            IRepositoryService repositoryService) : base(tracingService)
        {
            _environmentService = environmentService;
            _repositoryService = repositoryService;
        }



        /// <summary>
        /// Persists the given <see cref="Operation" />
        /// </summary>
        /// <param name="operationRoleBasedAuthorization">The operation role based authorisation.</param>
        /// <param name="commitImmediately">if set to <c>true</c> [commit immediately].</param>
        public void Persist(Operation operationRoleBasedAuthorization, bool commitImmediately=true)
        {
            _repositoryService.PersistOnCommit(operationRoleBasedAuthorization, x=>x.Id == 0);

            if (commitImmediately)
            {
                _repositoryService.GetContext().Commit(CommitType.Default);
            }
            
        }


        public void Delete(string caseSensitiveOperationFullName, bool commitImmediately = true)
        {
            _repositoryService.DeleteOnCommit<Operation>(x=>x.Name == caseSensitiveOperationFullName);

            if (commitImmediately)
            {
                _repositoryService.GetContext().Commit(CommitType.Default);
            }
        }

        public void DeletePermission(OperationPermission operationPermission, bool commitImmediately = true)
        {
            _repositoryService.DeleteOnCommit(operationPermission);

            if (commitImmediately)
            {
                _repositoryService.GetContext().Commit(CommitType.Default);
            }
        }


        /// <summary>
        /// Gets the <see cref="Operation"/> by name.
        /// </summary>
        /// <param name="caseSensitiveOperationFullName">Full name of the method.</param>
        /// <returns></returns>
        public Operation GetByName(string caseSensitiveOperationFullName)
        {
            //Get a single record 
            return _repositoryService.GetSingle <Operation>(x => x.Name == caseSensitiveOperationFullName);
        }

        /// <summary>
        /// Gets all the rules that can apply to this Application
        /// (see <see cref="IEnvironmentService.ApplicationName" />
        /// <para>
        /// The rules are intended to be cached in the infrastructure layer
        /// in order to not have to hit the database on every method.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public Operation[] GetAllEnabled()
        {
            var result = GetAllEnabled(_environmentService.ApplicationName);
            return result;
        }

        /// <summary>
        /// Gets all the rules that can apply to this given Application.
        /// <para>
        /// The rules are intended to be cached in the infrastructure layer
        /// in order to not have to hit the database on every method.
        /// </para>
        /// </summary>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        public Operation[] GetAllEnabled(string applicationName)
        {
            //Order the rules so that the rules that actually have an application
            //name matching the current application are returned before rules that
            //have no application name.
            Operation[] results = _repositoryService.GetByFilter<Operation>(
                x => x.Enabled,
                new IncludeSpecification<Operation>(o=>o.Permissions)
                , 
                null,
                x => x.OrderBy(y=>y.Name))
                .ToArray();

            return results;
        }

    }
}