﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Authorization.Implementations
// ReSharper restore CheckNamespace
{
    using System.Reflection;
    using System.Security.Principal;
    using XAct.Diagnostics;
    using XAct.Security.Authorization.MethodBased;
    using XAct.Services;

    public class OperationBasedAuthorizationService : XActLibServiceBase,IOperationBasedAuthorizationService
    {
        private readonly IOperationBasedAuthorizationManagementService _authorisationManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationBasedAuthorizationService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="authorisationManagementService">The authorisation management service.</param>
        public OperationBasedAuthorizationService(ITracingService tracingService, IOperationBasedAuthorizationManagementService authorisationManagementService)
            : base(tracingService)
        {
            _authorisationManagementService = authorisationManagementService;
        }




        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Operation/Method,
        /// </para>
        /// <para>
        /// * the method is decorated with <see cref="AllowAnonymousAttribute" />
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.SetOperationAllowAnonymous" /> was invoked on that method,
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.AllowAccessWhenOperationNotMentionedInDataStore" /> has been set to true.
        /// </para>
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="authorisationResult"></param>
        /// <param name="principal"></param>
        /// <returns></returns>
        public bool IsAuthorized(MethodInfo methodInfo, out AuthorizationResult authorisationResult,
                                 IPrincipal principal = null)
        {
            return _authorisationManagementService.IsAuthorized(methodInfo, out authorisationResult, principal);
        }

        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Operation/Method,
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementService.SetOperationAllowAnonymous" /> was invoked on that method,
        /// </para>
        /// <para>
        /// IMPORTANT:
        /// with this override,
        /// even if the method is decorated with <see cref="AllowAnonymousAttribute" />,
        /// if the user does not have sufficient permissions, access will be denied.
        /// </para>
        /// </summary>
        /// <param name="caseSensitiveOperationFullName"></param>
        /// <param name="authorisationResult"></param>
        /// <param name="principal"></param>
        /// <returns></returns>
        public bool IsAuthorized(string caseSensitiveOperationFullName, out AuthorizationResult authorisationResult,
                                 IPrincipal principal = null)
        {
            return _authorisationManagementService.IsAuthorized(caseSensitiveOperationFullName, out authorisationResult, principal);
        }

        /// <summary>
        /// Determines if:
        /// <para>
        /// * the current Principal has sufficient permissions to access the given Operation/Method,
        /// </para>
        /// <para>
        /// * the method is decorated with <see cref="AllowAnonymousAttribute" />
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementService.SetOperationAllowAnonymous" /> was invoked on that method,
        /// </para>
        /// <para>
        /// * <see cref="IOperationBasedAuthorizationManagementServiceConfiguration.AllowAccessWhenOperationNotMentionedInDataStore" /> has been set to true.
        /// </para>
        /// </summary>
        /// <param name="caseSensitiveOperationFullName"></param>
        /// <param name="authorisationResult"></param>
        /// <param name="allowAnonymous"></param>
        /// <param name="principal"></param>
        /// <returns></returns>
        public bool IsAuthorized(string caseSensitiveOperationFullName, out AuthorizationResult authorisationResult,
                                 bool allowAnonymous = false, IPrincipal principal = null)
        {
            return _authorisationManagementService.IsAuthorized(caseSensitiveOperationFullName, out authorisationResult,allowAnonymous, principal);
        }

    }
}