namespace XAct.Security.Authorization.Implementations
{
    using System.Security.Principal;

    public static class AuthorizationByAOPAttributeExtensions
    {
        public static bool TryCheck(this AuthorizationAttribute authorisationByAopAttribute, IPrincipal principal, out bool result,  out AuthorizationResult authorisationResult)
        {
            result = false;
            authorisationResult = AuthorizationResult.Undefined;

            if (authorisationByAopAttribute == null)
            {
                return false;
            }

            if (principal == null)
            {
                return false;
            }

            if (!authorisationByAopAttribute.Enabled)
            {
                return false;
            }

            if (authorisationByAopAttribute.Roles.IsNullOrEmpty())
            {
                return false;

            }

            string[] roles = authorisationByAopAttribute.Roles.Split(';', '|', ',');
            
            foreach (string role in roles)
            {
                string[] roleNameParts = role.Split(':');
                string roleName = roleNameParts[0];
                bool roleIsAcceptRole = (roleNameParts.Length <= 1) || (roleNameParts[1].ToUpperInvariant() != "D");
                
                if (!principal.IsInRole(roleName))
                {
                    continue;
                }

                if (roleIsAcceptRole)
                {
                    authorisationResult = AuthorizationResult.Authorized;
                    result = true;
                    return true;
                }
                else
                {
                    //It's a Deny role:
                    authorisationResult = AuthorizationResult.NotAuthorized;
                    result =false;
                    return true;
                }
            }
            //Never found:
            return false;
        }
    }
}