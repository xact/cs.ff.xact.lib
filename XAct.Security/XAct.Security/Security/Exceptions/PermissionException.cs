﻿namespace XAct.Security
{
    using System;

    /// <summary>
    /// An exception raised when the current Identity
    /// does not have enough Roles/Permissions to complete
    /// a requested Exception.
    /// </summary>
    public class PermissionException : NotAuthorizedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public PermissionException(string message):base(message){}
        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public PermissionException(string message, Exception innerException) : base(message, innerException) { }
        ///// <summary>
        ///// Initializes a new instance of the <see cref="PermissionException"/> class.
        ///// </summary>
        ///// <param name="serializationInfo">The serialization info.</param>
        ///// <param name="streamingContext">The streaming context.</param>
        //public PermissionException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }
}
