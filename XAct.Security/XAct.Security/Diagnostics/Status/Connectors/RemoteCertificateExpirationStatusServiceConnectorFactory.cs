﻿namespace XAct.Diagnostics.Status.Connectors
{
    using XAct.Diagnostics.Status.Connectors.Implementations;

    public class RemoteCertificateExpirationStatusServiceConnectorFactory
    {
        public static RemoteCertificateExpirationStatusConnector Create(string name, string title, string description,
                                                                        params RemoteCertificateExpirationStatusConnectorConfigurationItem[] certInfos)
        {
            var statusServiceConnector =
                XAct.DependencyResolver.Current.GetInstance<RemoteCertificateExpirationStatusConnector>();

            statusServiceConnector.ConfigureBasicInformation(name, title, description);

            statusServiceConnector.Configuration.RemoteSslUrls.Add(certInfos);

            return statusServiceConnector;
        }
    }
}