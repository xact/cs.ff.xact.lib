﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;

    public class RemoteCertificateExpirationStatusConnectorConfigurationItem : IHasTitle
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public TimeSpan Timeout { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteCertificateExpirationStatusConnectorConfigurationItem"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="url">The URL.</param>
        public RemoteCertificateExpirationStatusConnectorConfigurationItem(string title, string url, TimeSpan? timeout=null)
        {
            Title = title;
            Url = url;
            if ((!timeout.HasValue) || (timeout.Value.TotalMilliseconds <= 0))
            {
                timeout = TimeSpan.FromSeconds(5);
            }
            Timeout = timeout.Value;
        }
    }
}