﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    /// <summary>
    /// A reference to a local Cert to find and check.
    /// </summary>
    public class LocalCertificateExpirationStatusConnectorConfigurationItem:IHasTitle
    {

        /// <summary>
        /// A displayable title to use.
        /// Often just set to the same value as the cert reference, 
        /// but you have the option to use something else.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The unique Subject line (if the <see cref="ReferenceType"/> is Subject), 
        /// etc. 
        /// </summary>
        public string Reference { get; set; }


        /// <summary>
        /// The means by which to reference the cert (Subject, etc.)
        /// </summary>
        public LocalCertificateReferenceType ReferenceType { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="LocalCertificateExpirationStatusConnectorConfigurationItem"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="reference">The reference.</param>
        /// <param name="referenceType">Type of the reference.</param>
        public LocalCertificateExpirationStatusConnectorConfigurationItem(string title, string reference,
                                         LocalCertificateReferenceType referenceType =
                                             LocalCertificateReferenceType.Subject)
        {
            Title = title;

            Reference = reference;
            if (title.IsNullOrEmpty())
            {
                Title = Reference;
            }

            ReferenceType = referenceType;
        }
    }
}