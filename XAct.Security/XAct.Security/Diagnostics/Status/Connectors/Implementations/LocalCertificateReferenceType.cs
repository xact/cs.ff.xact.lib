﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    /// <summary>
    /// The type / meaning of the <see cref="LocalCertificateExpirationStatusConnectorConfigurationItem.Reference"/>
    /// </summary>
    public enum LocalCertificateReferenceType
    {
        /// <summary>
        /// An error state.
        /// </summary>
        Undefined=0,
        /// <summary>
        /// Search by Subject.
        /// </summary>
        Subject=1,

        SubjectDistinguishedName=2,

        Thumbprint=3
    }
}