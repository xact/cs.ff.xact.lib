﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A configuration package for instances of
    /// <see cref="CertificateExpirationStatusConnector"/>
    /// </summary>
    public class LocalCertificateExpirationStatusConnectorConfiguration : IHasTransientBindingScope
    {
        /// <summary>
        /// The amount of time prior to a certs expiration that should be considered a warning.
        /// </summary>
        public TimeSpan WarningTimeSpan { get; set; }

        /// <summary>
        /// The Subjects of the Certs to check.
        /// </summary>
        public List<LocalCertificateExpirationStatusConnectorConfigurationItem> CertSubjects { get { return _certSubjects ?? (_certSubjects = new List<LocalCertificateExpirationStatusConnectorConfigurationItem>()); } }
        private List<LocalCertificateExpirationStatusConnectorConfigurationItem> _certSubjects;


        /// <summary>
        /// Initializes a new instance of the <see cref="LocalCertificateExpirationStatusConnectorConfiguration"/> class.
        /// </summary>
        public LocalCertificateExpirationStatusConnectorConfiguration()
        {
            WarningTimeSpan = new TimeSpan(90, 0, 0, 0);
        }
    }
}