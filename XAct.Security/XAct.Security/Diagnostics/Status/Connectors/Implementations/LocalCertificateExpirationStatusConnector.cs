﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography.X509Certificates;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Environment;

    /// <summary>
    /// A StatusService connector that reports on the health of certificates.
    /// </summary>
    public class LocalCertificateExpirationStatusConnector : XActLibStatusServiceConnectorBase
    {
        private readonly IDateTimeService _dateTimeService;

        /// <summary>
        /// The configuration package for this Connector.
        /// </summary>
        public LocalCertificateExpirationStatusConnectorConfiguration Configuration { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="LocalCertificateExpirationStatusConnector" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="certificateExpirationStatusFeedConnectorConfiguration">The certificate expiration status feed connector configuration.</param>
        public LocalCertificateExpirationStatusConnector(IDateTimeService dateTimeService, LocalCertificateExpirationStatusConnectorConfiguration certificateExpirationStatusFeedConnectorConfiguration)
            : base()
        {
            _dateTimeService = dateTimeService;
            Configuration = certificateExpirationStatusFeedConnectorConfiguration;

            Name = "LocalCertificateExpiration";

        }


        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null)
        {

            //Using the base helper ensures it gets the right Title/Description
            //in the user's culture:
            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }

        private void MakeTable(StatusResponseTable table, object arguments)
        {
            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:

            List<LocalCertificateExpirationStatusConnectorConfigurationItem> certsToAnalyse = Configuration.CertSubjects;

            //Make headers:
            table.Headers.Add("Subect");
            table.Headers.Add("Exp");

            //Make rows:
            if (certsToAnalyse == null)
            {
                return;
            }

            foreach (LocalCertificateExpirationStatusConnectorConfigurationItem localCertificateReference in certsToAnalyse)
            {
                var row = MakeTableRow(localCertificateReference);
                
                table.Rows.Add(row);
            }
        }

        private StatusResponseTableRow MakeTableRow(LocalCertificateExpirationStatusConnectorConfigurationItem localCertificateInfo)
        {

            StatusResponseTableRow row = new StatusResponseTableRow();

            TimeSpan expirationTimeSpan;

            //Cell #1:
            row.Cells.Add(localCertificateInfo.Title);

            DateTime? expirationDate;
            if (!GetCertificateExpiry(localCertificateInfo.Reference, localCertificateInfo.ReferenceType, out expirationTimeSpan, out expirationDate))
            {
                row.Cells.Add("--/--");
                row.Status = ResultStatus.Undetermined;
            }
            else
            {

                row.Cells.Add(expirationDate.Value.Month + "/" + expirationDate.Value.Year);

                row.Status = (expirationTimeSpan > Configuration.WarningTimeSpan)
                                    ? ResultStatus.Success
                                    : (expirationTimeSpan.TotalHours > 0)
                                          ? ResultStatus.Warn
                                          : ResultStatus.Fail;
            }

            return row;
        }

        private bool GetCertificateExpiry(string certificateReference, LocalCertificateReferenceType referenceType,
                                          out TimeSpan timeSpanLeft, out DateTime? expirationDate)
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            X509Certificate2Collection x509Certificate2Collection;
            X509Certificate2 x509Certificate2;
            //TimeSpan timespan;

            store.Open(OpenFlags.ReadOnly);
            x509Certificate2Collection = (X509Certificate2Collection) store.Certificates;

            switch (referenceType)
            {
                case LocalCertificateReferenceType.Subject:
                    x509Certificate2Collection = x509Certificate2Collection.Find(X509FindType.FindBySubjectName, certificateReference, false);
                    break;
                case LocalCertificateReferenceType.SubjectDistinguishedName:
                    x509Certificate2Collection = x509Certificate2Collection.Find(X509FindType.FindBySubjectDistinguishedName,
                                                           certificateReference, false);
                    break;
                case LocalCertificateReferenceType.Thumbprint:
                    x509Certificate2Collection = x509Certificate2Collection.Find(X509FindType.FindByThumbprint, certificateReference, false);
                    break;
                default:
                    x509Certificate2Collection = x509Certificate2Collection.Find(X509FindType.FindBySubjectName, certificateReference, false);
                    break;
            }

            if (x509Certificate2Collection.Count == 0)
            {
                timeSpanLeft = TimeSpan.Zero;
                expirationDate = null;
                return false;
            }

            x509Certificate2 = x509Certificate2Collection[0];
            expirationDate = Convert.ToDateTime(x509Certificate2.GetExpirationDateString());
            timeSpanLeft = expirationDate.Value - _dateTimeService.NowUTC;

            return true;
        }
    }
}
