﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;

    public class RemoteCertificateExpirationStatusConnectorConfiguration : IHasTransientBindingScope
    {
        /// <summary>
        /// The amount of time prior to a certs expiration that should be considered a warning.
        /// </summary>
        public TimeSpan WarningTimeSpan { get; set; }

        /// <summary>
        /// The Subjects of the Certs to check.
        /// </summary>
        public List<RemoteCertificateExpirationStatusConnectorConfigurationItem> RemoteSslUrls { get { return _remoteSslUrls ?? (_remoteSslUrls = new List<RemoteCertificateExpirationStatusConnectorConfigurationItem>()); } }
        private List<RemoteCertificateExpirationStatusConnectorConfigurationItem> _remoteSslUrls;



        /// <summary>
        /// Initializes a new instance of the <see cref="LocalCertificateExpirationStatusConnectorConfiguration"/> class.
        /// </summary>
        public RemoteCertificateExpirationStatusConnectorConfiguration()
        {
            WarningTimeSpan = new TimeSpan(90, 0, 0, 0);
        }
    }
}