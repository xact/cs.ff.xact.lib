﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Environment;
    using XAct.Net;

    public class RemoteCertificateExpirationStatusConnector : XActLibStatusServiceConnectorBase
    {
        private readonly IDateTimeService _dateTimeService;

        static Dictionary<string,DateTime> _expirations = new Dictionary<string, DateTime>();
 
        /// <summary>
        /// The configuration package for this Connector.
        /// </summary>
        public RemoteCertificateExpirationStatusConnectorConfiguration Configuration { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="CertificateExpirationStatusConnector" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="certificateExpirationStatusFeedConnectorConfiguration">The certificate expiration status feed connector configuration.</param>
        public RemoteCertificateExpirationStatusConnector(IDateTimeService dateTimeService,
                                                          RemoteCertificateExpirationStatusConnectorConfiguration
                                                              certificateExpirationStatusFeedConnectorConfiguration)
            : base()
        {
            _dateTimeService = dateTimeService;
            Configuration = certificateExpirationStatusFeedConnectorConfiguration;

            Name = "CertificateExpiration";

        }


        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments = null, DateTime? startTimeUtc = null,
                                           DateTime? endTimeUtc = null)
        {

            //Using the base helper ensures it gets the right Title/Description
            //in the user's culture:
            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }

        private void MakeTable(StatusResponseTable table, object arguments)
        {
            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:
            List<RemoteCertificateExpirationStatusConnectorConfigurationItem> certsToAnalyse = 
                Configuration.RemoteSslUrls;


            //Make headers:
            table.Headers.Add("Subect");
            table.Headers.Add("Exp");

            //Make rows:
            if (certsToAnalyse == null)
            {
                return;
            }

            foreach (RemoteCertificateExpirationStatusConnectorConfigurationItem subjectName in certsToAnalyse)
            {
                var row = MakeTableRow(subjectName);
                table.Rows.Add(row);
            }
        }

        private StatusResponseTableRow MakeTableRow(RemoteCertificateExpirationStatusConnectorConfigurationItem remoteSslReferences)
        {

            StatusResponseTableRow row = new StatusResponseTableRow();


            //Cell #1:
            row.Cells.Add(remoteSslReferences.Title);

            DateTime? expirationDate;
            TimeSpan? expirationTimeSpan;
            if (!GetCertificateExpiry(remoteSslReferences, out expirationTimeSpan, out expirationDate))
            {
                row.Cells.Add("--/--");
                row.Status = ResultStatus.Undetermined;
            }
            else
            {

                row.Cells.Add(expirationDate.Value.Month + "/" + expirationDate.Value.Year);

                row.Status = (expirationTimeSpan > Configuration.WarningTimeSpan)
                                 ? ResultStatus.Success
                                 : (expirationTimeSpan.Value.TotalHours > 0)
                                       ? ResultStatus.Warn
                                       : ResultStatus.Fail;
            }

            return row;
        }

        static object _lock = new object();

            bool set = false;

        private bool GetCertificateExpiry(RemoteCertificateExpirationStatusConnectorConfigurationItem certificateInfo, out TimeSpan? timeSpanLeft,
                                          out DateTime? expirationDate)
        {
            RemoteCertificateValidationCallback callback;

            callback = new RemoteCertificateValidationCallback(customCertificateIsValidCallback);

            lock (_lock)
            {

                try
                {

                    if (set == false)
                    {
                        ServicePointManager.ServerCertificateValidationCallback += callback;
                        set = true;
                    }

                    TimeoutWebClient webClient = new TimeoutWebClient(certificateInfo.Timeout);
                    webClient.DownloadString(certificateInfo.Url);
                    //This will have triggered the request:

                    if (_expirations.ContainsKey(new Uri(certificateInfo.Url).Host))
                    {
                        expirationDate = _expirations[new Uri(certificateInfo.Url).Host];
                        timeSpanLeft = _dateTimeService.NowUTC.Subtract(expirationDate.Value);
                        return true;
                    }
                }
                finally
                {
                    if (set)
                    {
                        ServicePointManager.ServerCertificateValidationCallback -= callback;
                        set = false;
                    }
                }
            }
            expirationDate = null;
            timeSpanLeft = null;
            return false;
        }


        bool customCertificateIsValidCallback(Object sender,
                                                     X509Certificate certificate, X509Chain chain,
                                                     SslPolicyErrors sslPolicyErrors)
        {
            WebRequest webRequest = sender as WebRequest;

            DateTime expirationDate = Convert.ToDateTime(certificate.GetExpirationDateString());

            _expirations[webRequest.RequestUri.Host] = expirationDate;

            //Src: https://msdn.microsoft.com/en-us/library/office/dd633677%28v=exchg.80%29.aspx

            // If the certificate is a valid, signed certificate, return true.
            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
            {
                return true;
            }


            // If there are errors in the certificate chain, look at each error to determine the cause.
            if ((sslPolicyErrors & System.Net.Security.SslPolicyErrors.RemoteCertificateChainErrors) == 0)
            {
                // In all other cases, return false.
                return false;
            }
            if (chain != null && chain.ChainStatus != null)
            {
                foreach (System.Security.Cryptography.X509Certificates.X509ChainStatus status in chain.ChainStatus)
                {
                    if ((certificate.Subject == certificate.Issuer) &&
                        (status.Status ==
                         System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.UntrustedRoot))
                    {
                        // Self-signed certificates with an untrusted root are valid. 
                        continue;
                    }

                    if (status.Status !=
                        System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
                    {
                        // If there are any other errors in the certificate chain, the certificate is invalid,
                        // so the method returns false.
                        return false;
                    }
                }
            }

            
            // When processing reaches this line, the only errors in the certificate chain are 
            // untrusted root errors for self-signed certificates. These certificates are valid
            // for default Exchange server installations, so return true.
            return true;
        }
    }
}