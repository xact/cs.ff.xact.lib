﻿namespace XAct.Diagnostics.Status.Connectors
{
    using XAct.Diagnostics.Status.Connectors.Implementations;

    public class LocalCertificateExpirationStatusServiceConnectorFactory
    {
        public static LocalCertificateExpirationStatusConnector Create(string name, string title, string description,
                                                                  params LocalCertificateExpirationStatusConnectorConfigurationItem[] certInfos)
        {
            var statusServiceConnector =
                XAct.DependencyResolver.Current.GetInstance<LocalCertificateExpirationStatusConnector>();

            statusServiceConnector.ConfigureBasicInformation(name, title, description);

            statusServiceConnector.Configuration.CertSubjects.Add(certInfos);

            return statusServiceConnector;
        }
    }
}
