namespace XAct
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Essentially, a Session record.
    /// <para>
    /// MultiKey Index: <see cref="UserIdentifier"/>, <see cref="LoginProviderFK"/>, 
    /// <see cref="LoginProviderToken"/>.
    /// </para>
    /// </summary>
    public class UserLogin : IHasXActLibEntity, 
                                IHasDistributedGuidIdAndTimestamp, 
                             IHasEnabled, IHasAuditabilitySimple,
                             IHasUserIdentifier
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }



        /// <summary>
        /// Gets or sets the user identifier.
        /// <para>
        /// Member defined in the <see cref="IHasUserIdentifier" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public virtual string UserIdentifier { get; set; }



        //MaxLength 128
        /// <summary>
        /// Gets or sets the FK to the <see cref="UserLoginProvider"/>.
        /// </summary>
        /// <value>
        /// The login provider fk.
        /// </value>
        [DataMember]
        public virtual Guid LoginProviderFK { get; set; }
        /// <summary>
        /// Gets or sets the  <see cref="UserLoginProvider"/>.
        /// </summary>
        /// <value>
        /// The login provider.
        /// </value>
        [DataMember]
        public virtual UserLoginProvider LoginProvider {get; set; }


        /// <summary>
        /// The unique SSO session token.
        /// <para>
        /// In the case of an OAuth based login, the remote SSO will generate the OAuth token.
        /// </para>
        /// <para>
        /// In the case of a SAML based login, the local provider will generate a SessionToken.
        /// </para>
        /// <para>
        /// In the case of a UserName/Password based login, the local provider will generate a SessionToken.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string LoginProviderToken { get; set; }





 



        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        ///   <internal>
        /// As to why its Nullable: sometimes the contract is applied to items
        /// that are not Entities themselves, but pointers to objects that are not known
        /// if they are
        ///   </internal>
        ///   <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        ///   </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy" /></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public virtual string CreatedBy { get; set; }

        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// <para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? LastModifiedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the identity who Modified the document.
        /// <para>Member defined in<see cref="IHasDateTimeModifiedBy" /></para>
        /// </summary>
        /// <value>
        /// The Modified by.
        /// </value>
        [DataMember]
        public virtual string LastModifiedBy { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogin"/> class.
        /// </summary>
        public UserLogin()
        {
            this.GenerateDistributedId();
        }
    }
}