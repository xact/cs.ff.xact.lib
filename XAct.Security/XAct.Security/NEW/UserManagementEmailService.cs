//namespace XAct
//{
//    using System.Threading.Tasks;

//    /// <summary>
//    /// An implementation of the <see cref="IUserManagementMessageService"/>
//    /// to send messages to users.
//    /// </summary>
//    public class UserManagementEmailService : IUserManagementMessageService
//    {
//        /// <summary>
//        /// Sends the message asynchronous.
//        /// </summary>
//        /// <param name="message">The message.</param>
//        /// <returns></returns>
//        public Task SendMessageAsync(UserIdentityMessage message)
//        {
//            // Plug in your email service here to send an email.
//            return Task.FromResult(0);
//        }
//    }
//}