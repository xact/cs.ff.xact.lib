﻿namespace XAct.Tests.Services.Implementations
{
    using XAct.Net.Messaging;

    public class EmailResetMessage :SimpleMessage
    {
        public EmailResetMessage(string destination, string password)
        {
            Destination = destination;
            Body = "TODO: EmailREsetMessage";
        }
    }
}