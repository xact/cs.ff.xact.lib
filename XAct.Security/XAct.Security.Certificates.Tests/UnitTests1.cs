namespace XAct.Security.Tests.ForNUnit
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    /// NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class UnitTests1
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

                                    Singleton<IocContext>.Instance.ResetIoC();

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }


        [Test]
        public void UnitTest01()
        {
            //XAct.Security.SOMECLASSNAME ctrl = new XAct.Security.SOMECLASSNAME();
            Assert.IsTrue(true);
        }


     
    }


}


