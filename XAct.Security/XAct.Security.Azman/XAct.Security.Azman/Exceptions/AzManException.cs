﻿using System;
using System.Runtime.Serialization;

namespace XAct.Security
{
    public class AzManException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AzManException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public AzManException(string message) : base(message){}

        /// <summary>
        /// Initializes a new instance of the <see cref="AzManException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public AzManException(string message, Exception innerException) : base(message,innerException) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AzManException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected AzManException(SerializationInfo serializationInfo,StreamingContext streamingContext) : base(serializationInfo,streamingContext){}
    }
}
