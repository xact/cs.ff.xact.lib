﻿
using System;
using AZROLESLib;
using XAct.Diagnostics;
using System.Security.Principal;

namespace XAct.Security
{

    /// <summary>
    /// 
    /// </summary>
    public class AzManAuthorizationService : IAzManAuthorizationService
    {
        private readonly ILoggingService _loggingService;

        private IAzAuthorizationStore _azAuthorizationStore;

        private IAzApplication _azApplication;



        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="AzManAuthorizationService"/> class.
        /// </summary>
        /// <param name="loggingService">The logging service.</param>
        public AzManAuthorizationService(ILoggingService loggingService)
        {
            if (_loggingService == null)
            {
                throw new ArgumentNullException("loggingService");
            }

            _loggingService = loggingService;
        }
        #endregion


        /// <summary>
        /// Is the current identity allowed to perform the following operation?
        /// <para>
        /// Note: will work against current thread's IIdentity.
        /// </para>
        /// </summary>
        /// <param name="operationName"></param>
        /// <returns></returns>
        /// <internal>
        /// See AzMan operation.
        /// </internal>
        public bool IsOperationAllowed(string operationName)
        {
            if (operationName.IsNullOrEmpty()) { throw new ArgumentNullException("operationName"); }

            return IsOperationAllowed(System.Threading.Thread.CurrentPrincipal.Identity, operationName);
        }

        /// <summary>
        /// Is the current identity allowed to perform the following operation?
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="operationName"></param>
        /// <returns></returns>
        /// <internal>
        /// See AzMan operation.
        /// </internal>
        public bool IsOperationAllowed(IIdentity identity, string operationName)
        {
            if (identity == null){throw new ArgumentNullException("identity");}
            if (operationName.IsNullOrEmpty()) { throw new ArgumentNullException("operationName"); }

            WindowsIdentity windowsIdentity = identity as WindowsIdentity;

            if (windowsIdentity== null)
            {
                throw new ArgumentException("Identity must be of type WindowsIdentity in order to work with AzMan.");
            }

            IAzClientContext azClientContext = _azApplication.CreateAzManClientContext(windowsIdentity);

            int operationId = _azApplication.GetOperationID(operationName);

            return azClientContext.ValidateOperation(new int[] {operationId});
        }

    }
}
