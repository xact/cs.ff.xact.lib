﻿using System;
using AZROLESLib;

namespace XAct
{
    /// <summary>
    /// Extension methods to the AzAuthorizationStore
    /// </summary>
    /// <internal>
    /// Note that you start by initializing the store
    /// and then initializing an App
    /// against which you you check clientcontext rights.
    /// </internal>
    public static class AzAuthorizationStoreExtensions
    {
        /// <summary>
        /// Initialize the AzAuthorizationStore from the given xml file.
        /// </summary>
        /// <internal>
        /// Use a connection String that looks like this:
        /// <para>
        /// <code>
        /// <![CDATA[
        /// <configuration> 
        ///   <connectionStrings> 
        ///     <add name="LocalPolicyStore" 
        ///          connectionString="msxml://c:/RolesData/azmanstore.xml" />
        ///   </connectionStrings> 
        /// </configuration> 
        /// ]]>
        /// </code>
        /// </para>
        /// </internal>
        /// <param name="azAuthorizationStore"></param>
        /// <param name="urlToXmlPolicyFile"></param>
        public  static void InitializeFromXmlFile(this AzAuthorizationStoreClass azAuthorizationStore, string urlToXmlPolicyFile)
        {
            //azAuthorizationStore = new AzAuthorizationStoreClass();
            azAuthorizationStore.Initialize(0, urlToXmlPolicyFile, null);
        }


        /// <summary>
        /// Open a single Az Application within the Store.
        /// </summary>
        /// <internal>
        /// Once you have an AzAuthorizationStoreClass, you can open a single application within it.
        /// </internal>
        /// <param name="azAuthorizationStore"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        public static IAzApplication OpenApplication(this AzAuthorizationStoreClass azAuthorizationStore, string applicationName)
        {
            if (azAuthorizationStore == null){throw new ArgumentNullException("azAuthorizationStore");}
            if (applicationName.IsNullOrEmpty()){throw new ArgumentNullException("applicationName");}

            return azAuthorizationStore.OpenApplication("AzManDemo", null);
        }
    }
}
