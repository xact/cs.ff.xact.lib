﻿using AZROLESLib;

namespace XAct
{
    /// <summary>
    /// Extension methods to the IAzClientContext object
    /// <para>
    /// Note that every starts from the AuthorizationStore...
    /// </para>
    /// </summary>
    /// <internal>
    /// Reference: http://bit.ly/fZFmRh
    /// </internal>
    public static class IAzClientContextExtensions
    {
        public static bool ValidateOperation(this IAzClientContext azClientContext, int[] operationIds)
        {
            // Create an object array describing the operation IDs to query

            //// In this simple example, query for the single operation called
            //// 'Do Sensitive Operation' which has an ID of 1.
            //object[] operationIds = new object[] { 1 };

            // Check if user has access to the operations
            // The first argument, "Auditstring", is a string that is used if you 
            // have run-time auditing turned on
            object[] result = azClientContext.AccessCheck("Auditstring",
                                                          new object[1], 
                                                          operationIds, 
                                                          null, 
                                                          null, 
                                                          null, 
                                                          null, 
                                                          null)
                                                          as object[];

            // Test the integer array we got back to see which operations are
            // authorized
            int accessAllowed = (int)result[0];

            return (accessAllowed == 0) ? true : false;

        }
    }
}
