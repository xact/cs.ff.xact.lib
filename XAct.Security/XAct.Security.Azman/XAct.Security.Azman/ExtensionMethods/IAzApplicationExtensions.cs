﻿using System;
using System.Security.Principal;
using AZROLESLib;
using XAct.Security;

namespace XAct
{
    /// <summary>
    /// Extension methods to the IAzApplication object.
    /// <para>
    /// Note that every starts from the AuthorizationStore...
    /// </para>
    /// </summary>
    public static class IAzApplicationExtensions
    {

        //AzAuthorizationStoreClass AzManStore = new AzAuthorizationStoreClass();
        
        //AzManStore.Initialize(0, ConfigurationManager.ConnectionStrings ["LocalPolicyStore"].ConnectionString, null);

        //IAzApplication azApp = AzManStore.OpenApplication("AzManDemo", null);
  

        /// <summary>
        /// Create from the given windows identity an az client context that can be queried for operations.
        /// </summary>
        /// <param name="azApplication"></param>
        /// <param name="windowsIdentity"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static IAzClientContext CreateAzManClientContext (this IAzApplication azApplication, WindowsIdentity windowsIdentity)
        {
            if (azApplication == null){throw new ArgumentNullException("azApplication");}
            if (windowsIdentity == null){throw new ArgumentNullException("windowsIdentity");}

            return azApplication.InitializeClientContextFromToken((ulong)windowsIdentity.Token, null);
        }

        /// <summary>
        /// Creates the az man client context.
        /// </summary>
        /// <param name="azApplication">The az application.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="domainName">Name of the domain.</param>
        /// <returns></returns>
        public static IAzClientContext CreateAzManClientContext(this IAzApplication azApplication, string userName, string domainName)
        {
            if (azApplication == null){throw new ArgumentNullException("azApplication");}
            if (string.IsNullOrEmpty(userName)){throw new ArgumentNullException("userName");}
            if (string.IsNullOrEmpty(domainName)){throw new ArgumentNullException("domainName");}

            return azApplication.InitializeClientContextFromName(userName,domainName,null);
        }


        /// <summary>
        /// Gets the ID for the given operation.
        /// </summary>
        /// <param name="azApplication">The az application.</param>
        /// <param name="operationName">Name of the operation.</param>
        /// <returns></returns>
        public static int GetOperationID(this IAzApplication azApplication,  string operationName)
		{
			foreach (IAzOperation azOperation in azApplication.Operations)
			{
				if (string.CompareOrdinal(operationName,azOperation.Name)==0)
			    {
			        return azOperation.OperationID;
			    }
			}

			throw new AzManException("fail to find Operation ID for Operation: '{0}'".FormatStringUICulture(operationName));
		}




    }
}
