namespace XAct.Resources.Tests
{
    using System;
    using System.Globalization;
    using System.Web.Compilation;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class RepositoryResourceProviderFactoryTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>().FactoryDelegate =
            //    () => new EntityDbContext(new UnitTestDbContext());

                        Singleton<IocContext>.Instance.ResetIoC();

            //Initialize the db using the common initializer passing it one entity from this class
            //so that it can perform a query (therefore create db as necessary):
            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<Resource>();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();


        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanCreateAGlobalResourceProvider()
        {
            RepositoryResourceProviderFactory repositoryResourceProviderFactory 
                = new RepositoryResourceProviderFactory();

           IResourceProvider resourceProvider =
               repositoryResourceProviderFactory.CreateGlobalResourceProvider("FilterABC");

            Assert.IsTrue(resourceProvider!=null);
        }



        [Test]
        public void UnitTest02()
        {
            RepositoryResourceProviderFactory repositoryResourceProviderFactory
                = new RepositoryResourceProviderFactory();

            IResourceProvider resourceProvider =
                repositoryResourceProviderFactory.CreateGlobalResourceProvider("FilterABC");

            object result = resourceProvider.GetObject("KeyB", null);

            Assert.AreEqual("Some en value.", result);
        }


        [Test]
        public void UnitTest04()
        {
            RepositoryResourceProviderFactory repositoryResourceProviderFactory
                = new RepositoryResourceProviderFactory();

            IResourceProvider resourceProvider =
                repositoryResourceProviderFactory.CreateGlobalResourceProvider("FilterABC");

            object result = resourceProvider.GetObject("KeyB", null);
            object result2 = resourceProvider.GetObject("KeyB", new CultureInfo("fr-FR"));
            Assert.AreEqual("Some en value.", result);
            Assert.AreEqual("Some fr-FR value.", result2);
        }

        [Test]
        public void UnitTest05()
        {
            RepositoryResourceProviderFactory repositoryResourceProviderFactory
                = new RepositoryResourceProviderFactory();

            IResourceProvider resourceProvider =
                repositoryResourceProviderFactory.CreateGlobalResourceProvider("FilterABC");

            object result = resourceProvider.GetObject("KeyB", null);
            object result2 = resourceProvider.GetObject("KeyB", new CultureInfo("en"));
            Assert.AreEqual("Some en value.", result);
            Assert.AreEqual("Some en value.", result2);
            Assert.IsTrue(true);
        }

        [Test]
        public void UnitTest06()
        {
            RepositoryResourceProviderFactory repositoryResourceProviderFactory
                = new RepositoryResourceProviderFactory();

            IResourceProvider resourceProvider =
                repositoryResourceProviderFactory.CreateGlobalResourceProvider("FilterABC");

            object result = resourceProvider.GetObject("KeyB", null);
            object result2 = resourceProvider.GetObject("KeyB", new CultureInfo("fr"));
            Assert.AreEqual("Some en value.", result);
            Assert.AreEqual("Some fr value.", result2);
            Assert.IsTrue(true);
        }





        [Test]
        public void UnitTest08()
        {
            RepositoryResourceProviderFactory repositoryResourceProviderFactory
                = new RepositoryResourceProviderFactory();

            IResourceProvider resourceProvider =
                repositoryResourceProviderFactory.CreateGlobalResourceProvider("FilterABC");

            DateTime start = DateTime.Now;
            for (int i = 0; i < 1000; i++)
            {

                object result = resourceProvider.GetObject("KeyB", null);
                object result2 = resourceProvider.GetObject("KeyB", new CultureInfo("fr"));

                Assert.AreEqual("Some en value.", result);
                Assert.AreEqual("Some fr value.", result2);
            }
#pragma warning disable 168
            TimeSpan elapsed = DateTime.Now.Subtract(start);
#pragma warning restore 168
            Assert.IsTrue(true);
        }
        [Test]
        public void UnitTest09()
        {
            RepositoryResourceProviderFactory repositoryResourceProviderFactory
                = new RepositoryResourceProviderFactory();

            IResourceProvider resourceProvider =
                repositoryResourceProviderFactory.CreateGlobalResourceProvider("FilterABC");

            DateTime start = DateTime.Now;
            for (int i = 0; i < 100000; i++)
            {
                object result = resourceProvider.GetObject("KeyB", null);
                object result2 = resourceProvider.GetObject("KeyB", new CultureInfo("fr"));

                Assert.AreEqual("Some en value.", result);
                Assert.AreEqual("Some fr value.", result2);
            }
            TimeSpan elapsed = DateTime.Now.Subtract(start);

// ReSharper disable LocalizableElement
            Console.WriteLine("Elapsed:" + (elapsed.TotalMilliseconds/1000));
// ReSharper restore LocalizableElement
            
            Assert.IsTrue(true);
        }




        [Test]
        public void UnitTest16()
        {
            RepositoryResourceProviderFactory repositoryResourceProviderFactory
                = new RepositoryResourceProviderFactory();

            IResourceProvider resourceProvider =
                repositoryResourceProviderFactory.CreateLocalResourceProvider(string.Empty);

            object result = resourceProvider.GetObject("KeyA", null);
            object result2 = resourceProvider.GetObject("KeyA", new CultureInfo("fr"));
            Assert.AreEqual("Some en value.", result);
            Assert.AreEqual("Some fr value.", result2);
            Assert.IsTrue(true);
        }

    }





}


