﻿// ReSharper disable CheckNamespace
namespace XAct.Resources
// ReSharper restore CheckNamespace
{
    using System.Globalization;
    using System.Web.Compilation;

    /// <summary>
    /// Contract for a <see cref="IResourceProvider"/>
    /// that works with a <see cref="IRepositoryResourceRepository"/>
    /// <para>
    /// A resource provider facilitates the retrieving of values from a resource file. 
    /// When an expression of the form &lt;%$ Resources: classKey, resourceKey %&gt; 
    /// is encountered during page parsing, the resource provider returns 
    /// the localized value for the resource. The 
    /// ResourceProviderFactory class creates instances of IResourceProvider 
    /// objects for use in retrieving the values.
    /// </para>
    /// </summary>
    public interface IRepositoryResourceProvider : IResourceProvider, IImplicitResourceProvider
    {
        /// <summary>
        /// Gets the resource filter (eg: name of class. See RepositoryResourceProviderFactory for clarification).
        /// </summary>
        string ResourceFilter { get; }

        /// <summary>
        /// Default Culture of this <see cref="System.Web.Compilation.IResourceProvider"/>:
        /// </summary>
        CultureInfo DefaultCulture { get; }

        /// <summary>
        /// Initializes the classkey and defaultCulture value.
        /// </summary>
        /// <param name="classKey">The class key.</param>
        /// <param name="defaultCulture">The default culture.</param>
        void Initialize(string classKey, CultureInfo defaultCulture);
    }
}