namespace XAct.Resources
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Resources;
    using System.Text.RegularExpressions;
    using System.Web.Compilation;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// A custom implementation of the <see cref="IResourceReader"/> 
    /// contract to read Resources from a Repository.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Instantiated by <see cref="RepositoryResourceProvider.ResourceReader"/> 
    /// in order to retrieve a Dictionary filled by reading
    /// what is returned by the <c>RepositoryResourceRepository</c>
    /// </para>
    /// </remarks>
    /// <internal>
    /// The problem here is that the default framework solution brings back *all* values,
    /// irespective of culture, and it is up to the invoking 
    /// <see cref="IResourceProvider"/> 
    /// to parse through the results for the correct culture.
    /// </internal>
    /// <internal>
    /// Note: ServiceLifetimeType.TransientScope
    /// </internal>
    [DefaultBindingImplementation(typeof(IRepositoryResourceReader), BindingLifetimeType.TransientScope, Priority.Low)]
    public class RepositoryResourceReader : DisposableBase, IRepositoryResourceReader //IEnumerable<KeyValuePair<string, object>>
   {

        /// <summary>
       /// The unique filter Key/ClassName.
       /// </summary>
        public string ResourceFilter { get; private set; }
        /// <summary>
        /// Gets the culture info.
        /// </summary>
        public CultureInfo Culture { get; private set; }


        /// <summary>
        /// Gets a value indicating whether this <see cref="IRepositoryResourceReader"/> is initialized.
        /// </summary>
        /// <value>
        ///   <c>true</c> if initialized; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get { return _initialized; } }
        private bool _initialized;


        /// <summary>
        /// Dictionary to hold retrieved values:
        /// </summary>
        private Dictionary<string, object> _resourceDictionary;
       
        private readonly ITracingService _tracingService;
       private readonly IRepositoryResourceRepository _resourceRepository;



       /// <summary>
       /// Sets the culture.
       /// </summary>
       /// <param name="resourceFilter">The unique key (ie, ClassName. See RepositoryResourceProviderFactory)</param>
       /// <param name="cultureInfo">The culture info.</param>
       public void Initialize(string resourceFilter, CultureInfo cultureInfo)
       {
           if (_initialized)
           {
               // ReSharper disable LocalizableElement
               throw new ArgumentException("Cannot reset Culture once Initialized.");
               // ReSharper restore LocalizableElement
           }

           ResourceFilter = resourceFilter;
           Culture = cultureInfo;

           //Read the whole list from the repository:
           _resourceDictionary = _resourceRepository.GetResources(ResourceFilter, Culture);

           foreach (string key in _resourceDictionary.Keys.ToArray())
           {
              _resourceDictionary[key]  = ProcessKey(key);
           }

           _initialized = true;
       }

       string ProcessKey(string key)
       {
           object value;

           if (!_resourceDictionary.TryGetValue(key, out value))
           {
               return key;
           }
           string stringValue = value as string;
           if (stringValue == null)
           {
               return key;
           }

           foreach (string subKey in stringValue.FindLabeledPlaceHolders(true))
           {
               string replacement = ProcessKey(subKey);
               if (string.Compare(replacement, subKey) != 0)
               {
                   stringValue =
                       Regex.Replace(
                           stringValue,
                           "{" + subKey + "}",
                           replacement,
                           RegexOptions.Multiline);

                   stringValue =
                       Regex.Replace(
                           stringValue,
                           "#" + subKey + "#",
                           replacement,
                           RegexOptions.Multiline);
               }
           }
           return stringValue;
       }




        /// <summary>
       /// Initializes a new instance of the <see cref="RepositoryResourceReader"/> class.
       /// </summary>
       /// <param name="tracingService">The tracing service.</param>
       /// <param name="resourceRepository">The resource repository.</param>
       /// <remarks>
       /// This constructor is invoked within
       /// <see cref="RepositoryResourceProvider.ResourceReader"/>
       /// </remarks>
       public RepositoryResourceReader(ITracingService tracingService, IRepositoryResourceRepository resourceRepository)
       {
            tracingService.ValidateIsNotDefault("tracingService");
            resourceRepository.ValidateIsNotDefault("resourceRepository");

            _tracingService = tracingService;
           _resourceRepository = resourceRepository;
       }


       #region IResourceReader Members

       /// <summary>
       /// Closes the resource reader after releasing any resources associated with it.
       /// </summary>
       public void Close()
       {
           this.Dispose();
       }

       /// <summary>
       /// Returns an <see cref="T:System.Collections.IDictionaryEnumerator"/> of the resources for this reader.
       /// </summary>
       /// <returns>
       /// A dictionary enumerator for the resources for this reader.
       /// </returns>
       public IDictionaryEnumerator GetEnumerator()
       {
           _tracingService.Trace(TraceLevel.Verbose,"RepositoryResourceReader.GetEnumerator()");
           
           // NOTE: this is the only enumerator called by the runtime for 
           // implicit expressions

           if (Disposed)
           {
               throw new ObjectDisposedException("RepositoryResourceReader object is already disposed.");
           }

           if (!_initialized)
           {
               throw new Exception("RepositoryResourceReader is not yet Initialized().");
           }
           var result = this._resourceDictionary.GetEnumerator();
           return result;
       }

       #endregion

       #region IEnumerable Members

       /// <summary>
       /// Returns an enumerator that iterates through a collection.
       /// </summary>
       /// <returns>
       /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
       /// </returns>
       IEnumerator IEnumerable.GetEnumerator()
       {
           if (!_initialized)
           {
               throw new Exception("RepositoryResourceReader is not yet Initialized().");
           }

           return this._resourceDictionary.GetEnumerator();
       }

       #endregion


   }

}