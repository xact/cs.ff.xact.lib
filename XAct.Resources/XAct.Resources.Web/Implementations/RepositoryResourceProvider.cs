#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

namespace XAct.Resources
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Resources;
    using System.Web.Compilation;
    using XAct.Diagnostics;
    using XAct.Resources.Implementations;
    using XAct.Services;

    /// <summary>
    /// Resource provider accessing resources from the database.
    /// This type is thread safe.
    /// <para>
    /// A ResourceProvider is in essence a hosting container for a set of resources.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that a single provider instance exists per <see cref="ResourceFilter"/>, and is cached for repeated use.
    /// </para>
    /// </remarks>
    /// <internal>
    /// In a web app, instantiated by 
    /// <see cref="RepositoryResourceProviderFactory"/> after it has 
    /// been registered in the web.config file.
    /// <para>
    /// A ResourceProvider is a hosting container for a set of resources. 
    /// A resource set is made up of all the resources for all cultures for 
    /// specific group of resources say a single resource file 
    /// or a single page for local resources. 
    /// To put this in perspective with Resx files, all files with the same base 
    /// filename make up one resource set: Resources.resx, Resources.de.resx, Resources.fr.resx etc. 
    /// In the .NET framework
    /// the ResourceProvider presents these resources in nested IDictionary structures 
    /// which are exposed through a ResourceReader. 
    /// The Dictionaries are nested which is a little confusing at first: The top level 
    /// dictionary holds another set of dictionaries one for each of the cultures implemented 
    /// for the given ResourceSet. Each one of IDictionary entries then in turn contains 
    /// a dictionary of the actual resources keys and values pairs 
    /// that make up the actual resource data.
    /// </para>
    /// </internal>
    [DefaultBindingImplementation(typeof(IRepositoryResourceProvider), BindingLifetimeType.Undefined, Priority.Low)]
    public class RepositoryResourceProvider : DisposableBase, IRepositoryResourceProvider
    {
        //This will be the route one would use if coming in via 
        //ASP.NET using A ResourceProviderFactory, to build up a Provider
#pragma warning disable 649
        private bool _useTheASPNETShortcut;
#pragma warning restore 649

        #region Fields
        // resource cache dictionary of Culture->Dictionary{Key->Value}
        //Used only if _useTheASPNETShortcut = true:
        private readonly Dictionary<string, Dictionary<string, object>> _resourceCache
            = new Dictionary<string, Dictionary<string, object>>();

        //Dictionary to hold the UI specific Reader's generated.
        //Used only if _useTheASPNETShortcut = true:
        private readonly Dictionary<CultureInfo, IResourceReader> _aspNetOnlySolutionCultureSpecificReaders =
            new Dictionary<CultureInfo, IResourceReader>();

        //hence this flag:
// ReSharper disable ConvertToConstant.Local
        private readonly bool cacheReaders = true;
// ReSharper restore ConvertToConstant.Local
        #endregion

        #region Services
        private readonly ITracingService _tracingService;
        #endregion


        #region Properties

        /// <summary>
        /// Gets the class key.
        /// </summary>
        public string ResourceFilter {get; private set; }

        /// <summary>
        /// Default Culture of this <see cref="System.Web.Compilation.IResourceProvider"/>:
        /// </summary>
        public CultureInfo DefaultCulture { get; private set; }
        #endregion




        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryResourceProvider"/> class.
        /// <para>
        /// Instantiated by <see cref="RepositoryResourceProviderFactory"/>
        /// which then calls <see cref="Initialize"/>
        /// </para>
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public RepositoryResourceProvider(ITracingService tracingService)
        {
            _tracingService = tracingService;

            _tracingService.Trace(TraceLevel.Verbose, 
                "RepositoryResourceProvider.RepositoryResourceProvider()");
        }

        #region 
        /// <summary>
        /// Initializes the classkey and defaultCulture value.
        /// </summary>
        /// <param name="classKey">The class key.</param>
        /// <param name="defaultCulture">The default culture.</param>
        public void Initialize(string classKey, CultureInfo defaultCulture)
        {
            classKey.ValidateIsNotNullOrEmpty(("classkey"));
            defaultCulture.ValidateIsNotDefault("defaultCulture");

            _tracingService.Trace(TraceLevel.Verbose,
                "RepositoryResourceProvider.Initialize({0},{1})"
                .FormatStringCurrentCulture(this.ResourceFilter, defaultCulture.Name));

            ResourceFilter = classKey;
            DefaultCulture = defaultCulture;
        }
        #endregion

        #region IResourceProvider Members

        /// <summary>
        /// Returns a resource reader.
        /// </summary>
        public IResourceReader ResourceReader
        {
            get
            {
                _tracingService.Trace(TraceLevel.Verbose, "RepositoryResourceProvider.get_ResourceReader - type:{0}".FormatStringCurrentCulture(this.ResourceFilter));

                if (Disposed)
                {
                    throw new ObjectDisposedException("RepositoryResourceProvider object is already disposed.");
                }


                //Note: We use but the CultureInfo.CurrentUICulture, which underneath, 
                //will fall back to the default culture,
                //and if that fails, falls back to Invariant:
                return ShortcutSolutionGetAResourceReader(CultureInfo.CurrentUICulture);
            }
        }



        /// <summary>
        /// Retrieves a resource entry based on the specified culture and 
        /// resource key. 
        /// <para>
        /// The resource type is based on this instance of the
        /// RepositoryResourceProvider as passed to the constructor.
        /// </para>
        /// <para>
        /// To optimize performance, values retrieved from the underlying datastore
        /// are cached in an internal dictionary, per culture.
        /// </para>
        /// </summary>
        /// <param name="resourceKey">The resource key to find.</param>
        /// <param name="culture">The culture to search with.</param>
        /// <returns>If found, the resource string is returned. 
        /// Otherwise an empty string is returned.</returns>
        public object GetObject(string resourceKey, CultureInfo culture = null)
        {
            //_tracingService.Trace(
            //    TraceLevel.Verbose, 
            //    "RepositoryResourceProvider.GetObject({0}, {1}) - type:{2}"
            //    .FormatStringCurrentCulture(resourceKey, culture,this.ResourceFilter));

            if (base.Disposed)
            {
                throw new ObjectDisposedException("RepositoryResourceProvider object is already disposed.");
            }

            resourceKey.ValidateIsNotNullOrEmpty("resourceKey");


            //We want to full back to the most logical Culture:
            if (culture == null)
            {
                culture = CultureInfo.CurrentUICulture;
            }

            if (_useTheASPNETShortcut)
            {
                return ShortcutSolutionSearchForObjectInLocalResourceReaderDictionaryCache(resourceKey, culture);
            }

            //Instead of going to a Reader directly, 
            //we go the longer winded away, 
            //through the Manager, which in turn
            //goes to a ResourceSet, which goes to a Reader and finally a Repository:
            object value = this.ResourceManager.GetObject(resourceKey, culture);
            return value;

        }







        #region Shortcut
        private object ShortcutSolutionSearchForObjectInLocalResourceReaderDictionaryCache(string resourceKey, CultureInfo culture)
        {
            Dictionary<string, object> resCacheByCulture;

            //Do we have a culture specific dictionary?
            if (!_resourceCache.TryGetValue(culture.Name, out resCacheByCulture))
            {
                resCacheByCulture = new Dictionary<string, object>();

                    foreach (
                        KeyValuePair<string, object> dictionaryEntry in
                            this.ShortcutSolutionGetAResourceReader(culture))
                    {
                        //string key = dictionaryEntry.Key;
                        resCacheByCulture[dictionaryEntry.Key] = dictionaryEntry.Value;
                    }
                }
                else
                {
                    throw new NotImplementedException();
                }

                //Add -- or replace another thread's- no lock required really:
                _resourceCache[culture.Name] = resCacheByCulture;

            //Now that we have a culture specific dictionary, do we have the key?
            object resourceValue;
            resCacheByCulture.TryGetValue(resourceKey, out resourceValue);
            return resourceValue;
        }

        /// <summary>
        /// Gets a ResourceReader.
        /// <para>
        /// Invoked only if _useTheASPNETShortcut = true
        /// </para>
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        private IResourceReader ShortcutSolutionGetAResourceReader(CultureInfo cultureInfo)
        {
            //Return a new instance of the appropriate Reader,
            //using the constructor to fill in its internal dictionary:

            // this is required for implicit resources 
            // this is also used for the expression editor sheet 

            IResourceReader resourceReader;

            if (cacheReaders)
            {

                if (!_aspNetOnlySolutionCultureSpecificReaders.TryGetValue(cultureInfo, out resourceReader))
                {
                    resourceReader = ShortcutSolutionCreateCultureSpecificReader(cultureInfo);
        
                    _aspNetOnlySolutionCultureSpecificReaders[cultureInfo] = resourceReader;

                }
            }else
            {
                resourceReader = ShortcutSolutionCreateCultureSpecificReader(cultureInfo);
            }

            //Return it:
            return resourceReader;
        }

        /// <summary>
        /// Creates the culture specific reader.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        private IRepositoryResourceReader ShortcutSolutionCreateCultureSpecificReader(CultureInfo cultureInfo)
        {
            //Note that lifespan has to be OnDemand
            IRepositoryResourceReader repositoryResourceReader =
                DependencyResolver.Current.GetInstance<IRepositoryResourceReader>();

            //The Resource Reader, in turn, will invoke a Repository...

            //Initialize the repository to have the vars it needs to do its fallback magic:
            repositoryResourceReader.Initialize(ResourceFilter,cultureInfo);

            return repositoryResourceReader;
        }
        #endregion 

        #endregion

        /// <summary>
        /// Override to provide custom cleanup of resources.
        /// <para>
        /// An implementation of <see cref="XAct.DisposableBase"/>
        /// 	</para>
        /// </summary>
        protected override void Cleanup()
        {
            try
            {
                this._resourceCache.Clear();
            }
            finally
            {
                base.Cleanup();
            }
        }


        /// <summary>
        /// Gets a collection of implicit resource keys as specified by the prefix.
        /// </summary>
        /// <param name="keyPrefix">The prefix of the implicit resource keys to be collected.</param>
        /// <returns>
        /// An <see cref="T:System.Collections.ICollection"/> of implicit resource keys.
        /// </returns>
        public ICollection GetImplicitResourceKeys(string keyPrefix)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets an object representing the value of the specified resource key.
        /// </summary>
        /// <param name="key">The resource key containing the prefix, filter, and property.</param>
        /// <param name="culture">The <see cref="T:System.Globalization.CultureInfo"/> that represents the culture for which the resource is localized.</param>
        /// <returns>
        /// An <see cref="T:System.Object"/> representing the localized value of an implicit resource key.
        /// </returns>
        public object GetObject(ImplicitResourceKey key, CultureInfo culture)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Gets or sets the <see cref="ResourceManager"/>
        /// that this <see cref="RepositoryResourceProvider"/>
        /// wraps.
        /// <para>
        /// By default, it will be an 
        /// <see cref="RepositoryResourceManager"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The resource manager.
        /// </value>
        public ResourceManager ResourceManager
        {
            get
            {
                if (_resourceManager == null)
                {
                    
                    RepositoryResourceManager repositoryResourceManager =
                        new RepositoryResourceManager(this.ResourceFilter);

                    //repositoryResourceManager.Initialized(this.ResourceFilter);

                    //That makes it easier...not that it matters with the way the 
                    //db searchs for values.
                    repositoryResourceManager.IgnoreCase = true;
                
                    _resourceManager = repositoryResourceManager;

                }
                return _resourceManager;
            }
            set {
                if (value != null)
                {
                    _resourceManager = value;
                }
            }
        }
        private ResourceManager _resourceManager;

    }
}