namespace XAct.Resources
{
    using System.Globalization;
    using System.Web.Compilation;


    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// <para>
    /// An implementation of <see cref="System.Web.Compilation.ResourceProviderFactory"/>
    /// to built <see cref="RepositoryResourceProvider"/> as and when required  when ASP.NET is parsing
    /// aspx pages, as well as later, during runtime.
    /// </para>
    /// <para>
    /// Configuration would be as follows:
    /// <code>
    /// <![CDATA[
    /// <connectionStrings>
    ///     <add name="Resources"  providerName="System.Data.SqlClient" connectionString="Data Source=XACTLIBDBTESTS;Initial Catalog=SPIKES;Integrated Security=SSPI;"/>
    /// </connectionStrings>
    /// <system.web>
    ///   <globalization uiCulture="auto" culture="auto" 
    ///       resourceProviderFactoryType="XAct.Resources.RepositoryResourceProviderFactory, 
    ///                                    XAct.Resources.Persistence, 
    ///                                    Version=1.0.0.0, 
    ///                                    Culture=neutral, 
    ///                                    PublicKeyToken=null" />
    /// <system.web>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Note that for any given resource type, only one instance of DBResourceProvider is created. 
    /// After it is created, it is cached for future use. 
    /// So, the factory is called only if the provider instance does not yet exist in the cache. 
    /// The process of creating and caching the provider is encapsulated 
    /// in the localization API used to access resources.
    /// </para>
    /// </remarks>
    public class RepositoryResourceProviderFactory : ResourceProviderFactory
    {

//// ReSharper disable MemberCanBeMadeStatic.Local
//        private ITracingService TracingService
//// ReSharper restore MemberCanBeMadeStatic.Local
//        {
//            get { return DependencyResolver.Current.GetInstance<ITracingService>(false) ?? 
//                DefaultTracingService.RegisterInServiceLocator(); }
//        }



        /// <summary>
        /// When overridden in a derived class, creates a global resource provider.
        /// </summary>
        /// <param name="classKey">The name of the resource class.</param>
        /// <remarks>
        /// <para>
        /// If invoked as follows:
        /// <code>
        /// <![CDATA[
        /// <asp:Label ID="labHelloGlobal" runat="server" Text="<%$ Resources:CommonTerms, Hello %>"></asp:Label>
        /// ]]>
        /// </code>
        ///  a ResourceProvider is created with a classKey="CommonTerms"
        /// </para>
        /// </remarks>
        /// <returns>
        /// An <see cref="T:System.Web.Compilation.IResourceProvider"/>.
        /// </returns>
        public override IResourceProvider CreateGlobalResourceProvider(string classKey)
        {
            //TracingService.Trace(TraceLevel.Verbose,"RepositoryResourceProviderFactory.CreateGlobalResourceProvider({0})".FormatStringCurrentCulture(classKey));

            //Note that the Provider does not specify culture at this point.
            return InstantiateResourceProvider(classKey);
        }

        /// <summary>
        /// When overridden in a derived class, creates a local resource provider.
        /// </summary>
        /// <param name="virtualPath">the virtual path of the page including the application directory.</param>
        /// <remarks>
        /// <para>
        /// In other words,
        /// <code>
        /// <![CDATA[
        /// <asp:Label runat="server" Text="HelloDefault" meta:resourcekey="labHelloLocalResource1" />
        /// <asp:Label runat="server" Text="<%$ Resources:labHelloLocalResource1.Text %>" />
        /// ]]>
        /// </code>
        /// will pass a value that looks something like:
        /// <code>
        /// <![CDATA[
        /// /LocalizedWebSite/Expressions.aspx
        /// ]]>
        /// </code>
        /// This is modified, so that a ResourceProvider is created
        /// with a Type="Expressions.aspx"
        /// </para>
        /// </remarks>
        /// <returns>
        /// An <see cref="T:System.Web.Compilation.IResourceProvider"/>.
        /// </returns>
        public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
        {
            //TracingService.Trace(TraceLevel.Verbose, "RepositoryResourceProviderFactory.CreateLocalResourceProvider({0})".FormatStringCurrentCulture(virtualPath));


            // we should always get a path from the runtime
            string classKey = virtualPath;

            if (!string.IsNullOrEmpty(virtualPath))
            {
                //remove first char:
                virtualPath = virtualPath.Remove(0, 1);

                //remove everyting before and including the first slash:
                classKey = virtualPath.Remove(0, virtualPath.IndexOf('/') + 1);
            }
            //Note that the Provider does not specify culture at this point.
            return InstantiateResourceProvider(classKey);
        }


        static IResourceProvider InstantiateResourceProvider(string classKey)
        {
            IRepositoryResourceProvider repositoryResourceProvider =
                DependencyResolver.Current.GetInstance<IRepositoryResourceProvider>();

            
            //Initialize parameters:
            repositoryResourceProvider.Initialize(classKey,CultureInfo.CurrentUICulture);

            //Note that the Provider does not specify culture at this point.
            return repositoryResourceProvider;
        }


    }
}