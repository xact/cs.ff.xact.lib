﻿namespace XAct.Resources.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class ResourceModelPersistenceMap : EntityTypeConfiguration<Resource>, IResourceModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceModelPersistenceMap"/> class.
        /// </summary>
        public ResourceModelPersistenceMap()
        {

            this.ToXActLibTable("Resource");
            

            

            this.HasKey(m => m.Id);

            int colOrder = 0;
            var indexMember = 1; //Index is 1 based.


            this
                .Property(m => m.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this
                .Property(m => m.Filter)
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_ResourceFilterKeyCulture", indexMember++) { IsUnique = true }))
                .DefineRequired26CharFilter(colOrder++)
                ;


            this
                .Property(m => m.Key)
                .IsRequired()
                .HasMaxLength(128)//Not 64!
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_ResourceFilterKeyCulture", indexMember++) { IsUnique = true }))
                ;

            this
                .Property(m => m.CultureCode)
                .IsRequired()
                .HasMaxLength(16)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_ResourceFilterKeyCulture", indexMember++) { IsUnique = true }))
                ;

            this
                .Property(m => m.Value)
                .IsRequired()
                .IsMaxLength() //OK to be MaxLength. 4000 (50lx80c) would be too small.
                .HasColumnOrder(colOrder++)
                ;
        }
    }
}