﻿namespace XAct.Resources.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="IResourceDbContextSeeder"/> contract
    /// to seed the Resource tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class ResourceDbContextSeeder : XActLibDbContextSeederBase<XAct.Resources.Resource>, IResourceDbContextSeeder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ResourceDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}
