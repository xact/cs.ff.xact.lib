﻿// ReSharper disable CheckNamespace
namespace XAct.Resources
// ReSharper restore CheckNamespace
{
    using System.Globalization;
    using System.Resources;

    /// <summary>
    /// The contract for an <see cref="IResourceReader"/>
    /// that will retrieve the resources from a Repository.
    /// </summary>
    public interface IRepositoryResourceReader : IResourceReader
    {
        /// <summary>
        /// The unique resourceFilter/ClassName.
        /// </summary>
        string ResourceFilter { get; }


        /// <summary>
        /// Gets or sets the culture for this Reader.
        /// </summary>
        /// <value>
        /// The culture.
        /// </value>
        CultureInfo Culture { get; }


        /// <summary>
        /// Gets a value indicating whether this <see cref="IRepositoryResourceReader"/> is initialized.
        /// </summary>
        /// <value>
        ///   <c>true</c> if initialized; otherwise, <c>false</c>.
        /// </value>
        bool Initialized { get; }


        /// <summary>
        /// Sets the culture.
        /// </summary>
        /// <param name="resourceFilter">The resourceFilter.</param>
        /// <param name="cultureInfo">The culture info.</param>
        void Initialize(string resourceFilter, CultureInfo cultureInfo);
    }
}