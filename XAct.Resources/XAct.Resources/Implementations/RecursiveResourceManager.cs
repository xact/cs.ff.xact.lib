﻿//IMPORTANT: A little ticked off by this...
//Resources, the namespace, has ambiguity with Resources.resx.cs
//As Resources is a perfectly good and descriptive namespace (as 
//attested by the System.Resources...) going to keep it, and
//have to use Properties.Resources to get to Resources class.
//
//At least, that's the theory.

namespace XAct.Resources
{
    using System;
    using System.Collections.Specialized;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.IO;
    using System.Resources;
    using System.Text.RegularExpressions;

    /// <summary>
    ///   A ResourceManager that can recursively process 
    ///   TextBasedPlaceHolders (ie '{KeyName}')
    ///   in the resource string, in order to build up a final, complete message.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     An example would be something like.
    ///     LF="\r\n"
    ///     PDFReaderUrl="http://somewhere.adobe.com/Downloads/Acrobat";
    ///     DownloadPDF="You will need &lt;a href="{PDFReaderUrl}"&gt;PDF Reader&lt;/a&gt; 
    ///     to read the document.";
    ///     Agreement="Thank you for signing up.{LN}Every vote counts!"
    ///   </para>
    ///   <para>
    ///     You get the point -- a rather more understandable DSL
    ///     or process that both BA's and developers can use to communicate
    ///     between each other -- rather than 
    ///     trying to cut apart and stitch together string fragments
    ///     or worse -- generate duplicates.
    ///   </para>
    ///   <para>
    ///     Usage Example:
    ///   </para>
    ///   <para>
    ///     <code>
    ///       <![CDATA[
    /// //Generate a Crosscutting service 
    /// public interface IResourceService : IHasXActLibServiceDefinition{
    ///   string GetString(string name);
    /// }
    /// 
    /// //Implement it as a concrete class:
    /// public class ResourceService : XActLibServiceBase,  IResourceService {
    ///   //Fields:
    ///   private readonly RecursiveResourceManager _recursiveResourceManager; 
    /// 
    ///   //Constructor:
    ///   public ResourceService (){
    ///     
    ///     RecursiveResourceManager _recursiveResourceManager = 
    ///       new RecursiveResourceManager(MyCorp.MyApp.Resources.ResourceManager);
    ///   }
    ///   
    ///   //Implement wrappers around the methods:
    ///   public string GetString(string name){
    ///     _recursiveResourceManager.GetString(name);
    ///   }
    ///   //etc...for images, etc., even though they 
    ///   //obviously don't recurse.
    /// }
    /// ]]>
    ///     </code>
    ///   </para>
    ///   <para>
    ///     Final thoughts. Yes, one loses the safety of the 
    ///     typed classes that VS automatically builds.
    ///     But I think the time lost by not having that available
    ///     is very easily regained by the time regained from communicating
    ///     lucidly between BA's and Devs. 
    ///     Give it a try...
    ///   </para>
    /// </remarks>
    public class RecursiveResourceManager
    {
        /// <summary>
        /// </summary>
        /// <internal>
        ///   StringDictionary uses a hash as the key,
        ///   so it's case insensitive...no need for extra
        ///   hoop jumping.
        /// </internal>
        private readonly StringDictionary _cachedProcessedStrings = new StringDictionary();

        private readonly ResourceManager _resourceManager;

        /// <summary>
        ///   Gets the underlying <see cref = "ResourceManager" />
        ///   if one needs methods beyond what this 
        ///   <see cref = "RecursiveResourceManager" />
        ///   can provide.
        /// </summary>
        /// <value>The resource manager.</value>
        public ResourceManager ResourceManager
        {
            get { return _resourceManager; }
        }


        /// <summary>
        ///   Gets or sets a value indicating whether to process 
        ///   linefeeds characters (\r\n and \n) and replace them
        ///   with <see cref = "LineFeedReplacement" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [process linefeeds characters]; otherwise, <c>false</c>.
        /// </value>
        public bool ProcessLinefeedsCharacters { get; private set; }


        /// <summary>
        ///   Gets or sets the line feed replacement.
        ///   <para>
        ///     Default is Environment.NewLine but it could be 
        ///     replaced with "&lt;br/&gt;" in a website, for example.
        ///   </para>
        /// </summary>
        /// <value>The line feed replacement.</value>
        public string LineFeedReplacement { get; set; }

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "RecursiveResourceManager" /> class.
        /// </summary>
        /// <remarks>
        ///   RecursiveResourceManager is a service that 
        ///   allows for the retrieval
        ///   of resources such as the following:
        ///   MyText = "This is a {More} made up of{LF}{MoreText}"; 
        ///   More = "Fragment of Text";
        ///   MoreText="fragments of more {More}";
        /// 
        ///   The benefits are that the assemblage of strings
        ///   is handled out of sight, leaving BA's and developers
        ///   have a common language to work with each other.
        /// </remarks>
        /// <param name = "resourceManager">The resource manager.</param>
        public RecursiveResourceManager(ResourceManager resourceManager)
        {
            resourceManager.ValidateIsNotDefault("resourceManager");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            _resourceManager = resourceManager;

            ProcessLinefeedsCharacters = true;
            
            LineFeedReplacement = System.Environment.NewLine;
        }

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "RecursiveResourceManager" /> class.
        /// </summary>
        /// <remarks>
        ///   RecursiveResourceManager is a service that 
        ///   allows for the retrieval
        ///   of resources such as the following:
        ///   MyText = "This is a {More} made up of{LF}{MoreText}"; 
        ///   More = "Fragment of Text";
        ///   MoreText="fragments of more {More}";
        /// 
        ///   The benefits are that the assemblage of strings
        ///   is handled out of sight, leaving BA's and developers
        ///   have a common language to work with each other.
        /// </remarks>
        /// <param name = "resourceManager">The resource manager.</param>
        /// <param name = "processLineFeedCharacters">
        ///   Process "\r\n" and "\n" characters, replacing them with Environment.LineFeeds.
        /// </param>
        public RecursiveResourceManager(
            ResourceManager resourceManager,
            bool processLineFeedCharacters)
        {
            resourceManager.ValidateIsNotDefault("resourceManager");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            _resourceManager = resourceManager;
            ProcessLinefeedsCharacters = processLineFeedCharacters;
            LineFeedReplacement = System.Environment.NewLine;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Gets the value of the specified string resource.
        /// </summary>
        /// <param name = "name">The name.</param>
        /// <returns></returns>
        public string GetString(string name)
        {
            if (name.IsNullOrEmpty())
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            bool originalValue = _resourceManager.IgnoreCase;
            _resourceManager.IgnoreCase = true;

            string result = GetString(name, name, null);

            _resourceManager.IgnoreCase = originalValue;
            return result;
        }

        /// <summary>
        ///   Gets the value of the specified string resource,
        ///   localised to the specified culture.
        /// </summary>
        /// <param name = "name">The name.</param>
        /// <param name = "cultureInfo">The culture info.</param>
        /// <returns></returns>
        public string GetString(string name, CultureInfo cultureInfo)
        {
            if (name.IsNullOrEmpty())
            {
                throw new ArgumentNullException("name");
            }
            cultureInfo.ValidateIsNotDefault("cultureInfo");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            bool originalValue = _resourceManager.IgnoreCase;
            _resourceManager.IgnoreCase = true;

            string result = GetString(name, name, cultureInfo);

            _resourceManager.IgnoreCase = originalValue;
            return result;
        }

        /// <summary>
        ///   Gets an UnmanagedMemoryStream.
        /// </summary>
        /// <param name = "name">The name.</param>
        /// <returns></returns>
        public UnmanagedMemoryStream GetStream(string name)
        {
            if (name.IsNullOrEmpty())
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            return _resourceManager.GetStream(name);
        }

        /// <summary>
        ///   Gets an UnmanagedMemoryStream,
        ///   localised to the specified culture.
        /// </summary>
        /// <param name = "name">The name.</param>
        /// <param name = "cultureInfo">The culture info.</param>
        /// <returns></returns>
        public UnmanagedMemoryStream GetStream(string name, CultureInfo cultureInfo)
        {
            if (name.IsNullOrEmpty())
            {
                throw new ArgumentNullException("name");
            }
            cultureInfo.ValidateIsNotDefault("cultureInfo");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            return _resourceManager.GetStream(name);
        }

        /// <summary>
        ///   Gets a Typed item.
        ///   Use this method to get Images (Pngs, Bitmaps, etc.)
        /// </summary>
        /// <typeparam name = "TItem">The type of the item.</typeparam>
        /// <param name = "name">The name.</param>
        /// <returns></returns>
        public TItem GetItem<TItem>(string name) where TItem : class
        {
            if (name.IsNullOrEmpty())
            {
                throw new ArgumentNullException("name");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Pretty sure this will never be the route of execution, 
            //as have a more direct one specified.
            if (typeof (TItem).IsAssignableFrom(typeof (string)))
            {
                var result = GetString(name) as TItem;
                return result;
            }

            return _resourceManager.GetObject(name) as TItem;
        }


        /// <summary>
        ///   Gets a Typed item.
        ///   Use this method to get Images (Pngs, Bitmaps, etc.)
        /// </summary>
        /// <typeparam name = "TItem">The type of the item.</typeparam>
        /// <param name = "name">The name.</param>
        /// <param name = "cultureInfo">The culture info.</param>
        /// <returns></returns>
        public TItem GetItem<TItem>(string name, CultureInfo cultureInfo) where TItem : class
        {
            if (name.IsNullOrEmpty())
            {
                throw new ArgumentNullException("name");
            }
            cultureInfo.ValidateIsNotDefault("cultureInfo");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Pretty sure this will never be the route of execution, 
            //as have a more direct one specified.
            if (typeof (TItem).IsAssignableFrom(typeof (string)))
            {
                var result = GetString(name) as TItem;
                return result;
            }

            return _resourceManager.GetObject(name, cultureInfo) as TItem;
        }

        #endregion

        #region Private methods

        private string GetString(string name, string parentNamePath, CultureInfo cultureInfo)
        {
            //name.ValidateIsNotDefault(name);

            string resource;

            if (!_cachedProcessedStrings.ContainsKey(name))
            {
                resource = GetStringResource(name, cultureInfo);

                if (resource == null)
                {
                    throw new ArgumentException(
                        "String resource '{0}' [RecursivePath: '{1}'] not found in Resource file.".
                            FormatStringExceptionCulture(
                                name,
                                parentNamePath + name));
                }

                foreach (string keyword in resource.FindLabeledPlaceHolders())
                {
                    string replacement = GetString(keyword, parentNamePath + "/", cultureInfo);

                    resource =
                        Regex.Replace(
                            resource,
                            "{" + keyword + "}",
                            replacement,
                            RegexOptions.Multiline);

                    if (!ProcessLinefeedsCharacters)
                    {
                        continue;
                    }

                    //Take care of the general case first:
                    resource = resource.Replace("\\r\\n", LineFeedReplacement);
                    resource = resource.Replace("\\n", LineFeedReplacement);
                }
                _cachedProcessedStrings[name] = resource;
            }
            else
            {
                resource = _cachedProcessedStrings[name];
            }

            return resource;
        }

        private string GetStringResource(string name, CultureInfo cultureInfo)
        {
            return (cultureInfo != null)
                       ? _resourceManager.GetString(name, cultureInfo)
                       : _resourceManager.GetString(name);
        }

        #endregion
    }
}