﻿namespace XAct.Resources.Implementations
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using System.Resources;
    using System.Text.RegularExpressions;
    using XAct.Library.Settings;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Whereas the <c>RepositoryResourceProviderFactory</c>
    /// is how Resources are registered into an ASP.NET page,
    /// a Manager works directly in any assembly.
    /// </para>
    /// <para>
    /// In an ASP.NET based application, after registring the 
    /// <c>RepositoryResourceProviderFactory</c> in the web.config file,
    /// it instantiates <c>RepositoryResourceProvider</c>s -- one per 
    /// virtual path (ie, page) if Local, or per keyword, if Global.
    /// </para>
    /// <para>
    /// The <c>RepositoryResourceProvider</c> in turn 
    /// wraps calls to this <c>RepositoryResourceManager</c>
    /// </para>
    /// <para>
    /// This <see cref="RepositoryResourceManager"/>
    /// instantiates an <see cref="RepositoryResourceSet"/>, which in 
    /// turn instantiate an <see cref="RepositoryResourceReader"/>,
    /// which wraps an <c>RepositoryResourceRepository</c>.
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(IRepositoryResourceManager), BindingLifetimeType.Undefined, Priority.Low)]
    public class RepositoryResourceManager : ResourceManager, IRepositoryResourceManager, IXXX
    {

        /// <summary>
        /// Gets or sets a value indicating whether to allow Recursion when 
        /// retrieving Resource strings.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow recursion]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowRecursion { get; set; }

        private readonly RepositoryCultureResourceStringCache _recursiveResourceStringCache =
            new RepositoryCultureResourceStringCache();

        /// <summary>
        /// Gets the <see cref="T:System.Type"/> of the <see cref="T:System.Resources.ResourceSet"/> the <see cref="T:System.Resources.ResourceManager"/> uses to construct a <see cref="T:System.Resources.ResourceSet"/> object.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Type"/> of the <see cref="T:System.Resources.ResourceSet"/> the <see cref="T:System.Resources.ResourceManager"/> uses to construct a <see cref="T:System.Resources.ResourceSet"/> object.
        ///   </returns>
        public override Type ResourceSetType
        {
            get { return typeof (RepositoryResourceSet); }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryResourceManager"/> class.
        /// </summary>
        /// <param name="baseName">Name of the base.</param>
        public RepositoryResourceManager(string baseName)
        {
            AllowRecursion = Globalisation.AllowResourceRecursion;

            Initialize(baseName, null);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryResourceManager"/> class.
        /// </summary>
        /// <param name="resourceType">Type of the resource.</param>
        public RepositoryResourceManager(Type resourceType)
        {
            this.Initialize(resourceType.Name, resourceType.Assembly);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryResourceManager"/> class.
        /// </summary>
        /// <param name="baseName">Name of the base.</param>
        /// <param name="assembly">The assembly.</param>
        public RepositoryResourceManager(string baseName, Assembly assembly)
        {
            this.Initialize(baseName, assembly);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryResourceManager"/> class.
        /// </summary>
        /// <param name="baseName">Name of the base.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="usingResourceSet">The using resource set.</param>
        public RepositoryResourceManager(string baseName, Assembly assembly, Type usingResourceSet)
        {
            this.Initialize(baseName, assembly);
        }


        /// <summary>
        /// Core Configuration method that sets up the ResourceManager.
        /// For this
        /// implementation we only need the baseName which is the ResourceSet id
        /// (ie. the local or global resource set name) and the assembly name is
        /// simply ignored.
        /// This method essentially sets up the ResourceManager and holds all
        /// of the culture specific resource sets for a single ResourceSet.
        /// With
        /// ResX files each set is a file - in the database a ResourceSet is a group
        /// with the same ResourceSet Id.
        /// </summary>
        /// <param name="baseName">Name of the base.</param>
        /// <param name="assembly">The assembly.</param>
        protected void Initialize(string baseName, Assembly assembly)
        {
            this.BaseNameField = baseName;
            base.MainAssembly = null; // assembly;

            // ResourceSets contains a set of resources for each locale
#pragma warning disable 612,618
            ResourceSets = new Hashtable();
#pragma warning restore 612,618

        }






        //public override bool IgnoreCase
        //{
        //    get
        //    {
        //        return base.IgnoreCase;
        //    }
        //    set
        //    {
        //        base.IgnoreCase = value;
        //    }
        //}

        //public override Type ResourceSetType
        //{
        //    get
        //    {
        //        return base.ResourceSetType;
        //    }
        //}

        //public override string BaseName
        //{
        //    get
        //    {
        //        return base.BaseName;
        //    }
        //}


        //public override void ReleaseAllResources()
        //{
        //    base.ReleaseAllResources();
        //}


        //public override ResourceSet GetResourceSet(CultureInfo culture, bool createIfNotExists, bool tryParents)
        //{
        //    return base.GetResourceSet(culture, createIfNotExists, tryParents);
        //}

        //ResourceManager.GetString() makes use of this function
        /// <summary>
        /// 
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="createIfNotExists"></param>
        /// <param name="tryParents"></param>
        /// <returns></returns>
        protected override ResourceSet InternalGetResourceSet(
            CultureInfo culture, bool createIfNotExists, bool tryParents)
        {
            RepositoryResourceSet rs = null;

#pragma warning disable 612,618
            if (!ResourceSets.Contains(culture.Name))
#pragma warning restore 612,618
            {
                rs = new RepositoryResourceSet(); //ConnectionString, culture

                rs.InitializeReader(this.BaseNameField, culture);

#pragma warning disable 612,618
                ResourceSets[culture.Name] = rs;
#pragma warning restore 612,618
            }


#pragma warning disable 612,618
            rs = (RepositoryResourceSet) ResourceSets[culture.Name];
#pragma warning restore 612,618

            return rs;
        }


        //public override object GetObject(string name)
        //{
        //    return base.GetObject(name);
        //}
        //public override object GetObject(string name, CultureInfo culture)
        //{
        //    return base.GetObject(name, culture);
        //}

        /// <summary>
        /// Returns the value of the specified string resource.
        /// </summary>
        /// <param name="name">The name of the resource to retrieve.</param>
        /// <returns>
        /// The value of the resource localized for the caller's current UI culture, or null if <paramref name="name" /> cannot be found in a resource set.
        /// </returns>
        public override string GetString(string name)
        {
            string result;

            GetRecursiveString(CultureInfo.InvariantCulture, name, out result);

            return result;

            //return base.GetString(name);
        }

        /// <summary>
        /// Returns the value of the string resource localized for the specified culture.
        /// </summary>
        /// <param name="name">The name of the resource to retrieve.</param>
        /// <param name="culture">An object that represents the culture for which the resource is localized.</param>
        /// <returns>
        /// The value of the resource localized for the specified culture, or null if <paramref name="name" /> cannot be found in a resource set.
        /// </returns>
        public override string GetString(string name, CultureInfo culture)
        {

            string result;

            GetRecursiveString(culture, name, out result);
            return result;
        }


        /// <summary>
        /// Gets all resources for page.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetAllStrings(CultureInfo cultureInfo)
        {
            if (cultureInfo == null)
            {
                cultureInfo = CultureInfo.InvariantCulture;
            }
            Dictionary<string, string> results = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            ResourceSet rs = InternalGetResourceSet(cultureInfo, true, true);

            IDictionaryEnumerator iDictionaryEnumerator = rs.GetEnumerator();

            while (iDictionaryEnumerator.MoveNext())
            {
                results[iDictionaryEnumerator.Key.ToString()] = iDictionaryEnumerator.Value.ToString();
            }

            return results;
        }






        /// <summary>
        /// Returns the value of the specified <see cref="T:System.Object"/> resource.
        /// </summary>
        /// <param name="name">The name of the resource to get.</param>
        /// <returns>
        /// The value of the resource localized for the caller's current culture settings. If a match is not possible, null is returned. The resource value can be null.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// The <paramref name="name"/> parameter is null.
        ///   </exception>
        ///   
        /// <exception cref="T:System.Resources.MissingManifestResourceException">
        /// No usable set of resources has been found, and there are no neutral culture resources.
        ///   </exception>
        public override object GetObject(string name)
        {
            //#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
            //            this.InternalGetResourceSet()
            //#else
            //            RepositoryResourceSet rs = (RepositoryResourceSet)ResourceSets[string.Empty];
            //#endif
            //            RepositoryResourceSet rs = (RepositoryResourceSet)ResourceSets[string.Empty];

            object Value = base.GetObject(name);
            return Value;
        }

        /// <summary>
        /// Core worker method that returnsa  resource value for a
        /// given culture from the this resourcemanager/resourceset.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public override object GetObject(string name, CultureInfo culture)
        {
            InternalGetResourceSet(culture, true, true);

//#if CONTRACTS_FULL || NET40 || NET45
//            // Requires .Net 4, so hold for the moment
//            InternalGetResourceSet()
//#else
//            RepositoryResourceSet rs = (RepositoryResourceSet)ResourceSets[culture.Name];
//#endif
            object Value = base.GetObject(name, culture);
            //object Value =             GetObject(name, culture, true); 


            return Value;
        }



        //#pragma warning disable 109
        //        private  new Object GetObject(String name, CultureInfo culture, bool wrapUnmanagedMemStream)
        //#pragma warning restore 109
        //        {
        //            if (null == name)
        //                throw new ArgumentNullException("name");
        //            if (null == culture)
        //            {
        //                culture = CultureInfo.CurrentUICulture;
        //            }

        //            ResourceSet rs = InternalGetResourceSet(culture, true, true);

        //            if (rs != null)
        //            {
        //                Object value = rs.GetObject(name, this.IgnoreCase);
        //#if _DEBUG
        //                if (DEBUG >= 5) 
        //                    BCLDebug.Log("GetObject: string was: "+(value==null ? "<null>" : value));
        //#endif
        //                if (value != null)
        //                {
        //                    UnmanagedMemoryStream stream = value as UnmanagedMemoryStream;
        //                    if (stream != null && wrapUnmanagedMemStream)
        //                        return null;//new UnmanagedMemoryStreamWrapper(stream);
        //                    else
        //                        return value;
        //                }
        //            }

        //            // This is the CultureInfo hierarchy traversal code for resource 
        //            // lookups, similar but necessarily orthogonal to the ResourceSet
        //            // lookup logic. 
        //            ResourceSet last = null;
        //            while (!culture.Equals(CultureInfo.InvariantCulture) && !culture.Equals(System.Globalization.CultureInfo.CurrentUICulture))
        //            {
        //                culture = culture.Parent;
        //#if _DEBUG 
        //                if (DEBUG >= 5)
        //                    BCLDebug.Log("GetObject: Parent lookup in locale "+culture.Name+" [0x"+culture.LCID.ToString("x", CultureInfo.InvariantCulture)+"] for "+name); 
        //#endif
        //                rs = InternalGetResourceSet(culture, true, true);
        //                if (rs == null)
        //                    break;
        //                if (rs != last)
        //                {
        //                    Object value = rs.GetObject(name, this.IgnoreCase);
        //                    if (value != null)
        //                    {
        //                        UnmanagedMemoryStream stream = value as UnmanagedMemoryStream;
        //                        if (stream != null && wrapUnmanagedMemStream)
        //                            return null;//new UnmanagedMemoryStreamWrapper(stream);
        //                        else
        //                            return value;
        //                    }
        //                    last = rs;
        //                }
        //            }
        //            return null;
        //        } 





        private bool GetRecursiveString(CultureInfo cultureInfo, string key, out string result, string parentNamePath = null)
        {

            if (!this.AllowRecursion)
            {
                result = base.GetString(key, cultureInfo);
                return (result == null) ? false : true;
            }

            if (parentNamePath == null)
            {
                parentNamePath = key;
            }

            if (_recursiveResourceStringCache.TryGet(cultureInfo, key, out result))
            {
                return true;
            }

            //Ensure we're calling base, or we'll stack overflow:
            result = base.GetString(key, cultureInfo);

            if (result == null)
            {
                return false;
            }

            foreach (string subKeyWord in result.FindLabeledPlaceHolders())
            {
                //Have Keyword: Is there a replacement?

                //Recurse on this method to find child objects:
                string subReplacement;

                //The ParentNamePath is just for debugging purposes, to know 
                //how deep we are recursing. An int would not tell us what was the 
                //entry point.

                if (GetRecursiveString(cultureInfo, subKeyWord, out subReplacement, parentNamePath + "/"))
                {
                    result =
                        Regex.Replace(
                            result,
                            "{" + subKeyWord + "}",
                            subReplacement,
                            RegexOptions.Multiline);
                }
            }

            //Done with looping at this level, whatever it is, 
            //and since we know we were a keyword, we can persist it.

            _recursiveResourceStringCache.SaveEntry(cultureInfo ,key, result);

            return true;
        }


    }
}
