﻿namespace XAct.Resources.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Resources;
    using XAct.Services;

    /// <summary>
    /// <para>
    /// Instantiated by <see cref="RepositoryResourceManager"/>.
    /// </para>
    /// <para>
    /// Stores all the resources localized for one particular culture, 
    /// ignoring all other cultures, including any fallback rules.
    /// </para>
    /// <para>
    /// That ... I didn't do...the values in the invoked RepositoryResourceRepository
    /// already have fallback values. 
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// An <see cref="RepositoryResourceManager"/>
    /// instantates an <see cref="RepositoryResourceSet"/>s, which in 
    /// turn instantiates an <see cref="RepositoryResourceReader"/>,
    /// which wraps an <c>RepositoryResourceRepository</c>.
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(IRepositoryResourceSet), BindingLifetimeType.Undefined, Priority.Low)]
    public class RepositoryResourceSet : ResourceSet, IRepositoryResourceSet
    {

        /// <summary>
        ///   Gets or sets a value indicating whether to process 
        ///   linefeeds characters (\r\n and \n) and replace them
        ///   with <see cref = "LineFeedReplacement" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [process linefeeds characters]; otherwise, <c>false</c>.
        /// </value>
        public bool ProcessLinefeedsCharacters { get; private set; }


        /// <summary>
        ///   Gets or sets the line feed replacement.
        ///   <para>
        ///     Default is Environment.NewLine but it could be 
        ///     replaced with "&lt;br/&gt;" in a website, for example.
        ///   </para>
        /// </summary>
        /// <value>The line feed replacement.</value>
        public string LineFeedReplacement { get; set; }


        /// <summary>
        /// Initializes the reader.
        /// </summary>
        /// <param name="resourceFilter">The resource filter.</param>
        /// <param name="culture">The culture.</param>
        public void InitializeReader(string resourceFilter, CultureInfo culture)
        {
            //Create a new Reader:
            IRepositoryResourceReader reader = 
                DependencyResolver.Current.GetInstance<IRepositoryResourceReader>();

            //Pass the args to the reader:
            reader.Initialize(resourceFilter, culture);

            //At this point, the reader's internal dictionary is ready to be iterated
            //over (as it is of type IEnuemrable...)


            foreach (KeyValuePair<string, object> dictionaryEntry in reader)
            {
                //string key = dictionaryEntry.Key;
                Table[dictionaryEntry.Key] = dictionaryEntry.Value;
            }


            //Save it (not sure why it's still needed after this).
            base.Reader = reader;

        }


        /// <summary>
        /// Returns the preferred resource reader class for this kind of 
        /// <see cref="T:System.Resources.ResourceSet"/>.
        /// </summary>
        /// <returns>
        /// Returns the <see cref="T:System.Type"/> for 
        /// the preferred resource reader for this kind of 
        /// <see cref="T:System.Resources.ResourceSet"/>.
        /// </returns>
        public override Type GetDefaultReader()
        {
            return typeof(RepositoryResourceReader);
        }




    } 
}
