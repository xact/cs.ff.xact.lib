﻿// ReSharper disable CheckNamespace
namespace XAct.Resources
// ReSharper restore CheckNamespace
{
    using System.Globalization;

    /// <summary>
    /// TODO
    /// </summary>
    public interface IRepositoryResourceSet
    {
        /// <summary>
        /// Initializes the reader.
        /// <para>
        /// Invoked by an instance of 
        /// <see cref="IRepositoryResourceManager"/> 
        /// after it has been created via ServiceLocator.
        /// </para>
        /// </summary>
        /// <param name="resourceFilter">The resource filter.</param>
        /// <param name="culture">The culture.</param>
        void InitializeReader(string resourceFilter, CultureInfo culture);
    }
}