﻿
// ReSharper disable CheckNamespace
namespace XAct.Resources
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Whereas an implementation of <c>RepositoryResourceProviderFactory</c>
    /// is how Resources are registered into an ASP.NET page,
    /// a Manager works directly in any assembly.
    /// </para>
    /// <para>
    /// In an ASP.NET based application, after registring an 
    /// implementation of 
    /// <c>RepositoryResourceProviderFactory</c> 
    /// in the web.config file,
    /// it instantiates implementations of 
    /// <c>IRepositoryResourceProvider</c>s -- one per 
    /// virtual path (ie, page) if Local, or per keyword, if Global.
    /// </para>
    /// <para>
    /// The implementation of <c>IRepositoryResourceProvider</c> in turn 
    /// wraps calls to an implementation of 
    /// <see cref="IRepositoryResourceManager"/>
    /// </para>
    /// <para>
    /// This implementation of <see cref="IRepositoryResourceManager"/>
    /// instantates implementations of <see cref="IRepositoryResourceSet"/>s.
    /// </para>
    /// </remarks>
    public interface IRepositoryResourceManager
    {
    }

}
