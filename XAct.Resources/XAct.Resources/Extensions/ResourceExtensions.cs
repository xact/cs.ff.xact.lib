﻿//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using XAct.Environment;
//using XAct.Resources;

//namespace XAct
//{
//    /// <summary>
//    /// Extensions to the <see cref="Resource"/> object.
//    /// </summary>
//    public static class ResourceExtensions
//    {
//        /// <summary>
//        /// Helper method to quickly Build and fill in a Resource entity.
//        /// </summary>
//        /// <param name="resource">The resource.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="key">The key.</param>
//        /// <param name="value">The value.</param>
//        /// <param name="cultureInfo">The culture info.</param>
//        /// <returns></returns>
//        public static Resource BuildResource(this Resource resource, string filter, string key, string value, CultureInfo cultureInfo = null)
//        {
//            string cultureCode = (cultureInfo == null) ? (string)null : cultureInfo.ToString();

//            return resource.BuildResource(filter, key, value, cultureCode);
//        }

//        /// <summary>
//        /// Helper method to quickly Build and fill in a Resource entity.
//        /// </summary>
//        /// <param name="resource">The resource.</param>
//        /// <param name="filter">The filter.</param>
//        /// <param name="key">The key.</param>
//        /// <param name="value">The value.</param>
//        /// <param name="cultureCode">The culture code.</param>
//        /// <returns></returns>
//        public static Resource BuildResource(this Resource resource, string filter, string key, string value, string cultureCode = null)
//        {
//            if (cultureCode == null)
//            {
//                cultureCode = string.Empty;
//            }
//            var environmentService = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>();

//            string applicationIdentifier = environmentService.ApplicationName;

//            resource.Key = key;
//            resource.Value = value;
//            resource.Filter = filter;
//            resource.CultureCode = cultureCode;
//            return resource;
//        }

//    }

//}
