﻿// ReSharper disable CheckNamespace
namespace XAct.Resources.Implementations
// ReSharper restore CheckNamespace
{
    using System.Globalization;
    using XAct.Diagnostics;
    using XAct.Messages;
    using XAct.Messages.Services.Configuration;
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="ICultureSpecificResponseService"/> contract to
    /// process the <c>Id</c>
    /// (and <c>Arguments</c> if any)
    /// of an instance of <see cref="Message"/>,
    /// and set the <c>Message</c>
    /// property of the given <see cref="Message"/> 
    /// value to a culture specific resource string.
    /// </summary>
    public class CultureSpecificResponseService : XActLibServiceBase, ICultureSpecificResponseService
    {
        private readonly IResourceService _resourceService;
        private readonly ICultureSpecificMessageServiceConfiguration _cultureSpecificMessageServiceConfiguration;


        /// <summary>
        /// Gets or sets the configuration used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public ICultureSpecificMessageServiceConfiguration Configuration { get { return _cultureSpecificMessageServiceConfiguration; } }


        /// <summary>
        /// Initializes a new instance of the <see cref="CultureSpecificResponseService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="cultureSpecificMessageServiceConfiguration">The culture specific message service configuration.</param>
        /// <param name="resourceService">The resource service.</param>
        public CultureSpecificResponseService(ITracingService tracingService, ICultureSpecificMessageServiceConfiguration cultureSpecificMessageServiceConfiguration,
            IResourceService resourceService):base(tracingService)
        {
            _cultureSpecificMessageServiceConfiguration = cultureSpecificMessageServiceConfiguration;
            _resourceService = resourceService;
        }

        /// <summary>
        /// Sets the presentation attributes on each <see cref="Message" />
        /// in the <see cref="IResponse" /> implementation.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="cultureInfo">The culture information.</param>
        public void SetPresentationAttributes(IResponse response, CultureInfo cultureInfo)
        {

            if (response.Messages.Count == 0)
            {
                return;
            }

            foreach (Message message in response.Messages)
            {
                SetPresentationAttributes(message,cultureInfo);
            }

        }


        /// <summary>
        /// Uses the <c>Id</c> value given
        /// (and <c>Arguments</c> if any)
        /// to find the related culture specific resource,
        /// and set the <c>IText</c>
        /// of the given <see cref="Message" />
        /// implementation.
        /// property.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cultureInfo">The culture information.</param>
        public void SetPresentationAttributes(Message message, CultureInfo cultureInfo)
        {

            //The resource key is a composite of the Configuration.ResourceSet (eg: 'MessageCode')
            //and the unique Id of the MessageCode.
            string resourceSetName = _cultureSpecificMessageServiceConfiguration.ResourceFilter;

            //As the default ResourceKeyFormat is "{0}", this turns the messageCode.Id (eg: 123) into "123"


            string resourceKey =
                _cultureSpecificMessageServiceConfiguration.ResourceKeyFormat.FormatStringInvariantCulture(message.MessageCode.Id);

            //So, with "MessageCode" and "123", we can find the resource Text:
            string resourceText = _resourceService.GetString(resourceSetName, resourceKey, cultureInfo);


            string additionalInformationText;
            if (!_cultureSpecificMessageServiceConfiguration.AdditionalInformationResourceKeyFormat.IsNullOrEmpty())
            {
                string additionalInformationResourceKey =
                    _cultureSpecificMessageServiceConfiguration.AdditionalInformationResourceKeyFormat
                                                               .FormatStringInvariantCulture(message.MessageCode.Id);

                try
                {
                    additionalInformationText = _resourceService.GetString(resourceSetName,
                                                                           additionalInformationResourceKey, cultureInfo);
                }
                catch
                {
                    additionalInformationText = string.Empty;
                }
            }
            else
            {
                additionalInformationText = string.Empty;
            }



            string additionalInformationUrlText;
            if (!_cultureSpecificMessageServiceConfiguration.AdditionalInformationUrlFormat.IsNullOrEmpty())
            {
                additionalInformationUrlText =
                    _cultureSpecificMessageServiceConfiguration.AdditionalInformationUrlFormat
                                                               .FormatStringInvariantCulture(message.MessageCode.Id);
            }
            else
            {
                additionalInformationUrlText = string.Empty;
            }



            MessagePresentationAttributes messagePresentationAttributes =
                message.PresentationAttributes ?? (message.PresentationAttributes = new MessagePresentationAttributes());

            messagePresentationAttributes.Text = resourceText;
            messagePresentationAttributes.AdditionalInformation = additionalInformationText;
            messagePresentationAttributes.AdditionalInformationUrl = additionalInformationUrlText;
        }


    }
}