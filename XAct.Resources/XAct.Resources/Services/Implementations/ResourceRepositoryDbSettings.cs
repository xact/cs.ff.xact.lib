﻿//using XAct.Data;
//using XAct.Services;

//namespace XAct.Resources.Implementations
//{
//    /// <summary>
//    /// An implementation of RepositorySettings, 
//    /// for an DependencyInjectionContainer 
//    /// to pass to to
//    /// <c>RepositoryResourceRepository</c> 
//    /// </summary>
//    [DefaultBindingImplementation(typeof(IResourceRepositoryDbSettings),BindingLifetimeType.SingletonScope,Priority.Low)]
//    public class ResourceRepositoryDbSettings : DbRepositorySettings, IResourceRepositoryDbSettings
//    {
        

//        /// <summary>
//        /// Gets or sets the name of the resource table.
//        /// <para>Default value = 'Resources'</para>
//        /// </summary>
//        /// <value>
//        /// The name of the resource table.
//        /// </value>
//        public string ResourceTableName { get; set; }


//        /// <summary>
//        /// Initializes a new instance of the <see cref="ResourceRepositoryDbSettings"/> class.
//        /// </summary>
//        public ResourceRepositoryDbSettings()
//        {
//            ConnectionStringSettingsName = DependencyResolver.Current.GetInstance<IHostSettingsService>().Get<string>(
//                    "XActLibConnectionStringName", XAct.Library.Settings.Db.DefaultXActLibConnectionStringSettingsName);
//            ResourceTableName = "Resources";
//            ParameterPrefix = XAct.Library.Settings.Db.DbParameterPrefix;
//        }
//    }
//}
