﻿namespace XAct.Resources.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Resources.Implementations;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class ResourceManagementService : XActLibServiceBase, IResourceManagementService
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IRepositoryResourceRepository _repositoryResourceRepositoryService;


    //    readonly RepositoryResourceManager _repositoryResourceManager =
    //new RepositoryResourceManager((string)null);

        //Keep the dictionary of resourceManagers in one place (in Management) 
        //rather than two -- so that this Manager can clear it after adding resources.
        readonly Dictionary<string,RepositoryResourceManager> _resourceManagers =
            new Dictionary<string, RepositoryResourceManager>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="repositoryResourceRepositoryService">The repository resource repository service.</param>
        public ResourceManagementService(ITracingService tracingService, IEnvironmentService environmentService,IRepositoryResourceRepository repositoryResourceRepositoryService):base(tracingService)
        {
            _environmentService = environmentService;
            _repositoryResourceRepositoryService = repositoryResourceRepositoryService;
        }

        private RepositoryResourceManager GetResourceManagerByFilter(string filter)
        {
            RepositoryResourceManager repositoryResourceManager;
            //Dictionary Key cannot be null:
            string filterKey = (filter.IsNullOrEmpty()) ? "Default" : filter;

            if (!_resourceManagers.TryGetValue(filterKey, out repositoryResourceManager))
            {
                repositoryResourceManager = new RepositoryResourceManager(filter);
                _resourceManagers[filterKey] = repositoryResourceManager;
            }
            return repositoryResourceManager;
        }


        /// <summary>
        /// Gets the localized string resource, localised to
        /// the CurrentUICulture, unless specified otherwise.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public string GetString(string name, CultureInfo cultureInfo=null)
        {
         
            if (cultureInfo == null)
            {
                cultureInfo = System.Threading.Thread.CurrentThread.CurrentUICulture;
            }

            var result = GetResourceManagerByFilter(null).GetString(name,cultureInfo);
            return result;
        }



        /// <summary>
        /// Gets the localized string resource, localised to
        /// the CurrentUICulture, unless specified otherwise.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public string GetString(string filter, string name, CultureInfo cultureInfo = null)
        {
            if (filter == null) { filter = string.Empty; }

            if (cultureInfo == null)
            {
                cultureInfo = System.Threading.Thread.CurrentThread.CurrentUICulture;
            }

            //note: Can accept null filter without failing.
            var result = GetResourceManagerByFilter(filter).GetString(name, cultureInfo);
            return result;
        }




        /// <summary>
        /// Gets all string resources for the given filter/page.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        Dictionary<string, string> IResourceService.GetAllStrings(string filter, CultureInfo cultureInfo)
        {
            var result = GetAllStrings(filter, cultureInfo);
            return result;
        }

        /// <summary>
        /// Gets all string resources for the given filter/page.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException">Repo must implement {0}.FormatStringInvariantCulture(repo.GetType().Name)</exception>
        public Dictionary<string, string> GetAllStrings(string filter, CultureInfo cultureInfo = null)
        {
            if (filter == null) { filter = string.Empty; }

            IXXX repo = GetResourceManagerByFilter(filter) as IXXX;

            if (repo == null)
            {
                throw new NotImplementedException("Repo must implement {0}".FormatStringInvariantCulture(typeof(IXXX).Name));
            }
            return repo.GetAllStrings(cultureInfo);
        }



        /// <summary>
        /// Gets all string resources for the given filters/pages.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public Dictionary<string, Dictionary<string, string>> GetAllStrings(CultureInfo cultureInfo, params string[] filters)
        {
            Dictionary<string, Dictionary<string, string>> results = new Dictionary<string, Dictionary<string, string>>();

            if (filters == null)
            {
                return results;
            }
            foreach (string filter in filters)
            {
                string tmp = filter.IsNullOrEmpty() ? string.Empty : filter;

                results[tmp] = GetAllStrings(tmp, cultureInfo);
            }
            return results;

        } 











        /// <summary>
        /// Persists the specified <see cref="Resource" />.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
        public void Persist(CultureInfo cultureInfo, string filter, string key, string value, bool commitChangesNow = true)
        {
            string cultureInfoCode = (cultureInfo != null) ? cultureInfo.ToString() : string.Empty;
            Persist(cultureInfoCode, filter, key, value, commitChangesNow);
        }

        /// <summary>
        /// Persists the specified <see cref="Resource" />.
        /// </summary>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
        public void Persist(string cultureInfoCode, string filter, string key, string value, bool commitChangesNow = true)
        {
            if (cultureInfoCode == null) { cultureInfoCode = string.Empty; }
            
            

            Resource resource = _repositoryResourceRepositoryService.GetResource<Resource>(filter, key, new CultureInfo(cultureInfoCode));

            if (resource == null)
            {
                //if (filterKey == null)
                //{
                //    filterKey = string.Empty;
                //}

                resource = new Resource
                    {

                        CultureCode = cultureInfoCode,
                        Filter = filter,
                        Key = key
                    };


            }
            resource.Value = value;

            _repositoryResourceRepositoryService.PersistResource(resource, commitChangesNow);


            _resourceManagers.Clear();
        }

        /// <summary>
        /// Deletes the specified culture info.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        public void Delete(CultureInfo cultureInfo, string filter, string key)
        {
            string cultureInfoCode = (cultureInfo != null) ? cultureInfo.ToString() : string.Empty;
            Delete(cultureInfoCode,filter,key);
        }
        /// <summary>
        /// Deletes the specified <see cref="Resource" />
        /// </summary>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        public void Delete(string cultureInfoCode, string filter, string key)
        {
            if (cultureInfoCode == null) { cultureInfoCode = string.Empty; }
        }

    }
}