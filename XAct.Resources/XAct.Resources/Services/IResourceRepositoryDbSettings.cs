﻿//using XAct.Configuration;
//using XAct.Data;

//namespace XAct.Resources
//{
//    /// <summary>
//    /// Contract for an argument package that can be passed by a
//    /// DependencyInjectionContainer to <see cref="IRepositoryResourceRepository"/>
//    /// </summary>
//    [ConfigurationSettings]
//    public interface IResourceRepositoryDbSettings : IDbConnectionSettings
//    {

//        /// <summary>
//        /// Gets or sets the name of the resource table.
//        /// <para>Default value = 'Resources'</para>
//        /// </summary>
//        /// <value>
//        /// The name of the resource table.
//        /// </value>
//        string ResourceTableName { get; set; }

//    }
//}