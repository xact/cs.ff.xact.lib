﻿namespace XAct.Resources
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// 
    /// </summary>
    public class RepositoryCultureResourceStringCache
    {
        private readonly Dictionary<string, Dictionary<string, string>> _cache =
            new Dictionary<string, Dictionary<string, string>>(System.StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Persist a resource against a culture and key.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void SaveEntry(CultureInfo cultureInfo, string key, string value)
        {
            string code = cultureInfo.ToString();

            SaveEntry(code, key, value);
        }

        /// <summary>
        /// Persist a resource against a culture and key.
        /// </summary>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void SaveEntry(string cultureInfoCode, string key, string value)
        {

            Dictionary<string, string> cultureSpecificDictionary;

            if (!_cache.TryGetValue(cultureInfoCode, out cultureSpecificDictionary))
            {
                cultureSpecificDictionary =
                    new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

                _cache.Add(cultureInfoCode, cultureSpecificDictionary);
            }

            cultureSpecificDictionary[key] = value;
        }


        /// <summary>
        /// Tries to get a culture specific string from the inner dictionaries.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="useCultureInfoFallBackLogic">if set to <c>true</c> [use culture info fall back logic].</param>
        /// <returns></returns>
        public bool TryGet(CultureInfo cultureInfo, string key, out string result,
                           bool useCultureInfoFallBackLogic = false)
        {
            string cultureInfoCode = cultureInfo.ToString();

            return TryGet(cultureInfoCode, key, out result, useCultureInfoFallBackLogic);
        }

        /// <summary>
        /// Tries to get a culture specific string from the inner dictionaries.
        /// </summary>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="useCultureInfoFallBackLogic">if set to <c>true</c> [use culture info fall back logic].</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException">RepositoryCultureResourceStringDictionary.TryGet(useCultureInfoFallBackLogic=true) is not implemented at present.</exception>
        public bool TryGet(string cultureInfoCode, string key, out string result,
                           bool useCultureInfoFallBackLogic = false)
        {
            if (useCultureInfoFallBackLogic)
            {
                throw new NotImplementedException(
                    "RepositoryCultureResourceStringDictionary.TryGet(useCultureInfoFallBackLogic=true) is not implemented at present.");

            }

            Dictionary<string, string> cultureSpecificDictionary;

            if (!_cache.TryGetValue(cultureInfoCode, out cultureSpecificDictionary))
            {
                result = null;
                return false;
            }

            if (!cultureSpecificDictionary.TryGetValue(key, out result))
            {
                return false;
            }

            return true;
        }
    }
}