﻿namespace XAct.Resources
{
    /// <summary>
    /// Contract for a Service
    /// to return Resources
    /// that can be rendered to the end user.
    /// </summary>
    public interface IMyResourceManagementService : IResourceManagementService, IHasXActLibService
    {
    }
}
