﻿namespace XAct.Resources
{
    using System.Globalization;

    /// <summary>
    /// 
    /// </summary>
    public interface IResourceManagementService : IResourceService, IHasXActLibService
    {
        /// <summary>
        /// Persists the specified <see cref="Resource" />.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
        void Persist(CultureInfo cultureInfo, string filter, string key, string value, bool commitChangesNow = true);


        /// <summary>
        /// Persists the specified <see cref="Resource" />.
        /// </summary>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
        void Persist(string cultureInfoCode, string filter, string key, string value, bool commitChangesNow = true);

        /// <summary>
        /// Deletes the specified <see cref="Resource"/>
        /// </summary>
        /// <param name="cultureInfo">The culture info code.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        void Delete(CultureInfo cultureInfo, string filter, string key);


        /// <summary>
        /// Deletes the specified <see cref="Resource"/>
        /// </summary>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        void Delete(string cultureInfoCode, string filter, string key);

    }
}
