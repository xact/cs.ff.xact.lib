﻿namespace XAct.Resources.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using XAct.Environment;
    using XAct.Resources.Services.State;
    using XAct.Services;

    /// <summary>
    /// Implementation 
    /// of the <see cref="IMyResourceManagementService"/>
    /// contract.
    /// </summary>
    [DefaultBindingImplementation(typeof(IResourceManagementService), BindingLifetimeType.Undefined, Priority.Low /*OK:SecondaryBinding*/)]
    public class MyResourceManagementService : IMyResourceManagementService
    {
                private readonly IEnvironmentService _environmentService;
        private readonly IDistributedIdService _distributedIdService;
        private readonly IRepositoryResourceRepository _repositoryResourceRepositoryService;
        private readonly IMyResourceManagementServiceState _myResourceManagementServiceCache;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="MyResourceManagementService" /> class.
        /// </summary>
        /// <param name="myResourceManagementServiceCache">My resource management service cache.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="distributedIdService">The distributed identifier service.</param>
        /// <param name="repositoryResourceRepositoryService">The repository resource repository service.</param>
        public MyResourceManagementService(IMyResourceManagementServiceState myResourceManagementServiceCache,
            IEnvironmentService environmentService,
            IDistributedIdService distributedIdService,
            IRepositoryResourceRepository repositoryResourceRepositoryService)
        {
            _myResourceManagementServiceCache = myResourceManagementServiceCache;
            _environmentService = environmentService;
    _distributedIdService = distributedIdService;
    _repositoryResourceRepositoryService = repositoryResourceRepositoryService;
        }



        private IMyResourceManager GetResourceManagerByFilter(string filter)
        {
            IMyResourceManager repositoryResourceManager;
            //Dictionary Key cannot be null:

            if (filter == null)
            {
                filter = string.Empty;
            }

            //string filterKey = (filter.IsNullOrEmpty()) ? "" : filter;
            string filterKey = filter;

            if (!_myResourceManagementServiceCache.TryGetValue(filterKey, out repositoryResourceManager))
            {
                //Instantiate it by making it injectable:
                repositoryResourceManager = XAct.DependencyResolver.Current.GetInstance<IMyResourceManager>();

                //Set to Filter (not FilterKey) as we want it to search in db using column that is ""
                repositoryResourceManager.BaseName  = filter; 

                _myResourceManagementServiceCache[filterKey] = repositoryResourceManager;
            }
            return repositoryResourceManager;
        }


        /// <summary>
        /// Gets the localized string resource, localised to
        /// the CurrentUICulture, unless specified otherwise.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public string GetString(string name, CultureInfo cultureInfo=null)
        {
         
            if (cultureInfo == null)
            {
                cultureInfo = System.Threading.Thread.CurrentThread.CurrentUICulture;
            }

            var result = GetResourceManagerByFilter(null).GetString(name,cultureInfo);
            return result;
        }



        /// <summary>
        /// Gets the localized string resource, localised to
        /// the CurrentUICulture, unless specified otherwise.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public string GetString(string filter, string name, CultureInfo cultureInfo = null)
        {
            if (filter == null) { filter = string.Empty; }

            if (cultureInfo == null)
            {
                cultureInfo = System.Threading.Thread.CurrentThread.CurrentUICulture;
            }

            //note: Can accept null filter without failing.
            var resourceManager =  GetResourceManagerByFilter(filter);
            
            if (resourceManager==null)
            {
                return null;
            }
            var result = resourceManager.GetString(name, cultureInfo);
            return result;
        }




        /// <summary>
        /// Gets all string resources for the given filter/page.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        Dictionary<string, string> IResourceService.GetAllStrings(string filter, CultureInfo cultureInfo)
        {
            var result = GetAllStrings(filter, cultureInfo);
            return result;
        }

        /// <summary>
        /// Gets all string resources for the given filter/page.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException">Repo must implement {0}.FormatStringInvariantCulture(repo.GetType().Name)</exception>
        public Dictionary<string, string> GetAllStrings(string filter, CultureInfo cultureInfo = null)
        {
            if (filter == null) { filter = string.Empty; }

            IXXX repo = GetResourceManagerByFilter(filter) as IXXX;

            if (repo == null)
            {
                throw new NotImplementedException("Repo must implement {0}".FormatStringInvariantCulture(typeof(IXXX).Name));
            }

            return repo.GetAllStrings(cultureInfo);
        }



        /// <summary>
        /// Gets all string resources for the given filters/pages.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public Dictionary<string, Dictionary<string, string>> GetAllStrings(CultureInfo cultureInfo, params string[] filters)
        {
            Dictionary<string, Dictionary<string, string>> results = new Dictionary<string, Dictionary<string, string>>();

            if (filters == null)
            {
                return results;
            }
            foreach (string filter in filters)
            {
                string tmp = filter.IsNullOrEmpty() ? string.Empty : filter;

                results[tmp] = GetAllStrings(tmp, cultureInfo);
            }
            return results;

        } 











        /// <summary>
        /// Persists the specified <see cref="Resource" />.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
        public void Persist(CultureInfo cultureInfo, string filter, string key, string value, bool commitChangesNow = true)
        {
            string cultureInfoCode = (cultureInfo != null) ? cultureInfo.ToString() : string.Empty;
            Persist(cultureInfoCode, filter, key, value, commitChangesNow);
        }

        /// <summary>
        /// Persists the specified <see cref="Resource" />.
        /// </summary>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
        public void Persist(string cultureInfoCode, string filter, string key, string value, bool commitChangesNow = true)
        {
            if (cultureInfoCode == null) { cultureInfoCode = string.Empty; }

            CultureInfo cultureInfo = new CultureInfo(cultureInfoCode);

            Resource resource = _repositoryResourceRepositoryService.GetResource<Resource>(filter, key, cultureInfo);

            bool isNew;
            
            if (resource == null)
            {
                isNew = true;
                //if (filterKey == null)
                //{
                //    filterKey = string.Empty;
                //}

                resource = new Resource
                    {
                        Id = _distributedIdService.NewGuid(),
                        CultureCode = cultureInfo.Name,
                        Filter = filter,
                        Key = key
                    };


            }
            else
            {
                isNew = false;
            }
            resource.Value = value;

            _repositoryResourceRepositoryService.PersistResource(resource, isNew, commitChangesNow);

            //Reset everything:
            _myResourceManagementServiceCache.Clear();

        }


        /// <summary>
        /// Deletes the specified culture info.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        public void Delete(CultureInfo cultureInfo, string filter, string key)
        {
            string cultureInfoCode = (cultureInfo != null) ? cultureInfo.ToString() : string.Empty;
            Delete(cultureInfoCode,filter,key);
        }
        /// <summary>
        /// Deletes the specified <see cref="Resource" />
        /// </summary>
        /// <param name="cultureInfoCode">The culture info code.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        public void Delete(string cultureInfoCode, string filter, string key)
        {
            if (cultureInfoCode == null) { cultureInfoCode = string.Empty; }
        }

   }
}