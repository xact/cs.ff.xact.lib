﻿namespace XAct.Resources.Services.Implementations
{
    using System.Collections.Generic;
    using System.Globalization;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class ResourceService : IResourceService
    {
        private readonly ITracingService _tracingService;
        private readonly IResourceManagementService _resourceManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="resourceManagementService">The resource mangement service.</param>
        public ResourceService(ITracingService tracingService, IResourceManagementService resourceManagementService)
        {
            _tracingService = tracingService;
            _resourceManagementService = resourceManagementService;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //public void Ping()
        //{

        //}

        /// <summary>
        /// Gets the localized string resource, localised to
        /// the CurrentUICulture, unless specified otherwise.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public string GetString(string name, CultureInfo cultureInfo=null)
        {

            return _resourceManagementService.GetString(name,cultureInfo);
        }



        /// <summary>
        /// Gets the localized string resource, localised to
        /// the CurrentUICulture, unless specified otherwise.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public string GetString(string filter, string name, CultureInfo cultureInfo = null)
        {
            return _resourceManagementService.GetString(filter, name, cultureInfo);
        }



        Dictionary<string, string> IResourceService.GetAllStrings(string filter, CultureInfo cultureInfo)
        {
            return _resourceManagementService.GetAllStrings(filter, cultureInfo);
        }

        /// <summary>
        /// Gets all string resources for the given filter/page.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException">Repo must implement {0}.FormatStringInvariantCulture(repo.GetType().Name)</exception>
        public Dictionary<string, string> GetAllStrings(string filter, CultureInfo cultureInfo = null)
        {
            return _resourceManagementService.GetAllStrings(filter, cultureInfo);
        }


        /// <summary>
        /// Gets all string resources for the given filters/pages.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="filters">The filters.</param>
        /// <returns></returns>
        public Dictionary<string, Dictionary<string, string>> GetAllStrings(CultureInfo cultureInfo, params string[] filters)
        {
            return _resourceManagementService.GetAllStrings(cultureInfo, filters);

        } 

        
    }
}