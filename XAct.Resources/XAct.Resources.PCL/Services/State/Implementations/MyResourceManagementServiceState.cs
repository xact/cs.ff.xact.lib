﻿namespace XAct.Resources.Services.State.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// Cache of <see cref="IMyResourceManager"/>
    /// per resourceFilter/BaseName.
    /// </summary>
    public class MyResourceManagementServiceState : Dictionary<string,IMyResourceManager>, IMyResourceManagementServiceState, IHasXActLibServiceState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyResourceManagementServiceState"/> class.
        /// </summary>
        public MyResourceManagementServiceState():base(StringComparer.OrdinalIgnoreCase){}
    }
}