﻿namespace XAct.Resources.Services.State
{
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public interface IMyResourceManagementServiceState : IDictionary<string, IMyResourceManager>, IHasXActLibServiceState
    {
       
    }
}