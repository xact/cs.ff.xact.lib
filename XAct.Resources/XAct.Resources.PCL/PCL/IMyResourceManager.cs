﻿namespace XAct.Resources
{
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// 
    /// </summary>
    public interface IMyResourceManager
    {

        /// <summary>
        /// Gets the Name (the Resource Filter) of this <see cref="IMyResourceManager"/>.
        /// </summary>
        /// <value>
        /// The name of the base.
        /// </value>
        string BaseName { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether to allow Recursion when 
        /// retrieving Resource strings.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow recursion]; otherwise, <c>false</c>.
        /// </value>
        bool AllowRecursion { get; set; }







        /// <summary>
        /// Returns the value of the specified string resource.
        /// </summary>
        /// <param name="name">The name of the resource to retrieve.</param>
        /// <returns>
        /// The value of the resource localized for the caller's current UI culture, or null if <paramref name="name" /> cannot be found in a resource set.
        /// </returns>
        string GetString(string name);

        /// <summary>
        /// Returns the value of the string resource localized for the specified culture.
        /// </summary>
        /// <param name="name">The name of the resource to retrieve.</param>
        /// <param name="culture">An object that represents the culture for which the resource is localized.</param>
        /// <returns>
        /// The value of the resource localized for the specified culture, or null if <paramref name="name" /> cannot be found in a resource set.
        /// </returns>
        string GetString(string name, CultureInfo culture);

        /// <summary>
        /// Gets all resources for page.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        Dictionary<string, string> GetAllStrings(CultureInfo cultureInfo = null);







    }
}
