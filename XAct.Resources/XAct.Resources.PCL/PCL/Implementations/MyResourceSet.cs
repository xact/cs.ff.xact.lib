﻿namespace XAct.Resources.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IMyResourceSet), BindingLifetimeType.TransientScope, Priority.Low)]
    public class MyResourceSet : IMyResourceSet
    {
        private readonly IRepositoryResourceRepository _repositoryResourceRepository;
        private string _resourceFilter;
        private Dictionary<string, string> _resources; 


        /// <summary>
        /// Initializes a new instance of the <see cref="MyResourceSet"/> class.
        /// </summary>
        /// <param name="repositoryResourceRepository">The repository resource repository.</param>
        public MyResourceSet(IRepositoryResourceRepository repositoryResourceRepository) 
        {
            _repositoryResourceRepository = repositoryResourceRepository;
        }

        /// <summary>
        /// Initializes the specified resource filter.
        /// </summary>
        /// <param name="resourceFilter">The resource filter.</param>
        /// <param name="cultureInfo">The culture information.</param>
        public void Initialize(string resourceFilter, CultureInfo cultureInfo)
        {
            _resourceFilter = resourceFilter;
            _resources = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            foreach(KeyValuePair<string,object> kvp in _repositoryResourceRepository.GetResources(_resourceFilter,cultureInfo))
            {
                _resources.Add(kvp.Key,kvp.Value.ToString());
            }
        }

        /// <summary>
        /// Gets all strings.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetAllStrings()
        {
            return _resources;
        }


        /// <summary>
        /// Tries the get.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool TryGetValue(string key, out string value)
        {
            return _resources.TryGetValue(key, out value);
        }
    }
}