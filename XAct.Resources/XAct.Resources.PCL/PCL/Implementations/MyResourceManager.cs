﻿namespace XAct.Resources.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text.RegularExpressions;
//    using XAct.Library.Settings;
    using XAct.Library.Settings;
    using XAct.Services;

    /// <summary>
    /// A manager managers one Filter -> ResourceSet
    /// </summary>
    [DefaultBindingImplementation(typeof(IMyResourceManager), BindingLifetimeType.TransientScope, Priority.Low)]
    public class MyResourceManager : IMyResourceManager , IXXX
    {

        /// <summary>
        /// Gets the Name (the Resource Filter) of this <see cref="IMyResourceManager"/>.
        /// </summary>
        /// <value>
        /// The name of the base.
        /// </value>
        public string BaseName { get; set; }

        private IDictionary<string, IMyResourceSet> ResourceSets
        {
            get { return _resourceSets; }
        }
        private Dictionary<string, IMyResourceSet> _resourceSets = 
            new Dictionary<string, IMyResourceSet>(StringComparer.OrdinalIgnoreCase);

        //Culture to Key,Value
        private readonly RepositoryCultureResourceStringCache _recursiveResourceStringCache =
            new RepositoryCultureResourceStringCache();



        /// <summary>
        /// Gets or sets a value indicating whether to allow Recursion when 
        /// retrieving Resource strings.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow recursion]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowRecursion { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="MyResourceManager"/> class.
        /// </summary>
        public MyResourceManager()
        {
            AllowRecursion = Globalisation.AllowResourceRecursion;

        }

        /// <summary>
        /// Initializes the specified base name.
        /// </summary>
        /// <param name="baseName">Name of the base.</param>
        public void Initialize(string baseName)
        {
            this.BaseName = baseName;
        }

        

        /// <summary>
        /// Returns the value of the specified string resource.
        /// </summary>
        /// <param name="name">The name of the resource to retrieve.</param>
        /// <returns>
        /// The value of the resource localized for the caller's current UI culture, or null if <paramref name="name" /> cannot be found in a resource set.
        /// </returns>
        public string GetString(string name)
        {
            string result;

            GetRecursiveString(CultureInfo.InvariantCulture, name, out result);

            return result;

            //return base.GetString(name);
        }

        /// <summary>
        /// Returns the value of the string resource localized for the specified culture.
        /// </summary>
        /// <param name="name">The name of the resource to retrieve.</param>
        /// <param name="culture">An object that represents the culture for which the resource is localized.</param>
        /// <returns>
        /// The value of the resource localized for the specified culture, or null if <paramref name="name" /> cannot be found in a resource set.
        /// </returns>
        public string GetString(string name, CultureInfo culture)
        {

            string result;

            GetRecursiveString(culture, name, out result);
            return result;
        }


        private bool GetRecursiveString(CultureInfo cultureInfo, string key, out string result, string parentNamePath = null)
        {

            if (!this.AllowRecursion)
            {

                result = InternalGetString(key, cultureInfo);

                return (result != null);
            }

            if (parentNamePath == null)
            {
                parentNamePath = key;
            }

            if (_recursiveResourceStringCache.TryGet(cultureInfo, key, out result))
            {
                return true;
            }

            //Ensure we're calling base, or we'll stack overflow:
            result = InternalGetString(key, cultureInfo);

            if (result == null)
            {
                return false;
            }

            foreach (string subKeyWord in result.FindLabeledPlaceHolders())
            {
                //Have Keyword: Is there a replacement?

                //Recurse on this method to find child objects:
                string subReplacement;

                //The ParentNamePath is just for debugging purposes, to know 
                //how deep we are recursing. An int would not tell us what was the 
                //entry point.

                if (GetRecursiveString(cultureInfo, subKeyWord, out subReplacement, parentNamePath + "/"))
                {
                    result =
                        Regex.Replace(
                            result,
                            "{" + subKeyWord + "}",
                            subReplacement,
                            RegexOptions.Multiline);
                }
            }

            //Done with looping at this level, whatever it is, 
            //and since we know we were a keyword, we can persist it.

            _recursiveResourceStringCache.SaveEntry(cultureInfo ,key, result);

            return true;
        }



        /// <summary>
        /// Internals the get string.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <returns></returns>
        public string InternalGetString(string key, CultureInfo cultureInfo)
        {
            string value;
            if (GetAllStrings(cultureInfo).TryGetValue(key, out value))
            {
                return value;
            }
            return null;
        }


        /// <summary>
        /// Gets all resources for page.
        /// </summary>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetAllStrings(CultureInfo cultureInfo)
        {
            if (cultureInfo == null)
            {
                cultureInfo = CultureInfo.InvariantCulture;
            }

            IMyResourceSet resourceSet = InternalGetResourceSet(cultureInfo, true);

            return resourceSet.GetAllStrings();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="createIfNotExists"></param>
        /// <returns></returns>
        protected IMyResourceSet InternalGetResourceSet(
            CultureInfo culture, bool createIfNotExists)
        {
            IMyResourceSet resourceSet = null;


            if (!ResourceSets.ContainsKey(culture.Name))
            {
                resourceSet = XAct.DependencyResolver.Current.GetInstance<IMyResourceSet>();
                
                //Set ResourceFilter, and load data from Repo:
                resourceSet.Initialize(this.BaseName, culture);

                //Persist:
                ResourceSets[culture.Name] = resourceSet;
            }

            resourceSet = (IMyResourceSet)ResourceSets[culture.Name];

            return resourceSet;
        }





    }
}