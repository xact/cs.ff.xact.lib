﻿// ReSharper disable CheckNamespace
namespace XAct.Resources
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// The contract for a Repository to
    /// retrieve Resources from a datastore.
    /// </summary>
    public interface IRepositoryResourceRepository
    {

        /// <summary>
        /// Gets a single Resource.
        /// </summary>
        /// <param name="resourceFilter">Type of the resource.</param>
        /// <param name="key">The key.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        T GetResource<T>(string resourceFilter, string key, CultureInfo culture = null)
            where T : class, IResource;


        /// <summary>
        /// Persists the given <see cref="Resource" /> entity to the underlying datastore.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resource">The resource.</param>
        /// <param name="isNew">if set to <c>true</c> [is new].</param>
        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
        void PersistResource<T>(T resource, bool isNew, bool commitChangesNow=true)
            where T : class, IResource;




        /// <summary>
        /// Gets a dictionary of all values in the specified culture.
        /// </summary>
        /// <param name="resourceFilter">Type of the resource.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        Dictionary<string, object> GetResources(string resourceFilter, CultureInfo culture);

    }
}