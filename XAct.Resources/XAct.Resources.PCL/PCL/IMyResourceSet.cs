﻿namespace XAct.Resources
{
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// 
    /// </summary>
    public interface IMyResourceSet
    {
        /// <summary>
        /// Initializes the specified resource filter.
        /// </summary>
        /// <param name="resourceFilter">The resource filter.</param>
        /// <param name="cultureInfo">The culture information.</param>
        void Initialize(string resourceFilter, CultureInfo cultureInfo);

        /// <summary>
        /// Tries the get.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        bool TryGetValue(string key, out string value);

        /// <summary>
        /// Gets all strings.
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetAllStrings();
    }
}
