﻿namespace XAct.Resources
{
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// 
    /// </summary>
    public interface IXXX
    {
        /// <summary>
        /// Gets all resources for page.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        Dictionary<string, string> GetAllStrings(CultureInfo culture);

    }
}