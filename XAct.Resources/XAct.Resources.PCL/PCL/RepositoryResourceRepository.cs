﻿namespace XAct.Resources.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An EF based implementation of <see cref="IRepositoryResourceRepository"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(IRepositoryResourceRepository), BindingLifetimeType.Undefined, Priority.Low)]
    public class RepositoryResourceRepository : IRepositoryResourceRepository
    {
        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryResourceRepository" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService"></param>
        /// <param name="repositoryService">The repository service.</param>
        public RepositoryResourceRepository(ITracingService tracingService, IEnvironmentService environmentService, XAct.Domain.Repositories.IRepositoryService repositoryService)
        {
            _tracingService = tracingService;
            _environmentService = environmentService;
            _repositoryService = repositoryService;
        }


        /// <summary>
        /// Gets the resource.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resourceFilter">The resource filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="cultureInfo">The cultureInfo.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public T GetResource<T>(string resourceFilter, string key, CultureInfo cultureInfo = null)
            where T : class, IResource
        {

            bool useCodes = false;


            //ResourceFilter is now part of Key, and non-nullable:
            if (resourceFilter == null)
            {
                resourceFilter = string.Empty;
            }
            //string resourceFilterLowered = (resourceFilter == null) ? null : resourceFilter.ToLower();
            //string resourceFilterLowered = resourceFilter.ToLower();


            string cultureInfoCode;
            if (!useCodes)
            {
                cultureInfoCode = (cultureInfo == null) ? string.Empty : cultureInfo.ToString();

                return _repositoryService.GetSingle<T>(
                    r => (
                             //Now that Filter lowered is non-nullable:
                             (string.Compare(r.Filter, resourceFilter,StringComparison.OrdinalIgnoreCase)==0)

                             //(resourceFilterLowered == null
                             //     ? r.FilterLowered == null
                             //     : r.FilterLowered == resourceFilterLowered)
                             //(Object.Equals(r.FilterLowered, resourceFilterLowered))
                             &&
                             (string.Compare(r.CultureCode, cultureInfoCode,StringComparison.OrdinalIgnoreCase)==0)
                             &&
                             (string.Compare(r.Key, key, StringComparison.OrdinalIgnoreCase) == 0)
                )
                    );
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Persists the resource.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resource">The resource.</param>
        /// <param name="isNew">if set to <c>true</c> [is new].</param>
        /// <param name="commitChangesNow">if set to <c>true</c> [commit changes now].</param>
        public void PersistResource<T>(T resource,bool isNew, bool commitChangesNow = true)
            where T : class, IResource
        {
            //Need flag as without an Id, can't determine whether entry saved or not.
            if (isNew)
            {
                _repositoryService.AddOnCommit(resource);
            }
            else
            {
                _repositoryService.UpdateOnCommit(resource);
            }
            if (commitChangesNow)
            {
                _repositoryService.GetContext().Commit(CommitType.Default);
            }
        }

        class Tmp
        {
#pragma warning disable 169
            public string[] Codes;
            public Resource[] R2;
            public Resource[] R3;
#pragma warning restore 169

        }


        /// <summary>
        /// Invoked by 
        /// </summary>
        /// <param name="resourceFilter"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public Dictionary<string, object> GetResources(string resourceFilter, CultureInfo cultureInfo)
        {
            //ResourceFilter is now part of Key, and non-nullable:
            if (resourceFilter == null)
            {
                resourceFilter = string.Empty;
            }

            /*
            stringBuilder.Append("SELECT");
            stringBuilder.Append(" [Key], Value");
            stringBuilder.Append(" FROM {1}");
            stringBuilder.Append(" WHERE (FilterLowered={0})");
            stringBuilder.Append(" AND (CultureCodeLowered IN");
            stringBuilder.Append(stringCodes);
            stringBuilder.Append(" )");
            //Order by 'fr-FR', then 'fr', then ''
            stringBuilder.Append(" ORDER BY KeyLowered, LEN(CultureCodeLowered) ASC");
             */

            Dictionary<string, object> results = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            CultureInfo defaultUICulture =  _environmentService.DefaultCulture;

            //Build a list of cultures, starting with the DefaultCulture, adding 
            //on the current culture. (eg: ["en-US","en-NZ"] or ["en-US","en"])
            List<CultureInfo> culturesToQueryFor = new List<CultureInfo>();

            //Add Pass #1:
            culturesToQueryFor.Add(CultureInfo.InvariantCulture);


            //Add Pass #2:
            if ((!Equals(defaultUICulture, CultureInfo.InvariantCulture)))
            {
                culturesToQueryFor.Add(defaultUICulture);
            }

            //Add Pass #3:
            //We don't need to add the requested culture if it's exactly the same
            //as the DefaultCulture.
            if ((!Equals(cultureInfo, culturesToQueryFor.Last())) && (!Equals(cultureInfo,CultureInfo.InvariantCulture)))
            {
                culturesToQueryFor.Add(cultureInfo);
            }


            culturesToQueryFor.Reverse();

            List<Tmp> checks = new List<Tmp>();

            //We then use the above list to iterate through from 
            //the most generic (the Default)
            //to the most specific (eg "en-NZ")
            foreach (CultureInfo culture2 in culturesToQueryFor)
            {
                string[] codes = culture2.BuildCultureCodeList(true /*(cultureInfo == defaultCulture)*/);


                IQueryable<Resource> subresults = _repositoryService.GetByFilter<Resource>(
                    r => (
                             r.Filter.Equals(resourceFilter, System.StringComparison.OrdinalIgnoreCase)
                             &&
                             (codes.Contains(r.CultureCode))
                         ),
                    null,
                    null
                    );

                
               var r2 = subresults
                   .OrderBy(r => r.Key)
                   .ThenByDescending(r => r.CultureCode.Length)  //from fr-FR to fr to ""
                   .ToArray();

                var r3 = r2.DistinctBy(x => x.Key).ToArray();

                //.Select(s=> new {s.Key,s.Value})
                
                //IEnumerable<Resource> subResources = subresults.DistinctBy(x => x.KeyLowered);

#if DEBUG
#pragma warning disable 168
                var t = subresults.ToArray();
#pragma warning restore 168
#endif

                foreach (Resource s in r3)
                {
                    if (results.ContainsKey(s.Key))
                    {
                        continue;
                    }
                    results[s.Key] = s.Value;
                }

                checks.Add(new Tmp { Codes = codes, R2 = r2, R3 = r3 });
            }

            return results;
        }

    }
}
