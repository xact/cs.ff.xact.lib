//namespace XAct.Resources.Tests
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Diagnostics;
//    using System.Globalization;
//    using NUnit.Framework;
//    using XAct.Bootstrapper.Tests;
//    using XAct.Diagnostics;
//    using XAct.Domain.Repositories;
//    using XAct.Resources.Implementations;
//    using XAct.Tests;

//    /// <summary>
//    ///   NUNit Test Fixture.
//    /// </summary>
//    [TestFixture]
//    public class RepositoryResourceManagerTests
//    {
//        /// <summary>
//        ///   Sets up to do before any tests 
//        ///   within this test fixture are run.
//        /// </summary>
//        [TestFixtureSetUp]
//        public void TestFixtureSetUp()
//        {
//            //Trace.Listeners.Add(new TextWriterTraceListener(Console.Out)); Trace.WriteLine("...");
            
//            //run once before any tests in this testfixture have been run...
//            //...setup vars common to all the upcoming tests...
            
//                        YayaAttribute.BeforeTest();


//            //Initialize the db using the common initializer passing it one entity from this class
//            //so that it can perform a query (therefore create db as necessary):
//            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<Resource>();

//            //Trace.WriteLine(":::C:" + XAct.Library.Settings.Globalisation.DefaultUICulture);

//        }

//        /// <summary>
//        ///   Tear down after all tests in this fixture.
//        /// </summary>
//        [TestFixtureTearDown]
//        public void TestFixtureTearDown()
//        {
//        }

//        /// <summary>
//        ///   An Example Test.
//        /// </summary>
//        [Test]
//        public void CanCreateResourceManagerWithNoName()
//        {


//            RepositoryResourceManager manager = 
//                new RepositoryResourceManager((string)null);

//            Assert.IsTrue(manager != null);
//        }

//        /// <summary>
//        ///   An Example Test.
//        /// </summary>
//        [Test]
//        public void CanRetrieveACaseSensitiveValueNoCultureDefined()
//        {


//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);

//            string resourceValue  = manager.GetString("KeyA");
//            Assert.IsFalse(resourceValue.IsNullOrEmpty());
//        }

//        /// <summary>
//        ///   An Example Test.
//        /// </summary>
//        [Test]
//        public void CanRetrieveACaseInSensitiveValueNoCultureDefined()
//        {


//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);
//            manager.IgnoreCase = false;
//            string resourceValue = manager.GetString("kEya");
//            Assert.IsTrue(resourceValue.IsNullOrEmpty());
//        }
//        [Test]
//        public void CanRetrieveACaseInSensitiveValueNoCultureDefined2()
//        {


//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);
//            manager.IgnoreCase = true;
//            string resourceValue = manager.GetString("kEya");
//            Assert.IsFalse(resourceValue.IsNullOrEmpty());
//        }


//        [Test]
//        public void CanRetrieveACaseInSensitiveValueInCulture_fr_FR()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);
         
//            manager.IgnoreCase = true;
            
//            string resourceValue = manager.GetString("KeyA",new CultureInfo("fr-FR"));
//            Assert.AreEqual("Some fr-FR value.", resourceValue);
//        }
//        [Test]
//        public void CanRetrieveACaseInSensitiveValueInCulture_fr()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);

//            manager.IgnoreCase = true;

//            string resourceValue = manager.GetString("KeyA", new CultureInfo("fr"));
//            Assert.AreEqual("Some fr value.", resourceValue);
//        }

//        [Test]
//        public void CanRetrieveACaseInSensitiveValueInCulture_br_FR()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);

//            manager.IgnoreCase = true;

//            string resourceValue = manager.GetString("KeyA", new CultureInfo("br-FR"));
//            Assert.AreEqual("Some br-FR value.", resourceValue);
//        }


//                [Test]
//        public void CanRetrieveACaseInSensitiveValueInCulture_br()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);

//            manager.IgnoreCase = true;

//            string resourceValue = manager.GetString("KeyA", new CultureInfo("br"));
//            Assert.AreEqual("Some br value.", resourceValue);
//        }

//        [Test]
//        public void ThrowsExceptionIfUsingNonSensicalCultureCode()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);

//            manager.IgnoreCase = true;
//            try
//            {
//#pragma warning disable 168
//                string resourceValue = manager.GetString("KeyA", new CultureInfo("br-ZA"));
//#pragma warning restore 168
//            }
//#pragma warning disable 168
//            catch(System.Globalization.CultureNotFoundException)
//#pragma warning restore 168
//            {
//                Assert.IsTrue(true);
//                return;
//            }
//            Assert.IsFalse(true);
//        }

//        [Test]
//        public void CanRetrieveACaseInSensitiveValueInCulture_ne_BE()
//        {


//            Trace.WriteLine(":::1:" + XAct.Library.Settings.Globalisation.DefaultUICulture);
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string) null);

//            manager.IgnoreCase = true;
//            string resourceValue = manager.GetString("KeyA", new CultureInfo("nl-BE"));

//            //As no values for the given culture, will fallback to default
//            //culture, as defined in 
//            Trace.WriteLine(":::2:" + XAct.Library.Settings.Globalisation.DefaultUICulture);

//#pragma warning disable 168
//            string defaultCultureCode = XAct.Library.Settings.Globalisation.DefaultUICulture.ToString();
//#pragma warning restore 168

//            if (XAct.Library.Settings.Globalisation.DefaultUICulture.ToString() == "fr-FR")
//            {
//                //But when run in test rig, appears tests are run alpahbetically
//                //so the culture was updated to french due to 
//                //CanNotChangeDefaultCultureAfterStartup which is run before
//                //CanRetrieveACaseInSensitiveValueInCulture_ne_BE

//                Assert.AreEqual("Some fr-FR value.", resourceValue);
//            }
//            Assert.AreEqual("Some (invariant) value.", resourceValue);

//        }


//        [Test]
//        public void CanNotChangeDefaultCultureAfterStartup()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);


//            manager.IgnoreCase = true;
//            string resourceValue = manager.GetString("KeyA", new CultureInfo("nl-BE"));

//            //As no values for the given culture, will fallback to default
//            //culture, as defined in
//            CultureInfo hold = XAct.Library.Settings.Globalisation.DefaultUICulture;
//            XAct.Library.Settings.Globalisation.DefaultUICulture = new CultureInfo("fr-FR");

//#pragma warning disable 168
//            string defaultCultureCode = XAct.Library.Settings.Globalisation.DefaultUICulture.ToString();
//#pragma warning restore 168

//            resourceValue = manager.GetString("KeyA", new CultureInfo("nl-BE"));


//            //Let's change it back to original, to not mess up any later tests:
//            XAct.Library.Settings.Globalisation.DefaultUICulture = hold;

//            //Although 
//            Assert.AreEqual("Some (invariant) value.", resourceValue);

//        }



//        [Test]
//        public void DoNotThrowExceptionIfResourceNotFound()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string) null);

//                string result = manager.GetString("NonExistentKeyXYZ123");

//            Assert.IsTrue(result.IsNullOrEmpty());

//        }



//        [Test]
//        public void RetrievesARecursiveEntry()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);

//            string result = manager.GetString("RecursiveA1");

//            Assert.IsFalse(result.IsNullOrEmpty());

//        }

//        [Test]
//        public void RetrievesAndParsesCorrectlyARecursiveEntry()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string) null);

//            string result = manager.GetString("RecursiveA1");

//            Assert.IsFalse(result.IsNullOrEmpty());

//            Assert.AreEqual("Something that includes a second reference.", result);
//        }

//        [Test]
//        public void RetrievesAndParsesCorrectlyAMultipleRecursiveEntry()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string) null);

//            string result = manager.GetString("RecursiveB1");

//            Assert.IsFalse(result.IsNullOrEmpty());

//            Assert.AreEqual("Something that includes another sentence that includes yet another sentence.", result);
//        }


//        //[Test]
//        //public void ReleaseAllResourcesDoesNotCauseReloadOfResources()
//        //{
//        //    RepositoryResourceManager manager =
//        //        new RepositoryResourceManager((string)null);

//        //    string result = manager.GetString("KeyA");
//        //    Assert.IsFalse(result.IsNullOrEmpty());

//        //    //TODO: Should not throw an exception...but can't fix right now.
//        //    manager.ReleaseAllResources();

//        //    result = manager.GetString("KeyA");

//        //    Assert.IsTrue(result.IsNullOrEmpty());

//        //}

//        [Test]
//        public void ResetDbBackRepositories()
//        {
//#pragma warning disable 168
//            RepositoryResourceManager manager =
//#pragma warning restore 168
//                new RepositoryResourceManager((string) null);

            
//        }



//        [Test]
//        public void CanRetrieve30000ResourcesInLessThanASecond()
//        {
//            GetResourceNTimes(30000, 1000);
//        }
//        [Test]
//        public void CanRetrieve50000ResourcesInLessThanASecond()
//        {
//            GetResourceNTimes(50000, 1000);
//        }
//        [Test]
//        public void CanRetrieve100000ResourcesInLessThanASecond()
//        {
//            GetResourceNTimes(100000, 1000);
//        }
//        [Test]
//        public void CanRetrieve500000ResourcesInLessThanASecond()
//        {
//            GetResourceNTimes(500000, 1000);
//        }

//        [Test]
//        public void GetAllResourceStrings()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string) null);

//            IXXX m = (IXXX) manager;
//             Dictionary<string, string> dictionary = m.GetAllStrings(CultureInfo.InvariantCulture);
//            Assert.IsTrue(dictionary.Count>0);
//        }

//        [Test]
//        public void GetAllResourceStringsForGivenFilter()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)"FilterABC");

//            IXXX m = (IXXX) manager;
//             Dictionary<string, string> dictionary = m.GetAllStrings(CultureInfo.InvariantCulture);

//            Assert.IsTrue(dictionary.Count>0);
//        }


//        [Test]
//        public void GetAllResourceStringsForGivenFilterAndCheckKeyAForBeing_fr_FR()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)"FilterABC");

//            IXXX m = (IXXX)manager;
//            Dictionary<string, string> dictionary = m.GetAllStrings(new CultureInfo("fr-FR"));
//            Assert.IsTrue(dictionary.Count > 0);
//            Assert.AreEqual("Some *Filtered* fr-FR value.",dictionary["KeyA"]);
//        }

//        [Test]
//        public void GetAllResourceStringsForGivenFilterAndCheckKeyAForBeing_fr()
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)"FilterABC");

//            IXXX m = (IXXX)manager;
//            Dictionary<string, string> dictionary = m.GetAllStrings(new CultureInfo("fr"));
//            Assert.IsTrue(dictionary.Count > 0);
//            Assert.AreEqual("Some *Filtered* fr value.", dictionary["KeyA"]);
//        }


//        [Test]
//        public void CompareTimes()
//        {

//            TimeSpan elapsed1 = GetResourceNTimes(1000000, 2000, false);

//            TimeSpan elapsed2 = GetResourceNTimes(1000000, 2000,true);


//#pragma warning disable 168
//            TimeSpan dif = elapsed2.Add(-elapsed1);
//#pragma warning restore 168

//            //Results are about 4 times as long. But still...processing 1million resources per second....think I can live with that.

//        }

//        [Test]
//        public void CanGetResourceManagementService()
//        {
//            IResourceManagementService resourceManagementService =
//                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

//            Assert.IsNotNull(resourceManagementService);
//        }

//        [Test]
//        public void AddANewSetting()
//        {

//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);


//            string result = manager.GetString("NonExistentKey");
//            Assert.IsTrue(result.IsNullOrEmpty());

//            IResourceManagementService resourceManagementService =
//                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

//            resourceManagementService.Persist("en",null,"NonExistentKey","New Value!",true);
//            result = manager.GetString("NonExistentKey");
//            Assert.IsTrue(result.IsNullOrEmpty(), "'NonExistentKey' should not exist at this point.");


//            manager =
//                new RepositoryResourceManager((string)null);
//            result = manager.GetString("NonExistentKey");

//            //Finally returns:
//            Assert.IsFalse(result.IsNullOrEmpty(), "'NonExistentKey' should exist at this point.");


//        }



//        [Test]
//        public void AddANewSettingInInvariant()
//        {

//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string)null);


//            string result = manager.GetString("NonExistentKey2");
//            Assert.IsTrue(result.IsNullOrEmpty());

//            IResourceManagementService resourceManagementService =
//                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

//            resourceManagementService.Persist((string)null, null, "NonExistentKey2", "New Value!");
//            result = manager.GetString("NonExistentKey2");
//            Assert.IsTrue(result.IsNullOrEmpty());


//            manager =
//                new RepositoryResourceManager((string)null);
//            result = manager.GetString("NonExistentKey2");
//            //Finally returns:
//            Assert.IsFalse(result.IsNullOrEmpty());


//        }

//        [Test]
//        public void GetAView()
//        {
//            IResourceManagementService resourceManagementService =
//    XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

//            Dictionary<string, Dictionary<string, string>> results = resourceManagementService.GetAllStrings(null, "ViewA","ViewB");
//            Assert.AreEqual(2,results.Keys.Count);

//            Assert.AreEqual(2, results["ViewA"].Values.Count);
//            Assert.AreEqual(2, results["ViewB"].Values.Count);

//        }


































//        private static TimeSpan GetResourceNTimes(int iterations, int milliseconds=1000,bool allowRecursion=true)
//        {
//            RepositoryResourceManager manager =
//                new RepositoryResourceManager((string) null);

//            manager.AllowRecursion = allowRecursion;

//            manager.IgnoreCase = true;
//            string resourceValue = manager.GetString("KeyA", new CultureInfo("en-US"));
//            TimeSpan elapsed;

//            using (TimedScope scope = new TimedScope())
//            {
//                //As no values for the given culture, will fallback to default
//                //culture, as defined in 


//                for (int i = 0; i < iterations; i++)
//                {
//                    resourceValue = manager.GetString("KeyA", new CultureInfo("en-US"));
//                }

//                XAct.DependencyResolver.Current.GetInstance<ITracingService>().Trace(Diagnostics.TraceLevel.Info, "msg");
//                elapsed = scope.Elapsed;

//                Assert.IsTrue(elapsed.TotalMilliseconds <= milliseconds);
//            }

//            Assert.AreEqual("Some en value.", resourceValue);

//            return elapsed;
//        }


//        /*
//         *  resources.Add(new Resource(null,"KeyA", "Some br-FR value.", "br-FR"));
//            resources.Add(new Resource(null,"KeyA", "Some br value.","br"));
//            resources.Add(new Resource(null,"KeyA", "Some fr-FR value.","fr-FR"));
//            resources.Add(new Resource(null,"KeyA", "Some fr value.","fr"));
//            resources.Add(new Resource(null,"KeyA", "Some en-GB value.", "en-EN"));
//            resources.Add(new Resource(null,"KeyA", "Some en value.", "en"));
//            resources.Add(new Resource(null,"KeyA", "Some (invariant) value.", (string)null));

//            _tracingService.Trace(TraceLevel.Verbose, "ResourceDbContextSeeder.BuildResourceEntries.Step ({0})".FormatStringCurrentCulture(2));


//            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered br-FR value.", "br-FR"));
//            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered br value.", "br"));
//            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered fr-FR value.", "fr-FR"));
//            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered fr value.", "fr"));
//            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered en-GB value.", "en-EN"));
//            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered en value.", "en"));
//            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered (invariant) value.", (string)"FilterABC"));

//         */

//    }
//}


