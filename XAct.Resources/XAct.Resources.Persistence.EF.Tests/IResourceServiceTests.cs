namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using NUnit.Framework;
    using XAct.Diagnostics;
    using XAct.Resources;
    using XAct.Resources.Services.Implementations;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IResourceServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out)); Trace.WriteLine("...");
            
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            
                        InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            XAct.Library.Settings.Globalisation.DefaultUICulture = new CultureInfo("en");

            Trace.WriteLine(":::C:" + XAct.Library.Settings.Globalisation.DefaultUICulture);


        }


        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanRetrieveACaseSensitiveValueNoCultureDefined()
        {

            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string resourceValue = resourceManagementService.GetString("KeyA");
            Assert.IsFalse(resourceValue.IsNullOrEmpty());
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanRetrieveACaseInSensitiveValueNoCultureDefined()
        {

            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            
            string resourceValue = resourceManagementService.GetString("kEya");
            Assert.IsFalse(resourceValue.IsNullOrEmpty());
        }
        [Test]
        public void CanRetrieveACaseInSensitiveValueNoCultureDefined2()
        {


            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string resourceValue = resourceManagementService.GetString("kEya");
            Assert.IsFalse(resourceValue.IsNullOrEmpty());
        }


        [Test]
        public void CanRetrieveACaseInSensitiveValueInCulture_fr_FR()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string resourceValue = resourceManagementService.GetString("KeyA",new CultureInfo("fr-FR"));
            Assert.AreEqual("Some fr-FR value.", resourceValue);
        }
        [Test]
        public void CanRetrieveACaseInSensitiveValueInCulture_fr()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("fr"));
            Assert.AreEqual("Some fr value.", resourceValue);
        }

        [Test]
        public void CanRetrieveACaseInSensitiveValueInCulture_br_FR()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("br-FR"));
            Assert.AreEqual("Some br-FR value.", resourceValue);
        }


                [Test]
        public void CanRetrieveACaseInSensitiveValueInCulture_br()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("br"));
            Assert.AreEqual("Some br value.", resourceValue);
        }

        [Test]
        public void ThrowsExceptionIfUsingNonSensicalCultureCode()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            try
            {
#pragma warning disable 168
                string resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("br-ZA"));
#pragma warning restore 168
            }
#pragma warning disable 168
            catch(System.Globalization.CultureNotFoundException)
#pragma warning restore 168
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.IsFalse(true);
        }

        [Test]
        public void CanRetrieveACaseInSensitiveValueInCulture_ne_BE()
        {



            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();


            string resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("nl-BE"));



            if (XAct.Library.Settings.Globalisation.DefaultUICulture.ToString() == "fr-FR")
            {
                //But when run in test rig, appears tests are run alpahbetically
                //so the culture was updated to french due to 
                //CanNotChangeDefaultCultureAfterStartup which is run before
                //CanRetrieveACaseInSensitiveValueInCulture_ne_BE

                Assert.AreEqual("Some fr-FR value.", resourceValue);
            }
            else
            {
                Assert.AreEqual("Some (invariant) value.", resourceValue);
            }

        }

        //=====================================


        [Test]
        public void DoNotThrowExceptionIfResourceNotFound()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string result = resourceManagementService.GetString("NonExistentKeyXYZ123");

            Assert.IsTrue(result.IsNullOrEmpty());

        }



        [Test]
        public void RetrievesARecursiveEntry()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string result = resourceManagementService.GetString("RecursiveA1");

            Assert.IsFalse(result.IsNullOrEmpty());

        }

        [Test]
        public void RetrievesAndParsesCorrectlyARecursiveEntry()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string result = resourceManagementService.GetString("RecursiveA1");

            Assert.IsFalse(result.IsNullOrEmpty());

            Assert.AreEqual("Something that includes a second reference.", result);
        }

        [Test]
        public void RetrievesAndParsesCorrectlyAMultipleRecursiveEntry()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            string result = resourceManagementService.GetString("RecursiveB1");

            Assert.IsFalse(result.IsNullOrEmpty());

            Assert.AreEqual("Something that includes another sentence that includes yet another sentence.", result);
        }


        //[Test]
        //public void ReleaseAllResourcesDoesNotCauseReloadOfResources()
        //{
        //    RepositoryResourceManager manager =
        //        new RepositoryResourceManager((string)null);

        //    string result = manager.GetString("KeyA");
        //    Assert.IsFalse(result.IsNullOrEmpty());

        //    //TODO: Should not throw an exception...but can't fix right now.
        //    manager.ReleaseAllResources();

        //    result = manager.GetString("KeyA");

        //    Assert.IsTrue(result.IsNullOrEmpty());

        //}

//        [Test]
//        public void ResetDbBackRepositories()
//        {
//#pragma warning disable 168
//            RepositoryResourceManager manager =
//#pragma warning restore 168
//                new RepositoryResourceManager((string) null);

            
//        }
        [Test]
        public void CanRetrieve50ResourcesInLessThanASecond()
        {
            GetResourceNTimes_enUS(50, 1000);
        }

        [Test]
        public void CanRetrieve5000ResourcesInLessThanASecond()
        {
            GetResourceNTimes_enUS(5000, 1000);
        }

        [Test]
        public void CanRetrieve10000ResourcesInLessThanASecond()
        {
            GetResourceNTimes_enUS(10000, 1000);
        }

        [Test]
        public void CanRetrieve30000ResourcesInLessThanASecond()
        {
            GetResourceNTimes_enUS(30000, 1000);
        }
        [Test]
        public void CanRetrieve50000ResourcesInLessThanASecond()
        {
            GetResourceNTimes_enUS(50000, 1000);
        }
        [Test]
        public void CanRetrieve100000ResourcesInLessThanASecond()
        {
            GetResourceNTimes_enUS(100000, 1000);
        }
        [Test]
        public void CanRetrieve500000ResourcesInLessThanASecond()
        {
            GetResourceNTimes_enUS(250000, 1000);
        }

        ////HOLD
        //[Test]
        //public void GetAllResourceStrings()
        //{
        //    IResourceManagementService resourceManagementService =
        //        XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();


        //     Dictionary<string, string> dictionary = resourceManagementService.GetAllStrings(CultureInfo.InvariantCulture);
        //    Assert.IsTrue(dictionary.Count>0);
        //}

        [Test]
        public void GetAllResourceStringsForGivenFilter()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            var results = resourceManagementService.GetAllStrings(CultureInfo.InvariantCulture, "FilterABC");
            //RepositoryResourceManager manager =
            //    new RepositoryResourceManager((string)"FilterABC");

            //IXXX m = (IXXX) manager;
             //Dictionary<string, string> dictionary = m.GetAllStrings(CultureInfo.InvariantCulture);



            Assert.IsTrue(results["FilterABC"].Count > 0);
        }


        ////HOLD
        //[Test]
        //public void GetAllResourceStringsForGivenFilterAndCheckKeyAForBeing_fr_FR()
        //{
        //    IResourceManagementService resourceManagementService =
        //        XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

        //    Dictionary<string, string> dictionary = resourceManagementService.GetAllStrings(new CultureInfo("fr-FR"));
        //    Assert.IsTrue(dictionary.Count > 0);
        //    Assert.AreEqual("Some *Filtered* fr-FR value.",dictionary["KeyA"]);
        //}

        ////HOLD
        //[Test]
        //public void GetAllResourceStringsForGivenFilterAndCheckKeyAForBeing_fr()
        //{
        //    IResourceManagementService resourceManagementService =
        //        XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();


        //    Dictionary<string, string> dictionary = resourceManagementService.GetAllStrings(new CultureInfo("fr"));
        //    Assert.IsTrue(dictionary.Count > 0);
        //    Assert.AreEqual("Some *Filtered* fr value.", dictionary["KeyA"]);
        //}


        [Test]
        public void CompareTimes()
        {

            TimeSpan elapsed1 = GetResourceNTimes_enUS(1000000, 2000, false);

            TimeSpan elapsed2 = GetResourceNTimes_enUS(1000000, 2000,true);


#pragma warning disable 168
            TimeSpan dif = elapsed2.Add(-elapsed1);
#pragma warning restore 168

            //Results are about 4 times as long. But still...processing 1million resources per second....think I can live with that.
            Debug.Assert(true);
        }

        [Test]
        public void CanGetResourceManagementService()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            Assert.IsNotNull(resourceManagementService);
        }

        [Test]
        public void CanGetResourceManagementServiceOfExpectedType()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            Assert.AreEqual(typeof(MyResourceManagementService),  resourceManagementService.GetType());
        }

        [Test]
        public void AddANewSetting()
        {

            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();


            string result = resourceManagementService.GetString("NonExistentKey");
            Assert.IsTrue(result.IsNullOrEmpty());

            resourceManagementService.Persist( "en",null,"NonExistentKey","New Value!");
            result = resourceManagementService.GetString("NonExistentKey");


            //Finally returns:
            Assert.IsFalse(result.IsNullOrEmpty());


        }



        [Test]
        public void AddANewSettingInInvariant()
        {

            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();


            string result = resourceManagementService.GetString("NonExistentKey2");
            Assert.IsTrue(result.IsNullOrEmpty());

            resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            resourceManagementService.Persist((string)null, null, "NonExistentKey2", "New Value!");

            result = resourceManagementService.GetString("NonExistentKey2");
            //Finally returns:
            Assert.IsFalse(result.IsNullOrEmpty());


        }

        [Test]
        public void GetAView()
        {
            IResourceManagementService resourceManagementService =
    XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();

            Dictionary<string, Dictionary<string, string>> results = resourceManagementService.GetAllStrings(null, "ViewA","ViewB");
            Assert.AreEqual(2,results.Keys.Count);

            Assert.AreEqual(2, results["ViewA"].Values.Count);
            Assert.AreEqual(2, results["ViewB"].Values.Count);

        }



        [Test]
        public void CanNotChangeDefaultCultureAfterStartup()
        {
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();


            string resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("nl-BE"));

            //As no values for the given culture, will fallback to default
            //culture, as defined in
            //CultureInfo hold = XAct.Library.Settings.Globalisation.DefaultUICulture;

            //XAct.Library.Settings.Globalisation.DefaultUICulture = new CultureInfo("fr-FR");

            //resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("nl-BE"));


            //Let's change it back to original, to not mess up any later tests:
            //XAct.Library.Settings.Globalisation.DefaultUICulture = hold;

            //Although 
            Assert.AreEqual("Some (invariant) value.", resourceValue);


            //HOLD:
            Assert.IsTrue(true);
        }

































        private static TimeSpan GetResourceNTimes_enUS(int iterations, int milliseconds=1000,bool allowRecursion=true)
        { 
            IResourceManagementService resourceManagementService =
                XAct.DependencyResolver.Current.GetInstance<IResourceManagementService>();


            string resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("en-US"));
            TimeSpan elapsed;

            using (TimedScope scope = new TimedScope())
            {
                //As no values for the given culture, will fallback to default
                //culture, as defined in 

                for (int i = 0; i < iterations; i++)
                {
                    resourceValue = resourceManagementService.GetString("KeyA", new CultureInfo("en-US"));
                }

                XAct.DependencyResolver.Current.GetInstance<ITracingService>().Trace(Diagnostics.TraceLevel.Info, "msg");

                elapsed = scope.Elapsed;

                Assert.IsTrue(elapsed.TotalMilliseconds <= milliseconds);
            }

            Assert.AreEqual("Some en value.", resourceValue);

            /*
            resources.Add(new Resource(null,"KeyA", "Some br-FR value.", "br-FR"));
            resources.Add(new Resource(null,"KeyA", "Some br value.","br"));
            resources.Add(new Resource(null,"KeyA", "Some fr-FR value.","fr-FR"));
            resources.Add(new Resource(null,"KeyA", "Some fr value.","fr"));
            resources.Add(new Resource(null,"KeyA", "Some en-GB value.", "en-EN"));
            resources.Add(new Resource(null,"KeyA", "Some en value.", "en"));
            resources.Add(new Resource(null,"KeyA", "Some (invariant) value.", (string)null));

            _tracingService.Trace(TraceLevel.Verbose, "ResourceDbContextSeeder.BuildResourceEntries.Step ({0})".FormatStringCurrentCulture(2));

            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered br-FR value.", "br-FR"));
            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered br value.", "br"));
            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered fr-FR value.", "fr-FR"));
            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered fr value.", "fr"));
            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered en-GB value.", "en-EN"));
            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered en value.", "en"));
            resources.Add(new Resource("FilterABC", "KeyA", "Some Filtered (invariant) value.", (string)"FilterABC"));

         */



            return elapsed;
        }


    }
}


