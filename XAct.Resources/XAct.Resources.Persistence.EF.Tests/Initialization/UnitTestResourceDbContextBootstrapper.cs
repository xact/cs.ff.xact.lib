using System.Data.Entity;
using System.Linq;
using XAct.Bootstrapper.Tests;
using XAct.Services;

namespace XAct.Resources.Tests
{
    /// <summary>
    /// An implementation of the <see cref="IResourceEFDatabaseBootstrapper"/>
    /// to ensure the creation of a database as required for the resources.
    /// </summary>
    public class UnitTestResourceDbContextBootstrapper : IResourceEFDatabaseBootstrapper, IHasMediumBindingPriority
    {

        private readonly IUnitTestDatabaseInitializer _unitTestDatabaseInitializer;

        public UnitTestResourceDbContextBootstrapper(IUnitTestDatabaseInitializer unitTestDatabaseInitializer)
        {
            _unitTestDatabaseInitializer = unitTestDatabaseInitializer;
        }

        /// <summary>
        /// Ensures the Resources table Exists in the current db.
        /// </summary>
        public void Initialize()
        {


            //Set the Initialzer that will define the Db Model:
            Database.SetInitializer(_unitTestDatabaseInitializer);

            UnitTestResourceDbContext dbContext = new UnitTestResourceDbContext();

            DbSet<Resource> resources = dbContext.Set<Resource>();

            //Force a query, which in turn forces creation of Db:
            resources.FirstOrDefault();

            
            ////Get default settings (tablename, etc.
            //IResourceRepositoryDbSettings repositorySettings = 
            //    ServiceLocatorService.Current.GetInstance<IResourceRepositoryDbSettings>();

            //IResourceEFDatabaseContextSeeder resourceDbContextSeeder =
            //    ServiceLocatorService.Current.GetInstance<IResourceEFDatabaseContextSeeder>();
                
            //Database.SetInitializer(new UnitTestResourceDbContextInitializer(repositorySettings, resourceDbContextSeeder));


        }
    }


    //Invoke *early*, eg from a Boottrapper:
    //Database.SetInitializer<GraphContext>(new GraphInitializer());
}