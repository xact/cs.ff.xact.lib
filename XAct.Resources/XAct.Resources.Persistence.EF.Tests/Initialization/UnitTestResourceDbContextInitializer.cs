﻿using System.Data.Entity;
using System.Text;

namespace XAct.Resources.Tests
{
    /// <summary>
    /// The resourceDbInitializer invoked from the <see cref="UnitTestResourceDbContext"/>.
    /// <para>
    /// Used by <see cref="UnitTestResourceDbContextBootstrapper"/>
    /// to ensure the existince of a Db as required for Resources.
    /// </para>
    /// </summary>
    public class UnitTestResourceDbContextInitializer : DropCreateDatabaseIfModelChanges<UnitTestResourceDbContext>
    {
        private readonly IResourceRepositoryDbSettings _repositorySettings;
        private readonly IResourceEFDatabaseContextSeeder _dbContextSeeder;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTestResourceDbContextInitializer"/> class.
        /// </summary>
        /// <param name="repositorySettings">The repository settings.</param>
        /// <param name="dbContextSeeder">The db context seeder.</param>
        public UnitTestResourceDbContextInitializer(IResourceRepositoryDbSettings repositorySettings, IResourceEFDatabaseContextSeeder dbContextSeeder)
        {
            //Repository settings were used for when not being built using CodeFirst.
            //Keeping it just in case
            _repositorySettings = repositorySettings;

            _dbContextSeeder = dbContextSeeder;
        }

        /// <summary>
        /// Seeds the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        protected override void Seed(UnitTestResourceDbContext context)
        {
            _dbContextSeeder.Seed(context);        //etc    

            base.Seed(context);
        }


    }
}