﻿using System.Data.Entity;
using System.Diagnostics;
using XAct.Diagnostics;
using XAct.Services;
using XAct.Settings;

namespace XAct.Resources.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class UnitTestResourceDbContext : DbContext
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Gets or sets the Resource objects.
        /// </summary>
        /// <value>
        /// The resources.
        /// </value>
        public DbSet<Resource> Resources { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitTestResourceDbContext"/> class.
        /// </summary>
        public UnitTestResourceDbContext()
            : base(
                ServiceLocatorService.Current.GetInstance<IHostSettingsService>().Get<string>(
                    "XActLibConnectionStringName", XAct.Library.Settings.Db.DefaultXActLibConnectionStringSettingsName))
        {
            _typeName = this.GetType().Name;
            _tracingService = this.ServiceLocate<ITracingService>();
        }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            //Can't be injected easily, so use ServiceLocator:
            IResourceDbModelBuilder resourceDbModelBuilder =
                ServiceLocatorService.Current.GetInstance<IResourceDbModelBuilder>();

            resourceDbModelBuilder.OnModelCreating(modelBuilder);


            base.OnModelCreating(modelBuilder);
        }
    }
}