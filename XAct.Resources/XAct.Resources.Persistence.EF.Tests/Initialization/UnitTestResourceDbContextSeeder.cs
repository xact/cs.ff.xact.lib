namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Resources;
    using XAct.Resources.Initialization.DbContextSeeders;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class ResourceDbContextSeeder : XActLibDbContextSeederBase<Resource>, IResourceDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public ResourceDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _environmentService = environmentService;
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        /// <param name="dbContext">The context.</param>
        public override void SeedInternal(DbContext dbContext) 
        {
            SeedInternalHelper(dbContext, true, x => new { x.Filter, x.Key, x.CultureCode });
        }
 
        public override void CreateEntities()
        {

            this.InternalEntities = new List<Resource>();




            this.InternalEntities.Add(new Resource(Guid.NewGuid(), null, "KeyA", "Some br-FR value.", "br-FR"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), null, "KeyA", "Some br value.", "br"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), null, "KeyA", "Some fr-FR value.", "fr-FR"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), null, "KeyA", "Some fr value.", "fr"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), null, "KeyA", "Some en-GB value.", "en-EN"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), null, "KeyA", "Some en value.", "en"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), null, "KeyA", "Some (invariant) value.", (string)null));

            this.InternalEntities.Add(new Resource(Guid.NewGuid(), "FilterABC", "KeyA", "Some *Filtered* br-FR value.", "br-FR"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), "FilterABC", "KeyA", "Some *Filtered* br value.", "br"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), "FilterABC", "KeyA", "Some *Filtered* fr-FR value.", "fr-FR"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), "FilterABC", "KeyA", "Some *Filtered* fr value.", "fr"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), "FilterABC", "KeyA", "Some *Filtered* en-GB value.", "en-EN"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), "FilterABC", "KeyA", "Some *Filtered* en value.", "en"));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(), "FilterABC", "KeyA", "Some *Filtered* (invariant) value.", ""));



            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"/Classic/Page.aspx", "classicKey.Text", "Some (invariant) classic value.", (string)null));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"Classic/Page.aspx", "classicKey.Text", "Some (invariant) classic value(B).", (string)null));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"/Classic/Page.aspx", "classicKey", "Some (invariant) classic value@1.", (string)null));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"Classic/Page.aspx", "classicKey", "Some (invariant) classic value@1(B).", (string)null));

            this.InternalEntities.Add(new Resource(Guid.NewGuid(),null, "RecursiveA1", "Something that includes {RecursiveA2}", (string)null));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),null, "RecursiveA2", "a second reference.", (string)null));

            this.InternalEntities.Add(new Resource(Guid.NewGuid(),null, "RecursiveB1", "Something that includes {RecursiveB2}", (string)null));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),null, "RecursiveB2", "another sentence that includes {RecursiveB3}", (string)null));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),null, "RecursiveB3", "yet another sentence.", (string)null));
            
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),null, "RecursiveC1", "Something that includes {RecursiveC2}", (string)null));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),null, "RecursiveC2", "another sentence that includes {RecursiveC3}", (string)null));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),null, "RecursiveC3", "yet another sentence that unfortunately includes {RecursiveC1}.", (string)null));
            
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"ViewA", "FirstName", "First Name", ""));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"ViewA", "LastName", "Last Name", ""));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"ViewB", "SSN", "SSS #", ""));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"ViewB", "TaxId", "Tax Id", ""));
            this.InternalEntities.Add(new Resource(Guid.NewGuid(),"ViewC", "SomeKey", "Whatever", ""));

        }


    }


}