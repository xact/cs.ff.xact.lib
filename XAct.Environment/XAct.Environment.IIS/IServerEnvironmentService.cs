namespace XAct.Environment
{
    /// <summary>
    /// Contract for maintaining Servers.
    /// <para>
    /// 
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// The service contains rather dangerous methods
    /// (eg: <see cref="RestartServerAppPool"/>) so should
    /// isolated to ensure it's not misused (if it were
    /// a method in <see cref="IEnvironmentService"/> it would 
    /// much more hard to track down the instances of this
    /// service).
    /// </para>
    /// </remarks>
    public interface IServerEnvironmentService
    {


        /// <summary>
        /// Restarts the server app pool.
        /// <para>
        /// WARNING: It goes without saying that this method needs
        /// to be available on a page Authorized by only Server admin
        /// Roles.
        /// </para>
        /// </summary>
        void RestartServerAppPool();

    }
}