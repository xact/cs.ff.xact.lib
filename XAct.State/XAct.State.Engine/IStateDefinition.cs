namespace XAct.State
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IStateDefinition<T>
    {

    }

    /// <summary>
    /// 
    /// </summary>
    public interface IStateDefinition : IStateDefinition<string>
    {
        
    }
}