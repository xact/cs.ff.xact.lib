namespace XAct.State
{

    /// <summary>
    /// Contract for the definition of a transfer between
    /// <see cref="IStateDefinition"/>s.
    /// </summary>
    public interface ITransitionDefinition<TTrigger>  
        where TTrigger : struct
    {
        /// <summary>
        /// Gets or sets the trigger value.
        /// </summary>
        TTrigger Trigger { get; set; }

        /// <summary>
        /// Gets or sets the name of the state to move to from the current state.
        /// </summary>
        string TargetStateName { get; set; } 
    }
}