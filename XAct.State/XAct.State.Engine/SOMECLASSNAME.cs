//using System.Collections.Generic;
//using System.Collections.ObjectModel;

//namespace XAct.State
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="TTrigger">The type of the trigger.</typeparam>
//    public class StateSequenceDefinition<TTrigger> : RegionDefinition<TTrigger>
//        where TTrigger : struct
//    {
        
//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="TTrigger">The type of the trigger.</typeparam>
//    public class RegionDefinition<TTrigger> : IHasName
//        where TTrigger : struct
//    {
//        public string Name { get; set; }

//        /// <summary>
//        /// Gets the collection of States that belong to this Region.
//        /// </summary>
//        public ICollection<StateDefinition<TTrigger>> States { get { return _states??(_states = new Collection<StateDefinition<TTrigger>>());} }
//        private ICollection<StateDefinition<TTrigger>> _states;
//    }


//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="TTrigger">The type of the trigger.</typeparam>
//    public class StateDefinition<TTrigger> : IHasName
//        where TTrigger : struct
//    {
//        /// <summary>
//        /// Gets the name used to identify this state.
//        /// </summary>
//        public string Name { get; set; }

//        /// <summary>
//        /// Gets the collection of outgoing transitions.
//        /// </summary>
//        public ICollection<TransitionDefinition<TTrigger>> Transitions
//        {
//            get { return _transitions??(_transitions = new Collection<TransitionDefinition<TTrigger>>());}
//        }
//        ICollection<TransitionDefinition<TTrigger>> _transitions;
//    }

//    /// <summary>
//    /// The definition of the transfer between states.
//    /// </summary>
//    public class TransitionDefinition<TTrigger> : ITransitionDefinition<TTrigger>
//        where TTrigger : struct
//    {
//        public TTrigger Trigger { get; set; }

//        public string TargetStateName { get; set; }
//    }
//}