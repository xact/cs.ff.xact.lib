﻿namespace KW_NAMESPACENAME.Services.Implementations
{
    using XAct;
    using XAct.Services;

#pragma warning disable 1591
    public class SomeService :ISomeService
#pragma warning restore 1591
    {
#pragma warning disable 1591
        public SomeServiceEntity Foo() { return null; }
#pragma warning restore 1591

    }
}
