﻿
namespace KW_NAMESPACENAME
{
    using System;
    using System.Runtime.Serialization;
    using XAct;

    [DataContract]
#pragma warning disable 1591
    public class SomeServiceEntity : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
#pragma warning restore 1591
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }
    }
}
