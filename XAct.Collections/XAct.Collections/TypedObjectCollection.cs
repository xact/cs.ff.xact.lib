﻿namespace XAct.Collections {
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;

    /// <summary>
  /// A Collection that allows for verification of the item <c>Type</c>
  /// before it is added to collection.
  /// <para>
  /// This is a slight improvement to the <c>ObjectCollection</c>,
  /// but the argument checking method employed is not 
  /// to be confused with a truly Typed collection.
  /// </para>
  /// </summary>
  /// <remarks>
  /// <para>
  /// An example of its usage could be as follows:
  /// <code>
  /// <![CDATA[
  /// <UserControl x:Class="SilverlightApplication10.UserControl4"
  ///   xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 
  ///   xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" 
  ///   xmlns:sys="clr-namespace:System;assembly=mscorlib"
  ///   xmlns:collections="clr-namespace:System.Collections;assembly=XAct.Collections.Core"
  ///   Width="200" Height="250">
  ///     
  ///     <UserControl.Resources>
  ///         <collections:TypedObjectCollection 
  ///           x:Key="hardcodedValues" 
  ///           ItemType="System.Int32">
  ///             <sys:String>1</sys:String>
  ///             <sys:String>2</sys:String>
  /// 
  ///             <sys:Int32>4</sys:Int32>
  ///             <sys:Int32>5</sys:Int32>
  /// 
  ///             <!-- Would cause exception:
  ///                  <sys:String>Boing...</sys:String>
  ///             -->
  ///         </collections:TypedObjectCollection>
  ///     </UserControl.Resources>
  ///     
  ///     <Grid x:Name="LayoutRoot" Background="White">
  ///         <!-- Binding to list as src for Items -->
  ///         <ListBox  ItemsSource="{StaticResource hardcodedValues}"/>
  ///     </Grid>
  /// </UserControl>
  /// ]]>
  /// </code>
  /// </para>
  /// <para>
  /// See also <see cref="ObjectCollection"/>.
  /// </para>
  /// </remarks>
  public class TypedObjectCollection : Collection<object> {

    #region Properties
    /// <summary>
    /// Gets or sets the <see cref="Type"/> 
    /// of the item.
    /// </summary>
    /// <value>The type of the item.</value>
    public object ItemType {
      get { return _ItemType; }
      set {
        if (value == null) { return; }
        if (value is Type) {
          ItemType = value;
          return;
        }
        if (value is string) {
          string typeName = (string)value;
          if (typeName == string.Empty) { return; }

          //This is a red herring and would lead to ambiguity:
          //String would work (System.String)
          //Int would not (System.Int),
          //etc...
          //if (typeName.IndexOf('.')==-1){
          //  typeName = "System." + typeName;
          //}

          _ItemType = Type.GetType(typeName);
        }
      }
    }
    private Type _ItemType;
    #endregion


    #region Constructors
    /// <summary>
    /// Initializes a new instance of 
    /// the <see cref="TypedObjectCollection"/> class.
    /// </summary>
    public TypedObjectCollection() {
    }

    /// <summary>
    /// Initializes a new instance of the
    /// <see cref="TypedObjectCollection"/> class.
    /// </summary>
    /// <param name="collection">The collection.</param>
    public TypedObjectCollection(IEnumerable collection) {

      foreach (object obj in collection) {
        Add(obj);
      }
    }
    #endregion


    #region Overrides
    /// <summary>
    /// Inserts an element into the 
    /// <see cref="T:System.Collections.ObjectModel.Collection`1"/> 
    /// at the specified index.
    /// </summary>
    /// <param name="index">
    /// The zero-based index at which <paramref name="item"/> 
    /// should be inserted.</param>
    /// <param name="item">
    /// The object to insert. 
    /// The value can be null for reference types.</param>
    /// <exception cref="T:System.ArgumentOutOfRangeException">
    /// 	<paramref name="index"/> is less than zero.
    /// -or-
    /// <paramref name="index"/> is greater 
    /// than <see cref="P:System.Collections.ObjectModel.Collection`1.Count"/>.
    /// </exception>
    protected override void InsertItem(int index, object item) {
      if (_ItemType != null) {
        base.InsertItem(
          index,
          Convert.ChangeType(item, _ItemType, null)
          );
        return;
      }

      base.InsertItem(index, item);
    }
    #endregion

  }

}
