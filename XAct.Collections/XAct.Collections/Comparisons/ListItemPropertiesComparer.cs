﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XAct;

namespace XAct.Collections.Comparisons
{

    /// <summary>
    /// Generic Sort Class that accepts one or more properties names of the list items, and sorts by them.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This class makes it a breeze to compare two objects that have the same property names, and
    /// sort them by them. 
    /// </para>
    /// <para>
    /// Important!: Comparison is by String comparison.
    /// </para>
    /// </remarks>
    public class ListItemPropertiesComparer<T> : Comparer<object>
    {

        List<string> _propertyNames;

        #region Constructors.
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="primaryPropertyName">Name of a Property of the objects to Compare</param>
        /// <param name="additionalPropertyNames">Optional Additional Property Names to compare by.</param>
        /// <remarks>
        /// <para>
        /// This method makes it a breeze to compare two objects that have the same property names, and
        /// sort them by them. 
        /// </para>
        /// <para>
        /// Important!: Comparison is by String.
        /// </para>
        /// </remarks>
        public ListItemPropertiesComparer(string primaryPropertyName, params string[] additionalPropertyNames)
        {
            //I wanted to ensure that atleast one property is defined, hence the strange
            //way of defining the arguments.
            //But because of this strange method, have to combine the two parts into one for usage by CompareTo:
            _propertyNames = new List<string>();

            //Add the first arg to temp array:
            _propertyNames.Add(primaryPropertyName);

            //Add the others one by one:
            if (additionalPropertyNames == null)
            {
                return;
            }
            foreach (string tP in additionalPropertyNames)
            {
                _propertyNames.Add(tP);
            }
        }

        #endregion

        #region Public Methods (Compare)
        /// <summary>
        /// Compare two objects.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Important!: Comparison is STRING comparison.
        /// </para>
        /// </remarks>
        /// <param name="x">First object</param>
        /// <param name="y">Second object</param>
        /// <returns>-1 if First object is 'less than' second, 0 if same, 1 if Second is 'larger'.</returns>
        public override int Compare(object x, object y)
        {
            int tResult = 0;
            
            Type type = typeof(T);

            foreach (string propertyName in _propertyNames)
            {


                //Generic by reflection:
                //TODO: Could be made faster by caching.
                IComparable o1 = x.GetValue(propertyName) as IComparable; 
                object o2 = y.GetValue(propertyName) as IComparable;

                if (o1 == null)
                {
                    if (o2 == null)
                    {
                        //both null, continue to next val:
                        continue;
                    }
                    //If first is null, second is larger:
                    return -1;
                }

                tResult = o1.CompareTo(o2);
                if (tResult != 0)
                {
                    break;
                }
            }
            return tResult;
        }
        #endregion




    }//Class:End
}
