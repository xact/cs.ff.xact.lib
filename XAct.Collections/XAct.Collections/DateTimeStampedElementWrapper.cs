﻿namespace XAct.Collections
{
    using System;

    /// <summary>
    /// An implementation of the 
    /// <see cref="IDateTimeStampedElementWrapper{TItem}"/>
    /// to wrap elements in order to give them a last touched value.
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class DateTimeStampedElementWrapper<TItem> : IDateTimeStampedElementWrapper<TItem>
    {

        #region Properties

        /// <summary>
        /// Gets the time stamp.
        /// </summary>
        /// <value>The stamp.</value>
        public DateTime Touched
        {
            get
            {
                return _Touched;
            }
        }
        private DateTime _Touched;

        /// <summary>
        /// Gets the wrapped element.
        /// </summary>
        /// <value>The vertex.</value>
        public TItem Item
        {
            get
            {
                return _Item;
            }
        }
        private readonly TItem _Item;

        #endregion


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeStampedElementWrapper{TItem}"/> class.
        /// with Stamp initialize with DateTime.Now
        /// </summary>
        /// <param name="item">The wrapped item.</param>
        public DateTimeStampedElementWrapper(TItem item)
        {
            if (item.IsDefaultOrNotInitialized()) { throw new ArgumentNullException("item"); }

            _Item = item;
            _Touched = DateTime.Now;
        }
        #endregion


        #region Methods
        /// <summary>
        /// Updates the time stamp to DateTime.Now.
        /// </summary>
        public void Touch()
        {
            _Touched = DateTime.Now;
        }
        #endregion
    }
}
