﻿namespace XAct.Collections {
    using System.Collections;
    using System.Collections.ObjectModel;


    /// <summary>
  /// A Generic collection that has been typed to an <see cref="object"/>.
  /// <para>
  /// Note that (unlike <see cref="TypedObjectCollection"/>) 
  /// no Type checking is performed.
  /// </para>
  /// </summary>
  /// <remarks>
  /// <para>
  /// The need for this type of control is because Silverlight 2.0
  /// does not have a mechanism for working with generics from XAML.
  /// </para>
  /// <para>
  /// An example of its usage could be something like the following:
  /// <code>
  /// <![CDATA[
  /// <UserControl x:Class="SilverlightApplication10.UserControl4"
  ///   xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 
  ///   xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" 
  ///   xmlns:sys="clr-namespace:System;assembly=mscorlib"
  ///   xmlns:collections="clr-namespace:System.Collections;assembly=XAct.Collections.Core"
  ///   Width="200" Height="250">
  ///     
  ///     <UserControl.Resources>
  ///         <collections:ObjectCollection x:Key="hardcodedValues">
  ///             <sys:String>1</sys:String>
  ///             <sys:String>2</sys:String>
  ///         </collections:TypedObjectCollection>
  ///     </UserControl.Resources>
  ///     
  ///     <Grid x:Name="LayoutRoot" Background="White">
  ///         <!-- Binding to list as src for Items -->
  ///         <ListBox  ItemsSource="{StaticResource hardcodedValues}"/>
  ///     </Grid>
  /// </UserControl>
  /// ]]>
  /// </code>
  /// </para>
  /// <para>
  /// See also <see cref="TypedObjectCollection"/>.
  /// </para>
  /// </remarks>
  public class ObjectCollection : Collection<object> {

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the 
    /// <see cref="ObjectCollection"/> class.
    /// </summary>
    public ObjectCollection() {
    }

    /// <summary>
    /// Initializes a new instance of the 
    /// <see cref="ObjectCollection"/> class.
    /// </summary>
    /// <param name="collection">The collection.</param>
    public ObjectCollection(IEnumerable collection) {
      foreach (object obj in collection) {
        Add(obj);
      }
    }
    #endregion

  }
}
