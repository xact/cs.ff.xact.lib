/*
using System;
using System.Collections.Generic;
using System.Text;

namespace XAct.Generics {
  
  /// <summary>
  /// represent an hash set
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class HashSet<T> : ICollection<T> {

    
#region Fields


    protected Dictionary<T, object> dic = null;

    protected object PlaceHolder {
      get { return _PlaceHolder; }
    }
    private static readonly object _PlaceHolder = new object();

#endregion

    
#region Properties

    /// <summary>
    /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
    /// </summary>
    /// <value></value>
    /// <returns>The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</returns>
    public int Count {
      get { return dic.Count; }
    }

    /// <summary>
    /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.
    /// </summary>
    /// <value></value>
    /// <returns><c>true</c>if the <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only; otherwise, false.</returns>
    public bool IsReadOnly {
      get { return false; }
    }

#endregion


#region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="HashSet&lt;T&gt;"/> class.
    /// </summary>
    public HashSet() {
      dic = new Dictionary<T, object>();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="HashSet&lt;T&gt;"/> class.
    /// </summary>
    /// <param name="comparer">The comparer.</param>
    public HashSet(IEqualityComparer<T> comparer) {
      dic = new Dictionary<T, object>(comparer);
    }

#endregion

    
#region ICollection public methods

    /// <summary>
    /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
    /// </summary>
    /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
    /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.</exception>
    public void Add(T item) {
      dic[item] = PlaceHolder;
    }

    /// <summary>
    /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
    /// </summary>
    /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only. </exception>
    public void Clear() {
      dic.Clear();
    }

    /// <summary>
    /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> contains a specific value.
    /// </summary>
    /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
    /// <returns>
    /// true if item is found in the <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false.
    /// </returns>
    public bool Contains(T item) {
      return dic.ContainsKey(item);
    }

    /// <summary>
    /// Copies to.
    /// </summary>
    /// <param name="array">The array.</param>
    /// <param name="index">The index.</param>
    public void CopyTo(T[] array, int index) {
      dic.Keys.CopyTo(array, index);
    }

    /// <summary>
    /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.
    /// </summary>
    /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
    /// <returns>
    /// true if item was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false. This method also returns false if item is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"></see>.
    /// </returns>
    /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.</exception>
    public bool Remove(T item) {
      return dic.Remove(item);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
    /// </returns>
    public IEnumerator<T> GetEnumerator() {
      return dic.Keys.GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that iterates through a collection.
    /// </summary>
    /// <returns>
    /// An <see cref="T:System.Collections.IEnumerator"></see> object that can be used to iterate through the collection.
    /// </returns>
     System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
      return dic.Keys.GetEnumerator();
    }

#endregion

  }//Class:End
}//Namespace:End
 */