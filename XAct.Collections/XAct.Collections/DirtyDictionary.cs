namespace XAct.Collections {
    using System;
    using System.Collections;
    using System.Collections.Generic;


    /// <summary>
  /// Dictionary of values.
  /// </summary>
  /// <typeparam name="TKey">Type of Key.</typeparam>
  /// <typeparam name="TValue">Type of Value.</typeparam>
    public class DirtyDictionary<TKey, TValue> : IDictionary<TKey, TValue> { //, INotifyPropertyChanging, INotifyPropertyChanged
       

    #region Events Raised
        /// <summary>
        /// Event raised when the collection becomes Dirty.
        /// </summary>
        public event EventHandler Changed;

    ///// <summary>
    ///// Occurs when a property value is changing.
    ///// </summary>
    //  public event PropertyChangingEventHandler PropertyChanging;

    //  /// <summary>
    //  /// Occurs when a property value changes.
    //  /// </summary>
    //public event PropertyChangedEventHandler PropertyChanged;
    #endregion

    #region Fields
    private bool _IsDirtyScanned = true;
    /// <summary>
    /// Inner dictionary of DirtyDictionaryItem elements.
    /// </summary>
    protected Dictionary<TKey, DirtyDictionaryItem<TKey, TValue>> _InnerDictionary = new Dictionary<TKey, DirtyDictionaryItem<TKey, TValue>>();
    #endregion

    #region Properties - Custom
    /// <summary>
    /// Gets whether any value in the dictionary has been updated.
    /// Use <see cref="ResetDirty"/> to reset the flag to <c>false</c>. 
    /// </summary>
    public bool IsDirty {
      get {
          CheckDirty();
        return _IsDirty;
      }
      set {
        if (value && (_IsDirty == false)) {
          _IsDirty = true;
          //Raise event:
          OnChanged(EventArgs.Empty);
        }
      }
    }
    //Reset by ResetDirty, Clear
    //and, indirectly, by Remove.
    //Updated by this[], Add
    private bool _IsDirty;

    /// <summary>
    /// Determines whether a specific item in the dictionary has been changed, 
    /// and is therefore <c>dirty</c>.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <returns>
    /// 	<c>true</c> if the specified item has been changed and is therefore <c>dirty</c>; otherwise, <c>false</c>.
    /// </returns>
    public bool IsDirtyItem(TKey key) {
      return _InnerDictionary[key].IsDirty;
    }

    /// <summary>
    /// Returns a specific item in the underlying dictionary.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <returns></returns>
    public DirtyDictionaryItem<TKey, TValue> FullItem(TKey key) {
      return _InnerDictionary[key];
    }

    /// <summary>
    /// Resets the IsDirty flag on all items in the dictionary.
    /// </summary>
    public void ResetDirty() {
      foreach (KeyValuePair<TKey, DirtyDictionaryItem<TKey, TValue>> item in _InnerDictionary) {
        item.Value.ResetDirty();
      }
      _IsDirty = false;
    }
    #endregion


    #region Properties - Indexer - Implementation of IDictionary
    /// <summary>
    /// Gets a Collection containing the keys of
    /// the Dictionary.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>
    /// An Collection containing the keys of the object
    /// that implements Dictionary.
    /// </returns>
    public ICollection<TKey> Keys {
      get {
        return _InnerDictionary.Keys;

      }
    }
    #endregion

    #region Properties - Implementation of IDictionary
    //
    /// <summary>
    /// Gets an Collection containing the values in
    /// the Dictionary.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>
    /// An Collection containing the values in the
    /// object that implements Dictionary.
    /// </returns>
    public ICollection<TValue> Values {
      get {
        List<TValue> result = new List<TValue>();
        foreach (DirtyDictionaryItem<TKey, TValue> valItem in _InnerDictionary.Values) {
          result.Add(valItem.Value);
        }
        return result;
      }
    }

    /// <summary>
    /// Gets or sets the element with the specified key.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>
    /// The element with the specified key.
    /// </returns>
    /// <exception cref="System.NotSupportedException">The Dictionary is read-only.</exception>
    /// <exception cref="System.ArgumentNullException">key is null</exception>
    /// <exception cref="System.ArgumentException">The key was not found.</exception>
    public TValue this[TKey key] {
      get {
        DirtyDictionaryItem<TKey, TValue> valItem = _InnerDictionary[key];
        return valItem.Value;
      }
      set {
        DirtyDictionaryItem<TKey, TValue> valItem;
        if (_InnerDictionary.ContainsKey(key)) {
          valItem = _InnerDictionary[key];
          valItem.Value = value;
          //Changes to IsDirty
          //happens only if value was different than previous value
          //so use item to check:
          this.IsDirty = _IsDirty | valItem.IsDirty;
          //Which will raise event...
        }
        else {
          valItem = new DirtyDictionaryItem<TKey, TValue>(key, value);
          _InnerDictionary.Add(key, valItem);
          //valItem will be dirty:
          //so changes IsDirty:
          this.IsDirty = _IsDirty | valItem.IsDirty;
          //Which will raise event...
        }
      }
    }
    #endregion

    #region Properties - Implementation of ICollection
    /// <summary>
    /// Gets the number of elements in the collection.
    /// </summary>
    public int Count {
      get {
        return _InnerDictionary.Count;
      }
    }

    /// <summary>
    /// Gets a value indicating whether the Collection is read-only.
    /// </summary>
    /// <value></value>
    /// <returns><c>true</c>if the Collection is read-only; otherwise, false.</returns>
    public bool IsReadOnly {
      get {
        return _IsReadOnly;
      }
      set {
        if (value && (_IsReadOnly == false)) {
          _IsReadOnly = true;
        }
      }
    }
    private bool _IsReadOnly;
    #endregion


    #region Methods - Implementation of IDictionary

    /// <summary>
    /// Adds an element with the provided key and value to the Dictionary.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="key">The key.</param>
    /// <param name="value">The value to add.</param>
    /// <exception cref="System.NotSupportedException">The Dictionary is read-only.</exception>
    /// <exception cref="System.ArgumentException">An element with the same key already exists in the Dictionary</exception>
    /// <exception cref="System.ArgumentNullException">key is null</exception>
    public void Add(TKey key, TValue value) {
      DirtyDictionaryItem<TKey, TValue> valItem = new DirtyDictionaryItem<TKey, TValue>(key, value);
      _InnerDictionary.Add(key, valItem);

      //If new item is dirty (which it will be), 
      //then it affects the whole flag:
      this.IsDirty = _IsDirty | valItem.IsDirty;
      //Which will raise event...
    }


    /// <summary>
    /// Determines whether the Dictionary
    /// contains an element with the specified key.
    /// </summary>
    /// <returns>
    /// true if the Dictionary contains
    /// an element with the key; otherwise, false.
    /// </returns>
    /// <remarks>
    /// </remarks>
    /// <param name="key">The key to locate in the Dictionary.</param>
    /// <exception cref="System.ArgumentNullException">key is null</exception>
    public bool ContainsKey(TKey key) {
      return _InnerDictionary.ContainsKey(key);
    }

    /// <summary>
    /// Removes the element with the specified key from the Dictionary.
    /// </summary>
    /// <param name="key">The key of the element to remove.</param>
    /// <returns>
    /// true if the element is successfully removed; otherwise, false.  This method
    /// also returns false if key was not found in the original Dictionary.
    /// </returns>
    /// <exception cref="System.NotSupportedException">The Dictionary is read-only.</exception>
    /// <exception cref="System.ArgumentNullException">key is null</exception>
    public bool Remove(TKey key) {
      bool removed = _InnerDictionary.Remove(key);
      if (removed) {
        _IsDirtyScanned = false;
      }
      return removed;
    }


    /// <summary>
    /// Tries to get the value.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public bool TryGetValue(TKey key, out TValue value) {
      DirtyDictionaryItem<TKey, TValue> valItem;
      if (_InnerDictionary.TryGetValue(key, out valItem)) {
        value = valItem.Value;
        return true;
      }
      //FECK...this means Guid or other Empty structs not valid...
      value = default(TValue);
      return false;
    }

    #endregion

    #region Methods - Implementation of ICollection
    /// <summary>
    /// Adds an item to the Collection.
    /// </summary>
    /// <param name="item">The object to add to the Collection.</param>
    /// <exception cref="System.NotSupportedException">The collection is read-only.</exception>
    public void Add(KeyValuePair<TKey, TValue> item) {
      //use other overload to do the hard work (as well as setting IsDirty):
      this.Add(item.Key, item.Value);
    }

    ///
    /// <summary>
    /// Removes all items from the Collection.
    /// </summary>
    /// <exception cref="System.NotSupportedException">The collection is read-only.</exception>
    public void Clear() {
      _InnerDictionary.Clear();
      //Reset flag:
      _IsDirty = false;
    }


    ///
    /// <summary>
    /// Determines whether the Collection contains
    /// a specific value.
    /// </summary>
    /// <param name="item">The object to locate in the Collection.</param> 
    /// <returns><c>true</c>if item is found in the Collection; otherwise, false.</returns>
    public bool Contains(KeyValuePair<TKey, TValue> item) {
      return _InnerDictionary.ContainsKey(item.Key);
    }

    ///
    /// <summary>
    /// Removes the first occurrence of a specific object from the Collection.
    /// </summary>
    /// <param name="item">The object to remove from the Collection.</param>
    /// <remarks>
    /// true if item was successfully removed from the Collection;
    /// otherwise, false. This method also returns false if item is not found in
    /// the original Collection.
    /// </remarks>
    /// <exception cref="System.NotSupportedException">The collection is read-only.</exception>
    public bool Remove(KeyValuePair<TKey, TValue> item) {
      bool removed = _InnerDictionary.Remove(item.Key);
      if (removed) {
        _IsDirtyScanned = false;
      }
      return removed;
    }

    ///
    /// <summary>
    /// Copies the elements of the Collection to an
    /// System.Array, starting at a particular System.Array index.
    /// </summary>
    /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
    /// <exception cref="System.ArgumentNullException">array is null</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">arrayIndex is less than 0</exception>
    /// <exception cref="System.ArgumentException">array is multidimensional.-or-arrayIndex is equal to or greater than the
    /// length of array.-or-The number of elements in the source Collection
    /// is greater than the available space from arrayIndex to the end of the destination
    /// array.-or-Type KeyValuePair cannot be cast automatically to the type of the destination
    /// array.</exception>
    /// <param name="array">The one-dimensional System.Array that is the destination of the elements
    /// copied from Collection. The System.Array must be zero-based
    /// </param>
    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) {
      List<KeyValuePair<TKey, TValue>> results = new List<KeyValuePair<TKey, TValue>>();
      foreach (KeyValuePair<TKey, DirtyDictionaryItem<TKey, TValue>> item in _InnerDictionary) {
        KeyValuePair<TKey, TValue> itemPair = new KeyValuePair<TKey, TValue>(item.Key, item.Value.Value);
        results.Add(itemPair);
      }
      results.CopyTo(array, arrayIndex);
    }

    /// <summary>
    /// Returns an enumerator that iterates through the collection.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
    /// </returns>
    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
      //return (IEnumerator<KeyValuePair<TKey, TValue>>)_InnerDictionary.Values.GetEnumerator();
      //return _InnerDictionary.Values.GetEnumerator();

      foreach (KeyValuePair<TKey, DirtyDictionaryItem<TKey, TValue>> item in _InnerDictionary) {
        KeyValuePair<TKey, TValue> kvPair = new KeyValuePair<TKey, TValue>(item.Value.Key,item.Value.Value);

        yield return kvPair;

      }

    }

    // IEnumerable<T> inherits from IEnumerable, therefore this class 
    // must implement both the generic and non-generic versions of 
    // GetEnumerator. In most cases, the non-generic method can 
    // simply call the generic method.
    IEnumerator IEnumerable.GetEnumerator() {
      return GetEnumerator();
    }


    #endregion

    #region Protected - Raise Events
    /// <summary>
    /// Raises the <see cref="E:Changed"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
    virtual protected void OnChanged(EventArgs e) {
      if (Changed != null) {
        Changed(this, e);
      }
    }
    #endregion

    #region Protected Methods
    /// <summary>
    /// Scans items for any that are IsDirty.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Invoked by <see cref="IsDirty"/>{get;} property.
    /// </para>
    /// </remarks>
    protected void CheckDirty() {
      if (!_IsDirtyScanned) {
        //Already been scanned: _IsDirty is whatever it is:
        return;
      }
      foreach (KeyValuePair<TKey, DirtyDictionaryItem<TKey, TValue>> item in _InnerDictionary) {
        if (item.Value.IsDirty) {
          //Make sure this is set before getting out early:
          _IsDirtyScanned = true;
          return;
        }
      }
      //Aha! All is clean!
      _IsDirty = false;
      //And mark that we are scanned.
      //This will be reset by Remove method.
      _IsDirtyScanned = true;
    }
    #endregion


  }



  /// <summary>
  /// An individual Attribute element.
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TValue"></typeparam>
  public class DirtyDictionaryItem<TKey, TValue> {

    #region Properties
    /// <summary>
    /// Gets the attribute key.
    /// </summary>
    /// <value>The key.</value>
    public TKey Key {
      get {
        return _Key;
      }
    }
    private readonly TKey _Key;

    /// <summary>
    /// Gets or sets the attribute value.
    /// </summary>
    /// <value>The value.</value>
    public TValue Value {
      get {
        return _Value;
      }
      set {
        if (!Equals(value, _Value)) {
          _Value = value;
          _IsDirty = true;
        }
      }
    }
    private TValue _Value;

    /// <summary>
    /// Gets a value indicating whether the value has changed since construted.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that the Flag can only be <c>set;</c> to True.
    /// To reset the flag to False, use the <see cref="M:ResetDirty"/> method.
    /// </para>
    /// </remarks>
    /// <value><c>true</c> if this instance is dirty; otherwise, <c>false</c>.</value>
    public bool IsDirty {
      get {
        return _IsDirty;
      }
      set {
        if (value && (_IsDirty == false)) {
          _IsDirty = true;
        }
      }
    }
    //Reset by ResetDirty, but updated by every this[] change 
    private bool _IsDirty;
    #endregion

    #region Methods
    /// <summary>
    /// Resets the IsDirty flag to false.
    /// </summary>
    public void ResetDirty() {
      _IsDirty = false;
    }
    #endregion

    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="T:DirtyDictionaryItem&lt;TKey, TValue&gt;"/> class.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    public DirtyDictionaryItem(TKey key, TValue value) {
      if (key.Equals(default(TKey))) {
        throw new ArgumentNullException("key");
      }
      _Key = key;
      _Value = value;
      _IsDirty = true;
    }


    /// <summary>
    /// Initializes a new instance of the <see cref="T:DirtyDictionaryItem&lt;TKey, TValue&gt;"/> class.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="isDirty">if set to <c>true</c> [is dirty].</param>
    public DirtyDictionaryItem(TKey key, TValue value, bool isDirty) {
      if (key.Equals(default(TKey))) {
        throw new ArgumentNullException("key");
      }
      _Key = key;
      _Value = value;
      _IsDirty = isDirty;
    }
    #endregion
  }
}


