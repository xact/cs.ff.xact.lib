﻿namespace XAct.Collections {
    using System.Collections;
    using System.Collections.ObjectModel;

    /// <summary>
  /// An <see cref="ObservableCollection{T}"/>
  /// that accepts <see cref="System.Object"/>s, raising the 
  /// <see cref="ObservableCollection{T}.CollectionChanged"/> 
  /// event every time an item
  /// is added, removed, or changed.
  /// </summary>
  public class ObservableObjectCollection :
    ObservableCollection<object> {

    #region Constructors
    /// <summary>
    /// Initializes a new instance of 
    /// the <see cref="ObservableObjectCollection"/> class.
    /// </summary>
    /// <param name="list">The list of items to add to the collection.</param>
    /// <exception cref="System.ArgumentNullException">
    /// An exception is raised if <paramref name="list"/> is null.
    /// </exception>
    public ObservableObjectCollection(
      IEnumerable list) :

      this(list, false) {
    }

    /// <summary>
    /// Initializes a new instance of the 
    /// <see cref="ObservableObjectCollection"/> class.
    /// </summary>
    /// <param name="list">The list of items to add to the collection.</param>
    /// <param name="stripNulls">if set to <c>true</c> does 
    /// not add <c>null</c> list items.
    /// </param>
    /// <exception cref="System.ArgumentNullException">
    /// An exception is raised if <paramref name="list"/> is null.
    /// </exception>
    public ObservableObjectCollection(
      IEnumerable list,
      bool stripNulls) {
      //Check Args:
        list.ValidateIsNotDefault("list");
        //Add items to collection one at a time:
      foreach (object item in list) {
        if ((stripNulls) && (item == null)) {
          continue;
        }
        this.Add(item);
      }
    }
    #endregion

  }
}
