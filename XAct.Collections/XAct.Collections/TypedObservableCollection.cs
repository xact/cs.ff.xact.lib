﻿namespace XAct.Collections {
    using System;
    using System.Collections;
    using System.Collections.ObjectModel;

    /// <summary>
  /// An <see cref="ObservableCollection{T}"/>
  /// that accepts <see cref="System.Object"/>s, raising the 
  /// <see cref="ObservableCollection{T}.CollectionChanged"/> 
  /// event every time an item
  /// is added, removed, or changed.
  /// </summary>
  public class TypedObservableObjectCollection :
    ObservableCollection<object> {

    #region Properties
    /// <summary>
    /// Gets or sets the <see cref="Type"/> 
    /// of the item.
    /// </summary>
    /// <value>The type of the item.</value>
    public object ItemType {
      get { return _ItemType; }
      set {
        if (value == null) { return; }
        if (value is Type) {
          ItemType = value;
          return;
        }
        if (value is string) {
          string typeName = (string)value;
          if (typeName == string.Empty) { return; }

          //This is a red herring and would lead to ambiguity:
          //String would work (System.String)
          //Int would not (System.Int),
          //etc...
          //if (typeName.IndexOf('.')==-1){
          //  typeName = "System." + typeName;
          //}

          _ItemType = Type.GetType(typeName);
        }
      }
    }
    private Type _ItemType;
    #endregion



    #region Constructors
    /// <summary>
    /// Initializes a new instance of 
    /// the <see cref="ObservableObjectCollection"/> class.
    /// </summary>
    /// <param name="list">The list of items to add to the collection.</param>
    /// <exception cref="System.ArgumentNullException">
    /// An exception is raised if <paramref name="list"/> is null.
    /// </exception>
    public TypedObservableObjectCollection(
      IEnumerable list) :

      this(list, false) {
    }

    /// <summary>
    /// Initializes a new instance of the 
    /// <see cref="ObservableObjectCollection"/> class.
    /// </summary>
    /// <param name="list">The list of items to add to the collection.</param>
    /// <param name="stripNulls">if set to <c>true</c> does 
    /// not add <c>null</c> list items.
    /// </param>
    /// <exception cref="System.ArgumentNullException">
    /// An exception is raised if <paramref name="list"/> is null.
    /// </exception>
    public TypedObservableObjectCollection(
      IEnumerable list,
      bool stripNulls) {
      //Check Args:
        list.ValidateIsNotDefault("list");
        //Add items to collection one at a time:
      foreach (object item in list) {
        if ((stripNulls) && (item == null)) {
          continue;
        }
        this.Add(item);
      }
    }
    #endregion


    #region Overrides
    /// <summary>
    /// Inserts an element into the 
    /// <see cref="T:System.Collections.ObjectModel.Collection`1"/> 
    /// at the specified index.
    /// </summary>
    /// <param name="index">
    /// The zero-based index at which <paramref name="item"/> 
    /// should be inserted.</param>
    /// <param name="item">
    /// The object to insert. 
    /// The value can be null for reference types.</param>
    /// <exception cref="T:System.ArgumentOutOfRangeException">
    /// 	<paramref name="index"/> is less than zero.
    /// -or-
    /// <paramref name="index"/> is greater 
    /// than <see cref="P:System.Collections.ObjectModel.Collection`1.Count"/>.
    /// </exception>
    protected override void InsertItem(int index, object item) {
      if (_ItemType != null) {
        base.InsertItem(
          index,
          Convert.ChangeType(item, _ItemType, null)
          );
        return;
      }

      base.InsertItem(index, item);
    }
    #endregion

  }
}
