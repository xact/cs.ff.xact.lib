﻿#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
namespace XAct {
#endif

/// <summary>
    /// Extensions to the LinkedList
    /// </summary>
    public class LinkedListExtensions
    {
        ///// <summary>
        ///// Reverses the specified linked list.
        ///// </summary>
        ///// <remarks>
        ///// Src: http://www.codeproject.com/KB/recipes/ReverseLinkedList.aspx
        ///// </remarks>
        ///// <param name="linkedList">The linked list.</param>
        //public static void Reverse<T>(this LinkedList<T> linkedList)
        //{
        //    LinkedListNode<T> start = linkedList.Head;
        //    LinkedListNode<T> temp = null;

        //    // ------------------------------------------------------------
        //    // Loop through until null node (next node of the latest node) is found
        //    // ------------------------------------------------------------

        //    while (start != null)
        //    {
        //        // ------------------------------------------------------------
        //        // Swap the “Next” and “Previous” node properties
        //        // ------------------------------------------------------------

        //        temp = start.Next;
        //        start.Next = start.Previous;
        //        start.Previous = temp;

        //        // ------------------------------------------------------------
        //        // Head property needs to point to the latest node
        //        // ------------------------------------------------------------

        //        if (start.Previous == null)
        //        {
        //            linkedList.Head = start;
        //        }

        //        // ------------------------------------------------------------
        //        // Move on to the next node (since we just swapped 
        //        // “Next” and “Previous”
        //        // “Next” is actually the “Previous”
        //        // ------------------------------------------------------------

        //        start = start.Previous;
        //    }

        //    // ------------------------------------------------------------
        //    // That's it!
        //    // ------------------------------------------------------------
        //}
    }


#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
