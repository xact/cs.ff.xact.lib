﻿namespace XAct.Collections
{
    using System;

    /// <summary>
    /// Contract for an element wrapper, 
    /// that exposes a Date last used value.
    /// <para>
    /// Used by the <see cref="MRUList{TItem,TItemId}"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public interface IDateTimeStampedElementWrapper<TItem> 
    {
        /// <summary>
        /// Gets the DateTime the item was last touched.
        /// </summary>
        DateTime Touched { get; }
        
        /// <summary>
        /// Gets the wrapped Item.
        /// </summary>
        TItem Item {get;}

        /// <summary>
        /// Updates <see cref="Touched"/> to the current DateTime.
        /// </summary>
        void Touch();
    }
}