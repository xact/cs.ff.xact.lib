﻿namespace XAct
{
    using System;
    using System.Linq.Expressions;
    using AutoMapper;
    using XAct.UI.Views;

    /// <summary>
    /// Extension methods to the <see cref="IMemberConfigurationExpression{TSource}"/>
    /// object
    /// </summary>
    public static class IMemberConfigurationExpressionExtensions
    {
        /// <summary>
        /// Maps from the Src to the Target if the Src Value is not null and does not already match target.
        /// </summary>
        /// <typeparam name="TMember">The type of the member.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="x">The x.</param>
        /// <param name="mapping">The mapping.</param>
        public static void MapFromIfNotNullAndDontMatch<TMember,TSource>(this IMemberConfigurationExpression<TMember> x, Expression<Func<TMember,TSource>>  mapping)
        {
//#pragma warning disable 168
//            x.Condition(resolutionContext => { var x2 = resolutionContext.IsSourceValueNull; return true; });
//#pragma warning restore 168
            x.Condition(resolutionContext =>
                            {
                                //We're trying to ensure that we don't map if the Source is null, or it's source type is null:
                                ResolutionResult o=null;
                                //If Source Type is null, we didn't get anything posted back:
                                if (resolutionContext.SourceType == null)
                                {
                                    //Hack:https://github.com/AutoMapper/AutoMapper/issues/203
                                    o = resolutionContext.PropertyMap.ResolveValue(resolutionContext.Parent);
                                    //Red Herring: The Type was defined in binding, and will always be !null
                                    //if (o.Type == null){return false;}
                                }

                                //If Source is NULL, don't copy it over:
                                //Note for this to be correct for strings: see  http://stackoverflow.com/a/7459696
                                bool isNull = (o != null) ? (o.Value==null) : resolutionContext.IsSourceValueNull;
                                if (isNull)
                                {
                                    return false;
                                }

                                //RedHerring: As we don't know the type
                                //If source == target, don't copy it over:

                                try
                                {
                                    object sourceValue = (o != null) ? (o.Value) : resolutionContext.SourceValue;
                                    object targetValue = (o == null) ? resolutionContext.DestinationValue : resolutionContext.Parent.DestinationValue.GetMemberValue(resolutionContext.MemberName);

                                    //Compare Value:
                                    if (System.Object.Equals(sourceValue, targetValue))
                                    {
                                        return false;
                                    }
                                    //Not needed:
                                    //var sourceType = (o != null) ? (o.Type) : resolutionContext.SourceType;
                                    //var src = sourceValue.ConvertTo(sourceType);
                                    //var target = targetValue.ConvertTo(resolutionContext.DestinationType);

                                    //if (System.Object.Equals(src,target))
                                    //{
                                    //    return false;
                                    //}
                                    //if (src == target)
                                    //{
                                    //    return false;
                                    //}
// ReSharper disable EmptyGeneralCatchClause
                                }catch 
// ReSharper restore EmptyGeneralCatchClause
                                {
                                    
                                }
                                
                                //Last check: include ViewMode check:
                                //Get it *within* method/right thread:
                                //IViewModeService viewModeService = DependencyResolver.Current.GetInstance<IViewModeService>();
                                //Type sourceType = resolutionContext.Parent.SourceType;
                                //string sourcePropertyName = resolutionContext.PropertyMap.SourceMember.Name;
                                //ViewMode check = viewModeService.GetViewMode(sourceType, sourcePropertyName);
                                //if (check != ViewMode.Editable)
                                //{
                                //    return false;
                                //}

                                return true;
                            });
            //Map:
            x.MapFrom(mapping);            
        }
    

        /// <summary>
        /// Conditionals the state of the based on view.
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// </summary>
        /// <typeparam name="TMember">The type of the member.</typeparam>
        /// <param name="x">The x.</param>
        public static void ConditionalBasedOnViewMode<TMember>(this IMemberConfigurationExpression<TMember> x)
        {


            IViewModeService viewModeService = DependencyResolver.Current.GetInstance<IViewModeService>();

            x.Condition(resolutionContext =>
            {
                    
                    Type sourceType = resolutionContext.Parent.SourceType;

                string sourcePropertyName = resolutionContext.PropertyMap.SourceMember.Name;

                ViewMode check = viewModeService.GetViewMode(sourceType, sourcePropertyName);

                bool mappable = (check == ViewMode.Editable);
                return mappable;
            });

        }

        /// <summary>
        /// Ignores all members.
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDest">The type of the dest.</typeparam>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
            public static IMappingExpression<TSource, TDest> IgnoreAllMembers<TSource, TDest>(this IMappingExpression<TSource, TDest> expression)
            {
                expression.ForAllMembers(opt => opt.Ignore());
                return expression;
            }
    }
}
