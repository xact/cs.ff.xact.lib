﻿// ReSharper disable CheckNamespace
namespace XAct.ObjectMapping
// ReSharper restore CheckNamespace
{
    using System;
    using AutoMapper;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TSource">The type of the source.</typeparam>
    /// <typeparam name="TTarget">The type of the target.</typeparam>
    public abstract class AutoMapperTypeMapperBase<TSource, TTarget> : AutoMapperTypeMapperBase
    {
        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="AutoMapperTypeMapperBase{TSource,TTarget}" /> class.
        /// </summary>
        protected AutoMapperTypeMapperBase():base(typeof(TSource),typeof(TTarget))
        {
            
        }
    }

    /// <summary>
    /// An specialization of <see cref="TypeMapperBase{TSource,TTarget}"/>
    /// that uses <c>AutoMapper</c>
    /// </summary>
    public abstract class AutoMapperTypeMapperBase : TypeMapperBase, IInitializableTypeMapper
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoMapperTypeMapperBase" /> class.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targeType">Type of the targe.</param>
        protected AutoMapperTypeMapperBase(Type sourceType, Type targeType):base(sourceType,targeType)
        {
            
        } 

        /// <summary>
        /// Gets a flag indicating whether this instance's <see cref="Initialize"/> has been invoked.
        /// </summary>
        public bool Initialized { get; protected set; }


        /// <summary>
        /// Gets a value indicating whether <see cref="Initialize" /> should not be invoked or not when it is being registered.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [defer mapping]; otherwise, <c>false</c>.
        /// </value>
        public bool DeferInitialization { get; protected set; }




        /// <summary>
        /// Registers the map.
        /// <para>
        /// Called by <c>IMappingRegistrationService.Register</c>
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// For example:
        /// <code>
        /// <![CDATA[
        /// //Examples of creating custom maps between values:
        /// Mapper.CreateMap<Source3, Destination3>();
        /// Mapper.CreateMap<NestedSource3, NestedDestination3>()
        ///       .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Val));
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        public abstract void Initialize();




        /// <summary>
        /// Uses the Mapping already registered in the underlying AutoMapper
        /// to generate the target object.
        /// <para>
        /// Can be called multiple times from within 
        /// <see cref="Initialize"/>
        /// </para>
        /// <para>
        /// Example:
        /// <code>
        /// <![CDATA[
        ///  public override void Initialize()
        /// {
        /// base.CreateMap<TestInvoiceB, TestInvoiceBDTO>()
        ///     .ForMember(d => d.SomeLineItems, opt => opt.MapFrom(s => s.LineItems));
        /// 
        /// base.CreateMap<TestLineItemB, TestLineItemBDTO>()
        ///     .ForMember(d => d.TestInvoiceBId,
        ///                opt => opt.MapFrom(s => s.TestInvoiceBId))
        ///     .ForMember(d => d.XDescription,
        ///                opt => opt.MapFrom(s => s.Description)).
        ///     ForMember(d => d.ItemBmount, opt => opt.MapFrom(s => s.ItemAmount));
        ///     //Mapper.CreateMap<ICollection<TestLineItemA>, ICollection<TestLineItemADTO>>();
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TS">The type of the source.</typeparam>
        /// <typeparam name="TT">The type of the target.</typeparam>
        protected virtual IMappingExpression<TS, TT> CreateMap<TS, TT>()
        {
            IMappingExpression<TS, TT> mappingExpression = Mapper.CreateMap<TS, TT>();

            return mappingExpression;
        }




        /// <summary>
        /// Action executed before source is mapped to target.
        /// <para>
        /// This override, ensures that if 
        /// <see cref="DeferInitialization"/> was set to <c>true</c>
        /// when the Mapper was registered, that <see cref="Initialize"/> be invoked now,
        /// and <see cref="Initialized"/> be set to <c>true</c>
        /// </para>
        /// <remarks>
        /// <para>
        /// If you use a framework for mappings you can use this method
        /// to invoke <see cref="Initialize"/> in order to prepare or setup the map.
        /// </para>
        /// </remarks>
        /// </summary>
        protected override void PreInternalMap(object source = null)
        {
            if (this.Initialized)
            {
                return;
            }

            Initialize();

            //Keep the flag here in case end developer forgets to set it
            //during initializtion.
            this.Initialized = true;

        }


        /// <summary>
        /// Uses the Mapping already registered in the underlying AutoMapper
        /// to generate the target object.
        /// </summary>
        /// <param name="source">The source to map</param>
        /// <param name="target">The target.</param>
        /// <returns>
        /// An instance of the target.
        /// <para>
        /// IMPORTANT: The return does not need to be typed, 
        /// as the Map method is only invoked by IMappingService -- never 
        /// invoked directly.
        /// </para>
        /// </returns>
        /// <remarks>
        /// If you use a framework, use this method for setup or resolve mapping.
        /// <example>Automapper.Map{TSource,KTarget}</example>
        /// </remarks>
        protected override object InternalMap(object source, object target = null)
        {

// ReSharper disable RedundantNameQualifier
            //Use extension method to find out if it null
            return _targetType.IsDefault(target)
// ReSharper restore RedundantNameQualifier
                ? Mapper.Map(source,_sourceType,_targetType)
// ReSharper disable RedundantTypeArgumentsOfMethod
                : Mapper.Map(source, target,_sourceType,_targetType);
// ReSharper restore RedundantTypeArgumentsOfMethod


        }






        /// <summary>
        /// Sets the initialize flag so
        /// that <see cref="Initialized" />
        /// is not reinvoked.
        /// </summary>
        void IInitializableTypeMapper.SetInitialized()
        {
            Initialized = true;
        }
    }
}
