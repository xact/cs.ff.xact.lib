namespace XAct.ObjectMapping.Services.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of 
    /// <see cref="IObjectMappingService"/>
    /// to map entities to DTOs (and back again)
    /// using the registered Mappers.
    /// </summary>
    [DefaultBindingImplementation(typeof(IObjectMappingService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class AutoMapperObjectMappingService : XActLibServiceBase, IAutoMapperObjectMappingService
    {
        private readonly IObjectMappingServiceState _typeMapperCache;


        /// <summary>
        /// Initializes a new instance of the <see cref="AutoMapperObjectMappingService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="typeMapperCache">The type mapper cache.</param>
        public AutoMapperObjectMappingService(ITracingService tracingService, IObjectMappingServiceState typeMapperCache)
            : base(tracingService)
        {
            _typeMapperCache = typeMapperCache;
        }

        /// <summary>
        /// Tests this completeness of the registered maps.
        /// </summary>
        /// <returns></returns>
        public bool Test()
        {
            AutoMapper.Mapper.AssertConfigurationIsValid();
            return true;
        }


        /// <summary>
        /// Maps the specified source to the target object.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source value.</param>
        /// <param name="target">The target value.</param>
        /// <param name="contextName">Name of the context.</param>
        /// <param name="strict">if set to <c>true</c> [strict].</param>
        /// <returns></returns>
        public object Map(Type sourceType, Type targetType, object source, object target = null,
                          string contextName = null, bool strict=true)
        {
            source.ValidateIsNotDefault("source");

            _tracingService.Trace(TraceLevel.Verbose, "Mapping {0} -> {1}", sourceType.Name, targetType.Name);

            ITypeMapper typeMapper;

            if (!_typeMapperCache.TryGetMapper(sourceType,targetType, out typeMapper, contextName))
            {
                // What happens when none has been registered -- as in the default
                // case of ValueInjecter?
                if (strict)
                {
                    //In the case of AutoMapper:
                    throw new ArgumentException("Cannot find appropriate Mapper.");
                }
                else
                {
                    if (target == null)
                    {
                        return AutoMapper.Mapper.Map(source, sourceType, targetType);
                    }
                    else
                    {
                        return AutoMapper.Mapper.Map(source, target, sourceType, targetType);
                    }
                }
            }

            return typeMapper.Map(source, target);
            
        }


        /// <summary>
        /// Maps the specified source instance (eg: Entity) to the given target instance (eg: DTO).
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="contextName">Name of the condition.</param>
        /// <param name="strict">if set to <c>true</c>, requires the registration of a formal From/To Map before trying to map it.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Cannot find appropriate Mapper.</exception>
        public TTarget Map<TSource, TTarget>(TSource source, string contextName = null, bool strict = true)
            where TSource : class
            where TTarget : class, new()
        {
            source.ValidateIsNotDefault("source");

            _tracingService.Trace(TraceLevel.Verbose, "Mapping {0} -> {1}", source.GetType().Name, typeof (TTarget).Name);

            ITypeMapper typeMapper;

            if (! _typeMapperCache.TryGetMapper<TSource,TTarget>(out typeMapper,contextName))
            {
                // What happens when none has been registered -- as in the default
                // case of ValueInjecter?

                if (strict)
                {
                    //In the case of AutoMapper:
                    throw new ArgumentException("Cannot find appropriate Mapper.");
                }
                else
                {
                        return AutoMapper.Mapper.Map<TSource,TTarget>(source);
                }
            }

            return typeMapper.Map(source, null) as TTarget;
        }


        /// <summary>
        /// Maps the specified source instance (eg: Entity) to the given target instance (eg: DTO).
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="contextName">Name of the condition.</param>
        /// <param name="strict">if set to <c>true</c>, requires the registration of a formal From/To Map before trying to map it.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Cannot find appropriate Mapper.</exception>
        public TTarget Map<TSource, TTarget>(TSource source, TTarget target, string contextName = null, bool strict=true)
            where TSource : class
            where TTarget : class
        {
            source.ValidateIsNotDefault("source");

            _tracingService.Trace(TraceLevel.Verbose, "Mapping {0} -> {1}", source.GetType().Name, typeof(TTarget).Name);

            ITypeMapper typeMapper;

            if (!_typeMapperCache.TryGetMapper<TSource, TTarget>(out typeMapper, contextName))
            {
                // What happens when none has been registered -- as in the default
                // case of ValueInjecter?

                if (strict)
                {
                    //In the case of AutoMapper:
                    throw new ArgumentException("Cannot find appropriate Mapper.");
                }
                else
                {
                    return AutoMapper.Mapper.Map<TSource, TTarget>(source,target);
                }
            }

            return typeMapper.Map(source, target) as TTarget;
        }

    }
}
