namespace XAct.ObjectMapping.Services.Implementations
{
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="IObjectMappingRegistrationService"/>
    /// <para>
    /// Note that this service is intended to be invoked
    /// during the bootstrap stage.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(IObjectMappingRegistrationService), BindingLifetimeType.Undefined, Priority.Low /*OK:Secondary Binding*/)]
    public class AutoMapperObjectMappingRegistrationService : XActLibServiceBase, IAutoMapperObjectMappingRegistrationService
    {

        private readonly IObjectMappingServiceState _typeMapperCache;


        /// <summary>
        /// Initializes a new instance of the <see cref="AutoMapperObjectMappingService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="typeMapperCache">The type mapper cache.</param>
        public AutoMapperObjectMappingRegistrationService(ITracingService tracingService, IObjectMappingServiceState typeMapperCache):base(tracingService)
        {
            _typeMapperCache = typeMapperCache;
        }

        /// <summary>
        /// Tests this completeness of the registered maps.
        /// <para>
        /// Tip:
        /// </para>
        /// <para>
        /// When initializing, invoke this method every time 
        /// after adding a new map 
        /// (it's easier to check after adding a single map, than trying to analyse
        /// all maps in one go).
        /// </para>
        /// </summary>
        public bool Test()
        {
            AutoMapper.Mapper.AssertConfigurationIsValid();
            return true;
        }

        /// <summary>
        /// Registers the mapper.
        /// </summary>
        /// <param name="typeMapper">The type mapper.</param>
        /// <param name="contextName">Name of the context.</param>
        public void RegisterMapper(ITypeMapper typeMapper, string contextName=null)
        {
            typeMapper.ValidateIsNotDefault("typeMapper");

            _tracingService.Trace(TraceLevel.Verbose, "RegisteringMapper: {0}", typeMapper.ToString());


            //Add or replace:
            int index = typeMapper.GetHashCode();

            _typeMapperCache.RegisterMap(index, typeMapper,contextName);


            IInitializableTypeMapper initializableTypeMapper = 
                (IInitializableTypeMapper)typeMapper;

            if (!initializableTypeMapper.DeferInitialization)
            {
                //Add to underlying AutoMapper:
                initializableTypeMapper.Initialize();
                //If I don't set this, 
                initializableTypeMapper.SetInitialized();
            }
        }
    }
}
