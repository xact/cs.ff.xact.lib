namespace XAct.ObjectMapping
{
    /// <summary>
    /// An Automapper based specialization of the 
    /// <see cref="IObjectMappingRegistrationService"/>
    /// contract.
    /// </summary>
    public interface IAutoMapperObjectMappingRegistrationService : IObjectMappingRegistrationService, IHasXActLibService
    {

    }
}