namespace XAct.ObjectMapping
{
    /// <summary>
    /// Contract for a portable Type Mapper.
    /// <para>
    /// Note: Do not retrieve Mappers directly - go through IMappingService.
    /// </para>
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <typeparam name="TSource">The type of the source.</typeparam>
    /// <typeparam name="TTarget">The type of the target.</typeparam>
    public interface ITypeMapper<in TSource, TTarget> : ITypeMapper
        where TSource : class
        where TTarget : class, new()
    {

        
        
        /// <summary>
        /// The resolver.
        /// Adapt a {TSource} instance into an instance of type {TTarget}
        /// (new, or the one given if <paramref name="target"/> is not null).
        /// <para>
        /// Note that this is the method that
        /// the IMappingService.Map{TS,TT}
        /// is internally invoking.
        /// </para>
        /// </summary>
        /// <param name="source">The source item to adapt (eg: <c>Invoice</c>)</param>
        /// <param name="target">The (optional) existing target to merge into.</param>
        /// <returns>
        /// The target instance
        /// </returns>
        TTarget Map(TSource source, TTarget target = null);
    }
}