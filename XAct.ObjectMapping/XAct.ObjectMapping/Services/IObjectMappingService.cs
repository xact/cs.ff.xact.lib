namespace XAct.ObjectMapping
{
    using System;

    /// <summary>
    /// The contract for a Service to map
    /// Entities to DTOs and back again.
    /// </summary>
    /// <remark>
    /// Note that the management of the mappers
    /// (the registering of Mappers) is handled
    /// by a different interface (IMappingManagementService)
    /// usually accessed only by the application's
    /// bootstrapper/initialization.
    /// (SOC).
    /// </remark>
    public interface IObjectMappingService : IHasXActLibService
    {

        /// <summary>
        /// Tests this completeness of the registered maps.
        /// </summary>
        /// <returns></returns>
        bool Test();


        /// <summary>
        /// Maps the specified source to the target object.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source value.</param>
        /// <param name="target">The target value.</param>
        /// <param name="contextName">Name of the context.</param>
        /// <param name="strict">if set to <c>true</c>, requires the registration of a formal From/To Map before trying to map it.</param>
        /// <returns></returns>
        object Map(Type sourceType, Type targetType, object source, object target = null, string contextName = null, bool strict = true);

        /// <summary>
        /// Maps the given Source object to the instantiated target object.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="contextName">Name of the condition.</param>
        /// <param name="strict">if set to <c>true</c>, requires the registration of a formal From/To Map before trying to map it.</param>
        /// <returns></returns>
        TTarget Map<TSource, TTarget>(TSource source, string contextName = null, bool strict = true)
            where TSource : class
            where TTarget : class, new();


        /// <summary>
        /// Maps the given Source object to the instantiated target object.
        /// <para>
        /// If <paramref name="target" /> is not supplied,
        /// a
        /// </para>
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="contextName">Name of the condition.</param>
        /// <param name="strict">if set to <c>true</c>, requires the registration of a formal From/To Map before trying to map it.</param>
        /// <returns></returns>
        TTarget Map<TSource, TTarget>(TSource source, TTarget target, string contextName = null, bool strict=true)
            where TSource : class
            where TTarget : class;

    }
}