﻿// ReSharper disable CheckNamespace
namespace XAct.ObjectMapping.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// An implementation of 
    /// <see cref="IObjectMappingServiceState"/> to cache Types against
    /// their Maps, optionally, classified per context (in 
    /// </summary>
    /// <internal>
    /// The dictionary's key is an int intended for the value 
    /// that <see cref="ITypeMap"/>'s <c>.GetHashCode()</c> returns.
    /// <para>
    /// The reason for this is that a dictionary can more easily compare int's that Equals compare objects.
    /// </para>
    /// <para>
    /// The value is also a dictionary, generally with only one string key (string.Empty) for the default
    /// condition. In some mapper technologies, more than one mapping strategy can be mapped against
    /// a type.
    /// </para>
    /// </internal>
    public class ObjectMappingServiceState : Dictionary<int, IDictionary<string, ITypeMapper>>, IObjectMappingServiceState
    {


        /// <summary>
        /// Tries getting the mapper registered for the given types, and context.
        /// </summary>
        /// <typeparam name="TSourceType">The type of the source type.</typeparam>
        /// <typeparam name="TTargetType">The type of the target type.</typeparam>
        /// <param name="typeMapper">The type mapper that was found.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <returns></returns>
        public bool TryGetMapper<TSourceType, TTargetType>(out ITypeMapper typeMapper, string contextName = null)
        {
            TypeMap<TSourceType,TTargetType>typeMap= new TypeMap<TSourceType, TTargetType>();
            return TryGetMapper(typeMap, out typeMapper, contextName);
        }


        /// <summary>
        /// Tries getting the mapper registered for the given types, and context.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="typeMapper">The type mapper that was found.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <returns></returns>
        public bool TryGetMapper(Type sourceType, Type targetType, out ITypeMapper typeMapper, string contextName = null)
        {
            TypeMap typeMap = new TypeMap(sourceType,targetType);

            return TryGetMapper(typeMap, out typeMapper, contextName);
        }

        /// <summary>
        /// Tries getting the mapper registered for the given types, and context.
        /// </summary>
        /// <param name="typeMap">The TypeMap whose HashCode is used as the key.</param>
        /// <param name="typeMapper">The type mapper that was found.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <returns></returns>
        public bool TryGetMapper(ITypeMap typeMap, out ITypeMapper typeMapper, string contextName = null)
        {
            return TryGetMapper(typeMap.GetHashCode(), out typeMapper, contextName);
        }

        /// <summary>
        /// Tries getting the mapper registered for the given context.
        /// </summary>
        /// <param name="typeMapperHashCode">The type mapper hash code.</param>
        /// <param name="typeMapper">The type mapper that was found.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <returns></returns>
        public bool TryGetMapper(int typeMapperHashCode, out ITypeMapper typeMapper, string contextName = null)
        {
            typeMapper = null;

            IDictionary<string, ITypeMapper> mapperDictionary;
            if (!TryGetValue(typeMapperHashCode, out mapperDictionary))
            {
                return false;
            }
            //Have dictionary at this point
            if (contextName.IsNull())
            {
                contextName = string.Empty;
            }
// ReSharper disable AssignNullToNotNullAttribute
            return mapperDictionary.TryGetValue(contextName, out typeMapper);
// ReSharper restore AssignNullToNotNullAttribute
        }

        /// <summary>
        /// Registers the map.
        /// </summary>
        /// <param name="typeMapperHashCode">The type mapper hash code.</param>
        /// <param name="typeMapper">The type mapper to register.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <exception cref="System.ArgumentException">TypeMapper already registed under '{1}' contextName.FormatStringInvariantCulture(typeMapper.ToString(),contextName)</exception>
        public void RegisterMap(int typeMapperHashCode, ITypeMapper typeMapper, string contextName = null)
        {
            if (contextName.IsNull())
            {
                contextName = string.Empty;
            }
            
            IDictionary<string, ITypeMapper> mapperDictionary;
            if (!this.TryGetValue(typeMapperHashCode, out mapperDictionary))
            {
                this[typeMapperHashCode] = mapperDictionary = new Dictionary<string, ITypeMapper>();
            }

//// ReSharper disable AssignNullToNotNullAttribute
//            if (mapperDictionary.ContainsKey(contextName))
//// ReSharper restore AssignNullToNotNullAttribute
//            {
//                if (contextName.IsNullOrEmpty())
//                {
//                    contextName = "[default]";
//                }
//                throw new ArgumentException("TypeMapper already registed under '{1}' contextName".FormatStringInvariantCulture(typeMapper.ToString(),contextName));
//            }

            mapperDictionary[contextName] = typeMapper;

        }
    }
}