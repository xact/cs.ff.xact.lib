﻿namespace XAct.ObjectMapping
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public interface IObjectMappingServiceState : IDictionary<int, IDictionary<string, ITypeMapper>>, IHasXActLibServiceState
    {
        /// <summary>
        /// Tries getting the mapper registered for the given types, and context.
        /// </summary>
        /// <typeparam name="TSourceType">The type of the source type.</typeparam>
        /// <typeparam name="TTargetType">The type of the target type.</typeparam>
        /// <param name="typeMapper">The type mapper that was found.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <returns></returns>
        bool TryGetMapper<TSourceType, TTargetType>(out ITypeMapper typeMapper, string contextName = null);

        /// <summary>
        /// Tries getting the mapper registered for the given types, and context.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="typeMapper">The type mapper that was found.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <returns></returns>
        bool TryGetMapper(Type sourceType,Type targetType, out ITypeMapper typeMapper, string contextName = null);

        /// <summary>
        /// Tries getting the mapper registered for the given types, and context.
        /// </summary>
        /// <param name="key">The TypeMap whose HashCode is used as the key.</param>
        /// <param name="typeMapper">The type mapper that was found.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <returns></returns>
        bool TryGetMapper(ITypeMap key, out ITypeMapper typeMapper, string contextName = null);

        /// <summary>
        /// Tries getting the mapper registered for the given context.
        /// </summary>
        /// <param name="typeMapperHashCode">The type mapper hash code.</param>
        /// <param name="typeMapper">The type mapper that was found.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <returns></returns>
        bool TryGetMapper(int typeMapperHashCode, out ITypeMapper typeMapper, string contextName = null);
 
        /// <summary>
        /// Registers the map.
        /// </summary>
        /// <param name="typeMapperHashCode">The type mapper hash code.</param>
        /// <param name="typeMapper">The type mapper to register.</param>
        /// <param name="contextName">Name of the context (default is string.Empty).</param>
        /// <exception cref="System.ArgumentException">TypeMapper already registed under '{1}' contextName.FormatStringInvariantCulture(typeMapper.ToString(),contextName)</exception>
        void RegisterMap(int typeMapperHashCode, ITypeMapper typeMapper, string contextName = null);
    }
}