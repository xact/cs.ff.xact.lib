﻿namespace XAct.ObjectMapping
{
    /// <summary>
    /// Service to configure the <see cref="ITypeMapper"/>s
    /// that an implementation of <see cref="IObjectMappingService"/>
    /// will use.
    /// </summary>
    public interface IObjectMappingRegistrationService : IHasXActLibService
    {

        /// <summary>
        /// Tests this completeness of the registered maps.
        /// <para>
        /// Tip:
        /// </para>
        /// <para>
        /// When initializing, invoke this method every time 
        /// after adding a new map 
        /// (it's easier to check after adding a single map, than trying to analyse
        /// all maps in one go).
        /// </para>
        /// </summary>
        bool Test();

        /// <summary>
        /// Registers the <see cref="ITypeMapper{TS,TT}" /> with the service
        /// so that it can later be used by <see cref="IObjectMappingService"/>
        /// </summary>
        /// <param name="typeMapper">The type mapper.</param>
        /// <param name="contextName">Name of the context.</param>
        void RegisterMapper(ITypeMapper typeMapper, string contextName=null);

        ///// <summary>
        ///// Registers the <see cref="ITypeMapper{TSource,TTarget}"/> .
        ///// </summary>
        ///// <typeparam name="TSource">The type of the source.</typeparam>
        ///// <typeparam name="TTarget">The type of the target.</typeparam>
        ///// <param name="typeMapper">The type mapper.</param>
        //void RegisterMapper<TSource, TTarget>(ITypeMapper<TSource, TTarget> typeMapper)
        //    where TSource : class
        //    where TTarget : class, new();


    }
}
