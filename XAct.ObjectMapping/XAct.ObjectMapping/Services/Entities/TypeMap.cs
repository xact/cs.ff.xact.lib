﻿// ReSharper disable CheckNamespace
namespace XAct.ObjectMapping
// ReSharper restore CheckNamespace
{
    using System;


    /// <summary>
    /// A generic version of <see cref="TypeMap"/>
    /// </summary>
    /// <typeparam name="TSource">The type of the source.</typeparam>
    /// <typeparam name="TTarget">The type of the target.</typeparam>
    public class TypeMap<TSource, TTarget> : TypeMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TypeMap{TSource,TTarget}" /> class.
        /// </summary>
        public TypeMap() : base(typeof(TSource), typeof(TTarget)) { }
    }




    /// <summary>
    /// </summary>
    public class TypeMap : ITypeMap
    {
        /// <summary>
        /// The _source type
        /// </summary>
        protected readonly Type _sourceType ;

        /// <summary>
        /// The _target type
        /// </summary>
        protected readonly Type _targetType;


        /// <summary>
        /// Initializes a new instance of the <see cref="TypeMap{TTarget,TTarget}" /> class.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targetType">Type of the target.</param>
        public TypeMap(Type sourceType, Type targetType)
        {
            _sourceType = sourceType;
            _targetType = targetType;
        }




        /// <summary>
        /// Gets the type of the source to be mapped from.
        /// </summary>
        /// <value>
        /// The type of the source.
        /// </value>
        public Type SourceType { get { return _sourceType; } }

        /// <summary>
        /// Gets the type of the target the source is to mapped to.
        /// </summary>
        /// <value>
        /// The type of the target.
        /// </value>
        public Type TargetType { get { return _targetType; } }



        /// <summary>
        /// Returns a hash code for a TypeMap of the given pair of Types.
        /// </summary>
        /// <typeparam name="TSourceType">The type of the ource type.</typeparam>
        /// <typeparam name="TTargetType">The type of the artget type.</typeparam>
        /// <returns></returns>
        public static int GetHashCode<TSourceType, TTargetType>()
        {
            var result = GetHashCode(typeof(TSourceType), typeof(TTargetType));
            return result;
        }

        /// <summary>
        /// Returns a hash code for a TypeMap of the given pair of Types.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public static int GetHashCode(Type sourceType, Type targetType)
        {
            return sourceType.GetHashCode() ^ targetType.GetHashCode() * 31;
        }



        ///// <summary>
        ///// Gets the unique string descriptor of this <see cref="ITypeMap"/>
        ///// <para>
        ///// (eg: 'Invoice'&lt;-&gt;InvoiceDTO')
        ///// </para>
        ///// </summary>
        ///// <returns></returns>
        //public static string GetDescriptor()
        //{
        //    return GetDescriptor(typeof (TSource), typeof (TTarget));
        //}

        /// <summary>
        /// Gets the descriptor.
        /// <para>
        /// (eg: 'Invoice'&lt;-&gt;InvoiceDTO')
        /// </para>
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <returns></returns>
        static string GetDescriptor(Type sourceType, Type targetType)
        {
            return string.Format("{0}<->{1}", sourceType.FullName, targetType.FullName);
        }


        /// <summary>
        /// Returns a hash code that uniquely describes this Mapping 
        /// (two Instances of this object, describing the same Source/Target, will 
        /// have the same HashCode).
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (!_requestedHashCode.HasValue)
            {
                _requestedHashCode = TypeMap.GetHashCode(_sourceType, _targetType);
            }

            return _requestedHashCode.Value;

        }
        int? _requestedHashCode;




        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            //Use a protected method, as ToString is commonly overridden,
            //which would change the way the hash worked.
            var result = GetDescriptor(_sourceType,_targetType);
            return result;
        }


        /// <summary>
        /// <see cref="object.Equals(object)"/>
        /// </summary>
        /// <param name="obj"><see cref="System.Object.Equals(object)"/></param>
        /// <returns><see cref="System.Object.Equals(object)"/></returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            TypeMap spec = obj as TypeMap;

            if (spec == null)
            {
                return false;
            }

            if (spec.GetHashCode() != this.GetHashCode())
            {
                return false;
            }

            return true;
        }

    }
}