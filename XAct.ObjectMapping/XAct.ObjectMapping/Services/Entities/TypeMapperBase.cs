
// ReSharper disable CheckNamespace

namespace XAct.ObjectMapping
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// The type map configuration base. With this class
    /// you can specify the map to convert a source type into target type.
    /// </summary>
    public abstract class TypeMapperBase : TypeMap, ITypeMapper
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="TypeMapperBase{TSource,TTarget}" /> class.
        /// </summary>
        protected TypeMapperBase(Type sourceType, Type targetType)
            : base(sourceType, targetType)
        {

        }


        /// <summary>
        /// Maps the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public TTarget Map<TTarget>(object source, TTarget target = null)
            where TTarget:class 
        {
            return Map((object)source, (object)target) as TTarget;
        }

        /// <summary>
        /// Maps the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The more sources.</param>
        /// <returns></returns>
        public object Map(object source, object target = null)
        {
            /*
             * Resolve adapter pipeline
             * pre -> map -> post
             */

            //execute prefilter
            PreInternalMap(source);

            //map from source to target 
            //using conventions or specific things ( for each framework )
            object resultTarget = InternalMap(source, target);

            //execute postfilter
            PostInternalMap(resultTarget);

            //return adapted object
            return resultTarget;
        }





        /// <summary>
        /// Action executed before source is mapped to target
        /// by <see cref="InternalMap"/> being invoked by <see cref="Map"/>
        /// <remarks>
        /// </remarks>
        /// </summary>
        protected abstract void PreInternalMap(object source = null);


        /// <summary>
        /// Map a source entity to a target entity
        /// </summary>
        /// <param name="source">The source to map</param>
        /// <param name="target">The target.</param>
        /// <returns>
        /// A existing (or new if target is null) instance of the target.
        /// <para>
        /// IMPORTANT: The return does not need to be typed, 
        /// as the Map method is only invoked by IMappingService -- never 
        /// invoked directly.
        /// </para>
        /// </returns>
        /// <remarks>
        /// If you use a framework, use this method for setup or resolve mapping.
        /// <example>Automapper.Map{TSource,KTarget}</example>
        /// </remarks>
        protected abstract object InternalMap(object source, object target = null);



        /// <summary>
        /// Action executed after source is mapped to target when
        /// by <see cref="InternalMap"/> being invoked by <see cref="Map"/>
        /// <remarks>
        /// You can use this method for set more sources into adapted object
        /// </remarks>
        /// </summary>
        /// <param name="target">The item adapted </param>
        protected virtual void PostInternalMap(object target) { }



    }
}
