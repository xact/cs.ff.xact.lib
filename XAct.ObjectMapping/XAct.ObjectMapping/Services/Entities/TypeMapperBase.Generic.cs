
// ReSharper disable CheckNamespace
namespace XAct.ObjectMapping
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// The type map configuration base. With this class
    /// you can specify the map to convert a source type into target type.
    /// </summary>
    /// <typeparam name="TSource">The source </typeparam>
    /// <typeparam name="TTarget">The target type</typeparam>
    public abstract class TypeMapperBase<TSource, TTarget> : TypeMapperBase, ITypeMapper<TSource, TTarget>
        where TSource : class
        where TTarget : class,new()
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="TypeMapperBase{TSource,TTarget}" /> class.
        /// </summary>
        protected TypeMapperBase()
            : base(typeof(TSource),typeof(TTarget))
        {

        }

        /// <summary>
        /// Maps the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The more sources.</param>
        /// <returns></returns>
        public TTarget Map(TSource source, TTarget target = null)
        {
            return base.Map(source, target) as TTarget;
        }

    }
}
