
//namespace XAct.ObjectMapping
//{
//    /// <summary>
//    /// A contract for managing the ITypeMapper entities
//    /// that IMappingService then refers to.
//    /// <para>
//    /// Note that this service is intended to be invoked
//    /// during the bootstrap stage.
//    /// </para>
//    /// </summary>
//    public interface IMappingRegistrationService: IHasXActLibServiceDefinition
//    {
//        /// <summary>
//        /// Registers the <see cref="ITypeMapper{TS,TT}"/> with the service.
//        /// <para>
//        /// Important: Prefer where possible the typed overload of this method.
//        /// </para>
//        /// </summary>
//        /// <param name="typeMapper">The type mapper.</param>
//        void RegisterMapper(ITypeMap typeMapper);
        

//        ///// <summary>
//        ///// Registers the <see cref="ITypeMapper{TSource,TTarget}"/> .
//        ///// </summary>
//        ///// <typeparam name="TSource">The type of the source.</typeparam>
//        ///// <typeparam name="TTarget">The type of the target.</typeparam>
//        ///// <param name="typeMapper">The type mapper.</param>
//        //void RegisterMapper<TSource, TTarget>(ITypeMapper<TSource, TTarget> typeMapper)
//        //    where TSource : class
//        //    where TTarget : class, new();

//        /// <summary>
//        /// Returns the <see cref="ITypeMapper{TSource,TTarget}"/>
//        /// for the given Types.
//        /// <para>
//        /// Note that this is the method that <see cref="IMappingService.Map{TSource,TTarget}"/>
//        /// invokes internally.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TSource">The type of the source.</typeparam>
//        /// <typeparam name="TTarget">The type of the target.</typeparam>
//        /// <returns></returns>
//        bool TryGet<TSource, TTarget>(out ITypeMap result)
//            where TSource : class
//            where TTarget : class, new();


//    }
//}
