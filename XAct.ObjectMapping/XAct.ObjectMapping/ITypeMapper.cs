// ReSharper disable CheckNamespace
namespace XAct.ObjectMapping
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITypeMapper : ITypeMap
    {
        /// <summary>
        /// The resolver.
        /// Maps the properties of a Source onto a Target instance
        /// (new, or the one given if <paramref name="target" /> is not null).
        /// <para>
        /// Note that this is the method that
        /// the IMappingService.Map{TS,TT} is internally invoking.
        /// </para>
        /// </summary>
        /// <param name="source">The source item to adapt (eg: <c>Invoice</c>)</param>
        /// <param name="target">The (optional) existing target to merge into.</param>
        /// <returns>
        /// The target instance
        /// </returns>
        object Map(object source, object target = null);
    }


}