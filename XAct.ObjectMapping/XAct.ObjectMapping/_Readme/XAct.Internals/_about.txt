﻿
//Examples of creating custom maps between values:
Mapper.CreateMap<Source3, Destination3>();
Mapper.CreateMap<NestedSource3, NestedDestination3>()
    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Val));




An example would be:

namespace App.Application.BankingModule.DTOAdapters.Maps
{
    using AutoMapper;

    using App.Application.BankingModule.DTOs;
    using App.Domain.Aggregates.BankAccountAgg;
    using App.Infrastructure.Crosscutting.Adapters;

    /// <summary>
    /// The bank activity to bank activity dto map
    /// </summary>
    public class BankActivityToBankActivityDTOMap
        : TypeMapConfigurationBase<BankAccountActivity, BankActivityDTO>
    {
        protected override void BeforeMap(ref BankAccountActivity source)
        {
            Mapper.CreateMap<BankAccountActivity, BankActivityDTO>();
        }

        protected override void AfterMap(ref BankActivityDTO target, params object[] moreSources)
        {
            //don't need
        }

        protected override BankActivityDTO Map(BankAccountActivity source)
        {
            return Mapper.Map<BankAccountActivity, BankActivityDTO>(source);
        }
    }
}
