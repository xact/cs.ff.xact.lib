﻿
namespace XAct.ObjectMapping
{
    /// <summary>
    /// 
    /// </summary>
    public interface IInitializableTypeMapper
    {
        /// <summary>
        /// Gets a value indicating whether <see cref="Initialize" /> should not be invoked or not when it is being registered.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [defer mapping]; otherwise, <c>false</c>.
        /// </value>
        bool DeferInitialization { get; }




        /// <summary>
        /// Initializes the map.
        /// <para>
        /// Invoked by <c>IMappingRegistrationService.Register</c>
        /// unless <see cref="DeferInitialization"/> was set to <c>true</c>,
        /// in which case it is invoked by <see cref="TypeMapperBase.PreInternalMap"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// For example:
        /// <code>
        /// <![CDATA[
        /// //Examples of creating custom maps between values:
        /// Mapper.CreateMap<Source3, Destination3>();
        /// Mapper.CreateMap<NestedSource3, NestedDestination3>()
        ///       .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Val));
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        void Initialize();



        /// <summary>
        /// Sets the initialize flag so
        /// that <c>Initialized</c>
        /// is not reinvoked.
        /// </summary>
        void SetInitialized();
    }
}
