    namespace XAct.Tests
    {
        using System;
        using System.Linq;
        using NUnit.Framework;
        using XAct.ObjectMapping;
        using XAct.ObjectMapping.Implementations;
        using XAct.ObjectMapping.Services.Implementations;

        /// <summary>
        ///   NUNit Test Fixture.
        /// </summary>
        [TestFixture]
        public class IAutoMapperOBjectMappingServiceTests
        {
            /// <summary>
            ///   Sets up to do before any tests 
            ///   within this test fixture are run.
            /// </summary>
            [TestFixtureSetUp]
            public void TestFixtureSetUp()
            {
                //run once before any tests in this testfixture have been run...
                //...setup vars common to all the upcoming tests...

                            Singleton<IocContext>.Instance.ResetIoC();

            }

            /// <summary>
            ///   Tear down after all tests in this fixture.
            /// </summary>
            [TestFixtureTearDown]
            public void TestFixtureTearDown()
            {
            
            }


            [TearDown]
            public void MyTestTearDown()
            {
                GC.Collect();
            }


            [Test]
            public void CanGetIAutoMappingObjectMappingRegistrationService()
            {
                //ARRANGE
                var service = DependencyResolver.Current.GetInstance<IAutoMapperObjectMappingRegistrationService>();

                //ACT

                //ASSERT
                Assert.IsNotNull(service);

            }


            [Test]
            public void CanGetIAutoMappingObjectMappingRegistrationServiceOfExpectedType()
            {
                //ARRANGE
                var service = DependencyResolver.Current.GetInstance<IAutoMapperObjectMappingRegistrationService>();

                //ACT

                //ASSERT
                Assert.AreEqual(typeof(AutoMapperObjectMappingRegistrationService), service.GetType());
            }




            [Test]
            public void EnsureHashIsAlwaysTheSame()
            {
                ITypeMapper x = new TestInvoiceAMapper();
                int baseLine = x.GetHashCode();

                for (int i = 0; i < 10000;i++ )
                {
                    x = new TestInvoiceAMapper();
                    Assert.AreEqual(baseLine,x.GetHashCode());
                }
            }

            [Test]
            public void EnsureEqualityWorks()
            {
                ITypeMapper x = new TestInvoiceAMapper();
                ITypeMapper y = new TestInvoiceAMapper();

                Assert.IsTrue(x.Equals(y)); 
            }

            [Test]
            public void EnsureNonEqualityWorks()
            {
                ITypeMapper x = new TestInvoiceAMapper();
                ITypeMapper y = new TestInvoiceBMapper();

                Assert.IsFalse(x.Equals(y));
            }

            [Test]
            public void EnsureServiceIsAutoMapperBased()
            {
                IObjectMappingService mappingService = DependencyResolver.Current.GetInstance<IObjectMappingService>();

                IAutoMapperObjectMappingService autoMapperMappingService = mappingService as IAutoMapperObjectMappingService;

                Assert.IsNotNull(autoMapperMappingService);
            }

            [Test]
            public void EnsureRegistrationServiceIsAutoMapperBased()
            {
                IObjectMappingRegistrationService mappingRegistrationService = DependencyResolver.Current.GetInstance<IObjectMappingRegistrationService>();

                IAutoMapperObjectMappingRegistrationService autoMapperMappingRegistrationService = mappingRegistrationService as IAutoMapperObjectMappingRegistrationService;

                Assert.IsNotNull(autoMapperMappingRegistrationService);
            }


            /// <summary>
            ///   An Example Test.
            /// </summary>
            [Test]
            public void UseTestInvoiceAMapperDirectlyWithoutService()
            {
                TestInvoiceA invoice = CreateTestInvoiceA();
            
                TestInvoiceAMapper x = new TestInvoiceAMapper();

                TestInvoiceADTO y = x.Map < TestInvoiceADTO>(invoice);

                Assert.IsTrue(y.LineItems.Count>0);

                TestInvoiceAMapper x2 = new TestInvoiceAMapper();

                TestInvoiceADTO y2 = x2.Map < TestInvoiceADTO>(invoice);

                Assert.IsTrue(y2.LineItems.Count > 0);

            }




            /// <summary>
            ///   An Example Test.
            /// </summary>
            [Test]
            public void UseTestInvoiceBMapperDirectlyWithoutService()
            {
                TestInvoiceB invoice = CreateTestInvoiceB();

                TestInvoiceBMapper x = new TestInvoiceBMapper();

                TestInvoiceBDTO y = x.Map < TestInvoiceBDTO>(invoice);

                Assert.IsTrue(y.SomeLineItems.Count > 0);

                TestInvoiceBMapper x2 = new TestInvoiceBMapper();

                TestInvoiceBDTO y2 = x2.Map < TestInvoiceBDTO>(invoice);

                Assert.IsTrue(y2.SomeLineItems.Count > 0);



            }


            [Test]
            public void MapUsingServiceAndGenericMethod()
            {


                IObjectMappingService mappingService = DependencyResolver.Current.GetInstance<IObjectMappingService>();
                IObjectMappingRegistrationService mappingRegistrationService = DependencyResolver.Current.GetInstance<IObjectMappingRegistrationService>();

                mappingRegistrationService.RegisterMapper(new TestInvoiceAMapper());

                TestInvoiceA invoice = CreateTestInvoiceA();
                TestInvoiceADTO dto = mappingService.Map<TestInvoiceA, TestInvoiceADTO>(invoice);

                Assert.IsTrue(dto!=null);
                Assert.IsTrue(dto.LineItems.Count>0);
            }


            [Test]
            public void MapUsingServiceAndNonGenericMethod()
            {


                //Get the Management Service, and register a map:
                IObjectMappingRegistrationService mappingRegistrationService = DependencyResolver.Current.GetInstance<IObjectMappingRegistrationService>();

                mappingRegistrationService.RegisterMapper(new TestInvoiceAMapper());

                TestInvoiceA invoice = CreateTestInvoiceA();


                IObjectMappingService mappingService = DependencyResolver.Current.GetInstance<IObjectMappingService>();
                TestInvoiceADTO dto = (TestInvoiceADTO)mappingService.Map(typeof(TestInvoiceA), typeof(TestInvoiceADTO), invoice);

                Assert.IsTrue(dto != null);
                Assert.IsTrue(dto.LineItems.Count > 0);
            }

            [Test]
            public void CanHandleMapBeingRegisteredMultipleTimes()
            {
                //Get the Management Service, and register a map:
                IObjectMappingRegistrationService mappingRegistrationService = DependencyResolver.Current.GetInstance<IObjectMappingRegistrationService>();

                mappingRegistrationService.RegisterMapper(new TestInvoiceAMapper());
                mappingRegistrationService.RegisterMapper(new TestInvoiceAMapper());
                mappingRegistrationService.RegisterMapper(new TestInvoiceAMapper());
                mappingRegistrationService.RegisterMapper(new TestInvoiceAMapper());

                TestInvoiceA invoice = CreateTestInvoiceA();


                IObjectMappingService mappingService = DependencyResolver.Current.GetInstance<IObjectMappingService>();
                TestInvoiceADTO dto = (TestInvoiceADTO)mappingService.Map(typeof(TestInvoiceA), typeof(TestInvoiceADTO), invoice);

                Assert.IsTrue(dto != null);
                Assert.IsTrue(dto.LineItems.Count > 0);
            }





        


            private static TestInvoiceA CreateTestInvoiceA()
            {
                TestInvoiceA invoice = new TestInvoiceA();

                invoice.Id = 101;
                invoice.DateCreated = DateTime.Now;
                invoice.LineItems.Add(new TestLineItemA
                {
                    Id = 202,
                    TestInvoiceAId = 101,
                    Description = "Nails",
                    Count = 30,
                    ItemAmount = 0.15m
                });
                invoice.LineItems.First().Invoice = invoice;
                return invoice;
            }

            private static TestInvoiceB CreateTestInvoiceB()
            {
                TestInvoiceB invoice = new TestInvoiceB();

                invoice.Id = 101;
                invoice.DateCreated = DateTime.Now;
                invoice.LineItems.Add(new TestLineItemB
                {
                    Id = 202,
                    TestInvoiceBId = 101,
                    Description = "Nails",
                    Count = 30,
                    ItemAmount = 0.15m
                });
                invoice.LineItems.First().Invoice = invoice;
                return invoice;
            }


    
        }


    }


