
namespace XAct.Tests
{
    using XAct.ObjectMapping;

    public class TestInvoiceLineItemMapper : AutoMapperTypeMapperBase<TestLineItemA, TestLineItemADTO>
    {

        public override void Initialize()
        {
            base.CreateMap<TestLineItemA, TestLineItemADTO>();
        }



    }
}
