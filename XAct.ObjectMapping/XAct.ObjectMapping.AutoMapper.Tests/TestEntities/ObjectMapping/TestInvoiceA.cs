namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class TestInvoiceA
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public ICollection<TestLineItemA> LineItems { get; private set; }
        
        public TestInvoiceA()
        {
            LineItems = new Collection<TestLineItemA>();
        }
    }

    public class TestLineItemA
    {
        public int Id { get; set; }
        public int TestInvoiceAId {get;set;}
        public TestInvoiceA Invoice {get;set;}
        public string Description { get; set; }
        public int Count { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal Amount { get { return Count*ItemAmount; } set {}}
    }

    public class TestInvoiceADTO 
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public ICollection<TestLineItemADTO> LineItems { get { return _LineItems; } private set { _LineItems = value;}}
        private ICollection<TestLineItemADTO> _LineItems = new Collection<TestLineItemADTO>();
    }

    public class TestLineItemADTO
    {
        public int Id { get; set; }
        public int TestInvoiceAId { get; set; }
        public TestInvoiceA Invoice { get; set; }
        public string Description { get; set; }
        public int Count { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal Amount { get;set;}
    }
}
