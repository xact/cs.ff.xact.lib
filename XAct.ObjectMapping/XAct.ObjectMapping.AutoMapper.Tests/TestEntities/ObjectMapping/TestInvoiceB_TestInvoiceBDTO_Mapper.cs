namespace XAct.Tests
{
    using XAct.ObjectMapping;

    public class TestInvoiceBMapper : AutoMapperTypeMapperBase<TestInvoiceB, TestInvoiceBDTO>
    {
        public override void Initialize()
        {
            base.CreateMap<TestInvoiceB, TestInvoiceBDTO>()
                .ForMember(d => d.SomeLineItems, opt => opt.MapFrom(s => s.LineItems))
                ;

            base.CreateMap<TestLineItemB, TestLineItemBDTO>()
                .ForMember(d => d.TestInvoiceBId,
                           opt => opt.MapFrom(s => s.TestInvoiceBId))
                .ForMember(d => d.XDescription,
                           opt => opt.MapFrom(s => s.Description)).
                ForMember(d => d.ItemBmount, opt => opt.MapFrom(s => s.ItemAmount));

            //Mapper.CreateMap<ICollection<TestLineItemA>, ICollection<TestLineItemADTO>>();
        }
    }
}
