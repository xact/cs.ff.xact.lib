namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class TestInvoiceB
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public ICollection<TestLineItemB> LineItems { get; private set; }
        
        public TestInvoiceB()
        {
            LineItems = new Collection<TestLineItemB>();
        }
    }

    public class TestLineItemB
    {
        public int Id { get; set; }
        public int TestInvoiceBId {get;set;}
        public TestInvoiceB Invoice {get;set;}
        public string Description { get; set; }
        public int Count { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal Amount { get { return Count*ItemAmount; } set {}}
    }

    public class TestInvoiceBDTO 
    {
        public int Id { get; private set; }
        public DateTime DateCreated { get; set; }
        public ICollection<TestLineItemBDTO> SomeLineItems { get { return _SomeLineItems; } private set { _SomeLineItems = value;}}
        private ICollection<TestLineItemBDTO> _SomeLineItems = new Collection<TestLineItemBDTO>();
    }

    public class TestLineItemBDTO
    {
        public int Id { get; private set; }
        public int TestInvoiceBId { get; private set; }
        public TestInvoiceB Invoice { get; set; }
        public string XDescription { get; set; }
        public int Count { get; set; }
        public decimal ItemBmount { get; set; }
        public decimal Amount { get;set;}
    }
}
