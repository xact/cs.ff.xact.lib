
namespace XAct.Tests
{
    using XAct.ObjectMapping;

    public class TestInvoiceAMapper : AutoMapperTypeMapperBase<TestInvoiceA, TestInvoiceADTO>
    {



        public override void Initialize()
        {
            base.CreateMap<TestInvoiceA, TestInvoiceADTO>();
            base.CreateMap<TestLineItemA, TestLineItemADTO>();
            //Mapper.CreateMap<ICollection<TestLineItemA>, ICollection<TestLineItemADTO>>();
        }

    }
}
