﻿namespace XAct.Compilation
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// The response to send over the wire.
    /// </summary>
    [DataContract]
    public class CompilationServiceResponse
    {
        /// <summary>
        /// The name of the assembly created.
        /// </summary>
        [DataMember] public string AssemblyName;

        /// <summary>
        /// The raw bytes of the assembly.
        /// </summary>
        [DataMember] public byte[] AssemblyRawData;

        /// <summary>
        /// The compilation was successful.
        /// </summary>
        [DataMember] public bool CompilationSucceeded;

        /// <summary>
        /// A list of compilation errors -- if any.
        /// </summary>
        [DataMember] public IEnumerable<CompilationResultErrors> Errors;

        /// <summary>
        /// Any message.
        /// </summary>
        [DataMember] public string Message;
    }
}