﻿namespace XAct.Compilation
{
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// The results of a Compilation.
    /// </summary>
    public class CompilationResult
    {
        /// <summary>
        /// The created assembly (or null if failed).
        /// </summary>
        public Assembly Assembly;

        /// <summary>
        /// The location of the assembly.
        /// </summary>
        public string AssemblyLocation;

        /// <summary>
        /// The name of the assembly created.
        /// </summary>
        public string AssemblyName;

        /// <summary>
        /// 
        /// </summary>
        public bool CompilationSucceeded;

        /// <summary>
        /// An enumerable list of compilation errors.
        /// </summary>
        public IEnumerable<CompilationResultErrors> Errors;

        /// <summary>
        /// Any error message
        /// </summary>
        public string Message;
    }
}