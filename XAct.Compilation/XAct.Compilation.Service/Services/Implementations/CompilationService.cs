﻿namespace XAct.Compilation.Implementations
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using Microsoft.CSharp;
    using XAct.IO;

    /// <summary>
    /// </summary>
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CompilationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <param name="targetFramework"></param>
        /// <returns></returns>
        [OperationContract]
        public CompilationServiceResponse CompileText(string sourceCode, TargetFramework targetFramework)
        {
            //CompileHelper.SilverlightRootFolder =
            //    String.Format(@"{0}\App_Data\SilverlightRuntime\",
            //    HostingEnvironment.ApplicationPhysicalPath);


            CompilationResult compilationResult = Compile(TargetFramework.Silverlight, sourceCode);
            CompilationServiceResponse compilationServiceResponse = new CompilationServiceResponse();

            compilationServiceResponse.CompilationSucceeded = compilationResult.CompilationSucceeded;

            IIOService ioService = DependencyResolver.Current.GetInstance<IIOService>();

            compilationServiceResponse.AssemblyRawData =
                compilationServiceResponse.CompilationSucceeded
                    ? ioService.FileOpenReadAsync(compilationResult.AssemblyLocation).WaitAndGetResult().ReadAllBytes()
                    : new byte[] {};

            compilationServiceResponse.Errors = compilationResult.Errors;

            if (compilationServiceResponse.CompilationSucceeded)
            {
               ioService.FileDeleteAsync(compilationResult.AssemblyLocation).Wait();
            }

            return compilationServiceResponse;
        }


        /// <summary>
        /// Compiles C# source code
        /// </summary>
        /// <returns>compiled assembly if successful or null if errors or warnings were found</returns>
        public CompilationResult Compile(TargetFramework targetFramework, string text)
        {
            string[] defaultReferencedAssemblies =
                (targetFramework == TargetFramework.Silverlight)
                    ? new[]
                          {
                              "mscorlib.dll",
                              "system.dll",
                              "System.Core.dll",
                              "System.Net.dll",
                              "System.Windows.dll",
                              "System.Windows.Browser.dll",
                              "System.Xml.dll"
                          }
                    : new[] {"System.dll"}
                ;


            return Compile(targetFramework, text, defaultReferencedAssemblies, null, Path.GetTempFileName());
        }

        /// <summary>
        /// Compiles C# source code
        /// </summary>
        /// <param name="targetFramework">The target framework.</param>
        /// <param name="text">C# source code</param>
        /// <param name="referencedAssemblies">a list of assemblies to be referenced</param>
        /// <param name="locations">The directories in which to search for referenced assemblies.</param>
        /// <param name="outputAssemblyFileName">output file name, or null to suppress file output</param>
        /// <returns>
        /// compiled assembly if successful or null if errors or warnings were found
        /// </returns>
        public CompilationResult Compile(
            TargetFramework targetFramework,
            string text,
            string[] referencedAssemblies,
            string[] locations,
            string outputAssemblyFileName
            )
        {
            CompilationResult compilationResult = new CompilationResult();


            CompilerParameters compilerParameters = new CompilerParameters();

            compilerParameters.GenerateInMemory = true;
            compilerParameters.OutputAssembly = outputAssemblyFileName;
            compilerParameters.TreatWarningsAsErrors = false;
            compilerParameters.WarningLevel = 4;
            compilerParameters.TempFiles.KeepFiles = false;

            compilerParameters.CompilerOptions = "lib:" + string.Join(",", locations);


            if (targetFramework == TargetFramework.Silverlight)
            {
                // Silverlight only: ignore the rsp file, because we want to replace the
                // default desktop .net framework mscorlib and other "default" dlls
                // with the Silverlight ones
                compilerParameters.CompilerOptions = " /nostdlib ";
            }

            compilerParameters.ReferencedAssemblies.AddRange(referencedAssemblies);

            try
            {
                using (CSharpCodeProvider provider = new CSharpCodeProvider())
                {
                    CompilerResults results
                        = provider.CompileAssemblyFromSource(compilerParameters, text);

                    List<CompilationResultErrors> errorList =
                        new List<CompilationResultErrors>(results.Errors.Count);

                    foreach (CompilerError error in results.Errors)
                    {
                        CompilationResultErrors info = new CompilationResultErrors();
                        info.Column = error.Column;
                        info.ErrorNumber = error.ErrorNumber;
                        info.ErrorText = error.ErrorText;
                        info.FileName = error.FileName;
                        info.IsWarning = error.IsWarning;
                        info.Line = error.Line;
                        errorList.Add(info);
                    }

                    compilationResult.Errors = errorList.ToArray();

                    compilationResult.CompilationSucceeded = (results.Errors.Count == 0);
                    compilationResult.Assembly = compilationResult.CompilationSucceeded
                                                     ? results.CompiledAssembly
                                                     : null;

                    compilationResult.AssemblyLocation = compilationResult.CompilationSucceeded
                                                             ? outputAssemblyFileName
                                                             : null;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);

                compilationResult.Message = e.Message;
            }
            return compilationResult;
        }
    }
}