﻿namespace XAct.Compilation
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Defines a target framework, used to specify a framework to compile against
    /// </summary>
    [DataContract]
    public enum TargetFramework
    {
        /// <summary>
        /// Compact Framework
        /// </summary>
        CC,
        /// <summary>
        /// Full Framework
        /// </summary>
        FF,
        /// <summary>
        /// Silverlight Framework
        /// </summary>
        Silverlight
    }
}