﻿namespace XAct.Compilation
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Information about an error
    /// we don't use the CompilerError class in order to transport 
    /// the data in case of Silverlight project
    /// </summary>
    [DataContract]
    public class CompilationResultErrors
    {
        /// <summary>
        ///     Gets or sets the column number where the source of the error occurs.
        /// </summary>
        [DataMember] public int Column;

        /// <summary>
        ///     Gets or sets the error number.
        /// </summary>
        [DataMember] public string ErrorNumber;

        /// <summary>
        ///     Gets or sets the text of the error message.
        /// </summary>
        [DataMember] public string ErrorText;

        /// <summary>
        ///     Gets or sets the file name of the source file that contains the code which
        ///     caused the error.
        /// </summary>
        [DataMember] public string FileName;

        /// <summary>
        ///     Gets or sets a value that indicates whether the error is a warning.
        /// </summary>
        [DataMember] public bool IsWarning;

        /// <summary>
        ///     Gets or sets the line number where the source of the error occurs.
        /// </summary>
        [DataMember] public int Line;
    }
}