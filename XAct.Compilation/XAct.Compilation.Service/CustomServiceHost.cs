﻿using System;
using System.ServiceModel;

/// <summary>
/// 
/// </summary>
public class CustomServiceHost : ServiceHost
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CustomServiceHost"/> class.
    /// </summary>
    /// <param name="serviceType">Type of the service.</param>
    /// <param name="baseAddresses">The base addresses.</param>
    public CustomServiceHost(Type serviceType, params Uri[] baseAddresses)
        : base(serviceType, baseAddresses)
    {
    }

    ///// <summary>
    ///// Loads the service description information from the configuration file and applies it to the runtime being constructed.
    ///// </summary>
    ///// <exception cref="T:System.InvalidOperationException">The description of the service hosted is null.</exception>
    //protected override void ApplyConfiguration()
    //{
    //    base.ApplyConfiguration();
    //}
}