﻿
$apiKey="...";
$dryRun = true;
$maxCounter = 708;
//MaxGaps
$maxGaps = 100;


$srcFile = "D:\BB\CS.FF.XAct.Lib\NugetPackagesToCleanup.config";
$xml = [xml](get-content $srcFile);
$packageNodes = $xml.SelectNodes("/packages/package");





  foreach($node in $packageNodes){

    $package = $node.innerText;


    Write-Host("========================================");
    Write-Host ("...Package Name: `"$package`"")

     # Get Last Known Counter:
    $counter= $node.getAttribute("counter") -as [int];


    $deletedFlag = $node.getAttribute("deleted") -as [bool];

  

    $errorCounter =0;
    While ($counter -le $maxCounter) 
    {
        $counter = $counter +=1;
        Write-Host "Nuget.exe delete $package 0.0.$counter -ApiKey $apiKey -NoPrompt"
        #if not a dry run:
        if (!$dryRun){
            Nuget.exe delete $package 0.0.$counter -ApiKey $apiKey -NoPrompt

            if ($LastExitCode -eq 0){
               $errorCounter = 0;
            }else{
               $errorCounter += 1;
                Write-Host "...errorCounter: $errorCounter"
               if ($errorCounter -gt $maxGaps){
                  //Move on to next package:
                Write-Host "*** Warning: Too many successive errors: breaking out to next Package!"
                  break;
               }
            }
        }

        if (!$dryRun){
            $node.setAttribute("counter","$counter");
        }
        
        # that's a lot of saves....but better do it in case things are interupted.
        $doc.Save($srcFile);
    }# Next Counter
}# next package
  



