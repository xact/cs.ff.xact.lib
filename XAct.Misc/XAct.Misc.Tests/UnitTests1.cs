//namespace XAct.Misc.Tests
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Diagnostics;
//    using System.Linq;
//    using System.Reflection;
//    using NUnit.Framework;
//    using XAct;
//    using XAct.Data.EF.CodeFirst;
//    using XAct.Initialization;
//    using XAct.Services;
//    using XAct.Tests;

//    [TestFixture]
//    public class GetTypesDecoratedWithAttributeTests
//    {
//        /// <summary>
//        ///   Sets up to do before any tests 
//        ///   within this test fixture are run.
//        /// </summary>
//        [TestFixtureSetUp]
//        public void TestFixtureSetUp()
//        {
//            //run once before any tests in this testfixture have been run...
//            //...setup vars common to all the upcoming tests...

//            Singleton<IocContext>.Instance.ResetIoC();

//        }

//        [TestFixtureTearDown]
//        public void TestFixtureTearDown()
//        {

//        }

//        [Test]
//        public void CanSeeSomeAssembliesDecoratedWithDbInitializerThatAreWithinThisAssembly()
//        {
//            //ARRANGE:
//            AppDomain appDomain = AppDomain.CurrentDomain;
//            Func<InitializerAttribute, bool> filter = null;

//            //ACT 
//            //Assembly[] assemblies = appDomain.GetAssemblies().OrderBy(a => a.GetName().Name).ToArray();
//            IBindingDescriptor[] bidningDescriptors = XAct.DependencyResolver.BindingResults.BindingScanResults.BindingDescriptors.OrderBy(b => b.InterfaceType.Name).ToArray();
//            KeyValuePair<Type, InitializerAttribute>[] found = appDomain.GetTypesDecoratedWithAttribute<InitializerAttribute, IHasXActLibDbModelBuilder>(filter);

//            // ASSERT 
//            Assert.IsNotNull(found);
//            Assert.IsTrue(found.Length > 0);

//            //var check = bidningDescriptors.Select(bd => bd.InterfaceType.Name.Contains("ExampleDbModelBuilder")).ToArray();
//            Assert.IsTrue(bidningDescriptors.Contains(bd => bd.InterfaceType.Name.Contains("ExampleDbModelBuilder")));
//            Assert.IsTrue(found.Contains(kvp => kvp.Key.Name.Contains("ExampleDbModelBuilder")));




//        }




//        [Test]
//        public void CanSeeSomeAssembliesDecoratedWithDbInitializerThatAreDirectlyReferenced()
//        {
//            //ARRANGE:
//            AppDomain appDomain = AppDomain.CurrentDomain;
//            Func<InitializerAttribute, bool> filter = null;

//            //ACT 
//            Assembly[] assemblies = appDomain.GetAssemblies().OrderBy(a => a.GetName().Name).ToArray();
//            IBindingDescriptor[] bidningDescriptors =
//                XAct.DependencyResolver.BindingResults.BindingScanResults.BindingDescriptors.OrderBy(
//                    b => b.InterfaceType.Name).ToArray();
//            KeyValuePair<Type, InitializerAttribute>[] found =
//                appDomain.GetTypesDecoratedWithAttribute<InitializerAttribute, IHasXActLibDbModelBuilder>(filter);



//            // ASSERT 
//            Assert.IsNotNull(found);
//            Assert.IsTrue(found.Length > 0);

//            Assert.IsTrue(bidningDescriptors.Contains(bd => bd.InterfaceType.Name.Contains("ExampleDbModelBuilder")));

//            Assert.IsTrue(assemblies.Contains(a => a.GetName().Name.Contains("Resource")));
//            Assert.IsTrue(bidningDescriptors.Contains(bd => bd.InterfaceType.Name.Contains("ResourceDbModelBuilder")));
//            Assert.IsTrue(found.Contains(kvp => kvp.Key.Name.Contains("ResourceDbModelBuilder")));


//        }



//        /// <summary>
//        /// This third case demonstrates that unless the Assemby is directly Referenced
//        /// by the entry assembly, it won't get seen (ie, Misc1 -> Misc2 -> XYZ, if Misc1 !-> XYZ, 
//        /// XYZ won't be found in target bin, nor scanned. Yikes).
//        /// <para>
//        /// Note that this fails in UnitTests 
//        /// is with the following *.csproj hack, with or without the second.
//        /// <code>
//        /// <![CDATA[
//        ///   <Target Name="AfterResolveReferences">
//        ///    <!-- Redefine ReferencePath to include secondary dependencies-->
//        ///    <ItemGroup>
//        ///      <ReferencePath Include="@(ReferenceDependencyPaths)">
//        ///      </ReferencePath>
//        ///    </ItemGroup>
//        ///  </Target>
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// <para>
//        /// It also fails with adding:
//        /// <code>
//        /// <![CDATA[
//        ///   <PropertyGroup>
//        ///    <PipelineCollectFilesPhaseDependsOn>
//        ///      XCustomCollectFiles1;
//        ///      $(PipelineCollectFilesPhaseDependsOn);
//        ///    </PipelineCollectFilesPhaseDependsOn>
//        ///    <CopyAllFilesToSingleFolderForPackageDependsOn>
//        ///      XCustomCollectFiles2;
//        ///      $(CopyAllFilesToSingleFolderForPackageDependsOn);
//        ///    </CopyAllFilesToSingleFolderForPackageDependsOn>
//        ///    <CopyAllFilesToSingleFolderForMsdeployDependsOn>
//        ///      XCustomCollectFiles3;
//        ///      $(CopyAllFilesToSingleFolderForMsdeployDependsOn);
//        ///    </CopyAllFilesToSingleFolderForMsdeployDependsOn>
//        ///  </PropertyGroup>
//        ///  <Target Name="XCustomCollectFiles1">
//        ///    <Message Text="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" Importance="high" />
//        ///    <Message Text="Inside of CustomCollectFiles 1" Importance="high" />
//        ///    <Message Text="MSBuildThisFileDirectory: $(MSBuildThisFileDirectory)" Importance="high" />
//        ///    <Message Text="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" Importance="high" />
//        ///    <ItemGroup>
//        ///      <!-- Note that as we in *.csproj, we append Bin -->
//        ///      <_CustomFiles Include="$(MSBuildThisFileDirectory)\bin\**\*" />
//        ///      <FilesForPackagingFromProject Include="%(_CustomFiles.Identity)">
//        ///        <!-- Note files are relative to Target, so we prepend with Bin -->
//        ///        <DestinationRelativePath>bin\%(RecursiveDir)%(Filename)%(Extension)</DestinationRelativePath>
//        ///      </FilesForPackagingFromProject>
//        ///    </ItemGroup>
//        ///  </Target>
//        ///  <Target Name="XCustomCollectFiles2">
//        ///    <Message Text="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" Importance="high" />
//        ///    <Message Text="VS2010 WebDeploy Solution" Importance="high" />
//        ///    <Message Text="MSBuildThisFileDirectory: $(MSBuildThisFileDirectory)" Importance="high" />
//        ///    <Message Text="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" Importance="high" />
//        ///    <ItemGroup>
//        ///      <!-- Note that as we in *.csproj, we append Bin -->
//        ///      <_CustomFiles Include="$(MSBuildThisFileDirectory)\bin\**\*" />
//        ///      <FilesForPackagingFromProject Include="%(_CustomFiles.Identity)">
//        ///        <!-- Note files are relative to Target, so we prepend with Bin -->
//        ///        <DestinationRelativePath>bin\%(RecursiveDir)%(Filename)%(Extension)</DestinationRelativePath>
//        ///      </FilesForPackagingFromProject>
//        ///    </ItemGroup>
//        ///  </Target>
//        ///  <Target Name="XCustomCollectFiles3">
//        ///    <Message Text="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" Importance="high" />
//        ///    <Message Text="VS2012+ WebDeploy Solution" Importance="high" />
//        ///    <Message Text="MSBuildThisFileDirectory: $(MSBuildThisFileDirectory)" Importance="high" />
//        ///    <Message Text="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" Importance="high" />
//        ///    <ItemGroup>
//        ///      <!-- Note that as we in *.csproj, we append Bin -->
//        ///      <_CustomFiles Include="$(MSBuildThisFileDirectory)\bin\**\*" />
//        ///      <FilesForPackagingFromProject Include="%(_CustomFiles.Identity)">
//        ///        <!-- Note files are relative to Target, so we prepend with Bin -->
//        ///        <DestinationRelativePath>bin\%(RecursiveDir)%(Filename)%(Extension)</DestinationRelativePath>
//        ///      </FilesForPackagingFromProject>
//        ///    </ItemGroup>
//        ///  </Target>
//        ///
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </summary>
//        [Test]
//        public void CanSeeSomeAssembliesDecoratedWithDbInitializerThatAreIndirectlyReferenced_FAILS()
//        {
//            //ARRANGE:
//            AppDomain appDomain = AppDomain.CurrentDomain;
//            Func<InitializerAttribute, bool> filter = null;

//            //ACT 
//            Assembly[] assemblies = appDomain.GetAssemblies().OrderBy(a => a.GetName().Name).ToArray();
//            IBindingDescriptor[] bidningDescriptors = XAct.DependencyResolver.BindingResults.BindingScanResults.BindingDescriptors.OrderBy(b => b.InterfaceType.Name).ToArray();
//            KeyValuePair<Type, InitializerAttribute>[] found = appDomain.GetTypesDecoratedWithAttribute<InitializerAttribute, IHasXActLibDbModelBuilder>(filter);

//            // ASSERT 
//            Assert.IsNotNull(found);
//            Assert.IsTrue(found.Length > 0);

//            Assert.IsTrue(bidningDescriptors.Contains(bd => bd.InterfaceType.Name.Contains("ExampleDbModelBuilder")));

//            Assert.IsTrue(assemblies.Contains(a => a.GetName().Name.Contains("Resource")));
//            Assert.IsTrue(bidningDescriptors.Contains(bd => bd.InterfaceType.Name.Contains("ResourceDbModelBuilder")));

//            Assert.IsFalse(assemblies.Contains(a => a.GetName().Name.Contains("Assistance")));
//            Assert.IsFalse(bidningDescriptors.Contains(bd => bd.InterfaceType.Name.Contains("HelpDbModelBuilder")));
//            Assert.IsFalse(found.Contains(kvp => kvp.Key.Name.Contains("HelpDbModelBuilder")));
//        }


//                [Test]
//                public void CanSeeSomeAssembliesDecoratedWithDbInitializerThatAreIndirectlyReferenced_CONCULUSION()
//                {
//                    Trace.WriteLine("CONCLUSION FROM THESE TESTS:");
//                    Trace.WriteLine("From what I'm seeing in Unit Tests...");
//                    Trace.WriteLine("*HAVE* to Directly Reference the Lib's EF assemblies in the EntryHost.");
//                    Trace.WriteLine("");
//                    Trace.WriteLine("Note:");
//                    Trace.WriteLine("* Could have sworn this worked before PCL...but have no proof of it.");
//                    Trace.WriteLine("* This may just be a unit test framework limitiation, hence why I felt it worked before.");
//                    Trace.WriteLine("");
//                }

//    }




//    //[Initializer]
//    public interface IExampleDbModelBuilder :IHasXActLibDbModelBuilder
//    {
        
//    }
//}


