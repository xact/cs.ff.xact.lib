using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("KW_FF_ASSEMBLYNAME.Persistence.EF.Tests")]
[assembly: AssemblyDescription("An XActLib assembly:providing persistence (using EF) to KW_FF_ASSEMBLYNAME services.")]
#if DEBUG

[assembly: AssemblyConfiguration("DEBUG")]
#else 
[assembly: AssemblyConfiguration("")]
#endif

[assembly: AssemblyCompany("KW_COMPANYNAME")]
[assembly: AssemblyProduct("KW_PRODUCTNAME")]
[assembly: AssemblyCopyright("Copyright © KW_COMPANYNAME KW_COPYRIGHTYEAR")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

//PLC: [assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

//PLC: [assembly: Guid("a5474fb2-26d2-47b5-ae14-2280171bc865")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]