using XAct.Environment;
using XAct.Location.Services;
using XAct.Net.Services;

namespace XAct.Location.Tests
{
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class IGeoIPServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test(Description = "")]
        public void Get_Get_IDateTimeService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            Assert.That(service, Is.Not.Null);
        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test(Description = "")]
        public void Get_Get_IJsonRequestService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IJsonRequestService>();
            Assert.That(service, Is.Not.Null);
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test(Description = "")]
        public void Get_Get_GeoIPService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IGeoIPService>();
            Assert.That(service,Is.Not.Null);
        }


        [Test(Description = "")]
        public void Get_Get_IFreeGeoIPService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IFreeGeoIPService>();
            Assert.That(service, Is.Not.Null);
        }
        

                [Test(Description = "")]
        public void Can_Get_Coordinates_For_Current_Location()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IGeoIPService>();
            GeoIPInfo result;
            var r = service.GetIPInfo(null, out result);
            Assert.That(r, Is.True);
            Assert.That(result.Lat, Is.Not.Null);
            Assert.That(result.Lat,Is.Not.Empty);
        }
    }


}


