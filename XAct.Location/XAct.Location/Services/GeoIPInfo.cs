using System;

namespace XAct.Location.Services
{
#pragma warning disable 1591
    public class GeoIPInfo
    {
        public DateTime LastChecked { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string TimeZone { get; set; }

        public string ResponseStatusMessage { get; set; }
    }
#pragma warning restore 1591
}