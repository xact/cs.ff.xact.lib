﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Location.Services
{
    /// <summary>
    /// Contract for a service to return the Geo location information for a given IP.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface IGeoIPService: IHasXActLibService
    {
        /// <summary>
        /// Gets the Geo location information for a given IP.
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        bool GetIPInfo(string ip, out GeoIPInfo response);
    }
}
