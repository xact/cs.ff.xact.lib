﻿using System;
using XAct.Net.Services;

namespace XAct.Location.Services.Implementations
{
    /// <summary>
    /// Implementation of the <see cref="IGeoIPService"/> contract,
    /// using the free <c>http://ip-api.com</c> service.
    /// <para></para>
    /// </summary>
    public class IpApiGeoIpService : IPAPIGeoIPService
    {
        private IJsonRequestService _jsonService;

        /// <summary>
        /// Initializes a new instance of the <see cref="IpApiGeoIpService"/> class.
        /// </summary>
        /// <param name="jsonService">The json service.</param>
        public IpApiGeoIpService(IJsonRequestService jsonService)
        {
            _jsonService = jsonService;
        }

        /// <summary>
        /// Gets the Geo location information for a given IP.
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public bool GetIPInfo(string ip, out GeoIPInfo response)
        {

            IPAPIResponse r = _jsonService.MakeRequest<IPAPIResponse>("http://ip-api.com/json/" + ip);

            response = new GeoIPInfo();

            response.LastChecked = DateTime.UtcNow;
            response.Country = r.country;
            response.Region = r.regionName;
            response.City = r.city;
            response.PostalCode = r.zip;
            response.Lat = r.lat;
            response.Long = r.lon;
            response.TimeZone = r.TimeZone;

            response.ResponseStatusMessage = r.status;

            return r.status == "success";
        }

    }
}

namespace XAct.Location.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPAPIGeoIPService : IGeoIPService { }
}