using System;
//using System.Collections.Concurrent;
using System.Collections.Generic;
using XAct.Environment;
using XAct.Location.Services.Configuration;

namespace XAct.Location.Services.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="ICachedGeoIPService"/> contract
    /// </summary>
    public class CachedGeoIpService : ICachedGeoIPService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IGeoIPService _geoIpService;
        private readonly ICachedGeoIPServiceConfiguration _cachedGeoIpServiceConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedGeoIpService" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="geoIpService">The geo ip service.</param>
        /// <param name="cachedGeoIpServiceConfiguration">The cached geo ip service configuration.</param>
        public CachedGeoIpService(IDateTimeService dateTimeService, IGeoIPService geoIpService, ICachedGeoIPServiceConfiguration cachedGeoIpServiceConfiguration)
        {
            _dateTimeService = dateTimeService;
            _geoIpService = geoIpService;
            _cachedGeoIpServiceConfiguration = cachedGeoIpServiceConfiguration;
        }


        readonly Dictionary<string,CacheItem> _cache = new Dictionary<string, CacheItem>();

        /// <summary>
        /// Gets the Geo location information for a given IP.
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public bool GetIPInfo(string ip, out GeoIPInfo response)
        {
            lock (this)
            {
                CacheItem cacheItem;
                string ipKey = ip;
                if (ipKey.IsNull())
                {
                    ipKey = "[null]";}

                ipKey = ipKey.ToLower();
                bool found = _cache.TryGetValue(ipKey, out cacheItem);

                if (found && cacheItem.CachedTill > _dateTimeService.NowUTC)
                {
                    response = cacheItem.GeoIPInfo;
                    return true;
                }
                GeoIPInfo geoIPInfo;

                if (!_geoIpService.GetIPInfo(ip, out geoIPInfo))
                {
                    response = null;
                    return false;
                }
                DateTime newDateTime = _dateTimeService.NowUTC.Add(_cachedGeoIpServiceConfiguration.CacheDuration);
                if (!found)
                {
                    //Create:
                    cacheItem = new CacheItem
                    {
                        CachedTill = newDateTime,
                        GeoIPInfo = geoIPInfo
                    };
                    _cache[ipKey] = cacheItem;
                }
                else
                {
                    //Update:
                    cacheItem.CachedTill = newDateTime;
                    cacheItem.GeoIPInfo = geoIPInfo;
                }

                response = geoIPInfo;
                return true;
            }
        }


#pragma warning disable 1591
        public class CacheItem
        {
            public DateTimeOffset CachedTill { get; set; }
            public GeoIPInfo GeoIPInfo { get; set; }
        }
#pragma warning restore 1591

    }
}