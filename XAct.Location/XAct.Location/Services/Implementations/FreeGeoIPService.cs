using System;
using XAct.Net.Services;
using XAct.Services;

namespace XAct.Location.Services.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IGeoIPService),BindingLifetimeType.SingletonScope,Priority.Normal)]
    public class FreeGeoIPService : IFreeGeoIPService
    {
        private IJsonRequestService _jsonService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FreeGeoIPService"/> class.
        /// </summary>
        /// <param name="jsonService">The json service.</param>
        public FreeGeoIPService(IJsonRequestService jsonService)
        {
            _jsonService = jsonService;
        }

        /// <summary>
        /// Gets the Geo location information for a given IP.
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public bool GetIPInfo(string ip, out GeoIPInfo response)
        {

            FreeGeoIPResponse r = _jsonService.MakeRequest<FreeGeoIPResponse>("http://freegeoip.net/json/" + ip);

            response = new GeoIPInfo();

            if (r != null)
            {

                response.LastChecked = DateTime.UtcNow;
                response.Country = r.Country;
                response.Region = r.Region;
                response.City = r.City;
                response.PostalCode = r.Zip;
                response.Lat = r.Lat;
                response.Long = r.Long;
                response.TimeZone = r.TimeZone;
                response.ResponseStatusMessage = "success";
            }
            else
            {
                response.ResponseStatusMessage = "failure";
            }


            return true;
        }

    }
}