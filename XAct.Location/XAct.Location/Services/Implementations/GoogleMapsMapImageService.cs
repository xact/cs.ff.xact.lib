using System;
using System.Text;
using XAct.Dimensions;
using XAct.Location;

namespace XAct.Location.Services.Implementations
{
    /// <summary>
    /// An implementation of <see cref="IGoogleMapsMapImageService"/>
    /// </summary>
    public class GoogleMapsMapImageService : IGoogleMapsMapImageService
    {
        private string baseUrl = "https://maps.googleapis.com/maps/api/staticmap";

        /// <summary>
        /// Gets the URI for a static map image centered on the given coordinate.
        /// </summary>
        /// <param name="center">The center.</param>
        /// <param name="size">The size.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <param name="geoMarkers">The geo markers.</param>
        /// <returns></returns>
        public Uri GetUri(GeoCoordinates center=null, Size size=null, int zoomLevel=0, params GeoMarker[] geoMarkers)
        {
            if (size == null)
            {
                size = new Size(640, 480);
            }

            if (center == null)
            {
                center = new GeoCoordinates();
            }

            if (zoomLevel == 0)
            {
                zoomLevel = 16;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(baseUrl);

            sb.Append("?");
            sb.Append("center=");
            sb.Append(center);

            sb.Append("&");
            sb.Append("zoom=");
            sb.Append(zoomLevel);

            foreach (var marker in geoMarkers)
            {
                sb.Append(geoMarkers);
            }

            return new Uri(sb.ToString());
        }
    }
}