﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XAct.Dimensions;
using XAct.Location;

namespace XAct.Location.Services
{
    /// <summary>
    /// See: https://developers.google.com/maps/documentation/static-maps/intro
    /// https://developers.google.com/maps/documentation/geocoding/intro
    /// </summary>
    public interface IGoogleMapsMapImageService :IHasXActLibService
    {
        /// <summary>
        /// Gets the URI for a static map image centered on the given coordinate.
        /// </summary>
        /// <param name="center">The center.</param>
        /// <param name="size">The size.</param>
        /// <param name="zoomLevel">The zoom level.</param>
        /// <param name="geoMarkers">The geo markers.</param>
        /// <returns></returns>
        Uri GetUri(GeoCoordinates center=null, Size size=null, int zoomLevel=0, params GeoMarker[] geoMarkers);
    }
}
