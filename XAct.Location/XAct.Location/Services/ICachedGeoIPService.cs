using XAct.Location.Services;

namespace XAct.Location.Services
{
    /// <summary>
    /// Contract for a service to return the cached Geo location information for a given IP.
    /// <para>
    /// Cached hits the remote service less
    /// </para>
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface ICachedGeoIPService : IGeoIPService, IHasXActLibService
    {
        
    }
}