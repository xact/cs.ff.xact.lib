using System;

namespace XAct.Location.Services.Configuration.Implementations
{
    /// <summary>
    /// Implementation of <see cref="ICachedGeoIPServiceConfiguration"/>
    /// </summary>
    public class CachedGeoIpServiceConfiguration : ICachedGeoIPServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the duration to cache IP information.
        /// </summary>
        /// <value>
        /// The duration of the cache.
        /// </value>
        public TimeSpan CacheDuration { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedGeoIpServiceConfiguration"/> class.
        /// </summary>
        public CachedGeoIpServiceConfiguration()
        {
            CacheDuration = TimeSpan.FromMinutes(20);
        }

    }
}