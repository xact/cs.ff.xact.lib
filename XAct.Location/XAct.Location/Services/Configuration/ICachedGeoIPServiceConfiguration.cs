using System;

namespace XAct.Location.Services.Configuration
{
    /// <summary>
    /// Contract for a configuration object to configure
    /// instances of <see cref="ICachedGeoIPService"/>
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface ICachedGeoIPServiceConfiguration : IHasXActLibTestServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the duration to cache IP information.
        /// </summary>
        /// <value>
        /// The duration of the cache.
        /// </value>
        TimeSpan CacheDuration { get; set; }

    }
}