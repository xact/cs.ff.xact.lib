using System.Runtime.Serialization;

namespace XAct.Location.Services
{

#pragma warning disable 1591
#pragma warning restore 1591



    [DataContract]
#pragma warning disable 1591
    public class IPAPIResponse
    {

        [DataMember(Name = "status")]
        public string status { get; set; }
        [DataMember(Name = "country")]
        public string country { get; set; }
        [DataMember(Name = "regionName")]
        public string regionName { get; set; }
        [DataMember(Name = "city")]
        public string city { get; set; }
        [DataMember(Name = "zip")]
        public string zip { get; set; }
        [DataMember(Name = "lat")]
        public string lat { get; set; }
        [DataMember(Name = "lon")]
        public string lon { get; set; }

        [DataMember(Name = "timezone")]
        public string TimeZone { get; set; }

        //{
        //  "status": "success",
        //  "country": "United States",
        //  "countryCode": "US",
        //  "region": "CA",
        //  "regionName": "California",
        //  "city": "San Francisco",
        //  "zip": "94105",
        //  "lat": "37.7898",
        //  "lon": "-122.3942",
        //  "timezone": "America\/Los_Angeles",
        //  "isp": "Wikimedia Foundation",
        //  "org": "Wikimedia Foundation",
        //  "as": "AS14907 Wikimedia US network",
        //  "query": "208.80.152.201"
        //}
    }
#pragma warning restore 1591
}