using System.Runtime.Serialization;

namespace XAct.Location.Services
{
    [DataContract]
#pragma warning disable 1591
    public class FreeGeoIPResponse
    {
        [DataMember(Name = "ip")]
        public string Ip { get; set; }
        [DataMember(Name = "country_name")]
        public string Country { get; set; }
        [DataMember(Name = "region_name")]
        public string Region { get; set; }
        [DataMember(Name = "city")]
        public string City { get; set; }
        [DataMember(Name = "zip_code")]
        public string Zip { get; set; }
        [DataMember(Name = "latitude")]
        public string Lat { get; set; }
        [DataMember(Name = "longitude")]
        public string Long { get; set; }
        [DataMember(Name = "time_zone")]
        public string TimeZone { get; set; }
    }
#pragma warning restore 1591
}