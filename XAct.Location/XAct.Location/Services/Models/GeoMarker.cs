using System.Text;
using XAct.Location;

namespace XAct.Location.Services
{
    /// <summary>
    /// A set of markers to put on a static map image
    /// </summary>
    public class GeoMarker
    {
        /// <summary>
        /// Gets or sets the name of the color (red, blue, etc.).
        /// </summary>
        /// <value>
        /// The name of the color.
        /// </value>
        public string ColorName { get; set; }
        
        /// <summary>
        /// Gets or sets the optional single character to put in a label.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the coordinate at which to place the markers.
        /// </summary>
        /// <value>
        /// The coordinates.
        /// </value>
        public GeoCoordinates[] Coordinates { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var sb = ToGoogle();

            return sb.ToString();
        }

        private StringBuilder ToGoogle()
        {
            StringBuilder sb = new StringBuilder();
            if (!Label.IsNullOrEmpty())
            {
                sb.Append("label:");
                sb.Append(Label.Substring(0, 1).ToUpper());
            }
            if (!ColorName.IsNullOrEmpty())
            {
                sb.Append("color:");
                sb.Append(ColorName.ToUpper());
            }

            if (sb.Length > 0)
            {
                sb.Append("|");
            }

            foreach (GeoCoordinates geoCoordinates in Coordinates)
            {
                sb.Append("|");
                sb.Append(geoCoordinates.ToString());
            }
            return sb;
        }
    }
}