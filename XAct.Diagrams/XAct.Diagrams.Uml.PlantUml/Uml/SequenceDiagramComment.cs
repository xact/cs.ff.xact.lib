﻿namespace XAct.Diagrams.Uml
{
    /// <summary>
    /// A comment
    /// </summary>
    public class SequenceDiagramComment :ISequenceDiagramElement
    {
        /// <summary>
        /// Gets or sets the comment text.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceDiagramComment"/> class.
        /// </summary>
        /// <param name="comment">The comment.</param>
        public SequenceDiagramComment(string comment)
        {
            Comment = comment;
        }
        public override string ToString()
        {
            return "'{0}".FormatStringInvariantCulture(Comment);
        }
    }
}