﻿namespace XAct.Diagrams.Uml
{
    using System.Collections.Generic;
    using System.Text;
    using XAct;


    public class EntityDiagram
    {
        public string Type { get; set; }
        public string Title { get; set; }

        protected EntityDiagram(string type)
        {
            Type = type;
        }
        
        public List<EntityDiagramProperty> Properties
        {
            get { return _properties ?? (_properties = new List<EntityDiagramProperty>()); }
        }
        private List<EntityDiagramProperty> _properties;

        public List<EntityDiagramMethod> Methods
        {
            get { return _methods ?? (_methods = new List<EntityDiagramMethod>()); }
        }
        private List<EntityDiagramMethod> _methods;




        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("{0} \"{1}\" {{".FormatStringInvariantCulture(Type, Title).Trim());


            if (Properties.Count > 0)
            {
                stringBuilder.AppendLine(" ".Repeat(2) + ".. Properties ..");
                foreach (var property in Properties)
                {
                    stringBuilder.AppendLine("  " + property.ToString(this.Type=="interface"));
                }
            }

            if (Methods.Count > 0)
            {
                stringBuilder.AppendLine(" ".Repeat(2) + ".. Methods ..");
                foreach (var method in Methods)
                {
                    stringBuilder.AppendLine(" ".Repeat(2) + method.ToString());
                }
            }
            stringBuilder.AppendLine("}");

            return stringBuilder.ToString();
        }
    }
}