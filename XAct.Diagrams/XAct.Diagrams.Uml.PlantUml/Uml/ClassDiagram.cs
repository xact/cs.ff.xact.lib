﻿namespace XAct.Diagrams.Uml
{
    public class ClassDiagram : EntityDiagram
    {
        public ClassDiagram()
            : base("class")
        {

        }
    }

    public class AbstractClassDiagram : EntityDiagram
    {
        public AbstractClassDiagram()
            : base("abstract")
        {
        }
    }

    public class InterfaceDiagram : EntityDiagram
    {
        public InterfaceDiagram()
            : base("interface")
        {
        }
    }
}