﻿namespace XAct.Diagrams.Uml
{
    using XAct.Diagrams.Uml.Services.Configuration;

    /// <summary>
    /// Contract for a service to communicate with a remote PlantUml server.
    /// </summary>
    public interface IPlantUmlDiagramService : IHasXActLibService
    {
        /// <summary>
        /// Configuration
        /// </summary>
        IPlantUmlDiagramServiceConfiguration Configuration { get; }


        /// <summary>
        /// Serialize the given UML into a compressed base64 string
        /// ready to be sent to a PlantUml server to convert into
        /// an image.
        /// </summary>
        /// <param name="diagramText"></param>
        /// <returns></returns>
        string SerializeUml(string diagramText);

            /// <summary>
        /// Develops the url to the remote diagram image.
        /// </summary>
        /// <param name="diagramText">The diagram text.</param>
        /// <returns></returns>
        string DevelopImageUrl(string diagramText);


        /// <summary>
        /// Retrieves from the remote PlantUml server the image
        /// as a byte array.
        /// <para>
        /// The invoker must convert the byte array to a png/other format
        /// in a platform specific way -- as PCL does not provide such classes.
        /// </para>
        /// </summary>
        /// <param name="diagramText"></param>
        /// <returns></returns>
        byte[] RetrieveImageAsByteArray(string diagramText);

    }
}
