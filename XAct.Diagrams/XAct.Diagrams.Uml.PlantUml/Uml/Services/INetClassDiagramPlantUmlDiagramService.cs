﻿namespace XAct.Diagrams.Uml
{
    using System;

    public enum PackageRenderingType
    {
        None=0,
        Namespace=1,
        Assembly=2
    }

    public class RenderingStats
    {
        private Type[] _ignore;
        private Type[] _stopAt;
        public Type[] Types { get; set; }
        public PackageRenderingType ShowPackages { get; set; }
        public Type[] StopAt
        {
            get { return _stopAt??(_stopAt = new Type[0]); }
            set { _stopAt = value; }
        }

        public Type[] Ignore
        {
            get { return _ignore??(_ignore = new Type[0]); }
            set { _ignore = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingStats"/> class.
        /// </summary>
        public RenderingStats()
        {
            ShowPackages = PackageRenderingType.None;

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingStats" /> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="packageRenderingType">Type of the package rendering.</param>
        public RenderingStats(Type type, PackageRenderingType packageRenderingType = PackageRenderingType.None)
        {
            ShowPackages = packageRenderingType;
            Types = new Type[] {type};
        }

        ///// <summary>
        ///// Initializes a new instance of the <see cref="RenderingStats" /> class.
        ///// </summary>
        ///// <param name="types">The types.</param>
        //public RenderingStats(params Type[] types)
        //{
        //    ShowPackages = PackageRenderingType.None;
        //    Types = types;
        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingStats" /> class.
        /// </summary>
        /// <param name="types">The types.</param>
        /// <param name="packageRenderingType">Type of the package rendering.</param>
        /// <param name="stopAt">The stop at.</param>
        /// <param name="ignore">The ignore.</param>
        public RenderingStats(Type[] types, PackageRenderingType packageRenderingType = PackageRenderingType.None, Type[] stopAt = null, Type[] ignore = null)
        {
            ShowPackages = packageRenderingType;
            Types = types;
            StopAt = stopAt;
            Ignore = ignore;

        }

    //    /// <summary>
    //    /// Initializes a new instance of the <see cref="RenderingStats"/> class.
    //    /// </summary>
    //    /// <param name="packageRenderingType">Type of the package rendering.</param>
    //    /// <param name="types">The types.</param>
    //    public RenderingStats(PackageRenderingType packageRenderingType, params Type[] types)
    //    {
    //        ShowPackages = packageRenderingType;
    //        Types = types;
    //    }
   }

    public interface INetClassDiagramPlantUmlDiagramService :IHasXActLibService
    {
        /// <summary>
        /// Documents the type.
        /// </summary>
        /// <param name="renderingStats">The rendering stats.</param>
        /// <returns></returns>
        string DocumentType(RenderingStats renderingStats);



        /// <summary>
        /// Develops the url to the remote diagram image.
        /// </summary>
        /// <param name="renderingStats">The rendering stats.</param>
        /// <returns></returns>
        string DocumentTypeAsImageUrl(RenderingStats renderingStats);


        /// <summary>
        /// Retrieves from the remote PlantUml server the image
        /// as a byte array.
        /// <para>
        /// The invoker must convert the byte array to a png/other format
        /// in a platform specific way -- as PCL does not provide such classes.
        /// </para>
        /// </summary>
        /// <param name="renderingStats">The rendering stats.</param>
        /// <returns></returns>
        byte[] DocumentTypeAsByteArray(RenderingStats renderingStats);

    }
}