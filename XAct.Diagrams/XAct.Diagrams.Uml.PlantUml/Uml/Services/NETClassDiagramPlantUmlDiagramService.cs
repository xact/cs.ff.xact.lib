﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace XAct.Diagrams.Uml.Services.Implemtations
{
    using XAct.Collections;
    using XAct.Reflection;


    public class NetClassDiagramPlantUmlDiagramService : INetClassDiagramPlantUmlDiagramService
    {
        private readonly IPlantUmlDiagramService _plantUmlDiagramService;

        private delegate void Del2(TreeNode<string> namespaceNode);

        private delegate void Del(TreeNode<NodeTypeWrapper> TreeNode, TreeNode<NodeTypeWrapper> parentNode = null);


        /// <summary>
        /// Initializes a new instance of the <see cref="NetClassDiagramPlantUmlDiagramService" /> class.
        /// </summary>
        /// <param name="plantUmlDiagramService">The plant uml diagram service.</param>
        public NetClassDiagramPlantUmlDiagramService(IPlantUmlDiagramService plantUmlDiagramService)
        {
            _plantUmlDiagramService = plantUmlDiagramService;
        }

        /// <summary>
        /// Develops the url to the remote diagram image.
        /// </summary>
        /// <param name="renderingStats">The rendering stats.</param>
        /// <returns></returns>
        public string DocumentTypeAsImageUrl(RenderingStats renderingStats)
        {
            string diagramText = DocumentType(renderingStats);
            var result = _plantUmlDiagramService.DevelopImageUrl(diagramText);
            return result;
        }


        /// <summary>
        /// Retrieves from the remote PlantUml server the image
        /// as a byte array.
        /// <para>
        /// The invoker must convert the byte array to a png/other format
        /// in a platform specific way -- as PCL does not provide such classes.
        /// </para>
        /// </summary>
        /// <param name="renderingStats">The rendering stats.</param>
        /// <returns></returns>
        public byte[] DocumentTypeAsByteArray(RenderingStats renderingStats)
        {
            string diagramText = DocumentType(renderingStats);
            var result = _plantUmlDiagramService.RetrieveImageAsByteArray(diagramText);
            return result;
        }





        public string DocumentType(RenderingStats renderingStats)
        {

            List<TreeNode<NodeTypeWrapper>> baseTreeNodes = new List<TreeNode<NodeTypeWrapper>>();

            foreach (Type type in renderingStats.Types)
            {
                TreeNode<NodeTypeWrapper> baseTreeNode =
                    type.GetSubclassesAndInterfaces(true);

                baseTreeNodes.Add(baseTreeNode);
            }


            StringBuilder stringBuilder = new StringBuilder();
            switch (renderingStats.ShowPackages)
            {
                case PackageRenderingType.None:
                    RenderWithNoPackages(stringBuilder, baseTreeNodes, renderingStats);
                    break;
                case PackageRenderingType.Namespace:
                    RenderWithNamespacePackages(stringBuilder, baseTreeNodes, renderingStats);
                    break;
                case PackageRenderingType.Assembly:
                    RenderWithAssemblyPackages(stringBuilder, baseTreeNodes, renderingStats);
                    break;
            }

            //TreeNode<string>[] packageNames = null;
            //if (packageNames != null)
            //{

            //    foreach (var packageName in packageNames)
            //    {
            //        Del2 f = null;
            //        f = delegate(TreeNode<string> node)
            //                {
            //                    stringBuilder.AppendLine(
            //                        "{0} package {1} {{".FormatStringInvariantCulture(" ".Repeat(node.Level), node.Value));
            //                    foreach (var sn in node.Children)
            //                    {
            //                        f.Invoke(sn);
            //                    }
            //                    stringBuilder.AppendLine("}");
            //                };
            //        f.Invoke(packageName);
            //    }
            //}


            var result = stringBuilder.ToString();
            return result;

        }




        private void RenderWithNoPackages(StringBuilder stringBuilder, List<TreeNode<NodeTypeWrapper>> baseTreeNodes,
                                          RenderingStats renderingStats)
        {
            stringBuilder.AppendLine("set namespaceSeparator none");

            RenderClassesByDependencies(stringBuilder, baseTreeNodes, renderingStats);

            RenderEntityRelationships(stringBuilder, baseTreeNodes);

            var propRelationships = DeterminePropertyRelationships(baseTreeNodes);
            RenderRelationships(stringBuilder, propRelationships);
        }

        private void RenderWithNamespacePackages(StringBuilder stringBuilder,
                                                 List<TreeNode<NodeTypeWrapper>> baseTreeNodes,
                                                 RenderingStats renderingStats)
        {
            stringBuilder.AppendLine("set namespaceSeparator none");



            var packageDiagrams = SortTypesIntoPackages(baseTreeNodes, renderingStats);

            //TODO: go through relationships, and add to packageDiagrams:
            var propertyRelationships = DeterminePropertyRelationships(baseTreeNodes);

            foreach (PackageDiagram packageDiagram in packageDiagrams)
            {
                stringBuilder.AppendLine(packageDiagram.ToString());
            }

            RenderEntityRelationships(stringBuilder, baseTreeNodes);
            RenderRelationships(stringBuilder, propertyRelationships);
        }


        private void RenderWithAssemblyPackages(StringBuilder stringBuilder,
                                                List<TreeNode<NodeTypeWrapper>> baseTreeNodes,
                                                RenderingStats renderingStats)
        {

            stringBuilder.AppendLine("set namespaceSeparator none");

            var packages = DeterminePackagesFromAssemblies(baseTreeNodes, renderingStats);

            var propertyRelationshipInfos = DeterminePropertyRelationships(baseTreeNodes);

            var constructorRelationshipInfos = DetermineConstructorRelationships(baseTreeNodes);

            foreach (var relationshipInfo in propertyRelationshipInfos)
            {
                var type = relationshipInfo.TargetType;

                var package = packages.SingleOrDefault(x => x.Title == type.GetTypeInfo().Assembly.GetName().Name);
                if (package != null)
                {
                    if (package.Entities.Any(x => x.Title == type.GetName(true)))
                    {
                        continue;
                    }
                    package.Entities.Add(new ClassDiagram().CreateFrom(type));

                }
                else
                {
                    packages.Add(new PackageDiagram {Title = type.GetTypeInfo().Assembly.GetName().Name});
                }
            }

            foreach (var relationshipInfo in constructorRelationshipInfos)
            {
                var type = relationshipInfo.TargetType;

                var package = packages.SingleOrDefault(x => x.Title == type.GetTypeInfo().Assembly.GetName().Name);
                if (package != null)
                {
                    if (package.Entities.Any(x => x.Title == type.GetName(true)))
                    {
                        continue;
                    }
                    package.Entities.Add(new ClassDiagram().CreateFrom(type));

                }
                else
                {
                    packages.Add(new PackageDiagram { Title = type.GetTypeInfo().Assembly.GetName().Name });
                }
            }


            //Select(x => new PackageDiagram {Title = x.Value.Type.GetTypeInfo().Assembly.GetName().Name})
            //            .DistinctBy(x => x.Title));
            //}
            ////Reduce one last time:
            //var packages = tmpPackages.DistinctBy(x => x.Title).ToArray();



            foreach (var package in packages)
            {
                stringBuilder.Append(package);
            }

            RenderEntityRelationships(stringBuilder, baseTreeNodes);

            RenderRelationships(stringBuilder, propertyRelationshipInfos);

            RenderRelationships(stringBuilder, constructorRelationshipInfos);
        }















        private static List<PackageDiagram> SortTypesIntoPackages(List<TreeNode<NodeTypeWrapper>> baseTreeNodes,
                                                                  RenderingStats renderingStats)
        {
            //AA/BB/CC
            //AA
            //AA/DD/FF/GG
            //AA/DD/FF
            //BB
            //AA/CC
            //AA/BB/EE

            var processedTypes = new List<Type>();

            //The nested results:
            var results = new List<PackageDiagram>();
            //pointers to packages, but kept flat in order to scan more easily:
            var flatPackages = new List<PackageDiagram>();

            foreach (var baseTreeNode in baseTreeNodes)
            {

                foreach (var type in renderingStats.Ignore)
                {
                    foreach(var tmp in baseTreeNode.SelfAndDescendants.Where(x => x.Value.Type == type))
                    {
                        if (tmp.Parent != null)
                        {
                            tmp.Disconnect();
                        }
                    }
                }

                foreach (var type in renderingStats.StopAt)
                {
                    foreach(var tmp in baseTreeNode.SelfAndDescendants.Where(x => x.Value.Type == type))
                    {
                        while (tmp.Children.Any())
                        {
                            tmp.Children.Last().Disconnect();
                        }
                    }
                }



                var sortedNodes = baseTreeNode.SelfAndDescendants.OrderBy(x => x.Value.Type.Namespace.Length);

                foreach (var x in sortedNodes)
                {

                    var type = x.Value.Type;

                    if (processedTypes.Contains(type))
                    {
                        continue;
                    }

                    //Get the namespace of the source entity type:
                    var ns = type.Namespace;
                    //Break 'XAct.SubA.SubB' into 'XAct' 'SubA' 'SubB'
                    var nsParts = ns.Split('.');


                    var iMax = nsParts.Length;

                    PackageDiagram packageNode = null;

                    for (var i = iMax; i > -1; i--)
                    {

                        //Build up a Search text, from 'XAct' to 'XAct.Sub' to 'XAct.Sub.Sub'.
                        string nsSearchFor = string.Join(".", nsParts.Take(i).ToArray());
                        //Does it exist?
                        packageNode = flatPackages.SingleOrDefault(f => f.Title == nsSearchFor);

                        if (packageNode != null)
                        {
                            //found. But are we at 'XAct.SubA.SubB', or something shorter?
                            if (i < iMax)
                            {
                                //It is shorter -- add it
                                var tmp = new PackageDiagram {Title = ns};

                                flatPackages.Insert(flatPackages.IndexOf(packageNode) + 1, tmp);
                                //Insert it into it's parent.
                                //We know there's no gap, as we sorted the list by name length 
                                //before we started.
                                packageNode.Packages.Add(tmp);
                                packageNode = tmp;
                            }
                            break;
                        }
                    }
                    if (packageNode == null)
                    {
                        packageNode = new PackageDiagram {Title = ns};
                        results.Add(packageNode);
                        flatPackages.Add(packageNode);
                    }

                    //Add the Type to the package;
                    packageNode.Entities.Add(new ClassDiagram().CreateFrom(type));

                    processedTypes.Add(type);
                }
            }

            return results;
        }





        private TreeNode<string>[] DeterminePackagesFromNamespaces(TreeNode<NodeTypeWrapper> typeNode)
        {
            return null;
        }

        private List<PackageDiagram> DeterminePackagesFromAssemblies(List<TreeNode<NodeTypeWrapper>> baseTreeNodes,
                                                                 RenderingStats renderingStats)
        {


            //Create a Flat array of Packages (non-nested):




            var tmpPackages = new List<PackageDiagram>();

            foreach (var baseTreeNode in baseTreeNodes)
            {

                foreach (var type in renderingStats.Ignore)
                {
                    var tmp = baseTreeNode.SelfAndDescendants.SingleOrDefault(x => x.Value.Type == type);
                    if (tmp != null)
                    {
                        if (tmp.Parent != null)
                        {
                            tmp.Disconnect();
                        }
                    }
                }

                foreach (var type in renderingStats.StopAt)
                {
                    foreach(var typeFound in  baseTreeNode.SelfAndDescendants.Where(x => x.Value.Type == type))
                    {
                        while (typeFound.Children.Any())
                        {
                            typeFound.Children.Last().Disconnect();
                        }
                    }
                }


                tmpPackages.Add(
                    baseTreeNode
                        .SelfAndDescendants
                        .Select(x => new PackageDiagram {Title = x.Value.Type.GetTypeInfo().Assembly.GetName().Name})
                        .DistinctBy(x => x.Title));
            }
            //Reduce one last time:
            var packages = tmpPackages.DistinctBy(x => x.Title).ToList();

            var typesProcessed = new List<Type>();

            //Go through each Entity, and assign to a package
            foreach (var typeNode in baseTreeNodes)
            {
                foreach (var n in typeNode.SelfAndDescendants)
                {
                    if (!n.Value.Display)
                    {
                        continue;
                    }

                    if (typesProcessed.Contains(n.Value.Type))
                    {
                        continue;
                    }

                    var assembly = n.Value.Type.GetTypeInfo().Assembly.GetName().Name;
                    var package = packages.Single(x => x.Title == assembly);
                    package.Entities.Add(new ClassDiagram().CreateFrom(n.Value.Type));
                    typesProcessed.Add(n.Value.Type);
                }
            }



            return packages;

        }




        //This only is for Rendering when not in Namespaces or Assemblies.
        private static void RenderClassesByDependencies(StringBuilder stringBuilder,
                                                        List<TreeNode<NodeTypeWrapper>> baseTreeNodes,
                                                        RenderingStats renderingStats
            )
        {

            List<Type> processedTypes = new List<Type>();

            foreach (TreeNode<NodeTypeWrapper> baseTreeNode in baseTreeNodes)
            {

                foreach (var type in renderingStats.Ignore)
                {
                    var tmp = baseTreeNode.SelfAndDescendants.SingleOrDefault(x => x.Value.Type == type);
                    if (tmp != null)
                    {
                        if (tmp.Parent != null)
                        {
                            tmp.Disconnect();
                        }
                    }
                }

                foreach (var type in renderingStats.StopAt)
                {
                    foreach (var typeFound in baseTreeNode.SelfAndDescendants.Where(x => x.Value.Type == type))
                    {
                        while (typeFound.Children.Any())
                        {
                            typeFound.Children.Last().Disconnect();
                        }
                    }
                }


                Del fu = null;
                fu = delegate(TreeNode<NodeTypeWrapper> treeNode, TreeNode<NodeTypeWrapper> parentNode)
                         {
                             if (processedTypes.Contains(treeNode.Value.Type))
                             {
                                 return;
                             }

                             if (renderingStats.Ignore.Contains(treeNode.Value.Type))
                             {
                                 return;
                             }

                             processedTypes.Add(treeNode.Value.Type);

                             if (treeNode.Value.Display)
                             {
                                 stringBuilder.Append(new ClassDiagram().CreateFrom(treeNode.Value.Type));
                                 //Put some space between types:
                                 stringBuilder.AppendLine("");
                             }

                             if (renderingStats.StopAt.Contains(treeNode.Value.Type))
                             {
                                 return;
                             }

                             foreach (var childNode in treeNode.Children)
                             {
                                 fu(childNode, treeNode);
                             }

                         };


                //Invoke the recursive method:
                fu.Invoke(baseTreeNode, null);


                //Put some space between this BaseType and next:
                stringBuilder.AppendLine("");
                stringBuilder.AppendLine("");
                stringBuilder.AppendLine("");

            }
        }


        /// <summary>
        /// Renders the relationships.
        /// </summary>
        /// <param name="stringBuilder">The string builder.</param>
        /// <param name="baseTreeNodes">The base tree nodes.</param>
        /// <returns></returns>
        private static List<string> RenderEntityRelationships(StringBuilder stringBuilder,
                                                              IEnumerable<TreeNode<NodeTypeWrapper>> baseTreeNodes)
        {

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("'Entity Relationships:");
            stringBuilder.AppendLine("'-----------------------");

            List<string> results = new List<string>();

            foreach (var baseTreeNode in baseTreeNodes)
            {
                Del fu = null;
                fu = delegate(TreeNode<NodeTypeWrapper> treeNode, TreeNode<NodeTypeWrapper> parentNode)
                         {
                             if (parentNode != null)
                             {
                                 var parentType = parentNode.Value.Type.GetTypeInfo();
                                 string connectionType = (parentType.IsInterface)
                                                             ? "<|.."
                                                             : "<|--";

                                 var conStr = "\"{0}\" {1} \"{2}\"".FormatStringInvariantCulture(
                                     treeNode.Value.Type.GetName(true),
                                     connectionType,
                                     parentNode.Value.Type.GetName(true));

                                 if (!results.Contains(conStr))
                                 {
                                     results.Add(conStr);
                                 }
                             }

                             foreach (var childNode in treeNode.Children)
                             {
                                 fu(childNode, treeNode);
                             }
                         };


                //Invoke the recursive method:
                fu.Invoke(baseTreeNode, null);
            }

            foreach (string relationship in results)
            {
                stringBuilder.AppendLine(relationship);
            }

            return results;
        }
        public enum RelationshipType
        {
            OneToOne,
            OneToMany,
            ManyToOne,

        }

        public class DependencyRelationshipInfo : RelationshipInfo
        {
            public DependencyRelationshipInfo(Type sourceType, Type targetType)
                : base(sourceType, targetType)
            {
                TypeName = "Constructor Dependency Relationship";
                ArrowType = ">";
            }
        }

        public class PropertyRelationshipInfo : RelationshipInfo
        {
            public PropertyRelationshipInfo(Type sourceType, Type targetType)
                : base(sourceType, targetType)
            {
            TypeName = "Property Relationship";
            ArrowType = ">";
            }
        }

        public abstract class RelationshipInfo
        {
            public string TypeName = "";
                protected string LineType = "..";
                protected string ArrowType = "";

            public RelationshipType Type { get; set; }
            public bool SourceIsEnumerable { get; set; }
            public bool TargetIsEnumerable { get; set; }
            public Type SourceType { get; set; }
            public Type TargetType { get; set; }

            protected RelationshipInfo(Type sourceType, Type targetType)
            {
                SourceType = sourceType;
                TargetType = targetType;
            }

            public override string ToString()
            {
                string relationshipType = null;
                if (TargetIsEnumerable)
                {
                    relationshipType = relationshipType + "o";
                }
                relationshipType = relationshipType + LineType;

                relationshipType = relationshipType + ArrowType;
                
                var result = "\"{0}\" {1} \"{2}\"".FormatStringInvariantCulture(SourceType, relationshipType, TargetType);
                return result;
            }
        }


        private static PropertyRelationshipInfo[] DeterminePropertyRelationships(IEnumerable<TreeNode<NodeTypeWrapper>>
                                                                    baseTreeNodes)
        {
            List<PropertyRelationshipInfo> processed = new List<PropertyRelationshipInfo>();


            foreach (var baseTreeNode in baseTreeNodes)
            {
                Type srcType = baseTreeNode.Value.Type;


                foreach (PropertyInfo propertyInfo in srcType.GetProperties())
                {
                    Type targetType = propertyInfo.PropertyType;
                    bool targetIsEnumerable = targetType.IsEnumerable()|| targetType.IsArray;


                    while (targetType.IsGenericType)
                    {
                        targetType = targetType.GetGenericArguments().First();
                    }

                    
                    if (targetType.IsPrimitive)
                    {
                        continue;
                    }

                    if (targetType.FullName.StartsWith("System."))
                        
                    {
                        continue;
                    }

                    
                    if (processed.Any(x => x.SourceType == srcType && x.TargetType == targetType))
                    {
                        continue;
                    }

                    PropertyRelationshipInfo propRelationshipInfo = new PropertyRelationshipInfo(srcType, targetType);

                    //propRelationshipInfo.SourceIsEnumerable = srcType.IsEnumerable();
                    propRelationshipInfo.TargetIsEnumerable = targetIsEnumerable;// targetType.IsEnumerable();

                    processed.Add(propRelationshipInfo);

                }
            }
            return processed.ToArray();
        }






        private static void RenderRelationships(StringBuilder stringBuilder,
                                                        RelationshipInfo[] processed)
        {
            if ((processed == null)||(processed.Length == 0))
            {
                return;
            }

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("'{0}s:".FormatStringInvariantCulture(processed.First().TypeName));
            stringBuilder.AppendLine("'-----------------------");

            foreach (var p in processed)
            {
                stringBuilder.AppendLine(p.ToString());
            }
            
        }




        private static DependencyRelationshipInfo[] DetermineConstructorRelationships(IEnumerable<TreeNode<NodeTypeWrapper>>
                                                                    baseTreeNodes)
        {
            List<DependencyRelationshipInfo> processed = new List<DependencyRelationshipInfo>();


            foreach (var baseTreeNode in baseTreeNodes)
            {
                Type srcType = baseTreeNode.Value.Type;


                ConstructorInfo constructorInfo = srcType.GetConstructorWithMostArguments();
                if (constructorInfo == null)
                {
                    continue;
                }

                foreach (ParameterInfo parameterInfo in constructorInfo.GetParameters())
                {

                    Type targetType = parameterInfo.ParameterType;

                    bool targetIsEnumerable = targetType.IsEnumerable() || targetType.IsArray;


                    while (targetType.IsGenericType)
                    {
                        targetType = targetType.GetGenericArguments().First();
                    }


                    if (targetType.IsPrimitive)
                    {
                        continue;
                    }

                    if (targetType.FullName.StartsWith("System."))
                    {
                        continue;
                    }


                    if (processed.Any(x => x.SourceType == srcType && x.TargetType == targetType))
                    {
                        continue;
                    }

                    DependencyRelationshipInfo propRelationshipInfo = new DependencyRelationshipInfo(srcType, targetType);

                    //propRelationshipInfo.SourceIsEnumerable = srcType.IsEnumerable();
                    propRelationshipInfo.TargetIsEnumerable = targetIsEnumerable; // targetType.IsEnumerable();

                    processed.Add(propRelationshipInfo);

                }
            }
            return processed.ToArray();
        }
    }
}
