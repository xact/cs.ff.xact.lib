﻿namespace XAct.Diagrams.Uml
{
    using System.Collections.Generic;
    using System.Text;

    public class PackageDiagram
    {

        public string Title { get; set; }

        public List<PackageDiagram> Packages
        {
            get { return _packages ?? (_packages = new List<PackageDiagram>()); }
        }
        private List<PackageDiagram> _packages;

        public List<EntityDiagram> Entities
        {
            get { return _entities ?? (_entities = new List<EntityDiagram>()); }
        }
        private List<EntityDiagram> _entities;

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("{0} {1}{{".FormatStringInvariantCulture("package", Title));

            foreach (var entity in Entities)
            {
                stringBuilder.AppendLine(entity.ToString());
            }
            foreach (var package in Packages)
            {
                stringBuilder.AppendLine(package.ToString());
            }
            stringBuilder.AppendLine("}");
            return stringBuilder.ToString();
        }
    }
}