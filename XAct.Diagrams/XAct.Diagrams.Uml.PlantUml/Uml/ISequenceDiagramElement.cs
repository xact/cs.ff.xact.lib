﻿namespace XAct.Diagrams.Uml
{
    /// <summary>
    /// A common contract for all elements within a <see cref="SequenceDiagram"/>'s Sequences list.
    /// </summary>
    public interface ISequenceDiagramElement 
    {
    }
}