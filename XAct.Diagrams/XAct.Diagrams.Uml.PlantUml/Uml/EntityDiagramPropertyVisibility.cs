﻿namespace XAct.Diagrams.Uml
{
    public enum EntityDiagramMemberVisibility
    {
        Abstract,
        Public,
        Internal,
        Protected,
        Private,
    }
}