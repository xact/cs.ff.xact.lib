﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using XAct.Diagrams.Uml;

    public static class EntityDiagramExtensions
    {

        public static EntityDiagram CreateFrom(this EntityDiagram entityDiagram, Type type)
        {

            if (type.GetTypeInfo().IsAbstract)
            {
                entityDiagram.Type = "abstract";
            }
            if (type.GetTypeInfo().IsInterface)
            {
                entityDiagram.Type = "interface";
            }
            entityDiagram.SetTitle(type.GetName(true));

            foreach (PropertyInfo propertyInfo in type.GetTypeInfo().DeclaredProperties)
            {
                entityDiagram.AddProperty(propertyInfo);
            }
            foreach (MethodInfo methodInfo in type.GetTypeInfo().DeclaredMethods)
            {
                if (methodInfo.IsPropertyAccessor())
                {
                    continue;
                }
                entityDiagram.AddMethod(methodInfo);
            }
            return entityDiagram;
        }


        public static EntityDiagram SetTitle(this EntityDiagram entityDiagram, string title)
        {
            entityDiagram.Title = title;
            return entityDiagram;
        }

        public static EntityDiagram AddProperty(this EntityDiagram entityDiagram, PropertyInfo propertyInfo)
        {

            EntityDiagramMemberVisibility visibility =
                propertyInfo.CanRead | propertyInfo.CanWrite 
                    ? EntityDiagramMemberVisibility.Public
                          : EntityDiagramMemberVisibility.Private;

            entityDiagram.AddProperty(
                new EntityDiagramProperty(
                    visibility,
                    propertyInfo.PropertyType.GetName(),
                    propertyInfo.Name,
                    propertyInfo.CanRead,
                    propertyInfo.CanWrite));

            return entityDiagram;
        }

        public static EntityDiagram AddProperty(this EntityDiagram entityDiagram,
                                                EntityDiagramProperty entityDiagramProperty)
        {
            entityDiagram.Properties.Add(entityDiagramProperty);

            return entityDiagram;
        }

        public static EntityDiagram AddProperty(this EntityDiagram entityDiagram,
                                                EntityDiagramMemberVisibility visibility, Type propertyType,
                                                string propertyName, bool hasGetter = true, bool hasSetter = true)
        {
            entityDiagram.Properties.Add(new EntityDiagramProperty(visibility, propertyType.GetName(), propertyName,
                                                                   hasGetter, hasSetter));

            return entityDiagram;
        }

        public static EntityDiagram AddProperty(this EntityDiagram entityDiagram,
                                                EntityDiagramMemberVisibility visibility, string propertyTypeName,
                                                string propertyName, bool hasGetter = true, bool hasSetter = true)
        {
            entityDiagram.Properties.Add(new EntityDiagramProperty(visibility, propertyName, propertyName, hasGetter,
                                                                   hasSetter));

            return entityDiagram;
        }

        public static EntityDiagram AddMethod(this EntityDiagram entityDiagram, MethodInfo methodInfo)
        {

            AddMethod(entityDiagram, methodInfo.MapTo());

            return entityDiagram;
        }

        public static EntityDiagramMethod MapTo(this MethodInfo methodInfo)
        {
            EntityDiagramMemberVisibility visibility =
                methodInfo.IsPublicAbstract()
                ? EntityDiagramMemberVisibility.Abstract
                : methodInfo.IsPublic()
                    ? EntityDiagramMemberVisibility.Public
                    : methodInfo.IsInternal()
                        ? EntityDiagramMemberVisibility.Internal
                        : methodInfo.IsProtected()
                           ? EntityDiagramMemberVisibility.Protected
                           : EntityDiagramMemberVisibility.Private;

            var genericTypeNames = methodInfo.GetGenericArguments().Select(x => x.Name).ToArray();

            //var genericTypeNames = tmp.Where(x => x != null).Select(x => x.GetName()).ToArray();

            var parameterTypeNames = methodInfo
                .GetParameters()
                .Select(x => new EntityDiagramMethodArgument(x.ParameterType.GetName(), x.Name)).ToArray();

            EntityDiagramMethod entityDiagramMethod = new EntityDiagramMethod(
                visibility,
                methodInfo.ReturnType.GetName(),
                methodInfo.Name,
                genericTypeNames,
                parameterTypeNames);

            return entityDiagramMethod;


        }

        public static EntityDiagram AddMethod(this EntityDiagram entityDiagram, EntityDiagramMethod entityDiagramMethod)
        {
            entityDiagram.Methods.Add(entityDiagramMethod);

            return entityDiagram;
        }

        public static EntityDiagram AddMethod(
            this EntityDiagram entityDiagram,
            EntityDiagramMemberVisibility visibility,
            Type returnPropertyType,
            string methodName,
            IEnumerable<string> typeParams,
            IEnumerable<EntityDiagramMethodArgument> parameters)
        {
            return entityDiagram.AddMethod(visibility, returnPropertyType.GetName(), methodName, typeParams, parameters);
        }

        public static EntityDiagram AddMethod(this EntityDiagram entityDiagram,
                                              EntityDiagramMemberVisibility visibility,
                                              string returnPropertyType,
                                              string methodName,
                                              IEnumerable<string> typeParams,
                                              IEnumerable<EntityDiagramMethodArgument> parameters)
        {
            entityDiagram.Methods.Add(new EntityDiagramMethod(visibility, returnPropertyType, methodName, typeParams,
                                                              parameters));

            return entityDiagram;
        }
    }
}
