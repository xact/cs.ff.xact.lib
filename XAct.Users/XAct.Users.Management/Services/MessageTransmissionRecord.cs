//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace XAct.Users.Services
//{
 
//    /// <summary>
//    /// Entity to keep a record of Messages sent to Customers 
//    /// and other Entities.
//    /// </summary>
//    public class MessageTransmissionRecord : IHasId<int>, IHasSubjectAndBody 
//    {
//    public const int DELIVERYMETHOD_SMTP 1;

//        public int Id { get; set; }
//        public int DeliveryMethod { get; set; }
//        public string MessageId { get; set; }
//        public EmailAddress From { get; set; }
//        public EmailAddress To { get; set; }
//        public string Subject { get; set; }

//        public virtual ICollection<MessageTransmissionStatus> Statuses { get{return _statuses??(_statuses = new Collections<MessageTransmissionStatus>();)} }
//        private ICollection<MessageTransmissionStatus> _statuses;
//    }

//    public class MessageTransmissionStatus
//    {
//        public int Id { get; set; }
//        public int MessageTransmissionRecordFK { get; set; }
//    }
//}
