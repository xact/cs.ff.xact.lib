
namespace XAct.Users.Services
{
    /// <summary>
    /// Contract for a service to
    /// receive notifications from an MTA or Transactional Email service,
    /// and update the customer database.
    /// </summary>
    public interface ICustomerMessagingStatusUpdateService : IHasXActLibService
    {

        /// <summary>
        /// Message has been sent successfully.
        /// </summary>
        /// <param name="emailAddress"></param>
        void Send(EmailAddress emailAddress);


        /// <summary>
        /// Message has been sent, but the receiving server has indicated mail 
        /// is being delivered too quickly and Mandrill should slow down sending temporarily
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        void Deferral(EmailAddress emailAddress);

        /// <summary>
        /// Message has soft bounced.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        void SoftBounce(EmailAddress emailAddress);

        /// <summary>
        /// Message has hard bounced.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        void HardBounce(EmailAddress emailAddress);

        /// <summary>
        /// Recipient has opened the email.
        /// <para>
        /// Will only occur when open tracking is enabled
        /// </para>
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="ip">The recipient's ip.</param>
        /// <param name="location">The recipient's location.</param>
        void Open(EmailAddress emailAddress, string ip, string location);

        /// <summary>
        /// Recipient clicked a link in a message.
        /// <para>
        /// Will only occur when click tracking is enabled
        /// </para>
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="urlClicked">The URL clicked.</param>
        /// <param name="ip">The recipient's ip.</param>
        /// <param name="location">The recipient's location.</param>
        void Click(EmailAddress emailAddress, string urlClicked, string ip, string location);

        /// <summary>
        /// Recipient marked message as Spam.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="messageType">Type of the message.</param>
        void Spam(EmailAddress emailAddress, string messageType);

        /// <summary>
        /// Recipient has requested to be unsubscribed.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="messageType">Type of the message.</param>
        void Unsubscribe(EmailAddress emailAddress, string messageType);

        /// <summary>
        /// Message was rejected (by ?)
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="messageType"></param>
        void Reject(EmailAddress emailAddress, string messageType);



    }
}
