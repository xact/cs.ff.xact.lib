namespace XAct.Users.Services
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class EmailSummary
    {
#pragma warning disable 1591
        public string Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public EmailAddress To { get; set; }
        public EmailAddress From { get; set; }
        public string Subject { get; set; }
        public long Size { get; set; }
#pragma warning restore 1591

        //smtp_events - an array of JSON objects, each of which is an SMTP response received for the message. Each item in the array will contain the following keys:
        //    ts - the timestamp of the SMTP event
        //    type - the type of SMTP event, such as 'sent' or 'deferred'
        //    diag - the SMTP diagnostic or response message returned by the receiving server
        //    source_ip - the Mandrill IP address that was attempting to send the message
        //    destination_ip - the remote IP address of the server Mandrill was connected to for message relay

        //clicks - an array containing an item for each click recorded for the message. Each item contains the timestampe of the click and the URL that was clicked
    }
}