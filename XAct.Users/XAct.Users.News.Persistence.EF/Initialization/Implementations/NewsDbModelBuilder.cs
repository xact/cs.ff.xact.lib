namespace XAct.Users.News.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Users.News;
    using XAct.Users.News.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// 
    /// </summary>
    public class NewsDbModelBuilder : INewsDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewsDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public NewsDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<NewsCategory>)XAct.DependencyResolver.Current.GetInstance<INewsCategoryModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<NewsEntry>)XAct.DependencyResolver.Current.GetInstance<INewsEntryModelPersistenceMap>());

        }
    }
}