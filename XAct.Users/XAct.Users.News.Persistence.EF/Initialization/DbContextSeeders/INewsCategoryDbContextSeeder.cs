namespace XAct.Users.News.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// 
    /// </summary>
    public interface INewsCategoryDbContextSeeder : IHasXActLibDbContextSeeder<NewsCategory>
    {

    }
}