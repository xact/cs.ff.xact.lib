﻿namespace XAct.Users.News.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Users.News;

#pragma warning disable 1591
    public class NewsEntryDbContextSeeder : XActLibDbContextSeederBase<NewsEntry>, INewsEntryDbContextSeeder
#pragma warning restore 1591
    {
        private readonly INewsCategoryDbContextSeeder _newsEntryDbContextSeeder;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewsEntryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="newsCategoryDbContextSeeder">The news entry database context seeder.</param>
        public NewsEntryDbContextSeeder(
            INewsCategoryDbContextSeeder newsCategoryDbContextSeeder
            )
            : base(newsCategoryDbContextSeeder)
        {
            _newsEntryDbContextSeeder = newsCategoryDbContextSeeder;
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities() 
        {
        }


    }


}

