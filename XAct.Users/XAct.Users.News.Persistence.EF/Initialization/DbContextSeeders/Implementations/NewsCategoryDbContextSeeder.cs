﻿namespace XAct.Users.News.Initialization.DbContextSeeders.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Users.News;

#pragma warning disable 1591
    public class NewsCategoryDbContextSeeder : XActLibDbContextSeederBase<NewsCategory>, INewsCategoryDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewsEntryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public NewsCategoryDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _environmentService = environmentService;
        }

        /// <summary>
        /// Seeds the specified database context.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public override void SeedInternal(DbContext dbContext) 
        {
            //No Seeding required
            //MakeNewsCategoryDefinitions(dbContext);

            this.SeedInternalHelper(dbContext,false);

        }

        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities() 
        {

            this.InternalEntities = new List<NewsCategory>();

            this.InternalEntities.Add(
            new NewsCategory
                {
                    ResourceFilter = string.Empty,
                    Text = "Example",
                    Description = "Example Category Description..."
                }
                );


        }


    }


}

