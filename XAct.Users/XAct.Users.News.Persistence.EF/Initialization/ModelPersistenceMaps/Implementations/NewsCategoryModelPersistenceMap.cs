namespace XAct.Users.News.Initialization.ModelPersistenceMaps.Implementations
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using XAct.Data;
    using XAct.Users.News;

    /// <summary>
    /// Defines a EF Map on how to persist a <see cref="NewsCategory" /> into the database.
    /// </summary>
    public class NewsCategoryModelPersistenceMap : ReferenceDataPersistenceMapBase<NewsCategory,Guid>, INewsCategoryModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public NewsCategoryModelPersistenceMap():base("NewsCategory", DatabaseGeneratedOption.None)
        {
        }
    }
}