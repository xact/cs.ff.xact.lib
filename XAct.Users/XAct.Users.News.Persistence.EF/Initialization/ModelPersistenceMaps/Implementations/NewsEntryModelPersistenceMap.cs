namespace XAct.Users.News.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Defines a EF Map on how to persist a <see cref="News"/> into the database.
    /// </summary>
    public class NewsEntryModelPersistenceMap : EntityTypeConfiguration<XAct.Users.News.NewsEntry>, INewsEntryModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public NewsEntryModelPersistenceMap()
        {

            this.ToXActLibTable("NewsEntry");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;
            
            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);



            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);
            this
                .Property(m => m.Order)
                .DefineRequiredOrder(colOrder++)
                ;

            this
                .Property(m => m.ApplicationTennantId)
                .DefineRequiredApplicationTennantId(colOrder++);

            this
                .Property(m => m.StartDateTimeUtc)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.EndDateTimeUtc)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Subject)
                .IsRequired()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Body)
                .IsRequired()
                .IsMaxLength() //OK to be MaxLength. 4000 (50lx80c) would be too small.
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++)
                ;
            this
                .Property(m => m.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++)
                ;
            this
                .Property(m => m.LastModifiedBy)
                .DefineRequired64CharLastModifiedBy(colOrder++);
                ;
            this
                .Property(m => m.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++)
                ;
            this
                .Property(m => m.Tag)
                .DefineOptional256CharTag(colOrder++)
                ;


            //Relationships
            this
                .HasRequired(m => m.Category)
                .WithMany()
                .HasForeignKey(m => m.CategoryFK);

        }
    }
}