namespace XAct.Users.Tips.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Users.Tips.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// 
    /// </summary>
    public class TipDbModelBuilder : ITipDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TipDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public TipDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<TipDefinition>)XAct.DependencyResolver.Current.GetInstance<ITipDefinitionModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<TipUserSettings>)XAct.DependencyResolver.Current.GetInstance<ITipUserSettingsModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<TipUserViewMetrics>)XAct.DependencyResolver.Current.GetInstance<ITipUserViewMetricsModelPersistenceMap>());

        }
    }
}