namespace XAct.Users.Tips.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Defines a EF Map on how to persist a <see cref="TipDefinition" /> into the database.
    /// </summary>
    public class TipUserSettingsModelPersistenceMap : EntityTypeConfiguration<TipUserSettings>, ITipUserSettingsModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public TipUserSettingsModelPersistenceMap()
        {
            this.ToXActLibTable("TipUserSettings");


            this
                .HasKey(m => m.UserIdentifier);

            int colOrder = 0;

            this
                .Property(m => m.UserIdentifier)
                .DefineRequired64CharUserIdentifier(colOrder++);
            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);
            this
                .Property(m => m.Difficulty)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.NumberOfTimesToViewTips)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
        }
    }
}