namespace XAct.Users.Tips.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Defines a EF Map on how to persist a <see cref="TipDefinition"/> into the database.
    /// </summary>
    public class TipDefinitionModelPersistenceMap : EntityTypeConfiguration<TipDefinition>, ITipDefinitionModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public TipDefinitionModelPersistenceMap()
        {

            this.ToXActLibTable("TipDefinition");


            this
                .HasKey(m => new {m.Key});


            int colOrder = 0;
            
            //No need for a tennant identifier.
            
            this
                .Property(m => m.Key)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++)
                ;
            this
                .Property(m => m.Difficulty)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Title)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.Text)
                //(50lines x 80chars) Should be sufficient. Any longer and you're waffling. They want Help Not Tolstoy. 
                .DefineOptional4000CharText(colOrder++)
                ;
            this
                .Property(m => m.ResourceFilter)
                .DefineOptional256CharResourceFilter(colOrder++)
                ;
            this
                .Property(m => m.Tag)
                .DefineOptional256CharTag(colOrder++)
                ;
            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;
        }
    }
}