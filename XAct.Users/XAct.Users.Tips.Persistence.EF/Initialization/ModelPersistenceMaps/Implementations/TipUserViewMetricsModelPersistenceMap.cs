namespace XAct.Users.Tips.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Defines a EF Map on how to persist a <see cref="TipDefinition"/> into the database.
    /// </summary>
    public class TipUserViewMetricsModelPersistenceMap : EntityTypeConfiguration<TipUserViewMetrics>, ITipUserViewMetricsModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public TipUserViewMetricsModelPersistenceMap()
        {

            this.ToXActLibTable("TipUserViewMetrics");

            this
                .HasKey(m => new {m.UserIdentifier, m.Key});


            int colOrder = 0;
            int indexMember = 1;

            this
                .Property(m => m.ApplicationTennantId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationIdUserIdKey", indexMember++) { IsUnique = true }))
                ;
            this
                .Property(m => m.UserIdentifier)
                .HasMaxLength(1024)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationIdUserIdKey", indexMember++) { IsUnique = true }))
                ;
            
            this
                .Property(m => m.Key)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_ApplicationIdUserIdKey", indexMember++) { IsUnique = true }))
                ;
            this
                .Property(m => m.NumberOfTimesViewed)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;
            this
                .Property(m => m.LastAccessedOnUtc)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;



        }
    }
}

