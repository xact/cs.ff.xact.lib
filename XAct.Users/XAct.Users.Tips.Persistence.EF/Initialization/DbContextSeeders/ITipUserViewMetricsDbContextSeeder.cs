namespace XAct.Users.Tips.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Tip}"/>
    /// to seed the Tip tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface ITipUserViewMetricsDbContextSeeder : IHasXActLibDbContextSeeder<TipUserViewMetrics>
    {

    }

    //public interface ITipUserDbContextSeeder : IHasXActLibDbContextSeeder<TipUser>
    //{

    //}
    //public interface ITipUserViewDbContextSeeder : IHasXActLibDbContextSeeder<TipUserView>
    //{

    //}
}