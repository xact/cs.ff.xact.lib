namespace XAct.Users.Tips.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// 
    /// </summary>
    public interface ITipUserSettingsDbContextSeeder : IHasXActLibDbContextSeeder<TipUserSettings>
    {

    }
}