namespace XAct.Users.Tips.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

#pragma warning disable 1591
    public class TipDefinitionDbContextSeeder : XActLibDbContextSeederBase<TipDefinition>, ITipDefinitionDbContextSeeder
#pragma warning restore 1591
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipUserViewMetricsDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public TipDefinitionDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }

#pragma warning disable 1591
#pragma warning disable 1591
}