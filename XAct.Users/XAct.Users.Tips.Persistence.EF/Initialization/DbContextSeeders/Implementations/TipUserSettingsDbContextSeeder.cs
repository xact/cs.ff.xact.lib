namespace XAct.Users.Tips.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class TipUserSettingsDbContextSeeder : XActLibDbContextSeederBase<TipUserSettings>, ITipUserSettingsDbContextSeeder
#pragma warning restore 1591
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipUserSettingsDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public TipUserSettingsDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}