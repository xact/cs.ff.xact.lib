namespace XAct.Users.Tips.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class TipUserViewMetricsDbContextSeeder : XActLibDbContextSeederBase<TipUserViewMetrics>, ITipUserViewMetricsDbContextSeeder
#pragma warning restore 1591
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipUserViewMetricsDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public TipUserViewMetricsDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}