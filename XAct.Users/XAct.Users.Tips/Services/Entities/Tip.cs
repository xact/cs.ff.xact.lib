
namespace XAct.Users.Tips
{
    using System.Runtime.Serialization;

    /// <summary>
    /// A Message containing a culture specific tip.
    /// <para>
    /// Note that this is not an entity, and is not persisted to a datastore
    /// (see <see cref="TipDefinition"/>
    /// </para> 
    /// </summary>
    [DataContract]
    public class Tip
    {


        /// <summary>
        /// Gets the difficulty of the <see cref="Tip"/>.
        /// </summary>
        public Difficulty Difficulty { get { return _difficulty; }}
        [DataMember]
        private Difficulty _difficulty;

        /// <summary>
        /// Gets the culture specific title of the <see cref="Tip"/>.
        /// </summary>
        public string Title { get { return _title; }}
        [DataMember]
        private string _title;

        /// <summary>
        /// Gets the culture specific body/message of the <see cref="Tip"/>.
        /// </summary>
        public string Body { get { return _body; }}
        [DataMember]
        private string _body;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tip"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="body">The body.</param>
        /// <param name="difficulty">The difficulty.</param>
        public Tip(string title, string body, Difficulty difficulty)
        {
            _title = title;
            _body = body;
            _difficulty = difficulty;
        }

    }
}
