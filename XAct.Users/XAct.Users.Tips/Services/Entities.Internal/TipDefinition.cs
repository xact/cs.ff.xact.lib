namespace XAct.Users.Tips
{
    using System;
    using System.Runtime.Serialization;
    using XAct;

    /// <summary>
    /// An internal entity managed by <see cref="ITipService" />,
    /// in order to deliver <see cref="Tip" />s to end users.
    /// <para>
    /// Note that it is is intended to contain only the ResourceFilter and ResourceKey,
    /// but it is understood that in early stages of development, it's easier to
    /// embed values directly, to be moved and localized later.
    /// </para>
    /// </summary>
    [DataContract]
    public class TipDefinition :
        IHasXActLibEntity, 
        IHasDistributedGuidId,
        IHasApplicationTennantId, 
        IHasKey, 
        IHasEnabled, 
        IHasResourceFilterReadOnly,
        IHasTextAndTitle,
        IHasFilter, 
        IHasTag, IHasDescription
    {

        /// <summary>
        /// Gets or sets the global identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }

        /// <summary>
        /// The unique name of the Tip
        /// <para>
        /// Part of the Entity's composite key.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Key { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether this tip is enabled/displayed.
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the difficulty of the <see cref="Tip"/>.
        /// </summary>
        /// <value>
        /// The difficulty.
        /// </value>
        [DataMember]
        public virtual XAct.Difficulty Difficulty { get; set; }





        /// <summary>
        /// Gets or sets the filter/group of the resource 
        /// containing the text of the tip.
        /// </summary>
        /// <value>
        /// The resource filter.
        /// </value>
        [DataMember]
        public virtual string ResourceFilter { get; set; }

        /// <summary>
        /// Gets the title.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// <para>
        /// Member defined in the <see cref="IHasTitleReadOnly" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        [DataMember]
        public virtual string Title { get; set; }


        /// <summary>
        /// Gets or sets the text.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        [DataMember]
        public virtual string Text { get; set; }




        /// <summary>
        /// Gets the string that can be used as filter.
        /// <para>
        /// By convention the syntax is similar to CSV,
        /// but with ! for NOT clauses, &amp; for AND, etc:
        /// <example>
        /// <![CDATA[
        /// AA;!BB;CC&DD;CC&!DD
        /// ]]>
        /// </example>
        /// </para>
        /// <para>Member defined in<see cref="IHasFilter" /></para>
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
        [DataMember]
        public virtual string Filter { get; set; }



        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Tag { get; set; }


        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly"/></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="TipDefinition"/> class.
        /// </summary>
        public TipDefinition()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }

    }
}