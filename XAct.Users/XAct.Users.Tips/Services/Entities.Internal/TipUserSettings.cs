// ReSharper disable CheckNamespace
namespace XAct.Users.Tips
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Statistics for the current user,
    /// in order to select the most appropriate tips
    /// to display to the user.
    /// </summary>
    [DataContract]
    public class TipUserSettings : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasUserIdentifier, IHasEnabled
    {

        /// <summary>
        /// Gets or sets the unique global identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the application tennant identifier.
        /// </summary>
        /// <value>
        /// The application tennant identifier.
        /// </value>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }




        /// <summary>
        /// Gets or sets the user identifier.
        /// <para>
        /// Member defined in the <see cref="IHasUserIdentifier" /> contract.
        /// </para>
        /// <para>
        /// Part of the entity's composite Key.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string UserIdentifier { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets the number of times to view tips
        /// before moving on to next tip range.
        /// </summary>
        /// <value>
        /// The number of times to view tips.
        /// </value>
        [DataMember]
        public virtual int NumberOfTimesToViewTips { get; set; }


        /// <summary>
        /// The User's general level
        /// (used to filter Tips appropriate to the user).
        /// </summary>
        [DataMember]
        public virtual Difficulty Difficulty { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="TipUserSettings"/> class.
        /// </summary>
        public TipUserSettings()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TipUserSettings"/> class.
        /// </summary>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        public TipUserSettings(Guid applicationTennantId, string userIdentifier)
        {
            this.GenerateDistributedId();
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            UserIdentifier = userIdentifier;
            ApplicationTennantId = applicationTennantId;
            Difficulty = Difficulty.Beginner;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
    }
}
