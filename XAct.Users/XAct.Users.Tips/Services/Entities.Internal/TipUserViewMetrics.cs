using System;

namespace XAct.Users.Tips
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Entity to keep track of number of times a user has seen a tip.
    /// </summary>
    [DataContract]
    public class TipUserViewMetrics : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasUserIdentifier, IHasApplicationTennantId, IHasKey, IHasDateTimeAccessedOnUtc
    {

        /// <summary>
        /// Gets or sets the unique global identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// Gets or sets the user identifier.
        /// <para>
        /// Member defined in the <see cref="IHasUserIdentifier" /> contract.
        /// </para>
        /// <para>
        /// Part of the entity's composite key.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string UserIdentifier { get; set; }

        
        /// <summary>
        /// Gets or sets the application tennant identifier.
        /// </summary>
        /// <value>
        /// The application tennant identifier.
        /// </value>
        public virtual Guid ApplicationTennantId { get; set; }

        /// <summary>
        /// Gets or sets the tip key.
        /// <para>
        /// Part of the entity's composite key.
        /// </para>
        /// <para>
        /// Clearly state Module the Tip is for, eg: 'Core/UI/UseXYZ', and 'CustomModule/ImportExport/Something'</para>
        /// </summary>
        [DataMember]
        public virtual string Key { get; set; }

        /// <summary>
        /// Gets or sets the date the <see cref="Tip"/> 
        /// was last accessed by the current user, 
        /// expressed in UTC.
        /// </summary>
        [DataMember]
        public virtual DateTime? LastAccessedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the number of times the <see cref="Tip"/> has been viewed.
        /// </summary>
        [DataMember]
        public virtual int NumberOfTimesViewed { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="TipUserViewMetrics"/> class.
        /// </summary>
        public TipUserViewMetrics()
        {
            this.GenerateDistributedId();
            //this.Enabled = true;
        }
    }
}
