namespace XAct.Users.Tips
{
    using System.Globalization;

    /// <summary>
    /// Contract for a service 
    /// to manage the display of tips to users,
    /// helping them understand how to use an application.
    /// </summary>
    public interface ITipService : IHasXActLibService
    {
        /// <summary>
        /// Gets a tip to display, randomly.
        /// </summary>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="minDifficulty">The minimum difficulty.</param>
        /// <param name="maxDifficulty">The maximum difficulty.</param>
        /// <returns></returns>
        Tip GetRandomTip(Difficulty minDifficulty, Difficulty maxDifficulty = Difficulty.Undefined, CultureInfo cultureInfo=null);

        /// <summary>
        /// Gets a tip to display, based on what's been previously shown to the user.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <returns></returns>
        Tip GetNextRandomTipForUser(string userIdentifier, CultureInfo cultureInfo = null);

    }
}
