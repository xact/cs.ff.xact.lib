namespace XAct.Users.Tips
{
    using System.Globalization;

    /// <summary>
    /// Contract for a service 
    /// to manage 
    /// the creation, disabling, removal of Tips.
    /// </summary>
    public interface ITipManagementService : IHasXActLibService
    {

        /// <summary>
        /// Gets or sets the singleton configuration settings used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        ITipManagementServiceConfiguration Configuration { get; }

        /// <summary>
        /// Gets a tip to display, randomly selected from a subset of tips 
        /// within the specified difficulty range.
        /// 
        /// </summary>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="minDifficulty">The minimum difficulty.</param>
        /// <param name="maxDifficulty">The maximum difficulty.</param>
        /// <returns></returns>
        Tip GetRandomTip(Difficulty minDifficulty = Difficulty.Undefined, Difficulty maxDifficulty = Difficulty.Undefined, CultureInfo cultureInfo = null);

        /// <summary>
        /// Gets a tip to display, based on what's been previously shown to the user.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <returns></returns>
        Tip GetNextRandomTipForUser(string userIdentifier, CultureInfo cultureInfo = null);
    }
}
