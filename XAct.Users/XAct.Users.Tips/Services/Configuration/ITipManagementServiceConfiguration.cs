namespace XAct.Users.Tips
{
    /// <summary>
    /// Contract for a singleton configuration package
    /// injected during startup into an implementation
    /// of <see cref="ITipManagementService"/>.
    /// </summary>
    public interface ITipManagementServiceConfiguration : IHasXActLibServiceConfiguration, IHasApplicationTennantId
    {

        /// <summary>
        /// Gets or sets the caching key prefix used to persist a single
        /// person's user settings.
        /// <para>
        /// Saves a couple of hits to the database.
        /// </para>
        /// <para>
        /// The default will be something like "XAct:TipManagementService:UserSettings:"
        /// </para>
        /// </summary>
        /// <value>
        /// The caching key prefix.
        /// </value>
        string CachingKeyPrefix { get; set; }


        /// <summary>
        /// Gets or sets the default maximum difficulty to set for new users.
        /// </summary>
        Difficulty DefaultDifficultyForNewUsers { get; set; }

        /// <summary>
        /// Gets or sets the default max number of views to to show a tip.
        /// </summary>
        int DefaultNumberOfTimesToViewTipsForNewUsers { get; set; }



    }

}