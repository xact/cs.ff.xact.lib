namespace XAct.Users.Tips.Implementations
{
    using System;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="ITipManagementServiceConfiguration"/> 
    /// for a singleton configuration package
    /// injected during startup into an implementation
    /// of <see cref="ITipManagementService"/>.
    /// </summary>
    public class TipManagementServiceConfiguration : ITipManagementServiceConfiguration, IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets or sets the caching key prefix used to cache user settings for a single user.
        /// <para>
        /// The default will be something like "XAct:TipManagementService:UserSettings:"
        /// </para>
        /// </summary>
        public string CachingKeyPrefix { get; set; }

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        public Guid ApplicationTennantId { get; set; }


        /// <summary>
        /// Gets or sets the default maximum difficulty to set for new users.
        /// </summary>
        public Difficulty DefaultDifficultyForNewUsers { get; set; }

        /// <summary>
        /// Gets or sets the default max number of views to to show a tip.
        /// </summary>
        public int DefaultNumberOfTimesToViewTipsForNewUsers { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TipManagementServiceConfiguration" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        public TipManagementServiceConfiguration(IEnvironmentService environmentService)
        {

            DefaultDifficultyForNewUsers = Difficulty.Beginner;
            DefaultNumberOfTimesToViewTipsForNewUsers = 2;

            CachingKeyPrefix = XAct.Library.Settings.Caching.XActLibPrefix + "TipManagementService:";
        }
    }
}