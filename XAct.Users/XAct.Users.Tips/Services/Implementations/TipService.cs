namespace XAct.Users.Tips
{
    using System.Linq;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="ITipService"/>
    /// to manage the display of tips to users,
    /// helping them understand how to use an application.
    /// </summary>
    public class TipService : ITipService
    {


        private readonly ITipManagementService _tipManagementService;


        /// <summary>
        /// Initializes a new instance of the <see cref="TipService"/> class.
        /// </summary>
        /// <param name="tipManagementService">The tip management service.</param>
        public TipService(ITipManagementService tipManagementService)
        {
            _tipManagementService = tipManagementService;
        }

        /// <summary>
        /// Gets a tip to display, randomly.
        /// </summary>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="minDifficulty">The minimum difficulty.</param>
        /// <param name="maxDifficulty">The maximum difficulty.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Tip GetRandomTip(Difficulty minDifficulty, Difficulty maxDifficulty = Difficulty.Undefined, System.Globalization.CultureInfo cultureInfo = null)
        {
            return _tipManagementService.GetRandomTip(minDifficulty, maxDifficulty, cultureInfo);
        }


        /// <summary>
        /// Gets a tip to display, based on what's been previously shown to the user.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Tip GetNextRandomTipForUser(string userIdentifier,  System.Globalization.CultureInfo cultureInfo=null)
        {
            return _tipManagementService.GetNextRandomTipForUser(userIdentifier, cultureInfo);
        }
        



    }
}