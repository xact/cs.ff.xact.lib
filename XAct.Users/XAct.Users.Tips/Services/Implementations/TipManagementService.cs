namespace XAct.Users.Tips
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using XAct.Caching;
    using XAct.Domain.Repositories;
    using XAct.Domain.Repositories.Implementations;
    using XAct.Environment;
    using XAct.Resources;
    using XAct.Services;


    /// <summary>
    /// An implementation of the <see cref="ITipManagementService" />
    /// to manage
    /// the creation, disabling, removal of Tips.
    /// <para>
    /// Invoked by an implementation of <see cref="ITipService" />
    /// </para>
    /// </summary>
    /// <internal>
    /// We're talking tips here. No program has a ton of them.
    /// So it's reasonable to load the data in one go, and cache it by 
    /// Application, and then filter through these tips for those that are of the right 
    /// difficulty.
    /// </internal>
    /// <internal>
    /// Used <see cref="ICachingService"/> rather than a custom cache collection
    /// so that it release memory if application is not being used,
    /// while being self managing in terms of knowing how to reload the
    /// data as needed. 
    /// </internal>
    public class TipManagementService : ITipManagementService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;
        private readonly IClientEnvironmentService _clientEnvironmentService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IRepositoryService _repositoryService;
        private readonly IResourceService _resourceService;
        private readonly ICachingService _hostBasedCachingService;

        private readonly ITipManagementServiceConfiguration _tipManagementServiceConfiguration;

        private readonly string _cacheKeyPrefix = XAct.Library.Settings.Caching.XActLibPrefix + "TipManagementService:";

        private readonly string _cachedTipDefinitionCacheKey = XAct.Library.Settings.Caching.XActLibPrefix + "TipManagementService:" + ":TipDefinitions";

        private readonly string _cacheKeyTipUserSettingsPrefix = XAct.Library.Settings.Caching.XActLibPrefix +
                                                                 "TipManagementService:" + ":TipUserSettings:";

        /// <summary>
        /// Gets or sets the singleton configuration settings used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public ITipManagementServiceConfiguration Configuration
        {
            get { return _tipManagementServiceConfiguration; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="TipService" /> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="clientEnvironmentService">The client environment service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="hostBasedCachingService">The caching service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="resourceService">The resource service.</param>
        /// <param name="tipManagementServiceConfiguration">The tip management service configuration.</param>
        public TipManagementService(
            IDateTimeService dateTimeService,
            IEnvironmentService environmentService,
            IClientEnvironmentService clientEnvironmentService,
            IApplicationTennantService applicationTennantService,
            XAct.Caching.ICachingService hostBasedCachingService,
            //IResourceService resourceService,
            IRepositoryService repositoryService,
            IResourceService resourceService,
            ITipManagementServiceConfiguration tipManagementServiceConfiguration)
        {
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
            _clientEnvironmentService = clientEnvironmentService;
            _applicationTennantService = applicationTennantService;
            _repositoryService = repositoryService;
            _resourceService = resourceService;
            _hostBasedCachingService = hostBasedCachingService;
            _tipManagementServiceConfiguration = tipManagementServiceConfiguration;

            _cacheKeyPrefix = Configuration.CachingKeyPrefix;
            _cacheKeyTipUserSettingsPrefix = _cacheKeyPrefix + "TipUserSettings:";
            _cachedTipDefinitionCacheKey = _cacheKeyPrefix + "TipDefinitions";
        }

        /// <summary>
        /// Gets a tip to display, randomly selected from a subset of tips 
        /// within the specified difficulty range.
        /// <para>
        /// Retrieved from a cached set.
        /// </para>
        /// </summary>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="minDifficulty">The minimum difficulty.</param>
        /// <param name="maxDifficulty">The maximum difficulty.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Tip GetRandomTip(Difficulty minDifficulty = Difficulty.Beginner,
                                Difficulty maxDifficulty = Difficulty.Undefined,
                                System.Globalization.CultureInfo cultureInfo = null)
        {
            if (minDifficulty == Difficulty.Undefined)
            {
                minDifficulty = Difficulty.Beginner;
            }

            if (maxDifficulty == Difficulty.Undefined)
            {
                maxDifficulty = minDifficulty;
            }

            //How many do we have to select from?
            var tipDefinitions = GetFilteredCachedTipsCommonToAllUsers(minDifficulty, maxDifficulty);

            //Determine index a random TipDefinition within that group:
            int index = _environmentService.Random.Next(tipDefinitions.Count());

            //Get the tipdefinition:
            TipDefinition serializedTip = tipDefinitions.Skip(index).Take(1).First();
            
            //Localize it:
            return ConvertToTip(serializedTip,cultureInfo);
        }


        /// <summary>
        /// Gets a tip to display, based on what's been previously shown to the user.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Tip GetNextRandomTipForUser(string userIdentifier,
                              System.Globalization.CultureInfo cultureInfo = null)
        {
            Guid applicationTennantId = _applicationTennantService.Get();
            //Pick up settings for the current user.
            //As it doesn't change very often, we can afford to cache it:
            TipManagementServiceCacheObject tipManagementServiceCacheObject;

            _hostBasedCachingService.TryGet<TipManagementServiceCacheObject>(_cacheKeyTipUserSettingsPrefix + userIdentifier, out tipManagementServiceCacheObject,
                                         () =>
                                         InitializeUserSettingsRecordAndEnsureRecordsAreBuilt(applicationTennantId,
                                                                                              userIdentifier),
                                         XAct.Library.Settings.Caching.DefaultMediumCachingTimeSpan
                );


            if (!tipManagementServiceCacheObject.tipUserSettings.Enabled)
            {
                //We won't return anything,
                //and we won't update any counters, so if user re-enables, 
                //counters havn't raced through everything so that no tips can be seen.
                return null;
            }

            //Exists, is enabled, so get filtered set of tips:

            TipDefinition[] tipDefinitions =
                GetFilteredCachedTipsCommonToAllUsers(tipManagementServiceCacheObject.tipUserSettings.Difficulty, tipManagementServiceCacheObject.tipUserSettings.Difficulty).ToArray();

            //Hum....
            if (tipDefinitions.Length == 0)
            {
                //There are no tips for this level.
                //Need to increment the user's level
                //to cast the web wider.
                //But we leave the lower level, so that if any 
                //simple tips are added later, the user won't miss out on them.
                //unless he manually states he's better than that.
                if (!IncrementMaxDifficultyOfUser(tipManagementServiceCacheObject.tipUserSettings.ApplicationTennantId,tipManagementServiceCacheObject.tipUserSettings.UserIdentifier))
                {

                    _hostBasedCachingService.Clear(_cacheKeyTipUserSettingsPrefix + userIdentifier);

                    //Note: if no change was made (ie, user is already a guru, nothing more to teach)
                    //we get out early:
                    return null;
                }

                //Recurse and try again:
                var result = GetNextRandomTipForUser(userIdentifier, cultureInfo);
                return result;
            }

            //We have some TipDefinitions.
            //Lets' get prepared and extract Keys to Tips within User's range.
            //Get them to use as an IN clause for SQL later on:
            string[] tipDefinitionKeyNames = tipDefinitions.Select(y => y.Key).ToArray();

            //We want *one* TipUserViewMetric  with a key equal to one of the TipDefinitions we will be showing:
            TipUserViewMetrics tipUserViewMetrics;

            int allowedCount = tipManagementServiceCacheObject.tipUserSettings.NumberOfTimesToViewTips;

            if (allowedCount < 1)
            {
                tipUserViewMetrics =
                    _repositoryService.GetByFilter<TipUserViewMetrics>(
                        x =>
                        x.ApplicationTennantId == applicationTennantId
                        &&
                        x.UserIdentifier == userIdentifier
                        &&
                        tipDefinitionKeyNames.Contains(x.Key)
                        )
                        .OrderBy(x => x.NumberOfTimesViewed)
                        .FirstOrDefault();
            }
            else
            {
                tipUserViewMetrics =
                    _repositoryService.GetByFilter<TipUserViewMetrics>(
                        x =>
                        x.ApplicationTennantId == applicationTennantId
                        &&
                        x.UserIdentifier == userIdentifier
                        &&
                        tipDefinitionKeyNames.Contains(x.Key)
                        &&
                        x.NumberOfTimesViewed <= allowedCount
                        )
                                      .OrderBy(x => x.NumberOfTimesViewed)
                                      .FirstOrDefault();
            }

            if (tipUserViewMetrics == null)
            {
                //It's posible to have none that come back because all counters are past the min count.
                //In which case we want to boost up the user setting:
                if (!IncrementMaxDifficultyOfUser(applicationTennantId, userIdentifier))
                {
                    //But it appears already to be a guru. 
                    //And as guru just returned no tips, we're done.
                    return null;
                }

                //and then recurse in order to try again:
                var result = GetNextRandomTipForUser(userIdentifier, cultureInfo);
                return result;
            }


            //Found one:

            TipDefinition tipDefinitionToShow =
                (tipUserViewMetrics != null)
                    ? tipDefinitions.SingleOrDefault(x => x.Key == tipUserViewMetrics.Key)
                    : null;

            if (tipDefinitionToShow == null)
            {
                Debug.Assert(false, "No TipDefinition found. Surprised as all should have just been created and committed!");
                return null;
            }


            //Before giving it back, on the specific user metric record, 
            //update counters and date as to when last shown:
            tipUserViewMetrics.LastAccessedOnUtc = _dateTimeService.NowUTC;
            tipUserViewMetrics.NumberOfTimesViewed += 1;
            _repositoryService.UpdateOnCommit(tipUserViewMetrics);

            Tip tipToShow = ConvertToTip(tipDefinitionToShow);
            return tipToShow;
        }

        private TipUserSettings GetUserSettingsRecord(Guid applicationTennantId, string userIdentifier)
        {
            TipUserSettings tipUserStatistics =
                _repositoryService
                    .GetSingle<TipUserSettings>(
                        x =>
                        (
                        (x.ApplicationTennantId == applicationTennantId) &&
                        (x.UserIdentifier == userIdentifier)
                        )
                        );


            //which is how we'll know if he wants to see tips or not,
            //and if so, at which level.

            if (tipUserStatistics == null)
            {
                tipUserStatistics = CreateUserSettingsRecord(applicationTennantId, userIdentifier);
            }
            return tipUserStatistics;
        }

        private TipManagementServiceCacheObject InitializeUserSettingsRecordAndEnsureRecordsAreBuilt(
                                                                          Guid applicationTennantId,
                                                                          string userIdentifier)
        {
            TipUserSettings tipUserSettings = GetUserSettingsRecord( applicationTennantId,
                                                                    userIdentifier);


            TipUserViewMetrics justCreated =
                GetTipUserViewMetricsCreatingNewOnesIfTheyDontExist(applicationTennantId,
                                                                    userIdentifier);


            return new TipManagementServiceCacheObject(){
                tipUserSettings = tipUserSettings, JustCreatedViewMetrics = justCreated};
        }

        private TipUserSettings CreateUserSettingsRecord(Guid applicationTennantId, string userIdentifier)
        {
            TipUserSettings tipUserStatistics = new TipUserSettings(applicationTennantId, userIdentifier);

            tipUserStatistics.Difficulty = _tipManagementServiceConfiguration.DefaultDifficultyForNewUsers;
            tipUserStatistics.NumberOfTimesToViewTips =
                _tipManagementServiceConfiguration.DefaultNumberOfTimesToViewTipsForNewUsers;
            tipUserStatistics.Enabled = true;

            _repositoryService.AddOnCommit(tipUserStatistics);
            return tipUserStatistics;
        }


        /// <summary>
        /// Automatically Invoked by <see cref="GetNextRandomTipForUser" />
        /// when the user has run out of tips.
        /// <para>
        /// Returns true if changes were made -- or false if user is already a guru.
        /// </para>
        /// </summary>
        /// <param name="applicationHostIdentifier">The application host identifier.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <returns></returns>
        private bool IncrementMaxDifficultyOfUser(Guid applicationHostIdentifier, string userIdentifier)
        {
            TipUserSettings tipUserStatistics = GetUserSettingsRecord(applicationHostIdentifier, userIdentifier);



            Difficulty difficulty = tipUserStatistics.Difficulty;

            if (difficulty >= Difficulty.Guru)
            {
                //Can't be anything higher than Guru, so No changes.
                return false;
            }

            tipUserStatistics.Difficulty = (Difficulty) ((int) difficulty) + 1;

            //_repositoryService.AttachOnCommit(tipUserStatistics);
            _repositoryService.UpdateOnCommit(tipUserStatistics);

            return true;
        }

        private TipUserViewMetrics GetTipUserViewMetricsCreatingNewOnesIfTheyDontExist(
            Guid applicationTennantId,
                                                                         string userIdentifier)
        {
            List<TipUserViewMetrics> results = new List<TipUserViewMetrics>();

            //Get as a queryable, but don't execute it yet,
            //the Metrics for the current user, in the current org, in the current aplication:
            var r1 =
                _repositoryService
                    .GetByFilter<TipUserViewMetrics>(
                        tm => (
                            (tm.ApplicationTennantId == applicationTennantId) &&
                            (tm.UserIdentifier == userIdentifier)
                            ))
                    .Select(tm => new {tm.Key});

            //join the two together
            //in order to get a query for all missing metrics:
            string[] missingKeyNames =
                _repositoryService
                    .GetByFilter<TipDefinition>(
                        td => (td.Enabled == true) )
                    .Select(td => new {td.Key})
                    .Except(r1)
                    .Select(x => x.Key)
                    .ToArray();

            //Create the missing ones:

            //But commit them in a different scope so that we don't commit more than we should be (ie other work in progress).
#pragma warning disable 168
            using (var uows = new UnitOfWorkThreadScope())
#pragma warning restore 168
            {
                foreach (string missingKey in missingKeyNames)
                {
                    TipUserViewMetrics tipUserViewMetrics = new TipUserViewMetrics
                        {
                            ApplicationTennantId = applicationTennantId,
                            UserIdentifier = userIdentifier,
                            Key = missingKey
                        };

                    results.Add(tipUserViewMetrics);

                    _repositoryService.AddOnCommit<TipUserViewMetrics>(tipUserViewMetrics);
                }
                _repositoryService.GetContext().Commit();
            }

            //Return the records that have not been added yet...
            return results.ToArray().FirstOrDefault();
        }



        private IEnumerable<TipDefinition> GetFilteredCachedTipsCommonToAllUsers(Difficulty minDifficulty, Difficulty maxDifficulty)
        {

            var cache = GetAllCachedTipDefinitions();

            if (maxDifficulty == Difficulty.Undefined)
            {
                maxDifficulty = minDifficulty;
            }
            if (maxDifficulty < minDifficulty)
            {
                maxDifficulty = minDifficulty;
            }

            //Note: that it deoesn't cause an error if take is beyond collection:
            return cache.Where(x => x.Enabled
                                    &&
                                    x.Difficulty >= minDifficulty
                                    &&
                                    x.Difficulty <= maxDifficulty);
        }


        /// <summary>
        /// Gets all tips -- no matter Difficulty.
        /// </summary>
        /// <returns></returns>
        private IList<TipDefinition> GetAllCachedTipDefinitions()
        {
            IList<TipDefinition> cache;

            _hostBasedCachingService.TryGet(_cachedTipDefinitionCacheKey,
                                   out cache,
                                   () => cache = _repositoryService.GetByFilter<TipDefinition>(
                                       x => (x.Enabled == true)
                                                     )
                                                                   .ToList(),
                                   XAct.Library.Settings.Caching.DefaultReferenceCachingTimeSpan,
                                   true
                );

            return cache;
        }




        private Tip ConvertToTip(TipDefinition serializedTip, CultureInfo cultureInfo = null)
        {
            if (cultureInfo == null)
            {
                cultureInfo = _clientEnvironmentService.ClientUICulture;
            }

            string title = ((!serializedTip.ResourceFilter.IsNullOrEmpty())&&(!serializedTip.Title.IsNull()))
                               ? _resourceService.GetString(serializedTip.ResourceFilter,
                                                            serializedTip.Title)
                               : serializedTip.Title;



            string body = ((!serializedTip.ResourceFilter.IsNullOrEmpty())&&(!serializedTip.Text.IsNull()))
                              ? _resourceService.GetString(serializedTip.ResourceFilter, serializedTip.Text)
                              : serializedTip.Text;

            Tip tip = new Tip(title, body, serializedTip.Difficulty);

            return tip;
        }



    }
}