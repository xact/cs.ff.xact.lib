namespace XAct.Users.Tips
{
#pragma warning disable 1591
    public class TipManagementServiceCacheObject
    {
        public TipUserSettings tipUserSettings { get; set; }
        public TipUserViewMetrics JustCreatedViewMetrics { get; set; }
    }
#pragma warning restore 1591
}