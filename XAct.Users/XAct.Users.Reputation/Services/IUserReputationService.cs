﻿namespace XAct.Users.Reputation
{
    using System;

    /// <summary>
    /// Contract for a Service to manage the adding and removing
    /// of Repuation points from a reputation target (Entity / User).
    /// <para>
    /// Points could be added for answering a forum question, 
    /// removed for not responding in a timely fashion, etc.
    /// </para>
    /// <para>
    /// It works by having a preset number of <see cref="UserReputationOperation"/>s recorded (Id, Name,Value).
    /// The <see cref="UserReputationOperation.Value"/> is the number of points that the 
    /// operation adds or removes from the total score
    /// every time the operation is performed.
    /// </para>
    /// </summary>
    public interface IUserReputationService : IHasXActLibService
    {
        /// <summary>
        /// Performs the operation (adding the <see cref="UserReputationOperation.Value" /> Points)
        /// to the reputation target's total score.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// //As SomeGoodOperation has a Value of 5, 5 points are added to the target's total score.
        /// _reputationService(userId,Operations.SomeGoodOperation);
        /// //As SomeNefariousOperation has a Value of -30, 30 points are subtracted to the target's total score.
        /// _reputationService(userId,Operations.SomeNefariousOperation);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="reputationTargetId">The reputation target identifier.</param>
        /// <param name="reputationOperationKey">The reputation operation key/name.</param>
        void PerformOperation(string reputationTargetId, string reputationOperationKey);

        /// <summary>
        /// Gets the user's reputation score.
        /// </summary>
        /// <param name="reputationTargetId"></param>
        /// <returns></returns>
        KeyValue<int, string> GetValue(string reputationTargetId);
    }
}
