﻿namespace XAct.Users.Reputation.Services.Implementations
{

    /// <summary>
    /// An implementation of the 
    /// <see cref="IUserReputationService"/>
    /// Contract for a Service to manage the adding and removing
    /// of Repuation points from a reputation target (Entity / User).
    /// <para>
    /// Points could be added for answering a forum question, 
    /// removed for not responding in a timely fashion, etc.
    /// </para>
    /// <para>
    /// It works by having a preset number of <see cref="UserReputationOperation"/>s recorded (Id, Name,Value).
    /// The <see cref="UserReputationOperation.Value"/> is the number of points that the 
    /// operation adds or removes from the total score
    /// every time the operation is performed.
    /// </para>
    /// </summary>
    public class UserReputationService : IUserReputationService
    {
        private readonly IUserReputationManagementService _reputationManagementService;

        /// <summary>
        /// Prevents a default instance of the <see cref="UserReputationService"/> class from being created.
        /// </summary>
        /// <param name="reputationManagementService">The reputation management service.</param>
        public UserReputationService(IUserReputationManagementService reputationManagementService)
        {
            _reputationManagementService = reputationManagementService;
        }

        /// <summary>
        /// Performs the operation (adding the <see cref="UserReputationOperation.Value" /> Points)
        /// to the reputation target's total score.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// _reputationService(userId,Operations.SomeGoodOperation);
        /// _reputationService(userId,Operations.SomeNefariousOperation);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="reputationTargetId">The reputation target identifier.</param>
        /// <param name="reputationOperationKey">The reputation operation key/name.</param>
        public void PerformOperation(string reputationTargetId, string reputationOperationKey)
        {
            _reputationManagementService.PerformOperation(reputationTargetId, reputationOperationKey);
        }


        /// <summary>
        /// Gets the user's reputation score.
        /// </summary>
        /// <param name="reputationTargetId"></param>
        /// <returns></returns>
        public KeyValue<int, string> GetValue(string reputationTargetId)
        {
            return _reputationManagementService.GetValue(reputationTargetId);
        }

    }
}