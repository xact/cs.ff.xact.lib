﻿namespace XAct.Users.Reputation.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Metrics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Users.Reputation.Services.Configuration;

    /// <summary>
    /// An implementation of the 
    /// <see cref="IUserReputationManagementService"/>
    /// Contract for a Service to manage the adding and removing
    /// of Repuation points from a reputation target (Entity / User).
    /// <para>
    /// Points could be added for answering a forum question, 
    /// removed for not responding in a timely fashion, etc.
    /// </para>
    /// <para>
    /// It works by having a preset number of <see cref="UserReputationOperation"/>s recorded (Id, Name,Value).
    /// The <see cref="UserReputationOperation.Value"/> is the number of points that the 
    /// operation adds or removes from the total score
    /// every time the operation is performed.
    /// </para>
    /// </summary>
    public class UserReputationManagementService : IUserReputationManagementService
    {
        private readonly ICounterService _counterManagementService;
        private readonly IUserReputationManagementServiceConfiguration _reputationManagementServiceConfiguration;
        private readonly IUserReputationManagementServiceState _reputationManagementServiceCache;

        /// <summary>
        /// The key used to register reputations using the implementation of
        /// <see cref="ICounterService"/>
        /// </summary>
        public static string CounterKey = "Users/Reputation";



        Guid ApplicationTennantId
        {
            get { return Guid.Empty; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserReputationManagementService" /> class.
        /// </summary>
        /// <param name="counterManagementService">The counter service.</param>
        /// <param name="reputationManagementServiceConfiguration">The reputation management service configuration.</param>
        /// <param name="reputationManagementServiceCache">The reputation management service cache.</param>
        public UserReputationManagementService(
            ICounterService counterManagementService,
            IUserReputationManagementServiceConfiguration reputationManagementServiceConfiguration,
            IUserReputationManagementServiceState reputationManagementServiceCache)
        {
            _counterManagementService = counterManagementService;
            _reputationManagementServiceConfiguration = reputationManagementServiceConfiguration;
            _reputationManagementServiceCache = reputationManagementServiceCache;

            
            InitializeOperationsCache();

        }


        /// <summary>
        /// Performs the operation (adding the <see cref="UserReputationOperation.Value" /> Points)
        /// to the reputation target's total score.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// _reputationService(userId,Operations.SomeGoodOperation);
        /// _reputationService(userId,Operations.SomeNefariousOperation);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="reputationTargetId">The reputation target identifier.</param>
        /// <param name="reputationOperationKey">The reputation operation key/name.</param>
        public void PerformOperation(string reputationTargetId, string reputationOperationKey)
        {

            UserReputationOperation reputationOperation;

            if (!_reputationManagementServiceCache.TryGetValue(reputationOperationKey, out reputationOperation))
            {
                //Don't know of any Operation by that name:
                return;
            }

            //counterService doesn't like null: 
            if (reputationTargetId == null)
            {
                reputationTargetId = string.Empty;
            }

            //Have Operation schema information, therefore know number of points
            //to add or remove. In turn, invokes OffsetOperation method:
            OffsetValue(reputationTargetId, reputationOperation.Value);

        }


        /// <summary>
        /// Adds or removes the specific value from the
        /// specified entity/user's reputation score.
        /// <para>
        /// IMPORTANT:
        /// this operation affects the Operation's Value directly,
        /// without using an <see cref="UserReputationOperation" />, and is intended
        /// for Maintenance, Corrections, etc.
        /// </para>
        /// </summary>
        /// <param name="reputationTargetId">The reputation target identifier.</param>
        /// <param name="valueOffset">The amount to increment or decrement the repuation score.</param>
        public void OffsetValue(string reputationTargetId, int valueOffset)
        {
            if (valueOffset == 0)
            {
                return;
            }

            //counterService doesn't like null: 
            if (reputationTargetId == null)
            {
                reputationTargetId = string.Empty;
            }

            //We ccould use our own entries...or pass it off to the counterManagementService.


            //If using CounterManagementService, the key is 
            //"Reputation/TargetId" 

            _counterManagementService.OffsetValue(
                key: "Reputation",
                valueOffset: valueOffset,
                minValueAllowed: 0,
                maxValueAllowed: Int32.MaxValue,
                targetId: reputationTargetId, //the identifier of the user ('ssigal')
                applicationTennantId: ApplicationTennantId //Universal
                );
        }



        /// <summary>
        /// Sets the target user/entity's reputation value.
        /// <para>
        /// IMPORTANT:
        /// this operation affects the Operation's Value directly,
        /// without using an <see cref="UserReputationOperation" />, and is intended
        /// for Maintenance, Resets, etc.
        /// </para>
        /// <para>
        /// WARNING: In most scerios, consider using either
        /// <see cref="IUserReputationService.PerformOperation" /> or
        /// <see cref="OffsetValue" />
        /// before considering the use of <see cref="SetValue" />
        /// </para>
        /// </summary>
        /// <param name="reputationTargetId">The reputation target identifier.</param>
        /// <param name="value">The value.</param>
        public void SetValue(string reputationTargetId, int value)
        {

            //counterService doesn't like null: 
            if (reputationTargetId == null)
            {
                reputationTargetId = string.Empty;
            }

            _counterManagementService.SetValue(
                key: "Reputation",
                value: value,
                targetId: reputationTargetId,
                applicationTennantId: ApplicationTennantId //Universal
                );
        }


        /// <summary>
        /// Gets the target user/entity's reputation value.
        /// </summary>
        /// <param name="reputationTargetId">The reputation target identifier.</param>
        /// <returns></returns>
        public KeyValue<int, string> GetValue(string reputationTargetId)
        {

            //counterService doesn't like null: 
            if (reputationTargetId == null)
            {
                reputationTargetId = string.Empty;
            }

            int score = _counterManagementService.GetValue(
                key: "Reputation",
                targetId: reputationTargetId,
                applicationTennantId: ApplicationTennantId //Universal
                );

            return new KeyValue<int, string> {Key = score, Value = Score(score)};
        }





        private void InitializeOperationsCache()
        {
            _reputationManagementServiceCache.Initialize();

        }

        private string Score(int score)
        {
            string result = string.Empty;

            foreach (KeyValuePair<int, string> kvp in _reputationManagementServiceConfiguration.Scores)
            {
                if (score >= kvp.Key)
                {
                    result = kvp.Value;
                }
            }
            return result;
        }


    }
}