﻿namespace XAct.Users.Reputation.Services.Implementations
{
    using System.Collections.Generic;
    using XAct.Domain.Repositories;
    using XAct.Environment;

    /// <summary>
    /// Implementation of the <see cref="IUserReputationManagementServiceState"/>
    /// to persist a cache of known <see cref="UserReputationOperation"/> entities
    /// so that one doesn't have to hit the db everytime an operation is referenced
    /// ny name, and not Id.
    /// </summary>
    public class UserReputationManagementServiceState : Dictionary<string, UserReputationOperation>,
                                                    IUserReputationManagementServiceState
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Reputations the management service.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public UserReputationManagementServiceState(IEnvironmentService environmentService,
                                                IRepositoryService repositoryService)
        {
            _environmentService = environmentService;
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get; set; }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            lock (this)
            {
                this.Clear();

                foreach (UserReputationOperation reputationOperation in _repositoryService
                    .GetByFilter<UserReputationOperation>(x => true)
                    )
                {
                    this.Add(reputationOperation.Key, reputationOperation);
                }
            }
            Initialized = true;
        }
    }
}