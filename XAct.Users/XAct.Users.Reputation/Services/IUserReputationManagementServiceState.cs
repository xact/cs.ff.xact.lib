﻿namespace XAct.Users.Reputation
{
    using System.Collections.Generic;

    /// <summary>
    /// A Contract for a Cache
    /// to persist a cache of known <see cref="UserReputationOperation"/> entities
    /// so that one doesn't have to hit the db everytime an operation is referenced
    /// ny name, and not Id.
    /// </summary>
    public interface IUserReputationManagementServiceState : IDictionary<string, UserReputationOperation>, IHasInitialize, IHasXActLibServiceState
    {
    }
}