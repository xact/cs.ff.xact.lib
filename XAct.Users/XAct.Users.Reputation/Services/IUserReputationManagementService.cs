﻿namespace XAct.Users.Reputation
{
    using System;

    /// <summary>
    /// Contract for a Service to manage the adding and removing
    /// of Repuation points from a reputation target (Entity / User).
    /// <para>
    /// Points could be added for answering a forum question, 
    /// removed for not responding in a timely fashion, etc.
    /// </para>
    /// <para>
    /// It works by having a preset number of <see cref="UserReputationOperation"/>s recorded (Id, Name,Value).
    /// The <see cref="UserReputationOperation.Value"/> is the number of points that the 
    /// operation adds or removes from the total score
    /// every time the operation is performed.
    /// </para>
    /// </summary>
    public interface IUserReputationManagementService : IUserReputationService, IHasXActLibService
    {



        /// <summary>
        /// Adds or removes the specific value from the
        /// specified entity/user's reputation score.
        /// <para>
        /// IMPORTANT:
        /// this operation affects the Operation's Value directly,
        /// without using an <see cref="UserReputationOperation" />, and is intended
        /// for Maintenance, Corrections, etc.
        /// </para>
        /// </summary>
        /// <param name="reputationTargetId">The reputation target identifier.</param>
        /// <param name="valueOffset">The amount to increment or decrement the repuation score.</param>
        void OffsetValue(string reputationTargetId, int valueOffset);


        /// <summary>
        /// Sets the target user/entity's reputation value.
        /// <para>
        /// IMPORTANT:
        /// this operation affects the Operation's Value directly,
        /// without using an <see cref="UserReputationOperation" />, and is intended
        /// for Maintenance, Resets, etc.
        /// </para>
        /// <para>
        /// WARNING: In most scerios, consider using either
        /// <see cref="IUserReputationService.PerformOperation" /> or
        /// <see cref="OffsetValue" />
        /// before considering the use of <see cref="SetValue" />
        /// </para>
        /// </summary>
        /// <param name="reputationTargetId">The reputation target identifier.</param>
        /// <param name="value">The value.</param>
        void SetValue(string reputationTargetId, int value);


    }
}