﻿namespace XAct.Users.Reputation
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A single Reputation score, at a given date and time.
    /// <para>
    /// ORMs (such as EF) are not well suited to increment/decrement scores
    /// in a multi-thread/multi-host environment.
    /// </para>
    /// <para>
    /// The solution is to record each change separately, and sum those changes.
    /// </para>
    /// <para>
    /// For performance reasons, those entries are summarized at regular intervals
    /// (eg, when retrieving the SUM of records, if the number of records returned
    /// is larger than 1, lock the table, delete all records prior to that last records
    /// datetime, and enter a new sum value).
    /// </para>
    /// </summary>
    [DataContract]
    public class ReputationOperationEntry : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasValue<int>, IHasDateTimeCreatedOnUtc
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// Gets or sets the FK of the Operation this score change refers to.
        /// </summary>
        [DataMember]
        public virtual Guid OperationFK { get; set; }


        /// <summary>
        /// Gets or sets a flag indicating whether the 
        /// <see cref="Value"/> is an offset, or a sum of offsets.
        /// </summary>
        [DataMember]
        public virtual bool IsASummaryValue { get; set; }

        /// <summary>
        /// Gets or sets the offset value (plus/minus).
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [DataMember]
        public virtual int Value { get; set; }



        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        ///   <internal>
        /// As to why its Nullable: sometimes the contract is applied to items
        /// that are not Entities themselves, but pointers to objects that are not known
        /// if they are
        ///   </internal>
        ///   <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        ///   </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }

    }
}