﻿namespace XAct.Users.Reputation
{
    using System;
    using System.Runtime.Serialization;



    /// <summary>
    /// Entity to describe a single Operation
    /// that affects the reputation of a user.
    /// <para>
    /// In effect, it'sIUserReputationServiceeration, 
    /// and contains noIUserReputationManagementService
    /// </para>
    /// <para>
    /// See <see cref="IUserReputationService"/>
    /// and <see cref="IUserReputationManagementService"/>
    /// </para>
    /// </summary>
    [DataContract]
    public class UserReputationOperation :
        IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp,
        IHasApplicationTennantId,
        IHasEnabled,
        IHasOrder,
        IHasKeyValue<int>,
        IHasResourceFilterReadOnly,
        IHasTextAndTitleReadOnly
        //IHasTextAndTitleAndTextAndTitleResourceFilterAndKeyReadOnly
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Part of the Composite key.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether [enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enabled]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }



        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual int Order { get; set; }


        /// <summary>
        /// Gets or sets the unique key/name for this operation.
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// <para>
        /// Part of the Composite key.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Key { get; set; }


        /// <summary>
        /// Gets or sets the positive or negative
        /// score of the <see cref="UserReputationOperation"/>
        /// </summary>
        [DataMember]
        public virtual int Value { get; set; }



        /// <summary>
        /// Gets or sets the display name of the operation.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The subject.
        /// </value>
        [DataMember]
        public virtual string Title { get; set; }


        /// <summary>
        /// Gets or sets the text.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Text { get; set; }



        /// <summary>
        /// Gets or sets the  Filter for the Resource 
        /// describing the Operation 
        /// in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        /// <value>
        /// The resource filter.
        /// </value>
        [DataMember]
        public virtual string ResourceFilter { get; set; }




    }
}