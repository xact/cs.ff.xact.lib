﻿//using System;

//namespace XAct.Users.Reputation
//{
//    using System.Runtime.Serialization;

//    /// <summary>
//    /// Entity to record the reputation of an entity.
//    /// </summary>
//    [DataContract]
//    public class ReputationStatistics : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, IHasValue<int>, IHasFilter, IHasDescription
//    {


//        /// <summary>
//        /// Application Id of the Entity/User.
//        /// <para>Member defined in<see cref="IHasId{T}" /></para>
//        /// </summary>
//        public virtual Guid Id { get; set; }


//        /// <summary>
//        /// Gets or sets the datastore concurrency check timestamp.
//        /// <para>
//        /// Note that this is filled in when persisted in the db -- 
//        /// so it's usable to determine whether to generate the 
//        /// Guid <c>Id</c>.
//        ///  </para>
//        /// </summary>
//        [DataMember]
//        public virtual byte[] Timestamp { get; set; }


//        /// <summary>
//        /// Gets or sets the organisation id of the record
//        /// within a multi-tenant application.
//        /// <para>
//        /// Design tip: it is preferable to allow users to register only online,
//        /// but if the app has to allow the creation of new users/tenants offline,
//        /// investigate <see cref="IDistributedIdService" />
//        /// </para>
//        /// </summary>
//        public virtual Guid ApplicationTennantId { get; set; }


//        /// <summary>
//        /// Gets or sets the cumulative reputational score of the identity.
//        /// <para>Member defined in<see cref="IHasValue{T}" /></para>
//        /// </summary>
//        /// <value>
//        /// The value.
//        /// </value>
//        public virtual int Value { get; set; }

//        /// <summary>
//        /// Gets the optional string that can be used as filter.
//        /// <para>Member defined in<see cref="IHasFilter" /></para>
//        /// </summary>
//        public string Filter { get; set; }

//        /// <summary>
//        /// Gets or sets the description.
//        /// <para>Member defined in<see cref="IHasDescription" /></para>
//        /// </summary>
//        /// <value>
//        /// The description.
//        /// </value>
//        public string Description { get; set; }
//    }
//}
