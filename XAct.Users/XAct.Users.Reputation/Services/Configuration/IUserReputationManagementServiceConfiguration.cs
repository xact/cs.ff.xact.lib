﻿namespace XAct.Users.Reputation.Services.Configuration
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a singleton object to configure <see cref="IUserReputationManagementService"/>
    /// </summary>
    public interface IUserReputationManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Dictionary of Reputation scores to labels
        /// (eg: 0=Novice, 100=Helper, 500=XPert, 1000=Guru,-100=Banned)
        /// </summary>
        IDictionary<int, string> Scores { get; }
    }
}