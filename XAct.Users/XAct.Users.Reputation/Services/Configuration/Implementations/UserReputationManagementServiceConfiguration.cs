namespace XAct.Users.Reputation.Services.Configuration
{
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public class UserReputationManagementServiceConfiguration : IUserReputationManagementServiceConfiguration
    {
        private readonly Dictionary<int, string> _scores = new Dictionary<int, string>();

        /// <summary>
        /// Gets or sets the scores.
        /// </summary>
        /// <value>
        /// The scores.
        /// </value>
        public IDictionary<int, string> Scores
        {
            get { return _scores; }
        }

    }
}