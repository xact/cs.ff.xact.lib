namespace XAct.Users.News
{
    using System;
    using XAct.Domain.Repositories;

    /// <summary>
    /// Contract for a service to manage <see cref="NewsCategory"/> entities.
    /// </summary>
    public interface INewsCategoryManagementService : IReferenceDataRepository<NewsCategory,Guid>, IHasXActLibService
    {

    }


}

