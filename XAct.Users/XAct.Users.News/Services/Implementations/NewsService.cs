namespace XAct.Users.News.Services.Implementations
{
    using System.Linq;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="INewsService"/>
    /// contract to return news/event messages
    /// between specific dates.
    /// </summary>
    public class NewsService : INewsService
    {

        private readonly INewsManagementService _newsManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewsService" /> class.
        /// </summary>
        /// <param name="newsManagementService">The news management service.</param>
        public NewsService(
            INewsManagementService newsManagementService)
        {
            _newsManagementService = newsManagementService;
        }


        /// <summary>
        /// Retrieves the <see cref="NewsItem"/>s to display on screen.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        public NewsItem[] RetrieveNewsItems(PagedQuerySpecification pagedQuerySpecification)
        {
            return _newsManagementService.RetrieveNewsItems(pagedQuerySpecification).ToArray();
        }
    }
}