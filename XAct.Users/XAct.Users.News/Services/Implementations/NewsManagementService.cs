namespace XAct.Users.News.Services.Implementations
{
    using System;
    using System.Linq;
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Messages;
    using XAct.Users.News.Services.Configuration;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="INewsManagementService"/>
    /// service to manage <see cref="News"/> entities
    /// </summary>
    public class NewsManagementService : DistributedGuidIdRepositoryServiceBase<NewsEntry>, INewsManagementService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly INewsManagementServiceConfiguration _newsManagementServiceConfiguration;
        private readonly INewsCategoryManagementService _newsCategoryManagementService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewsManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="newsManagementServiceConfiguration">The news management service configuration.</param>
        /// <param name="newsCategoryManagementService">The news category management service.</param>
        public NewsManagementService(ITracingService tracingService,
            IDateTimeService dateTimeService,
                                     IApplicationTennantService applicationTennantService,
                                     IRepositoryService repositoryService,
                                     INewsManagementServiceConfiguration newsManagementServiceConfiguration,
                                     INewsCategoryManagementService newsCategoryManagementService)
            : base(tracingService, repositoryService)
        {
            _dateTimeService = dateTimeService;
            _applicationTennantService = applicationTennantService;
            _newsManagementServiceConfiguration = newsManagementServiceConfiguration;
            _newsCategoryManagementService = newsCategoryManagementService;
        }

        /// <summary>
        /// Gets the configuration settings for this service instance.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public INewsManagementServiceConfiguration Configuration
        {
            get { return _newsManagementServiceConfiguration; }
        }

        /// <summary>
        /// Gets the service used to manage the <see cref="NewsCategory" />s
        /// under which <see cref="News" /> items are filed.
        /// </summary>
        public INewsCategoryManagementService Categories
        {
            get { return _newsCategoryManagementService; }
        }



        /// <summary>
        /// Retrieve a page of records, to display
        /// to an editor. From there, a news Editor can <c>Get</c> one record
        /// in order to edit it.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <returns></returns>
        public IQueryable<NewsEntry> Retrieve(PagedQuerySpecification pagedQuerySpecification,
                                              Guid? applicationTennantId = null)
        {
            if (!applicationTennantId.HasValue)
            {
                applicationTennantId = _applicationTennantService.Get();
            }
            return
                base.Retrieve(
                    x => (x.ApplicationTennantId == applicationTennantId.Value || x.ApplicationTennantId == Guid.Empty),null,
                    pagedQuerySpecification);
        }

        /// <summary>
        /// Retrieves <see cref="NewsItem" />s ready to display.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <returns></returns>
        public IQueryable<NewsItem> RetrieveNewsItems(PagedQuerySpecification pagedQuerySpecification,
                                                      Guid? applicationTennantId = null)
        {
            DateTime now = _dateTimeService.NowUTC;

            if (!applicationTennantId.HasValue)
            {
                applicationTennantId = _applicationTennantService.Get();
            }

            IQueryable<NewsItem> results =
                _repositoryService.GetByFilter<NewsEntry>(
                    x =>
                    (x.Enabled)
                    &&
                    //For this specific application and all applications:
                    ((x.ApplicationTennantId == applicationTennantId.Value) || (x.ApplicationTennantId == Guid.Empty))
                    &&
                    (now >= x.StartDateTimeUtc)
                    &&
                    (now < x.EndDateTimeUtc),
                    new IncludeSpecification<NewsCategory>(),
                    pagedQuerySpecification,
                    q => q
                             .OrderBy(d => d.StartDateTimeUtc)
                             .ThenBy(x => x.EndDateTimeUtc)
                             .ThenBy(x => x.Order)
                             .ThenBy(x => x.Category.Text)
                    )
                    //Return a non-editable Message:
                                  .Select(
                                      x => new NewsItem
                                          {
                                              //Id = x.Id,
                                              StartDateTimeUtc = x.StartDateTimeUtc,
                                              EndDateTimeUtc = x.EndDateTimeUtc,
                                              CategoryId = x.Category.Id,
                                              CategoryName = x.Category.Text,
                                              Title = x.Subject,
                                              Body = x.Body,
                                              LastModifiedDateTimeUtc = x.LastModifiedOnUtc
                                          }
                    )
                                  .Skip(pagedQuerySpecification.PageIndex*pagedQuerySpecification.PageSize)
                                  .Take(pagedQuerySpecification.PageSize);

            return results;
        }
    }
}