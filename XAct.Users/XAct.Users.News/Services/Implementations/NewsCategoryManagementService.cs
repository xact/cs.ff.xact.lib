namespace XAct.Users.News.Services.Implementations
{
    using System;
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Users.News.Services.Configuration;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="INewsCategoryManagementService"/>
    /// contract.
    /// Used to manage <see cref="NewsCategory"/> elements.
    /// </summary>
    public class NewsCategoryManagementService :
        ReferenceDataDistributedGuidIdRepositoryServiceBase<NewsCategory> ,INewsCategoryManagementService
    {
        private readonly INewsManagementServiceConfiguration _newsManagementServiceConfiguration;

        /// <summary>
        /// Gets the news categories.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="newsManagementServiceConfiguration">The news management service configuration.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public NewsCategoryManagementService(
            ITracingService tracingService,
            INewsManagementServiceConfiguration newsManagementServiceConfiguration,
            IRepositoryService repositoryService)
            : base(tracingService, repositoryService)
        {
            _newsManagementServiceConfiguration = newsManagementServiceConfiguration;
        }

    }
}