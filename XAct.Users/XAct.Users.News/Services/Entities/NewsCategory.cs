namespace XAct.Users.News
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Category for
    /// </summary>
    [DataContract]
    public class NewsCategory : XAct.Messages.ReferenceDataGuidIdBase, IHasXActLibEntity
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="NewsCategory"/> class.
        /// </summary>
        public NewsCategory()
        {
            this.GenerateDistributedId();

        }


    }
}