using System;

namespace XAct.Users.News
{
    using System.Runtime.Serialization;

    [DataContract]
#pragma warning disable 1591
    public class NewsItem: IHasDistributedGuidId
    {
        [DataMember]
        public virtual Guid Id { get; set; }


        [DataMember]
        public virtual DateTime? StartDateTimeUtc { get; set; }

        [DataMember]
        public virtual DateTime? EndDateTimeUtc { get; set; }

        [DataMember]
        public virtual Guid CategoryId { get; set; }

        [DataMember]
        public virtual string CategoryName { get; set; }

        [DataMember]
        public virtual string Title { get; set; }

        [DataMember]
        public virtual string Body { get; set; }

        [DataMember]
        public virtual DateTime? LastModifiedDateTimeUtc { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="NewsItem"/> class.
        /// </summary>
        public NewsItem()
        {
            this.GenerateDistributedId();
            //this.Enabled = true;
        }
    }
#pragma warning restore 1591
}
