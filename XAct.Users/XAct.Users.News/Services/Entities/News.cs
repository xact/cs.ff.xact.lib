using System;

namespace XAct.Users.News
{
    using System.Runtime.Serialization;
    using XAct.Environment;

    /// <summary>
    /// A news Entry, displayable using the <see cref="INewsService"/>
    /// </summary>
    [DataContract]
    public class NewsEntry :
        IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp,  
        IHasApplicationTennantId, 
        IHasEnabled, 
        IHasOrder, 
        IHasNullableStartDateTimeEndDateTimeUtc, 
        IHasSubjectAndBody, 
        IHasDateTimeCreatedBy, 
        IHasDateTimeModifiedBy, 
        IHasTag
    {


        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        /// <value>
        /// The organisation id.
        /// </value>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }


        /// <summary>
        /// Gets or sets the start date (UTC).
        /// </summary>
        /// <value>
        /// The start date UTC.
        /// </value>
        [DataMember]
        public virtual DateTime? StartDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets the end date (UTC).
        /// </summary>
        [DataMember]
        public virtual DateTime? EndDateTimeUtc { get; set; }


        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        ///   <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        ///   </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy" /></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public virtual string CreatedBy { get; set; }

        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// <para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? LastModifiedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the who Modified the document.
        /// <para>Member defined in<see cref="IHasDateTimeModifiedBy" /></para>
        /// </summary>
        /// <value>
        /// The Modified by.
        /// </value>
        [DataMember]
        public virtual string LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the message subject.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The subject.
        /// </value>
        [DataMember]
        public virtual string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The body.
        /// </value>
        [DataMember]
        public virtual string Body { get; set; }

        /// <summary>
        /// Gets or sets the FK of the customizable News entry category.
        /// </summary>
        /// <value>
        /// The news category fk.
        /// </value>
        [DataMember]
        public virtual Guid CategoryFK { get; set; }
        
        /// <summary>
        /// The news Category
        /// </summary>
        [DataMember]
        public virtual NewsCategory Category { get; set; }

        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Tag { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="NewsEntry"/> class.
        /// </summary>
        public NewsEntry()
        {
            this.GenerateDistributedId();

        }
    }
}
