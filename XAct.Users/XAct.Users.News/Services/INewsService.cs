namespace XAct.Users.News
{
    using XAct.Messages;

    /// <summary>
    /// Contract for a service to return news items
    /// that are valid within a certain date.
    /// </summary>
    public interface INewsService : IHasXActLibService
    {

        /// <summary>
        /// Retrieves the <see cref="NewsItem"/>s to display on screen.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        NewsItem[] RetrieveNewsItems(PagedQuerySpecification pagedQuerySpecification);

    }
}