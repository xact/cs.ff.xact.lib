namespace XAct.Users.News
{
    using System;
    using System.Linq;
    using XAct.Domain.Repositories;
    using XAct.Messages;
    using XAct.Users.News.Services.Configuration;


    /// <summary>
    /// Service to create/edit/delete news items.
    /// </summary>
    public interface INewsManagementService : ISimpleRepository<NewsEntry, Guid>, IHasXActLibService
    {

        /// <summary>
        /// Gets the configuration settings for this service instance.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        INewsManagementServiceConfiguration Configuration { get; }


        /// <summary>
        /// Gets the service used to manage the <see cref="NewsCategory"/>s
        /// under which <see cref="News"/> items are filed.
        /// </summary>
        INewsCategoryManagementService Categories { get; }

        

        /// <summary>
        /// Retrieve a page of records, to display
        /// to an editor. From there, a news Editor can <c>Get</c> one record
        /// in order to edit it.
        /// </summary>
        /// <param name="pagedQuerySpecification"></param>
        /// <param name="applicationTennantId"></param>
        /// <returns></returns>
        IQueryable<NewsEntry> Retrieve(PagedQuerySpecification pagedQuerySpecification, Guid? applicationTennantId = null);


        /// <summary>
        /// Retrieves <see cref="NewsItem"/>s ready to display.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <param name="applicationTennantId">The application tennant identifier.</param>
        /// <returns></returns>
        IQueryable<NewsItem> RetrieveNewsItems(PagedQuerySpecification pagedQuerySpecification, Guid? applicationTennantId=null);
    }
}

