namespace XAct.Users.News.Services.Configuration
{
    /// <summary>
    /// Contract for an configuration package used to configure
    /// <see cref="INewsManagementService"/>
    /// </summary>
    public interface INewsManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {
        

    }
}