namespace XAct.Users.News.Services.Configuration.Implementations
{
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="INewsManagementServiceConfiguration"/>
    /// in order to configure an instance of 
    /// <see cref="INewsManagementService"/>
    /// </summary>
    public class NewsManagementServiceConfiguration : INewsManagementServiceConfiguration, IHasXActLibServiceConfiguration
    {

    }
}