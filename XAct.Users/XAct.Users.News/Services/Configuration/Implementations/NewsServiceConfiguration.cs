namespace XAct.Users.News.Services.Configuration.Implementations
{
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="INewsServiceConfiguration"/>
    /// </summary>
    public class NewsServiceConfiguration : INewsServiceConfiguration, IHasXActLibServiceConfiguration
    {
        
    }
}