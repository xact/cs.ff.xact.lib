namespace XAct.Users.News.Services.Configuration
{
    /// <summary>
    /// Contract for a singleton object used to configure
    /// all instances of <see cref="INewsService"/>
    /// </summary>
    public interface INewsServiceConfiguration : IHasXActLibServiceConfiguration
    {
        
    }
}