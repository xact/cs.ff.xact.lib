﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;


    /// <summary>
    /// Extensions to the returned <see cref="AuthShippingAddress"/>
    /// </summary>
    public static class AuthShippingAddressExtensions
    {


        /// <summary>
        /// Maps the given <see cref="AuthBillingAddress"/>
        /// to a <see cref="TransactionAddress"/>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static TransactionAddress MapTo(this AuthShippingAddress source,
                                                        TransactionAddress target = null)
        {

            if (target == null)
            {
                target = new TransactionAddress();
            }

            target.Name = source.name;
            //Shipping, doesn't have the contact info that Billing does:
            //target.Email = source.email;
            //target.PhoneNumber = source.phoneNumber;
            //target.PhoneExt = source.phoneExt;


            target.StreetLine1 = source.addrLine1;
            target.StreetLine2 = source.addrLine2;
            target.Neighbourhood = null;
            target.City = source.city;
            target.Region = source.state;
            target.Country = source.country;


            return target;
        }


    }
}
