﻿
namespace XAct
{
    using System;
    using System.Collections.Generic;
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the <see cref="TransactionLineItem"/> object.
    /// </summary>
    public static class TransactionLineItemExtensions
    {

        /// <summary>
        /// Maps the given <see cref="TransactionLineItem"/>
        /// to a <see cref="AuthLineitem"/>.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static AuthLineitem MapTo(this TransactionLineItem source, AuthLineitem target = default(AuthLineitem))
        {
            if (target == null){target = new AuthLineitem();}

            //The type of line item that is being passed in. (Always Lower Case, ‘product’, ‘shipping’, ‘tax’ or ‘coupon’, defaults to ‘product’) Required
            target.type = SerializeLineItemType(source.Type);
            //Y or N. Will default to Y if the type is shipping. Optional
            target.tangible = source.Tangible?"Y":"N";
            //Name of the item passed in. (128 characters max, cannot use ‘<' or '>’, defaults to capitalized version of ‘type’.) Required
            target.name = source.Name.Substring(0,Math.Min(source.Name.Length,128)).Replace("<","").Replace(">","");
            //Your custom product identifier. Optional
            target.productId = source.ProductId;
            //Price of the line item. Format: 0.00-99999999.99, defaults to 0 if a value isn’t passed in or if value is incorrectly formatted, no negatives (use positive values for coupons). Required
            target.price = source.Price;
            //Quantity of the item passed in. (0-999, defaults to 1 if not passed in or incorrectly formatted.) Optional
            target.quantity = source.Quantity;
            //Any start up fees for the product or service. Can be negative to provide discounted first installment pricing, but cannot equal or surpass the product price. Optional
            target.startupFee = source.StartupFee;

            //Sets billing frequency. Ex. ‘1 Week’ to bill order once a week. (Can use # Week, # Month or # Year) Required for recurring lineitems.
            target.recurrence = SerializeDuration(source.RecurrenceType, source.RecurrenceTypeAmount,false);
            //Sets how long to continue billing. Ex. ‘1 Year’. (Forever or # Week, # Month, # Year) Required for recurring lineitems.
            target.duration = SerializeDuration(source.RecurrenceDurationType, source.RecurrenceDurationTypeAmount,false);
            

            if (target.options == null) { target.options = new List<AuthLineitemOption>(); }
            foreach (TransactionLineItemOption lineItemOption in source.Options)
            {
                target.options.Add(lineItemOption.MapTo());
            }
            return target;
        }

        private static string SerializeLineItemType(this LineItemType lineItemType)
        {
            switch (lineItemType)
            {
                case LineItemType.Product:
                    return "product";
                case LineItemType.Shipping:
                    return "shipping";
                case LineItemType.Tax:
                    return "tax";
                case LineItemType.Coupon:
                    return "coupon";
                default:
                    return "product";
            }
        }


        private static string SerializeDuration(Recurrence recurrence, int amount,bool throwExceptionIfNotRecognized=true)
        {
            string tmp = amount.ToString();

            switch (recurrence)
            {
                case Recurrence.Week:
                    tmp += " Week";
                    break;
                case Recurrence.Month:
                    tmp += " Month";
                    break;
                case Recurrence.Year:
                    tmp += " Year";
                    break;
                case Recurrence.Eternity:
                    //remove the int value:
                    tmp = "Forever";
                    break;
                default:
                    if (throwExceptionIfNotRecognized)
                    {
                        throw new ArgumentException("Recurrence pattern is not valid.");
                    }
                    tmp = null;
                    break;
            }
            return tmp;
        }
    }
}
