﻿namespace XAct.ExtensionMethods
{
    using System;
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the returned <see cref="AuthLineitem"/> objects.
    /// </summary>
    public static class AuthLineitemExtensions
    {
        public static TransactionLineItem MapTo(this AuthLineitem source , TransactionLineItem target = null)
        {

            if (target == null){target = new TransactionLineItem();}

            //Name of the item passed in. (128 characters max, cannot use ‘<' or '>’, defaults to capitalized version of ‘type’.) Required
            target.Name = source.name;

            //The type of line item that is being passed in. (Always Lower Case, ‘product’, ‘shipping’, ‘tax’ or ‘coupon’, defaults to ‘product’) Required
            //Could have used Enum parse -- but maybe the enum names change in the future, so prefer this approach for now:
            switch (source.type)
            {
                case "product":
                    target.Type = LineItemType.Product;
                    break;
                case "shipping":
                    target.Type = LineItemType.Shipping;
                    break;
                case "tax":
                    target.Type = LineItemType.Tax;
                    break;
                case "coupon":
                    target.Type = LineItemType.Coupon;
                    break;
                default:
                    target.Type = LineItemType.Product;
                    break;
            }
            //Price of the line item. Format: 0.00-99999999.99, defaults to 0 if a value isn’t passed in or if value is incorrectly formatted, no negatives (use positive values for coupons). Required
            target.Price = source.price.HasValue ? source.price.Value : 0.00m;
            target.ProductId = source.productId;
            //Quantity of the item passed in. (0-999, defaults to 1 if not passed in or incorrectly formatted.) Optional
            target.Quantity = source.quantity.HasValue?source.quantity.Value:1;

            int tmp2;
            Recurrence tmp1;

            Parse(source.recurrence, out tmp1, out tmp2);
            target.RecurrenceType = tmp1;
            target.RecurrenceTypeAmount = tmp2;

            Parse(source.recurrence, out tmp1, out tmp2);
            target.RecurrenceDurationType = tmp1;
            target.RecurrenceDurationTypeAmount = tmp2;


            return target;
        }

        static void Parse(string text, out Recurrence recurrence, out int quantity)
        {
            if (text.IsNullOrEmpty())
            {
                quantity = 0;
                recurrence = Recurrence.Undefined;

                return;
            }
            //As per: "Sets how long to continue billing. Ex. ‘1 Year’. (Forever or # Week, # Month, # Year) Required for recurring lineitems."
            if (text == "Forever")
            {
                quantity = 0;
                recurrence = Recurrence.Eternity;
            }

            string[] parts = text.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length != 2)
            {
                throw new ArgumentException("Cannot break {0} into two parts (quantity/type).".FormatStringInvariantCulture(text));
            }

            quantity = int.Parse(parts[0]);

            switch (parts[1])
            {
                //As per: "Sets how long to continue billing. Ex. ‘1 Year’. (Forever or # Week, # Month, # Year) Required for recurring lineitems."
                case "Week":
                    recurrence = Recurrence.Week;
                    break;
                case "Month":
                    recurrence = Recurrence.Month;
                    break;
                case "Year":
                    recurrence = Recurrence.Year;
                    break;
                default:
                    throw new ArgumentException("Cannot determine duration type from: '{0}'".FormatStringInvariantCulture(parts[1]));
            }

        }
    }
}
