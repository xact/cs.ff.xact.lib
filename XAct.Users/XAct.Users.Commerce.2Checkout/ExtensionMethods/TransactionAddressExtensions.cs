﻿namespace XAct
{
    using System;
    using System.Linq;
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// 
    /// </summary>
    public static class TransactionAddressExtensions
    {
        /// <summary>
        /// Maps the given <see cref="TransactionAddress"/>
        /// to the target 2Checkout <see cref="AuthBillingAddress"/>
        /// </summary>
        public static AuthBillingAddress MapToBillingAddress(this TransactionAddress source,
                                               AuthBillingAddress target = default(AuthBillingAddress))
        {

            if (source == null)
            {
                return null;
            }

            var tmp = source.Name + source.Email + source.Phone + source.Phone + source.StreetLine1 + source.StreetLine2 + source.City + source.Region + source.PostalCode + source.Country;

            if (tmp.SafeTrim().IsNullOrEmpty())
            {
                return null;
            }


            if (target == null){target = new AuthBillingAddress();}

            target.name = source.Name;

            target.email = source.Email;
            target.phoneNumber = source.Phone;
            target.phoneExt = source.PhoneExt;



            target.addrLine1 = source.StreetLine1;
            target.addrLine2 = source.StreetLine2;
            target.city = source.City;
            target.state = source.Region;
            target.zipCode = source.PostalCode;
            target.country = source.Country;


            return target;

        }





        /// <summary>
        /// Maps the given <see cref="TransactionAddress"/>
        /// to the target 2Checkout <see cref="AuthShippingAddress"/>
        /// </summary>
        public static AuthShippingAddress MapToShippingddress(this TransactionAddress source,
                                               AuthShippingAddress target = default(AuthShippingAddress))
        {

            if (source == null)
            {
                return null;
            }
            var tmp = source.Name + source.StreetLine1 + source.StreetLine2 + source.City + source.Region +
                      source.PostalCode + source.Country;
            if (tmp.SafeTrim().IsNullOrEmpty())
            {
                return null;
            }

            if (target == null) { target = new AuthShippingAddress(); }


            target.name = source.Name;

            //Note that 2Checkout doesn't use the whole information:
            //target.email = purchaseAuthnRecordAddress.Email;
            //target.phoneNumber = purchaseAuthnRecordAddress.PhoneNumber;
            //target.phoneExt = purchaseAuthnRecordAddress.PhoneExt;


            target.addrLine1 = source.StreetLine1;
            target.addrLine2 = source.StreetLine2;
            target.city = source.City;
            target.state = source.Region;
            target.zipCode = source.PostalCode;
            target.country = source.Country;

            

            return target;
        }
    }
}