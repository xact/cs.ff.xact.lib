﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the returned <see cref="AuthBillingAddress"/>
    /// </summary>
    public static class AuthBillingAddressExtensions
    {
        /// <summary>
        /// Maps the given <see cref="AuthBillingAddress"/>
        /// to a <see cref="TransactionAddress"/>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static TransactionAddress MapTo(this AuthBillingAddress source,
                                                        TransactionAddress target = null)
        {

            if (target == null)
            {
                target = new TransactionAddress();
            }

            target.Name = source.name;
            target.Email = source.email;
            target.Phone = source.phoneNumber;
            target.PhoneExt = source.phoneExt;

            target.StreetLine1 = source.addrLine1;
            target.StreetLine2 = source.addrLine2;
            target.Neighbourhood = null;
            target.City = source.city;
            target.Region = source.state;
            target.Country = source.country;

            return target;
        }




    }
}