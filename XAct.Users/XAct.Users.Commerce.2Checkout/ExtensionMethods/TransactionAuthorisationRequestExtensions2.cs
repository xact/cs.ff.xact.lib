﻿
namespace XAct
{
    using System;
    using System.Collections.Generic;
    using TwoCheckout;
    using XAct.Messages;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the <see cref="TransactionAuthorisationRequest"/>
    /// </summary>
    public static class TransactionAuthorisationRequestExtensions2
    {

        /// <summary>
        /// Validate the given <see cref="TransactionAuthorisationRequest"/>
        /// </summary>
        /// <param name="purchaseAuthnRecord"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public static bool Validate(this TransactionAuthorisationRequest purchaseAuthnRecord, IResponse response)
        {
            if ((purchaseAuthnRecord.Total != 0.00m) && (purchaseAuthnRecord.LineItems.Count>0))
            {
                throw new ArgumentException("Do not use Total if giving line items.");
            }

            return true;
        }


        /// <summary>
        /// Maps the given <see cref="TransactionAuthorisationRequest" />
        /// to a 2Checkout <see cref="ChargeAuthorizeServiceOptions" />
        /// </summary>
        /// <param name="purchaseAuthnRecord">The purchase authn record.</param>
        /// <param name="sellerId">The seller identifier.</param>
        /// <param name="privateKey">The private key.</param>
        /// <param name="chargeAuthorizeServiceOptions">The charge authorize service options.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Only Tokens should be passed to 2CO -- not full CCNumber/Exp/CNC.</exception>
        public static ChargeAuthorizeServiceOptions MapTo(this TransactionAuthorisationRequest purchaseAuthnRecord, string sellerId, string privateKey, ChargeAuthorizeServiceOptions chargeAuthorizeServiceOptions = default(ChargeAuthorizeServiceOptions))
        {




            if (chargeAuthorizeServiceOptions == null)
            {
                chargeAuthorizeServiceOptions = new ChargeAuthorizeServiceOptions();
            }

            chargeAuthorizeServiceOptions.token = purchaseAuthnRecord.CCInfo.Token;

            chargeAuthorizeServiceOptions.sellerId = sellerId;
            chargeAuthorizeServiceOptions.privateKey = privateKey;



            chargeAuthorizeServiceOptions.merchantOrderId = purchaseAuthnRecord.MerchantOrderId;

            chargeAuthorizeServiceOptions.currency = purchaseAuthnRecord.CurrencyCode;



            chargeAuthorizeServiceOptions.billingAddr = purchaseAuthnRecord.BillingInformation.MapToBillingAddress();
            chargeAuthorizeServiceOptions.shippingAddr = purchaseAuthnRecord.ShippingInformation.MapToShippingddress();


            if (purchaseAuthnRecord.LineItems.Count > 0)
            {
                //Leave as null if no items
                if (chargeAuthorizeServiceOptions.lineItems == null)
                {
                    chargeAuthorizeServiceOptions.lineItems = new List<AuthLineitem>();
                }
                foreach (TransactionLineItem purchaseAuthnRequestLineItem in purchaseAuthnRecord.LineItems)
                {
                    chargeAuthorizeServiceOptions.lineItems.Add(purchaseAuthnRequestLineItem.MapTo());
                }
            }
            else
            {
                //Only use if not passing in line items.
                chargeAuthorizeServiceOptions.total = purchaseAuthnRecord.Total;
            }



            return chargeAuthorizeServiceOptions;

        }
    }
}
