﻿namespace XAct
{
    using TwoCheckout;
    using XAct.ExtensionMethods;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the returned <see cref="Authorization"/> object.
    /// </summary>
    public static class AuthorizationExtensions
    {

        /// <summary>
        /// Extension method to map the returned <see cref="Authorization"/>
        /// to a standard <see cref="TransactionAuthorisation"/>.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static TransactionAuthorisationMessage MapTo(this Authorization source, TransactionAuthorisationMessage target=null)
        {
            if (target == null){target = new TransactionAuthorisationMessage();}

            //OrderNumber/SaleId 
            //* n TransactionId/InvoiceId (each recurring payment date will get a different Invoice number)


            
            target.OrderNumber = source.orderNumber.HasValue?source.orderNumber.Value.ToString():string.Empty;

            target.TransactionId = source.transactionId.HasValue?source.transactionId.Value.ToString():string.Empty;
            
            target.Total = source.total.HasValue? source.total.Value:0.00m;

            target.CurrencyCode = source.currencyCode;



            //The reason all the following properties are important to manage,
            //even though in some cases we have them before they were sent off to be
            //processed, is that the system used by many payment processors is to handle
            //the whole cc preparation + address collecting, etc. on their site,
            //and it's only in the finaly returned ACK object that you receive 
            //information as to whether the operation was successful + a copy of the 
            //billing/shipping info that their site collected.

            //ie, how we know what they purchased:
            foreach (var li in source.lineItems)
            {
                target.LineItems.Add(li.MapTo());
            }

            target.BillingAddress = source.billingAddr.MapTo();
            target.ShippingAddress = source.shippingAddr.MapTo();

            return target;
        } 
    }

}
