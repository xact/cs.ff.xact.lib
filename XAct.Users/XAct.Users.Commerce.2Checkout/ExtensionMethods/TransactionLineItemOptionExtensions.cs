﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the <see cref="TransactionLineItemOption"/> object.
    /// </summary>
    public static class TransactionLineItemOptionExtensions
    {
        /// <summary>
        /// Maps the given <see cref="TransactionLineItemOption"/>
        /// to a <see cref="AuthLineitemOption"/>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static AuthLineitemOption MapTo(this TransactionLineItemOption source,
                                               AuthLineitemOption target = default(AuthLineitemOption))
        {

            if (target ==null){target = new AuthLineitemOption();}

            target.optName = source.Label;
            target.optValue = source.Value;
            target.optSurcharge = source.Surcharge;

            return target;
        }
    }
}