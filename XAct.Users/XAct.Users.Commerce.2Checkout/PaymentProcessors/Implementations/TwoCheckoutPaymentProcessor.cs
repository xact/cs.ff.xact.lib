﻿namespace XAct.Users.Commerce.Implementations
{
    using TwoCheckout;
    using XAct.Services;
    using XAct.Users.Commerce.Services.Configuration;
    using XAct.Users.Commerce.Services.Entities;


    /// <summary>
    /// An implementation of the <see cref="ITwoCheckoutPaymentProcessor"/>
    /// contract.
    /// </summary>
    [DefaultBindingImplementation(typeof (ITwoCheckoutPaymentProcessor),BindingLifetimeType.TransientScope,Priority.Low)]
    public class TwoCheckoutPaymentProcessor : ITwoCheckoutPaymentProcessor
    {
        private readonly ITwoCheckoutPaymentProcessorConfiguration _checkoutPaymentProcessorConfiguration;
        private readonly ITwoCheckoutPaymentProcessorPrivateConfiguration _checkoutPaymentProcessorPrivateConfiguration;

        /// <summary>
        /// The Service that calls 2Checkout
        /// </summary>
        private TwoCheckout.AccountService _accountService;

        private ChargeService _chargeService;
        private TwoCheckout.CouponService _couponService;
        private TwoCheckout.NotificationService _notificationService;
        private TwoCheckout.OptionService _optionService;
        private TwoCheckout.ProductService _productService;
        private TwoCheckout.ReturnService _returnService;
        private TwoCheckout.SaleService _saleService;

        public TwoCheckoutPaymentProcessor(ITwoCheckoutPaymentProcessorConfiguration checkoutPaymentProcessorConfiguration,ITwoCheckoutPaymentProcessorPrivateConfiguration paymentProcessorPrivateConfiguration)
        {
            //Note that we don't have to handle passing username/passowrd in the case of 2Checkout:
            _checkoutPaymentProcessorConfiguration = checkoutPaymentProcessorConfiguration;
            _checkoutPaymentProcessorPrivateConfiguration = paymentProcessorPrivateConfiguration;


            _accountService = new AccountService();
            _chargeService = new ChargeService();
            _couponService = new CouponService();
            _notificationService = new NotificationService();
            _optionService = new OptionService();
            _productService = new ProductService();
            _returnService = new ReturnService();


            _saleService = new SaleService();

        }

        
        public TransactionAuthorisationMessage Charge(TransactionAuthorisationRequest purchaseAuthnRequest)
        {

            
            


            TwoCheckout.ChargeAuthorizeServiceOptions chargeAuthorizeServiceOptions = 
                purchaseAuthnRequest.MapTo(

                _checkoutPaymentProcessorConfiguration.SellerId, 
                _checkoutPaymentProcessorPrivateConfiguration.PrivateKey
                );

            //Customer.merchantOrderId = "123";

            //Customer.token = "MzIwNzI3ZWQtMjdiNy00NTVhLWFhZTEtZGUyZGQ3MTk1ZDMw";

            //Perform the synchronous operation, 
            //and receive back the Authorisation object
            //which will contain a summary of all that 2Checkout knows:
            try
            {

                //var check  = TwoCheckoutConfig.PrivateKey;

                Authorization authorization = _chargeService.Authorize(chargeAuthorizeServiceOptions);

                TransactionAuthorisationMessage result = authorization.MapTo();

                return result;
            }
                // ReSharper disable RedundantCatchClause
#pragma warning disable 168
            catch (TwoCheckoutException e)
#pragma warning restore 168
            {
                //Assert.IsInstanceOf<TwoCheckoutException>(e);
                throw;
            }
            // ReSharper restore RedundantCatchClause

        }


        //void Refund()
        //{


        //    var ArgsObject = new SaleRefundServiceOptions();
         
        //    ArgsObject.invoice_id = invoice_id;
        //    ArgsObject.amount;
        //    ArgsObject.comment = "test refund";
        //    ArgsObject.category = 5;




        //    var result = ServiceObject.Refund(ArgsObject);
        //}
    }
}