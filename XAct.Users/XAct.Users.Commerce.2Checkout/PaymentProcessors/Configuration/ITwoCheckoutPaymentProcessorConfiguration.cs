﻿namespace XAct.Users.Commerce.Services.Configuration
{
    /// <summary>
    /// Contract for a configuraiton package for an implementation 
    /// of <see cref="ITwoCheckoutPaymentProcessor"/>
    /// </summary>
    public interface ITwoCheckoutPaymentProcessorConfiguration :IHasInitializedReadOnly
    {
        /// <summary>
        /// The merchant Id as it is at the Payment Processor's.
        /// <para>
        /// Eg: at 2Checkout it might be one thing, at PayPal, it will be another.
        /// </para>
        /// </summary>
        string MerchantId { get; }

        string SellerId { get; }

        string PublicKey { get; }

        void Initialize(string merchantId, string publicKey);
    }


    public interface ITwoCheckoutPaymentProcessorPrivateConfiguration: IHasInitializedReadOnly
    {

        bool DemoMode { set; }

        string SecretWord { set; }

        bool SandboxMode { set; }

        string ApiUserName { set; }

        string ApiPassword { set; }

        string PrivateKey { get; }

        void Initialize(bool sandboxMode, string apiUserName, string apiPasword, string PrivateKey, string secretWord);

    }
}