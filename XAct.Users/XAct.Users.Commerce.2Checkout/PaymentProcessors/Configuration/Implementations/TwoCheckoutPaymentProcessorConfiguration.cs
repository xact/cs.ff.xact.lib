﻿namespace XAct.Users.Commerce.Implementations
{
    using System;
    using TwoCheckout;
    using XAct.Services;
    using XAct.Users.Commerce.Services.Configuration;

    /// <summary>
    /// An implementation of the 
    /// <see cref="ITwoCheckoutPaymentProcessorConfiguration"/> contract
    /// to provide a configuration package to an instance of
    /// <see cref="ITwoCheckoutPaymentProcessor"/>
    /// </summary>
    [DefaultBindingImplementation(typeof (ITwoCheckoutPaymentProcessorConfiguration), BindingLifetimeType.SingletonScope
        , Priority.Low)]
    public class TwoCheckoutPaymentProcessorConfiguration : ITwoCheckoutPaymentProcessorConfiguration
    {
        private string _merchantId;
        private string _sellerId;
        private string _publicKey;

        public bool Initialized { get; private set; }

        /// <summary>
        /// The merchant Id as it is at the Payment Processor's.
        /// <para>
        /// Eg: at 2Checkout it might be one thing, at PayPal, it will be another.
        /// </para>
        /// </summary>
        public string MerchantId
        {
            get
            {
                CheckInitialized(); return _merchantId;
            }
            private set { _merchantId = value; }
        }

        public string SellerId
        {
            get { CheckInitialized(); return _sellerId; }
            private set
            {
                TwoCheckoutConfig.SellerID = value;

                _sellerId = value;
            }
        }

        public string PublicKey
        {
            get { CheckInitialized(); return _publicKey; }
            private set
            {
                
                _publicKey = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="TwoCheckoutPaymentProcessorConfiguration"/> class.
        /// </summary>
        public TwoCheckoutPaymentProcessorConfiguration()
        {
            //TwoCheckoutConfig.ApiUsername = "APIuser1817037";
            //TwoCheckoutConfig.ApiPassword = "APIpass1817037";


        }

        public void Initialize(string merchantId, string publicKey)
        {
            MerchantId = merchantId;
            SellerId = merchantId;
            PublicKey = publicKey;
            Initialized = true;
        }
        private void CheckInitialized()
        {
            if (!Initialized)
            {
                throw new Exception("TwoCheckoutPaymentProcessorConfiguration not initialized.");
            }
        }
    }

    [DefaultBindingImplementation(typeof (ITwoCheckoutPaymentProcessorPrivateConfiguration),
        BindingLifetimeType.SingletonScope, Priority.Low)]
    public class TwoCheckoutPaymentProcessorPrivateConfiguration : ITwoCheckoutPaymentProcessorPrivateConfiguration
    {
        private string _privateKey;

        public bool Initialized { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool DemoMode
        {
            set
            {
                TwoCheckoutConfig.Demo = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool SandboxMode
        {
            set
            {
                TwoCheckoutConfig.Sandbox = value;
            }
        }

        /// <summary>
        /// Sets the name of the API user.
        /// </summary>
        public string ApiUserName
        {
            set
            {
                TwoCheckoutConfig.ApiUsername = value;
            }
        }

        /// <summary>
        /// Sets the password of the API user.
        /// </summary>
        public string ApiPassword
        {
            set { TwoCheckoutConfig.ApiPassword = value; }
        }

        public string PrivateKey
        {
            get { return _privateKey; }
            set
            {
                _privateKey = value;
                TwoCheckoutConfig.PrivateKey = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public string SecretWord
        {
            set
            {
                TwoCheckoutConfig.SecretWord = value;
            }
        }

        public void Initialize(bool sandboxMode, string apiUserName, string apiPassword, string privateKey, string secretWord)
        {
            SandboxMode = sandboxMode;
            ApiUserName = apiUserName;
            ApiPassword = apiPassword;
            PrivateKey = privateKey;
            SecretWord = secretWord;
        }
    }
}