﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace XAct.Commerce.Services.Validators
//{
//    using XAct.Messages;

//    public static class LineItemValidator
//    {
//        private string[] validTypes = new string[] {"product", "shipping", "tax", "coupon"};
//        public bool Validate(this TwoCheckout.AuthLineitem target, IResponse response)
//    {
//        if (validTypes.All(x => x != target.type))
//        {
//            response.AddMessage(new MessageCode(MessageCodeIds.))
//        }

//            ms	Array of lineitem objects using the attributes specified below. 
//Will be returned in the order that they are passed in. 
//(Passed as a sub object to the Authorization Object.) 
//(Only Use if you are not passing in total.)
//type	The type of line item that is being passed in. (Always Lower Case, ‘product’, ‘shipping’, ‘tax’ or ‘coupon’, defaults to ‘product’) Required
//name	Name of the item passed in. (128 characters max, cannot use ‘<' or '>’, defaults to capitalized version of ‘type’.) Required
//quantity	Quantity of the item passed in. (0-999, defaults to 1 if not passed in or incorrectly formatted.) Optional
//price	Price of the line item. Format: 0.00-99999999.99, defaults to 0 if a value isn’t passed in or if value is incorrectly formatted, no negatives (use positive values for coupons). Required
//tangible	Y or N. Will default to Y if the type is shipping. Optional
//productId	Your custom product identifier. Optional
//recurrence	Sets billing frequency. Ex. ‘1 Week’ to bill order once a week. (Can use # Week, # Month or # Year) Required for recurring lineitems.
//duration	Sets how long to continue billing. Ex. ‘1 Year’. (Forever or # Week, # Month, # Year) Required for recurring lineitems.
//startupFee	Any start up fees for the product or service. Can be negative to provide discounted first installment pricing, but cannot equal or surpass the product price. Optional
//    }
            
//            sellerId	Your 2Checkout account number. Required
//        privateKey	Your 2Checkout Private Key.  Required
//merchantOrderId	Your custom order identifier. Required.
//token	The credit card token. Required.
//currency	ARS, AUD, BRL, GBP, CAD, DKK, EUR, HKD, INR, ILS, JPY, LTL, MYR, MXN, NZD, NOK, PHP, RON, RUB, SGD, ZAR, SEK, CHF, TRY, AED, USD. Use to specify the currency for the sale. Required.
//total	The Sale Total. Format: 0.00-99999999.99, defaults to 0 if a value isn’t passed in or if value is incorrectly formatted, no negatives (Only Use if you are not passing in lineItems.)
//    }
//}
