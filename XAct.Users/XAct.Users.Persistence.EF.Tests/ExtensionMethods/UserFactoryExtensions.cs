﻿namespace XAct
{
    using System;
    using XAct.Users;

    public static class UserFactoryExtensions
    {
        public static User BuildDefault(this UserFactory userFactory, string key=null)
        {
            User user = UserFactory.Build();

            switch (key)
            {
                case "JohnSmith":
                    userFactory.BuildDefaultForJohnSmith();
                    break;
                case "Default":
                case "":
                case null:
                    userFactory.BuildDefaultForJohnSmith();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("key not recognized.");
            }
            return user;
        }

        public static User BuildDefaultForJohnSmith(this UserFactory userFactory)
        {
            User user = UserFactory.Build();

// ReSharper disable JoinDeclarationAndInitializer
            UserProperty property;
// ReSharper restore JoinDeclarationAndInitializer

            property = new UserProperty {Key = "FirstName"};
            property.SerializeValue("John");
            user.Properties.Add(property);

            property = new UserProperty {Key = "LastName"};
            property.SerializeValue("Smith");
            user.Properties.Add(property);

            property = new UserProperty {Key = "DOB"};
            property.SerializeValue(new DateTime(1970, 1, 1));
            user.Properties.Add(property);

            return user;
        }

    }
}
