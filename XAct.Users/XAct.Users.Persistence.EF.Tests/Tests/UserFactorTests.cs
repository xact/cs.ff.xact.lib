namespace XAct.Tests
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Diagrams.Uml;
    using XAct.Services;
    using XAct.Services.Implementations;
    using XAct.Tests;
    using XAct.Users;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UserFactorTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //Singleton<IocContext>.Instance.ResetIoC();

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            //YayaAttribute.BeforeTest();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test(Description = "")]
        public void Can_Get_User_Factory()
        {
            var factory = XAct.DependencyResolver.Current.GetInstance<UserFactory>();


            Assert.That(factory, Is.Not.Null);
        }

        [Test(Description = "")]
        public void Can_Use_UserFactory_To_Build_A_User()
        {
            var factory = XAct.DependencyResolver.Current.GetInstance<UserFactory>();

            factory.BuildDefault("Default");

            Assert.IsTrue(true);
        }


        [Test(Description = "")]
        public void Can_Use_UserFactory_To_Build_A_Default_John_Smith_User()
        {
            var factory = XAct.DependencyResolver.Current.GetInstance<UserFactory>();

            User user = 
                factory.BuildDefault("JohnSmith");
            
            
            Assert.That(user,Is.Not.Null);
            Assert.That(user.Properties, Is.Not.Null);
            Assert.That(user.Properties.Count, Is.EqualTo(0));
        }


        [Test(Description = "")]
        public void Describe_User_As_Uml_Image_Url()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INetClassDiagramPlantUmlDiagramService>();

            var url = service.DocumentTypeAsImageUrl(new RenderingStats(typeof(User)));
            Trace.Write(url);
            Assert.That(url, Is.Not.Empty);
        }

        [Test(Description = "")]
        public void Describe_UserProperty_As_Uml_Image_Url()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INetClassDiagramPlantUmlDiagramService>();

            var url = service.DocumentTypeAsImageUrl(new RenderingStats(typeof(UserProperty)));
            Trace.Write(url);
            Assert.That(url, Is.Not.Empty);
        }

        [Test(Description = "")]
        public void Describe_UserClaim_As_Uml_Image_Url()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INetClassDiagramPlantUmlDiagramService>();

            var url = service.DocumentTypeAsImageUrl(new RenderingStats(typeof(UserProperty)));
            Trace.Write(url);
            Assert.That(url, Is.Not.Empty);
        }


    }
}


