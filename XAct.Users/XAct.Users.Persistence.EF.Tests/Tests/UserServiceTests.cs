﻿
namespace XAct.Tests
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Services;
    using XAct.Services.Implementations;
    using XAct.Users;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UserServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //Singleton<IocContext>.Instance.ResetIoC();

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            //YayaAttribute.BeforeTest();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_UserService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();

            Assert.That(service, Is.Not.Null);
        }

        [Test]
        public void Can_Get_UserService_Of_Expected_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();

            Assert.That(service, Is.Not.Null);
            Assert.That(service.GetType(), Is.EqualTo(typeof (UserService)));
        }

        [Test]
        public void Can_Get_User_Using_UserService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();

            var user = service.GetById(1.ToGuid());

            Assert.That(user, Is.Not.Null);
        }

        [Test]
        public void Can_Get_User_With_Properties_And_Claims()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();

            var user = service.GetById(1.ToGuid());

            Assert.That(user, Is.Not.Null);
            Assert.That(user.Properties.Count, Is.AtLeast(1));
            Assert.That(user.Claims.Count, Is.AtLeast(1));
        }

        [Test]
        public void Persist_Changed_User_With_Properties_And_Claims()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();
            var unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            Trace.WriteLine("Step 1");
            var user = service.GetById(1.ToGuid());
            var countBefore = user.Properties.Count;
            Trace.WriteLine("Step 2");

            user.Properties.Set("PropNew3B", "foo");
            Trace.WriteLine("Step 3");
            unitOfWorkService.GetCurrent().Commit();

            Trace.WriteLine("Step 4");
            var user2 = service.GetById(1.ToGuid());
            var countAfter = user2.Properties.Count;
            Trace.WriteLine("Step 5");

            var property =
                user2.Properties.SingleOrDefault(
                    x => string.Compare("PropNew3B", x.Key, StringComparison.OrdinalIgnoreCase) == 0);
            Trace.WriteLine("Step 6");

            Assert.That(user, Is.Not.Null, "1");
            Assert.That(countAfter, Is.GreaterThan(countBefore), "2");
            Assert.That(property, Is.Not.Null, "3");
            Assert.That(property.DeserializeValue(), Is.EqualTo("foo"), "4");
        }


        [Test]
        public void Persist_Changed_User_With_Properties_And_Claims_Then_Delete_Property()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();
            var unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            //Get user:
            var user = service.GetById(1.ToGuid());
            var countBefore = user.Properties.Count;

            //Set a new Property:
            user.Properties.Set("PropNew2B", "foo");
            unitOfWorkService.GetCurrent().Commit();

            //Retrieve:
            var user2 = service.GetById(1.ToGuid());
            //CountAfter should now be higher count than last time: 
            var countAfter = user2.Properties.Count;

            //
            var property =
                user2.Properties.SingleOrDefault(
                    x => string.Compare("PropNew2B", x.Key, StringComparison.OrdinalIgnoreCase) == 0);

            //Deleting it should cause count to lower itself:
            user2.Properties.Delete("PropNew2B");
            unitOfWorkService.GetCurrent().Commit();
            var countAfterAfter = user.Properties.Count;

            Assert.That(user, Is.Not.Null, "1");
            Assert.That(countAfter, Is.GreaterThan(countBefore), "PropNew2 should have increaded count of properties.");
            Assert.That(countAfterAfter, Is.EqualTo(countBefore), "Count should be back to original count");
            Assert.That(property, Is.Not.Null, "3");
            Assert.That(property.DeserializeValue(), Is.EqualTo("foo"), "4");
        }

    }
}




