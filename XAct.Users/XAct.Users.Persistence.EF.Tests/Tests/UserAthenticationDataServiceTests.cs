﻿
using XAct.Users.Services;
using XAct.Users.Services.Implementations;

namespace XAct.Tests
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Services;
    using XAct.Services.Implementations;
    using XAct.Users;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UserAthenticationDataServiceTests
    {

        private Guid _userAId;
        private Guid _userBId;
        private Guid _userCId;

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {

            _userAId = 1.ToGuid();
            _userBId = 2.ToGuid();
            _userCId = 3.ToGuid();

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //Singleton<IocContext>.Instance.ResetIoC();

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            //YayaAttribute.BeforeTest();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_UserAuthenticationDataService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            Assert.That(service, Is.Not.Null);
        }

        [Test]
        public void Can_Get_UserAuthenticationDataService_Of_Expected_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            Assert.That(service, Is.Not.Null);
            Assert.That(service.GetType(), Is.EqualTo(typeof (UserAuthenticationDataService)));
        }


        [Test]
        public void Can_Get_User_Enabled_Status()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            bool result = service.GetEnabled(_userAId);

            Assert.That(result, Is.True);

        }

        [Test]
        public void Can_Set_User_Enabled_Status()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.False);

            var userId = _userAId;
            bool result = service.GetEnabled(userId);
            Assert.That(result, Is.True);

            service.SetEnabled(userId, false);
            result = service.GetEnabled(userId);
            Assert.That(result, Is.False);

        }


        [Test]
        public void Can_Set_User_Enabled_Status_Caching_Enabled()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = true;
            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.True);

            var userId = _userBId;
            bool result = service.GetEnabled(userId);
            Assert.That(result, Is.True);

            service.SetEnabled(userId, false);
            result = service.GetEnabled(userId);
            Assert.That(result, Is.False);

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = false;
        }



        [Test]
        public void Can_Get_And_Increment_And_Reset_User_FailedAttempts_Counter()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = false;
            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.False);


            var userId = _userBId;
            var original = service.GetFailedAttemptCounter(userId);

            service.IncrementFailedAttemptCounter(userId);
            var newValue = service.GetFailedAttemptCounter(userId);

            Assert.That(newValue, Is.GreaterThan(original),"Not greater than...");


            service.ResetFailedAttemptCounterAndLockoutDateTimeUtc(userId,false);
            var resetValue = service.GetFailedAttemptCounter(userId);
            Assert.That(resetValue, Is.EqualTo(0));


        }



        [Test]
        public void Can_Get_And_Increment_And_Reset_User_FailedAttempts_Counter_When_Cached()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = true;
            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.True);


            var userId = _userCId;
            var original = service.GetFailedAttemptCounter(userId);

            service.IncrementFailedAttemptCounter(userId);
            var newValue = service.GetFailedAttemptCounter(userId);

            Assert.That(newValue, Is.GreaterThan(original));


            service.ResetFailedAttemptCounterAndLockoutDateTimeUtc(userId,false);
            var resetValue = service.GetFailedAttemptCounter(userId);
            Assert.That(resetValue, Is.EqualTo(0));


        }



        [Test]
        public void Can_Get_User_Hash()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = false;
            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.False);

            var userId = _userAId;
            var result =service.GetPasswordHash(userId);

            Assert.That(result, Is.Not.Null);
            Assert.That(result,Is.Not.Empty);

        }


        [Test]
        public void Can_Set_User_Hash()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = false;
            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.False);

            var userId = _userAId;
            var result = service.GetPasswordHash(userId);
            var result2 = result;

            Assert.That(result2, Is.EqualTo(result));

            service.SetPasswordHash(userId, "FooBar");
            result2 = service.GetPasswordHash(userId);

            Assert.That(result2, Is.Not.EqualTo(result));

        }

        [Test]
        public void Can_Set_User_Hash_When_Cached()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = true;
            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.True);

            var userId = _userBId;
            var result = service.GetPasswordHash(userId);
            var result2 = result;

            Assert.That(result2, Is.EqualTo(result));

            service.SetPasswordHash(userId, "FooBar");
            result2 = service.GetPasswordHash(userId);

            Assert.That(result2, Is.Not.EqualTo(result));

        }


        [Test]
        public void Can_Set_User_LockoutDateTime()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = false;
            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.False);

            var userId = _userAId;
            var result = service.GetLockoutEndDateTimeUtc(userId);
            var result2 = result;

            Assert.That(result2, Is.EqualTo(result));

            service.SetLockoutEndDateTimeUtc(userId, DateTimeOffset.UtcNow);
            result2 = service.GetLockoutEndDateTimeUtc(userId);

            Assert.That(result2, Is.Not.EqualTo(result));

        }

        [Test]
        public void Can_Set_User_LockoutDateTime_When_Cached()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel = true;
            Assert.That(service.Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel, Is.True);

            var userId = _userBId;
            var result = service.GetLockoutEndDateTimeUtc(userId);
            var result2 = result;

            Assert.That(result2, Is.EqualTo(result));

            service.SetLockoutEndDateTimeUtc(userId, DateTimeOffset.UtcNow);
            result2 = service.GetLockoutEndDateTimeUtc(userId);

            Assert.That(result2, Is.Not.EqualTo(result));

        }
        [Test]
        public void Can_Create_A_New_Record()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            var userId = Guid.NewGuid();

            service.Create(userId, "Bar");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            XAct.DependencyResolver.Current.GetInstance<IRepositoryService>().GetContext().Commit();

            var check = service.GetPasswordHash(userId);
            Assert.That(check, Is.EqualTo("Bar"));
            }


        [Test]
        public void Can_Set_Expired_DateTimeUtc()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataService>();

            var userId = Guid.NewGuid();

            service.Create(userId, "Bar");
            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();



            var dto = DateTimeOffset.UtcNow.AddHours(2);
            service.SetLockoutEndDateTimeUtc(userId, dto);

            XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();

            var dto2 = service.GetLockoutEndDateTimeUtc(userId);

            Assert.That(dto2, Is.Not.Null);
            Assert.That(dto2, Is.GreaterThan(DateTimeOffset.UtcNow.AddHours(1)),"Check1");
            Assert.That(dto2, Is.LessThan(DateTimeOffset.UtcNow.AddHours(4)), "Check1");


        }
    }


}




