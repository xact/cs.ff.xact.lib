﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Tests.Tests
{
    using System.Diagnostics;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Services.Implementations;
    using XAct.Users;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class DelegationUsingUserServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //Singleton<IocContext>.Instance.ResetIoC();

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            //YayaAttribute.BeforeTest();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_UserService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();

            Assert.That(service, Is.Not.Null);
        }

        [Test]
        public void Can_Get_UserService_Of_Expected_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();

            Assert.That(service, Is.Not.Null);
            Assert.That(service.GetType(), Is.EqualTo(typeof (UserService)));
        }



        [Test]
        public void Can_Delegate()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            string userName = "JSmith012";
            string userName2 = "BSmithBC";


            var user = UserFactory.Build();
            user.Name = userName;
            user.Properties.Set("SomeKey", 13);
            service.PersistOnCommit(user);

            var user2 = UserFactory.Build();
            user2.Name = userName2 ;
            user.Properties.Set("SomeKey", 26);
            service.PersistOnCommit(user);


            user.AllowedDelegates.Add(user2);
            service.PersistOnCommit(user);

            uowService.GetCurrent().Commit();

            var user1B = service.GetByName(userName, true, true);

            Assert.That(user1B, Is.Not.Null);
            Assert.That(user1B.Properties.Count, Is.GreaterThan(0),"Have Users");
            Assert.That(user1B.AllowedDelegates.Count, Is.GreaterThan(0),"Have Delegeates");

            Assert.That(user1B.AllowedDelegates.First().Properties.Count, Is.EqualTo(0), "And correctly didn't pick up remote Deleteegate's Properties.");


        }



        [Test]
        public void Add_User_As_Delegate()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            string userName = "JSmith0124567";
            string userName2 = "BSmithBCDEG";


            var user = UserFactory.Build();
            user.Name = userName;
            user.Properties.Set("SomeKey", 13);
            service.PersistOnCommit(user);

            var user2 = UserFactory.Build();
            user2.Name = userName2;
            user2.Properties.Set("SomeKey", 26);
            service.PersistOnCommit(user2);

            uowService.GetCurrent().Commit();


            service.AddUserAsDelegate(userName, userName2);
            //Should not need to commit() as method did it.
            //uowService.Current.Commit();


            var user1B = service.GetByName(userName, true, true);

            Assert.That(user1B, Is.Not.Null);
            Assert.That(user1B.Properties.Count, Is.GreaterThan(0), "Have Users");
            Assert.That(user1B.AllowedDelegates.Count, Is.GreaterThan(0), "Have Delegeates");

            Assert.That(user1B.AllowedDelegates.First().Properties.Count, Is.EqualTo(1), "Note how Delegate's properties were picked up...");
            Assert.That(user1B.AllowedDelegates.First().Properties.Get<int>("SomeKey"), Is.EqualTo(26), "heres proof...");


            
        }



        [Test]
        public void Is_User_An_AllowedDelegate_For()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            string userName = "JSmith0124567asdf";
            string userName2 = "BSmithBCDEGasd";


            var user = UserFactory.Build();
            user.Name = userName;
            user.Properties.Set("SomeKey", 13);
            service.PersistOnCommit(user);

            var user2 = UserFactory.Build();
            user2.Name = userName2;
            user2.Properties.Set("SomeKey", 26);
            service.PersistOnCommit(user2);

            uowService.GetCurrent().Commit();


            service.AddUserAsDelegate(userName, userName2);
            //Should not need to commit() as method did it.
            //uowService.Current.Commit();

            bool isValidDelegate = service.IsUserAnAllowedDelegateFor(userName2, userName);

            bool isValidDelegate2 = service.IsUserAnAllowedDelegateFor(userName, userName2);

            Trace.WriteLine(isValidDelegate);
            Trace.WriteLine(isValidDelegate2);


            Assert.That(isValidDelegate, Is.True);



        }








    }
}
