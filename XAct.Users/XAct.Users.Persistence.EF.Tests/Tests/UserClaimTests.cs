﻿
namespace XAct.Tests
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Services;
    using XAct.Services.Implementations;
    using XAct.Users;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UserClaimTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //Singleton<IocContext>.Instance.ResetIoC();

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            //YayaAttribute.BeforeTest();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_User_With_Properties_And_Claims()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();

            var user = service.GetById(1.ToGuid());

            Assert.That(user, Is.Not.Null);
            Assert.That(user.Properties.Count, Is.AtLeast(1));
            Assert.That(user.Claims.Count, Is.AtLeast(1));
        }

        [Test]
        public void Persist_Changed_User_With_Properties_And_Claims()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();
            var unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            Trace.WriteLine("Step 1");
            var user = service.GetById(1.ToGuid());
            var countBefore = user.Properties.Count;

            user.Properties.Set("ClaimNew3", "foo");
            unitOfWorkService.GetCurrent().Commit();

            Trace.WriteLine("Step 2");
            user = service.GetById(1.ToGuid());
            var countAfter = user.Properties.Count;
            Trace.WriteLine("Step 3");

            var Claim =
                user.Properties.SingleOrDefault(
                    x => string.Compare("ClaimNew3", x.Key, StringComparison.OrdinalIgnoreCase) == 0);
            Trace.WriteLine("Step 4");

            Assert.That(user, Is.Not.Null, "1");
            Assert.That(countAfter, Is.GreaterThan(countBefore), "2");
            Assert.That(Claim, Is.Not.Null, "3");
            Assert.That(Claim.DeserializeValue(), Is.EqualTo("foo"), "4");
        }


        [Test]
        public void Persist_Changed_User_With_Properties_And_Claims_Then_Delete_Claim()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserService>();
            var unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

            //Get user:
            var user = service.GetById(1.ToGuid());
            var countBefore = user.Properties.Count;

            //Set a new Claim:
            user.Properties.Set("ClaimNew2", "foo");
            unitOfWorkService.GetCurrent().Commit();

            //Retrieve:
            user = service.GetById(1.ToGuid());
            //CountAfter should now be higher count than last time: 
            var countAfter = user.Properties.Count;

            //
            var Claim =
                user.Properties.SingleOrDefault(
                    x => string.Compare("ClaimNew2", x.Key, StringComparison.OrdinalIgnoreCase) == 0);

            //Deleting it should cause count to lower itself:
            user.Properties.Delete("ClaimNew2");
            unitOfWorkService.GetCurrent().Commit();
            var countAfterAfter = user.Properties.Count;

            Assert.That(user, Is.Not.Null, "1");
            Assert.That(countAfter, Is.GreaterThan(countBefore), "ClaimNew2 should have increaded count of properties.");
            Assert.That(countAfterAfter, Is.EqualTo(countBefore), "Count should be back to original count");
            Assert.That(Claim, Is.Not.Null, "3");
            Assert.That(Claim.DeserializeValue(), Is.EqualTo("foo"), "4");
        }

    }
}




