﻿
namespace XAct.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Services;
    using XAct.Services.Implementations;
    using XAct.Users;
    using XAct.Users.Services.Implementations;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UserManagementServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            //Singleton<IocContext>.Instance.ResetIoC();

            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            //YayaAttribute.BeforeTest();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_UserService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();

            Assert.That(service, Is.Not.Null);
        }

        [Test]
        public void Can_Get_UserService_Of_Expected_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();

            Assert.That(service, Is.Not.Null);
            Assert.That(service.GetType(), Is.EqualTo(typeof(UserManagementService)));
        }

        [Test]
        public void Can_Get_User_Using_UserService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();

            var user = service.GetById(1.ToGuid());

            Assert.That(user, Is.Not.Null);
        }

        [Test]
        public void Can_Get_User_With_Properties_And_Claims()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();

            var user = service.GetById(1.ToGuid());

            Assert.That(user, Is.Not.Null);
            Assert.That(user.Properties.Count, Is.AtLeast(1));
            Assert.That(user.Claims.Count, Is.AtLeast(1));
        }

        [Test]
        public void Persist_Changed_User_With_Properties_And_Claims()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();

            var user = service.GetById(1.ToGuid());
            var countBefore = user.Properties.Count;

            user.Properties.Set("PropNew", "foo");
            service.PersistOnCommit(user);


            user = service.GetById(1.ToGuid());
            var countAfter = user.Properties.Count;

            var property =
                user.Properties.SingleOrDefault(
                    x => string.Compare("PropNew", x.Key, StringComparison.OrdinalIgnoreCase) == 0);

            Assert.That(user, Is.Not.Null, "1");
            Assert.That(countAfter, Is.GreaterThan(countBefore), "2");
            Assert.That(property, Is.Not.Null, "3");
            Assert.That(property.DeserializeValue(), Is.EqualTo("foo"), "4");
        }


        [Test]
        public void Persist_Changed_User_With_Properties_And_Claims_Then_Delete_Property()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();

            //Get user:
            var user = service.GetById(1.ToGuid());
            var countBefore = user.Properties.Count;

            //Set a new Property:
            user.Properties.Set("PropNew2", "foo");
            service.PersistOnCommit(user);

            //Retrieve:
            user = service.GetById(1.ToGuid());
            //CountAfter should now be higher count than last time: 
            var countAfter = user.Properties.Count;

            //
            var property =
                user.Properties.SingleOrDefault(
                    x => string.Compare("PropNew2", x.Key, StringComparison.OrdinalIgnoreCase) == 0);

            //Deleting it should cause count to lower itself:
            user.Properties.Delete("PropNew2");
            service.PersistOnCommit(user);
            var countAfterAfter = user.Properties.Count;

            Assert.That(user, Is.Not.Null, "1");
            Assert.That(countAfter, Is.GreaterThan(countBefore), "PropNew2 should have increaded count of properties.");
            Assert.That(countAfterAfter, Is.EqualTo(countBefore), "Count should be back to original count");
            Assert.That(property, Is.Not.Null, "3");
            Assert.That(property.DeserializeValue(), Is.EqualTo("foo"), "4");
        }


        [Test]
        public void Can_Persist_New_User()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<IUserManagementService>();
            var uowService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
            User user = UserFactory.Build();

            string userName = "JSmithAMBO";

            user.Name = userName;
            user.Properties.Set("SomeKey",13);
            
            service.PersistOnCommit(user);
            uowService.GetCurrent().Commit();

            
            var user2 = service.GetByName(userName,true,true);

            Assert.That(user,Is.Not.Null,"1");
            Assert.That(user2, Is.Not.Null,"2");

            Assert.That(user2.Properties.Count, Is.GreaterThan(0),"3");


        }
    }
}




