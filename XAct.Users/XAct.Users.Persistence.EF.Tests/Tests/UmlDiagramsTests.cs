namespace XAct.Tests
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Diagrams.Uml;
    using XAct.Services;
    using XAct.Services.Implementations;
    using XAct.Tests;
    using XAct.Users;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UmlDiagramsTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();


        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }





        [Test(Description = "")]
        public void Describe_User_As_Uml_Image_Url()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INetClassDiagramPlantUmlDiagramService>();

            var url = service.DocumentTypeAsImageUrl(new RenderingStats(typeof(User)));
            Trace.Write(url);
            Assert.That(url, Is.Not.Empty);
        }

        [Test(Description = "")]
        public void Describe_UserProperty_As_Uml_Image_Url()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INetClassDiagramPlantUmlDiagramService>();

            var url = service.DocumentTypeAsImageUrl(new RenderingStats(typeof(UserProperty)));
            Trace.Write(url);
            Assert.That(url, Is.Not.Empty);
        }

        [Test(Description = "")]
        public void Describe_UserClaim_As_Uml_Image_Url()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INetClassDiagramPlantUmlDiagramService>();

            var url = service.DocumentTypeAsImageUrl(new RenderingStats(typeof(UserClaim)));
            Trace.Write(url);
            Assert.That(url, Is.Not.Empty);
        }

        [Test(Description = "")]
        public void Describe_User_UserProperty_And_UserClaim_As_Uml_Image_Url()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<INetClassDiagramPlantUmlDiagramService>();

            var url = service.DocumentTypeAsImageUrl(
                new RenderingStats(
                    new Type[]
                        {
                            typeof (User),
                            typeof (UserProperty),
                            typeof (UserClaim)
                        }));
            Trace.Write(url);
            Assert.That(url, Is.Not.Empty);
        }


    }
}


