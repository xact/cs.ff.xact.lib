namespace XAct.Users.Initialization.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;

    public class UnitTestUserPropertyDbContextSeeder : UnitTestXActLibDbContextSeederBase<UserProperty>, IUserPropertyDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;
        private readonly IPrincipalService _principalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContextSeeder"/> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="userDbContextSeeder">The user database context seeder.</param>
        public UnitTestUserPropertyDbContextSeeder(
            IDateTimeService dateTimeService, ITracingService tracingService, 
            IEnvironmentService environmentService, IPrincipalService principalService,

                                                 IUserDbContextSeeder userDbContextSeeder)
            : base(tracingService,userDbContextSeeder)
        {
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
            _principalService = principalService;
        }

        public override void CreateEntities()
        {

            this.InternalEntities = new List<UserProperty>();


            CreatePropsForUser1();


            CreatePropsForUser2();
        }

        private void CreatePropsForUser1()
        {
            var prop = new UserProperty
                {
                    UserFK = 1.ToGuid(),
                    Key = "PropA",
                    CreatedBy =_principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy =_principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                };
            prop.SerializeValue(new DateTime(1980, 1, 1));
            this.InternalEntities.Add(prop);


            prop = new UserProperty
                {
                    UserFK = 1.ToGuid(),
                    Key = "PropB",
                    CreatedBy =_principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy =_principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                };
            prop.SerializeValue(123.34m);
            this.InternalEntities.Add(prop);
        }

        private void CreatePropsForUser2()
        {
            UserProperty prop;
            prop = new UserProperty
                {
                    UserFK = 2.ToGuid(),
                    Key = "PropA",
                    CreatedBy =_principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy = _principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                };
            prop.SerializeValue(new DateTime(1980, 1, 1));
            this.InternalEntities.Add(prop);


            prop = new UserProperty
                {
                    UserFK = 2.ToGuid(),
                    Key = "PropC",
                     CreatedBy =_principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy = _principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                };
            prop.SerializeValue("foo");
            this.InternalEntities.Add(prop);
        }
    }
}