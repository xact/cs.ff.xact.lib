namespace XAct.Users.Initialization.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;

    public class UnitTestUserClaimEntityDbContextSeeder : UnitTestXActLibDbContextSeederBase<UserClaim>, IUserClaimDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IPrincipalService _principalService;
        private readonly IDateTimeService _dateTimeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="userDbContextSeeder">The user database context seeder.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="userPropertyDbContextSeeder">The user property database context seeder.</param>
        public UnitTestUserClaimEntityDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
                                                 IUserDbContextSeeder userDbContextSeeder, IPrincipalService principalService, IDateTimeService dateTimeService,
            IUserPropertyDbContextSeeder userPropertyDbContextSeeder)
            : base(tracingService, userDbContextSeeder, userPropertyDbContextSeeder)
        {
            _environmentService = environmentService;
            _principalService = principalService;
            _dateTimeService = dateTimeService;
        }


        public override void CreateEntities()
        {

            this.InternalEntities = new List<UserClaim>();


            CreatePropsForUser1();


            CreatePropsForUser2();
        }

        private void CreatePropsForUser1()
        {
            var prop = new UserClaim
                {
                    UserFK = 1.ToGuid(),
                    Key = "ClaimA",
                    Authority = "<someAuthority>",
                    CreatedBy =_principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy = _principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                };
            prop.SerializeValue(new DateTime(1980, 1, 1));
            this.InternalEntities.Add(prop);


            prop = new UserClaim
                {
                    UserFK = 1.ToGuid(),
                    Key = "ClaimB",
                    Authority = "<someAuthority>",
                    CreatedBy = _principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy =_principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                };
            prop.SerializeValue(123.34m);
            this.InternalEntities.Add(prop);
        }

        private void CreatePropsForUser2()
        {
            UserClaim prop;
            prop = new UserClaim
                {
                    UserFK = 2.ToGuid(),
                    Key = "ClaimA",
                    Authority = "<someAuthority>",
                    CreatedBy = _principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy =_principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                };
            prop.SerializeValue(new DateTime(1980, 1, 1));
            this.InternalEntities.Add(prop);


            prop = new UserClaim
                {
                    UserFK = 2.ToGuid(),
                    Key = "ClaimC",
                    Authority = "<someAuthority>",
                    CreatedBy = _principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy = _principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                };
            prop.SerializeValue("foo");
            this.InternalEntities.Add(prop);
        }
    }
}