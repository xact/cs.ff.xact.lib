using System;

namespace XAct.Users.Initialization.Implementations
{
    using System.Collections.Generic;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;

#pragma warning disable 1591


    /// <summary>
    /// 
    /// </summary>
    public class UnitTestUserAuthenticationDataDbContextSeeder :
        UnitTestXActLibDbContextSeederBase<UserAuthenticationData>, IUserAuthenticationDataDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IPrincipalService _principalService;
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="environmentService">The environment service.</param>
        public UnitTestUserAuthenticationDataDbContextSeeder(ITracingService tracingService,
            IDateTimeService dateTimeService, IPrincipalService principalService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _dateTimeService = dateTimeService;
            _principalService = principalService;
            _environmentService = environmentService;
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {

            this.InternalEntities = new List<UserAuthenticationData>();

            this.InternalEntities.Add(new UserAuthenticationData
            {
                Id = 1.ToGuid(),
                UserId = 1.ToGuid(),
                LastModifiedOnUtc = DateTimeOffset.UtcNow.UtcDateTime,
                Enabled = true,
                PasswordHash = "foo",
                CurrentFailedAttempts = 0,
                LockedOutTillDateTimeUtc = DateTimeOffset.UtcNow.UtcDateTime
            });
            this.InternalEntities.Add(new UserAuthenticationData
            {
                Id = 2.ToGuid(),
                UserId = 2.ToGuid(),
                LastModifiedOnUtc = DateTimeOffset.UtcNow.UtcDateTime,
                Enabled = true,
                PasswordHash = "foo",
                CurrentFailedAttempts = 0,
                LockedOutTillDateTimeUtc = DateTimeOffset.UtcNow.UtcDateTime
            });
            this.InternalEntities.Add(new UserAuthenticationData
            {
                Id = 3.ToGuid(),
                UserId = 3.ToGuid(),
                LastModifiedOnUtc = DateTimeOffset.UtcNow.UtcDateTime,
                Enabled = true,
                PasswordHash = "foo",
                CurrentFailedAttempts = 0,
                LockedOutTillDateTimeUtc = DateTimeOffset.UtcNow.UtcDateTime
            });
            this.InternalEntities.Add(new UserAuthenticationData
            {
                Id = 4.ToGuid(),
                UserId = 4.ToGuid(),
                LastModifiedOnUtc = DateTimeOffset.UtcNow.UtcDateTime,
                Enabled = true,
                PasswordHash = "foo",
                CurrentFailedAttempts = 0,
                LockedOutTillDateTimeUtc = DateTimeOffset.UtcNow.UtcDateTime
            });

        }
    }
}

