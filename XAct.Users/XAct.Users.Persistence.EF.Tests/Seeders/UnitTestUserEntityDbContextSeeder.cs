namespace XAct.Users.Initialization.Implementations
{
    using System.Collections.Generic;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;

#pragma warning disable 1591


    /// <summary>
    /// 
    /// </summary>
    public class UnitTestUserDbContextSeeder : UnitTestXActLibDbContextSeederBase<User>, IUserDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IPrincipalService _principalService;
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="principalService">The principal service.</param>
        /// <param name="environmentService">The environment service.</param>
        public UnitTestUserDbContextSeeder(ITracingService tracingService, IDateTimeService dateTimeService, IPrincipalService principalService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _dateTimeService = dateTimeService;
            _principalService = principalService;
            _environmentService = environmentService;
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {

            this.InternalEntities = new List<User>();

            this.InternalEntities.Add(new User
                {
                    Id = 1.ToGuid(),
                    CreatedBy =_principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy =_principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                });

            this.InternalEntities.Add(new User
                {
                    Id = 2.ToGuid(),
                    CreatedBy =_principalService.CurrentIdentityIdentifier,
                    CreatedOnUtc = _dateTimeService.NowUTC,
                    LastModifiedBy =_principalService.CurrentIdentityIdentifier,
                    LastModifiedOnUtc = _dateTimeService.NowUTC
                });

        }


    }


}

