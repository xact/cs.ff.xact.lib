﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XAct
{
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Users;

    /// <summary>
    /// Extensions to the User Property collection.
    /// </summary>
    public static class ICollectionUserPropertyExtensions
    {
        /// <summary>
        /// Sets the specified property (creates new if not part of the collection already).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="properties">The properties.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="principalService">The principal service.</param>
        public static void Set<T>(this ICollection<UserProperty> properties, string key, T value,
                                  IDateTimeService dateTimeService = null, IPrincipalService principalService = null)
        {
            if (dateTimeService == null)
            {
                dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            }

            if (principalService == null)
            {
                principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();
            }

            var property =
                properties.SingleOrDefault(x => string.Compare(key, x.Key, StringComparison.OrdinalIgnoreCase) == 0);
            if (property == null)
            {
                property = new UserProperty
                    {
                        Key = key,
                        CreatedBy = principalService.CurrentIdentityIdentifier,
                        CreatedOnUtc = dateTimeService.NowUTC,
                    };
                properties.Add(property);
            }

            property.LastModifiedBy = principalService.CurrentIdentityIdentifier;
            property.LastModifiedOnUtc = dateTimeService.NowUTC;
            property.SerializeValue(value);

            return;
        }

        /// <summary>
        /// Gets the properties Typed Value.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="properties"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static TValue Get<TValue>(this ICollection<UserProperty> properties, string key)
        {
            var property =
                properties.SingleOrDefault(x => string.Compare(key, x.Key, StringComparison.OrdinalIgnoreCase) == 0);

            if (property == null)
            {
                
            }
            TValue result = property.DeserializeValue<TValue>();

            return result;
        }

        /// <summary>
        /// Deletes the Property from the User Property collection.
        /// </summary>
        /// <param name="properties">The properties.</param>
        /// <param name="key">The key.</param>
        /// <param name="repositoryService">The repository service.</param>
        public static void Delete(this ICollection<UserProperty> properties, string key, IRepositoryService repositoryService=null)
        {
            if (repositoryService == null)
            {
                repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            }
            var property = properties.SingleOrDefault(x => string.Compare(key, x.Key, StringComparison.OrdinalIgnoreCase) == 0);
            if (property != null)
            {
                properties.Remove(property);
                repositoryService.DeleteOnCommit(property);
            }

            return;
        }
    }
}
