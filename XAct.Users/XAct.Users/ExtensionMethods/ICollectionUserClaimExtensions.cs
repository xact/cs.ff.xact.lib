﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XAct
{
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Users;

    /// <summary>
    /// Extensions to the User Claim collection.
    /// </summary>
    public static class ICollectionUserClaimExtensions
    {
        /// <summary>
        /// Sets the specified Claim (creates new if not part of the collection already).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="properties">The properties.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="principalService">The principal service.</param>
        public static void Set<T>(this ICollection<UserClaim> properties, string key, T value,
                                  IDateTimeService dateTimeService = null, IPrincipalService principalService = null)
        {
            if (dateTimeService == null)
            {
                dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            }

            if (principalService == null)
            {
                principalService = XAct.DependencyResolver.Current.GetInstance<IPrincipalService>();
            }

            var Claim =
                properties.SingleOrDefault(x => string.Compare(key, x.Key, StringComparison.OrdinalIgnoreCase) == 0);
            if (Claim == null)
            {
                Claim = new UserClaim
                    {
                        Key = key,
                        CreatedBy = principalService.CurrentIdentityIdentifier,
                        CreatedOnUtc = dateTimeService.NowUTC,
                    };
                properties.Add(Claim);
            }

            Claim.LastModifiedBy = principalService.CurrentIdentityIdentifier;
            Claim.LastModifiedOnUtc = dateTimeService.NowUTC;
            Claim.SerializeValue(value);

            return;
        }

        /// <summary>
        /// Deletes the Claim from the User Claim collection.
        /// </summary>
        /// <param name="properties">The properties.</param>
        /// <param name="key">The key.</param>
        /// <param name="repositoryService">The repository service.</param>
        public static void Delete(this ICollection<UserClaim> properties, string key, IRepositoryService repositoryService=null)
        {
            if (repositoryService == null)
            {
                repositoryService = XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();
            }
            var Claim = properties.SingleOrDefault(x => string.Compare(key, x.Key, StringComparison.OrdinalIgnoreCase) == 0);
            if (Claim != null)
            {
                properties.Remove(Claim);
                repositoryService.DeleteOnCommit(Claim);
            }

            return;
        }
    }
}
