﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace XAct.Users
{

    /// <summary>
    /// Contract for a service to retrieve and persist <see cref="User"/>
    /// entities.
    /// </summary>
    public interface ICurrentUserService : ICurrentUserService<User>, IHasXActLibService
    {
    }

    /// <summary>
    /// Contract for a service to retrieve and persist <see cref="User"/>
    /// entities.
    /// <para>
    /// Note how <see cref="User"/> is not the same thing as Identity.
    /// </para>
    /// <para>
    /// Some reasons are:
    /// * User is serialiable, Identity may not be.
    /// * A User can impersonate different Identities as needed, while
    ///   not switching the User -- to which properties can be attached.
    /// </para>
    /// </summary>
    public interface ICurrentUserService<TUser> 
        where TUser : UserBase<TUser>
    {



        /// <summary>
        /// Get the current thread <see cref="IIdentity"/>'s User.
        /// </summary>
        TUser GetUser();

    }

    

}
