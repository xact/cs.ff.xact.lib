namespace XAct.Users
{
    using System;
    using System.Security.Principal;
    using XAct;

    /// <summary>
    /// 
    /// <para>
    /// Note how <see cref="User"/> is not the same thing as Identity.
    /// </para>
    /// <para>
    /// Some reasons are:
    /// * User is serialiable, Identity may not be.
    /// * A User can impersonate different Identities as needed, while
    ///   not switching the User -- to which properties can be attached.
    /// </para>
    /// </summary>
    public interface IUserService : IUserService<User>, IHasXActLibService
    {
        
    }

        /// <summary>
    /// Contract for a service to retrieve and persist <see cref="User"/>
    /// entities.
    /// </summary>
    public interface IUserService<TUser> 
        where TUser : UserBase<TUser>
    {

        /// <summary>
        /// Gets the specified user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TUser GetById(Guid id);


        /// <summary>
        /// Get the current thread <see cref="IIdentity"/>'s User.
        /// </summary>
        TUser GetUser();

    }
}
