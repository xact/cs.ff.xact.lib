﻿namespace XAct.Users.Services.Configuration.Implementations
{
    /// <summary>
    /// Implementation of the <see cref="IUserManagementServiceConfiguration"/>
    /// contract for a singleton configuration object for the services.
    /// </summary>
    public class UserManagementServiceConfiguration : IUserManagementServiceConfiguration
    {
        /// <summary>
        /// Rarely, users can be defined in an app specific manner. Rarely...
        /// </summary>
        public bool UsersAreAppHostSpecific { get; set; }
    }
}