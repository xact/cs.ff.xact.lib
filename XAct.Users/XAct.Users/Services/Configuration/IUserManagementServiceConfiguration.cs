﻿namespace XAct.Users.Services.Configuration
{
    /// <summary>
    /// Contract for a singleton configuration object for the services.
    /// </summary>
    public interface IUserManagementServiceConfiguration : IHasXActLibServiceConfiguration 
    {
        /// <summary>
        /// Rarely, users can be defined in an app specific manner. Rarely...
        /// </summary>
        bool UsersAreAppHostSpecific { get; set; }

    }
}
