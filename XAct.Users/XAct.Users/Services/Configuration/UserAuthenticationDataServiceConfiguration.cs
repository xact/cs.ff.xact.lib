using System;

namespace XAct.Users.Services.Configuration.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="IUserAuthenticationDataServiceConfiguration"/> contract.
    /// </summary>
    public class UserAuthenticationDataServiceConfiguration : IUserAuthenticationDataServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether to use the request context to temporarily cache the entity.
        /// <para>
        /// Saves hitting the Db uneccessarily.
        /// </para>
        /// </summary>
        public bool UseContextToTemporarilyCacheUserAuthenticationDataModel { get; set; }


    }
}