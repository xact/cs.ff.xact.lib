namespace XAct.Users.Services.Configuration
{
    using System;

    /// <summary>
    /// Contract for a singleton configuration package required by 
    /// <see cref="IUserManagementService{TUser}"/>
    /// </summary>
    public interface IUserMembershipManagementServiceConfiguration :IHasServiceConfiguration
    {
    }
}