﻿namespace XAct.Users.Services.Configuration
{
    /// <summary>
    /// Contract for a Configuration object for 
    /// <see cref="IUserAuthenticationDataService"/>
    /// </summary>
    public interface IUserAuthenticationDataServiceConfiguration : IHasXActLibTestServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether to use the request context to temporarily cache the entity.
        /// <para>
        /// Saves hitting the Db uneccessarily.
        /// </para>
        /// </summary>
        bool UseContextToTemporarilyCacheUserAuthenticationDataModel { get; set; }
    }
}