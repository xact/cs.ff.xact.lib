namespace XAct.Services.Implementations
{
    using XAct.Users;

    /// <summary>
    /// 
    /// </summary>
    public interface IUserServiceState : IHasServiceState
    {
        /// <summary>
        /// Gets the current Context' user, if any.
        /// </summary>
        /// <typeparam name="TUser">The type of the user.</typeparam>
        /// <returns></returns>
        TUser GetCurrent<TUser>() where TUser : UserBase<TUser>;
        /// <summary>
        /// Sets the current.
        /// </summary>
        /// <typeparam name="TUser">The type of the user.</typeparam>
        /// <param name="value">The value.</param>
        void SetCurrent<TUser>(TUser value) where TUser : UserBase<TUser>;
    }
}