namespace XAct.Services.Implementations
{
    using XAct.State;
    using XAct.Users;

    /// <summary>
    /// <para>
    /// Note how we keep User separate from Thread Identity.
    /// </para>
    /// <para>
    /// The reason is:
    /// * User is serializable, Some identities are not.
    /// </para>
    /// </summary>
    public class UserServiceState : IUserServiceState
    {
        /// <summary>
        /// The key used to cache the current <see cref="User"/>
        /// within the current context.
        /// </summary>
        public const string ContextKey = "User";

        private readonly IContextStateService _contextStateService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserServiceState"/> class.
        /// </summary>
        /// <param name="contextStateService">The context state service.</param>
        public UserServiceState(IContextStateService contextStateService)
        {
            _contextStateService = contextStateService;
        }

        /// <summary>
        /// Gets the current Context' user, if any.
        /// </summary>
        /// <typeparam name="TUser">The type of the user.</typeparam>
        /// <returns></returns>
        public TUser GetCurrent<TUser>() where TUser : UserBase<TUser>
        {
            return _contextStateService.Items[ContextKey] as TUser;
        }

        /// <summary>
        /// Sets the current.
        /// </summary>
        /// <typeparam name="TUser">The type of the user.</typeparam>
        /// <param name="value">The value.</param>
        public void SetCurrent<TUser>(TUser value) where TUser : UserBase<TUser>
        {
            _contextStateService.Items[ContextKey] = value;
        }
    }
}