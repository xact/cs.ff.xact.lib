using System;
using XAct.Domain.Repositories;
using XAct.Environment;
using XAct.State;
using XAct.Users.Services.Configuration;

namespace XAct.Users.Services.Implementations
{
    /// <summary>
    /// An implementation of the <see cref="IUserAuthenticationDataService" />
    /// contract.
    /// </summary>
    public class UserAuthenticationDataService : UserAuthenticationDataService<Guid>, IUserAuthenticationDataService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAuthenticationDataService{TId}" /> class.
        /// </summary>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="contextStateService">The context state service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="userAuthenticationDataServiceConfiguration">The user authentication data service configuration.</param>
        public UserAuthenticationDataService(
            IRepositoryService repositoryService,
            IContextStateService contextStateService,
            IDateTimeService dateTimeService,
            IUserAuthenticationDataServiceConfiguration userAuthenticationDataServiceConfiguration
            ) :
                base(repositoryService, contextStateService,dateTimeService, userAuthenticationDataServiceConfiguration)
        {

        }


        /// <summary>
        /// Gets the specific record.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        protected override UserAuthenticationDataBase<Guid> InternalGetSingleByUserId(Guid userId)
        {
            var result = _repositoryService.GetSingle<UserAuthenticationData>(x => x.UserId == userId);
            //May be null:
            return result;
        }


        /// <summary>
        /// Persist the specific record.
        /// </summary>
        /// <param name="userAuthenticationData">The user.</param>
        protected override void InternalPersistOnCommit(UserAuthenticationDataBase<Guid> userAuthenticationData)
        {
            UserAuthenticationData x = userAuthenticationData as UserAuthenticationData;

            _repositoryService.PersistOnCommit<UserAuthenticationData, Guid>(x, true);

        }

        /// <summary>
        /// Create a new record to be persisted.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="passwordHash">The password hash.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        protected override UserAuthenticationDataBase<Guid> InternalCreate(Guid userId, string passwordHash, bool enabled=true)
        {
            var result = new UserAuthenticationData();
            result.Enabled = enabled;
            result.UserId = userId;
            result.PasswordHash = passwordHash;
            result.LastModifiedOnUtc = _dateTimeService.NowUTC;
            result.CurrentFailedAttempts = 0;

            return result;
        }
    }






    /// <summary>
    /// An abstract base implementation of the
    /// <see cref="IUserAuthenticationDataService{TId}" />
    /// contract
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public abstract class UserAuthenticationDataService<TId> : IUserAuthenticationDataService<TId>
        where TId : struct
    {

        /// <summary>
        /// The Context key used to store references to the UserAuthenticationData.
        /// </summary>
        protected const string _UserAuthenticationDataContextKey = "UserAuthenticationData:";


        /// <summary>
        /// The _repository service
        /// </summary>
        protected readonly IRepositoryService _repositoryService;

        /// <summary>
        /// The context service where the record is persisted briefly
        /// in order to not have to rehit the db.
        /// </summary>
        protected readonly IContextStateService _contextStateService;

        /// <summary>
        /// The DateTimeService.
        /// </summary>
        protected readonly IDateTimeService _dateTimeService;


        /// <summary>
        /// Gets the configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public IUserAuthenticationDataServiceConfiguration Configuration { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="UserAuthenticationDataService{TId}" /> class.
        /// </summary>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="contextStateService">The context state service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="userAuthenticationDataServiceConfiguration">The user authentication data service configuration.</param>
        protected UserAuthenticationDataService(
            IRepositoryService repositoryService,
            IContextStateService contextStateService,
            IDateTimeService dateTimeService,
            IUserAuthenticationDataServiceConfiguration userAuthenticationDataServiceConfiguration
            )
        {
            Configuration = userAuthenticationDataServiceConfiguration;
            _repositoryService = repositoryService;
            _contextStateService = contextStateService;
            _dateTimeService = dateTimeService;
        }

        /// <summary>
        /// Abstract method to be implemented by implementations of this
        /// base class.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        protected abstract UserAuthenticationDataBase<TId> InternalGetSingleByUserId(TId userId);


        /// <summary>
        /// After making changes to the Entity, ensure the changes will be persisted
        /// upon UnitOfWorkService's Commit being invoked.
        /// </summary>
        /// <param name="userAuthenticationData">The user.</param>
        protected abstract void InternalPersistOnCommit(UserAuthenticationDataBase<TId> userAuthenticationData);

        /// <summary>
        /// Create a new record to be persisted.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="passwordHash">The password hash.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <returns></returns>
        protected abstract UserAuthenticationDataBase<TId> InternalCreate(TId userId, string passwordHash, bool enabled=true);

        /// <summary>
        /// After making changes to the Entity, ensure the changes will be persisted
        /// upon UnitOfWorkService's Commit being invoked.
        /// </summary>
        /// <param name="userAuthenticationData">The user.</param>
        protected void PersistOnCommit(UserAuthenticationDataBase<TId> userAuthenticationData)
        {
            userAuthenticationData.LastModifiedOnUtc = _dateTimeService.NowUTC;
            InternalPersistOnCommit(userAuthenticationData);
        }

        /// <summary>
        /// Creates and persists the specified user authentication data record.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="passwordHash">The password hash.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        public void Create(TId userId, string passwordHash, bool enabled=true)
        {
            var userAuthenticationData = InternalCreate(userId, passwordHash,enabled);
            InternalPersistOnCommit(userAuthenticationData);
        }



        /// <summary>
        /// Gets whether the User can sign in or not.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        public bool GetEnabled(TId userId)
        {
            var model = GetContextSavedEntity(userId);
            var result = GetEnabled(model);
            return result;
        }

        /// <summary>
        /// Gets whether the User can sign in or not.
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        /// <returns></returns>
        public bool GetEnabled(UserAuthenticationDataBase<TId> userAuthenticationData)
        {
            var result = userAuthenticationData.Enabled;
            return result;
        }

        /// <summary>
        /// Sets whether the user can sign in or not.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        public void SetEnabled(TId userId, bool enabled)
        {
            var model = GetContextSavedEntity(userId);
            SetEnabled(model, enabled);
        }


        /// <summary>
        /// Sets whether the user can sign in or not.
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        public void SetEnabled(UserAuthenticationDataBase<TId> userAuthenticationData, bool enabled)
        {
            userAuthenticationData.Enabled = enabled;
            PersistOnCommit(userAuthenticationData);
        }




        /// <summary>
        /// Resets the failed attempt counter to zero.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        public int GetFailedAttemptCounter(TId userId)
        {
            var model = GetContextSavedEntity(userId);
            var result = GetFailedAttemptCounter(model);
            return result;
        }



        /// <summary>
        /// Resets the failed attempt counter.
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        public int GetFailedAttemptCounter(UserAuthenticationDataBase<TId> userAuthenticationData)
        {
            var result = userAuthenticationData.CurrentFailedAttempts;
            return result;
        }




        /// <summary>
        /// Increments the failed attempt counter.
        /// </summary>
        public int IncrementFailedAttemptCounter(TId userId)
        {
            var model = GetContextSavedEntity(userId);
            var result = IncrementFailedAttemptCounter(model);
            return result;
        }


        /// <summary>
        /// Increments the failed attempt counter.
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        public int IncrementFailedAttemptCounter(UserAuthenticationDataBase<TId> userAuthenticationData)
        {
            userAuthenticationData.CurrentFailedAttempts++;
            userAuthenticationData.TotalFailedAttempts++;

            PersistOnCommit(userAuthenticationData);

            return userAuthenticationData.CurrentFailedAttempts;
        }




        /// <summary>
        /// Resets the failed attempt counter to zero.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="resetTotalCounterAsWell">if set to <c>true</c> [reset total counter as well].</param>
        public void ResetFailedAttemptCounterAndLockoutDateTimeUtc(TId userId,bool resetTotalCounterAsWell)
        {
            var model = GetContextSavedEntity(userId);
            ResetFailedAttemptCounterAndLockoutDateTimeUtc(model, resetTotalCounterAsWell);
        }



        /// <summary>
        /// Resets the failed attempt counter.
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        /// <param name="resetTotalCounterAsWell">if set to <c>true</c> [reset total counter as well].</param>
        public void ResetFailedAttemptCounterAndLockoutDateTimeUtc(UserAuthenticationDataBase<TId> userAuthenticationData, bool resetTotalCounterAsWell)
        {
            userAuthenticationData.CurrentFailedAttempts = 0;
            userAuthenticationData.LastModifiedOnUtc = null;

            if (resetTotalCounterAsWell)
            {
                userAuthenticationData.TotalFailedAttempts = 0;
            }

            PersistOnCommit(userAuthenticationData);
        }



        /// <summary>
        /// Gets the UTC datetime until when the account is locked out
        /// (if in the past, the user is not locked out).
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public DateTimeOffset? GetLockoutEndDateTimeUtc(TId userId)
        {
            var model = GetContextSavedEntity(userId);
            var result = GetLockoutEndDateTimeUtc(model);
            return result;
        }




        /// <summary>
        /// Gets the UTC datetime until when the account is locked out
        /// (if in the past, the user is not locked out).
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        /// <returns></returns>
        DateTimeOffset? GetLockoutEndDateTimeUtc(UserAuthenticationDataBase<TId> userAuthenticationData)
        {
            var result = userAuthenticationData.LockedOutTillDateTimeUtc;
            return result;
        }


        /// <summary>
        /// Sets the UTC datetime until which the user cannot log in
        /// (if in the past, the user is not locked out).
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="dateTimeOffset">The date time offset.</param>
        public void SetLockoutEndDateTimeUtc(TId userId, DateTimeOffset? dateTimeOffset)
        {
            var model = GetContextSavedEntity(userId);
            SetLockoutEndDateTimeUtc(model, dateTimeOffset);
        }

        /// <summary>
        /// Sets the UTC datetime until which the user cannot log in
        /// (if in the past, the user is not locked out).
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        /// <param name="dateTimeOffset">The date time offset.</param>
        /// <returns></returns>
        public void SetLockoutEndDateTimeUtc(UserAuthenticationDataBase<TId> userAuthenticationData, DateTimeOffset? dateTimeOffset)
        {
            userAuthenticationData.LockedOutTillDateTimeUtc = dateTimeOffset.HasValue
                ? (DateTime?) dateTimeOffset.Value.UtcDateTime
                : null;

            PersistOnCommit(userAuthenticationData);
        }




        /// <summary>
        /// Gets the user's password hash.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public string GetPasswordHash(TId userId)
        {
            var model = GetContextSavedEntity(userId);
            var result = GetPasswordHash(model);
            return result;
        }

        /// <summary>
        /// Gets the user's password hash.
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        /// <returns></returns>
        public string GetPasswordHash(UserAuthenticationDataBase<TId> userAuthenticationData)
        {
            var result = userAuthenticationData.PasswordHash;
            return result;
        }

        /// <summary>
        /// Sets the user's password hash.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="passwordHash">The password hash.</param>
        public void SetPasswordHash(TId userId, string passwordHash)
        {
            var model = GetContextSavedEntity(userId);
            SetPasswordHash(model, passwordHash);

        }

        /// <summary>
        /// Sets the user's password hash.
        /// </summary>
        /// <param name="userAuthenticationData">The user authentication data.</param>
        /// <param name="passwordHash">The password hash.</param>
        public void SetPasswordHash(UserAuthenticationDataBase<TId> userAuthenticationData, string passwordHash)
        {
            userAuthenticationData.PasswordHash = passwordHash;
            PersistOnCommit(userAuthenticationData);
        }


        









        private UserAuthenticationDataBase<TId> GetContextSavedEntity(TId userId)
        {
            UserAuthenticationDataBase<TId> result;

            if (Configuration.UseContextToTemporarilyCacheUserAuthenticationDataModel)
            {
                string key = _UserAuthenticationDataContextKey + userId;
                result =
                    _contextStateService.Items[key] as UserAuthenticationDataBase<TId>;
                if (result == null)
                {
                    result = InternalGetSingleByUserId(userId);
                    if (result != null)
                    {
                        _contextStateService.Items[key] = result;
                    }
                }
            }
            else
            {
                result = InternalGetSingleByUserId(userId);
            }
            return result;
        }
    }
}
