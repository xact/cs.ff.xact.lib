using System.Security.Principal;
using XAct.Environment;
using XAct.Users;

namespace XAct.Services.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class CurrentUserServiceBase<TUser> : ICurrentUserService<TUser> where TUser : UserBase<TUser>, new()
    {
        private readonly IPrincipalService _principalService;
        private readonly IUserManagementService<TUser> _userManagementService;
        private readonly IUserServiceState _userServiceState;

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentUserServiceBase{TUser}" /> class.
        /// </summary>
        /// <param name="principalService">The principal service.</param>
        /// <param name="userManagementService">The user management service.</param>
        /// <param name="userServiceState">State of the user service.</param>
        protected CurrentUserServiceBase(IPrincipalService principalService,
            IUserManagementService<TUser> userManagementService,
            IUserServiceState userServiceState
            )
        {
            _principalService = principalService;
            _userManagementService = userManagementService;
            _userServiceState = userServiceState;
        }


        /// <summary>
        /// Get the current thread <see cref="IIdentity"/>'s User.
        /// </summary>
        /// <returns></returns>
        public TUser GetUser()
        {
            TUser user = _userServiceState.GetCurrent<TUser>();
            if (user != null)
            {
                return user;
            }
            user = _userManagementService.GetByName(_principalService.CurrentIdentityIdentifier);
            _userServiceState.SetCurrent(user);
            return user;
        }
    }
}