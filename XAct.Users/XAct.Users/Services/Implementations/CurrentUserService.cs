using XAct.Environment;
using XAct.Users;

namespace XAct.Services.Implementations
{
    /// <summary>
    /// An implementation of <see cref="ICurrentUserService"/>
    /// to get the current <see cref="User"/>.
    /// <para>
    /// Note how <see cref="User"/> is not the same thing as Identity.
    /// </para>
    /// <para>
    /// Some reasons are:
    /// * User is serialiable, Identity may not be.
    /// * A User can impersonate different Identities as needed, while
    ///   not switching the User -- to which properties can be attached.
    /// </para>
    /// </summary>
    public class CurrentUserService : CurrentUserServiceBase<User>, ICurrentUserService     {

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentUserService"/> class.
        /// </summary>
        /// <param name="principalService">The principal service.</param>
        /// <param name="userManagementService">The user management service.</param>
        /// <param name="userServiceState">State of the user service.</param>
        public CurrentUserService(IPrincipalService principalService,
    IUserManagementService userManagementService,
    IUserServiceState userServiceState
    ):base(principalService,userManagementService,userServiceState)
        {
        }

    }
}