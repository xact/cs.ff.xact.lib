namespace XAct.Services.Implementations
{
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;
    using XAct.Users;

    /// <summary>
    /// An implementation of <see cref="IUserService"/>
    /// <para>
    /// Note how <see cref="User"/> is not the same thing as Identity.
    /// </para>
    /// <para>
    /// Some reasons are:
    /// * User is serialiable, Identity may not be.
    /// * A User can impersonate different Identities as needed, while
    ///   not switching the User -- to which properties can be attached.
    /// </para>
    /// </summary>
    public class UserService : UserServiceBase<User>, IUserService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        /// <param name="userManagementService">The user management service.</param>
        /// <param name="currentUserService">The current user service.</param>
        public UserService(IUserManagementService userManagementService, ICurrentUserService currentUserService) : 
            base(userManagementService,currentUserService)
        {
        }
    }
}
