namespace XAct.Services.Implementations
{
    using System;
    using XAct.Users;


    /// <summary>
    /// 
    /// </summary>
    public abstract class UserServiceBase<TUser> :IUserService<TUser>
        where TUser : UserBase<TUser>, new()
    {
        private readonly IUserManagementService<TUser> _userManagementService;
        private readonly ICurrentUserService<TUser> _currentUserService;


        /// <summary>
        /// Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        /// <param name="userManagementService">The user management service.</param>
        /// <param name="currentUserService">The current user service.</param>
        protected UserServiceBase(
            IUserManagementService<TUser> userManagementService ,
            ICurrentUserService<TUser> currentUserService)
        {
            _userManagementService = userManagementService;
            _currentUserService = currentUserService;
        }


        /// <summary>
        /// Gets the specified user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TUser GetById(Guid id)
        {

            var result = _userManagementService.GetById(id);

            return result;
        }


        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <returns></returns>
        public TUser GetUser()
        {
            return _currentUserService.GetUser();
        }
    }
}