﻿namespace XAct.Users.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Expressions;
    using XAct.Users;
    using XAct.Users.Services.Configuration;


    /// <summary>
    /// 
    /// <para>
    /// Note how <see cref="User"/> is not the same thing as Identity.
    /// </para>
    /// <para>
    /// Some reasons are:
    /// * User is serialiable, Identity may not be.
    /// * A User can impersonate different Identities as needed, while
    ///   not switching the User -- to which properties can be attached.
    /// </para>
    /// </summary>
    public class UserManagementService : UserManagementService<User>, IUserManagementService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="userManagementServiceConfiguration">The user management service configuration.</param>
        public UserManagementService(ITracingService tracingService,
            IApplicationTennantService applicationTennantService,
                                        IRepositoryService repositoryService,
             IUserManagementServiceConfiguration userManagementServiceConfiguration)
            : base(tracingService, repositoryService, applicationTennantService, userManagementServiceConfiguration)
        {
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TUser">The type of the user.</typeparam>
    public abstract class UserManagementService<TUser> :
        DistributedGuidIdRepositoryServiceBase<TUser>, 
        IUserManagementService<TUser>
        where TUser : UserBase<TUser>, new()
    {
        private readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Configuration for the <see cref="IUserManagementService{TUser}"/>
        /// </summary>
        public IUserManagementServiceConfiguration Configuration { get; private set; }


/// <summary>
/// Initializes a new instance of the <see cref="UserManagementService{TUser}" /> class.
/// </summary>
/// <param name="tracingService">The tracing service.</param>
/// <param name="repositoryService">The repository service.</param>
/// <param name="applicationTennantService">The application tennant service.</param>
/// <param name="userManagementServiceConfiguration">The user management service configuration.</param>
        protected UserManagementService(ITracingService tracingService,
                                        IRepositoryService repositoryService,
            IApplicationTennantService applicationTennantService,
            IUserManagementServiceConfiguration userManagementServiceConfiguration
            )
            : base(tracingService, repositoryService)
        {
            _applicationTennantService = applicationTennantService;
            Configuration = userManagementServiceConfiguration;
        }

        /// <summary>
        /// Gets user by its Id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override TUser GetById(Guid id)
        {
            var result = _repositoryService.GetSingle(IdEquality(id), new IncludeSpecification<TUser>(x => x.Properties, x => x.Claims));

            return result;

        }

        /// <summary>
        /// Gets user by its Id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="includeClaimsAndProperties">if set to <c>true</c> [include claims and properties].</param>
        /// <returns></returns>
        public TUser GetById(Guid id, bool includeClaimsAndProperties)
        {
            TUser result;
            if (includeClaimsAndProperties)
            {
                result = _repositoryService.GetSingle(IdEquality(id),

                                                      new IncludeSpecification<TUser>(x => x.Properties,
                                                                                      x => x.Claims));
            }
            else
            {
                result = _repositoryService.GetSingle(IdEquality(id));

            }

            return result;

        }


        /// <summary>
        /// Gets the name of the user by.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="includeClaimsAndProperties">if set to <c>true</c> [include claims and properties].</param>
        /// <param name="includeAllowedDelegates">if set to <c>true</c> [include allowed delegates].</param>
        /// <returns></returns>
        public TUser GetByName(string userName, bool includeClaimsAndProperties = true, bool includeAllowedDelegates = false)
        {
            //ApplicationHost?

            var includes = new List<Expression<Func<TUser, object>>> ();
            if (includeClaimsAndProperties)
            {
                includes.Add(x=>x.Properties);
                includes.Add(x => x.Claims);
            }
            if (includeAllowedDelegates)
            {
                includes.Add(x => x.AllowedDelegates);
            }
            var predicate = BuildWhereUserPredicate(userName);
            var result = includes.Count>0 
                ? _repositoryService.GetSingle<TUser>(predicate, new IncludeSpecification<TUser>(includes.ToArray())) 
                : _repositoryService.GetSingle<TUser>(predicate);

            return result;
        }



        /// <summary>
        /// Creates a new User record, and registers it to be persisted upon next Commit.
        /// </summary>
        /// <param name="user">The user.</param>
        public void CreateUser(TUser user)
        {
            _repositoryService.PersistOnCommit<TUser, Guid>(user);
        }

        /// <summary>
        /// Disables the specified user (setting it's Enabled flag to false).
        /// </summary>
        /// <param name="user"></param>
        public void DisableUser(TUser user)
        {
            user.Enabled = false;
            _repositoryService.UpdateOnCommit(user);
        }

        /// <summary>
        /// Enable the specified user (setting its Enabled flag to true)
        /// </summary>
        /// <param name="user"></param>
        public void EnableUser(TUser user)
        {
            user.Enabled = true;
            _repositoryService.UpdateOnCommit(user);
        }

        /// <summary>
        /// Deletes the specified user if it exists.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        public void DeleteOnCommit(string userIdentifier)
        {
            var user = GetByName(userIdentifier);

            if (user == null)
            {
                return;
            }
            _repositoryService.DeleteOnCommit<TUser>(user);

        }

        /// <summary>
        /// Determines whether the specified delegate is valid for the specified user.
        /// </summary>
        /// <param name="userName">The user identifier.</param>
        /// <param name="delegator">The delegate user identifier.</param>
        public bool IsUserAnAllowedDelegateFor(string userName, string delegator)
        {
            //var predicate = BuildWhereUserPredicate(delegator);

            //Didn't work, and don't have time to keep on looknig
            //var result =_repositoryService
            //    .GetByFilter<TUser>(predicate,
            //                     new IncludeSpecification<User>(x => x.AllowedDelegates))
            //    .Any(x => x.AllowedDelegates.Select(y=>y.Name).Contains(userName));
#if DEBUG
            var user = this.GetByName(delegator,true,true);
            if (user == null)
            {
                return false;
            }

            return user.AllowedDelegates.Any(x => string.Compare(x.Name, userName,StringComparison.OrdinalIgnoreCase)==0);
#endif
        }


        /// <summary>
        /// Adds the user as delegate.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="delegateUserIdentifier">The delegate user identifier.</param>
        /// <returns></returns>
        public bool AddUserAsDelegate(string userName, string delegateUserIdentifier)
        {
            var user = this.GetByName(userName,true,true);
            var delegateUser = GetByName(delegateUserIdentifier);


            if (user.AllowedDelegates.Any(x => x.Id == delegateUser.Id))
            {
                return false;
            }
            
            user.AllowedDelegates.Add(delegateUser);


            _repositoryService.PersistOnCommit<TUser,Guid>(user);
            return true;
        }

        /// <summary>
        /// Removes the user as delegate.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="delegateUserIdentifier">The delegate user identifier.</param>
        /// <returns></returns>
        public bool RemoveUserAsDelegate(string userName, string delegateUserIdentifier)
        {
            var user = this.GetByName(userName, true, true);
            var delegateUser = GetByName(delegateUserIdentifier);

            var d =
                user.AllowedDelegates.SingleOrDefault(
                    x => string.Compare(x.Name, delegateUserIdentifier, StringComparison.OrdinalIgnoreCase) == 0);

            var sameEntity = delegateUser == d;
            Debug.Assert(sameEntity,"Same Entity");

            if (user.AllowedDelegates.All(x => x.Id != delegateUser.Id))
            {
                return false;
            }

            user.AllowedDelegates.Remove(d);

            _repositoryService.PersistOnCommit<TUser, Guid>(user);
            return true;
        }


        private Expression<Func<TUser, bool>> BuildWhereUserPredicate(string userName)
        {
            Expression<Func<TUser, bool>> predicate =
                (x) => x.Name == userName;

            if (this.Configuration.UsersAreAppHostSpecific)
            {
                var applicationHostId = _applicationTennantService.Get();
                predicate =
                    PredicateBuilder.And<TUser>(
                        predicate,
                        x => ((IHasApplicationTennantId) x).ApplicationTennantId == applicationHostId);
            }

            return predicate;
        }
    }

}