﻿using System;
using XAct.Users.Services.Configuration;

namespace XAct.Users.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUserAuthenticationDataService : IUserAuthenticationDataService<Guid>
    {
        
    }



    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IUserAuthenticationDataService<TId> : IHasXActLibService
        where TId : struct
    {

        /// <summary>
        /// Gets the configuration for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        IUserAuthenticationDataServiceConfiguration Configuration { get; }


        /// <summary>
        /// Creates and persists the specified user authentication data record.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="passwordHash">The password hash.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        void Create(TId userId, string passwordHash, bool enabled = true);

        /// <summary>
        /// Gets whether the User can sign in or not.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        bool GetEnabled(TId userId);

        /// <summary>
        /// Sets whether the user can sign in or not.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        void SetEnabled(TId userId, bool enabled);

        /// <summary>
        /// Increments the failed attempt counter.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        int GetFailedAttemptCounter(TId userId);


        /// <summary>
        /// Increments the failed attempt counter.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        int IncrementFailedAttemptCounter(TId userId);

        /// <summary>
        /// Resets the failed attempt counter to zero.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="resetTotalCounterAsWell">if set to <c>true</c> [reset total counter as well].</param>
        void ResetFailedAttemptCounterAndLockoutDateTimeUtc(TId userId, bool resetTotalCounterAsWell);


        /// <summary>
        /// Gets the UTC datetime until when the account is locked out
        /// (if in the past, the user is not locked out).
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        DateTimeOffset? GetLockoutEndDateTimeUtc(TId userId);

        /// <summary>
        /// Sets the UTC datetime until which the user cannot log in
        /// (if in the past, the user is not locked out).
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="dateTimeOffset">The date time offset.</param>
        void SetLockoutEndDateTimeUtc(TId userId, DateTimeOffset? dateTimeOffset);



        /// <summary>
        /// Gets the user's password hash.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        string GetPasswordHash(TId userId);

        /// <summary>
        /// Sets the user's password hash.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="passwordHash">The password hash.</param>
        void SetPasswordHash(TId userId, string passwordHash);

    }
}
