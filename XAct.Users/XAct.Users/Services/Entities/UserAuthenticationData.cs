﻿using System;
using System.Runtime.Serialization;

namespace XAct.Users
{
    /// <summary>
    /// Default implementation
    /// </summary>
    [DataContract]
    public class UserAuthenticationData : UserAuthenticationDataBase<Guid>, IHasDistributedGuidId
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="UserAuthenticationData"/> class.
        /// </summary>
        public UserAuthenticationData()
        {
            this.GenerateDistributedId();
        }

        
    }


    /// <summary>
    /// Base abstract class definition.
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public abstract class UserAuthenticationDataBase<TId> : IHasId<TId>, IHasTimestamp, IHasEnabled, IHasDateTimeModifiedOnUtc, IHasXActLibEntity where TId :struct
    {


        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasId{TId}" />.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual TId Id { get; set; }


        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// <see cref="UserAuthenticationDataBase{TId}"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets the FK of the <see cref="UserBase{TId}"/>  to whch this AuthenticationData applies.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public virtual TId UserId { get; set; }



        /// <summary>
        /// Gets or sets the number of failed attempts
        /// since <see cref="LockedOutTillDateTimeUtc"/>
        /// was reset.
        /// </summary>
        [DataMember]
        public virtual int CurrentFailedAttempts { get; set; }


        /// <summary>
        /// Gets or sets the total number of failed attempts
        /// since last successful.
        /// </summary>
        [DataMember]
        public virtual int TotalFailedAttempts { get; set; }



        /// <summary>
        /// Gets or sets the future datetime (UTC) till which this user is locked out.
        /// </summary>
        [DataMember]
        public virtual DateTime? LockedOutTillDateTimeUtc { get; set; }


        /// <summary>
        /// Gets or sets the hash of the password.
        /// </summary>
        [DataMember]
        public virtual string PasswordHash { get; set; }



        /// <summary>
        /// Gets or sets the last date/time (UTC) when user attempted to sign 
        /// in/signed in.
        /// </summary>
        /// <value>
        /// The last attemp date time UTC.
        /// </value>
        [DataMember]
        public virtual DateTime? LastModifiedOnUtc { get; set; }







        /// <summary>
        /// Initializes a new instance of the <see cref="UserAuthenticationDataBase{TId}"/> class.
        /// </summary>
        protected UserAuthenticationDataBase()
        {
            Enabled = true;

        }

    }
}
