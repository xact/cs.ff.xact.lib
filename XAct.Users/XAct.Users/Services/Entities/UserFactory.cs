namespace XAct
{
    using XAct.Environment;
    using XAct.Users;

    /// <summary>
    /// A factory for building basic users.
    /// <para>
    /// Tip: 
    /// As <see cref="User"/>s are simply Messages, it's really 
    /// just a place to hang on Unit Test extension methods.
    /// </para>
    /// </summary>
    public class UserFactory
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IPrincipalService _principalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserFactory"/> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="principalService">The principal service.</param>
        public UserFactory(IDateTimeService dateTimeService, IPrincipalService principalService)
        {
            _dateTimeService = dateTimeService;
            _principalService = principalService;
        }

        /// <summary>
        /// Gets the default instance of the user factory.
        /// </summary>
        /// <value>
        /// The default.
        /// </value>
        public static UserFactory Default
        {
            get
            {
                return _default ?? (_default = XAct.DependencyResolver.Current.GetInstance<UserFactory>());
            }
        }

        private static UserFactory _default;

        /// <summary>
        /// Build a new <see cref="User"/> model.
        /// </summary>
        /// <returns></returns>
        public static User Build()
        {
            return UserFactory.Default.BuildUser();
        }

        /// <summary>
        /// Builds the user.
        /// </summary>
        /// <returns></returns>
        public User BuildUser()
        {
            var result = new User();
            result.CreatedOnUtc = _dateTimeService.NowUTC;
            result.CreatedBy = _principalService.CurrentIdentityIdentifier;
            result.LastModifiedOnUtc = result.CreatedOnUtc;
            result.LastModifiedBy = result.CreatedBy;

            return result;
        }
    }
}