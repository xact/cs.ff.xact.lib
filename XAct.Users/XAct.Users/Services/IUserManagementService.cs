﻿namespace XAct.Users
{
    using System;
    using XAct.Domain.Repositories;

    /// <summary>
    /// 
    /// <para>
    /// Note how <see cref="User"/> is not the same thing as Identity.
    /// </para>
    /// <para>
    /// Some reasons are:
    /// * User is serialiable, Identity may not be.
    /// * A User can impersonate different Identities as needed, while
    ///   not switching the User -- to which properties can be attached.
    /// </para>
    /// </summary>
    public interface IUserManagementService : IUserManagementService<User>, IHasXActLibService
    {
        
    }

    /// <summary>
    /// 
    /// <para>
    /// Note how <see cref="User"/> is not the same thing as Identity.
    /// </para>
    /// <para>
    /// Some reasons are:
    /// * User is serialiable, Identity may not be.
    /// * A User can impersonate different Identities as needed, while
    ///   not switching the User -- to which properties can be attached.
    /// </para>
    /// </summary>
    /// <typeparam name="TUser">The type of the user.</typeparam>
    public interface IUserManagementService<TUser> : ISimpleRepository<TUser, Guid>
        where TUser : UserBase<TUser>, new()
    {


        

        /// <summary>
        /// Gets user by its Id.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="includeClaimsAndProperties">if set to <c>true</c> [include claims and properties].</param>
        /// <returns></returns>
        TUser GetById(Guid name, bool includeClaimsAndProperties);

        /// <summary>
        /// Gets the User by its <c>UserName</c>.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="includeClaimsAndProperties">if set to <c>true</c> [include claims and properties].</param>
        /// <param name="includeAllowedDelegates">if set to <c>true</c> [include allowed delegates].</param>
        /// <returns></returns>
        TUser GetByName(string name, bool includeClaimsAndProperties=true, bool includeAllowedDelegates=false);


        /// <summary>
        /// Creates a new User record, and registers it to be persisted upon next Commit.
        /// </summary>
        /// <param name="user">The user.</param>
        void CreateUser(TUser user);


        /// <summary>
        /// Disables the specified user (setting it's Enabled flag to false).
        /// </summary>
        /// <param name="user"></param>
        void DisableUser(TUser user);

        /// <summary>
        /// Enable the specified user (setting its Enabled flag to true)
        /// </summary>
        /// <param name="user"></param>
        void EnableUser(TUser user);



        /// <summary>
        /// Deletes the specified user if it exists.
        /// </summary>
        /// <param name="userIdentifier"></param>
        void DeleteOnCommit(string userIdentifier);


        /// <summary>
        /// Determines whether [is user an allowed delegate for] [the specified user name].
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="delegator">The delegator.</param>
        bool IsUserAnAllowedDelegateFor(string userName, string delegator);

        /// <summary>
        /// Adds the <paramref name="delegateUserIdentifier"/>
        /// as a delegate for the specified <paramref name="userName"/>
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="delegateUserIdentifier">The delegate user identifier.</param>
        /// <returns></returns>
        bool AddUserAsDelegate(string userName, string delegateUserIdentifier);

        /// <summary>
        /// Removes the <paramref name="delegateUserIdentifier"/>
        /// as a valid delegate for the specified <paramref name="userName"/>
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="delegateUserIdentifier"></param>
        bool RemoveUserAsDelegate(string userName, string delegateUserIdentifier);

    }
}