﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace XAct.Users.Services
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public interface IUserAuthenticationDataService : IUserAuthenticationDataService<Guid>
//    {

//    }



//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="TId">The type of the identifier.</typeparam>
//    public interface IUserAuthenticationDataService<TId> : IHasXActLibService
//        where TId : struct
//    {

//        /// <summary>
//        /// Increments the failed attempt counter.
//        /// </summary>
//        /// <param name="userId">The user identifier.</param>
//        void IncrementFailedAttemptCounter(TId userId);

//        /// <summary>
//        /// Resets the failed attempt counter to zero.
//        /// </summary>
//        /// <param name="userId">The user identifier.</param>
//        void ResetFailedAttemptCounter(TId userId);
//    }
//}
