namespace XAct.Users.Reputation.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Metrics;
    using XAct.Tests;
    using XAct.Users.Reputation.Services.Configuration;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class ReputationServiceTests
    {
        private string _fakeUserId = "ssigal";


        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            IUserReputationManagementServiceConfiguration reputationServiceConfguration =
    XAct.DependencyResolver.Current.GetInstance<IUserReputationManagementServiceConfiguration>();

            reputationServiceConfguration.Scores.Add(0,"Member");
            reputationServiceConfguration.Scores.Add(10, "Participant");
            reputationServiceConfguration.Scores.Add(100, "XPert");
            reputationServiceConfguration.Scores.Add(500, "FrikkinGenius");

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_ReputationManagementService()
        {

            IUserReputationManagementService reputationManagementService =
                XAct.DependencyResolver.Current.GetInstance<IUserReputationManagementService>();

            Assert.IsNotNull(reputationManagementService);
        }

        [Test]
        public void Can_Get_ReputationService()
        {
            IUserReputationService reputationService =
                XAct.DependencyResolver.Current.GetInstance<IUserReputationService>();

            Assert.IsNotNull(reputationService);
        }


        [Test]
        public void Can_Get_CounterService()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<ICounterService>();

            Assert.IsNotNull(service);
        }

        [Test]
        public void Can_Offset_Reputation_Of_A_User()
        {

            IUserReputationManagementService reputationManagementService =
                XAct.DependencyResolver.Current.GetInstance<IUserReputationManagementService>();

            KeyValue<int,string> score =  reputationManagementService.GetValue(_fakeUserId);

            reputationManagementService.OffsetValue(_fakeUserId,2);

            KeyValue<int, string> score2 = reputationManagementService.GetValue(_fakeUserId);

            Assert.AreEqual(score.Key+2,score2.Key);
        }

        [Test]
        public void Can_Offset_Reputation_And_Get_Varied_Reputation_Terms()
        {

            IUserReputationManagementService reputationManagementService =
                XAct.DependencyResolver.Current.GetInstance<IUserReputationManagementService>();

            string name = "someKid";

            KeyValue<int, string> score;
            score = reputationManagementService.GetValue(name);
            Assert.AreEqual("Member", score.Value);

            
            reputationManagementService.OffsetValue(name, 5);
            score = reputationManagementService.GetValue(name);
            Assert.AreEqual("Member", score.Value);

            reputationManagementService.OffsetValue(name, 5);
            score = reputationManagementService.GetValue(name);
            Assert.AreEqual("Participant", score.Value);


            reputationManagementService.OffsetValue(name, 110);
            score = reputationManagementService.GetValue(name);
            Assert.AreEqual("XPert", score.Value);


            reputationManagementService.OffsetValue(name, 1000);
            score = reputationManagementService.GetValue(name);
            Assert.AreEqual("FrikkinGenius", score.Value);

            
        }

    }
}


