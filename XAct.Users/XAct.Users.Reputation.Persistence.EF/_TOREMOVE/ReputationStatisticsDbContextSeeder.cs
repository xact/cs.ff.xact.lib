//namespace XAct.Users.Reputation.Services.Initialization
//{
//    using System;
//    using XAct.Data.EF.CodeFirst;
//    using XAct.Diagnostics;

//    /// <summary>
//    /// An implementation of the <see cref="IReputationStatisticsDbContextSeeder"/>
//    /// </summary>
//    public class ReputationStatisticsDbContextSeeder : XActLibDbContextSeederBase<ReputationStatistics>, IReputationStatisticsDbContextSeeder
//    {
//        public ReputationStatisticsDbContextSeeder(ITracingService tracingService) : base(tracingService)
//        {
//        }

//        public override void SeedInternal<TContext>(TContext dbContext)
//        {
//            //Do nothing.
//        }
//    }
//}