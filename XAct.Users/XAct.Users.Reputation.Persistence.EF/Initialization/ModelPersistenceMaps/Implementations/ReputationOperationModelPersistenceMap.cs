﻿namespace XAct.Users.Reputation.Initialization.ModelPersistenceMaps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// 
    /// </summary>
    public class ReputationOperationModelPersistenceMap : EntityTypeConfiguration<UserReputationOperation>, IReputationOperationModelPersistenceMap
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ReputationOperationModelPersistenceMap"/> class.
        /// </summary>
        public ReputationOperationModelPersistenceMap()
        {

            this.ToXActLibTable("ReputationOperation");


            // Composite key
            this
                .HasKey(m => m.Key);


            int colOrder = 0;

            this
                .Property(m => m.ApplicationTennantId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Key)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Value)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);


            this
                .Property(m => m.Title)
                .IsOptional()
                .HasMaxLength(1024)
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Text)
                //(50lines x 80chars) Should be sufficient. Any longer and you're waffling. They want Help Not Tolstoy. 
                .DefineOptional4000CharText(colOrder++);
            ;

            this
                .Property(m => m.ResourceFilter)
                .DefineOptional256CharResourceFilter(colOrder++)
                ;

            //this
            //    .Property(m => m.TitleResourceKey)
            //    .IsOptional()
            //    .HasMaxLength(256)
            //    .HasColumnOrder(colOrder++)
            //    ;

            //this
            //    .Property(m => m.ResourceKey)
            //    .IsRequired()
            //    .HasMaxLength(256)
            //    .HasColumnOrder(colOrder++)
            //    ;

        }
    }
}