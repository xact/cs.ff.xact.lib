﻿// ReSharper disable CheckNamespace
namespace XAct.Users.Reputation.Initialization.Implementations
// ReSharper restore CheckNamespace
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Services;
    using XAct.Users.Reputation.Initialization.ModelPersistenceMaps;

    /// <summary>
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class ReputationDbModelBuilder : XActLibServiceBase, IReputationDbModelBuilder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ReputationDbModelBuilder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ReputationDbModelBuilder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<UserReputationOperation>)XAct.DependencyResolver.Current.GetInstance<IReputationOperationModelPersistenceMap>());
        }
    }
}
