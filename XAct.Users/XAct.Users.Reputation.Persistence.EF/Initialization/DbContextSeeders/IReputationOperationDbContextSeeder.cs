namespace XAct.Users.Reputation.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{ReputationOperation}"/>
    /// to seed the <c>MethodAuthorization</c> tables with default data.
    /// </summary>
        //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IReputationOperationDbContextSeeder : IHasXActLibDbContextSeeder<UserReputationOperation>
    {

    }
}