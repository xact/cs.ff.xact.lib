﻿namespace XAct.Users.Reputation.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="IReputationOperationDbContextSeeder"/> contract
    /// to seed the <c>MethodAuthorization</c> tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class ReputationOperationDbContextSeeder : XActLibDbContextSeederBase<UserReputationOperation>, IReputationOperationDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ReputationOperationDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ReputationOperationDbContextSeeder(ITracingService tracingService):base(tracingService)
        {
        }


        public override void CreateEntities()
        {
        }
    }
}
