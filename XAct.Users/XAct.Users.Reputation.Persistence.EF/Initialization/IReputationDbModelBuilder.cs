// ReSharper disable CheckNamespace
namespace XAct.Users.Reputation.Initialization
// ReSharper restore CheckNamespace
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for the <see cref="IHasXActLibDbModelBuilder"/>
    /// specific to setting up XActLib MethodAuthorization capabilities.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface IReputationDbModelBuilder : IHasXActLibDbModelBuilder
    {

    }
}