namespace XAct.Users.News.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Environment.Services;
    using XAct.Messages;
    using XAct.Users.News;
    using XAct.Users.News.Services.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class NewsEntryManagementServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            //YayaAttribute.BeforeTest();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetINewsManagementService()
        {
            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            Assert.IsNotNull(newsManagementService);
        }

        [Test]
        public void CanGetINewsManagementServiceOfExpectedType()
        {
            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            Assert.AreEqual(typeof(NewsManagementService), newsManagementService.GetType());
        }

        [Test]
        public void RetrieveJanuarysNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(0, results.Count());
        }

        [Test]
        public void RetrieveMarchNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 3, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(1, results.Count());
        }

        [Test]
        public void RetrieveAprilNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 4, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(0, results.Count());
        }

        [Test]
        public void RetrieveJuneNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 6, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(2, results.Count());
        }

        [Test]
        public void RetrieveSeptNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 9, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(1, results.Count());
        }
        [Test]
        public void RetrieveOctNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 10, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(0, results.Count());
        }


        [Test]
        public void RetrieveDecNewsAsNeutral()
        {
            IApplicationTennantManagementService applicationTennantManagementService = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>();

            applicationTennantManagementService.Set(Guid.Empty);

            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(1, results.Count());
        }

        [Test]
        public void RetrieveDecNewsAsCorp1()
        {
            IApplicationTennantManagementService applicationTennantManagementService = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>();

            applicationTennantManagementService.Set(1.ToGuid());

            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();
            applicationTennantManagementService.Set(Guid.Empty);

            Assert.AreEqual(2, results.Count());
        }


        [Test]
        public void RetrieveDecNewsAsCorpB()
        {
            IApplicationTennantManagementService applicationTennantManagementService = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>();

            applicationTennantManagementService.Set(2.ToGuid());

            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();
            applicationTennantManagementService.Set(Guid.Empty);

            Assert.AreEqual(2, results.Count());
        }


        [Test]
        public void RetrieveJanOfNextYear()
        {
            IApplicationTennantManagementService applicationTennantManagementService = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>();

            applicationTennantManagementService.Set(2.ToGuid());

            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc).AddMonths(1));

            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();

            var results = newsManagementService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();
            applicationTennantManagementService.Set(Guid.Empty);

            Assert.AreEqual(0, results.Count());
        }


        [Test]
        public void CanGetCategory()
        {
            INewsManagementService newsManagementService =
                XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();


            string catName = "Foo";

            NewsCategory category = newsManagementService.Categories.GetByText(catName);

            Assert.IsNotNull(category);
        }


        public void CanAddNewItem()
        {
            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();
            IUnitOfWorkService unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();


            string catName = "Foo";
            
            NewsCategory category = newsManagementService.Categories.GetByText(catName);

            
            NewsEntry newsEntry = new NewsEntry();

            newsEntry.Category = category;
            newsEntry.Subject = "Halloween News";
            newsEntry.Body = "Get scary at Foo Bar...etc.";
            newsEntry.StartDateTimeUtc = new DateTime(DateTime.Now.Year + 1, 12, 1);
            newsEntry.EndDateTimeUtc = newsEntry.StartDateTimeUtc.Value.MonthEnd();

            newsManagementService.PersistOnCommit(newsEntry);

            unitOfWorkService.GetCurrent().Commit();

            Assert.IsTrue(true);

        }


        public void CanRetrieveNewItem()
        {
            //IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            INewsManagementService newsManagementService = XAct.DependencyResolver.Current.GetInstance<INewsManagementService>();
            IUnitOfWorkService unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();


            string catName = "Foo";

            NewsCategory category = newsManagementService.Categories.GetByText(catName);


            NewsEntry newsEntry = new NewsEntry();

            newsEntry.Category = category;
            newsEntry.Subject = "Halloween News";
            newsEntry.Body = "Get scary at Foo Bar...etc.";
            newsEntry.StartDateTimeUtc = new DateTime(DateTime.Now.Year + 1, 12, 1);
            newsEntry.EndDateTimeUtc = newsEntry.StartDateTimeUtc.Value.MonthEnd();

            newsManagementService.PersistOnCommit(newsEntry);

            unitOfWorkService.GetCurrent().Commit();


            var copy = newsManagementService.GetById(newsEntry.Id);

            Assert.IsNotNull(copy);

        }

    }
}