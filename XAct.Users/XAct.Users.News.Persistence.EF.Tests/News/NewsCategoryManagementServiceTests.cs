    using System;
    using NUnit.Framework;
    using XAct.Domain.Repositories;
    using XAct.Users.News;
    using XAct.Users.News.Services.Implementations;
    using XAct.Tests;

/// <summary>
///   NUNit Test Fixture.
/// </summary>
[TestFixture(Description = "Some Fixture")]
public class NewsCategoryManagementServiceTests
{
    /// <summary>
    ///   Sets up to do before any tests 
    ///   within this test fixture are run.
    /// </summary>
    [TestFixtureSetUp]
    public void TestFixtureSetUp()
    {
        //run once before any tests in this testfixture have been run...
        //...setup vars common to all the upcoming tests...
        //YayaAttribute.BeforeTest();
        InitializeUnitTestDbIoCContextAttribute.BeforeTest();
    }

    /// <summary>
    ///   Tear down after all tests in this fixture.
    /// </summary>
    [TestFixtureTearDown]
    public void TestFixtureTearDown()
    {
        //IoCBootStrapper.Instance =null;
    }


    [TearDown]
    public void MyTestTearDown()
    {
        GC.Collect();
    }


    /// <summary>
    ///   An Example Test.
    /// </summary>
    [Test]
    public void CanGetINewsService()
    {
        INewsCategoryManagementService newsCategoryManagementService = XAct.DependencyResolver.Current.GetInstance<INewsCategoryManagementService>();

        Assert.IsNotNull(newsCategoryManagementService);
    }

    [Test]
    public void CanGetINewsServiceOfExpectedType()
    {
        INewsCategoryManagementService newsCategoryManagementService = XAct.DependencyResolver.Current.GetInstance<INewsCategoryManagementService>();

        Assert.AreEqual(typeof(NewsCategoryManagementService), newsCategoryManagementService.GetType());
    }

    [Test]
    public void CanRetrieveCategory()
    {
        INewsCategoryManagementService newsCategoryManagementService =
            XAct.DependencyResolver.Current.GetInstance<INewsCategoryManagementService>();


        string catName = "Foo";

        NewsCategory category = newsCategoryManagementService.GetByText(catName);

        Assert.IsNotNull(category);

    }

    [Test]
    public void CanPersistNewCategory()
    {
        INewsCategoryManagementService newsCategoryManagementService =
            XAct.DependencyResolver.Current.GetInstance<INewsCategoryManagementService>();
        IUnitOfWorkService unitOfWorkService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();
        NewsCategory newsCategory = new NewsCategory();

        newsCategory.ResourceFilter = string.Empty;
        newsCategory.Text = "NewCat";

        newsCategoryManagementService.PersistOnCommit(newsCategory);

        unitOfWorkService.GetCurrent().Commit();

        Assert.IsTrue(true);

    }



}