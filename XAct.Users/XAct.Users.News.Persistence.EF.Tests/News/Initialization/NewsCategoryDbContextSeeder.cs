namespace XAct.Users.News.Tests
{
    using System.Collections.Generic;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Users.News.Initialization.DbContextSeeders;

    public class NewsCategoryDbContextSeeder : UnitTestXActLibDbContextSeederBase<NewsCategory>, INewsCategoryDbContextSeeder,
                                               IHasMediumBindingPriority
    {
        private readonly IPrincipalService _principalService;

        public NewsCategoryDbContextSeeder(ITracingService tracingService, IPrincipalService principalService, params IDbContextSeeder[] seeders) : base(tracingService, seeders)
        {
            _principalService = principalService;
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<NewsCategory>();



            this.InternalEntities.Add(
                new NewsCategory
                    {
                        ResourceFilter = string.Empty,
                        Text = "Foo",
                        Description = "Foo Description..."
                    }
                );

        }
    }
}