namespace XAct.Users.News.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Users.News;
    using XAct;
    using XAct.Services;
    using XAct.Users.News.Initialization.DbContextSeeders;

    public class NewsEntryDbContextSeeder : UnitTestXActLibDbContextSeederBase<NewsEntry>, INewsEntryDbContextSeeder, IHasMediumBindingPriority
    {
        private readonly INewsCategoryDbContextSeeder _newsCategoryDbContextSeeder;
        private readonly IPrincipalService _principalService;


        /// <summary>
        /// Initializes a new instance of the <see cref="NewsEntryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="newsCategoryDbContextSeeder">The news category database context seeder.</param>
        /// <param name="principalService">The principal service.</param>
        public NewsEntryDbContextSeeder(
            ITracingService tracingService, 
            INewsCategoryDbContextSeeder newsCategoryDbContextSeeder,
            IPrincipalService principalService)
            : base(tracingService, newsCategoryDbContextSeeder)
        {
            _newsCategoryDbContextSeeder = newsCategoryDbContextSeeder;
            _principalService = principalService;
        }

        /// <summary>
        /// Seeds the specified database context.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public override void SeedInternal(DbContext dbContext) 
        {

            SeedInternalHelper(dbContext,true,x=>x.Subject);

        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<NewsEntry>();


            var fooCat =  _newsCategoryDbContextSeeder.Entities.Single(x => x.Text == "Foo");



            var e = new NewsEntry
                                  { 
                                      
                                      ApplicationTennantId = Guid.Empty,
                                      Enabled = true,
                                      CategoryFK = fooCat.Id,
                                      Subject = "March only",
                                      Body = "whoooohooo",
                                      StartDateTimeUtc = new DateTime(DateTime.Now.Year, 3, 1, 0, 0, 0, DateTimeKind.Utc),
                CreatedOnUtc = DateTime.UtcNow,
                                      LastModifiedOnUtc = DateTime.UtcNow,
                                      CreatedBy = _principalService.CurrentIdentityIdentifier,
                                      LastModifiedBy = _principalService.CurrentIdentityIdentifier
                                  };

            e.EndDateTimeUtc = e.StartDateTimeUtc.Value.MonthEnd();
            this.InternalEntities.Add(e);



            e = new NewsEntry
            {

                ApplicationTennantId = Guid.Empty,
                Enabled = true,
                CategoryFK = fooCat.Id,
                Subject = "May thru June",
                Body = "whoooohooo",
                StartDateTimeUtc = new DateTime(DateTime.Now.Year, 5, 1, 0, 0, 0, DateTimeKind.Utc),
                CreatedOnUtc = DateTime.UtcNow,
                LastModifiedOnUtc = DateTime.UtcNow,
                CreatedBy = _principalService.CurrentIdentityIdentifier,
                LastModifiedBy = _principalService.CurrentIdentityIdentifier
                
            };

            e.EndDateTimeUtc = e.StartDateTimeUtc.Value.AddMonths(1).MonthEnd();
            this.InternalEntities.Add(e);


            e = new NewsEntry
            {
                ApplicationTennantId = Guid.Empty,
                Enabled = true,
                CategoryFK = fooCat.Id,
                Subject = "June thru Sept",
                Body = "whoooohooo",
                StartDateTimeUtc = new DateTime(DateTime.Now.Year, 6, 1, 0, 0, 0, DateTimeKind.Utc),
                CreatedOnUtc = DateTime.UtcNow,
                LastModifiedOnUtc = DateTime.UtcNow,
                CreatedBy = _principalService.CurrentIdentityIdentifier,
                LastModifiedBy = _principalService.CurrentIdentityIdentifier
            };

            e.EndDateTimeUtc = e.StartDateTimeUtc.Value.AddMonths(3).MonthEnd();
            this.InternalEntities.Add(e);


            e = new NewsEntry
            {
                ApplicationTennantId = Guid.Empty,
                Enabled = true,
                CategoryFK = fooCat.Id,
                Subject = "Dec As Neutral",
                Body = "whoooohooo",
                StartDateTimeUtc = new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc),
                CreatedOnUtc = DateTime.UtcNow,
                LastModifiedOnUtc = DateTime.UtcNow,
                CreatedBy = _principalService.CurrentIdentityIdentifier,
                LastModifiedBy = _principalService.CurrentIdentityIdentifier
            };

            e.EndDateTimeUtc = e.StartDateTimeUtc.Value.MonthEnd();
            this.InternalEntities.Add(e);

        e = new NewsEntry
            {
                ApplicationTennantId = 1.ToGuid(),
                Enabled = true,
                CategoryFK = fooCat.Id,
                Subject = "Dec As CorpA",
                Body = "whoooohooo",
                StartDateTimeUtc = new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc),
                CreatedOnUtc = DateTime.UtcNow,
                LastModifiedOnUtc = DateTime.UtcNow,
                CreatedBy = _principalService.CurrentIdentityIdentifier,
                LastModifiedBy = _principalService.CurrentIdentityIdentifier
            };

            e.EndDateTimeUtc = e.StartDateTimeUtc.Value.MonthEnd();
            this.InternalEntities.Add(e);

            e = new NewsEntry
            {
                ApplicationTennantId = 2.ToGuid(),
                Enabled = true,
                CategoryFK = fooCat.Id,
                Subject = "Dec As CorpB",
                Body = "whoooohooo",
                StartDateTimeUtc = new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc),
                CreatedOnUtc = DateTime.UtcNow,
                LastModifiedOnUtc = DateTime.UtcNow,
                CreatedBy = _principalService.CurrentIdentityIdentifier,
                LastModifiedBy = _principalService.CurrentIdentityIdentifier
            };

            e.EndDateTimeUtc = e.StartDateTimeUtc.Value.MonthEnd();
            this.InternalEntities.Add(e);

            e = new NewsEntry
            {
                ApplicationTennantId = 2.ToGuid(),
                Enabled = true,
                CategoryFK = fooCat.Id,
                Subject = "Dec As CorpB",
                Body = "whoooohooo",
                StartDateTimeUtc = new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc),
                CreatedOnUtc = DateTime.UtcNow,
                LastModifiedOnUtc = DateTime.UtcNow,
                CreatedBy = _principalService.CurrentIdentityIdentifier,
                LastModifiedBy = _principalService.CurrentIdentityIdentifier
            };

            e.EndDateTimeUtc = e.StartDateTimeUtc.Value.MonthEnd();
            this.InternalEntities.Add(e);

        }


    }


}
