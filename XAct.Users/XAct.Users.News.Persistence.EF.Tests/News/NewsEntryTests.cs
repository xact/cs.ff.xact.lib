namespace XAct.Users.News.Tests
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Users.News;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class NewsEntryTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            Singleton<IocContext>.Instance.ResetIoC();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        //TEST:FAILING
        [Test]
        public void CanCreatNews()
        {
            NewsEntry newsEntry = new NewsEntry();

            newsEntry.Id = Guid.NewGuid();
            newsEntry.ApplicationTennantId = Guid.NewGuid();

            newsEntry.Subject = "Some Event";
            newsEntry.Body = "Some Body...";

            newsEntry.CreatedOnUtc = DateTime.UtcNow;
            newsEntry.CreatedBy = "skys";
            newsEntry.LastModifiedOnUtc = DateTime.UtcNow;
            newsEntry.LastModifiedBy = "skys";

            newsEntry.StartDateTimeUtc = DateTime.UtcNow.AddDays(-3);
            newsEntry.EndDateTimeUtc = DateTime.UtcNow.AddDays(+30);

            Assert.IsNotNull(newsEntry);
        }


        //TEST:FAILING
        [Test]
        public void NewEntryCanRoundTripAcrossTiers()
        {
            NewsEntry newsEntry = new NewsEntry();

            newsEntry.Id = Guid.NewGuid();
            newsEntry.ApplicationTennantId = 1.ToGuid();

            newsEntry.Subject = "Some Event";
            newsEntry.Body = "Some Body...";

            newsEntry.CreatedOnUtc = DateTime.UtcNow;
            newsEntry.CreatedBy = "skys";
            newsEntry.LastModifiedOnUtc = DateTime.UtcNow;
            newsEntry.LastModifiedBy = "skys";

            newsEntry.StartDateTimeUtc = DateTime.UtcNow.AddDays(-3);
            newsEntry.EndDateTimeUtc = DateTime.UtcNow.AddDays(+30);

            string serializedString = newsEntry.ToDataContractString<NewsEntry>();

            NewsEntry newsEntry2 = serializedString.DeserializeFromDataContractSerializedString<NewsEntry>();


            Assert.AreNotEqual(Guid.Empty, newsEntry2.Id, "Id");

            Assert.AreNotEqual(Guid.Empty, newsEntry2.ApplicationTennantId, "Org");

            Assert.AreEqual("Some Event", newsEntry2.Subject, "Subject");
            Assert.AreEqual("Some Body...", newsEntry2.Body,"Body");

            Assert.IsTrue(newsEntry2.CreatedOnUtc.HasValue, "CreatedOn 1");
            Assert.IsTrue(newsEntry2.LastModifiedOnUtc.HasValue, "ModifiedOn 1");
            Assert.AreEqual(DateTime.UtcNow.Date, newsEntry2.CreatedOnUtc.Value.Date, "CreatedOn Date");
            Assert.AreEqual(DateTime.UtcNow.Date, newsEntry2.LastModifiedOnUtc.Value.Date, "ModifiedOn Date");
            Assert.AreEqual("skys", newsEntry2.CreatedBy, "CreatedBy");
            Assert.AreEqual("skys", newsEntry2.LastModifiedBy, "ModifiedBy");

            Assert.AreEqual(DateTime.UtcNow.Date.AddDays(-3), newsEntry2.StartDateTimeUtc.Value.Date, "StartDate");
            Assert.AreEqual(DateTime.UtcNow.Date.AddDays(+30), newsEntry2.EndDateTimeUtc.Value.Date, "EndDate");

        }


    }
}


