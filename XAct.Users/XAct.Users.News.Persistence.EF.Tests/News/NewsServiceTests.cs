namespace XAct.Users.News.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Environment;
    using XAct.Environment.Services;
    using XAct.Messages;
    using XAct.Users.News;
    using XAct.Users.News.Services.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class NewsServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            //YayaAttribute.BeforeTest();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }
        
        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanGetINewsService()
        {
            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            Assert.IsNotNull(newsService);
        }

        [Test]
        public void CanGetINewsServiceOfExpectedType()
        {
            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            Assert.AreEqual(typeof(NewsService), newsService.GetType());
        }

        [Test]
        public void RetrieveJanuarysNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(0, results.Length);
        }

        [Test]
        public void RetrieveMarchNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 3, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(1, results.Length);
        }

        [Test]
        public void RetrieveAprilNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 4, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(0, results.Length);
        }

        [Test]
        public void RetrieveJuneNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 6, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(2, results.Length);
        }

        [Test]
        public void RetrieveSeptNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 9, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(1, results.Length);
        }
        [Test]
        public void RetrieveOctNews()
        {
            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 10, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(0, results.Length);
        }


        [Test]
        public void RetrieveDecNewsAsNeutral()
        {
            IApplicationTennantManagementService applicationTennantManagementService = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>();

            applicationTennantManagementService.Set(Guid.Empty);

            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();

            Assert.AreEqual(1, results.Length);
        }

        [Test]
        public void RetrieveDecNewsAsCorp1()
        {
            IApplicationTennantManagementService applicationTennantManagementService = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>();

            applicationTennantManagementService.Set(1.ToGuid());

            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();
            applicationTennantManagementService.Set(Guid.Empty);

            Assert.AreEqual(2, results.Length);
        }


        [Test]
        public void RetrieveDecNewsAsCorp2()
        {
            IApplicationTennantManagementService applicationTennantManagementService = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>();

            applicationTennantManagementService.Set(2.ToGuid());

            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();
            applicationTennantManagementService.Set(Guid.Empty);

            Assert.AreEqual(2, results.Length);
        }


        [Test]
        public void RetrieveJanOfNextYear()
        {
            IApplicationTennantManagementService applicationTennantManagementService = XAct.DependencyResolver.Current.GetInstance<IApplicationTennantManagementService>();

            applicationTennantManagementService.Set(2.ToGuid());

            IDateTimeService dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            dateTimeService.SetUTCOffset(new DateTime(DateTime.Now.Year, 12, 1, 0, 0, 0, DateTimeKind.Utc).AddMonths(1));

            INewsService newsService = XAct.DependencyResolver.Current.GetInstance<INewsService>();

            var results = newsService.RetrieveNewsItems(new PagedQuerySpecification());

            dateTimeService.ResetUTCOffset();
            applicationTennantManagementService.Set(Guid.Empty);

            Assert.AreEqual(0, results.Length);
        }

    }
}