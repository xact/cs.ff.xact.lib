﻿namespace XAct.Users.Commerce.Implementations
{
    using TwoCheckout;
    using XAct.Commerce.Services.Entities.Transaction;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="ITwoCheckoutService"/>
    /// contract.
    /// </summary>
    [DefaultBindingImplementation(typeof (ITwoCheckoutService))]
    public class TwoCheckoutService : ITwoCheckoutService
    {
        /// <summary>
        /// The Service that calls 2Checkout
        /// </summary>
        private TwoCheckout.AccountService _accountService;

        private ChargeService _chargeService;
        private TwoCheckout.CouponService _couponService;
        private TwoCheckout.NotificationService _notificationService;
        private TwoCheckout.OptionService _optionService;
        private TwoCheckout.ProductService _productService;
        private TwoCheckout.ReturnService _returnService;
        private TwoCheckout.SaleService _saleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TwoCheckoutService"/> class.
        /// </summary>
        public TwoCheckoutService()
        {
            _accountService = new AccountService();
            _chargeService = new ChargeService();
            _couponService = new CouponService();
            _notificationService = new NotificationService();
            _optionService = new OptionService();
            _productService = new ProductService();
            _returnService = new ReturnService();


            _saleService = new SaleService();

        }



        //public void Authorize(Services.Entities.Billing billing)
        //{
        //    //private TwoCheckout.NotificationService;
        //    //    _chargeService.Authorize()
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaseAuthnRequest"></param>
        public PurchaseAuthnRequestAuthorisation Charge(PurchaseAuthnRequest purchaseAuthnRequest)
        {
            TwoCheckout.ChargeAuthorizeServiceOptions chargeAuthorizeServiceOptions = purchaseAuthnRequest.MapTo();

            //Customer.merchantOrderId = "123";

            //Customer.token = "MzIwNzI3ZWQtMjdiNy00NTVhLWFhZTEtZGUyZGQ3MTk1ZDMw";

            //Perform the synchronous operation, 
            //and receive back the Authorisation object
            //which will contain a summary of all that 2Checkout knows:
            Authorization authorization;
            try
            {
                authorization = _chargeService.Authorize(chargeAuthorizeServiceOptions);
                PurchaseAuthnRequestAuthorisation result =
                    authorization.MapTo();
                return result;
            }
            catch (TwoCheckoutException e)
            {
                //Assert.IsInstanceOf<TwoCheckoutException>(e);
                throw;
            }



        }
    }
}