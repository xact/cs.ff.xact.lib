﻿namespace XAct.Users.Commerce.Implementations
{
    using TwoCheckout;
    using XAct.Services;
    using XAct.Users.Commerce.Services.Configuration;

    /// <summary>
    /// An implementation of the 
    /// <see cref="ITwoCheckoutServiceConfiguration"/> contract
    /// to provide a configuration package to an instance of
    /// <see cref="ITwoCheckoutService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof(ITwoCheckoutServiceConfiguration))]
    public class TwoCheckoutServiceConfiguration : ITwoCheckoutServiceConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="TwoCheckoutServiceConfiguration"/> class.
        /// </summary>
        public TwoCheckoutServiceConfiguration()
        {
            TwoCheckoutConfig.ApiUsername = "APIuser1817037";
            TwoCheckoutConfig.ApiPassword = "APIpass1817037";
        }
    }
}