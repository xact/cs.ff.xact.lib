﻿using System;
using System.Linq;
using System.Text;

namespace XAct.Users.Commerce.Services.Entities
{
        using System;

        public class Billing
        {
            public Decimal? amount { get; set; }

            public string bill_method { get; set; }

            public long? billing_id { get; set; }

            public Decimal? customer_amount { get; set; }

            public long? customer_id { get; set; }

            public DateTime? date_deposited { get; set; }

            public DateTime? date_end { get; set; }

            public DateTime? date_fail { get; set; }

            public DateTime? date_next { get; set; }

            public DateTime? date_pending { get; set; }

            public DateTime? date_start { get; set; }

            public long? lineitem_id { get; set; }

            public string recurring_status { get; set; }

            public string status { get; set; }

            public Decimal? usd_amount { get; set; }

            public Decimal? vendor_amount { get; set; }
        }
}



