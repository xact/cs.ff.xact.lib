﻿namespace XAct.Users.Commerce.Services.Enums
{
    /// <summary>
    /// An enumeration of the 
    /// codes that 2Checkout service could return 
    /// after submitting a purchase request.
    /// </summary>
    public enum ErrorCode{
    
        /// <summary>
        /// No Error. The operation was successful.
        /// </summary>
        Ok=0,

        /// <summary>
        /// Seller unauthorized to use the API or incorrect private key.
        /// </summary>
        DeclinedDueToSellerUnanauthorized = 300,

        /// <summary>
        /// Unable to process the request	Incorrect attributes or malformed JSON object.
        /// </summary>
        InvalidToken=200,

        /// <summary>
        ///  parameter error	Missing required attributes or invalid token.
        /// </summary>
        BadRequest = 400,

        /// <summary>
        /// Credit Card failed to authorize.
        /// </summary>
        DeclinedDueToAuthorizationFailure = 600,

        /// <summary>
        ///Payment Authorization Failed:  Please update your cards expiration date and try again, or try another payment method.
        /// </summary>
        DeclinedDueToExpirationDate = 601,

        /// <summary>
        /// Please verify your Credit Card details are entered correctly and try again, or try another payment method.	
        /// </summary>
        DeclinedDueToAuthorizationFailure2 = 602,

        /// <summary>
        /// Your credit card has been declined because of the currency you are attempting to pay in.  
        /// </summary>
        DeclinedDueToInvalidCurrency = 603,


        /// <summary>
        /// Credit is not enabled on this type of card, please contact your bank for more information or try another payment method.
        /// </summary>
        DeclinedDueToCreditNotBeingAuthorizedOnThisCard = 604,


        /// <summary>
        /// Payment Authorization Failed: Invalid transaction type for this credit card, please use a different card and try submitting the payment again, or contact your bank for more information.
        /// </summary>
        DecliendDueToInvalidTransactionType = 605,

        /// <summary>
        /// Credit Card failed to authorize.
        /// </summary>
        DeclinedDueToFailedAuthentication = 606,

        /// <summary>
        /// Credit Card failed to authorize.
        /// </summary>
        DeclinedDueToFailedAuthentication2 = 607,

    }
}
