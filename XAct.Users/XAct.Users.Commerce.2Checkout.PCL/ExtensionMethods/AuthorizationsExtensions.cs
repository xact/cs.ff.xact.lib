﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Commerce.Services.Entities.Transaction;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the returned <see cref="Authorization"/> object.
    /// </summary>
    public static class AuthorizationsExtensions
    {

        /// <summary>
        /// Extension method to map the returned <see cref="Authorization"/>
        /// to a standard <see cref="PurchaseAuthnRequestAuthorisation"/>.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static PurchaseAuthnRequestAuthorisation MapTo(this Authorization source, PurchaseAuthnRequestAuthorisation target=null)
        {
            if (target == null){target = new PurchaseAuthnRequestAuthorisation();}

            target.TransactionId = source.transactionId;
            
            target.Total = source.total.HasValue? source.total.Value:0.00m;

            target.CurrencyCode = source.currencyCode;



            //The reason all the following properties are important to manage,
            //even though in some cases we have them before they were sent off to be
            //processed, is that the system used by many payment processors is to handle
            //the whole cc preparation + address collecting, etc. on their site,
            //and it's only in the finaly returned ACK object that you receive 
            //information as to whether the operation was successful + a copy of the 
            //billing/shipping info that their site collected.

            //ie, how we know what they purchased:
            target.LineItems = source.lineItems.MapTo();

            target.BillingAddress = source.billingAddr.MapTo();

            target.ShippingAddress = source.shippingAddr.MapTo();

            return target;
        } 
    }

    public static class AuthorizationShippingAddressExtensions
    {
        public static PurchaseAuthnRequestAddress MapTo(this AuthShippingAddress source, PurchaseAuthnRequestAddress target = null)
        {
            if (target == null)
            {
                target = new PurchaseAuthnRequestAddress();
            }

            target.Name = source.name;

            target.Address = source.MapTo();

            return target;
        }
    }
}
