﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// 
    /// </summary>
    public static class PurchaseAuthnRecordAddressExtensions
    {
        /// <summary>
        /// Maps the given <see cref="PurchaseAuthnRequestAddress"/>
        /// to the target 2Checkout <see cref="AuthBillingAddress"/>
        /// </summary>
        public static AuthBillingAddress MapTo(this PurchaseAuthnRequestAddress purchaseAuthnRecordAddress,
                                               AuthBillingAddress target = default(AuthBillingAddress))
        {
            if (target == null){target = new AuthBillingAddress();}

            target.name = purchaseAuthnRecordAddress.Name;

            target.email = purchaseAuthnRecordAddress.Email;
            target.phoneNumber = purchaseAuthnRecordAddress.PhoneNumber;
            target.phoneExt = purchaseAuthnRecordAddress.PhoneExt;

            purchaseAuthnRecordAddress.Address.MapTo(target);


            return target;

        }
        /// <summary>
        /// Maps the given <see cref="PurchaseAuthnRequestAddress"/>
        /// to the target 2Checkout <see cref="AuthShippingAddress"/>
        /// </summary>
        public static AuthShippingAddress MapTo(this PurchaseAuthnRequestAddress purchaseAuthnRecordAddress,
                                               AuthShippingAddress target = default(AuthShippingAddress))
        {
            if (target == null) { target = new AuthShippingAddress(); }


            target.name = purchaseAuthnRecordAddress.Name;

            //Note that 2Checkout doesn't use the whole information:
            //target.email = purchaseAuthnRecordAddress.Email;
            //target.phoneNumber = purchaseAuthnRecordAddress.PhoneNumber;
            //target.phoneExt = purchaseAuthnRecordAddress.PhoneExt;

            purchaseAuthnRecordAddress.Address.MapTo(target);

            return target;
        }
    }
}