﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the AuthorizationBillingAddress.
    /// </summary>
    public static class AuthorizationBillingAddressExtensions
    {
        /// <summary>
        /// Maps the returned <see cref="Authorization"/>.<see cref="AuthBillingAddress"/>
        /// to a <see cref="PurchaseAuthnRequestAddress"/>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static PurchaseAuthnRequestAddress MapTo(this AuthBillingAddress source, PurchaseAuthnRequestAddress target = null)
        {
            if (target == null)
            {
                target = new PurchaseAuthnRequestAddress();
            }

            target.Name = source.name;

            target.Email = source.email;
            target.PhoneNumber = source.phoneNumber;
            target.PhoneExt = source.phoneExt;

            target.Address = source.MapTo();

            return target;
        }
    }
}