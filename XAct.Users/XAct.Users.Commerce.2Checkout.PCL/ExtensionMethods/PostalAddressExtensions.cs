﻿namespace XAct
{
    using System;
    using System.Linq;
    using TwoCheckout;
    using XAct.Identity.Entities.Location.Postal;

    /// <summary>
    /// Extensions to the <see cref="PostalAddress"/>
    /// that are specific to its use for purchasing
    /// via <c>2Checkout</c>.
    /// </summary>
    public static class PostalAddressExtensions
    {
        private static string[] _countriesRequiringZip =
            new string[]
                {
                    "ARG", "AUS", "BGR", "CAN", "CHN", "CYP", "EGY", "FRA", "IND", "IDN", "ITA", "JPN", "MYS", "MEX", "NLD"
                    , "PAN", "PHL", "POL", "ROU", "RUS", "SRB", "SGP", "ZAF", "ESP", "SWE", "THA", "TUR", "GBR", "USA"
                };


        /// <summary>
        /// Gets a flag indicating whether ZipCode is required (it depends on the country).
        /// </summary>
        /// <param name="postalAddress"></param>
        /// <returns></returns>
        public static bool IsPostalCodeRequired(this PostalAddress postalAddress)
        {
            return _countriesRequiringZip.Any(x=>string.Compare(x,postalAddress.Country,StringComparison.CurrentCultureIgnoreCase)==0);
        }

        /// <summary>
        /// Maps the given <see cref="PostalAddress"/>
        /// to a 2Checkout <see cref="TwoCheckout.AuthBillingAddress"/>.
        /// </summary>
        public static AuthBillingAddress MapTo(this PostalAddress postalAddress, AuthBillingAddress target = default(AuthBillingAddress))
        {
            if (target == null)
            {
                target = new AuthBillingAddress();
            }

            target.addrLine1 = postalAddress.StreetLine1;
            target.addrLine2 = postalAddress.StreetLine1;
            target.city = postalAddress.City;
            target.state = postalAddress.Region;
            target.zipCode = postalAddress.PostalCode;
            target.country = postalAddress.Country;


            return target;

        }


        /// <summary>
        /// Maps the given <see cref="PostalAddress"/>
        /// to a 2Checkout <see cref="TwoCheckout.AuthBillingAddress"/>.
        /// </summary>
        public static AuthShippingAddress MapTo(this PostalAddress postalAddress, AuthShippingAddress target = default(AuthShippingAddress))
        {
            if (target == null)
            {
                target = new AuthShippingAddress();
            }

            target.addrLine1 = postalAddress.StreetLine1;
            target.addrLine2 = postalAddress.StreetLine1;
            target.city = postalAddress.City;
            target.state = postalAddress.Region;
            target.zipCode = postalAddress.PostalCode;
            target.country = postalAddress.Country;


            return target;

        }
    }
}
