﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Commerce.Services.Entities.Transaction;

    /// <summary>
    /// Extensions to the <see cref="PurchaseAuthnRequestLineItemOption"/> object.
    /// </summary>
    public static class PurchaseAuthnRequestLineItemOptionExtensions
    {
        /// <summary>
        /// Maps the given <see cref="PurchaseAuthnRequestLineItemOption"/>
        /// to a <see cref="AuthLineitemOption"/>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static AuthLineitemOption MapTo(this PurchaseAuthnRequestLineItemOption source,
                                               AuthLineitemOption target = default(AuthLineitemOption))
        {

            if (target ==null){target = new AuthLineitemOption();}

            target.optName = source.Label;
            target.optValue = source.Value;
            target.optSurcharge = source.Surcharge;
        }
    }
}