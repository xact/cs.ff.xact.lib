﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Identity.Entities.Location.Postal;
    using XAct.Users.Commerce.Services.Entities;


    /// <summary>
    /// Extensions to the <see cref="AuthShippingAddress"/>
    /// </summary>
    public static class AuthShippingAddressExtensions
    {

        /// <summary>
        /// Maps the given returned <see cref="AuthShippingAddress"/>
        /// to a <see cref="PostalAddress"/>
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
       public static PostalAddress MapTo(this AuthShippingAddress source,
                                                      PostalAddress target = null)
       {
           if (target == null){target = new PostalAddress();}


           target.StreetLine1 = source.addrLine1;
           target.StreetLine2 = source.addrLine2;
           target.Neighbourhood = null;
           target.City = source.city;
           target.Region = source.state;
           target.Country = source.country;

           return target;
       }
    }


}
