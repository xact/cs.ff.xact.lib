﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extension methods for LineItems.
    /// </summary>
    public static class XAuthLineItemExtensions
    {
        public static AuthLineitem MapTo(this  AuthLineItem source, AuthLineitem target = default(AuthLineitem))
        {
            if (target == null)
            {
                target = new AuthLineitem();
            }

            target.duration = source.duration;

            target.name = source.name;
            target.type = source.type;

            target.productId = source.productId;

            target.tangible = source.tangible;

            target.quantity = source.quantity;
            target.price = source.price;


            target.recurrence = source.recurrence;
            target.duration = source.duration;
            target.startupFee = source.startupFee;

            //Hum
            target.options = source.options;


            return target;
        }

    }
}