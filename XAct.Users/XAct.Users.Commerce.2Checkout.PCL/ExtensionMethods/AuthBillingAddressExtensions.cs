﻿namespace XAct
{
    using TwoCheckout;
    using XAct.Identity.Entities.Location.Postal;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the <see cref="AuthBillingAddress"/>
    /// </summary>
    public static class AuthBillingAddressExtensions
    {
        /// <summary>
        /// Maps the given <see cref="AuthBillingAddress"/>
        /// to a <see cref="PurchaseAuthnRequestAddress"/>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static PurchaseAuthnRequestAddress MapTo(this AuthBillingAddress source,
                                                        PurchaseAuthnRequestAddress target = null)
        {

            if (target == null)
            {
                target = new PurchaseAuthnRequestAddress();
            }

            target.Name = source.name;
            target.Email = source.email;
            target.PhoneNumber = source.phoneNumber;
            target.PhoneExt = source.phoneExt;

            target.Address = source.MapTo();
        }


    /// <summary>
        /// Maps the given returned <see cref="AuthBillingAddress"/>
        /// to a <see cref="PostalAddress"/>
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public static PostalAddress MapTo(this AuthBillingAddress source,
                                          PostalAddress target = null)
        {
            if (target == null) { target = new PostalAddress(); }

            target.StreetLine1 = source.addrLine1;
            target.StreetLine2 = source.addrLine2;
            target.Neighbourhood = null;
            target.City = source.city;
            target.Region = source.state;
            target.Country = source.country;

            return target;
        }
    }
}