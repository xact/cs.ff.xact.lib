﻿
namespace XAct
{
    using System;
    using System.Collections.Generic;
    using TwoCheckout;
    using XAct.Commerce.Services.Entities.Transaction;
    using XAct.Messages;

    /// <summary>
    /// Extensions to the <see cref="PurchaseAuthnRequest"/>
    /// </summary>
    public static class PurchaseAuthnRecordExtensions
    {

        /// <summary>
        /// Validate the given <see cref="PurchaseAuthnRequest"/>
        /// </summary>
        /// <param name="purchaseAuthnRecord"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public static bool Validate(PurchaseAuthnRequest purchaseAuthnRecord, IResponse response)
        {
            if ((purchaseAuthnRecord.Total != 0.00m) && (purchaseAuthnRecord.LineItems.Count>0))
            {
                throw new ArgumentException("Do not use Total if giving line items.");
            }

            return true;
        }


        /// <summary>
        /// Maps the given <see cref="PurchaseAuthnRequest"/>
        /// to a 2Checkout <see cref="ChargeAuthorizeServiceOptions"/>
        /// </summary>
        /// <param name="purchaseAuthnRecord"></param>
        /// <param name="chargeAuthorizeServiceOptions"></param>
        /// <returns></returns>
        public static ChargeAuthorizeServiceOptions MapTo(this PurchaseAuthnRequest purchaseAuthnRecord, ChargeAuthorizeServiceOptions chargeAuthorizeServiceOptions = default(ChargeAuthorizeServiceOptions))
        {

//public class ChargeAuthorizeServiceOptions
//{
//    public string sellerId { get; set; }

//    public string privateKey { get; set; }

//    public string token { get; set; }
//  }



            if (chargeAuthorizeServiceOptions == null)
            {
                chargeAuthorizeServiceOptions = new ChargeAuthorizeServiceOptions();
            }



            chargeAuthorizeServiceOptions.merchantOrderId = purchaseAuthnRecord.OrderId;

            chargeAuthorizeServiceOptions.currency = purchaseAuthnRecord.CurrencyCode;

            //Only use if not passing in line items.
            chargeAuthorizeServiceOptions.total = purchaseAuthnRecord.Total;


            purchaseAuthnRecord.BillingInformation.MapTo(chargeAuthorizeServiceOptions.billingAddr);
            purchaseAuthnRecord.ShippingInformation.MapTo(chargeAuthorizeServiceOptions.shippingAddr);

            if (chargeAuthorizeServiceOptions.lineItems == null)
            {
                chargeAuthorizeServiceOptions.lineItems = new List<AuthLineitem>();
            }
            foreach (PurchaseAuthnRequestLineItem purchaseAuthnRequestLineItem in  purchaseAuthnRecord.LineItems)
            {
                chargeAuthorizeServiceOptions.lineItems.Add(purchaseAuthnRequestLineItem.MapTo());
            }



            return chargeAuthorizeServiceOptions;

        }
    }
}
