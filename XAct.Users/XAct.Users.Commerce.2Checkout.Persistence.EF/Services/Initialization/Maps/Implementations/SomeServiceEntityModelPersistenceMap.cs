namespace XAct.Users.Commerce.Services.Initialization.Maps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct;
    using XAct.Services;

    [DefaultBindingImplementation(typeof (ISomeServiceEntityModelPersistenceMap), BindingLifetimeType.SingletonScope,
        Priority.Low)]
    public class SomeServiceEntityModelPersistenceMap : EntityTypeConfiguration<SomeServiceEntity>,
                                                        ISomeServiceEntityModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public SomeServiceEntityModelPersistenceMap()
        {

            this
                .ToTable(
                    "{0}SomeServiceEntity".FormatStringInvariantCulture(
                        XAct.Library.Settings.Db.DefaultXActLibDbTablePrefix ?? string.Empty));

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(colOrder++);


            this.Property(x => x.Timestamp)
                .IsRowVersion()
                .HasColumnOrder(colOrder++);
        }
    }
}
