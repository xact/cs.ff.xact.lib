namespace XAct.Users.Commerce.Services.Initialization.Implementations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct.Users.Commerce;
    using XAct.Data.EF.CodeFirst;
    using XAct.Environment;
    using XAct;
    using XAct.Services;

    [DefaultBindingImplementation(typeof(ISomeServiceContextSeeder))]
#pragma warning disable 1591
    public class SomeServiceContextSeeder : XActLibDbContextSeederBase, ISomeServiceContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SomeServiceContextSeeder"/> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        public SomeServiceContextSeeder(IEnvironmentService environmentService)
        {
            _environmentService = environmentService;
        }

        /// <summary>
        /// Seeds the specified database context.
        /// </summary>
        /// <typeparam name="TContext">The type of the context.</typeparam>
        /// <param name="dbContext">The database context.</param>
        public override void Seed<TContext>(TContext dbContext) /*where TContext : DbContext*/
        {
            //No Seeding required
            //MakeSomeServiceCategoryDefinitions(dbContext);

        }

        //private void MakeSomeServiceCategoryDefinitions<TContext>(TContext dbContext) where TContext : DbContext
        //{

        //    var table1 = dbContext.Set<SomeServiceCategory>();

        //    table1.AddOrUpdate(
        //        x => x.Text,
        //        new SomeServiceCategory
        //            {
        //                UseResourceFilterAndKey = false,
        //                Text = "Example",
        //                Description = "Example Category Description..."
        //            }
        //        );
        //    dbContext.SaveChanges();

        //}


    }


}

