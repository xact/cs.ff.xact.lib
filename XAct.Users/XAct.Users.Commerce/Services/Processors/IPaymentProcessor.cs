﻿namespace XAct.Users.Commerce.Services
{
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Contract for a payment process, managed
    /// by an instance of <see cref="IPaymentProcessingService"/>
    /// </summary>
    public interface IPaymentProcessor : IHasXActLibProcessor
    {
        /// <summary>
        /// Perform a charge operation with the remote Payment Processing service.
        /// Does not persist the result (that's up to the controller/service that invoked it
        /// </summary>
        /// <param name="purchaseAuthnRequest"></param>
        /// <returns></returns>
        TransactionAuthorisationMessage Charge(TransactionAuthorisationRequest purchaseAuthnRequest);

    }
}