﻿namespace XAct.Users.Commerce.Services.Configuration.Implementations
{
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IPaymentProcessingService"/> contract
    ///  for a configuration object for 
    /// initializing an instance of the <see cref="IPaymentProcessingServiceConfiguration"/>
    /// service.
    /// </summary>
    public class PaymentProcessingServiceConfiguration : IPaymentProcessingServiceConfiguration
    {
        /// <summary>
        /// The default processor to use when processing payments.
        /// </summary>
        public IPaymentProcessor DefaultProcessor { get; set; }

    }
}