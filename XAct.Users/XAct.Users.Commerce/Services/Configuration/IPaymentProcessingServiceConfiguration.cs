﻿namespace XAct.Users.Commerce.Services.Configuration
{
    using XAct.Users.Commerce.Services;

    /// <summary>
    /// Contract for a configuration object for
    /// initializing an instance of the <see cref="IPaymentProcessingService" />
    /// service.
    /// </summary>
    public interface IPaymentProcessingServiceConfiguration :IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets the default processor.
        /// </summary>
        /// <value>
        /// The default processor.
        /// </value>
        IPaymentProcessor DefaultProcessor { get; set; }
    }
}