﻿namespace XAct.Users.Commerce.Services
{
    using System;
    using XAct.Domain.Repositories;
    using XAct.Services;
    using XAct.Users.Commerce.Services.Configuration;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// An implementation of the <see cref="IPaymentProcessingService"/>
    /// to process payment transactions via instances of 
    /// <see cref="IPaymentProcessor"/>s.
    /// </summary>
    public class PaymentProcessingService : IPaymentProcessingService
    {
        /// <summary>
        /// Gets this service's singleton configuration package.
        /// </summary>
        public IPaymentProcessingServiceConfiguration Configuration
        {
            get { return _configuration; }
        }

        private readonly IPaymentProcessingServiceConfiguration _configuration;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentProcessingService" /> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="repositoryService">The repository service.</param>
        public PaymentProcessingService(IPaymentProcessingServiceConfiguration configuration,
                                        IRepositoryService repositoryService)
        {
            _configuration = configuration;
            _repositoryService = repositoryService;
        }

        /// <summary>
        /// Process the given <see cref="TransactionAuthorisationRequest" />
        /// against a specified (or default) <see cref="IPaymentProcessor" />
        /// </summary>
        /// <param name="transactionAuthorisationRequest">The purchase authn request.</param>
        /// <param name="paymentProcessor">The payment processor.</param>
        /// <returns></returns>
        public TransactionAuthorisation Charge(TransactionAuthorisationRequest transactionAuthorisationRequest,
                                               IPaymentProcessor paymentProcessor = null)
        {
            if (paymentProcessor == null)
            {
                paymentProcessor = _configuration.DefaultProcessor;
            }

            //transactionAuthorisationRequest.ValidateBillingAddress();

            _repositoryService.PersistOnCommit<TransactionAuthorisationRequest, Guid>(transactionAuthorisationRequest,
                                                                                      true);

            TransactionAuthorisationMessage transactionAuthorisationMessage =
                paymentProcessor.Charge(transactionAuthorisationRequest);



            ProcessAuthorisation(transactionAuthorisationMessage, transactionAuthorisationRequest);

            //Maybe we don't need to return all of it?
            //For now...
            return transactionAuthorisationRequest.Authorisation;
        }


        /// <summary>
        /// Process a returned authorisation
        /// </summary>
        /// <param name="transactionAuthorisationMessage">The transaction authorisation message.</param>
        /// <returns></returns>
        public TransactionAuthorisationRequest ProcessAuthorisation(
            TransactionAuthorisationMessage transactionAuthorisationMessage)
        {
            transactionAuthorisationMessage.ValidateIsNotDefault("transactionAuthorisationMessage");


            TransactionAuthorisation target = new TransactionAuthorisation();

            return ProcessAuthorisation(transactionAuthorisationMessage, null, target);

        }

        //public void Refund(string merchantOrderId)
        //{
        //    TransactionAuthorisationRequest transactionAuthorisationRequest =
        //        GetTransactionAuthorisationRequest(merchantOrderId);

        //    Refund(transactionAuthorisationRequest);
        //}


        /// <summary>
        /// Refunds the specified transaction authorisation request.
        /// </summary>
        /// <param name="transactionAuthorisationRequest">The transaction authorisation request.</param>
        public void Refund(TransactionAuthorisationRequest transactionAuthorisationRequest)
        {
            
            //var history = new TransactionHistory()

            //_repositoryService.PersistOnCommit<TransactionAuthorisationRequest>(transactionAuthorisationRequest);
        }


        


        /// <summary>
        /// Process a returned authorisation
        /// </summary>
        /// <param name="transactionAuthorisationMessage">The transaction authorisation message.</param>
        /// <param name="transactionAuthorisationRequest">The transaction authorisation request.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        private TransactionAuthorisationRequest ProcessAuthorisation(
            TransactionAuthorisationMessage transactionAuthorisationMessage,
            TransactionAuthorisationRequest transactionAuthorisationRequest,
            TransactionAuthorisation target = null)
        {
            transactionAuthorisationMessage.ValidateIsNotDefault("transactionAuthorisationMessage");

            if (target == null)
            {
                target = new TransactionAuthorisation();
            }

            target = transactionAuthorisationMessage.MapTo(target);

            if (transactionAuthorisationRequest == null)
            {
                //We just have the auth, so we want to find the record
                //that initiated this transaction:
                transactionAuthorisationRequest =
                    _repositoryService
                        .GetSingle<TransactionAuthorisationRequest>(
                            x => x.MerchantOrderId == transactionAuthorisationMessage.MerchantOrderId);
            }


            if (transactionAuthorisationMessage == null)
            {
                //We looked, but didn't find it.

                //we have to make a new one:
                transactionAuthorisationRequest = new TransactionAuthorisationRequest();

                transactionAuthorisationRequest.BillingInformation = transactionAuthorisationMessage.BillingAddress;
                transactionAuthorisationRequest.ShippingInformation = transactionAuthorisationMessage.ShippingAddress;
                foreach (var lineItem in transactionAuthorisationMessage.LineItems)
                {
                    transactionAuthorisationRequest.LineItems.Add(lineItem);
                }
            }


            //Whether we found an existing one, or just created a whole new Request object
            //Hook them together:
            transactionAuthorisationRequest.Authorisation = target;

            //Persist the response as well:
            _repositoryService.PersistOnCommit<TransactionAuthorisation, Guid>(target, true);


            return transactionAuthorisationRequest;

        }


        //OrderNumber/SaleId 
        //* n TransactionId/InvoiceId (each recurring payment date will get a different Invoice number)
        //

//To issue a full refund, you are not required to enter a price - a price would only be entered if a partial refund is desired. You would not pass in lineitems in this case, either.

//If you wish to issue a refund for a single lineitem, you would need to first call detail_sale using either the sale_id or invoice_id as the input parameter: https://www.2checkout.com/documentation/api/sales/detail-sale

//The information returned will contain all invoice_id values that exist under the sale_id. Under each invoice_id, there will be at least one lineitem_id value - using the relevant lineitem_id you can then call refund_lineitem to issue a refund for just that lineitem: https://www.2checkout.com/documentation/api/sales/refund-lineitem






        /// <summary>
        /// Gets the <see cref="TransactionAuthorisationRequest"/>
        /// with the specified Id.
        /// </summary>
        public TransactionAuthorisationRequest GetTransactionById(Guid id)
        {
            var result = _repositoryService.GetSingle<TransactionAuthorisationRequest>(x => x.Id == id,
                                                                                       new IncludeSpecification
                                                                                           <
                                                                                           TransactionAuthorisationRequest
                                                                                           >(
                                                                                           x => x.BillingInformation,
                                                                                           x => x.ShippingInformation,
                                                                                           x => x.LineItems,
                                                                                           x => x.Authorisation));

            return result;
        }


        /// <summary>
        /// Gets the <see cref="TransactionAuthorisationRequest" />
        /// with the specified OrderId.
        /// </summary>
        /// <param name="merchantOrderId"></param>
        /// <param name="includeSubObjects"></param>
        /// <returns></returns>
        public TransactionAuthorisationRequest GetTransactionAuthorisationRequest(string merchantOrderId,bool includeSubObjects=true)
        {

            TransactionAuthorisationRequest transactionAuthorisationRequest;

            if (includeSubObjects)
            {
                transactionAuthorisationRequest =
                    _repositoryService
                        .GetSingle<TransactionAuthorisationRequest>(
                            x => x.MerchantOrderId == merchantOrderId,

                            new IncludeSpecification
                                <
                                TransactionAuthorisationRequest
                                >(
                                x => x.BillingInformation,
                                x => x.ShippingInformation,
                                x => x.LineItems,
                                x => x.Authorisation)
                        );
            }
            else
            {
                transactionAuthorisationRequest =
                    _repositoryService
                        .GetSingle<TransactionAuthorisationRequest>(
                            x => x.MerchantOrderId == merchantOrderId
                        );
            }

            return transactionAuthorisationRequest;
        }

    }
}
