﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.Serialization;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// A purchase request.
    /// <para>
    /// The inforrmation within is transmitted to the Payment Processor,
    /// which returns a <see cref="TransactionAuthorisation"/>
    /// which will be associated to this object.
    /// </para>
    /// <para>
    /// In the case of single, one-time purchases, there is only one <see cref="TransactionAuthorisationRequest"/>,
    /// with a unique <see cref="MerchantOrderId"/> (eg: "X123")
    /// and <see cref="MerchantTransactionId"/> (eg: (eg:X123-01")
    /// </para>
    /// <para>
    /// In the case of services, and therefore recurring payments, there is still only
    /// one <see cref="MerchantOrderId"/> (eg:"X123"), but there can be several <see cref="MerchantTransactionId"/>'s (eg: "X123-01", "X123-02", etc.)
    /// </para>
    /// <para>
    /// 
    /// </para>
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionAuthorisationRequest))]
    [KnownType(typeof(TransactionAddress))]
    [KnownType(typeof(TransactionLineItem))]
    [KnownType(typeof(TransactionLineItemOption))]
    [KnownType(typeof(TransactionMerchantInformation))]
    public class TransactionAuthorisationRequest : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasTotal
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// The merchant's Order identity (eg: 'X1234').
        /// <para>
        /// Important: this is a non-unique number 
        /// as recurring payments all have the same parent <see cref="MerchantOrderId"/> 
        /// </para>
        /// <para>
        /// Note that in the case of a single one-time payment, there will be one 
        /// <see cref="MerchantOrderId"/>, and one <see cref="MerchantTransactionId"/>.
        /// </para>
        /// <para>
        /// In the case of recurring payments, there will be one MerchantOrderId,
        /// but *n* MerchantTransactionIds, one for each payment.
        /// </para>
        /// </summary>
        [DataMember]
        public string MerchantOrderId { get; set; }


        /// <summary>
        /// The merchant's Transaction identifier (eg: 'X1234-02')
        /// for the individual transaction.
        /// <para>
        /// The value is Unique (contrary to the the <see cref="MerchantOrderId"/>,
        /// which can be the same over several <see cref="TransactionAuthorisationRequest"/>
        /// </para>
        /// </summary>
        [DataMember]
        public string MerchantTransactionId { get; set; }




        /// <summary>
        /// The currencycode (eg: USD).
        /// </summary>
        [DataMember]
        public virtual string CurrencyCode { get; set; }


        /// <summary>
        /// The amount of the transaction, in the 
        /// currency defined by the 
        /// <see cref="CurrencyCode"/>
        /// </summary>
        [DataMember]
        public virtual decimal Total
        {
            get
            {
                return LineItems.Sum(x => (x.Price*x.Quantity) + x.Options.Sum(y => y.Surcharge));
            }
            set
            {
                //do nothing.
            }
        }



        #region CCInfo 
        /// <summary>
        /// The token containing the credit card info.
        /// </summary>
        public TransactionAuthorisationRequestCCInfo CCInfo
        {
            get { return _ccInfo ?? (_ccInfo = new TransactionAuthorisationRequestCCInfo()); }
        }
        [DataMember]
        private TransactionAuthorisationRequestCCInfo _ccInfo;
        #endregion


        #region Merchant
        /// <summary>
        /// The FK to the <see cref="TransactionMerchantInformation"/>
        /// containing information about the merchant/vendor placing this request.
        /// </summary>
        [DataMember]
        public Guid MerchantFK { get; set; }

        /// <summary>
        /// Information about the Merchant.
        /// </summary>
        [DataMember]
        public TransactionMerchantInformation Merchant { get; set; }

        #endregion



        #region Billing
        /// <summary>
        /// The FK to the <see cref="TransactionAddress"/>
        /// containing billing information.
        /// </summary>
        [DataMember]
        public virtual Guid BillingInformationFK { get; set; }

        /// <summary>
        /// The Billing address (address, phone number at that address).
        /// </summary>
        [DataMember]
        //[ForeignKey("BillingAddressId")]
        public virtual TransactionAddress BillingInformation { get; set; }
        #endregion



        #region Shipping
        /// <summary>
        /// The FK to the <see cref="TransactionAddress"/>
        /// containing shipping information.
        /// </summary>
        [DataMember]
        public virtual Guid? ShippingInformationFK { get; set; }

        /// <summary>
        /// The Shipping information (address, phone number at that address).
        /// </summary>
        [DataMember]
        //[ForeignKey("ShippingAddressId")]
        public virtual TransactionAddress ShippingInformation { get; set; }
        #endregion


        #region LineItems
        /// <summary>
        /// Gets the line items of this <see cref="TransactionAuthorisationRequest"/>.
        /// </summary>
        public virtual ICollection<TransactionLineItem> LineItems { get { return _lineItems ?? (_lineItems = new Collection<TransactionLineItem>()); } }
        [DataMember]
        private ICollection<TransactionLineItem> _lineItems;
        #endregion



        #region Authorisation
        
        /// <summary>
        /// The FK to the <see cref="TransactionAuthorisation"/> associated to this <see cref="TransactionAuthorisationRequest"/>
        /// </summary>
        [DataMember]
        public virtual Guid? AuthorisationFK { get; set; }


        /// <summary>
        /// The <see cref="TransactionAuthorisation"/> associated to this <see cref="TransactionAuthorisationRequest"/>
        /// </summary>
        [DataMember]
        public virtual TransactionAuthorisation Authorisation { get; set; }

        #endregion


                /// <summary>
        /// Initializes a new instance of the <see cref="TransactionAuthorisationRequest"/> class.
        /// </summary>
        public TransactionAuthorisationRequest()
        {
            this.GenerateDistributedId();
        }

    }
}