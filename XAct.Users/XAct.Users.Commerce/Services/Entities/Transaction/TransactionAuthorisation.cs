﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The Authorisation returned for a 
    /// <see cref="TransactionAuthorisationRequest"/>
    /// <para>
    /// This is a Message, that is parsed into a <see cref="TransactionAuthorisation"/>
    /// and -- as needed -- <see cref="TransactionAuthorisationRequest"/>, 
    /// <see cref="TransactionAddress"/>s and <see cref="TransactionLineItem"/>s.
    /// </para>
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionAuthorisation))]
    public class TransactionAuthorisation : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp 
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        //#region Purchase Request
        ///// <summary>
        ///// The FK to the <see cref="TransactionAuthorisationRequest"/>
        ///// that this Authorisation is for.
        ///// </summary>
        //[DataMember]
        //public Guid AuthorisationRequestFK { get; set; }


        ///// <summary>
        ///// The <see cref="TransactionAuthorisationRequest"/>
        ///// that this Authorisation is for.
        ///// </summary>
        //[DataMember]
        //public TransactionAuthorisationRequest AuthorisationRequest { get; set; }
        //#endregion


        /// <summary>
        /// The response code indicating success or failure of 
        /// the operation.
        /// <para>
        /// See <see cref="ResponseText"/>
        /// </para>
        /// </summary>
        [DataMember]
        public PurchaseOperationResponseCode ResponseCode { get; set; }

        /// <summary>
        /// A textual description of the operation outcome.
        /// <para>
        /// See <see cref="ResponseCode"/>
        /// </para>
        /// </summary>
        [DataMember]
        public string ResponseText { get; set; }

        /// <summary>
        /// This is the Payment Processor's Order Number.
        /// <para>
        /// It's often an incremental long, incrementing across
        /// all vendor/merchants (not just the current merchant). 
        /// </para>
        /// <para>
        /// Not sure yet how it relates to <see cref="TransactionId"/>
        /// </para>
        /// </summary>
        [DataMember]
        public string OrderNumber { get; set; }

        /// <summary>
        /// This is the Payment Processor's Transaction Id.
        /// <para>
        /// Not sure yet how it relates to <see cref="OrderNumber"/>
        /// </para>
        /// </summary>
        [DataMember]
        public string TransactionId { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionAuthorisation"/> class.
        /// </summary>
        public TransactionAuthorisation()
        {
            this.GenerateDistributedId();
        }

    }
}