﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// A lineitem within a <see cref="TransactionAuthorisationRequest"/>
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionLineItem))]
    [KnownType(typeof(TransactionLineItemOption))]
    [KnownType(typeof(TransactionAuthorisationRequest))]
    public class TransactionLineItem : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp    
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// The FK to the transaction that owns this LineItem.
        /// </summary>
        [DataMember]
        public virtual Guid TransactionFK { get; set; }


        /// <summary>
        /// ???
        /// </summary>
        [DataMember]
        public virtual LineItemType Type { get; set; }

        /// <summary>
        /// The vendor's reference to the product (eg: 'SOFT123')
        /// </summary>
        [DataMember] 
        public virtual string ProductId { get; set; }

        /// <summary>
        /// Gets or sets whether the product is Tangible
        /// (this affects shipping considerations).
        /// </summary>
        [DataMember] 
        public virtual bool Tangible { get; set; }

        /// <summary>
        /// The name of the product ("FooBar Special")
        /// </summary>
        [DataMember] 
        public virtual string Name { get; set; }

        /// <summary>
        /// The quantity of <see cref="Name"/> being purchased.
        /// </summary>
        [DataMember] 
        public virtual decimal Quantity { get; set; }


        /// <summary>
        /// The price at which a single unit of <see cref="Name"/>
        /// is being sold.
        /// <para>
        /// If <see cref="Recurrence"/> is set, this will be charged
        /// a number of times in the future.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Decimal Price { get; set; }


        /// <summary>
        /// If <see cref="Recurrence"/>,
        /// is set, gets or sets whether there is an initial setup fee.
        /// </summary>
        [DataMember]
        public virtual Decimal? StartupFee { get; set; }


        /// <summary>
        /// Gets or sets the interval type to recur payments, if any.
        /// </summary>
        [DataMember]
        public virtual Recurrence RecurrenceType { get; set; }

        /// <summary>
        /// Gets or sets the recurrence pattern of the payment, if any.
        /// </summary>
        [DataMember]
        public virtual int RecurrenceTypeAmount { get; set; }

        /// <summary>
        /// If <see cref="Recurrence"/> is defined,
        /// this defines how long the Recurrence should continue.
        /// </summary>
        [DataMember]
        public virtual Recurrence RecurrenceDurationType { get; set; }

        /// <summary>
        /// If <see cref="Recurrence"/> is defined,
        /// this defines how <see cref="RecurrenceDurationType"/>.
        /// intervals to repeat
        /// </summary>
        [DataMember]
        public virtual int RecurrenceDurationTypeAmount { get; set; }


        /// <summary>
        /// optional information to  
        /// decribe the LineItem in further detail.
        /// </summary>
        public virtual Collection<TransactionLineItemOption> Options { get { return _options ?? (_options = new Collection<TransactionLineItemOption>()); } }
        [DataMember] Collection<TransactionLineItemOption> _options;



        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionLineItem"/> class.
        /// </summary>
        public TransactionLineItem()
        {
            this.GenerateDistributedId();
        }

    }
}