﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The Message containing the Credit card token 
    /// (or the full information).
    /// <para>
    /// Note that in both cases no information is persisted
    /// in a datastore.
    /// </para>
    /// </summary>
    [DataContract]
    public class TransactionAuthorisationRequestCCInfo 
    {
        /// <summary>
        /// Gets a flag indicating whether to use the token -- or the full information.
        /// </summary>
        [DataMember]
        public virtual bool UseToken { get; set; }

        /// <summary>
        /// Gets or sets the token containing the encoded CC Number, ExpM, ExpY, CNC.
        /// </summary>
        [DataMember]
        public virtual string Token { get; set; }


        /// <summary>
        /// Gets or sets the cc number.
        /// </summary>
        /// <value>
        /// The cc number.
        /// </value>
        [DataMember]
        public virtual string CCNumber { get; set; }

        /// <summary>
        /// Gets or sets the cc expiration month.
        /// </summary>
        /// <value>
        /// The cc expiration month.
        /// </value>
        [DataMember]
        public virtual string CCExpirationMonth { get; set; }

        /// <summary>
        /// Gets or sets the cc expiration year.
        /// </summary>
        /// <value>
        /// The cc expiration year.
        /// </value>
        [DataMember]
        public virtual string CCExpirationYear { get; set; }

        /// <summary>
        /// Gets or sets the cc security number.
        /// </summary>
        /// <value>
        /// The cc security number.
        /// </value>
        [DataMember]
        public virtual string CCSecurityNumber { get; set; }

    }
}