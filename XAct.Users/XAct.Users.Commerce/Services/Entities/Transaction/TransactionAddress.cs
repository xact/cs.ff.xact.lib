﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System;
    using System.Runtime.Serialization;


    /// <summary>
    /// An <see cref="TransactionAuthorisationRequest"/> Address.
    /// <para>
    /// Note that this entity is not just the information
    /// within a PostalAddress, but contains
    /// contact information (name, tel, email) as well.
    /// </para>
    /// <para>
    /// In the case of a shipping address, this contact information
    /// can be used to get in touch for delivery reasons, and
    /// for billing purposes, this contact information is used
    /// to verify the credit card.
    /// </para>
    /// </summary>
    /// <internal>
    /// It's a PurchaseAddress, and not a PurcahseAuthnRequestAddress
    /// as the same entity is used for <see cref="TransactionAuthorisationMessage"/>
    /// </internal>
    [DataContract]
    [KnownType(typeof(TransactionAddress))]
    public class TransactionAddress : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasPostalAddress
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// The name of the person billing information should be sent to.
        /// <para>
        /// When using for the Billing address, 
        /// this should match the Buyer's card name.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// The email address of the person to whom to send billing information to.
        /// </summary>
        [DataMember]
        public virtual string Email { get; set; }


        /// <summary>
        /// Gets or sets the phone number of the physical billing address.
        /// <para>
        /// For billing purposes, used as verification, and/or calling 
        /// if there are issues with the card used.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Phone { get; set; }

        /// <summary>
        /// Gets or sets the phone number extension of the physical billing address.
        /// <para>
        /// For billing purposes, used as verification, and/or calling 
        /// if there are issues with the card used.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string PhoneExt { get; set; }



        /// <summary>
        /// Gets or sets the instructions.
        /// </summary>
        /// <value>
        /// The instructions.
        /// </value>
        [DataMember]
        public virtual string Instructions { get; set; }

        /// <summary>
        /// Gets or sets the first street .
        /// </summary>
        [DataMember]
        public virtual string StreetLine1 { get; set; }

        /// <summary>
        /// Gets or sets optional additional street information.
        /// </summary>
        [DataMember]
        public virtual string StreetLine2 { get; set; }

        /// <summary>
        /// Gets or sets optional information about neighbourhood
        /// (eg: 'Soho').
        /// </summary>
        [DataMember]
        public virtual string Neighbourhood { get; set; }

        /// <summary>
        /// Gets or sets the name of the village, town, or city.
        /// </summary>
        [DataMember]
        public virtual string City { get; set; }

        /// <summary>
        /// Gets or sets the optional name of the region ('State' in the US).
        /// <para>
        /// eg: North Island.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Region { get; set; }

        /// <summary>
        /// Gets or sets the postal code (Zip in the US).
        /// </summary>
        [DataMember]
        public virtual string PostalCode { get; set; }


        /// <summary>
        /// Gets or sets the name of the Country.
        /// </summary>
        [DataMember]
        public virtual string Country { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionAddress"/> class.
        /// </summary>
        public TransactionAddress()
        {
            this.GenerateDistributedId();
        }
    }
}