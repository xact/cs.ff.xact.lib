﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Information about the merchant placing
    /// the <see cref="TransactionAuthorisationRequest"/>
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionMerchantInformation))]
    public class TransactionMerchantInformation : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// The merchant Id as it is at the Payment Processor's.
        /// <para>
        /// Eg: at 2Checkout it might be one thing, at PayPal, it will be another.
        /// </para>
        /// </summary>
        [DataMember]
        public string MerchantId { get; set; }



                /// <summary>
        /// Initializes a new instance of the <see cref="TransactionMerchantInformation"/> class.
        /// </summary>
        public TransactionMerchantInformation()
        {
            this.GenerateDistributedId();
        }


    }
}