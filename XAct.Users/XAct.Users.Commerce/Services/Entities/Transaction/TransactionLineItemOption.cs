﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System;
    using System.Runtime.Serialization;




    /// <summary>
    /// An option that describes a <see cref="TransactionLineItem"/>
    /// in more detail.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TransactionLineItem))]
    [KnownType(typeof(TransactionLineItemOption))]
    public class TransactionLineItemOption : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// The FK to the 
        /// <see cref="TransactionLineItem"/>
        /// that this option describes.
        /// </summary>
        [DataMember]
        public virtual Guid LineItemFK { get; set; }

        /// <summary>
        /// The 
        /// <see cref="TransactionLineItem"/>
        /// that this option describes.
        /// </summary>
        [DataMember]
        public virtual TransactionLineItem LineItem { get; set; }


        /// <summary>
        /// The name of this line item option.
        /// (eg: 'Size')
        /// </summary>
        [DataMember]
        public virtual string Label { get; set; }

        /// <summary>
        /// The value of this line item option.
        /// (eg: 'XL')
        /// </summary>
        [DataMember]
        public virtual string Value { get; set; }


        /// <summary>
        /// The surcharge to apply to the <see cref="LineItem"/>'s <see cref="TransactionLineItem.Price"/>
        /// <para>
        /// For example, the Size=XL, might incur an xtra dollar surcharge.
        /// </para>
        /// <para>
        /// IMPORTANT: this surcharge is applied only once to the lineitem, 
        /// irrespective of the LineItem Quantity.
        /// </para>
        /// <para>
        /// The default is 0.00
        /// </para>
        /// </summary>
        [DataMember]
        public virtual decimal Surcharge { get; set; }
 
       
        
        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionLineItemOption"/> class.
        /// </summary>
        public TransactionLineItemOption()
        {
            this.GenerateDistributedId();
        }

    }
}