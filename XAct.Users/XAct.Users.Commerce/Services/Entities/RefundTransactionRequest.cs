﻿namespace XAct.Users.Commerce.Implementations
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
#pragma warning disable 1591
    public class RefundTransactionRequest :IHasDistributedGuidIdAndTimestamp
    {

        [DataMember]
        public virtual Guid Id { get; set; }

        public virtual byte[] Timestamp { get; set; }

        [DataMember]
        public virtual string SaleId { get; set; }


        [DataMember]
        public virtual string InvoiceId { get; set; }
        
        [DataMember]
        public virtual string Category { get; set; }
        
        [DataMember]
        public virtual string Currency { get; set; }
        
        [DataMember]
        public virtual RefundReason RefundReason { get; set; }
        
        [DataMember]
        public virtual string LineItemId { get; set; }


        [DataMember]
        public virtual string Comment { get; set; }


        public RefundTransactionRequest()
        {
            this.GenerateDistributedId();
        }
    }
#pragma warning restore 1591
}