﻿namespace XAct.Users.Commerce.Implementations
{
    using System.Runtime.Serialization;

    [DataContract]
#pragma warning disable 1591
    public enum RefundReason
    {
        [EnumMember] DidNotReceiveOrder = 1,
        [EnumMember] DidNotLikeItem = 2,
        [EnumMember] ItemNotAsDescribed = 3,
        [EnumMember] Fraud = 4,
        [EnumMember] Other = 5,
        [EnumMember] ItemNotAvailable = 6,
        //[EnumMember] //7	Do Not Use (Internal use only)
        [EnumMember] NoResponseFromMerchant = 8,
        [EnumMember] RecurringLastInstallment = 9,
        [EnumMember] Cancellation = 10,
        [EnumMember] BilledInError = 11,
        [EnumMember] ProhibitedProduct = 12,
        [EnumMember] ServiceRefundedAtMerchantsRequest = 13,
        [EnumMember] NonDelivery = 14,
        [EnumMember] NotAsDescribed = 15,
        [EnumMember] OutOfStock = 16,
        [EnumMember] Duplicate = 17,

    }
#pragma warning restore 1591
}