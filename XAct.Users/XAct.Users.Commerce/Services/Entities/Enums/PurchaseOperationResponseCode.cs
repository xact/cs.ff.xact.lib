﻿

namespace XAct.Users.Commerce.Services.Entities
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The response code indicating the success or failure
    /// of the purchase operation.
    /// </summary>
    [DataContract]
    public enum PurchaseOperationResponseCode
    {
        /// <summary>
        /// No Error. The operation was successful.
        /// </summary>
        [EnumMember]
        Ok = 200,

        /// <summary>
        /// Seller unauthorized to use the API or incorrect private key.
        /// </summary>
        [EnumMember]
        DeclinedDueToSellerUnanauthorized = 300,

        /// <summary>
        /// Unable to process the request	Incorrect attributes or malformed JSON object.
        /// </summary>
        [EnumMember]
        InvalidToken = 200,

        /// <summary>
        ///  parameter error	Missing required attributes or invalid token.
        /// </summary>
        [EnumMember]
        BadRequest = 400,

        /// <summary>
        /// Credit Card failed to authorize.
        /// </summary>
        [EnumMember]
        DeclinedDueToAuthorizationFailure = 600,

        /// <summary>
        ///Payment Authorization Failed:  Please update your cards expiration date and try again, or try another payment method.
        /// </summary>
        [EnumMember]
        DeclinedDueToExpirationDate = 601,

        /// <summary>
        /// Please verify your Credit Card details are entered correctly and try again, or try another payment method.	
        /// </summary>
        [EnumMember]
        DeclinedDueToAuthorizationFailure2 = 602,

        /// <summary>
        /// Your credit card has been declined because of the currency you are attempting to pay in.  
        /// </summary>
        [EnumMember]
        DeclinedDueToInvalidCurrency = 603,


        /// <summary>
        /// Credit is not enabled on this type of card, please contact your bank for more information or try another payment method.
        /// </summary>
        [EnumMember]
        DeclinedDueToCreditNotBeingAuthorizedOnThisCard = 604,


        /// <summary>
        /// Payment Authorization Failed: Invalid transaction type for this credit card, please use a different card and try submitting the payment again, or contact your bank for more information.
        /// </summary>
        [EnumMember]
        DecliendDueToInvalidTransactionType = 605,

        /// <summary>
        /// Credit Card failed to authorize.
        /// </summary>
        [EnumMember]
        DeclinedDueToFailedAuthentication = 606,

        /// <summary>
        /// Credit Card failed to authorize.
        /// </summary>
        [EnumMember]
        DeclinedDueToFailedAuthentication2 = 607,


    }
}
