﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Users.Commerce.Services.Entities
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The type of interval
    /// <para>
    /// Note: the enum values used are the same as <see cref="TimeInterval"/>
    /// </para>
    /// </summary>
    [DataContract]
    public enum Recurrence
    {
        /// <summary>Undefined. Value=0</summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>A Day. Value=9</summary>
        [EnumMember]
        Day = 9,
        
        /// <summary>A Week. Value=10</summary>
        [EnumMember]
        Week = 10,
        
        /// <summary>A month. Value=11</summary>
        [EnumMember]
        Month = 11,
        
        /// <summary>A Quarter. Value=12</summary>
        [EnumMember]
        Quarter = 12,
        
        /// <summary>A Year. Value=13</summary>
        [EnumMember]
        Year = 13,

        /// <summary>An Eternity. Value=20</summary>
        [EnumMember]
        Eternity = int.MaxValue,

    }
}
