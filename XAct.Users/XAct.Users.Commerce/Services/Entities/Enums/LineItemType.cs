﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The type of the <see cref="TransactionLineItem"/>
    /// </summary>
    [DataContract]
    public enum LineItemType
    {
        /// <summary>
        /// The value is unedefined.
        /// <para>
        /// This ia an error state.
        /// </para>
        /// <para>
        /// Value = 0
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined=0,
   
        /// <summary>
        /// The transaction line item describes a product.
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        Product = 1,



        /// <summary>
        /// The transaction line item describes tax applied.
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        [EnumMember]
        Tax = 2,

        /// <summary>
        /// The transaction line item describes shpping costs.
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember]
        Shipping = 3,

        /// <summary>
        /// .
        /// <para>
        /// Value = 4
        /// </para>
        /// </summary>
        [EnumMember]
        Coupon = 4,

    }
}