﻿
namespace XAct.Users.Commerce.Services.Entities
{
    /// <summary>
    /// Contract for elements that have a Total amount.
    /// </summary>
    public interface IHasTotal
    {

        /// <summary>
        /// The total of the transaction (the sum of the amounts in all lineItems + lineimem surcharges).
        /// </summary>
        decimal Total { get; set; }
    }
}
