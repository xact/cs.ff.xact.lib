﻿
namespace XAct.Users.Commerce.Services
{
    using System;
    using XAct.Users.Commerce.Services.Configuration;
    using XAct.Users.Commerce.Services.Entities;


    /// <summary>
    /// Contract for a service
    /// to process payment transactions via instances of 
    /// <see cref="IPaymentProcessor"/>s.
    /// </summary>
    public interface IPaymentProcessingService : IHasXActLibService
    {
        /// <summary>
        /// Gets this service's singleton configuration package.
        /// </summary>
        IPaymentProcessingServiceConfiguration Configuration { get; }

        /// <summary>
        /// Process the given <see cref="TransactionAuthorisationRequest" />
        /// against a specified (or default) <see cref="IPaymentProcessor" />
        /// </summary>
        /// <param name="transactionAuthnRequest">The purchase authn request.</param>
        /// <param name="paymentProcessor">The payment processor.</param>
        /// <returns></returns>
        TransactionAuthorisation Charge(TransactionAuthorisationRequest transactionAuthnRequest,
                                        IPaymentProcessor paymentProcessor = null);



        /// <summary>
        /// Process a returned authorisation
        /// </summary>
        /// <param name="transactionAuthorisationMessage">The transaction authorisation message.</param>
        TransactionAuthorisationRequest ProcessAuthorisation(
            TransactionAuthorisationMessage transactionAuthorisationMessage);

        ///// <summary>
        ///// Process a returned authorisation
        ///// </summary>
        ///// <param name="transactionAuthorisationMessage">The transaction authorisation message.</param>
        ///// <param name="transactionAuthorisationRequest">The transaction authorisation request.</param>
        ///// <param name="target">The target.</param>
        ///// <returns></returns>
        //TransactionAuthorisation ProcessAuthorisation(
        //    TransactionAuthorisationMessage transactionAuthorisationMessage,
        //    TransactionAuthorisationRequest transactionAuthorisationRequest = null,
        //    TransactionAuthorisation target = null);


        /// <summary>
        /// Gets the <see cref="TransactionAuthorisationRequest"/>
        /// with the specified Id.
        /// </summary>
        TransactionAuthorisationRequest GetTransactionById(Guid id);


        /// <summary>
        /// Gets the <see cref="TransactionAuthorisationRequest"/>
        /// with the specified OrderId.
        /// </summary>
        TransactionAuthorisationRequest GetTransactionAuthorisationRequest(string merchantOrderId,
                                                                           bool includeSubObjects = true);

    }
}
