﻿namespace XAct.Users.Commerce.Services.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// The Authorisation returned for a 
    /// <see cref="TransactionAuthorisationRequest"/>
    /// <para>
    /// This is a Message, that is parsed into a <see cref="TransactionAuthorisation"/>
    /// and -- as needed -- <see cref="TransactionAuthorisationRequest"/>, 
    /// <see cref="TransactionAddress"/>s and <see cref="TransactionLineItem"/>s.
    /// </para>
    /// </summary>
    [DataContract]
    public class TransactionAuthorisationMessage  : IHasTotal
    {

        /// <summary>
        /// The response code indicating success or failure of 
        /// the operation.
        /// <para>
        /// See <see cref="ResponseText"/>
        /// </para>
        /// </summary>
        [DataMember]
        public PurchaseOperationResponseCode ResponseCode { get; set; }


        /// <summary>
        /// A textual description of the operation outcome.
        /// <para>
        /// See <see cref="ResponseCode"/>
        /// </para>
        /// </summary>
        [DataMember]
        public string ResponseText { get; set; }


        /// <summary>
        /// The merchant Order Id that matches
        /// up with <see cref="TransactionAuthorisationRequest"/> OrderId.
        /// <para>
        /// Note that in the case of a single one-time payment, there will be one 
        /// MerchantOrderId, and one MerchantTransactionId.
        /// </para>
        /// <para>
        /// In the case of recurring payments, there will be one MerchantOrderId,
        /// but *n* MerchantTransactionIds, one for each payment.
        /// </para>
        /// </summary>
        [DataMember]
        public string MerchantOrderId { get; set; }


        /// <summary>
        /// This is the Payment Processor's Order Number.
        /// <para>
        /// An OrderNumber can have *n* TransactionNumbers associated to it.
        /// </para>
        /// <para>
        /// In the case of single one time payment, there will be one 
        /// Order number, and one Transaction number.
        /// </para>
        /// <para>
        /// In the case of recurring payments, there will be *n* Transaction numbers
        /// all with the same Order number.
        /// </para>
        /// <para>
        /// It's often an incremental long, incrementing across
        /// all vendor/merchants (not just the current merchant). 
        /// </para>
        /// //OrderNumber/SaleId 
        /// * n TransactionId/InvoiceId (each recurring payment date will get a different Invoice number)
        /// </summary>
        [DataMember]
        public string OrderNumber { get; set; }


        /// <summary>
        /// This is the Payment Processor's Transaction Id.
        /// <para>
        /// Not sure yet how it relates to <see cref="OrderNumber"/>
        /// </para>
        /// </summary>
        [DataMember]
        public string TransactionId { get; set; }

        /// <summary>
        /// The CurrencyCode that describes the Currency 
        /// the <see cref="Total"/> was charged in.
        /// <para>
        /// This returned value should match <see cref="TransactionAuthorisationRequest"/> CurrencyCode
        /// </para>
        /// </summary>
        [DataMember]
        public string CurrencyCode { get; set; }


        /// <summary>
        /// The amount charged.
        /// <para>
        /// This returned value should match <see cref="TransactionAuthorisationRequest"/> Total.
        /// </para>
        /// </summary>
        [DataMember]
        public decimal Total { get; set; }


        #region Billing Address

        /// <summary>
        /// The FK to the <see cref="TransactionAddress"/>
        /// that describes the Billing address for this Purchase request.
        /// </summary>
        public Guid BillingAddressFK { get; set; }

        /// <summary>
        /// The Billing address used for the transaction.
        /// </summary>
        /// <internal>
        /// With some payment processing flows, it's tempting to think
        /// that if the information is already available in <see cref="TransactionAuthorisationRequest"/>
        /// that this information is not required.
        /// But some systems handle the whole payment processing sequence
        /// (including the choice of items purchased and addresses) on their
        /// website. It's only in the returned final ACK object (this object)
        /// that you'll be given a chance to know these values -- from these properties.
        /// </internal>
        public TransactionAddress BillingAddress
        {
            get { return _billingAddress; }
            set { _billingAddress = value; }
        }
        [DataMember]
        private TransactionAddress _billingAddress;
        #endregion


        #region Shipping Address

        /// <summary>
        /// The FK to the <see cref="TransactionAddress"/>
        /// that describes the Billing address for this Purchase request.
        /// </summary>
        public Guid ShippingAddressFK { get; set; }

        /// <summary>
        /// The Shipping address used for the transaction.
        /// </summary>
        /// <internal>
        /// With some payment processing flows, it's tempting to think
        /// that if the information is already available in <see cref="TransactionAuthorisationRequest"/>
        /// that this information is not required.
        /// But some systems handle the whole payment processing sequence
        /// (including the choice of items purchased and addresses) on their
        /// website. It's only in the returned final ACK object (this object)
        /// that you'll be given a chance to know these values -- from these properties.
        /// </internal>
        public TransactionAddress ShippingAddress
        {
            get { return _shippingAddress; }
            set { _shippingAddress = value; }
        }
        [DataMember]
        private TransactionAddress _shippingAddress;

        #endregion



        /// <summary>
        /// Gets or sets the line item.
        /// </summary>
        /// <internal>
        /// With some payment processing flows, it's tempting to think
        /// that if the information is already available in <see cref="TransactionAuthorisationRequest"/>
        /// that this information is not required.
        /// But some systems handle the whole payment processing sequence
        /// (including the choice of items purchased and addresses) on their
        /// website. It's only in the returned final ACK object (this object)
        /// that you'll be given a chance to know these values -- from these properties.
        /// </internal>
        public ICollection<TransactionLineItem> LineItems
        {
            get { return _lineItems??(_lineItems = new Collection<TransactionLineItem>()); }
        }
        [DataMember]
        private ICollection<TransactionLineItem> _lineItems;



    }
}