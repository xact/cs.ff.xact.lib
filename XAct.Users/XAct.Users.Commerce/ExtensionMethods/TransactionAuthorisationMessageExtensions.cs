﻿namespace XAct
{
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extensions to the <see cref="TransactionAuthorisationMessage"/>
    /// object.
    /// </summary>
    public static class TransactionAuthorisationMessageExtensions
    {
        /// <summary>
        /// Maps the returned <see cref="TransactionAuthorisationMessage"/>
        /// to a persistable <see cref="TransactionAuthorisation"/>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static TransactionAuthorisation MapTo(
            this TransactionAuthorisationMessage source, TransactionAuthorisation target=null)
        {
            if (target == null)
            {
                target = new TransactionAuthorisation();
            }

            target.OrderNumber = source.OrderNumber;
            target.TransactionId = source.TransactionId;
            target.ResponseCode = source.ResponseCode;
            target.ResponseText = target.ResponseText;

            return target;
        }
    }
}
