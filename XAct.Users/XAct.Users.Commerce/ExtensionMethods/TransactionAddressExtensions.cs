﻿namespace XAct
{
    using System;
    using System.Linq;
    using XAct.Messages;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Extension methods to the <see cref="TransactionAddress"/> object.
    /// </summary>
    public static class TransactionAddressExtensions
    {

        //There may be more, but 2Checkout had already done investigation on this
        //and we can start by using this as a check:
        private static string[] _countriesRequiringZip = new string[]
                {
                    "ARG", "AUS", "BGR", "CAN", "CHN", "CYP", "EGY", "FRA", "IND", "IDN", "ITA", "JPN", "MYS", "MEX", "NLD"
                    , "PAN", "PHL", "POL", "ROU", "RUS", "SRB", "SGP", "ZAF", "ESP", "SWE", "THA", "TUR", "GBR", "USA"
                };


        /// <summary>
        /// Gets a flag indicating whether ZipCode is required (it depends on the country).
        /// </summary>
        /// <param name="transactionAddress"></param>
        /// <returns></returns>
        public static bool IsPostalCodeRequired(this TransactionAddress transactionAddress)
        {
            return _countriesRequiringZip.Any(x => string.Compare(x, transactionAddress.Country, StringComparison.CurrentCultureIgnoreCase) == 0);
        }



        /// <summary>
        /// Validate the required Billing address of a new purchase Transaction.
        /// </summary>
        /// <param name="transactionAddress"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public static bool ValidateBillingAddress(this TransactionAddress transactionAddress, IResponse response)
        {

            if (transactionAddress.Name.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingName);
            }
            if (transactionAddress.Email.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingEmail);
            }
            if (transactionAddress.Phone.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingPhone);
            }

            if (transactionAddress.Country.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingCountry);
            }
            //Check postal code in only some cases.
            if (transactionAddress.PostalCode.IsNullOrEmpty() && transactionAddress.IsPostalCodeRequired())
            {
                return response.AddMessage(MessageCodes.MissingBillingPostalCode);
            }
            if (transactionAddress.Region.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingRegion);
            }
            if (transactionAddress.Country.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingCity);
            }
            if (transactionAddress.Country.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingCountry);
            }

            return response.Success;
        }


        /// <summary>
        /// Validate the Shipping address.
        /// </summary>
        /// <param name="transactionAddress"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public  static bool ValidateShippingAddress(this TransactionAddress transactionAddress, IResponse response)
        {

            if (transactionAddress.Name.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingName);
            }

            //----

            if (transactionAddress.Country.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingCountry);
            }
            //Check postal code in only some cases.
            if (transactionAddress.PostalCode.IsNullOrEmpty() && transactionAddress.IsPostalCodeRequired())
            {
                return response.AddMessage(MessageCodes.MissingBillingPostalCode);
            }
            if (transactionAddress.Region.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingRegion);
            }
            if (transactionAddress.Country.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingCity);
            }
            if (transactionAddress.Country.IsNullOrEmpty())
            {
                return response.AddMessage(MessageCodes.MissingBillingCountry);
            }

            return response.Success;
        }
    


    }
}