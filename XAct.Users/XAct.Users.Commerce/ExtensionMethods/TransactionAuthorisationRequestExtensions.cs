﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct
{
    using XAct.Messages;
    using XAct.Users.Commerce.Services.Entities;

#pragma warning disable 1591
    public class MessageCodes
    {
        public static MessageCode MissingBillingName = new MessageCode(1, Severity.NonBlockingWarning);
        public static MessageCode MissingBillingEmail = new MessageCode(1, Severity.NonBlockingWarning);
        public static MessageCode MissingBillingPhone = new MessageCode(1, Severity.NonBlockingWarning);
        public static MessageCode MissingBillingStreetAddress1 = new MessageCode(1, Severity.NonBlockingWarning);
        public static MessageCode MissingBillingCity = new MessageCode(1, Severity.NonBlockingWarning);
        public static MessageCode MissingBillingPostalCode = new MessageCode(1, Severity.NonBlockingWarning);
        public static MessageCode MissingBillingRegion = new MessageCode(1, Severity.NonBlockingWarning);
        public static MessageCode MissingBillingCountry = new MessageCode(1, Severity.NonBlockingWarning);
    }

#pragma warning restore 1591


    /// <summary>
    /// Extensions to the <see cref="TransactionAuthorisationRequest"/>
    /// object.
    /// </summary>
    public static class TransactionAuthorisationRequestExtensions
    {

        /// <summary>
        /// Validates the specified transaction authorisation request.
        /// </summary>
        /// <param name="transactionAuthorisationRequest">The transaction authorisation request.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        public static bool Validate(this TransactionAuthorisationRequest transactionAuthorisationRequest, IResponse response)
        {
            if (transactionAuthorisationRequest == null)
            {
                return response.AddMessage(MessageCodes.MissingBillingStreetAddress1);
            }

            transactionAuthorisationRequest.BillingInformation.ValidateBillingAddress(response);

            transactionAuthorisationRequest.ShippingInformation.ValidateShippingAddress(response);

            

            return response.Success;
        }
    }
}
