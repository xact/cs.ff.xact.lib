namespace XAct.Commerce.Tests
{
    using System;
    using System.Data.Entity;
    using System.Diagnostics;
    using System.Linq;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Domain.Repositories;
    using XAct.Tests;
    using XAct.Users.Commerce.Implementations;
    using XAct.Users.Commerce.Services;
    using XAct.Users.Commerce.Services.Configuration;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class PaymentProcessingServiceTests
    {

        //Create Token from:
        //http://jsbin.com/davilaxiwu/1/edit
        //Or:
        //http://jsfiddle.net/skysigal/cs7cjbnw/#base

#pragma warning disable 169
        //private string GoodCC = "4000000000000002";
#pragma warning restore 169


        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            XAct.DependencyResolver.Current.GetInstance<IRepositoryService>()
                .GetContext().GetInnerItem<DbContext>()
                .Configuration.ProxyCreationEnabled = false;


            var configuration = 
                XAct.DependencyResolver.Current.GetInstance<IPaymentProcessingService>().Configuration;


            var publicConfiguration =
                XAct.DependencyResolver.Current.GetInstance<ITwoCheckoutPaymentProcessorConfiguration>();


            //901258725 (SS)
            //Private: 14DF547B-AAC3-455E-9C89-127CC5E99627
            //Public: E68FC9F2-8E65-4A7C-A811-316202A670A0

            //901259118 (MB)
            //Private: D36165B0-416B-4258-AFF8-2E91C4010BC1
            //Public: D5E0CEF5-4093-4651-9C6E-60A76010B8F8

            //
            bool useMB = false;
            if (useMB)
            {
                publicConfiguration.Initialize("901259118", "D5E0CEF5-4093-4651-9C6E-60A76010B8F8");

                var privateConfiguration =
                    XAct.DependencyResolver.Current.GetInstance<ITwoCheckoutPaymentProcessorPrivateConfiguration>();

                privateConfiguration.Initialize(true, "machinebrains", "Tnp=tk22co", "D36165B0-416B-4258-AFF8-2E91C4010BC1",
                                                string.Empty);
            }
            else
            {
                publicConfiguration.Initialize("901258725", "E68FC9F2-8E65-4A7C-A811-316202A670A0");

                var privateConfiguration =
                    XAct.DependencyResolver.Current.GetInstance<ITwoCheckoutPaymentProcessorPrivateConfiguration>();

                privateConfiguration.Initialize(true, "skysigal", "Tnp=tk22co", "14DF547B-AAC3-455E-9C89-127CC5E99627",
                                                string.Empty);
            }

            configuration.DefaultProcessor = XAct.DependencyResolver.Current.GetInstance<TwoCheckoutPaymentProcessor>();


        }
        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanGetPaymentProcessingServiceConfiguration()
        {
            var configuration = XAct.DependencyResolver.Current.GetInstance<XAct.Users.Commerce.Services.Configuration.IPaymentProcessingServiceConfiguration>();
            Assert.IsNotNull(configuration);
        }

        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanGetPaymentProcessingService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<XAct.Users.Commerce.Services.IPaymentProcessingService>();
            Assert.IsNotNull(service);
        }

        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanGetPaymentProcessingServiceOfExpectedType()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<XAct.Users.Commerce.Services.IPaymentProcessingService>();
            Assert.AreEqual(typeof(PaymentProcessingService),service.GetType());
        }


        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanGetSomePreviousTransactions()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<XAct.Users.Commerce.Services.IPaymentProcessingService>();

            TransactionAuthorisationRequest request = service.GetTransactionById(1.ToGuid());

            Assert.IsNotNull(request,"Did not find transaction");
        }



        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanProcessTransactionAuthorisationRequest()
        {

            //Create Token from:
            //http://jsbin.com/davilaxiwu/1/edit
            //Or:
            //http://jsfiddle.net/skysigal/cs7cjbnw/#base


            var service = XAct.DependencyResolver.Current.GetInstance<XAct.Users.Commerce.Services.IPaymentProcessingService>();

            var transactionAuthnRequest = new TransactionAuthorisationRequest();

            transactionAuthnRequest.MerchantOrderId = "abc123";

            transactionAuthnRequest.CCInfo.Token = "MTc1N2Y3ZmMtNGI0MC00ZTMzLWFjY2QtZmQ1M2QxMWUxMDFh";

            transactionAuthnRequest.BillingInformation = new TransactionAddress();
            transactionAuthnRequest.BillingInformation.Name = "Testing Tester";
            transactionAuthnRequest.BillingInformation.Email = "example@2co.com";
            transactionAuthnRequest.BillingInformation.Phone = "5555555555";
            transactionAuthnRequest.BillingInformation.PhoneExt = "";
            transactionAuthnRequest.BillingInformation.StreetLine1= "123 Test St";
            transactionAuthnRequest.BillingInformation.StreetLine2 = "";
            transactionAuthnRequest.BillingInformation.Neighbourhood = "";
            transactionAuthnRequest.BillingInformation.City= "Columbus";
            transactionAuthnRequest.BillingInformation.Region = "Ohio";
            transactionAuthnRequest.BillingInformation.PostalCode = "43123";
            transactionAuthnRequest.BillingInformation.Country = "USA";


            //transactionAuthnRequest.ShippingInformation = new TransactionAddress();
            //transactionAuthnRequest.ShippingInformation.Name = "Testing Tester";
            //transactionAuthnRequest.ShippingInformation.Email = "example@2co.com";
            //transactionAuthnRequest.ShippingInformation.PhoneNumber = "5555555555";
            //transactionAuthnRequest.ShippingInformation.PhoneExt = "";
            //transactionAuthnRequest.ShippingInformation.StreetLine1 = "123 Test St";
            //transactionAuthnRequest.ShippingInformation.StreetLine2 = "";
            //transactionAuthnRequest.ShippingInformation.Neighbourhood = "";
            //transactionAuthnRequest.ShippingInformation.City = "Columbus";
            //transactionAuthnRequest.ShippingInformation.Region = "Ohio";
            //transactionAuthnRequest.ShippingInformation.PostalCode = "43123";
            //transactionAuthnRequest.ShippingInformation.Country = "USA";

            transactionAuthnRequest.CurrencyCode = "USD";
            transactionAuthnRequest.Total = 246.90m;


            transactionAuthnRequest.LineItems.Add(new TransactionLineItem
                {
                    ProductId="TEST01",
                    Name = "FooBar",
                    Price = 123.45m,
                    Quantity = 2
                });


            TransactionAuthorisation authorisation = service.Charge(transactionAuthnRequest);

            Assert.IsNotNull(authorisation);
        }

        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanProcessTransactionAuthorisationRequestAndRefund()
        {
            //Create Token from:
            //http://jsbin.com/davilaxiwu/1/edit
            //Or:
            //http://jsfiddle.net/skysigal/cs7cjbnw/#base


            var service = XAct.DependencyResolver.Current.GetInstance<XAct.Users.Commerce.Services.IPaymentProcessingService>();

            var transactionAuthnRequest = new TransactionAuthorisationRequest();

            transactionAuthnRequest.MerchantOrderId = "abc123";

            transactionAuthnRequest.CCInfo.Token = "MTc1N2Y3ZmMtNGI0MC00ZTMzLWFjY2QtZmQ1M2QxMWUxMDFh";

            transactionAuthnRequest.BillingInformation = new TransactionAddress();
            transactionAuthnRequest.BillingInformation.Name = "Testing Tester";
            transactionAuthnRequest.BillingInformation.Email = "example@2co.com";
            transactionAuthnRequest.BillingInformation.Phone = "5555555555";
            transactionAuthnRequest.BillingInformation.PhoneExt = "";
            transactionAuthnRequest.BillingInformation.StreetLine1 = "123 Test St";
            transactionAuthnRequest.BillingInformation.StreetLine2 = "";
            transactionAuthnRequest.BillingInformation.Neighbourhood = "";
            transactionAuthnRequest.BillingInformation.City = "Columbus";
            transactionAuthnRequest.BillingInformation.Region = "Ohio";
            transactionAuthnRequest.BillingInformation.PostalCode = "43123";
            transactionAuthnRequest.BillingInformation.Country = "USA";


            //transactionAuthnRequest.ShippingInformation = new TransactionAddress();
            //transactionAuthnRequest.ShippingInformation.Name = "Testing Tester";
            //transactionAuthnRequest.ShippingInformation.Email = "example@2co.com";
            //transactionAuthnRequest.ShippingInformation.PhoneNumber = "5555555555";
            //transactionAuthnRequest.ShippingInformation.PhoneExt = "";
            //transactionAuthnRequest.ShippingInformation.StreetLine1 = "123 Test St";
            //transactionAuthnRequest.ShippingInformation.StreetLine2 = "";
            //transactionAuthnRequest.ShippingInformation.Neighbourhood = "";
            //transactionAuthnRequest.ShippingInformation.City = "Columbus";
            //transactionAuthnRequest.ShippingInformation.Region = "Ohio";
            //transactionAuthnRequest.ShippingInformation.PostalCode = "43123";
            //transactionAuthnRequest.ShippingInformation.Country = "USA";

            transactionAuthnRequest.CurrencyCode = "USD";
            transactionAuthnRequest.Total = 246.90m;


            transactionAuthnRequest.LineItems.Add(new TransactionLineItem
            {
                ProductId = "TEST01",
                Name = "FooBar",
                Price = 123.45m,
                Quantity = 2
            });


            TransactionAuthorisation authorisation = service.Charge(transactionAuthnRequest);

            Assert.IsNotNull(authorisation);

            //service.Refund

        }

    }
}


