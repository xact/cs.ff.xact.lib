namespace XAct.Commerce.Tests
{
    using System;
    using System.Data.Entity;
    using System.Diagnostics;
    using System.Linq;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Domain.Repositories;
    using XAct.Tests;
    using XAct.Users.Commerce.Services;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class EntitySerializationTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            XAct.DependencyResolver.Current.GetInstance<IRepositoryService>()
                .GetContext().GetInnerItem<DbContext>()
                .Configuration.ProxyCreationEnabled = false;

            

            //DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>().FactoryDelegate =
            //    () => new EntityDbContext(new UnitTestDbContext());

            //Initialize the db using the common initializer passing it one entity from this class
            //so that it can perform a query (therefore create db as necessary):
            //DependencyResolver.Current.GetInstance<IUnitTestDbBootstrapper>().Initialize<ViewModeCaseDefinition>();

        }
        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeTransactionLineItemOption()
        {
            TransactionLineItemOption source = new TransactionLineItemOption();
            source.Label = "foo";
            source.Value = "bar";
            source.Surcharge = 1.23m;
            source.LineItemFK = Guid.NewGuid();

            var target = source.TestWCFSerialization();

            Assert.AreEqual(source.Id,target.Id);
            Assert.AreEqual(source.Label, target.Label);
            Assert.AreEqual(source.Value, target.Value);
            Assert.AreEqual(source.Surcharge, target.Surcharge);
            Assert.AreEqual(source.LineItemFK, target.LineItemFK);
        }

        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeTransactionLineItem()
        {
            TransactionLineItem source = new TransactionLineItem();
            source.Name="foo";
            source.Price = 1.23m;
            source.ProductId = "abc123";
            source.Quantity = 123;
            source.RecurrenceType = Recurrence.Month;
            source.RecurrenceTypeAmount = 2;
            
            source.RecurrenceDurationType = Recurrence.Month;
            source.RecurrenceDurationTypeAmount = 2;

            source.StartupFee = 123.45m;
            source.Tangible = true;
            source.TransactionFK = Guid.NewGuid();
            source.Type = LineItemType.Shipping;



            var target = source.TestWCFSerialization();

            Assert.AreEqual(source.Id, target.Id);
            Assert.AreEqual(source.Name, target.Name);
            Assert.AreEqual(source.Price, target.Price);
            Assert.AreEqual(source.ProductId, target.ProductId);
            Assert.AreEqual(source.Quantity, target.Quantity);
            Assert.AreEqual(source.RecurrenceType, target.RecurrenceType);
            Assert.AreEqual(source.RecurrenceTypeAmount, target.RecurrenceTypeAmount);
            Assert.AreEqual(source.RecurrenceDurationType, target.RecurrenceDurationType);
            Assert.AreEqual(source.RecurrenceDurationTypeAmount, target.RecurrenceDurationTypeAmount);
        }


        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeTransactionAddress()
        {
            TransactionAddress source = new TransactionAddress();


            source.Name = "foo";
            source.Phone = "12345";
            source.PhoneExt = "x123";
            source.Email = "foo@bar.com";
            source.StreetLine1 = "st1";
            source.StreetLine1 = "st2";
            source.Neighbourhood = "n";
            source.City = "c1";
            source.Region = "r1";
            source.PostalCode = "12345";
            source.Country = "co1";



            var target = source.TestWCFSerialization();

            Assert.AreEqual(source.City, target.City);
            Assert.AreEqual(source.Country, target.Country);
            Assert.AreEqual(source.Id, target.Id);
            Assert.AreEqual(source.Email, target.Email);
            Assert.AreEqual(source.Name, target.Name);
            Assert.AreEqual(source.Neighbourhood, target.Neighbourhood);
            Assert.AreEqual(source.PhoneExt, target.PhoneExt);
            Assert.AreEqual(source.Phone, target.Phone);
            Assert.AreEqual(source.PostalCode, target.PostalCode);
            Assert.AreEqual(source.Region, target.Region);
            Assert.AreEqual(source.StreetLine1, target.StreetLine1);
            Assert.AreEqual(source.StreetLine2, target.StreetLine2);
            Assert.AreEqual(source.Timestamp, target.Timestamp);

        }



        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeTransactionAuthorisation()
        {
            TransactionAuthorisation source = new TransactionAuthorisation();

            source.OrderNumber = "123";
            source.ResponseCode = PurchaseOperationResponseCode.BadRequest;
            source.ResponseText = "Foo";
            source.TransactionId = "12345";

            TransactionAuthorisation target = source.TestWCFSerialization();

            Assert.AreEqual(source.OrderNumber,target.OrderNumber);
            Assert.AreEqual(source.ResponseCode,target.ResponseCode);
            Assert.AreEqual(source.ResponseText,target.ResponseText);
            Assert.AreEqual(source.TransactionId,target.TransactionId);

        }


        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeTransactionMerchantInformation()
        {
            TransactionMerchantInformation source = new TransactionMerchantInformation();

            source.MerchantId = Guid.NewGuid().ToString();

            TransactionMerchantInformation target = source.TestWCFSerialization();

            Assert.AreEqual(source.MerchantId,target.MerchantId);
        }


        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeSimpleEntity()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            var id = 1.ToGuid();
            var source = service.GetSingle<TransactionAuthorisationRequest>(x => x.Id == id);


            TransactionAuthorisationRequest target = source.TestWCFSerialization();

            Assert.AreEqual(source.MerchantFK, target.MerchantFK);
            Assert.AreEqual(source.AuthorisationFK, target.AuthorisationFK);
            Assert.AreEqual(source.BillingInformationFK, target.BillingInformationFK);
            Assert.AreEqual(source.CurrencyCode, target.CurrencyCode);
            Assert.AreEqual(source.Id, target.Id);
            Assert.AreEqual(source.MerchantFK, target.MerchantFK);
            Assert.AreEqual(source.MerchantOrderId, target.MerchantOrderId);
            Assert.AreEqual(source.ShippingInformationFK, target.ShippingInformationFK);
            Assert.AreEqual(source.Timestamp, target.Timestamp);
            Assert.AreEqual(source.Total, target.Total);
        }



        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeSimpleTransactionAddress()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            var id = 1.ToGuid();
            var tmp = service.GetSingle<TransactionAuthorisationRequest>(x => x.Id == id, new IncludeSpecification<TransactionAuthorisationRequest>(x => x.BillingInformation));

            Guid addressId = tmp.BillingInformationFK;

            var source = service.GetSingle<TransactionAddress>(x => x.Id == addressId);


            TransactionAddress target = source.TestWCFSerialization();

            Assert.AreEqual(source.City, target.City);
            Assert.AreEqual(source.Country, target.Country);
            Assert.AreEqual(source.Id, target.Id);
            Assert.AreEqual(source.Email, target.Email);
            Assert.AreEqual(source.Name, target.Name);
            Assert.AreEqual(source.Neighbourhood, target.Neighbourhood);
            Assert.AreEqual(source.PhoneExt, target.PhoneExt);
            Assert.AreEqual(source.Phone, target.Phone);
            Assert.AreEqual(source.PostalCode, target.PostalCode);
            Assert.AreEqual(source.Region, target.Region);
            Assert.AreEqual(source.StreetLine1, target.StreetLine1);
            Assert.AreEqual(source.StreetLine2, target.StreetLine2);
            Assert.AreEqual(source.Timestamp, target.Timestamp);

        }

        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeSimpleTransactionLineItem()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IRepositoryService>();

            var id = 1.ToGuid();
            var tmp = service.GetSingle<TransactionAuthorisationRequest>(x => x.Id == id, new IncludeSpecification<TransactionAuthorisationRequest>(x => x.LineItems));

            Guid lineItemId = tmp.LineItems.First().Id;

            var source = service.GetSingle<TransactionLineItem>(x => x.Id == lineItemId);


            TransactionLineItem target = source.TestWCFSerialization();

            Assert.AreEqual(source.Id, target.Id);
            Assert.AreEqual(source.Name, target.Name);
            Assert.AreEqual(source.Price, target.Price);
            Assert.AreEqual(source.ProductId, target.ProductId);
            Assert.AreEqual(source.Quantity, target.Quantity);
            Assert.AreEqual(source.RecurrenceType, target.RecurrenceType);
            Assert.AreEqual(source.RecurrenceTypeAmount, target.RecurrenceTypeAmount);
            Assert.AreEqual(source.RecurrenceDurationType, target.RecurrenceDurationType);
            Assert.AreEqual(source.RecurrenceDurationTypeAmount, target.RecurrenceDurationTypeAmount);

        }



        
        
        
        
        
        
        
        
        [Test]
        [Category("Services")]
        [Category("Commerce")]
        public void CanSerializeEntity()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<XAct.Users.Commerce.Services.IPaymentProcessingService>();

            var id = 1.ToGuid();
            TransactionAuthorisationRequest source = service.GetTransactionById(id);

            TransactionAuthorisationRequest target = source.TestWCFSerialization();

            Assert.AreEqual(source.MerchantFK, target.MerchantFK);
            Assert.AreEqual(source.AuthorisationFK, target.AuthorisationFK);
            Assert.AreEqual(source.BillingInformationFK, target.BillingInformationFK);
            Assert.AreEqual(source.BillingInformation.City, target.BillingInformation.City);
            Assert.AreEqual(source.BillingInformation.Country, target.BillingInformation.Country);
            Assert.AreEqual(source.BillingInformation.Id, target.BillingInformation.Id);
            Assert.AreEqual(source.BillingInformation.Email, target.BillingInformation.Email);
            Assert.AreEqual(source.BillingInformation.Name, target.BillingInformation.Name);
            Assert.AreEqual(source.BillingInformation.Neighbourhood, target.BillingInformation.Neighbourhood);
            Assert.AreEqual(source.BillingInformation.PhoneExt, target.BillingInformation.PhoneExt);
            Assert.AreEqual(source.BillingInformation.Phone, target.BillingInformation.Phone);
            Assert.AreEqual(source.BillingInformation.PostalCode, target.BillingInformation.PostalCode);
            Assert.AreEqual(source.BillingInformation.Region, target.BillingInformation.Region);
            Assert.AreEqual(source.BillingInformation.StreetLine1, target.BillingInformation.StreetLine1);
            Assert.AreEqual(source.BillingInformation.StreetLine2, target.BillingInformation.StreetLine2);
            Assert.AreEqual(source.BillingInformation.Timestamp, target.BillingInformation.Timestamp);
            Assert.AreEqual(source.CurrencyCode, target.CurrencyCode);
            Assert.AreEqual(source.Id, target.Id);
            Assert.AreEqual(source.LineItems.Count, target.LineItems.Count);
            Assert.AreEqual(source.LineItems.First().Id, target.LineItems.First().Id);
            Assert.AreEqual(source.LineItems.First().Name, target.LineItems.First().Name);
            Assert.AreEqual(source.LineItems.First().Price, target.LineItems.First().Price);
            Assert.AreEqual(source.LineItems.First().ProductId, target.LineItems.First().ProductId);
            Assert.AreEqual(source.LineItems.First().Quantity, target.LineItems.First().Quantity);
            Assert.AreEqual(source.LineItems.First().RecurrenceDurationType, target.LineItems.First().RecurrenceDurationType);
            Assert.AreEqual(source.LineItems.First().RecurrenceDurationTypeAmount, target.LineItems.First().RecurrenceDurationTypeAmount);
            Assert.AreEqual(source.LineItems.First().RecurrenceType, target.LineItems.First().RecurrenceType);
            Assert.AreEqual(source.LineItems.First().RecurrenceTypeAmount, target.LineItems.First().RecurrenceTypeAmount);

            //Assert.AreEqual(source.LineItems.First().Options.First()., target.LineItems.First().RecurrenceTypeAmount);

            Assert.AreEqual(source.MerchantFK, target.MerchantFK);
            Assert.AreEqual(source.MerchantOrderId, target.MerchantOrderId);
            Assert.AreEqual(source.ShippingInformationFK, target.ShippingInformationFK);
            Assert.AreEqual(source.ShippingInformation.City, target.ShippingInformation.City);
            Assert.AreEqual(source.ShippingInformation.Country, target.ShippingInformation.Country);
            Assert.AreEqual(source.ShippingInformation.Id, target.ShippingInformation.Id);
            Assert.AreEqual(source.ShippingInformation.Email, target.ShippingInformation.Email);
            Assert.AreEqual(source.ShippingInformation.Name, target.ShippingInformation.Name);
            Assert.AreEqual(source.ShippingInformation.Neighbourhood, target.ShippingInformation.Neighbourhood);
            Assert.AreEqual(source.ShippingInformation.PhoneExt, target.ShippingInformation.PhoneExt);
            Assert.AreEqual(source.ShippingInformation.Phone, target.ShippingInformation.Phone);
            Assert.AreEqual(source.ShippingInformation.PostalCode, target.ShippingInformation.PostalCode);
            Assert.AreEqual(source.ShippingInformation.Region, target.ShippingInformation.Region);
            Assert.AreEqual(source.ShippingInformation.StreetLine1, target.ShippingInformation.StreetLine1);
            Assert.AreEqual(source.ShippingInformation.StreetLine2, target.ShippingInformation.StreetLine2);
            Assert.AreEqual(source.ShippingInformation.Timestamp, target.ShippingInformation.Timestamp);
            Assert.AreEqual(source.Timestamp, target.Timestamp);
            Assert.AreEqual(source.Total, target.Total);


            Assert.IsNotNull(source, "Did not find transaction");
        }

    }
}


