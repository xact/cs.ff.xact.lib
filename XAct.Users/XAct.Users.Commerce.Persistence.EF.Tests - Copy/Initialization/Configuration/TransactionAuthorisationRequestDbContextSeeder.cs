﻿
namespace XAct.Users.Commerce.Tests.Initialization.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Identity.Entities.Location.Postal;
    using XAct.Services;
    using XAct.Tests.Implementations;
    using XAct.Users.Commerce.Initialization.DbContextSeeders;
    using XAct.Users.Commerce.Services.Entities;

    public class TransactionAuthorisationRequestDbContextSeeder :
        XActLibDbContextSeederBase<TransactionAuthorisationRequest>, ITransactionAuthorisationRequestDbContextSeeder,
        IHasMediumBindingPriority
    {
        private class X
        {
            public string ProductId { get; set; }
            public string Name { get; set; }
            public int Quantity { get; set; }
            public decimal Price { get; set; }
            public decimal Surcharge { get; set; }
        }


        public TransactionAuthorisationRequestDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {

        }


        public override void CreateEntities()
        {

            this.InternalEntities = new List<TransactionAuthorisationRequest>();

            this.InternalEntities.Add(MakeTransaction(1, "CORP-1", "USD", 1));
            this.InternalEntities.Add(MakeTransaction(2, "CORP-2", "USD", 2));
            this.InternalEntities.Add(MakeTransaction(3, "CORP-3", "USD", 3));

        }


        private TransactionAuthorisationRequest MakeTransaction(int id, string merchangeTransactionId,
                                                                string currencyCode = "USD", int count = 1)
        {


            List<X> lineitemInfos = new List<X>
                {
                    new X {ProductId = "Foo123", Name = "Foo Bar Soap", Price = 1.23m, Quantity = 1, Surcharge = 0.00m},
                    new X
                        {
                            ProductId = "Foo234",
                            Name = "Foo Bar Whatever",
                            Price = 2.34m,
                            Quantity = 2,
                            Surcharge = 0.00m
                        },
                    new X
                        {
                            ProductId = "Foo345",
                            Name = "Foo Bar Whiskey",
                            Price = 34.99m,
                            Quantity = 3,
                            Surcharge = 4.00m
                        },

                };



            TransactionAuthorisationRequest request = new TransactionAuthorisationRequest();

            request.Id = id.ToGuid();
            request.CurrencyCode = currencyCode;
            request.MerchantTransactionId = merchangeTransactionId;
            var totalForTax = 0.00m;


            for (int i = 0; i < count; i++)
            {
                X lineItemInfo = lineitemInfos[i];


                var itemTotal = 0.00m;

                var li = new TransactionLineItem
                    {

                        Type = LineItemType.Product,
                        ProductId = lineItemInfo.ProductId,
                        Name = lineItemInfo.Name,
                        Price = lineItemInfo.Price,
                        Quantity = Math.Max(1, lineItemInfo.Quantity)

                    };
                request.LineItems.Add(li);

                itemTotal += li.Price*li.Quantity;

                if (lineItemInfo.Surcharge > 0.00m)
                {
                    var li2 = new TransactionLineItemOption
                        {
                            Label = "Foo",
                            Value = "Bar",
                            LineItemFK = li.Id,
                            LineItem = li,
                            Surcharge = lineItemInfo.Surcharge,

                        };
                    li.Options.Add(li2);
                    itemTotal += li2.Surcharge;
                }

                totalForTax += itemTotal;
            }



            var li3 = new TransactionLineItem
                {
                    Type = LineItemType.Product,
                    ProductId = "ST",
                    Name = "Sales Tax",
                    Price = totalForTax*1.075m,
                };
            request.LineItems.Add(li3);



            request.Total = totalForTax + li3.Price;

            request.MerchantOrderId = "xyz123";

            request.BillingInformation = new TransactionAddress
                {
                    Name = "Foo Guy",
                    Email = "fooguybill@bar.com",
                    Phone = "+64 21 123 4567",
                    StreetLine1 = "Bill Street Line #1",
                    StreetLine2 = "Line #2",
                    City = "City",
                    Country = "Some Country",
                    Neighbourhood = "Whawha",
                    PostalCode = "12345",
                    Region = "Foo Region"
                };
            request.ShippingInformation = new TransactionAddress
                {
                    Name = "Foo Guy",
                    Email = "fooguyship@bar.com",
                    Phone = "+64 21 123 4567",
                    StreetLine1 = "Ship Street Line #1",
                    StreetLine2 = "Line #2",
                    City = "City",
                    Country = "Some Country",
                    Neighbourhood = "Whawha",
                    PostalCode = "12345",
                    Region = "Foo Region"
                };

            return request;
        }

    }
}


