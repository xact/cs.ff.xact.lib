namespace XAct.Users.Tips.Tests.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;
    using XAct.Users.Tips.Initialization.DbContextSeeders;
            


    public class TipUserViewMetricsDbContextSeeder : XActLibDbContextSeederBase<TipUserViewMetrics>, ITipUserViewMetricsDbContextSeeder, IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TipUserViewMetricsDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public TipUserViewMetricsDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {

        }

        /// <summary>
        /// Seeds the specified database context.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public override void SeedInternal(DbContext dbContext) 
        {
            SeedInternalHelper(dbContext,true,x=>new {x.UserIdentifier,x.Key});

        }




        public override void CreateEntities() 
        {
            this.InternalEntities = new List<TipUserViewMetrics>();

            this.InternalEntities.Add(
                new TipUserViewMetrics
                    {
                        UserIdentifier = "skys",
                        Key = "TipForBeginner",
                        LastAccessedOnUtc = DateTime.UtcNow.AddDays(-3),
                        NumberOfTimesViewed = 1,
                    }
                );
        }
    }


}
