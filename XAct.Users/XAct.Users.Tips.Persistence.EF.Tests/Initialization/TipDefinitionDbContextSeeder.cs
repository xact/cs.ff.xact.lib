namespace XAct.Users.Tips.Tests.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Users.Tips.Initialization.DbContextSeeders;

    public class TipDefinitionDbContextSeeder : XActLibDbContextSeederBase<TipDefinition>, ITipDefinitionDbContextSeeder,
                                                IHasMediumBindingPriority
    {
        public TipDefinitionDbContextSeeder(ITracingService tracingService, params IDbContextSeeder[] seeders)
            : base(tracingService, seeders)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.Key);
        }

        public override void CreateEntities()
        {

            this.InternalEntities = new List<TipDefinition>();

            this.InternalEntities.Add(
                new TipDefinition
                    {
                        ApplicationTennantId = Guid.Empty,
                        Key = "TipForBeginner",
                        Description = "...",
                        Difficulty = Difficulty.Beginner,
                        Enabled = true,
                        Title = "Begginer Tip",
                        Text = "whoooohooo",
                        ResourceFilter = "RFilter"
                    });

            this.InternalEntities.Add(
                new TipDefinition
                    {
                        ApplicationTennantId = Guid.Empty,
                        Key = "TipForNormal",
                        Description = "...",
                        Difficulty = Difficulty.Normal,
                        Enabled = true,
                        Title = "Normal Tip",
                        Text = "whoooohooo",
                        ResourceFilter = "RFilter"
                    });

            this.InternalEntities.Add(
                new TipDefinition
                    {
                        //OrganisationId = Guid.Empty,
                        Key = "TipForAdvanced",
                        Description = "...",
                        Difficulty = Difficulty.Advanced,
                        Enabled = true,
                        Title = "Advanced Tip",
                        Text = "whoooohooo",
                        ResourceFilter = "RFilter"
                    });
            this.InternalEntities.Add(
                new TipDefinition
                    {
                        //OrganisationId = Guid.Empty,
                        Key = "TipForExpert",
                        Description = "...",
                        Difficulty = Difficulty.Expert,
                        Enabled = true,
                        Title = "Expert Tip",
                        Text = "whoooohooo",
                        ResourceFilter = "RFilter"
                    });
            this.InternalEntities.Add(
                new TipDefinition
                    {
                        //OrganisationId = Guid.Empty,
                        Key = "TipForGuru",
                        Description = "...",
                        Difficulty = Difficulty.Guru,
                        Enabled = true,
                        Title = "Guru Tip",
                        Text = "whoooohooo",
                        ResourceFilter = "RFilter"
                    });
        }


    }
}