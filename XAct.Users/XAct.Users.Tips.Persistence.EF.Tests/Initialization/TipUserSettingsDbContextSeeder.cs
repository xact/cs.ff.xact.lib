namespace XAct.Users.Tips.Tests.Implementations
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Users.Tips.Initialization.DbContextSeeders;

    public class TipUserSettingsDbContextSeeder : XActLibDbContextSeederBase<TipUserSettings>, ITipUserSettingsDbContextSeeder, IHasMediumBindingPriority
    {

        public TipUserSettingsDbContextSeeder(ITracingService tracingService, params IDbContextSeeder[] seeders) : base(tracingService, seeders)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext,true,x=>x.UserIdentifier);   
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<TipUserSettings>();

            this.InternalEntities.Add(
                new TipUserSettings
                    {
                        Enabled = true,
                        Difficulty = Difficulty.Beginner,
                        NumberOfTimesToViewTips = 2,
                        UserIdentifier = "jasmins"
                    });

            this.InternalEntities.Add(
                new TipUserSettings
                    {
                        Enabled = true,
                        Difficulty = Difficulty.Beginner,
                        NumberOfTimesToViewTips = 2,
                        UserIdentifier = "chap1"
                    });


            this.InternalEntities.Add(
                new TipUserSettings
                    {
                        Enabled = true,
                        Difficulty = Difficulty.Beginner,
                        NumberOfTimesToViewTips = 2,
                        UserIdentifier = "chap2"
                    });


            this.InternalEntities.Add(
                new TipUserSettings
                    {
                        Enabled = true,
                        Difficulty = Difficulty.Beginner,
                        NumberOfTimesToViewTips = 2,
                        UserIdentifier = "chap3"
                    });

            this.InternalEntities.Add(
                new TipUserSettings
                    {
                        Enabled = true,
                        Difficulty = Difficulty.Beginner,
                        NumberOfTimesToViewTips = 2,
                        UserIdentifier = "skys"
                    });

            this.InternalEntities.Add(
                new TipUserSettings
                    {
                        Enabled = true,
                        Difficulty = Difficulty.Expert,
                        NumberOfTimesToViewTips = 2,
                        UserIdentifier = "paulc"
                    });

        }
    }
}