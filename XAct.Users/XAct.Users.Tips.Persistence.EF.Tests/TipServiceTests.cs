namespace XAct.Users.Tips.Tests
{
    using System;
    using System.Data.Entity.Validation;
    using System.Globalization;
    using NUnit.Framework;
    using XAct.Users.Tips.Implementations;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Tests;
    //using XAct.Extensions;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class TipServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {

            try
            {
                InitializeUnitTestDbIoCContextAttribute.BeforeTest();
            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetTipManagementServiceConfiguration()
        {
            TipManagementServiceConfiguration tipServiceConfiguration =
                XAct.DependencyResolver.Current.GetInstance<TipManagementServiceConfiguration>();


            Assert.IsNotNull(tipServiceConfiguration);
        }

        [Test]
        public void ManagementServiceIsAvailable()
        {
            ITipManagementService tipService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipManagementService>();


            Assert.IsTrue(tipService != null);
        }
        [Test]
        public void ManagementServiceIsOfExpectedService()
        {
            ITipManagementService tipManagementService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipManagementService>();


            Assert.AreEqual(typeof(TipManagementService), tipManagementService.GetType());
        }


        [Test]
        [Category("TipService")]
        public void ServiceIsAvailable()
        {
            ITipService tipService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipService>();


            Assert.IsTrue(tipService != null);
        }

        [Test]
        [Category(XAct.Bootstrapper.Tests.Constants.Tips)]
        [Category(XAct.Bootstrapper.Tests.Constants.Services)]
        public void ServiceIsExpectedService()
        {
            ITipService tipService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipService>();


            Assert.AreEqual(typeof(TipService),tipService.GetType());
        }

        [Test]
        [Category(XAct.Bootstrapper.Tests.Constants.Tips)]
        public void CanGetARandomTip()
        {
            ITipService tipService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipService>();

            Tip tip = tipService.GetRandomTip(Difficulty.Beginner, Difficulty.Advanced, CultureInfo.InvariantCulture);


            Assert.IsNotNull(tip);


        }

        [Test]
        public void CanGetARandomTipThatIsAlwaysBetweenDifficultyRangeSpecified()
        {
            ITipService tipService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipService>();


            for (int i = 0; i < 1000; i++)
            {
                Tip tip = tipService.GetRandomTip(Difficulty.Beginner, Difficulty.Normal,CultureInfo.InvariantCulture);

                Assert.IsNotNull(tip);
                Assert.IsTrue(tip.Difficulty >= Difficulty.Beginner);
                Assert.IsTrue(tip.Difficulty <= Difficulty.Normal);
            }

            //NOTE THAT WE ARE NOT UPDATING THE SETTINGS OF THE USER, HENCE WHY WE CAN GO UP TO A THOUSAND TESTS.
        }

        [Test]
        public void CanGetARandomTipThatIsAlwaysBetweenDifficultyRangeSpecified2()
        {
            ITipService tipService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipService>();


            for (int i = 0; i < 1000; i++)
            {
                Tip tip = tipService.GetRandomTip(Difficulty.Normal, Difficulty.Advanced, CultureInfo.InvariantCulture);

                Assert.IsNotNull(tip);
                Assert.IsTrue(tip.Difficulty >= Difficulty.Normal);
                Assert.IsTrue(tip.Difficulty <= Difficulty.Advanced);
            }

            //NOTE THAT WE ARE NOT UPDATING THE SETTINGS OF THE USER, HENCE WHY WE CAN GO UP TO A THOUSAND TESTS.
        }


        //TEST:FAILING
        [Test]
        public void GetNextRandomTipForUser()
        {
            ITipManagementService tipManagementService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipManagementService>();


            Tip tip = tipManagementService.GetNextRandomTipForUser("chap1", CultureInfo.InvariantCulture);

            //Note that we are not updating the counter for the user.
            
            Assert.IsNotNull(tip);
        
            //But do note that some other test is going to update chap1's counters.
        }


        //TEST:FAILING
        [Test]
        public void GetNextRandomTipForUserAndEnsureAlwaysBetweenDifficultyRange()
        {
            ITipManagementService tipManagementService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipManagementService>();


            for (int i = 0; i < 200; i++)
            {

                Tip tip = tipManagementService.GetNextRandomTipForUser("jasmins", CultureInfo.InvariantCulture);

                //Console.WriteLine("{0}: {1}: {2}", i + 1, tip.Difficulty, tip.Title);

                //Note that we are not updating the counter for the user.
                //as we are not committing.

                Assert.IsNotNull(tip);
                //Because we have not updated anything, it should always remain (for now) for beginner level:
                Assert.IsTrue(tip.Difficulty == Difficulty.Beginner);
            }
            
        }


        [Test]
        public void GetNextRandomTipForUserAndEnsureAlwaysBetweenDifficultyRange2()
        {
            ITipManagementService tipManagementService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipManagementService>();


            Difficulty lastDifficulty = Difficulty.Undefined;
            

            for (int i = 1; i < 5; i++)
            {

                for (int j = 0; j < 2; j++)
                {
                    Tip tip = tipManagementService.GetNextRandomTipForUser("chap2", CultureInfo.InvariantCulture);

                    if (tip != null)
                    {
                        Console.WriteLine("{0}: {1}: {2}", i + 1, tip.Difficulty, tip.Title);
                        Assert.IsNotNull(tip);
                        Assert.IsTrue(tip.Difficulty >= lastDifficulty);
                        lastDifficulty = tip.Difficulty;
                    }
                    else
                    {
                        Console.WriteLine("{0},  NULL",i+1);
                        
                    }

                    //Commit to update counter:


                    XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();
                }
            }
        }



        [Test]
        public void GetNextRandomTipForUserAndEnsureAlwaysBetweenDifficultyRange3()
        {
            ITipManagementService tipManagementService =
                XAct.DependencyResolver.Current.GetInstance<XAct.Users.Tips.ITipManagementService>();


            Difficulty lastDifficulty = Difficulty.Undefined;


            for (int i = 1; i < 50; i++)
            {

                for (int j = 0; j < 2; j++)
                {
                    Tip tip = tipManagementService.GetNextRandomTipForUser("chap1", CultureInfo.InvariantCulture);

                    if (tip != null)
                    {
                        Console.WriteLine("{0}: {1}: {2}", i + 1, tip.Difficulty, tip.Title);
                        Assert.IsNotNull(tip);
                        Assert.IsTrue(tip.Difficulty >= lastDifficulty);
                        lastDifficulty = tip.Difficulty;
                    }
                    else
                    {
                        Console.WriteLine("{0},  NULL", i + 1);

                    }

                    //Commit to update counter:


                    XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>().GetCurrent().Commit();
                }
            }
        }



        //[Test]
        //public void CanRoundTripTip()
        //{
        //    //A WCF/JSON serialization test:

        //    Tip tip = new Tip("A title", "A body", Difficulty.Expert);

        //    string serializedEntity = tip.ToDataContractString<Tip>();
        //    Tip tip2 = serializedEntity.DeserializeFromDataContractSerializedString<Tip>();

        //    Assert.AreEqual("A title", tip2.Title);
        //    Assert.AreEqual("A body", tip2.Body);

        //    Assert.AreEqual(Difficulty.Expert, tip2.Difficulty);
        //}

        //[Test]
        //public void CanRoundTripTipDefinition()
        //{
        //    //A WCF/JSON serialization test:
        //    TipDefinition tipDefinition = new TipDefinition();

        //    tipDefinition.Description = "...";
        //    tipDefinition.Difficulty = Difficulty.Expert;
        
        //    tipDefinition.Enabled = true;
        //    tipDefinition.Key = "Key...";
        //    tipDefinition.ResourceKey = "RKey...";
        //    tipDefinition.ResourceFilter = "RFilter...";
        //    tipDefinition.Tag = "Tag...";
        
        
        //    string serializedEntity = tipDefinition.ToDataContractString<TipDefinition>();
        //    TipDefinition x2 = serializedEntity.DeserializeFromDataContractSerializedString<TipDefinition>();
        
        //    Assert.AreEqual("...", x2.Description);
        //    Assert.AreEqual(Difficulty.Expert, x2.Difficulty);
        //    Assert.AreEqual(true, x2.Enabled);
        //    Assert.AreEqual("Key...", x2.Key);
        //    Assert.AreEqual("RKey...", x2.ResourceKey);
        //    Assert.AreEqual("RFilter...", x2.ResourceFilter);
        //    Assert.AreEqual("Tag...",x2.Tag);
        //}



    }
}