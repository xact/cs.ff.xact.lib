using XAct.Data.EF.CodeFirst;

namespace XAct.Users.Initialization
{
    public interface IUserAuthenticationDataDbContextSeeder : IHasXActLibDbContextSeeder<UserAuthenticationData>
    {

    }
}