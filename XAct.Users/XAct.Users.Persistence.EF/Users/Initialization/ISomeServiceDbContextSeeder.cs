namespace XAct.Users.Initialization
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{SomeServiceEntity}"/>
    /// to seed the Tip tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IUserDbContextSeeder : IHasXActLibDbContextSeeder<User>
    {

    }
}

