namespace XAct.Users.Initialization
{
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// Contract for the <see cref="IHasXActLibDbModelBuilder"/>
    /// specific to setting up XActLib Helps capabilities.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S02_Initialization)]
    public interface IUserDbModelBuilder : IHasXActLibDbModelBuilder
    {
    }


}