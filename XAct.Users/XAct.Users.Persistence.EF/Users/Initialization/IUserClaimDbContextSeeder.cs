namespace XAct.Users.Initialization
{
    using XAct.Data.EF.CodeFirst;

    public interface IUserClaimDbContextSeeder : IHasXActLibDbContextSeeder<UserClaim>
    {

    }
}