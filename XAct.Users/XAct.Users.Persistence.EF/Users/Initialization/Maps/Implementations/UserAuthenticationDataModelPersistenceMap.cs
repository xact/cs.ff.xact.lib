using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using XAct;
using System.Data.Entity.ModelConfiguration;

namespace XAct.Users.Initialization.Maps.Implementations
{
    public class UserAuthenticationDataModelPersistenceMap : EntityTypeConfiguration<UserAuthenticationData>, IUserAuthenticationDataModelPersistenceMap
    {


        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public UserAuthenticationDataModelPersistenceMap()
        {
            this.ToXActLibTable("UserAuthenticationData");

            this
                .HasKey(m => new { m.Id });

            int colOrder = 0;
            int indexMember = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.UserId)
                .IsRequired()
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_UserId", indexMember++) { IsUnique = true }))
                ;

            indexMember = 0;


            this.Property(x => x.PasswordHash)
                .DefineRequired64CharLastModifiedBy(colOrder++);

            this.Property(x => x.CurrentFailedAttempts)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.TotalFailedAttempts)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.LockedOutTillDateTimeUtc)
                //.IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(x => x.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++)
                ;
        }
    }
}