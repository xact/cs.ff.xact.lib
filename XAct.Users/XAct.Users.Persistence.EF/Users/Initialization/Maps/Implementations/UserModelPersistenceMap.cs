namespace XAct.Users.Initialization.Maps.Implementations
{
    using System.Data.Entity.ModelConfiguration;

    public class UserModelPersistenceMap : EntityTypeConfiguration<User>,
                                                        IUserModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public UserModelPersistenceMap()
        {

            this.ToXActLibTable("User");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++);
            this.Property(x => x.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);

            this.Property(x => x.LastModifiedBy)
                .DefineRequired64CharLastModifiedBy(colOrder++);
            this.Property(x => x.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++);

            this.Property(x => x.DeletedBy)
                .DefineOptional64CharDeletedBy(colOrder++);
            this.Property(x => x.DeletedOnUtc)
                .DefineOptionalDeletedOnUtc(colOrder++);


            //Relationships:



            this.HasMany(x => x.Properties)
                .WithOptional()
                .HasForeignKey(x => x.UserFK);


            this.HasMany(x => x.Claims)
                .WithOptional()
                .HasForeignKey(x => x.UserFK);



            this
                .HasMany(p => p.AllowedDelegates)
                .WithMany()
                .Map(x =>
                {
                    x.ToXActLibTable("User_User");
                    x.MapLeftKey("MessageId");
                    x.MapRightKey("MessageTagId");
                });
        }
    }
}
