namespace XAct.Users.Initialization.Maps.Implementations
{
    using XAct;
    using System.Data.Entity.ModelConfiguration;

    public class UserClaimModelPersistenceMap : EntityTypeConfiguration<UserClaim>,
                                                        IUserClaimModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public UserClaimModelPersistenceMap()
        {

            this.ToXActLibTable("UserClaim");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.UserFK)
                .IsRequired();



            //Key/Value:
            this.Property(x => x.Key)
                .DefineRequired64CharKey(colOrder++);
            this.Property(x => x.Authority)
                .IsRequired()
                .HasMaxLength(2048);
            this.Property(x => x.SerializedValueType)
                .DefineRequired1024CharSerializationValueType(colOrder++);
            this.Property(x => x.SerializationMethod)
                .DefineRequiredSerializationMethod(colOrder++);
            this.Property(x => x.SerializedValue)
                .DefineOptional4000CharSerializationValue(colOrder++);



            //Auditing:
            this.Property(x => x.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++);
            this.Property(x => x.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);
            this.Property(x => x.LastModifiedBy)
                .DefineRequired64CharLastModifiedBy(colOrder++);
            this.Property(x => x.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++);

        }
    }
}
