namespace XAct.Users.Initialization
{
    using XAct.Data.EF.CodeFirst;

    public interface IUserPropertyDbContextSeeder : IHasXActLibDbContextSeeder<UserProperty>
    {

    }
}