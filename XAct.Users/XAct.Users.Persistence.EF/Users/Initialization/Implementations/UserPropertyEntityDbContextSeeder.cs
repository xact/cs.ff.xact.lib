namespace XAct.Users.Initialization.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;

    public class UserPropertyDbContextSeeder : XActLibDbContextSeederBase<UserProperty>, IUserPropertyDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="userDbContextSeeder"></param>
        public UserPropertyDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
                                                 IUserDbContextSeeder userDbContextSeeder)
            : base(tracingService,userDbContextSeeder)
        {
            _environmentService = environmentService;
        }

        public override void CreateEntities()
        {
        }
    }
}