namespace XAct.Users.Initialization.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;

#pragma warning disable 1591


    /// <summary>
    /// 
    /// </summary>
    public class UserDbContextSeeder : XActLibDbContextSeederBase<User>, IUserDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public UserDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _environmentService = environmentService;
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }


}

