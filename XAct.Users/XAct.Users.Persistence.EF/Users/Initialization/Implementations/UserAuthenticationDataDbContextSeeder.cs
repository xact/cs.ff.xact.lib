using XAct.Data.EF.CodeFirst;
using XAct.Diagnostics;
using XAct.Environment;
using XAct.Users.Initialization.Implementations;

namespace XAct.Users.Initialization.Implementations
{
    public class UserAuthenticationDataDbContextSeeder : XActLibDbContextSeederBase<UserAuthenticationData>, IUserAuthenticationDataDbContextSeeder
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public UserAuthenticationDataDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _environmentService = environmentService;
        }


        /// <summary>
        /// Creates the entities.
        /// </summary>
        public override void CreateEntities()
        {
        }
    }
}