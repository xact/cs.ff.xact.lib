using XAct.Users.Services;

namespace XAct.Users.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Diagnostics;
    using XAct.Users.Initialization.Maps;

    /// <summary>
    /// 
    /// </summary> 
    public class UserDbModelBuilder : IUserDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public UserDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<User>)XAct.DependencyResolver.Current.GetInstance<IUserModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<UserProperty>)XAct.DependencyResolver.Current.GetInstance<IUserPropertyModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<UserClaim>)XAct.DependencyResolver.Current.GetInstance<IUserClaimModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<UserAuthenticationData>)XAct.DependencyResolver.Current.GetInstance<IUserAuthenticationDataModelPersistenceMap>());

        }
    }
}