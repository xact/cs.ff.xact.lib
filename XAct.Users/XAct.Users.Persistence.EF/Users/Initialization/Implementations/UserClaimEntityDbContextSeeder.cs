namespace XAct.Users.Initialization.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;

    public class UserClaimEntityDbContextSeeder : XActLibDbContextSeederBase<UserClaim>, IUserClaimDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="userDbContextSeeder">The user database context seeder.</param>
        /// <param name="userPropertyDbContextSeeder">The user property database context seeder.</param>
        public UserClaimEntityDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService,
                                                 IUserDbContextSeeder userDbContextSeeder,
            IUserPropertyDbContextSeeder userPropertyDbContextSeeder)
            : base(tracingService, userDbContextSeeder, userPropertyDbContextSeeder)
        {
            _environmentService = environmentService;
        }

        public override void CreateEntities()
        {
        }
    }
}