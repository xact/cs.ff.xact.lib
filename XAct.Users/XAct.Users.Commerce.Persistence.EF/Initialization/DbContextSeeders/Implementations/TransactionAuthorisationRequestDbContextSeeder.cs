namespace XAct.Users.Commerce.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Users.Commerce.Services.Entities;

#pragma warning disable 1591
    public class TransactionAuthorisationRequestDbContextSeeder : XActLibDbContextSeederBase<TransactionAuthorisationRequest>, ITransactionAuthorisationRequestDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionAuthorisationRequestDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public TransactionAuthorisationRequestDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _environmentService = environmentService;
        }

        

        public override void CreateEntities()
        {
        }
    }


}

