namespace XAct.Users.Commerce.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Users.Commerce.Services.Entities;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{TransactionAuthorisationRequest}"/>
    /// to seed the Tip tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface ITransactionAuthorisationRequestDbContextSeeder : IHasXActLibDbContextSeeder<TransactionAuthorisationRequest>
    {

    }

}

