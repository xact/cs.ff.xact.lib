namespace XAct.Users.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Users.Commerce.Services.Entities;

    public class TransactionAuthorisationRequestModelPersistenceMap : EntityTypeConfiguration<TransactionAuthorisationRequest>,
                                                        ITransactionAuthorisationRequestModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public TransactionAuthorisationRequestModelPersistenceMap()
        {

            this.ToXActLibTable("TransactionAuthorisationRequest");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;
            int indexMember = 1; //Indexs of db's are 1 based.

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            //-------------------------------------------
            //IGNORE the cc info.
            this.Ignore(x => x.CCInfo);
            //-------------------------------------------
            



            this.Property(x => x.MerchantFK)
                .IsRequired()
                .HasColumnOrder(colOrder++);

            //THe vendor's OrderId:
            this.Property(x => x.MerchantOrderId)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_MerchantOrderId", 1) { IsUnique = false }))
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_MerchantOrderId_MerchantTransactionId", indexMember++) { IsUnique = true }))
                ;

            //Reset:
            this.Property(x => x.MerchantTransactionId)
                .IsRequired()
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_MerchantTransactionId", 1) { IsUnique = true }))
                .HasColumnAnnotation(
                "Index",
                new IndexAnnotation(
                    new IndexAttribute("IX_MerchantOrderId_MerchantTransactionId", indexMember++) { IsUnique = true }))
                .HasColumnOrder(colOrder++);



            //The transaction's CurrencyCode:
            this.Property(x => x.CurrencyCode)
                .IsRequired()
                .HasMaxLength(3)
                .HasColumnOrder(colOrder++);

            //The transaction's Amount:
            this.Property(x => x.Total)
                .IsRequired()
                .HasColumnOrder(colOrder++);




            //Relationships:
            this
                .HasRequired(x => x.BillingInformation)
                .WithMany()
                //theoretically one or more Transactions can refer to one Address(never will), so other side is WithMany()
                .HasForeignKey(x => x.BillingInformationFK)
                //because we are not re-using these addresses:
                .WillCascadeOnDelete(false);
                


            //    SetUp : System.Data.SqlServerCe.SqlCeException : 
            //    The referential relationship will result in a cyclical reference that is not allowed. 
            //        [ Constraint name = FK_dbo.XActLib_TransactionAuthorisationRequest_dbo.TransactionAddresses_ShippingInformationFK
            //}
            this
                .HasOptional(x => x.ShippingInformation)
                .WithMany()
                .HasForeignKey(x => x.ShippingInformationFK)
                .WillCascadeOnDelete(false);
                


                

            //A transaction can have n lineitems.
            this.HasMany(x => x.LineItems)
                .WithRequired() //two navigation
                .HasForeignKey(x => x.TransactionFK);


            //relationship  
            this
                .HasOptional(x => x.Authorisation)
                .WithMany()
                //theoretically one or more Transactions can refer to one Auth(never will), so other side is WithMany()
                .HasForeignKey(x => x.AuthorisationFK)
                //because we are not re-using the associated:
                .WillCascadeOnDelete(true);

        }
    }
}
