namespace XAct.Users.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Users.Commerce.Services.Entities;

    public class TransactionLineItemModelPersistenceMap :
        EntityTypeConfiguration<TransactionLineItem>,
        ITransactionLineItemModelPersistenceMap
    {

        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public TransactionLineItemModelPersistenceMap()
        {

            this.ToXActLibTable("TransactionLineItem");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x => x.TransactionFK)
                .HasColumnOrder(colOrder++);


            this.Property(x => x.Type)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Tangible)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.ProductId)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Name)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Price)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Quantity)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.StartupFee)
                .HasColumnOrder(colOrder++);


            this.Property(x => x.RecurrenceType)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.RecurrenceTypeAmount)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.RecurrenceDurationType)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.RecurrenceDurationTypeAmount)
                .HasColumnOrder(colOrder++);

            //Relationships:
            this.HasMany(x => x.Options)
                .WithRequired(x => x.LineItem) //two way navigation
                .HasForeignKey(x => x.LineItemFK);


        }
    }
}