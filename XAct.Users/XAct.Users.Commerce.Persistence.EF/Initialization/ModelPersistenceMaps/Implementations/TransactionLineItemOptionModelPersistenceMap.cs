namespace XAct.Users.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Users.Commerce.Services.Entities;

    public class TransactionLineItemOptionModelPersistenceMap :
        EntityTypeConfiguration<TransactionLineItemOption>,
        ITransactionLineItemOptionModelPersistenceMap
    {

        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public TransactionLineItemOptionModelPersistenceMap()
        {

            this.ToXActLibTable("TransactionLineItemOption");
            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x => x.LineItemFK)
                 .HasColumnOrder(colOrder++);

            this.Property(x => x.Label)
                 .HasColumnOrder(colOrder++);

            this.Property(x => x.Value)
                 .HasColumnOrder(colOrder++);

            this.Property(x => x.Surcharge)
                 .HasColumnOrder(colOrder++);
        }
    }
}