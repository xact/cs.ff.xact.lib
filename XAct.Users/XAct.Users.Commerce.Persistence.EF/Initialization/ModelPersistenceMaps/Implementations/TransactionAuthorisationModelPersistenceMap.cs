namespace XAct.Users.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Users.Commerce.Services.Entities;

    public class TransactionAuthorisationModelPersistenceMap :
        EntityTypeConfiguration<TransactionAuthorisation>,
        ITransactionAuthorisationModelPersistenceMap
    {

        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public TransactionAuthorisationModelPersistenceMap()
        {
            this.ToXActLibTable("TransactionAuthorisation");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);



            this.Property(x => x.TransactionId)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.OrderNumber)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.ResponseCode)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.ResponseText)
                .HasColumnOrder(colOrder++);




        }
    }
}