namespace XAct.Identity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;


    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TEntityAlias">The type of the entity alias.</typeparam>
    /// <typeparam name="TEntityClaim">The type of the claim.</typeparam>
    /// <typeparam name="TEntityAddress">The type of the postal address.</typeparam>
    public abstract class IdentityWithAddressesBase<TEntity,TEntityAlias,TEntityClaim, TEntityAddress>
        : IdentityBase<TEntity,TEntityAlias,TEntityClaim>
          , IHasIdentityWithAddresses<TEntity,TEntityAlias,TEntityClaim,TEntityAddress>
        where TEntity : class, IHasDistributedGuidIdAndTimestamp, new()
        where TEntityAlias : class, IHasDistributedGuidIdAndTimestamp, IHasPreferred, IHasNames, new()
        where TEntityClaim : class, IHasDistributedGuidIdAndTimestamp, IHasPersistableClaim, new()
        where TEntityAddress : class, IHasDistributedGuidIdAndTimestamp, IHasPostalAddress, new()
    {
        private ICollection<TEntityAddress> _addresses;


        /// <summary>
        /// The FK to the preferred billing address of the
        /// identity.
        /// <para>
        /// Sales systems can use this value as a starting 
        /// point for a transaction.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid? PreferredBillingAddressFK { get; set; }

        /// <summary>
        /// Gets or sets the preferred billing address.
        /// </summary>
        /// <value>
        /// The preferred billing address.
        /// </value>
        [DataMember]
        public TEntityAddress PreferredBillingAddress { get; set; }

        /// <summary>
        /// The FK to the preferred shipping address of the
        /// identity.
        /// <para>
        /// Sales systems can use this value as a starting 
        /// point for a transaction.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid? PreferredShippingAddressFK { get; set; }


        /// <summary>
        /// Gets or sets the preferred billing address.
        /// </summary>
        /// <value>
        /// The preferred billing address.
        /// </value>
        [DataMember]
        public TEntityAddress PreferredShippingAddress { get; set; }

        /// <summary>
        /// Collection of addresses related to the entity.
        /// </summary>
        [DataMember]
        public virtual ICollection<TEntityAddress> Addresses
        {
            get { return _addresses ?? (_addresses = new Collection<TEntityAddress>()); }
        }
    }
}