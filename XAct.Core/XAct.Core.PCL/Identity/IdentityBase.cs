namespace XAct.Identity
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.Serialization;


    /// <summary>
    /// An abstract base class for identity (individual or organisation).
    /// <para>
    /// Used as the basis of <c>XAct.Commerce.Customers.Customer</c>
    /// </para>
    /// <para>
    /// The advantage of creating a common base class
    /// is the ability to keep a common theme working through 
    /// Customers, Purchases, Contacts, etc. 
    /// </para>
    /// <para>
    /// Note that the entity does not implement <see cref="IHasNames"/>
    /// but instead keeps a collection of Aliases, one of which is marked as the preferred one.
    /// Although this leads to an atypical data storage schema, this allows searching against aliases.
    /// </para>
    /// </summary>
    [DataContract]
    public abstract class IdentityBase<TEntity, TEntityAlias, TClaim> 
        : IHasDistributedGuidIdAndTimestamp, IHasIdentity<TEntity,TEntityAlias,TClaim>
        where TEntity : class, IHasDistributedGuidIdAndTimestamp, new()
        where TEntityAlias : class, IHasDistributedGuidIdAndTimestamp, IHasPreferred, IHasNames, new()
        where TClaim : class, IHasDistributedGuidIdAndTimestamp, IHasPersistableClaim, new()
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// Gets the FK to the parent entity (for an individual, that would be his Org/Work).
        /// </summary>
        /// <internal>
        /// This might not be satisfactory (graph versus hierarchy is probably more realistic).
        /// </internal>
        public virtual Guid ParentFK { get; set; }

        /// <summary>
        /// Gets or sets the parent entity (for an individual, that would be his Org/Work).
        /// </summary>
        /// <internal>
        /// This might not be satisfactory (graph versus hierarchy is probably more realistic).
        /// </internal>
        [DataMember]
        public virtual TEntity Parent { get; set; }


        /// <summary>
        /// Gets or sets the identity claims
        /// associated to this identity.
        /// <para>
        /// Only one will be used to track the user for tax purposes.
        /// </para>
        /// </summary>
        /// <value>
        /// The identity claims.
        /// </value>
        public virtual ICollection<TClaim> IdentityClaims {
            get { return _identityClaims ?? (_identityClaims = new Collection<TClaim>()); }
        }
        [DataMember]
        private ICollection<TClaim> _identityClaims;



        /// <summary>
        /// A user may have more than one name.
        /// This is the preferred one.
        /// </summary>
        [DataMember]
        public virtual Guid? PreferredAliasFK
        {
            get { return _preferredAliasFK; }
            set
            {
                PreferredAlias =_aliases.SingleOrDefault(x => x.Id == value);
            }
        }


        /// <summary>
        /// A user may have more than one name.
        /// This is the preferred one.
        /// </summary>
        [IgnoreDataMember]
        public virtual TEntityAlias PreferredAlias
        {
            get
            {
                return Aliases.SingleOrDefault(x => x.Preferred == true);
            }set
            {
                if (!Aliases.Contains(value))
                {
                    throw new ArgumentException("PreferredAlias must be already a member of the Aliases collection.");
                }

                //trying to not set change properties unless I have to:
                foreach (TEntityAlias alias in Aliases)
                {
                    if (alias.Preferred == true)
                    {
                        if (alias != value)
                        {
                            alias.Preferred = false;
                        }
                    }
                    else
                    {
                        if (alias == value)
                        {
                            alias.Preferred = true;
                        }
                    }
                }
                _preferredAliasFK = value.Id;
            }
        }




        /// <summary>
        /// The collection of aliases the entity 
        /// is known under.
        /// </summary>
        public virtual ICollection<TEntityAlias> Aliases { get { return _aliases ?? (_aliases = new Collection<TEntityAlias>()); } }
        [DataMember]
        private ICollection<TEntityAlias> _aliases;

        private Guid? _preferredAliasFK;


        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }
        
        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// <para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? LastModifiedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the datetime the item was deleted, expressed in UTC.
        /// <para>
        /// Defined in the <see cref="XAct.IHasDateTimeTrackabilityUtc" /> contract.
        /// </para>
        /// <para>
        /// Note: Generally updated by a db trigger, or saved first, then deleted.
        /// </para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? DeletedOnUtc { get; set; }
        
        /// <summary>
        /// Gets or sets the who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy" /></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public virtual string CreatedBy { get; set; }
        
        /// <summary>
        /// Gets or sets the who Modified the document.
        /// <para>Member defined in<see cref="IHasDateTimeModifiedBy" /></para>
        /// </summary>
        /// <value>
        /// The Modified by.
        /// </value>
        [DataMember]
        public virtual string LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets information as to whom deleted the record.
        /// <para>Member defined in<see cref="IHasDateTimeDeletedBy" /></para>
        /// </summary>
        /// <value>
        /// The deleted by.
        /// </value>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        ///   </para>
        [DataMember]
        public virtual string DeletedBy { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityBase{TEntity, TEntityAlias, TClaim}"/> class.
        /// </summary>
        protected IdentityBase()
        {
            this.GenerateDistributedId();
            //this.Enabled = true;
        } 
    }
}