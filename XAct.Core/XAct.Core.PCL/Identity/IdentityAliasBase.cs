namespace XAct.Identity
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public abstract class IdentityAliasBase : IHasDistributedGuidIdAndTimestamp, IHasNames, IHasPreferred
    {
 
        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        [DataMember]
        public virtual bool Preferred { get; set; }

        [DataMember]
        public virtual string Title { get; set; }
        
        [DataMember]
        public virtual string FirstName { get; set; }
        
        [DataMember]
        public virtual string MoreNames { get; set; }

        [DataMember]
        public virtual string SurName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityAliasBase"/> class.
        /// </summary>
        protected IdentityAliasBase()
        {
            this.GenerateDistributedId();
        }

    }
}