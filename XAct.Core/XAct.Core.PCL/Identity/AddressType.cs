﻿namespace XAct.Identity
{
    using System.Runtime.Serialization;

    /// <summary>
    /// The type of an Address
    /// <para>
    /// See <see cref="IdentityWithAddressesBase{TEntity,TEntityAlias,TEntityClaim, TEntityAddress}"/>
    /// </para>
    /// </summary>
    [DataContract]
    public enum AddressType
    {
        /// <summary>
        /// The Address is to be used for Billing purposes. 
        /// </summary>
        [EnumMember]
        Billing = 1,

        /// <summary>
        /// The Address is a Shipping address. 
        /// </summary>
        [EnumMember]
        Shipping = 2,
    }
}