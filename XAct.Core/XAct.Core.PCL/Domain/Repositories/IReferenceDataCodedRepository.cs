﻿namespace XAct.Domain.Repositories
{
    using XAct.Data.Repositories.Implementations;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// See:
    /// <see cref="IHasReferenceData{TId}"/>
    /// <see cref="IHasApplicationTennantIdSpecificReferenceData{TId}"/>
    /// <see cref="IReferenceDataRepository{TEntity,TId}"/>
    /// <see cref="IReferenceDataCodedRepository{TEntity,TId}"/>
    /// <see cref="DistributedGuidIdRepositoryBase{TEntity}"/>
    /// <see cref="ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase{TEntity}"/>
    /// </internal>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IReferenceDataCodedRepository<TEntity, TId>:IReferenceDataRepository<TEntity,TId>
        where TEntity : class, IHasCodedReferenceData<TId>, new()
        where TId : struct
    {


        /// <summary>
        /// Gets the Element by it's <see cref="IHasCode.Code"/>.
        /// </summary>
        TEntity GetByCode(string code);
    }


}