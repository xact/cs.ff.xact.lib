﻿namespace XAct.Domain.Repositories
{
    using XAct.Data.Repositories.Implementations;

    /// <summary>
    /// See: 
    /// </summary>
    /// <internal>
    /// See:
    /// <see cref="IReferenceDataCodedRepository{TEntity,TId}"/>
    /// <see cref="BasicDistributedGuidRepositoryBase{TEntity}"/>
    /// <see cref="DistributedGuidIdRepositoryServiceBase{TElement}"/>
    /// <see cref="ApplicationTennantIdSpecificDistributedGuidIdRepositoryBase{TEntity}"/>
    /// <see cref="ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase{TEntity}"/>
    /// </internal>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public interface IReferenceDataRepository<TEntity, TId> : ISimpleRepository<TEntity, TId>, IHasService
        where TEntity : class, IHasReferenceData<TId>,  new()
        where TId :struct
    {

        /// <summary>
        /// Gets the specified Element, by its <see cref="IHasText.Text"/>
        /// </summary>
        TEntity GetByText(string name);

    }
}