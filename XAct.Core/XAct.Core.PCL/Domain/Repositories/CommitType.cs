﻿namespace XAct.Domain.Repositories
{
    /// <summary>
    /// Enumeration of the types of Commit
    /// that the <see cref="IContext.Commit"/>
    /// method can perform.
    /// </summary>
    public enum CommitType
    {

        /// <summary>
        /// The default value (AcceptAllChanges)
        /// </summary>
        Default = AcceptAllChanges,
        
            /// <summary>
        /// Update the in memory Original tracking values of the Entity
        /// to match the Current values, and set the flag to Unchanged
        /// (ie, state that all changes have been written to db, and that
        /// there are no more un-written values in memory).
        /// </summary>
        AcceptAllChanges=1,

        /// <summary>
        /// Write to Db, but do not update In memory cache values/state.
        /// <para>
        /// Wait till Commit is called with <see cref="CompleteChanges"/> 
        /// (which just changes
        /// in memory state, without writting anything to db, as it's already
        /// been done).
        /// </para>
        /// </summary>
        DetectAndHoldChangesTillComplete = 2,

        
        /// <summary>
        /// Write to Db, but do not update In memory cache values/state.
        /// <para>
        /// Wait till Commit is called with <see cref="CompleteChanges"/> 
        /// (which just changes
        /// in memory state, without writting anything to db, as it's already
        /// been done).
        /// </para>
        /// </summary>
        HoldChangesTillComplete=4,



        /// <summary>
        /// Update the in memory cache, doing same changes as 
        /// <see cref="AcceptAllChanges"/>, but without writting to Db
        /// (as it's already been done  in <see cref="HoldChangesTillComplete"/>
        /// </summary>
        CompleteChanges=8,
    }
}
