﻿
//// ReSharper disable CheckNamespace
//namespace XAct.Domain.Repositories
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// A Contract for a Factory used to create new <see cref="IUnitOfWork"/>s.
//    /// <para>
//    /// Note that the <see cref="Create"/> method returns a *new* 
//    /// <see cref="UnitOfWork"/> every time it is invoked.
//    /// </para>
//    /// <para>
//    /// To return a 'Current' UoW in a consistent fashion, use
//    /// instead an implementation of 
//    /// <see cref="IUnitOfWorkManagementService"/>.
//    /// </para>
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Usage would be:
//    /// <code>
//    /// <![CDATA[
//    /// IUnitOfWorkFactory unitOfWorkFactory = 
//    ///   XAct.Services.ServiceLocatorService.Current.GetInstance<IUnitOfWorkFactory>();
//    /// using (IUnitOfWork unitOfWork = unitOfWorkFactory){ 
//    ///   IUnitOfWork unitOfWork = unitOfWorkFactory.Create();
//    /// 
//    ///       //Using the uow, create a new Repository:
//    ///       using (IExampleRepository exRepository = new ExampleRepository(abstractObjectContext)){
//    ///         ...
//    ///         repository.MakeChanges(...);
//    ///         ...
//    ///         uow.Commit();
//    ///       }
//    /// }
//    /// ]]>
//    /// </code>
//    /// <para>
//    /// </para>
//    /// </para>
//    /// </remarks>
//    /// <internal>
//    /// See: http://bit.ly/vEClOu
//    /// </internal>
//    public interface IUnitOfWorkFactory
//    {
//        /// <summary>
//        /// Creates an <see cref="IUnitOfWork"/>
//        /// <para>
//        /// The contextKey (default is null/emptyString/"default") 
//        /// can be used to generate different contexts (eg: "LoggingDb" or other).
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// <code>
//        /// Usage example:
//        /// <![CDATA[
//        /// 
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <param name="contextKey">(Optional) Name of the context.</param>
//        /// <returns>An <see cref="IUnitOfWork"/></returns>
//        IUnitOfWork Create<TContext>(string contextKey = null) where TContext : IContext;

//    }
//}
