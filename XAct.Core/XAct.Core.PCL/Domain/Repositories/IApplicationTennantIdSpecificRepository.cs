﻿namespace XAct.Domain.Repositories
{
    public interface IApplicationTennantIdSpecificRepository<TEntity, TId>
        : ISimpleRepository<TEntity,TId>
        where TEntity : class, IHasApplicationTennantId, new()
        where TId : struct
    {

    }
}