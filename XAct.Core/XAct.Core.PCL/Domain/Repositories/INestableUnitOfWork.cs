﻿
//namespace XAct.Domain.Repositories
//{
//    /// <summary>
//    /// A nestable <see cref="IUnitOfWork"/>
//    /// that Commits it changes as a block across all 
//    /// nested <see cref="IUnitOfWork"/>s.
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// According to Martin Fowler:
//    /// </para>
//    /// <para>
//    /// "When you're pulling data in and out of a database, 
//    /// it's important to keep track of what you've changed; 
//    /// otherwise, that data won't be written back into the database. 
//    /// Similarly you have to insert new objects you create 
//    /// and remove any objects you delete. 
//    /// You can change the database with each change to your object model, 
//    /// but this can lead to lots of very small database calls, 
//    /// which ends up being very slow. Furthermore it requires 
//    /// you to have a transaction open for the whole interaction, 
//    /// which is impractical if you have a business transaction that 
//    /// spans multiple requests. The situation is even worse if 
//    /// you need to keep track of the objects you've read so 
//    /// you can avoid inconsistent reads.
//    /// A Unit of Work keeps track of everything you do during a 
//    /// business transaction that can affect the database. 
//    /// When you're done, it figures out everything that needs to be 
//    /// done to alter the database as a result of your work."
//    /// </para>
//    /// </remarks>
//    public interface INestableUnitOfWork : IUnitOfWork
//    {
//        /// <summary>
//        /// Gets the <see cref="INestableUnitOfWork"/>
//        /// that manages this <see cref="INestableUnitOfWork"/>
//        /// <para>
//        /// The parent's <see cref="IUnitOfWork.Commit"/> method
//        /// will trigger this child <see cref="IUnitOfWork"/>'s
//        /// <see cref="IUnitOfWork.Commit"/> method
//        /// </para>
//        /// <para>
//        /// Units of work will be committed, last sibling first, deepest first.
//        /// </para>
//        /// <para>
//        /// Defined in <see cref="INestableUnitOfWork"/>
//        /// </para>
//        /// </summary>
//        INestableUnitOfWork Parent { get; }

//        /// <summary>
//        /// Add's the given <see cref="INestableUnitOfWork"/>
//        /// to this <see cref="INestableUnitOfWork"/>'s 
//        /// internal collection of 
//        /// <see cref="INestableUnitOfWork"/> items.
//        /// <para>
//        /// <para>
//        /// When this <see cref="INestableUnitOfWork"/>
//        /// is <see cref="IUnitOfWork.Commit"/>'ed, it will trigger
//        /// a <see cref="IUnitOfWork.Commit"/> on all the 
//        /// <see cref="INestableUnitOfWork"/>'s (deepest, last sibling first).
//        /// </para>
//        /// Defined in <see cref="INestableUnitOfWork"/>
//        /// </para>
//        /// </summary>
//        /// <param name="childUnitOfWork">The child <see cref="INestableUnitOfWork"/>.</param>
//        /// <returns>
//        ///   <see cref="INestableUnitOfWork"/>
//        /// </returns>
//        void Add(INestableUnitOfWork childUnitOfWork);

//        /// <summary>
//        /// Removes this <see cref="INestableUnitOfWork"/>
//        /// from it's parent <see cref="INestableUnitOfWork"/>.
//        /// <para>
//        /// In other words, if one wants to still commit this 
//        /// individual <see cref="INestableUnitOfWork"/>, one will
//        /// have to invoke its <see cref="IUnitOfWork.Commit"/>
//        /// method (it won't be invoked by the root <see cref="INestableUnitOfWork"/>).
//        /// </para>
//        /// <para>
//        /// Defined in <see cref="INestableUnitOfWork"/>
//        /// </para>
//        /// </summary>
//        void RemoveFromParent();  
//    }
//}
