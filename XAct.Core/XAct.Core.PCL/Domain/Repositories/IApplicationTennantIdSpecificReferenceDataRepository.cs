﻿namespace XAct.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// See:
    /// <see cref="IHasApplicationTennantIdSpecificReferenceData{TId}"/>
    /// <see cref="IApplicationTennantIdSpecificReferenceDataRepository{TEntity,TId}"/>
    /// </internal>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IApplicationTennantIdSpecificReferenceDataRepository<TEntity, TId> : ISimpleRepository<TEntity, TId>, IHasService
        where TEntity : class, IHasApplicationTennantIdSpecificReferenceData<TId>, IHasReferenceData<TId>, IHasApplicationTennantId, new()
        where TId : struct
    {

        /// <summary>
        /// Gets the specified Element, by its <see cref="IHasText.Text"/>
        /// </summary>
        TEntity GetByText(string name);

    }
}