﻿//namespace XAct.Domain.Repositories
//{
//    /// <summary>
//    /// A contract for a service that returns the current 
//    /// <see cref="IUnitOfWork"/>, or invokes
//    /// <see cref="IUnitOfWorkFactory"/> to create one.
//    /// </summary>
//    public interface IComplexUnitOfWorkManagementService : IUnitOfWorkManagementService, IHasXActLibServiceDefinition
//    {


//        /// <summary>
//        /// Gets the current <see cref="IUnitOfWork"/>,
//        /// or invokes <see cref="IUnitOfWorkFactory.Create{TContext}"/>
//        /// to create a new <see cref="IUnitOfWork"/>.
//        /// </summary>
//        /// <typeparam name="TContext">The type of the context.</typeparam>
//        /// <returns></returns>
//        /// <remarks>
//        /// This property is intented to be invoked only from within
//        /// the implementation of an <see cref="IRepositoryService{TAggregateRootEntity}"/>.
//        /// <code>
//        /// <![CDATA[
//        ///   public class EFRepository : IRepository {
//        ///     ...
//        ///     private ObjectContext ObjectContext {
//        ///       get {
//        ///         //If defined by constructor, use that:
//        ///         if (_objectContext != null){return _objectContext;}
//        ///         
//        ///         //If not defined by constructor, use service to get current UoW,
//        ///         //Defining the type of Current UoW we want back (we need type,
//        ///         //as there might be two different ORMs in play we want back
//        ///         IUnitOfWork unitOfWork = 
//        ///            _unitOfWorkManagementService.GetCurrent<IEntityObjectContext>();
//        /// 
//        ///         //From which the ObjectContext can be retrieved:
//        ///         IContext abstractObjectContext = ((UnitOfWork)unitOfWork).Context;
//        ///         ObjectContext objectContext = abstractObjectContext.InnerObject as ObjectContext;
//        /// 
//        ///         return objectContext;
//        ///       }
//        ///     }
//        ///   ...
//        ///   }
//        /// ]]>
//        /// 	</code>
//        /// </remarks>
//        IUnitOfWork GetCurrent<TContext>() where TContext : IContext;

//        /// <summary>
//        /// Peeks this instance.
//        /// </summary>
//        /// <typeparam name="TContext">The type of the context.</typeparam>
//        /// <returns></returns>
//        IUnitOfWork Peek<TContext>();


//        /// <summary>
//        /// Pushes the specified <see cref="IUnitOfWork"/> onto the (<typeparamref name="TContext"/> specific) stack.
//        /// <para>
//        /// The long winded version of usage would be similar to:
//        /// <code>
//        /// <![CDATA[
//        /// //Management is not the same as factory, hence two classes:
//        /// IUnitOfWorkFactory unitOfWorkFactory =
//        ///    XAct.Services.ServiceLocator.Current.Get<IUnitOfWorkFactory>();
//        /// 
//        /// IUnitOfWorkManagementService unitOfWorkManagementService =
//        ///    XAct.Services.ServiceLocator.Current.Get<IUnitOfWorkManagementService>();
//        /// 
//        /// //Obviously, with DI, and not SL, one only sees two lines really:
//        /// IUnitOfWork uow = IUnitOfWorkFactory.Create<IEFObjectContext>(); 
//        /// IUnitOfWorkManagementService.Push<IEFObjectContext>(uoW);
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </summary>
//        /// <param name="unitOfWork">The unit of work.</param>
//        void Push<TContext>(IUnitOfWork unitOfWork);

//        /// <summary>
//        /// Pop the latest <see cref="IUnitOfWork"/> back off the (<typeparamref name="TContext"/> specific) stack.
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Once you are finished with the current UoW, you just pop it off:
//        /// </para>
//        /// <para>
//        /// The long winded version of usage would be similar to:
//        /// <code>
//        /// <![CDATA[
//        /// //Management is not the same as factory, hence two classes:
//        /// IUnitOfWorkManagementService unitOfWorkManagementService =
//        ///    XAct.Services.ServiceLocator.Current.Get<IUnitOfWorkManagementService>();
//        /// 
//        /// IUnitOfWorkManagementService.Pop<IEFObjectContext>();
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        IUnitOfWork Pop<TContext>();

//        /// <summary>
//        /// Counts the number of <see cref="IUnitOfWork"/> on the (<typeparamref name="TContext"/> specific) stack.
//        /// <para>
//        /// Useful to Assert that you know how many you have added, and where you are at.
//        /// </para>
//        /// </summary>
//        int Count<TContext>();
//    }
//}