﻿using System.Collections.Generic;

namespace XAct.Domain.Repositories.Implementations
{
    using System;
    using XAct.Domain.Repositories.Configuration;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class UnitOfWorkServiceConfiguration : IUnitOfWorkServiceConfiguration
    {

        public void SetFactoryDelegate(Func<IUnitOfWork> func, string contextKey= null)
        {
            if (contextKey.IsNullOrEmpty())
            {
                contextKey = XAct.Library.Constants.UnitOfWork.DefaultContextKey;
            }
            contextKey = contextKey.ToLower();

            _delegates[contextKey] = func;
        }

        public Func<IUnitOfWork> GetFactoryDelegate(string contextKey=null)
        {
            if (contextKey.IsNullOrEmpty())
            {
                contextKey = XAct.Library.Constants.UnitOfWork.DefaultContextKey;
            }
            contextKey = contextKey.ToLower();
            Func<IUnitOfWork> result;

            return _delegates.TryGetValue(contextKey, out result) ? result : null;
        }

        readonly IDictionary<string, Func<IUnitOfWork>> _delegates = new Dictionary<string,Func<IUnitOfWork>>();
    }
}