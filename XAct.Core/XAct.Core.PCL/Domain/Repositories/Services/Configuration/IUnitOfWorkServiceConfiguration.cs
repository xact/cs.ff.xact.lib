﻿// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories.Configuration
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface IUnitOfWorkServiceConfiguration : IHasXActLibServiceConfiguration
    {
        
        void SetFactoryDelegate(Func<IUnitOfWork> func, string key = null);

        Func<IUnitOfWork> GetFactoryDelegate(string key=null);
    }
}