﻿// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories
// ReSharper restore CheckNamespace
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Messages;

    /// <summary>
    ///   The interface for a Generic Repository.
    ///  <para>
    /// Default implementation is <c>XAct.Domain.Repositories.EntityDbContextGenericRepositoryService</c>
    /// defined in <c>XAct.Data.EF</c>
    /// </para>
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <internal>
    ///   <para>
    ///     A Repository must remain persitence ignorant, 
    ///     not knowing how or when changes will be persisted
    ///     to the underlying datastore, leaving the 'when' to being
    ///     handled by the <see cref="IUnitOfWork"/>. 
    ///   </para>
    ///   <para>
    ///     http://elegantcode.com/2009/12/15/model-framework-ef4-generic-repository-and-unit-of-work-prototype/comment-page-1/
    ///   </para>
    /// <para>
    /// Updated with inspiration from http://lostechies.com/jimmybogard/2009/09/03/ddd-repository-implementation-patterns/
    /// </para>
    /// </internal>
    public interface IRepositoryService : IHasXActLibService
    {


        /// <summary>
        /// Gets the current <see cref="IUnitOfWork"/> from the underlying 
        /// <c>IUnitOfWorkManagementService</c>.
        /// <para>
        /// Use that to <c>Execute</c> it's internal <see cref="IContext"/>.
        /// </para>
        /// <para>
        /// Note that I am not especially happy having the Repository (Domain) have a reference 
        /// to an infrastructural context -- but it's just easier not having to inject two
        /// different contracts into every location the contract is needed.
        /// </para>
        /// </summary>
        IUnitOfWork GetContext(string contextKey=null);
    

        /// <summary>
        /// Determines if any elements with the specified condition exists.
        /// (faster than Count).
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="filter"></param>
        /// <returns></returns>
        bool Exists<TModel>(Expression<Func<TModel, bool>> filter = null) where TModel : class;

        /// <summary>
        /// Get the total of objects that meet the criteria.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// 	</para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// int howManyExpensiveThings =
        ///   _exampleRepository.Count<Example>(ex=>ex.Price > 10000);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="filter">The (optional) predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <returns></returns>
        /// <internal>
        /// Generally implemented without using <see cref="GetByFilter{TModel}"/>
        /// as not all Providers will create the same SQL.
        ///   </internal>
        int Count<TModel>(Expression<Func<TModel, bool>> filter = null)
            where TModel : class;

        /// <summary>
        /// Gets whether the object(s) exists in the datastore.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// bool hasExpensiveStuff =
        ///   _exampleRepository.Contains<Example>(ex=>ex.Price > 10000);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        bool Contains<TModel>(Expression<Func<TModel, bool>> filter)
            where TModel : class;


        //GetAll is a stupid red herring, seriously putting in jeopardy the rest of the code 
        //by exposing a means to pull back every single record in one fell swoop.
        //GetByFilter at least puts in place a means of Paging the results.

        //[Obsolete("Doesn't give much value -- better to be explicit using other GetSingle")]
        //TModel GetSingle(params object[] keys);

        /// <summary>
        /// Find a single model, based on the given predicate filter.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// 	</para>
        /// </summary>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <param name="includeSpecification">The include specifications.</param>
        /// <param name="mergeOptions">The merge options.</param>
        /// <returns></returns>
        /// <remarks>
        /// Usage Example:
        /// <code>
        /// 		<![CDATA[
        /// Example one =
        /// _exampleRepository.GetSingle<Example>(ex=>ex.Id=1,new IncludeSpecification("SubExampleItems"));
        /// Example two =
        /// _exampleRepository.GetSingle<Example>(ex=>ex.Id=2,null);
        /// Example three =
        /// _exampleRepository.GetSingle<Example>(ex=>ex.Id=3);
        /// ]]>
        /// 	</code>
        /// </remarks>
        TModel GetSingle<TModel>(Expression<Func<TModel, bool>> filter,
            IIncludeSpecification includeSpecification = null, 
            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined)
            where TModel : class;


        /// <summary>
        /// Returns an <see cref="IQueryable"/> of all entities
        /// in the datastore that match the given filter,
        /// using the given paging specs.
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
    /// 	</para>
        /// 	<para>IMPORTANT: Note the Syntax for the OrderBy (q =&gt; q.OrderBy(d =&gt; d.Name)), it is not just a property reference (and therefore allows for Field + Direction + Chaining).</para>
        /// 	<para>
        /// Invoke as follows:
        /// <code>
        /// 			<![CDATA[
        /// var departmentsQuery =
        /// _exampleRepository
        /// .GetByFilter(
        /// contact => contact.Age >= 18,
        /// new IncludeSpecification(c =>.Addresses, c=>Invoices.Select(i=>i.LineItems)),
        /// paging: new PagedQuerySpecification(20,0),
        /// orderBy: q => q.OrderBy(d => d.Name),
        /// new IncludeSpecification("Addresses"));
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </summary>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <param name="includeSpecification">The (optional) list of child Entities (Properties) to include in this query to avoid the later cost of Lazy loading.</param>
        /// <param name="pagedQuerySpecs">The (optional) paged query specs.</param>
        /// <param name="orderBy">The (optional) order by.</param>
        /// <param name="mergeOptions">The merge options.</param>
        /// <returns></returns>
        IQueryable<TModel> GetByFilter<TModel>(
            Expression<Func<TModel, bool>> filter,
            IIncludeSpecification includeSpecification=null,
            IPagedQuerySpecification pagedQuerySpecs=null,
            Func<IQueryable<TModel>, IOrderedQueryable<TModel>> orderBy = null,
            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined
            )
            where TModel : class;



        /// <summary>
        /// Persists the given entity.
        /// <para>
        /// The implementation invokes  <paramref name="isEntityNew"/> 
        /// to determine whether to invoke
        /// <see cref="AddOnCommit{TModel}"/> (for new Aggregate Root Entities)
        /// or <see cref="UpdateOnCommit{TModel}"/>
        /// 	</para>
        /// 	<para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// 	<para>
        /// Defined in <see cref="IRepositoryService"/>
        /// 	</para>
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <param name="model">The aggregate root.</param>
        /// <param name="isEntityNew">The is entity new.</param>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects
        /// or displays the results.
        /// In other words, the Controller get's DI'ed
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// 	<para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page.
        /// </para>
        /// 	<para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        void PersistOnCommit<TModel>(TModel model, Func<TModel,bool> isEntityNew)
            where TModel : class;



        /// <summary>
        /// Adds or updates a model which matches the given criteria.
        /// Used only for seeding.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <param name="identifierExpression">The identifier expression.</param>
        /// <param name="models">The models.</param>
        void AddOrUpdate<TModel>(Expression<Func<TModel, object>> identifierExpression, params TModel[] models)
            where TModel : class;


        /// <summary>
        /// Persists the given entity.
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService" />
        /// </para>
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <typeparam name="TId">The type of the identifier.</typeparam>
        /// <param name="model">The aggregate root.</param>
        /// <param name="generateDistributeIdentityIfRequired">if set to <c>true</c>, generates a distribute identity (guid) if required.</param>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects
        /// or displays the results.
        /// In other words, the Controller get's DI'ed
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page.
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        void PersistOnCommit<TModel, TId>(TModel model, bool generateDistributeIdentityIfRequired=false)
            where TModel : class, IHasId<TId>
            ;


        /// <summary>
        ///   Adds the given model to the datastore,
        ///   when the active 
        ///   <see cref = "IUnitOfWork" />
        ///   is Committed.
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name = "model"></param>
        void AddOnCommit<TModel>(TModel model)
            where TModel : class;



        /// <summary>
        ///   Adds the given model to the datastore,
        ///   when the active 
        ///   <see cref = "IUnitOfWork" />
        ///   is Committed.
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <param name = "model"></param>
        /// <param name="generateDistributeIdentityIfRequired">if set to <c>true</c>, generates a distribute identity (guid) if required.</param>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        void AddOnCommit<TModel, TId>(TModel model,
                                                    bool generateDistributeIdentityIfRequired = true)
            where TModel : class, IHasId<TId>
            ;


        /// <summary>
        /// Attaches untracked -- but already saved at some point --
        /// entities.the on commit.
        /// <para>
        /// NOTE: When possible (not all ORMs can pull it off) prefer
        /// <see cref="UpdateOnCommit{TModel}"/> to <see cref="AttachOnCommit{TModel}"/>
        /// as higher levels should not have to keep track of what has
        /// been added or not.
        /// </para>
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name="model">The aggregate root entity.</param>
        /// <returns></returns>
        void AttachOnCommit<TModel>(TModel model)
            where TModel : class;


        /// <summary>
        /// Detaches the specified aggregate root entity.
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <param name="model">The aggregate root entity.</param>
        void Detach<TModel>(TModel model)
            where TModel : class;


        /// <summary>
        /// Update object changes and save to database.
        /// <para>
        /// Note that if entity is not being tracked (ie, untracked)
        /// will Attach first, then change state to Modified.
        /// </para>
        /// <para>
        /// By doing it this way, removes from higher levels the need 
        /// to keep track of what has been attached or not.
        /// </para>
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name="model">Specified the object to save.</param>
        void UpdateOnCommit<TModel>(TModel model)
            where TModel : class;



        /// <summary>
        /// Marks the element for deleting, without incuring the cost
        /// of retrieving the element.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TId"></typeparam>
        /// <param name="id"></param>
        void DeleteOnCommit<TModel, TId>(TId id)
            where TModel : class, IHasId<TId>, new();


        /// <summary>
        ///   Removes the given model from the datastore
        ///   when the active 
        ///   <see cref = "IUnitOfWork" />
        ///   is Committed.
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name = "model"></param>
        /// <returns>The number of Elements changed in the current commit (may not be exact match to number of deleted).</returns>
        void DeleteOnCommit<TModel>(TModel model)
            where TModel : class;


        /// <summary>
        /// Removes entities from the datastore
        /// that match the given filter,
        /// when the active
        /// <see cref="IUnitOfWork"/>
        /// is Committed.
        /// <para>
        /// Note: Intended to be implemented *explicity* by a RepositoryBase,
        /// as it is an expensive method to invoke without constraints.
        /// </para>
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name="predicate">The predicate.</param>
        /// <returns>The number of Elements changed in the current commit (may not be exact match to number of deleted).</returns>
        void DeleteOnCommit<TModel>(Expression<Func<TModel, bool>> predicate)
            where TModel : class;



        /// <summary>
        /// Determines whether the specified entity is attached, and within the dbcontext.
        /// </summary>
        /// <typeparam name="TModel">The type of the aggregate root entity.</typeparam>
        /// <param name="model">The aggregate root entity.</param>
        /// <returns></returns>
        bool IsAttached<TModel>(TModel model)
            where TModel : class;

        //    void Attach<TModel>(TModel entity);


        /// <summary>
        /// Determines whether the specified model is new.
        /// <para>
        /// Invoke from Persist:
        /// <code>
        /// <![CDATA[
        /// _repositoryService.Persist(x,_repositoryService.IsNew(x));
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TId">The type of the identifier.</typeparam>
        /// <param name="model">The model.</param>
        /// <param name="generateIdentity">if set to <c>true</c> [generate identity].</param>
        /// <returns></returns>
        bool IsNew<T, TId>(T model, bool generateIdentity = false)
            where T : IHasId<TId>
            ;

    }
}