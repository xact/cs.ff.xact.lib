﻿namespace XAct.Domain.Repositories
{
    using System;
    using XAct.Domain.Repositories.Configuration;

    /// <summary>
    /// 
    /// </summary>
    public interface IUnitOfWorkService : IHasXActLibService, IHasWebThreadBindingScope
    {
        /// <summary>
        /// Gets or sets the singleton configuration settings used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IUnitOfWorkServiceConfiguration Configuration { get; }


        /// <summary>
        /// Registers the <see cref="IUnitOfWork"/> factory.
        /// </summary>
        /// <param name="contextFactoryDelegate">The context factory delegate.</param>
        void RegisterFactory<TContext>(Func<IUnitOfWork> contextFactoryDelegate)
            where TContext : IContext;


        string DefaultContextKey { get; set; }
        
        void SetCurrent(IUnitOfWork unitOfWork, string contextKey = null);

        IUnitOfWork GetCurrent(string contextKey = null);
    }
}