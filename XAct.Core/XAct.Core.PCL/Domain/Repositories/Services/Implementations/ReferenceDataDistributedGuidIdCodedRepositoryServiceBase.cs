﻿namespace XAct.Data.Repositories.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;


    public abstract class ReferenceDataDistributedGuidIdCodedRepositoryServiceBase<TEntity> :
        DistributedGuidIdRepositoryServiceBase<TEntity>
        , IReferenceDataCodedRepository<TEntity, Guid>
        where TEntity : class, IHasCodedReferenceData<Guid>, IHasDistributedGuidIdAndTimestamp, new()
    {
        protected ReferenceDataDistributedGuidIdCodedRepositoryServiceBase(ITracingService tracingService,
                                                          IRepositoryService repositoryService)
            : base(tracingService, repositoryService)
        {
        }



        /// <summary>
        /// Gets the Element by it's <see cref="IHasText.Text"/>.
        /// </summary>
        public TEntity GetByText(string text)
        {
            return _repositoryService.GetSingle<TEntity>(x => x.Text == text);
        }

        /// <summary>
        /// Gets the Element by it's <see cref="IHasCode.Code"/>.
        /// </summary>
        public TEntity GetByCode(string code)
        {
            return _repositoryService.GetSingle<TEntity>(x => x.Code == code);
        }
    }
}