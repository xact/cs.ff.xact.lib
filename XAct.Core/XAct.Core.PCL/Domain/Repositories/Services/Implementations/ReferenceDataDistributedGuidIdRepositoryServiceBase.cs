﻿namespace XAct.Data.Repositories.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;

    public abstract class ReferenceDataDistributedGuidIdRepositoryServiceBase<TEntity> :
        DistributedGuidIdRepositoryServiceBase<TEntity>, IReferenceDataRepository<TEntity, Guid>
        where TEntity : class, IHasDistributedGuidIdAndTimestamp, IHasReferenceData<Guid>, new()
    {
        protected ReferenceDataDistributedGuidIdRepositoryServiceBase(ITracingService tracingService,
                                                                      IRepositoryService repositoryService)
            : base(tracingService, repositoryService)
        {
        }


        public TEntity GetByText(string text)
        {
            return _repositoryService.GetSingle<TEntity>(x => x.Text == text);
        }
    }
}