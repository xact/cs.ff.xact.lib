﻿namespace XAct.Data.Repositories.Implementations
{
    using System;
    using System.Linq.Expressions;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;


    /// <summary>
    /// An abstract base class for a typed repository to manage 
    /// entities that implement <see cref="IHasId{Id}"/> where the Id type
    /// is a Distributed Guid Id (ie, generated via the <see cref="IDistributedIdService"/>).
    /// <para>
    /// See:
    ///   <see cref="ISimpleRepository{TModel,TId}"/>
    ///   <see cref="IApplicationTennantIdSpecificRepository{TEntity,TId}"/>
    ///   <see cref="IReferenceDataRepository{TEntity,TId}" />
    ///   <see cref="IReferenceDataCodedRepository{TEntity,TId}" />
    ///   <see cref="IApplicationTennantIdSpecificReferenceDataRepository{TEntity, TId}"/>
    /// </para>
    /// <para>
    /// See: 
    ///   <see cref="ServiceBase"/>
    ///   <see cref="RepositoryServiceBase{TModel,Guid}" />
    ///   <see cref="DistributedGuidIdRepositoryServiceBase{TElement}"/>
    ///   <see cref="ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase{TEntity}"/>
    ///   <see cref="DistributedGuidIdRepositoryServiceBase{TElement}" />
    ///   <see cref="ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase{TModel}"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TElement">The type of the element.</typeparam>
    public abstract class DistributedGuidIdRepositoryServiceBase<TElement> : RepositoryServiceBase<TElement,Guid>

        where TElement : class, IHasDistributedGuidIdAndTimestamp, new()
    {

        protected DistributedGuidIdRepositoryServiceBase(ITracingService tracingService,
                                                         
                                                         IRepositoryService repositoryService)
            : base(tracingService, repositoryService)
        {
        }


        protected override Expression<Func<TElement, bool>> IdEquality(Guid id)
        {
            Expression<Func<TElement, bool>> result = (x) => x.Id == id;
            return result;
        }

    }
}