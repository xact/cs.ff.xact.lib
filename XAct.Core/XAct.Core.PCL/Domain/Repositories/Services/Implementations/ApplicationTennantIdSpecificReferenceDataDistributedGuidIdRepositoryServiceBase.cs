﻿namespace XAct.Data.Repositories.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;

    public abstract class ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase<TEntity> :
        ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase<TEntity>
        , IApplicationTennantIdSpecificReferenceDataRepository<TEntity, Guid>
        where TEntity : class, IHasApplicationTennantIdSpecificDistributedGuidId, IHasApplicationTennantId, IHasApplicationTennantIdSpecificReferenceData<Guid>, IHasDistributedGuidIdAndTimestamp
            , new()
    {

        protected ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase(ITracingService tracingService,
                                                                            IApplicationTennantService
                                                                                applicationTennantService,
                                                                            IRepositoryService repositoryService)
            : base(tracingService, applicationTennantService, repositoryService)
        {
        }


        public TEntity GetByText(string text)
        {
            var applicationTennantId = _applicationTennantService.Get();
            return
                _repositoryService.GetSingle<TEntity>(
                    x => x.ApplicationTennantId == applicationTennantId && x.Text == text);
        }

    }
}