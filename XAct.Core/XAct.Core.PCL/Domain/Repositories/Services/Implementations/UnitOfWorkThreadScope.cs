﻿using System.Collections.Generic;

namespace XAct.Domain.Repositories
{
    using System;
    using XAct.Domain.Repositories.Configuration;

    /// <summary>
    /// Creates a thread specific context for retrieving a different
    /// ServiceLocator.
    /// <para>
    /// Usage:
    /// <code>
    /// <![CDATA[
    /// var r1 = XAct.DependencyLocator.Current.GetInstance<ISomeService>();
    /// var r2 = XAct.DependencyLocator.Current.GetInstance<ISomeService>();
    /// 
    /// using (var scope = new ServiceLocatorThreadScope()){
    ///   r2 = XAct.DependencyLocator.Current.GetInstance<ISomeService>();
    ///   //r1 == r2
    ///   //but
    ///   //r3 != r1
    ///   //which is useful in the case of multi-threaded scenarios, such
    ///   //as when using a PriorityQueue<T>
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public class UnitOfWorkThreadScope : IDisposable
    {
        //[ThreadStatic]
        //public static string ThreadScopeName;

        [ThreadStatic]
        private static IDictionary<string, IUnitOfWork> ThreadUnitOfWork = new Dictionary<string, IUnitOfWork>();

        public static bool TryGet(string contextKey, out IUnitOfWork unitOfWork)
        {
            if (contextKey.IsNullOrEmpty()) { contextKey = XAct.Library.Constants.UnitOfWork.DefaultContextKey; }
            contextKey = contextKey.ToLower();

            if (ThreadUnitOfWork == null) { ThreadUnitOfWork = new Dictionary<string, IUnitOfWork>();}
            var result = ThreadUnitOfWork.TryGetValue(contextKey, out unitOfWork);
            return result;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkThreadScope"/> class.
        /// </summary>
        public UnitOfWorkThreadScope(string contextKey = null, bool useThreadStorage=true)
        {
            if (contextKey.IsNullOrEmpty())
            {
                contextKey = XAct.Library.Constants.UnitOfWork.DefaultContextKey;
            }
            contextKey = contextKey.ToLower();

            var unitOfWorkServiceConfiguration =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>();

            IUnitOfWork result;
            if (TryGet(contextKey, out result))
            {
                return;
            }
            result = unitOfWorkServiceConfiguration.GetFactoryDelegate(contextKey).Invoke();
            if (result == null)
            {
                throw new Exception("UnitOfWorkServiceConfiguration.GetFactoryDelegate returned null. Did you forget to invoe SetFactoryDelegate in your app's Bootstrapper?");
            }
            if (useThreadStorage)
            {
                ThreadUnitOfWork[contextKey] = result;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkThreadScope"/> class.
        /// </summary>
        public UnitOfWorkThreadScope(IUnitOfWorkServiceConfiguration unitOfWorkServiceConfiguration)
        {
            IUnitOfWork result;
            if (TryGet(XAct.Library.Constants.UnitOfWork.DefaultContextKey, out result))
            {
                return;
            }
            unitOfWorkServiceConfiguration.GetFactoryDelegate(XAct.Library.Constants.UnitOfWork.DefaultContextKey).Invoke();
            ThreadUnitOfWork[XAct.Library.Constants.UnitOfWork.DefaultContextKey] = result;
        }

        public void Dispose()
        {
            ThreadUnitOfWork = null;
        }
    }
}