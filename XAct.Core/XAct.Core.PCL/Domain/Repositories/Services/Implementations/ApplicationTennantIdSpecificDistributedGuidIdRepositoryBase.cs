﻿namespace XAct.Data.Repositories.Implementations
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Expressions;
    using XAct.Messages;
    using XAct.Services;


    /// <summary>
    /// An abstract base class for a typed repository to manage 
    /// entities that implement <see cref="IHasId{Id}"/> where the Id type
    /// is a Distributed Guid Id (ie, generated via the <see cref="IDistributedIdService"/>)
    /// and implements <see cref="IHasApplicationTennantId"/>.
    /// <para>
    /// See:
    ///   <see cref="ISimpleRepository{TModel,TId}"/>
    ///   <see cref="IApplicationTennantIdSpecificRepository{TEntity,TId}"/>
    ///   <see cref="IReferenceDataRepository{TEntity,TId}" />
    ///   <see cref="IReferenceDataCodedRepository{TEntity,TId}" />
    ///   <see cref="IApplicationTennantIdSpecificReferenceDataRepository{TEntity, TId}"/>
    /// </para>
    /// <para>
    /// See: 
    ///   <see cref="ServiceBase"/>
    ///   <see cref="RepositoryServiceBase{TModel,Guid}" />
    ///   <see cref="DistributedGuidIdRepositoryServiceBase{TElement}"/>
    ///   <see cref="ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase{TEntity}"/>
    ///   <see cref="DistributedGuidIdRepositoryServiceBase{TElement}" />
    ///   <see cref="ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase{TModel}"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TElement">The type of the element.</typeparam>
    public abstract class ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase<TElement> : 
        DistributedGuidIdRepositoryServiceBase<TElement>,
        IApplicationTennantIdSpecificRepository<TElement,Guid> where TElement :
        class,  IHasApplicationTennantIdSpecificDistributedGuidId, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, new()
    {
        protected readonly IApplicationTennantService _applicationTennantService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase{TElement}"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="applicationTennantService">The application tennant service.</param>
        /// <param name="repositoryService">The repository service.</param>
        protected ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase(
            ITracingService tracingService, 
            IApplicationTennantService applicationTennantService,
            IRepositoryService repositoryService) : base(tracingService, repositoryService)
        {
            _applicationTennantService = applicationTennantService;
        }

        public override void DeleteOnCommit(TElement element)
        {
            if (element.ApplicationTennantId != _applicationTennantService.Get())
            {
                throw new Exception("Cannot delete an element whose ApplicationTennantId does not match current one.");
            }

            base.DeleteOnCommit(element);
        }
        public override void PersistOnCommit(TElement element)
        {
            if (element.ApplicationTennantId != _applicationTennantService.Get())
            {
                throw new Exception("Cannot persist an element whose ApplicationTennantId does not match current one.");
            }
            base.PersistOnCommit(element);
        }
        public override TElement GetById(Guid id)
        {
            var applicationTennantId = _applicationTennantService.Get();

            var result =
                _repositoryService.GetSingle<TElement>(
                x => (x.ApplicationTennantId == applicationTennantId) 
                    && (x.Id == id));

            return result;
        }


        public override bool Exists(Guid id)
        {

            var applicationTennantId = _applicationTennantService.Get();

            var predicate = PredicateBuilder.Create<TElement>(x => x.ApplicationTennantId == applicationTennantId);

            predicate = predicate.And(x => x.Id == id);

            return _repositoryService.Exists<TElement>(predicate);
        }

        public override bool Exists(Expression<Func<TElement, bool>> where)
        {
            var applicationTennantId = _applicationTennantService.Get();

            var predicate = PredicateBuilder.Create<TElement>(x => x.ApplicationTennantId == applicationTennantId);

            if (where != null)
            {
                predicate = predicate.And(where);
            }
            
            return _repositoryService.Exists<TElement>(predicate);

        }
        public override int Count()
        {
            var applicationTennantId = _applicationTennantService.Get();

            var predicate = PredicateBuilder.Create<TElement>(x => x.ApplicationTennantId == applicationTennantId);

            return _repositoryService.Count<TElement>(predicate);
        }

        public override int Count(Expression<Func<TElement, bool>> where)
        {

            
            var applicationTennantId = _applicationTennantService.Get();

            var predicate = PredicateBuilder.Create<TElement>(x => x.ApplicationTennantId == applicationTennantId);

            if (where != null)
            {
                predicate = predicate.And(where);
            }
            
            return _repositoryService.Count<TElement>(predicate);

        }


        public override IQueryable<TElement> Retrieve(PagedQuerySpecification pagedQuerySpecification)
        {
            var applicationTennantId = _applicationTennantService.Get();
            return Retrieve(x => x.ApplicationTennantId == applicationTennantId, null, pagedQuerySpecification);
        }

        protected override TElement Get(Expression<Func<TElement, bool>> where)
        {
            var applicationTennantId = _applicationTennantService.Get();

            var predicate = PredicateBuilder.Create<TElement>(x => x.ApplicationTennantId == applicationTennantId);

            if (where != null)
            {
                predicate = predicate.And(where);
            }

            var result = _repositoryService.GetSingle<TElement>(predicate);

            return result;
        }


        protected override IQueryable<TElement> Retrieve(Expression<Func<TElement, bool>> where, Func<IQueryable<TElement>, IOrderedQueryable<TElement>> orderBy = null, PagedQuerySpecification pagedQuerySpecification = null)
        {
            if (pagedQuerySpecification == null) { pagedQuerySpecification = new PagedQuerySpecification(); }

            var applicationTennantId = _applicationTennantService.Get();

            var predicate = PredicateBuilder.Create<TElement>(x => x.ApplicationTennantId == applicationTennantId);

            if (where != null)
            {
                predicate = predicate.And(where);
            }
            
            return _repositoryService.GetByFilter<TElement>(predicate, null, pagedQuerySpecification, orderBy);

        }





    }
}