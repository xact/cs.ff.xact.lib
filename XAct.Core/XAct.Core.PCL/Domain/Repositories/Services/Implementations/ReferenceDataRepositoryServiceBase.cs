﻿
namespace XAct.Data.Repositories.Implementations
{
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;


    public abstract class ReferenceDataRepositoryServiceBase<TElement, TId> : 
        RepositoryServiceBase<TElement,TId>, IReferenceDataRepository<TElement,TId>
        where TElement : class, IHasReferenceData<TId>,new()
        where TId:struct

    {
        protected ReferenceDataRepositoryServiceBase(ITracingService tracingService, IRepositoryService repositoryService) : 
            base(tracingService, repositoryService)
        {
        }

        /// <summary>
        /// Gets the specified Category, by its name
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public TElement GetByText(string text)
        {
            return _repositoryService.GetSingle<TElement>(x => x.Text == text);
        }

    }
}
