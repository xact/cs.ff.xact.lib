﻿namespace XAct.Domain.Repositories.Implementations
{
    using System;
    using System.Globalization;
    using XAct.Domain.Repositories.Configuration;
    using XAct.Services;
    using XAct.State;


    /// <summary>
    /// An implementation of the <see cref="IUnitOfWorkService"/>
    /// to return a UoW containing the current repository's db context.
    /// </summary>
    public class UnitOfWorkService : IUnitOfWorkService
    {
        public const string ContextKeyPrefix = "XAct.Lib.UnitOfWork";
        private string _contextKey = XAct.Library.Constants.UnitOfWork.DefaultContextKey;

        private readonly IContextStateService _contextStateService;
        private readonly IUnitOfWorkServiceConfiguration _unitOfWorkServiceConfiguration;
        private Func<IUnitOfWork> _factoryMethod;



        /// <summary>
        /// Gets or sets the configuration used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IUnitOfWorkServiceConfiguration Configuration
        {
            get { return _unitOfWorkServiceConfiguration; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkService"/> class.
        /// </summary>
        public UnitOfWorkService(IContextStateService contextStateService,
                                 IUnitOfWorkServiceConfiguration unitOfWorkServiceConfiguration)
        {
            _contextStateService = contextStateService;
            _unitOfWorkServiceConfiguration = unitOfWorkServiceConfiguration;
        }


        /// <summary>
        /// Registers the factory.
        /// </summary>
        /// <param name="contextFactoryDelegate">The context factory delegate.</param>
        public void RegisterFactory<TContext>(Func<IUnitOfWork> contextFactoryDelegate) where TContext : IContext
        {
            _factoryMethod = contextFactoryDelegate;
        }



        /// <summary>
        /// The default context is 
        /// XAct.Library.Constants.UnitOfWork.DefaultContextKey
        /// </summary>
        public string DefaultContextKey
        {
            get { return _contextKey; }
            set
            {
                if (value.IsNullOrEmpty())
                {
                    //Can never be cleared:
                    value = XAct.Library.Constants.UnitOfWork.DefaultContextKey;
                }
                _contextKey = value;
            }
        }

        public void SetCurrent(IUnitOfWork unitOfWork,string contextKey=null)
        {
            //If no name supplied, we're talking about the current context
            if (contextKey.IsNullOrEmpty())
            {
                contextKey = this.DefaultContextKey;
            }
            //Should we double check?
            //if (contextKey.IsNullOrEmpty())
            //{
            //    contextKey = XAct.Library.Constants.UnitOfWork.DefaultContextKey;
            //}

            contextKey = contextKey.ToLower();

            string fullContextKey = ContextKeyPrefix + ":" + contextKey;

            _contextStateService.Items[fullContextKey] = unitOfWork;
        }


        public IUnitOfWork GetCurrent(string contextKey=null)
        {


            //If no name supplied, we're talking about the current context
            if (contextKey.IsNullOrEmpty())
            {
                contextKey = this.DefaultContextKey;
            }
            //Should we double check?
            //if (contextKey.IsNullOrEmpty())
            //{
            //    contextKey = XAct.Library.Constants.UnitOfWork.DefaultContextKey;
            //}


            IUnitOfWork result;
            if (UnitOfWorkThreadScope.TryGet(contextKey,out result))
            {
                return result;
            }


            contextKey = contextKey.ToLower();

            string fullContextKey = ContextKeyPrefix + ":" + contextKey;

            //We're looking for a previously created DbContext.
            //We only use the Func to create a new one if none was
            //found.

            //In a multi-thread app, we can't simply use a backing private var.
            //And certainly not a static private(as the thread can be reallocated to other
            //iis requests without asking, and that means your var would leak to other threads
            //and vice-versa...)
            //So we ue the thread specific context, which in IIS is souped up in a way that
            //it is serailzied/deserialized when thread swapping -- ensuring that vars don't leak
            //across threads.
            IUnitOfWork unitOfWork = _contextStateService.Items[fullContextKey] as IUnitOfWork;

            if (unitOfWork != null)
            {
                return unitOfWork;
            }

            //If not found an instance, we use the factory to create one.

            //The factory can be specific to this IUnitOfWorkService instance:
            Func<IUnitOfWork> factoryDelegate = _factoryMethod;

            //Or from a factory shared across instances:
            if (_factoryMethod == null)
            {
                factoryDelegate = _unitOfWorkServiceConfiguration.GetFactoryDelegate(contextKey);
            }
            //Either way: we create a UnitOfWork, which is most often an EntityDbCOntext object
            //which implements IUnitOfWork, as well as IContext.
            if (factoryDelegate != null)
            {
                unitOfWork = factoryDelegate.Invoke();
            }
            //ANd we cahe it.
            _contextStateService.Items[fullContextKey] = unitOfWork;

            return unitOfWork;
            
        }

    }
}