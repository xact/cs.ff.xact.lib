﻿//namespace XAct.Domain.Repositories.Implementations
//{
//    using System;
//    using System.Collections.Generic;
//    using XAct.Services;
//    using XAct.State;



//    /// <summary>
//    /// A default implementation of the 
//    /// <see cref="IUnitOfWorkManagementService"/>
//    /// </summary>
//    [DefaultBindingImplementation(typeof(IComplexUnitOfWorkManagementService))]
//    public class ComplextUnitOfWorkManagementService : IComplexUnitOfWorkManagementService
//    {
//        private readonly IContextStateService _contextStateService;

//        /// <summary>
//        /// Gets a value indicating whether [create uo W if now found].
//        /// </summary>
//        /// <value>
//        /// 	<c>true</c> if [create uo W if now found]; otherwise, <c>false</c>.
//        /// </value>
//        public bool CreateUoWIfNowFound { get; set; }


//        /// <summary>
//        /// Initializes a new instance of the 
//        /// <see cref="ComplexUnitOfWorkManagementService"/> class.
//        /// </summary>
//        /// <param name="contextStateService">The context state service.</param>
//        public ComplexUnitOfWorkManagementService(IContextStateService contextStateService)
//        {
//            contextStateService.ValidateIsNotDefault("contextStateService");

//            _contextStateService = contextStateService;
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="ComplexUnitOfWorkManagementService"/> class.
//        /// </summary>
//        /// <param name="contextStateService">The context state service.</param>
//        /// <param name="configuration">The configuration.</param>
//        public ComplexUnitOfWorkManagementService(IContextStateService contextStateService,
//                                                  UnitOfWorkManagementServiceConfiguration configuration)
//            : this(contextStateService)
//        {
//            this.CreateUoWIfNowFound = configuration.CreateUoWIfNotFound;
//        }





//        /// <summary>
//        /// Gets the current <see cref="IUnitOfWork"/>,
//        /// or invokes <see cref="IUnitOfWorkFactory.Create{TContext}"/>
//        /// (which in turn invokes 
//        /// <see cref="IContextFactory.Create{TContext}"/>
//        /// to create a new <see cref="IUnitOfWork"/>.
//        /// </summary>
//        public IUnitOfWork GetCurrent<TContext>() where TContext :IContext
//        {

//            //Get it out of this thread's context:
//            IUnitOfWork unitOfWork = Peek<TContext>();

//            if (unitOfWork!=null)
//            {
//                return unitOfWork;
//            }

//            //As no unit of work exists yet.
//            //we can either throw an exception,
//            //expressing that someone should have Registered 
//            //a UoW by the time we got here, 
//            if (!CreateUoWIfNowFound)
//            {
//                throw new Exception("No UoW of type {0} has yet been registered with the UnitOfWorkManagementService.".FormatStringExceptionCulture(typeof(TContext)));
//            }

//            //or 
//            //we have to get around to creating one.



//            //First time invoked on this request:
//            IUnitOfWorkFactory unitOfWorkFactory =
//                DependencyResolver.Current.GetInstance<IUnitOfWorkFactory>();

//            //We use the Factory service to create a new UoW,
//            //which will in turn generate a new Context:
//            unitOfWork = unitOfWorkFactory.Create<TContext>();

//            //Save it so we don't have to redo this:
//            Push<TContext>(unitOfWork);
            
//            return unitOfWork;
//        }

//        private Stack <IUnitOfWork> GetStack<TContext>()
//        {

//            Dictionary<object, Stack<IUnitOfWork>> dictionary =
//                _contextStateService.Items["XAct.Lib.UnitOfWorks"] as Dictionary<object, Stack<IUnitOfWork>>;

//            if (dictionary == null)
//            {
//                _contextStateService.Items["XAct.Lib.UnitOfWorks"]  = dictionary = new Dictionary<object, Stack<IUnitOfWork>>();
//            }



//            Stack <IUnitOfWork> stack;
//            Type type = typeof (TContext);
            
//            if (!dictionary.TryGetValue(type,out stack))
//            {
//                stack = new Stack<IUnitOfWork>();
//                dictionary.Add(type,stack);
//            }

//            return stack;
//        }


//        /// <summary>
//        /// Peeks into the current running context's Stack
//        /// (in a website, the running context is the HttpContext, 
//        /// in a desktop app, a static dictionary) 
//        /// </summary>
//        /// <typeparam name="TContext">The type of the context.</typeparam>
//        /// <returns></returns>
//        public IUnitOfWork Peek<TContext>()
//        {
//            //Gets the TContext type specific stack (eg: a stack of IEntityDbContext only)
//            Stack<IUnitOfWork> uowStack = GetStack<TContext>();

//            if (uowStack.Count > 0)
//            {
//                IUnitOfWork unitOfWork = uowStack.Peek();

//                return unitOfWork;
//            }
//            return null;
//        }


//        /// <summary>
//        /// Pushes the specified <see cref="IUnitOfWork"/> onto the stack.
//        /// <para>
//        /// The long winded version of usage would be similar to:
//        /// <code>
//        /// 			<![CDATA[
//        /// //Management is not the same as factory, hence two classes:
//        /// IUnitOfWorkFactory unitOfWorkFactory =
//        /// XAct.Services.ServiceLocator.Current.Get<IUnitOfWorkFactory>();
//        /// 
//        /// IUnitOfWorkManagementService unitOfWorkManagementService =
//        /// XAct.Services.ServiceLocator.Current.Get<IUnitOfWorkManagementService>();
//        /// 
//        /// //Obviously, with DI, and not SL, one only sees two lines really:
//        /// IUnitOfWork uow = IUnitOfWorkFactory.Create<IEFObjectContext>();
//        /// IUnitOfWorkManagementService.Push<IEFObjectContext>(uoW);
//        /// ]]>
//        /// 		</code>
//        /// 	</para>
//        /// </summary>
//        /// <typeparam name="TContext"></typeparam>
//        /// <param name="unitOfWork">The unit of work.</param>
//        public void Push<TContext>(IUnitOfWork unitOfWork)
//        {
//            //Gets the TContext type specific stack (eg: a stack of IEntityDbContext only)
//            Stack<IUnitOfWork> stack = GetStack<TContext>();

//            if (stack.Contains(unitOfWork))
//            {
//                throw new Exception("Stack already contains UnitOfWork");
//            }
//            stack.Push(unitOfWork);
//        }

//        /// <summary>
//        /// Pop the latest <see cref="IUnitOfWork"/> back off the stack.
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Once you are finished with the current UoW, you just pop it off:
//        /// </para>
//        /// <para>
//        /// The long winded version of usage would be similar to:
//        /// <code>
//        /// <![CDATA[
//        /// //Management is not the same as factory, hence two classes:
//        /// IUnitOfWorkManagementService unitOfWorkManagementService =
//        ///    XAct.Services.ServiceLocator.Current.Get<IUnitOfWorkManagementService>();
//        /// 
//        /// IUnitOfWorkManagementService.Pop<IEFObjectContext>();
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        public IUnitOfWork Pop<TContext>()
//        {
//            //Gets the TContext type specific stack (eg: a stack of IEntityDbContext only)
//            Stack<IUnitOfWork> stack = GetStack<TContext>();

//            return stack.Pop();
//        }

//        /// <summary>
//        /// Counts the number of <see cref="IUnitOfWork"/> on the stack.
//        /// <para>
//        /// Useful to Assert that you know how many you have added, and where you are at.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TContext"></typeparam>
//        public int Count<TContext>()
//        {
//            //Gets the TContext type specific stack (eg: a stack of IEntityDbContext only)
//            Stack<IUnitOfWork> stack = GetStack<TContext>();

//            return stack.Count;
//        }
//    }
//}
