﻿namespace XAct.Data.Repositories.Implementations
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// Abstract base type specific repository class suitable as a foundation for type specific repositories.
    /// <para>
    /// The repository element must implement <see cref="IHasId{TId}"/> as a bare minimum requirement.
    /// </para> 
    /// <para>
    /// See:
    ///   <see cref="ISimpleRepository{TModel,TId}"/>
    ///   <see cref="IApplicationTennantIdSpecificRepository{TEntity,TId}"/>
    ///   <see cref="IReferenceDataRepository{TEntity,TId}" />
    ///   <see cref="IReferenceDataCodedRepository{TEntity,TId}" />
    ///   <see cref="IApplicationTennantIdSpecificReferenceDataRepository{TEntity, TId}"/>
    /// </para>
    /// <para>
    /// See: 
    ///   <see cref="ServiceBase"/>
    ///   <see cref="RepositoryServiceBase{TModel,Guid}" />
    ///   <see cref="DistributedGuidIdRepositoryServiceBase{TElement}"/>
    ///   <see cref="ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase{TEntity}"/>
    ///   <see cref="ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase{TModel}"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TModel">The type of the element.</typeparam>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public abstract class RepositoryServiceBase<TModel,TId> : ServiceBase,  
        ISimpleRepository<TModel,TId> , IHasService
        where TModel : class, IHasId<TId>, new()
        where TId :struct
    {

// ReSharper disable InconsistentNaming
        protected readonly IRepositoryService _repositoryService;
// ReSharper restore InconsistentNaming

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryServiceBase{TElement, TId}" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        protected RepositoryServiceBase(ITracingService tracingService, IRepositoryService repositoryService):base(tracingService)
        {
            _repositoryService = repositoryService;
        }

        protected abstract Expression<Func<TModel, bool>> IdEquality(TId id);

        public virtual bool Exists(TId id)
        {
            var result = _repositoryService.Exists<TModel>(IdEquality(id));
            return result;
        }

        public virtual bool Exists(Expression<Func<TModel, bool>> where)
        {
            var result = _repositoryService.Exists(where);
            return result;
        }


        public virtual int Count()
        {
            var result = _repositoryService.Count<TModel>();
            return result;
        }

        public virtual int Count(Expression<Func<TModel, bool>> where)
        {
            var result = _repositoryService.GetByFilter<TModel>(where).Count();
            return result;
        }



        /// <summary>
        /// Gets the specified element.
        /// </summary>
        /// <internal>
        /// Implement as:
        /// <code>
        /// <![CDATA[
        /// return _repositoryService.GetSingle<TElement>(x => x.Id == id);
        /// ]]>
        /// </code>
        /// </internal>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public virtual TModel GetById(TId id)
        {
            var result = _repositoryService.GetSingle(IdEquality(id));

            return result;
        }

        /// <summary>
        /// Persists the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        public virtual void PersistOnCommit(TModel element)
        {
            _repositoryService.PersistOnCommit<TModel,TId>(element,true);
        }


        /// <summary>
        /// Deletes the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        public virtual void DeleteOnCommit(TModel element)
        {
            _repositoryService.DeleteOnCommit(element);
        }

        /// <summary>
        /// Gets the news categories.
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        public virtual IQueryable<TModel> Retrieve(PagedQuerySpecification pagedQuerySpecification)
        {
            return Retrieve(x => true,null, pagedQuerySpecification);
        }


        protected virtual TModel Get(Expression<Func<TModel, bool>> where)
        {
            return _repositoryService.GetSingle<TModel>(where);
        }


        protected virtual IQueryable<TModel> Retrieve(Expression<Func<TModel, bool>> where, Func<IQueryable<TModel>, IOrderedQueryable<TModel>> orderBy = null, PagedQuerySpecification pagedQuerySpecification=null)
        {
            if (pagedQuerySpecification == null){pagedQuerySpecification = new PagedQuerySpecification();}
            return _repositoryService.GetByFilter<TModel>(where, null, pagedQuerySpecification,orderBy);

        }

    }
}