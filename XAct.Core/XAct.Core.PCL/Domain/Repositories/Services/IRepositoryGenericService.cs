﻿// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories
// ReSharper restore CheckNamespace
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Messages;

    /// <summary>
    ///   The interface for a generic Repository.
    /// </summary>
    /// <typeparam name = "TAggregateRootEntity"></typeparam>
    /// <remarks>
    ///   <para>
    ///     Usage (when not using an DependencyInjectionContainer) would be:
    ///     <code>
    ///       <![CDATA[
    /// using (ObjectContext origObjectContext = new XAct_App()){
    ///   using (IObjectContext abstractObjectContext = new EntityObjectContext(origObjectContext)){
    ///     using (IUnitOfWork uow = new UnitOfWork(abstractObjectContext)){
    ///       using (IRepositoryService repository = new ExampleRepository(abstractObjectContext)){
    ///         ...
    ///         repository.MakeChanges(...);
    ///         ...
    ///         uow.Commit();
    ///       }
    ///     }
    ///   }
    /// }
    /// ]]>
    ///     </code>
    ///   </para>
    /// </remarks>
    /// <internal>
    ///   <para>
    ///     A Repository must remain persitence ignorant, 
    ///     not knowing how or when changes will be persisted
    ///     to the underlying datastore, leaving the 'when' to being
    ///     handled by the <see cref="IUnitOfWork"/>. 
    ///   </para>
    ///   <para>
    ///     http://elegantcode.com/2009/12/15/aggregateRootEntity-framework-ef4-generic-repository-and-unit-of-work-prototype/comment-page-1/
    ///   </para>
    /// </internal>
    [Obsolete]
    public interface IRepositoryService<TAggregateRootEntity> : IHasXActLibService
        /* where T : class -- NO -- TOO LIMITING */
    {


        /// <summary>
        /// Gets the current <see cref="IUnitOfWork"/> from the underlying 
        /// <c>IUnitOfWorkManagementService</c>.
        /// <para>
        /// Use that to <c>Execute</c> it's internal <see cref="IContext"/>.
        /// </para>
        /// <para>
        /// Note that I am not especially happy having the Repository (Domain) have a reference 
        /// to an infrastructural context -- but it's just easier not having to inject two
        /// different contracts into every location the contract is needed.
        /// </para>
        /// </summary>
        IUnitOfWork Context { get; }


        /// <summary>
        /// Get the total of objects that meet the criteria.
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// 	</para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// IRepository<Example> _exampleRepository = ...
        /// int howManyExpensiveThings =
        ///   _exampleRepository.Count(ex=>ex.Price > 10000);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="filter">The (optional) predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <returns></returns>
        /// <internal>
        /// Generally implemented without using <see cref="GetByFilter"/>
        /// as not all Providers will create the same SQL.
        ///   </internal>
        int Count(Expression<Func<TAggregateRootEntity, bool>> filter=null);

        /// <summary>
        /// Gets whether the object(s) exists in the datastore.
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// IRepository<Example> _exampleRepository = ...
        /// bool hasExpensiveStuff =
        ///   _exampleRepository.Contains(ex=>ex.Price > 10000);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        bool Contains(Expression<Func<TAggregateRootEntity, bool>> filter);


        //GetAll is a stupid red herring, seriously putting in jeopardy the rest of the code 
        //by exposing a means to pull back every single record in one fell swoop.
        //GetByFilter at least puts in place a means of Paging the results.

        //[Obsolete("Doesn't give much value -- better to be explicit using other GetSingle")]
        //TAggregateRootEntity GetSingle(params object[] keys);

        /// <summary>
        /// Find a single aggregateRootEntity, based on the given predicate filter.
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// 	</para>
        /// </summary>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <param name="includeSpecification">The include specifications.</param>
        /// <param name="mergeOptions">The merge options.</param>
        /// <returns></returns>
        /// <remarks>
        /// Usage Example:
        /// <code>
        /// 		<![CDATA[
        /// IRepository<Example> _exampleRepository = ...
        /// Example one =
        /// _exampleRepository.GetSingle(ex=>ex.Id=1,new IncludeSpecification("SubExampleItems"));
        /// Example two =
        /// _exampleRepository.GetSingle(ex=>ex.Id=2,null);
        /// Example three =
        /// _exampleRepository.GetSingle(ex=>ex.Id=3);
        /// ]]>
        /// 	</code>
        /// </remarks>
        TAggregateRootEntity GetSingle(Expression<Func<TAggregateRootEntity, bool>> filter,
            IIncludeSpecification includeSpecification = null, 
            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined);


        /// <summary>
        /// Returns an <see cref="IQueryable"/> of all entities
        /// in the datastore that match the given filter,
        /// using the given paging specs.
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// 	</para>
        /// </summary>
        /// <param name="filter">The predicate (or <see cref="ISpecification{TAggregateRoot}"/>) filter.</param>
        /// <param name="includeSpecification">The (optional) list of child Entities (Properties) to include in this query to avoid the later cost of Lazy loading.</param>
        /// <param name="pagedQuerySpecs">The (optional) paged query specs.</param>
        /// <param name="orderBy">The (optional) order by.</param>
        /// <param name="mergeOptions">The merge options.</param>
        /// <returns></returns>
        IQueryable<TAggregateRootEntity> GetByFilter(
            Expression<Func<TAggregateRootEntity, bool>> filter,
            IIncludeSpecification includeSpecification=null,
            IPagedQuerySpecification pagedQuerySpecs=null,
            Func<IQueryable<TAggregateRootEntity>, IOrderedQueryable<TAggregateRootEntity>> orderBy = null,
            XAct.Domain.Repositories.MergeOption mergeOptions = XAct.Domain.Repositories.MergeOption.Undefined
            );



        /// <summary>
        /// Persists the given entity.
        /// <para>
        /// The implementation invokes <see cref="IsEntityNew"/>
        /// to determine whether to invoke 
        /// <see cref="AddOnCommit"/> (for new Aggregate Root Entities)
        /// or <see cref="UpdateOnCommit"/>
        /// </para>
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name="aggregateRootEntity">The aggregate root.</param>
        /// <returns></returns>
        void PersistOnCommit(TAggregateRootEntity aggregateRootEntity);

        /// <summary>
        /// Determines whether the entity has ever been saved before.
        /// <para>
        /// Invoked by <see cref="PersistOnCommit"/>
        /// to decide whether to in turn invoke 
        /// <see cref="AddOnCommit"/>, 
        /// or 
        /// <see cref="UpdateOnCommit"/> 
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// </para>
        /// </summary>
        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
        /// <returns>
        ///   <c>true</c> if [is entity new] [the specified aggregate root entity]; otherwise, <c>false</c>.
        /// </returns>
        bool IsEntityNew(TAggregateRootEntity aggregateRootEntity);


        /// <summary>
        ///   Adds the given aggregateRootEntity to the datastore,
        ///   when the active 
        ///   <see cref = "IUnitOfWork" />
        ///   is Committed.
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name = "aggregateRootEntity"></param>
        void AddOnCommit(TAggregateRootEntity aggregateRootEntity);


        /// <summary>
        /// Attaches untracked -- but already saved at some point --
        /// entities.the on commit.
        /// <para>
        /// NOTE: When possible (not all ORMs can pull it off) prefer
        /// <see cref="UpdateOnCommit"/> to <see cref="AttachOnCommit"/>
        /// as higher levels should not have to keep track of what has
        /// been added or not.
        /// </para>
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name="aggregateRootEntity">The aggregate root entity.</param>
        /// <returns></returns>
        void AttachOnCommit(TAggregateRootEntity aggregateRootEntity);


        /// <summary>
        /// Update object changes and save to database.
        /// <para>
        /// Note that if entity is not being tracked (ie, untracked)
        /// will Attach first, then change state to Modified.
        /// </para>
        /// <para>
        /// By doing it this way, removes from higher levels the need 
        /// to keep track of what has been attached or not.
        /// </para>
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name="aggregateRootEntity">Specified the object to save.</param>
        void UpdateOnCommit(TAggregateRootEntity aggregateRootEntity);



        /// <summary>
        ///   Removes the given aggregateRootEntity from the datastore
        ///   when the active 
        ///   <see cref = "IUnitOfWork" />
        ///   is Committed.
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name = "aggregateRootEntity"></param>
        /// <returns>The number of Elements changed in the current commit (may not be exact match to number of deleted).</returns>
        void DeleteOnCommit(TAggregateRootEntity aggregateRootEntity);


        /// <summary>
        /// Removes entities from the datastore
        /// that match the given filter,
        /// when the active
        /// <see cref="IUnitOfWork"/>
        /// is Committed.
        /// <para>
        /// Note: Intended to be implemented *explicity* by a RepositoryBase,
        /// as it is an expensive method to invoke without constraints.
        /// </para>
        /// <para>
        /// IMPORTANT: Unless you Commit after this operation, most ORMs
        /// will return unaltered entities in any subsequent queries 
        /// for the same entities. EF being one such ORM.
        /// </para>
        /// <para>
        /// Defined in <see cref="IRepositoryService{TAggregateRootEntity}"/>
        /// </para>
        /// </summary>
        /// <remarks>
        /// There is no need to return the number of records committed,
        /// as it won't be happening just then -- only when 
        /// the UnitOfWork Commits the underlying ORM Context.
        /// <para>
        /// Note that a common question is 
        /// "When, and by whom should the UoW be Committed?"
        /// The answer is, in MVC, the Controller, before it redirects 
        /// or displays the results. 
        /// In other words, the Controller get's DI'ed 
        /// an Application Service (eg: ISomeService) and a IUoW.
        /// THe Controller invokes a method in ISomeService, which 
        /// in turn invokes a Command method in the Repository.
        /// The Controller commits Unit of Work before finishing handling the POST.
        /// </para>
        /// <para>
        /// In ASP.NET it would be done by the page, at the end of Rendering,
        /// or just before Redirecting to another page. 
        /// </para>
        /// <para>
        /// Application_EndRequest is an unsatisfactory
        /// solution, as it is not invoked if the Page redirects.
        /// </para>
        /// </remarks>
        /// <param name="predicate">The predicate.</param>
        /// <returns>The number of Elements changed in the current commit (may not be exact match to number of deleted).</returns>
        void DeleteOnCommit(Expression<Func<TAggregateRootEntity, bool>> predicate);




    }
}