﻿namespace XAct.Domain.Repositories
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using XAct.Data.Repositories.Implementations;
    using XAct.Messages;
    using XAct.Services;

    /// <summary>
    /// Base interface for simple repositories.
    /// <para>
    /// See:
    ///   <see cref="ISimpleRepository{TModel,TId}"/>
    ///   <see cref="IApplicationTennantIdSpecificRepository{TEntity,TId}"/>
    ///   <see cref="IReferenceDataRepository{TEntity,TId}" />
    ///   <see cref="IReferenceDataCodedRepository{TEntity,TId}" />
    ///   <see cref="IApplicationTennantIdSpecificReferenceDataRepository{TEntity, TId}"/>
    /// </para>
    /// <para>
    /// See: 
    ///   <see cref="ServiceBase"/>
    ///   <see cref="RepositoryServiceBase{TModel,Guid}" />
    ///   <see cref="DistributedGuidIdRepositoryServiceBase{TModel}"/>
    ///   <see cref="ApplicationTennantIdSpecificDistributedGuidIdRepositoryServiceBase{TEntity}"/>
    ///   <see cref="DistributedGuidIdRepositoryServiceBase{TModel}" />
    ///   <see cref="ApplicationTennantIdSpecificReferenceDataDistributedGuidIdRepositoryServiceBase{TModel}"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TModel">The type of the element.</typeparam>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface ISimpleRepository<TModel,TId>
        where TModel:class,new()
        where TId:struct
    {

        /// <summary>
        /// Determines whether the specified record has been persisted and exists.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Exists(TId id);

        bool Exists(Expression<Func<TModel, bool>> where);

        int Count();


        int Count(Expression<Func<TModel, bool>> where);

        /// <summary>
        /// Gets the specified element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        TModel GetById(TId id);

        /// <summary>
        /// Persists the new or updated <see cref="TModel"/>.
        /// </summary>
        /// <param name="element">The news category.</param>
        void PersistOnCommit(TModel element);

        /// <summary>
        /// Delete the 
        /// </summary>
        /// <param name="element"></param>
        void DeleteOnCommit(TModel element);

        /// <summary>
        /// Retrieves a queryable set of the specified .
        /// </summary>
        /// <param name="pagedQuerySpecification">The paged query specification.</param>
        /// <returns></returns>
        IQueryable<TModel> Retrieve(PagedQuerySpecification pagedQuerySpecification);

        ///// <summary>
        ///// Get ReferenceData for specifying the Category of the <see cref="TModel"/> item.
        ///// </summary>
        ///// <param name="pagedQuerySpecification">The paged query specification.</param>
        ///// <returns></returns>
        //IQueryable<TModel> Retrieve(Func<TModel,bool> where,  PagedQuerySpecification pagedQuerySpecification);

    }
}