﻿// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    ///   The IObjectContext object abstracts away (by wrapping) the 
    ///   <c>DataContext (in LinqToSql)</c>, <c>ObjectContext (in EF4)</c>
    ///   <c>DbContext (in EF4.2), </c> etc.
    ///   so that one can refer to their functionality
    ///   (specifically, their respective 
    ///   <c>SubmitChanges</c>/<c>CommitChanges</c> methods).
    ///   indirectly.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///   The primary advantage of using an abstract 
    ///   <see cref="IContext"/> rather than a vendor specific 
    ///   class, is to be able to convert more of the required code
    ///   (see <see cref="IUnitOfWork"/>, 
    ///   <c>IUnitOfWorkManagementService</c>, etc.)
    ///   into vendor neutral library code.
    ///   </para>
    ///   <para>
    ///     An example of implementing this for Linq To Sql would
    ///     be something like:
    ///     <example>
    ///       <code>
    ///         <![CDATA[
    /// public class LinqObjectContext : IObjectContext {
    ///   DataContext _DataContext;
    ///   public LinqObjectContext(DataContext dataContext){ 
    ///     _DataContext = dataContext;
    ///   }
    ///   void CommitChanges() { _DataContext.SubmitChanges(); }
    /// }
    /// ]]>
    ///       </code>
    ///     </example>
    ///     whereas for Entity Framework 4, this might be something like:
    ///     <code>
    ///       <![CDATA[
    /// public class EntityObjectContext : IObjectContext {
    ///   ObjectContext _ObjectContext;
    ///   public EntityObjectContext(ObjectContext objectContext){
    ///     _ObjectContext = objectContext;
    ///   }
    ///   void CommitChanges() {_ObjectContext.CommitChanges()}
    /// }
    /// ]]>
    ///     </code>
    ///   </para>
    ///   <para>
    ///     Usage (when not using an Inversion of Control COntainer (DependencyInjectionContainer)
    ///     to do Dependency Inversion (DI)) would be:
    ///     <code>
    ///       <![CDATA[
    /// using (ObjectContext origObjectContext = new XAct_App()){
    ///   using (IObjectContext abstractObjectContext = new EntityObjectContext(origObjectContext)){
    ///     using (IUnitOfWork uow = new UnitOfWork(abstractObjectContext)){
    ///       using (IRepository repository = new ExampleRepository(abstractObjectContext)){
    ///         ...
    ///         repository.MakeChanges(...);
    ///         ...
    ///         uow.Commit();
    ///       }  
    ///     }
    ///   }
    /// }
    /// ]]>
    ///     </code>
    ///   </para>
    /// </remarks>
    public interface IContext :  IHasInnerItemReadOnly, IDisposable
    {

        ///// <summary>
        ///// Gets the type of the context.
        ///// <para>
        ///// Property is used by the 
        ///// <see cref="IContextFactory"/> to determine which type of 
        ///// entity to instantiate.
        ///// </para>
        ///// </summary>
        ///// <value>
        ///// The type of the context.
        ///// </value>
        //Type ContextType {get;}

        /// <summary>
        /// Commit pending changes to the DataStore.
        /// <para>
        /// Defined in <see cref="IContext"/>.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Implemented per ORM provider. An example for EF 4.2 would be
        /// <c>XAct.Data.EntityDbContext</c> which was implemented as follows:
        /// <code>
        /// <![CDATA[
        /// public class EntityDbContext : IEntityDbContext {
        ///   private readonly DbContext _dbContext;
        ///   public EntityDbContext(DbContext objectContext) {
        ///     if (objectContext.IsNull()) { throw new ArgumentNullException("objectContext"); }
        ///     _dbContext = objectContext;
        ///   }
        ///   public int Commit() {return _dbContext.SaveChanges();}
        ///   public void Dispose() {_dbContext.Dispose(); }
        ///   private object IObjectWrapper<object>.InnerObject {get {return _dbContext; }}
        ///   Note: Intended to be implemented Explicitly,to not drag around dependencies.
        ///   DbSet<T> IEntityDbContext.CreateDbSet<T>() {return _dbContext.Set<T>();}
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <returns>The number of objects written to the underlying database.</returns>
        int Commit(CommitType commitType = CommitType.Default);


    }


}