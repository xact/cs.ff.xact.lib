﻿namespace XAct.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IContextSpecification : IHasName
    {
        /// <summary>
        /// Gets or sets the name of the Context specification.
        /// <para>
        /// Eg: "Tombstone" or "Default"
        /// </para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        new string Name { get; set; }
    }
}
