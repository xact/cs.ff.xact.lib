﻿//namespace XAct.Domain.Repositories.Implementations
//{
//    using XAct.Services;

//    /// <summary>
//    /// A default implementation of the <see cref="IUnitOfWorkFactory"/>
//    /// <para>
//    /// Note that the <see cref="Create{TContext}"/> method returns a *new* 
//    /// <see cref="UnitOfWork"/> every time it is invoked.
//    /// </para>
//    /// <para>
//    /// To return a 'Current' UoW in a consistent fashion, use
//    /// instead use an implementation of 
//    /// <see cref="IUnitOfWorkManagementService"/>.
//    /// </para>
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Usage would be:
//    /// <code>
//    /// <![CDATA[
//    /// IUnitOfWorkFactory unitOfWorkFactory = 
//    ///   XAct.Services.ServiceLocatorService.Current.GetInstance<IUnitOfWorkFactory>();
//    /// using (IUnitOfWork unitOfWork = unitOfWorkFactory){ 
//    ///   IUnitOfWork unitOfWork = unitOfWorkFactory.Create();
//    ///   ...
//    /// }
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// </remarks>
//    [DefaultBindingImplementation(typeof(IUnitOfWorkFactory))]
//    public class UnitOfWorkFactory : IUnitOfWorkFactory
//    {
//        private readonly IContextFactory _objectContextFactory;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="UnitOfWorkFactory"/> class.
//        /// </summary>
//        /// <param name="objectContextFactory">The _object context factory.</param>
//        public UnitOfWorkFactory(IContextFactory objectContextFactory)
//        {
//            objectContextFactory.ValidateIsNotDefault("objectContextFactory");

//            _objectContextFactory = objectContextFactory;
//        }

//        /// <summary>
//        /// Creates an <see cref="IUnitOfWork"/>
//        /// 	<para>
//        /// The contextKey  (eg: "AspNET")can be used to generate secondary contexts.
//        /// </para>
//        /// 	<para>
//        /// If undefined, returns a default Context
//        /// (most small, mid-size app's only have one context).
//        /// </para>
//        /// 	<para>
//        /// Note: internally, it in turn invokes
//        /// <see cref="IContextFactory.Create{TContext}"/>
//        /// 	</para>
//        /// </summary>
//        /// <typeparam name="TContext"></typeparam>
//        /// <param name="contextKey">(Optional) Name of the context.</param>
//        /// <returns>
//        /// An <see cref="IUnitOfWork"/>
//        /// </returns>
//        public IUnitOfWork Create<TContext>(string contextKey = null)
//            where TContext:IContext
//        {
//            //Create a new IObjectContext:
//            IContext objectContext =
//                _objectContextFactory.Create<TContext>(contextKey);

//            //From that, create a new UnitOfWork:
//            IUnitOfWork unitOfWork = new UnitOfWork(objectContext);

//            //... and return it:
//            return unitOfWork;
//        }

//    }
//}
