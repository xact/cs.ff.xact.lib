﻿//// ReSharper disable CheckNamespace
//namespace XAct.Domain.Repositories
//// ReSharper restore CheckNamespace
//{
//    using System;

//    /// <summary>
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Usage as follows:
//    /// </para>
//    /// <para>
//    /// First, implement the <see cref="IContextFactory"/> contract:
//    /// <code>
//    /// <![CDATA[
//    /// public class ObjectContextFactory {
//    ///   public ObjectContextFactory(){}
//    ///   public IObjectContext Create(string tag = null){
//    ///     if (tag.ToLower() == "aspecificcontexttag"){return MyOtherContext();}
//    ///     //return default context:
//    ///     return new MyAppContext();
//    ///   }
//    /// }
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// <para>
//    /// Once that's done, all you have to do is create a repo:
//    /// <code>
//    /// <![CDATA[
//    /// IMyRepo myRepo = DependencyResolver.Current.GetInstance<IMyRepo>();
//    /// ]]>
//    /// </code>
//    /// It will get injected with an instance of <see cref="IUnitOfWorkManagementService"/>,
//    /// which will back the internal <c>ObjectContext</c>/<c>DbContext</c> property, and 
//    /// will Create or return a Current UoW (using, if it has to, the <see cref="IContextFactory"/>
//    /// defined earlier.
//    /// </para>
//    /// <para>
//    /// After having done changes, use the <see cref="IUnitOfWorkManagementService"/> to
//    /// get the current UoW, in order to commit it:
//    /// <code>
//    /// <![CDATA[
//    /// using (IRepository repository = new ExampleRepository(abstractObjectContext)){
//    ///   ...
//    ///   repository.MakeQueries(...);
//    ///   //Commit may not be necessary yet...
//    /// 
//    ///   repository.MakeChanges(...);
//    ///   //If querying for same objects again, need to Commit() first:
//    ///   ...
//    ///   IUnitOfWork unitOfWork = DependencyResolver.Current.GetInstance<IUnitOfWorkManagementService>().Current;
//    ///   unitOfWork.Commit();
//    ///   ...
//    ///   repository.MakeQueries(...);
//    ///   ...
//    ///   repository.MakeChanges(...);
//    ///   unitOfWork.Commit();
//    ///   ...
//    ///   etc.
//    /// }
//    /// ]]>
//    /// </code> 
//    /// </para>
//    /// <para>
//    /// Note that it's common to tack static properties to UnitOfWork
//    /// to get the Current instance. Don't agree (SOC: it's a Management issue).
//    /// </para>
//    /// </remarks>
//    /// <internal>
//    ///   <para>
//    ///     A Repository must remain persitence ignorant, 
//    ///     not knowing how or when changes will be persisted
//    ///     to the underlying datastore. Hence why this interface
//    ///     does not expose a Commit() method.
//    ///     See <see cref = "UnitOfWork" />, which handles this.
//    ///   </para>
//    /// </internal>
//    /// <internal>
//    /// You'll see sometimes a UnitOfWork contain Repositories as properties.
//    /// I think  that misses the point entirely. And is unneeded. 
//    /// </internal>
//    public class UnitOfWork : IUnitOfWork //, IHasInnerItemReadOnly //INestableUnitOfWork, 
//    {
//        /// <summary>
//        /// Gets the inner item.
//        /// </summary>
//        /// <typeparam name="TItem">The type of the item.</typeparam>
//        /// <returns></returns>
//        public TItem GetInnerItem<TItem>()
//        {
//            return _innerObject.ConvertTo<TItem>();
//        }

//        /// <summary>
//        /// Gets the <see cref="IContext"/>
//        /// that this <see cref="IUnitOfWork"/>
//        /// is wrapping, and managing.
//        /// </summary>
//        public IContext Context
//        {
//            get { return _innerObject; }
//        }
//        private readonly IContext _innerObject;

//        #region Constructors

//        ///// <summary>
//        /////   Initializes a new instance of the <see cref = "UnitOfWork" /> class.
//        ///// </summary>
//        ///// <param name = "objectContext">The object context.</param>
//        //public UnitOfWork(IContext objectContext)
//        //{
//        //    _innerObject = objectContext;
//        //}

//        #endregion

//        #region Implementation of IDisposable

//        /// <summary>
//        ///   Performs application-defined tasks associated with freeing, 
//        ///   releasing, or resetting unmanaged resources.
//        /// </summary>
//        public void Dispose()
//        {
//            if (_innerObject != null)
//            {
//                _innerObject.Dispose();
//            }

//            GC.SuppressFinalize(this);
//        }

//        #endregion



//        /// <summary>
//        ///   Commits pending changes to the
//        ///   Repository.
//        /// <para>
//        /// Defined in <see cref="IUnitOfWork"/>
//        /// </para>
//        /// </summary>
//        public int Commit(CommitType commitType = CommitType.Default)
//        {
//            ////Go from Deepest, LastSibling first:
//            //for(int i=_childCollection.Count-1;i>-1;i--)
//            //{
//            //    _childCollection[i].Commit(commitType);
//            //}

//           int result = _innerObject.Commit(commitType );

//            return result;
//        }


//        ///// <summary>
//        ///// Gets the <see cref="INestableUnitOfWork"/>
//        ///// that manages this <see cref="INestableUnitOfWork"/>
//        ///// 	<para>
//        ///// The parent's <see cref="IUnitOfWork.Commit"/> method
//        ///// will trigger this child <see cref="IUnitOfWork"/>'s
//        ///// <see cref="IUnitOfWork.Commit"/> method
//        ///// </para>
//        ///// 	<para>
//        ///// Units of work will be committed, last sibling first, deepest first.
//        ///// </para>
//        ///// 	<para>
//        ///// Defined in <see cref="INestableUnitOfWork"/>
//        ///// 	</para>
//        ///// </summary>
//        //public INestableUnitOfWork Parent { get; private set; }

//        ///// <summary>
//        ///// Add's the given <see cref="INestableUnitOfWork"/>
//        ///// to this <see cref="INestableUnitOfWork"/>'s
//        ///// internal collection of
//        ///// <see cref="INestableUnitOfWork"/> items.
//        ///// <para>
//        ///// <para>
//        ///// When this <see cref="INestableUnitOfWork"/>
//        ///// is <see cref="IUnitOfWork.Commit"/>'ed, it will trigger
//        ///// a <see cref="IUnitOfWork.Commit"/> on all the
//        ///// <see cref="INestableUnitOfWork"/>'s (deepest, last sibling first).
//        ///// </para>
//        ///// Defined in <see cref="INestableUnitOfWork"/>
//        ///// 	</para>
//        ///// </summary>
//        ///// <param name="childUnitOfWork">The child <see cref="INestableUnitOfWork"/>.</param>
//        ///// <returns>
//        /////   <see cref="INestableUnitOfWork"/>
//        ///// </returns>
//        //public void Add(INestableUnitOfWork childUnitOfWork)
//        //{
//        //    childUnitOfWork.ValidateIsNotDefault("childUnitOfWork");

//        //    UnitOfWork unitOfWork = (UnitOfWork)childUnitOfWork;

//        //    unitOfWork.Parent = this;
            
//        //    _childCollection.Add(unitOfWork);
//        //}

//        //readonly IList<UnitOfWork> _childCollection = new List<UnitOfWork>();

//        ///// <summary>
//        ///// Removes this <see cref="INestableUnitOfWork"/>
//        ///// from it's parent <see cref="INestableUnitOfWork"/>.
//        ///// <para>
//        ///// In other words, if one wants to still commit this
//        ///// individual <see cref="INestableUnitOfWork"/>, one will
//        ///// have to invoke its <see cref="IUnitOfWork.Commit"/>
//        ///// method (it won't be invoked by the root <see cref="INestableUnitOfWork"/>).
//        ///// </para>
//        ///// 	<para>
//        ///// Defined in <see cref="INestableUnitOfWork"/>
//        ///// 	</para>
//        ///// </summary>
//        //public void RemoveFromParent()
//        //{
//        //    if (this.Parent !=null)
//        //    {
//        //        UnitOfWork parentUoW = this.Parent as UnitOfWork;

//        //        if (parentUoW == null)
//        //        {
//        //            throw new Exception("Parent UoW is not of type UnitOfWork.");
//        //        }

//        //        parentUoW._childCollection.Remove(this);
//        //    }
//        //}

//    }
//}