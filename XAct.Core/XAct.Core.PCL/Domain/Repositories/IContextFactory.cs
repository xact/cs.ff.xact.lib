﻿
//namespace XAct.Domain.Repositories
//{
//    /// <summary>
//    /// A contract for an <see cref="IContext"/> Factory.
//    /// <para>
//    /// Whereas most of the other Contracts are generic in nature,
//    /// and can be satisfied by a generic class def, this Contract
//    /// has to be created per app.
//    /// </para>
//    /// <para>
//    /// This Factory is intended to be invoked by the 
//    /// <see cref="IUnitOfWorkFactory"/> to create new 
//    /// <see cref="IUnitOfWork"/>s.
//    /// </para>
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// In most small apps, there is only one Context in the whole
//    /// app (one connectionstring). In any non-trivial app there
//    /// are more than one (one for the app, and at least one more
//    /// for Integration reasons, as well as Tombstone record deletion.
//    /// </para>
//    /// <para>
//    /// An implementation example would be something like:
//    /// <code>
//    /// <![CDATA[
//    /// public class ObjectContextFactory {
//    /// 
//    ///   public ObjectContextFactory(){}
//    /// 
//    ///   public IObjectContext Create(string tag = null){
//    /// 
//    ///     //The tag allows for routing between different connection strings.
//    ///     if (tag.ToLower() == "aspecificcontexttag"){return MyOtherContext();}
//    ///     
//    ///     //return default context:
//    ///     return new MyAppContext();
//    ///   }
//    /// }
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// <para>
//    /// Notice that the app
//    /// </para>
//    /// </remarks>
//    public interface IContextFactory
//    {


//        /// <summary>
//        /// Creates a new <see cref="IContext"/>
//        /// every time it is invoked.
//        /// </summary>
//        /// <param name="tag">The tag, which can be used to 
//        /// route the creation of various 
//        /// <see cref="IContext"/> instances.</param>
//        /// <returns></returns>
//        IContext Create<TContext>(string tag = null)where TContext : IContext;
//    }
//}
