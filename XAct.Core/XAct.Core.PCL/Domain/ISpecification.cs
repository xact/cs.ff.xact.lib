﻿namespace XAct.Domain.Repositories
{
    using System;
    using System.Linq.Expressions;

    /// <summary>
    /// A Contract for a Query Specification, that can be passed
    /// to a implementation of 
    /// <see cref="IRepositoryService{TAggregateRootEntity}"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// An example of an implementation would be:
    /// <code>
    /// <![CDATA[
    ///  public class NZCustomersSpecification : ISpecification<Customer> {
    ///   public Expression<Func<Customer, bool>> IsSatisfied { 
    ///     get {return c => c.Country= "NZ";}
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Usage is then as follows:
    /// <code>
    /// <![CDATA[
    /// _customerRepository.Count(new NZCustomersSpecification().IsSatisified);
    /// ]]>
    /// </code>
    /// or - if IRepository is implemented more tightly - would accept only a Specification,
    /// rather than the Expression{Func{TEntity,bool}}
    /// </para>
    /// </remarks>
    /// <typeparam name="TAggregateRootEntity">The type of the entity.</typeparam>
    public interface ISpecification<TAggregateRootEntity> where TAggregateRootEntity : class
    {
        /// <summary>
        /// Gets the Specification's Expression.
        /// </summary>
        Expression<Func<TAggregateRootEntity, bool>> IsSatisfied { get; }
    }
}
