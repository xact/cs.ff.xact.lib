namespace XAct.Domain
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> Enum.
    /// </para>
    /// Status synchronization flags
    /// </summary>
    /// <internals>
    /// For bitwise manipulations see: http://codeidol.com/csharp/csharpckbk2/Classes-and-Structures/Turning-Bits-On-or-Off/
    /// <code>
    /// <![CDATA[
    /// |= X  (set)
    /// &= ~X (unset)
    /// 
    /// &= X (leave up only X, dropping all else, if X already up).
    /// ]]>
    /// </code>
    /// </internals>
    [Flags]
    public enum OfflineModelState:int
    {
        #region X

        /// <summary>
        /// An error state.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The initial state for retrieved existing objects.
        /// <para>
        /// No changes have been made, therefore no save is required.
        /// </para>
        /// <para>
        /// Value = 1 (Binary: 1) 
        /// </para>
        /// </summary>
        UnchangedExisting = Clean,

        /// <summary>
        /// THe object is new -- changes have to be saved (using a SQL INSERT statement or equivalent).
        /// <para>
        /// Value = 16 (Binary: 10000)
        /// </para>
        /// </summary>
        New = LocalCreationPending,

        /// <summary>
        /// The object has been persisted/is existing, and changes have to be resaved (using a SQL UPDATE statement or equivalent).
        /// <para>
        /// Value = 32 (Binary: 100000)
        /// </para>
        /// </summary>
        Updated = LocalUpdatePending,

        /// <summary>
        /// The object is to be deleted whether it exists in the datastore or not, (using a SQL DELETE statement or equivalent).
        /// <para>
        /// Value = 8 (Binary: 1000)
        /// </para>
        /// </summary>
        Deleted = RemoteDeletionPending,

        #endregion

        #region X

        /// <summary>
        /// FOR INTERNAL USE ONLY. 
        /// <para>
        /// READ-ONLY.
        /// </para>
        /// <para>
        /// Value is 1. (Binary: 1)
        /// </para>
        /// All values of in-memory element are persisted to both Local storage, and Remote storage.
        /// </summary>
        Clean = 1,


        /// <summary>
        /// FOR INTERNAL USE ONLY.
        /// <para>
        /// READ-ONLY.
        /// </para>
        /// <para>
        /// Value is 2  (Binary: 10)
        /// </para>
        /// New In-memory element has not yet been persisted remotely.
        /// Note that this affects <see cref="RemotePending"/> and <see cref="CreationPending"/>.
        /// </summary>
        RemoteCreationPending = 2,

        /// <summary>
        /// FOR INTERNAL USE ONLY.
        /// <para>
        /// READ-ONLY.
        /// </para>
        /// <para>
        /// Value is 4 (Binary: 100).
        /// </para>
        /// Changes to in-memory element have not yet been persisted Remotely.
        /// </summary>
        RemoteUpdatePending = 4,

        /// <summary>
        /// FOR INTERNAL USE ONLY.
        /// <para>
        /// READ-ONLY.
        /// </para>
        /// <para>
        /// Value is 8 (Binary: 1000).
        /// </para>
        /// Deletion of in-memory element has not yet been processed Remotely.
        /// </summary>
        RemoteDeletionPending = 8,


        /// <summary>
        /// FOR INTERNAL USE ONLY.
        /// <para>
        /// READ-ONLY.
        /// </para>
        /// <para>
        /// Value is 16 (Binary: 10000).
        /// </para>
        /// Changes have not been updated to the local db.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This flags should never appears in the local db.
        /// </para>
        /// </remarks>
        LocalCreationPending = 16,

        /// <summary>
        /// FOR INTERNAL USE ONLY.
        /// <para>
        /// READ-ONLY.
        /// </para>
        /// <para>
        /// Value is 32 (Binary: 100000).
        /// </para>
        /// </summary>
        LocalUpdatePending = 32,


        /// <summary>
        /// FOR INTERNAL USE ONLY.
        /// <para>
        /// READ-ONLY.
        /// </para>
        /// <para>
        /// Value is 64 (Binary: 1000000).
        /// </para>
        /// </summary>
        LocalDeletionPending = 64, // non (non sense)

        #endregion

        #region X

        /// <summary>
        /// Element has changes that need to be persisted to local storage.
        /// <para>
        /// READ/WRITE.
        /// </para>
        /// <para>
        /// Value is 112 (Binary: 1110000)
        /// </para>
        /// This is a CombinationFlag: (LocalCreationPending | LocalUpdatePending | LocalDeletionPending)
        /// </summary>
        LocalPending = (LocalCreationPending | LocalUpdatePending | LocalDeletionPending),


        /// <summary>
        /// Element has changes that need to be persisted to remote storage.
        /// <para>
        /// READ/WRITE.
        /// </para>
        /// <para>
        /// Value is 2+4+8=14 (Binary:1110)
        /// </para>
        /// This is a CombinationFlag: (RemoteCreationPending | RemoteUpdatePending | RemoteDeletionPending)
        /// </summary>
        RemotePending = (RemoteCreationPending | RemoteUpdatePending | RemoteDeletionPending),

        #endregion

        #region X

        /// <summary>
        /// Element has been created in memory, but not persisted to one or more of Local or Remote storage.
        /// <para>
        /// READ/WRITE.
        /// </para>
        /// <para>
        /// Value is 16+2=18 (Binary: 10010)
        /// </para>
        /// This is a CombinationFlag: (LocalCreationPending | RemoteCreationPending)
        /// </summary>
        CreationPending = (LocalCreationPending | RemoteCreationPending),

        /// <summary>
        /// Element has been modified in memory, but not persisted to one or more of Local or Remote storage.
        /// <para>
        /// READ/WRITE.
        /// </para>
        /// <para>
        /// Value is 32+4=36 (Binary: 100100)
        /// </para>
        /// This is a CombinationFlag: (LocalUpdatePending | RemoteUpdatePending).
        /// </summary>
        UpdatePending = (LocalUpdatePending | RemoteUpdatePending),

        /// <summary>
        /// Element has been deleted from memory, but not removed from one or more of Local or Remote storage.
        /// <para>
        /// READ/WRITE.
        /// </para>
        /// <para>
        /// Value is 64+8=72 (Binary: 1001000)
        /// </para>
        /// This is a CombinationFlag: (LocalDeletionPending | RemoteDeletionPending).
        /// </summary>
        DeletionPending = (LocalDeletionPending | RemoteDeletionPending)

        #endregion
    }


}


