﻿using System;


namespace XAct.Domain
{

    public class SimpleAuditableAttribute : AuditableAttribute
    {
        public string SourceIdentifier { get; set; }
    }


    /// <summary>
    /// Attach this Attribute to entities that you want to Audit changes to.
    /// <para>
    /// Requires a second entity which subclasses from the same abstract base
    /// (in order to have the same number of Columns), as well as implements
    /// <see cref="IHasIsAuditModel"/>
    /// </para>
    /// <para>
    /// <code>
    /// public class St
    /// <![CDATA[
    /// //Decorate the class you want to audit, with an attribute
    /// //pointing to the AuditEntity type:
    /// [Auditable(Type = typeof(StudentAudit))]
    /// public class Student :StudentBase, IHas {
    ///   //Move all properties from Student to StudentBase
    /// }
    /// 
    /// public class StudentAudit {
    ///    //So they can be reused from this class as well.
    /// }
    /// ]]>
    /// </code>
    /// Works in conjunction with IUnitOfWorkCommitPreProcessorService:
    /// <code>
    /// <![CDATA[
    /// //Register last
    /// preProcessorService.PreProcessors.Add(XAct.DependencyResolver.Current.GetInstance<AuditableChangesStrategy>());
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// In addition, need to create a Map for the Audit table, that is identicial to the source table
    /// with the addition of columns specific to it.
    /// </para>
    /// </summary>
    public class AuditableAttribute :Attribute 
    {

        /// <summary>
        /// The Type of the class used to Audit the given table.
        /// <para>
        /// Note that it *must* be a subclass of the table being audited 
        /// (so that it has the same Properties).
        /// </para>
        /// </summary>
        public Type Type { get; set; }

    }

    public class AuditablePropertyAttribute : Attribute
    {

        /// <summary>
        /// Path to source property in source object (eg: "Gender.Text")
        /// </summary>
        public string SourcePropertyDotPath { get; set; }

        /// <summary>
        /// If an enum is not available, use this to choose the right value (0 based)
        /// eg: ["Undefined","Male","Female"]
        /// </summary>
        public string[] Options { get; set; }

    }




}
