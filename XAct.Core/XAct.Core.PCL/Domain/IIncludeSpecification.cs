﻿
namespace XAct.Domain.Repositories
{
    /// <summary>
    /// Contract to define what child properties to include in the request,
    /// when invoking 
    /// <see cref="IRepositoryService{TAggregateRootEntity}.GetByFilter"/>
    /// and 
    /// <see cref="IRepositoryService{TAggregateRootEntity}.GetSingle"/>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage:
    /// <code>
    /// <![CDATA[
    ///   var departmentsQuery = 
    ///      _exampleRepository
    ///        .GetByFilter(
    ///          paging: new PagedQuerySpecification(20,0),
    ///          include:new IncludeSpecification(c =>.Addresses, c=>Invoices.Select(i=>i.LineItems)),
    ///          filter: contact => contact.Age >= 18, 
    ///          orderBy: q => q.OrderBy(d => d.Name), 
    ///          new IncludeSpecification("Addresses"));
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public interface IIncludeSpecification 
    {
        /// <summary>
        /// Gets the list of sub properties to include.
        /// <para>
        /// Each Repository will Type as necessary.
        /// </para>
        /// </summary>
        object[] Includes { get; }
    }
}
