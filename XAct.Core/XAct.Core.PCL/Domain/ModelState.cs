﻿//namespace XAct.Domain
//{
//    /// <summary>
// /// <para>
// /// An <c>XActLib</c> contract.
// /// </para>
//    /// An enumeration of the state of the whole domain object
//    /// (not just a value within the entity).
//    /// </summary>
//    /// <remarks>
//    /// Used by <see cref="IModelState"/>
//    /// </remarks>
//    public enum ModelState
//    {
//        /// <summary>
//        /// An error state.
//        /// </summary>
//        Undefined = 0,

//        /// <summary>
//        /// The initial state for retrieved existing objects.
//        /// <para>
//        /// No changes have been made, therefore no save is required.
//        /// </para>
//        /// </summary>
//        Unchanged = 1,

//        /// <summary>
//        /// THe object is new -- changes have to be saved (using a SQL INSERT statement or equivalent).
//        /// </summary>
//        New = 2,

//        /// <summary>
//        /// The object has been persisted/is existing, and changes have to be resaved (using a SQL UPDATE statement or equivalent).
//        /// </summary>
//        Modified = 3,

//        /// <summary>
//        /// The object is to be deleted whether it exists in the datastore or not, (using a SQL DELETE statement or equivalent).
//        /// </summary>
//        Deleted = 4
//    }
//}
