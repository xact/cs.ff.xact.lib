﻿namespace XAct.Domain.Repositories  
{
    /// <summary>
    /// A generic implementation of the <see cref="IIncludeSpecification"/> contract.
    /// used to define what child properties to include in the request,
    /// when invoking 
    /// <see cref="IRepositoryService{TAggregateRootEntity}.GetByFilter"/>
    /// and 
    /// <see cref="IRepositoryService{TAggregateRootEntity}.GetSingle"/>
    /// <para>
    /// An example of its usage would be:
    /// <code>
    /// <![CDATA[
    /// _exampleRepository
    ///        .GetByFilter(
    ///          contact => contact.Id >= 3, 
    ///          new IncludeSpecification("Addresses"));
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// The means of specifying what to Include/Early Bind when returning
    /// elements from an ORM is not neutral -- it's highly vendor specific.
    /// </para>
    /// <para>
    /// By encapsulating the information required into a argument package
    /// Application layer code can specify what to early bind without having
    /// to rely on vendor specific implementations of 
    /// Linq statements (eg: Include in EF).
    /// </para>
    /// <para>
    /// See also <c>PagedQuerySpecification</c>
    /// </para>
    /// </remarks>
    public class IncludeSpecification : IIncludeSpecification
    {
        /// <summary>
        /// Gets the list of property entities to include in the request.
        /// <para>
        /// Each Repository will Type as necessary.
        /// </para>
        /// </summary>
        public object[] Includes { get; private set; }


        /// <summary>
        /// Prevents a default instance of the 
        /// <see cref="IncludeSpecification"/> class from being created.
        /// </summary>
        /// <param name="propertyNames">The property names.</param>
        public IncludeSpecification(params object[] propertyNames)
        {
            Includes = propertyNames;
        }

     
    }
}