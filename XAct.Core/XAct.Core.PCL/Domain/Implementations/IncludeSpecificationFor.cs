namespace XAct.Domain.Repositories
{
    using System;
    using System.Linq.Expressions;

    /// <summary>
    /// A generic implementation of the <see cref="IIncludeSpecification{TEntity}"/> contract.
    /// used to define what child properties to include in the request,
    /// when invoking 
    /// <see cref="IRepositoryService{TAggregateRootEntity}.GetByFilter"/>
    /// and 
    /// <see cref="IRepositoryService{TAggregateRootEntity}.GetSingle"/>
    /// <para>
    /// This one is a specialization of <see cref="IIncludeSpecification"/>
    /// that takes typed Expressions rather than loose objects/strings.
    /// </para>
    /// <para>
    /// An example of its usage would be:
    /// <code>
    /// <![CDATA[
    /// _exampleRepository
    ///        .GetByFilter(
    ///          contact => contact.Id >= 3, 
    ///          new IncludeSpecification(c =>.Addresses, c=>Invoices.Select(i=>i.LineItems)));
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class IncludeSpecification<TEntity> : IIncludeSpecification<TEntity>
    {
        /// <summary>
        /// Gets the list of the entities sub properties to include.
        /// <para>
        /// For nested properties use the Select method:
        /// <code>
        /// <![CDATA[
        /// _exampleRepository
        ///        .GetByFilter(
        ///          contact => contact.Id >= 3, 
        ///          new IncludeSpecification(c =>.Addresses, c=>Invoices.Select(i=>i.LineItems)));
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// </para>
        /// </summary>
        public Expression<Func<TEntity, object>>[] Includes { get; private set; }

        /// <summary>
        /// Gets the list of sub properties to include.
        /// <para>
        /// Each Repository will Type as necessary.
        /// </para>
        /// </summary>
        object[] IIncludeSpecification.Includes
        {
            get
            {
                //From POC this worked:
                return this.Includes;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IncludeSpecification&lt;TEntity&gt;"/> class.
        /// </summary>
        /// <param name="propertyExpressions">The property expressions.</param>
        public IncludeSpecification(params Expression<Func<TEntity, object>>[] propertyExpressions)
        {
            Includes = propertyExpressions;
        }
    }
}