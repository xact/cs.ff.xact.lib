// ReSharper disable CheckNamespace
namespace XAct.Domain.Repositories
// ReSharper restore CheckNamespace
{

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// <para>
    /// According to Martin Fowler:
    /// </para>
    /// <para>
    /// "When you're pulling data in and out of a database, 
    /// it's important to keep track of what you've changed; 
    /// otherwise, that data won't be written back into the database. 
    /// Similarly you have to insert new objects you create 
    /// and remove any objects you delete. 
    /// You can change the database with each change to your object model, 
    /// but this can lead to lots of very small database calls, 
    /// which ends up being very slow. Furthermore it requires 
    /// you to have a transaction open for the whole interaction, 
    /// which is impractical if you have a business transaction that 
    /// spans multiple requests. The situation is even worse if 
    /// you need to keep track of the objects you've read so 
    /// you can avoid inconsistent reads.
    /// A Unit of Work keeps track of everything you do during a 
    /// business transaction that can affect the database. 
    /// When you're done, it figures out everything that needs to be 
    /// done to alter the database as a result of your work."
    /// </para>
    /// <para>
    /// Usage as follows:
    /// </para>
    /// <para>
    /// First, implement the <c>IContextFactory</c> contract:
    /// <code>
    /// <![CDATA[
    /// public class ObjectContextFactory {
    ///   public ObjectContextFactory(){}
    ///   public IObjectContext Create(string tag = null){
    ///     if (tag.ToLower() == "aspecificcontexttag"){return MyOtherContext();}
    ///     //return default context:
    ///     return new MyAppContext();
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Once that's done, all you have to do is create a repo:
    /// <code>
    /// <![CDATA[
    /// IMyRepo myRepo = DependencyResolver.Current.GetInstance<IMyRepo>();
    /// ]]>
    /// </code>
    /// It will get injected with an instance of <c>IUnitOfWorkManagementService</c>,
    /// which will back the internal <c>ObjectContext</c>/<c>DbContext</c> property, and 
    /// will Create or return a Current UoW (using, if it has to, the <c>IContextFactory</c>
    /// defined earlier.
    /// </para>
    /// <para>
    /// After having done changes, use the <c>IUnitOfWorkManagementService</c> to
    /// get the current UoW, in order to commit it:
    /// <code>
    /// <![CDATA[
    /// using (IRepository repository = new ExampleRepository(abstractObjectContext)){
    ///   ...
    ///   repository.MakeQueries(...);
    ///   //Commit may not be necessary yet...
    /// 
    ///   repository.MakeChanges(...);
    ///   //If querying for same objects again, need to Commit() first:
    ///   ...
    ///   IUnitOfWork unitOfWork = DependencyResolver.Current.GetInstance<IUnitOfWorkManagementService>().Current;
    ///   unitOfWork.Commit();
    ///   ...
    ///   repository.MakeQueries(...);
    ///   ...
    ///   repository.MakeChanges(...);
    ///   unitOfWork.Commit();
    ///   ...
    ///   etc.
    /// }
    /// ]]>
    /// </code> 
    /// </para>
    /// <para>
    /// Note that it's common to tack static properties to UnitOfWork
    /// to get the Current instance. Don't agree (SOC: it's a Management issue).
    /// </para>
    /// </remarks>
    /// <internal>
    ///   Interface implemented by <c>UnitOfWork</c>. 
    /// </internal>
    public interface IUnitOfWork : IContext
    {
    }
}