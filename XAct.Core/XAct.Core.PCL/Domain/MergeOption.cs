﻿
namespace XAct.Domain.Repositories
{
    /// <summary>
    /// Specifies how objects being loaded into the object context are merged with
    /// objects already in the object context.
    /// </summary>
    public enum MergeOption
    {
        /// <summary>
        /// 
        /// </summary>
        Undefined = -1,

        /// <summary>
        /// Objects that already exist in the object context are not loaded from the
        /// data source. 
        /// <para>
        /// This is the default behavior.
        /// </para>
        /// </summary>
        AppendOnly = 0,
    
        /// <summary>
        /// Objects are always loaded from the data source. Any property changes made
        /// to objects in the object context are overwritten by the data source values.
        /// </summary>
        OverwriteChanges = 1,

        /// <summary>
        /// In the Entity Framework version 3.5 SP1, the unmodified properties of objects
        /// in the object context are overwritten with server values.
        /// </summary>
        PreserveChanges = 2,
        /// <summary>
        /// Objects are maintained in a System.Data.Entity.EntityState.Detached state and are
        /// not tracked in the System.Data.Entity.Core.Objects.ObjectStateManager.
        /// </summary>
        NoTracking = 3,
    }
}
