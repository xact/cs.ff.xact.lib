﻿namespace XAct.Domain.Repositories
{
    using System;
    using System.Linq.Expressions;

    /// <summary>
    /// A specialization of <see cref="IIncludeSpecification"/>
    /// that takes typed Expressions rather than loose objects/strings.
    /// <para>
    /// An example of its usage would be:
    /// <code>
    /// <![CDATA[
    /// _exampleRepository
    ///        .GetByFilter(
    ///          contact => contact.Id >= 3, 
    ///          new IncludeSpecification(c =>.Addresses, c=>Invoices.Select(i=>i.LineItems)));
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IIncludeSpecification<TEntity> : IIncludeSpecification
    {
        /// <summary>
        /// Gets the list of the entities sub properties to include.
        /// <para>
        /// For nested properties use the Select method:
        /// <code>
        /// <![CDATA[
        /// _exampleRepository
        ///        .GetByFilter(
        ///          contact => contact.Id >= 3, 
        ///          new IncludeSpecification(c =>.Addresses, c=>Invoices.Select(i=>i.LineItems)));
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// </para>
        /// </summary>
        new Expression<Func<TEntity, object>>[] Includes { get; }
    }
}
