﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using XAct.Services.IoC.Initialization;

    public class DependencyResolver : IDependencyResolver
    {
        public static IDependencyResolver Current
        {
            get { return _current; }
        }

        /// <summary>
        /// Gets the named context.
        /// </summary>
        /// <param name="name">The name.</param>
        public static IDependencyResolver GetNamedContext(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The IUnityContainer or IKernel object:
        /// <para>
        /// Property set when the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
        /// </para>
        /// </summary>
        public static object DependencyInjectionContainer;


        ///// <summary>
        ///// The IServiceLocator object, that wraps
        ///// the UnityServiceLocator, which in turn wraps
        ///// IUnityContainer.
        ///// <para>
        ///// Property set when the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
        ///// </para>
        ///// </summary>
        //public static object DependencyInjectionServiceLocator;

        /// <summary>
        /// The dependeny injection container connector
        /// (UnityServiceLocator or NinjectConnector)
        /// <para>
        /// Property set when the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
        /// </para>
        /// </summary>
        public static object DependencyInjectionContainerConnector;


        /// <summary>
        /// Gets or sets the information regarding the initialization/bootstrapping process.
        /// <para>
        /// Null until bootstrapping sequence is actually commenced.
        /// </para>
        /// </summary>
        public static IInitializeLibraryBindingsResults BindingResults { get; set; }


        /// <summary>
        /// Gets or sets the last resolution exception.
        /// </summary>
        /// <value>
        /// The last resolution exception.
        /// </value>
        public static Exception LastResolutionException { get; set; }

        //public XAct.Library.Status.Initialization. X
        //{
        //get { return XAct.Library.Status.Initialization.BindingResults; }
        //}

        private static IDependencyResolver _current = new ServiceLocatorDependencyResolver();

        public void SetInternal(object o)
        {
            _current = o as IDependencyResolver;
        }



        public T GetInstance<T>(bool throwExceptionIfNotFound = true)
            //where T : class
        {
            var result = (T) GetInstance(typeof (T), throwExceptionIfNotFound);
            return result;
        }

        public object GetInstance(Type type, bool throwExceptionIfNotFound = true)
        {
            try
            {
                var result = _current.GetInstance(type);
                return result;
            }
            catch (System.Exception e)
            {
                DependencyResolver.LastResolutionException = e;

                if (throwExceptionIfNotFound)
                {
                    throw;
                }
                return null;
            }
#pragma warning disable 1058
            catch
#pragma warning restore 1058
            {
                if (throwExceptionIfNotFound)
                {
                    throw;
                }
            }
            return null;
        }

        public T GetInstance<T>(string tag, bool throwExceptionIfNotFound = true)
            //where T : class
        {
            var result = (T) GetInstance(typeof (T), tag, throwExceptionIfNotFound);
            return result;
        }

        public object GetInstance(Type type, string tag, bool throwExceptionIfNotFound = true)
        {
            try
            {
                var result = _current.GetInstance(type, tag);
                return result;
            }
            catch (System.Exception e)
            {
                DependencyResolver.LastResolutionException = e;

                if (throwExceptionIfNotFound)
                {
                    throw;
                }
                return null;
            }
#pragma warning disable 1058
            catch
#pragma warning restore 1058
            {
                if (throwExceptionIfNotFound)
                {
                    throw;
                }
            }
            return null;
        }




        public IEnumerable<T> GetInstances<T>()
            where T : class
        {
            var result = GetInstances(typeof (T)).Select(o => o as T);
            return result;
        }

        public IEnumerable<object> GetInstances(Type type)
        {
            try
            {
                var result = _current.GetInstances(type);
                return result;
            }
            catch (System.Exception e)
            {
                DependencyResolver.LastResolutionException = e;

            }
#pragma warning disable 1058
            catch
#pragma warning restore 1058
            {

            }
            return null;
        }

    }



    public interface IDependencyResolver
    {
        void SetInternal(object o);

        object GetInstance(Type type, bool throwExceptionIfNotFound = true);
        object GetInstance(Type type, string tag, bool throwExceptionIfNotFound = true);

        //IEnumerable<object> GetInstances(Type type, string tag);

        T GetInstance<T>(bool throwExceptionIfNotFound = true)
            //where T : class
            ;

        T GetInstance<T>(string tag, bool throwExceptionIfNotFound = true)
            //where T : class
            ;


        IEnumerable<object> GetInstances(Type type);

        IEnumerable<T> GetInstances<T>()
            where T : class;
    }

    public class NullDependencyResolver
    {
        public object GetInstance(Type type)
        {
            return "Bar";
        }

        public object GetInstance(Type type, string tag)
        {
            return "Bar2";

        }

        public IEnumerable<object> GetInstances(Type type)
        {
            return new object[] {"Foo", "Bar"};

        }
    }

    public class ServiceLocatorDependencyResolver : IDependencyResolver
    {
        private Type _locatorType;
        private Func<Type, object> _getInstanceDelegate;
        private Func<Type, string, object> _getInstance2Delegate;
        private Func<Type, IEnumerable<object>> _getAllInstancesDelegate;

        public void SetInternal(object o)
        {
            _locatorType = o.GetType();

            MethodInfo getInstanceMethodInfo = _locatorType.GetMethod("GetInstance", new[] {typeof (Type)});
            MethodInfo getInstance2MethodInfo = _locatorType.GetMethod("GetInstance",
                                                                       new[] {typeof (Type), typeof (string)});
            MethodInfo getAllInstancesMethodInfo = _locatorType.GetMethod("GetAllInstances", new[] {typeof (Type)});


            _getInstanceDelegate =
                (Func<Type, object>)
                Delegate.CreateDelegate(typeof (Func<Type, object>), o, getInstanceMethodInfo);

            _getInstance2Delegate =
                (Func<Type, string, object>)
                Delegate.CreateDelegate(typeof (Func<Type, string, object>), o, getInstance2MethodInfo);

            _getAllInstancesDelegate =
                (Func<Type, IEnumerable<object>>)
                Delegate.CreateDelegate(typeof (Func<Type, IEnumerable<object>>), o, getAllInstancesMethodInfo);
        }

        public T GetInstance<T>(bool throwExceptionIfNotFound = true)
            //where T : class
        {
            return (T) GetInstance(typeof (T), throwExceptionIfNotFound);
        }

        public object GetInstance(Type type, bool throwExceptionIfNotFound = true)
        {
            Initialize();
            try
            {
                return _getInstanceDelegate.Invoke(type);
            }
            catch (System.Exception e)
            {
                DependencyResolver.LastResolutionException = e;

                if (throwExceptionIfNotFound)
                {
                    throw;
                }
                return null;
            }
#pragma warning disable 1058
            catch
#pragma warning restore 1058
            {
                if (throwExceptionIfNotFound)
                {
                    throw;
                }
                return null;
            }
        }

        public T GetInstance<T>(string tag, bool throwExceptionIfNotFound = true)
            //where T : class
        {
            return (T) GetInstance(typeof (T), tag, throwExceptionIfNotFound);
        }

        public object GetInstance(Type type, string tag, bool throwExceptionIfNotFound = true)
        {
            Initialize();
            try
            {
                return _getInstance2Delegate.Invoke(type, tag);
            }
            catch (System.Exception e)
            {
                DependencyResolver.LastResolutionException = e;

                if (throwExceptionIfNotFound)
                {
                    throw;
                }
                return null;
            }
#pragma warning disable 1058
            catch
#pragma warning restore 1058
            {
                if (throwExceptionIfNotFound)
                {
                    throw;
                }
                return null;
            }
        }


        public IEnumerable<T> GetInstances<T>()
            where T : class
        {
            var result = GetInstances(typeof (T)).Select(o => o as T);
            return result;
        }

        public IEnumerable<object> GetInstances(Type type)
        {
            Initialize();
            return _getAllInstancesDelegate.Invoke(type);
        }

        private void Initialize()
        {
            if (_initialized)
            {
                return;
            }
            lock (this)
            {
                if (_initialized)
                {
                    return;
                }

                if (_getInstanceDelegate != null)
                {
                    //Someone did it already, manaully. No need to do it.
                    _initialized = true;
                    return;
                }
                //const string serviceLocatorAssemblyQualifiedName =
                //    "Microsoft.Practices.ServiceLocation.ServiceLocator, Microsoft.Practices.ServiceLocation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";
                
                const string serviceLocatorAssemblyQualifiedName =
                    "Microsoft.Practices.ServiceLocation.ServiceLocator, Microsoft.Practices.ServiceLocation, Version=1.3.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

                var serviceLocatorType = Type.GetType(serviceLocatorAssemblyQualifiedName);

                if (serviceLocatorType != null)
                {
                    PropertyInfo propertyInfo = serviceLocatorType.GetProperty("Current",
                                                                               BindingFlags.Static | BindingFlags.Public);


                    var sl = propertyInfo.GetValue(null, null);

                    SetInternal(sl);
                }


                _initialized = true;
            }
        }

        private bool _initialized;

    }
}