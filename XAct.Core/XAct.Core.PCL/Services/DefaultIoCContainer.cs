﻿using XAct.Diagnostics;
using XAct.Environment;

#if CONTRACTS_FULL || NET40 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace XAct.Services
{
    /// <summary>
    /// Default IoC Container for the XActLib.
    /// Used until until someone registers something better.
    /// </summary>
    /// <internal><para>8/7/2011: Sky</para></internal>
    internal class DefaultIoCContainer : SimpleContainer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultIoCContainer"/> class.
        /// </summary>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public DefaultIoCContainer()
        {
            ITraceSwitchService t = new TraceSwitchService();

            this.RegisterInstance(t);
            this.RegisterInstance<ITracingService>(new DefaultTracingService(t));
            this.RegisterInstance<IEnvironmentService>(new EnvironmentService());
        }
    }
}