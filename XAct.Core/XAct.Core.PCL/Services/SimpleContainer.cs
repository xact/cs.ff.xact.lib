﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Practices.ServiceLocation;

#if CONTRACTS_FULL || NET40 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace XAct.Services
{
    /// <summary>
    /// Very lightweight, nestable, IoC container.
    /// <para>
    /// Implements the ServiceLocator contract.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage:
    /// <code>
    /// <![CDATA[
    /// DefaultIoCContainer container = new DefaultIoCContainer();
    /// ServiceLocatorProvider x = () => container;
    /// Microsoft.Practices.ServiceLocation.ServiceLocator.SetLocatorProvider(x);
    /// ]]>
    /// </code>
    /// Then register stuff:
    /// <code>
    /// <![CDATA[
    /// this.RegisterInstance<ITraceSwitchService>(t);
    /// this.RegisterInstance<ILoggingService>(new DefaultLoggingService(t));
    /// this.RegisterInstance<IEnvironmentService>(new EnvironmentService());
    /// ]]>
    /// </code>
    /// Then pull it back out:
    /// <code>
    /// <![CDATA[
    /// ILoggingService tracingService =
    ///   Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<ILoggingService>();
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <internal>
    /// <![CDATA[
    /// http://bit.ly/ruUXI6
    /// ]]>
    /// </internal>
    internal class SimpleContainer : ServiceLocatorImplBase, IDisposable
    {
        #region Events

        /// <summary>
        /// Occurs when the Container is being Disposed.
        /// </summary>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public event EventHandler Disposing;

        #endregion

        #region Fields

        // type -> key -> obtainer
        /// <summary>
        /// Dictionary of Type (Interface Type) to 
        /// Dictionary of Creators.
        /// <para>
        /// Each creator is a Func that takes the owner Container,
        /// the ServiceContractType, 
        /// an optional tag, 
        /// and returns an object (that matches the contract).
        /// </para>
        /// </summary>
        protected readonly Dictionary<Type, Dictionary<string, Func<SimpleContainer, Type, string, object>>>
            _typeToCreators
                = new Dictionary<Type, Dictionary<string, Func<SimpleContainer, Type, string, object>>>();

        /// <summary>
        /// Flag indicating whether <see cref="Dispose(bool)"/>
        /// has been invoked.
        /// </summary>
        protected bool _disposed;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the parent container (or null if it is the root container).
        /// </summary>
        /// <value>The parent container.</value>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public IServiceLocator ParentContainer { get; set; }

        #endregion

        #region Implementation of ServiceLocatorImplBase

        /// <summary>
        /// Gets the single instance that matches the specified serviceType.
        /// </summary>
        /// <param name="serviceContractType">Type of the service.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// <internal><para>8/7/2011: Sky</para></internal>
    [DebuggerHidden()]
        protected override object DoGetInstance(Type serviceContractType, string key)
        {
            serviceContractType.ValidateIsNotNull("serviceContractType");

            //Get the dictionary of Creators registered against the service Type:
            Dictionary<string, Func<SimpleContainer, Type, string, object>> creatorsDictionary = 
                GetCreatorsDictionary(serviceContractType);

            //If found...
            if (creatorsDictionary != null && creatorsDictionary.Count > 0)
            {
                //
                Func<SimpleContainer, Type, string, object> creatorFunc;

                //We first try to find it by key (elegant linear code btw):
                if (string.IsNullOrEmpty(key) || !creatorsDictionary.TryGetValue(key, out creatorFunc) ||
                    creatorFunc == null)
                {
                    // fallback to empty key
                    creatorsDictionary.TryGetValue(string.Empty, out creatorFunc);
                }
                //If found,
                if (creatorFunc != null)
                {
                    //invoke the creator Func
                    return creatorFunc(this, serviceContractType, key);
                }
            }

            // try to use parent container before failing the resolving process:
            if (ParentContainer != null)
            {
                return ParentContainer.GetInstance(serviceContractType, key);
            }
            throw new ActivationException(string.Format("Service '{0}' with key '{1}' not found.",
                                                        serviceContractType.AssemblyQualifiedName, key));
        }

        [System.Diagnostics.DebuggerHidden()]
        private Dictionary<string, Func<SimpleContainer, Type, string, object>> GetCreatorsDictionary(Type serviceContractType)
        {
            try
            {
                return _typeToCreators[serviceContractType];
            }catch
            {
                return null;
            }
        }


        /// <summary>
        /// Get all instances that match the service Type.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns></returns>
        /// <internal><para>8/7/2011: Sky</para></internal>
        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
        {
            serviceType.ValidateIsNotNull("serviceType");

            Dictionary<string, Func<SimpleContainer, Type, string, object>> creatorsDictionary =
                _typeToCreators[serviceType];

            if (creatorsDictionary == null || creatorsDictionary.Count == 0)
            {
                yield break;
            }

            foreach (
                var creatorsPair in creatorsDictionary)
            {
                //Invoke each creator Func:
                Func<SimpleContainer, Type, string, object> creatorFunc = creatorsPair.Value;

                yield return creatorFunc(this, serviceType, creatorsPair.Key);
            }

            if (ParentContainer != null)
            {
                foreach (object instance in ParentContainer.GetAllInstances(serviceType))
                {
                    yield return instance;
                }
            }
        }

        #endregion

        #region Public Methods specific to this IoC

        /// <summary>
        /// Registers a creator against the specified service contract type.
        /// </summary>
        /// <typeparam name="TServiceContract">The type of the service contract.</typeparam>
        /// <param name="creator">The creator.</param>
        /// <param name="key">The key.</param>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public void RegisterCreator<TServiceContract>(Func<SimpleContainer, Type, string, object> creator,
                                                      string key = null)
        {
            RegisterCreator(typeof (TServiceContract), creator, key);
        }


        /// <summary>
        /// Registers a creator against the specified service contract type.
        /// </summary>
        /// <param name="type">The service contract type.</param>
        /// <param name="key">The optional key.</param>
        /// <param name="creatorFunc">The creator function.</param>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public void RegisterCreator(Type type, Func<SimpleContainer, Type, string, object> creatorFunc, string key)
        {
            creatorFunc.ValidateIsNotNull("creatorFunc");

            Dictionary<string, Func<SimpleContainer, Type, string, object>> creatorsDictionary;

            //Extract from the cache, the dictionary of creators, registered by Type:
            if (!_typeToCreators.TryGetValue(type, out creatorsDictionary))
            {
                //If first time, add a new nested dictionary:
                _typeToCreators.Add(type,
                                    creatorsDictionary =
                                    new Dictionary<string, Func<SimpleContainer, Type, string, object>>(
                                        StringComparer.OrdinalIgnoreCase));
            }
            //Register the creator against a name, or an empty string (the default):
            creatorsDictionary[key ?? string.Empty] = creatorFunc;
        }

        #endregion

        #region disposing

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }

            try
            {
                Dispose(true);
            }
            finally
            {
                _disposed = true;
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// Raises the <see cref="Disposing"/> event.
        /// </summary>
        /// <param name="ea">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// <internal><para>8/7/2011: Sky</para></internal>
        protected void OnDisposing(EventArgs ea)
        {
            EventHandler eh = Disposing;
            if (eh != null)
            {
                eh(this, ea);
            }
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        /// <internal><para>8/7/2011: Sky</para></internal>
        protected virtual void Dispose(bool disposing)
        {
            OnDisposing(EventArgs.Empty);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="SimpleContainer"/> is reclaimed by garbage collection.
        /// </summary>
        /// <internal><para>8/7/2011: Sky</para></internal>
        ~SimpleContainer()
        {
            Dispose(false);
        }

        #endregion
    }
}