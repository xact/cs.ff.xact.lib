﻿namespace XAct.Services
{
    using System;

    /// <summary>
    /// Contract for a service to convert an object of Source Type to target Type.
    /// <para>
    /// Do not confuse with <c>IObjectMappingService</c>.
    /// </para>
    /// </summary>
    public interface IConversionService : IHasXActLibService
    {
        
        /// <summary>
        ///   Convert a type to another type.
        /// <para>
        ///   Note how this is slightly better than the tradional
        ///   'ConvertTo' method, which returns all as an object that 
        ///   then needs to be boxed.
        /// </para>
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The value.</param>
        /// <returns></returns>
        TTarget Convert<TTarget>(object source);

        /// <summary>
        ///   Convert a type to another type.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <returns></returns>
        object Convert(object source, Type targetType);
 
    }
}
