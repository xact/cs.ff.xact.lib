﻿namespace XAct.Services
{
    using System;

    /// <summary>
    /// <para>
    /// See <see cref="BindingDescriptor"/>
    /// </para>
    /// <para>
    /// See <see cref="ServiceLocatorService"/>
    /// </para>
    /// </summary>
    public class InstanceBindingDescriptor : IInstanceBindingDescriptor
    {
        /// <summary>
        /// Gets or sets the type of the interface.
        /// <para>
        /// eg: <c>IExServiceConfiguration</c>
        /// </para>
        /// </summary>
        /// <value>
        /// The type of the interface.
        /// </value>
        public Type InterfaceType { get; set; }


        /// <summary>
        /// Gets or sets the instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public object Instance
        {
            get {
                return _instance ?? _instanceBuilder.Invoke();
            }   
            set { _instance = value; }
        }

        private object _instance;
        private readonly Func<object> _instanceBuilder;  

        /// <summary>
        /// Gets or sets the Service Lifecycle.
        /// <para>
        /// eg: <c>IExService</c>
        /// </para>
        /// </summary>
        /// <value>
        /// The type of the service life.
        /// </value>
        public BindingLifetimeType ServiceLifeType { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="InstanceBindingDescriptor"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="instance">The instance.</param>
        /// <param name="serviceLifeType">Type of the service life.</param>
        public InstanceBindingDescriptor(Type type, object instance, BindingLifetimeType serviceLifeType = BindingLifetimeType.SingletonPerWebRequestScope)
        {
            type.ValidateIsNotDefault("type");
            instance.ValidateIsNotDefault("instance");

            InterfaceType = type;
            _instance = instance;
            
            ServiceLifeType = serviceLifeType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InstanceBindingDescriptor"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="instanceBuilder">The instance builder.</param>
        /// <param name="serviceLifeType">Type of the service life.</param>
        public InstanceBindingDescriptor(Type type, Func<object> instanceBuilder, BindingLifetimeType serviceLifeType = BindingLifetimeType.SingletonPerWebRequestScope)
        {
            type.ValidateIsNotDefault("type");
            instanceBuilder.ValidateIsNotDefault("instanceBuilder");

            InterfaceType = type;
            _instanceBuilder = instanceBuilder;

            ServiceLifeType = serviceLifeType;
        }
    }
}
