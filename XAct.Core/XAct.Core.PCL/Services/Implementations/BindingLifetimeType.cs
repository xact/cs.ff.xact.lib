﻿namespace XAct.Services
{
    using System.Runtime.Serialization;



    [DataContract]
    public enum BindingType
    {
        /// <summary>
        /// No information as to how the binding was created.
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// The binding was build by hand.
        /// </summary>
        [EnumMember]
        Custom=1,

        /// <summary>
        /// THe binding was defined as an Attribute (ie, using <see cref="DefaultBindingImplementationAttribute"/>
        /// </summary>
        [EnumMember]
        Attribute =2,

        /// <summary>
        /// Defined via an interface (ie, implements <see cref="IHasBindingScope"/>).
        /// </summary>
        [EnumMember]
        Interface = 3
    }


    /// <summary>
    /// Type of LifeTime
    /// </summary>
    [DataContract]
    public enum BindingLifetimeType
    {
        /// <summary>
        /// 
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// Each time the service is requested, 
        /// a new instance will be built.
        /// <para>
        /// In Unity, referred to as Transient.
        /// </para>
        /// </summary>
        [EnumMember]
        TransientScope = 1,

        /// <summary>
        /// Same object returned every time, throughout app, across all threads.
        /// </summary>
        [EnumMember]
        SingletonScope = 2,

        /// <summary>
        /// Same object returned every time, per thread.
        /// </summary>
        [EnumMember]
        SingletonPerThreadScope = 3,


        /// <summary>
        /// Same object returned every time, per request.
        /// <para>
        /// Falls back to SingletonPerThreadScope when not in a web context.
        /// </para>
        /// </summary>
        [EnumMember]
        SingletonPerWebRequestScope = 4
    }
}