﻿// ReSharper disable CheckNamespace
namespace XAct.Services
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a package of variables indicating how the Binding registration went.
    /// </summary>
    public interface IBindingDescriptorResult
    {
        /// <summary>
        /// Gets the <see cref="IBindingDescriptor"/> that was trying to be registered.
        /// </summary>
        /// <value>
        /// The binding descriptor.
        /// </value>
        IBindingDescriptor BindingDescriptor { get; }

        /// <summary>
        /// Gets a value indicating whether this the binding was skipped (indicating it was already registered earlier).
        /// </summary>
        /// <value>
        ///   <c>true</c> if skipped; otherwise, <c>false</c>.
        /// </value>
        bool Skipped { get; }

        
        /// <summary>
        /// Gets the (optional) exception raised when it tried to Register the Binding.
        /// </summary>
        /// <value>
        /// The exception.
        /// </value>
        Exception Exception { get; }


        /// <summary>
        /// Gets a value indicating whether to throw the Exception. Default is yes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [T hrow exception]; otherwise, <c>false</c>.
        /// </value>
        bool ThrowException { get; }
    }

    /// <summary>
    /// Package of variables indicating how the Binding registration went.
    /// </summary>
    public class BindingDescriptorResult : IBindingDescriptorResult
    {
        /// <summary>
        /// Gets the <see cref="IBindingDescriptor"/> that was trying to be registered.
        /// </summary>
        /// <value>
        /// The binding descriptor.
        /// </value>
        public IBindingDescriptor BindingDescriptor { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this the binding was skipped (indicating it was already registered earlier).
        /// </summary>
        /// <value>
        ///   <c>true</c> if skipped; otherwise, <c>false</c>.
        /// </value>
        public bool Skipped { get; private set; }

        /// <summary>
        /// Gets the (optional) exception raised when it tried to Register the Binding.
        /// </summary>
        /// <value>
        /// The exception.
        /// </value>
        public Exception Exception { get; private set; }



        /// <summary>
        /// Gets a value indicating whether to throw the Exception. Default is yes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [T hrow exception]; otherwise, <c>false</c>.
        /// </value>
        public bool ThrowException { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingDescriptorResult" /> class.
        /// </summary>
        /// <param name="bindingDescriptor">The binding descriptor.</param>
        /// <param name="skipped">if set to <c>true</c> [skipped].</param>
        /// <param name="exception">The exception.</param>
        /// <param name="throwException">if set to <c>true</c> [throw exception].</param>
        public BindingDescriptorResult(IBindingDescriptor bindingDescriptor, bool skipped, Exception exception =null, bool throwException = true)
        {
            this.BindingDescriptor = bindingDescriptor;
            Skipped = skipped;
            Exception = exception;
            ThrowException = throwException;
        }
    }
    /// <summary>
    /// An implementation of <see cref="IBindingDescriptor"/>
    /// to describe the service to register in an DependencyInjectionContainer.
    /// <para>
    /// </para>
    /// </summary>
    public class BindingDescriptor : IBindingDescriptor
    {
        /// <summary>
        /// The way the Binding was discovered (via enum, or inheritence of <see cref="IHasBindingScope"/>
        /// </summary>
        public BindingType BindingType { get; set; }


        /// <summary>
        /// Gets or sets the type of the interface.
        /// </summary>
        /// <value>
        /// The type of the interface.
        /// </value>
        public Type InterfaceType { get; set; }

        /// <summary>
        /// Gets or sets the type of the implementation to register against the interface.
        /// </summary>
        /// <value>
        /// The type of the implementation.
        /// </value>
        public Type ImplementationType { get; set; }

        /// <summary>
        /// Gets or sets the Service Lifecycle.
        /// </summary>
        /// <value>
        /// The type of the service life.
        /// </value>
        public BindingLifetimeType ServiceLifeType { get; set; }


        /// <summary>
        /// Gets or sets the optional tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public string Tag { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="BindingDescriptor"/> class.
        /// </summary>
        public BindingDescriptor()
        {
        }


        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="BindingDescriptor" /> class.
        /// <para>
        /// Example of code usage:
        /// <code>
        /// <![CDATA[
        /// class Program {
        /// static void Main(string[] args){
        /// UnityBootstrapper.Initialize(
        /// null,
        /// null,
        /// null,
        /// new ServiceRegistrationDescriptor(
        /// typeof(IEx),
        /// typeof(Ex),
        /// ServiceLifetimeType.SingletonPerWebRequestScope
        /// )
        /// );
        /// }
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="bindingType">Type of the binding.</param>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="implementationType">Type of the implementation.</param>
        /// <param name="serviceLifetimeType">Type of the service lifetime.</param>
        /// <param name="tag">The tag.</param>
        public BindingDescriptor(BindingType bindingType, Type interfaceType, Type implementationType, BindingLifetimeType serviceLifetimeType = BindingLifetimeType.Undefined, string tag = null)
        {
            this.BindingType = bindingType;
            this.InterfaceType = interfaceType;
            this.ImplementationType = implementationType;
            this.Tag = tag;

            if (serviceLifetimeType == BindingLifetimeType.Undefined)
            {
// ReSharper disable RedundantNameQualifier
                serviceLifetimeType = Library.Settings.Bindings.DefaultServiceLifespan;
// ReSharper restore RedundantNameQualifier
            }

            this.ServiceLifeType = serviceLifetimeType;

        }


        /// <summary>
        /// Initializes the specified service lifetime type.
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface.</typeparam>
        /// <typeparam name="TImplementation">The type of the implementation.</typeparam>
        /// <param name="serviceLifetimeType">Type of the service lifetime.</param>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        public BindingDescriptor Initialize<TInterface, TImplementation>(BindingLifetimeType serviceLifetimeType = BindingLifetimeType.SingletonScope,string tag=null)
        {
            
            this.InterfaceType = typeof(TInterface);
            this.ImplementationType = typeof(TImplementation);
            this.ServiceLifeType = serviceLifetimeType;
            this.Tag = tag;

            return this;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "Binding:{0}=>{1} (Lifetime:{2}, Priority:{3}, Tag:{4}) ({5})".FormatStringCurrentCulture(this.InterfaceType, this.ImplementationType, this.ServiceLifeType.ToString(), "?", this.Tag, base.ToString());
        }

    }
}
