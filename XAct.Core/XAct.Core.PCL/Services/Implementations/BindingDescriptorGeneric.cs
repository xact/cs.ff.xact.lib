﻿
// ReSharper disable CheckNamespace
namespace XAct.Services
// ReSharper restore CheckNamespace
{
    using XAct.Library.Settings;

    /// <summary>
    /// <para>
    /// A vendor agnostic way of defining Services to be
    /// registered in an DependencyInjectionContainer such as Ninject or Unity
    /// </para>
    /// <para>
    /// This one is a Generic-enabled version of
    /// <see cref="BindingDescriptor"/>, 
    /// making for a bit more fluid initialization.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage:
    /// <code>
    /// <![CDATA[
    /// class Program {
    ///   static void Main(string[] args){
    ///     UnityBootstrapper.Initialize(
    ///       null,
    ///       null,
    ///       null,
    ///       new ServiceRegistrationDescriptor<IEx,Ex>(
    ///         ServiceLifetimeType.SingletonPerWebRequestScope
    ///         )
    ///     );
    ///     
    ///     //All good to go...
    ///     Console.WriteLine(
    ///       DependencyResolver.Current
    ///         .GetInstance<IEx>().DoSomething());
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <typeparam name="TInterface">The type of the interface.</typeparam>
    /// <typeparam name="TImplementation">The type of the instance.</typeparam>
    public class BindingDescriptor<TInterface, TImplementation> : BindingDescriptor
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="BindingDescriptor&lt;TInterface, TImplementation&gt;" /> class.
        /// <para>
        /// Uses <see cref="BindingType.Undefined"/> and <see cref="Bindings.DefaultServiceLifespan"/>
        /// </para>
        /// </summary>
        public BindingDescriptor()
// ReSharper disable RedundantNameQualifier
            : base(BindingType.Undefined,  typeof(TInterface), typeof(TImplementation),Library.Settings.Bindings.DefaultServiceLifespan)
// ReSharper restore RedundantNameQualifier
        {

        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="BindingDescriptor&lt;TInterface, TImplementation&gt;" /> class.
        /// </summary>
        /// <param name="bindingType">Type of the binding.</param>
        /// <param name="serviceLifetimeType">Type of the service lifetime.</param>
        /// <param name="tag">The tag.</param>
        public BindingDescriptor(BindingType bindingType, BindingLifetimeType serviceLifetimeType, string tag =null)
            : base(bindingType, typeof(TInterface), typeof(TImplementation), serviceLifetimeType,tag)
        {

        }



    }
}
