﻿namespace XAct.Services
{
    using System;


    /// <summary>
    /// An attribute to attach to Services that are default library services.
    /// <para>
    /// This attribute is attached to the Service Implementation, 
    /// defining the Service Interface.
    /// </para>
    /// <para>
    /// When <c>XAct.Library.Initialization.Bindings.RegisterBindings</c> 
    /// is invoked by a Boostrapper,
    /// it scans all assemblies for class definitions decorated with this attribute
    /// and registers them in the current DependencyInjectionContainer (Unity, Ninject, etc.)
    /// </para>
    /// <para>
    /// Think of it as being similar to a pre .NET40 equivalent of MEF
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Example of setting up a service:
    /// <code>
    /// <![CDATA[
    /// [DefaultBindingImplementation(typeof(IMyService),ServiceLifetimeType.Singleton,"Alternate")]
    /// [DefaultBindingImplementation(typeof(IMyService),ServiceLifetimeType.Singleton)]
    /// public class MyService : IMyService {
    ///   ...
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// With the above in place, all that is needed is:
    /// <code>
    /// <![CDATA[
    /// void Bootstrapper(){
    ///   XAct.Library.Initialization.Bindings.RegisterBindings();
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Services can then be retrieved using:
    /// <code>
    /// <![CDATA[
    /// IMyService myservice = 
    ///   DependencyResolver.Current.Get<IMyService>();
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class,AllowMultiple=true,Inherited=false)]
    public class DefaultBindingImplementationAttribute : Attribute, IHasPriorityReadOnly, IHasTagReadOnly
    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="DefaultBindingImplementationAttribute"/> class.
//        /// <para>
//        /// Default Scope: Singleton
//        /// </para>
//        /// 	<para>
//        /// Default Priority: 0
//        /// </para>
//        /// 	<para>
//        /// Default Name: null
//        /// </para>
//        /// </summary>
//        /// <param name="serviceInterface">The service interface.</param>
//        /// <internal>
//        /// The reason for this overload is that Attribute
//        /// constructor arguments can't have default arguments (compiler exception raised).
//        ///   </internal>
//// ReSharper disable RedundantNameQualifier
//// ReSharper disable RedundantArgumentDefaultValue
//        public DefaultBindingImplementationAttribute(Type serviceInterface):this(serviceInterface,BindingLifetimeType.Undefined,Priority.Low,"")
//// ReSharper restore RedundantArgumentDefaultValue
//// ReSharper restore RedundantNameQualifier
//        {
//            //ServiceInterface = serviceInterface;
//            //LifeSpan = XAct.Lib.DefaultServiceLifespan;
//            //Name = null;
//            //Priority = 0;
//        }
        ///// <summary>
        ///// Initializes a new instance of the <see cref="DefaultBindingImplementationAttribute"/> class.
        ///// </summary>
        ///// <param name="serviceInterface">The service interface.</param>
        ///// <param name="lifeSpan">The life span.</param>
        ///// <param name="priority">The priority.</param>
        ///// <param name="tag">The tag.</param>
        //public DefaultBindingImplementationAttribute(Type serviceInterface, BindingLifetimeType lifeSpan, int priority = 0, string tag = "")
        //    :this(serviceInterface,lifeSpan,((Priority)priority),tag)
        //{

        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultBindingImplementationAttribute"/> class.
        /// <para>
        /// Lifespan: as this is for registering Services, which are by their very nature stateless,
        /// the default LifeSpan is Singleton, available on all requests on all threads.
        /// </para>
        /// 	<para>
        /// Priority: the default priority is Normal.
        /// If you want to ensure your services replace
        /// library services, set their priority to <c>High</c>.
        /// </para>
        /// 	<para>
        /// Name: if you need to register two or more service implementations, one can
        /// tag them for later retrival.
        /// </para>
        /// </summary>
        /// <param name="serviceInterface">The service interface.</param>
        /// <param name="lifeSpan">The life span.</param>
        /// <param name="priority">The priority.</param>
        /// <param name="tag">The tag.</param>
        /// <internal>8/15/2011: Sky</internal>
        public DefaultBindingImplementationAttribute(Type serviceInterface, BindingLifetimeType lifeSpan , XAct.Priority priority, string tag = "")
        {
            ServiceInterface = serviceInterface ;
            LifeSpan = (lifeSpan != BindingLifetimeType.Undefined) ? lifeSpan : Library.Settings.Bindings.DefaultServiceLifespan;
            Tag = tag;
            Priority = priority;
        }

        /// <summary>
        /// Gets or sets the service interface.
        /// </summary>
        /// <value>The service interface.</value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public Type ServiceInterface { get; private set; }

        /// <summary>
        /// Gets the Service life span.
        /// </summary>
        public BindingLifetimeType LifeSpan { get; private set; }

        /// <summary>
        /// Gets the priority in which to register services.
        /// <para>
        /// -10 = Library Configuration 
        /// -5 Early Services (custom replacements of Library Services)
        /// 0 Library Services
        /// </para>
        /// </summary>
        public Priority Priority { get; private set; }

        /// <summary>
        /// The optional tag
        /// </summary>
        public string Tag { get; private set; }
    }
}