﻿
namespace XAct.Services
{
    using System.Collections.Generic;

    /// <summary>
    /// An implementation of <see cref="IBindingDescriptorGroup"/>
    /// for a host to pass to an DependencyInjectionContainer Bootrapper Services in a modular way.
    /// </summary>
    public class BindingDescriptorGroup : IBindingDescriptorGroup
    {
        ///// <summary>
        ///// Initializes a new instance of the <see cref="ServiceBindingDescriptorGroup"/> class.
        ///// </summary>
        //public ServiceBindingDescriptorGroup()
        //{
        //}


        /// <summary>
        /// Initializes a new instance of the <see cref="BindingDescriptorGroup"/> class.
        /// </summary>
        /// <param name="serviceBindingDescriptors">The service binding descriptors.</param>
        public BindingDescriptorGroup(params IBindingDescriptorBase[] serviceBindingDescriptors)
        {
            _serviceBindings.AddRange(serviceBindingDescriptors);
        }


        /// <summary>
        /// Gets the nested service bindings.
        /// </summary>
        public List<IBindingDescriptorBase> ServiceBindings 
        {
            get { return _serviceBindings; }
        }
        readonly List<IBindingDescriptorBase> _serviceBindings 
            = new List<IBindingDescriptorBase>();
// ReSharper restore InconsistentNaming


        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "BindingGroup:{0} Bindings({1})".FormatStringCurrentCulture(_serviceBindings.Count, base.ToString());
        }

    }
}
