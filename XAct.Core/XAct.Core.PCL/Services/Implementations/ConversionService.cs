﻿namespace XAct.Services.Implementations
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Reflection;

    public class ConversionService : IConversionService
    {
        /// <summary>
        ///   Convert a type to another type.
        /// <para>
        ///   Note how this is slightly better than the tradional
        ///   'ConvertTo' method, which returns all as an object that 
        ///   then needs to be boxed.
        /// </para>
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The value.</param>
        /// <returns></returns>
        public TTarget Convert<TTarget>(object source)
        {
            return (TTarget) this.Convert(source, typeof (TTarget));
        }


        /// <summary>
        /// Convert a type to another type.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <returns></returns>
        public object Convert(object source, Type targetType)
        {

            targetType.ValidateIsNotDefault("destinationType");
            Contract.EndContractBlock();

            if (source.IsDefault())
            {
                //The pre-generics equivalent of default(T):
                //Remark: strings are not ValueType, so are returned as null.
                //Remark: guid are ValueType, so are returned as Guid.Empty.
                //Remark: int are ValueType, so are returned as Int32.Empty.
                return (targetType.IsValueType)
                           ? Activator.CreateInstance(targetType)
                           : null;
            }

            //Value is not null, so safe to get type:
            Type srcType = source.GetType();

            if (srcType == typeof(string))
            {
                if (targetType == typeof(string))
                {
                    //Don'type change the normal behavior then...
                    //AND 
                    //Might as well get out early...
                    return source;
                }
                // ReSharper disable ConvertIfStatementToConditionalTernaryExpression
                if (String.IsNullOrEmpty((string)source))
                // ReSharper restore ConvertIfStatementToConditionalTernaryExpression
                {
                    //Empty string... Hum...
                    //But being converted to a string, or something else?
                    //But if going to be converted........
                    //Much better results to let it decide from a null, 
                    //rather than choke on an empty string:
                    source = null;
                }
                else
                {
                    //we know its not going to be a string
                    //so trim it:
                    source = ((string)source).Trim();
                }
            }


            try
            {
                return System.Convert.ChangeType(source, targetType, null);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }

            //Ok  so we failed doing it the simplest way...
            //But is there a constructor that we can use?
            //For example, Version (which is a class) 
            //can take a string arg:
            ConstructorInfo constructorInfo =
                targetType.GetConstructor(new[] { srcType });
            if (constructorInfo != null)
            {
                try
                {
                    return constructorInfo.Invoke(new[] {source});
                }
                catch
                {
                }
            }
            return (targetType.IsValueType)
                       ? Activator.CreateInstance(targetType)
                       : null;

        }

    }
}