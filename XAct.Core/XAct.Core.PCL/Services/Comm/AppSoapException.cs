﻿

//namespace XAct.Services.Comm.ServiceModel
//{
//    using System;
//    using System.Globalization;
//    using System.Runtime.Serialization;

//    /// <summary>
//    /// A generic FaultContract.
//    /// </summary>
//#pragma warning disable 1591
//    [DataContract]
//    public class AppSoapException
//    {
//        [DataMember]
//        public string Message { get; set; }

//        /// <summary>
//        /// Gets or sets the name of the method that threw the Exception.
//        /// </summary>
//        /// <value>
//        /// The method.
//        /// </value>
//        [DataMember]
//        public string Method { get; set; }

//        /// <summary>
//        /// Gets or sets the name of the method that threw the Exception.
//        /// </summary>
//        /// <value>
//        /// The assembly.
//        /// </value>
//        [DataMember]
//        public string Assembly { get; set; }

//        /// <summary>
//        /// Gets or sets the service side's stack trace.
//        /// </summary>
//        /// <value>
//        /// The stack.
//        /// </value>
//        [DataMember]
//        public string Stack { get; set; }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="AppSoapException" /> class.
//        /// </summary>
//        /// <internal>
//        /// Always provide an argumentless constructor.
//        /// </internal>
//        public AppSoapException()
//        {
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="AppSoapException" /> class.
//        /// </summary>
//        /// <param name="publicApi">if set to <c>true</c> [public API].</param>
//        /// <param name="e">The e.</param>
//        /// <param name="includeStack">if set to <c>true</c> [include stack].</param>
//        public AppSoapException(bool publicApi = false, Exception e = null, bool includeStack = false)
//        {
//            //In the Message, state the MessageType:

//            if (e == null)
//            {
//                //easy day at the office.
//                return;
//            }
//            //The message will be the Type that caused the Exception
//            Message = e.GetType().Name + ": " + e.Message;

//            //See if we can prepend the ClassName:
//            if (e.TargetSite != null)
//            {
//                Method = e.TargetSite.DeclaringType.FullName + " :: " + e.TargetSite;
//            }


//            Assembly = (!string.IsNullOrEmpty(e.Source)) ? e.Source : null;

//            if (!publicApi)
//            {

//                if (includeStack)
//                {
//                    //Only dream of include the stack if the API endpoint is internal:
//                    Stack = "ServerStack: " + e.StackTrace;
//                }
//            }
//        }

//        /// <summary>
//        /// Returns a <see cref="System.String" /> that represents this instance.
//        /// </summary>
//        /// <returns>
//        /// A <see cref="System.String" /> that represents this instance.
//        /// </returns>
//        public override string ToString()
//        {
//            return string.Format(CultureInfo.InvariantCulture, "{0}", Method);
//        }
//    }
//}
//#pragma warning restore 1591
