﻿//using System;
//using System.Collections.Generic;

//namespace XAct.Services
//{
//    /// <summary>
//    ///   Provides an interface that *mirrors*
//    /// the MSPP Common ServiceLocator service
//    /// </summary>
//    public interface IServiceLocatorService : IServiceProvider, IHasXActLibServiceDefinition
//    {
//        /// <summary>
//        /// Gets the inner DependencyInjectionContainer.
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <returns></returns>
//        T GetInnerServiceLocator<T>() ;

//        /// <summary>
//        /// Get's an instance of the Service of the specified Type.
//        /// </summary>
//        /// <param name="serviceType">The interface to the requested Service.</param>
//        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
//        /// <returns>
//        /// The instance.
//        /// </returns>
//        object GetInstance(Type serviceType, bool raiseExceptionIfNotFound = true);

//        /// <summary>
//        /// Get's an instance of the Service of the specified Type.
//        /// </summary>
//        /// <param name="serviceType">The interface to the requested Service.</param>
//        /// <param name="key">The string key.</param>
//        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
//        /// <returns>
//        /// The instance.
//        /// </returns>
//        object GetInstance(Type serviceType, string key, bool raiseExceptionIfNotFound = true);


//        /// <summary>
//        ///   Get All Instances that match the given Service Interface Type.
//        /// </summary>
//        /// <param name = "serviceType">Type of the service.</param>
//        /// <returns>An enumerable collection of the instantiated Services.</returns>
//        IEnumerable<object> GetAllInstances(Type serviceType);

//        /// <summary>
//        /// Get's an instance of the Service of the specified Type.
//        /// </summary>
//        /// <typeparam name="TService">The interface to the requested Service.</typeparam>
//        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
//        /// <returns>
//        /// The instance.
//        /// </returns>
//        TService GetInstance<TService>(bool raiseExceptionIfNotFound = true/*, bool autoRegisterIfNotFound = false*/);

//        /// <summary>
//        /// Get's an instance of the Service of the specified Type.
//        /// </summary>
//        /// <typeparam name="TService">The interface to the requested Service.</typeparam>
//        /// <param name="key">The string key.</param>
//        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
//        /// <returns>
//        /// The instance.
//        /// </returns>
//        TService GetInstance<TService>(string key, bool raiseExceptionIfNotFound = true);

//        /// <summary>
//        ///   Get All Instances that match the given Service Interface Type.
//        /// </summary>
//        /// <typeparam name = "TService"></typeparam>
//        /// <returns>An enumerable collection of the instantiated Services.</returns>
//        IEnumerable<TService> GetAllInstances<TService>();
//    }
//}