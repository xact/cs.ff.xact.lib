﻿// ReSharper disable CheckNamespace
namespace XAct.Services
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for registering services.
    /// </summary>
    /// <internal>
    /// I was going to enherit from IServiceBindingBase and keep them
    /// separate...but i think the value outweights the 'pure'
    /// </internal>
    public interface IBindingDescriptor : IBindingDescriptorBase
    {
        /// <summary>
        /// The way the Binding was discovered (via enum, or inheritence of <see cref="IHasBindingScope"/>
        /// </summary>
        BindingType BindingType {get;set;}

        /// <summary>
        /// Gets or sets the type of the interface.
        /// </summary>
        /// <value>
        /// The type of the interface.
        /// </value>
        Type InterfaceType { get; set; }

        /// <summary>
        /// Gets or sets the type of the implementation to register against the interface.
        /// </summary>
        /// <value>
        /// The type of the implementation.
        /// </value>
        Type ImplementationType { get; set; }

        /// <summary>
        /// Gets or sets the Service Lifecycle.
        /// </summary>
        /// <value>
        /// The type of the service life.
        /// </value>
        BindingLifetimeType ServiceLifeType { get; set; }

        /// <summary>
        /// Gets or sets the optional tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        string Tag { get; set; }
}
}