namespace XAct.Services
{
    using System;

    /// <summary>
    /// Contract for defining an DependencyInjectionContainer Instance (versus a Service) Binding
    /// <para>
    /// <see cref="IBindingDescriptor"/>
    /// </para>
    /// </summary>
    public interface IInstanceBindingDescriptor
    {
        /// <summary>
        /// Gets or sets the type of the interface.
        /// <para>
        /// eg: <c>IExServiceConfiguration</c>
        /// </para>
        /// </summary>
        /// <value>
        /// The type of the interface.
        /// </value>
        Type InterfaceType { get; set; }

        /// <summary>
        /// Gets or sets the instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        object Instance { get; set; }

        /// <summary>
        /// Gets or sets the Service Lifecycle.
        /// <para>
        /// eg: <c>IExService</c>
        /// </para>
        /// </summary>
        /// <value>
        /// The type of the service life.
        /// </value>
        BindingLifetimeType ServiceLifeType { get; set; }
    }
}