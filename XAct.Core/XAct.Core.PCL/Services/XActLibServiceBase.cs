﻿
namespace XAct.Services
{
    using XAct.Diagnostics;

    /// <summary>
    /// Common abstract base for most Services.
    /// </summary>
    public abstract class XActLibServiceBase : ServiceBase, IHasXActLibService
    {
        protected XActLibServiceBase(ITracingService tracingService) : base(tracingService)
        {
        }
    }
}

