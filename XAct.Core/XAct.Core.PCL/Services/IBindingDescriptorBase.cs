﻿
namespace XAct.Services
{
    using System;

    /// <summary>
    /// Base contract for 
    /// <see cref="IBindingDescriptorGroup"/>
    /// and
    /// <see cref="IBindingDescriptor"/>
    /// </summary>
    public interface IBindingDescriptorBase /*: IComparable */
    {
        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        string ToString();
    }
}
