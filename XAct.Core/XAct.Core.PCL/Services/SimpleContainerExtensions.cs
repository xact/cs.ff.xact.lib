﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.Practices.ServiceLocation;
using XAct;
using XAct.Services;

#if CONTRACTS_FULL || NET40 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

    /// <summary>
    /// Extensions to the SimpleContainer IoC
    /// </summary>
    public static class SimpleContainerExtensions
    {
        #region Containers

        /// <summary>
        /// Creates the permanent child simpleContainer.
        /// </summary>
        /// <param name="simpleContainer">The simpleContainer.</param>
        /// <returns></returns>
        internal static SimpleContainer CreatePermanentChildContainer(this SimpleContainer simpleContainer)
        {
            return new SimpleContainer {ParentContainer = simpleContainer};
        }

        /// <summary>
        /// Creates the child simpleContainer.
        /// </summary>
        /// <param name="simpleContainer">The simpleContainer.</param>
        /// <returns></returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        internal static SimpleContainer CreateChildContainer(this SimpleContainer simpleContainer)
        {
            SimpleContainer child = simpleContainer.CreatePermanentChildContainer();
            simpleContainer.Disposing += (sender, eventArgs) => child.Dispose();
            return child;
        }

        #endregion

        #region singleton registration

        /// <summary>
        /// Registers the instance.
        /// </summary>
        /// <typeparam name="TServiceContract">The type of the service contract.</typeparam>
        /// <param name="simpleContainer">The simpleContainer.</param>
        /// <param name="serviceInstance">The service instance.</param>
        /// <param name="key">The key.</param>
        /// <param name="lifeSpanAssociatedToContainer">if set to <c>true</c> [life span associated to simpleContainer].</param>
        /// <internal><para>8/15/2011: Sky</para></internal>
        internal static void RegisterInstance<TServiceContract>(this SimpleContainer simpleContainer,
                                                              TServiceContract serviceInstance,
                                                              string key = null,
                                                              bool lifeSpanAssociatedToContainer = false)
        {
            simpleContainer.RegisterInstance(
                typeof (TServiceContract),
                serviceInstance,
                key,
                lifeSpanAssociatedToContainer);
        }

        /// <summary>
        /// Registers the instance.
        /// </summary>
        /// <param name="simpleContainer">The simpleContainer.</param>
        /// <param name="serviceContractType">Type of the service contract.</param>
        /// <param name="serviceInstance">The service instance.</param>
        /// <param name="key">The key.</param>
        /// <param name="lifeSpanAssociatedToContainer">if set to <c>true</c> [life span associated to simpleContainer].</param>
        /// <internal><para>8/15/2011: Sky</para></internal>
        internal static void RegisterInstance(this SimpleContainer simpleContainer,
                                            Type serviceContractType,
                                            object serviceInstance, string key = null,
                                            bool lifeSpanAssociatedToContainer = false)
        {
            if (lifeSpanAssociatedToContainer)
            {
                var asIDisposable = serviceInstance as IDisposable;

                if (asIDisposable != null)
                {
                    simpleContainer.Disposing += (sender, eventArgs) =>
                                               {
                                                   if (asIDisposable == null)
                                                   {
                                                       return;
                                                   }
                                                   try
                                                   {
                                                       asIDisposable.Dispose();
                                                   }
                                                   finally
                                                   {
                                                       asIDisposable = null;
                                                   }
                                               };
                }
            }

            //We create a func that returns always the same instance, and save that:
            simpleContainer.RegisterCreator(serviceContractType, (c, t, k) => serviceInstance, key);
        }

        #endregion

        #region implementing type registration

        /// <summary>
        /// Registers in the <see cref="SimpleContainer"/> 
        /// the specified Service Type against it's Contract Type.
        /// <para>
        /// Note: by default it is registered as a singleton (the same instance 
        /// returned each time, and it dependent on the lifespan of the SimpleContainer).
        /// </para>
        /// </summary>
        /// <typeparam name="TServiceContract">The type of the service contract.</typeparam>
        /// <typeparam name="TServiceImplementation">The type of the service implementation.</typeparam>
        /// <param name="simpleContainer">The simpleContainer.</param>
        /// <param name="key">The key.</param>
        /// <param name="asSingleton">if set to <c>true</c> [as singleton].</param>
        /// <param name="lifeSpanAssociatedToContainer">if set to <c>true</c> [life span associated to simpleContainer].</param>
        internal static void RegisterService<TServiceContract, TServiceImplementation>(this SimpleContainer simpleContainer,
                                                                              string key = null,
                                                                              bool asSingleton = true,
                                                                              bool lifeSpanAssociatedToContainer = true)
        {
            Type a = typeof (TServiceContract);
            Type b = typeof (TServiceImplementation);

            simpleContainer.RegisterService(a, b, key, asSingleton, lifeSpanAssociatedToContainer);
        }

        /// <summary>
        /// Registers in the <see cref="SimpleContainer"/> 
        /// the specified Service Type against it's Contract Type.
        /// <para>
        /// Note: by default it is registered as a singleton (the same instance 
        /// returned each time, and it dependent on the lifespan of the SimpleContainer).
        /// </para>
        /// </summary>
        /// <param name="simpleContainer">The simpleContainer.</param>
        /// <param name="serviceContractType">Type of the service contract.</param>
        /// <param name="serviceImplementationType">Type of the service implementation.</param>
        /// <param name="key">The key.</param>
        /// <param name="asSingleton">if set to <c>true</c> [as singleton].</param>
        /// <param name="lifeSpanAssociatedToContainer">if set to <c>true</c> [life span associated to simpleContainer].</param>
        /// <internal>8/7/2011: Sky</internal>
        internal static void RegisterService(this SimpleContainer simpleContainer,
                                    Type serviceContractType,
                                    Type serviceImplementationType,
                                    string key = null,
                                    bool asSingleton = true,
                                    bool lifeSpanAssociatedToContainer = true)
        {
            if (asSingleton)
            {
                //If we are going to register it as a singleton, 
                //a not perfect solution is to sort it out now...
                object serviceInstance
                    =
                    CompileCreatorForInstanceType(serviceImplementationType)
                        .Invoke(
                            simpleContainer,
                            serviceContractType,
                            key);

                //Unfortunately, this means it will pop if you register B before A...

                simpleContainer.RegisterInstance(serviceContractType, serviceInstance, key, lifeSpanAssociatedToContainer);

                return;
            }

            simpleContainer.RegisterCreator(serviceContractType,
                                      CompileCreatorForInstanceType(serviceImplementationType),
                                      key);
        }

        #endregion

        #region Func

        /// <summary>
        /// <para>
        /// Builds a Compiler for the given type, that uses reflection to recursively
        /// solve required arguments:
        /// </para>
        /// </summary>
        /// <param name="instanceType"></param>
        /// <returns></returns>
        /// <internal>
        /// Used to be called CompileCreatorForType.
        /// </internal>
        internal static Func<SimpleContainer, Type, string, object> CompileCreatorForInstanceType(Type instanceType)
        {
            instanceType.ValidateIsNotNull("instanceType");

            //Get the constructor of the Instance Type (eg: Service, not IService)
            ConstructorInfo[] ctors = instanceType.GetConstructors();

            if (ctors == null || ctors.Length == 0)
            {
                throw new ActivationException(
                    string.Format(
                        "Appropriate constructor not found in type '{0}'.", instanceType.FullName));
            }

            //Get the first one (is that the best policy?):
            ConstructorInfo ctor = ctors.SelectMostAppropriateConstructor();

            ParameterExpression containerParameter = Expression.Parameter(typeof (SimpleContainer), "simpleContainer");
            ParameterExpression typeParameter = Expression.Parameter(typeof (Type), "type");
            ParameterExpression keyParameter = Expression.Parameter(typeof (string), "key");

            ParameterInfo[] parameterInfo = ctor.GetParameters();

            LambdaExpression creator
                = Expression.Lambda(typeof (Func<SimpleContainer, Type, string, object>),
                                    Expression
                                        .New(ctor, parameterInfo
                                                       .Select(
                                                           p =>
                                                           (Expression)
                                                           Expression.Call(containerParameter, "GetInstance", new[]
                                                                                                                  {
                                                                                                                      p.
                                                                                                                          ParameterType
                                                                                                                  }))),
                                    containerParameter,
                                    typeParameter,
                                    keyParameter);

            Func<SimpleContainer, Type, string, object> result =
                (Func<SimpleContainer, Type, string, object>) creator.Compile();

            return result;
        }

        /// <summary>
        /// <para>
        /// Used by <see cref="CompileCreatorForInstanceType"/>
        /// </para>
        /// </summary>
        /// <param name="constructorInfos"></param>
        /// <returns></returns>
        private static ConstructorInfo SelectMostAppropriateConstructor(this ConstructorInfo[] constructorInfos)
        {
            return constructorInfos[0];
        }

        #endregion
    }
