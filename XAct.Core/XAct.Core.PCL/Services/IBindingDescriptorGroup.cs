﻿
// ReSharper disable CheckNamespace

namespace XAct.Services
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    /// A contract for a group of <see cref="IBindingDescriptor"/>
    /// elements.
    /// <para>
    /// This is loosely inspired (not as gracefully...) on Ninject's
    /// modular initialization story.
    /// </para>
    /// </summary>
    public interface IBindingDescriptorGroup : IBindingDescriptorBase
    {

        /// <summary>
        /// Gets the nested service bindings.
        /// </summary>
        List<IBindingDescriptorBase> ServiceBindings { get; }
    }
}
