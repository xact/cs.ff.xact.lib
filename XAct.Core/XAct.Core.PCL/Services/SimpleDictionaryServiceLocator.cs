﻿//using System;
//using System.Collections.Generic;
//using Microsoft.Practices.ServiceLocation;

//#if CONTRACTS_FULL || NET40 // Requires .Net 4, so hold for the moment 
//using System.Diagnostics.Contracts;
//#endif

//namespace XAct.Services
//{
//    /// <summary>
//    /// An extremely simple implementation of the microsoft P&amp;P
//    /// <see cref="ServiceLocator"/>, that uses a static dictionary
//    /// to save instances registered via the <see cref="RegisterService{TService}"/>
//    /// method.
//    /// <para>
//    /// Superceded by <see cref="SimpleContainer"/>,
//    /// but as a case study of making a ServiceLocator in less than 5 minutes...
//    /// </para>
//    /// </summary>
//    /// <internal><para>7/31/2011: Sky</para></internal>
//    [Obsolete]
//    public class SimpleDictionaryServiceLocator : ServiceLocatorImplBase
//    {
//        private static readonly Dictionary<Type, object> _instances;

//        [Obsolete]
//        static SimpleDictionaryServiceLocator()
//        {
//            _instances = new Dictionary<Type, object>();
//        }

//        /// <summary>
//        /// Registers the service.
//        /// </summary>
//        /// <typeparam name="TService"></typeparam>
//        /// <param name="instance">The instance.</param>
//        /// <internal><para>7/31/2011: Sky</para></internal>
//        [Obsolete]
//        public static void RegisterService<TService>(TService instance)
//        {
//            _instances[typeof (TService)] = instance;
//        }

//        /// <summary>
//        /// Gets the specified instance.
//        /// <para>
//        /// Important: at present, '<c>key</c>' is ignored.
//        /// </para>
//        /// </summary>
//        /// <param name="serviceType">Type of the service.</param>
//        /// <param name="key">The key.</param>
//        /// <returns></returns>
//        /// <internal><para>7/31/2011: Sky</para></internal>
//        protected override object DoGetInstance(Type serviceType, string key)
//        {
//            if (_instances.ContainsKey(serviceType))
//            {
//                return _instances[serviceType];
//            }
//            return null;
//        }

//        /// <summary>
//        /// Gets all instances matching the defined instance.
//        /// <para>
//        /// Important: at present, not supported.
//        /// </para>
//        /// </summary>
//        /// <param name="serviceType">Type of the service.</param>
//        /// <returns></returns>
//        /// <internal><para>7/31/2011: Sky</para></internal>
//        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
//        {
//            throw new NotSupportedException();
//            //return _instances.GetAll(serviceType);
//        }
//    }
//}