﻿namespace XAct.Services
{
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public abstract class ServiceBase : IHasService
    {
// ReSharper disable InconsistentNaming
        /// <summary>
        /// The tracing service
        /// </summary>
        protected readonly ITracingService _tracingService;
        /// <summary>
        /// The Type of this class -- use for logging.
        /// </summary>
        protected string _typeName ;
// ReSharper restore InconsistentNaming

        /// <summary>
        /// Initializes a new instance of the <see cref="XActLibServiceBase" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        protected ServiceBase(ITracingService tracingService)
        {
            _tracingService = tracingService;
            _typeName = this.GetType().Name;
        }
    }
}