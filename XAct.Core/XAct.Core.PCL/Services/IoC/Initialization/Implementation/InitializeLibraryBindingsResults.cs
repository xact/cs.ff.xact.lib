﻿// ReSharper disable CheckNamespace
namespace XAct.Services.IoC.Initialization
// ReSharper restore CheckNamespace

{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.IO;

#pragma warning disable 1591

#pragma warning restore 1591

    /// <summary>
    /// Class of variables returned from calling
    /// <c>XAct.Library.Initialization.Bindings.RegisterBindings</c>
    /// stored in
    /// <c>XAct.Library.Status.InitializationResults</c>
    /// </summary>
    public class InitializeLibraryBindingsResults : IInitializeLibraryBindingsResults
    {

        /// <summary>
        /// Gets or sets the binding scan results.
        /// </summary>
        /// <value>
        /// The binding scan results.
        /// </value>
        public BindingScanResults BindingScanResults { get { return _bindingScanResults ?? (_bindingScanResults = new BindingScanResults()); } }
        BindingScanResults _bindingScanResults = new BindingScanResults();

        /// <summary>
        /// 
        /// </summary>
        public bool IsInitialized { get { return _isInitialized; } set { VerifyIsLocked();_isInitialized=value; } }
        private bool _isInitialized;

        /// <summary>
        /// The time it took to load and scan all variables.
        /// </summary>
        public TimeSpan TimeToRegisterElapsed { get { return _timeToRegisterElapsed; } set { VerifyIsLocked(); _timeToRegisterElapsed = value; } }
        private TimeSpan _timeToRegisterElapsed;
        
        /// <summary>
        /// The time it took to load and scan all variables.
        /// </summary>
        public TimeSpan TotalTimeElapsed { get { return _timeElapsed; } set { VerifyIsLocked();_timeElapsed=value; } }
        private TimeSpan _timeElapsed;


        ///// <summary>The inner DependencyInjectionContainer (Unity/Ninject, etc.) -- not the CommonLocator</summary>
        //public object ServiceLocator { get { return _serviceLocator; } set { VerifyIsLocked(); _serviceLocator = value; } }
        //private object _serviceLocator;


        /// <summary>
        /// The list of Services that were initialized:
        /// </summary>
        public IBindingDescriptor[] ServiceBindingsRegistered { get { return _serviceBindingsRegistered; } set { VerifyIsLocked(); _serviceBindingsRegistered = value; } }
        private IBindingDescriptor[] _serviceBindingsRegistered;

        /// <summary>
        /// The list of Services that were skipped:
        /// </summary>
        public IBindingDescriptor[] ServiceBindingsSkipped { get { return _serviceBindingsSkipped; } set { VerifyIsLocked(); _serviceBindingsSkipped = value; } }
        private IBindingDescriptor[] _serviceBindingsSkipped;


        public IBindingDescriptor[] InstanceTypesNotBound
        {
            get { return _instanceTypesNotBound; }
            set { VerifyIsLocked(); _instanceTypesNotBound = value; }
        }
        private IBindingDescriptor[] _instanceTypesNotBound;


        /// <summary>
        /// Outputs to the <see cref="ITracingService"/> (if available)
        /// a report of the assemblies, services it registered (or skipped).
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public void TraceBindingReport(ITracingService tracingService = null)
        {
            tracingService = tracingService ??
                                       DependencyResolver.Current.GetInstance<ITracingService>(false);
            if (tracingService == null)
            {
                return;
            }

			string[] lines = CreateBindingReport();


	        foreach(string line in lines)
			{
				tracingService.Trace(TraceLevel.Verbose, line);
			}
        }

		/// <summary>
		/// Creates a report of the assemblies, services registered (or skipped).
		/// </summary>
		/// <returns></returns>
	    public string[] CreateBindingReport()
		{
		    IInitializeLibraryBindingsResults bindingResults = this;

		    List<string> lines = new List<string>();

			lines.Add("InitializeLibraryBindingResults: Report");
		    lines.Add("---------------------------------------");
            lines.Add("* Summary: ");
            lines.Add("* {0}".FormatStringInvariantCulture(bindingResults));
            lines.Add("");

            lines.Add("* Binding Initialization: Timings: ");
            lines.Add("    * Time to load Files:    " + bindingResults.BindingScanResults.TimeToLoadFilesElapsed);
            lines.Add("    * Time to scan Files:    " + bindingResults.BindingScanResults.TimeToScanElapsed);
            lines.Add("    * Time to load Register: " + bindingResults.TimeToRegisterElapsed);
            lines.Add("    * Time (Total):          " + bindingResults.TotalTimeElapsed);
            lines.Add("");

            lines.Add("* Binding Initialization: SearchPaths Used: ");
		    lines.AddRange(
                bindingResults.BindingScanResults.AssemblySearchPaths.Select(searchPath => "    * {0}".FormatStringInvariantCulture(searchPath)));
            lines.Add("");

            lines.Add("* Binding Initialization: Assemblies Scanned: ");
            string[] loadedAssemblies = bindingResults.BindingScanResults.AssemblyNames.OrderBy(n => n).ToArray();
		    lines.AddRange(loadedAssemblies.Select(loadedPath => "    * {0}".FormatStringInvariantCulture(loadedPath)));
            lines.Add("");

            //lines.Add("--InitializeLibraryBindingResults: Loaded Assemblies: ");
            //{
            //    string[] assemblyNames =
            //        AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetName().Name).OrderBy(n => n).ToArray();

            //    lines.AddRange(assemblyNames.Select(assemblyName => "----:  {0}".FormatStringInvariantCulture(assemblyName)));
            //}

            IBindingDescriptor[] registered = bindingResults.ServiceBindingsRegistered;
            lines.Add("* Binding Initialization: Bindings Registered: ");
		    {
			    lines.AddRange(registered.Select(bindingDescriptor => "    * {0}".FormatStringInvariantCulture(bindingDescriptor)));
		    }
            lines.Add("");

            IBindingDescriptor[] skipped = bindingResults.ServiceBindingsSkipped;
            lines.Add("* Binding Initialization: Bindings Skipped (already registered against another Instance Type): ");
		    {
			    lines.AddRange(skipped.Select(bindingDescriptor => "    * {0}".FormatStringInvariantCulture(bindingDescriptor)));
		    }
            lines.Add("");

            string[] instanceTypesNotBound = bindingResults.BindingScanResults.InstanceTypesNotBound;
            lines.Add("* Bindings Initialization: Bindings Skipped (Instance type found, but could not guess appropriate Interface Name): ");
		    lines.Add("  * Tip: often due to typos (HelpSevice !=> IHelpService, as missing an 'r')");
            {
                if (instanceTypesNotBound != null)
                {
                    lines.AddRange(instanceTypesNotBound.Select(s => "    * {0}".FormatStringInvariantCulture(s)));
                }
            }
            lines.Add("");
            
            return lines.ToArray();
	    }


        //private DumpBindingReport()
        //{

        //    string[] bindingReport = XAct.DependencyResolver.BindingResults.CreateBindingReport();


        //    string directoryFullName =
        //        XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().ApplicationBasePath;

        //    do
        //    {
        //        directoryFullName = 
        //            XAct.DependencyResolver.Current.GetInstance<IPathService>().GetDirectoryName(directoryFullName);
        //    } 
        //    while (directoryFullName.Contains("\\bin", StringComparison.CurrentCultureIgnoreCase));
            
        //    string fullName =
        //        XAct.DependencyResolver.Current.GetInstance<IPathService>().Combine(directoryFullName, "bindingReport.txt");


        //   XAct.DependencyResolver.Current.GetInstance<IIOService>().FileOpenWriteAsync().WaitAndGetResult().AppendAllText(););););
        //    System.IO.File.WriteAllLines(fullName, bindingReport);
        

        //}


	    private void VerifyIsLocked()
        {
            if (_isInitialized) {throw new ArgumentException("Cannot update values once initialized/Locked.");}
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return
                "Took {0}ms to find (in {1} assemblies) {2} services (skipping {3}). PermissionToScanDir:{4}".FormatStringCurrentUICulture(
                    _timeElapsed.TotalMilliseconds.ToString(),
                    (BindingScanResults.AssemblyNames == null) ? "?" : BindingScanResults.AssemblyNames.Length.ToString(), 
                    (_serviceBindingsRegistered==null)?"?":_serviceBindingsRegistered.Length.ToString(),
                    (_serviceBindingsSkipped==null)?"?":_serviceBindingsSkipped.Length.ToString(),
                    BindingScanResults.HadPermissionsToScanDir
                    );
        }
    }
}