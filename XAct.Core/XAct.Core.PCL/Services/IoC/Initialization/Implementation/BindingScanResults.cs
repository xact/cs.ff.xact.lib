﻿namespace XAct.Services.IoC.Initialization
{
    using System;

    /// <summary>
    /// An object containing the results of a complete
    /// scan for Binding information.
    /// <para>
    /// The difference between <see cref="BindingScanResults"/>
    /// and <see cref="InitializeLibraryBindingsResults"/>
    /// is that <see cref="InitializeLibraryBindingsResults"/>
    /// is the results of registering the Bindings, whereas
    /// <see cref="IBindingScanResults"/> is just the information
    /// of scanning, beforehand (<see cref="InitializeLibraryBindingsResults"/>
    /// needs <see cref="IBindingScanResults"/>, not the other way around).
    /// </para>
    /// </summary>
    public class BindingScanResults : IBindingScanResults
    {
        public bool HadPermissionsToScanDir { get; set; }
        public string[] AssemblySearchPaths { get; set; }
        public TimeSpan TimeToLoadFilesElapsed { get; set; }
        public string[] AssemblyNames { get; set; }
        public TimeSpan TimeToScanElapsed { get; set; }
        public IBindingDescriptor[] BindingDescriptors { get; set; }
        public bool UsedTempAppDomain { get; set; }
        
        /// <summary>
        /// The list of Services that were skipped due to finding
        /// Implementation, but being able to guess Interface to register
        /// it under (happens only if not being configured using Attir:
        /// </summary>
        public string[] InstanceTypesNotBound { get; set; }

        public BindingScanResults()
        {
            AssemblySearchPaths = new string[0];
            AssemblyNames = new string[0];
            BindingDescriptors = new IBindingDescriptor[0];
            InstanceTypesNotBound = new string[0];
        }
    }
}