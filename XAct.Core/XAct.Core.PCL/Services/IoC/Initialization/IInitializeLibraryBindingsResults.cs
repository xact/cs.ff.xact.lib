﻿namespace XAct.Services.IoC.Initialization
{
    using System;

    /// <summary>
    /// Contract for the response message returned by
    /// <c>XAct.Library.Bindings.Initialize</c>
    /// </summary>
    public interface IInitializeLibraryBindingsResults 
    {
        /// <summary>
        /// Gets or sets the binding scan results.
        /// </summary>
        /// <value>
        /// The binding scan results.
        /// </value>
        BindingScanResults BindingScanResults { get;  }

        /// <summary>
        /// 
        /// </summary>
        bool IsInitialized { get; set; }


        /// <summary>
        /// The time it took to load and scan all variables.
        /// </summary>
        TimeSpan TimeToRegisterElapsed { get; set; }


        /// <summary>
        /// The time it took to load and scan all variables.
        /// </summary>
        TimeSpan TotalTimeElapsed { get; set; }




        /// <summary>
        /// The list of Services that were initialized:
        /// </summary>
        IBindingDescriptor[] ServiceBindingsRegistered { get; set; }

        /// <summary>
        /// The list of Services that were skipped:
        /// </summary>
        IBindingDescriptor[] ServiceBindingsSkipped { get; set; }


		///// <summary>
		///// Outputs to the <see cref="ITracingService"/> (if available)
		///// a report of the assemblies, services it registered (or skipped).
		///// </summary>
		///// <param name="tracingService">The tracing service.</param>
		//void TraceBindingReport(ITracingService tracingService = null);

		/// <summary>
		/// Creates a report of the assemblies, services registered (or skipped).
		/// </summary>
		/// <returns></returns>
	    string[] CreateBindingReport();

    }
}