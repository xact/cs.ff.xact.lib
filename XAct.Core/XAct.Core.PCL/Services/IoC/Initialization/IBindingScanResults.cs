﻿namespace XAct.Services.IoC.Initialization
{
    using System;

    /// <summary>
    /// Contract for an object containing the results of a complete
    /// scan for Binding information.
    /// <para>
    /// The difference between <see cref="BindingScanResults"/>
    /// and <see cref="InitializeLibraryBindingsResults"/>
    /// is that <see cref="InitializeLibraryBindingsResults"/>
    /// is the results of registering the Bindings, whereas
    /// <see cref="IBindingScanResults"/> is just the information
    /// of scanning, beforehand (<see cref="InitializeLibraryBindingsResults"/>
    /// needs <see cref="IBindingScanResults"/>, not the other way around).
    /// </para>
    /// </summary>
    public interface IBindingScanResults
    {
        /// <summary></summary>
        bool UsedTempAppDomain { get; }

        /// <summary>
        /// The time it took to load assemblies to scan.
        /// </summary>
        TimeSpan TimeToLoadFilesElapsed { get; }

        /// <summary>
        /// The time it took to load and scan all variables.
        /// </summary>
        TimeSpan TimeToScanElapsed { get; }


        /// <summary></summary>
        bool HadPermissionsToScanDir { get; }

        /// <summary></summary>
        string[] AssemblySearchPaths { get; }

        /// <summary></summary>
        string[] AssemblyNames { get; }

        /// <summary>
        /// Gets the found binding descriptors.
        /// </summary>
        /// <value>
        /// The binding descriptors.
        /// </value>
        IBindingDescriptor[] BindingDescriptors { get; }


        /// <summary>
        /// The list of Services that were skipped due to finding
        /// Implementation, but being able to guess Interface to register
        /// it under (happens only if not being configured using Attir:
        /// </summary>
        string[] InstanceTypesNotBound { get; set; }


    }
}