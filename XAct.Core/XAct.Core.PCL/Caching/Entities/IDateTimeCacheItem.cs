﻿namespace XAct.Caching
{
    using System;

    /// <summary>
    /// The contract for a time based cached item.
    /// <para>
    /// See <see cref="ICachingService"/>.
    /// </para>
    /// </summary>
    /// <typeparam name="TData">The type of the data.</typeparam>
    /// <internal><para>5/8/2011: Sky</para></internal>
    public interface IDateTimeCacheItem<TData> : ICacheItem<TData>
    {
        /// <summary>
        /// Gets the key.
        /// </summary>
        string Key { get; }

        /// <summary>
        /// Gets or sets the expires on.
        /// </summary>
        /// <value>The expires on.</value>
        DateTime ExpiresOn { get; }
    }
}