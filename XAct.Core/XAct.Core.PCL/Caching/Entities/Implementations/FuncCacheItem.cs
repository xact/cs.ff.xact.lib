﻿namespace XAct.Caching
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public class FuncCacheItem<TData> : ICacheItem<TData>
    {
        /// <summary>
        /// Gets the <see cref="Valid"/> delegate.
        /// </summary>
        public Func<bool> ValidDelegate { get; protected set; }

        /// <summary>
        /// Gets the data factory delegate.
        /// </summary>
        public Func<TData> DataFactoryDelegate { get; protected set; }




        /// <summary>
        /// Initializes a new instance of the <see cref="FuncCacheItem&lt;TData&gt;"/> class.
        /// </summary>
        /// <param name="isValid">The is valid.</param>
        /// <param name="dataFactory">The data factory.</param>
        public FuncCacheItem(Func<TData> dataFactory, Func<bool> isValid)
        {
            isValid.ValidateIsNotDefault("isValid");
            dataFactory.ValidateIsNotDefault("dataFactory");

            ValidDelegate = isValid;
            DataFactoryDelegate = dataFactory;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public TData Value
        {
            get
            {
                if (!_dataFetched)
                {
                    _value = DataFactoryDelegate.Invoke();
                    _dataFetched=true;
                }
                return _value;
            }
        }
        private TData _value;

        /// <summary>
        /// Resets this instance so that next call to Value re-evaluates the method.
        /// </summary>
        protected bool _dataFetched;

        /// <summary>
        /// Gets a value indicating whether this entity is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if valid; otherwise, <c>false</c>.
        /// </value>
        public bool Valid
        {
            get
            {
                bool valid = ValidDelegate.Invoke();
                return valid;
            }

        }



    }
}
