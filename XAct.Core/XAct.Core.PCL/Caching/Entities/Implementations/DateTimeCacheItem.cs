﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace XAct.Caching
{
    using System;
    using XAct.Environment;

    /// <summary>
    /// The default (time based) cached item container.
    /// <para>
    /// Used by <see cref="ICachingService"/>
    /// (whether implementing an Http or non Http based solution).
    /// </para>
    /// </summary>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public class DateTimeCacheItem<TData> : FuncCacheItem<TData>
    {

        #region IDateTimeCacheItem<TData> Members

        /// <summary>
        /// Gets or sets the expires on.
        /// </summary>
        /// <value>The expires on.</value>
        public DateTime ExpiresOn { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeCacheItem{TData}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="expiresOn">The expires on.</param>
        public DateTimeCacheItem(TData value, DateTime expiresOn)
            : base(
                () => (value),
                () => (DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC < expiresOn.ToUniversalTime())
            )

        {
            ExpiresOn = expiresOn;
        }

        #endregion
    }
}