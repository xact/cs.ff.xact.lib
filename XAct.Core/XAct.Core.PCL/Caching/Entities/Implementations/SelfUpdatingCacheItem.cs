﻿namespace XAct.Caching
{
    using System;
    using XAct.Environment;

    /// <summary>
    /// A Self contained Cached Item that knows how to refresh itself.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The idea is to get rid of the usual 
    /// repetivive
    /// Check, Refresh, Return code, and 
    /// put it all in one object.
    /// </para>
    /// <para>
    /// <code>
    /// Set one up:
    /// <![CDATA[
    ///   IHasValue<ClientTypes>
    ///   _cachedClientTypes = ISelfUpdatingCacheItem<ClientTypes>(
    ///     _environmentService,
    ///     _cachingService,
    ///     "SomeUniqueKeyAboutClientTypes",
    ///     GetClientTypes());
    /// ]]>
    /// </code>
    /// Later, just get the value:
    /// <code>
    /// <![CDATA[
    /// var freshResults = _cachedClientTypes.Values;
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    /// <internal>
    /// Picked up this idea from Carl W. 
    /// <para>
    /// Not sure what I think...yet. 
    /// </para>
    /// </internal>
    public class SelfUpdatingCacheItem<TItem> : FuncCacheItem<TItem>
    {

        /// <summary>
        /// Gets a value indicating whether this CacheItem is self updating.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [self updating]; otherwise, <c>false</c>.
        /// </value>
        public bool SelfUpdating { get; private set; }

        /// <summary>
        /// The amount of time the cache is kept.
        /// <para>
        /// When set, it clears and refreshes the cached item.
        /// </para>
        /// </summary>
        public TimeSpan TimeSpan { get; private set; }



        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="SelfUpdatingCacheItem{TItem}"/> class.
        /// </summary>
        public SelfUpdatingCacheItem(
            Func<TItem> dataFactory,
            TimeSpan timeSpan,
            DateTime invalidateUTCDateTime,
            bool selfUpdating = false)
            : base(dataFactory, () => (DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC < invalidateUTCDateTime.ToUniversalTime()))
        {
            SelfUpdating = selfUpdating;
            TimeSpan = timeSpan;
        }



        /// <summary>
        /// Resets this instance so that next call to Value re-evaluates the method.
        /// </summary>
        public void Reset(Func<bool> isValid)
        {
            base.ValidDelegate = isValid;
            base._dataFetched = false;
        }

    }
}
