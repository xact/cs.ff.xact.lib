﻿namespace XAct.Caching
{
  

    /// <summary>
    /// A contract for a cached item.
    /// </summary>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public interface ICacheItem<TData> : IHasValid
    {
        /// <summary>
        /// Gets the cached value.
        /// <para>
        /// Can be null.
        /// </para>
        /// </summary>
        /// <value>The value.</value>
        TData Value { get; }

    }
}