﻿
namespace XAct.Caching
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Cross environment (Web/Desktop) caching was not available 
    /// prior to .NET40. 
    /// <para>
    /// Not all projects have the luxury of using NET40 (and the System.Caching namespace is not available on all plaforms).
    /// </para>
    /// <para>
    /// This namespace solves that by providing
    /// </para>
    /// </summary>
    /// <internal>
    /// This private class is required by Sandcastle to document Namespaces: http://bit.ly/vqzRiK
    /// </internal>
    [CompilerGenerated]
    class NamespaceDoc { }
}
