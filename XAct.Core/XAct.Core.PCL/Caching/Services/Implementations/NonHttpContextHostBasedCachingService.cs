﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace XAct.Caching.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    using XAct.Environment;

    /// <summary>
    /// A Caching Service that does not rely on the IIS based solution.
    /// </summary>
    public class NonHttpContextCachingService : ICachingService
    {
        //This is our cache (equivalent of System Web cache):
        private readonly IDictionary<string, IHasValid> _items = new Dictionary<string, IHasValid>();
        private readonly ITracingService _tracingService;
        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NonHttpContextCachingService" /> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <internal>5/11/2011: Sky</internal>
        public NonHttpContextCachingService(ITracingService tracingService,
            IDateTimeService dateTimeService,
            IEnvironmentService environmentService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            environmentService.ValidateIsNotDefault("environmentService");

            _tracingService = tracingService;
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
        }

        #endregion

        #region Implementation of ICachingService

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache"></param>
        /// <param name="isValid">The is valid.</param>
        public void Set<TData>(string key, TData objectToCache, Func<bool> isValid)
        {
            key.ValidateIsNotDefault("key");
            isValid.ValidateIsNotDefault("isValid");

            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Add({0},{1})".FormatStringCurrentCulture(key, objectToCache));

            FuncCacheItem<TData> funcCacheItem =
                new FuncCacheItem<TData>(() => (objectToCache), isValid);

            lock (_items)
            {
                //Unlike the Http Based solution -- where we are using Add, here we are just index setting
                //So don't have to remove first, etc:
                _items[key] = funcCacheItem;
            }
        }

        /// <summary>
        /// Caches the given value for the specified time.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="expiryTimespan">The timespan to cache data for.</param>
        public void Set<TData>(string key, TData objectToCache, TimeSpan expiryTimespan)
        {
            key.ValidateIsNotDefault("key");
            //Do not raise exception if expiryTimespan = emtpy.

            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Add({0},{1})".FormatStringCurrentCulture(key, objectToCache));

            DateTime futureUtcDateTime = _dateTimeService.NowUTC.Add(expiryTimespan);

            FuncCacheItem<TData> funcCacheItem =
                new FuncCacheItem<TData>(
                    () => (objectToCache),
                    () => (DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC < futureUtcDateTime)
                    );

            lock (_items)
            {
                //Unlike the Http Based solution -- where we are using Add, here we are just index setting
                //So don't have to remove first, etc:
                _items[key] = funcCacheItem;
            }
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        public void Set<TData>(string key, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true)
        {
            key.ValidateIsNotDefault("key");
            dataFactory.ValidateIsNotDefault("dataFactory");
            //Do not raise exception if expiryTimespan = emtpy.

            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Add({0})".FormatStringCurrentCulture(key));

            DateTime futureUtcDateTime = _dateTimeService.NowUTC.Add(expiryTimespan);

            SelfUpdatingCacheItem<TData> funcCacheItem =
                new SelfUpdatingCacheItem<TData>(
                    dataFactory,
                    expiryTimespan,
                    futureUtcDateTime,
                    selfUpdating);

            lock (_items)
            {
                //Unlike the Http Based solution -- where we are using Add, here we are just index setting
                //So don't have to remove first, etc:
                _items[key] = funcCacheItem;
            }
        }


        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <returns>True if found, and valid.</returns>
        public bool TryGet<TData>(string key, out TData result)
        {
            key.ValidateIsNotDefault("key");
            _tracingService.Trace(TraceLevel.Verbose, "TracingService.TryGet({0})".FormatStringCurrentCulture(key));

// ReSharper disable RedundantArgumentDefaultValue
            return InternalTryGet(key, out result,null,TimeSpan.Zero,false);
// ReSharper restore RedundantArgumentDefaultValue
        }


        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// If not found, registers the datafactory, and returns its results.
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns></returns>
        public bool TryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true)
        {
            key.ValidateIsNotDefault("key");
            dataFactory.ValidateIsNotDefault("dataFactory");
            //Do not raise exception if expiryTimespan = emtpy.

            _tracingService.Trace(TraceLevel.Verbose, "TracingService.TryGet({0})".FormatStringCurrentCulture(key));


            return InternalTryGet(key, out result, dataFactory, expiryTimespan, selfUpdating);
        }


        private bool InternalTryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = false)
        {

            IHasValid t;

            _items.TryGetValue(key,out t);

            ICacheItem < TData > item = t as ICacheItem<TData>; 
            if (item == null)
            {
                if (dataFactory !=null)
                {
                    this.Set(key,dataFactory,expiryTimespan,selfUpdating);
                    return TryGet(key, out result);
                }

                result = default(TData);
                return false;
            }


            if (!item.Valid)
            {
                SelfUpdatingCacheItem<TData> tmp = item as SelfUpdatingCacheItem<TData>;

                if ((tmp == null) || (!tmp.SelfUpdating))
                {
                    //It wasn't valid, and 
                    //It's wasn't a SelfUpdatingCacheItem<T> (it was a FuncCacheItem<T>)
                    //and it's not self updating (atleast the original insert wasn't)
                    //But do we have enough info (now, this time, compared to when first set)
                    //as to turn it self updatingCacheItem?

                    //It wasn't valid, and it's not self updating...so remove:
                    Remove(key);

                    if (!selfUpdating)
                    {
                        //And we still are not self-updating:
                        result = default(TData);
                        return false;
                    }


                    //which means that now that it is clear/Removed, 
                    //we can now teach it how to get the value, and be on our merry way:

                    //Set the dataFactory:
                    this.Set(key, dataFactory, expiryTimespan, selfUpdating);
                    //And then try to get it, which will set the key:
                    return TryGet(key, out result);

                }


                DateTime futureUtcDateTime = _dateTimeService.NowUTC.Add(tmp.TimeSpan);

                item = new SelfUpdatingCacheItem<TData>(tmp.DataFactoryDelegate, 
                                                        tmp.TimeSpan,
                                                        futureUtcDateTime,
                                                        tmp.SelfUpdating);
            }

            result = item.Value;
            return true;
        }


        /// <summary>
        /// Removes the cached item that has the given key name.
        /// </summary>
        /// <param name="key">The key.</param>
        public void Remove(string key)
        {
            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Remove({0})".FormatStringCurrentCulture(key));
            lock (_items)
            {
                _items.Remove(key);
            }
        }

        /// <summary>
        /// Clears out all cached items.
        /// </summary>
        public void Clear(string prefix=null)
        {
            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Clear()");

            lock (_items)
            {
                if (prefix.IsNullOrEmpty())
                {

                    _items.Clear();
                }
                else
                {
                    List<string> keysToDelete = new List<string>();
                    foreach (string key in _items.Keys.Where(key => key.StartsWith(prefix)))
                    {
                        keysToDelete.Add(key);
                    }
                    //Do not delete while in an iteration
                    foreach (string key in keysToDelete)
                    {
                        _items.Remove(key);
                    }
                }
            }
        }

        #endregion


    }
}