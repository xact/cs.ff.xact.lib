﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

namespace XAct.Caching.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    /// <internal><para>5/11/2011: Sky</para></internal>
    public class CachingService : ICachingService
    {

        private readonly ICachingService _cachingService;
        private readonly ICachingService _nonWebCachingService;
        private readonly IEnvironmentService _environmentService;
        private readonly IDateTimeService _dateTimeService;
        private readonly ITracingService _tracingService;


        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CachingService" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="tracingService">The logging service.</param>
        /// <internal>5/11/2011: Sky</internal>
        public CachingService(IEnvironmentService environmentService, IDateTimeService dateTimeService, ITracingService tracingService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            environmentService.ValidateIsNotDefault("environmentService");
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            _tracingService = tracingService;
            _environmentService = environmentService;
            _dateTimeService = dateTimeService;

            _nonWebCachingService =
    new NonHttpContextCachingService(tracingService, _dateTimeService, environmentService);

            if (IsWebEnvironment)
            {//If the XAct.UI.Web assembly is loaded, then there will be an implementation
                //to use ...otherwise fallback to the slower HttpBased solution.
                IHttpContextCachingService httpContextCachingService =
                    DependencyResolver.Current.GetInstance<IHttpContextCachingService>();

                _cachingService = httpContextCachingService;


            }
            else
            {
                //If not web, use a static cache based solution:
                _cachingService = _nonWebCachingService;

            }

            _tracingService.Trace(0, TraceLevel.Verbose,
                                  "ICachingService generated:{0}".FormatStringCurrentCulture(
                                      _cachingService.GetType().Name));
        }

        #endregion

        #region Implementation of ICachingService

        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        /// <value></value>
        /// <internal><para>5/11/2011: Sky</para></internal>
        public bool TryGet<TData>(string key, out TData result)
        {
            //Every call is just a wrapper to the inner solution chosen above:
            var r =
                this.GetCachingService().TryGet(key, out result);
            return r;
        }

        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// If not found, registers the datafactory, and returns its results.
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns></returns>
        public bool TryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true)
        {
            key.ValidateIsNotNullOrEmpty("key");
            dataFactory.ValidateIsNotDefault("dataFactory");


            //Note: not going to turn back on 0 timespan (eg: appsettings may not be setup yet)
            //...just a waste of resources.
            var r =
                this.GetCachingService().TryGet(key, out result, dataFactory, expiryTimespan, selfUpdating);
            return r;
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache"></param>
        /// <param name="isValid">The is valid.</param>
        public void Set<TData>(string key, TData objectToCache, Func<bool> isValid)
        {
            key.ValidateIsNotNullOrEmpty("key");
            isValid.ValidateIsNotDefault("isValid");

            this.GetCachingService().Set(key, objectToCache, isValid);
        }

        /// <summary>
        /// Caches the given value for the specified time.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="expiryTimespan">The timespan to cache data for.</param>
        public void Set<TData>(string key, TData objectToCache, TimeSpan expiryTimespan)
        {
            key.ValidateIsNotNullOrEmpty("key");
            //Do not raise exception if expiryTimespan = empty.
            this.GetCachingService().Set(key, objectToCache, expiryTimespan);
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        public void Set<TData>(string key, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true)
        {
            key.ValidateIsNotNullOrEmpty("key");
            dataFactory.ValidateIsNotDefault("dataFactory");

            this.GetCachingService().Set(key, dataFactory, expiryTimespan, selfUpdating);
        }




        /// <summary>
        /// Removes the cached item that has the given key name.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <internal><para>5/11/2011: Sky</para></internal>
        public void Remove(string key)
        {
            key.ValidateIsNotNullOrEmpty("key");

            this.GetCachingService().Remove(key);
        }

        /// <summary>
        /// Clears out all cached items.
        /// </summary>
        /// <internal><para>5/11/2011: Sky</para></internal>
        public void Clear(string prefix = null)
        {
            this.GetCachingService().Clear(prefix);
        }

        #endregion

        #region Private


        /// <summary>
        /// Gets a value indicating whether the current environement is an http/iis based one.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is web environment; otherwise, <c>false</c>.
        /// </value>
        /// <internal><para>5/11/2011: Sky</para></internal>
        private bool IsWebEnvironment
        {
            get
            {
                return (_environmentService.HttpContext != null);
            }
        }

        #endregion


        private ICachingService GetCachingService()
        {

            //Reset:
            //return _cachingService;


            ICachingService CachingService =
                (IsWebEnvironment)
                ? (ICachingService)XAct.DependencyResolver.Current.GetInstance<IHttpContextCachingService>()
                : _nonWebCachingService;

            return CachingService;
        }




    }
}