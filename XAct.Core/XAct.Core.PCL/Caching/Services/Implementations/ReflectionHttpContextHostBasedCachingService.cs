﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

namespace XAct.Caching.Implementations
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An HttpContext based Caching Service.
    /// <para>
    /// Not as powerful as the newer .NET 4.0 based solution,
    /// but good enough for 99% of scenarios.
    /// </para>
    /// </summary>
    /// <internal>
    /// IMPORTANT: Do NOT bind this implemenation to IHttpContextCachingService
    /// so that if any implementation is imported from XAct.Web, 
    /// to use that instead.
    /// </internal>
    public class ReflectionHttpContextCachingService : IHttpContextCachingService
    {
        #region Services

        private readonly IEnvironmentService _environmentService;
        private readonly ITracingService _tracingService;
        private readonly IDateTimeService _dateTimeService;

        #endregion



        private MethodInfo _addItemMemberInfo;
        private MethodInfo _getEnumeratorMemberInfo;
        private MethodInfo _getItemMemberInfo;
        private object _httpContextCache;
        private MethodInfo _removeItemMemberInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReflectionHttpContextCachingService" /> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <internal>5/11/2011: Sky</internal>
        public ReflectionHttpContextCachingService(ITracingService tracingService, 
            IDateTimeService dateTimeService, IEnvironmentService environmentService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            environmentService.ValidateIsNotDefault("environmentService");
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            _tracingService = tracingService;
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
        }

        #region IMplementation of ICacheService















        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        /// <value></value>
        /// <internal><para>5/11/2011: Sky</para></internal>
        public bool TryGet<TData>(string key, out TData result)
        {
            key.ValidateIsNotDefault("key");

            _tracingService.Trace(TraceLevel.Verbose, "TracingService.TryGet({0})".FormatStringCurrentCulture(key));

            // ReSharper disable RedundantArgumentDefaultValue
            return InternalTryGet(key, out result,null,TimeSpan.Zero, false);
// ReSharper restore RedundantArgumentDefaultValue
        }
        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// If not found, registers the datafactory, and returns its results.
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns></returns>
        public bool TryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true)
        {
            key.ValidateIsNotDefault("key");
            dataFactory.ValidateIsNotDefault("dataFactory");
            //Do not raise exception if expiryTimespan = emtpy.
            _tracingService.Trace(TraceLevel.Verbose, "TracingService.TryGet({0})".FormatStringCurrentCulture(key));

            return InternalTryGet(key, out result, dataFactory, expiryTimespan, selfUpdating);
        }

        private bool InternalTryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = false)
        {
            object unTypedObject = GetItemByReflection(key);

            if (unTypedObject == null)
            {
                //nothing found, so maybe nothing else to do:
                if (dataFactory != null)
                {
                    //Set the dataFactory:
                    Set(key, dataFactory, expiryTimespan, selfUpdating);

                    //And then try to get it, which will set the key:
                    return TryGet(key, out result);
                }

                //It was null, but there was no data factory,
                //so just return default/null value:
                result = default(TData);
                return false;
            }

            //There was something in cache, 
            //and it theoretically should be one of our fancy ICacheItem<T>
            //objects.

            //Try to convert it to one of our objects:
            ICacheItem<TData> cacheItem = unTypedObject as ICacheItem<TData>;

            if (cacheItem == null)
            {
                //I guess someone else added something, they now want, through us
                //Sure doesn't seem to have been added by us, as it would have
                //been a derivative of ICacheItem:
                result = (TData)unTypedObject;
                //Say we found something even if it wasn't us who put it in in the first place:
                return true;
            }
            //It was added by us, but is no longer valid:
            if (!cacheItem.Valid)
            {
                SelfUpdatingCacheItem<TData> tmp = cacheItem as SelfUpdatingCacheItem<TData>;


                if ((tmp == null) || (!tmp.SelfUpdating))
                {
                    //It wasn't valid, and 
                    //It's wasn't a SelfUpdatingCacheItem<T> (it was a FuncCacheItem<T>)
                    //and it's not self updating (atleast the original insert wasn't)
                    //But do we have enough info (now, this time, compared to when first set)
                    //as to turn it self updatingCacheItem?

                    //It wasn't valid, and it's not self updating...so remove:
                    Remove(key);

                    if (!selfUpdating)
                    {
                        //And we still are not self-updating:
                        result = default(TData);
                        return false;
                    }


                    //which means that now that it is clear/Removed, 
                    //we can now teach it how to get the value, and be on our merry way:

                    //Set the dataFactory:
                    this.Set(key, dataFactory, expiryTimespan, selfUpdating);
                    //And then try to get it, which will set the key:
                    return TryGet(key, out result);

                }


                //Was self updating, so calc a new future date:
                DateTime futureUtcDateTime = _dateTimeService.NowUTC.Add(tmp.TimeSpan);

                //Reset with new Future Date:
                tmp.Reset(() => (DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC < futureUtcDateTime));
            }

            //Get Value. Note that if just RReinvoke, to refresh data:
            result = cacheItem.Value;
            return true;
        }




        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache"></param>
        /// <param name="isValid">The is valid.</param>
        public void Set<TData>(string key, TData objectToCache, Func<bool> isValid)
        {
            key.ValidateIsNotDefault("key");
            isValid.ValidateIsNotDefault("isValid");
            //Do not raise exception if expiryTimespan = emtpy.

            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Add({0},{1})".FormatStringCurrentCulture(key, objectToCache));

            //The default behaviour is to save just the value.
            //We do it differently: we save a whole object, that
            //has a delegate backing an IsValid property:
            FuncCacheItem<TData> funcCacheItem =
                new FuncCacheItem<TData>(() => (objectToCache), isValid);

            Insert(key,funcCacheItem);
        }

        /// <summary>
        /// Caches the given value for the specified time.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="expiryTimespan">The timespan to cache data for.</param>
        public void Set<TData>(string key, TData objectToCache, TimeSpan expiryTimespan)
        {
            key.ValidateIsNotDefault("key");
            //Do not raise exception if expiryTimespan = emtpy.

            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Add({0},{1})".FormatStringCurrentCulture(key, objectToCache));

            DateTime futureUtcDateTime = _dateTimeService.NowUTC.Add(expiryTimespan);

            //The default behaviour is to save just the value.
            //We do it differently: we save a whole object, that
            //has a delegate backing an IsValid property:
            FuncCacheItem<TData> funcCacheItem =
                new FuncCacheItem<TData>(
                    () => (objectToCache), 
                    () => (DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC < futureUtcDateTime)
                    );

            Insert(key,funcCacheItem);
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        public void Set<TData>(string key, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true)
        {
            key.ValidateIsNotDefault("key");
            //Do not raise exception if expiryTimespan = emtpy.
            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Add({0})".FormatStringCurrentCulture(key));

            DateTime futureUtcDateTime = _dateTimeService.NowUTC.Add(expiryTimespan);

            //Instead of using a FuncCacheItem which will only encapsulate a bool,
            //use a SelfUpdatingCacheItem which also encapsulates TimeSpan, 
            //so that when expired, can relaunch itself if need be:
            SelfUpdatingCacheItem<TData> funcCacheItem =
                new SelfUpdatingCacheItem<TData>(
                    dataFactory,
                    expiryTimespan,
                    futureUtcDateTime,
                    selfUpdating);

            Insert(key, funcCacheItem);
        }

        /// <summary>
        /// Removes the cached item that has the given key name.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <internal><para>5/11/2011: Sky</para></internal>
        public void Remove(string key)
        {
            key.ValidateIsNotDefault("key");

            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Remove({0})".FormatStringCurrentCulture(key));

            if (key.IsNullOrEmpty())
            {
                throw new ArgumentNullException("key");
            }
            RemoveItemByReflection(key);

        }

        /// <summary>
        /// Clears out all cached items.
        /// </summary>
        /// <internal><para>5/11/2011: Sky</para></internal>
        public void Clear(string prefix=null)
        {
            _tracingService.Trace(TraceLevel.Verbose, "TracingService.Clear()".FormatStringCurrentCulture());

            lock (HttpContextCache)
            {
                IDictionaryEnumerator dictionaryEnumerator = GetEnumeratorByReflection();
                List<string> keys = new List<string>();
                while (dictionaryEnumerator.MoveNext())
                {
                    string key = dictionaryEnumerator.Key as string;
                    if ((prefix == null)||(key.StartsWith(prefix)))
                    {
                    keys.Add(key);
                    }
                }
                foreach (string key in keys)
                {
                    Remove(key);
                }

            }
        }

        #endregion


        private object HttpContextCache
        {
            get
            {
                if (_httpContextCache != null)
                {
                    return _httpContextCache;
                }

                object httpContext = _environmentService.HttpContext;
                Type httpContextType = httpContext.GetType();

                //IMPORTANT: There is one instance of the Cache class per application domain. 
                //As a result, the Cache object that is returned by the Cache property is 
                //the Cache object for all requests in the application domain.
                PropertyInfo propertyInfo = httpContextType.GetProperty("Cache",
                                                                        BindingFlags.Public | BindingFlags.Instance);

                _httpContextCache = propertyInfo.GetValue(httpContext, null);

                return _httpContextCache;
            }
        }


        /// <summary>
        /// Caches the specified item.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="cacheItem">The cache item.</param>
        /// <internal><para>5/11/2011: Sky</para></internal>
        private void Insert<TData>(string key, ICacheItem<TData> cacheItem)
        {

            if (cacheItem.Valid)
            {
                //THis is to allow for when timespan is set to 0...maybe due to someone forgetting to enter a time in AppSettings.
                _tracingService.Trace(TraceLevel.Verbose,
                                      "Adding {0} to cache even if already invalid".FormatStringCurrentCulture(key));
            }

            //IMPORTANT: Found out that in .NET45 (and maybe this was always the case)
            //that Add does not add to the cache if there was already one (!) 
            //But that it also doesn't complain if you request to Remove an object from 
            //cache, even if the item was not yet in cache.
            //So, you can either Remove it first
            //HttpContext.Current.Cache.Remove(key);
            //Or instead Insert.
            //I Prefer Insert.

            //Notice that we are not using the default anemic solution, 
            //and storing not just the value, but the whole cache item,
            //with a very future date.
            //Expiration is checked not against the Cache system, 
            //but against the ICacheItem delegate.
            //If expired, and not self updating, it removes itself:
            //HttpContext.Current.Cache.Add(key, cacheItem, null, DateTime.MaxValue, new TimeSpan(0,0,0,0),CacheItemPriority.Normal, null);
            //Clear in case it was already there:

            //Notice that we are not using the default anemic solution, 
            //and storing not just the value, but the whole cache item:
            InsertToCacheByReflection(key, cacheItem, DateTime.MaxValue);

        }


        private void InsertToCacheByReflection(string key, object value, DateTime absoluteExpirationDate)
        {
            object httpContextCache = HttpContextCache;
            if (_addItemMemberInfo == null)
            {
                Type httpContextCacheType = httpContextCache.GetType();

                Type x1 = httpContextCacheType.Assembly.GetType("System.Web.Caching.CacheDependency");
                Type x2 = httpContextCacheType.Assembly.GetType("System.Web.Caching.CacheItemPriority");
                Type x3 = httpContextCacheType.Assembly.GetType("System.Web.Caching.CacheItemRemovedCallback");

                Type [] types = new Type[]{typeof(string),typeof(object),x1,typeof(DateTime),x2,x3};

                _addItemMemberInfo = httpContextCacheType.GetMethod("Insert", types /*BindingFlags.Public | BindingFlags.Instance*/);
            }
            _addItemMemberInfo.Invoke(httpContextCache,
                                      new[] { key, value, null, absoluteExpirationDate, new TimeSpan(0, 0, 0, 0), 3, null });
        }

        private void AddToCacheByReflection(string key, object value, DateTime absoluteExpirationDate)
        {
            object httpContextCache = HttpContextCache;
            if (_addItemMemberInfo == null)
            {
                Type httpContextCacheType = httpContextCache.GetType();
                Type x1 = httpContextCacheType.Assembly.GetType("System.Web.Caching.CacheDependency");
                Type x2 = httpContextCacheType.Assembly.GetType("System.Web.Caching.CacheItemPriority");
                Type x3 = httpContextCacheType.Assembly.GetType("System.Web.Caching.CacheItemRemovedCallback");

                Type[] types = new Type[] { typeof(string), typeof(object), x1, typeof(DateTime), typeof(TimeSpan), x2, x3 };

                _addItemMemberInfo = httpContextCacheType.GetMethod("Add", types /*BindingFlags.Public | BindingFlags.Instance*/);
            }
            _addItemMemberInfo.Invoke(httpContextCache,
                                      new[]
                                          {key, value, null, absoluteExpirationDate, new TimeSpan(0, 0, 0, 0), 3, null});
        }

        private object GetItemByReflection(string key)
        {
            object httpContextCache = HttpContextCache;
            if (_getItemMemberInfo == null)
            {
                Type httpContextCacheType = httpContextCache.GetType();

                //OK. No ambiguity, no Type[] needed.
                Type[] types = new Type[] {typeof (string)};
                _getItemMemberInfo = httpContextCacheType.GetMethod("Get", types); //BindingFlags.Public | BindingFlags.Instance);
            }
            return _getItemMemberInfo.Invoke(httpContextCache, new object[] {key});
        }

        private void RemoveItemByReflection(string key)
        {
            object httpContextCache = HttpContextCache;
            if (_removeItemMemberInfo == null)
            {
                Type httpContextCacheType = httpContextCache.GetType();

                //OK. No ambiguity, no Type[] needed.
                Type[] types = new Type[] { typeof(string) };
                _removeItemMemberInfo = httpContextCacheType.GetMethod("Remove", types);
                                                                       //BindingFlags.Public | BindingFlags.Instance);
            }
            _removeItemMemberInfo.Invoke(httpContextCache, new object[] {key});
        }


        private IDictionaryEnumerator GetEnumeratorByReflection()
        {
            object httpContextCache = HttpContextCache;
            if (_getEnumeratorMemberInfo == null)
            {
                Type httpContextCacheType = httpContextCache.GetType();

                //Clear does not exist.??
                _getEnumeratorMemberInfo = httpContextCacheType.GetMethod("Clear",
                                                                          BindingFlags.Public | BindingFlags.Instance);
            }
            //This might have to be a null rather than an empty array...hum...
            IDictionaryEnumerator dictionaryEnumerator =
                _getEnumeratorMemberInfo.Invoke(httpContextCache, new object[] {}) as
                IDictionaryEnumerator;

            return dictionaryEnumerator;
        }




    }
}

//        /// <summary>
//        /// Summary description for HttpApplicationCache.
//        /// </summary>
//        internal class ApplicationCache : ICachingService
//        {
//            #region IApplicationCache Members

//            protected object HttpContext
//            {
//                get { 
//                    return XAct.Environment.HttpContext;
//                }
//            }

//            protected object CurrentContextCache
//            {
//                get { 
//                    return null;//HttpContext....
//                }
//            }

//            public object this[string key]
//            {
//                get
//                {
//                    return this.CurrentContextCache[key];
//                }
//                set
//                {
//                    this.CurrentContextCache[key] = value;
//                }
//            }

//            public void Add(object o, string key, TimeSpan ts)
//            {
//                if (ts == TimeSpan.Zero)
//                    this.CurrentContextCache[key] = o;
//                else
//                    this.CurrentContextCache.Add(
//                        key, o, null,
//                        DateTime.Now + ts, TimeSpan.Zero,
//                        System.Web.Caching.CacheItemPriority.Normal, null);
//            }

//            public void Remove(string key)
//            {
//                this.CurrentContextCache.Remove(key);
//            }

//            public void RemoveAll()
//            {
//                lock (this.CurrentContextCache)
//                {
//                    System.Collections.IDictionaryEnumerator cacheEnum = 
//                        (IEnumerable)this.CurrentContextCache.GetEnumerator();

//                    while (cacheEnum.MoveNext())
//                    {
//                        this.CurrentContextCache.Remove((string) cacheEnum.Key);
//                    }
//                }
//            }

//            #endregion
//        }
//    }
//}