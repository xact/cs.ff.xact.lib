// ReSharper disable CheckNamespace
namespace XAct.Caching.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Globalization;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="ICultureInfoBasedCachingService"/>
    /// </summary>
    public class CultureInfoBasedCachingService : ICultureInfoBasedCachingService
    {
        private readonly ICachingService _cachingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CultureInfoBasedCachingService" /> class.
        /// </summary>
        /// <param name="cachingService">The caching service.</param>
        public CultureInfoBasedCachingService(ICachingService cachingService)
        {
            _cachingService = cachingService;
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="isValid">The is valid.</param>
        public void Set<TData>(CultureInfo cultureInfo, string key, TData objectToCache, Func<bool> isValid)
        {
            _cachingService.Set(MakeCultureBasedKey(cultureInfo, key), objectToCache, isValid);
        }

        /// <summary>
        /// Caches the given value for the specified time.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="expiryTimespan">The timespan to cache data for.</param>
        public void Set<TData>(CultureInfo cultureInfo, string key, TData objectToCache, TimeSpan expiryTimespan)
        {
            _cachingService.Set(MakeCultureBasedKey(cultureInfo, key), objectToCache, expiryTimespan);
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="dataFactory">The data factory.Can be as simple as ()=&gt;(dataToCache)</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        public void Set<TData>(CultureInfo cultureInfo, string key, Func<TData> dataFactory, TimeSpan expiryTimespan,
                               bool selfUpdating = true)
        {
            _cachingService.Set(MakeCultureBasedKey(cultureInfo, key), dataFactory, expiryTimespan, selfUpdating);
        }

        /// <summary>
        /// Gets the <see cref="System.Object" /> with the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        /// <value></value>
        public bool TryGet<TData>(CultureInfo cultureInfo, string key, out TData result)
        {
            return _cachingService.TryGet(MakeCultureBasedKey(cultureInfo, key), out result);
        }

        /// <summary>
        /// Gets the <see cref="System.Object" /> with the specified key.
        /// If not found, registers the datafactory, and returns its results.
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns></returns>
        public bool TryGet<TData>(CultureInfo cultureInfo, string key, out TData result, Func<TData> dataFactory,
                                  TimeSpan expiryTimespan,
                                  bool selfUpdating = true)
        {
            return _cachingService.TryGet(MakeCultureBasedKey(cultureInfo, key), out result, dataFactory,
                                          expiryTimespan, selfUpdating);
        }

        /// <summary>
        /// Removes the cached item that has the given key name.
        /// </summary>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        public void Remove(CultureInfo cultureInfo, string key)
        {
            _cachingService.Remove(MakeCultureBasedKey(cultureInfo, key));
        }

        /// <summary>
        /// Clears out all cached items.
        /// </summary>
        /// <param name="cultureInfo"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Clear(CultureInfo cultureInfo)
        {
            _cachingService.Clear(MakePrefix(cultureInfo));
        }

        private string MakeCultureBasedKey(CultureInfo cultureInfo, string key)
        {
            return MakePrefix(cultureInfo)  + key.ToLowerInvariant();
        }

        private string MakePrefix(CultureInfo cultureInfo)
        {
            return "CultureInfoSpecific:" + 
                    ((cultureInfo == null || cultureInfo.IsNeutralCulture)
                    ? "Invariant"
                    : cultureInfo.ToString())  
                    + ":" ;
        }
    }
}