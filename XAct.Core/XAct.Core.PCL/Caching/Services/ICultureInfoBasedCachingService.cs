﻿namespace XAct.Caching
{
    using System;
    using System.Globalization;

    /// <summary>
    /// The interface for a service that caches data.
    /// </summary>
    public interface ICultureInfoBasedCachingService : IHasXActLibService
    {
        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="isValid">The is valid.</param>
        void Set<TData>(CultureInfo cultureInfo, string key, TData objectToCache, Func<bool> isValid);

        /// <summary>
        /// Caches the given value for the specified time.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="expiryTimespan">The timespan to cache data for.</param>
        void Set<TData>(CultureInfo cultureInfo, string key, TData objectToCache, TimeSpan expiryTimespan);


        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="dataFactory">The data factory.Can be as simple as ()=>(dataToCache)</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        void Set<TData>(CultureInfo cultureInfo, string key, Func<TData> dataFactory, TimeSpan expiryTimespan,
                        bool selfUpdating = true);

        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// </summary>
        /// <value></value>
        /// <typeparam name="TData">The type of the data.</typeparam>
        bool TryGet<TData>(CultureInfo cultureInfo, string key, out TData result);


        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// If not found, registers the datafactory, and returns its results.
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns></returns>
        bool TryGet<TData>(CultureInfo cultureInfo, string key, out TData result, Func<TData> dataFactory,
                           TimeSpan expiryTimespan, bool selfUpdating = true);

        /// <summary>
        /// Removes the cached item that has the given key name.
        /// </summary>
        /// <param name="cultureInfo"></param>
        /// <param name="key">The key.</param>
        void Remove(CultureInfo cultureInfo, string key);

        /// <summary>
        /// Clears out all cached items.
        /// </summary>
        void Clear(CultureInfo cultureInfo);
    }
}

