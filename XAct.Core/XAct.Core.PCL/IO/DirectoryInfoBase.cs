﻿namespace XAct.IO
{
    using System.Collections.Generic;


    public abstract class DirectoryInfoBase<TIOService, TFileInfo> : FileSystemInfoBase<TIOService>, IDirectoryInfo
        where TIOService : IIOService
        where TFileInfo : class, IFileInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryInfoBase{TIOService}"/> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="fullName">The full name.</param>
        protected DirectoryInfoBase(IIOService managingIOService, string fullName)
            : this(managingIOService,
                   XAct.DependencyResolver.Current.GetInstance<IPathService>(),
                   fullName)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryInfoBase{TIOService}"/> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fullName">The full name.</param>
        protected DirectoryInfoBase(IIOService managingIOService, IPathService pathService, string fullName) :
            base(managingIOService, XAct.DependencyResolver.Current.GetInstance<IPathService>(), fullName)
        {
        }



        public IFileInfo[] GetFiles()
        {
            List<IFileInfo> results = new List<IFileInfo>();
            foreach (string path in base.IOService.GetDirectoryFileNamesAsync(this.FullName).Result)
            {
                TFileInfo fileInfo =

                    System.Activator.CreateInstance(typeof (TFileInfo), new object[] {IOService, PathService, path}) as
                    TFileInfo;

                results.Add(fileInfo);
            }

            return results.ToArray();
        }

    }
}