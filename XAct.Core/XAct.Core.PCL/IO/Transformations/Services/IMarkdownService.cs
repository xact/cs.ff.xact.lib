namespace XAct.IO.Transformations
{
    /// <summary>
    ///   The contract for a service for transforming markdown to a marked up output.
    /// </summary>
    /// <internal>
    /// The reason the contract is moved righ up into Core is because it is fundamental
    /// to help, and later, exceptions that can be displayed in any context (web or desktop).
    /// </internal>
    public interface IMarkdownService : IHasXActLibService
    {
        /// <summary>
        /// Transforms the specified markdown text to html output.
        /// </summary>
        /// <param name="markdownText">The markdown text.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        string Transform(string markdownText, Format format = Format.Auto);
    }
}