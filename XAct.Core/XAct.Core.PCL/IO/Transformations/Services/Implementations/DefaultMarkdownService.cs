namespace XAct.IO.Transformations.Implementations
{
    using XAct.Services;

    /// <summary>
    /// A default implementation of 
    /// <see cref="IDefaultMarkdownService"/>
    /// which is a empty service, that does no transformation.
    /// It's just there to to not break the library.
    /// </summary>
    [DefaultBindingImplementation(typeof(IMarkdownService), BindingLifetimeType.Undefined, Priority.VeryLow)]
    public class DefaultMarkdownService : IDefaultMarkdownService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultMarkdownService" /> class.
        /// </summary>
        public DefaultMarkdownService()
        {
            
        }
        /// <summary>
        /// Transforms the specified markdown text to html output.
        /// </summary>
        /// <param name="markdownText">The markdown text.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        public string Transform(string markdownText, Format format = Format.Auto)
        {
            return markdownText;
        }
    }
}