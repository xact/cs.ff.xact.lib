namespace XAct.IO.Transformations
{
    /// <summary>
    /// An empty service, that does no transformation.
    /// It's just there to to not break the library.
    /// </summary>
    public interface IDefaultMarkdownService : IMarkdownService, IHasXActLibService
    {
        
    }
}