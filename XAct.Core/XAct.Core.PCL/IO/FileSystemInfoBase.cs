﻿namespace XAct.IO
{
    using System;
    using XAct.Enums;

    public abstract class FileSystemInfoBase<TIOService> : IFileSystemInfo
        where TIOService : IIOService
    {

        /// <summary>
        /// Gets the <see cref="IIOService" /> based
        /// service managint this file system entry.
        /// </summary>
        /// <value>
        /// The io service.
        /// </value>
        public IIOService IOService { get; private set; }

        protected IPathService PathService { get; private set; }
        
        public string Name { get; private set; }

        public string FullName { get; private set; }



        /// <summary>
        /// Gets or sets a value indicating whether the file exists.
        /// </summary>
        public bool Exists
        {
            get
            {
                return IOService.DirectoryExistsAsync(this.FullName).WaitAndGetResult();
            }
        }

        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        ///   <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        ///   </internal>
        public System.DateTime? CreatedOnUtc
        {
            get { return IOService.DirectoryGetDateTimeAsync(this.FullName, AuditableEvent.Created).WaitAndGetResult<DateTime>(); }
            set
            {
                if (!value.HasValue) { return; }
                IOService.DirectorySetDateTimeAsync(this.FullName, AuditableEvent.Created, value.Value).Wait();
            }
        }

        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// <para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        public System.DateTime? LastModifiedOnUtc
        {
            get { return IOService.DirectoryGetDateTimeAsync(this.FullName, AuditableEvent.Modified).WaitAndGetResult(); }
            set
            {
                if (!value.HasValue) { return; }
                IOService.DirectorySetDateTimeAsync(this.FullName, AuditableEvent.Modified, value.Value).Wait();
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemInfoBase{TIOService}" /> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fullName">The full name.</param>
        /// <param name="name">The name.</param>
        protected FileSystemInfoBase(IIOService managingIOService, 
            IPathService pathService,
            string fullName)
        {

            IOService = managingIOService;

            PathService = pathService;
            FullName = fullName;
            Name = PathService.GetFileName(fullName);
        }



    }
}