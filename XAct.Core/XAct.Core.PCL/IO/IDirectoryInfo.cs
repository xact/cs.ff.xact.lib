﻿namespace XAct.IO
{
    /// <summary>
    /// Contract for portable (PCL)
    /// representation of a directory.
    /// <para>
    /// A specialization of the <see cref="IFileSystemInfo"/> contract.
    /// </para>
    /// </summary>
    public interface IDirectoryInfo : IFileSystemInfo
    {
        IFileInfo[] GetFiles();
    }

}