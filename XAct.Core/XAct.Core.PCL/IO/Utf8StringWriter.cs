﻿namespace XAct.IO
{
    using System;
    using System.Text;

    /// <summary>
    /// By default StringWriter uses UTF16.
    /// <para>
    /// This StringWriter rectifies this.
    /// </para>
    /// </summary>
    public sealed class Utf8StringWriter : StringWriterWithEncoding
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Utf8StringWriter"/> class.
        /// </summary>
        public Utf8StringWriter(): base(Encoding.UTF8){}
        /// <summary>
        /// Initializes a new instance of the <see cref="Utf8StringWriter"/> class.
        /// </summary>
        /// <param name="stringBuilder">The string builder.</param>
        public Utf8StringWriter(StringBuilder stringBuilder) : base(stringBuilder, Encoding.UTF8) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="Utf8StringWriter"/> class.
        /// </summary>
        /// <param name="formatProvider">An <see cref="T:System.IFormatProvider" /> object that controls formatting.</param>
        public Utf8StringWriter(IFormatProvider formatProvider) : base(formatProvider, Encoding.UTF8) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="Utf8StringWriter"/> class.
        /// </summary>
        /// <param name="formatProvider">The format provider.</param>
        /// <param name="stringBuilder">The string builder.</param>
        public Utf8StringWriter(IFormatProvider formatProvider, StringBuilder stringBuilder) : base(stringBuilder, formatProvider, Encoding.UTF8) { }
    }
}