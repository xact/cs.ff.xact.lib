﻿
    namespace XAct.IO
    {
        using System;
        using System.IO;
        using System.Text;

        /// <summary>
        /// A string writer that allows for alternate encodings
        /// than the default Utf16
        /// </summary>
        public class StringWriterWithEncoding : StringWriter
        {
            private readonly Encoding encoding;

            /// <summary>
            /// Initializes a new instance of the <see cref="StringWriterWithEncoding"/> class.
            /// </summary>
            /// <param name="encoding">The encoding.</param>
            public StringWriterWithEncoding(Encoding encoding)
            {
                this.encoding = encoding;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="StringWriterWithEncoding"/> class.
            /// </summary>
            /// <param name="stringBuilder">The string builder.</param>
            /// <param name="encoding">The encoding.</param>
            public StringWriterWithEncoding(StringBuilder stringBuilder, Encoding encoding)
                : base(stringBuilder)
            {
                this.encoding = encoding;
            }
            /// <summary>
            /// Initializes a new instance of the <see cref="StringWriterWithEncoding"/> class.
            /// </summary>
            /// <param name="formatProvider">The format provider.</param>
            /// <param name="encoding">The encoding.</param>
            public StringWriterWithEncoding(IFormatProvider formatProvider, Encoding encoding)
                : base(formatProvider)
            {
                this.encoding = encoding;
            }
            /// <summary>
            /// Initializes a new instance of the <see cref="StringWriterWithEncoding"/> class.
            /// </summary>
            /// <param name="stringBuilder">The string builder.</param>
            /// <param name="formatProvider">The format provider.</param>
            /// <param name="encoding">The encoding.</param>
            public StringWriterWithEncoding(StringBuilder stringBuilder, IFormatProvider formatProvider, Encoding encoding)
                : base(stringBuilder, formatProvider)
            {
                this.encoding = encoding;
            }

            /// <summary>
            /// Gets the <see cref="T:System.Text.Encoding" /> in which the output is written.
            /// </summary>
            /// <returns>The Encoding in which the output is written.</returns>
            public override Encoding Encoding
            {
                get { return encoding; }
            }
        }
    }
