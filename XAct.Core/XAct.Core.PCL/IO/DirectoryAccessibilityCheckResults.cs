namespace XAct.IO.Implementations
{
    using System;

#pragma warning disable 1591
    public class DirectoryAccessibilityCheckResults
    {
        public bool Exists { get; set; }
        public ResultStatus CanRead { get; set; }

        public ResultStatus CanWrite { get; set; }

        public ResultStatus CanDelete { get; set; }
        public Exception Exception { get; set; }

    }
#pragma warning restore 1591
}