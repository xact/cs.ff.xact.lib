﻿namespace XAct.IO
{
    /// <summary>
    /// A portable (PCL) implementation of a 
    /// File System File object.
    /// <para>
    /// A specialization of the <see cref="IFileSystemInfo"/> contract.
    /// </para>
    /// </summary>
    public interface IFileInfo : IFileSystemInfo
    {

        /// <summary>
        /// Gets the string representing the extension part of the file
        /// </summary>
        string Extension { get;  }


        // /// <summary>
        // /// Gets the size, in bytes, of the current file
        // /// </summary>
        // /// <value>
        // /// The length.
        // /// </value>
        // long Length { get; set; }

        /// <summary>
        /// Parent directory.
        /// </summary>
        IDirectoryInfo Directory { get;  }


        /// <summary>
        /// Gets or sets a value that determines if the current file is read only
        /// </summary>
        bool IsReadOnly { get; }


        /// <summary>
        /// Gets the size of the file.
        /// </summary>
        long Size { get; }
    }
}
