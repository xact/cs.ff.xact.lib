﻿namespace XAct.IO
{
    public class FileInfoBase<TIOService,TDirectoryInfo> : FileSystemInfoBase<TIOService>, IFileInfo
        where TIOService : IIOService
        where TDirectoryInfo :class, IDirectoryInfo
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="FileInfoBase{TIOService}"/> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="fullName">The full name.</param>
        protected FileInfoBase(IIOService managingIOService, string fullName)
            : this(managingIOService,
                   XAct.DependencyResolver.Current.GetInstance<IPathService>(),
                   fullName)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryInfoBase{TIOService}"/> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fullName">The full name.</param>
        protected FileInfoBase(IIOService managingIOService, IPathService pathService, string fullName) :
            base(managingIOService, XAct.DependencyResolver.Current.GetInstance<IPathService>(), fullName)
        {
        }


        public string Extension
        {
            get
            {
                return PathService.GetExtension(this.FullName);
            } 
        }


        public IDirectoryInfo Directory { 
            get
            {
                if (_parentDirectory != null)
                {
                    return _parentDirectory;
                }

                string parentDirectoryPath =  PathService.GetDirectoryName(this.FullName);

                _parentDirectory =

                    System.Activator.CreateInstance(typeof(TDirectoryInfo), new object[] { IOService, PathService, parentDirectoryPath }) as
                    TDirectoryInfo;

                return _parentDirectory;

            }
        } 
        private TDirectoryInfo _parentDirectory;


        public bool IsReadOnly { get; private set; }




        public long Size
        {
            get { return IOService.FileGetSizeAsync(this.FullName).WaitAndGetResult(); }
        }


        #region NotImplemented
        //public abstract long Length { get; }
        #endregion
    }
}