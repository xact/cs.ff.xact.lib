﻿namespace XAct.IO
{
    public interface IFileSystemInfo: IHasDateTimeCreatedOnUtc, IHasDateTimeModifiedOnUtc
    {

        /// <summary>
        /// Gets the <see cref="IIOService"/> based
        /// service managint this file system entry.
        /// </summary>
        /// <value>
        /// The io service.
        /// </value>
        IIOService IOService { get; }

        /// <summary>
        /// Gets or sets the name of the file (without directory, with extension).
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get;  }

        /// <summary>
        /// Gets the full path of the directory or file.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        string FullName { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the file exists.
        /// </summary>
        bool Exists { get; }
   

    }
}