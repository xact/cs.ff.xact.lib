﻿namespace XAct.IO
{
    using System;

    /// <summary>
    /// Contract for a service to provide
    /// a common means of determinining file name path parsing
    /// in a Portable/PCL manner.
    /// </summary>
    public interface IPathService : IHasXActLibService
    {
        int GetRootLength(String path);

        char[] GetInvalidPathChars();
        char[] GetInvalidFileNameChars();


        string Combine (string directory, string fileName);

        string GetFileName(String path);
        string GetExtension(string fileName);
        string GetFileNameWithoutExtension(String path);
        string GetDirectoryName(String path);
        string ChangeExtension(String path, String extension);

                // Tests if a path includes a file extension. The result is
        // true if the characters that follow the last directory
        // separator ('\\' or '/') or volume separator (':') in the path include 
        // a period (".") other than a terminal period. The result is false otherwise.
        // 
        bool HasExtension(String path);

                    // Tests if the given path contains a root. A path is considered rooted
        // if it starts with a backslash ("\") or a drive letter and a colon (":"). 
        //
        bool IsPathRooted(String path);




    }
}