﻿namespace XAct.IO
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using XAct.Enums;
    using XAct.IO.Implementations;


    // Provides methods for processing directory strings in an ideally
    // cross-platform manner.  Most of the methods don't do a complete 
    // full parsing (such as examining a UNC hostname), but they will
    // handle most string operations. 
    // 
    // File names cannot contain backslash (\), slash (/), colon (:),
    // asterick (*), question mark (?), quote ("), less than (<;), 
    // greater than (>;), or pipe (|).  The first three are used as directory
    // separators on various platforms.  Asterick and question mark are treated
    // as wild cards.  Less than, Greater than, and pipe all redirect input
    // or output from a program to a file or some combination thereof.  Quotes 
    // are special.
    // 
    // We are guaranteeing that Path.SeparatorChar is the correct 
    // directory separator on all platforms, and we will support
    // Path.AltSeparatorChar as well.  To write cross platform 
    // code with minimal pain, you can use slash (/) as a directory separator in
    // your strings.
     // Class contains only static data, no need to serialize

    /// <summary>
    ///   Interface for handling IO access to the host system.
    ///   <para>
    ///     Note that this does guarantee that we are talking about the FileSystem itself:
    ///     it depends on the capabilities of the host (client, or server, etc).
    ///   </para>
    /// </summary>
    /// <internal>
    ///   The reason for wrapping standard static read write methods
    ///   with a service is that it depends on the platform for where
    ///   we are saving.
    ///   <para>
    ///     On a RIA client application, we are probably talking about saving
    ///     to Isolated storage, or maybe a WCF callback to the server -- whereas
    ///     on the server, we're probably talking about the FileSystem.
    ///   </para>
    ///   <para>
    ///     Decoupling the implementation from the contract allows us to use
    ///     common contract invocations, yet deal with different solutions as needed.
    ///   </para>
    /// </internal>
    /// <internal>
    /// Usage of a class for all HD access allows for a centralized
    /// service that can be mocked/stubbed for testing...a major issue
    /// when talking about HD Access...
    /// </internal>
    /// <internal>
    /// As to why it's async: http://blogs.msdn.com/b/dsplaisted/archive/2012/08/27/how-to-make-portable-class-libraries-work-for-you.aspx
    /// </internal>
// ReSharper disable InconsistentNaming
    public interface IIOService : IHasXActLibService
        // ReSharper restore InconsistentNaming
    {

        //Notes: used by:
        //* ICompilationService.
        //* IPersistedFileAttachements
        //* 

        /// <summary>
        /// Gets the name of the Directory Accessibility test file.
        /// </summary>
        /// <value>
        /// The name of the access test file.
        /// </value>
        string DirectoryAccessTestFileName { get; }


        /// <summary>
        /// Opens the specified resource with the given settings.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="fileMode">The file mode.</param>
        /// <param name="fileAccess">The file access.</param>
        /// <param name="fileShare">The file share.</param>
        /// <returns></returns>
        Task<Stream> FileOpenAsync(string uri, XAct.IO.FileMode fileMode, XAct.IO.FileAccess fileAccess,
                              XAct.IO.FileShare fileShare);



        /// <summary>
        /// Opens the named resource and returns a readonly stream.
        /// </summary>
        /// <param name="uri">The full uri to the resource.</param>
        /// <returns></returns>
        Task<Stream> FileOpenReadAsync(string uri);

        /// <summary>
        /// Opens the named resource and returns a write only stream.
        /// <para>
        /// Note that by default Windows will happily open a stream to replace an existing file,
        /// so check first whether a file exists.
        /// </para>
        /// </summary>
        /// <param name="uri">The uri.</param>
        /// <param name="replaceAnyIfFound">if set to <c>true</c> replace any if file already exists.</param>
        /// <param name="append">if set to <c>true</c> opens the stream for appending.</param>
        /// <returns></returns>
        Task<Stream> FileOpenWriteAsync(string uri, bool replaceAnyIfFound = true, bool append = false);
         
        /// <summary>
        /// Opens the named resource and returns a <see cref="StreamReader"/>.
        /// </summary>
        /// <param name="uri">The uri.</param>
        /// <returns>A <see cref="StreamReader"/>.</returns>
        [Obsolete("Obfuscates what is really going on. Prefer instead using ioService.OpenRead(path).OpenStreamReader()"
            )]
        Task<StreamReader> FileOpenTextAsync(string uri);


        /// <summary>
        /// Replaces the destination, with the source, 
        /// after moving the destination to a Backup position.
        /// </summary>
        /// <param name="sourceUri">Name of the source file.</param>
        /// <param name="destinationUri">Name of the destination file.</param>
        /// <param name="destinationBackupUri">Name of the destination backup file.</param>
        Task FileReplaceAsync(string sourceUri, string destinationUri, string destinationBackupUri = null);

        /// <summary>
        /// Copies the specified source resource to the target URI.
        /// </summary>
        /// <param name="sourceUri">The source URI.</param>
        /// <param name="destinationUri">The destination URI.</param>
        /// <param name="overwriteAllowed">if set to <c>true</c> [overwrite] any existing file.</param>
        /// <internal>5/8/2011: Sky</internal>
        Task FileCopyAsync(string sourceUri, string destinationUri, bool overwriteAllowed = false);

        /// <summary>
        /// Creates a new resource at the specified location, returning a stream.
        /// </summary>
        /// <param name="uri">The uri.</param>
        /// <returns></returns>
        [Obsolete("Obfuscates what is really going on: prefer using ioService.OpenWrite(path,false, false)")]
        Task<Stream> FileCreateAsync(string uri);

        /// <summary>
        /// Deletes the specified URI.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <internal><para>5/8/2011: Sky</para></internal>
        Task FileDeleteAsync(string uri);

        /// <summary>
        /// Determines if the specified resource exists.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        /// <internal><para>5/8/2011: Sky</para></internal>
        Task<bool> FileExistsAsync(string uri);


        /// <summary>
        /// Gets the portable information about the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        Task<IFileInfo> GetFileInfoAsync(string fileName);

        /// <summary>
        /// Moves the specified resource.
        /// <para>
        /// Note that there must be no file in the destinationUri.
        /// If there is, it should be deleted first,
        /// using <see cref="DeleteFile"/>
        /// 	</para>
        /// </summary>
        /// <param name="sourceUri">Name of the source file.</param>
        /// <param name="destinationUri">Name of the destination file.</param>
        /// <param name="overWriteAllowed">if set to <c>true</c> [over write allowed].</param>
        /// <internal>5/8/2011: Sky</internal>
        Task FileMoveAsync(string sourceUri, string destinationUri, bool overWriteAllowed = false);

        /// <summary>
        /// Appends the given string to the specified resource.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="contents">The contents.</param>
        /// <internal><para>5/8/2011: Sky</para></internal>
        [Obsolete(
            "Obfuscates whats really going on. Prefer using ioService.OpenWrite(uri,false, true).OpenStreamWriter().Write(contents);"
            )]
        Task FileAppendAllTextAsync(string uri, string contents);



        /// <summary>
        /// Check the file's accessibility.
        /// </summary>
        /// <param name="fileFullName">Full name of the file.</param>
        /// <param name="desiredFileAccess">The desired file access.</param>
        /// <param name="desiredFileShare">The desired file share.</param>
        /// <returns></returns>
        Task<bool> FileCheckAccessibilityAsync(string fileFullName,
                                          XAct.IO.FileAccess desiredFileAccess = XAct.IO.FileAccess.Read,
                                          XAct.IO.FileShare desiredFileShare = XAct.IO.FileShare.Read);



        /// <summary>
        /// Get's the filenames of the files in the directory.
        /// <para>
        /// At present, don't think this deserves its own service. We'll see.
        /// </para>
        /// </summary>
        /// <param name="directoryName">Name of the directory.</param>
        /// <param name="searchPattern">The search pattern.</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
        Task<string[]> GetDirectoryFileNamesAsync(string directoryName, string searchPattern = null,
                                       HierarchicalOperationOption searchOption = HierarchicalOperationOption.TopOnly);


        /// <summary>
        /// Creates the directory.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        Task DirectoryCreateAsync(string directoryPath);


        /// <summary>
        /// Determines if the directory exists.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <param name="createIfMissing">if set to <c>true</c> [create if missing].</param>
        /// <returns></returns>
        Task<bool> DirectoryExistsAsync(string directoryPath, bool createIfMissing = false);


        /// <summary>
        /// Determines if the directory exists.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <returns></returns>
        Task DirectoryDeleteAsync(string directoryPath);


        /// <summary>
        /// Gets the files's dates.
        /// </summary>
        /// <param name="fileFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        Task<DateTime> FileGetDateTimeAsync(string fileFullName, AuditableEvent type);



        /// <summary>
        /// Gets the files's size.
        /// </summary>
        /// <param name="fileFullName">Full name of the directory.</param>
        Task<long> FileGetSizeAsync(string fileFullName);



        /// <summary>
        /// Sets the files's dates.
        /// </summary>
        /// <param name="fileFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        /// <param name="dateTimeUtc">The date time UTC.</param>
        Task FileSetDateTimeAsync(string fileFullName, AuditableEvent type, DateTime dateTimeUtc);


        /// <summary>
        /// Gets the directory's dates.
        /// </summary>
        /// <param name="directoryFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        Task<DateTime> DirectoryGetDateTimeAsync(string directoryFullName, AuditableEvent type);


        /// <summary>
        /// Sets the directory's dates.
        /// </summary>
        /// <param name="directoryFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        /// <param name="dateTimeUtc">The date time UTC.</param>
        Task DirectorySetDateTimeAsync(string directoryFullName, AuditableEvent type, DateTime dateTimeUtc);


        /// <summary>
        /// Checks the directory's accessibility.
        /// </summary>
        /// <param name="directoryFileName">The directory information.</param>
        /// <param name="tryForceCreateDirectory">if set to <c>true</c> [try force create directory].</param>
        Task<DirectoryAccessibilityCheckResults> DirectoryCheckAccessibilityAsync(string directoryFileName, bool tryForceCreateDirectory);


    }
}