﻿namespace XAct.IO
{
    /// <summary>
    /// The default implementation of the <see cref="IPathService"/>
    /// <para>
    /// The <see cref="IPathService"/> is a means of providing 
    /// a common means of determinining file name path parsing
    /// in a Portable/PCL manner.
    /// <c>PCL</c>
    /// </para>
    /// </summary>
    public interface IDefaultPathService : IPathService, IHasXActLibService
    {
            
    }
}