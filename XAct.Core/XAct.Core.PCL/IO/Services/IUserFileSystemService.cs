﻿namespace XAct.IO
{
    using System.Security.Principal;

    /// <summary>
    /// Contract for a service to return 
    /// entry points to the current 
    /// <see cref="IIdentity"/>'s WellKnown 
    /// file system locations (Local, Roaming, Temp).
    /// <para>
    /// Very rarely relevant to Server based development.
    /// </para>
    /// </summary>
    public interface IUserFileSystemService : IHasXActLibService
    {
        /// <summary>
        /// The current identity's local folder
        /// associated to this application,
        /// on this device only .
        /// <para>
        /// Contents are not synchronized with other devices.
        /// </para>
        /// <para>
        /// Note that the application identity is the app user on mobile, 
        /// service account in a web application)
        /// </para>
        /// </summary>
        IDirectoryInfo UserLocal { get; }

        /// <summary>
        /// The current identity's roaming folder, 
        /// which is synchronized across devices.
        /// <para>
        /// Note that the application identity is the app user on mobile, 
        /// service account in a web application)
        /// </para>
        /// </summary>
        IDirectoryInfo UserRoaming { get; }

        /// <summary>
        /// The current identity's temp folder 
        /// <para>
        /// Potentially cleared 
        /// every time the application is run).
        /// </para>
        /// </summary>
        IDirectoryInfo UserTemp { get; }





        /// <summary>
        /// The common (shared by all users of this application on this device) local folder, 
        /// associated to this application, on this device only.
        /// <para>
        /// Contents are not synchronized with other devices.
        /// </para>
        /// 
        /// <para>
        /// Note that the application identity is the app user on mobile, 
        /// service account in a web application)
        /// </para>
        /// <para>
        /// Note that there is no concept of CommonRoaming.
        /// </para>
        /// </summary>
        IDirectoryInfo CommonLocal { get; }


        /// <summary>
        /// The application's root folder.
        /// <para>
        /// Not available under all platforms (use
        /// <see cref="UserRoaming"/>
        /// or 
        /// <see cref="UserLocal"/>
        /// for user files.
        /// </para>
        /// </summary>
        IDirectoryInfo ApplicationRoot { get; }


    }
}