﻿namespace XAct.IO
{
    /// <summary>
    /// Enum specifiying which Date/Time 
    /// (Create, Modify, Access) attribute.
    /// </summary>
    public enum FileDateType
    {
        /// <summary>
        /// File Created DateTime (UTC).
        /// </summary>
        Created = 1,


        /// <summary>
        /// File Last Accessed (Touched) DateTime (UTC).
        /// </summary>
        LastAccessed = 2,

        /// <summary>
        /// File Last Modified/Written DateTime (UTC).
        /// </summary>
        LastModified = 3,
    } ;
}