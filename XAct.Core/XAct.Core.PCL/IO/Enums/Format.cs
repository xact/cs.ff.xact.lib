﻿namespace XAct.IO
{
    /// <summary>
    /// The format of the output.
    /// </summary>
    public enum Format
    {
        /// <summary>
        /// No format is defined.
        /// <para>
        /// This is an error state.
        /// </para>
        /// <para>
        /// Value=1.
        /// </para>
        /// </summary>
        Undefined =0,

        /// <summary>
        /// The default value (not the same as Auto, which is based on context).
        /// <para>
        /// Value=1.
        /// </para>
        /// </summary>
        Default=1,
        /// <summary>
        /// Output format is automatically defined by Context.
        /// <para>
        /// Value=2.
        /// </para>
        /// </summary>
        Auto=2,
        /// <summary>
        /// Output is in same format as Input.
        /// <para>
        /// Value=3.
        /// </para>
        /// </summary>
        InputFormat=3,

        /// <summary>
        /// Output is ascii/text for Console output.
        /// <para>
        /// Value=4.
        /// </para>
        /// </summary>
        Console=4,
        /// <summary>
        /// Output is in Html.
        /// <para>
        /// Value=5.
        /// </para>
        /// </summary>
        Html=5,

        ///// <summary>
        ///// Output is in Html, but is not wrapped in <![CDATA[<p></p>]]>
        ///// (as with <see cref="Html"/> output) so that it can be used in
        ///// labels.
        ///// <para>
        ///// Value=5.
        ///// </para>
        ///// </summary>
        //HtmlPartials = 6,

        /// <summary>
        /// Output is in PDF.
        /// <para>
        /// Value=6.
        /// </para>
        /// </summary>
        PDF=10,
    }
}
