﻿namespace XAct.Configuration
{
    using System;

    /// <summary>
    /// An exception to raise for some type of configuration error.
    /// <para>
    /// A reason to avoid the System exception dedicated to Configuration
    /// Exceptions is that it requires System.configuration dll.
    /// </para>
    /// </summary>
    public class ConfigurationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public ConfigurationException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ConfigurationException(string message, Exception innerException) : base(message, innerException) { }

        //PCL_CONVERSION:SerializationInfo is not available.
        ///// <summary>
        ///// Initializes a new instance of the <see cref="ConfigurationException"/> class.
        ///// </summary>
        ///// <param name="serializationInfo">The serialization info.</param>
        ///// <param name="streamingContext">The streaming context.</param>
        //public ConfigurationException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }
}
