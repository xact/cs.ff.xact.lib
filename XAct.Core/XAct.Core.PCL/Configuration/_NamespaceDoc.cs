﻿
namespace XAct.Configuration
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Configuration -- in NET -- is a dangerous topic.
    /// <para>
    /// Without a good understanding of the pitfalls, one quickly gets...
    /// an unportable nightmare.
    /// </para> 
    /// </summary>
    /// <remarks>
    /// <para>
    /// I had better explain why I consider Configuration an area trickier than most...
    /// </para>
    /// <para>
    /// THe first issue is that .NET10 came out touting an Xml based configuration solution.
    /// That's great...except that it's really only been available on Server and Desktop.
    /// If you were developing for Compact Framework -- or later Ag -- you were stuffed
    /// and had to come up with an alternate strategy.
    /// </para>
    /// <para>
    /// Secondly, the default config file requires IO access. Ie...not easily Sandboxed,
    /// and therefore needs elevated permissions...not a ton, but still, it's not Green.
    /// </para>
    /// <para>
    /// And it turns out you can't get around that with IsolatedStorage -- the default 
    /// configuration system doesn't take it into account.
    /// </para>
    /// <para>
    /// Then there's AppSettings. Which is a basically not much better than an INI file.
    /// Not only does it render untyped variables, which are always a bug waiting to happen, 
    /// but values are parsed upon retrieval (ie a poorly formatted string blows
    /// up late in execution, rather than in an early bootstrap event). Goes against the 
    /// maintenance pattern of Break Early.
    /// </para>
    /// <para>
    /// AppSettings, or ApplicationSettings are Monolithic. They are designed for an app...
    /// with no notion of Modules being added dynamically later.
    /// </para>
    /// <para>
    /// Configuration of an app is so core...yet the .NET implementation requires dependencies. 
    /// Want an app setting? Ur...you need to add a reference to System.Configuration.
    /// </para>
    /// <para>
    /// It's it's infrastructure. Not only that, it's 
    /// a Vendor's (in this case MS) infrastructure. Your app, should be protected from changes
    /// in their product.
    /// </para>
    /// <para>
    /// I'm sure I've forgotten some of the other reasons the config system has made
    /// me gnash my teeth over the years, but what's above should be enough of a reason as to
    /// why one should use a more Modular, Green, Typed, Dynanic Configuration system
    /// </para>
    /// </remarks>
    /// <internal>
    /// This class is required by Sandcastle to document Namespaces: http://bit.ly/vqzRiK
    /// </internal>
    [CompilerGenerated]
    class NamespaceDoc { }
}
