﻿namespace XAct.Configuration
{
    /// <summary>
    /// A contract for an Application Settings service that
    /// provides indirection and any underlying configuration 
    /// technology (such as AppSettings)
    /// </summary>
    /// <internal><para>5/11/2011: Sky</para></internal>
    public interface IApplicationConfigurationService : IHasXActLibService
    {
        //IMPORTANT: Don't provide a string based Indexer...(at least not public) as it 
        //           quickly gets to be a bug source due to typeless keys.
        ///// <summary>
        ///// Gets or sets the <see cref="System.Object"/> with the specified ERROR.
        ///// </summary>
        ///// <value></value>
        ///// <internal>As to why it's not a generic: http://bit.ly/mC5FTx </internal>
        ///// <internal><para>5/11/2011: Sky</para></internal>
        //object this[string key] { get; }
    }
}