﻿namespace XAct
{
    using System;
    using System.IO;
    using System.Text;

    /// <summary>
    /// 
    /// </summary>
    public class WrappedObject
    {
        /// <summary>
        /// Gets the wrapped object.
        /// </summary>
        public object Item { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="WrappedObject"/> class.
        /// </summary>
        /// <param name="innerItem">The inner item.</param>
        public WrappedObject(object innerItem)
        {
            Item=innerItem;
        }
    }


}
