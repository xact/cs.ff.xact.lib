﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Location
{
    public class GeoCoordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public GeoCoordinates() { }

        public GeoCoordinates(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public override string ToString()
        {
            return ToGoogle();
        }

        private string ToGoogle()
        {
            return string.Format("{0},{1}", Latitude.ToString("0.000000"), Longitude.ToString("0.000000"));
        }
    }

}
