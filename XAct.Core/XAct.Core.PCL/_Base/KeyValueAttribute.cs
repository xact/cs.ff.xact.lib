﻿namespace XAct
{
    using System;

    /// <summary>
    /// An attribute to attach a key/value combination to any object.
    /// </summary>
    public class KeyValueAttribute : Attribute, IHasKeyValueReadOnly<string>
    {
        /// <summary>
        /// Gets the key.
        /// <para>Member defined in<see cref="XAct.IHasKeyReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string Key { get; private set; }
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyValueAttribute"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public KeyValueAttribute(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}