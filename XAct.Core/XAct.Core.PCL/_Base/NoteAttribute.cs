﻿namespace XAct
{
    using System;

    /// <summary>
    /// Attribute to attach a note to any object.
    /// </summary>
    public class NoteAttribute : Attribute , IHasNoteReadOnly
    {
        /// <summary>
        /// Gets the category of the note -- if any
        /// (see also <see cref="KeyValueAttribute"/>).
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        public string Category { get; private set; }



        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <value>
        /// The note.
        /// </value>
        public string Note { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="NoteAttribute" /> class.
        /// </summary>
        /// <param name="note">The note.</param>
        /// <param name="category">The category.</param>
        public NoteAttribute(string note, string category = null)
        {
            Note = note;
            Category = category;
        }
    }
}
