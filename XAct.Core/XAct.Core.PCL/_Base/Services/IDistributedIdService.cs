﻿namespace XAct
{
    using System;
    /// <summary>
    /// Contract for a service to generate sequential Guids suitable 
    /// for distributed scenarios.
    /// <para>
    /// Traditional Guids, although unique across systems, are random,
    /// creating unpredicatable, indexes, that ultimately affect
    /// insert and retrieval speeds.
    /// </para>
    /// <para>
    /// Alternative mechanisms are the use of Composite Keys 
    /// made up of a MachineGuid + Machine specific autoincrement index.
    /// An issue with this strategy is the need for a separate locking 
    /// autoincrement number service, as the int index column
    /// can't be an autoincrement
    /// (or else you cant insert the index from another machine).
    /// </para>
    /// <para>
    /// SequentialId() is acceptable, as it is unique per server.
    /// But of concern is that there is no indication that the algorythm
    /// used in CE is identical to the one used in SQL Server full, so
    /// there may be conflict at some point in the future.
    /// </para>
    /// <para>
    /// Alternately, one can use a custom service, with total control
    /// of the algorythm used to generate the Guids, that are ordered
    /// by time. This is what this service is for.
    /// </para>
    /// <para>
    /// Research:
    /// </para>
    /// <para>
    /// Research: Twitter, Oxidation and
    /// <see cref="http://blog.stephencleary.com/2009/08/alternative-guids-for-mobile-devices.html"/>
    /// </para>
    /// <para>
    /// See also <see cref="https://github.com/peschkaj/rustflakes/blob/master/RustFlakes/Oxidation.cs"/>
    /// </para>
    /// <para>
    /// See also: <see cref="http://www.informit.com/articles/article.aspx?p=25862&seqNum=7"/>
    /// </para>
    /// </summary>
    public interface IDistributedIdService : IHasXActLibService
    {
        /// <summary>
        /// Generates a Guid suitable for distributed scenarios.
        /// </summary>
        /// <returns></returns>
        Guid NewGuid();
    }


    /*
public static void Main(string[] args)
		{
			var o = new RustFlakes.Oxidation(new byte[] {0, 1, 2, 3, 4, 5});

			var start = DateTime.UtcNow;

			for (var i = 0; i < 65536; i++)
			{
				o.Oxidize();
			}

			var finish = DateTime.UtcNow;
			var diff = finish - start;
			Console.WriteLine(string.Format("Generated 65536 oxidations in {0}ms", diff.TotalMilliseconds));
		}
	}     */



}
