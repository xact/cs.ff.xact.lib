﻿namespace XAct.Implementations
{
    using System;
    using System.Linq;
    using System.Runtime.InteropServices;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IDistributedIdService"/>
    /// in order to generate sequentail Guid suitable
    /// for distributed scenarios.
    /// </summary>
    public class DistributedIdService : IDistributedIdService
    {
        //See https://github.com/peschkaj/rustflakes/blob/master/RustFlakes/Oxidation.cs

        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly Oxidation _oxidation;
        private readonly SequentialGuidGenerator _sequentialGuidGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributedIdService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public DistributedIdService(ITracingService tracingService, IEnvironmentService environmentService)
        {
            _tracingService = tracingService;
            _environmentService = environmentService;

            byte[] identifier = _environmentService.MachineName.ToByteArray();

            _oxidation = new Oxidation(identifier);
            _sequentialGuidGenerator = new SequentialGuidGenerator(environmentService);
        }



        /// <summary>
        /// Generates a Guid suitable for distributed scenarios.
        /// </summary>
        /// <returns></returns>
        public Guid NewGuid()
        {
            Guid result;
            
            //result = _oxidation.Oxidize();
            result = _sequentialGuidGenerator.NewSequentialGuid(SequentialGuidType.SequentialAtEnd);
            return result;
        }


        //Src: http://stackoverflow.com/a/3563872/1052767
        // Define other methods and classes here
        [System.Runtime.InteropServices.StructLayout(LayoutKind.Explicit)]
        private struct DecimalGuidConverter
        {
            [System.Runtime.InteropServices.FieldOffset(0)] public decimal Decimal;
            [System.Runtime.InteropServices.FieldOffset(0)] public Guid Guid;
        }



        /// <summary>
        /// <para>
        /// Src: https://github.com/peschkaj/rustflakes
        /// </para>
        /// </summary>
        public class Oxidation
        {
            private readonly ulong _epoch;
            private ulong _lastOxidizedInMs;
            private readonly UInt32 _identifier;
            private UInt16 _counter;

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="identifier"></param>
            public Oxidation(byte[] identifier)
            {
                _epoch = ((ulong) (new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).Ticks/10);

                _lastOxidizedInMs = CurrentTimeCounter();

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(identifier);

                // shorten identifier to 6 bytes
                if (identifier.Length > 6)
                    identifier = identifier.Take(6).ToArray();

                _identifier = BitConverter.ToUInt32(identifier, 0);
                _counter = 0;
            }


            /// <summary>
            /// <para>
            /// Src:
            /// </para>
            /// </summary>
            /// <returns></returns>
            public Guid Oxidize()
            {
                HandleTime();

                /* Shift _identifier to the left by 48 bits to occupy the appropriate
                 * position in the Decimal.
                 */
                uint i = _identifier << 48;

                /* Add UInt64.MaxValue to lastOxidizedInMs to bit shift by 64 bits.
                 * Convert to decimal first to avoid bits wrapping around in the background.
                 * 
                 * Add the shifted identifier
                 * Add the counter
                 */
                decimal d = (_lastOxidizedInMs + (decimal) UInt64.MaxValue) + i + _counter;
                DecimalGuidConverter dgc = new DecimalGuidConverter();
                dgc.Decimal = d;

                return dgc.Guid;
            }


            private void HandleTime()
            {
                ulong ct = CurrentTimeCounter();

                if (_lastOxidizedInMs < ct)
                {
                    _lastOxidizedInMs = ct;
                    _counter = 0;
                }
                else if (_lastOxidizedInMs > ct)
                {
                    throw new Exception("Clock is running backwards");
                }
                else
                {
                    _counter++;
                }
            }

            private ulong CurrentTimeCounter()
            {
                return ((ulong) DateTime.UtcNow.Ticks/10) - _epoch;
            }
        }

        public enum SequentialGuidType
        {
            SequentialAsString,
            SequentialAsBinary,
            SequentialAtEnd
        }

        public class SequentialGuidGenerator
        {
            private IEnvironmentService _environmentService;

            public SequentialGuidGenerator(IEnvironmentService environmentService)
            {
                _environmentService = environmentService;
            }

            public Guid NewSequentialGuid(SequentialGuidType guidType)
            {

                byte[] randomBytes = new byte[10];
                _environmentService.Random.NextBytes(randomBytes);

                //private static readonly RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();
                //_rng.GetBytes(randomBytes);


                long timestamp = DateTime.UtcNow.Ticks/10000L;
                byte[] timestampBytes = BitConverter.GetBytes(timestamp);

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(timestampBytes);
                }

                byte[] guidBytes = new byte[16];

                switch (guidType)
                {
                    case SequentialGuidType.SequentialAsString:
                    case SequentialGuidType.SequentialAsBinary:
                        Buffer.BlockCopy(timestampBytes, 2, guidBytes, 0, 6);
                        Buffer.BlockCopy(randomBytes, 0, guidBytes, 6, 10);

                        // If formatting as a string, we have to reverse the order
                        // of the Data1 and Data2 blocks on little-endian systems.
                        if (guidType == SequentialGuidType.SequentialAsString && BitConverter.IsLittleEndian)
                        {
                            Array.Reverse(guidBytes, 0, 4);
                            Array.Reverse(guidBytes, 4, 2);
                        }
                        break;

                    case SequentialGuidType.SequentialAtEnd:
                        Buffer.BlockCopy(randomBytes, 0, guidBytes, 0, 10);
                        Buffer.BlockCopy(timestampBytes, 2, guidBytes, 10, 6);
                        break;
                }

                return new Guid(guidBytes);
            }
        }

    }
}