﻿

namespace XAct
{
    using System.Runtime.Serialization;

    [DataContract]
    public class NameValue<TValue> : IHasNamedValue<TValue>
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public TValue Value { get; set; }

    }

}
