﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Dimensions
{
    public class Size
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public Size() { }

        public Size(int width, int height)
        {
            Width = width;
            Height = height;
        }
        public override string ToString()
        {
            return ToGoogle();
        }

        private string ToGoogle()
        {
            return string.Format("{0}x{1}", Width, Height);
        }
    }

}
