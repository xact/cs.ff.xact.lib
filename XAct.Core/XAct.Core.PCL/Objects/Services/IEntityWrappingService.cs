﻿// ReSharper disable CheckNamespace

namespace XAct.Entities
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    ///   Service to return Data/ORM Entities as Wrapped Domain Entities
    /// </summary>
    /// <typeparam name = "TWrappedEntity">The type of the wrapped entity.</typeparam>
    /// <typeparam name = "TEntityWrapper">The type of the entity wrapper.</typeparam>
    /// <typeparam name = "TEntityWrapperInterface">The type of the wrapping entity interface.</typeparam>
    public interface IEntityWrappingService<TEntityWrapperInterface, TEntityWrapper, TWrappedEntity>
        : IHasXActLibService
        /* where TWrappedEntity :class-- NO -- TOO LIMITING */
        where TEntityWrapper : class, IHasInnerItemReadOnly, TEntityWrapperInterface
    {
        /// <summary>
        ///   Wraps the given Data/Orm entity with a Domain Entity:
        /// </summary>
        /// <param name = "entityToWrap"></param>
        /// <returns></returns>
        TEntityWrapperInterface WrapEntity(TWrappedEntity entityToWrap);

        /// <summary>
        ///   Wraps the given colleciton of Data/Orm entity within a domain Entity collection.
        /// </summary>
        /// <param name = "collectionOfEntitiesToWrap">The collection of entities to wrap.</param>
        /// <returns></returns>
        ICollection<TEntityWrapperInterface> WrapCollection(ICollection<TWrappedEntity> collectionOfEntitiesToWrap);
    }
}