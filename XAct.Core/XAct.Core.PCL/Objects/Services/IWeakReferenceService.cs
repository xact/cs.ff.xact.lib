﻿namespace XAct
{
    using System;

    /// <summary>
    /// Contract for a service to track objects using <see cref="WeakReference"/>s.
    /// </summary>
    /// <typeparam name="TOuterWrapper">The type of the outer wrapper.</typeparam>
    /// <typeparam name="TInnerObject">The type of the inner object.</typeparam>
    public interface IWeakReferenceService<TOuterWrapper, in TInnerObject>
        : IHasXActLibService
        where TOuterWrapper : class, IHasWeakReferenced<TInnerObject>, new()
    {
        /// <summary>
        /// Gets or sets the number of times <see cref="Get"/>
        /// can be accessed before <see cref="Purge"/> is invoked.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        /// <value>The interations that can be done.</value>
        int PurgeCounterMax { get; set; }

        /// <summary>
        /// Gets the number of Items currently being tracked.
        /// <para>
        /// See difference with <see cref="CountAlive"/>.
        /// </para>
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        /// <value>The count.</value>
        int Count { get; }

        /// <summary>
        /// Gets the number of Items currently being tracked and are alive,
        /// unlike <see cref="Count"/>, which will include Items that are no longer alive.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        int CountAlive { get; }

        /// <summary>
        /// Registers an object for tracking using a <see cref="WeakReference"/>.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        /// <param name="watchedObject">The watched object.</param>
        /// <param name="raiseErrorIfAlreadyRegistered">if set to <c>true</c> [raise error if already registered].</param>
        /// <returns></returns>
        TOuterWrapper Register(TInnerObject watchedObject, bool raiseErrorIfAlreadyRegistered = true);


        /// <summary>
        /// Registers an object for tracking using a <see cref="WeakReference"/>.
        /// <br/>
        /// If already registered, just returns the existing <typeparamref name="TOuterWrapper"/>.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        /// <param name="watchedObject"></param>
        /// <param name="raiseErrorIfAlreadyRegistered"></param>
        /// <returns></returns>
        TOuterWrapper Register(TOuterWrapper watchedObject, bool raiseErrorIfAlreadyRegistered);

        /// <summary>
        /// Gets the <typeparamref name="TOuterWrapper"/> for the specified weakreferenced object.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the watchedObject argument is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the watchedObject was not registered previously.</exception>
        /// <value>An existing <typeparamref name="TOuterWrapper"/>.</value>
        TOuterWrapper Get(TInnerObject watchedObject);

        /// <summary>
        /// Clean the internal dictionary of all
        /// <typeparamref name="TOuterWrapper"/> that point to dataObjects
        /// that are no longer in memory.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        void Purge();
    }
}