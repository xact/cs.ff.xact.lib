﻿namespace XAct.Entities.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics;
    //using XAct.Extensions;

    /// <summary>
    /// Implementation of the <see cref="IWeakReferenceService{TOuterWrapper,TInnerObject}"/>
    /// contract to track objects using <see cref="WeakReference"/>s.
    /// </summary>
    //[DefaultBindingImplementation(typeof(IWeakReferenceService<TOuterWrapper, in TInnerObject> ) )]
    public class WeakReferenceService<TOuterWrapper, TInnerObject> : 
        IWeakReferenceService<TOuterWrapper, TInnerObject> 
        where TOuterWrapper : class, IHasWeakReferenced<TInnerObject>, new()
    {

        #region Fields
        //Every xxx invocations of Get(), we look to cleanup refs that are not alive.
        private int _purgeCheckCounter;
        private readonly Dictionary<int, TOuterWrapper> _cacheDictionary = new Dictionary<int, TOuterWrapper>();
        #endregion


        #region Services
        private readonly ITracingService _tracingService;
        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets the number of times <see cref="Get"/>
        /// can be accessed before <see cref="Purge"/> is invoked.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The default value is 500, the minimum value is 100.
        /// </para>
        /// </remarks>
        /// <value>The interations that can be done.</value>
        public int PurgeCounterMax
        {
            get
            {
                return _purgeCounterMax;
            }
            set
            {
                _purgeCounterMax = Math.Max(100, value);
            }
        }
        private int _purgeCounterMax = 5000;

        /// <summary>
        /// Gets the number of Items currently being tracked.
        /// <para>
        /// Note that this number is not necessarily correct.
        /// For a more precise number, invoke <see cref="Purge"/> first.
        /// </para>
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get
            {
                return _cacheDictionary.Count;
            }
        }



        /// <summary>
        /// Gets the number of Items currently being tracked and are alive,
        /// unlike <see cref="Count"/>, which will include Items that are no longer alive.
        /// </summary>
        public int CountAlive
        {
            get
            {
                return _cacheDictionary.Values.Count(outerWrapper => outerWrapper.WeakReference.IsAlive);
            }
        }





        #endregion


        /// <summary>
        /// Initializes a new instance of the <see cref="WeakReferenceService&lt;TOuterWrapper, TInnerObject&gt;"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public WeakReferenceService(ITracingService tracingService)
        {
            _tracingService = tracingService;
            _tracingService.Trace(TraceLevel.Verbose, "Instantiating WeakReferenceTrackingService");
        }

        #region Methods
        /// <summary>
        /// Determines whether the specified DataObject is being tracked.
        /// </summary>
        /// <param name="watchedObject">The <c>watchedObject</c>.</param>
        /// <returns>
        /// 	<c>true</c> if the specified data object is tracked; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the watchedObject argument is null.</exception>
        public bool IsTracked(TInnerObject watchedObject)
        {
            int key = watchedObject.MakeUniqueIdentifier();

            return _cacheDictionary.ContainsKey(key);
        }


        /// <summary>
        /// Registers the specified watched object.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        /// <param name="watchedObject">The watched object.</param>
        /// <param name="raiseErrorIfAlreadyRegistered">if set to <c>true</c> [raise error if already registered].</param>
        /// <returns></returns>
        public TOuterWrapper Register(TInnerObject watchedObject, bool raiseErrorIfAlreadyRegistered=true)
        {
            TOuterWrapper outerWrapper = new TOuterWrapper();
            outerWrapper.WeakReference.Target  = watchedObject;
            
            return Register(outerWrapper, raiseErrorIfAlreadyRegistered);
        }

        /// <summary>
        /// Registers an object for tracking via <see cref="WeakReference"/>.
        /// <br/>
        /// If already registered, just returns the existing <typeparamref name="TOuterWrapper"/>.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        /// <param name="outerObject"></param>
        /// <param name="raiseErrorIfAlreadyRegistered"></param>
        /// <returns></returns>
        public TOuterWrapper Register(TOuterWrapper outerObject, bool raiseErrorIfAlreadyRegistered = true)
        {
            if (outerObject.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("outerObject");
            }

            int key = outerObject.WeakReference.Target.MakeUniqueIdentifier();

            if (_cacheDictionary.ContainsKey(key))
            {
                if (raiseErrorIfAlreadyRegistered)
                {
                    throw new ArgumentException("InnerObject already being tracked.");
                }
                //Update it:
                outerObject = _cacheDictionary[key];
            }
            else
            {
                //And save it:
                _cacheDictionary[key] = outerObject;
            }

            return outerObject;
        }

        /// <summary>
        /// Gets the <typeparamref name="TOuterWrapper"/> for the specified data object.
        /// <para>Member defined in<see cref="XAct.IWeakReferenceService{TOuterWrapper,TInnerObject}"/></para>
        /// </summary>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the watchedObject argument is null.</exception>
        /// <exception cref="System.ArgumentNullException">An exception is raised if the watchedObject was not registered previously.</exception>
        /// <value>An existing <typeparamref name="TOuterWrapper"/>.</value>
        public TOuterWrapper Get(TInnerObject watchedObject)
        {
            //Look for it in the hash:
            int key = watchedObject.MakeUniqueIdentifier();

            TOuterWrapper outerWrapper;

            if (!_cacheDictionary.TryGetValue(key, out outerWrapper))
            {
                throw new Exception("Could not find Tracking Vars for given watched object.");
            }

            //Look for empties?

            if (_purgeCheckCounter >= _purgeCounterMax)
            {
                Purge();
            }else
            {
                _purgeCheckCounter += 1;
            }

            return outerWrapper;
        }





        /// <summary>
        /// Clean the internal dictionary of all
        /// <typeparamref name="TOuterWrapper"/> that point to dataObjects
        /// that are no longer in memory.
        /// </summary>
        public void Purge()
        {

            int[] keysToRemove =
                _cacheDictionary
                .Where(p => !p.Value.WeakReference.IsAlive)
                .Select(p => p.Key).ToArray();

            foreach (int key in keysToRemove)
            {
                _cacheDictionary.Remove(key);
            }

            //Reset counter:
            _purgeCheckCounter = 0;
        }

        #endregion


    }
}