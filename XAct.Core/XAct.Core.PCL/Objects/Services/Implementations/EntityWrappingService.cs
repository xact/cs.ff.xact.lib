﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

// ReSharper disable CheckNamespace
namespace XAct.Entities.Implementations
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using XAct.Collections;

    /// <summary>
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <typeparam name = "TWrappedEntity">The type of the wrapped entity.</typeparam>
    /// <typeparam name = "TEntityWrapper">The type of the entity wrapper.</typeparam>
    /// <typeparam name = "TEntityWrapperInterface">The type of the wrapping entity interface.</typeparam>
    public class EntityWrappingService<TEntityWrapperInterface, TEntityWrapper, TWrappedEntity>
        : IEntityWrappingService<TEntityWrapperInterface, TEntityWrapper, TWrappedEntity>
        where TWrappedEntity : class
        where TEntityWrapper : class, IHasInnerItem<TWrappedEntity>, TEntityWrapperInterface
    {

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "EntityWrappingService&lt;TEntityWrapperInterface, TEntityWrapper, TWrappedEntity&gt;" /> 
        ///   class.
        /// </summary>
        public EntityWrappingService()
        {
        }

        #endregion

        #region IEntityWrappingService<TEntityWrapperInterface,TEntityWrapper,TWrappedEntity> Members

        /// <summary>
        ///   Wraps the entity.
        /// </summary>
        /// <param name = "entityToWrap">The entity to wrap.</param>
        /// <returns></returns>
        public TEntityWrapperInterface WrapEntity(TWrappedEntity entityToWrap)
        {
            //We can use the ServiceLocator to find
            //the type of the instance...

            //Use the service to get the wrapper, 
            //without having to actually name 
            TEntityWrapper entityWrapper =
                (TEntityWrapper)
                DependencyResolver.Current.GetInstance(typeof (TEntityWrapperInterface));
            //Maybe better than:
            //Activator.CreateInstance(entityWrapperType, null);

            //Set the inner object:
            entityWrapper.SetInnerObject(entityToWrap);

            //Return the interface of the wrapper, not the wrapper itself:
            return entityWrapper;
        }

        /// <summary>
        ///   Wraps the collection.
        /// </summary>
        /// <param name = "collectionOfEntitiesToWrap">The collection of entities to wrap.</param>
        /// <returns></returns>
        public ICollection<TEntityWrapperInterface> WrapCollection(
            ICollection<TWrappedEntity> collectionOfEntitiesToWrap)
        {
            return
                new ObjectWrapperCollection<TWrappedEntity, TEntityWrapper, TEntityWrapperInterface>(
                    collectionOfEntitiesToWrap);
        }

        #endregion
    }
}