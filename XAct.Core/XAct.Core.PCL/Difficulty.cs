﻿namespace XAct
{
    using System.Runtime.Serialization;


    /// <summary>
    /// Enumeration of difficulty.
    /// <para>
    /// Used by TipService among others.
    /// </para>
    /// </summary>
    [DataContract]
    public enum Difficulty
    {
        /// <summary>
        /// The difficult is not set.
        /// <para>
        /// This is an error condition.
        /// </para>
        /// <para>
        /// Value=0
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined=0,

        /// <summary>
        /// For beginners.
        /// <para>
        /// Value=1
        /// </para>
        /// </summary>
        [EnumMember]
        Beginner = 1,

        /// <summary>
        /// For everyday users.
        /// <para>
        /// Value=2
        /// </para>
        /// </summary>
        [EnumMember]
        Normal = 2,


        /// <summary>
        /// For advanced everyday users.
        /// <para>
        /// Value=3
        /// </para>
        /// </summary>
        [EnumMember]
        Advanced = 3,

        /// <summary>
        /// For Experts.
        /// <para>
        /// Value=4
        /// </para>
        /// </summary>
        [EnumMember]
        Expert = 4,

        /// <summary>
        /// For gurus.
        /// <para>
        /// Value=5
        /// </para>
        /// </summary>
        [EnumMember]
        Guru = 5
    }
}
