﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    ///   Base class for Object Wrappers 
    /// <para>
    /// Object wrappers are used that wrap inner objects
    /// that without wrapping would drag Dependencies
    /// up and down the stack.
    /// </para>
    /// <para>
    /// Examples would be with EntityFramework entities, QuickGraph, etc.
    /// </para>
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <typeparam name = "TWrappedEntity">The Type of the Data entity that is being wrapped.</typeparam>
    /// <internal>
    ///   <para>
    ///     Note: a  Domain object is more of a projection than a 1-to-1 wrapper:
    ///     you don't have to map properties 
    ///   </para>
    /// </internal>
    public abstract class ObjectWrapperBase<TWrappedEntity>
        : IHasInnerItemReadOnly
        where TWrappedEntity : class
    {

        #region Properties
        /// <summary>
        ///   Gets or sets a value indicating whether this 
        ///   <see cref = "ObjectWrapperBase&lt;TWrappedEntity&gt;" /> 
        ///   is safe to expose to 3rd party assemblies.
        /// </summary>
        /// <value><c>true</c> if safe; otherwise, <c>false</c>.</value>
        protected bool SafeEntity
        {
            get { return _safeEntity; }
            set
            {
                if ((value == false) || _safeEntity)
                {
                    return;
                }
                _safeEntity = /*value*/ true;
            }
        }
        private bool _safeEntity;


        /// <summary>
        ///   The wrapped entity.
        /// <para>
        /// This will be the entity that would cause dependencies
        /// and needs to have mediator methods and properties 
        /// to work it.
        /// </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Inheritors can use this property to build their
        ///     implementations relatively faster:
        ///     <code>
        ///       <![CDATA[
        /// public class Example : EntityWrapperBase<ORM.Example>, IExample 
        /// {
        ///   public Example(ORM.Example example):base(example){}
        ///   ...
        ///   public int ExampleId 
        ///   {
        ///     get { return this.WrappedEntity.ExampleId; }
        ///     set { this.WrappedEntity.ExampleId = value; }
        ///   }
        ///   ...
        /// }
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <internal>
        ///   Interesting that the compiler doesn't complain that there
        ///   are two properties with the same name, as long as one is
        ///   emplicitly implemented.
        /// </internal>
        public  TWrappedEntity2 GetInnerItem<TWrappedEntity2>()
        {
            object o = (!_safeEntity) ? _wrappedEntity : null;
            return o.ConvertTo<TWrappedEntity2>();
        }


        /// <summary>
        /// Sets the inner object.
        /// </summary>
        /// <param name="o">The o.</param>
        public void SetInnerObject(object o)
        {

            //Can't reset it:
            if (!_safeEntity && _wrappedEntity != null)
            {
                return;
            }

            o.ValidateIsNotDefault("value");
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            _wrappedEntity = o;
        }

        private object _wrappedEntity;
        #endregion


        #region Equality Comparers

        //We provide custom Comparers as we don't really want to compare this object
        //as much as comparing 



        /// <summary>
        ///   Determines whether the specified <see cref = "System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name = "obj">The <see cref = "System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref = "System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (!typeof (ObjectWrapperBase<TWrappedEntity>).IsAssignableFrom(obj.GetType()))
            {
                return false;
            }

            return Equals((ObjectWrapperBase<TWrappedEntity>) obj);
        }

        /// <summary>
        ///   Determines whether the specified <see cref = "ObjectWrapperBase{TWrappedEntity}" /> is equal to this instance.
        /// <para>
        /// Note that as this an <see cref="IHasInnerItemReadOnly"/>,
        /// it is comparing the <c>InnerObject</c>, not this object itself.
        /// </para>
        /// </summary>
        /// <param name = "other">The <see cref = "ObjectWrapperBase{TWrappedEntity}" /> based entity to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref = "ObjectWrapperBase{TWrappedEntity}" /> based entity is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        protected virtual bool Equals(ObjectWrapperBase<TWrappedEntity> other)
        {
            //Note, this internal method only called if both args are not null.

            TWrappedEntity a = ((IHasInnerItemReadOnly)this).GetInnerItem<TWrappedEntity>();
            TWrappedEntity b = ((IHasInnerItemReadOnly)other).GetInnerItem<TWrappedEntity>();

            return a.GetHashCode() == b.GetHashCode();

        }


        /// <summary>
        ///   Returns a hash code for this instance.
        /// <para>
        /// Note that it is in fact returning the hashcode of the InnerObject.
        /// </para>
        /// </summary>
        /// <returns>
        ///   A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return ((IHasInnerItemReadOnly)this).GetInnerItem<TWrappedEntity>().GetHashCode();
        }

        #endregion


        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(ObjectWrapperBase<TWrappedEntity> a, object b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || (b == null))
            {
                return false;
            }
            
            return Equals(a, b);

        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(ObjectWrapperBase<TWrappedEntity> a, object b)
        {
            return !(a == b);
        }

    }
}