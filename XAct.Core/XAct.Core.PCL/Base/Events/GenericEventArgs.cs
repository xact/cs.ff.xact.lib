﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics;
using System.Diagnostics.Contracts;
#endif

// ReSharper disable CheckNamespace

namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Diagnostics;

    /// <summary>
    /// A Generic payload event arg package.
    /// </summary>
    /// <typeparam name="TPayload">The type of the payload.</typeparam>
    /// <internal><para>7/19/2011: Sky</para></internal>
    public class GenericEventArgs<TPayload> : EventArgs
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public TPayload Value { get; private set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="GenericEventArgs&lt;TPayload&gt;"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        [DebuggerStepThrough]
        public GenericEventArgs(TPayload value)
        {
            Value = value;
        }

        #endregion
    }
}