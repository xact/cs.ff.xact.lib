﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{

    /// <summary>
    /// A Context argument.
    /// </summary>
    public class Context : IHasGetContext
    {
        private readonly object _Context;

        /// <summary>
        /// Initializes a new instance of the <see cref="Context"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public Context(object context)
        {
            context.ValidateIsNotDefault("context");

            _Context = context;
        }

        #region IContext Members

        /// <summary>
        /// Gets the context, Typed as per the requestor's needs.
        /// </summary>
        /// <returns></returns>
        public TContextType GetContext<TContextType>()
        {
            return _Context.ConvertTo<TContextType>();
        }

        #endregion
    }
}