﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// A general Attribute that can be used to associate any note to any object.
    /// <para>
    /// Can be used (carefully...) for associating notes that can be used as filters. Untyped Attributes if you will...
    /// </para>
    /// </summary>
    public class TextAttribute :Attribute
    {
        /// <summary>
        /// Gets or sets the text of this Attribute.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text { get; set; }
    }
}
