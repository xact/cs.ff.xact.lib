﻿namespace XAct
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Date/Time intervals
    /// </summary>
    /// <internal>
    /// Used by DateTime ExtensionMethods
    /// </internal>
    [DataContract(Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public enum TimeInterval
    {
        /// <summary>Undefined. Value=0</summary>
        [EnumMember] Undefined = 0,
        /// <summary>Tick. Value=1</summary>
        [EnumMember] Tick = 1,
        /// <summary>Millisecond. Value=2</summary>
        [EnumMember] Millisecond = 2,
        /// <summary>Second. Value=3</summary>
        [EnumMember] Second = 3,
        /// <summary>Minute. Value=4</summary>
        [EnumMember] Minute = 4,
        /// <summary>Hour. Value=5</summary>
        [EnumMember] Hour = 5,
        /// <summary>Three Hours. Value=6</summary>
        [EnumMember] ThreeHours = 6,
        /// <summary>Six Hours. Value=7</summary>
        [EnumMember] SixHours = 7,
        /// <summary>Twelve Hours. Value=8</summary>
        [EnumMember] TwelveHours = 8,
        /// <summary>A Day. Value=9</summary>
        [EnumMember] Day = 9,
        /// <summary>A Week. Value=10</summary>
        [EnumMember] Week = 10,
        /// <summary>A month. Value=11</summary>
        [EnumMember] Month = 11,
        /// <summary>A Quarter. Value=12</summary>
        [EnumMember] Quarter = 12,
        /// <summary>A Year. Value=13</summary>
        [EnumMember] Year = 13,
        /// <summary>A Decade. Value=14</summary>
        [EnumMember] Decade = 14,
        /// <summary>A Century. Value=15</summary>
        [EnumMember] Century = 15,
        /// <summary>A Millenium. Value=16</summary>
        [EnumMember] Millenium = 16,
        /// <summary>A Megaannum (a million years). Value=17</summary>
        [EnumMember] Megaannum = 17,
        /// <summary>A Gigaannum (a billion years). Value=17</summary>
        [EnumMember] Gigaannum = 17,
        /// <summary>A Teraannum (a trillion years). Value=18</summary>
        [EnumMember] Teraannum = 17,
        /// <summary>A Eternity. Value=20</summary>
        [EnumMember] Eternity = int.MaxValue

    }
}