﻿namespace XAct
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enum to express locking beyond a simple boolean <c>IsEditable</c>.
    /// </summary>
    [DataContract(Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public enum LockState
    {
        /// <summary>
        /// The User or Group Setting is unlocked.
        /// </summary>
        [EnumMember] Unlocked = 0,

        /// <summary>
        /// The Setting is locked, due to a parent Group's Setting.
        /// </summary>
        [EnumMember] EnheritedLocked = 1,

        /// <summary>
        /// This Group Setting is unlocked.
        /// </summary>
        [EnumMember] Locked = 2
    }
}