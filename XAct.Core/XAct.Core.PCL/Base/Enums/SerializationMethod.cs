﻿namespace XAct
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration of commone serialization methods
    /// </summary>
    [DataContract(Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public enum SerializationMethod
    {
        /// <summary>
        /// An Error state.
        /// <para>Value = 0</para>
        /// </summary>
        [EnumMember] Undefined = 0,

        /// <summary>
        /// String.
        /// <para>Value = 1</para>
        /// </summary>
        /// <internal>
        /// TypeConverter typeConverter = TypeDescriptor.GetConverter(settingType);
        /// string serializedObject = typeConverter.ConvertToInvariantString(instance);
        /// var newInstance = typeConverter.ConvertFromInvariantString(serializedObject);
        /// </internal>
        [EnumMember] String = 1,

        /// <summary>
        /// Json.
        /// <para>Value = 4</para>
        /// </summary>
        [EnumMember] Json = 4,

        /// <summary>
        /// Xml.
        /// <para>Value = 2</para>
        /// <para>
        /// Remember that XmlSerializer will fail for Type, Font, Color, etc.
        /// </para>
        /// </summary>
        [EnumMember] Xml = 2,

        /// <summary>
        /// Base64 serialization of a Binary serialization job.
        /// <para>Value = 3</para>
        /// </summary>
        [EnumMember] Base64Binary = 3,

    }
}