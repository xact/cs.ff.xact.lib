﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Attribute required to stop
    /// CA1062 ValidateArgumentsOfPublicMethods from firing.
    /// </summary>
    /// <remarks>
    /// See: https://cs.byu.edu/aggregator/sources/10
    /// </remarks>
    public sealed class ValidatedNotNullAttribute : Attribute { }
}
