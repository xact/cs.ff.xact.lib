
// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Base class for implementing Disposable.
    /// </summary>
    public abstract class DisposableBase: IDisposable
    {

        /// <summary>
        /// Gets a value indicating whether this <see cref="DisposableBase"/> is disposed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if disposed; otherwise, <c>false</c>.
        /// </value>
        protected bool Disposed
        {
           get
           {
                lock(this)
                {
                    return _disposed;
                }
            }
        }
        private bool _disposed;

    #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            lock (this)
            {
                if (_disposed == false)
                {
                    Cleanup();
                    _disposed = true;

                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion

        /// <summary>
        /// Override to provide custom cleanup of resources.
        /// <para>
        /// An implementation of <see cref="XAct.DisposableBase"/>
        /// </para>
        /// </summary>
        protected virtual void Cleanup()
        {
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="DisposableBase"/> is reclaimed by garbage collection.
        /// </summary>
        ~DisposableBase()   
        {
            Cleanup();
        } 

    }

}
