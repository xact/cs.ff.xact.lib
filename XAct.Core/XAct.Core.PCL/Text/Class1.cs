﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace XAct
{
    [DataContract]
    public enum CaseSensitivity
    {
        [EnumMember]
        Undefined,
        [EnumMember]
        Sensitive,
        [EnumMember]
        Insensitive
    }
}
