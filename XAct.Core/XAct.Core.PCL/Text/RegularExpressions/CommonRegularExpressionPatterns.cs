﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace XAct.Text.RegularExpressions
{
    /// <summary>
    ///   Static Collection of common regular expressions.
    /// </summary>
    public static class CommonRegularExpressionPatterns
    {
        #region Nested type: CodeAnalysis

        /// <summary>
        ///   Regular Expression Patterns to validate Code specific text
        ///   (Variable Names, etc)
        /// </summary>
        /// <internal>
        ///   See: http://www.regular-expressions.info/examplesprogrammer.html
        /// </internal>
        public static class CodeAnalysis
        {
            /// <summary>
            ///   Regular Expression pattern to match variable Names.
            /// </summary>
            public static string VariableName = "[a-zA-Z][a-zA-Z0-9_]+";

            /// <summary>
            ///   Regular Expression pattern to match an unsigned integer.
            /// </summary>
            public static string UIntFragment = @"\b\d+\b";

            /// <summary>
            ///   Regular Expression pattern to match a signed integer.
            /// </summary>
            public static string IntFragment = @"[-+]?\b\d+\b";
        }

        #endregion

        #region Nested type: Validation

        /// <summary>
        ///   Regular Expression Patterns to Validate various forms of input.
        /// </summary>
        public static class Validation
        {
            /// <summary>
            ///   Regex pattern to match dates.
            /// </summary>
            public static string DateValidationOld = @"^\d{1,2}/\d{1,2}/(\d{2}|\d{4})$";


            /// <summary>
            ///   Validates that the date is between/inclusive
            ///   1900-01-01 and 2099-12-12
            /// </summary>
            /// <internal>
            ///   See http://www.regular-expressions.info/dates.html
            /// </internal>
            public static string DateValidation = @"^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$";

            /// <summary>
            ///   Pattern to match a number within the range of 0 to 255.
            /// </summary>
            public static string Binary0To255 = "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

            /// <summary>
            ///   Regex pattern to match an IP4 address.
            /// </summary>
            public static string Ip4Validation = @"\b{0}\.{0}\.{0}\.{0}\b".FormatStringInvariantCulture(Binary0To255);


            /// <summary>
            ///   Regex pattern to check that the value length is between a min and max length.
            ///   <para>
            ///     Requires two arguments: min, max.
            ///   </para>
            /// </summary>
            public static string MinMaxStringLength = "^.{{{0},{1}}}$";

            /// <summary>
            ///   Regex pattern to validate a text fragment as an unsigned integer.
            /// </summary>
            public static string UnsignedInteger = "^" + CodeAnalysis.UIntFragment + "$";

            /// <summary>
            ///   Regex pattern to validate a text fragment as signed integer.
            /// </summary>
            public static string Integer = "^" + CodeAnalysis.IntFragment + "$";


            /// <summary>
            /// Regex pattern to validate a single email address - as defined by RFC
            /// <para>
            /// http://tools.ietf.org/html/rfc2822#section-3.4.1
            /// </para>
            /// </summary>
            public static string EmailAddressRFCStrict = @"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])";

            /// <summary>
            /// Regex pattern to validate a text fragment as a single email address:
            /// </summary>
            public static string EmailAddressStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
               + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
               + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
               + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
               + @"[a-zA-Z]{2,}))$";


            /// <summary>
            /// Regex pattern to validate a single fragment as an email address:
            /// </summary>
            public static string EmailAddressLenient = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";


            /// <summary>
            /// Password expresion that requires 
            /// one lower case letter, 
            /// one upper case letter, 
            /// one digit, 
            /// 6-13 length, 
            /// no spaces. 
            /// </summary>
            public static string Password = @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,8}$";
        }

        #endregion
    }
}