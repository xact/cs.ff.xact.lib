﻿namespace XAct.Validation
{
    using System.Collections.Generic;
    using XAct.Messages;
    
    /// <summary>
    /// The contract for a ValidationService that validates Entities.
    /// <para>
    /// Note that this contract can be applied per layer (ie, just as easily in Domain, as in Application, Data, UI)
    /// changing Validation requirements based on layer.
    /// </para>
    /// </summary>
    /// <internal>
    /// A validation service is a very interesting example
    /// of various things that don't work...and why service
    /// is the solution.
    /// A validate method in the entity means:
    /// * because it cannot be overridden per layer, it has to validate
    ///   for all in all layers...monolithic.
    /// * if it relies on AppSettings...you're buggered. It's either 
    ///   by DI...or it becomes Monolithic from below...
    /// * If you move it to an ExtensionMethod, it's slightly better
    ///   except that you still need AppSettings...can't do that with
    ///   extension methods. Same problem for logging too.
    /// * Has to be in a Service that can be DI'ed with Settings and logging.
    /// </internal>
    /// <internal><para>5/11/2011: Sky</para></internal>
    public interface IValidationService<T> : IHasXActLibService
    {
        /// <summary>
        /// Validate the given entity, returning an array of error message.
        /// <para>
        /// Returns true if the error message array is empty.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">Type of the entity to validate.</typeparam>
        /// <param name="entityToValidate"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        bool Validate<TData>(TData entityToValidate, IResponse<Message> response);

        /// <summary>
        /// Validate the given entity, returning an array of error message.
        /// <para>
        /// Returns true if the error message array is empty.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">Type of the entity to validate.</typeparam>
        /// <param name="entityToValidate"></param>
        /// <param name="responseMessages"></param>
        /// <returns></returns>
        bool Validate<TData>(TData entityToValidate, IList<Message> responseMessages);



    }
}