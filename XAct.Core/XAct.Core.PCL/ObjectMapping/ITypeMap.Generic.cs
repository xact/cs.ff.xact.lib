﻿namespace XAct.ObjectMapping
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TSource">The type of the source.</typeparam>
    /// <typeparam name="TTarget">The type of the target.</typeparam>
    public interface ITypeMap<TSource,TTarget> : ITypeMap
    {
        
    }
}
