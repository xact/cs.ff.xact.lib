﻿using System;

namespace XAct.ObjectMapping
{
    /// <summary>
    /// Attribut to decorate Object Maps with so that they can be found by reflection.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ObjectMapAttribute : Attribute
    {
        public string ZoneTag { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectMapAttribute"/> class.
        /// </summary>
        /// <param name="zoneTag">The zone tag.</param>
        public ObjectMapAttribute(string zoneTag = null)
        {
            ZoneTag = zoneTag;
        }
    }
}
