﻿namespace XAct.ObjectMapping
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface ITypeMap //: IHasIdReadOnly<int>
    {
        /// <summary>
        /// Gets the type of the source to be mapped from.
        /// </summary>
        /// <value>
        /// The type of the source.
        /// </value>
        Type SourceType { get; }

        /// <summary>
        /// Gets the type of the target to map the source to.
        /// </summary>
        /// <value>
        /// The type of the target.
        /// </value>
        Type TargetType { get; }
    }
}