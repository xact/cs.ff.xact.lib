﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace System.Runtime.CompilerServices
{
    ///// <summary>
    ///// Proxy for class available in .NET 3.5
    ///// </summary>
    //public class ExtensionAttribute :Attribute {
    //}
}