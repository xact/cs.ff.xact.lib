
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace XAct.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    ///   A Typed hash set.
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    public class HashSetCollection<T> : ICollection<T>, IHasIsReadOnly
    {
        #region Fields

        private static readonly object _placeHolder = new object();

        /// <summary>
        ///   Internal cache.
        /// </summary>
        protected Dictionary<T, object> InternalDictionary;


        /// <summary>
        ///   TODO: Gets the place holder.
        /// </summary>
        /// <value>The place holder.</value>
        protected object PlaceHolder
        {
            get { return _placeHolder; }
        }

        #endregion

        #region Properties

        /// <summary>
        ///   Gets the number of elements contained in the <see cref = "T:System.Collections.Generic.ICollection`1"></see>.
        /// </summary>
        /// <value></value>
        /// <returns>The number of elements contained in the <see cref = "T:System.Collections.Generic.ICollection`1"></see>.</returns>
        public int Count
        {
            get { return InternalDictionary.Count; }
        }

        /// <summary>
        ///   Gets a value indicating whether the <see cref = "T:System.Collections.Generic.ICollection`1"></see> is read-only.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref = "T:System.Collections.Generic.ICollection`1"></see> is read-only; otherwise, false.</returns>
        public bool IsReadOnly
        {
            get { return false; }
            set { throw new NotImplementedException(); }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "HashSetCollection&lt;T&gt;" /> class.
        /// </summary>
        public HashSetCollection()
        {
            InternalDictionary = new Dictionary<T, object>();
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "HashSetCollection&lt;T&gt;" /> class.
        /// </summary>
        /// <param name = "comparer">The comparer.</param>
        public HashSetCollection(IEqualityComparer<T> comparer)
        {
            InternalDictionary = new Dictionary<T, object>(comparer);
        }

        #endregion

        #region ICollection public methods

        /// <summary>
        ///   Adds an item to the <see cref = "T:System.Collections.Generic.ICollection`1"></see>.
        /// </summary>
        /// <param name = "item">The object to add to the <see cref = "T:System.Collections.Generic.ICollection`1"></see>.</param>
        /// <exception cref = "T:System.NotSupportedException">The <see cref = "T:System.Collections.Generic.ICollection`1"></see> is read-only.</exception>
        public void Add(T item)
        {
            InternalDictionary[item] = PlaceHolder;
        }

        /// <summary>
        ///   Removes all items from the <see cref = "T:System.Collections.Generic.ICollection`1"></see>.
        /// </summary>
        /// <exception cref = "T:System.NotSupportedException">The <see cref = "T:System.Collections.Generic.ICollection`1"></see> is read-only. </exception>
        public void Clear()
        {
            InternalDictionary.Clear();
        }

        /// <summary>
        ///   Determines whether the <see cref = "T:System.Collections.Generic.ICollection`1"></see> contains a specific value.
        /// </summary>
        /// <param name = "item">The object to locate in the <see cref = "T:System.Collections.Generic.ICollection`1"></see>.</param>
        /// <returns>
        ///   true if item is found in the <see cref = "T:System.Collections.Generic.ICollection`1"></see>; otherwise, false.
        /// </returns>
        public bool Contains(T item)
        {
            return InternalDictionary.ContainsKey(item);
        }

        /// <summary>
        ///   Copies to.
        /// </summary>
        /// <param name = "array">The array.</param>
        /// <param name = "arrayIndex">The index.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            InternalDictionary.Keys.CopyTo(array, arrayIndex);
        }

        /// <summary>
        ///   Removes the first occurrence of a specific object from the <see cref = "T:System.Collections.Generic.ICollection`1"></see>.
        /// </summary>
        /// <param name = "item">The object to remove from the <see cref = "T:System.Collections.Generic.ICollection`1"></see>.</param>
        /// <returns>
        ///   true if item was successfully removed from the <see cref = "T:System.Collections.Generic.ICollection`1"></see>; otherwise, false. This method also returns false if item is not found in the original <see cref = "T:System.Collections.Generic.ICollection`1"></see>.
        /// </returns>
        /// <exception cref = "T:System.NotSupportedException">The <see cref = "T:System.Collections.Generic.ICollection`1"></see> is read-only.</exception>
        public bool Remove(T item)
        {
            return InternalDictionary.Remove(item);
        }

        /// <summary>
        ///   Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        ///   A <see cref = "T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<T> GetEnumerator()
        {
            return InternalDictionary.Keys.GetEnumerator();
        }

        /// <summary>
        ///   Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        ///   An <see cref = "T:System.Collections.IEnumerator"></see> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return InternalDictionary.Keys.GetEnumerator();
        }

        #endregion
    }


}


