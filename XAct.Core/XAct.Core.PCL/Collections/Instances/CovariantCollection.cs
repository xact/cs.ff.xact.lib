﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

namespace XAct.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// CovariantCollection is a collection   
    /// that allows for exposing collection properties as interfaces.
    /// <para>
    /// This is normally not allowed (a collection can't accept an Add(ISomething)).
    /// </para>
    /// <para>
    /// See Remarks
    /// </para>
    /// </summary>
    /// <remarks>
    /// Covariant lists are very useful to allow in cases
    /// when one has to match preexisting entity contracts:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// ICollection<IItem> x = (Collection<Item>)y;
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <typeparam name = "TExposedAs">
    /// The type of the exposed item.
    /// </typeparam>
    /// <typeparam name = "TInnerCollectionItem">
    /// The type of the wrapped collection item.
    /// </typeparam>
    [CollectionDataContract]
    //[Serializable]
    public class CovariantCollection<TExposedAs, TInnerCollectionItem> :
        ICollection<TExposedAs>, IHasInnerItemReadOnly, IHasIsReadOnly
    {
        //private bool _beenSet;

        private readonly ICollection<TInnerCollectionItem> _wrappedCollection;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref = "CovariantCollection&lt;TExposedAs, TInnerCollectionItem&gt;" /> 
        /// class.
        /// </summary>
        /// <param name = "collectionToWrap">The collection to wrap.</param>
        public CovariantCollection(ICollection<TInnerCollectionItem> collectionToWrap)
        {
            _wrappedCollection = collectionToWrap;
            //if (_wrappedCollection != null)
            //{
 //               _beenSet = true;
            //}
        }

        #endregion

        #region ICollection<TExposedAs> Members

        /// <summary>
        ///   Adds the specified item.
        /// </summary>
        /// <param name = "item">The item.</param>
        public void Add(TExposedAs item)
        {
            if (Equals(item, null))
            {
                throw new ArgumentNullException("item");
            }
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Convert the Interface back up to 
            //the instance type:
            TInnerCollectionItem origItem =
                ConvertInterfaceTypeBackToInnerCollectionType(item);

            _wrappedCollection.Add(origItem);
        }

        /// <summary>
        ///   Clears this instance.
        /// </summary>
        public void Clear()
        {
            _wrappedCollection.Clear();
        }

        /// <summary>
        ///   Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name = "item">The item.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(TExposedAs item)
        {
            if (Equals(item, null))
            {
                throw new ArgumentNullException("item");
            }
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            TInnerCollectionItem origItem =
                ConvertInterfaceTypeBackToInnerCollectionType(item);

            return _wrappedCollection.Contains(origItem);
        }

        /// <summary>
        ///   Copies to.
        /// </summary>
        /// <param name = "array">The array.</param>
        /// <param name = "arrayIndex">Index of the array.</param>
        public void CopyTo(TExposedAs[] array, int arrayIndex)
        {
            int pos = arrayIndex;
            foreach (object o in _wrappedCollection)
            {
                if (pos < array.Length)
                {
                    array[pos] = (TExposedAs) o;
                    pos++;
                    continue;
                }
                break;
            }
        }

        /// <summary>
        ///   Gets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get { return _wrappedCollection.Count; }
        }

        /// <summary>
        ///   Gets a value indicating whether this instance is read only.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is read only; otherwise, <c>false</c>.
        /// </value>
        public bool IsReadOnly
        {
            get { return _wrappedCollection.IsReadOnly; }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        ///   Removes the specified item.
        /// </summary>
        /// <param name = "item">The item.</param>
        /// <returns></returns>
        public bool Remove(TExposedAs item)
        {
            if (Equals(item, null))
            {
                throw new ArgumentNullException("item");
            }
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            TInnerCollectionItem origItem =
                ConvertInterfaceTypeBackToInnerCollectionType(item);

            return _wrappedCollection.Remove(origItem);
        }

        /// <summary>
        ///   Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<TExposedAs> GetEnumerator()
        {
            return _wrappedCollection.Cast<TExposedAs>().GetEnumerator();
        }

        /// <summary>
        ///   Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        ///   An <see cref = "T:System.Collections.IEnumerator" /> 
        /// object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _wrappedCollection.Cast<TExposedAs>().GetEnumerator();
        }

        #endregion

        #region Private Helper Methods

        private static TInnerCollectionItem
            ConvertInterfaceTypeBackToInnerCollectionType(TExposedAs item)
        {
            return
                (TInnerCollectionItem) Convert.ChangeType(item, typeof (TInnerCollectionItem),System.Globalization.CultureInfo.InvariantCulture);
        }

        #endregion

        #region IObjectWrapper<ICollection<TInnerCollectionItem>> Members

        T IHasInnerItemReadOnly.GetInnerItem<T>()
        {
            return _wrappedCollection.ConvertTo<T>(); 
            //set
            //{
            //    if (_beenSet)
            //    {
            //        throw new NotImplementedException("Can't set it once it's been set.");
            //    }
            //    _wrappedCollection = value;
            //    _beenSet = true;
            //}
        }

        #endregion
    }
}