﻿//using System.Globalization;

#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

// ReSharper disable CheckNamespace

namespace XAct.Collections
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// <para>
    /// Note that the wrapping entity must implement
    /// <see cref="IHasInnerItemReadOnly"/>
    /// (generally implemented Explicity)
    /// in order to provide access to the wraped (original)
    /// element.
    /// </para>
    /// <para>
    /// An excellent choice would be to enherit
    /// from <see cref="ObjectWrapperBase{TWrappedEntity}"/>
    /// as it would provide type equality based on the wrapped entity -- not the wrapepr -- out of the box.
    /// </para>
    /// </summary>
    /// <typeparam name = "TWrappedEntity">The type of the wrapped entity (eg: Contact).</typeparam>
    /// <typeparam name = "TEntityWrapper">The type of the wrapping entity (eg: ReadOnlyContact).</typeparam>
    /// <typeparam name = "TEntityWrapperInterface">The type of the wrapping entity interface (eg: IReadOnlyContact).</typeparam>
    public class ObjectWrapperCollection<TWrappedEntity, TEntityWrapper, TEntityWrapperInterface>
        :
            ICollection<TEntityWrapperInterface>, IHasIsReadOnly
        where TWrappedEntity : class
        where TEntityWrapper : class, IHasInnerItemReadOnly, TEntityWrapperInterface
    {
        //private static readonly CultureInfo _invariantCulture = CultureInfo.CurrentCulture;

        private readonly ICollection<TWrappedEntity> _wrappedCollection;

        #region ICollection<TEntityWrapperInterface> Members

        /// <summary>
        ///   Adds the specified item to the collection.
        /// </summary>
        /// <param name = "item">The item.</param>
        public void Add(TEntityWrapperInterface item)
        {
            //We can take this liberty:
            TEntityWrapper entityWrapper = item as TEntityWrapper;
            if (entityWrapper == null)
            {
                Resources.ErrMsgItemIsNotOfRightType.FormatStringCurrentCulture(item.GetType(), typeof (TEntityWrapper));
            }
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            TWrappedEntity wrappedEntity = entityWrapper.GetInnerItem<TWrappedEntity>();
            _wrappedCollection.Add(wrappedEntity);
        }

        /// <summary>
        ///   Clears this collection.
        /// </summary>
        public void Clear()
        {
            _wrappedCollection.Clear();
        }

        /// <summary>
        ///   Determines whether this collection contains the specified item.
        /// </summary>
        /// <param name = "item">The item.</param>
        /// <returns>
        ///   <c>true</c> if the collection containsthe specified item; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(TEntityWrapperInterface item)
        {
            //We can take this liberty:

            TEntityWrapper entityWrapper = item as TEntityWrapper;

            if (entityWrapper == null)
            {
                throw new ArgumentException(
                    Resources.ErrMsgItemIsNotOfRightType.FormatStringCurrentCulture(
                        item.GetType(),
                        typeof (TEntityWrapper)));
            }
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            TWrappedEntity wrappedEntity = entityWrapper.GetInnerItem<TWrappedEntity>();

            return _wrappedCollection.Contains(wrappedEntity);
        }

        /// <summary>
        ///   Copies collection to the given array.
        /// </summary>
        /// <param name = "array">The array.</param>
        /// <param name = "arrayIndex">Index of the array.</param>
        public void CopyTo(TEntityWrapperInterface[] array, int arrayIndex)
        {
            int pos = arrayIndex;
            foreach (object o in _wrappedCollection)
            {
                if (pos < array.Length)
                {
                    array[pos] = (TEntityWrapperInterface) o;
                    pos++;
                    continue;
                }
                break;
            }
        }

        /// <summary>
        ///   Gets the count of the number of items in the collection.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get { return _wrappedCollection.Count; }
        }

        /// <summary>
        ///   Gets a value indicating whether this instance is read only.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is read only; otherwise, <c>false</c>.
        /// </value>
        public bool IsReadOnly
        {
            get { return _wrappedCollection.IsReadOnly; }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        ///   Removes the specified item.
        /// </summary>
        /// <param name = "item">The item.</param>
        /// <returns></returns>
        public bool Remove(TEntityWrapperInterface item)
        {
            //We can take this liberty:
            TEntityWrapper entityWrapper = item as TEntityWrapper;
            if (entityWrapper == null)
            {
                throw new ArgumentException(
                    Resources
                        .ErrMsgItemIsNotOfRightType
                        .FormatStringExceptionCulture(
                            item.GetType(),
                            typeof (TEntityWrapper)));
            }
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            TWrappedEntity wrappedEntity = entityWrapper.GetInnerItem<TWrappedEntity>();
            return _wrappedCollection.Remove(wrappedEntity);
        }

        /// <summary>
        ///   Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<TEntityWrapperInterface> GetEnumerator()
        {
            IEnumerator<TWrappedEntity> innerIterator = _wrappedCollection.GetEnumerator();

            while (innerIterator.MoveNext())
            {
                TWrappedEntity wrappedEntity = innerIterator.Current;

                TEntityWrapperInterface entityWrapper = BuildEntityWrapper(wrappedEntity);

                yield return entityWrapper;
            }
        }

        /// <summary>
        ///   Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        ///   An <see cref = "T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            IEnumerator<TWrappedEntity> innerIterator = _wrappedCollection.GetEnumerator();

            while (innerIterator.MoveNext())
            {
                TWrappedEntity wrappedEntity = innerIterator.Current;

                Type entityWrapperType = typeof (TEntityWrapper);
                object[] constructorArgs = new object[] {wrappedEntity};
                TEntityWrapperInterface entityWrapper =
                    (TEntityWrapperInterface) Activator.CreateInstance(entityWrapperType, constructorArgs);

                yield return entityWrapper;
            }
        }

        #endregion

        private static TEntityWrapperInterface BuildEntityWrapper(TWrappedEntity wrappedEntity)
        {
            Type entityWrapperType = typeof (TEntityWrapper);
            object[] constructorArgs = new object[] {wrappedEntity};
            return (TEntityWrapperInterface) Activator.CreateInstance(entityWrapperType, constructorArgs);
        }

        #region Nested type: Resources

        /// <summary>
        ///   Constants used in this class.
        /// </summary>
        protected static class Resources
        {
            /// <summary>
            ///   Error Message that Item is not of right type.
            /// </summary>
            public static string ErrMsgItemIsNotOfRightType = XAct.Properties.Resources.ErrMsgItemIsNotOfRightType;
        }

        #endregion

        #region Constructor

        /// <summary>
        ///  Initializes a new instance of the 
        /// <see cref = "ObjectWrapperCollection&lt;TWrappedEntity, TEntityWrapper, TEntityWrapperInterface&gt;" />
        ///  class.
        /// </summary>
        /// <param name = "collectionToWrap">The collection to wrap.</param>
        public ObjectWrapperCollection(ICollection<TWrappedEntity> collectionToWrap)
        {
            _wrappedCollection = collectionToWrap;
        }

        #endregion
    }
}