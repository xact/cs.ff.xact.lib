﻿
namespace XAct.Collections
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Namespace of Collections required by the rest of the Library. 
    /// </summary>
    /// <remarks>
    /// Some of the Collections worth noting are:
    /// <para>
    /// The <see cref="ReadOnlyDictionary{TKey,TValue}"/>, which is useful 
    /// for holding configuration values.
    /// </para>
    /// <para>
    /// The <see cref="CovariantCollection{TExposedAs, TInnerCollectionItem}"/>
    /// is an interesting one. Normally you can't expose collections as
    /// <![CDATA[ICollection<ISomething>]]> because the collection can ensure that
    /// the ISomething is trully derives from the target Something... this collection
    /// gets around that nicely.
    /// </para>
    /// </remarks>
    /// <internal>
    /// This class is required by Sandcastle to document Namespaces: http://bit.ly/vqzRiK
    /// </internal>
    [CompilerGenerated]
    class NamespaceDoc { }
}
