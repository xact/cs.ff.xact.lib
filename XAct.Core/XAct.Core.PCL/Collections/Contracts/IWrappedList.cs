﻿// ReSharper disable CheckNamespace

namespace XAct.Collections
// ReSharper restore CheckNamespace
{
    using System.Collections;

    /// <summary>
    ///   Interface that a Collection
    ///   that wraps a collection implements.
    /// </summary>
    public interface IWrappedList
    {
        /// <summary>
        ///   Gets the wrapped list.
        ///   <para>
        ///     Note that this is usually
        ///     implemented Explicitly.
        ///   </para>
        /// </summary>
        /// <internal>
        ///   Implemented by <see cref = "XAct.Collections.WrappedList{TItem}" />
        /// </internal>
        /// <value>The inner list (or null).</value>
        IList InnerList { get; }
    }
}