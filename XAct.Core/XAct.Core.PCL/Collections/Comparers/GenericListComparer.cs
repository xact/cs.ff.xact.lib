
namespace XAct.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

    // ReSharper disable CheckNamespace
    /// <summary>
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    public class GenericSubListComparer<T> : IComparer<T>
    {
        #region Fields

        private readonly int[] _indexes;
        private readonly SortDirection _sortDirection = SortDirection.Ascending;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "GenericSubListComparer&lt;T&gt;" /> class.
        /// </summary>
        /// <param name = "indexesToSortBy">The indexes to sort by.</param>
        public GenericSubListComparer(params int[] indexesToSortBy)
            : this(SortDirection.Ascending, indexesToSortBy)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "GenericSubListComparer&lt;T&gt;" /> class.
        /// </summary>
        /// <param name = "sortDirection">The sort order.</param>
        /// <param name = "indexesToSortBy">The indexes to sort by.</param>
        public GenericSubListComparer(SortDirection sortDirection, params int[] indexesToSortBy)
        {
            _indexes = indexesToSortBy;
            _sortDirection = sortDirection;
        }

        #endregion

        #region IComparer<T> Members

        /// <summary>
        ///   Compares the specified x.
        /// </summary>
        /// <param name = "x">The x.</param>
        /// <param name = "y">The y.</param>
        /// <returns></returns>
        public int Compare(T x, T y)
        {
            int score = 0;
            foreach (int index in _indexes)
            {
                IComparable x1 = (IComparable) ((IList) x)[index];
                IComparable y1 = (IComparable) ((IList) y)[index];

                score = (_sortDirection.Equals(SortDirection.Ascending)) ? x1.CompareTo(y1) : y1.CompareTo(x1);

                if (score != 0)
                {
                    return score;
                }
            }
            return score;
        }

        #endregion
    }
}