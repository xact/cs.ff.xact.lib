namespace XAct.Collections
{
    using System.Collections;

    /// <summary>
    /// </summary>
    public class VersionComparer : IComparer
    {
        #region IComparer Members

        /// <summary>
        ///   Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name = "x">The first object to compare.</param>
        /// <param name = "y">The second object to compare.</param>
        /// <returns>
        ///   Value
        ///   Condition
        ///   Less than zero
        ///   <paramref name = "x" /> is less than <paramref name = "y" />.
        ///   Zero
        ///   <paramref name = "x" /> equals <paramref name = "y" />.
        ///   Greater than zero
        ///   <paramref name = "x" /> is greater than <paramref name = "y" />.
        /// </returns>
        /// <exception cref = "T:System.ArgumentException">
        ///   Neither <paramref name = "x" /> nor <paramref name = "y" /> implements the <see cref = "T:System.IComparable" /> interface.
        ///   -or-
        ///   <paramref name = "x" /> and <paramref name = "y" /> are of different types and neither one can handle comparisons with the other.
        /// </exception>
        public int Compare(object x, object y)
        {
            return QuickCompare(x, y);
        }

        #endregion

        private static int QuickCompare(object x, object y)
        {
            int[] x1 = (int[]) x;
            int[] y2 = (int[]) y;

            int r = 0;
            int iMax = x1.Length;
            if (y2.Length < iMax)
            {
                iMax = y2.Length;
            }
            if (iMax > 3)
            {
                iMax = 3;
            }
            for (int i = 0; i < iMax; i++)
            {
                r = ((x1[i] == y2[i]) ? 0 : ((x1[i] < y2[i]) ? -1 : +1));
                if (r != 0)
                {
                    return r;
                }
            }
            return r;
        }
    }
}