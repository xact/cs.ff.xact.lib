
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

// ReSharper disable CheckNamespace

namespace XAct.Collections
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections;

    /// <summary>
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    public class DescendingComparer<T> : IComparer
    {
        #region IComparer Members

        /// <summary>
        ///   Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name = "x">The first object to compare.</param>
        /// <param name = "y">The second object to compare.</param>
        /// <returns>
        ///   Value
        ///   Condition
        ///   Less than zero
        ///   <paramref name = "x" /> is less than <paramref name = "y" />.
        ///   Zero
        ///   <paramref name = "x" /> equals <paramref name = "y" />.
        ///   Greater than zero
        ///   <paramref name = "x" /> is greater than <paramref name = "y" />.
        /// </returns>
        /// <exception cref = "T:System.ArgumentException">
        ///   Neither <paramref name = "x" /> nor <paramref name = "y" /> implements the <see cref = "T:System.IComparable" /> interface.
        ///   -or-
        ///   <paramref name = "x" /> and <paramref name = "y" /> are of different types and neither one can handle comparisons with the other.
        /// </exception>
        public int Compare(object x, object y)
        {
            return QuickCompare(x, y);
        }

        #endregion

        private static int QuickCompare(Object x, Object y)
        {
            T p1 = (T) x;
            IComparable ic1 = (IComparable) p1;

            T p2 = (T) y;
            IComparable ic2 = (IComparable) p2;

            return (0 - ic1.CompareTo(ic2));
        }
    }
}