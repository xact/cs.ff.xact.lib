﻿namespace XAct.Collections.Comparers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FuncComparer<T> : IEqualityComparer<T>
    {
        private readonly Func<T, T, bool> _comparer;
        private readonly Func<T, object> _keyExtractor;

        // Allows us to simply specify the key to compare with: y => y.ID
        /// <summary>
        /// Initializes a new instance of the <see cref="FuncComparer&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="keyExtractor">The key extractor.</param>
        public FuncComparer(Func<T, object> keyExtractor) : this(keyExtractor, null) { }

        // Allows us to tell if two objects are equal: (x, y) => y.ID == x.ID
        /// <summary>
        /// Initializes a new instance of the <see cref="FuncComparer&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="comparer">The comparer.</param>
        public FuncComparer(Func<T, T, bool> comparer) : this(null, comparer) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FuncComparer&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="keyExtractor">The key extractor.</param>
        /// <param name="comparer">The comparer.</param>
        public FuncComparer(Func<T, object> keyExtractor, Func<T, T, bool> comparer)
        {
            this._keyExtractor = keyExtractor;
            this._comparer = comparer;
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type <c>T</c> to compare.</param>
        /// <param name="y">The second object of type <c>T</c> to compare.</param>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(T x, T y)
        {
            if (_comparer != null)
            {
                return _comparer(x, y);
            }
            object valX = _keyExtractor(x);
            if (valX is IEnumerable<object>)
            { // The special case where we pass a list of keys
                return ((IEnumerable<object>)valX).SequenceEqual((IEnumerable<object>)_keyExtractor(y));
            }
            return valX.Equals(_keyExtractor(y));
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// The type of <paramref name="obj"/> is a reference type and <paramref name="obj"/> is null.
        ///   </exception>
        public int GetHashCode(T obj)
        {
            if (_keyExtractor == null)
            {
                return obj.ToString().ToLower().GetHashCode();
            }
            object val = _keyExtractor(obj);
            if (val is IEnumerable<object>)
            { // The special case where we pass a list of keys
                return (int)((IEnumerable<object>)val).Aggregate((x, y) => x.GetHashCode() ^ y.GetHashCode());
            }
            return val.GetHashCode();
        }
    }
}
