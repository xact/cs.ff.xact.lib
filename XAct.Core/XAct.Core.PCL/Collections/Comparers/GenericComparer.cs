// ReSharper disable CheckNamespace

namespace XAct.Collections
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Reflection;

#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif


    /// <summary>
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    public class GenericComparer<T> : IComparer<T>
    {
        private readonly SortDirection _sortDirection = SortDirection.Ascending;
        private Collection<PropertyInfo> _propertyInfos;

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "GenericComparer&lt;T&gt;" /> class.
        /// </summary>
        /// <param name = "propertyNames">The property names.</param>
        public GenericComparer(params string[] propertyNames) : this(SortDirection.Ascending, propertyNames)
        {
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "GenericComparer&lt;T&gt;" /> class.
        /// </summary>
        /// <param name = "sortDirection">The sort order.</param>
        /// <param name = "propertyNames">The property names.</param>
        public GenericComparer(SortDirection sortDirection, params string[] propertyNames)
        {
            Initialize(propertyNames);
            _sortDirection = sortDirection;
        }

        #endregion

        #region IComparer<T> Members

        /// <summary>
        ///   Compares the specified x.
        /// </summary>
        /// <param name = "x">The x.</param>
        /// <param name = "y">The y.</param>
        /// <returns></returns>
        public int Compare(T x, T y)
        {
            int score = 0;
            foreach (PropertyInfo propInfo in _propertyInfos)
            {
                IComparable x1 = (IComparable) propInfo.GetValue(x, null);
                IComparable y1 = (IComparable) propInfo.GetValue(y, null);

                score = (_sortDirection.Equals(SortDirection.Ascending)) ? x1.CompareTo(y1) : y1.CompareTo(x1);

                if (score != 0)
                {
                    return score;
                }
            }
            return score;
        }

        #endregion

        private void Initialize(params string[] propertyNames)
        {
            _propertyInfos = new Collection<PropertyInfo>();
            Type type = typeof (T);
            foreach (string propertyName in propertyNames)
            {
                PropertyInfo propInfo = type.GetProperty(propertyName,
                                                         BindingFlags.Instance | BindingFlags.Public |
                                                         BindingFlags.IgnoreCase);
                if (propInfo == null)
                {
                    throw new ArgumentException("PropName not found.");
                }
                _propertyInfos.Add(propInfo);
            }
        }
    }
}