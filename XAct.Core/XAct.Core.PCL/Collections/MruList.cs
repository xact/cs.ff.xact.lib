﻿namespace XAct.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represent a collection,
    /// where the items are sorted according to the time they where
    /// last added/asked for from this collection (ie: "Most Recently Used").
    /// </summary>
    /// <internal>
    /// OLIVIER: This appears to be a stack...commands should maybe be 
    /// Push, Pop, Peek...plus your special MakeNewest...
    /// </internal>
    public class MRUList<TItem, TItemId> : IEnumerable<TItem>
    {

        /// <summary>
        /// A single element in the MRUList.
        /// </summary>
        public class MruItem
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="MRUList&lt;TItem, TItemId&gt;.MruItem"/> class.
            /// </summary>
            /// <param name="k">The k.</param>
            /// <param name="v">The v.</param>
            public MruItem(TItemId k, TItem v)
            {
                _key = k;
                _value = v;
            }

            /// <summary>
            /// Gets the key.
            /// </summary>
            public TItemId Key
            {
                get { return _key; }
            }
            private readonly TItemId _key;

            /// <summary>
            /// Gets the value.
            /// </summary>
            public TItem Value
            {
                get { return _value; }
            }
            private readonly TItem _value;
        }

        #region Fields

        // A delegate method to find the unique Id of the Item (something like i =>i.Id would do)
        private readonly Func<TItem, TItemId> _idMethod;

        //private readonly LinkedList<TItem> _linkedList;
        //private readonly Dictionary<TItemId, LinkedListNode<TItem>> _itemIndex;


        // The linked list of items in MRU order
        private readonly LinkedList<MruItem> _linkedList;

        // The dictionary of keys and nodes
        private readonly Dictionary<TItemId, LinkedListNode<MruItem>> _itemDictionary;

        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the add Newest/Latest items at start of the LinkedList.
        /// <para>
        /// The default is false, as it allows to iterate over items from the oldest to the newest, 
        /// which is beneficial in some cases -- such as looking for a way to break out of the iteration
        /// when the list item Count &lt;= someMaxValue.
        /// </para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if [add newest at start]; otherwise, <c>false</c>.
        /// </value>
        public bool AddNewestAtStart
        {
            get
            {
                return _addNewestAtStart;
            }
            set
            {
                if (Count != 0)
                {
                    throw new ArgumentException("Cannot change direction of MRUList if Count > 0.");
                }
                _addNewestAtStart = value;
            }
        }
        private bool _addNewestAtStart;

        /// <summary>
        /// Return the specified item if present in collection.
        /// <para>
        /// Invokes <c>MakeNewest{TItem}</c>
        /// </para>
        /// <para>
        /// An exception is thrown if not found.
        /// </para>
        /// </summary>
        /// <value>The item found.</value>
        /// <exception cref="ArgumentException">Exception raised if item not found.</exception>
        public TItem this[TItemId itemId]
        {
            get
            {
                return Find(itemId);
            }
        }


        /// <summary>
        /// Gets the number of items in the list.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get
            {
                return _linkedList.Count;
            }
        }
        #endregion

        private int _maxCapacity =-1;


        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MRUList{TItem,TItemId}"/> class.
        /// <para>
        /// Default is to Add items at 
        /// </para>
        /// </summary>
        public MRUList(Func<TItem, TItemId> anonymousMethodToRetrieveVertexIdentifier)
        {


            _idMethod = anonymousMethodToRetrieveVertexIdentifier;
            
            _linkedList = new LinkedList<MruItem>();
            _itemDictionary = new Dictionary<TItemId, LinkedListNode<MruItem>>();
        }

        #endregion



        #region Methods

        /// <summary>
        /// Determines whether the list contains the specified item.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>
        /// 	<c>true</c> if the contains the specified item; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(TItemId itemId)
        {
            return (!Equals(Find(itemId, false),default(TItem)));
        }

        /// <summary>
        /// Tries to the find the specified Vertex.
        /// Does not throw an error if not found.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <param name="throwAnExceptionIfNotFound">if set to <c>true</c> [throw an exception if not found].</param>
        /// <returns></returns>
        public TItem Find(TItemId itemId, bool throwAnExceptionIfNotFound=true)
        {

            LinkedListNode<MruItem> linkedListNode;

            if (!_itemDictionary.TryGetValue(itemId, out linkedListNode))
            {
                if (throwAnExceptionIfNotFound)
                {
                    throw new ArgumentException("item not found.");
                }
                return default(TItem);
            }

            //Found. So mark it as newest:
            MakeNewest(linkedListNode);

            return linkedListNode.Value.Value;
        }


        /// <summary>
        /// Brings the item to the End/Top of the List.
        /// <para>
        /// If the item is not already in the list, it is added.
        /// </para>
        /// </summary>
        /// <param name="item">The item.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the item is null.</exception>
        public void MakeNewest(TItem item)
        {
            if (item.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("item");
            }

            TItemId id = _idMethod(item);
// ReSharper disable RedundantNameQualifier
            if (Equals(Find(id, false), default(TItem)))
// ReSharper restore RedundantNameQualifier
            {
                //Was not found....add it to this list.
                this.Add(item);
            }
            else
            {
                MakeNewest(id);
            }
        }


        /// <summary>
        /// Ascends the item with the specified Id to the top of the internal LinkedList.
        /// i.e. put it in top of the MRU list
        /// </summary>
        /// <param name="itemId">The itemId.</param>
        public void MakeNewest(TItemId itemId)
        {
            LinkedListNode<MruItem> linkedListNode = _itemDictionary[itemId];

            MakeNewest(linkedListNode);
        }



        /// <summary>
        /// Removes the most recently used item (now at the beginning of the MRU List).
        /// </summary>
        public TItem RemoveNewest()
        {
            var result = Remove(PeekNewest());
            return result;
        }

        /// <summary>
        /// Returns the most recently used item (now at the beginning of the MRU List)
        /// without removing it from the list.
        /// </summary>
        /// <returns></returns>
        public TItem PeekNewest()
        {
            var result = GetLatest().Value.Value;
            return result;
        }

        /// <summary>
        /// Removes the most recently used item (now at the beginning of the MRU List).
        /// </summary>
        public IEnumerable<TItem> RemoveOldestWhileCount(int maxCount)
        {
            while (this.Count < maxCount)
            {
                var result = Remove(PeekOldest());
                yield return result;
            }
        }

        /// <summary>
        /// Removes the most recently used item (now at the beginning of the MRU List).
        /// </summary>
        public TItem RemoveOldest()
        {
            return Remove(PeekOldest());
        }

        /// <summary>
        /// Returns the oldest used item (now at the end of the MRU List)
        /// without removing it from the list.
        /// </summary>
        /// <returns></returns>
        public TItem PeekOldest()
        {
            var result = GetOldest().Value.Value;
            return result;
        }

        /// <summary>
        /// Adds the specified item in collction.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the item is null.</exception>
        /// <summary>
        /// Adds the specified time stamped item. (protected)
        /// </summary>
        /// <exception cref="ArgumentNullException">An exception is raised if the argument is null.</exception>
        public void Add(TItem item)
        {
            if (item.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("item");
            }

            TItemId id = _idMethod(item);


                // Check to see if the key is already in the dictionary
                if (_itemDictionary.ContainsKey(id))
                {
                    throw new ArgumentException("An item with the same key already exists.");
                }

                // If the list is at capacity, remove the LRU item.
                if (_itemDictionary.Count == _maxCapacity)
                {
                    RemoveOldest();
                }


                LinkedListNode<MruItem> linkedListNode = new LinkedListNode<MruItem>(new MruItem(id, item));

                _itemDictionary.Add(id, linkedListNode);

                AddAsNewest(linkedListNode);

        
        }




        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the item is null.</exception>
        public TItem Remove(TItem item)
        {
            if (item.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("item");
            }

            return Remove(_idMethod(item));
        }

        /// <summary>
        /// Removes the item with the specified Id.
        /// Returns true if the item has been effectively removed
        /// return false if the item wasn't in the collection
        /// </summary>
        /// <param name="itemId">The item's unique Id.</param>
        /// <returns></returns>
        public TItem Remove(TItemId itemId)
        {
            lock (this)
            {
                if (!_itemDictionary.ContainsKey(itemId))
                {
                    return default(TItem);
                }

                LinkedListNode<MruItem> linkedListNode= _itemDictionary[itemId];

                _linkedList.Remove(linkedListNode);
                _itemDictionary.Remove(itemId);

                MruItem mruItem = linkedListNode.Value;

                return mruItem.Value;
            }
        }



        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            _linkedList.Clear();
            _itemDictionary.Clear();
        }


        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<TItem> GetEnumerator()
        {
            return _linkedList.Select(mruItem => mruItem.Value).GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"></see> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _linkedList.GetEnumerator();
        }
        #endregion

        #region Protected Methods

        /// <summary>
        /// BringToTops the specified node. (protected)
        /// </summary>
        /// <param name="linkedListNode">The node.</param>
        /// <exception cref="ArgumentNullException">An exception is raised if the node is null.</exception>
        void MakeNewest(LinkedListNode<MruItem> linkedListNode)
        {
            linkedListNode.ValidateIsNotDefault("linkedListNode");
            //Move node to end of linked list (ie front):
            _linkedList.Remove(linkedListNode);

            //Add at beginning of List
            AddAsNewest(linkedListNode);

        }

        #endregion

        void AddAsNewest(LinkedListNode<MruItem> linkedListNode)
        {
            if (_addNewestAtStart)
            {
                _linkedList.AddFirst(linkedListNode);
            }
            else
            {
                _linkedList.AddLast(linkedListNode);
            }
        }


// ReSharper disable UnusedMember.Local
        void AddAsOldest(LinkedListNode<MruItem> linkedListNode)
// ReSharper restore UnusedMember.Local
        {
            if (_addNewestAtStart)
            {
                _linkedList.AddLast(linkedListNode);
            }
            else
            {
                _linkedList.AddFirst(linkedListNode);
            }
        }

        LinkedListNode<MruItem> GetLatest()
        {
            if (_addNewestAtStart)
            {
                return _linkedList.First;
            }
// ReSharper disable RedundantIfElseBlock
            else
// ReSharper restore RedundantIfElseBlock
            {
                return _linkedList.Last;
            }
        }
        LinkedListNode<MruItem> GetOldest()
        {
            if (_addNewestAtStart)
            {
                return _linkedList.Last;
            }
// ReSharper disable RedundantIfElseBlock
            else
// ReSharper restore RedundantIfElseBlock
            {
                return _linkedList.First;
            }
        }


        

    }
}
