﻿using System.Runtime.Serialization;

namespace XAct.Collections
{
    /// <summary>
    ///   Direction in which to sort Data.
    /// </summary>
    [DataContract]
    public enum SortDirection
    {
        /// <summary>
        /// No Direction Specified
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        ///   <para>
        ///     Value is 1.
        ///   </para>
        /// </summary>
        [EnumMember]
        Ascending = 1,

        /// <summary>
        ///   <para>
        ///     Value is 2.
        ///   </para>
        /// </summary>
        [EnumMember]
        Descending = 2,
    }
}