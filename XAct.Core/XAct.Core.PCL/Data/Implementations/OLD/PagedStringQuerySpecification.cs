﻿
//using System.Runtime.Serialization;

//namespace XAct.Data
//{
//    /// <summary>
//    /// A specialization of <see cref="PagedQuerySpecification"/> 
//    /// and 
//    /// implementation of <see cref="IPagedStringQuerySpecification"/>
//    /// <para>
//    /// IMPORTANT: Deprecated in favour of <see cref="PagedSearchTermQuerySpecification"/>
//    /// </para>
//    /// </summary>
//    /// <internal>
//    /// Just seemed so common a scenario, that I added it to Core
//    /// </internal>
//    [DataContract(Name = "PagedStringQuerySpecification", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
//    public class PagedStringQuerySpecification : PagedQuerySpecification, IPagedStringQuerySpecification
//    {
//        /// <summary>
//        /// Gets or sets the search term.
//        /// </summary>
//        /// <value>
//        /// The search term.
//        /// </value>
//        [DataMember(Name="SearchTerm")]
//        public string SearchTerm { get; set; }
//        /// <summary>
//        /// Gets or sets optional information to limit the search range.
//        /// </summary>
//        /// <value>
//        /// The search context.
//        /// </value>
//        [DataMember(Name = "SearchContext")]
//        public string SearchContext { get; set; }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="PagedStringQuerySpecification"/> class.
//        /// </summary>
//        /// <param name="searchTerm">The search term.</param>
//        /// <param name="searchContext">The search context.</param>
//        /// <param name="pageIndex">0 based Index of the page.</param>
//        /// <param name="pageSize">Size of the page.</param>
//        /// <param name="retrieveTotalRecords">if set to <c>true</c> [retrieve total records].</param>
//        public PagedStringQuerySpecification(string searchTerm, string searchContext=null, int pageIndex = 0, int pageSize = 20, bool retrieveTotalRecords = true):base(pageIndex,pageSize,retrieveTotalRecords)
//        {
//            SearchTerm = searchTerm;
//            SearchContext = searchContext;
//        }


//        /// <summary>
//        /// Returns a <see cref="System.String"/> that represents this instance.
//        /// </summary>
//        /// <returns>
//        /// A <see cref="System.String"/> that represents this instance.
//        /// </returns>
//        public override string ToString()
//        {
//            return
//                "PagedStringQuerySpecification(SearchTerm:{0},SearchCOntext:{1},PageIndex:{2},PageSize,{3},RetriveTotalRecords:{4}).{5}"
//                    .FormatStringCurrentCulture(SearchTerm, SearchContext, PageIndex, PageSize,
//                                                 RetrieveTotalRecordsInSource, base.ToString());
//        }
//    }
//}
