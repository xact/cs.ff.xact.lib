namespace XAct.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class DbRepositorySettings : IDbConnectionSettings
    {
        /// <summary>
        /// Gets or sets the name of the connection string settings.
        /// <para>Default value = 'Resources'</para>
        /// </summary>
        /// <value>
        /// The name of the connection string settings.
        /// </value>
        public string ConnectionStringSettingsName { get; set; }


        /// <summary>
        /// Gets or sets the parameter prefix.
        /// <para>Default value = '@'</para>
        /// </summary>
        /// <value>
        /// The parameter prefix.
        /// </value>
        public string ParameterPrefix { get; set; }
    }
}