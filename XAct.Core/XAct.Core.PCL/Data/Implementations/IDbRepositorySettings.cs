﻿
namespace XAct.Data
{
    /// <summary>
    /// Base settings for repositories stored in a db.
    /// </summary>
    public interface IDbConnectionSettings
    {
        /// <summary>
        /// Gets or sets the name of the connection string settings.
        /// <para>Default value = 'Resources'</para>
        /// </summary>
        /// <value>
        /// The name of the connection string settings.
        /// </value>
        string ConnectionStringSettingsName { get; set; }


        /// <summary>
        /// Gets or sets the parameter prefix.
        /// <para>Default value = '@'</para>
        /// </summary>
        /// <value>
        /// The parameter prefix.
        /// </value>
        string ParameterPrefix { get; set; }
    }
}
