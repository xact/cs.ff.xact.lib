﻿//namespace XAct.Data
//{
//    /// <summary>
//    /// 
//    /// 
//    /// </summary>
//    /// <internal>
//    /// Defined in XAct.Data as it is not a Domain entity, 
//    /// but a datastorage artifact.
//    /// </internal>
//    public class DistributedIdentity : IHasDistributedIdentities
//    {
//        /// <summary>
//        /// The unique Guid of the machine on which
//        /// the record was created.
//        /// <para>Member defined in<see cref="XAct.IHasDistributedIdentities"/></para>
//        /// </summary>
//        /// <value></value>
//        public string MachineId { get; set; }

//        /// <summary>
//        /// The unique Id of the record on the creator's machine.
//        /// <para>Member defined in<see cref="XAct.IHasDistributedIdentities"/></para>
//        /// </summary>
//        /// <value></value>
//        public int Id { get; set; }
//    }
//}
