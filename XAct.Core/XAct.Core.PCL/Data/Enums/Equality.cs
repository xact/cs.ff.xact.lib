﻿namespace XAct
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An enumeration to define the equality for comparisons across the wire.
    /// <para>
    /// Used by SearchTerm.
    /// </para>
    /// </summary>
    [DataContract(Name = "Equality",Namespace="http://xact-solutions.com/contracts/v0.1/xact")]
    public enum Equality
    {
        /// <summary>
        /// An error condition.
        /// <para>
        /// Value:0
        /// </para>
        /// </summary>
         [EnumMember(Value="Undefined")]
        Undefined = 0,

        /// <summary>
        /// A comparison for data type only.
        /// <para>
        /// Value:1
        /// </para>
        /// </summary>
         [EnumMember(Value = "DataTypeCheck")]
         DataTypeCheck = 1,

        /// <summary>
        /// A comparison for equality.
        /// <para>
        /// Value:2
        /// </para>
        /// </summary>
         [EnumMember(Value = "Equal")]
         Equal = 2,

        /// <summary>
        /// A comparison for non equality.
        /// <para>
        /// Value:3
        /// </para>
        /// </summary>
         [EnumMember(Value = "NotEqual")]
         NotEqual = 3,

        /// <summary>
        /// A comparison for less than or equal to.
        /// <para>
        /// Value:4
        /// </para>
        /// </summary>
         [EnumMember(Value = "LessThan")]
         LessThan = 4,

        /// <summary>
        /// A comparison for less than.
        /// <para>
        /// Value:5
        /// </para>
        /// </summary>
         [EnumMember(Value = "LessThanEqual")]
         LessThanEqual = 5,

        /// <summary>
        /// A comparison for greater than.
        /// <para>
        /// Value:6
        /// </para>
        /// </summary>
         [EnumMember(Value = "GreaterThan")]
         GreaterThan = 6,

        /// <summary>
        /// A comparison for greater than or equal to.
        /// <para>
        /// Value:7
        /// </para>
        /// </summary>
         [EnumMember(Value = "GreaterThanEqual")]
         GreaterThanEqual = 7
    }
}
