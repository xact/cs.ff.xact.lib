﻿using XAct.Entities;

namespace XAct
{
}


namespace XAct.Data.Updates
{
using System;
using System.Runtime.Serialization;


    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage is as follows:
    /// </para>
    /// <para>
    /// Seeding encounters something like:
    /// <code>
    /// <![CDATA[
    /// var environmentIdentifier = _hostSettingsService.Get<string>("EnvironmentIdentifier"); 
    /// var check = updateLogs.SingleOrDefault(x=>x.Name=="XYZ");
    /// if (check == null || (!check.Applied && check.IsValidEnvironment(environmentIdentifier))){
    ///    if (hostSettings.Get("EnvironmentIndentifier" == "XYZ")){
    ///      context.AddOrUpdate(...);
    ///    }
    ///    ...
    ///    updateLogs.Add(new UpdateLog("XYZ","Updated xyz...",true));
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    [DataContract]
    public class DataStoreUpdateLog : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasEnabled, IHasName, IHasDateTimeCreatedOnUtc, IHasDescription
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets an optional CSV list of the environments 
        /// this update applies to.
        /// <para>
        /// The default is string.Empty (ie, all environments),
        /// but you can provide values that align up with
        /// the HostSetting's  "EnvironmentIdentifier" value 
        /// (eg: "ST", "UAT,PP,PROD", etc.)
        /// </para>
        /// </summary>
        /// <value>
        /// The environment identifier.
        /// </value>
        [DataMember]
        public virtual string EnvironmentIdentifier { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets the name of the identifier.
        /// <para>
        /// The Name is a displayable text identifier.
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Name { get; set; }


        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        ///   <internal>
        /// As to why its Nullable: sometimes the contract is applied to items
        /// that are not Entities themselves, but pointers to objects that are not known
        /// if they are
        ///   </internal>
        ///   <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        ///   </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }


        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }

        
        /// <summary>
        /// Initializes a new instance of the <see cref="DataStoreUpdateLog"/> class.
        /// </summary>
        public DataStoreUpdateLog()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
            
        }

    }

}
