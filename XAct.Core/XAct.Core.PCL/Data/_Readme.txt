﻿I don't like much the idea of having XAct.Data namespace in Core,
because it gives the wrong impression that's ok to use Data stuff
up in the UI layer.

It's not. It's just that queries to Domain Repositories need some
paging definition in order to not send too much over the wire.
And yet remain agnostic to System.Data..at least Db.


