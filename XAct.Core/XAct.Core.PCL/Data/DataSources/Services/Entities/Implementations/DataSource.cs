﻿namespace XAct.Data.DataSources
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Implementation of <see cref="IDataSource"/>
    /// <para>
    /// A datasource defines the connection info required
    /// to connect to a remote datastore, and pick up the
    /// data itself.
    /// </para>
    /// <para>
    /// It is meant to be minimal in size.
    /// </para>
    /// <para>
    /// YUML.ME: 
    /// <![CDATA[
    /// [Vertex]*-1[DataSource]
    /// ]]>
    /// </para>
    /// </summary>
    /// <internal>
    /// Cannot attach metadata, as Metadata either requires
    /// .NET35FULL, or .NET40FULL. As this is is an edge case UI
    /// problem, will deal with it with other strategies rather than forcing
    /// the whole website to shift
    /// </internal>
    [DataContract]
    public class DataSource : IHasXActLibEntity, IDataSource, IHasDistributedGuidIdAndTimestamp
    {

        /// <summary>
        /// Gets or sets the Datastore Id for this DataStore entity.
        /// </summary>
        /// <remarks>
        /// <para>
        /// I've opted for Guid as it simplifies distributed
        /// development.
        /// </para>
        /// </remarks>
        /// <value>
        /// The id.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// The recognizeable, name of the DataSource descriptor
        /// <para>Eg: 'Contacts'</para>
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        [DataMember]
        public virtual string Name { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="DataSource"/> is local (same db) or remote.
        /// <para>
        /// If local, only the <see cref="DataStoreTableName"/> is required.
        /// </para>
        /// <para>
        /// If the data is remote/another datasource, all the Connector properties are needed in order
        /// to open a connection, and all the DataStor properties are needed, in order to get to the right database, table
        /// and Attributes within the remote service end point.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual bool IsRemote { get; set; }

        /// <summary>
        /// The type of Connector (a Convention/Wellknown string)
        /// <para>Eg: 'LocalAppDb', 'RemoteDb', 'Facebook', etc.</para>
        /// </summary>
        [DataMember]
        public virtual string ConnectorClassification { get; set; }



        /// <summary>
        /// The <see cref="Type"/> 
        /// dependent name of the Provider
        /// <para>In the case of Type=Db, this would 
        /// the Type of DbProviderType (eg: SqlServerProvider, etc.).</para>
        /// </summary>
        [DataMember]
        public virtual string ConnectionProviderType { get; set; }

        /// <summary>
        /// The connection string to the specified <see cref="ConnectionProviderType"/>
        /// </summary>
        [DataMember]
        public virtual string ConnectionString { get; set; }


        /// <summary>
        /// Gets or sets any optional data that the Connector might need to connect to the remote datasource.
        /// <para>
        /// The data must be saved as an Xml file.
        /// </para>
        /// 	<para>
        /// 		<![CDATA[
        /// <?xml version="1.0" encoding="utf-8" ?>
        /// <dataSource><refreshRate>00:00:30</refreshRate></dataSource>
        /// ]]>
        /// 	</para>
        /// </summary>
        /// <value>
        /// The connection data.
        /// </value>
        [DataMember]
        public virtual string ConnectionMetaData { get; set; }

        /// <summary>
        /// The name of the Catalog/Db
        /// <para>
        /// eg: 'PIM'
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string DataStoreCatalogName { get; set; }

        /// <summary>
        /// The name of the Table within the Catalog/Db.
        /// <para>
        /// eg: 'Address'
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string DataStoreTableName { get; set; }


        /// <summary>
        /// A convention based serialized list of the column names used to uniquely identify
        /// a record in the above DataStore TableName.
        /// <para>eg: "ID", or "MachineName;DateTimeUtc"</para>
        /// </summary>
        [DataMember]
        public virtual string DataStoreSerializedIdentifierNames { get; set; }




        /// <summary>
        /// Optional serialized information to help rendering remote information.
        /// <para>
        /// eg: {"Icon":"Contacts.png"}
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string DisplayMetaData { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="DataSource"/> class.
        /// </summary>
        public DataSource()
        {
            this.GenerateDistributedId();
        }
    }
}
