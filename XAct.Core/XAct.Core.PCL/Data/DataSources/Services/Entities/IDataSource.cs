﻿namespace XAct.Data.DataSources
{
    using System;

    /// <summary>
    /// The contract for the description of a single DataSource.
    /// <para>
    /// A DataSource can be a Db, 
    /// or something more complex.
    /// </para>
    /// </summary>
    public interface IDataSource : IHasName
    {

        /// <summary>
        /// The recognizeable, name of the DataSource descriptor
        /// <para>Eg: 'Contacts'</para>
        /// </summary>
        new string Name { get; set; }

        /// <summary>
        /// The type of Connector (a Convention/Wellknown string)
        /// <para>Eg: 'Db', 'Facebook', etc.</para>
        /// </summary>
        string ConnectorClassification { get; set; }


        /// <summary>
        /// The <see cref="Type"/> dependent name of the Provider
        /// <para>In the case of Type=Db, this would the Type of DbProviderType (eg: SqlServerProvider, etc.).</para>
        /// </summary>
        string ConnectionProviderType { get; set; }


        /// <summary>
        /// The connection string to the specified <see cref="ConnectionProviderType"/>
        /// </summary>
        string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets any additional data that the Connector might need to connect to the remote datasource.
        /// <para>
        /// The serialization format is determined by convention (JSON is common).
        /// </para>
        /// <para>
        /// <![CDATA[
        /// <?xml version="1.0" encoding="utf-8" ?>
        /// <dataSource><refreshRate>00:00:30</refreshRate></dataSource>
        /// ]]>
        /// </para>
        /// </summary>
        /// <value>
        /// The connection data.
        /// </value>
        string ConnectionMetaData { get; set; }

        /// <summary>
        /// The name of the Catalog/Db
        /// <para>
        /// eg: 'PIM'
        /// </para>
        /// </summary>
        string DataStoreCatalogName { get; set; }

        /// <summary>
        /// The name of the Table within the Catalog/Db.
        /// <para>
        /// eg: 'Address'
        /// </para>
        /// </summary>
        string DataStoreTableName { get; set; }

        /// <summary>
        /// A serialized list of the column names used to uniquely identify
        /// a record in the above DataStore TableName.
        /// <para>eg: "ID", or "Machine;DateTime", or "FirstName:string|LastName:string"</para>
        /// </summary>
        string DataStoreSerializedIdentifierNames { get; set; }




        /// <summary>
        /// Information to help with the display.
        /// <para>
        /// eg: {"Icon":"Contacts.png"}
        /// </para>
        /// </summary>
        string DisplayMetaData { get; set; }

    }
}



