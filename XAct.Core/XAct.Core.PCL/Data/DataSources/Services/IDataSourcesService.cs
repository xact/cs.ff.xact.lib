﻿namespace XAct.Data.DataSources
{
    using System;

    /// <summary>
    /// Contract for a service to manage <see cref="IDataSource"/>s.
    /// </summary>
    public interface IDataSourcesService : IHasXActLibService
    {
        /// <summary>
        /// Gets the data source connector.
        /// </summary>
        /// <param name="dataSourceId">The data source id.</param>
        /// <returns></returns>
        IDataSource GetDataSourceConnector(Guid dataSourceId);
    }
}
