﻿namespace XAct.Data.DataSources
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// An implementation of 
    /// <see cref="IHasDataSourceIdentifier"/>
    /// </summary>
    [DataContract]
    public class DataSourceIdentifier : IHasDataSourceIdentifierReadOnly
    {


        /// <summary>
/// Initializes a new instance of the <see cref="DataSourceIdentifier"/> class.
/// </summary>
        public DataSourceIdentifier()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceIdentifier"/> class.
        /// </summary>
        /// <param name="dataSourceId">The data source id.</param>
        /// <param name="serializedIdentities">The identities.</param>
        public DataSourceIdentifier(Guid dataSourceId, string serializedIdentities)
        {
            Initialize(dataSourceId, serializedIdentities);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceIdentifier"/> class.
        /// </summary>
        /// <param name="dataSourceId">The data source id.</param>
        /// <param name="serializedIdentities">The identities.</param>
        public void Initialize(Guid dataSourceId, string serializedIdentities)
        {
            if (dataSourceId.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("dataSourceId");
            }

            if (serializedIdentities.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("serializedIdentities");
            }

            _sourceId = dataSourceId;
            _serializedIdentities = serializedIdentities;
        }


        /// <summary>
        /// A unique identifier for the datasource (eg: the XXX Table in YYY Db in ZZZ ConnectionString)
        /// </summary>
        /// <remarks>
        /// The Guid (rather than a shorter Int) allows developers to develop offline, 
        /// rather than having to first reserve a DataSource Id, centrally.
        /// </remarks>
        public Guid DataSourceId
        {
            get { return _sourceId; }
        }
        [DataMember]
        private Guid _sourceId;

        /// <summary>
        /// The unique identifiers for the data object in the source XXX table.
        /// <para>
        /// In most cases, I expect it be just a single serialized int/Guid (ie, ToString()).
        /// </para>
        /// </summary>
        public string DataSourceSerializedIdentities { get { return _serializedIdentities; } }
        [DataMember]
        private string _serializedIdentities;

    }
}