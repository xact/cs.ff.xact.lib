﻿namespace XAct.Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using XAct.Messages;

    /// <summary>
    /// A single page of data from a larger enumerable collection.
    /// </summary>
    /// <typeparam name="TItem">The type of data.</typeparam>
    public class CollectionPage<TItem> : Collection<TItem>, IPageable
    {
        #region IPagedList Members

        /// <summary>
        /// Gets or sets the total count of all records.
        /// </summary>
        /// <value>The total count.</value>
        public int TotalCount
        {
            get { return _totalCount; }
            set { ThrowNotImplemented("TotalCount", value); }
        }

        private readonly int _totalCount;

        /// <summary>
        /// Gets the current page index (1 based, due to most used bye UI).
        /// </summary>
        /// <value></value>
        public int PageIndex
        {
            get { return _pageIndex; }
            set { ThrowNotImplemented("PageIndex", value); }
        }

        private readonly int _pageIndex;

        /// <summary>
        /// Gethe current page size.
        /// </summary>
        /// <value></value>
        public int PageSize
        {
            get { return _pageSize; }
            set { ThrowNotImplemented("PageSize", value); }
        }

        private readonly int _pageSize;

        /// <summary>
        /// A boolean indicating whether there is a previous page
        /// (ie, PageIndex &gt; 1)
        /// </summary>
        /// <value></value>
        public bool HasPreviousPage
        {
            get { return _hasPreviousPage; }
        }

        private readonly bool _hasPreviousPage;

        /// <summary>
        /// A flag indicating whether there is a later page to switch to.
        /// </summary>
        /// <value></value>
        public bool HasNextPage
        {
            get { return _hasNextPage; }
        }

        private readonly bool _hasNextPage;

        /// <summary>
        /// Gets the total number of pages.
        /// </summary>
        /// <value></value>
        public int TotalPages
        {
            get { return _numberOfPages; }
        }

        private readonly int _numberOfPages;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionPage&lt;TItem&gt;"/> class.
        /// </summary>
        /// <param name="source">The enunerable source data.</param>
        /// <param name="index">The index.</param>
        /// <param name="pageSize">Size of the page.</param>
        public CollectionPage(IQueryable<TItem> source, int index, int pageSize)
        {
            //Get the count of *all* matching records before getting just a page of them.
            _totalCount = source.Count();
            _pageIndex = index;
            _pageSize = pageSize;
            _numberOfPages = ((TotalCount/PageSize) + 1);
            _hasPreviousPage = (PageIndex > 1);
            _hasNextPage = ((_pageIndex)*_pageSize) < TotalCount;

            foreach (TItem item in source.Skip((_pageIndex-1)*_pageSize).Take(_pageSize))
            {
                this.Add(item);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionPage&lt;TItem&gt;"/> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="index">The index.</param>
        /// <param name="pageSize">Size of the page.</param>
        public CollectionPage(IEnumerable<TItem> source, int index, int pageSize)
        {
            this.TotalCount = source.Count();

            _pageIndex = index;
            _pageSize = pageSize;
            _numberOfPages = ((TotalCount/PageSize) + 1);
            _hasPreviousPage = (PageIndex > 1);
            _hasNextPage = (_pageIndex*_pageSize) < TotalCount;

            foreach (TItem item in source.Skip((_pageIndex-1) *_pageSize).Take(_pageSize))
            {
                this.Add(item);
            }
        }

        #endregion

        #region Private Methods 

        /// <summary>
        /// Throws an exception explaining that this 
        /// is just a Page of some original data source, and
        /// one cannot page through it.
        /// </summary>
        /// <param name="argumentName">Name of the argument.</param>
        /// <param name="argumentValue">Argument Value.</param>
        /// <exception cref="NotImplementedException"></exception>
// ReSharper disable UnusedParameter.Local
        private static void ThrowNotImplemented(string argumentName, object argumentValue)
// ReSharper restore UnusedParameter.Local
        {
            throw new NotImplementedException("This is a readonly Collection.");
        }

        #endregion
    }
}