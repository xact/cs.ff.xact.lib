﻿namespace XAct
{
    /// <summary>
    /// Elegant work around to .NET not having a 
    /// means to define a generic property.
    /// </summary>
    /// <remarks>
    /// Usage:
    /// <code>
    /// <![CDATA[
    /// class SomeClass {
    ///   public MyProp<int> SomeProperty { get; set; }
    /// }
    /// ]]></code>
    /// Which you can then use as follows:
    /// <code>
    /// <![CDATA[
    /// SomeClass instance = new SomeClass();
    /// instance.SomeProperty = 32;
    /// int someInt = instance.SomeProperty;
    /// ]]></code>
    /// </remarks>
    /// <internal>
    /// See: http://stackoverflow.com/a/2587293
    /// </internal>
    /// <typeparam name="T"></typeparam>
    public class GenericProperty<T>
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public T Value { get; set; }

        /// <summary>
        /// Implicit operator to retrieve not the <see cref="GenericProperty{T}"/>
        /// but its <see cref="Value"/>
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static implicit operator T(GenericProperty<T> value)
        {
            return value.Value;
        }

        /// <summary>
        /// Implicit operator to set not the <see cref="GenericProperty{T}"/>
        /// but its <see cref="Value"/>
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static implicit operator GenericProperty<T>(T value)
        {
            return new GenericProperty<T> { Value = value };
        }
    }
}

