﻿namespace XAct.Settings
{
    using XAct.Settings;

    /// <summary>
    /// Contract for a service to retrieve Key/Value settings fromthe Host computers settings.
    /// </summary>
    public interface IHostSettingsService : IKeySettingsService, IHasXActLibService
    {

        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IHostSettingsServiceConfiguration Configuration { get; }
    }
}