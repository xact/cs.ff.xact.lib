﻿namespace XAct.Settings
{
    /// <summary>
    /// Contract for a singleton argument package
    /// to configure implementations of 
    /// <see cref="IHostSettingsService"/>
    /// </summary>
    public interface IHostSettingsServiceConfiguration : IHasXActLibServiceConfiguration, IHasInitialize
    {
        
    }
}