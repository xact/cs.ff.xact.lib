﻿namespace XAct.Settings
{

    /// <summary>
    /// The contract for a Service that returns 
    /// readonly Application Settings, common to 
    /// all instances of the application, irrespective
    /// of the load balanced Host it is running on.
    /// <para>
    /// Very often, the service is implemented using
    /// <c>XAct.Settings.ApplicationSettings.AppSettings</c>
    /// </para>
    /// <para>
    /// Important: although the notion of ApplicationSettings
    /// are umbiqutious, there are several disadvantages
    /// to using ApplicationSettings versus 
    /// <c>ProfileSettings</c> (that *can* includ readonly
    /// settings, which is the same as ApplicationSettings).
    /// One of these is that Application Settings
    /// are not serializeable (a separeate mechanism
    /// has to be created to push them to a RIA client)
    /// and are more an old school web server, or WinForm
    /// concept.
    /// </para>
    /// </summary>
    public interface IKeySettingsService : IHasXActLibService
    {
        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        TValue Get<TValue>(string key, bool throwExceptionOnConversionException = true);

        /// <summary>
        /// Gets the Typed Application setting matching the given key.
        /// <para>
        /// If the result is null, <paramref name="defaultValue"/>.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value if no value was found.</param>
        /// <param name="throwExceptionOnConversionException">if set to <c>true</c> [throw exception on conversion exception].</param>
        /// <returns></returns>
        TValue Get<TValue>(string key, TValue defaultValue, bool throwExceptionOnConversionException = true);
    }

}