﻿namespace XAct
{
    using System;
    using XAct.Diagnostics;

    /// <summary>
    /// Shortcuts for common XActLib operations.
    /// </summary>
    public static class Shortcuts
    {
        /// <summary>
        /// Activates the specified Type 
        /// using the <see cref="IDependencyResolver"/>
        /// if it is an interface, or Activator if it is a Type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        public static T ActivateEx<T>(Type t)
            where T : class 
        {
            try
            {
                object o = t.IsInterface ? DependencyResolver.Current.GetInstance(t) : Activator.CreateInstance(t);
                return o as T; // as TClassContract
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Traces the specified message to the current <see cref="ITracingService" /> as a Verbose message.
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public static void Trace(TraceLevel traceLevel, string message, params object[] args)
        {

            //Log completion:
            if (XAct.DependencyResolver.BindingResults == null)
            {
                return;
            }
            if (!XAct.DependencyResolver.BindingResults.IsInitialized)
            {
                return;

            }
            ITracingService tracingService =
                DependencyResolver.Current.GetInstance<ITracingService>(false);

            if (tracingService != null)
            {
                return;
            }
            XAct.DependencyResolver.Current.GetInstance<ITracingService>()
                    .Trace(
                        traceLevel, message, args);
        }


        /// <summary>
        /// Traces the exception.
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public static void TraceException(TraceLevel traceLevel, Exception exception, string message, params object[] args)
        {
            if (XAct.DependencyResolver.BindingResults == null)
            {
                return;
            }
            if (!XAct.DependencyResolver.BindingResults.IsInitialized)
            {
                return;

            }
            ITracingService tracingService =
                DependencyResolver.Current.GetInstance<ITracingService>(false);

            if (tracingService != null)
            {
                return;
            }

            tracingService.TraceException(traceLevel, exception, message, args);
        }



        /// <summary>
        /// Returns the implementation of the given interface.
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface.</typeparam>
        /// <param name="tag">The tag.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        public static TInterface ServiceLocate<TInterface>(string tag = null, bool raiseExceptionIfNotFound =true)
        {
            return tag.IsNullOrEmpty()
                       ? DependencyResolver.Current.GetInstance<TInterface>(raiseExceptionIfNotFound)
                       : DependencyResolver.Current.GetInstance<TInterface>(tag, raiseExceptionIfNotFound);
        }


        /// <summary>
        /// Returns the implementation of the given interface.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        public static object ServiceLocate(Type interfaceType, string tag = null, bool raiseExceptionIfNotFound = true)
        {
            return tag.IsNullOrEmpty()
                       ? DependencyResolver.Current.GetInstance(interfaceType, raiseExceptionIfNotFound)
                       : DependencyResolver.Current.GetInstance(interfaceType, tag, raiseExceptionIfNotFound);
        }

    }
}