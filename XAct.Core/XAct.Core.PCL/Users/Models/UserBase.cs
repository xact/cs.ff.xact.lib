namespace XAct.Users
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// An application user.
    /// <para>
    /// Note: it is very important to understand that there is a distinction 
    /// between <see cref="User"/> -- which is a Business Domain entity,
    /// containing properties specific to a user in the context of the business (eg: SS#, etc.),
    /// and <see cref="User"/>, which is a Technical Domain entity
    /// used to track the <c>UserMembership</c> in terms of authentication (2FEnabled, 2FContactType, 2FContactInfo, 2FInfoConfirmed, AttemptsCounter,LockedoutUntil, etc.)
    /// </para>
    /// <para>
    /// Why not include those settings in the
    /// </para>
    /// </summary>
    [DataContract]
#pragma warning disable 1591
    public abstract class UserBase<TUser> : IHasDistributedGuidIdAndTimestamp, IHasEnabled, IHasAuditability
        where TUser:UserBase<TUser>
#pragma warning restore 1591
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// The unique name of the User.
        /// <para>
        /// Note: this is not the display name, nor the email.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets the properties that belong to this user.
        /// </summary>
        /// <value>
        /// The properties.
        /// </value>
        public virtual ICollection<UserProperty> Properties { get { return _properties ?? (_properties = new Collection<UserProperty>()); } }
        [DataMember]
        private ICollection<UserProperty> _properties;


        /// <summary>
        /// Gets the properties that belong to this user.
        /// </summary>
        /// <value>
        /// The properties.
        /// </value>
        public virtual ICollection<UserClaim> Claims { get { return _claims ?? (_claims = new Collection<UserClaim>()); } }
        [DataMember]
        private ICollection<UserClaim> _claims;


        public virtual ICollection<TUser> AllowedDelegates { get { return _delegates ?? (_delegates = new Collection<TUser>()); } }
        [DataMember]
        private ICollection<TUser> _delegates;


        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        ///   <internal>
        /// As to why its Nullable: sometimes the contract is applied to items
        /// that are not Entities themselves, but pointers to objects that are not known
        /// if they are
        ///   </internal>
        ///   <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        ///   </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy" /></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public virtual string CreatedBy { get; set; }

        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// <para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? LastModifiedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the identity who Modified the document.
        /// <para>Member defined in<see cref="IHasDateTimeModifiedBy" /></para>
        /// </summary>
        /// <value>
        /// The Modified by.
        /// </value>
        [DataMember]
        public virtual string LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the datetime the item was deleted, expressed in UTC.
        /// <para>
        /// Defined in the <see cref="XAct.IHasDateTimeTrackabilityUtc" /> contract.
        /// </para>
        /// <para>
        /// Note: Generally updated by a db trigger, or saved first, then deleted.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? DeletedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets information as to whom deleted the record.
        /// <para>Member defined in<see cref="IHasDateTimeDeletedBy" /></para>
        /// </summary>
        /// <value>
        /// The deleted by.
        /// </value>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        ///   </para>
        [DataMember]
        public virtual string DeletedBy { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        protected UserBase()
        {
            this.GenerateDistributedId();
            this.Enabled = true;
        }



    }
}