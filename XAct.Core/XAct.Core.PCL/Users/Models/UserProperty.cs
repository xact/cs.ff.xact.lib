namespace XAct.Users
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A property that belongs to a <see cref="User"/>
    /// </summary>
    [DataContract]
    public class UserProperty : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasKey, IHasSerializedTypeValueAndMethod, IHasDateTimeCreatedBy, IHasDateTimeModifiedBy
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }




        /// <summary>
        /// The FK to the User record.
        /// </summary>
        [DataMember]
        public virtual Guid UserFK { get; set; }


        /// <summary>
        /// Gets or sets the key.
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }


        /// <summary>
        /// Gets or sets the serialization method.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The serialization method.
        /// </value>
        [DataMember]
        public virtual SerializationMethod SerializationMethod { get; set; }
        
        /// <summary>
        /// Gets or sets the Assembly qualified name of the Value that is serialized.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValueType { get; set; }
        /// <summary>
        /// Gets or sets the serialized value.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValue { get; set; }




        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        ///   <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        ///   </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy" /></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public virtual string CreatedBy { get; set; }

        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// <para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual DateTime? LastModifiedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the identity who Modified the document.
        /// <para>Member defined in<see cref="IHasDateTimeModifiedBy" /></para>
        /// </summary>
        /// <value>
        /// The Modified by.
        /// </value>
        [DataMember]
        public virtual string LastModifiedBy { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserProperty"/> class.
        /// </summary>
        public UserProperty()
        {
            this.GenerateDistributedId();
            //this.Enabled = true;
        }

    }
}