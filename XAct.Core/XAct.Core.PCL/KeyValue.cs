﻿namespace XAct
{
    using System.Runtime.Serialization;

    /// <summary>
    /// A serializable KeyValue pair.
    /// </summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    [DataContract]
    public class KeyValue<TKey, TValue>
    {
        /// <summary>
        /// The Key.
        /// </summary>
        [DataMember]
        public TKey Key { get; set; }



        /// <summary>
        /// The value.
        /// </summary>
        [DataMember]
        public TValue Value { get; set; }
    }
}