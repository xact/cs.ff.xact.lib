﻿namespace XAct.Categorization
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Messages;

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Namespace = XAct.Library.Constants.DataContracts.BaseDataContractNamespace)]
    public class TagCategory : ReferenceDataGuidIdBase,
        IHasXActLibEntity, 
        IHasApplicationTennantIdSpecificDistributedGuidIdReferenceData
    {

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="TagCategory"/> class.
        /// </summary>
        public TagCategory()
        {
            this.GenerateDistributedId();

            this.Enabled = true;
        }

    }
}
