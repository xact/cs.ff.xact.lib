﻿namespace XAct
{
    using System.Runtime.Serialization;

    [DataContract]
    public enum Progress
    {
        /// <summary>
        /// Failed
        /// <para>
        /// Value = 0.
        /// </para>
        /// <para>
        /// This is one of the rare cases 
        /// where I don't set Undefined to 0,
        /// rather than the first Enum value.
        /// So that if forgotten to be set, it's
        /// considered an Error message. 
        /// </para>
        /// </summary>
        [EnumMember(Value = "0")]
        Fail = 0,

        /// <summary>
        /// Warn
        /// <para>
        /// Value = 1.
        /// </para>
        /// </summary>
        [EnumMember(Value = "1")]
        Warn = 1,

        /// <summary>
        /// Success
        /// <para>
        /// Value = 2.
        /// </para>
        /// </summary>
        [EnumMember(Value = "2")]
        Success = 2,

        /// <summary>
        /// InProgress
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember(Value = "1")]
        Pending = 1,

        /// <summary>
        /// InProgress
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember(Value = "2")]
        Error = 2,

        /// <summary>
        /// InProgress
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember(Value = "3")]
        Paused = 3,

        /// <summary>
        /// InProgress
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember(Value = "4")]
        InProgress = 4,

        /// <summary>
        /// InProgress
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember(Value = "5")]
        Completed = 5

    }
}