﻿namespace XAct.Enums
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Enumeration of types of auditable events.
    /// </summary>
    [Flags]
    [DataContract(Name = "AuditableEvent", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/")]
    public enum AuditableEvent
    {
        /// <summary>
        /// Undefined.
        /// <para>
        /// An error condition.
        /// </para>
        /// <para>
        /// Value = 0
        /// </para>
        /// </summary>
        Undefined=0,

        /// <summary>
        /// Created.
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        Created = 1,

        /// <summary>
        /// Accessed.
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        Accessed = 2,

        /// <summary>
        /// Modified/Updated.
        /// <para>
        /// Value = 4
        /// </para>
        /// </summary>
        Modified = 4,

        //Disabled=8,

        /// <summary>
        /// Deleted
        /// <para>
        /// Value = 16
        /// </para>
        /// </summary>
        Deleted=16,
    }
}
