﻿
namespace XAct
{
    using System.Runtime.Serialization;
    using XAct.Messages;

    /// <summary>
    /// 
    /// <para>
    /// Note that the values used
    /// are the same as <see cref="XAct.Diagnostics.TraceLevel"/>
    /// </para>
    /// </summary>
    [System.Runtime.Serialization.DataContract]
    public enum Severity : byte
    {

        /// <summary>
        /// The Severity was not set.
        /// <para>
        /// Value = 0
        /// </para>
        /// <para>
        /// This is an error condition.
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined =0,

        /// <summary>
        /// THe <see cref="Message"/>
        /// is an <c>Error</c>.
        /// <para>
        /// Operation did not complete.
        /// </para>
        /// <para>
        /// Loosely equivalent to HTTP 5xx series 
        /// (Server error messages) and some of the HTTP 4xx Series (AuthN/AuthZ).
        /// </para>
        /// <para>
        /// Use Cases:
        /// </para>
        /// <para>
        /// * UnAuthorised.
        /// </para>
        /// <para>
        /// * UnAuthenticated.
        /// </para>
        /// <para>
        /// * Service unavailable.
        /// </para>
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        Error = 1,



        /// <summary>
        /// An operation blocking warning.
        /// <para>
        /// The logic is that it is not an exception, but
        /// that Client side additional information is required.
        /// <para>
        /// Operation did not complete.
        /// </para>
        /// <para>
        /// Loosely equivalent to HTTP 4xx series (Client Side error).
        /// </para>
        /// </para>
        /// <para>
        /// Use Cases are:
        /// </para>
        /// <para>
        /// * Create Validation Error: because Add operation could not proceed.
        /// </para>
        /// <para>
        /// * Retrieve Error: because Id was not found (equivalent to HTTP Code 404).
        /// </para>
        /// <para>
        /// * Update Validation Error: because Update operation could not proceed.
        /// </para>
        /// <para>
        /// * Delete Error: because object not found.
        /// </para>
        /// <para>
        /// </para>
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        [EnumMember]
        BlockingWarning = 2,

        /// <summary>
        /// THe <see cref="Message"/>
        /// is a <c>Warning</c>.
        /// <para>
        /// Operation completed.
        /// </para>
        /// <para>
        /// Use Cases:
        /// </para>
        /// <para>
        /// 
        /// </para>
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember]
        NonBlockingWarning = 3,

        /// <summary>
        /// The <see cref="Message"/>
        /// is for Information purposes only.
        /// <para>
        /// Loosely equivalent to HTTP 1xx series (Information messages).
        /// </para>
        /// <para>
        /// Execution continued.
        /// </para>
        /// <para>
        /// Uses cases are:
        /// </para>
        /// <para>
        /// * Create: message was created.
        /// </para>
        /// <para>
        /// * Retrieve: record found.
        /// </para>
        /// <para>
        /// * Update: message was updated.
        /// </para>
        /// <para>
        /// * Delete: record successfully deleted.
        /// </para>
        /// <para>
        /// * Message was sent.
        /// </para>
        /// <para>
        /// Value = 4
        /// </para>
        /// </summary>
        [EnumMember]
        Info = 4,





        /// <summary>
        /// A message to add information to the cause of the Exception.
        /// <para>
        /// IMPORTANT: 
        /// Messages marked as SystemInfo are not converted into Display Attributes,
        /// and are stripped out from the rendered output.
        /// </para>
        /// <para>
        /// The reason it is only considered Info is that if there is an Error, you 
        /// want to have a Message that explains that to the User. This is possible using a Generic Error + System routing info,
        /// and the SystemInfo is never rendered -- but is problematic if you use SystemError, and strip it out so that it is not 
        /// rendered (the user would see nothing). </para>
        /// </summary>
        [EnumMember]
        SystemInfo = 5

    }
}
