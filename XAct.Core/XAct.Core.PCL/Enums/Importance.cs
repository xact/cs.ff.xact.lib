﻿namespace XAct
{
    using System.Runtime.Serialization;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> Enum.
    /// </para>
    /// Priority (message, alert, etc.)
    /// </summary>
    [DataContract(Name = "Importance", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/")]
    public enum Importance
    {
        /// <summary>
        /// <para>Value=1</para>
        /// </summary>
        [EnumMember(Value = "3")]
        Critical = 3,
        /// <summary>
        /// <para>Value=1</para>
        /// </summary>
        [EnumMember(Value = "2")]
        Urgent = 2,
        /// <summary>
        /// <para>Value=1</para>
        /// </summary>
        [EnumMember(Value = "1")]
        High = 1,
        /// <summary>
        /// <para>Value=0</para>
        /// </summary>
        [EnumMember(Value = "0")]
        Normal = 0,
        /// <summary>
        /// <para>Value=-1</para>
        /// </summary>
        [EnumMember(Value = "-1")]
        Low = -1,

        /// <summary>
        /// <para>Value=-2</para>
        /// </summary>
        [EnumMember(Value = "-2")]
        VeryLow = -2,

    }
}
