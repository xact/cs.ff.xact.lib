﻿
namespace XAct
{
    using System.Runtime.Serialization;

    [DataContract]
    public enum HierarchicalOperationOption
    {
        /// <summary>
        /// No option was selected.
        /// <para>
        /// An error state.
        /// </para>
        /// <para>
        /// Value = 0
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined=0,
        /// <summary>
        /// Process only the top element.
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>

        [EnumMember]
        TopOnly=1,


        /// <summary>
        /// Process the top element and recursively 
        /// all it's children.
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        Recursive=2,
    }
}
