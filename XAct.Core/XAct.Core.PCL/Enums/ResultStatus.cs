﻿namespace XAct
{
    using System.Runtime.Serialization;

    /// <summary>
    ///   <para>
    /// An <c>XActLib</c> Enum.
    /// </para>
    /// An enumeration that provides more nuanced values than just True/False.
    /// </summary>
    /// <internal>
    /// Used by XAct.Diagnostics.IntegrationTests
    ///   </internal>
    [DataContract(Name = "ResultStatus", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/")]
    public enum ResultStatus
    {

        /// <summary>
        /// <para>
        /// Value = 0
        /// </para>
        /// <para>
        /// This is an error condition.
        /// </para>
        /// </summary>
        [EnumMember(Value = "0")]Undefined = 0,


        /// <summary>
        /// The operation was aborted.
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember(Value = "1")] Aborted = 1,

        /// <summary>
        /// The result of the operation was indeterminate.
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        [EnumMember(Value = "2")]
        Undetermined = 2,


        /// <summary>
        /// The result of the operation was Failure.
        /// <para>
        /// Value = 3.
        /// </para>
        /// <para>
        /// This is one of the rare cases 
        /// where I don't set Undefined to 0,
        /// rather than the first Enum value.
        /// So that if forgotten to be set, it's
        /// considered an Error message. 
        /// </para>
        /// </summary>
        [EnumMember(Value = "3")] Fail = 3,
        
        /// <summary>
        /// The result of the operation was Warn.
        /// <para>
        /// Value = 4.
        /// </para>
        /// </summary>
        [EnumMember(Value = "4")] Warn = 4,

        /// <summary>
        /// The result of the operation was Success.
        /// <para>
        /// Value = 5.
        /// </para>
        /// </summary>
        [EnumMember(Value = "5")] Success = 5,
    }
}