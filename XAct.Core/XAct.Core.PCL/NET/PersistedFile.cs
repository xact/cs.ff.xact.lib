﻿#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

// ReSharper disable CheckNamespace
namespace XAct.Net.Messaging
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;


    /// <summary>
    /// A file persisted 
    /// </summary>
    /// <internal>
    /// Intentionally an Anemic Entity for easier serialization. 
    /// <para>
    /// Behavior provided by Extension Methods.
    /// </para>
    /// </internal>
    [DataContract(Name = "PersistedFile")]
    public class PersistedFile :
        IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp,
        IHasApplicationTennantId,
        IHasNamedValue<byte[]>, 
        IPersistedFile, 
        IHasTag,
        IHasDescription
        
    {
        
        ///// <summary>
        ///// Froms the file.
        ///// </summary>
        ///// <param name="fileName">Name of the file.</param>
        ///// <param name="ioService">The <see cref="IIOService"/> to use (can be from FileSystem in full desktop or 
        ///// IsolatedStorage based system.
        ///// </param>
        ///// <para>
        ///// If <paramref name="ioService"/> is left blank, will use 
        ///// <see cref="XAct.DependencyResolver"/> to retrieve an implementation
        ///// of <see cref="IFSIOService"/>
        ///// </para>
        ///// <returns></returns>
        //public static PersistedFile FromFile(string fileName, IIOService ioService=null)
        //{
        //    if (ioService == null)
        //    {
        //        ioService = XAct.DependencyResolver.Current.GetInstance<IFSIOService>();
        //    }

        //    PersistedFile messageAttachment = new PersistedFile();
        //    messageAttachment.LoadFromFile(fileName,ioService);
        //    return messageAttachment;
        //}

        ///// <summary>
        ///// Froms the stream.
        ///// </summary>
        ///// <param name="stream">The stream.</param>
        ///// <returns></returns>
        //public static PersistedFile FromStream(Stream stream)
        //{
        //    PersistedFile messageAttachment = new PersistedFile();
        //    messageAttachment.LoadFromStream(stream);
        //    return messageAttachment;
        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="XAct.Net.Messaging.PersistedFile"/> class.
        /// </summary>
        public PersistedFile()
        {
            this.GenerateDistributedId();
            //this.Enabled = true;
        }


        /// <summary>
        /// Gets or sets the datastore id of this address element.
        /// <para>Member defined in <see cref="IMessageIdentified"/>.</para>
        /// <para>
        /// IMPORTANT: 
        /// </para>
        /// <para>
        /// The Id is not created automatically.
        /// as (in EF, it's property is decorated with
        /// .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
        /// </para>
        /// <para>
        /// The value is supplied when the owner <see cref="PersistedFile"/>
        /// is Committed.
        /// </para>
        /// <para>
        /// If you add <see cref="PersistedFileMetadata"/> entities 
        /// after you have commited the owner <see cref="PersistedFile"/>
        /// they will not be assigned a new Guid -- probably causing 
        /// an exception trying to save more than one record with an Id
        /// of <c>Guid.Empty</c>.
        /// </para>
        /// </summary>
        /// <internal>
        /// Note: NonSerialized, as Clients creating new records don't yet have an id to serialize, 
        /// and event handlers only need to react to the message -- 
        /// not have enough to update a message in midflight.
        /// </internal>
        /// <internal>Note: NonSerialized can only be applied to private fields.</internal>
        [DataMember(Name = "Id", Order = 1, IsRequired = true)]
        public virtual Guid Id { get; set; }





        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }






        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        /// <value>
        /// The unique organisation id.
        /// </value>
        [DataMember(Name = "ApplicationTennantId", Order = 2, IsRequired = true)]
        public virtual Guid ApplicationTennantId { get; set; }






        ///// <summary>
        ///// Gets or sets a value indicating whether this file was loaded from a Stream versus a File.
        ///// </summary>
        ///// <value>
        /////   <c>true</c> if this instance is stream; otherwise, <c>false</c>.
        ///// </value>
        //[DataMember(Name = "IsStream", Order = 5, IsRequired = true)]
        //public virtual bool IsStream { get; set; }

        /// <summary>
        /// Gets the size of the attachment (<see cref="Value"/> Length).
        /// <para>
        /// Value is a calculated field, and is not serialized.
        /// </para>
        /// </summary>
        /// <value>The size.</value>
        //[NonSerialized]
        public virtual int Size
        {
            get
            {
                _size = ((this.Value != null) ? this.Value.Length : 0);
                return _size;
            }
            protected set { _size = value; }
        }
        [DataMember(Name = "Size", Order = 4)]
        private int _size;

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        [DataMember(Name = "ContentType", Order = 5)]
        public virtual string ContentType { get; set; }


        /// <summary>
        /// Gets the date the attachment was last modified.
        /// <para>Member defined in <see cref="IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        /// <value>The date modified.</value>
        [DataMember(Name = "LastModifiedOn", Order = 6)]
        public virtual DateTime? LastModifiedOnUtc { get; set; }

        /// <summary>
        /// Gets the date the attachment was created.
        /// <para>Member defined in <see cref="IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        /// <value>The date created.</value>
        [DataMember(Name = "CreatedOn", Order = 7)]
        public virtual DateTime? CreatedOnUtc { get; set; }

        /// <summary>
        /// Not Used.
        /// <para>Member defined in <see cref="IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        [DataMember(Name = "DeletedOn", Order = 8)]
        public virtual DateTime? DeletedOnUtc { get; set; }




        /// <summary>
        /// Gets or sets the name
        /// of this attachment.
        /// <para>Member defined in <see cref="IHasNamedValue{TValue}"/>.</para>
        /// </summary>
        /// <value>The name.</value>
        public virtual string Name
        {
            get { return this._name; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value",
                        // ReSharper disable LocalizableElement
                                                    "Value cannot be null when setting Name.");
                    // ReSharper restore LocalizableElement
                }
                if (value.Length == 0)
                {
                    // ReSharper disable LocalizableElement
                    throw new ArgumentException("String is Empty (Length == 0).", "value");
                    // ReSharper restore LocalizableElement
                }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
                // ReSharper disable RedundantCheckBeforeAssignment
                if (value != this._name)
                // ReSharper restore RedundantCheckBeforeAssignment
                {
                    this._name = value;
                }
            }
        }
        [DataMember(Name = "Name", Order = 9, IsRequired = true)]
        private string _name;


        /// <summary>
        /// Gets or sets the content id that needs to be set if you want to 
        /// embed the file in a message.
        /// </summary>
        /// <value>
        /// The content id.
        /// </value>
        [DataMember(Name = "ContentId", Order = 10, IsRequired = false)]
        public virtual string ContentId { get; set; }

        /// <summary>
        /// Gets or sets the value
        /// of this attachment.
        /// <para>Member defined in <see cref="IHasNamedValue{TValue}"/>.</para>
        /// </summary>
        /// <value>The value.</value>
        [DataMember(Name = "Value", Order = 11, EmitDefaultValue = false, IsRequired = true)]
        public virtual byte[] Value { get; set; }

        /// <summary>
        /// Gets or sets the optional tag for this file.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        [DataMember(Name = "Tag", Order = 12, EmitDefaultValue = false)]
        public virtual string Tag { get; set; }

        
        /// <summary>
        /// Gets or sets an optional description for this file.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember(Name = "Description", Order = 13, EmitDefaultValue = false)]
        public virtual string Description { get; set; }


        /// <summary>
        /// Gets the collection of related metadata.
        /// </summary>
        /// <value>
        /// The metadata.
        /// </value>
        public virtual ICollection<PersistedFileMetadata> Metadata
        {
            get
            {
                return _metadata??(_metadata = new Collection<PersistedFileMetadata>());
            }
        }
        [DataMember(Order = 15)]
        private ICollection<PersistedFileMetadata> _metadata;


    }
}