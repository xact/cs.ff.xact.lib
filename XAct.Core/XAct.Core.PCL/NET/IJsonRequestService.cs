﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Net.Services
{
    /// <summary>
    /// Contract for making Http Requests, returning JSON.
    /// </summary>
    public interface IJsonRequestService :IHasXActLibService
    {
        /// <summary>
        /// Makes the request for a JSON response.
        /// </summary>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="requestUrl">The request URL.</param>
        /// <returns></returns>
        TResponse MakeRequest<TResponse>(string requestUrl)
            where TResponse : class;
    }





}
