namespace XAct
{

    /// <summary>
    /// Contract for a File persisted to 
    /// hard drive.
    /// <para>
    /// Implemented by <c>XAct.Net.Messaging.MessageAttachment</c>,
    /// <c>PersistedFile</c> and <c>FileStorage</c>
    /// </para>
    /// </summary>
    public interface IPersistedFile :       
        IHasNamedValue<byte[]>, IHasDateTimeTrackabilityUtc

    {
        

        ///// <summary>
        ///// Gets or sets a value indicating whether 
        ///// this file was loaded from a Stream versus a File.
        ///// </summary>
        ///// <value>
        /////   <c>true</c> if this instance is stream; otherwise, <c>false</c>.
        ///// </value>
        //bool IsStream { get; set; }


        /// <summary>
        /// Gets the size of the file (<see cref="IHasNamedValue{T}.Value"/> Length).
        /// <para>
        /// Value is a calculated field, and is not serialized.
        /// </para>
        /// </summary>
        /// <value>The size.</value>
        //[NonSerialized]
        int Size { get; }

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        string ContentType { get; set; }


        ///// <summary>
        ///// Gets the date the file was last modified.
        ///// <para>Member defined in <see cref="IHasDateTimeTrackability"/>.</para>
        ///// </summary>
        ///// <value>The date modified.</value>
        //DateTime? LastModifiedOn { get; set; }

        ///// <summary>
        ///// Gets the date the file was created.
        ///// <para>Member defined in <see cref="IHasDateTimeTrackability"/>.</para>
        ///// </summary>
        ///// <value>The date created.</value>
        //DateTime? CreatedOn { get; set; }

        ///// <summary>
        ///// Not Used.
        ///// <para>Member defined in <see cref="IHasDateTimeTrackability"/>.</para>
        ///// </summary>
        //DateTime? DeletedOn { get; set; }




        ///// <summary>
        ///// Gets or sets the name
        ///// of this file.
        ///// <para>Member defined in <see cref="IHasNamedValue{TValue}"/>.</para>
        ///// </summary>
        ///// <value>The name.</value>
        //string Name { get; set; }

        


        ///// <summary>
        ///// Gets or sets the value
        ///// of this file.
        ///// <para>Member defined in <see cref="IHasNamedValue{TValue}"/>.</para>
        ///// </summary>
        ///// <value>The value.</value>
        //byte[] Value { get; set; }



    }
}