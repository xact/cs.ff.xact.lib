﻿using XAct.Environment;

namespace XAct.Net.Messaging
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Optional attributes to associate to the
    /// <see cref="PersistedFile" />
    /// </summary>
    [DataContract]
    public class PersistedFileMetadata : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasOwnerFK<Guid>, IHasKeyValue<string>, IHasAuditabilitySimple, IHasTag
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// <para>
        /// IMPORTANT: 
        /// </para>
        /// <para>
        /// The Id is not created automatically.
        /// as (in EF, it's property is decorated with
        /// .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
        /// </para>
        /// <para>
        /// The value is supplied when the owner <see cref="PersistedFile"/>
        /// is Committed.
        /// </para>
        /// <para>
        /// If you add <see cref="PersistedFileMetadata"/> entities 
        /// after you have commited the owner <see cref="PersistedFile"/>
        /// they will not be assigned a new Guid -- probably causing 
        /// an exception trying to save more than one record with an Id
        /// of <c>Guid.Empty</c>.
        /// </para>
        /// </summary>
        [DataMember(IsRequired = true)]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the Id of the 
        /// <see cref="PersistedFile"/>
        /// that manages a collection of these
        /// <see cref="PersistedFile"/>
        /// attributes.
        /// </summary>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public virtual Guid OwnerFK { get; set; }


        /// <summary>
        /// Gets or sets the key.
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// </summary>
        [DataMember(IsRequired = true, EmitDefaultValue = false)]
        public virtual string Key { get; set; }
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public virtual string Value { get; set; }   


        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public virtual DateTime? CreatedOnUtc { get; set; }

        
        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public virtual string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the last modified on.
        /// </summary>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public virtual DateTime? LastModifiedOnUtc { get; set; }
        
        /// <summary>
        /// Gets or sets the last modified by.
        /// </summary>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public virtual string LastModifiedBy { get; set; }



        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public virtual string Tag { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="PersistedFileMetadata"/> class.
        /// </summary>
        public PersistedFileMetadata()
        {
            this.GenerateDistributedId();
            //this.Enabled = true;
            //this.CreatedOnUtc = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC;
           // this.LastModifiedOnUtc = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC;
        }

    }
}