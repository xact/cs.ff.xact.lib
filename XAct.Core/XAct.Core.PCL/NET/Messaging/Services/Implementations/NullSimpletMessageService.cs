namespace XAct.Net.Messaging.Services.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class NullSimpleMessageService : XActLibServiceBase, ISimpleMessageService, IHasLowBindingPriority
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NullSimpleMessageService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public NullSimpleMessageService(ITracingService tracingService) : base(tracingService)
        {
        }

        public void Send(SimpleMessage simpleMessage)
        {
            throw new ArgumentException("No ISimpleMessageService has been added to the project. Consider Nugeting XAct.Net assemblies in orter to get SMTP delivery services.");
        }

    }
}