namespace XAct.Net.Messaging
{

    /// <summary>
    /// Service to send messages to Users.
    /// </summary>
    public interface ISimpleMessageService
    {
        /// <summary>
        /// Send the specified message to the recipient.
        /// </summary>
        /// <param name="userIdentityMessage"></param>
        void Send(SimpleMessage userIdentityMessage);
    }
}