namespace XAct.Net.Messaging
{
    using System.Runtime.Serialization;

    /// <summary>
    /// A message to be sent to an end user.
    /// </summary>
    [DataContract]
    public class SimpleMessage
    {
        /// <summary>
        /// Destination, i.e. To email, or SMS phone number
        /// </summary>
        [DataMember]
        public virtual string Destination { get; set; }

        /// <summary>
        /// Subject
        /// </summary>
        [DataMember]
        public virtual string Subject { get; set; }

        /// <summary>
        /// Message contents
        /// </summary>
        [DataMember]
        public virtual string Body { get; set; }
    }
}