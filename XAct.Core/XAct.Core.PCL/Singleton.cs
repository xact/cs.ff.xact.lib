﻿namespace XAct
{
    /// <summary>
    /// A generic Singleton Factory.
    /// <para>
    /// <code>
    /// <![CDATA[
    /// //One way of using it (although I would *never*
    /// //recommend using a singleton as first resort):
    /// var mySingleton = Singleton<AppDbContext>();
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public class Singleton<TContext> where TContext : class, new()
    {
        // Singleton...havn't seen one of these since....oh...2006?
        private static TContext _singleton;

        /// <summary>
        /// Gets the singleton instance of {TContext}.
        /// <para>
        /// Note: Creates it upon first request.
        /// </para>
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public static TContext Instance
        {
            get { return _singleton ?? (_singleton = System.Activator.CreateInstance<TContext>()); }
        }

    }
}
