﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using XAct.Domain;

//namespace XAct.UI
//{
//    /// <summary>
//    /// Interface for Persisting user displayed 
//    /// <c>XAct.UI.SelectableListItem</c> elements.
//    /// </summary>
//    /// <internal>
//    /// Hate the idea of adding UI namespace to XAct.Core
//    /// but it's needed by Domain Entities at all layers of the UI,
//    /// including XAct.UI...so it's either here, or XAct.Domain.
//    /// <para>
//    /// But it's not Domain either, as it's implementation...hum...
//    /// </para>
//    /// </internal>
//    public interface IDisplayableSelectableListItem : IHasEnabled,IHasOrder,IHasFilter,IHasNamedValue<string> //: INamedValue<string>
//    {

//    }
//}