﻿

// ReSharper disable CheckNamespace
namespace XAct.UI.Views
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// An enumeration of UI States (Inherited, Skipped, Hidden, Display, ReadOnly, Editable)
    /// </summary>
    /// <internal>
    /// The reason <see cref="ViewMode"/> is in Core is because it will be referenced
    /// when writting to the Db, as well as the UI layer. Ie...basically everywhere.
    /// </internal>
    public enum ViewMode
    {
        /// <summary>
        /// Undefined (therefore falls back to View's ViewBag).
        /// <para>
        /// Value = 0.
        /// </para>
        /// </summary>
        Undefined = 0,




        /// <summary>
        /// Inherited value from underlying <see cref="XAct.UI.Views.IViewModeService"/>.
        /// <para>
        /// Value = 1.
        /// </para>
        /// </summary>
        Rule = 1,


        /// <summary>
        /// Field is not generated.
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        Skipped = 2,

        /// <summary>
        /// Field is generated but hidden.
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        Hidden = 3,

        /// <summary>
        /// Display only (eg: SPAN).
        /// Use ONLY for GET scenarios (or you lose data on roundtrip...use ReadOnly for POSTs).
        /// <para>
        /// Value = 4
        /// </para>
        /// </summary>
        Display = 4,


        /// <summary>
        /// Editor is displayed, but disabled.
        /// <para>
        /// Value = 5
        /// </para>
        /// <para>
        /// Prefer ReadOnly
        /// </para>
        /// </summary>
        Disabled = 5,

        /// <summary>
        /// Readonly.
        /// <para>
        /// A common strategy is to shows same as 'Display',
        /// while rendering a Hidden 'Editable' underneath, as well.
        /// </para>
        /// <para>
        /// Value = 6
        /// </para>
        /// </summary>
        ReadOnly = 6,


        /// <summary>
        /// Editable.
        /// <para>
        /// Value = 7
        /// </para>
        /// </summary>
        Editable = 7,
    }

}
