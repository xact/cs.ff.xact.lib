// ReSharper disable CheckNamespace
namespace XAct.UI.Views
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// The current ViewMode Context, containing the Name
    /// specified using using <c>IViewModeManagementService</c>
    /// </summary>
    /// <remarks>
    /// Usage example:
    /// <para>
    /// <code>
    /// <example>
    /// <![CDATA[
    /// IViewModeContext viewModeContext = DependencyResolver.Current.GetInstance<IViewModeService>.Current;
    /// if (viewModeContext.Name== "Edit"){
    ///   //Show an MVC EditorFor ...
    /// }else{
    ///   //Show a MVC DisplayFor ...
    /// }
    /// ]]>
    /// </example>
    /// </code>
    /// </para>
    /// </remarks>
    public interface IViewModeContext : IHasName
    {
    }
}