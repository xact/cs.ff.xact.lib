// ReSharper disable CheckNamespace
namespace XAct.UI.Views
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a service to retrieve the desired 
    /// <see cref="ViewMode"/> for a property in a ViewModel, according 
    /// to rules already registered in an internal singleton cache
    /// using <c>IViewModeManagementService</c>.
    /// </summary>
    public interface IViewModeService : IHasXActLibService
    {
        /// <summary>
        /// Gets the current (thread specific) <see cref="IViewModeContext"/>
        /// <para>
        /// This is then used by the 
        /// <see cref="GetViewMode(Type,string,string)"/>
        /// overloads.
        /// </para>
        /// </summary>
        IViewModeContext CurrentContext { get; }


        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <paramref name="viewModel"/>
        /// when rendered in the already defined/current <c>View Case</c>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from 
        /// <c>Constants.ViewModeCaseNames</c> ('Create', 'View', 'Edit', etc.) 
        /// </para>
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <returns>The <see cref="ViewMode"/></returns>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        ViewMode GetViewMode(Type viewModel, string viewPropertyName, string tag = null);

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <c>ViewModel</c>.
        /// when rendered in the already defined/current <c>View Case</c>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from 
        /// <c>Constants.ViewModeCaseNames</c> ('Create', 'View', 'Edit', etc.) 
        /// </para>
        /// </summary>
        /// <param name="viewModelFullName">The <c>FullName</c> of the view model's <see cref="Type"/>.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>The <see cref="ViewMode"/></returns>
        ViewMode GetViewMode(string viewModelFullName, string viewPropertyName, string tag = null);

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <paramref name="viewModel"/>
        /// when rendered in the specified <paramref name="viewModeCaseName"/>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from 
        /// <c>Constants.ViewModeCaseNames</c> ('Create', 'View', 'Edit', etc.) 
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">Name of the view case.</param>
        /// <param name="viewModel">The view model.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>The <see cref="ViewMode"/></returns>
        ViewMode GetViewMode(string viewModeCaseName, Type viewModel, string viewPropertyName, string tag = null);

        /// <summary>
        /// Gets the <see cref="ViewMode"/> for the specified Property in the specified <c>ViewModel</c>.
        /// when rendered in the specified <paramref name="viewModeCaseName"/>.
        /// <para>
        /// Note: for consistency's sake, consider using the constants strings from 
        /// <c>Constants.ViewModeCaseNames</c> ('Create', 'View', 'Edit', etc.) 
        /// </para>
        /// </summary>
        /// <param name="viewModeCaseName">Name of the view case.</param>
        /// <param name="viewModelFullName">The <c>FullName</c> of the view model's <see cref="Type"/>.</param>
        /// <param name="viewPropertyName">Name of the view property.</param>
        /// <param name="tag">The (optional) tag/specific case identifier.</param>
        /// <returns>The <see cref="ViewMode"/></returns>
        ViewMode GetViewMode(string viewModeCaseName, string viewModelFullName, string viewPropertyName,string tag);
    }
}