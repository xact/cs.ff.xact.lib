﻿namespace XAct.UI
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for generic UI Framework-Agnostic version of a ListItem.
    /// <para>
    /// Key characteristics are that it is for a string key,
    /// suitable for tranferring both 
    /// IHasReferenceData{Id} and
    /// IHasReferenceData{Guid} objects.
    /// </para>
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Used extensively by MVP framework.
    ///   </para>
    /// </remarks>
    /// <internal>
    /// The reason the contract is so down from XAct.UI is because it
    /// needs to be applied to Domain entities, that are persisted in datastore.
    /// Ie, used all the way at the top, and bottom of the stack.
    /// </internal>
    public interface ISelectableItem<TValue> :  IHasEnabled, IHasOrder, IHasKeyValue<TValue>, IHasTag, IHasFilter,
        IHasSelected
        //NOTE: NO Direct inheritence with IHasReferenceData as Id is transposed to Key
    {

    }

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for generic UI Framework-Agnostic version of a ListItem.
    /// <para>
    /// Key characteristics are that it is for a string key, object value.
    /// Suitable for tranferring IHasReferenceData{Guid} objects.
    /// </para>
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Used extensively by MVP framework.
    ///   </para>
    /// </remarks>
    /// <internal>
    /// The reason the contract is so down from XAct.UI is because it
    /// needs to be applied to Domain entities, that are persisted in datastore.
    /// Ie, used all the way at the top, and bottom of the stack.
    /// </internal>
    public interface ISelectableItem : ISelectableItem<string>{}
}