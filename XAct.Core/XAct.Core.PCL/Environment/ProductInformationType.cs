﻿namespace XAct.Environment
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Type of <see cref="ProductInformation"/>
    /// </summary>
    [DataContract]
    public enum ProductInformationType
    {
        /// <summary>
        /// The undefined
        /// <para>
        /// Value = 0
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined=0,


        /// <summary>
        /// The default value, which is the same as
        /// <see cref="LicensedSeller"/>
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        Default = LicensedSeller,

        /// <summary>
        /// The product information 
        /// describes the original
        /// developer of the software.
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        Developer = 1,

        /// <summary>
        /// The product information 
        /// describes the Owner of the software
        /// <para>
        /// An owner may have commissioned software
        /// from a <see cref="Developer"/>
        /// </para>
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        [EnumMember]
        Owner = 2,

        /// <summary>
        /// The product information 
        /// describes a Licensed Seller.
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        LicensedSeller = 3
    }
}