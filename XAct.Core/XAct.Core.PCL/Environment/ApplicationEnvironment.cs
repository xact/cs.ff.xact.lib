﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Reflection;

    public static partial class ApplicationEnvironment
    {
//        /// <summary>
//        ///   Replace '|DataDirectory|' in the given path, with the path 
//        ///   to the application's designated Data directory.
//        ///   <para>
//        ///     If not rooted, can be prepended with the <see cref = "AppDir" />.
//        ///   </para>
//        /// </summary>
//        /// <param name = "tmpPath">The path to parse.</param>
//        /// <param name = "ensureRooted">
//        ///   Whether or not to ensure the string is rooted.
//        /// </param>
//        /// <returns>The expanded path.</returns>
//        public static string ExpandDataDirectoryMacro(
//            string tmpPath,
//            bool ensureRooted)
//        {
//            if (string.IsNullOrEmpty(tmpPath))
//            {
//                return string.Empty;
//            }
//            //CASE INSENSITIVE REPLACEMENT OF |DATADIRECTORY| MACRO:
//            const string dataDirectoryPattern = "\\|DataDirectory\\|";

//            if (Regex.IsMatch(
//                tmpPath,
//                dataDirectoryPattern,
//                RegexOptions.IgnoreCase)
//                )
//            {
//#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//                //PC:
//                object dataDirectoryObject =
//                    AppDomain.CurrentDomain.GetData("DataDirectory");

//                string dataDirectory =
//                    (dataDirectoryObject != null)
//                        ? dataDirectoryObject + @"\"
//                        : AppDir + @"\Data\";
//#else
//    //CE:
//        string dataDirectory = AppDir + @"\Data\";
//#endif
//                tmpPath =
//                    Regex.Replace(
//                        tmpPath,
//                        dataDirectoryPattern,
//                        dataDirectory,
//                        RegexOptions.IgnoreCase);
//            }
//            return (ensureRooted) ? EnsureDirectoryRooted(tmpPath) : tmpPath;
//        }


    }

    /// <summary>
    ///   A static class of helpful static methods 
    ///   commonly needed to investigate 
    ///   Environment properties.
    /// </summary>
    /// <internal>
    /// Absolutely hate the name -- should be Environment -- 
    /// but it conflicts with name of the static class
    /// System.Environment.
    /// </internal>
    public static partial class ApplicationEnvironment
    {
        #region Constants

        /// <summary>
        ///   Class of constants used within the
        ///   <see cref = "Environment" />
        ///   class.
        /// </summary>
        public static class Constants
        {
            /// <summary>
            ///   Constant used to save the ApplicationName in the Request's hashtable.
            /// </summary>
            public const string ApplicationNameKey = "ApplicationName";

            /// <summary>
            ///   FQN of System.Web
            /// </summary>
            public const string SystemWebAssemblyString =
                "System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";
        }

        #endregion






        #region Public Methods



        /// <summary>
        ///   Gets a value from the current HTTP Context dictionary
        ///   <para>
        ///     Uses Reflection to not cause a dependency on System.Web.
        ///   </para>.
        /// </summary>
        /// <typeparam name = "TItem">The type of the item.</typeparam>
        /// <param name = "propertyName">Name of the property.</param>
        /// <returns></returns>
        public static TItem GetHttpContextPropertyByReflection<TItem>(string propertyName)
        {
            object httpContext = HttpContext;

            if (httpContext == null)
            {
                return default(TItem);
            }

            //get the property info for the specified property
            PropertyInfo propertInfo =
                httpContext.GetType().GetProperty(
                    propertyName,
                    (BindingFlags.Public | BindingFlags.Instance));

            //get the Typed property value
            return propertInfo.GetValue(httpContext, null).ConvertTo<TItem>();
        }

        /// <summary>
        ///   Puts a value into the current HTTP Context dictionary
        ///   <para>
        ///     Uses Reflection to not cause a dependency on System.Web.
        ///   </para>.
        /// </summary>
        /// <param name = "propertyName">Name of the property.</param>
        /// <param name = "propertyValue">The property value.</param>
        public static void SetHttpContextPropertyByReflection(string propertyName, object propertyValue)
        {
            object httpContext = HttpContext;

            if (httpContext == null)
            {
                return;
            }

            //get the property info for the specified property
            PropertyInfo propertInfo =
                httpContext.GetType().GetProperty(
                    propertyName,
                    (BindingFlags.Public | BindingFlags.Instance));

            //set the property value
            propertInfo.SetValue(httpContext, propertyValue, null);
        }

        #endregion


        ///// <summary>
        /////   Ensures that the given path, if not rooted, 
        /////   is prepended with the <see cref = "AppDir" />.
        ///// </summary>
        ///// <param name = "tmpPath"></param>
        ///// <returns></returns>
        //private static string EnsureDirectoryRooted(string tmpPath)
        //{
        //    if (!Path.IsPathRooted(tmpPath))
        //    {
        //        tmpPath = Path.Combine(AppDir, tmpPath);
        //    }
        //    return tmpPath;
        //}


        ///<summary>
        ///  The HttpContext by Reflection 
        ///  (so that this assembly does not end up having a reference
        ///  to System.Web)
        ///</summary>
        public static object HttpContext
        {
            get
            {
                if (_cachedHttpContextPropertyInfo == null)
                {
                    const string path = "System.Web.HttpContext";
                    const string assemblyName = Constants.SystemWebAssemblyString;

                    const string contextPath = "Current";

                    //get without raising error if not found:
                    Type type = Type.GetType(path + ", " + assemblyName, true);

                    if (type == null)
                    {
                        return null;
                    }


                    //get the HTTP context property info
                    _cachedHttpContextPropertyInfo = type.GetProperty(contextPath,
                                                                      (BindingFlags.Public | BindingFlags.Static));
                }
                //get a reference to the current HTTP context
                return _cachedHttpContextPropertyInfo.GetValue(null, null);
            }
        }

        private static PropertyInfo _cachedHttpContextPropertyInfo;


//        /// <summary>
//        ///   Gets or Sets the name of the application 
//        ///   using the custom membership provider.
//        ///   Default value is '/'
//        /// </summary>
//        /// <remarks>
//        ///   <para>
//        ///     It is essential that the ApplicationName is set.
//        ///     See 
//        ///     <see href = "http://weblogs.asp.net/scottgu/archive/2006/04/22/443634.aspx" />
//        ///     for why.
//        ///   </para>
//        /// </remarks>
//        /// <internal>
//        ///   <![CDATA[
//        /// 138 appdomain.SetData (".appDomain", "*"); 
//        /// 118 appdomain.SetData (".appPath", physicalDir);   
//        /// 139 appdomain.SetData (".appPath", physicalDir); 
//        /// 119 appdomain.SetData (".appVPath", virtualDir);   
//        /// 140 appdomain.SetData (".appVPath", virtualDir); 
//        /// 120 appdomain.SetData (".domainId", domain_id);   
//        /// 141 appdomain.SetData (".domainId", domain_id); 
//        /// 121 appdomain.SetData (".hostingVirtualPath", virtualDir);   
//        /// 142 appdomain.SetData (".hostingVirtualPath", virtualDir); 
//        /// 122 appdomain.SetData (".hostingInstallDir", 
//        ///   Path.GetDirectoryName (typeof (Object).Assembly.CodeBase)); 
//        /// ]]>
//        /// </internal>
//        /// <value></value>
//        /// <returns>
//        ///   The name of the application using the custom membership provider.
//        /// </returns>
//        public static string ApplicationName
//        {
//            get
//            {
//#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//                //PC:
//                //Reference to Web dll's only happens 
//                //if on PC or server, not CE:
//                object httpContext =
//                    HttpContext;

//                if (httpContext != null)
//                {
//                    //IN IIS:
//                    //We are definately in a web application, 
//                    //so its safe to refer to these properties:
//                    if (
//                        (string.IsNullOrEmpty(_applicationName)) ||
//                        (_applicationName == "/"))
//                    {
//                        //Use alternate way that leaves 
//                        //no binding on System.Web.Hosting
//                        //_ApplicationName = 
//                        //System.Web.Hosting.HostingEnvironment.
//                        //  ApplicationVirtualPath;
//                        //The GetData(".appVPath") comes back as 
//                        //something like "/MyWebSite" (no final slash):

//                        return
//                            (string)
//                            AppDomain.CurrentDomain.GetData(".appVPath");

//                        //For the record, GetData(".appPath") comes back With 
//                        //a slash at the end:
//                        //"D:\SYS\PROFILES\S\MYDOCUMENTS\CODE\MyWebSite\"
//                    }
//                    if (_applicationName == "~")
//                    {
//                        //This means we are suppossed to work 
//                        //with the httpContext hashtable using a 
//                        //key of 
//                        //Constants.ApplicationNameKey
//                        //to store the value:
//                        //Note: The Session object is *within* Items as:
//                        //return (HttpSessionState) this.Items["AspSession"];
//                        string o =
//                            GetHttpContextPropertyByReflection<string>(Constants.ApplicationNameKey);

//                        return (string.IsNullOrEmpty(o)) ? "/" : o;
//                    }

//                    //We are expected to use the normal value:
//                    return _applicationName;
//                }
//                //We are in PC, but not in IIS:
//#endif
//                //We are in PC or CE:
//                //And we are NOT in a web application:
//                if ((string.IsNullOrEmpty(_applicationName)) ||
//                    (_applicationName == "/") || (_applicationName == "~")
//                    )
//                {
//                    //We are using the default value...
//                    //which should fall back on the name of the application.
//                    //Note that it must 
//                    //not remain a "/" as it messes up the path.
//                    //And must be differentiated from other applications in the 
//                    //SpecialFolder.ApplicationData folder...
//                    //Comes back as "MyApp.exe", no slashes. With extension.
//#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//                    //PC:
//                    //Works on Desktop
//                    //Comes back as just *.exe name, no path info:
//                    _applicationName =
//                        (string) AppDomain.CurrentDomain.GetData("APP_NAME");


//                    if (string.IsNullOrEmpty(_applicationName))
//                    {
//                        //For some unknown reason "APP_NAME" didn't cough up 
//                        //a result...so try solution #2:
//                        //This works for Desktop 
//                        //(returns the *.exe assembly, no path info):
//                        //Not Available on CF:
//                        _applicationName =
//                            AppDomain.CurrentDomain.
//                                SetupInformation.ApplicationName ?? AppDomain.CurrentDomain.FriendlyName;

//                        //Surprise! The above last line appears to not 
//                        //always work...(failed in iSight.Shell)...
//                    }

//#else
//    //In CE:
//          _ApplicationName = Path.GetFileName(GetEntryAssemblyName());
//#endif
//                }
//                Debug.Assert(
//                    !string.IsNullOrEmpty(_applicationName)
//                    );

//                /*
//                 * //There's really no reason that it 
//                   //would not be filled at this point...
//                if (string.IsNullOrEmpty(_ApplicationName)) {
//                    //Still didn't work....
//                    //Ok...going to have to do all by hand:
//                    //CF: Note that another way to do this would
//                    //be to use 
//                    //System.Windows.Forms.Application.ProductName;
//                    //or
//                    //Application.ExecutablePath;
//                    System.Reflection.Assembly a = GetEntryAssembly();

//                    //In order to get the assembly name
//                    //(which is again *.exe, and without path info):
//                    _ApplicationName = a.GetName().Name;
//                }
//                */

//                //Its filled for sure...
//                return _applicationName;
//            }
//            set
//            {
//#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//                //PC:
//                //Reference to Web dll's only happens 
//                //if on PC or server, not CE:
//                if (_applicationName != "~")
//                {
//                    return;
//                }
//                //This means we are suppossed to work 
//                //with the httpContext hashtable 
//                //using a key of 
//                //Constants.ApplicationNameKey
//                //to store the value:
//                object httpContext = HttpContext;

//                if (httpContext == null)
//                {
//                    return;
//                }
//                //This means we are suppossed to work 
//                //with the httpContext hashtable 
//                //using a key of 
//                //Constants.ApplicationNameKey
//                //to store the value:
//                //Note: The Session object is *within* Items as:
//                //return (HttpSessionState) this.Items["AspSession"];
//                SetHttpContextPropertyByReflection(Constants.ApplicationNameKey, value);

//                return;
//#endif
//            }
//        }

//        /// <summary>
//        /// Gets the cached Hash of the lowercased <see cref="ApplicationName"/> .
//        /// </summary>
//        public static string ApplicationNameHash
//        {
//            get
//            {
//                if (string.IsNullOrEmpty(_applicationNameHash))
//                {
//                    _applicationNameHash = ApplicationName.ToLowerInvariant().CalculateHash();
//                }
//                return _applicationNameHash;
//            }
//        }

//        private static string _applicationNameHash;


//        /// <summary>
//        ///   Returns the EntryAssembly.
//        /// </summary>
//        /// <returns>The Assembly</returns>
//        // ReSharper disable UnusedMember.Local
//        private static string GetEntryAssemblyName()
//            // ReSharper restore UnusedMember.Local
//        {
//#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//            //PC:
//            //http://blogs.msdn.com/asanto/archive/2003/09/08/26710.aspx
//            //says to never trust EntryAssembly(), but rather, trust:
//            string result = Process.GetCurrentProcess().ProcessName;
//#else
//        result = EnvCF.GetEntryAssemblyName();

//#endif
//            return result;
//        }


//        /// <summary>
//        ///   Returns the name of the EntryAssembly.
//        /// </summary>
//        /// <returns>The name of the assembly</returns>
//// ReSharper disable UnusedMember.Local
//        public  static Assembly GetEntryAssembly()
//            //not used in FF, but needed by CF
//// ReSharper restore UnusedMember.Local
//        {
//#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//            //PC:
//            //http://blogs.msdn.com/asanto/archive/2003/09/08/26710.aspx
//            //says to never trust EntryAssembly(), but rather, trust:
//            //return System.Reflection.Assembly.GetEntryAssembly();
//            Assembly assembly = Assembly.GetEntryAssembly();


//            //Returns NULL if within a UnitTest:
//            //Reason: http://social.msdn.microsoft.com/Forums/eu/vststest/thread/7ff8caae-56c6-4baa-94c9-7224f69dbd17

//            //Returns NULL within ASP.NET
//            //http://discuss.fogcreek.com/dotnetquestions/default.asp?cmd=show&ixPost=5284&ixReplies=1

//            return assembly;

//#else
//        return EnvCF.GetEntryAssembly();
//#endif
//        }
    }
}