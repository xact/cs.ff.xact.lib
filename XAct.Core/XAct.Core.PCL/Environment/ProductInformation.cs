﻿namespace XAct.Environment
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Information about the product.
    /// </summary>
    [DataContract]
    public class ProductInformation
    {


        /// <summary>
        /// Gets or sets the type of <see cref="ProductInformation"/>
        /// <para>
        /// Note: part of the CompositeKey.
        /// </para>
        /// </summary>
        [DataMember]
        public ProductInformationType Type { get; set; }

        /// <summary>
        /// Gets or sets the unique (short) name of the company.
        /// <para>
        /// This is the short name (eg: 'Adobe') of the company,
        /// as compared to the full name referenced
        /// via <see cref="CompanyDisplayNameResourceKey"/>
        /// </para>
        /// <para>
        /// Note: part of the CompositeKey.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string CompanyName { get; set; }


        /// <summary>
        /// Gets or sets the unique name of the product.
        /// <para>
        /// This is the short name (eg: 'Adobe') of the company,
        /// as compared to the full name referenced
        /// via <see cref="ProductDisplayTitleResourceKey"/>
        /// </para>
        /// <para>
        /// Note: part of the CompositeKey.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string ProductName { get; set; }


        /// <summary>
        /// Gets or sets the resource filter
        /// to use to retrieve production information (display title, display subtitle, etc.)
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        [DataMember]
        public virtual string ResourceFilter { get; set; }







        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of the product's
        /// company name.
        /// </summary>
        [DataMember]
        public virtual string CompanyDisplayNameResourceKey { get; set; }



        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// display company contact address resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        [DataMember]
        public virtual string  CompanyDisplayAddressResourceKey { get; set; }


        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// display company contact information resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        [DataMember]
        public virtual string CompanyDisplayContactInformationResourceKey { get; set; }






        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of the product's
        /// displayed Copyright.
        /// </summary>
        [DataMember]
        public virtual string ProductDisplayCopyrightResourceKey { get; set; }








        /// <summary>
        /// Gets or sets the 
        /// Resource Key for the 
        /// of the product's
        /// Display Name/Title.
        /// </summary>
        [DataMember]
        public virtual string ProductDisplayTitleResourceKey { get; set; }

        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of the product's
        /// Display Subtitle.
        /// </summary>
        [DataMember]
        public virtual string ProductDisplaySubTitleResourceKey { get; set; }

        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of the product's
        /// (one-paragraph) 
        /// Description.
        /// </summary>
        [DataMember]
        public virtual string ProductDisplayDescriptionResourceKey { get; set; }


        
        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of the product's
        /// Url (from which they can retrieve additional information
        /// on support, etc.
        /// </summary>
        [DataMember]
        public virtual string ProductDisplayProductUrlResourceKey { get; set; }


        


        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's base
        /// API endpoint
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// <para>
        /// Using a Resource to hold the endpoint is contraversial,
        /// but is appropriate for the support endpoint to allow 
        /// returning culture specific response. 
        /// Using Resource Keys for the rest of the API endpoints
        /// as well was just precautionary.
        /// </para>
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string ProductAPIBaseEndpointResourceKey { get; set; }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// API endpoint -- for various support operations --
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// <para>
        /// Using a Resource to hold the endpoint is contraversial,
        /// but is appropriate for the support endpoint to allow 
        /// returning culture specific response. 
        /// Using Resource Keys for the rest of the API endpoints
        /// as well was just precautionary.
        /// </para>
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string ProductAPIErrorReportingEndpointResourceKey { get; set; }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// API endpoint -- for various support operations --
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// <para>
        /// Using a Resource to hold the endpoint is contraversial,
        /// but is appropriate for the support endpoint to allow 
        /// returning culture specific response. 
        /// Using Resource Keys for the rest of the API endpoints
        /// as well was just precautionary.
        /// </para>
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string ProductAPISupportEndpointResourceKey { get; set; }

        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of an API endpoint
        /// to invoke for licensing checks.
        /// <para>
        /// For example, can be:
        /// <code>
        /// <![CDATA[
        /// GET http://mycompany.com/API/v3/licenses/...sn_of_app
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Using a Resource to hold the endpoint is contraversial,
        /// but is appropriate for the support endpoint to allow 
        /// returning culture specific response. 
        /// Using Resource Keys for the rest of the API endpoints
        /// as well was just precautionary.
        /// </para>
        /// <para>
        /// Most platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string ProductAPILicensingEndpointResourceKey { get; set; }

        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of an API endpoint
        /// to invoke for checks for later versions.
        /// <para>
        /// Using a Resource to hold the endpoint is contraversial,
        /// but is appropriate for the support endpoint to allow 
        /// returning culture specific response. 
        /// Using Resource Keys for the rest of the API endpoints
        /// as well was just precautionary.
        /// </para>
        /// <para>
        /// Most platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        /// <value>
        /// The product API updates uvailable endpoint resource key.
        /// </value>
        [DataMember]
        public virtual string ProductAPIUpdatesUvailableEndpointResourceKey { get; set; }

    }
}
