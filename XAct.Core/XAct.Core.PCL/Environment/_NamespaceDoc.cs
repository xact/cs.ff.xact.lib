﻿
namespace XAct.Environment
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Namespace to contain classes and services related to describing the Environment
    /// within which the Application is running. 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Why?
    /// </para>
    /// <para>
    /// Any library code will have to contain both windows and web specific code.
    /// The trick is how to write code that works in both, using the same services,
    /// without failing if System.Web is not available (as it would be in a Windows
    /// app).
    /// </para>
    /// <para>
    /// Not only must the code work, not drag in dependencies, but also not need
    /// elevated priveleges to run.
    /// </para>
    /// <para>
    /// A EnvironmentService can hide the complexities of determing different environment
    /// variables depending on the running context, making the rest of the library -- and
    /// your code that relies on it -- blissfully ignorant of the differences.
    /// </para>
    /// <para>Not only that, but it can provide other environmental variables -- such as Time --
    /// that make testing easier (have you ever tried changing the time on a server 
    /// just to check how Reporting will occur at the end of the month?).
    /// </para>
    /// </remarks>
    /// <internal>
    /// This class is required by Sandcastle to document Namespaces: http://bit.ly/vqzRiK
    /// </internal>
    [CompilerGenerated]
    class NamespaceDoc { }
}
