﻿
namespace XAct.Environment

{
    using System.Security.Principal;

    public interface IPrincipalService : IHasXActLibService
    {
        /// <summary>
        /// Gets or Sets the thread principal (and HttpContext User if within a web application).
        /// </summary>
        IPrincipal Principal { get; set; }



        /// <summary>
        /// Gets the current identity identifier (the most unique identifier for a user).
        /// <para>
        /// For a windows identity, this will be the SID, others, the name -- but 
        /// note that a name is not guaranteed to be consistent.
        /// </para>
        /// </summary>
        /// <value>
        /// The current identity identifier.
        /// </value>
        string CurrentIdentityIdentifier { get; }
    }
}
