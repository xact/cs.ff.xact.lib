﻿//namespace XAct.Environment
//{
//    /// <summary>
//    /// Contract for a service to return information regarding 
//    /// the company who built the application.
//    /// </summary>
//    public interface IProductDeveloperInformationService: IHasXActLibServiceDefinition
//    {

//        /// <summary>
//        /// Gets the Company associated to owning the product's production
//        /// (not the same as the Developer company, if it was outsourced).
//        /// </summary>
//        /// <value>
//        /// The application company.
//        /// </value>
//        string ApplicationCompany { get; }

//        /// <summary>
//        /// Gets the application name, as displayed in UI Views and other locations.
//        /// <para>
//        /// Not to be confused with the Assembly name.
//        /// </para>
//        /// <para>
//        /// If resold, can be different than base application name.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The application title.
//        /// </value>
//        string ApplicationTitle { get; }




//        /// <summary>
//        /// A short description of the application's functionality.
//        /// </summary>
//        string ApplicationDescription { get; }



//        /// <summary>
//        /// Gets the copyright text for the application.
//        /// <para>
//        /// When outsourced, this copyright generally belongs to the product owner.
//        /// <para>
//        /// When simply resold, the copyright is the same as the deveoper's
//        /// copyright.
//        /// </para>
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The application copyright.
//        /// </value>
//        string ApplicationCopyright { get; }



//    }
//}