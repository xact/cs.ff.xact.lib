﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Environment
{
    public interface IDateTimeService : IHasXActLibService
    {



        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IDateTimeServiceConfiguration Configuration { get; }


        /// <summary>
        /// Gets the current DateTime.
        /// <para>
        /// May be offset for testing purposes.
        /// </para>
        /// </summary>
        /// <remarks>
        /// Time can be offset by a number of different ways:
        /// <para>
        /// At Startup, via two variables in the config file:
        /// <code>
        /// <![CDATA[
        /// <configuration>
        ///   <configSections>
        ///     <sectionGroup name="applicationSettings" type="System.Configuration.ApplicationSettingsGroup, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" >
        ///       <section name="XAct.Properties.Settings" type="System.Configuration.ClientSettingsSection, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
        ///     </sectionGroup>
        ///   </configSections>
        ///   <applicationSettings>
        ///     <XAct.Properties.Settings>
        ///       <setting name="Now" serializeAs="String"><value>2011-08-01</value></setting>
        ///       <setting name="NowOffset" serializeAs="String"><value>00:00:00</value></setting>
        ///     </XAct.Properties.Settings>
        ///   </applicationSettings>
        /// </configuration>
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Via the ApplicationEnvironment method:
        /// <code>
        /// <![CDATA[
        /// ApplicationEnvironment.SetTime(DateTime.Now.AddDays(1));
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <value>The now.</value>
        /// <internal><para>6/8/2011: Sky</para></internal>
        [Obsolete("Use UTC, and convert to Local at UI level or other required ouput only.", false)]
        DateTime Now { get; }


        /// <summary>
        /// Gets the current DateTime UTC.
        /// <para>
        /// May be offset for testing purposes.
        /// </para>
        /// </summary>
        /// <remarks>
        /// Time can be offset by a number of different ways:
        /// <para>
        /// At Startup, via two variables in the config file:
        /// <code>
        /// <![CDATA[
        /// <configuration>
        ///   <configSections>
        ///     <sectionGroup name="applicationSettings" type="System.Configuration.ApplicationSettingsGroup, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" >
        ///       <section name="XAct.Properties.Settings" type="System.Configuration.ClientSettingsSection, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
        ///     </sectionGroup>
        ///   </configSections>
        ///   <applicationSettings>
        ///     <XAct.Properties.Settings>
        ///       <setting name="Now" serializeAs="String"><value>2011-08-01</value></setting>
        ///       <setting name="NowOffset" serializeAs="String"><value>00:00:00</value></setting>
        ///     </XAct.Properties.Settings>
        ///   </applicationSettings>
        /// </configuration>
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Via the ApplicationEnvironment method:
        /// <code>
        /// <![CDATA[
        /// ApplicationEnvironment.SetTime(DateTime.Now.AddDays(1));
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <value>The now.</value>
        /// <internal><para>6/8/2011: Sky</para></internal>
        DateTime NowUTC { get; }





        /// <summary>
        /// Use the given UTC DateTime to calculate the Offset, 
        /// that is applied to current time every time 
        /// NowUTC is requested.
        /// </summary>
        /// <param name="nowUTC">The now.</param>
        /// <internal><para>8/7/2011: Sky</para></internal>
        void SetUTCOffset(DateTime nowUTC);

        /// <summary>
        /// Sets the UTC Offset (for testing purposes).
        /// </summary>
        /// <param name="timeSpanOffset">The time span offset.</param>
        /// <internal>8/7/2011: Sky</internal>
        void SetUTCOffset(TimeSpan timeSpanOffset);

        /// <summary>
        /// Resets the UTC Offset to 0.
        /// </summary>
        void ResetUTCOffset();
    }
}
