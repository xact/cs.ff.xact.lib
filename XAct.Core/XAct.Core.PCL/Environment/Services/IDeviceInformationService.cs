﻿namespace XAct.Environment
{
    public interface IDeviceInformationService : IHasXActLibService
    {
        string GetUniqueDeviceID(string salt = null);
    }
}