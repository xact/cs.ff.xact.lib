﻿
namespace XAct.Environment
{
    /// <summary>
    /// Contract for a service to manage the Application's Environment.
    /// </summary>
    public interface IEnvironmentManagementService : IHasXActLibService
    {

        /// <summary>
        /// Restarts the application.
        /// </summary>
        void RestartApplication();
    }
}
