﻿namespace XAct.Environment
{

    /// <summary>
    /// Contract for a service to 
    /// return information regarding the product
    /// (title, subtitle, description, etc.)
    /// including information regarding the product's developer/owner/distributor.
    /// <para>
    /// A service implementation is configure using 
    /// <see cref="IProductInformationServiceConfiguration"/>
    /// </para>
    /// </summary>
    public interface IProductInformationService : IHasXActLibService
    {

        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IProductInformationServiceConfiguration Configuration { get; }

        /// <summary>
        /// Gets the specific product information,
        /// if any defined.
        /// </summary>
        /// <param name="productInformationType">Type of the product information.</param>
        /// <returns></returns>
        ProductInformation GetProductInformation(ProductInformationType productInformationType = ProductInformationType.Default);








        /// <summary>
        /// Gets or sets the unique name of the company.
        /// <para>
        /// The is the 'short' name of the company (eg: 'Adobe')
        /// as compared to the full diplay name of the company
        /// as referenced via <see cref="CompanyDisplayNameResourceKey"/>
        /// </para>
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        string CompanyName(ProductInformationType productInformationType = ProductInformationType.Default);



        /// <summary>
        /// Gets or sets the unique name of the product.
        /// </summary>
        string ProductName(ProductInformationType productInformationType = ProductInformationType.Default);





        /// <summary>
        /// Gets the resource filter
        /// to use to retrieve production information (display title, display subtitle, etc.)
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string ResourceFilter(ProductInformationType productInformationType = ProductInformationType.Default);






        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// display company name resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string CompanyDisplayNameResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);


        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// display company adddress resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string CompanyDisplayAddressResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);
        
        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// display company contact information resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string CompanyDisplayContactInformationResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);


        

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// display copyright resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string ProductDisplayCopyrightResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);












        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// display Name/Title resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string ProductDisplayTitleResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// display SubTitle resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string ProductDisplaySubTitleResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// (one paragraph) display description resource string
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string ProductDisplayDescriptionResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);


        
        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// Url (from which they can retrieve additional information on support, etc._
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// </summary>
        string ProductDisplayProductUrlResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);


        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's base
        /// API endpoint
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        string ProductAPIBaseEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);


        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// API endpoint -- for reporting errors --
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        string ProductAPIErrorReportingEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);


        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// API endpoint -- for various support operations --
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        string ProductAPISupportEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// API endpoint -- to invoke for licensing checks --
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// <para>
        /// For example, can be:
        /// <code>
        /// <![CDATA[
        /// GET http://mycompany.com/API/v3/licenses/...sn_of_app
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        string ProductAPILicensingEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);

        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of an API endpoint
        /// to invoke for checks for later versions.
        /// <para>
        /// Most platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        /// <value>
        /// The product API updates uvailable endpoint resource key.
        /// </value>
        string ProductAPIUpdatesAvailableEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default);

    }
}
