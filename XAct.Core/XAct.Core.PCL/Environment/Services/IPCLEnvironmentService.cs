﻿namespace XAct.Environment.Implementations
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPCLEnvironmentService : IEnvironmentService, IHasXActLibService
    {
        /// <summary>
        /// Gets or sets the singleton configuration settings used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        new IPCLEnvironmentServiceConfiguration Configuration { get; }   
    }
}