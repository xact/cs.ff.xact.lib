﻿namespace XAct.Environment
{
    using System;
    using System.Reflection;

    public interface IEnvironmentServiceConfiguration : IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets or sets the Entry Assembly.
        /// <para>This will be used by <see cref="IProductInformationService"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The entry assembly.
        /// </value>
        Assembly EntryAssembly { get; set; }


        string AppDir { get; set; }

        /// <summary>
        /// The unique name of this application.
        /// <para>
        /// Default is "/"
        /// </para>
        /// </summary>
        string ApplicationIdentifier { get; set; }


        /// <summary>
        /// Gets or sets the a string that identifies
        /// which environment we are in (PROD/ST/SIT/UAT/PROD, etc.).
        /// </summary>
        /// <value>
        /// The environment identifier.
        /// </value>
        string EnvironmentIdentifier { get; set; }

        /// <summary>
        /// Gets a value indicating whether the currrent process is interactive.
        /// <para>
        /// A Windows Service (unless it has 'interact with desktop' set to true)
        /// would return false. So would IIS. In such cases, don't show Modal dialogs
        /// ...as nobody will be able to reacto them.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is interactive; otherwise, <c>false</c>.
        /// </value>
        bool UserInteractive { get; set; }


        /// <summary>
        /// Gets the name of the current user's domain (eg: 'CORP').
        /// </summary>
        /// <value>The name of the domain.</value>
        string DomainName { get; set; }

        /// <summary>
        /// Gets the name of the machine.
        /// </summary>
        /// <value>The name of the machine.</value>
        string MachineName { get; set; }


    }
}
