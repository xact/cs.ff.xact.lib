﻿namespace XAct.Environment.Implementations
{
    using System.Reflection;
    using XAct.Services;

    public class ProductInformationServiceConfiguration : IProductInformationServiceConfiguration, IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Information about the Application.
        /// </summary>
        public ProductInformation DeveloperInformation { get; set; }

        /// <summary>
        /// Information about the Owner of the application.
        /// <para>
        /// If not provided, the implementation of
        /// <see cref="IProductInformationService" />
        /// uses the <see cref="DeveloperInformation" />
        /// </para>
        /// </summary>
        public ProductInformation OwnerInformation { get; set; }

        /// <summary>
        /// Information about the Owner of the application.
        /// <para>
        /// If not provided, the implementation of
        /// <see cref="IProductInformationService" />
        /// uses the <see cref="OwnerInformation" />
        /// </para>
        /// </summary>
        public ProductInformation ResellerInformation { get; set; }

        ///// <summary>
        ///// The Aplication's entry assembly -- or whatever assembly
        ///// the Product Name, copyright, etc. should be extracted from.
        ///// </summary>
        //        public Assembly Assembly { get; set; }


    }
}