﻿namespace XAct.Environment
{
    using System;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IEnvironmentServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.Low /*OK:overrriden by FF*/)]
    public class PCLEnvironmentServiceConfiguration : IPCLEnvironmentServiceConfiguration, IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// The unique name of this application.
        /// <para>
        /// Default is "/"
        /// </para>
        /// </summary>
        public string ApplicationIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the name of the current user's domain (eg: DCCORP).
        /// <para>
        /// Note:
        /// Will come back as CORP\SkyS whether signed
        /// in using CORP\SkyS or CORP.local\SkyS
        /// return WindowsIdentity.GetCurrent().Name;
        /// </para>
        /// </summary>
        /// <value>The name of the domain.</value>
        public string DomainName { get; set; }

        /// <summary>
        /// Gets or sets the name of the machine (SKYSW7LT).
        /// </summary>
        /// <value>The name of the machine.</value>
        public string MachineName { get; set; }


        /// <summary>
        /// Gets or sets the a string that identifies
        /// which environment we are in (PROD/ST/SIT/UAT/PROD, etc.).
        /// </summary>
        /// <value>
        /// The environment identifier.
        /// </value>
        public string EnvironmentIdentifier { get; set; }



        /// <summary>
        /// Gets a value indicating whether the currrent process is interactive.
        /// <para>
        /// A Windows Service (unless it has 'interact with desktop' set to true)
        /// would return false. So would IIS. In such cases, don't show Modal dialogs
        /// ...as nobody will be able to reacto them.
        /// </para>
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is interactive; otherwise, <c>false</c>.
        /// </value>
        public bool UserInteractive { get; set; }





        public PCLEnvironmentServiceConfiguration()
        {
            ApplicationIdentifier = "/";
            EnvironmentIdentifier = "/";
            UserInteractive = false;
            AppDir = string.Empty;
        }

        /// <summary>
        /// Gets or sets the Entry Assembly.
        /// <para>This will be used by <see cref="IProductInformationService" />
        /// </para>
        /// </summary>
        /// <value>
        /// The entry assembly.
        /// </value>
        public System.Reflection.Assembly EntryAssembly { get; set; }



        /// <summary>
        /// Gets or sets the application dir.
        /// <para>
        /// In a FF app, this is AppDomain.CurrentDomain.BaseDirectory 
        /// and therefore will be foo/bar/bin/debug in NUnit and Console apps.
        /// In a FF web app, this will be foo/bar (excluding bin/debug)
        /// </para>
        /// <para>
        /// WARNING: In a PCL environment, this will not be set.
        /// </para>
        /// </summary>
        /// <value>
        /// The application dir.
        /// </value>
        public string AppDir
        {
            get; 
            set;
        }

    }
}