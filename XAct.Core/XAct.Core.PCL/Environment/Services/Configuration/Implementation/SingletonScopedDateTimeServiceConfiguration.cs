﻿namespace XAct.Environment
{
    using System;
    using XAct.Services;

    public class DateTimeServiceConfiguration : IDateTimeServiceConfiguration, IHasXActLibServiceConfiguration
    { 
        /// <summary>
        /// Gets or sets the now UTC offset to apply to <see cref="IEnvironmentService.NowUTC"/>.
        /// </summary>
        /// <value>
        /// The now UTC offset.
        /// </value>
        public TimeSpan NowUtcOffset { get; set; }



        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="SingletonScopedDateTimeServiceConfiguration"/> class.
        /// </summary> 
        public DateTimeServiceConfiguration()
        {
            NowUtcOffset = new TimeSpan();
        }

    }
}