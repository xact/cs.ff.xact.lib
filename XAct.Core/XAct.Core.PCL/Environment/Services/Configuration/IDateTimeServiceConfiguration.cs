﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Environment
{
    public interface IDateTimeServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the now UTC offset to apply to <see cref="IEnvironmentService.NowUTC"/>.
        /// </summary>
        /// <value>
        /// The now UTC offset.
        /// </value>
        TimeSpan NowUtcOffset { get; set; }

    }
}
