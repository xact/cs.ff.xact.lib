﻿namespace XAct.Environment
{
    using System.Reflection;

    /// <summary>
    /// Contract for configuration information for 
    /// <see cref="IProductInformationService"/>
    /// </summary>
    public interface IProductInformationServiceConfiguration : IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Information about the Application.
        /// </summary>
        ProductInformation DeveloperInformation { get; set; }

        /// <summary>
        /// Information about the Owner of the application.
        /// <para>
        /// If not provided, the implementation of
        /// <see cref="IProductInformationService"/>
        /// uses the <see cref="DeveloperInformation"/>
        /// </para>
        /// </summary>
        ProductInformation OwnerInformation { get; set; }

        /// <summary>
        /// Information about the Owner of the application.
        /// <para>
        /// If not provided, the implementation of
        /// <see cref="IProductInformationService"/>
        /// uses the <see cref="OwnerInformation"/>
        /// </para>
        /// </summary>
        ProductInformation ResellerInformation { get; set; }


        
        
        ///// <summary>
        ///// The Aplication's entry assembly -- or whatever assembly
        ///// the Product Name, copyright, etc. should be extracted from.
        ///// </summary>
        //Assembly Assembly { get; set; }


        
    }
}