﻿namespace XAct.Environment.Services.Implementations
{
    using System.Globalization;
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="IClientEnvironmentService"/>
    /// to provide information about a remote 
    /// User Agent / Browser's environment.
    /// </summary>
    public class ClientEnvironmentService : IClientEnvironmentService
    {
        /// <summary>
        /// Gets the remote User Agent's CurrentUICulture
        /// (if set).
        /// </summary>
        public virtual CultureInfo ClientUICulture
        {
            get
            {
                //return just the Thread's.
                return System.Threading.Thread.CurrentThread.CurrentUICulture;
            }
        }

        /// <summary>
        /// Gets or sets the IP of the remote User Agent/Browser
        /// <para>
        /// returns null when the HttpContext.Current is not available.
        /// </para>.
        /// </summary>
        /// <value>
        /// The client ip.
        /// </value>
        /// <exception cref="System.NotImplementedException">
        /// </exception>
        public virtual string ClientIP
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }
    }
}