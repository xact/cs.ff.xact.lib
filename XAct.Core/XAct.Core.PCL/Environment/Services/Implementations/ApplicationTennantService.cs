﻿namespace XAct.Environment.Services.Implementations
{
    using System;
    using XAct.Services;

    public class ApplicationTennantService : IApplicationTennantService
    {
        private readonly IApplicationTennantManagementService _applicationTennantManagementService;

        public Guid Get()
        {
            return _applicationTennantManagementService.Get();
        }

        public ApplicationTennantService(IApplicationTennantManagementService applicationTennantManagementService)
        {
            _applicationTennantManagementService = applicationTennantManagementService;
        }
    }
}