﻿namespace XAct.Environment.Services.Implementations
{
    using System.Security.Principal;
    using XAct.Services;
    using XAct.State;

    [DefaultBindingImplementation(typeof(IPrincipalService),BindingLifetimeType.Undefined,Priority.Low /*OK:Overridden*/)]
    public class DefaultPrincipalService : IDefaultPrincipalService
    {
        private readonly IContextStateService _contextStateService;
        private readonly IEnvironmentService _environmentService;

        public DefaultPrincipalService(IContextStateService contextStateService,IEnvironmentService environmentService)
        {
            _contextStateService = contextStateService;
            _environmentService = environmentService;
        }

        /// <summary>
        /// Sets the thread principal (and HttpContext User if within a web application).
        /// </summary>
        public IPrincipal Principal
        {
            get
            {

                return _contextStateService.Items["IPrincipal"] as IPrincipal;
            }
            set
            {
                IPrincipal principal = value;

                SetHttpUserPropertyIfAvailable(principal);
                //Set on thread *AS WELL*:
                _contextStateService.Items["IPrincipal"] = principal;
                

            }
        }


        /// <summary>
        /// Gets the current identity identifier (the most unique identifier for a user).
        /// <para>
        /// For a windows identity, this will be the SID, others, the name -- but 
        /// note that a name is not guaranteed to be consistent.
        /// </para>
        /// </summary>
        /// <value>
        /// The current identity identifier.
        /// </value>
        public string CurrentIdentityIdentifier
        {
            get
            {
                ////System.Security.Principal.
                //WindowsIdentity wi =
                //    CurrentPrincipal.Identity as WindowsIdentity;
                //if ((wi != null) && (wi.User != null))
                //{
                //    return wi.User.AccountDomainSid.ToString();

                //    //return wi.Owner.AccountDomainSid.ToString();
                //}

                if ((this.Principal!=null))
                {
                    var result = this.Principal.Identity.Name;
                    return result;
                }
                return null;
            }
        }





        private void SetHttpUserPropertyIfAvailable(IPrincipal principal)
        {
            object httpContext = this._environmentService.HttpContext;

            //But set on Context:
            if (httpContext != null)
            {
                httpContext.GetType().GetProperty("User").SetValue(httpContext, principal, null);
            }
        }
    }
}