﻿
// ReSharper disable CheckNamespace
namespace XAct.Environment.Services.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using XAct.Environment.Implementations;
    using XAct.Services;

    /// <summary>
    ///   Implementation of <see cref = "IEnvironmentService" />,
    ///   to handle queries about the 
    ///   application environment (base path, memory used, etc).
    /// </summary>
    /// <remarks>
    /// Why not just use the ApplicationEnvironment class directly?
    /// <para>
    /// Good question.
    /// </para>
    /// <para>
    /// ApplicationEnvironment is a static class...which is exactly
    /// what we are trying to avoid using in other assemblies (a direct
    /// ref to a static class is what got us into Monolithic development
    /// in the first place, right?!
    /// </para>
    /// <para>
    /// This is an just one way of implementating the EnvironmentService contract.
    /// That when used with an DependencyInjectionContainer protects the rest of the code from reference
    /// to any specific class.
    /// It just so happens to use ApplciationEnvironment -- but the end user
    /// doesn't need to know that. The result is highly decoupled code...even
    /// for the basic services such as logging and Environment...
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(IEnvironmentService), BindingLifetimeType.Undefined, Priority.Low /*OK: overridden by FF*/)]
    public class PCLEnvironmentService : IPCLEnvironmentService
    {

        
        private readonly IEnvironmentManagementService _environmentManagementService;

        
        private object _httpContext = 0;

        private string _newLine;



        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IEnvironmentServiceConfiguration IEnvironmentService.Configuration
        {
            get { return _pclenvironmentServiceConfiguration; }
        }


        /// <summary>
        /// Gets or sets the singleton configuration settings used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IPCLEnvironmentServiceConfiguration Configuration { get { return _pclenvironmentServiceConfiguration; } }
        private readonly IPCLEnvironmentServiceConfiguration _pclenvironmentServiceConfiguration;




        /// <summary>
        /// Initializes a new instance of the <see cref="PCLEnvironmentService" /> class.
        /// </summary>
        /// <param name="environmentManagementService">The environment management service.</param>
        /// <param name="environmentServiceConfiguration">The environment service configuration.</param>
        public PCLEnvironmentService(IPCLEnvironmentServiceConfiguration environmentServiceConfiguration,
                                  IEnvironmentManagementService environmentManagementService)
        {
            _environmentManagementService = environmentManagementService;
            _pclenvironmentServiceConfiguration = environmentServiceConfiguration;
        }

        /// <summary>
        /// Gets a Random Generator.
        /// </summary>
        /// <value>The random.</value>
        public Random Random { get { return _random ?? (_random = new Random()); } }
        private Random _random;


        /// <summary>
        /// Gets or sets the default culture.
        /// <para>Default value = 'en'</para>
        /// </summary>
        /// <value>
        /// The default culture.
        /// </value>
        public CultureInfo DefaultCulture
        {
            get { return XAct.Library.Settings.Globalisation.DefaultUICulture; }
        }



        /// <summary>
        /// Gets a value indicating whether the currrent process is interactive.
        /// <para>
        /// A Windows Service (unless it has 'interact with desktop' set to true)
        /// would return false. So would IIS. In such cases, don't show Modal dialogs
        /// ...as nobody will be able to reacto them.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is interactive; otherwise, <c>false</c>.
        /// </value>
        public bool IsUserInteractive
        {
            get
            {
                return _pclenvironmentServiceConfiguration.UserInteractive;
            }
        }



        /// <summary>
        /// Gets the name of the current user's domain (eg: JQDEV).
        /// </summary>
        /// <value>The name of the domain.</value>
        public string DomainName
        {
            get
            {
                return _pclenvironmentServiceConfiguration.DomainName;
            }
        }

        /// <summary>
        /// Gets the name of the machine.
        /// </summary>
        /// <value>The name of the machine.</value>
        public string MachineName
        {
            get
            {
                return _pclenvironmentServiceConfiguration.MachineName;
            }
        }








        /// <summary>
        ///   Gets the name of the application.
        /// <para>
        ///   Gets or Sets the name of the application 
        ///   using the custom membership provider.
        ///   Default value is '/'
        /// </para>
        /// </summary>
        /// <value>The name of the application.</value>
        public string ApplicationName
        {
            get
            {
                return _pclenvironmentServiceConfiguration.ApplicationIdentifier;
            }
        }

        public string EnvironmentName
        {
            get { return _pclenvironmentServiceConfiguration.EnvironmentIdentifier; }
        }

        /// <summary>
        /// Gets the cached Hash of the lowercased <see cref="ApplicationName"/> .
        /// </summary>
        public string ApplicationNameHash
        {
            get
            {
                if (_applicationName != ApplicationName)
                {
                    _applicationName = ApplicationName;
                    _applicationNameHash = _applicationName.ToLowerInvariant().CalculateHash();
                }
                return _applicationNameHash;
            }
        }

        private static string _applicationName;
        private static string _applicationNameHash;


        /// <summary>
        /// Gets or sets the entry assembly.
        /// <para>
        /// If no assembly given, uses <see cref="System.Reflection.Assembly.GetEntryAssembly"/>
        /// </para>
        /// <para>
        /// IMPORTANT: It is always preferable to retrieve the desired Assembly (GetEntryAssembly is null in ASP.NET, and UniTests).
        /// </para>
        /// </summary>
        /// <value>
        /// The entry assembly.
        /// </value>
        public Assembly EntryAssembly 
        {
            get
            {
                return _pclenvironmentServiceConfiguration.EntryAssembly;
            }
        }







        /// <summary>
        /// Maps the VirtualPath ('~/SubDir/SomeFile.htm') to the Apps dir ('c:\somedir\SubDir\SomeFile.htm');
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        /// <internal><para>8/2/2011: Sky</para></internal>
        public string MapPath(string virtualPath)
        {
            throw new NotImplementedException("Not yet sure how I will do MapPath under PCL.");
        }


        /// <summary>
        ///   Gets the application's base path.
        /// </summary>
        /// <value>The application's base path.</value>
        public string ApplicationBasePath
        {
            get
            {
                return _pclenvironmentServiceConfiguration.AppDir;
            }
        }

        /// <summary>
        /// Gets the HTTP context.
        /// </summary>
        /// <value>The HTTP context.</value>
        public object HttpContext
        {
            get
            {
                ////As it's a instantiatable class, we can do slightly better
                ////than the underlying class:
                //if (!_httpContext.Equals(0))
                //{
                //    return _httpContext;
                //}

                _httpContext = ApplicationEnvironment.HttpContext;

                return _httpContext;
            }
        }


        /// <summary>
        /// Returns a context relative new line ("<br/>" when in an HttpContext, Environment.NewLine in windows apps).
        /// </summary>
        /// <value></value>
        /// <internal><para>7/17/2011: Sky</para></internal>
        public string NewLine
        {
            get { return _newLine ?? (_newLine = (HttpContext != null) ? "<br/>" : System.Environment.NewLine); }
        }





        #region IEnvironmentService Members


        ///// <summary>
        /////   Gets the name of the company registered on the given assembly.
        ///// </summary>
        ///// <param name = "assembly">The assembly.</param>
        ///// <returns>The company name.</returns>
        //public string GetCompanyName(Assembly assembly)
        //{
        //    return Environment.GetCompanyName(assembly);
        //}


        /// <summary>
        ///   Gets the total amount of memory thought to be allocated.
        /// </summary>
        /// <param name = "waitForGarbageCollectionBeforeReturning">Wait for garbage collection before returning, or return immediately.</param>
        /// <returns>The amount of memory allocated</returns>
        /// <internal>
        /// Available in Silverlight as well since Ag 3
        /// Can't find mention of it being in in CF
        /// </internal>
        public long GetTotalMemoryAllocated(bool waitForGarbageCollectionBeforeReturning=false)
        {
            //Note: Available in Silverlight as well since Ag 3
            return GC.GetTotalMemory(waitForGarbageCollectionBeforeReturning);
        }

        #endregion

        //
        //public void RestartApplication ()
        //{
        //    if (this.HttpContext == null)
        //    {
        //        http://geekswithblogs.net/mtreadwell/archive/2004/06/06/6123.aspx
        //        System.Diagnostics.Process.Start(this.EntryAssembly.Location);
        //        Application.Current.Shutdown();
        //        if (System.Windows.Forms.Application.MessageLoop)
        //          {
        //          Use this since we are a WinForms app
        //          System.Windows.Forms.Application.Exit()
        //          }
        //          else{
        //            Use this since we are a console app
        //            System.Environment.Exit(1)
        //          }
        //    }
        //    else
        //    {
        //      System.Web.HttpRuntime.UnloadAppDomain();
        //    }
        //}
    }
}