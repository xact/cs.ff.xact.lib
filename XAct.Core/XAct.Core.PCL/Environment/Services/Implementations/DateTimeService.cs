namespace XAct.Environment.Services.Implementations
{
    using System;
    using XAct.Services;

    public class DateTimeService : IDateTimeService
    {
        
        public IDateTimeServiceConfiguration Configuration
        {
            get { return _configuration; }
        }

        private IDateTimeServiceConfiguration _configuration;

        /// <summary>
        /// Gets the current DateTime.
        /// <para>
        /// May be offset for testing purposes.
        /// </para>
        /// </summary>
        /// <remarks>
        /// Time can be offset by a number of different ways:
        /// <para>
        /// At Startup, via two variables in the config file:
        /// </para>
        /// </remarks>
        /// <value>The now.</value>
        /// <internal>6/8/2011: Sky</internal>
        /// <internal><para>6/8/2011: Sky</para></internal>
        [Obsolete("Use UTC, and convert to Local at UI level or other required ouput only.", false)]
        public DateTime Now
        {
            get
            {
                var result = this.NowUTC.ToLocalTime();
                return result;
            }
        }


        /// <summary>
        /// Gets the current DateTime UTC.
        /// <para>
        /// May be offset for testing purposes.
        /// </para>
        /// </summary>
        /// <value>The now.</value>
        public DateTime NowUTC
        {
            get
            {
                //Given we are 1 July, we want to get back to July second,
                //1 - (-1) = 2
                DateTime offsetTime = DateTime.UtcNow.Subtract(_configuration.NowUtcOffset);

                return offsetTime;

            }
        }
        ///// <summary>
        ///// Gets the current DateTime UTC.
        ///// <para>
        ///// May be offset for testing purposes.
        ///// </para>
        ///// </summary>
        ///// <value>The now.</value>
        //public DateTime NowUTC
        //{
        //    get
        //    {
        //        //Given we are 1 July, we want to get back to July second,
        //        //1 - (-1) = 2
        //        DateTime offsetTime = DateTime.UtcNow.Subtract(_configuration.NowUtcOffset);

        //        return offsetTime;

        //    }
        //}


        /// <summary>
        /// Use the given UTC DateTime to calculate the Offset, 
        /// that is applied to current time every time 
        /// NowUTC is requested.
        /// </summary>
        /// <param name="nowUTC">The now.</param>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public void SetUTCOffset(DateTime nowUTC)
        {
            if (nowUTC.Kind != DateTimeKind.Utc)
            {
                throw new Exception("SetNowUTC expects a dateTime in UTC format.");
            }
            if (nowUTC == DateTime.MinValue)
            {
                return;
            }

            //If given future date, will be negativeAmount.
            //1 July - 2 July = -1 day
            //Eg:
            _configuration.NowUtcOffset = DateTime.UtcNow.Subtract(nowUTC);
        }
        /// <summary>
        /// Sets the UTC Offset (for testing purposes).
        /// </summary>
        /// <param name="timeSpanOffset">The time span offset.</param>
        /// <internal>8/7/2011: Sky</internal>
        public void SetUTCOffset(TimeSpan timeSpanOffset)
        {
            _configuration.NowUtcOffset = timeSpanOffset;

        }


        /// <summary>
        /// Resets the UTC Offset to 0.
        /// </summary>
        public void ResetUTCOffset()
        {
            _configuration.NowUtcOffset = default(TimeSpan);
        }




        public DateTimeService(IDateTimeServiceConfiguration dateTimeServiceConfiguration)
        {
            _configuration = dateTimeServiceConfiguration;
        }

    }
}