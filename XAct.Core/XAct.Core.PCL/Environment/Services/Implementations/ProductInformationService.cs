﻿namespace XAct.Environment.Services.Implementations
{
    using System;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IProductInformationService"/> to 
    /// return information regarding the product
    /// (title, subtitle, description, etc.)
    /// including information regarding the product's developer/owner/distributor.
    /// </summary>
    public class ProductInformationService : IProductInformationService
    {
        private ProductInformation[] _productInformations;



        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IProductInformationServiceConfiguration Configuration
        {
            get
            {
                return _productInformationServiceConfiguration;
            }
        }
        private readonly IProductInformationServiceConfiguration _productInformationServiceConfiguration;


        /// <summary>
        /// Initializes a new instance of the <see cref="ProductInformationService"/> class.
        /// </summary>
        /// <param name="productInformationServiceConfiguration">The product information service configuration.</param>
        public ProductInformationService(IProductInformationServiceConfiguration productInformationServiceConfiguration)
        {
            
            _productInformationServiceConfiguration = productInformationServiceConfiguration;

            _productInformations = new[]
                {
                    //Positions in array line up with enum values of ProductInformationType
                    null/*0*/,
                    _productInformationServiceConfiguration.DeveloperInformation/*1*/,
                    _productInformationServiceConfiguration.OwnerInformation/*2*/,
                    _productInformationServiceConfiguration.ResellerInformation/*3*/
                };
        }



        
        /// <summary>
        /// Gets the specific product information,
        /// if any defined.
        /// </summary>
        /// <param name="productInformationType">Type of the product information.</param>
        /// <returns></returns>
        public ProductInformation GetProductInformation(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            return _productInformations[(int) productInformationType];
        }









        /// <summary>
        /// Gets the resource filter
        /// to use to retrieve production information (display title, display subtitle, etc.)
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ResourceFilter(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            var result = this.FallthruValue(m => m.ResourceFilter);
            return result;
        }

        /// <summary>
        /// Gets or sets the unique name of the company.
        /// <para>
        /// The is the 'short' name of the company (eg: 'Adobe')
        /// as compared to the full diplay name of the company
        /// as referenced via <see cref="CompanyDisplayNameResourceKey"/>
        /// </para>
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string CompanyName(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.CompanyName, productInformationType, allowFallthru);
            return result;
        }

        /// <summary>
        /// Gets or sets the unique name of the product.
        /// <para>
        /// The is the 'short' name of the product
        /// as compared to the full diplay name of the company
        /// as referenced via <see cref="ProductDisplayTitleResourceKey"/>
        /// </para>
        /// <para>
        /// Note they very easily can be the same.
        /// </para>
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductName(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductName, productInformationType, allowFallthru);
            return result;
        }



        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// display company name resource string
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string CompanyDisplayNameResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.CompanyDisplayNameResourceKey, productInformationType, allowFallthru);
            return result;
        }
        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// display company adddress resource string
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string CompanyDisplayAddressResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.CompanyDisplayAddressResourceKey, productInformationType, allowFallthru);
            return result;
        }
        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// display company contact information resource string
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string CompanyDisplayContactInformationResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.CompanyDisplayContactInformationResourceKey, productInformationType, allowFallthru);
            return result;
        }





        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// display copyright resource string
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductDisplayCopyrightResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductDisplayCopyrightResourceKey, productInformationType, allowFallthru);
            return result;
        }






        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// display Name/Title resource string
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductDisplayTitleResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;

            var result = this.FallthruValue(m => m.ProductDisplayTitleResourceKey, productInformationType, allowFallthru)
                ?? this.ProductName(productInformationType);
            return result;
        }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// display SubTitle resource string
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductDisplaySubTitleResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;

            var result = this.FallthruValue(m => m.ProductDisplaySubTitleResourceKey, productInformationType, allowFallthru);
            return result;
        }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// (one paragraph) display description resource string
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductDisplayDescriptionResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductDisplayDescriptionResourceKey, productInformationType, allowFallthru);
            return result;
        }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// Url (from which they can retrieve additional information on support, etc._
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductDisplayProductUrlResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductDisplayProductUrlResourceKey, productInformationType, allowFallthru);
            return result;
        }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's base
        /// API endpoint
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductAPIBaseEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductAPIBaseEndpointResourceKey, productInformationType, allowFallthru);
            return result;
        }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve 
        /// the product's 
        /// API endpoint -- for reporting errors --
        /// from an implementation of 
        /// <c>IResourceService</c>.
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        public string ProductAPIErrorReportingEndpointResourceKey(
            ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductAPIErrorReportingEndpointResourceKey, productInformationType, allowFallthru);
            return result;
            
        }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// API endpoint -- for various support operations --
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductAPISupportEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductAPISupportEndpointResourceKey, productInformationType, allowFallthru);
            return result;
        }

        /// <summary>
        /// Gets the resource key
        /// to use to retrieve
        /// the product's
        /// API endpoint -- to invoke for licensing checks --
        /// from an implementation of
        /// <c>IResourceService</c>.
        /// <para>
        /// For example, can be:
        /// <code>
        /// <![CDATA[
        /// GET http://mycompany.com/API/v3/licenses/...sn_of_app
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Many modern platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        public string ProductAPILicensingEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductAPILicensingEndpointResourceKey, productInformationType, allowFallthru);
            return result;
        }

        /// <summary>
        /// Gets or sets the
        /// Resource Key
        /// of an API endpoint
        /// to invoke for checks for later versions.
        /// <para>
        /// Most platforms provide their own AppStore mechanism
        /// these days. But for legacy apps this is still relevant.
        /// </para>
        /// </summary>
        /// <param name="productInformationType"></param>
        /// <returns></returns>
        /// <value>
        /// The product API updates uvailable endpoint resource key.
        ///   </value>
        public string ProductAPIUpdatesAvailableEndpointResourceKey(ProductInformationType productInformationType = ProductInformationType.Default)
        {
            bool allowFallthru = true;
            var result = this.FallthruValue(m => m.ProductAPIUpdatesUvailableEndpointResourceKey, productInformationType, allowFallthru);
            return result;
        }






        
        private string FallthruValue(Func<ProductInformation, string> methodName, ProductInformationType productInformationType= ProductInformationType.LicensedSeller, bool allowFallthru=false)
        {
            string result=null;

            int iStart = (int) productInformationType;

            for (int i = iStart; i > 0; i--)
            {
                ProductInformation productInformation = _productInformations[i];

                if (productInformation == null)
                {
                    continue;
                }

                result = methodName.Invoke(productInformation);
                
                if (!result.IsNullOrEmpty())
                {
                    //Found one
                    return result;
                }
                if (!allowFallthru)
                {
                    break;
                }
            }
            return result;
        }
    }
}