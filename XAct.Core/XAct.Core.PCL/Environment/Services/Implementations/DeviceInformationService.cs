﻿namespace XAct.Environment.Services.Implementations
{
    using XAct.Services;

    public class DeviceInformationService :IDeviceInformationService
    {
        private readonly IEnvironmentService _environmentService;


        public DeviceInformationService(IEnvironmentService environmentService)
        {
            _environmentService = environmentService;
        }




        /// <summary>
        ///   Gets the unique device ID.
        ///   <para>
        ///     This is a Unique ID of the phone, 
        ///     hashed with the supplied key.
        ///   </para>
        /// </summary>
        /// <param name = "hashSalt">The hash key.</param>
        /// <remarks>
        ///   <para>
        ///   </para>
        /// </remarks>
        /// <internal>
        ///   See: http://www.eggheadcafe.com/forumarchives/
        ///   pocketpcdeveloper/jan2006/post25956729.asp
        /// </internal>
        /// <returns>A 40 char (20bytes x 2chars) string.</returns>
        public string GetUniqueDeviceID(string hashSalt=null)
        {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
            //PC:
            //Do Nothing.
#else
            if (string.IsNullOrEmpty(_cachedDeviceUniqueId)){
                _cachedDeviceUniqueId =CFSpecific.GetUniqueDeviceIdOnCF(hashkey); 
            }
#endif

            if (string.IsNullOrEmpty(_cachedDeviceUniqueId))
            {
                if (hashSalt ==null)
                {
                    hashSalt = "Default";
                }

                _cachedDeviceUniqueId =
                    (hashSalt.ToLower() + ":" + _environmentService.DomainName + "\\" + _environmentService.MachineName).CalculateHash();
            }

            return CachedDeviceUniqueId;
        }


        public static string CachedDeviceUniqueId { get { return _cachedDeviceUniqueId; } set { _cachedDeviceUniqueId = value; } }
        private static string _cachedDeviceUniqueId;

        
    }
}