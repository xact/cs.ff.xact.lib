﻿

namespace XAct.Environment.Services.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Services;

    public class ApplicationTennantManagementService : XActLibServiceBase, IApplicationTennantManagementService
    {
        [ThreadStatic] 
        static Guid id;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationTennantManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ApplicationTennantManagementService(ITracingService tracingService) : base(tracingService)
        {
        }

        /// <summary>
        /// Gets this current user's Application Tennant Id  (ie, OrganisationId).
        /// </summary>
        /// <returns></returns>
        public Guid Get()
        {
            return id;
        }
        /// <summary>
        /// Sets this current user's Application Tennant Id  (ie, OrganisationId).
        /// </summary>
        public void Set(Guid value)
        {
            id = value;
        }
    }
}
