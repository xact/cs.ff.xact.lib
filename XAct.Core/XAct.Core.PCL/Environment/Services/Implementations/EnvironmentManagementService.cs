namespace XAct.Environment.Services.Implementations
{
    using System;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="IEnvironmentManagementService"/>
    /// to manage the Application's Environment.
    /// </summary>
    public class EnvironmentManagementService : IEnvironmentManagementService
    {


        /// <summary>
        /// Restarts the application.
        /// </summary>
        public void RestartApplication()
        {
            RestartServerAppPool();
        }

        /// <summary>
        /// Restarts the server's AppPool.
        /// </summary>
        public void RestartServerAppPool()
        {
            
            throw new NotImplementedException();
            
            //This needs to be converted to Reflection.
            //HttpRuntime.UnloadAppDomain();
        }
    }
}