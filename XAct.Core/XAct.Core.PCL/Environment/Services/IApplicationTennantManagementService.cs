﻿namespace XAct.Environment
{
    using System;

    public interface IApplicationTennantManagementService : IHasXActLibService
    {
        /// <summary>
        /// Gets this current user's Application Tennant Id  (ie, OrganisationId).
        /// </summary>
        Guid Get();
        /// <summary>
        /// Sets this current user's Application Tennant Id  (ie, OrganisationId).
        /// </summary>
        void Set(Guid value);

    }
}