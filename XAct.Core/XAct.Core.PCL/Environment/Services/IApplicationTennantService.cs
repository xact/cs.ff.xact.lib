﻿using System;

namespace XAct.Environment
{
    public interface IApplicationTennantService : IHasXActLibService
    {

        Guid Get();
    }
}
