﻿namespace XAct.Environment
{
    using System.Globalization;

    /// <summary>
    /// A service to encapsulate information about a 
    /// remote User Agent accessing the system.
    /// </summary>
    public interface IClientEnvironmentService : IHasXActLibService
    {
        /// <summary>
        /// Gets the remote User Agent's CurrentUICulture
        /// (if set).
        /// </summary>
        CultureInfo ClientUICulture { get;  }

        /// <summary>
        /// Gets or sets the IP of the remote User Agent/Browser
        /// <para>
        /// returns null when the HttpContext.Current is not available.
        /// </para>.
        /// </summary>
        /// <value>
        /// The client ip.
        /// </value>
        string ClientIP { get;  }
    }
}
