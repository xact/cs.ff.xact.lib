﻿namespace XAct.Environment
{
    using System;
    using System.Globalization;
    using XAct.Settings;

    /// <summary>
    ///   Contract for a Service that handles 
    ///   Environment operations (get base dir, etc).
    /// </summary>
    public interface IEnvironmentService : IHasXActLibService
    {

        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IEnvironmentServiceConfiguration Configuration { get; }

        /// <summary>
        /// Gets a Random Generator.
        /// </summary>
        /// <value>The random.</value>
        Random Random { get; }


        /// <summary>
        /// Gets or sets the default culture.
        /// <para>Default value = 'en'</para>
        /// </summary>
        /// <value>
        /// The default culture.
        /// </value>
        CultureInfo DefaultCulture { get; }



        /// <summary>
        /// Gets a value indicating whether the currrent process is interactive.
        /// <para>
        /// A Windows Service (unless it has 'interact with desktop' set to true)
        /// would return false. So would IIS. In such cases, don't show Modal dialogs
        /// ...as nobody will be able to reacto them.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is interactive; otherwise, <c>false</c>.
        /// </value>
        bool IsUserInteractive { get; }





        
        /// <summary>
        /// Gets the name of the current user's domain (eg: JQDEV).
        /// </summary>
        /// <value>The name of the domain.</value>
        string DomainName { get; }


        /// <summary>
        /// Gets the name of the machine.
        /// </summary>
        /// <value>The name of the machine.</value>
        string MachineName { get; }

        #region Retired
        /// <summary>
        ///   Gets the name of the application.
        /// <para>
        ///   Gets or Sets the name of the application 
        ///   using the custom membership provider.
        ///   Default value is '/'
        /// </para>
        /// </summary>
        /// <value>The name of the application.</value>
        string ApplicationName { get; }


        ///// <summary>
        ///// Gets the hash of the application name.
        ///// </summary>
        string ApplicationNameHash { get; }
        #endregion

        /// <summary>
        /// The deployment environment of the app
        /// (DEV, ST, SIT, UAT, PROD, etc.)
        /// <para>
        /// The name is used to retrieve settings
        /// from <c>IApplicationSettingsService</c>
        /// depending on the value in the 
        /// <see cref="IHostSettingsService"/>
        /// </para>
        /// </summary>
        string EnvironmentName { get; }


        /// <summary>
        ///   Gets the application's base path.
        /// </summary>
        /// <value>The application's base path.</value>
        //TRUE...but looking for WARNING: Unable to extract metadata from....error in nuget packagin:[Obsolete("Not Portable",false)]
        string ApplicationBasePath { get; }


        /// <summary>
        /// The HttpContext. 
        /// <para>
        /// Will be null in a non-IIS based environment (ie desktop)
        /// </para>
        /// <para>
        /// Note that it is returned as an Object (and not HttpContext)
        /// to remain compilable in Windows Client applications when
        /// System.Web is not available.
        /// </para>
        /// <para>
        /// Note that internally, Reflection is being used.
        /// </para>
        /// </summary>
        object HttpContext { get; }


        /// <summary>
        /// Returns a context relative new line 
        /// ("<br/>" when <see cref="HttpContext"/> is not null,
        /// <see cref="System.Environment.NewLine"/> when not.
        /// </summary>
        string NewLine { get; }

        /// <summary>
        /// Maps the VirtualPath ('~/SubDir/SomeFile.htm') 
        /// to a Dir relative to <see cref="ApplicationBasePath"/> 
        /// eg: ('c:\somedir\SubDir\SomeFile.htm');
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        string MapPath(string virtualPath);


        ///// <summary>
        /////   Gets the name of the company registered on the given assembly.
        ///// </summary>
        ///// <param name = "assembly">The assembly.</param>
        ///// <returns>The company name.</returns>
        //string GetCompanyName(Assembly assembly);




        /// <summary>
        ///   Gets the total amount of memory *thought* to be allocated.
        /// <para>
        /// With Garbage Collection and other mitigating factors, it's
        /// not exactly a hard fact at any one time.
        /// </para>
        /// </summary>
        /// <param name = "waitForGarbageCollectionBeforeReturning">Wait for garbage collection before returning, or return immediately.</param>
        /// <returns>The amount of memory allocated</returns>
        long GetTotalMemoryAllocated(bool waitForGarbageCollectionBeforeReturning=false);



    }
}