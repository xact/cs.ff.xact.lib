﻿namespace XAct.Environment
{
    using System;

    /// <summary>
    /// Interface for clientside persistence.
    /// <para>
    /// Often just implemented as an Http cookie persistence mechanism
    /// (see XAct.UI.Web).
    /// </para>
    /// </summary>
    public interface IClientPersistenceService : IHasXActLibService
    {
        /// <summary>
        /// Gets the value from the specified clientside persistence Container.
        /// </summary>
        /// <param name="containerName">Required name of the container.</param>
        /// <param name="key">The (optional) key. If none provided, uses technology specific strategy for default key.</param>
        /// <returns></returns>
        string GetValue(string containerName, string key=null);

        /// <summary>
        /// Sets the persisted value in the specified client side persistence container.
        /// </summary>
        /// <param name="containerName">Required name of the container.</param>
        /// <param name="key">The optional key. If none provided, uses technology specific strategy for default key.</param>
        /// <param name="value">The value.</param>
        /// <param name="duration">The duration (if null, the cookie is session-based).</param>
        void SetValue(string containerName, string value, TimeSpan duration=default(TimeSpan), string key = null);

        /// <summary>
        /// Clears the Clientside persistence Container,
        /// and all its contained variables.
        /// </summary>
        /// <param name="containerName">Name of the container.</param>
        /// <returns></returns>
        void ClearContainer(string containerName);
    }
}