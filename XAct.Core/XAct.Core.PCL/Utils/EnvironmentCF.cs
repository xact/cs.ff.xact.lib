﻿
#if  ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
using System;
using System.Runtime.InteropServices;

namespace XAct.AppEnvironment
{

    public pariclass ApplicationEnvironmentCF
    {

    static public string AppDir {
        get {
            if (string.IsNullOrEmpty(_AppDir)){
                //More complicated, with API rights required:
                _AppDir = Path.GetDirectoryName(GetEntryAssembly().Location);
            }
            return _AppDir;
        }
    }
    private static string _AppDir;


        /// <summary>
        ///   Gets the unique device ID.
        ///   <para>
        ///     This is a Unique ID of the phone, 
        ///     hashed with the supplied key.
        ///   </para>
        /// </summary>
        /// <param name = "hashKey">The hash key.</param>
        /// <remarks>
        ///   <para>
        ///   </para>
        /// </remarks>
        /// <internal>
        ///   See: http://www.eggheadcafe.com/forumarchives/
        ///   pocketpcdeveloper/jan2006/post25956729.asp
        /// </internal>
        /// <returns>A 40 char (20bytes x 2chars) string.</returns>
        public static string GetUniqueDeviceId(string hashKey)
        {
            //CE:    
            if (!hashKey.IsNotNullOrEmpty()){throw new ArgumentNullException("hashKey");}

// ReSharper disable ConvertIfStatementToReturnStatement
            if (!string.IsNullOrEmpty(_cachedDeviceUniqueID))
// ReSharper restore ConvertIfStatementToReturnStatement
            {
                //Save the little CE's resources...
                return _cachedDeviceUniqueID;
            }

    //Start with any ol' string:

    int size = 20;
    // get the byte array for the string
    byte[] buffer = Encoding.ASCII.GetBytes(hashKey);

    // some parameters for the native call

    // our out parameter

    // Call the native GetDeviceUniqueID method from coredll.dll
    uint returnedSize = size;
    byte[] outputBuffer = new byte[size];
    //This function returns an application-specific hash of the device identifier.
    GetDeviceUniqueID(buffer, buffer.Length, 1, outputBuffer, ref returnedSize);

    // check that we got something
    if (returnedSize == 0){
        return string.Empty;
    }

    //Convert byte array to string:
    //Manually:
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < DeviceOutput.Length; i++){
        sb.Append("{0:x2}".FormatStrignInvariant(outputBuffer[i]));
    }
    _CachedDeviceUniqueID = sb.ToString();

    //Or via framework:
    _CachedDeviceUniqueID = Convert.ToString(outputBuffer);

            return _cachedDeviceUniqueID;
        }
        private static string _cachedDeviceUniqueID;
    }








        public static Assembly GetEntryAssembly() {
        //CE:
      System.Text.StringBuilder sb = 
      new System.Text.StringBuilder(
         0x100 * System.Runtime.InteropServices.Marshal.SystemDefaultCharSize);
      int returnedSize = API.GetModuleFileName(IntPtr.Zero, sb, 0xff);
      if (returnedSize <= 0) {
        return null;
      }
      if (returnedSize > 0xff) {
        throw new System.Exception(
          "Returned Assembly name longer than MAX_PATH chars.");
      }
      return System.Reflection.Assembly.LoadFrom(sb.ToString());
    }


            public static string GetEntryAssemblyName(){
      StringBuilder sb = null;
      IntPtr hModule = GetModuleHandle(IntPtr.Zero);
      if (IntPtr.Zero != hModule) {
        sb = new StringBuilder(255);
        if (0 == API.GetModuleFileName(hModule, sb, sb.Capacity)) {
          sb = null;
        }
      }
      string result = sb.ToString();
    return result;
    }


#region Nested API Class
        public static class API
    {
    [DllImport("coredll.dll")] 
    private static extern int GetDeviceUniqueID(
      [In, Out] byte[] appdata, 
      int cbApplictionData, 
      int dwDeviceIDVersion, 
      [In, Out] byte[] deviceIDOuput, 
      ref uint pcbDeviceIDOutput); 

    [
      System.Runtime.InteropServices.DllImport(
      "CoreDll.dll", 
      SetLastError = true)
      ]
    private static extern IntPtr GetModuleHandle(IntPtr ModuleName);

    [System.Runtime.InteropServices.DllImport(
      "coredll.dll", 
      SetLastError = true)
      ]
    private static extern int GetModuleFileName(
      IntPtr hModule, 
      System.Text.StringBuilder lpFilename, 
      int nSize);

    }


#endregion




}
#endif