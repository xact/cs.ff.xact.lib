﻿
namespace XAct.Utils
{
    /// <summary>
    /// Helper methods for working with Uri strings,
    /// that can't be done as extension methods.
    /// </summary>
    public static class Uri
    {

        /// <summary>
        /// Equivalent of Path.Combine, but for forward facing slashes.
        /// </summary>
        /// <param name="uri1">The uri1.</param>
        /// <param name="uri2">The uri2.</param>
        /// <returns></returns>
        public static string Combine(string uri1, string uri2)
        {
            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return string.Format("{0}/{1}", uri1, uri2);
        } 

    }
}
