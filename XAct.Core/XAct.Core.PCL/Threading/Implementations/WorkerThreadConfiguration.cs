﻿namespace XAct.Threading
{
    using System;

    /// <summary>
    /// Configration package for <see cref="WorkerThread{TItem}"/> 
    /// Constructor.
    /// <para>
    /// <![CDATA[
    /// MessageProcessingThreadConfiguraion : WorkerTHreadConfiguration<Message> {
    /// 
    ///   MessageProcessingThreadConfiguraion(string tag, Message message):
    ///     this(tag, message, (m)=>{m.Processed=true;return true;}){}
    /// }
    /// 
    /// var threadConfiguraion = new MessageProcessingThreadConfiguraion("op123",message);
    /// 
    /// var workerThreadPool = new workerThreadPool(_tracingService, threadConfiguration);
    /// 
    /// workerThreadPool.Start();
    /// 
    /// 
    /// ]]>
    /// </para>
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class WorkerThreadConfiguration<TItem>: IHasTagReadOnly, IHasItemReadOnly<TItem>
    {
        /// <summary>
        /// Gets or sets the tag.
        /// <para>
        /// A friendly Tag to help trace which object was completed (some payloads may not have 
        ///    a unique enough identity to determine which element was processed.
        /// </para>
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public string Tag { get; private set; }

        /// <summary>
        /// Gets or sets the item to be processed.
        /// </summary>
        /// <value>
        /// The item.
        /// </value>
        public TItem Item { get; private set; }
        
        /// <summary>
        /// Gets or sets the processing function.
        /// </summary>
        /// <value>
        /// The processing function.
        /// </value>
        public Func<TItem, bool> ProcessingFunction { get; private set; }


        /// <summary>
        /// The recorded result of <see cref="WorkerThread{TItem}"/>  having invoked 
        /// <see name="ProcessingFunction"/>
        /// </summary>
        public WorkerThreadProcessingSummary<TItem> Results { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkerThreadConfiguration&lt;TItem&gt;"/> class.
        /// </summary>
        /// <param name="tag">The optional tag used to help track which item was just processed.</param>
        /// <param name="item">The item to be processed.</param>
        /// <param name="processingFunction">The anonymous function used to process <paramref name="item"/>.</param>
        public WorkerThreadConfiguration(TItem item, Func<TItem,bool> processingFunction, string tag=null)
        {
            item.ValidateIsNotDefault("item");
            item.ValidateIsNotDefault("processingFunction");

            ProcessingFunction = processingFunction;
            
            Item = item;
            Tag = tag;
            
            Results = new WorkerThreadProcessingSummary<TItem>();
        }


    }
}