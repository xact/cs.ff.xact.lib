﻿namespace XAct.Threading
{
    using System;
    using System.Threading;
    using XAct.Diagnostics;
    using XAct.Events;

    /// <summary>
    /// A Worker thread to Process a specific argument package
    /// <para>
    /// Lifecycle is 
    /// Start, Processing, [CancelledCancelled], [ProcessingError], Processed
    /// </para>
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class WorkerThread<TItem>
    {
        /// <summary>
        /// Occurs when <see cref="Start"/> is invoked, before <see cref="ProcessingCancelled"/>.
        /// </summary>
        public event EventHandler<GenericCancelEventArgs<WorkerThreadConfiguration<TItem>>> Processing;

        /// <summary>
        /// Occurs when Event was cancelled before it started.
        /// </summary>
        public event EventHandler<GenericEventArgs<WorkerThreadConfiguration<TItem>>> ProcessingCancelled;

        /// <summary>
        /// Occurs when an Error occurred durening Processing.
        /// </summary>
        public event EventHandler<GenericExceptionEventArgs<WorkerThreadConfiguration<TItem>>> ProcessingError;

        /// <summary>
        /// Occurs when Processing completed successfully.
        /// </summary>
        public event EventHandler<GenericEventArgs<WorkerThreadConfiguration<TItem>>> ProcessingComplete;


        private readonly ITracingService _tracingService;
        //This class's State -- it contains 
        //* a friendly Tag to help trace which object was completed (some payloads may not have 
        //    a unique enough identity to determine which element was processed.
        //* the object to be processed, 
        //* the delegate to the method that will process it
        private WorkerThreadConfiguration<TItem> _workerThreadConfiguration;


        private DateTime _processStartDateTime;

        /// <summary>
        /// Gets the <see cref="WorkerThread{TItem}"/> configuration.
        /// From this, one can get back to the Item, and the Processor used.
        /// </summary>
        public WorkerThreadConfiguration<TItem> WorkerThreadConfiguration { get { return _workerThreadConfiguration; } }



        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="WorkerThread&lt;TItem&gt;"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public WorkerThread(ITracingService tracingService)
        {
            tracingService.ValidateIsNotDefault("tracingService");

            _tracingService = tracingService;

        }



        /// <summary>
        /// Enqueues the request in the ThreadPool,
        /// giving it the payload to process -- which will contain
        /// * the item to process
        /// * an anonymous function delegate to process it
        /// </summary>
        /// <param name="workerThreadConfiguration">The worker thread configuration.</param>
        public void Start(WorkerThreadConfiguration<TItem> workerThreadConfiguration)
        {
            workerThreadConfiguration.ValidateIsNotDefault("workerThreadConfiguration");

            _workerThreadConfiguration = workerThreadConfiguration;

            //We have the option of passing around State, but
            //there really isn't much point as it's already within 
            //scope of the DoWork Method
            //when it gets triggered.
            ThreadPool.QueueUserWorkItem(this.Process, _workerThreadConfiguration);
             
            _tracingService.Trace(TraceLevel.Verbose,"WorkerThread.DoWork() [Started]"
                .FormatStringCurrentCulture(
                    WorkerThreadConfiguration.Tag, //1
                    WorkerThreadConfiguration.Item //2
                    ));
        }







        /// <summary>
        /// Invoked by the <see cref="ThreadPool"/>
        /// when it is ready to do the work
        /// </summary>
        /// <param name="stateInformation">The state information.</param>
        private void Process(object stateInformation)
        {

            //Ignoring the state information, as we already have it typed:
            //TItem _item = (TItem) stateInformation;

            _workerThreadConfiguration.Results.Started = _processStartDateTime = DateTime.Now;

            try
            {

                //Raise OnProcessing event before doing any work to give a last chance
                //to cancel:
                GenericCancelEventArgs<WorkerThreadConfiguration<TItem>> eventArgs
                    = new GenericCancelEventArgs<WorkerThreadConfiguration<TItem>>(WorkerThreadConfiguration);

                this.OnProcessing(eventArgs);

                if (eventArgs.Cancel)
                {
                    this.OnProcessingCancelled(new GenericEventArgs<WorkerThreadConfiguration<TItem>>(WorkerThreadConfiguration));
                    return;
                }

                
                //Wasn't cancelled, so invoke the delegate in the 
                //configuration package, passing it the Item to be processed.
                //The delegate returns a bool indicating success (beyond just raising an exception):
                _workerThreadConfiguration.Results.ProcessSucessful =
                    WorkerThreadConfiguration
                        .ProcessingFunction
                        .Invoke(_workerThreadConfiguration.Item);


                //Appears delegate didn't cause an exception:
                this.OnProcessingComplete(new GenericEventArgs<WorkerThreadConfiguration<TItem>>(WorkerThreadConfiguration));


            }
            catch (Exception e)
            {

                GenericExceptionEventArgs<WorkerThreadConfiguration<TItem>> eventArgs =
                    new GenericExceptionEventArgs<WorkerThreadConfiguration<TItem>>(
                        e,
                        WorkerThreadConfiguration);

                this.OnError(eventArgs);
            } //~catch
        }










        #region Log and Raise Events [Processing, ProcessingCancelled, ProcessingError, Processed]


        private void OnProcessing(GenericCancelEventArgs<WorkerThreadConfiguration<TItem>> e)
        {
            _workerThreadConfiguration.Results.ProcessStarted = true;
            _workerThreadConfiguration.Results.Duration = DateTime.Now.Subtract(_processStartDateTime);

            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "WorkerThread.DoWork() [ProcessingStarted] [Elapsed:{0}ms] [Tag:{1}] [Item:{2}]"
                    .FormatStringCurrentCulture(
                        (int)_workerThreadConfiguration.Results.Duration.TotalMilliseconds, //0
                        WorkerThreadConfiguration.Tag, //1
                        WorkerThreadConfiguration.Item //2
                    ));


                if (this.Processing != null)
            {
                this.Processing(this, e);
            }
        }

        //Started, but Cancelled:
        private void OnProcessingCancelled(GenericEventArgs<WorkerThreadConfiguration<TItem>> e)
        {
            _workerThreadConfiguration.Results.ProcessCancelled = true;
            _workerThreadConfiguration.Results.Duration = DateTime.Now.Subtract(_processStartDateTime);

            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "WorkerThread.DoWork() [ProcessingCancelled] [Elapsed:{0}ms] [Tag:{1}] [Item:{2}]"
                    .FormatStringCurrentCulture(
                        (int)_workerThreadConfiguration.Results.Duration.TotalMilliseconds, //0
                        WorkerThreadConfiguration.Tag, //1
                        WorkerThreadConfiguration.Item //2
                    ));



            if (this.ProcessingCancelled != null)
            {
                this.ProcessingCancelled(this, e);
            }
        }

        //ProcessStarted, not cancelled, but didn't make it to ProcessComplete
        private void OnError(GenericExceptionEventArgs<WorkerThreadConfiguration<TItem>> e)
        {
            _workerThreadConfiguration.Results.Error = e.Exception;
            _workerThreadConfiguration.Results.Duration = DateTime.Now.Subtract(_processStartDateTime);

            _tracingService.TraceException(
                TraceLevel.Warning,
                e.Exception,
                "WorkerThread.DoWork() [ProcessingError....] [Elapsed:{0}ms] [Tag:{1}] [Item:{2}]"
                    .FormatStringCurrentCulture(
                        (int)_workerThreadConfiguration.Results.Duration.TotalMilliseconds, //0
                        WorkerThreadConfiguration.Tag, //1
                        WorkerThreadConfiguration.Item //2
                    ));

            if (this.ProcessingError != null)
            {
                this.ProcessingError(this, e);
            }
        }


        // Completed successfully, without error:
        private void OnProcessingComplete(GenericEventArgs<WorkerThreadConfiguration<TItem>> e)
        {
            _workerThreadConfiguration.Results.ProcessComplete = true;
            _workerThreadConfiguration.Results.Duration = DateTime.Now.Subtract(_processStartDateTime);


            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "WorkerThread.DoWork() [ProcessingComplete.] [Elapsed:{0}ms] [Tag:{1}] [Item:{2}] [Result:{3}]"
                    .FormatStringCurrentCulture(
                        (int)_workerThreadConfiguration.Results.Duration.TotalMilliseconds, //0
                        WorkerThreadConfiguration.Tag, //1
                        WorkerThreadConfiguration.Item, //2
                        WorkerThreadConfiguration.Results.ProcessSucessful //3
                    ));

            if (this.ProcessingComplete != null)
            {
                this.ProcessingComplete(this, e);
            }
        }

        #endregion

    }

}

