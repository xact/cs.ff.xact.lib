﻿namespace XAct.Threading
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class WorkerThreadProcessingSummary<TItem> : WorkerThreadProcessingSummary
    {
        /// <summary>
        /// Item processed.
        /// </summary>
        public TItem Item { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class WorkerThreadProcessingSummary{

        /// <summary>
        /// Gets or sets the time the process started.
        /// </summary>
        /// <value>
        /// The started.
        /// </value>
        public DateTime Started { get; set; }

        /// <summary>
        /// Gets or sets the duration of the process.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        public TimeSpan Duration { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the process was ever started.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [process started]; otherwise, <c>false</c>.
        /// </value>
        public bool ProcessStarted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the process was cancelled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [process cancelled]; otherwise, <c>false</c>.
        /// </value>
        public bool ProcessCancelled { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the function was invoked successfully
        /// (does not mean it returned true -- just that it didn't throw an exception)
        /// </summary>
        public bool ProcessComplete { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the function delegate used
        /// was successful.
        /// </summary>
        public bool? ProcessSucessful { get; set; }

        /// <summary>
        /// Gets or sets the Exception raised if any.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public Exception Error { get; set; }
    }
}