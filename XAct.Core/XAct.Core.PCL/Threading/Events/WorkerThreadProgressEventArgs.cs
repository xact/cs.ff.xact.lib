﻿using System;

namespace XAct.Threading
{
    public class WorkerThreadProgressChangedEventArgs<TUserState> : 
        EventArgs , IWorkerThreadProgressChangedEventArgs<TUserState>
    {
        /// <summary>
        /// Gets the Work progress, as a percentage between 0 and 100.
        /// </summary>
        /// <value>The progress percentage.</value>
        public int ProgressPercentage { get { return _progressPercentage; } }
        private readonly int _progressPercentage;


        public TUserState UserState { get { return _userState; } }
        private readonly TUserState _userState;

          
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="WorkerThreadProgressChangedEventArgs&lt;TUserState&gt;"/> class.
        /// </summary>
        /// <param name="progressPercentage">The progress percentage.</param>
        public WorkerThreadProgressChangedEventArgs(int progressPercentage)
        {
            _progressPercentage = progressPercentage;
        }
        
        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="WorkerThreadProgressChangedEventArgs&lt;TUserState&gt;"/> class.
        /// </summary>
        /// <param name="progressPercentage">The progress percentage.</param>
        /// <param name="userState">State of the user.</param>
        public WorkerThreadProgressChangedEventArgs(int progressPercentage, TUserState userState)
        {
            _progressPercentage = progressPercentage;
            _userState = userState;
        }
        #endregion
    }

}
