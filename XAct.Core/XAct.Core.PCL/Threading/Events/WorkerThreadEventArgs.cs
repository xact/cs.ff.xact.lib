﻿using System;

namespace XAct.Threading
{

    public abstract class WorkerThreadEventArgs<TRequestPayload, TRequestResponse> :
        EventArgs,
        IWorkerThreadEventArgs<TRequestPayload, TRequestResponse>
    {
        public IRequestWorkerThreadManager<TRequestPayload, TRequestResponse> RequestWorkerThreadManager
        {
            get
            {
                return _requestWorkerThreadManager;
            }
        }
        private readonly IRequestWorkerThreadManager<TRequestPayload, TRequestResponse> _requestWorkerThreadManager;



        #region Constructors
        public RequestWorkerThreadEventArgs(IRequestWorkerThreadManager<TRequestPayload, TRequestResponse> requestWorkerThreadManager)
        {
            //ArgumentValidation.IsInitialized(message, "message");
            _requestWorkerThreadManager = requestWorkerThreadManager;
        }
        #endregion
    }
}
