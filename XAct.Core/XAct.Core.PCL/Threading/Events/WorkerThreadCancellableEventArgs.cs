﻿using System.ComponentModel;

namespace XAct.Threading
{

    public class WorkerThreadCancellableEventArgs<TRequestPayload, TRequestResponse> : 
        WorkerThreadEventArgs<>
        IWorkerThreadCancellableEventArgs,
{
    // Properties


        #region Constructors
        public WorkerThreadCancellableEventArgs(IRequestEventArgs<TRequestPayload, TRequestResponse> requestWorkerThreadManager)
        {
            //ArgumentValidation.IsInitialized(message, "message");
            _RequestWorkerThreadManager = requestWorkerThreadManager;
        }

        public WorkerThreadCancellableEventArgs(TMessage message, bool cancelled)
        : base(cancelled)
    {
        //ArgumentValidation.IsInitialized(message, "message");
        _message = message;
    }
        #endregion

}
}
