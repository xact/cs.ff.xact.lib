﻿using System;

namespace XAct.Threading
{


    public class WorkerThreadExceptionEventArgs<TRequestPayload, TRequestResponse> :  
        RequestWorkerThreadEventArgs<TRequestPayload, TRequestResponse>,
        IWorkerThreadExceptionEventArgs,
        IWorkerThreadStatusEventArgs
    {

        #region Implementation of IRequestWorkerThreadStatusEventArgs
        public Exception Exception
        {
            get
            {
                return _exception;
            }
        }
        private readonly Exception _exception;
        #endregion 

        #region Implementation of Status
        public bool Success
        {
            get
            {
                return _status;
            }
        }
        private readonly bool _status;
        #endregion

        // Methods
        public WorkerThreadExceptionEventArgs(RequestWorkerThreadManager<TRequestPayload, TRequestResponse> requestWorkerManager, Exception exception)
            : base(requestWorkerManager)
        {
            //ArgumentValidation.IsInitialized(exception, "exception");
            _exception = exception;
            _status = false;
        }

     
    }

}
