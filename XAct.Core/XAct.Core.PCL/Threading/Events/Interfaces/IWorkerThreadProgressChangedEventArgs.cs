﻿
namespace XAct.Threading
{
    interface IWorkerThreadProgressChangedEventArgs<TUserState>
    {
        /// <summary>
        /// Gets the Work progress, as a percentage between 0 and 100.
        /// </summary>
        /// <value>The progress percentage.</value>
        int ProgressPercentage { get; }

        TUserState UserState { get; }
    }
}
