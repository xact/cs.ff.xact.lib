﻿namespace XAct.Threading
{
    /// <summary>
    /// Interface for events raised to query whether 
    /// operations should be cancelled.
    /// </summary>
    public interface IWorkerThreadCancellableEventArgs
    {
        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// <see cref="IWorkerThreadCancellableEventArgs"/> 
        /// should be cancelled.
        /// </summary>
        /// <value><c>true</c> if cancel; otherwise, <c>false</c>.</value>
        bool Cancel { get; set; }
    }
}