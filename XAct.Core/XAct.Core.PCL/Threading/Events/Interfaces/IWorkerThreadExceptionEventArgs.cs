﻿
using System;

namespace XAct.Threading
{
    public interface IWorkerThreadExceptionEventArgs
    {
        /// <summary>
        /// The Exception that caused this Event to be raised.
        /// </summary>
        /// <value>The exception.</value>
        Exception Exception { get; }
    }
}
