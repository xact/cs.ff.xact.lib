﻿namespace XAct.Threading
{
    public interface IWorkerThreadEventArgs<TRequestPayload, TRequestResponse>
    {
        /// <summary>
        /// Gets the <see cref="IRequestWorkerThreaderManager{}"/>.
        /// </summary>
        /// <value>The request worker thread manager.</value>
        IRequestWorkerThreadManager<TRequestPayload, TRequestResponse> RequestWorkerThreadManager { get; }
    }
}