﻿namespace XAct.Threading
{

    public class WorkerThreadStatusEventArgs<TRequestPayload, TRequestResponse> :  
        RequestWorkerThreadEventArgs<TRequestPayload, TRequestResponse>,
        IWorkerThreadStatusEventArgs
    {
        #region Implementation of IMessageStatusEventArgs
        public bool Success
        {
            get
            {
                return _success;
            }
        }
        private readonly bool _success;
        #endregion



        #region Constructors
        public RequestStatusEventArgs(RequestWorkerThreadManager<TRequestPayload, TRequestResponse> requestWorkerThreadManager, bool successFlag)
            : base(requestWorkerThreadManager)
        {
            //ArgumentValidation.IsInitialized(message, "messsage");
            //ArgumentValidation.IsInitialized(deliveryServiceProtocolName, "deliveryServiceProtocolName");

            _success = successFlag;
        }
        #endregion

    }

}
