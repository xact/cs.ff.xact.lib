﻿namespace XAct.Threading
{
    /// <summary>
    /// Contract for a service to unspool and process 
    /// prioritized items.
    /// </summary>
    public interface IPrioritizedSpoolProcessor : ISpoolProcessor
    {

    }

    public interface ISpoolProcessor : IHasStartStop
    {
      
    }


}