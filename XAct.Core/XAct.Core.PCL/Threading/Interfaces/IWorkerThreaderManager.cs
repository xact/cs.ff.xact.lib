﻿using System;

namespace XAct.Threading.Interfaces
{
    public interface IWorkerThreaderManager<TRequestPayload, TRequestResponse>
    {
        #region Events Raised
        event EventHandler<CancellableRequestEventArgs<IRequestWorkerThreadManager<TRequestResponse>> WorkStarted;
        event EventHandler<RequestExceptionEventArgs<IRequestWorkerThreadManager<TRequestResponse>> MessageSendingError;
        event EventHandler<RequestStatusEventArgs<IRequestWorkerThreadManager<TRequestResponse>> WorkComplete;
        event EventHandler<IRequestWorkerThreadManager<TRequestResponse>> WorkComplete;
        #endregion

        /*
    // Events
    [SRDescription("BackgroundWorker_DoWork"), SRCategory("PropertyCategoryAsynchronous")]
    public event DoWorkEventHandler DoWork;
    [SRDescription("BackgroundWorker_ProgressChanged"), SRCategory("PropertyCategoryAsynchronous")]
    public event ProgressChangedEventHandler ProgressChanged;
    [SRCategory("PropertyCategoryAsynchronous"), SRDescription("BackgroundWorker_RunWorkerCompleted")]
    public event RunWorkerCompletedEventHandler RunWorkerCompleted;
    */

        #region Properties

        bool IsBusy { get; }
        bool CancellationPending { get; }
        bool WorkerReportsProgress { get; }
        bool WorkerSupportCancellation { get; }
        #endregion

        void Start();

        void Interupt();


        public void RunWorkerAsync();
        public void RunWorkerAsync(object argument);
        public void ReportProgress(int percentProgress);
        public void ReportProgress(int percentProgress, object userState);

        public void CancelAsync();

        protected virtual void OnDoWork(DoWorkEventArgs e);
        protected virtual void OnProgressChanged(ProgressChangedEventArgs e);
        protected virtual void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e);

    }
}
