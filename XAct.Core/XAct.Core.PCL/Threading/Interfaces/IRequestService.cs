﻿namespace XAct.Threading
{
    public interface IRequestService
    {
        bool PerformSyncRequest();
    }
}