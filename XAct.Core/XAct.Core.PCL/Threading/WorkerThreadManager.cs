﻿using System;
using System.Threading;
using XAct.Diagnostics;

namespace XAct.Threading
{


    public class RequestWorkerThreadManager<TRequestResponse>: IRequestWorkerThreadManager<TRequestResponse>
    {

        #region Events Raised
        public event EventHandler<WorkerThreadCancellableEventArgs<TRequestPayload,TRequestResponse> MessageSending;
        public event EventHandler<RequestExceptionEventArgs<IRequestWorkerThreadManager<TRequestResponse>> MessageSendingError;
        public event EventHandler<RequestStatusEventArgs<IRequestWorkerThreadManager<TRequestResponse>> MessageSent;
        public event EventHandler<IRequestWorkerThreadManager<TRequestResponse>> WorkComplete;
        #endregion

        #region Properties
        public ILoggingService LoggingService
        {
            get
            {
                return _loggingService;
            }
            
        }
        private readonly ILoggingService _loggingService;


        #endregion


#region Constructors
        public RequestWorkerThreadManager(string id, ILoggingService loggingService, Func<TRequestResponse> serviceRequestFunction)
        {
            //ArgumentValidation.IsInitialized(deliveryService, "deliveryService");
            //ArgumentValidation.IsInitialized(message, "message");
            _loggingService = loggingService;
            _message = message;
        }
#endregion


#region Public Methods


        public void Start()
        {
            object state = null;
            
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.DoWork), state);

            //Debugging.WriteLine("[Id:{0}] Worker Thread Started.", new object[] { this.Message.Id });
        }




        private void DoWork(object stateInformation)
        {
            Exception exception;

            try
            {
                DateTime now = DateTime.Now;

                _loggingService.Log("RequestWorkerThreadManager.DoWork() [Begin...] (Id:'{0}', StartTime:'{1}')", this.Message.ToString(), now, DateTime.Now.ToString("HH:mm:ss"));
                
                CancellableRequestEventArgs<TMessage> e = new CancellableRequestEventArgs<TMessage>(this.Message);

                this.OnSendingMessage(e);
                bool successFlag = false;

                if (e.Cancel)
                {
                    //Debugging.WriteLine("[Id:{0}] Message WorkerThread.DoWork() [Cancelled] Cancelled by an event handler to OnSendingMessage()", new object[] { this.Message.Id });
                }
                else
                {
                    try
                    {
                        successFlag = _requestService.PerformSyncRequest(_message);
                    }
                    catch (Exception exception1)
                    {
                        exception = exception1;
                        //Tracing.WriteLineIf(TraceLevel.Error, exception, "[Id:{1}] DeliveryService '{2}' caused an exception." + Environment.NewLine + "The error message was: '{0}'.", new object[] { exception.Message, this.Message.Id, _requestService.Name });
                        RequestExceptionEventArgs<TMessage> args2 = new RequestExceptionEventArgs<TMessage>(_message, _requestService.Protocol, exception);
                        this.OnMessageSendingError(args2);
                    }
                    //Debugging.WriteLine("[Id:{0}] Message WorkerThread.DoWork() [Complete] (Success='{1}', EndTime='{2}', Elapsed='{3}ms')", new object[] { this.Message.Id, successFlag, DateTime.Now.ToString("HH:mm:ss"), (int)DateTime.Now.Subtract(now).TotalMilliseconds });
                }

                this.OnSentMessage(new RequestStatusEventArgs<TMessage>(this.Message, _requestService.Protocol, successFlag));
                
                this.OnWorkComplete(EventArgs.Empty);
            }
            catch (Exception exception2)
            {
                exception = exception2;
                //Tracing.WriteLineIf(TraceLevel.Error, exception, "[Id:{1}] DeliveryService '{2}' caused an exception." + Environment.NewLine + "The error message was: '{0}'.", new object[] { exception.Message, this.Message.Id, _requestService.Name });
            }
        }

        private void InvokeFunction(Func<TRequestResult> functionToInvoke)
        {
            functionToInvoke.Invoke()        }


        public void Interupt()
        {
        }



        #region Raise Events
        private void OnSendingMessage(CancellableRequestEventArgs<TMessage> e)
        {
            if (this.MessageSending != null)
            {
                this.MessageSending(this, e);
            }
        }

        private void OnSentMessage(RequestStatusEventArgs<TMessage> e)
        {
            if (this.MessageSent != null)
            {
                this.MessageSent(this, e);
            }
        }

        private void OnMessageSendingError(RequestExceptionEventArgs<TMessage> e)
        {
            if (this.MessageSendingError != null)
            {
                this.MessageSendingError(this, e);
            }
        }


        private void OnWorkComplete(EventArgs e)
        {
            if (this.WorkComplete != null)
            {
                this.WorkComplete(this, e);
            }
        }
        #endregion
    }
}

