﻿using System;
using System.ComponentModel;
using System.Threading;

namespace XAct.Threading
{

    public class BackgroundWorker
    {

        // Nested Types
        private delegate void WorkerThreadStartDelegate(object argument);

        // Fields
        private AsyncOperation _asyncOperation;
        private bool _isRunning;
        private readonly SendOrPostCallback _operationCompleted;
        private readonly SendOrPostCallback _progressReporter;
        private readonly WorkerThreadStartDelegate _threadStart;


        #region Events
        public event DoWorkEventHandler DoWork;
        public event ProgressChangedEventHandler ProgressChanged;
        public event RunWorkerCompletedEventHandler RunWorkerCompleted;
        #endregion



        // Properties
        /// <summary>
        /// Gets a value indicating whether 
        /// <see cref="CancelAsync"/>
        /// has been invoked.
        /// </summary>
        /// <value><c>true</c> if the operation has been cancelled; otherwise, <c>false</c>.</value>
        public bool CancellationPending
        {
            get
            {
                return _cancellationPending;
            }
        }
        private bool _cancellationPending;

        /// <summary>
        /// Gets a value indicating whether this worker has been started.
        /// </summary>
        /// <value><c>true</c> if this instance is busy; otherwise, <c>false</c>.</value>
        public bool IsBusy
        {
            get
            {
                return _isRunning;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this worker reports progress.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if worker reports progress; otherwise, <c>false</c>.
        /// </value>
        public bool WorkerReportsProgress { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this worker 
        /// supports cancellation
        /// via calls to <see cref="CancelAsync"/>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [worker supports cancellation]; otherwise, <c>false</c>.
        /// </value>
        public bool WorkerSupportsCancellation { get; set; }

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="BackgroundWorker"/> class.
        /// </summary>
        public BackgroundWorker()
        {
            _threadStart = new WorkerThreadStartDelegate(this.WorkerThreadStart);
            _operationCompleted = new SendOrPostCallback(this.AsyncOperationCompleted);
            _progressReporter = new SendOrPostCallback(this.ProgressReporter);
        }
        #endregion



        /// <summary>
        /// Begins the asynchronous method invocation.
        /// </summary>
        public void RunWorkerAsync()
        {
            this.RunWorkerAsync(null);
        }

        /// <summary>
        /// Begins the asynchronous method invocation, with the given argument payload.
        /// </summary>
        /// <param name="argument">The argument.</param>
        public void RunWorkerAsync(object argument)
        {
            if (_isRunning)
            {
                throw new InvalidOperationException("Worker already been started");
            }

            _isRunning = true;
            _cancellationPending = false;

            _asyncOperation = AsyncOperationManager.CreateOperation(null);

            _threadStart.BeginInvoke(argument, null, null);
        }



        /// <summary>
        /// Reports progress to any registered listeners.
        /// </summary>
        /// <param name="percentProgress">The percent progress.</param>
        public void ReportProgress(int percentProgress)
        {
            this.ReportProgress(percentProgress, null);
        }

        /// <summary>
        /// Reports progress to any registered listeners.
        /// </summary>
        /// <param name="percentProgress">The percent progress.</param>
        /// <param name="userState">State of the user.</param>
        public void ReportProgress(int percentProgress, object userState)
        {
            if (!this.WorkerReportsProgress)
            {
                throw new InvalidOperationException("ThreadPoolWorker.WorkerReportsProgress == false");
            }

            ProgressChangedEventArgs arg = new ProgressChangedEventArgs(percentProgress, userState);

            if (_asyncOperation != null)
            {
                _asyncOperation.Post(_progressReporter, arg);
            }
            else
            {
                _progressReporter(arg);
            }
        }


        public void CancelAsync()
        {
            if (!this.WorkerSupportsCancellation)
            {
                throw new InvalidOperationException("ThreadPoolWorker.WorkerSupportsCancellation == false");
            }
            _cancellationPending = true;
        }





        #region Private Methods
        private void WorkerThreadStart(object argument)
        {
            object result = null;
            Exception error = null;
            bool cancelled = false;
            try
            {
                DoWorkEventArgs e = new DoWorkEventArgs(argument);

                //Raise event that will be handled to do the actual work
                this.OnDoWork(e);

                if (e.Cancel)
                {
                    cancelled = true;
                }
                else
                {
                    result = e.Result;
                }
            }
            catch (Exception exception2)
            {
                error = exception2;
            }

            RunWorkerCompletedEventArgs arg = new RunWorkerCompletedEventArgs(result, error, cancelled);

            _asyncOperation.PostOperationCompleted(_operationCompleted, arg);
        }

        private void AsyncOperationCompleted(object arg)
        {
            _isRunning = false;
            _cancellationPending = false;
            this.OnRunWorkerCompleted((RunWorkerCompletedEventArgs)arg);
        }

        private void ProgressReporter(object arg)
        {
            this.OnProgressChanged((ProgressChangedEventArgs)arg);
        }


        #endregion

        #region Raise Events
        /// <summary>
        /// Raises the <see cref="E:DoWork"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        protected virtual void OnDoWork(DoWorkEventArgs e)
        {
            if (DoWork == null)
            {
                return;
            }
            DoWork(this, e);
        }

        /// <summary>
        /// Raises the <see cref="E:ProgressChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnProgressChanged(ProgressChangedEventArgs e)
        {
            if (ProgressChanged == null)
            {
                return;
            }
            ProgressChanged(this, e);
        }

        /// <summary>
        /// Raises the <see cref="E:RunWorkerCompleted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e)
        {
            if (RunWorkerCompleted == null)
            {
                return;
            }
            RunWorkerCompleted(this, e);
        }
        #endregion

    }
}