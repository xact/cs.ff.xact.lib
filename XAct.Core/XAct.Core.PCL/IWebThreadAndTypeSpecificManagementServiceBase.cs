
namespace XAct
{
    /// <summary>
    /// Contract for an abstract ManagementServices to return a current context per thread,
    /// that is cloned from a default base one. Useful for having base settings, 
    /// but allowing customization as needed for a request.
    /// <para>
    /// Advantages over using a thread specific ServiceLocator factorisation is that
    /// the current thread's implementation can be cloned from a base default one.
    /// </para>
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public interface IWebThreadAndTypeSpecificManagementServiceBase<TContext> 
        //where TContext : ICloneable
    {


        /// <summary>
        /// Creates a new instance of the context.
        /// </summary>
        /// <returns></returns>
        TContext Create<TSpecializedContext>() where TSpecializedContext : TContext;

        /// <summary>
        /// Pushes the specified context on to the Context stack.
        /// </summary>
        /// <param name="context">The context.</param>
        void Push<TSpecializedContext>(TSpecializedContext context) where TSpecializedContext : TContext;

        /// <summary>
        /// Pops the specified context off the Context stack.
        /// </summary>
        TContext Pop<TSpecializedContext>() where TSpecializedContext : TContext;


        /// <summary>
        /// Counts the number of items pushed onto the internal stack.
        /// </summary>
        /// <returns></returns>
        int Count<TSpecializedContext>() where TSpecializedContext : TContext;

    }
}