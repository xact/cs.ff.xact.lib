﻿using System;


namespace XAct.Reflection
{

    public class NodeTypeWrapper
    {
        /// <summary>
        /// The Type being investigated
        /// </summary>
        public Type Type { get; set; }
        //Whether to display this Type.
        public bool Display { get; set; }

        public NodeTypeWrapper(Type type)
        {
            Display = true;
            Type = type;
        }

        public override string ToString()
        {
            return "[{0}] {1}".FormatStringInvariantCulture((Display ? "*" : "-"), Type.Name.ToString());
        }
    }

}
