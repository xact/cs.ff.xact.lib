﻿//namespace XAct
//{
//    using System;

//    /// <summary>
//    /// The contract for an Entity that uses 
//    /// <c>XAct.Resources.IResourceService, XAct.Resources</c>
//    /// to render messages.
//    /// <para>
//    /// An example usage would be:
//    /// <code>
//    /// <![CDATA[
//    /// public class IExampleType : IHasId<Id>, IHasName, IHasResourceFilterAndKey {
//    /// ...
//    /// }
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// <para>
//    /// See also: <see cref="IHasTextAndTitleResourceFilterAndKeyReadOnly"/>
//    /// as well as <see cref="IHasUseResourceFilterAndKeyReadOnly"/>
//    /// </para>
//    /// </summary>
//    [Obsolete]
//    public interface IHasResourceFilterAndKeyReadOnly: IHasResourceFilter /*IHasResourceKeyReadOnly*/
//    {
//    }
//}