﻿namespace XAct
{
    using System;
    
    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// See <see cref="IHasUserIdentifier"/>
    /// </internal>
    public interface IHasApplicationTennant: IHasDistributedGuidIdAndTimestamp, IHasNullableStartDateTimeEndDateTimeUtc, IHasNameAndDescription
    {

        //Going to also need:


        /// <summary>
        /// Identifier as it is registered in an SSO, that keeps track of Attributes for the origanisation.
        /// </summary>
        string SSOIdentifier { get; set; }

    }
}
