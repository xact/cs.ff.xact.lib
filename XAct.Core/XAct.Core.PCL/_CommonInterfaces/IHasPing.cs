﻿
namespace XAct.Services.Comm.ServiceModel
{
    /// <summary>
    /// Contract for Services (generally WCF)
    /// that are pingable, to see if they are up and responding.
    /// </summary>
    public interface IHasPing
    {
        /// <summary>
        /// Pings this instance.
        /// <para>
        /// By Convention, returns DateTime.UtcNow.ToString();
        /// </para>
        /// </summary>
        /// <returns></returns>
        string Ping();
    }
}
