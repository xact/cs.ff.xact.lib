﻿namespace XAct
{
    public interface IHasSuccessReadOnly
    {
        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// oepration completed successfully.
        /// </summary>
        bool Success { get; }
        
    }
}