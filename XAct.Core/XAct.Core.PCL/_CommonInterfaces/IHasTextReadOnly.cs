﻿namespace XAct
{
    public interface IHasTextReadOnly
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        string Text { get; }
        
    }
}