namespace XAct
{
    using System;

    /// <summary>
    /// The identity of the Tenant/Organisation the record belongs
    /// to within a multi-tenant application.
    /// <para>
    /// See also <see cref="IHasApplicationIdentifier"/>
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Use of an OrganisationIdentifier in a multi-tenant application
    /// has design implications worth considering.
    /// <para>
    /// Not having an organisationIdentifier in a multitenant applicaition
    /// is not justfiable in terms of security. The risk is too great that 
    /// a Select statement will introduced that 
    /// </para>
    /// </para>
    /// </remarks>
    public interface IHasOrganisationIdentifierReadOnly
    {
        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// In a 
        /// </para>
        /// <para>
        /// Design tip: it is preferable to allow users to register only online, 
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The organisation id.
        /// </value>
        Guid OrganisationId { get;  }
    }
}