﻿
namespace XAct
{
    using System;

    /// <summary>
    /// Has a <see cref="Guid"/> for datastore Id.
    /// </summary>
    public interface IHasGuidIdentity:IHasId<Guid>
    {
        
    }
}
