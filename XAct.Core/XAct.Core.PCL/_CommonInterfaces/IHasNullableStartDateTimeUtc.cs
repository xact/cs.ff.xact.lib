namespace XAct
{
    using System;

    /// <summary>
    /// Contract for an entity that has a StartDate (UTC).
    /// <para>
    /// Used for News and Project Tasks .
    /// </para>
    /// </summary>
    /// <internal>
    /// <para>
    /// See 
    /// <see cref="IHasNullableStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableStartDateTimeEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeEndDateTimeUtc"/>
    /// </para>
    /// </internal>
    public interface IHasNullableStartDateTimeUtc
    {
        /// <summary>
        /// Gets or sets the start date (UTC).
        /// </summary>
        /// <value>
        /// The start date UTC.
        /// </value>
        DateTime? StartDateTimeUtc { get; set; }
    }
}