﻿namespace XAct
{
    public interface IHasPostalAddress : IHasPostalAddressMinimal
    {


        /// <summary>
        /// Gets or sets the name to put on the address.
        /// <para>
        /// Important: the name used on an address does not
        /// automatically match with the name of the owner of an address.
        /// An example would be, for a corporation, the Name on the address 
        /// could be Reception (whereas the name of the company would be something 
        /// like XYZ Inc.)
        /// </para>
        /// <para>
        /// Another common case where the address name may be different is on a Billing Address
        /// where the name is used for Verification purposes (eg: J SMITH), whereas the full
        /// Customer's full name is John Wilbert Smith.
        /// </para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the email address associated to this Address.
        /// <para>
        /// In the case of a Billing Address, the Email is used for Verification, 
        /// and in the case of a Shipping Address, the email is used to alert that
        /// physical delivery is imminent, attempted, failed.
        /// </para>
        /// </summary>
        string Email { get; set; }

        /// <summary>
        /// Gets or sets the phone number associated to this Address.
        /// <para>
        /// In the case of a Billing Address, the number is used for Verification, 
        /// and in the case of a Shipping Address, the number is used to alert that
        /// physical delivery is imminent, attempted, failed.
        /// </para>
        /// </summary>
        string Phone { get; set; }

        /// <summary>
        /// Gets or sets the phone number associated to this Address.
        /// <para>
        /// In the case of a Billing Address, the number is used for Verification, 
        /// and in the case of a Shipping Address, the number is used to alert that
        /// physical delivery is imminent, attempted, failed.
        /// </para>
        /// </summary>
        string PhoneExt { get; set; }

        /// <summary>
        /// Gets or sets the phone number associated to this Address.
        /// <para>
        /// In the case of a Billing Address, this is ignored.
        /// </para>
        /// <para>
        /// In the case of a Shipping Address, this can be used to
        /// provide additional information ('Leave on Doorstep', etc.).
        /// </para>
        /// </summary>
        string Instructions { get; set; }

    }
}
