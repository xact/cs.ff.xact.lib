﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an entity that defines its importance.
    /// </summary>
    public interface IHasImportance
    {
        /// <summary>
        /// Gets or sets the importance of the message/entity/etc.
        /// <para>Member defined in<see cref="IHasImportance"/></para>
        /// </summary>
        /// <value>The importance.</value>
        Importance Importance { get; set; }
    }
}
