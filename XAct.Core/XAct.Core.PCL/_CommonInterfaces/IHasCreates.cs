﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    ///   Contract used for Factories (mostly Business Layer Aggregate entities).
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     See <c>XAct.Domain.Repositories.IRepositoryService{TEntityWrapperInterface}"</c>
    ///     where the Repository pattern (which returns Aggregate objects from the Persistence/Data Layer),
    ///     is combined with the Factory Pattern, 
    ///     in order to keep the Business or UI Layer from having a means to directly 
    ///     instantiate (and therefore tighly couple, which is not a good thing...) any BO's.
    ///   </para>
    /// <para>
    /// Note that I have never been happy with nomenclature. It's not IFactory, but IHasCreates
    /// is rather Naf. ...
    /// </para>
    /// </remarks>
    /// <internal>
    /// FIX: Made internal as a Repository in App.Data that uses this base interface
    /// will drag a ref to XAct.Domain up to App.Biz layer -- not at all acceptable.
    /// </internal>
    /// <typeparam name = "TAggregateRootEntity"></typeparam>
    public interface IHasCreates<TAggregateRootEntity> : IHasCreate<TAggregateRootEntity>, IHasCreateCollection<TAggregateRootEntity>
    {
    }
}