namespace XAct
{
    using System;

    /// <summary>
    /// Contract for an entity that has an actual End Date (UTC).
    /// <para>
    /// Used for News and Project Tasks .
    /// </para>
    /// </summary>
    /// <internal>
    /// <para>
    /// See 
    /// <see cref="IHasNullableStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableStartDateTimeEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeEndDateTimeUtc"/>
    /// </para>
    /// </internal>
    public interface IHasNullableEndDateTimeUtc
    {
        /// <summary>
        /// Gets or sets the actual end date (UTC).
        /// </summary>
        DateTime? EndDateTimeUtc { get; set; }
    }
}