﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Interface to implement date tracking on an Item.
    /// <para>
    /// See also <see cref="IHasAuditability"/>.
    /// </para>
    /// </summary>
    public interface IHasDateTimeTrackabilityUtcSimple : IHasDateTimeCreatedOnUtc, IHasDateTimeModifiedOnUtc
    {
    }
}