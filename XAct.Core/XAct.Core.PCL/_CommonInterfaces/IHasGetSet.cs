﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for typed access to variables by key (Properties can't be Typed).
    /// </summary>
    public interface IHasGetSet
    {
        /// <summary>
        /// Gets the value.
        /// <para>Member defined in<see cref="IHasGetSet"/></para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <returns></returns>
        TValue Get<TValue>(string key);

        /// <summary>
        /// Sets the specified value.
        /// <para>Member defined in<see cref="IHasGetSet"/></para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        void Set<TValue>(string key, TValue value);
    }
}
