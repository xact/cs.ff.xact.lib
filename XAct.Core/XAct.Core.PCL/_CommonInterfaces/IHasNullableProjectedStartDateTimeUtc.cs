namespace XAct
{
    using System;

    /// <summary>
    /// The projected start datetime.
    /// <para>
    /// Used for Task and Project planning.
    /// </para>
    /// </summary>
    /// <internal>
    /// <para>
    /// See 
    /// <see cref="IHasNullableStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableStartDateTimeEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeEndDateTimeUtc"/>
    /// </para>
    /// </internal>
    public interface IHasNullableProjectedStartDateTimeUtc : IHasNullableStartDateTimeUtc
    {
        /// <summary>
        /// Gets or sets the projected started date.
        /// </summary>
        DateTime? ProjectedStartDateTimeUtc { get; set; }

        
    }
}