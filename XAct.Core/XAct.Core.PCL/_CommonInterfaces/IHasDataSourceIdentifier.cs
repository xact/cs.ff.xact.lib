﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// The Contract for a object that defines the source of a element.
    /// <para>
    /// Intended to be implemented by a CodeFirst ComplexType - of type 
    /// <see cref="IHasDataSourceIdentified"/>, that is then attached 
    /// as a property of the Vertex itself.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// The whole goal is to offer some kind of record (eg: Vertex) a way
    /// to describe its source:
    /// <code>
    /// <![CDATA[
    /// public Vertex : IDataSourceIdentified {
    ///    ...
    ///    public VertexDataSource DataSource {get;set}
    /// }
    /// public VertexDataSource : IDataSourceIdentifier {
    ///   ...
    /// } 
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public interface IHasDataSourceIdentifier : IHasDataSourceIdentifierReadOnly
    {
        /// <summary>
        /// A unique identifier (FK) for the <c>IDataSource</c> 
        /// (eg: the XXX Table in YYY Db in ZZZ ConnectionString)
        /// <para>Member defined in<see cref="IHasDataSourceIdentifier"/></para>
        /// </summary>
        /// <remarks>
        /// The Guid (rather than a shorter Int) allows developers to develop offline, 
        /// rather than having to first reserve a DataSource Id, centrally.
        /// </remarks>
        new Guid DataSourceId { get; set; }

        /// <summary>
        /// The unique identifiers for the data object in the source XXX table. 
        /// <para>
        /// It is up to the Connector to know how to deserialize the value
        /// into a string, guid, etc. depending on the Connector's knowledge
        /// of the source catalog.
        /// </para>
        /// <para>Member defined in<see cref="IHasDataSourceIdentifier"/></para>
        /// </summary>
        /// <remarks>
        /// I agonized a long time on whether this should be a dictionary,
        /// but 
        /// a) makes it hard to impossible to use ORMs effectively,
        /// b) saves time serialize/deserialize as the most common use is in the serialized state.
        /// </remarks>
        new string DataSourceSerializedIdentities { get; set; }
    }
}