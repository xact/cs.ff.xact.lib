﻿namespace XAct
{
    using System;

    /// <summary>
    /// The identity of the Tenant/Organisation the record belongs
    /// to within a multi-tenant application.
    /// <para>
    /// See also <see cref="IHasApplicationIdentifier"/>
    /// </para>
    /// </summary>
    public interface IHasApplicationTennantIdReadOnly
    {
        /// <summary>
        /// Gets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online, 
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService"/>
        /// </para>
        /// </summary>
        Guid ApplicationTennantId { get; }
    }

    /// <summary>
    /// The identity of the Tenant/Organisation the record belongs
    /// to within a multi-tenant application.
    /// <para>
    /// See also <see cref="IHasApplicationIdentifier"/>
    /// </para>
    /// </summary>
    public interface IHasApplicationTennantId :IHasApplicationTennantIdReadOnly
    {
        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online, 
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService"/>
        /// </para>
        /// </summary>
        new Guid ApplicationTennantId { get; set; }
    }
}