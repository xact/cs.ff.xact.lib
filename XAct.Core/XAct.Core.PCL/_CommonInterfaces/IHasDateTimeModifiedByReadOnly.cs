namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasDateTimeModifiedByReadOnly : IHasDateTimeModifiedOnUtcReadOnly
    {
        /// <summary>
        /// Gets or sets the identity who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeModifiedByReadOnly"/></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        string LastModifiedBy { get; }
    }
}