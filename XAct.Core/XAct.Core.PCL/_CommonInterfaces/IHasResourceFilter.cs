﻿namespace XAct
{
    public interface IHasResourceFilter :IHasResourceFilterReadOnly
    {
        /// <summary>
        /// Gets or sets the Filter for the Resource in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        new string ResourceFilter { get; set; }
    }
}