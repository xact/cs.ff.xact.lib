﻿

/// <summary>
/// <para>
/// An <c>XActLib</c> contract.
/// </para>
/// A contract to define an entity or collection as ReadOnly.
/// </summary>
    public interface IHasIsReadOnly
    {
        /// <summary>
        /// Gets a value indicating whether the properties of this instance are read only.
        /// <para>Member defined in<see cref="IHasIsReadOnly"/></para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is read only; otherwise, <c>false</c>.
        /// </value>
        bool IsReadOnly { get; set; }
    }

