﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an object that is orderable.
    /// <para>
    /// Contract defined 
    /// </para>
    /// </summary>
    public interface IHasOrder: IHasOrderReadOnly
    {
        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder"/>.
        /// </para>
        /// </summary>
        new int Order { get; set; }
    }
}
