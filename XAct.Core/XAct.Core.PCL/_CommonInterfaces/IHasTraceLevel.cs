﻿namespace XAct
{
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public interface IHasTraceLevel
    {
        TraceLevel TraceLevel { get; set; }
    }
}