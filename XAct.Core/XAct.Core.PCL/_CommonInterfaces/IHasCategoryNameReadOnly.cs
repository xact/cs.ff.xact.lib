﻿namespace XAct
{
    /// <summary>
    /// The contract for a object that has a GroupName.
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasCategoryNameReadOnly
    {
        /// <summary>
        /// Gets the group name of the object.
        /// <para>Member defined in<see cref="XAct.IHasCategoryNameReadOnly"/></para>
        /// </summary>
        string CategoryName { get; }
    }
}