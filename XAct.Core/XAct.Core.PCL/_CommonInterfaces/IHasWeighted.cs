﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract to get the weight of an entity (Vertex, Edge, other).
    /// </summary>
    public interface IHasWeight
    {
        /// <summary>
        /// Gets or sets the weight of the object.
        /// <para>
        /// Defined in the <see cref="IHasWeight"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        int Weight {get;set;}
    }
}
