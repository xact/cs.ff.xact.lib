namespace XAct
{
    using System.Collections.Generic;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a clas that creates collections for instances (ie, a factory).
    /// <para>
    /// See also <see cref="IHasCreate{TInstanceType}"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TInstanceType">The type of the instance type.</typeparam>
    public interface IHasCreateCollection<TInstanceType>
    {
        /// <summary>
        /// Creates a collection to hold instances.
        ///   <para>
        ///     Examples of use would be:
        ///     <example>
        ///       <code>
        ///         <![CDATA[
        /// //Avoid direct instantiation/tight coupling:
        /// //ContactCollection contacts = new ContactCollection();
        /// //in preference for instantiation via a factory:
        /// ICollection<IContact> contacts = _ContactRepository.CreateInstanceCollection();
        /// contacts.Add(_ContactRepository.CreateInstance());
        /// ]]>
        ///       </code>
        ///     </example>
        ///   </para>
        /// <para>Member defined in<see cref="IHasCreateCollection{TInstanceType}"/></para>
        /// </summary>
        /// <returns></returns>
        ICollection<TInstanceType> CreateInstanceCollection();
        
    }
}