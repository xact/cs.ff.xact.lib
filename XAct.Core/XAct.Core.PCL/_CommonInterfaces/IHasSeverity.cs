﻿
namespace XAct
{
    using XAct.Messages;

    /// <summary>
    /// 
    /// </summary>
    public interface IHasSeverity
    {

        /// <summary>
        /// Gets or sets the severity of the <see cref="Message"/>.
        /// </summary>
        /// <value>
        /// The severity.
        /// </value>
        Severity Severity { get; set; }
    }
}
