﻿

// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for serializing a Typed Value in a datastore.
    /// <para>
    /// Note that one cannot automatically add IHasValue{T} as one does not know the Type...
    /// </para>
    /// <para>
    /// Use <see cref="IHasSerializedTypeValueAndMethodExtensions.DeserializeValue{T}"/>
    /// and 
    /// <see cref="IHasSerializedTypeValueAndMethodExtensions.SerializeValue{T}"/>
    /// </para>
    /// to work with these properties.
    /// </summary>
    public interface IHasSerializedTypeValueAndMethod
    {
        /// <summary>
        /// Gets or sets the serialization method.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod"/> contract.
        /// </para>
        /// </summary>
        /// <value>The serialization method.</value>
        SerializationMethod SerializationMethod { get; set; }



        /// <summary>
        /// Gets or sets the Assembly qualified name of the Value that is serialized.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod"/> contract.
        /// </para>
        /// </summary>
        /// <value>The type.</value>
        /// <internal><para>8/16/2011: Sky</para></internal>
        string SerializedValueType { get; set; }



        /// <summary>
        /// Gets or sets the serialized value.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod"/> contract.
        /// </para>
        /// </summary>
        /// <value>The value.</value>
        /// <internal><para>8/16/2011: Sky</para></internal>
        string SerializedValue { get; set; }
    }
}
