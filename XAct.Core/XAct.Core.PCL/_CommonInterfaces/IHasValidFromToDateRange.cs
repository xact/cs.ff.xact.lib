﻿namespace XAct
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for objects that are valid for only a while (eg, a Marketing Campaign, or News board).
    /// </summary>
    public interface IHasValidFromToDateRange
    {
        /// <summary>
        /// Gets or sets the starting date which this item is valid.
        /// <para>
        /// Member defined in the <see cref="IHasValidFromToDateRange"/> contract.
        /// </para>
        /// </summary>
        /// <value>The valid from.</value>
        DateTime? ValidFrom { get; set; }

        /// <summary>
        /// Gets or sets the end date to which this item is valid.
        /// <para>
        /// Member defined in the <see cref="IHasValidFromToDateRange"/> contract.
        /// </para>
        /// </summary>
        /// <value>The valid to.</value>
        DateTime? ValidTo { get; set; }

    }
}
