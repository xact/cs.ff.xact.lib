﻿namespace XAct
{
    /// <summary>
    /// Contract for entities that provide both a
    /// display <c>Text</c> and a <c>Description</c>.
    /// <para>
    /// Implemented by <see cref="IHasReferenceDataReadOnly"/>
    /// </para>
    /// <para>
    /// See also <see cref="IHasTextAndTitle"/>
    /// which is used for in-app <c>Notifications</c>.
    /// </para>
    /// <para>
    /// See also <see cref="IHasSubjectAndBody"/>
    /// wwhic is used for external <c>Notifications</c>.
    /// </para>
    /// </summary>
    public interface IHasTextAndDescriptionReadOnly : IHasTextReadOnly, IHasDescriptionReadOnly
    {
        
    }

    /// <summary>
    /// Contract for entities that provide both a
    /// display <c>Text</c> and a <c>Description</c>.
    /// <para>
    /// Implemented by <see cref="IHasReferenceData"/>
    /// </para>
    /// <para>
    /// See also <see cref="IHasTextAndTitle"/>
    /// which is used for in-app <c>Notifications</c>.
    /// </para>
    /// <para>
    /// See also <see cref="IHasSubjectAndBody"/>
    /// wwhic is used for external <c>Notifications</c>.
    /// </para>
    /// </summary>
    public interface IHasTextAndDescription : IHasTextAndDescriptionReadOnly, IHasText, IHasDescription
    {

    }
}