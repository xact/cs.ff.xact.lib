﻿namespace XAct
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract for indicating when an object was last Touched/Accessed.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Implemented by <c>IDateTimeStampedElementWrapper</c>
    /// (in <c>XAct.Collections</c>)
    /// </para>
    /// </remarks>
    public interface IHasTouched
    {
        /// <summary>
        /// Gets the DateTime the item was last touched.
        /// <para>
        /// Member defined in the <see cref="IHasTouched"/> contract.
        /// </para>
        /// </summary>
        DateTime LastAccessed { get; }

        /// <summary>
        /// Updates <see cref="LastAccessed"/> to the current DateTime.
        /// <para>
        /// Member defined in the <see cref="IHasTouched"/> contract.
        /// </para>
        /// </summary>
        void Touch();

    }
}
