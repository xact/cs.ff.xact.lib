namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for retreieving the current Context.
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public interface IHasCurrentReadOnly<TContext> 
        //where TContext : class
    {
        /// <summary>
        /// Gets the current context object.
        /// <para>Member defined in<see cref="IHasCurrentReadOnly{TContext}"/></para>
        /// </summary>
        /// <value>The context.</value>
        TContext Current
        {
            get;
        }
    }
}