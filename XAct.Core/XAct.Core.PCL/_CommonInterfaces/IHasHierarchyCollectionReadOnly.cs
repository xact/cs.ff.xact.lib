﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A Contract for hierarchical elements.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IHasHierarchyCollectionReadOnly<T>: IHasChildrenCollectionReadOnly<T>, IHasParentReadOnly<T>
    {

    }
}
