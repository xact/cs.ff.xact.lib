namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// 
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public interface IHasItemReadOnly<TItem>
    {
        /// <summary>
        /// Gets the item.
        /// <para>Member defined in<see cref="IHasItemReadOnly{TItem}"/></para>
        /// </summary>
        /// <value>
        /// The item.
        /// </value>
        TItem Item { get; }
    }
}