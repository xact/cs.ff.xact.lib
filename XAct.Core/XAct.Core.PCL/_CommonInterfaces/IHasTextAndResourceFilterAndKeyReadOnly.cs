﻿//namespace XAct
//{
//    using System;

//    /// <summary>
//    /// Contract for entities that have Text -- but intent 
//    /// to one day move to a resource driven architecture.
//    /// <para>
//    /// Resource driven multi-lingual application architecture
//    /// is the more desired way to develop software -- but the
//    /// reality is that the cost of developing resources up
//    /// front may be too exhorbitant for POCs. 
//    /// </para>
//    /// <para>
//    /// Consider using this contract for POC's and similar
//    /// sitations where the added cost of preparing resources
//    /// would be too onerous -- while laying down an architecture
//    /// that allows for only requiring smalll modification later
//    /// down the road.
//    /// </para>
//    /// <para>
//    /// WARNING: 
//    /// In other projects than POCs, But be wary of side effects: having easy
//    /// access to string methods without strong oversight
//    /// of their committed work will cause developers to
//    /// use the shortcut options at the peril of the architecure.
//    /// </para>
//    /// </summary>
//    [Obsolete]
//    public interface IHasTextAndResourceFilterAndKeyReadOnly : IHasTextReadOnly, IHasUseResourceFilterAndKeyReadOnly , IHasResourceFilterAndKeyReadOnly
//    {
        
//    }




//}