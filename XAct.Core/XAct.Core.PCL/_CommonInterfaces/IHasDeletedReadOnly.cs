namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an entity that is soft deleteable.
    /// </summary>
    public interface IHasDeletedReadOnly
    {
        /// <summary>
        /// Gets a value indicating whether this entity is deleted.
        /// <para>Member defined in<see cref="IHasDeletedReadOnly"/></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if deleted; otherwise, <c>false</c>.
        /// </value>
        bool Deleted { get; }
    }
}