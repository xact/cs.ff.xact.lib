namespace XAct
{
    public interface IHasParentReadOnly<TParent>
    {
        /// <summary>
        /// Gets the parent item if any.
        /// <para>Member defined in<see cref="IHasHierarchyCollection{T}"/></para>
        /// </summary>
        /// <value>The parent.</value>
        TParent Parent { get; set; }
    }
}