﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Interface for any context.
    /// <para>
    /// Note that it is implemented as a Method,
    /// as one cannot combine Generics and properties 
    /// (C# cannot do something like TContectType Context&lt;TContextType&gt;{get;set;}
    /// </para>
    /// </summary>
    /// <internal>
    /// <para>
    /// Dummy interface for Generic where clause only
    /// </para>
    /// <para>
    /// See also <see cref="IHasInnerItemReadOnly"/>, although that contract
    /// defines the Type much earlier.
    /// </para>
    /// </internal>
    public interface IHasGetContext 
    {

        /// <summary>
        /// Gets the context (Typed).
        /// <para>
        /// Note that it is implemented as a Method,
        /// as one cannot combine Generics and properties 
        /// (C# cannot do something like TContectType Context&lt;TContextType&gt;{get;set;}
        /// </para>
        /// <para>Member defined in<see cref="IHasGetContext"/></para>
        /// </summary>
        /// <typeparam name="TContextType">The type of the context type.</typeparam>
        /// <returns></returns>
        TContextType GetContext<TContextType>();
    }
}