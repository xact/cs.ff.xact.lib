﻿//namespace XAct
//{
//    /// <summary>
//    /// The contract for a Message (with Subject/Body) Entity that uses 
//    /// <c>XAct.Resources.IResourceService, XAct.Resources</c>
//    /// to render messages.
//    /// <para>
//    /// An example usage would be:
//    /// <code>
//    /// <![CDATA[
//    /// public class IExampleType : IHasId<Id>, IHasName, IHasSubjectAndBodyResourceFilterAndKeys {
//    /// ...
//    /// }
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// </summary>
//    public interface IHasSubjectAndBodyResourceFilterAndKeysReadOnly : IHasResourceFilterAndKeyReadOnly
//    {

//        /// <summary>
//        /// The Key to the Subject Resource in the <c>XAct.Resources.IResourceService, XAct.Resources</c>.
//        /// </summary>
//        string SubjectResourceKey { get;  }
        

//    }
//}