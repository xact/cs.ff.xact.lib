namespace XAct
{
    /// <summary>
    /// The projected start and end datetime.
    /// <para>
    /// Used for Task and Project planning.
    /// </para>
    /// </summary>
    /// <internal>
    /// <para>
    /// See 
    /// <see cref="IHasNullableStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableStartDateTimeEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeEndDateTimeUtc"/>
    /// </para>
    /// </internal>
    public interface IHasNullableProjectedStartDateTimeEndDateTimeUtc : IHasNullableStartDateTimeEndDateTimeUtc, IHasNullableProjectedStartDateTimeUtc, IHasNullableProjectedEndDateTimeUtc
    {
        
    }
}