﻿

namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// Contract used for <c>ISelectableItem</c>
    /// and other similar contracts.
    /// </para>
    /// </summary>
    public interface IHasSelected : IHasSelectedReadOnly
    {
        /// <summary>
        /// Gets or sets whether a value indicating whether this item is selected.
        /// <para>
        /// Member defined in the <see cref="IHasSelected"/> contract.
        /// </para>
        /// </summary>
        /// <value><c>true</c> if selected; otherwise, <c>false</c>.</value>
        new bool Selected { get; set; }
    }
}
