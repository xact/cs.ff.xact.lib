﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for any element that has a Title Text.
    /// </summary>
    public interface IHasTitleReadOnly
    {
        /// <summary>
        /// Gets the title.
        /// <para>
        /// Member defined in the <see cref="IHasTitleReadOnly"/> contract.
        /// </para>
        /// </summary>
        string Title { get; }
    }
}
