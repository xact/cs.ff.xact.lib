﻿
namespace XAct.Authentication
{
    /// <summary>
    /// A contract for a set of classic authentication settings.
    /// </summary>
    /// <remarks>
    /// So common, that it should be a common interface.
    /// </remarks>
    public interface IHasUserNameAndPasswordReadOnly : IHasName
    {
        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        new string Name { get;  }

        /// <summary>
        /// Gets the user password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        string Password { get; }
    }
}
