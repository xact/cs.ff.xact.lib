﻿namespace XAct
{
    /// <summary>
    /// Contract for entities that have a Text property.
    /// <para>
    /// Note: where possible, prefer the use of <see cref="IHasValue{T}"/>
    /// allowing for the user to Type as needed.
    /// </para>
    /// </summary>
    public interface IHasText :IHasTextReadOnly
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        new string Text { get; set; }
    }
}