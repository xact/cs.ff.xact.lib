namespace XAct
{
    public interface IHasIsSimpleAuditModel :IHasIsAuditModel
    {
        /// <summary>
        /// Name of the source table being audited.
        /// </summary>
        string Source { get; set; }
    }
}