namespace XAct
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// A contract to provide information as to when 
    /// a Record was accesed.
    /// </para>
    /// <para>
    /// See also <see cref="IHasAuditability"/>.
    /// </para>
    /// </summary>
    public interface IHasDateTimeAccessedOnUtcReadOnly
        
    {
        /// <summary>
        /// Gets the date this entity was last accessed, expressed in UTC.
        /// </summary>
        DateTime? LastAccessedOnUtc { get; }
    }
}