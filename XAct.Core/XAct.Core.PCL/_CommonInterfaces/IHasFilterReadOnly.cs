﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// Used in drop down filtering, etc.
    /// </para>
    /// </summary>
    public interface IHasFilterReadOnly
    {
        /// <summary>
        /// Gets the string that can be used as filter.
        /// <para>
        /// By convention the syntax is similar to CSV, 
        /// but with ! for NOT clauses, &amp; for AND, etc:
        /// <example>
        /// <![CDATA[
        /// AA;!BB;CC&DD;CC&!DD
        /// ]]>
        /// </example>
        /// </para>
        /// <para>Member defined in<see cref="IHasFilterReadOnly"/></para>
        /// </summary>
        string Filter { get; }
    }
}
