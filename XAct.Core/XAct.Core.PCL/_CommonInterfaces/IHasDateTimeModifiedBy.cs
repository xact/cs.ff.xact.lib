namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasDateTimeModifiedBy : IHasDateTimeModifiedByReadOnly, IHasDateTimeModifiedOnUtc
    {
        /// <summary>
        /// Gets or sets the identity who Modified the document.
        /// <para>Member defined in<see cref="IHasDateTimeModifiedBy"/></para>
        /// </summary>
        /// <value>
        /// The Modified by.
        /// </value>
        new string LastModifiedBy { get; set; }
    }
}