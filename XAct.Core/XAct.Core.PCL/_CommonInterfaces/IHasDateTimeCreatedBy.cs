﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasDateTimeCreatedBy : IHasDateTimeCreatedByReadOnly , IHasDateTimeCreatedOnUtc
    {
        /// <summary>
        /// Gets or sets the who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy"/></para>
        /// </summary>
        new string CreatedBy { get; set; }
    }
}
