namespace XAct
{
    public interface IHasParent<TParent>:IHasParentReadOnly<TParent>
    {
        /// <summary>
        /// Gets the parent item if any.
        /// <para>Member defined in<see cref="IHasHierarchyCollection{T}"/></para>
        /// </summary>
        /// <value>The parent.</value>
        new TParent Parent { get; set; }
    }
}