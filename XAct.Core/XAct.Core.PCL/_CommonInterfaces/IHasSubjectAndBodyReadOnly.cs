﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a message.
    /// </summary>
    public interface IHasSubjectAndBodyReadOnly
    {
        /// <summary>
        /// Gets the message subject.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBodyReadOnly"/> contract.
        /// </para>
        /// </summary>
        /// <value>The subject.</value>
        string Subject { get; }
        /// <summary>
        /// Gets the message body.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBodyReadOnly"/> contract.
        /// </para>
        /// </summary>
        /// <value>The body.</value>
        string Body { get; }
    }
}
