﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract for objects that need to be defined as Valid or not.
    /// </summary>
    public interface IHasValid
    {
        /// <summary>
        /// Gets a value indicating whether this entity is valid.
        /// <para>
        /// Member defined in the <see cref="IHasValid"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if valid; otherwise, <c>false</c>.
        /// </value>
        bool Valid { get; }
    }
}
