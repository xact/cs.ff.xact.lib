namespace XAct
{


    public interface IHasClaim
    {
        /// <summary>
        /// The claim type (Name, SS, etc.)
        /// </summary>
        string Type { get; set; }

        /// <summary>
        /// The identity claim for the entity.
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// The guarantor of the claim.
        /// </summary>
        string Authority { get; set; }

    }
}