﻿namespace XAct
{
    using System;

    public interface IHasCorrelationGuidIdReadOnly : IHasCorrelationIdReadOnly<Guid>
    {
    }
}