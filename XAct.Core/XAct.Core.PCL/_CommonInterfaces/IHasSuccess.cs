﻿
namespace XAct
{
    public interface IHasSuccess : IHasSuccessReadOnly
    {
        /// <summary>
        /// Gets or sets a value indicating whether this 
        /// oepration completed successfully.
        /// </summary>
        new bool Success { get; set; }
    }
}
