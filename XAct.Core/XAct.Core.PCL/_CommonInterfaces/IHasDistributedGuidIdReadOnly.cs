﻿namespace XAct
{
    using System;

    public interface IHasDistributedGuidIdReadOnly :IHasDistributedIdReadOnly<Guid>
    {
    
    }
}