namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an object that is described.
    /// </summary>
    public interface IHasDescriptionReadOnly
    {
        /// <summary>
        /// Gets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly"/></para>
        /// </summary>
        string Description { get; }
    }
}