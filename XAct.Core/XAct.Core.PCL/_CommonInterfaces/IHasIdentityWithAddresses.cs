namespace XAct
{
    using System;
    using System.Collections.Generic;

    public interface IHasIdentityWithAddresses<TEntity,TEntityAlias,TClaim,TPostalAddress> : IHasIdentity<TEntity,TEntityAlias,TClaim>
        where TEntityAlias : class, IHasDistributedGuidIdAndTimestamp, IHasNames, new()
        where TClaim : class, IHasDistributedGuidIdAndTimestamp, IHasPersistableClaim, new()
        where TPostalAddress : class, IHasDistributedGuidIdAndTimestamp, IHasPostalAddress, new()
    {
        /// <summary>
        /// The FK to the preferred billing address of the
        /// identity.
        /// <para>
        /// Sales systems can use this value as a starting 
        /// point for a transaction.
        /// </para>
        /// </summary>
        Guid? PreferredBillingAddressFK { get; set; }

        TPostalAddress PreferredBillingAddress { get; set; }

        /// <summary>
        /// The FK to the preferred shipping address of the
        /// identity.
        /// <para>
        /// Sales systems can use this value as a starting 
        /// point for a transaction.
        /// </para>
        /// </summary>
        Guid? PreferredShippingAddressFK { get; set; }


        TPostalAddress PreferredShippingAddress { get; set; }

        /// <summary>
        /// Collection of addresses related to the entity.
        /// </summary>
        ICollection<TPostalAddress> Addresses { get;  }
   
    }
}