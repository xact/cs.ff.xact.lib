namespace XAct
{
    public interface IHasStatusFK
    {
        string StatusFK { get; set; }
    }
}