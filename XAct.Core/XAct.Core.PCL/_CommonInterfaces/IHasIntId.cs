﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Has an integer as Id.
    /// </summary>
    /// <remarks>
    /// See also <see cref="IHasGuidIdentity"/>
    /// </remarks>
    public interface IHasIntId:IHasId<int>
    {
    }
}
