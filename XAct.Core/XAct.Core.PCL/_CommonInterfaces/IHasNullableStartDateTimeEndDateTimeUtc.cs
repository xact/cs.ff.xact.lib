namespace XAct
{
    /// <summary>
    /// Contract for an entity that has a Start and End date (UTC)
    /// <para>
    /// Used for News and Project Tasks .
    /// </para>
    /// </summary>
    /// <internal>
    /// <para>
    /// See 
    /// <see cref="IHasNullableStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableStartDateTimeEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeEndDateTimeUtc"/>
    /// </para>
    /// </internal>
    public interface IHasNullableStartDateTimeEndDateTimeUtc : IHasNullableStartDateTimeUtc, IHasNullableEndDateTimeUtc
    {
        
    }
}