﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an object that has a read/write Value.
    /// <code>
    /// <![CDATA[
    /// public class SomeKeyValue : INamed, IValue<string> {
    ///   public string Name {get;set;}
    ///   public string Value {get;set;}
    /// }
    /// ]]>
    /// </code>
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IHasValue<TValue> : IHasValueReadOnly<TValue>
    {
        /// <summary>
        /// Gets or sets the value.
        /// <para>Member defined in<see cref="IHasValueReadOnly{TValue}"/></para>
        /// </summary>
        new TValue Value { get; set; }
    }
}
