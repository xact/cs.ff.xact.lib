﻿using System.Collections.Generic;

namespace XAct
{
    using XAct.Messages;

    /// <summary>
    /// Contract for messages that include metadata.
    /// <para>
    /// Implemented by <see cref="IHasReferenceData{T,T}"/>
    /// </para>
    /// </summary>
    public interface IHasMetadataReadOnly 
    {
        ICollection<Metadata> Metadata { get; }
    }
}
