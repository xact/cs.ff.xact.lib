namespace XAct
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an object that exposes an <see cref="Exception"/>
    /// </summary>
    public interface IHasExceptionReadOnly
    {
        /// <summary>
        /// Gets the exception.
        /// <para>Member defined in<see cref="IHasExceptionReadOnly"/></para>
        /// </summary>
        /// <value>The exception.</value>
        Exception Exception { get;  }
    }
}