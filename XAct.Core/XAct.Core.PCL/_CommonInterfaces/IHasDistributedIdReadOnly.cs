﻿namespace XAct
{
    public interface IHasDistributedIdReadOnly<TId> :IHasIdReadOnly<TId>
        where TId:struct
    {
        
    }
}