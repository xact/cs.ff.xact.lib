namespace XAct
{
    /// <summary>
    /// 
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface  IHasUserIdentifierReadOnly
    {
        /// <summary>
        /// Gets the user identifier.
        /// <para>
        /// Member defined in the <see cref="IHasUserIdentifierReadOnly"/> contract.
        /// </para>
        /// </summary>
        /// <value>The user identifier.</value>
        string UserIdentifier { get;  }
    }
}