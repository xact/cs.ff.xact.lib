﻿
namespace XAct
{
    /// <summary>
    /// Contract for objects that are Initializable.
    /// <para>
    /// See <see cref="IHasInitialized"/>
    /// </para>
    /// </summary>
    public interface IHasInitialize :IHasInitializedReadOnly
    {
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Initialize();
    }
}