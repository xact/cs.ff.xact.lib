﻿

namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
   public interface IHasNote: IHasNoteReadOnly
    {
        /// <summary>
        /// Gets or sets the note.
        /// <para>
        /// Member defined in the <see cref="IHasNote"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The note.
        /// </value>
       new string Note { get; set; }
    }
}
