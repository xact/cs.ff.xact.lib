﻿namespace XAct
{
    public interface IHasSettingRenderingHints : IHasRenderingHints,  IHasSettingRenderingHintsReadOnly
    {
        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to retrieve a Resource
        /// for a Label.
        /// <para>
        /// In many cases it will be just the Resource Id.
        /// </para>
        /// <para>
        /// But it could be a richer system such as
        /// <code>
        /// label:RES_L123;Hint:RES_H123
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// The render label hints.
        /// </value>
        new string RenderLabelHints { get; set; }


        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to choose an appropriate platform
        /// specific control.
        /// <para>
        /// Examples are:
        /// <code>
        /// <![CDATA[
        /// * Bool
        /// * Int
        /// * Text (or String)
        /// * Date
        /// * Time
        /// * DateTime
        /// * CustomXYZ
        /// ]]>
        /// </code>
        /// or more verbosely:
        /// <code>
        /// <![CDATA[
        /// * Type:Bool
        /// * Type:Int
        /// * Type:Text;MaxLength:40;
        /// * Type:Date
        /// * Type:Time
        /// * Type:DateTime
        /// * Type:CustomXYZ
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// A CSS formatted string.
        /// </value>
        new string RenderEditControlHints { get; set; }


        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to choose an appropriate platform
        /// specific control.
        /// <para>
        /// Examples are:
        /// <code>
        /// <![CDATA[
        /// * Bool
        /// * Int
        /// * Text (or String)
        /// * Date
        /// * Time
        /// * DateTime
        /// * CustomXYZ
        /// ]]>
        /// </code>
        /// or more verbosely:
        /// <code>
        /// <![CDATA[
        /// * Type:Bool
        /// * Type:Int
        /// * Type:Text;MaxLength:40;
        /// * Type:Date
        /// * Type:Time
        /// * Type:DateTime
        /// * Type:CustomXYZ
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// A CSS formatted string.
        /// </value>
        new string RenderViewControlHints { get; set; }

        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to choose an appropriate platform
        /// specific validation strategy.
        /// </summary>
        /// <value>
        /// A CSS formatted string.
        /// </value>
        new string RenderEditValidationHints { get; set; }

    }
}