﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// Contract used on services that can be enabled or not.
    /// </para>
    /// </summary>
    public interface IHasEnabled : IHasEnabledReadOnly
    {
        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled"/></para>
        /// </summary>
        new bool Enabled { get; set; }
    }
}
