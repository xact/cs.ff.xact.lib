﻿namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasDistributedGuidIdAndTimestamp : IHasDistributedGuidId, IHasTimestamp
    {
        
    }
}