﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an entity that is soft deleteable.
    /// </summary>
    public interface IHasDeleted : IHasDeletedReadOnly
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IHasDeletedReadOnly"/> is deleted.
        /// <para>Member defined in<see cref="IHasDeleted"/></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if deleted; otherwise, <c>false</c>.
        /// </value>
        new bool Deleted { get; set; }
    }

}
