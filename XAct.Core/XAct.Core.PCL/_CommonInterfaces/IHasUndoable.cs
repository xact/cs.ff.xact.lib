﻿
namespace XAct
{
    public interface IHasUndoable :IHasUndoableReadOnly
    {
        new bool Undoable { get; set; }
    }

    public interface IHasUndoableReadOnly
    {
        bool Undoable { get; }
    }

}
