﻿namespace XAct
{
    /// <summary>
    /// Contract for objects that have a Start method.
    /// <see cref="IHasStop"/>
    /// </summary>
    public interface IHasStart
    {

    }
}