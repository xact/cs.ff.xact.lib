﻿namespace XAct
{
    public interface IHasPostalAddressMinimal
    {
        /// <summary>
        /// Gets or sets the first street .
        /// </summary>
        string StreetLine1 { get; set; }

        /// <summary>
        /// Gets or sets optional additional street information.
        /// </summary>
        string StreetLine2 { get; set; }

        /// <summary>
        /// Gets or sets optional information about neighbourhood
        /// (eg: 'Soho').
        /// </summary>
        string Neighbourhood { get; set; }

        /// <summary>
        /// Gets or sets the name of the village, town, or city.
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Gets or sets the optional name of the region ('State' in the US).
        /// <para>
        /// eg: North Island.
        /// </para>
        /// </summary>
        string Region { get; set; }

        /// <summary>
        /// Gets or sets the postal code (Zip in the US).
        /// </summary>
        string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the Country.
        /// </summary>
        string Country { get; set; }
    }
}