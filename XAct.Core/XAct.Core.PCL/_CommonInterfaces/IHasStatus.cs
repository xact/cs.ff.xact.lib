using System;

namespace XAct
{
    public interface IHasStatus : IHasStatusReadOnly
    {
        new string Status { get; set; }

    }

    public interface IHasStatusReadOnly
    {
        string Status { get; }
    }
}