namespace XAct
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// A contract to provide information as to when 
    /// a Record was accessed.
    /// </para>
    /// <para>
    /// See also <see cref="IHasAuditability"/>.
    /// </para>
    /// </summary>
    public interface IHasDateTimeAccessedOnUtc : IHasDateTimeAccessedOnUtcReadOnly
    {
        /// <summary>
        /// Gets or sets the date this entity was last accessed, expressed in UTC.
        /// </summary>
        new DateTime? LastAccessedOnUtc { get; set; }
    }
}