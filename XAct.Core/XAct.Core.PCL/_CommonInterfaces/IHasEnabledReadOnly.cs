namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// Contract used on services that can be enabled or not.
    /// </para>
    /// </summary>
    public interface IHasEnabledReadOnly
    {
        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabledReadOnly"/></para>
        /// </summary>
        bool Enabled { get; }
    }
}