﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract for an entity Id of type 
    /// <see cref="IHasDistributedIdentities"/>
    /// allowing the record to be synced across 
    /// peers without conflict
    /// </summary>
    public interface IHasDistributedIdentityComplexType
    {
        /// <summary>
        /// Gets or sets the distributed identity (machine.
        /// <para>Member defined in<see cref="IHasDistributedIdentityComplexType"/></para>
        /// </summary>
        /// <value>The distributed identity.</value>
        IHasDistributedIdentities DistributedIdentity { get; set; }
    }
}
