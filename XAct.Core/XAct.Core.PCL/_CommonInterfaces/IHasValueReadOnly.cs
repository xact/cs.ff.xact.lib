﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Interface for an object that has read-only acess to a Value property.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IHasValueReadOnly<TValue>
    {
        /// <summary>
        /// Gets the value.
        /// <para>
        /// Note: ReadOnly.
        /// </para>
        /// <para>
        /// Defined in the <see cref="IHasValueReadOnly{TValue}"/> contract.
        /// </para>
        /// </summary>
        TValue Value { get; }
    }
}
