namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasDateTimeCreatedByReadOnly : IHasDateTimeCreatedOnUtcReadOnly
    {
        /// <summary>
        /// Gets or sets the who created the document.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedByReadOnly"/></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        string CreatedBy { get;  }
    }
}