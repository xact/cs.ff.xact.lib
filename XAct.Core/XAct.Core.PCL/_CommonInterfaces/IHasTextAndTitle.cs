﻿namespace XAct
{
    using XAct.Messages;

    /// <summary>
    /// Contract for message 
    /// entities that implement
    /// a title and text body.
    /// <para>
    /// Implemented by <see cref="RecursiveTextAndTitleBase{T}"/>
    /// </para>
    /// <para>
    /// See also <see cref="IHasTextAndDescriptionReadOnly"/>
    /// which is implemented by <see cref="IHasReferenceDataReadOnly{T}"/>
    /// </para>
    /// <para>
    /// See also <see cref="IHasSubjectAndBody"/>
    /// which is implemented by outgoing messages.
    /// </para>
    /// </summary>
    public interface IHasTextAndTitle : IHasText, IHasTitle
    {

    }
}