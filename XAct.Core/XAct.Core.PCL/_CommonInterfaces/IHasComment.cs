﻿
// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// The contract for an entity that has a single Comment associated to it.
    /// <para>
    /// Note: it's often poor design to embed a comment field in the entity:
    /// not only for SOC reasons, but also because it limits it to one comment for 
    /// and doesn't allow for response, nor knowing who's adding the comment.
    /// </para>
    /// </summary>
    public interface IHasComment : IHasDateTimeCreatedBy
    {
        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        string Comment { get; set; }
    }

}
