﻿namespace XAct
{
    using System;

    public interface IHasCorrelationGuidId : IHasCorrelationId<Guid>, IHasCorrelationGuidIdReadOnly
    {
        new Guid CorrelationId { get; set; }
    }
}