namespace XAct
{
    using System;
    using XAct.Domain;

    /// <summary>
    /// Contract to apply to the entity 
    /// mentioned in <see cref="AuditableAttribute"/>.
    /// <para>
    /// Applied to entities that are the
    /// <see cref="AuditableAttribute" />
    /// of a reocrd is pointing to.
    /// </para>
    /// </summary>
    public interface IHasIsAuditModel
    {

        /// <summary>
        /// Gets or sets the Id of the audit record (not the Id of the record being Audited).
        /// </summary>
        /// <value>
        /// The audit identifier.
        /// </value>
        Guid AuditId { get; set; }

        /// <summary>
        /// The Action that was done to the record ("C"reate, "U"pdate, "D"elete).
        /// </summary>
        string Action { get; set; }


        /// <summary>
        /// Gets or sets the audit action on.
        /// </summary>
        /// <value>
        /// The audit action on.
        /// </value>
        DateTime AuditActionOn { get; set; }

        /// <summary>
        /// Gets or sets the audit action by.
        /// </summary>
        /// <value>
        /// The audit action by.
        /// </value>
        string AuditActionBy { get; set; }
    }
}