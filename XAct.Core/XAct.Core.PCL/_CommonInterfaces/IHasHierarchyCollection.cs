namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// A Contract for hierarchical elements.
    /// </para>
    /// <para>
    /// See also <see cref="IHasParentFK{T}"/>
    /// </para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IHasHierarchyCollection<T> : IHasChildrenCollectionReadOnly<T>, IHasHierarchyCollectionReadOnly<T>, IHasParent<T>
    {
        /// <summary>
        /// Gets the parent item if any.
        /// <para>Member defined in<see cref="IHasHierarchyCollection{T}"/></para>
        /// </summary>
        /// <value>The parent.</value>
        new T Parent { get; set; }
    }
}