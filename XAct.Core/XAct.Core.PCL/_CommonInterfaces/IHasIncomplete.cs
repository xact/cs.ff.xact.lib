﻿

// ReSharper disable CheckNamespace
namespace XAct.Graphs
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for a entity that is a placeholder -- in other words,
    /// a entity that is known to exist -- just not hydrated from the
    /// datastore.
    /// <para>
    /// Used by <c>XAct.Graph</c> library.
    /// </para>
    /// </summary>
    public interface IHasIncomplete
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is a place holder.
        /// <para>
        /// Defined in the <see cref="IHasIncomplete"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is A place holder; otherwise, <c>false</c>.
        /// </value>
        bool IsIncomplete { get; set; }

        /// <summary>
        /// Clears this entity's data
        /// and converts it to back to just a PlaceHolder
        /// <para>
        /// Defined in the <see cref="IHasIncomplete"/> contract.
        /// </para>
        /// </summary>
        void Clear();
    }
}
