﻿namespace XAct
{
    /// <summary>
    ///   <summary>
    /// Each reply message should contain a Correlation Identifier,
    /// a unique identifier that indicates which request message this reply is for.
    ///   </summary>
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IHasCorrelationIdReadOnly<out TId>
    {
        TId CorrelationId { get; }
    }
}