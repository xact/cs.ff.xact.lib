﻿
// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A contract to retrieve and persist a WellKnown entity.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IHasRetrievePersist<T>
    {

        /// <summary>
        /// Retrieves the current entity.
        /// </summary>
        /// <returns></returns>
        T Retrieve();

        /// <summary>
        /// Persists the specified entity.
        /// </summary>
        void Persist();
    }
}
