﻿namespace XAct
{
    /// <summary>
    /// Contract applied to all Model Persistence Maps
    /// </summary>
    public interface IHasXActLibModelPersistenceMap : IHasModelPersistenceMap
    {
    }
}