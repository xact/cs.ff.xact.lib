﻿
namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IHasInitialize<T>
    {
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Initialize(T itemToInitialize);
    }
}
