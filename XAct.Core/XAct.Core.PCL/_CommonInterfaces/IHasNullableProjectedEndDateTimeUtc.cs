namespace XAct
{
    using System;

    /// <summary>
    /// The projected end datetime.
    /// <para>
    /// Used for ScheduledTasks and Project Tasks .
    /// </para>
    /// </summary>
    /// <internal>
    /// <para>
    /// See 
    /// <see cref="IHasNullableStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableStartDateTimeEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedEndDateTimeUtc"/>, 
    /// <see cref="IHasNullableProjectedStartDateTimeEndDateTimeUtc"/>
    /// </para>
    /// </internal>
    public interface IHasNullableProjectedEndDateTimeUtc : IHasNullableEndDateTimeUtc
    {
        DateTime? ProjectedEndDateTimeUtc { get; set; }
    }
}