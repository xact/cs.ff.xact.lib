﻿namespace XAct
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// When deleting records in a database shared by different
    /// applications, the process has to be done ...gingerly...if one
    /// is not to lock the table.
    /// <para>
    /// The process is:
    /// * update the records with a datetime=now AND random number/Guid
    ///   where dateTime less than now minus 5 minutes, and or key = null.
    ///     * the random number is to allow load distributed scenarios.
    /// * pick up those same records (with datetime = now and key=key).
    /// * aggregate them (eg: sum)
    /// * delete them
    /// </para>
    /// </summary>
    public interface IHasSyncronizedDeleteInformation
    {
        /// <summary>
        /// A random value, different per server.
        /// </summary>
        Guid MarkedForDeletionKey { get; set; }

        /// <summary>
        /// A flag used to delete records when updating Sum values.
        /// </summary>
        [DataMember]
        DateTime? MarkedForDeletionDateTimeUtc { get; set; }

    }
}