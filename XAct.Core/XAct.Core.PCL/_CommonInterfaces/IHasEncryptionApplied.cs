﻿
// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract to that remote objects are transmitted with a means to indicate whether
    /// the data was received encrypted or not.
    /// <para>
    /// This is to ensure that transmissions to clients (browsers or RIA's) are readable.
    /// </para>
    /// <para>
    /// This could maybe still be combined with a Read/Write/Updatable flag...although
    /// this would not adhere to SOC (Authorization/Data).
    /// </para>
    /// <remarks>
    /// This has design implications: data cannot be searched as it is obfuscated.
    /// </remarks>
    /// </summary>
    public interface IHasEncryptionApplied
    {
        /// <summary>
        /// Gets or sets a value indicating whether this distributed entity is complete, 
        /// or was obfuscated as the remote user does not acess read access to the entity.
        /// <para>
        /// 
        /// </para>
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has obfuscation; otherwise, <c>false</c>.
        /// </value>
        bool ObfuscationApplied { get; set; }
        
        /// <summary>
        /// A memory only property, not persisted.
        /// </summary>
        DateTime? ObfuscationRequiredLastChecked { get; set; }

    }
}
