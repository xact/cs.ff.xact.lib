﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// Contract for serializing a Typed in a datastore.
    /// </para>
    /// </summary>
    public interface IHasTypeName
    {
        /// <summary>
        /// Gets or sets the Assembly qualified name of the Type that is serialized.
        /// </summary>
        /// <value>The type.</value>
        /// <internal><para>8/16/2011: Sky</para></internal>
        string TypeName { get; set; }

    }
}