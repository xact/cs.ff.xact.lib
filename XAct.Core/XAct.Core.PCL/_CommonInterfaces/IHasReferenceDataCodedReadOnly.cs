﻿namespace XAct
{
    /// <summary>
    /// Immutable Reference Data.
    /// <para>
    /// Examples are `Gender`, `PostageType`, etc.
    /// </para>
    /// <para>
    /// Note that the properties 'Title' and 'Text' were used rather
    /// than just 'Text', or 'Text' and 'Description',
    /// due to the intended use. If designing for Dropdowns only
    /// (UI's of the turn of the century), only 'Text' would have been
    /// required. Almost never 'Text' and 'Description' (windows ui's didn't usually
    /// show a tooltip per dropdown option.
    /// But take into account designing for tablets, and drop downs are
    /// now no longer used (too small for fingers) and instead full lists
    /// with Title and Textual Description are more often used.
    /// </para>
    /// <para>
    /// That said, `Text` and `Title` should be used in only the most trivial
    /// of POCs that will never require resource management.
    /// Instead, prefer setting
    /// <see cref="IHasUseResourceFilterAndKeyReadOnly.UseResourceFilterAndKey" />
    /// to true, and using `ResourceFilter` and `ResourceKey` properties
    /// in conjunction with
    /// <c>XAct.Resources.IResourceService</c> to generate a culture specific
    /// representation of the reference data.
    /// </para>
    /// </summary>
    /// <typeparam name="TId"></typeparam>
    public interface IHasCodedReferenceDataReadOnly<TId> : IHasReferenceDataReadOnly<TId>,
                                                   IHasCodeReadOnly
        where TId : struct
    {
         
    }

    /// <summary>
    /// Immutable Reference Data.
    /// <para>
    /// Examples are `Gender`, `PostageType`, etc.
    /// </para>
    /// <para>
    /// Note that the properties 'Title' and 'Text' were used rather
    /// than just 'Text', or 'Text' and 'Description',
    /// due to the intended use. If designing for Dropdowns only
    /// (UI's of the turn of the century), only 'Text' would have been
    /// required. Almost never 'Text' and 'Description' (windows ui's didn't usually
    /// show a tooltip per dropdown option.
    /// But take into account designing for tablets, and drop downs are
    /// now no longer used (too small for fingers) and instead full lists
    /// with Title and Textual Description are more often used.
    /// </para>
    /// <para>
    /// That said, `Text` and `Title` should be used in only the most trivial
    /// of POCs that will never require resource management.
    /// Instead, prefer setting
    /// <see cref="IHasUseResourceFilterAndKeyReadOnly.UseResourceFilterAndKey" />
    /// to true, and using `ResourceFilter` and `ResourceKey` properties
    /// in conjunction with
    /// <c>XAct.Resources.IResourceService</c> to generate a culture specific
    /// representation of the reference data.
    /// </para>
    /// </summary>
    /// <typeparam name="TId"></typeparam>
    public interface IHasCodedReferenceData<TId> : IHasCodedReferenceDataReadOnly<TId>, IHasReferenceData<TId>, IHasCode
        where TId : struct
    {

    }


}