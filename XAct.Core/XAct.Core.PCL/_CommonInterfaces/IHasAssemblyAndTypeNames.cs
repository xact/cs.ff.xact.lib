namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasAssemblyAndTypeNames : IHasMethodName
    {
        /// <summary>
        /// Gets or sets the name of the assembly.
        /// </summary>
        /// <value>
        /// The name of the assembly.
        /// </value>
        string AssemblyName { get; set; }

        /// <summary>
        /// Gets or sets the name of the class.
        /// </summary>
        /// <value>
        /// The name of the class.
        /// </value>
        string TypeFullName { get; set; }
    }
}