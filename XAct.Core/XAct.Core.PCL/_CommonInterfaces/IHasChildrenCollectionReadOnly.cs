namespace XAct
{
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IHasChildrenCollectionReadOnly<T>
    {

        /// <summary>
        /// Gets the collection of children items.
        /// <para>Member defined in<see cref="IHasHierarchyCollection{T}"/></para>
        /// </summary>
        /// <value>The children.</value>
        ICollection<T> Children { get; } 
    }
}