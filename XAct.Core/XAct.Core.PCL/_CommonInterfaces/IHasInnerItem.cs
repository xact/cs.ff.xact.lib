namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    ///   Contract for EntityWrappers.
    ///   <para>
    ///     Provides a way to get to the wrapped entity via an Explicitly defined interface.
    ///   </para>
    /// <para>
    /// Note: this interface is generally implemented Explicity.
    /// </para>
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Implemented by <see cref = "ObjectWrapperBase{TWrappedEntity}" />.
    ///   </para>
    /// </remarks>
    /// <typeparam name = "TWrappedEntity"></typeparam>
    public interface IHasInnerItem<out TWrappedEntity> :
        IHasInnerItemReadOnly
    /* where TWrappedEntity : class -- NO -- TOO LIMITING */
    {
        /// <summary>
        /// Sets the wrapped object.
        /// <para>
        /// Defined in <see cref="IHasInnerItem{T}"/>
        /// </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Property is intended to be implemented explicitly:
        ///     <code>
        ///       <![CDATA[
        /// public class EntityWrapper {
        ///   ...
        ///   WrappedEntity IEntityWrapper<WrappedEntity>.WrappedEntity {
        ///      get { return _WrappedEntity; } 
        ///   }
        ///   ...
        /// }
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        void SetInnerObject(object o);
    }
}