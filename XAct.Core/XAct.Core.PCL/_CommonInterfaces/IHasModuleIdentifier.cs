namespace XAct
{
    public interface IHasModuleIdentifier : IHasModuleIdentifierReadOnly
    {
        new string ModuleIdentifier { get; set; }
    }
}