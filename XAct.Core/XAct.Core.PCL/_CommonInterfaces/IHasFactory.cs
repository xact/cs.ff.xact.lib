﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract for a class that provides a Factory (implements <see cref="IHasCreates{TAggregateEntity}"/>.
    /// <para>
    /// Used by <c>System.Data.Repositories.IRepositoryExService</c>
    /// </para>
    /// </summary>
    /// <typeparam name="TAggregateEntity">The type of the aggregate entity.</typeparam>
    public interface IHasFactory<TAggregateEntity>
    {
        /// <summary>
        /// The factory for this Entity.
        /// <para>
        /// Note that the factory will have been DI'ed into the repository
        /// via the constructor.
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasFactory{TValue}"/></para>
        /// </summary>
        IHasCreates<TAggregateEntity> Factory { get; }
    }
}
