﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an object that is described.
    /// </summary>
    public interface IHasDescription : IHasDescriptionReadOnly
    {
        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly"/></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        new string Description { get; set; }
    }
}
