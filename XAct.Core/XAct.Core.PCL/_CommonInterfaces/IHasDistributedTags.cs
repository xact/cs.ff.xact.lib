﻿namespace XAct
{
    using System.Collections.Generic;
    using XAct.Categorization;

    /// <summary>
    /// Contract to apply to Models that can have zero or more Tags
    /// associated to them.
    /// </summary>
    public interface IHasDistributedTags:IHasDistributedGuidIdReadOnly
    {
        /// <summary>
        /// Gets the collection of optional tags associated to this object.
        /// </summary>
        ICollection<Tag> Tags { get; set; }
    }

    /// <summary>
    /// Contract to apply to Models that can have zero or more Tags
    /// associated to them.
    /// </summary>
    public interface IHasDistributedTagsReadOnly
    {
        /// <summary>
        /// Gets the collection of optional tags associated to this object.
        /// </summary>
        ICollection<Tag> Tags { get; }
    }
}
