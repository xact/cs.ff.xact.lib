﻿
namespace XAct
{
    /// <summary>
    /// Contract for element that has coordinates.
    /// </summary>
    public interface IHasIntCoordinate
    {
        /// <summary>
        /// Gets or sets the X axis value.
        /// </summary>
        int X { get; set; }
        /// <summary>
        /// Gets or sets the Z axis value.
        /// </summary>
        int Y { get; set; }
        /// <summary>
        /// Gets or sets the Z axis or layer value.
        /// </summary>
        int Z { get; set; }
    }
}
