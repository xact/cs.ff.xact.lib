﻿namespace XAct
{
    using XAct.Domain;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// The contract for Domain entities that need to be tracked
    /// (this is required for the implement of a Poor-Man's Repository)
    /// </summary>
    /// <remarks>
    /// <para>
    /// Used to keep track of the state of Entities (or other objects)
    /// as they retrieved from a Repository, and optionally changed, needing a subsequent save.
    /// </para>
    /// <para>
    /// Note that the interface is rather course, as it only 
    /// defines the whole entity as changed -- not 
    /// the specific properties.
    /// </para>
    /// </remarks>
    public interface IHasModelState
    {
        /// <summary>
        /// Gets the <see cref="ModelState"/> of the object.
        /// <para>
        /// Model states will be:
        /// <code>
        /// <![CDATA[
        /// * Undefined
        /// * Unchanged
        /// * New
        /// * Modified
        /// * Deleted
        /// ]]>
        /// </code>
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasModelState"/></para>
        /// </summary>
        OfflineModelState ModelState { get; set; }
    }
}