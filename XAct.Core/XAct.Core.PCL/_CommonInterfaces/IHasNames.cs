namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasNames
    {
        /// <summary>
        /// Gets the Title (Mr, Mrs, Ms, Dr., etc.)
        /// </summary>
        string Title { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the more names.
        /// </summary>
        /// <value>
        /// The more names.
        /// </value>
        string MoreNames { get; set; }
        
        /// <summary>
        /// Gets or sets the name of the sur.
        /// </summary>
        /// <value>
        /// The name of the sur.
        /// </value>
        string SurName { get; set; }
    }
}