﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// The contract for any object that has a name.
    /// <para>
    /// A Name is considered as being an immutable text identifier of an element.
    /// </para>
    /// <para>
    /// It's very much like <see cref="IHasKey"/>, although <see cref="IHasKey"/> is generally used only within collections.
    /// </para>
    /// <para>
    /// See also: <see cref="IHasText"/> (for mutable data).
    /// </para>
    /// </summary>
    public interface IHasName  : IHasNameReadOnly
    {

        /// <summary>
        /// Gets the name of the object.
        /// <para>
        /// The Name is a displayable text identifier.
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        new string Name { get; set; }
    }
}