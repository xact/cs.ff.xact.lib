﻿namespace XAct
{
    /// <summary>
    /// Contract for interfaces that report on test results.
    /// </summary>
    public interface IHasResultStatus : IHasResultStatusReadOnly
    {
        /// <summary>
        /// Get the result of a remote test.
        /// </summary>
        new ResultStatus Status { get; set; }
    }

    /// <summary>
    /// Contract for interfaces that report on test results.
    /// </summary>
    public interface IHasResultStatusReadOnly
    {
        /// <summary>
        /// Get the result of a remote test.
        /// </summary>
        ResultStatus Status { get; }
    }
}
