namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a message.
    /// </summary>
    public interface IHasSubjectAndBody
    {
        /// <summary>
        /// Gets or sets the message subject.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody"/> contract.
        /// </para>
        /// </summary>
        string Subject { get; set; }
        /// <summary>
        /// Gets or sets the message body.
        /// <para>
        /// Member defined in the <see cref="IHasSubjectAndBody"/> contract.
        /// </para>
        /// </summary>
        string Body { get; set; }
    }
}