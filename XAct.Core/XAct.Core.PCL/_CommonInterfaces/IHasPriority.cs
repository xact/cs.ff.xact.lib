﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an objec that has priority
    /// </summary>
    public interface IHasPriority : IHasPriorityReadOnly
    {
        /// <summary>
        /// Gets or sets the priority.
        /// <para>
        /// Member defined in the <see cref="IHasPriority"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        new Priority Priority { get; set; }
    }
}
