﻿namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasRenderingHintsReadOnly : IHasRenderingImageHintsReadOnly
    {
        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to group Settings together.
        /// </summary>
        string RenderGroupingHints { get; }


        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to determine the rendering order the within a Group.
        /// </summary>
        int RenderOrderHint { get;  }

    }
}