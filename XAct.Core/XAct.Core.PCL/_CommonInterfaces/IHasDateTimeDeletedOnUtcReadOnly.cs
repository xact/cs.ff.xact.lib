namespace XAct
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// Contract to provide a DeletedOn nullable DateTime property.
    /// </para>
    /// <para>
    /// Although most times recording the date at which a record was deleted
    /// is for Auditing, and therefore requires a "By" property,
    /// the DeletedOn date can be used for sync purposes only.
    /// </para>
    /// </summary>
    public interface IHasDateTimeDeletedOnUtcReadOnly
    {

        /// <summary>
        /// Gets or sets the datetime the item was deleted, expressed in UTC.
        /// <para>
        /// Defined in the <see cref="XAct.IHasDateTimeTrackabilityUtc"/> contract.
        /// </para>
        /// <para>
        /// Note: Generally updated by a db trigger, or saved first, then deleted.
        /// </para>
        /// </summary>
        /// <internal>
        /// <para>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see 
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        /// </para>
        /// </internal>
        DateTime? DeletedOnUtc { get;  }

    }
}