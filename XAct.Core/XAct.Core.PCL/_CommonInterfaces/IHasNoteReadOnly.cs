namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasNoteReadOnly
    {
        /// <summary>
        /// Gets the note.
        /// <para>
        /// Member defined in the <see cref="IHasNoteReadOnly"/> contract.
        /// </para>
        /// </summary>
        string Note { get; }
    }
}