﻿
namespace XAct
{
    using System;
    using XAct.Entities.Implementations;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract for objects that implement a WeakReference
    /// to another object.
    /// </summary>
    /// <remarks>
    /// See <see cref="WeakReferenceService{TOuterWrapper,TInnerObject}"/>
    /// </remarks>
    /// <typeparam name="TVar">The type of the var.</typeparam>
    public interface IHasWeakReferenced<TVar>
    {
        /// <summary>
        /// Gets the <see cref="WeakReference"/> to the object.
        /// <para>Member defined in<see cref="IHasWeakReferenced{TVar}"/></para>
        /// </summary>
        WeakReference WeakReference { get; }       
    }
}
