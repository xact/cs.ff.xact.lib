namespace XAct
{
    using System;

    public interface IHasPersistableClaim : IHasDistributedGuidIdAndTimestamp, IHasClaim
    {
        
        /// <summary>
        /// The FK of the entity that this Claim describes.
        /// </summary>
        Guid OwnerFK { get; set; }


    }
}