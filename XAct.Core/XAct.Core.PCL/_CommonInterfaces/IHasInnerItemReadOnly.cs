﻿

// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    ///   Contract for EntityWrappers.
    ///   <para>
    ///     Provides a way to get to the wrapped entity via an Explicitly defined interface.
    ///   </para>
    /// <para>
    /// Note: this interface is generally implemented Explicity.
    /// </para>
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Implemented by <see cref = "ObjectWrapperBase{TWrappedEntity}" />.
    ///   </para>
    /// </remarks>
    public interface IHasInnerItemReadOnly
    {
        /// <summary>
        /// Gets the wrapped object.
        /// <para>
        /// </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Property is intended to be implemented explicitly:
        ///     <code>
        ///       <![CDATA[
        /// public class EntityWrapper {
        ///   ...
        ///   WrappedEntity IEntityWrapper.GetInnerItem<WrappedEntity>()
        ///       return _wrappedEntity.ConvertTo<WrappedEntity>(); 
        ///   }
        ///   ...
        /// }
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        TItem GetInnerItem<TItem>();


    }
}
