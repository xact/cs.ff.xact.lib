﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Interface for objects that can be marked as Dirty.
    /// </summary>
    public interface IHasDirtyFlag
    {
        /// <summary>
        /// Gets a value indicating whether the value has changed since constructed.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note that the Flag can only be <c>set;</c> to <c>true</c>.
        /// To reset the flag to <c>false</c>, use the <see cref="ResetDirty"/> method.
        /// </para>
        /// <para>Member defined in<see cref="IHasDirtyFlag"/></para>
        /// </remarks>
        /// <value><c>true</c> if this instance is dirty; otherwise, <c>false</c>.</value>
        bool IsDirty { get; set; }

        /// <summary>
        /// Resets the <see cref="IsDirty"/> flag to <c>false</c>.
        /// <para>Member defined in<see cref="IHasDirtyFlag"/></para>
        /// </summary>
        void ResetDirty();
    }
}