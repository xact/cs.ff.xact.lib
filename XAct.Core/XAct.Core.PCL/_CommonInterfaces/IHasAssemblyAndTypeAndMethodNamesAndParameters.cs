﻿namespace XAct
{
    using XAct.Commands;

    /// <summary>
    /// A Contract that is a specialization of 
    /// <see cref="ICommandMessage"/>
    /// in order to demonstrate how to perform a latebound method, 
    /// using the Command Pattern. 
    /// </summary>
    public interface IHasAssemblyAndTypeAndMethodNamesAndParameters : IHasAssemblyAndTypeAndMethodNames
    {
        /// <summary>
        /// Gets or sets the optional parameters required by the method.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        object[] Parameters { get; set; }
    }
}