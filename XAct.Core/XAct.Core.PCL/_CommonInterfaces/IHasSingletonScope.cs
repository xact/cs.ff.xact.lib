﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct
{
    public interface IHasNoBindingRequiredScope
    {
        
    }

    public interface IHasBindingScope : IHasLowBindingPriority
    {

    }

    public interface IHasSingletonBindingScope : IHasBindingScope
    {
    }

    public interface IHasWebThreadBindingScope : IHasBindingScope
    {
        
    }

    public interface IHasTransientBindingScope : IHasBindingScope
    {
        
    }


    public interface IHasXActLibService : IHasService
    {
        
    }

    public interface IHasService :IHasSingletonBindingScope
    {
        
    }

    public interface IHasXActLibServiceConfiguration : IHasServiceConfiguration
    {
        
    }

    /// <summary>
    /// <para>
    /// By default Test ServiceConfigurations are of Medium Priority, to superceed the default Low Priority 
    /// of <see cref="IHasService"/>
    /// </para>
    /// </summary>
    public interface IHasXActLibTestServiceConfiguration : IHasServiceConfiguration, IHasMediumBindingPriority
    {

    }



    
    public interface IHasServiceConfiguration : IHasSingletonBindingScope
    {

    }

    public interface IHasServiceState : IHasSingletonBindingScope
    {

    }
    public interface IHasXActLibServiceState : IHasServiceState, IHasLowBindingPriority
    {
        
    }

    public interface IHasLowBindingPriority : IHasBindingPriority { }
    public interface IHasMediumBindingPriority : IHasBindingPriority { }
    public interface IHasHighBindingPriority : IHasBindingPriority { }
    public interface IHasBindingPriority {}

    /// <summary>
    /// An instance type
    /// </summary>
    public interface IHasXActLibProcessor : IHasTransientBindingScope
    {
        
    }
}
