﻿namespace XAct
{
    public interface IHasTextAndTitleReadOnly : IHasTextReadOnly, IHasTitleReadOnly
    {

    }
}