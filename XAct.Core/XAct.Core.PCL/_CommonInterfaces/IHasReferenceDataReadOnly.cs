﻿namespace XAct
{

    /// <summary>
    /// Contract for persisting *mutable* reference data that supports the domain model.
    /// <para>
    /// For Immutable Reference Data, use <see cref="IHasCodedReferenceDataReadOnly{TId}"/>
    /// </para>
    /// <para>
    /// Note that the properties 'Title' and 'Text' were used rather
    /// than just 'Text', or 'Text' and 'Description',
    /// due to the intended use. If designing for Dropdowns only
    /// (UI's of the turn of the century), only 'Text' would have been
    /// required. Almost never 'Text' and 'Description' (windows ui's didn't usually
    /// show a tooltip per dropdown option.
    /// But take into account designing for tablets, and drop downs are
    /// now no longer used (too small for fingers) and instead full lists
    /// with Title and Textual Description are more often used.
    /// </para>
    /// <para>
    /// That said, `Text` and `Title` should be used in only the most trivial
    /// of POCs that will never require resource management.
    /// Instead, prefer setting
    /// <see cref="IHasUseResourceFilterAndKeyReadOnly.UseResourceFilterAndKey" />
    /// to true, and using `ResourceFilter` and `ResourceKey` properties
    /// in conjunction with
    /// <c>XAct.Resources.IResourceService</c> to generate a culture specific
    /// representation of the reference data.
    /// </para>
    /// <para>
    /// NOTE: This contract does not implement <see cref="IHasCode"/>.
    /// See <see cref="IHasCodedReferenceDataReadOnly{TId}"/>.
    /// </para>
    /// </summary>
    /// <typeparam name="TId">The type of the datastore identifier.</typeparam>
    public interface IHasReferenceDataReadOnly<TId> :
        IHasIdReadOnly<TId>,
        IHasReferenceDataReadOnly //The datastore Id for refential identity.
        where TId : struct
    {

    }


    /// <summary>
    /// Contract for persisting reference data that supports the domain model.
    /// <para>
    /// Examples are `Gender`, `PostageType`, etc.
    /// </para>
    /// <para>
    /// Note that the properties 'Title' and 'Text' were used rather
    /// than just 'Text', or 'Text' and 'Description',
    /// due to the intended use. If designing for Dropdowns only
    /// (UI's of the turn of the century), only 'Text' would have been
    /// required. Almost never 'Text' and 'Description' (windows ui's didn't usually
    /// show a tooltip per dropdown option.
    /// But take into account designing for tablets, and drop downs are
    /// now no longer used (too small for fingers) and instead full lists
    /// with Title and Textual Description are more often used.
    /// </para>
    /// <para>
    /// That said, `Text` and `Title` should be used in only the most trivial
    /// of POCs that will never require resource management.
    /// Instead, prefer setting
    /// <see cref="IHasUseResourceFilterAndKeyReadOnly.UseResourceFilterAndKey" />
    /// to true, and using `ResourceFilter` and `ResourceKey` properties
    /// in conjunction with
    /// <c>XAct.Resources.IResourceService</c> to generate a culture specific
    /// representation of the reference data.
    /// </para>
    /// </summary>
    public interface IHasReferenceDataReadOnly : 
        IHasEnabledReadOnly,
        IHasOrderReadOnly,
        IHasResourceFilterReadOnly,
        IHasTextAndDescriptionReadOnly,
        IHasTagReadOnly,
        IHasFilterReadOnly

    {

    }
}