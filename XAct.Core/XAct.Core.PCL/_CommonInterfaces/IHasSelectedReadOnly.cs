namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// Contract used for <c>ISelectableItem</c>
    /// and other similar contracts.
    /// </para>
    /// </summary>
    public interface IHasSelectedReadOnly
    {
        /// <summary>
        /// Gets a value indicating whether this item is selected.
        /// <para>
        /// Member defined in the <see cref="IHasSelectedReadOnly"/> contract.
        /// </para>
        /// </summary>
        /// <value><c>true</c> if selected; otherwise, <c>false</c>.</value>
        bool Selected { get;  }
    }
}