namespace XAct
{
    /// <summary>
    /// Contract to indicate which element in a collection is the preferred value.
    /// </summary>
    public interface IHasPreferred
    {
        /// <summary>
        /// Gets or sets a flag indicating whether this is the preferred option.
        /// </summary>
        bool Preferred { get; set; }
    }
}