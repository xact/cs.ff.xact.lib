﻿namespace XAct
{
    /// <summary>
    /// Gets or sets the Quantity of this Item.
    /// </summary>
    public interface IHasQuantity
    {
        decimal Quantity { get; set; }
    }
}