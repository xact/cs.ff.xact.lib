﻿namespace XAct
{
    /// <summary>
    /// Each reply message should contain a Correlation Identifier, 
    /// a unique identifier that indicates which request message this reply is for.
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IHasCorrelationId<TId> : IHasCorrelationIdReadOnly <TId>
    {
        new TId CorrelationId { get; set; }
        
    }
}