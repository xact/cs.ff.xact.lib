namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for unique integer identifiers
    /// that can be shared across networks in a peer to peer 
    /// manner.
    /// <para>
    /// The issue is ...mobiles. We're back to the 60's era of
    /// servers and clients, that need synchronisation. But Guids
    /// create random insertions, so are expensive to index.
    /// Sequential Guids (MS only) cause conflict in a P2P environment.
    /// </para>
    /// <para>
    /// A two part key allows for the creation of records 
    /// on various machines, without conflict, while staying better
    /// sorted than Guids.
    /// </para>
    /// </summary>
    public interface IHasDistributedIdentitiesReadOnly: IHasMachineIdReadOnly , IHasIdReadOnly<int>
    {

    }
}