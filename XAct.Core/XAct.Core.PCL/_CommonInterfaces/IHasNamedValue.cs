﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Interface to define read/write (get/set) Key/Values.
    /// </summary>
    /// <remarks>
    /// Was used in <c>XAct.Net.Messaging</c>
    /// to define contracts for Message parts.
    /// </remarks>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IHasNamedValue<TValue> : IHasNamedValueReadOnly<TValue> , IHasValue<TValue>
    {

        /// <summary>
        /// Gets or sets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasNamedValue{TValue}"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        new string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// <para>Member defined in<see cref="XAct.IHasNamedValue{TValue}"/></para>
        /// </summary>
        /// <value>The value.</value>
        new TValue Value { get; set; }

    }
}
