namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an object that is orderable.
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasOrderReadOnly
    {
        /// <summary>
        /// Gets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrderReadOnly"/>.
        /// </para>
        /// </summary>
        int Order { get; }
    }
}