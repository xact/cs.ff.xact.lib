﻿namespace XAct
{
    /// <summary>
    /// Id of parent/owner object.
    /// <para>
    /// Reserve the use of
    /// <see cref="IHasOwnerFK{T}"/>
    /// for Non-Hierarchical scenarios (eg: when an object is managing a collection of attributes).
    /// </para>
    /// </summary>
    public interface IHasParentFKReadOnly<TId>
    {
        TId ParentFK { get; }
    }
}