﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a clas that creates instances (ie, a factory).
    /// <para>
    /// See also <see cref="IHasCreateCollection{TInstanceType}"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TInstanceType">The type of the instance type.</typeparam>
    public interface IHasCreate<TInstanceType>
    {
        /// <summary>
        /// Creates a single instance.
        /// <para>
        ///     <example>
        ///       <code>
        ///         <![CDATA[
        /// //Avoid direct instantiation/tight coupling:
        /// //IContact contact = new Contact();
        /// //in preference for instantiation via a factory:
        /// IContact contact = _contactRepository.CreateInstance();
        /// contact.First = "John"; //etc...
        /// ]]>
        ///       </code>
        ///     </example>
        /// </para>
        /// <para>Member defined in<see cref="IHasCreate{TInstanceType}"/></para>
        /// </summary>
        /// <returns></returns>
        TInstanceType CreateInstance();
    }
}
