﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// 
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IHasKeyValue<TValue> :IHasKey, IHasValue<TValue>
    {

    }
}
