﻿//// ReSharper disable CheckNamespace
//namespace XAct
//// ReSharper restore CheckNamespace
//{
//    [Obsolete]
//    public interface IHasTitleResourceFilterAndKeyReadOnly : IHasResourceFilter
//    {
//        /// <summary>
//        /// Gets the resource key for the title.
//        /// </summary>
//        /// <value>
//        /// The title resource key.
//        /// </value>
//        string TitleResourceKey { get; }
//    }
//}