﻿
namespace XAct
{
    


    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a simple Command Function (not Action), providing an
    /// Execute method that returns an argument.
    /// </summary>
    public interface IHasExecutableFunction<out TResult>
    {
        /// <summary>
        /// Executes the operation.
        /// <para>Member defined in<see cref="IHasExecutableFunction{TResult}"/></para>
        /// </summary>
        TResult Execute();
    }

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a simple Command Function (not Action), providing an
    /// Execute method takes an argument, and returns an argument.
    /// </summary>
    public interface IHasExecutableFunction<out TResult, in TArg>
    {
        /// <summary>
        /// Executes the operation, taking an argument.
        /// <para>Member defined in<see cref="IHasExecutableFunction{TResult,TArg}"/></para>
        /// </summary>
        /// <param name="argument">The argument.</param>
        /// <returns></returns>
        TResult Execute(TArg argument);
    }


    /// <summary>
    ///   <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a simple Command Function (not Action), providing an
    /// Execute method takes an argument, and returns an argument.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <typeparam name="TArg">The type of the argument.</typeparam>
    /// <typeparam name="TArg2">The type of the arg2.</typeparam>
    public interface IHasExecutableFunction<out TResult, in TArg, TArg2>
    {
        /// <summary>
        /// Executes the operation, taking an argument.
        /// <para>Member defined in<see cref="IHasExecutableFunction{TResult,TArg}" /></para>
        /// </summary>
        /// <param name="argument">The argument.</param>
        /// <param name="output2">The argument2.</param>
        /// <returns></returns>
        TResult Execute(TArg argument, ref TArg2 output2);
    }

}
