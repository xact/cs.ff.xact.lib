namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Interface for objects that point to a related datasource.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The whole goal is to offer some kind of record (eg: Vertex) a way
    /// to describe its source:
    /// <code>
    /// <![CDATA[
    /// public Vertex : IDataSourceIdentified {
    ///    ...
    ///    public VertexDataSource DataSource {get;set}
    /// }
    /// public VertexDataSource : IDataSourceIdentifier {
    ///   ...
    /// } 
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public interface IHasDataSourceIdentified
    {
        /// <summary>
        /// Gets the identifier that specifies the source the data object.
        /// <para>Member defined in<see cref="IHasDataSourceIdentified"/></para>
        /// </summary>
        IHasDataSourceIdentifier DataSource { get; }
    }


}


