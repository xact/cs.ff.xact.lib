namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// A contract to provide information as to when 
    /// a Record was deleted, and by whom.
    /// </para>
    /// <para>
    /// See also <see cref="IHasAuditability"/>.
    /// </para>
    /// </summary>
    public interface IHasDateTimeDeletedByReadOnly : IHasDateTimeDeletedOnUtcReadOnly
    {
        /// <summary>
        /// Gets information as to whom deleted the record.
        /// <para>
        /// See also <see cref="IHasAuditability"/>.
        /// </para>
        /// <para>Member defined in<see cref="IHasDateTimeDeletedByReadOnly"/></para>
        /// </summary>
        /// <value>The deleted by.</value>
        string DeletedBy { get; }
    }
}