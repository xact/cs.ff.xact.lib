﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasNameAndDescription: IHasName,IHasDescription , IHasNameAndDescriptionReadOnly
    {

        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        new string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription"/></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        new string Description { get; set; }

    }
}
