﻿namespace XAct
{
    public interface IHasApplicationTennantIdSpecificDistributedGuidId :
        IHasDistributedGuidIdAndTimestamp,
        IHasApplicationTennantId
    {
        
    }
}