namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a simple Command Action (not Function), providing an
    /// Execute method that returns void.
    /// </summary>
    public interface IHasExecutableAction
    {
        /// <summary>
        /// Executes the operation.
        /// <para>Member defined in<see cref="IHasExecutableAction"/></para>
        /// </summary>
        void Execute();
    }

    /// <summary>
    /// Contract for a simple Command Action (not Function), providing an
    /// Execute method that returns void.
    /// </summary>
    public interface IHasExecutableAction<in TArg>
    {
        /// <summary>
        /// Executes the operation, without returning an argument.
        /// <para>Member defined in<see cref="IHasExecutableAction{TArg}"/></para>
        /// </summary>
        void Execute(TArg argument);
    }
}