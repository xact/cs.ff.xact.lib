﻿namespace XAct
{
    /// <summary>
    /// Contract for objects that have a Start and Stop method.
    /// <see cref="IHasStop"/>
    /// </summary>
    public interface IHasStartStop : IHasStart, IHasStop
    {
        
    }
}