﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// The contract for any object that has a readonly Tag.
    /// <para>
    /// Useful for Filtering objects later.
    /// </para>
    /// </summary>
    public interface IHasTag: IHasTagReadOnly
    {
        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag"/></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        new string Tag { get; set; }
    }


}