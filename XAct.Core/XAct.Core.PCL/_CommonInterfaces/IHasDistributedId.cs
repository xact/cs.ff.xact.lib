﻿namespace XAct
{
    public interface IHasDistributedId<TId> : IHasDistributedIdReadOnly<TId>, IHasId<TId>
        where TId : struct
    {
    }
}