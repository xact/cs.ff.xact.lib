﻿

namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public interface IHasItem<TItem> : IHasItemReadOnly<TItem>

    {
        /// <summary>
        /// Gets or sets the item.
        /// <para>Member defined in<see cref="IHasItem{TItem}"/></para>
        /// </summary>
        /// <value>
        /// The item.
        /// </value>
        new TItem Item { get; set; }
    }
}
