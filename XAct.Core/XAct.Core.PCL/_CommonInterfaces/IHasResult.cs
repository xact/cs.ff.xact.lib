﻿
namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract for a object that contains a generic Result property.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public interface IHasResult<out TResult>
    {
        /// <summary>
        /// A Read only result.
        /// <para>
        /// Member defined in the <see cref="IHasResult{TResult}"/> contract.
        /// </para>
        /// </summary>
        TResult Result { get; }
    }
}
