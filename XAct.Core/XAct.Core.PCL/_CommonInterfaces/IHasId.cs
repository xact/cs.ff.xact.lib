﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    ///   <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// The contract for an entity that has an Identity (Id) value.
    /// </summary>
    /// <typeparam name="TId">The type of the id.</typeparam>
    /// <internal>8/9/2011: Sky</internal>
    public interface IHasId<TId> : IHasIdReadOnly<TId>
        //where TId : struct
    {
        /// <summary>
        /// Gets the Entity's datastore Id.
        /// <para>
        /// Member defined in <see cref="XAct.IHasId{TId}"/>.
        /// </para>
        /// </summary>
        new TId Id { get; set; }
    }

}