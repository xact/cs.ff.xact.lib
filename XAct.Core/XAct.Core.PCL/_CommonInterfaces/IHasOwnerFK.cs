﻿namespace XAct
{
    /// <summary>
    /// FK of owner record.
    /// <para>
    /// Reserve the use of
    /// <see cref="IHasParentFK{T}"/>
    /// for Hierarchical scenarios.
    /// </para>
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IHasOwnerFK<TId> : IHasOwnerFKReadOnly<TId>
    {
        new TId OwnerFK { get; }
    }
}