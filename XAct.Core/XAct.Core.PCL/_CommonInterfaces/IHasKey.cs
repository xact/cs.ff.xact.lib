namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// The name of an element within a Collection (see Key/Value Collections).
    /// <para>
    /// A Key is considered immutable.
    /// </para>
    /// <para>
    /// See also: <see cref="IHasName"/>
    /// </para>
    /// </summary>
    public interface IHasKey : IHasKeyReadOnly
    {

        /// <summary>
        /// Gets or sets the key.
        /// <para>Member defined in<see cref="IHasKey"/></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        new string Key { get; set; }
    }
}