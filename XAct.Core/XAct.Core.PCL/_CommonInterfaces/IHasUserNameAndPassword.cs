﻿
namespace XAct.Authentication
{
    /// <summary>
    /// A contract for a set of classic authentication settings.
    /// </summary>
    /// <remarks>
    /// So common, that it should be a common interface.
    /// </remarks>
    public interface IHasUserNameAndPassword : IHasUserNameAndPasswordReadOnly
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        new string Name { get; set; }

        /// <summary>
        /// Gets or sets the user password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        new string Password { get; set; }
    }

}
