namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for Entities that are bound to a specific Application.
    /// <para>
    /// Used by <c>Resource</c> entries, <c>HistoryEntry</c> entities, etc.
    /// </para>
    /// </summary>
    public interface IHasApplicationIdentifierReadOnly
    {
        /// <summary>
        /// Gets a string identifier (name, guid, other) specifying the Application this Entity belong to.
        /// <para>Member defined in<see cref="IHasApplicationIdentifierReadOnly"/></para>
        /// <para>
        /// See <see cref="IHasOrganisationIdentifierReadOnly"/>
        /// </para>
        /// </summary>
        string ApplicationIdentifier { get; }
    }
}