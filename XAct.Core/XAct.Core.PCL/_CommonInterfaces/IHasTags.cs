﻿using System.Collections.Generic;

namespace XAct
{
    using XAct.Categorization;

    public interface IHasTagsReadOnly
    {
        ICollection<Tag> Tags { get; } 
    }

}
