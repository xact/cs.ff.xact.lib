﻿namespace XAct
{
    /// <summary>
    /// Contract for Tags to associate to other items 
    /// (eg: Message entities, etc.)
    /// </summary>
    /// <typeparam name="T">Type of Id (int, Guid)</typeparam>
    public interface IHasTag<TId> : IHasId<TId>, IHasEnabled, IHasOrder, IHasName
        where TId : struct
    {
        
    }
}