namespace XAct
{
    /// <summary>
    /// 
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// </summary>
    public interface IHasUserIdentifier : IHasUserIdentifierReadOnly
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// <para>
        /// Member defined in the <see cref="IHasUserIdentifier"/> contract.
        /// </para>
        /// </summary>
        /// <value>The user identifier.</value>
        new string UserIdentifier { get; set; }
    }
}