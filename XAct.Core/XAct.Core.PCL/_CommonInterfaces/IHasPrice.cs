﻿namespace XAct
{
    /// <summary>
    /// Contract for elements that have a price
    /// </summary>
    public interface IHasPrice
    {
        /// <summary>
        /// Gets or sets the item's list Price.
        /// </summary>
        decimal Price { get; set; }
    }
}