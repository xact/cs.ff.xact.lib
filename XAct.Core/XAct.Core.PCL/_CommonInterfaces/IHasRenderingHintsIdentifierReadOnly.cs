﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    public interface IHasRenderingHintsIdentifierReadOnly
    {
        /// <summary>
        /// Gets the Identifier to an Entity Record 
        /// (that implements a contract similar to <see cref="IHasSettingRenderingHints"/>)
        /// describing how to render the 
        /// <c>Setting</c> or other similar entity.
        /// <para>
        /// IMPORTANT: 
        /// If this entity implements both <see cref="IHasRenderingHintsIdentifierReadOnly"/>
        /// and <see cref="IHasSettingRenderingHintsReadOnly"/> (probably for convenvience/faster developement)  
        /// one sets *either* the <see cref="RenderHintsIdentifier"/>
        /// *OR* the Group/Order/Label/ViewControl/EditControl/EditValidation
        /// values -- not both.
        /// </para>
        /// </summary>
        /// <value>
        /// The render hints identifier.
        /// </value>
        string RenderHintsIdentifier { get; }
        
    }
}