// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IHasKeyValueReadOnly<TValue>:IHasKeyReadOnly,IHasValueReadOnly<TValue>
    {
        
    }
}