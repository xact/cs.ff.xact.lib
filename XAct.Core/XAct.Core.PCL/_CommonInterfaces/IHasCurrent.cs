﻿

namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for retrieving the current Context.
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public interface IHasCurrent<TContext>
    {
        /// <summary>
        /// Gets or sets the current context object.
        /// <para>Member defined in<see cref="IHasCurrent{TContext}"/></para>
        /// </summary>
        /// <value>The context.</value>
        TContext Current
        {
            get; set;
        }
    }
}
