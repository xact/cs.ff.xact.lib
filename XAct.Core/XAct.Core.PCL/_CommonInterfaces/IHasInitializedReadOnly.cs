﻿namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasInitializedReadOnly
    {
        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="IHasInitialize"/>.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        bool Initialized { get; }
    }
}