﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{

    public interface IHasRenderingHints : IHasRenderingImageHints, IHasRenderingHintsReadOnly
    {
        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to group Settings together.
        /// </summary>
        new string RenderGroupingHints { get; set; }


        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to determine the rendering order the within a Group.
        /// </summary>
        new int RenderOrderHint { get; set; }


        
    }
}