namespace XAct
{
    using XAct.Commands;

    /// <summary>
    /// A Contract that is a specialization of 
    /// <see cref="ICommandMessage"/>
    /// in order to demonstrate how to perform a latebound method, 
    /// using the Command Pattern.
    /// <para>
    /// Also used by <c>XAct.Scheduling</c>
    /// </para> 
    /// </summary>
    public interface IHasAssemblyAndTypeAndMethodNames : IHasAssemblyAndTypeNames, IHasMethodName
    {
    }
}