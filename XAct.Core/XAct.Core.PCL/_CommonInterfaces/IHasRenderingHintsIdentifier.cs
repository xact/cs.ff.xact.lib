﻿//// ReSharper disable CheckNamespace
//namespace XAct
//// ReSharper restore CheckNamespace
//{
//    public interface IHasRenderingHintsIdentifier : IHasRenderingHintsIdentifierReadOnly
//    {
//        /// <summary>
//        /// Gets the Identifier to an Entity/Record
//        /// (that implements a contract similar to <see cref="IHasRenderingHints"/>)
//        /// describing how to render the 
//        /// <c>Setting</c> or other similar entity.
//        /// <para>
//        /// IMPORTANT: 
//        /// If this entity implements both <see cref="IHasRenderingHintsIdentifier"/>
//        /// and <see cref="IHasRenderingHints"/> (probably for convenvience/faster developement)  
//        /// one sets *either* the <see cref="RenderHintsIdentifier"/>
//        /// *OR* the Group/Order/Label/ViewControl/EditControl/EditValidation
//        /// values -- not both.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The render hints identifier.
//        /// </value>
//        new string RenderHintsIdentifier { get; set; }

//    }
//}