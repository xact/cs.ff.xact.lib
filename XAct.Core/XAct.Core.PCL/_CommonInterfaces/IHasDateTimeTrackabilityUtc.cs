﻿namespace XAct
 
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Interface to implement date tracking on an Item.
    /// <para>
    /// See also <see cref="IHasAuditability"/>.
    /// </para>
    /// </summary>
    public interface IHasDateTimeTrackabilityUtc : IHasDateTimeTrackabilityUtcSimple, IHasDateTimeDeletedOnUtc
    {



        ///// <summary>
        ///// Gets or sets the datetime this entity was last been accessed (viewed), expressed in UTC
        ///// <para>
        ///// Defined in the <see cref="XAct.Domain.IDateTimeTrackable"/> contract.
        ///// </para>
        ///// </summary>
        ///// <remarks>
        ///// This field is not used/updated right now.
        ///// It could be useful for automatically cleaning up the local DB
        ///// based on the last time the GraphVertex has been extracted ...
        ///// ... but implementing this means that we must do an SQL UPDATE
        ///// every time we extract a vertex from the DB  -- which brings with it
        ///// performance issues ...
        ///// </remarks>
        //DateTime? DateTimeLastAccessedOn { get; set; }




    }
}