﻿namespace XAct
{
    /// <summary>
    /// Contract for objects that have a Stop method.
    /// <see cref="IHasStart"/>
    /// </summary>
    public interface IHasStop
    {

    }
}