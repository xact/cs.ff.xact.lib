﻿
namespace XAct
{
    /// <summary>
    /// Contract to mark an object as excluded from a set.
    /// </summary>
    public interface IHasExcluded
    {
        /// <summary>
        /// Gets or sets a value indicating whether the current object is Excluded.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [excluded]; otherwise, <c>false</c>.
        /// </value>
        bool Excluded { get; set; }
    }
}
