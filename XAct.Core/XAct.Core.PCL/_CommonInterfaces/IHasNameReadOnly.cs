﻿

// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// The contract for any object that has a readonly name.
    /// <para>
    /// A Name is considered immutable.
    /// </para>
    /// <para>
    /// See also: <see cref="IHasKey"/>
    /// </para>
    /// <para>
    /// See also: <see cref="IHasText"/> (for mutable data).
    /// </para>
    /// </summary>
    public interface IHasNameReadOnly
    {
        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasNameReadOnly"/></para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>        
        string Name { get; }
    }
}
