﻿//namespace XAct
//{
//    using System;

//    /// <summary>
//    /// The contract for a ReferenceData entity (with Text and Description) that uses 
//    /// <c>XAct.Resources.IResourceService, XAct.Resources</c>
//    /// to render its Text and optional description within a list of options.
//    /// <para>
//    /// An example usage would be:
//    /// <code>
//    /// <![CDATA[
//    /// public class IGenderType : IHasId<Id>, IHasName, IHasTextAndDescriptionResourceFilterAndKeys {
//    /// ...
//    /// }
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// </summary>
//    [Obsolete]
//    public interface IHasTextAndDescriptionResourceFilterAndKeyReadOnly : IHasResourceFilterAndKeyReadOnly
//    {
//        /// <summary>
//        /// The Key to the Description Resource in the 
//        /// <c>XAct.Resources.IResourceService, XAct.Resources</c>.
//        /// </summary>
//        string DescriptionResourceKey { get; }   
//    }

//}