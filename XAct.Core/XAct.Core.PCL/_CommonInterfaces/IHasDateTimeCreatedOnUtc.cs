﻿namespace XAct
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Interface to implement date tracking on an Item.
    /// <para>
    /// See also <see cref="IHasAuditability"/>.
    /// </para>
    /// </summary>
    public interface IHasDateTimeCreatedOnUtc : IHasDateTimeCreatedOnUtcReadOnly
    {
        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc"/>.</para>
        /// </summary>
        /// <internal>
        /// <para>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see 
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        /// </para>
        /// </internal>
        /// <internal>
        /// As to why its Nullable: sometimes the contract is applied to items
        /// that are not Entities themselves, but pointers to objects that are not known
        /// if they are 
        /// </internal>
        /// <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        /// </internal>
        new DateTime? CreatedOnUtc { get; set; }
        
    }
}