﻿namespace XAct
{
    public interface IHasResourceFilterReadOnly 
    { 
        /// <summary>
        /// Gets the Filter for the Resource in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        string ResourceFilter { get; }
        
    }
}