﻿namespace XAct
{
    /// <summary>
    /// Contract for entities that have unique codes (eg: Product, etc.)
    /// </summary>
    public interface IHasCode: IHasCodeReadOnly
    {
        /// <summary>
        /// The unique textual code (eg: 'X123') for this object.
        /// </summary>
        new string Code { get; set; }
    }
}