﻿namespace XAct
{
    using System;

    /// <summary>
    /// 
    /// <para>
    ///   <see cref="IHasApplicationTennantId"/>
    ///   <see cref="IHasApplicationTennantIdSpecificReferenceData{TId}"/>
    ///   <see cref="IHasApplicationTennantIdSpecificDistributedGuidIdReferenceData"/>
    /// </para>
    /// </summary>
    public interface IHasApplicationTennantIdSpecificDistributedGuidIdReferenceData :
        IHasApplicationTennantIdSpecificDistributedGuidId,
        IHasApplicationTennantIdSpecificReferenceData<Guid>,
        IHasDistributedGuidIdAndTimestamp
    {

    }
}