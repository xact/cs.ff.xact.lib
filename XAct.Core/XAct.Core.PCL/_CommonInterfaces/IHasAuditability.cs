﻿
// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract for Domain elements that
    /// are Auditable.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usable on Domain Identity and Value objects
    /// to keep track of who did what changes.
    /// </para>
    /// <para>
    /// Often used with in conjunction with Db triggers
    /// which record the changes in an AuditTable (ie, 
    /// Changes to a Contact item trigger a subsequent 
    /// change in a Contact_Audit table.
    /// </para>
    /// <para>
    /// Note that I have great reservations about using 
    /// these fields in Domain object, as they are not 
    /// within the definition of the Domain entity, but more
    /// of a Application requirement.
    /// </para>
    /// <para>
    /// But the speed gains from not hitting the db 
    /// twice (object, then audit information)
    /// is with current technology, appears to be a necessary evil.
    /// </para>
    /// </remarks>
    public interface IHasAuditability : IHasAuditabilitySimple, IHasDateTimeDeletedBy
        
    {
        ///// <summary> 
        ///// Date time stamp as to when the domain object was created. 
        ///// <para>Member defined in <see cref="XActs.Entities.IAuditable"/>.</para>
        ///// </summary> 
        //DateTime? CreatedOn { get; set; }

        ///// <summary> 
        ///// Identifier of the User who created the object. 
        ///// <para>Member defined in <see cref="XAct.IAuditable"/>.</para>
        ///// </summary> 
        //string CreatedBy { get; set; }

        ///// <summary> 
        ///// Date time stamp of when the domain object was last modified. 
        ///// <para>Member defined in <see cref="XAct.IAuditable"/>.</para>
        ///// </summary> 
        //DateTime? LastModifiedOn { get; set; }

        ///// <summary> 
        ///// The identifier for the user who last modified the domain object. 
        ///// <para>Member defined in <see cref="XAct.IAuditable"/>.</para>
        ///// </summary> 
        //new string LastModifiedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the date the item was deleted on.
        ///// <para>Member defined in <see cref="XAct.IAuditable"/>.</para>
        ///// </summary>
        ///// <value>
        ///// The deleted on.
        ///// </value>
        //DateTime? DeletedOn { get; set; }

        ///// <summary>
        ///// Gets the identifier for the user who deleted the object.
        ///// <para>Member defined in <see cref="XAct.IAuditable"/>.</para>
        ///// </summary>
        //new string DeletedBy { get; set; }
    }

    public interface IHasAuditabilitySimple :
        //IHasDateTimeTrackabilityUtcSimple, 
        IHasDateTimeCreatedBy, IHasDateTimeModifiedBy
    {

         
    }

}