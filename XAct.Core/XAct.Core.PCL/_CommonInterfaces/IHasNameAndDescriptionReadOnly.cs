﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// A contract for a read-only Name/Description
    /// <para>
    /// A lot of entities have this pair of properties.
    /// </para>
    /// </summary>
    public interface IHasNameAndDescriptionReadOnly
    {
        /// <summary>
        /// Gets or sets the name.
        /// <para>
        /// Member defined in the <see cref="IHasNameAndDescriptionReadOnly"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }

        /// <summary>
        /// Gets or sets the description.
        /// <para>
        /// Member defined in the <see cref="IHasNameAndDescriptionReadOnly"/> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        string Description { get; }
    }
}