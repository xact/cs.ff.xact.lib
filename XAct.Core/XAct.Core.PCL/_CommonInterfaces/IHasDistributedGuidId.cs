﻿namespace XAct
{
    using System;

    /// <summary>
    /// Contract for a Guid Id
    /// <para>
    /// Tip: Use <see cref="XAct.IDistributedIdService"/>
    /// </para>
    /// </summary>
    public interface IHasDistributedGuidId :  IHasDistributedGuidIdReadOnly, IHasDistributedId<Guid>
    {
        
    }
}