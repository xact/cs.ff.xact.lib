namespace XAct
{
    public interface IHasModuleIdentifierReadOnly
    {
        string ModuleIdentifier { get; }
    }
}