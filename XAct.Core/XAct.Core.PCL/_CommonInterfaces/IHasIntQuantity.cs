﻿namespace XAct
{
    /// <summary>
    /// The contract for an object that has a quantity (eg: LineItem)
    /// <para>
    /// See <see cref="IHasQuantity"/>
    /// </para>
    /// </summary>
    public interface IHasIntQuantity
    {
        /// <summary>
        /// The quantity.
        /// </summary>
        int Quantity { get; set; }
    }
}