namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for any element that has a Title that can be updated.
    /// </summary>
    public interface IHasTitle : IHasTitleReadOnly
    {
        /// <summary>
        /// Gets or sets the title.
        /// <para>
        /// Member defined in the <see cref="IHasTitle"/> contract.
        /// </para>
        /// </summary>
        new string Title { get; set; }
    }
}