﻿//namespace XAct
//{
//    /// <summary>
//    /// Contract to indicate whether rendering should
//    /// use the <see cref="IHasUseResourceFilterAndKeyReadOnly"/>
//    /// values, or fall back to an interim text value
//    /// embedded directly in the object.
//    /// <para>
//    /// Note that there is no need for a non-readonly 
//    /// contract as this field will only be set via 
//    /// resource seeding.
//    /// </para>
//    /// </summary>
//    public interface IHasUseResourceFilterAndKeyReadOnly : IHasResourceFilter
//    {
//        /// <summary>
//        /// Gets or sets a flag indicating whether 
//        /// to use the resource filter and key
//        /// to retrieve culture specific text or not.
//        /// </summary>
//        bool UseResourceFilterAndKey { get; }

//    }
//}