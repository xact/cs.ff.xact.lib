﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// <para>
    /// A contract to get or set information as to when 
    /// a Record was deleted, and by whom.
    /// </para>
    /// <para>
    /// See also <see cref="IHasAuditability"/>.
    /// </para>
    /// </summary>
    public interface IHasDateTimeDeletedBy : IHasDateTimeDeletedByReadOnly, IHasDateTimeDeletedOnUtc
    {
        /// <summary>
        /// Gets or sets information as to whom deleted the record.
        /// <para>Member defined in<see cref="IHasDateTimeDeletedBy"/></para>
        /// </summary>
        /// <para>
        /// See also <see cref="IHasAuditability"/>.
        /// </para>
        /// <value>The deleted by.</value>
        new string DeletedBy { get; set; }

    }
}
