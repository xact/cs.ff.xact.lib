﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for a Readonly (get only) Named Value.
    /// </summary>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    public interface IHasNamedValueReadOnly<TValue> : IHasName, IHasValueReadOnly<TValue>
    {

        /// <summary>
        /// Gets the value.
        /// <para>Member defined in<see cref="IHasNamedValueReadOnly{TValue}"/></para>
        /// </summary>
        /// <value>The value.</value>
        new TValue Value { get; }

    }
}