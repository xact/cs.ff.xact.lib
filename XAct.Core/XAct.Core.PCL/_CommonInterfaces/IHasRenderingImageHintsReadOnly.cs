﻿namespace XAct
{
    public interface IHasRenderingImageHintsReadOnly
    {
        /// <summary>
        /// Gets a hint as to the icons to use if the full item cannot be rendered.
        /// <para>
        /// Images could be for controls tucked away, or simply rendered beside/instead of labels.
        /// </para>
        /// </summary>
        string RenderingImageHints { get; }
    }
}