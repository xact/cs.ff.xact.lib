﻿
namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IHasHierarchicalKeyValueCollection<T> :IHasKeyValue<T>, IHasHierarchyCollection<IHasKeyValue<T>>
    {
    }
}
