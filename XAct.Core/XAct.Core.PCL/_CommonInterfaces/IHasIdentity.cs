namespace XAct
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TEntityAlias">The type of the entity alias.</typeparam>
    /// <typeparam name="TClaim">The type of the claim.</typeparam>
    public interface IHasIdentity<TEntity,TEntityAlias,TClaim> : IHasDistributedGuidIdAndTimestamp, IHasAuditability, IHasParentFK<Guid>
        where TEntityAlias : class, IHasDistributedGuidIdAndTimestamp, IHasNames, new()
        where TClaim : class, IHasDistributedGuidIdAndTimestamp, IHasPersistableClaim, new()
    {


        /// <summary>
        /// Gets or sets the parent entity (for an individual, that would be his Org/Work).
        /// </summary>
        /// <internal>
        /// This might not be satisfactory (graph versus hierarchy is probably more realistic).
        /// </internal>
        /// <value>
        /// The parent entity.
        /// </value>
        TEntity Parent { get; set; }

        /// <summary>
        /// A user may have more than one name.
        /// This is the preferred one.
        /// </summary>
        Guid? PreferredAliasFK { get; set; }


        /// <summary>
        /// The preferred name of the Identity
        /// amongst potential <see cref="Names"/>
        /// the identity is known as.
        /// </summary>
        TEntityAlias PreferredAlias { get; set; }


        /// <summary>
        /// The collection of aliases the entity 
        /// is known under.
        /// </summary>
        ICollection<TEntityAlias> Aliases { get; }



        /// <summary>
        /// Gets or sets the identity claims
        /// associated to this identity.
        /// <para>
        /// Only one will be used to track the user for tax purposes.
        /// </para>
        /// </summary>
        /// <value>
        /// The identity claims.
        /// </value>
        ICollection<TClaim> IdentityClaims { get; }

    }
}