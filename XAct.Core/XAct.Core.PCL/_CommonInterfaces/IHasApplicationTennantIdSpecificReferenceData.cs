﻿namespace XAct
{
    using System;



    /// <summary>
    /// 
    /// <para>
    /// See:
    ///   <see cref="IHasApplicationTennantId"/>
    ///   <see cref="IHasApplicationTennantIdSpecificReferenceData{TId}"/>
    ///   <see cref="IHasApplicationTennantIdSpecificDistributedGuidIdReferenceData"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IHasApplicationTennantIdSpecificReferenceData<TId> : 
        IHasReferenceData<TId>, IHasApplicationTennantId
        where TId : struct
    {
        
    }
}