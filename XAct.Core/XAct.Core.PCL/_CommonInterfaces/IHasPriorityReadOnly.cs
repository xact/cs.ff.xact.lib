﻿

namespace XAct
{
    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an object that defines priority.
    /// </summary>
    public interface IHasPriorityReadOnly
    {
        /// <summary>
        /// Gets the priority.
        /// <para>
        /// Member defined in the <see cref="IHasPriorityReadOnly"/> contract.
        /// </para>
        /// </summary>
        Priority Priority { get; }
    }
}
