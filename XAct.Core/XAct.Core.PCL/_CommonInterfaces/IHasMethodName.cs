﻿namespace XAct
{
    /// <summary>
    /// <para>
    /// See <see cref="IHasAssemblyAndTypeAndMethodNames"/>
    /// </para>
    /// </summary>
    public interface IHasMethodName
    {
        string MethodName { get; set; }
    }
}