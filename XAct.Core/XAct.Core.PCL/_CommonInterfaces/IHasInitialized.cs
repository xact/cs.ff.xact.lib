﻿

// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasInitialized : IHasInitializedReadOnly
    {
        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="IHasInitialize"/>.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        new bool Initialized { get; set; }
    }
}
