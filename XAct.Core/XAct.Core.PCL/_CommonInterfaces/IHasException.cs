﻿namespace XAct
{
    using System;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Contract for an object that exposes an <see cref="Exception"/>
    /// </summary>
    public interface IHasException : IHasExceptionReadOnly
    {
        /// <summary>
        /// Gets or sets the exception.
        /// <para>Member defined in<see cref="IHasException"/></para>
        /// </summary>
        /// <value>The exception.</value>
        new Exception Exception { get; set; }
    }
}
