﻿namespace XAct.Diagnostics.Status.Services.Connectors.Implementations
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class StatusServiceMetricsFeedConnectorConfigurationItem
    {
        /// <summary>
        /// Name of Counter Category
        /// </summary>
        [DataMember]
        public string CounterCategoryName { get; set; }
        /// <summary>
        /// Name of Counter
        /// </summary>
        [DataMember]
        public string CounterName { get; set; }

        /// <summary>
        /// Not used yet.
        /// </summary>
        [DataMember]
        public string InstanceName { get; set; }

        /// <summary>
        /// Whether the counter is read only or not.
        /// </summary>
        [DataMember]
        public bool ReadOnly { get; set; }

        /// <summary>
        /// 1.x
        /// </summary>
        [DataMember]
        public double Multiplier { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusServiceMetricsFeedConnectorConfigurationItem"/> class.
        /// </summary>
        public StatusServiceMetricsFeedConnectorConfigurationItem()
        {
            Multiplier = 1.0;
        }

        /// <summary>
        /// Gets a string made from: CounterCategoryName + ":" CounterName
        /// </summary>
        /// <returns></returns>
        public string GetUniqueName ()
        {
            return CounterCategoryName + ":" + CounterName;
        }
    }
}