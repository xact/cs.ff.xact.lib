﻿namespace XAct.Diagnostics.Status.Connectors
{
    using System;
    using XAct.Diagnostics.Services.Implementations;

    /// <summary>
    /// Contract for a Connector that can return a 
    /// <see cref="StatusResponse"/>
    /// </summary>
    public interface IStatusServiceConnector : IHasName, IHasTransientBindingScope
    {
        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time to use when retrieving metrics.</param>
        /// <param name="endTimeUtc">The end time to use when retrieving metrics (if null, NowUtc is used).</param>
        /// <returns></returns>
        StatusResponse Get(object arguments = null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null);
    }
}