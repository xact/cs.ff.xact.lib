﻿namespace XAct.Diagnostics.Status.Connectors
{
    using System.Collections.Generic;
    using XAct.Diagnostics.Status.Services.Connectors.Implementations;

    /// <summary>
    /// 
    /// </summary>
    public interface IStatusServiceMetricsFeedConnectorConfiguration 
    {
        /// <summary>
        /// List of the specifications of the Counters to register
        /// with <see cref="IMetricsFeedService"/>.
        /// </summary>
        List<StatusServiceMetricsFeedConnectorConfigurationItem> CounterSpecs { get; }
    }
}