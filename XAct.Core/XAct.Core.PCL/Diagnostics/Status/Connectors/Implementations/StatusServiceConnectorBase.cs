﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics.Services.Implementations;

    /// <summary>
    /// An abstract base implementation of the <see cref="IStatusServiceConnector"/>
    /// contract.
    /// </summary>
    public abstract class StatusServiceConnectorBase : IStatusServiceConnector,  IHasTitle, IHasDescription,
        IHasResourceFilterReadOnly

    {

        /// <summary>
        /// Gets the unique name of the Connector.
        /// <para>
        /// This is the name sent by the User Agent when requesting status reports.
        /// </para>
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Gets the Filter for the Resource in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        public string ResourceFilter { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// <para>
        /// Tip: clear this (or don't set it) 
        /// when requesting continuous feed data,
        /// as the server already would have it from the initial 
        /// request.
        /// </para>
        /// <para>
        /// Member defined in the <see cref="IHasTitle" /> contract.
        /// </para>
        /// </summary>
        public string Title { get; set; }




        /// <summary>
        /// Description of purpose for connector.
        /// <para>
        /// Tip: clear this (or don't set it) 
        /// when requesting continuous feed data,
        /// as the server already would have it from the initial 
        /// request.
        /// </para>
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the text description of the connector's function.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// </summary>
        public string StatusText { get; set; }



        /// <summary>
        /// Gets the <see cref="StatusResponse" />
        /// containing Data and/or Metrics.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public abstract StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null);



        /// <summary>
        /// Initializes a new instance of the <see cref="StatusServiceConnectorBase"/> class.
        /// <para>
        /// Note that end developers should develop an <c>Initialize()</c> method that initializes the controller
        /// with Title, Text, and any custom parameters needed, prior to the <see cref="Get"/>
        /// method being invoked.
        /// </para>
        /// </summary>
        protected StatusServiceConnectorBase()
        {
        }

        /// <summary>
        /// Helper method to create a new 
        /// <see cref="StatusResponse"/>
        /// with the <see cref="StatusResponse.Title"/> and <see cref="StatusResponse.Description"/>
        /// filled in.
        /// </summary>
        /// <returns></returns>
        protected StatusResponse BuildReponseObject()
        {

            StatusResponse result = new StatusResponse();

            result.Name = this.Name;

            result.ResourceFilter = ResourceFilter;
            result.Title = Title;
            result.Description = Description;
            result.Message = StatusText;

            return result;
        }



        /// <summary>
        /// ...
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arguments"></param>
        /// <param name="builder"></param>
        /// <returns></returns>
        protected List<T> ProcessArguments<T>(object arguments, Func<string, T> builder)
    where T : class
        {
            List<T> results = new List<T>();



            //But, in rare circumstances, one could pass in info.
            //The argument could be a string or array of strings.
            var stringArgument = arguments as string;
            if (stringArgument != null)
            {
                results.Add(builder.Invoke(stringArgument));
                return results;
            }

            string[] stringArguments = arguments as string[];
            if (stringArguments != null)
            {
                foreach (string tmp in stringArguments)
                {
                    results.Add(builder.Invoke(tmp));
                }
                return results;
            }

            T typedArgument = arguments as T;
            if (typedArgument != null)
            {
                results.Add(typedArgument);
                return results;
            }

            IEnumerable<T> typedEnumerableArgument = arguments as IEnumerable<T>;
            if (typedEnumerableArgument != null)
            {
                results.AddRange(typedEnumerableArgument);
                return results;
            }

            return results;

        }





    }
}