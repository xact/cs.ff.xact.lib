﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using XAct.Resources;

    /// <summary>
    /// An abstract base implementation of the <see cref="IStatusServiceConnector"/>
    /// contract for use within XActLib.
    /// <para>
    /// End users should use <see cref="StatusServiceConnectorBase"/>.
    /// </para>
    /// </summary>
    public abstract class XActLibStatusServiceConnectorBase : StatusServiceConnectorBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="XActLibStatusServiceConnectorBase"/> class.
        /// </summary>
        protected XActLibStatusServiceConnectorBase():base()
        {
        }

    }
}