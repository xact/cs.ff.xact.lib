﻿namespace XAct.Diagnostics.Status.Connectors
{

    /// <summary>
    /// A specialized <see cref="IStatusServiceConnector"/>
    /// for retrieving information from <see cref="IMetricsFeedService"/>.
    /// <para>
    /// The specialized contract is recoginized when the connector 
    /// is being registered, allowing  
    /// <see cref="IStatusServiceConfiguration"/> implementation
    /// to ensure the feed is initiated within <see cref="IMetricsFeedService"/>
    /// </para>
    /// </summary>
    public interface IStatusServiceMetricsFeedConnector : IStatusServiceConnector
    {
        /// <summary>
        /// Configuration for this Connector.
        /// <para>
        /// Used when registering the Connector, to ensure 
        /// Metric readings start getting picked up.
        /// </para>
        /// </summary>
        IStatusServiceMetricsFeedConnectorConfiguration Configuration { get; }
    }
}