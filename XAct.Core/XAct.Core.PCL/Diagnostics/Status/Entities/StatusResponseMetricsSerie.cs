﻿namespace XAct.Diagnostics.Services.Implementations
{
    using System.Runtime.Serialization;


    /// <summary>
    /// A single serie of metrics,
    /// as one or more of the collection within 
    /// <see cref="MetricsStatusResponse"/>
    /// <para>
    /// Serialized as:
    /// <code>
    /// <![CDATA[
    /// {
    ///   "Name"=abc",
    ///   "Status"=1,
    ///   [
    ///   {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
    ///   {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
    ///   ...
    ///   {"DateTime"="xyz","Value"=0.00,"Tag"="abc"}
    ///   ]
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    [DataContract]
    public class StatusResponseMetricsSerie : IHasName, IHasResultStatus
    {
        /// <summary>
        /// The unique name of the <see cref="StatusResponseResponseMetricsSerie"/>.
        /// </summary>
        [DataMember]
        public virtual string Name { get; set; }


        /// <summary>
        /// Get the result of a remote test.
        /// </summary>
        [DataMember]
        public ResultStatus Status { get; set; }

        /// <summary>
        /// An array of 
        /// <see cref="MetricsSerieEntry"/>
        /// items
        /// </summary>
        [DataMember]
        public virtual MetricsSerieEntry[] Items { get; set; }
    }
}