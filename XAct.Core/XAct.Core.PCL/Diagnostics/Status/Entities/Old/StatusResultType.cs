﻿//namespace XAct.Diagnostics.Services.Implementations
//{
//    using System.Runtime.Serialization;

//    /// <summary>
//    /// The Type of the <see cref="StatusResponse"/>.
//    /// <para>
//    /// A <see cref="StatusResponse"/> can contain a list of Metrics (useful to populate a running chart of perf counters)
//    /// or list of <c>Status</c> results (useful to render a list of Tests performed on the server).
//    /// </para>
//    /// </summary>
//    [DataContract]
//    public enum StatusResultType
//    {

//        /// <summary>
//        /// The <see cref="StatusResponse"/>
//        /// contains a Metric response.
//        /// </summary>
//        [EnumMember()]
//        Metrics=1,

//        /// <summary>
//        /// The Result contains a a list of Status entries (ie, Line + Flag)
//        /// </summary>
//        [EnumMember()]
//        Status=2,

//        //At present can't think of more types...will come.
//    }
//}