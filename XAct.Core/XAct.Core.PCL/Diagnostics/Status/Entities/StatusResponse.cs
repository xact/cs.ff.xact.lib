﻿namespace XAct.Diagnostics.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;


    /// <summary>
    /// The response to a inquiry as to (server side) status.
    /// </summary>
    /// <remarks>
    /// IMPORTANT: The reason these elements are part of XAct.Core.PCL 
    /// rather than XAct.Diagnostics.Status is that XAct.Diagnostics.Status
    /// has a lot of dependencies (PerfCounters, Db.EF, etc.)
    /// <para>
    /// We don't want all those References up at the Presenetation/serialization
    /// layer.
    /// </para>
    /// </remarks>
    [DataContract]
    public class StatusResponse : IHasName, IHasResourceFilter, IHasResultStatusReadOnly 
        
        //,IHasTextAndDescriptionReadOnly
    {

        /// <summary>
        /// Get the result of a remote test.
        /// </summary>
        public static readonly ResultStatus[] ResultStatuses = new ResultStatus[]
            {
                ResultStatus.Success,
                ResultStatus.Aborted,
                ResultStatus.Undetermined,
                ResultStatus.Warn,
                ResultStatus.Aborted,
                ResultStatus.Undefined
            };


        /// <summary>
        /// The summary Status (Aborted, Undetermined, Success, Fail, Warn). 
        /// of the individual Tests results within <see cref="Data"/>.
        /// <para>
        /// 
        /// </para>
        /// </summary>
        public ResultStatus Status
        {
            get
            {
                //In case there are no rows at all:
                _status = ResultStatus.Success;

                //Done this way so it can survive serialization across a wire:
                foreach (ResultStatus resultStatus in StatusResponse.ResultStatuses)
                {
                    if (this.Data.Rows.Any(x => x.Status == resultStatus))
                    {
                        _status = resultStatus;
                    }
                }
                if (_status == ResultStatus.Undefined)
                {
                     _status = ResultStatus.Undefined;
                    
                    Message = "Note: Cannot summarize ResultStatus if one of the StatusQueryResponseStatusDataTableRow.Status values is 'Undefined'.";
                }
                return _status;
            }
        }
        [DataMember(Name = "Status")]
        private ResultStatus _status;



        /// <summary>
        /// The textual representation of <see cref="ResultStatus"/>
        /// (makes it easier to render 'Success', 'Fail', rather than 0, 1
        /// </summary>
        [DataMember(Name = "StatusText")]
        public string StatusText
        {
            private set {}
            get
            {
                var result = this.Status.ToString();
                return result;
            }
        }

        /// <summary>
        /// The unique name of <c>IStatusServiceConnector</c>
        /// that generated this response.
        /// </summary>
        [DataMember]
        public virtual string Name { get; set; }



        /// <summary>
        /// Gets or sets the resource filter.
        /// </summary>
        /// <value>
        /// The resource filter.
        /// </value>
        [DataMember]
        public virtual string ResourceFilter { get; set; }

        /// <summary>
        /// The displayable Title of the readings.
        /// </summary>
        [DataMember]
        public virtual string Title { get; set; }

        /// <summary>
        /// The displayable Description of the readings.
        /// </summary>
        [DataMember]
        public virtual string Description { get; set; }

        /// <summary>
        /// An optional message summarizing the status.
        /// </summary>
        [DataMember]
        public virtual string Message { get; set; }

        /// <summary>
        /// Arguments for the optional <see cref="Message"/>.
        /// </summary>
        [DataMember]
        public virtual string[] MessageArguments { get; set; }


        /// <summary>
        /// A grid of data returned from one or more tests.
        /// </summary>
        public virtual StatusResponseTable Data { get { return _data ?? (_data = new StatusResponseTable()); } }
        [DataMember(Name="Data")] StatusResponseTable _data;



        /// <summary>
        /// An optional series of metrics.
        /// <para>
        /// Serialized as:
        /// <code>
        /// <![CDATA[
        /// {
        ///   Name="abc",
        ///   Status=1,
        ///   Items = 
        ///     [
        ///     {
        ///       "Name"=abc",
        ///       "Status"=1,
        ///       [
        ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
        ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
        ///         ...
        ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"}
        ///       ]
        ///     },
        ///     {
        ///       "Name"=abc",
        ///       "Status"=1,
        ///       [
        ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
        ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
        ///         ...
        ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"}
        ///       ]
        ///     }
        ///     ]
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        public StatusResponseMetricsSeries Series { get { return _series ?? (_series = new StatusResponseMetricsSeries()); } }
        [DataMember(Name="Series")] private StatusResponseMetricsSeries _series;


        ///// <summary>
        ///// Array (usually of only size=1) values that can be presented
        ///// to the user.
        ///// </summary>
        //public virtual List<NameStringValue> KeyValues { get { return _keyValues ?? (_keyValues = new List<NameStringValue>()); } }
        //[DataMember]
        //private List<NameStringValue> _keyValues;


        /// <summary>
        /// Information to Hint to the UI on how to best render the information.
        /// <para>
        /// An example would be ICON="People", and letting the UI choose the appropriate Icon to represent this.
        /// </para>
        /// </summary>
        public virtual List<NameStringValue> DisplayMetadata { get {return _displayMetadata ?? (_displayMetadata = new List<NameStringValue>()); }}
        [DataMember(Name="DisplayMetadata")] List<NameStringValue> _displayMetadata;

    }
}