﻿namespace XAct.Diagnostics.Services.Implementations
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A single entry within a <see cref="StatusResponseMetricsSerie"/>
    /// <para>
    /// Serialized as:
    /// <code>
    /// <![CDATA[
    /// {"DateTime"="xyz","Value"=0.00,"Tag"="abc"}
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    [DataContract]
    public class MetricsSerieEntry
    {

        /// <summary>
        /// The Utc DateTime at which the metric was recorded. 
        /// </summary>
        [DataMember]
        public virtual DateTime DateTime { get; set; }

        /// <summary>
        /// The recorded metric.
        /// </summary>
        [DataMember]
        public virtual double Value { get; set; }

        /// <summary>
        /// An optional Tag that can be applied to the metric.
        /// </summary>
        [DataMember]
        public virtual string Tag { get; set; }
    }
}