﻿namespace XAct.Diagnostics.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Row element within <see cref="StatusResponseTable"/>
    /// </summary>
    [DataContract]
    public class StatusResponseTableRow
    {


        /// <summary>
        /// The Status (Aborted, Undetermined, Success, Fail, Warn). 
        /// of this row/item/test.
        /// <para>
        /// Will be used as part of the summary status 
        /// available via <see cref="StatusResponse.Status"/>
        /// </para>
        /// </summary>
        [DataMember]
        public ResultStatus Status
        {
            get { return _status; }
            set
            {
                if (value == ResultStatus.Undefined)
                {
                    throw new ArgumentException(
                        "Cannot set StatusQueryResponseStatusDataTableRow.Status value to 'Undefined'.");
                }
                _status = value;
            }
        }


        [DataMember(Name = "StatusText")]
        public string StatusText
        {

            private set { }
            get
            {
                var result = this.Status.ToString();
                return result;
            }
        }


        /// <summary>
        /// The Textual column data generated by the test.
        /// <para>
        /// The number of cells must align with the number of columns
        /// defined in <see cref="StatusResponseTable.Headers"/>
        /// </para>
        /// </summary>
        public List<string> Cells
        {
            get { return _cells ?? (_cells = new List<string>()); }
        }

        [DataMember(Name = "Cells")] private List<string> _cells;
        private ResultStatus _status = ResultStatus.Undetermined;


        /// <summary>
        /// Optional Additional information about the test
        /// (eg: for Debug Exception messages explaining why the test result is as it is).
        /// <para>
        /// Use with caution to now allow leakage of sensitive data.
        /// </para>
        /// </summary>
        [DataMember]
        public string AdditionalInformation { get; set; }
    }
}