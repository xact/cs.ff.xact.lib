﻿namespace XAct.Diagnostics.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// An element within <see cref="StatusResponse"/>
    /// to contain Metric series.
    /// <para>
    /// Serialized as:
    /// <code>
    /// <![CDATA[
    /// {
    ///   Name="abc",
    ///   Status=1,
    ///   Items = 
    ///     [
    ///     {
    ///       "Name"=abc",
    ///       "Status"=1,
    ///       [
    ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
    ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
    ///         ...
    ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"}
    ///       ]
    ///     },
    ///     {
    ///       "Name"=abc",
    ///       "Status"=1,
    ///       [
    ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
    ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"},
    ///         ...
    ///         {"DateTime"="xyz","Value"=0.00,"Tag"="abc"}
    ///       ]
    ///     }
    ///     ]
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    [DataContract]
    public class StatusResponseMetricsSeries 
    {

        public List<StatusResponseMetricsSerie> Series { get { return _series ?? (_series = new List<StatusResponseMetricsSerie>()); } }
        [DataMember]
        private List<StatusResponseMetricsSerie> _series;
    }
}