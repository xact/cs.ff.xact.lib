﻿namespace XAct.Diagnostics.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// <para>
    /// An array of these objects, each one specifying the test
    /// performed, optionally more information that could
    /// be displayed to describe the test better if hovered over (the <c>Description</c>)
    /// </para>
    /// </summary>
    [DataContract]
    public class StatusResponseTable 
    {

        /// <summary>
        /// The culture specific text of the columns
        /// to display above the test results.
        /// </summary>
        public List<string> Headers { get { return _headers ?? (_headers = new List<string>()); } }
        [DataMember(Name = "Headers")]
        private List<string> _headers;


        /// <summary>
        /// The rows of data generated by the status tests.
        /// <para>
        /// Each row has two properties, it's <see cref="StatusResponseTableRow.Status"/>
        /// and its <see cref="StatusResponseTableRow.Cells"/> (ie Columns) 
        /// that align up with the <see cref="Headers"/> property.
        /// </para>
        /// <para>
        /// The worst case value of the individual <see cref="StatusResponseTableRow.Status"/>
        /// values is displayed in the parent 
        /// <see cref="StatusResponse.Status"/>
        /// </para>
        /// </summary>
        public List<StatusResponseTableRow> Rows { get { return _rows ?? (_rows = new List<StatusResponseTableRow>()); } }
        [DataMember(Name = "Rows")]
        List<StatusResponseTableRow> _rows;




    }
}