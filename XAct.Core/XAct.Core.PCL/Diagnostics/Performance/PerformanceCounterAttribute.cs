﻿using System;

namespace XAct.Diagnostics.Performance
{

    public enum PerformanceCounterUpdateType
    {

        /// <summary>
        /// Increments the Counter by one. 
        /// </summary>
        Increment = 1,


        /// <summary>
        /// Sets a whole bunch of Counters by convention.
        /// </summary>
        Automatic = 2,


        /*
        /// <summary>
        /// Increments the performance counter by 1, whether exception raised or not.
        /// <para>
        /// This is the default behaviour.
        /// </para>
        /// </summary>
        [Obsolete]
        IncrementInvokedCounter = 11,

        /// <summary>
        /// Increments the performance counter value by 1 if no exception was raised.
        /// </summary>
        [Obsolete]
        IncrementInvokedNonExceptionCounter = 12,

        /// <summary>
        /// Increments the performance counter value by 1 if an exception was raised.
        /// </summary>
        [Obsolete]
        IncrementInvokedExceptionCounter = 13,


        /// <summary>
        /// Increments the performance counter value by 1 if the response is a <see cref="IResponse"/>.
        /// </summary>
        [Obsolete]
        IncrementResponseCounter = 21,

        /// <summary>
        /// Increments the performance counter value by 1 if the response is a <see cref="IResponse"/> and its <c>Success</c> value is <c>true</c>.
        /// </summary>
        [Obsolete]
        IncrementResponseSuccessCounter = 22,


        /// <summary>
        /// Increments the performance counter value by 1 if the response is a <see cref="IResponse"/> and its <c>Success</c> value is <c>false</c>.
        /// </summary>
        [Obsolete]
        IncrementResponseFailureCounter = 23,

        /// <summary>
        /// Increments the performance counter value by 1 for every db operation done in the current Unit of Work.
        /// </summary>
        [Obsolete]
        SetUnitOfWorkOperationCounter = 31,

        [Obsolete]
        SetUnitOfWorkOperationDurationTicks = 32,


        /// <summary>
        /// Updates the performance value by the duration operation ticks.
        /// </summary>
        [Obsolete]
        IncrementDurationTicks = 41,

        /// <summary>
        /// Sets the performance value's raw value to the operation duration ticks.
        /// </summary>
        [Obsolete]
        SetDurationTicks = 42,

        */

    }



    /// <summary>
    /// Attribute that can be attached to Service facade methods,
    /// in order for an AOP interceptor to determine the name of the 
    /// PerformanceCounter to invoke
    /// </summary>
    [System.AttributeUsage(
        (System.AttributeTargets.Class | System.AttributeTargets.Interface | System.AttributeTargets.Method),
        AllowMultiple = true,
        Inherited = true)]
    public class PerformanceCounterAttribute : Attribute, IHasCategoryNameReadOnly, IHasNameReadOnly, IHasEnabledReadOnly, IHasOrder
    {
        /// <summary>
        /// Gets or sets a value indicating whether this named performance counter is to 
        /// incremented every time the decorated method is invoked.
        /// </summary>
        public bool Enabled { get; private set; }


        /// <summary>
        /// The Performance Counter Type.
        /// <para>
        /// The default is <c>IncrementInvoked</c>.
        /// </para>
        /// </summary>
        public PerformanceCounterUpdateType Type { get; set; }

        /// <summary>
        /// Gets the performance counter Category.
        /// </summary>
        public string CategoryName { get; private set; }

        /// <summary>
        /// The Name(s) of the Performance Counters to increment.
        /// <para>
        /// The value can be a CSV list of performance counter names.</para>
        /// </summary>
        public string Name { get; private set; }


        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        public int Order { get; set; }


        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public string Tag { get; set; }



        /// <summary>
        /// Only need to reset Sub Operation Counters (Tagged ones) if in a stateless environment.
        /// </summary>
        public bool ResetSubOperationCounters { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceCounterAttribute" /> class.
        /// </summary>
        /// <param name="categoryName">Name of the group.</param>
        /// <param name="counterName">The name.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="tag">The sub operation tag(s) (e.g: "RepositoryService.Queries", '"RepositoryService.Queries.Deferred" : "RepositoryService.Queries.Immediate", etc.)</param>
        /// <param name="updateType">Type of the update.</param>
        /// <param name="resetSubOperationCounters">if set to <c>true</c> [reset sub operation counters].</param>
        public PerformanceCounterAttribute(string categoryName, string counterName, bool enabled = true, string tag = null, PerformanceCounterUpdateType updateType = PerformanceCounterUpdateType.Increment, bool resetSubOperationCounters=false)
        {
            CategoryName = categoryName;
            Name = counterName;
            Enabled = enabled;
            Tag = tag.IsNullOrEmpty() ? null : tag;
            Type = updateType;
            ResetSubOperationCounters = resetSubOperationCounters;
        }

    }
}
