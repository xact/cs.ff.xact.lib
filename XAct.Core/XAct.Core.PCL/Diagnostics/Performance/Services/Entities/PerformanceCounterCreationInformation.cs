﻿namespace XAct.Diagnostics.Performance.Services.Entities
{
    using System;

    /// <summary>
    /// The description of an implementation of
    /// <see cref="IPerformanceCounter"/>
    /// </summary>
    public class PerformanceCounterCreationInformation : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasNameAndDescription
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets the name under which to register the <see cref="IPerformanceCounter"/>.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _name;


        public virtual string InstanceName
        {
            get { return _instanceName; }
            set { _instanceName = value; }
        }


        public virtual bool ReadOnly
        {
            get { return _readOnly; }
            set { _readOnly = value; }
        }

        /// <summary>
        /// Gets or sets the description.
        /// <para>
        /// Member defined in the <see cref="IHasNameAndDescriptionReadOnly" /> contract.
        /// </para>
        /// </summary>
        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        private string _description;


        /// <summary>
        /// Gets or sets the type of the <see cref="IPerformanceCounter"/>.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public PerformanceCounterType Type
        {
            get { return _type; }
            set { _type = value; }
        }
        private PerformanceCounterType _type;
        private string _instanceName;
        private bool _readOnly;


        /// <summary>
        /// Gets or sets FK to the *optional* BASE Category associated to this element.
        /// </summary>
        public Guid? BaseFK { get; set; }

        /// <summary>
        /// Gets or sets FK to the *optional* BASE Category associated to this element.
        /// </summary>
        public virtual PerformanceCounterCreationInformation Base { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceCounterCreationInformation" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="type">The type.</param>
        public PerformanceCounterCreationInformation(string name, string description, PerformanceCounterType type)
        {
            _name = name;
            _description = description;
            _type = type;
        }

    }
}