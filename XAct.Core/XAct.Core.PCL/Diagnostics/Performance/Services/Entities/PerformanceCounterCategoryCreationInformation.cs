﻿namespace XAct.Diagnostics.Performance.Services.Entities
{
    using System;


    /// <summary>
    /// The argument package required during the application's startup/initialization sequence
    /// in order to ensure that Performance Counters are created, so that the application can then use them.
    /// <para>
    /// It is important to note that Performance counters need to be registered before usage -- and additional
    /// Performance Counters cannot be added later. 
    /// </para>
    /// <para>
    /// It is important to note that on Windows Servers, the creation/installation of Performance counters
    /// requires Admin authorisation on the host.
    /// </para>
    /// </summary>
    public class PerformanceCounterCategoryCreationInformation : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasInitialized, IHasNameAndDescription
    {

        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Initialized
        {
            get { return _initialized; }
            set
            {
                if (_initialized)
                {
                    throw new ArgumentException("'{0}.Initialized' is already set to true.".FormatStringInvariantCulture(this.GetType().Name));
                }
                _initialized = value;
            }
        }
        bool _initialized;

        /// <summary>
        /// Gets or sets the unique datastore identifier.
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets the name of the Category under which 
        /// instances of <see cref="IPerformanceCounter"/> are organized.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        /// <internal>8/13/2011: Sky</internal>
        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        /// <summary>
        /// Gets or sets the description to attach to category under which 
        /// instances of <see cref="IPerformanceCounter"/> are organized.
        /// <para>Member defined in<see cref="XAct.IHasNameAndDescription" /></para>
        /// </summary>
        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        /// <summary>
        /// Gets the collection of <see cref="PerformanceCounterCreationInformation"/>
        /// that describe the <see cref="IPerformanceCounter"/> implementations to register.
        /// </summary>
        public virtual PerformanceCounterCreationInformation[] CounterCreationInformation { get { return _counterCreationInformations; } set { _counterCreationInformations = value; } }
        private PerformanceCounterCreationInformation[] _counterCreationInformations;
        private string _description;
        private string _name;


        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceCounterCategoryCreationInformation" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="counterCreationInformations">The performance counter creation informations.</param>
        public PerformanceCounterCategoryCreationInformation(string name, string description, params PerformanceCounterCreationInformation[] counterCreationInformations)
            : this()
        {
            _name = name;
            _description = description;
            _counterCreationInformations = counterCreationInformations;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceCounterCategoryCreationInformation"/> class.
        /// </summary>
        public PerformanceCounterCategoryCreationInformation()
        {
        }

    }
}
