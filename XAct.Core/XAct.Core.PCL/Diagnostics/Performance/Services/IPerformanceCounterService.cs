﻿// ReSharper disable CheckNamespace
namespace XAct.Diagnostics.Performance
// ReSharper restore CheckNamespace
{
    using XAct.Diagnostics.Performance.Services.Configuration;

    /// <summary>
    /// Contract for a service to update existing
    /// <see cref="IPerformanceCounter" />s
    /// already registered using
    /// <see cref="IPerformanceCounterService" />.
    /// </summary>
    public interface IPerformanceCounterService : IHasXActLibService
    {
        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IPerformanceCounterServiceConfiguration Configuration { get; }


        /// <summary>
        /// Ensures the Category exists, describing the Counters, and then creates an initial set of Counters
        /// from them (their instances named after the Counter creation names).
        /// </summary>
        /// <returns></returns>
        bool Initialize();

        /// <summary>
        /// Gets the performance counter's raw value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        TValue GetRawValue<TValue>(string categoryName, string performanceCounterName, bool raiseExceptionIfNotFound = false);

        /// <summary>
        /// Gets the performance counter's raw value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="instanceName">Name of the instance.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        TValue GetRawValue<TValue>(string categoryName, string performanceCounterName, string instanceName=null, bool raiseExceptionIfNotFound = false);








        /// <summary>
        /// Gets the performance counter's computed value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        TValue GetComputedValue<TValue>(string categoryName, string performanceCounterName, bool raiseExceptionIfNotFound = false);

        /// <summary>
        /// Gets the performance counter's computed value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="instanceName">Name of the instance.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        TValue GetComputedValue<TValue>(string categoryName, string performanceCounterName, string instanceName=null, bool raiseExceptionIfNotFound = false);














        /// <summary>
        /// Sets the specified category name.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        bool Set(string categoryName, string performanceCounterName, long amount, bool raiseExceptionIfNotFound = false);

        /// <summary>
        /// Sets the specified category name.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="instanceName">Name of the instance.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        bool Set(string categoryName, string performanceCounterName, string instanceName, long amount, bool raiseExceptionIfNotFound = false);







        /// <summary>
        /// Updates the value of the counter.
        /// <para>
        /// Used to update simple <see cref="IPerformanceCounter" />s that have only one counter
        /// within the set.
        /// </para>
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        /// on the counter.
        /// </para>
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="amount">The amount to increment, or decrement the counter by.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        bool Offset(string categoryName, string performanceCounterName, long amount, bool raiseExceptionIfNotFound = false);

        
        /// <summary>
        /// Updates the value of the counter.
        /// <para>
        /// Used to update simple <see cref="IPerformanceCounter" />s that have only one counter
        /// within the set.
        /// </para>
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        /// on the counter.
        /// </para>
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="instanceName">Name of the instance.</param>
        /// <param name="amount">The amount to increment, or decrement the counter by.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        bool Offset(string categoryName, string performanceCounterName, string instanceName, long amount, bool raiseExceptionIfNotFound = false);










        /// <summary>
        /// Updates the value of the bottom (denominator) counter (ie, the 'b' in an 'a/b' configuration).
        /// of <see cref="IPerformanceCounter" />.
        /// <para>
        /// </para>
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        /// on the counter.
        /// </para>
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="amount">The amount to increment, or decrement the counter by.</param>
        /// <param name="nameExtension">The extension to put on the end of the Counter name (default is " (Base)".</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        bool UpdateDenominator(string categoryName, string performanceCounterName, long? amount = null, string nameExtension = " (Base)", bool raiseExceptionIfNotFound = false);

        /// <summary>
        /// Updates the value of the bottom (denominator) counter (ie, the 'b' in an 'a/b' configuration).
        /// of <see cref="IPerformanceCounter" />.
        /// <para>
        /// </para>
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        /// on the counter.
        /// </para>
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="instanceName"></param>
        /// <param name="amount">The amount to increment, or decrement the counter by.</param>
        /// <param name="nameExtension">The extension to put on the end of the Counter name (default is " (Base)".</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        bool UpdateDenominator(string categoryName, string performanceCounterName, string instanceName=null, long? amount = null, string nameExtension = " (Base)", bool raiseExceptionIfNotFound = false);
    }
}
