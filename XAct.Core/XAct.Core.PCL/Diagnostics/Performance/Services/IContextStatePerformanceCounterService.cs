﻿using System;

namespace XAct.Diagnostics.Performance.Services
{
    public interface IContextStatePerformanceCounterService : IHasXActLibService
    {
        void IncrementContextOperationCounter(string tag, int offset = 1);
        int GetContextOperationCounter(string tag);
        void ResetContextOperationCounter(string tag);

        void IncrementContextOperationDuration(string tag, TimeSpan timeSpan = default(TimeSpan));
        TimeSpan GetContextOperationDuration(string tag);
        void ResetContextOperationDuration(string tag);

    }
}
