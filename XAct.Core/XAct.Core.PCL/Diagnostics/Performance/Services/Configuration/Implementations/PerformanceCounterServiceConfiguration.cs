namespace XAct.Diagnostics.Performance.Services.Configuration.Implementations
{
    using XAct.Diagnostics.Performance.Services.Entities;
    using XAct.Services;

    /// <summary>
    /// An implementation of 
    /// <see cref="IPerformanceCounterServiceConfiguration"/>
    ///  for a singleton configuration package
    /// to configure implementations of <see cref="IApplicationPerformanceCounterService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof (IPerformanceCounterServiceConfiguration), BindingLifetimeType.SingletonScope,
        Priority.Low)]
    public class PerformanceCounterServiceConfiguration : IPerformanceCounterServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether the Service 
        /// updates Performance Counters or not
        /// (requires that the Performance Counters were installed before hand).
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// <para>
        /// Default is <c>true</c>.
        /// </para>
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        private bool _enabled = true;


        /// <summary>
        /// Gets or sets a value indicating whether the Category
        /// should be registered during the application's initialization sequence.
        /// <para>
        /// IMPORTANT: Will require full Admin rights.
        /// </para>
        /// <para>
        /// Default is <c>false</c>.
        /// </para>
        /// </summary>
        public bool AttemptSelfInstall
        {
            get { return _attemptSelfInstall; }
            set { _attemptSelfInstall = value; }
        }

        private bool _attemptSelfInstall = false;


        /// <summary>
        /// Gets or sets a value indicating whether to force category recreation if self re-installing.
        /// </summary>
        public bool ForceCategoryRecreationIfSelfInstalling
        {
            get { return _forceCategoryRecreationIfSelfInstalling; }
            set { _forceCategoryRecreationIfSelfInstalling = value; }
        }

        private bool _forceCategoryRecreationIfSelfInstalling = false;


        /// <summary>
        /// Gets or sets a value indicating whether to raise exceptions if the named performance counter not found.
        /// <para>
        /// Default is <c>true</c>.
        /// </para>
        /// </summary>
        public bool RaiseExceptionsIfPerformanceCounterNotFound
        {
            get { return _raiseExceptionsIfPerformanceCounterNotFound; }
            set { _raiseExceptionsIfPerformanceCounterNotFound = value; }
        }

        private bool _raiseExceptionsIfPerformanceCounterNotFound = false;


        /// <summary>
        /// Gets the information required to create the default application Category.
        /// </summary>
        public PerformanceCounterCategoryCreationInformation[] CounterCategoryCreationInformation { get; set; }


        /// <summary>
        /// Gets the PerformanceCounterSets to register against the <see cref="CounterCategoryCreationInformation"/>.
        /// </summary>
        ///public IPerformanceCounterSet[] PerformanceCounterSets { get; set; }

        public PerformanceCounterServiceConfiguration()
        {
            //Leave contructor argumentless, so that it can be invoked via ServiceLocator during
            //application configuration, and configured at that point, by submitting config information
            //to Register method.
            Enabled = true;


        }

    }
}