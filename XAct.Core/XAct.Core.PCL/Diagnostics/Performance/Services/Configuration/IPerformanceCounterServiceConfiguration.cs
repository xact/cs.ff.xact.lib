namespace XAct.Diagnostics.Performance.Services.Configuration
{
    using XAct.Diagnostics.Performance.Services.Entities;

    /// <summary>
    /// Contract for a singleton configuration package
    /// to configure implementations of <see cref="IPerformanceCounterService"/>
    /// </summary>
    public interface IPerformanceCounterServiceConfiguration : IHasXActLibServiceConfiguration, IHasEnabled
    {

        /// <summary>
        /// Gets or sets a value indicating whether the Category
        /// should be registered during the application's initialization sequence.
        /// <para>
        /// IMPORTANT: Will require full Admin rights.
        /// </para>
        /// </summary>
        bool AttemptSelfInstall { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether to force category recreation if self re-installing.
        /// </summary>
        bool ForceCategoryRecreationIfSelfInstalling { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether 
        /// to raise raise exceptions if performance counters are not found.
        /// </summary>
        /// <value>
        /// <c>true</c> if [raise exceptions if performance counter not found]; otherwise, <c>false</c>.
        /// </value>
        bool RaiseExceptionsIfPerformanceCounterNotFound { get; set; }

        /// <summary>
        /// Gets the information required to create the default application Category.
        /// </summary>
        /// <value>
        /// The category creation information.
        /// </value>
        PerformanceCounterCategoryCreationInformation[] CounterCategoryCreationInformation { get; set; }

    }
}