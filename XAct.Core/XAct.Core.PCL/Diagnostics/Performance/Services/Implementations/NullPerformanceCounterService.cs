﻿namespace XAct.Diagnostics.Performance.Services.Implementations
{
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct.Services;

    public class NullPerformanceCounterService :IPerformanceCounterService
    {
        public IPerformanceCounterServiceConfiguration Configuration { get; private set; }
        public bool Initialize()
        {
            return true;
        }

        public TValue GetRawValue<TValue>(string categoryName, string performanceCounterName, bool raiseExceptionIfNotFound = false)
        {
            return default(TValue);

        }

        public TValue GetRawValue<TValue>(string categoryName, string performanceCounterName, string instanceName = null, bool raiseExceptionIfNotFound = false)
        {
            return default(TValue);

        }






        public TValue GetComputedValue<TValue>(string categoryName, string performanceCounterName, bool raiseExceptionIfNotFound = false)
        {
            return default(TValue);
        }

        public TValue GetComputedValue<TValue>(string categoryName, string performanceCounterName, string instanceName=null, bool raiseExceptionIfNotFound = false)
        {
            return default(TValue);
        }





        public bool Set(string categoryName, string performanceCounterName, long amount, bool raiseExceptionIfNotFound = false)
        {
            return false;
        }
        public bool Set(string categoryName, string performanceCounterName, string instanceName, long amount, bool raiseExceptionIfNotFound = false)
        {
            return false;
        }







        public bool Offset(string categoryName, string performanceCounterName, long amount, bool raiseExceptionIfNotFound = false)
        {
            return false;
        }
        public bool Offset(string categoryName, string performanceCounterName, string instanceName, long amount, bool raiseExceptionIfNotFound = false)
        {
            return false;
        }





        public bool UpdateDenominator(string categoryName, string performanceCounterName, long? amount = null, string nameExtension = " (Base)", bool raiseExceptionIfNotFound = false)
        {
            return false;
        }
        public bool UpdateDenominator(string categoryName, string performanceCounterName, string instanceName = null, long? amount = null, string nameExtension = " (Base)", bool raiseExceptionIfNotFound = false)
        {
            return false;
        }
    }
}
