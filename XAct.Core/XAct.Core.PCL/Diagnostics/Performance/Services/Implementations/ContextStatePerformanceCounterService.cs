﻿namespace XAct.Diagnostics.Performance.Services.Implementations
{
    using System;
    using XAct.Services;
    using XAct.State;

    public class ContextStatePerformanceCounterService : IContextStatePerformanceCounterService
    {
        private readonly IContextStateService _contextStateService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextStatePerformanceCounterService"/> class.
        /// </summary>
        /// <param name="contextStateService">The context state service.</param>
        public ContextStatePerformanceCounterService(IContextStateService contextStateService)
        {
            _contextStateService = contextStateService;
        }

        public void IncrementContextOperationCounter(string tag, int offset = 1)
        {
            if (tag.IsNullOrEmpty())
            {
                return;
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationCount", tag);

            lock (this)
            {
                int value = (int) (_contextStateService.Items[key] ?? 0) + offset;

                _contextStateService.Items[key] = value;
            }
        }

        public int GetContextOperationCounter(string tag)
        {
            if (tag.IsNullOrEmpty())
            {
                return 0;
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationCount", tag);

            key = string.Format(key, tag);
            lock (this)
            {
                int value = (int) (_contextStateService.Items[key] ?? 0);
                return value;
            }
        }

        public void ResetContextOperationCounter(string tag)
        {
            if (tag.IsNullOrEmpty())
            {
                return;
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationCount", tag);

            _contextStateService.Items[key]  = 0;
        }

        public void IncrementContextOperationDuration(string tag, TimeSpan timeSpan = default(TimeSpan))
        {
            if (tag.IsNullOrEmpty())
            {
                return;
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationDuration", tag);
            lock (this)
            {
                TimeSpan newValue = ((TimeSpan) (_contextStateService.Items[key] ?? TimeSpan.Zero)).Add(timeSpan);
                _contextStateService.Items[key] = newValue;
            }

        }

        public TimeSpan GetContextOperationDuration(string tag)
        {
            if (tag.IsNullOrEmpty())
            {
                return default(TimeSpan);
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationDuration", tag);
            lock (this)
            {
                TimeSpan value = (TimeSpan) (_contextStateService.Items[key] ?? TimeSpan.Zero);
                return value;
            }
        }
        public void ResetContextOperationDuration(string tag)
        {
            if (tag.IsNullOrEmpty())
            {
                return;
            }
            string key = string.Format("XAct.Lib.UnitOfWork.{0}.OperationDuration", tag);

            lock (this)
            {
                _contextStateService.Items[key] = TimeSpan.Zero;
            }
        }
    }
}