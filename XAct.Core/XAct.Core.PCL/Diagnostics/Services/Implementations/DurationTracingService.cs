﻿using System;

namespace XAct.Diagnostics
{
    using System.Collections.Generic;
    using System.Threading;
    using XAct.Diagnostics.Services;
    using XAct.Diagnostics.Services.Configuration;


    public class DurationTracingService : IDurationTracingService
    {
        public IDurationTracingServiceState State { get; private set; }

        public DurationTracingService(IDurationTracingServiceState state)
        {
            State = state;
        }

        public void Start(string tag)
        {
            GetTimer(tag).Start();
        }

        public void Stop(string tag)
        {
            GetTimer(tag).Stop();
        }

        public void Clear(string tag)
        {
            GetTimer(tag).Clear();
        }

        public KeyValue<string, TimeSpan>[] Report()
        {
            List<KeyValue<string,TimeSpan>> results = new List<KeyValue<string, TimeSpan>>();
            foreach (string key in State.Timers.Keys)
            {
                results.Add(new KeyValue<string, TimeSpan>{Key=key,Value=State.Timers[key].Duration});
            }
            return results.ToArray();
        }

        private Timer GetTimer(string tag)
        {
            Timer timer;
            if (!State.Timers.TryGetValue(tag, out timer))
            {
                State.Timers[tag] = timer = new Timer();
            }
            return timer;
        }


    }
}
