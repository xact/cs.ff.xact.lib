﻿namespace XAct.Diagnostics
{
    using System;
    using XAct.Environment;

    public class Timer
    {
        private IDateTimeService DateTimeService;

        public DateTime Started { get; private set; }
        public DateTime Stopped { get; private set; }
        public bool Running { get; set; }
        public TimeSpan Duration
        {
            get
            {
                return Running ? new TimeSpan() : Stopped.Subtract(Started);
            }
        }

        public Timer()
        {
            DateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();

            Clear();
        }

        public void Start()
        {
            Started = DateTimeService.NowUTC;
            Running = true;
        }
        public void Stop()
        {
            Stopped = DateTimeService.NowUTC;
            Running = true;
        }
        public void Clear()
        {
            Started = new DateTime();
            Stopped = Started;
            Running = false;
        }

    }
}