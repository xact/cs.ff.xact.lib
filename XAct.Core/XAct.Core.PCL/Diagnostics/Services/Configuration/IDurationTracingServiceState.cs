﻿namespace XAct.Diagnostics.Services.Configuration
{
    using System.Collections.Generic;

    public interface IDurationTracingServiceState : IHasXActLibServiceState
    {
        IDictionary<string, Timer> Timers { get; }
    }
}