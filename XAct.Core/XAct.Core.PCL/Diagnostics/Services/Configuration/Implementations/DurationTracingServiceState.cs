﻿namespace XAct.Diagnostics.Services.Configuration.Implementations
{
    using System.Collections.Generic;

    public class DurationTracingServiceState : IDurationTracingServiceState
    {
        public IDictionary<string, Timer> Timers
        {
            get { return _timers; }
        }

        private IDictionary<string, Timer> _timers = new Dictionary<string, Timer>();
    }
}