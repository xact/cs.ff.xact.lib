﻿namespace XAct.Diagnostics
{
    using System;

    /// <summary>
    /// Contract for injectable service for logging.
    /// </summary>
    public interface ITracingService : IHasXActLibService
    {
        /*
        /// <summary>
        ///   Writes the given message to the current Trace output.
        /// <para>
        /// IMPORTANT: Use sparingly! In fact, avoid using this method except in the rarest of cases, 
        /// as it bypasses the logic of the TraceLevel arguments in the other
        /// message signature. 
        /// </para>
        /// <para>
        /// TIP: Implement Implicitly.
        /// </para>
        /// </summary>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void TraceLine(string message, params object[] arguments);
        */

        /// <summary>
        /// QuickTraces the specified message, prefixing it with the name of the method.
        /// <para>
        /// It's really a debugging method, to use sparingly, as it uses costly Reflection,
        /// as well adding no unqualitified messages (missing TraceLevel).
        /// </para>
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="arguments">The arguments.</param>
        void QuickTrace(string message = null, params object[] arguments);


        /// <summary>
        ///   Logs the given message to the current Trace output.
        /// </summary>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void Trace(TraceLevel traceLevel, string message, params object[] arguments);


        /// <summary>
        ///   Logs the given message to the current Trace output.
        /// <para>
        /// If called directly, the stackTraceOffset should be 0.
        /// But if this infrastructure service is called indirectly
        /// (eg wrapped by a business service) the stackTraceOffset should be 1)
        /// </para>
        /// </summary>
        /// <param name="stackTraceOffset"></param>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void Trace(int stackTraceOffset, TraceLevel traceLevel, string message, params object[] arguments);


        /// <summary>
        ///   Logs the exception.
        ///   <para>
        ///     In most cases, use the other overload as it does not require specifying the ErrorLevel.
        ///   </para>
        ///   <para>
        ///     This overload can be useful to log exceptions as Warnings only.
        ///   </para>
        /// </summary>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "exception">The exception.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void TraceException(TraceLevel traceLevel, Exception exception, string message, params object[] arguments);

        /// <summary>
        ///   Logs the exception.
        ///   <para>
        ///     In most cases, use the other overload as it does not require specifying the ErrorLevel.
        ///   </para>
        /// <para>
        /// If called directly, the stackTraceOffset should be 0.
        /// But if this infrastructure service is called indirectly
        /// (eg wrapped by a business service) the stackTraceOffset should be 1)
        /// </para>
        ///   <para>
        ///     This overload can be useful to log exceptions as Warnings only.
        ///   </para>
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "exception">The exception.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void TraceException(int stackTraceFrameOffset, TraceLevel traceLevel, Exception exception, string message,
                            params object[] arguments);


        /// <summary>
        ///   Logs the given message to the current Trace output.
        /// <para>
        /// Important:
        /// This method is decorated with ConditionalAttribute("DEBUG")
        /// so will be stripped out of Release code.
        /// </para>
        /// </summary>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void DebugTrace(TraceLevel traceLevel, string message, params object[] arguments);

        /// <summary>
        ///   Logs the given message to the current Trace output.
        /// <para>
        /// Important:
        /// This method is decorated with ConditionalAttribute("DEBUG")
        /// so will be stripped out of Release code.
        /// </para>
        /// <para>
        /// If called directly, the stackTraceOffset should be 0.
        /// But if this infrastructure service is called indirectly
        /// (eg wrapped by a business service) the stackTraceOffset should be 1)
        /// </para>
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void DebugTrace(int stackTraceFrameOffset, TraceLevel traceLevel, string message, params object[] arguments);

        /// <summary>
        ///   Logs the exception.
        /// <para>
        /// Important:
        /// This method is decorated with ConditionalAttribute("DEBUG")
        /// so will be stripped out of Release code.
        /// </para>
        /// </summary>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "exception">The exception.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void DebugTraceException(TraceLevel traceLevel, Exception exception, string message, params object[] arguments);


        /// <summary>
        ///   Logs the exception.
        /// <para>
        /// Important:
        /// This method is decorated with ConditionalAttribute("DEBUG")
        /// so will be stripped out of Release code.
        /// </para>
        /// <para>
        /// If called directly, the stackTraceOffset should be 0.
        /// But if this infrastructure service is called indirectly
        /// (eg wrapped by a business service) the stackTraceOffset should be 1)
        /// </para>
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "exception">The exception.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        void DebugTraceException(int stackTraceFrameOffset, TraceLevel traceLevel, Exception exception, string message,
                                 params object[] arguments);
    }

    
}