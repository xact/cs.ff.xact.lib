﻿namespace XAct.Diagnostics.Services
{
    using XAct.Diagnostics.Services.Configuration;

    public interface IDurationTracingService : IHasXActLibService
    {
        IDurationTracingServiceState State { get; }

        void Start(string tag);
        void Stop(string tag);
        void Clear(string tag);
    }
}