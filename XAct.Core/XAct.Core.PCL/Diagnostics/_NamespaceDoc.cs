﻿
namespace XAct.Diagnostics
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Namespace to contain Tracing and other Diagnostics tools. 
    /// </summary>
    /// <remarks>
    /// <para>
    /// Regarding Tracing versus Logging:
    /// </para>
    /// <para>
    /// It's a bit sad that after all these years, Logging and Tracing are 
    /// still confusing to many. It doesn't help that the best Tracing solution
    /// out there is called Log4Net.
    /// </para>
    /// <para>
    /// Tracing -- to a text file stream, EventLog, or WCF service is a temporary
    /// operation. In other words, the record isn't intended to last for ever.
    /// It is a way for Support to Diagnose how the application is behaving -- or misbehaving.
    /// </para>
    /// <para>
    /// Logging, on the other hand, is a much more permanent operation, more akin
    /// to Auditing. Logs are used to keep track of operations, to know who, or what
    /// did what, when. It's less of a Diagnosis tool than a record keeping artifact.
    /// </para>
    /// <para>
    /// ...
    /// </para>
    /// <para>
    /// Custom TraceListeners:
    /// </para>
    /// <para>
    /// The .NET Framework has a perfectly good Tracing system built into it
    /// accessible from anywhere, with a decently configurable TraceListener
    /// based output.
    /// </para>
    /// <para>
    /// In absolutely *all* scenarios I would suggest *never* polluting your system
    /// with any third party tracing framework (log4net, enterprise library, common loggging 
    /// etc.) and just use the write a custom TraceListener that collects the output
    /// and delivers it to whatever output you wih.
    /// </para>
    /// <para>
    /// In fact, that's exactly what we did with the Log4NetTraceListener. The library
    /// code is free from any temporal vendor tracing mechanism that is in vogue today
    /// and probably will be out of vogue in a couple of years (logging frameworks 
    /// seem to fall out of favour quickly). 
    /// </para>
    /// <para>...</para>
    /// <para>Configuring TraceListeners:</para>
    /// <para>
    /// We found only one scenario where configuring TraceListeners via the Config file
    /// was impractical: K2 Server. With limited access to the Server and it'se config file,
    /// that's the only time where we found a need to 
    /// configure trace listeners via code, in a bootstrapper.
    /// </para>
    /// </remarks>
    /// <internal>
    /// This class is required by Sandcastle to document Namespaces: http://bit.ly/vqzRiK
    /// </internal>
    [CompilerGenerated]
    class NamespaceDoc { }
}
