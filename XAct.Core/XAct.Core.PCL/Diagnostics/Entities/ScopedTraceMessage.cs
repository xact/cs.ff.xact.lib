﻿namespace XAct.Diagnostics
{
    using System;

    /// <summary>
    /// A TraceMessage that can be used to Open/Close messages (using the 'using' keyword)
    /// <para>
    /// <code>
    /// <![CDATA[
    /// using (new ScopedMessage("Some Method Name")){
    ///   //blah...
    /// }
    /// ]]>
    /// </code>
    /// outputs:
    /// <code>
    /// <![CDATA[
    /// Begin...:Some Method Name
    /// Complete:SOme Method Name
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public class ScopedTraceMessage :IDisposable
    {
        private readonly TraceLevel _traceLevel; 
        private readonly ITracingService _tracingService;
        private readonly string _savedMessage;
        /// <summary>
        /// Scopeds the trace message.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">The arguments.</param>
        public ScopedTraceMessage(ITracingService tracingService, TraceLevel traceLevel, string message, object[] arguments)
        {
            _tracingService = tracingService;
            _traceLevel = traceLevel;
            _savedMessage = message.FormatStringCurrentCulture(arguments);
            tracingService.Trace(traceLevel, "Begin...: " + _savedMessage);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// <para>
        /// Writes a scope closing message to the tracing service.
        /// </para>
        /// </summary>
        public void Dispose()
        {
            _tracingService.Trace(_traceLevel, "Complete: " + _savedMessage);
        }
    }
}
