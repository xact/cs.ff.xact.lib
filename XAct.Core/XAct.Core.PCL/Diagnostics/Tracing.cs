﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Timers;

namespace XAct.Diagnostics
{

    /// <summary>
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Tracing is configurable on an application, assembly, and/or class name.
    ///   </para>
    ///   <para>
    ///     For example, the following will trace only Warnings and Errors in most
    ///     of the app, the Info messages in the Client assembly, and trace everything
    ///     in the Message class:
    ///     <code>
    ///       <![CDATA[
    /// <system.diagnostics>
    ///   <switches>
    ///     <add name="Application" value="Warning" />
    ///     <add name="Xero.Messaging.Client" value="Info" />
    ///     <add name="Xero.Messaging.Client.Message" value="Verbose"/>
    ///   </switches>
    ///   <trace autoflush="true" indentsize="2">
    ///     <listeners>
    ///       <remove name="Default" />
    ///       <add name="textListener"
    ///            type="System.Diagnostics.TextWriterTraceListener"
    ///            traceOutputOptions="None"
    ///            initializeData="c:\Logfiles\Xero.Messaging\Client\Xero.Messaging.Client.log" />
    ///     </listeners>
    ///   </trace>
    /// </system.diagnostics>
    /// ]]>
    ///     </code>
    ///   </para>
    /// </remarks>
    [SuppressMessage("Microsoft.Performance", "CA1810", Justification = "Not sure how to attach EventHandler without a static event constructor.")]
    public static class Tracing
    {
        private static readonly TraceSwitchService _traceSwitchService = new TraceSwitchService();

        private readonly static CultureInfo _invariantCulture = CultureInfo.CurrentCulture;

        public static int FrameMethodOffset = 1;

// ReSharper disable InconsistentNaming
        private static readonly Timer _Timer = new Timer{Interval=1000,AutoReset=false,Enabled=false};
// ReSharper restore InconsistentNaming


        static Tracing()
        {
            _Timer.Elapsed += TimerElapsed;
            //_Timer.Interval = 1000;
            //_Timer.AutoReset = false;
            //_Timer.Enabled = false;
        }


        /// <summary>
        ///   Traces the write line (Level is assumed to be Verbose).
        /// </summary>
        /// <param name = "message">The message.</param>
        /// <param name = "args">The args.</param>
        public static void WriteLine(string message, params object[] args)
        {
            try
            {
                Type classInstanceType = new StackTrace().GetFrame(FrameMethodOffset).GetMethod().DeclaringType;

                WriteLineIf(classInstanceType, TraceLevel.Verbose, null, message, args);
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }
        }


        /// <summary>
        ///   Traces the write line if.
        /// </summary>
        /// <param name = "traceLevel">The trace level.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "args">The args.</param>
        public static void WriteLineIf(
            TraceLevel traceLevel,
            string message,
            params object[] args)
        {
            try
            {
                Type classInstanceType = new StackTrace().GetFrame(FrameMethodOffset).GetMethod().DeclaringType;
                WriteLineIf(classInstanceType, traceLevel, null, message, args);
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }
        }


        /// <summary>
        ///   Writes the line if.
        /// </summary>
        /// <param name = "classInstanceType">Type of the class instance.</param>
        /// <param name = "traceLevel">The trace level.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "args">The args.</param>
        public static void WriteLineIf(Type classInstanceType, TraceLevel traceLevel, string message, object[] args)
        {
            try
            {
                WriteLineIf(classInstanceType, traceLevel, null, message, args);
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }
        }


        /// <summary>
        ///   Traces the given 
        ///   <paramref name = "message" /> 
        ///   if the relevent TraceSwitch's Level is 
        ///   equal or higher than the given 
        ///   <paramref name = "traceLevel" /> 
        ///   argument.
        ///   <para>
        ///     WARNING: Do not call this method from a method 
        ///     that has a chance of being Inlined.
        ///   </para>
        /// </summary>
        /// <param name = "traceLevel">
        ///   The level that the relevant TraceSwitch has to be at to log this message.
        /// </param>
        /// <param name = "exception">The exception object (if any).</param>
        /// <param name = "message">The text message to trace.</param>
        /// <param name = "args">
        ///   The optional arguments to string format into the message.
        /// </param>
        public static void WriteLineIf(
            TraceLevel traceLevel,
            Exception exception,
            string message,
            params object[] args)
        {
            try
            {
                Type classInstanceType = new StackTrace().GetFrame(FrameMethodOffset).GetMethod().DeclaringType;

                WriteLineIf(classInstanceType, traceLevel, exception, message, args);
            }
            catch (Exception exception2)
            {
                exception = exception2;
                Debug.WriteLine(exception.Message);
            }
        }

        /// <summary>
        ///   Traces the given 
        ///   <paramref name = "message" /> 
        ///   if the relevent TraceSwitch's Level is 
        ///   equal or higher than the given 
        ///   <paramref name = "traceLevel" /> 
        ///   argument.
        /// </summary>
        /// <param name = "classInstanceType">Type of the class instance.</param>
        /// <param name = "traceLevel">
        ///   The level that the relevant TraceSwitch has to be at to log this message.
        /// </param>
        /// <param name = "exception">The exception object (if any).</param>
        /// <param name = "message">The text message to trace.</param>
        /// <param name = "args">
        ///   The optional arguments to string format into the message.
        /// </param>
        public static void WriteLineIf(
            Type classInstanceType,
            TraceLevel traceLevel,
            Exception exception,
            string message,
            params object[] args)
        {
            try
            {
                if (classInstanceType == typeof (Tracing))
                {
                    //Obviously useless....
                    //Quick hack 
                    Debug.WriteLine("*** Error: Wrong ClassInstanceType");
                    traceLevel = TraceLevel.Verbose;
                }

                if (traceLevel == TraceLevel.Off)
                {
                    return;
                }

                TraceSwitch traceSwitch = _traceSwitchService.GetTraceSwitchByType(classInstanceType);

                if (traceSwitch.Level < traceLevel)
                {
                    return;
                }


                //Now waste time building string message:
                message = BuildMessage(message, args);

                switch (traceLevel)
                {
                    case TraceLevel.Verbose:
                        //Only when verbose is Debug stream used...
                        message = PrefixMessage(traceLevel, message);
                        Debug.WriteLine(message);
                        ResetFlushTimer();
                        break;
                        //The rest goes to Trace stream...
                        //with full information as specified in config files...
                    case TraceLevel.Info:
                        Trace.TraceInformation(message);
                        ResetFlushTimer();
                        break;
                    case TraceLevel.Warning:
                        Trace.TraceWarning(message);
                        ResetFlushTimer();
                        break;
                    case TraceLevel.Error:
                        Trace.TraceError(message);
                        ResetFlushTimer();
                        break;
                }

                if (exception != null)
                {
                    int counter = 0;
                    string prefix = "";
                    Exception e = exception;
                    while (e != null)
                    {
                        Trace.TraceError(e.Message);
                        e = e.InnerException;
                        if (prefix.Length == 0)
                        {
                            prefix = "InnerException:";
                        }
                        if (counter == 5)
                        {
                            break;
                        }
                        counter++;
                    }
                    Trace.TraceError(exception.StackTrace);
                    Trace.Flush();
                }
            }
            catch (Exception exception2)
            {
                Debug.WriteLine(exception2.Message);
            }
        }


        private static void ResetFlushTimer()
        {
            _Timer.Stop(); //Kill last iteration:
            _Timer.Start(); //Start again
        }

        private static void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            //Same thing as Debug.Flush:
            Trace.Flush();
        }


        private static string BuildMessage(string message, params object[] args)
        {
            string msg;
            try
            {
                msg = string.Format(
                    CultureInfo.InvariantCulture,
                    message,
                    args);
            }
            catch
            {
                msg = "[ERR: string.Format() failed]" + message;
            }
            return msg;
        }

        /// <summary>
        ///   Prefixes a message intended for Debug output with 
        ///   same stuff that Trace prepends.
        ///   <para>
        ///     Invoked only if going to Debug listeners (ie Verbose).
        ///   </para>
        /// </summary>
        /// <param name = "traceLevel">
        ///   The level that the relevant TraceSwitch has to be at to log this message.
        /// </param>
        /// <param name = "message">The message.</param>
        /// <returns></returns>
        private static string PrefixMessage(TraceLevel traceLevel, string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return string.Empty;
            }

            try
            {
                //string traceLevelText = traceLevel.ToString();

                return
                    string.Format(
                        _invariantCulture,
                        "{0} {1}: {2}: [{3}] {4}",
                        Path.GetFileName(
                            Process.GetCurrentProcess()
                                .MainModule
                                .FileName),
                        traceLevel.ToString().PadRight(7, '.'),
                        "0",
                        DateTime.Now.ToString("MM-dd HH:mm:ss"),
                        message);
            }
// ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
// ReSharper restore EmptyGeneralCatchClause
            {
            }
            return message;
        }
    }

//~class
}