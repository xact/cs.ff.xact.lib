﻿namespace XAct.Initialization
{
    using System;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Attribute to associate to define initializers.
    /// </summary>
    public class InitializerAttribute : Attribute, IHasEnabledReadOnly, IHastInitializationStage, IHasTagReadOnly, IHasOrderReadOnly, IHasDescriptionReadOnly
    {
        /// <summary>
        /// Gets or sets the name of the module ('Core', 'Module1', etc.)
        /// </summary>
        /// <value>
        /// The module.
        /// </value>
        public string ModuleName { get; private set; }

        /// <summary>
        /// Gets the 'set' tag of the object.
        /// Used most often to describe the 'Tier' to initialize, 
        /// <para>
        /// Default is <c>String.Empty</c> (not null, which as a search term means all).
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        public string Tag { get; private set; }

        /// <summary>
        /// Gets or sets the initialization stage.
        /// <para>
        /// Default is 0 (AppInit).
        /// </para>
        /// </summary>
        /// <value>
        /// The initialization stage.
        /// </value>
        public InitializationStage InitializationStage { get; private set; }
 
        /// <summary>
        /// Gets or sets the order.
        /// <para>
        /// Default value = 5.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="InitializerAttribute"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; private set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="InitializerAttribute"/> class.
        /// </summary>
        public InitializerAttribute()
        {
            Order = 5;

            //Default is String.Empty to allow for Searching by Null to mean 'all', 
            //versus default ones...
            Tag = string.Empty;

            InitializationStage = InitializationStage.S02_Initialization;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InitializerAttribute" /> class.
        /// </summary>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="tag">The tag (string.Empty).</param>
        /// <param name="initializationStage">The initialization stage (Default=InitializationStage.Init).</param>
        /// <param name="order">The order (Default=5).</param>
        /// <param name="description">The description.</param>
        public InitializerAttribute(string moduleName, string tag, InitializationStage initializationStage, int order = 5, string description=null)
        {
            Contract.Assert(!moduleName.IsNullOrEmpty(),"InitializerAttribute requires a ModuleName.");
            this.ModuleName = moduleName;
            Tag = tag ?? string.Empty;
            this.InitializationStage = initializationStage;
            Order = order;
            Description = description;
        }

    }
}
