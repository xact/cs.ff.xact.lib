﻿
namespace XAct.Initialization
{
    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// Consider design of MVC ActionFilters -- which are executed in following order: Authorize, Action, Result, OutputCache, Exception, Error.
    /// </internal>
    public enum InitializationStage
    {
        /// <summary>
        /// The value has not been set.
        /// <para>
        /// This is an error state.
        /// </para>
        /// </summary>
        Undefined=0,

        /// <summary>
        /// Steps that happen before the Bootstrapper has initialized any services.
        /// <para>
        /// Examples:
        /// </para>
        /// <para>
        /// * Invoke a step that registers the Services.
        /// </para>
        /// <para>Value = 1</para>
        /// </summary>
        S01_PreInitialization = 1,


        /// <summary>
        /// All initilialization happens *after* Bootstrapper has Registered Services. 
        /// <para>
        /// Invoke using
        /// </para>
        /// <para>
        /// Examples:
        /// </para>
        /// <para>
        /// * Invoke ModelBuilders to create Db Schema.
        /// </para>
        /// <para>
        /// * Registering ObjectMappingService Maps (doesn't rely on Resources, Db, etc. until first invoked).
        /// </para>
        /// <para>Value = 2</para>
        /// </summary>
        S02_Initialization = 2,

        /// <summary>
        /// Integration steps (DbConnection will be used etc).
        /// <para>
        /// Examples:
        /// </para>
        /// <para>
        /// </para>
        /// <para>
        /// * Seeding (Seeding can't occur till after Model Builder occurred in <see cref="S02_Initialization"/> step).
        /// </para>
        /// <para>Value = 3</para>
        /// </summary>
        S03_Integration = 3,

        /// <summary>
        /// Second stage of integration, relying on <see cref="S03_Integration"/> having been completed.
        /// <para>
        /// Examples:
        /// </para>
        /// <para>
        /// </para>
        /// <para>
        /// * Cache priming (Reference data can't be invoked from Db until Seeding occurred in <see cref="S03_Integration"/>).
        /// </para>
        /// <para>Value = 4</para>
        /// </summary>
        S04_PostIntegration = 4,

    }
}
