﻿
namespace XAct.Initialization
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHastInitializationStage
    {
        /// <summary>
        /// Gets or sets the initialization stage.
        /// </summary>
        /// <value>
        /// The initialization stage.
        /// </value>
        InitializationStage InitializationStage
        {
            get;
            //set;
        }
    }
}
