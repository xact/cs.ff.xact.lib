﻿
namespace XAct.Constants
{
    public static class PerformanceCounters
    {


        public static string RepositoryServiceQueriesAll = "RepositoryService.Queries.All";
        public static string RepositoryServiceQueriesDeferred = "RepositoryService.Queries.Deferred";
        public static string RepositoryServiceQueriesImmediate = "RepositoryService.Queries.Immediate";
        public static string RepositoryServiceQueriesCommitDuration = "RepositoryService.Queries.Commit.Duration";

        public static string RepositoryServiceQueriesCommitEntitiesChanged = "RepositoryService.Queries.Commit.Entities.Changed";
        public static string RepositoryServiceQueriesCommitEntitiesAdded = "RepositoryService.Queries.Commit.Entities.Added";
        public static string RepositoryServiceQueriesCommitEntitiesUpdated = "RepositoryService.Queries.Commit.Entities.Updated";
        public static string RepositoryServiceQueriesCommitEntitiesDeleted = "RepositoryService.Queries.Commit.Entities.Deleted";

    }
}
