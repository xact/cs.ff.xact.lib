﻿
namespace XAct.Constants
{
    /// <summary>
    /// Class of Constants used when serializing WCF elements.
    /// </summary>
    public class Serialization
    {

        /// <summary>
        /// (Protected) Initializes a new instance of the <see cref="Serialization"/> class.
        /// </summary>
        protected Serialization(){}

        /// <summary>
        /// Default root namespace,
        /// used in various parts of library 
        /// to serialize and version WCF.
        /// <para>
        /// Value is: 
        /// <![CDATA[http://xact-solutions.com/contracts]]>
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage would be as follows:
        /// <code>
        /// <![CDATA[
        ///   [DataContract(Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
         ///  public enum TimeInterval
         ///  {
         ///    /// <summary>Undefined. Value=0</summary>
         ///    [EnumMember]
         ///    Undefined = 0,
         ///    /// <summary>Tick. Value=1</summary>
         ///    [EnumMember]
         ///    Tick = 1,
         ///    /// <summary>Millisecond. Value=2</summary>
         ///    [EnumMember]
         ///    Millisecond = 2,
         ///    ...
         ///    /// <summary>A Millenium. Value=16</summary>
         ///    [EnumMember]
         ///    Millenium = 16
        ///    }
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        public static string DefaultNamespaceRoot = "http://xact-solutions.com/contracts";

    }
}
