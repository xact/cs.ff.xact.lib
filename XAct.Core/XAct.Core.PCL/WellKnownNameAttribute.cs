﻿using System;

namespace XAct
{

    /// <summary>
    /// An attribute that allows attaching a Name to an object.
    /// </summary>
    public class WellKnownNameAttribute : Attribute, IHasName
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Wells the known name attribute.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public WellKnownNameAttribute(string name)
        {
            Name = name;
        }
    }
}
