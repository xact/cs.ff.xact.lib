﻿namespace XAct.Library.Settings
{
    /// <summary>
    /// Settings regarding the library's 
    /// handling of IO operations.
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IO
// ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// In the application needs to be a Green
        /// install, set this to False so that IO
        /// operations prefer using IsolatedStorage.
        /// <para>
        /// Default setting is <c>false</c>.
        /// </para>
        /// </summary>
        public static bool PreferIsolatedStorage = false;
    }
}
