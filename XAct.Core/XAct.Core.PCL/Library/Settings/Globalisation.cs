namespace XAct.Library.Settings
{
    using System.Globalization;

    /// <summary>
    /// 
    /// </summary>
    public static class Globalisation
    {
        /// <summary>
        /// Gets or sets the default culture.
        /// <para>Default value = 'en'</para>
        /// </summary>
        /// <value>
        /// The default culture.
        /// </value>
        public static CultureInfo DefaultUICulture = new CultureInfo("en");


        /// <summary>
        /// Recursion is a neat feature in the RepositoryResourceManager - but it eats up about 50% of the speed.
        /// <para>
        /// Your choice.
        /// </para>
        /// </summary>
        public static bool AllowResourceRecursion = true;
    }
}