namespace XAct.Library.Settings
{
    using XAct.Services;

    /// <summary>
	/// 
	/// </summary>
	public static class Bindings
	{

        public static bool ScanByInterfaces
        {
            get { return _scanByInterfaces; }
            set { _scanByInterfaces = value; }
        }
        private static bool _scanByInterfaces = true;

        /// <summary>
		/// The path that XActLib BootStrapper 
		/// searches in for assemblies.
		/// <para>
		/// If left null, defaults to <c>AppDomain.CurrentDomain.SetupInformation.PrivateBinPath</c>
		/// </para>
		/// </summary>
		public static string AssemblySearchPath { get; set; }


		/// <summary>
		/// Gets or sets the override name of the DependencyInjectionContainer's FQ AssemblyName
		/// (name, version, key)
		/// <para>
		/// By default, empty.
		/// </para>
		/// </summary>
		/// <value>
		/// The name of the DependencyInjectionContainer FQN assembly.
		/// </value>
		public static string IoCFQAssemblyNameOverride { get; set; }

		/// <summary>
		/// Gets or sets the default service lifespan.
		/// </summary>
		/// <value>
		/// As all library services are stateless, 
		/// the default service 
		/// lifespan for a Stateless service is 
		/// <see cref="BindingLifetimeType.SingletonScope"/>
		/// (it is up to individual services to consider
		/// their threading requirements).
		/// </value>
		public static BindingLifetimeType DefaultServiceLifespan
		{
			get { return _defaultServiceLifespan; }
			set { _defaultServiceLifespan = value; }
		}
		private static BindingLifetimeType _defaultServiceLifespan =
			BindingLifetimeType.SingletonScope;

	}
}