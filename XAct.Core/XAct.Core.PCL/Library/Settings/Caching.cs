namespace XAct.Library.Settings
{
    using System;

    /// <summary>
    /// Default Db Settings.
    /// </summary>
	public class Caching
    {
        /// <summary>
        /// The Prefix put in front of XActLib caching elements.
        /// </summary>
        public static string XActLibPrefix = "XActLib:";

        /// <summary>
        /// The default amount of time to cache reference data.
        /// </summary>
        public static TimeSpan DefaultShortCachingTimeSpan = TimeSpan.FromMinutes(1);


        /// <summary>
        /// The default medium caching time span
        /// </summary>
        public static TimeSpan DefaultMediumCachingTimeSpan = TimeSpan.FromMinutes(5);

        /// <summary>
        /// The default amount of time to cache reference data.
        /// </summary>
        public static TimeSpan DefaultReferenceCachingTimeSpan = TimeSpan.FromMinutes(20);
	}
}