﻿namespace XAct.Library.Settings
{
    /// <summary>
    /// Public settings regarding State management in the application.
    /// </summary>
    public static class State
    {
        /// <summary>
        /// The when HTTP state is not available make context state thread based rather than singleton
        /// </summary>
        public static bool WhenHttpStateIsNotAvailableMakeContextStateThreadBasedRatherThanSingleton = true;
    }
}
