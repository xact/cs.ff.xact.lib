﻿namespace XAct.Library.Settings
{
    using System;

    public static class Comparison
    {
        /// <summary>
        /// When comparing DateTime, allow for the following differential between the two DateTimes:
        /// </summary>
        public static TimeSpan DateTimeComparisonTimeSpan = new TimeSpan(); 
    }
}