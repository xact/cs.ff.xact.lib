﻿namespace XAct.Library.Settings
{
    using System;
    using XAct.Services;

    /// <summary>
    /// Static Class of Settings relevant to the IoC,
    /// most of them set when 
    /// the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
    /// </summary>
    public static class IoC
    {

        /// <summary>
        /// A flag to tell Bindings class to use a temp AppDomain when scanning for Resources.
        /// </summary>
        public static bool UseTempAppDomainWhenScanningForServices = false;


        /// <summary>
        /// <para>
        /// Delegate set when the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
        /// </para>
        /// </summary>
        public static Action<IBindingDescriptor> RegisterBindingMethod;

        /// <summary>
        /// The register binding check method
        /// <para>
        /// Delegate set when the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
        /// </para>
        /// </summary>
        public static Func<Type, string, bool> IsBindingRegisteredMethod;

        /// <summary>
        /// Resets all, clearing
        /// <see cref="XAct.Library.Status.Initialization.DependencyInjectionContainer"/>
        /// <see cref="XAct.Library.Status.Initialization.DependencyInjectionContainerConnector"/>
        /// <see cref="XAct.Library.Status.Initialization.DependencyInjectionServiceLocator"/>
        /// </summary>
        public static Action ResetAllMethod;

    }
}
