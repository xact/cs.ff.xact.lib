namespace XAct.Library.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public enum SeedingCommitLevel
    {
        /// <summary>
        /// Commit in all cases.
        /// <para>
        /// Use at the end of the main seeding method.
        /// </para>
        /// <para>
        /// Use also after a chunk of records that are needed in subsequent
        /// seeding (an example being after seeding <c>Category</c>, 
        /// which will be referenced
        /// by <c>Person</c> seeding).
        /// </para>
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        Always=0,

        /// <summary>
        /// Commit at logical intervals.
        /// <para>
        /// Put this after AddOrUpdating a whole group of entries.
        /// </para>
        /// <para>
        /// This is the default value for
        /// <see cref="XAct.Library.Settings.Db.CommitRegularlyDuringSeeding"/>
        /// </para>
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        Regularly=1,

        /// <summary>
        /// Commit at regular intervals.
        /// <para>
        /// Apply after each AddOrUpdate. Only required when debugging,
        /// trying to pin down exact line that is causing issues.
        /// </para>
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        Excessively_UseOnlyForDebuggingPurposes=2
    }

    /// <summary>
    /// The type of seeding this entails.
    /// </summary>
    public enum SeedingType
    {
        /// <summary>
        /// Do not Seed.
        /// </summary>
        SkipSeeding =-1,

        /// <summary>
        /// The immutable reference data.
        /// <para>
        /// Immutable data that can can be reseeded every time the application starts. 
        /// </para>
        /// <para>
        /// Use only for immutable data that you want to ensure is not 
        /// changed by support or end users.
        /// </para>
        /// <para>
        /// Value=0
        /// </para>
        /// </summary>
        SeedEveryTime = 0,

        /// <summary>
        /// The immutable reference data.
        /// <para>
        /// Immutable data can be reseeded every time the application
        /// starts if need be. 
        /// </para>
        /// <para>
        /// As its immutable, it probably won't
        /// affect any changes (unless you're setting Created/Edited
        /// values to CurrentTime, etc).
        /// </para>
        /// <para>
        /// IMPORTANT: Use with Caution.
        /// </para>
        /// <para>
        /// Value=1
        /// </para>
        /// </summary>
        ResetImmutableReferenceData = 1,

        /// <summary>
        /// 
        /// </summary>
        ResetMutableAppSettings = 2,

        /// <summary>
        /// Use for seeding Application Settings
        /// and other reference data that is is mutable
        /// from either Support or end users.
        /// <para>
        /// IMPORTANT: Use with Caution.
        /// </para>
        /// <para>
        /// Value=3
        /// </para>
        /// </summary>
        ResetMutableReferenceData=3,



        /// <summary>
        /// Resets demo data.
        /// <para>
        /// IMPORTANT: Use with Caution.
        /// </para>
        /// <para>
        /// Under most circumstances
        /// seeding should avoid reseting
        /// User data/categories, etc.
        /// </para>
        /// <para>
        /// Value=4
        /// </para>
        /// </summary>
        ResetMutableUserData=5
    }

    /// <summary>
    /// Default Db Settings.
    /// </summary>
	public class Db
    {
        /// <summary>
        /// Committing Migration seeding can take a lot of time,
        /// impacting development startup times.
        /// <para>
        /// It can save a little time to only commit at the end -- or at least in less places.
        /// </para>
        /// <para>
        /// Yet, for debugging purposes, it helps to pin down where exactly the issue is coming from.
        /// </para>
        /// <para>
        /// Hence this enum.
        /// </para>
        /// <para>
        /// The default value is <see cref="SeedingCommitLevel.Regularly"/>
        /// </para>
        /// </summary>
        public static SeedingCommitLevel CommitRegularlyDuringSeeding = SeedingCommitLevel.Regularly;


        /// <summary>
        /// The seeding type
        /// <para>
        /// Some seeding is only needed after first install -- some seeding is
        /// needed after deploying migrations. This flag indicates when to roll
        /// what seeding.
        /// </para>
        /// </summary>
        public static SeedingType SeedingType = SeedingType.ResetImmutableReferenceData;

        /// <summary>
        /// 
        /// </summary>
        /// <para>
        /// Default Value is '<c>XActLib_</c>'
        /// </para> 
        public static string DefaultXActLibDbTablePrefix
        {
            get { return _defaultXActLibDbTablePrefix??string.Empty; }
            set { _defaultXActLibDbTablePrefix = value; }
        }

        static string _defaultXActLibDbTablePrefix  = "XActLib_";

        /// <summary>
        /// The default database schema to use for 
        /// AppDbContexts used for accessing XActLib database
        /// tables.
        /// <para>Default is "XActLib" </para>
        /// </summary>
        public static string DefaultXActLibSchema { get { return _defaultXActLibSchema; } set
        {
            _defaultXActLibSchema = value;
        } }
        private static string _defaultXActLibSchema = "XActLib";


		/// <summary>
		/// Default connection string identifier.
		/// <para>
		/// Default Value is '<c>XActLib</c>'
		/// </para> 
		/// </summary>
		public static string DefaultXActLibConnectionStringSettingsName = "XActLib";

		/// <summary>
		/// Different engines use different Parameter Prefixes (SqlServer uses '@', Sqlite uses '?')
		/// <para>
		/// NOTE: Consider carefully whether you really need DbParameters - maybe Code First is
		/// a better way to deal with database queries.
		/// </para>
        /// <para>
        /// Default Value is '<c>@</c>'
        /// </para> 
        /// </summary>
		public static string DbParameterPrefix = "@";

		/// <summary>
		/// <para>
		/// Default value is <c>False</c>
		/// </para>
		/// </summary>
		public static bool SeedDemoEntries = false;


        /// <summary>
        /// Use this for hosting WCF when HttpContext compatibility
        /// cannot be turned on for WCF endpoint.
        /// </summary>
        public static bool ForceNewContextEveryTime = false;

	}
}