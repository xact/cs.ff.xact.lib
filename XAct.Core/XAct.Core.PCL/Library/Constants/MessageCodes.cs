﻿using XAct.Messages;

namespace XAct.Library.Constants
{
    public static class MessageCodes
    {
        public const int XActBaseMessageCode = 1000000;

        public const int OperationNotAuthenticated = XActBaseMessageCode + 1;
        public const int OperationNotAuthorized = XActBaseMessageCode + 2;
        public const int OperationNotPossible = XActBaseMessageCode + 3;
        public const int OperationValidationFailed = XActBaseMessageCode + 4;

        public const int ValueCannotBeNull = XActBaseMessageCode + 101;
        public const int ValueCannotBeEmpty = XActBaseMessageCode + 102;
        public const int ValueCannotBeParsed = XActBaseMessageCode + 103;
        public const int ValueCannotBeOutOfValidRange = XActBaseMessageCode + 104;

    }

    //public static class MessageCodes2
    //{
    //    [MessageCodeMetadata("XActMessageCode","OperationNotAuthenticated",1)]
    //    public MessageCode OperationNotAuthenticated = new MessageCode(1, Severity.BlockingWarning);
    //}
}