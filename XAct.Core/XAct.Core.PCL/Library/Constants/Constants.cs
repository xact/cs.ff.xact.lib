﻿namespace XAct.Library.Constants
{
    public static class DataContracts
    {
        /// <summary>
        /// The base data contract namespace
        /// </summary>
        public const string BaseDataContractNamespace = "http://xact-solutions.com/contracts/xact/";
        public const string BaseDataContract_0_1_Namespace = BaseDataContractNamespace + "v0.1/";
    }
}
