﻿// ReSharper disable CheckNamespace
namespace XAct.Library.Constants
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// 
    /// </summary>
    public class Compilation
    {
        /// <summary>
        /// The XAct Solutions, Inc. Signed Key
        /// <para>
        /// Value is '9f1417dfe4ed073e'
        /// </para>
        /// </summary>
        public const string AssemblyKey = "9f1417dfe4ed073e";


    }
}