﻿namespace XAct.Library.Constants
{
    /// <summary>
    /// 
    /// </summary>
    public class UnitOfWork
    {
        /// <summary>
        /// The default context key
        /// </summary>
        public const string DefaultContextKey = "Default";
    }

    public static class AppSettingKeys
    {
        /// <summary>
        /// WIthin DbContext constructor, ask whether User wants to hook in debugger.
        /// <para>Useful when diagnosing failres of invoking a DBContext from the Package Manager Console, or build server.</para>
        /// </summary>
        public const string EnableDebugger = "EnableDebugger";

        /// <summary>
        /// Turns on NConfig if requested.
        /// <para>
        /// At present, suggest that NConfig is only used in Dev, not Prod.
        /// </para>
        /// </summary>
        public const string EnableNConfig = "EnableNConfig";
    }
}