﻿
namespace XAct.Library.Status
{
    using XAct.Services.IoC.Initialization;

    ///// <summary>
    ///// A class of properties describing stats regarding the library's initialization/binding sequence.
    ///// <para>
    ///// Usually used to figure out if bootstrapping, etc. succeeded as planned.
    ///// </para>
    ///// </summary>
    //public static class Initialization
    //{
    //    /// <summary>
    //    /// The IUnityContainer or IKernel object:
    //    /// <para>
    //    /// Property set when the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
    //    /// </para>
    //    /// </summary>
    //    public static object DependencyInjectionContainer;


    //    ///// <summary>
    //    ///// The IServiceLocator object, that wraps
    //    ///// the UnityServiceLocator, which in turn wraps
    //    ///// IUnityContainer.
    //    ///// <para>
    //    ///// Property set when the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
    //    ///// </para>
    //    ///// </summary>
    //    //public static object DependencyInjectionServiceLocator;

    //    /// <summary>
    //    /// The dependeny injection container connector
    //    /// (UnityServiceLocator or NinjectConnector)
    //    /// <para>
    //    /// Property set when the Bootstrapper (eg: <c>XAct.Services.IoC.UnityBootstrapper.Initialize</c>) is invoked.
    //    /// </para>
    //    /// </summary>
    //    public static object DependencyInjectionContainerConnector;


    //    /// <summary>
    //    /// Gets or sets the information regarding the initialization/bootstrapping process.
    //    /// <para>
    //    /// Null until bootstrapping sequence is actually commenced.
    //    /// </para>
    //    /// </summary>
    //    public static IInitializeLibraryBindingsResults BindingResults { get; set; }
    //}
}
