﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public interface IPagedResponse<TItem> : IResponse, IPageable
    {
        /// <summary>
        /// Gets or sets the total number of records
        /// in the datastore (usually larger than
        /// <see cref="IPageable.TotalCount"/>, which is just
        /// the Size of the Matched Records prior to 
        /// paging being applied).
        /// <para>
        /// The default (unfiled value) is -1.
        /// </para>
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        int TotalCountInDataStore { get; set; }

    }
}