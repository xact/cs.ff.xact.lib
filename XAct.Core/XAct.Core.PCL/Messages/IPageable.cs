﻿namespace XAct.Messages
{
    /// <summary>
    /// An interface for iterating through data, in steps/pages.
    /// </summary>
    public interface IPageable
    {
        /// <summary>
        ///   Gets or sets the total number of items in the result set (before Paging Applied).
        /// </summary>
        /// <value>The total count.</value>
        int TotalCount { get; set; }

        /// <summary>
        /// Gethe current page size.
        /// </summary>
        int PageSize { get; set; }

        /// <summary>
        /// Gets the total number of pages.
        /// </summary>
        int TotalPages { get; }

        /// <summary>
        /// Gets the current page index (1 based, due to most used on UI).
        /// </summary>
        int PageIndex { get; set; }

        /// <summary>
        /// A boolean indicating whether there is a previous page
        /// (ie, PageIndex > 0)
        /// </summary>
        bool HasPreviousPage { get; }

        /// <summary>
        /// A flag indicating whether there is a later page to switch to.
        /// </summary>
        bool HasNextPage { get; }
    }
}