namespace XAct.Messages
{
    /// <summary>
    /// 
    /// </summary>
    public interface IQuerySearchTerm
    {
        /// <summary>
        /// Gets or sets the key (ie, the name of the column, such as 'LastName' or 'Age').
        /// </summary>
        /// <value>The key.</value>
        string Key { get; set; }

        /// <summary>
        /// Gets or sets the *serialized* value to search for (eg: 'Smith', or '33').
        /// <para>
        /// The Type is known by the eventual handler, and therefore it is up
        /// to the handler to correctly parse the string serialized value into the
        /// target type.
        /// </para>
        /// </summary>
        /// <value>The value.</value>
        string Text { get; set; }

        /// <summary>
        /// Gets or sets the equality (eg: Equals).
        /// </summary>
        /// <value>The equality.</value>
        Equality Equality { get; set; }

        /// <summary>
        /// Gets or sets the parsed value.
        /// <para>
        /// This object is not passed across the wire, but is used to pass up and down the stack the value
        /// parsed from <see cref="Text"/>.
        /// </para>
        /// </summary>
        /// <internal>
        /// <para>
        /// IMPORTANT: Remember that WCF cannot serialize Objects.
        /// </para>
        /// </internal>
        /// <value>The value.</value>
        object Value { get; set; }
    }
}