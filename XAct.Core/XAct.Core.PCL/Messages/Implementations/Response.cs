﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Generic response object for null data response (ie, just an acknoledgement of success or failure).
    /// <para>
    /// See <see cref="Response{TData}"/> for carrying information back.
    /// </para>
    /// <para>
    /// The class is marked <c>sealed</c> as Inheritence (and therefore the use of KnownType attribute
    /// is considered bad WCF design as it affects performance negatively).
    /// </para>
    /// </summary>
    [DataContract(Name = "Response", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public sealed class Response : IResponse
    {
        private static readonly Severity[] _noSuccess = new[] {Severity.Error, Severity.BlockingWarning};

        /// <summary>
        /// Gets or sets the time it took to perform the query.
        /// <para>
        /// Note that the property is not marked serializable
        /// as it is only for returning the value to the requestor
        /// in the Application layer, to be transferred to a 
        /// <see cref="PagedResponse{TItem}"/> object.
        /// </para>
        /// <para>
        /// Do not mark with WCF or other serialization directives
        /// as it is only required internally.
        /// </para>
        /// </summary>
        /// <value>The time elapsed.</value>
        [DataMember(Name = "TimeElapsed")]
        public TimeSpan TimeElapsed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:XAct.Messages.Response"/> 
        /// has any messages with <see cref="Severity"/> of <see cref="Severity.Error"/>
        /// or <see cref="Severity.BlockingWarning"/>
        /// </summary>
        /// <value>
        /// <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success
        {
            get
            {
                return _success = Response.SuccessMethod(this);
            }
        }

        [DataMember(Name = "Success")]
        private bool _success;

        /// <summary>
        /// Gets or sets a collection of optional messages that describe the
        /// reason why <see cref="Success"/> is as it is.
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        public List<Message> Messages
        {
            get
            {
                var result = this._messages = _messages ?? (_messages = new List<Message>());
                return result;
            }
        }
        [DataMember(Name = "Messages")] private List<Message> _messages;


        /// <summary>
        /// Gets or sets an list of additional metadata that upper tiers can use as they need.
        /// </summary>
        /// <value>
        /// The metadata.
        /// </value>
        public List<Metadata> Metadata { get { return _metadata; } }
        [DataMember(Name = "Metadata")]
        private List<Metadata> _metadata;  

        /// <summary>
        /// Initializes a new instance of the <see cref="Response" /> class.
        /// </summary>
        public Response()
        {
            OnDeserializing(default(StreamingContext));
        }


        /// <summary>
        /// Invoked prior to serialization, to persist the private _success property with correct value.
        /// </summary>
        /// <param name="context"></param>
        /// <internal>
        /// Don't see a ISerializationCallback to use.
        /// </internal>
        [OnSerialized]
        private void OnSerialized(StreamingContext context)
        {
            _success = _messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

            //_success = this.Messages.All(m => !new[] { Severity.Error, Severity.BlockingWarning }.Contains(m.Severity));
        }

        [OnDeserializing]
        public void OnDeserializing(StreamingContext streamingContext)
        {
            if (_messages == null) { _messages = new List<Message>(); }
            if (_metadata == null) { _metadata = new List<Metadata>(); }
        }


        ///// <summary>
        ///// Adds the specified message to the
        ///// Response object's Message array.
        ///// </summary>
        ///// <param name="severity">The severity.</param>
        ///// <param name="messageCode">The message code.</param>
        ///// <param name="arguments">The arguments.</param>
        //public void AddMessage(Severity severity, ulong messageCode, params string[] arguments)
        //{
        //    Message message = new Message(severity, messageCode, arguments);

        //    //Notice how we do not set the Text Property.

        //    //That is set later using ICultureSpecificMessageService.

        //    _messages.Add(message);
        //}

        /// <summary>
        /// Adds the specified message to the
        /// Response object's Message array.
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="arguments">The arguments.</param>
        public bool AddMessage(MessageCode messageCode, params string[] arguments)
        {
            //Notice how we do not set the Text Property.

            //That is set later using ICultureSpecificMessageService.

            Message message = new Message(messageCode, arguments);

            this.Messages.Add(message);

            return this.Success;
        }


        public static Func<IResponse,bool> SuccessMethod 
        {
            get
            {

                //return
                //    _success =
                //    _messages.All(m => !new[] {Severity.Error, Severity.BlockingWarning}.Contains(m.Severity));

                return 
                    _successMethod ?? 
                    (
                    _successMethod = (x)=> x.Messages.All(m => ! _noSuccess.Contains(m.MessageCode.Severity))
                    );
            }
            set
            {
                _successMethod = value;
            }
        }

        private static Func<IResponse, bool> _successMethod;

    }

}