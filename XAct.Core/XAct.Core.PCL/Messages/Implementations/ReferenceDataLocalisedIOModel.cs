﻿namespace XAct.Messages
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    [DataContract]
    public class ReferenceDataIOModel :IReferenceDataIOModel
    {
        /// <summary>
        /// Gets or sets the key (Datastore Id).
        /// <para>Member defined in<see cref="T:XAct.IHasKey" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }

        /// <summary>
        /// The visible Text displayed in a drop down or similar UI 
        /// element.
        /// <para>
        /// Note that the Text should be retrieved
        /// in a culture-specific manner,
        /// and cached in the front tier.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Text { get; set; }


        /// <summary>
        /// Gets the description of the <see cref="Text"/>.
        /// <para>
        /// In the era of drop downs (UI's of the turn of the century)
        /// there was almost never a need for a Description (few programs
        /// showed a tooltip when an option was selected).
        /// </para>
        /// <para>
        /// But in the era of tablets, where dropdowns are too small for fingers,
        /// the preferred option is to render lists -- often with a title (the 
        /// <see cref="Text"/>) and a little <see cref="Description"/>
        /// that goes with it. Hence the inclusion of <see cref="Description"/>
        /// in the ReferenceData object.
        /// </para>
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        [DataMember]
        public virtual string Description { get; set; }

        /// <summary>
        /// An optional element to associate to the element.
        /// </summary>
        [DataMember]
        public virtual string Tag { get; set; }

        /// <summary>
        /// Gets the string that can be used as filter.
        /// <para>
        /// By convention the syntax is similar to CSV,
        /// but with ! for NOT clauses, &amp; for AND, etc:
        /// <example>
        /// <![CDATA[
        /// AA;!BB;CC&DD;CC&!DD
        /// ]]>
        /// </example>
        /// </para>
        /// <para>Member defined in<see cref="T:XAct.IHasFilter" /></para>
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
        [DataMember]
        public virtual string Filter { get; set; }


        /// <summary>
        /// Gets a collection of additional metadata
        /// that can be associated to the object.
        /// </summary>
        /// <value>
        /// The metadata.
        /// </value>
        public virtual ICollection<Metadata> Metadata
        {
            get { return _metadata ?? (_metadata = new Collection<Metadata>()); }
        }
        [DataMember(Name="Metadata")] private ICollection<Metadata> _metadata;


        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceDataIOModel"/> class.
        /// </summary>
        public ReferenceDataIOModel()
        {
            OnDeserializing(default(StreamingContext));
        }




        /// <summary>
        /// Called when serializing.
        /// </summary>
        /// <internal>
        /// We can't serialize the method...but we 
        /// can freeze the result of the method call:
        /// </internal>
        /// <param name="streamingContext">The streaming context.</param>
        /// <internal><para>8/17/2011: Sky</para></internal>
        [OnDeserializing]
// ReSharper disable UnusedMember.Local
// ReSharper disable UnusedParameter.Local
        private void OnDeserializing(StreamingContext streamingContext)
        {
            if (_metadata == null)
            {
                _metadata  = new Collection<Metadata>();
            }
        }
    }
}
