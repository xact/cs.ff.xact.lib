﻿
namespace XAct.Messages
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary/>
    [DataContract]
    public abstract class RecursiveTextAndTitleBase<T> : IHasTextAndTitle, IHasChildrenCollectionReadOnly<T>
        where T : class, IHasTextAndTitle
    {


        /// <summary>
        /// Gets or sets the key.
        /// 
        /// <para>
        /// Member defined in<see cref="T:XAct.IHasKey"/>
        /// </para>
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The key.
        /// 
        /// </value>
        [DataMember]
        public string Key { get; set; }


        /// <summary>
        /// Gets or sets the title.
        /// <para>
        /// Member defined in the <see cref="IHasTitle" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [DataMember]
        public string Title { get; set; }


        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        [DataMember]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets a collection of nested child entries, if any exist.
        /// </summary>
        /// <value>
        /// The child entries.
        /// </value>
        public ICollection<T> Children
        {
            get
            {
                var result = this._children ?? (this._children = (ICollection<T>) new Collection<T>());
                return result;
            }
        }

        [DataMember] private ICollection<T> _children;


        protected RecursiveTextAndTitleBase()
        {
            OnDeserializing(default(StreamingContext));
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext streamingContext)
        {
            if (_children == null)
            {
                _children = new Collection<T>();
            }
        }
    }
}



