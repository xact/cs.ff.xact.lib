namespace XAct.Messages
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// See:
    /// <see cref="ReferenceDataBase{TId}"/>
    /// <see cref="ReferenceDataCodedBase{TId}"/>
    /// <see cref="ReferenceDataGuidIdBase"/>
    /// <see cref="ApplicationTennantIdSpecificReferenceDataGuidIdBase"/>
    /// <see cref="ApplicationTennantIdSpecificReferenceDataGuidIdCodedBase"/>
    /// </internal>
    [DataContract]
    public abstract class ReferenceDataGuidIdBase : ReferenceDataBase<Guid>,  IHasDistributedGuidIdAndTimestamp
    {

        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceDataGuidIdBase"/> class.
        /// </summary>
        protected ReferenceDataGuidIdBase()
        {
            this.GenerateDistributedId();
        }

    }
}