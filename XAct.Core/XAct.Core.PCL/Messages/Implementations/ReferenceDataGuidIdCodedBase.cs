﻿namespace XAct.Messages
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Messages;


    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// See:
    /// <see cref="ReferenceDataBase{TId}"/>
    /// <see cref="ReferenceDataCodedBase{TId}"/>
    /// <see cref="ReferenceDataGuidIdBase"/>
    /// <see cref="ApplicationTennantIdSpecificReferenceDataGuidIdBase"/>
    /// <see cref="ApplicationTennantIdSpecificReferenceDataGuidIdCodedBase"/>
    /// </internal>
    [DataContract]
    public abstract class ReferenceDataGuidIdCodedBase : ReferenceDataGuidIdBase, IHasCodedReferenceDataReadOnly<Guid>
    {
        [DataMember]
        public string Code { get; set; }

        
        /// <summary>
        /// Initializes a new instance of the <see cref="ReferenceDataGuidIdCodedBase"/> class.
        /// </summary>
        protected ReferenceDataGuidIdCodedBase()
        {
            this.GenerateDistributedId();
        }

    }
}