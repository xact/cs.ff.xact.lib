﻿
namespace XAct.Messages
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Metadata
    /// </summary>
    [DataContract(Name = "Metadata", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public class Metadata : KeyValueBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Metadata" /> class.
        /// </summary>
        public Metadata() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Metadata" /> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public Metadata(string key, string value) : base(key, value)
        {
        }

    }
}
