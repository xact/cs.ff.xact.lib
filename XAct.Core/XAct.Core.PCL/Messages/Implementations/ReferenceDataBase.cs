﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Entities;

    //[DataContract]
    //public abstract class ReferenceDataBase : ReferenceDataBase<int>
    //{
        
    //}

    /// <summary>
    /// <para>
    /// See <see cref="ReferenceDataGuidIdBase"/>
    /// </para>
    /// </summary>
    /// <typeparam name="TId"></typeparam>
    [DataContract]
    public abstract class ReferenceDataBase<TId> : IHasReferenceData<TId>
        where TId : struct
    {

        /// <summary>
        /// Gets or sets the datastore 
        /// identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual TId Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="T:XAct.IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember]
        public virtual int Order { get; set; }



        /// <summary>
        /// Gets or sets the text to display 
        /// in the dropdown, or list
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [DataMember]
        public virtual string Text { get; set; }

        /// <summary>
        /// Gets the description to render
        /// under the <see cref="Text"/>,
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        [DataMember]
        public virtual string Description { get; set; }


        /// <summary>
        /// Gets the resource Filter to use when looking
        /// up the Text and Description resources using
        /// <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        [DataMember]
        public virtual string ResourceFilter { get; set; }


        /// <summary>
        /// Gets the string that can be used as filter.
        /// <para>
        /// By convention the syntax is similar to CSV,
        /// but with ! for NOT clauses, &amp; for AND, etc:
        /// <example>
        /// <![CDATA[
        /// AA;!BB;CC&DD;CC&!DD
        /// ]]>
        /// </example>
        /// </para>
        /// <para>Member defined in<see cref="T:XAct.IHasFilter" /></para>
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
        [DataMember]
        public virtual string Filter { get; set; }


        /// <summary>
        /// An optional element to associate to the element.
        /// </summary>
        [DataMember]
        public virtual string Tag { get; set; }


    }
}