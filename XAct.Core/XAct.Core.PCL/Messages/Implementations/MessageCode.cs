﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;


    /// <summary>
    /// <para>
    /// MessageCode is a specific attempt at getting applications
    /// to be developed in a multi culture capable way right from the start.
    /// </para>
    /// <para>
    /// By removing the possiblity of embedding strings directly, 
    /// developers have to choose an existing MessageCode -- or register a new
    /// one. 
    /// </para>
    /// <para>
    /// As the application develops, more message codes, more Resource table entries
    /// etc. At the end, the application is easily translatable.
    /// </para>
    /// </summary>
    /// <internal>
    /// <para>
    /// Note: regarding conditional serialization of ResourceTags, consider
    /// http://www.codeproject.com/Articles/417168/Conditional-WCF-DataContract-Serialization-Using
    /// (although it's not PCL compliant, hence not implemented).
    /// </para>
    /// </internal>
    [DataContract(Name="MessageCode")]
    public struct MessageCode : IHasIdReadOnly<long>
    {
        /// <summary>
        /// Gets or sets the unique Identifier
        /// of the Message.
        /// <para>
        /// At the Presentation layer boundary, 
        /// the Id is then used (with or without 
        /// an application specific Suffix)
        /// to retrieve the cached Resource 
        /// string.
        /// </para>
        /// <para>
        /// IMPORTANT: the reason for the ulong
        /// is to handle porting legacy software, 
        /// where message codes often were created
        /// using Flag combinations.
        /// </para>
        /// <para>
        /// Parameter size is 8 bytes.
        /// </para>
        /// </summary>
        public long Id { get { return _id; } }
        [DataMember(Name="Id")] private long _id;




        ///// <summary>
        ///// The message is a System message, that should not be rendered to the end user.
        ///// <para>
        ///// Prefer the concept of setting severity to SystemInfo.
        ///// </para>
        ///// </summary>
        //public bool IsSystemMessage { get { return _isSystemMessage; } }
        //[DataMember(Name = "IsSystemMessage")]
        //private bool _isSystemMessage;

        
        /// <summary>
        /// Gets or sets the <see cref="Severity"/> of the message.
        /// <para>
        /// Note that when attached to implementations of 
        /// <see cref="IResponse"/>, the <see cref="IResponse.Success"/>
        /// value will Success unless there are any messages
        /// with a Severity of <c>Error</c>
        /// or <c>BlockingWarning</c>
        /// </para>
        /// <para>
        /// Parameter size is 1 byte.
        /// </para>
        /// </summary>
        public Severity Severity { get { return _severity; } }
        [DataMember(Name="Severity")]
        private Severity _severity;






        ////[IgnoreDataMember()]
        //public MessageCodeMetadata Metadata
        //{
        //    get { return _metadata; }
        //}
        //[DataMember(Name = "Metadata")]
        //private MessageCodeMetadata _metadata;

        /////// <summary>
        ///// Initializes a new instance of the <see cref="MessageCode" /> struct.
        ///// </summary>
        ///// <param name="id">The identifier.</param>
        ///// <param name="severity">The severity.</param>
        ///// <param name="argumentCount">The argument count.</param>
        ///// <param name="resourceSet">The resource set.</param>
        ///// <param name="resourceKey">The resource key.</param>
        //public MessageCode(long id, Severity severity
        //    //,string resourceSet = null, string resourceKey = null, byte argumentCount = 0
        //    )
        //    :this(id, severity
        //    //,new MessageCodeMetadata(resourceSet, resourceKey, argumentCount)
        //    )
        //{
        //}

        ///// <summary>
        ///// Initializes a new instance of the <see cref="MessageCode"/> struct.
        ///// </summary>
        ///// <param name="id">The identifier.</param>
        ///// <param name="severity">The severity.</param>
        ///// <param name="text">The text.</param>
        ///// <param name="argumentCount">The argument count.</param>
        //public MessageCode(long id, Severity severity, string text = null, byte argumentCount = 0)
        //    : this(id, severity, new MessageCodeMetadata(text, argumentCount))
        //{
        //}


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageCode"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="severity">The severity.</param>
        public MessageCode(long id, Severity severity
            //, MessageCodeMetadata messageCodeMetadata
            )
        {
            _id = id;
            _severity = severity;
            //_metadata = messageCodeMetadata ?? new MessageCodeMetadata(null, 0);
        }




        /// <summary>
        /// Provides a means to compare two <see cref="MessageCode"/>s.
        /// </summary>
        /// <param name="c1">The c1.</param>
        /// <param name="c2">The c2.</param>
        /// <returns></returns>
         public static bool operator == (MessageCode c1, MessageCode c2)
         {
             return c1.Equals(c2);
         }

        /// <summary>
         /// Provides a means to compare two <see cref="MessageCode"/>s.
         /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static bool operator !=(MessageCode c1, MessageCode c2)
        {

            return !c1.Equals(c2); // !(c1 == c2);
        }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(MessageCode other)
        {
            //if (other == null)
            //{
            //    return false;
            //}

            return Id == other.Id && Severity == other.Severity; // && ArgumentCount == other.ArgumentCount;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is MessageCode && Equals((MessageCode)obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)Severity;
                //hashCode = (hashCode * 397) ^ ArgumentCount.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            //string metaData = _metadata != null ? ", " + _metadata.ToString() : string.Empty;

            return "[MessageCode: [Severity:{0}, Code:{1}]"
                .FormatStringInvariantCulture(this.Severity, this.Id);
        }
    }
}
