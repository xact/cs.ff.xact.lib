﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Implementation of <see cref="IPagedSearchTermQuerySpecification"/>
    /// which provides paging information (by implementing 
    /// <see cref="IPagedQuerySpecification"/>) 
    /// but also Search and Filter information.
    /// to provide a means to transit a request over the wire.
    /// <para>
    /// Regarding Repositories and ORMs: although Linq 
    /// already provides Skip and Take, there are still several 
    /// arguments in favour of using an argument package instead.
    /// First of all, with just an IQueryable return value, there
    /// is no strong way of enforcing that the results are paged before returning
    /// the value.
    /// Even if a Skip and Take argument was added to the 
    /// <c>IRepository{TAggregateRootEntity}.GetByFilter</c> method,
    /// in order to enforce paging of some kind, it doesn't provide any 
    /// enforcements of constraints of size.
    /// In addition, with just Linq, there is no way of returning
    /// a value specifying how many records are in the Db.
    /// </para>
    /// </summary>
    [DataContract(Name = "PagedSearchTermQuerySpecification", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public sealed class PagedSearchTermQuerySpecification : IPagedSearchTermQuerySpecification //PCL, IDeserializationCallback

    {

        /// <summary>
        /// Gets or sets the zero-based index of the page to return.
        /// </summary>
        /// <value>
        /// The index of the page.
        /// </value>
        // ReSharper disable ConvertToAutoProperty
        [DataMember(Name = "PageIndex")]
        public int PageIndex { get; set; } 

        /// <summary>
        /// Gets or sets the size of the page to return.
        /// </summary>
        /// <value>
        /// The size of the page.
        /// </value>
        [DataMember(Name = "PageSize")]
        public int PageSize { get; set; }



        /// <summary>
        /// Gets a value indicating whether to return the total number of records.
        /// <para>
        /// The default value is <c>True</c>, even if it does take a little longer
        /// (a second query).
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the Total Record count is important; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "RetrieveTotalRecordsInSource")]
        public bool RetrieveTotalRecordsInSource { get; private set; }


        /// <summary>
        /// Gets or sets the total records found.
        /// <para>
        /// The default (unfiled value) is -1.
        /// </para>
        /// <para>
        /// Note that the property is not marked serializable
        /// as it is only for returning the value to the requestor
        /// in the Application layer, to be transferred to a 
        /// <see cref="PagedResponse{TItem}"/> object.
        /// </para>
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        public int TotalRecordsInSource { get { return _totalRecordsInSource; } set { _totalRecordsInSource = value; } }
        private int _totalRecordsInSource = -1;

        /// <summary>
        /// Gets or sets the time it took to perform the query.
        /// <para>
        /// Note that the property is not marked serializable
        /// as it is only for returning the value to the requestor
        /// in the Application layer, to be transferred to a 
        /// <see cref="PagedResponse{TItem}"/> object.
        /// </para>
        /// </summary>
        /// <value>The time elapsed.</value>
        public TimeSpan TimeElapsed { get; set; }




        /// <summary>
        /// Gets or sets the search terms.
        /// </summary>
        /// <value>The search terms.</value>
        [DataMember(Name="SearchTerms")]
        public QuerySearchTerm[] SearchTerms { get; set; }


        /// <summary>
        /// Gets or sets an optional array of <see cref="QuerySortSpecification"/>.
        /// </summary>
        /// <value>The sort specifications.</value>
        [DataMember(Name = "SortSpecifications")]
        public QuerySortSpecification[] SortSpecifications { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="PagedSearchTermQuerySpecification"/> class.
        /// </summary>
        /// <param name="searchTerms">The search terms.</param>
        /// <param name="sortSpecifications">The sort specifications.</param>
        /// <param name="pageIndex">0 based Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="retrieveTotalRecords">if set to <c>true</c> [retrieve total records].</param>
        public PagedSearchTermQuerySpecification(QuerySearchTerm[] searchTerms=null, QuerySortSpecification[] sortSpecifications=null, int pageIndex = 0, int pageSize = 20, bool retrieveTotalRecords = false)
         {

             PageIndex = pageIndex;
             PageSize = pageSize;
             RetrieveTotalRecordsInSource = retrieveTotalRecords;

            SearchTerms = searchTerms;
             SortSpecifications = sortSpecifications;

            OnDeserializing(default(StreamingContext));
         }


        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach(QuerySearchTerm st in this.SearchTerms)
            {
// ReSharper disable RedundantToStringCall
                sb.Append(st.ToString());
// ReSharper restore RedundantToStringCall
            }

            
            return
                "PagedStringQuerySpecification(SearchTerms:{0}, PageIndex:{1}, PageSize,{2}, RetriveTotalRecords:{3}).{4}"
                    .FormatStringCurrentCulture(sb.ToString(), PageIndex, PageSize,
                                                 RetrieveTotalRecordsInSource, base.ToString());
        }



        [OnDeserializing]
        public void OnDeserializing(StreamingContext streamingContext)
        {
            if (SearchTerms == null) { SearchTerms = new QuerySearchTerm[0]; }
        }

    }
}
