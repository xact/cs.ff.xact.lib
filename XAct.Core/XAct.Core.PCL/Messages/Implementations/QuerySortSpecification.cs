﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;
    using XAct.Collections;

    /// <summary>
    /// An implementation of the <see cref="IQuerySortSpecification"/>
    /// <para>
    /// Used by <see cref="PagedSearchTermQuerySpecification"/>
    /// </para>
    /// </summary>
    [DataContract(Name = "QuerySortSpecification", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public sealed class QuerySortSpecification : IQuerySortSpecification
    {
        /// <summary>
        ///   Gets the sort expresion.
        /// </summary>
        /// <value>The sort expresion.</value>
        [DataMember(Name = "Key")]
        public string Key { get; set; }

        /// <summary>
        ///   Gets the sort direction.
        /// </summary>
        /// <value>The sort direction.</value>
        [DataMember(Name = "Direction")]
        public SortDirection Direction { get; set; }

    }
}
