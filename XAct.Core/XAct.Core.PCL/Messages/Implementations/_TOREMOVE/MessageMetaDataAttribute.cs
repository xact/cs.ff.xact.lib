﻿
//namespace XAct.Messages
//{
//    /// <summary>
//    /// Attribute that can be applied to application specific enum
//    /// values in order to associate the right 
//    /// Severity to a message code.
//    /// <para>
//    /// Example of use:
//    /// <code>
//    /// <![CDATA[
//    /// enum SomeMsgEnum {
//    ///   SUCCESS=0
//    ///   [DefaultMessageSeverityAttribute(Severity.Error)]
//    ///   ERR_AUTHN=1,
//    ///   [DefaultMessageSeverityAttribute(Severity.Error)]
//    ///   ERR_AUTNZ
//    ///   [DefaultMessageSeverityAttribute(Severity.BlockingWarning)]
//    ///   ERR_VALIDATION
//    /// }
//    /// which when you write an application specific service
//    /// to attach messages, you can do something like the following:
//    ///
//    /// public ResponseMessageService  : IResponseMessageService {
//    /// private readonly IResponseMessageServiceCache _responseMessageServiceCache;
//    /// public ResponseMessageService(IResponseMessageServiceCache responseMessageServiceCache)
//    /// {
//    ///    _responseMessageServiceCache = responseMessageServiceCache;
//    /// }
//    /// public void AppendMessage(IResponse response, MessageCode messageCode, params string[] arguments)
//    /// {
//    ///     response.Messages.Add(CreateMessage(messageCode, arguments));
//    /// }
//    /// public Message CreateMessage(MessageCode messageCode, params string[] arguments)
//    /// {
//    ///     Severity severity = DetermineSeverity(messageCode);
//    ///     Message message = new Message(severity, (ulong) messageCode, arguments);
//    ///     return message;
//    /// }
//    /// public Severity DetermineSeverity(MessageCode messageCode)
//    /// {
//    ///     Severity severity;
//    ///     if (_responseMessageServiceCache.TryGetValue(messageCode, out severity))
//    ///     {
//    ///         _responseMessageServiceCache[messageCode] = severity = 
//    ///            messageCode.GetAttributeValue<MessageMetaDataAttribute, string>(a => a.DefaultSeverity);
//    ///     }
//    ///     return severity;
//    /// }
//    /// }
//    /// 
//    /// which you then use easily:
//    /// 
//    /// _responseMessageService.AppendMessage(response,MessageCodes.SUCCESS);
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// </summary>
//    public class MessageMetaDataAttribute : System.Attribute, IHasNote
//    {



//        /// <summary>
//        /// Gets or sets the default severity to associate to 
//        /// the given message/enum
//        /// </summary>
//        /// <value>
//        /// The default severity.
//        /// </value>
//        public Severity DefaultSeverity { get; private set; }

//        /// <summary>
//        /// The number of arguments expected when
//        /// string.Formatting the culture specific string
//        /// of a <see cref="MessagePresentationAttributes"/>.
//        /// </summary>
//        /// <value>
//        /// The message argument count.
//        /// </value>
//        public int MessageArgumentCount { get; set; }


//        /// <summary>
//        /// Gets or sets an optional note.
//        /// <para>
//        /// Member defined in the <see cref="IHasNote" /> contract.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The note.
//        /// </value>
//        public string Note { get; set; }

//        /// <summary>
//        /// Initializes a new instance of the
//        /// <see cref="MessageMetaDataAttribute" /> class.
//        /// </summary>
//        /// <param name="severity">The severity.</param>
//        /// <param name="messageArgumentCount">The message argument count.</param>
//        public MessageMetaDataAttribute(Severity severity, int messageArgumentCount = 0)
//        {
//            DefaultSeverity = severity;
//            MessageArgumentCount = messageArgumentCount;
//        }
//    }
//}
