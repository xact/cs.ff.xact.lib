﻿
//namespace XAct.Messages
//{
//    using System;

//    /// <summary>
//    /// The number of arguments expected when
//    /// string.Formatting the culture specific string
//    /// of a <see cref="MessagePresentationAttributes"/>.
//    /// </summary>
//    public class MessageArgumentCountAttribute : Attribute
//    {
//        /// <summary>
//        /// Gets or sets the number of Arguments
//        /// to be serialized into the 
//        /// <see cref="MessagePresentationAttributes.Message"/>
//        /// culture specific resource.
//        /// </summary>
//        /// <value>
//        /// The count.
//        /// </value>
//        public int Count { get; set; }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="MessageArgumentCountAttribute"/> class.
//        /// </summary>
//        /// <param name="count">The number of arguments.</param>
//        public MessageArgumentCountAttribute(int count)
//        {
//            Count = count;
//        }
//    }
//}
