// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// <para>
    /// Used by <see cref="PagedSearchTermQuerySpecification"/>
    /// </para>
    /// </summary>
    [DataContract(Name = "SearchTerm", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public sealed class QuerySearchTerm : IQuerySearchTerm 
    {
        /// <summary>
        /// Gets or sets the key (ie, the name of the column, such as 'LastName' or 'Age').
        /// </summary>
        /// <value>The key.</value>
        [DataMember(Name="Key")]
        public string Key { get; set; }
        /// <summary>
        /// Gets or sets the *serialized* value to search for (eg: 'Smith', or '33').
        /// <para>
        /// The Type is known by the eventual handler, and therefore it is up
        /// to the handler to correctly parse the string serialized value into the
        /// target type.
        /// </para>
        /// </summary>
        /// <value>The value.</value>
        [DataMember(Name="Text")]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the equality (eg: Equals).
        /// </summary>
        /// <value>The equality.</value>
        [DataMember(Name="Equality")]
        public Equality Equality { get; set; }

        /// <summary>
        /// Gets or sets the parsed value.
        /// <para>
        /// This object is not passed across the wire, but is used to pass up and down the stack the value
        /// parsed from <see cref="Text"/>.
        /// </para>
        /// </summary>
        /// <internal>
        /// <para>
        /// IMPORTANT: Remember that WCF cannot serialize Objects.
        /// </para>
        /// </internal>
        /// <value>The value.</value>
        public object Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuerySearchTerm" /> class.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <param name="equality">The equality.</param>
        public QuerySearchTerm(string term, Equality equality= Equality.Equal)
            : this()
        {
            this.Text = term;
            this.Equality = Equality.Equal;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuerySearchTerm"/> class.
        /// </summary>
        public QuerySearchTerm()
        {
            this.Equality = Equality.Equal;
            

            OnDeserializing(default(StreamingContext));
        }



        [OnDeserializing]
        public void OnDeserializing(StreamingContext streamingContext)
        {
            if (Equality == Equality.Undefined)
            {
                Equality = Equality.Equal;
            }
        }


        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string eq;
            switch(this.Equality)
            {
                case Equality.NotEqual:
                    eq = "!=";
                    break;
                    case Equality.Equal:
                    eq = "==";
                    break;
                    case Equality.Undefined:
                    eq = "[!undefined!]";
                    break;
                    case Equality.GreaterThan:
                    eq = ">";
                    break;
                    case Equality.GreaterThanEqual:
                    eq = ">=";
                    break;
                    case Equality.LessThan:
                    eq = "<";
                    break;
                    case Equality.LessThanEqual:
                    eq = "<=";
                    break;
                    case Equality.DataTypeCheck:
                    eq = "~=";
                    break;
                default:
                    eq = "[???]";
                    break;

            }


            return "{0}{1}{2}".FormatStringInvariantCulture(this.Key, eq, this.Value);
        }
    }
}