﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// An implementation of 
    /// <see cref="IPagedQuerySpecification"/>
    /// to pass paging specifications to an 
    /// <c>XAct.Domain.IRepository{TAggregateRootEntity}</c>,
    /// or to <c>PagedResult{TItem}</c>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage would be:
    /// <code>
    /// <![CDATA[
    ///   var paging = new PagedQuerySpecification(20,0);
    ///   var departmentsQuery = 
    ///      _exampleRepository
    ///        .GetByFilter(
    ///          paging: paging,
    ///          filter: contact => contact.Age >= 18 
    ///          orderBy: q => q.OrderBy(d => d.Name), 
    ///          new IncludeSpecification("Addresses"));
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Regarding Repositories and ORMs: although Linq 
    /// already provides Skip and Take, there are still several 
    /// arguments in favour of using an argument package instead.
    /// First of all, with just an IQueryable return value, there
    /// is no strong way of enforcing that the results are paged before returning
    /// the value.
    /// Even if a Skip and Take argument was added to the 
    /// <c>IRepository{TAggregateRootEntity}.GetByFilter</c> method,
    /// in order to enforce paging of some kind, it doesn't provide any 
    /// enforcements of constraints of size.
    /// In addition, with just Linq, there is no way of returning
    /// a value specifying how many records are in the Db.
    /// </para>
    /// </remarks>
    [DataContract(Name = "PagedQuerySpecification", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public class PagedQuerySpecification : IPagedQuerySpecification
    {

        /// <summary>
        /// Gets the zero-based index of the page to return.
        /// </summary>
        /// <value>
        /// The index of the page.
        /// </value>
        // ReSharper disable ConvertToAutoProperty
        [DataMember(Name = "PageIndex")]
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets the size of the page to return .
        /// </summary>
        /// <value>
        /// The size of the page.
        /// </value>
        [DataMember(Name = "PageSize")]
        public int PageSize { get; set; }



        /// <summary>
        /// Gets a value indicating whether to return the total number of records.
        /// <para>
        /// The default value is <c>True</c>, even if it does take a little longer
        /// (a second query).
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the Total Record count is important; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "RetrieveTotalRecordsInSource")]
        public bool RetrieveTotalRecordsInSource { get; private set; }



        /// <summary>
        /// Gets or sets the total records found.
        /// <para>
        /// The default (unfiled value) is -1.
        /// </para>
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        [DataMember(Name = "TotalRecordsInSource")]
        public int TotalRecordsInSource { get { return _totalRecordsInSource; } set { _totalRecordsInSource = value; } }
        private int _totalRecordsInSource = -1;

        /// <summary>
        /// Gets or sets the time it took to perform the query.
        /// </summary>
        /// <value>The time elapsed.</value>
        [DataMember(Name = "TimeElapsed")]
        public TimeSpan TimeElapsed { get; set; }



        

        /// <summary>
        /// Initializes a new instance of the <see cref="PagedQuerySpecification"/> class.
        /// </summary>
        /// <param name="pageIndex">0 based Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="retrieveTotalRecords">if set to <c>true</c> [retrieve total records].</param>
        public PagedQuerySpecification(int pageIndex = 0, int pageSize = 20, bool retrieveTotalRecords = false)
        {
            //QuerySortSpecification[] sortSpecifications = null, 
            //SortSpecifications = sortSpecifications;
            PageSize = pageSize;
            PageIndex = pageIndex;
            RetrieveTotalRecordsInSource = retrieveTotalRecords;
        }


    }
}
