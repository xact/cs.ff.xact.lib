﻿namespace XAct.Messages
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// See:
    /// <see cref="ReferenceDataBase{TId}"/>
    /// <see cref="ReferenceDataCodedBase{TId}"/>
    /// <see cref="ReferenceDataGuidIdBase"/>
    /// <see cref="ApplicationTennantIdSpecificReferenceDataGuidIdBase"/>
    /// <see cref="ApplicationTennantIdSpecificReferenceDataGuidIdCodedBase"/>
    /// </internal>
    [DataContract]
    public abstract class ReferenceDataCodedBase<TId> : ReferenceDataBase<TId>, IHasCodedReferenceDataReadOnly<TId>
        where TId : struct
    {
        [DataMember]
        public string Code { get; set; }
    }
}