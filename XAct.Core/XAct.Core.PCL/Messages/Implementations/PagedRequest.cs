﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract(Name = "PagedRequest{0}", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public sealed class PagedRequest<T> : IPagedDataRequest<T>
    {
        /// <summary>
        /// Gets or sets the request data/payload.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [DataMember(Name = "Data")]
        public T Data { get; set; }





        /// <summary>
        /// Gets the zero-based index of the page to return.
        /// </summary>
        /// <value>
        /// The index of the page.
        /// </value>
        // ReSharper disable ConvertToAutoProperty
        [DataMember(Name = "PageIndex")]
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets the size of the page to return .
        /// </summary>
        /// <value>
        /// The size of the page.
        /// </value>
        [DataMember(Name = "PageSize")]
        public int PageSize { get; set; }



        /// <summary>
        /// Gets a value indicating whether to return the total number of records.
        /// <para>
        /// The default value is <c>True</c>, even if it does take a little longer
        /// (a second query).
        /// </para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the Total Record count is important; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "RetrieveTotalRecordsInSource")]
        public bool RetrieveTotalRecordsInSource { get; private set; }



        /// <summary>
        /// Gets or sets the total records found.
        /// <para>
        /// The default (unfiled value) is -1.
        /// </para>
        /// </summary>
        /// <value>
        /// The total records.
        /// </value>
        [DataMember(Name = "TotalRecordsInSource")]
        public int TotalRecordsInSource { get { return _totalRecordsInSource; } set { _totalRecordsInSource = value; } }
        private int _totalRecordsInSource = -1;

        /// <summary>
        /// Gets or sets the time it took to perform the query.
        /// </summary>
        /// <value>The time elapsed.</value>
        [DataMember(Name = "TimeElapsed")]
        public TimeSpan TimeElapsed { get; set; }



        



        /// <summary>
        /// Initializes a new instance of the <see cref="Request{T}"/> class.
        /// </summary>
        public PagedRequest():this(0,20,true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PagedRequest{T}" /> class.
        /// </summary>
        /// <param name="pageIndex">0 based Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="retrieveTotalRecords">if set to <c>true</c> [retrieve total records].</param>
        /// <param name="data">The data.</param>
        public PagedRequest(T data, int pageIndex = 0, int pageSize = 20, bool retrieveTotalRecords = true)
            : this(pageIndex, pageSize, retrieveTotalRecords)
        {
            this.Data = data;
        }

                /// <summary>
        /// Initializes a new instance of the <see cref="PagedQuerySpecification"/> class.
        /// </summary>
        /// <param name="pageIndex">0 based Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="retrieveTotalRecords">if set to <c>true</c> [retrieve total records].</param>
        public PagedRequest(int pageIndex = 0, int pageSize = 20, bool retrieveTotalRecords = true)
        {
            //QuerySortSpecification[] sortSpecifications = null, 
            //SortSpecifications = sortSpecifications;
            PageSize = pageSize;
            PageIndex = pageIndex;
            RetrieveTotalRecordsInSource = retrieveTotalRecords;

        }

    }
}
