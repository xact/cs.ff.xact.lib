﻿namespace XAct.Messages
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract(Name = "Request{0}", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public sealed class Request<T>
    {
        /// <summary>
        /// Gets or sets the request data/payload.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [DataMember(Name = "Data")]
        public T Data { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="Request{T}"/> class.
        /// </summary>
        public Request()
        {
        }

    }
}
