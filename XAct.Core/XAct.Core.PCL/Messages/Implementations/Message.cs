﻿
// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
	/// A Message object to pass around between 
	/// application layers only.
	/// <para>
	/// Messages are intended to be added
	/// to the <see cref="Response.Messages"/> 
	/// List of the <see cref="Response"/>
	/// object.
	/// </para>
	/// <para>
	/// At the Presentation Tier boundary, 
	/// just before serialization to the Client Tier, 
	/// the Message is passed through a In most cases translated to
    /// <c>ICultureSpecificMessage</c> to set the <c>Message.Text</c> 
    /// </para>
	/// </summary>
    [DataContract(Name = "Message", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public sealed class Message 
	{

        /// <summary>
        /// Gets the <see cref="MessageCode"/>
        /// that contains the unique <c>long</c>
        /// code for the message.
        /// </summary>
        /// <value>
        /// The message code.
        /// </value>
        public MessageCode MessageCode { get { return _messageCode; } }
        [DataMember(Name = "MessageCode")] private MessageCode _messageCode;
    

		/// <summary>
		/// Gets the Collection of Arguments to format
		/// into the resource string represented by <see cref="MessageCode"/> Id
		/// <para>
		/// Note that not all messages will have associated arguments.
		/// </para>
		/// <para>
		/// IMPORTANT: the argument cannot be object[] as object[] cannot
		/// be serialized by wcf across tier boundaries.
		/// </para>
		/// </summary>
		/// <value>
		/// The arguments.
		/// </value>
		[DataMember(Name = "Arguments")]
		public string[] Arguments { get; private set; }



        /// <summary>
        /// Gets or sets the rendering culture specific properties of the Message.
        /// <para>
        /// Null until set using a service similar to 
        /// <c>CultureSpecificMessageService</c>
        /// </para>
        /// </summary>
        /// <value>
        /// The culture specific message.
        /// </value>
        [DataMember(Name = "PresentationAttributes")]
        public MessagePresentationAttributes PresentationAttributes { get; set; }



        /// <summary>
        /// Gets or sets optional nested messages.
        /// <para>
        /// Useful when having to add bullet points.
        /// </para>
        /// <para>
        /// Note that only the root Message is taken into 
        /// consideration when an 
        /// <see cref="IResponse"/> implementation
        /// is determining the value of <see cref="IResponse.Success"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        public List<Message> InnerMessages { get { return _innerMessages ?? (_innerMessages = new List<Message>()); } }
        [DataMember(Name = "InnerMessages")] private List<Message> _innerMessages;
    

        /// <summary>
        /// Initializes a new instance of the <see cref="Message" /> class.
        /// </summary>
        public Message()
        {
            //Required to allow other constructor.
            OnDeserializing(default(StreamingContext));
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// <para>
        /// IMPORTANT: the argument cannot be object[] as object[] cannot
        /// be serialized by wcf across tier boundaries.
        /// </para>
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="arguments">The arguments.</param>
        public Message(MessageCode messageCode, params string[] arguments)
        {
            _messageCode = messageCode;
            Arguments = arguments;

            //Required to allow other constructor.
            OnDeserializing(default(StreamingContext));
        }

        [OnDeserializing]
        public void OnDeserializing(StreamingContext streamingContext)
        {
            if (_innerMessages == null) { _innerMessages = new List<Message>(); }
        }
    }
}



