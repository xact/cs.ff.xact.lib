﻿namespace XAct.Messages
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Metadata
    /// </summary>
    [DataContract(Name = "KeyValueBase", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public abstract class KeyValueBase
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember(Name = "Key")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [DataMember(Name = "Value")]
        public string Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Metadata" /> class.
        /// </summary>
        protected KeyValueBase(){}

        /// <summary>
        /// Initializes a new instance of the <see cref="Metadata" /> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        protected KeyValueBase(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}