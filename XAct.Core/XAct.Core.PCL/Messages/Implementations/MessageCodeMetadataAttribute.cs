﻿namespace XAct.Messages
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Attribute to associate Resource information to a MessageCode without 
    /// embedding it into the MessageCode, which is serialized to UserAgents
    /// (no need for the schema leak).
    /// <para>
    /// Usage:
    /// <code>
    /// <![CDATA[
    /// public static class MessageCodes {
    ///   [MessageCodeResourceMetadata("MessageCodes","XYZ")]
    ///   public static MessageCode XYZ = new MessageCode(123,Severity.Info,1);
    ///   ...
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    [DataContract]
    public class MessageCodeMetadataAttribute : Attribute, IHasTextReadOnly, IHasResourceFilterReadOnly  // , IHasTextAndResourceFilterAndKeyReadOnly
    {

        /// <summary>
        /// Gets the argument count.
        /// </summary>
        /// <value>
        /// The argument count.
        /// </value>
        public byte ArgumentCount
        {
            get { return _argumentCount; }
        }
        [DataMember(Name="ArgumentCount")]
        private readonly byte _argumentCount;




        /// <summary>
        /// Gets the ResourceService Filter.
        /// </summary>
        /// <value>
        /// The resource key.
        /// </value>
        public string ResourceFilter
        {
            get { return _resourceFilter; }
            //et { return _resourceFilter; }
        }
        [DataMember(Name = "ResourceFilter")]
        private readonly string _resourceFilter;




        /// <summary>
        /// Gets or sets the text.
        /// <para>
        /// IMPORTANT: Text is the Resource Key if <see cref="ResourceFilter"/> is set.
        /// </para>
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public string Text
        {
            get { return _text; }
        }
        [DataMember(Name = "Text")]
        private string _text;


        /// <summary>
        /// Initializes a new instance of the <see cref="MessageCodeMetadataAttribute" /> class.
        /// </summary>
        /// <param name="resourceFilter">The resource set.</param>
        /// <param name="resourceKey">The resource key.</param>
        /// <param name="argumentCount">The argument count.</param>
        public MessageCodeMetadataAttribute(string resourceFilter, string resourceKey, byte argumentCount)
        {
            _resourceFilter = resourceFilter;
            _text = resourceKey;
            _argumentCount = argumentCount;
        }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="MessageCodeMetadataAttribute"/> class,
        /// setting the Text, and not using the Resource system.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="argumentCount">The argument count.</param>
        public MessageCodeMetadataAttribute(string text, byte argumentCount)
        {
            _text = text;
            _argumentCount = argumentCount;
        }

    }
}
