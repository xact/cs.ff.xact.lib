namespace XAct.Messages
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Abstract base class for a Guid Id based ReferenceData that is specific to defined ApplicationTennant
    /// </summary>
    /// <internal>
    /// See: <see cref="ApplicationTennantIdSpecificReferenceDataGuidIdBase"/>
    /// 
    /// </internal>
    [DataContract]
    public abstract class ApplicationTennantIdSpecificReferenceDataGuidIdCodedBase : ReferenceDataGuidIdBase,
                                                                                     IHasApplicationTennantId
    {
        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        public virtual Guid ApplicationTennantId { get; set; }
    }
}