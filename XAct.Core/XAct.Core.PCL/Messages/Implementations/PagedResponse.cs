﻿// ReSharper disable CheckNamespace
namespace XAct.Messages
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// <para>
    /// The class is marked <c>sealed</c> as Inheritence (and therefore the use of KnownType attribute
    /// is considered bad WCF design as it affects performance negatively).
    /// </para>
    /// </summary>
    /// <typeparam name = "TItem"></typeparam>
    [DataContract(Name = "PagedResponse{0}", Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public sealed class PagedResponse<TItem> : IResponse, IPageable 
    {

        private static Severity[] _noSuccess = new[] { Severity.Error, Severity.BlockingWarning };

        /// <summary>
        /// Gets or sets the total number of items in the result set (before Paging Applied).
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The total items.
        /// </value>
        [DataMember(Name = "TotalCount")]
        public int TotalCount { get; set; }

        /// <summary>
        /// Gets or sets the size of the page.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The size of the page.
        /// </value>
        [DataMember(Name = "PageSize")]
        public int PageSize { get; set; }

        /// <summary>
        /// Gets the total pages (basically, PageSize / TotalCount)
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The total pages.
        /// </value>
        public int TotalPages
        {
            get
            {

                if (this.TotalCount <= 0)
                {
                    _totalPages = 0;
                }
                else
                {
                    _totalPages = (int)Math.Ceiling( (double)(this.TotalCount / (Decimal)this.PageSize));
                }
                return _totalPages;
            }
        }
        [DataMember(Name = "TotalPages")]
        private int _totalPages;

        /// <summary>
        /// Gets or sets the current page.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The current page.
        /// </value>
        [DataMember(Name = "PageIndex")]
        public int PageIndex { get; set; }

        /// <summary>
        /// A boolean indicating whether there is a previous page
        ///             (ie, PageIndex &gt; 0)
        /// 
        /// </summary>
        /// 
        /// <value/>
        public bool HasPreviousPage
        {
            get
            {
                _hasPreviousPage  =
                this.PageIndex > 1;

                return _hasPreviousPage;
            }
        }
        [DataMember(Name = "HasPreviousPage")]
        private bool _hasPreviousPage;

        /// <summary>
        /// A flag indicating whether there is a later page to switch to.
        /// 
        /// </summary>
        /// 
        /// <value/>
        public bool HasNextPage
        {
            get
            {
                _hasNextPage = this.PageIndex * this.PageSize < this.TotalCount;
                return _hasNextPage;
            }
        }

        [DataMember(Name = "HasNextPage")]
        private bool _hasNextPage;

        /// <summary>
        /// Gets or sets the total number of records
        ///             in the datastore (usually larger than
        ///             <see cref="P:XAct.Messages.PagedResponse`1.TotalCount"/>, which is just
        ///             the Size of the Matched Records prior to
        ///             paging being applied).
        /// 
        /// <para>
        /// The default (unfiled value) is -1.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The total records.
        /// 
        /// </value>
        public int TotalCountInDataStore
        {
            get
            {
                return this._totalRecordsInSource;
            }
            set
            {
                this._totalRecordsInSource = value;
            }
        }
        [DataMember(Name = "TotalCountInDataStore")]
        private int _totalRecordsInSource = -1;

        /// <summary>
        /// Gets or sets the time it took to perform the query.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The time elapsed.
        /// </value>
        //[DataMember(Name = "TimeElapsed")]
        public TimeSpan TimeElapsed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:XAct.Messages.PagedResponse`1"/> 
        /// has any messages with <see cref="Severity"/> of <see cref="Severity.Error"/>
        /// or <see cref="Severity.BlockingWarning"/>
        /// </summary>
        /// <value>
        /// <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success
        {
            get
            {
                return _success = Response.SuccessMethod(this);
            }
        }
        [DataMember(Name = "Success")]
        private bool _success;


        



        /// <summary>
        /// Gets or sets a collection of optional messages that describe the
        ///             reason why <see cref="P:XAct.Messages.PagedResponse`1.Success"/> is as it is.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The messages.
        /// 
        /// </value>
        public List<Message> Messages
        {
            get
            {
                var result = this._messages = _messages ?? (_messages = new List<Message>());
                return result;
            }
        }
        [DataMember(Name = "Messages")]
        private List<Message> _messages;

        /// <summary>
        /// Gets or sets an list of additional metadata that upper tiers can use as they need.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The metadata.
        /// 
        /// </value>
        public List<Metadata> Metadata
        {
            get
            {
                return this._metadata;
            }
        }
        [DataMember(Name = "Metadata")]
        private List<Metadata> _metadata;

        /// <summary>
        /// Gets or sets the response data/payload.
        /// 
        /// </summary>
        /// 
        /// <value>
        /// The data.
        /// 
        /// </value>
        [DataMember(Name = "Data")]
        public ICollection<TItem> Data { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:XAct.Messages.PagedResponse`1"/> class.
        /// 
        /// </summary>
        public PagedResponse()
        {
            this.OnDeserializing(default(StreamingContext));
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="PagedResponse{T}" /> class.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="pageIndex">0 based Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalSetCount">The total count.</param>
        public PagedResponse(ICollection<TItem> items, int pageIndex, int pageSize, int totalSetCount)
            : this()
        {
            //this.Data.AddRange(items);
            this.Data = items;
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
            this.TotalCount = totalSetCount;

            //Required to allow other constructor.
            OnDeserializing(default(StreamingContext));

        }


        [OnDeserializing]
        public void OnDeserializing(StreamingContext streamingContext)
        {
            if (this.Data == null) this.Data = new List<TItem>();
            if (this._messages == null) this._messages = new List<Message>();
            if (this._metadata == null)
            {
                this._metadata = new List<Metadata>();
            }
        }

        /// <summary>
        /// Invoked prior to serialization, to persist the private _success property with correct value.
        /// </summary>
        /// <param name="context"></param>
        /// <internal>
        /// Don't see a ISerializationCallback to use.
        /// </internal>
        [OnSerialized]
        private void OnSerialized(StreamingContext context)
        {
            _success = _messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

            //_success = this.Messages.All(m => !new[] { Severity.Error, Severity.BlockingWarning }.Contains(m.Severity));
        }

        /// <summary>
        /// Adds the specified message to the
        /// Response object's Message array.
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="arguments">The arguments.</param>
        public bool AddMessage(MessageCode messageCode, params string[] arguments)
        {
            //Notice how we do not set the Text Property.

            //That is set later using ICultureSpecificMessageService.

            Message message = new Message(messageCode, arguments);

            this.Messages.Add(message);

            return this.Success;
        }



        ///// <summary>
        ///// Adds the specified message to the
        ///// Response object's Message array.
        ///// </summary>
        ///// <param name="severity">The severity.</param>
        ///// <param name="messageCode">The message code.</param>
        ///// <param name="arguments">The arguments.</param>
        //public void AddMessage(Severity severity, ulong messageCode, params string[] arguments)
        //{
        //    Message message = new Message(severity, messageCode, arguments);

        //    //Notice how we do not set the Text Property.

        //    //That is set later using ICultureSpecificMessageService.

        //    _messages.Add(message);
        //}


    }
}