namespace XAct.Messages
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// See:
    /// <see cref="ApplicationTennantIdSpecificReferenceDataGuidIdCodedBase"/>
    /// </internal>
    [DataContract]
    public abstract class ApplicationTennantIdSpecificReferenceDataGuidIdBase : ReferenceDataGuidIdBase,
        IHasApplicationTennantIdSpecificDistributedGuidIdReferenceData,
        IHasApplicationTennantIdSpecificReferenceData<Guid>,
    IHasApplicationTennantId
    {
        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        public virtual Guid ApplicationTennantId { get; set; }
    }
}