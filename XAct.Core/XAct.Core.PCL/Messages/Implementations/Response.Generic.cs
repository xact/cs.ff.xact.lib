﻿namespace XAct.Messages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Generic response object for non paged data.
    /// <para>
    /// The class is marked <c>sealed</c> as Inheritence (and therefore the use of KnownType attribute
    /// is considered bad WCF design as it affects performance negatively).
    /// </para>
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    [DataContract(Name = "Response{0}")]
    public sealed class Response<TData> : IResponse<TData>//, IDeserializationCallback
    {
        private static Severity[] _noSuccess = new[] { Severity.Error, Severity.BlockingWarning };


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="XAct.Messages.PagedResponse{TData}"/> 
        /// has any messages with <see cref="Severity"/> of <see cref="Severity.Error"/>
        /// or <see cref="Severity.BlockingWarning"/>
        /// </summary>
        /// <value>
        /// <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success
        {
            get
            {
                return _success = Response.SuccessMethod(this);
            }
        }
        [DataMember(Name = "Success")]
        private bool _success;

        /// <summary>
        /// Gets or sets a collection of optional messages that describe the
        /// reason why <see cref="Success"/> is as it is.
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        public List<Message> Messages
        {
            get
            {
                var result = this._messages = _messages ?? (_messages = new List<Message>());
                return result;
            }
        }
        
        [DataMember(Name = "Messages")] private List<Message> _messages;


        /// <summary>
        /// Gets a value indicating whether this instance has data.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has data; otherwise, <c>false</c>.
        /// </value>
        public bool HasData
        {
            get
            {
                _hasData = !Data.IsDefault();
                return _hasData;
            }
        }
        [DataMember(Name = "HasData")]
        private bool _hasData;


        /// <summary>
        /// Gets or sets the response data/payload.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [DataMember(Name = "Data")]
        public TData Data { get; set; }


        /// <summary>
        /// Gets or sets the time it took to perform the query.
        /// <para>
        /// Note that the property is not marked serializable
        /// as it is only for returning the value to the requestor
        /// in the Application layer, to be transferred to a 
        /// <see cref="PagedResponse{TItem}"/> object.
        /// </para>
        /// <para>
        /// Do not mark with WCF or other serialization directives
        /// as it is only required internally.
        /// </para>
        /// </summary>
        /// <value>The time elapsed.</value>
        [DataMember(Name = "TimeElapsed")]
        public TimeSpan TimeElapsed { get; set; }



        /// <summary>
        /// Gets or sets an list of additional metadata that upper tiers can use as they need.
        /// </summary>
        /// <value>
        /// The metadata.
        /// </value>
        public List<Metadata> Metadata { get { return _metadata; } }
        [DataMember(Name = "Metadata")]
        private List<Metadata> _metadata;  


        /// <summary>
        /// Initializes a new instance of the <see cref="Response{TData}" /> class.
        /// </summary>
        public Response()
        {
            OnDeserializing(default(StreamingContext));

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Response{TData}" /> class.
        /// </summary>
        /// <internal>
        /// KEEP IT Non-public so that Unity doesn't blow up when deserializing.
        /// </internal>
        /// <param name="data">The data.</param>
        public Response(TData data)
        {
            Data = data;
            
            OnDeserializing(default(StreamingContext));
        }

        
        /// <summary>
        /// Invoked prior to serialization, to persist the private _success property with correct value.
        /// </summary>
        /// <param name="context"></param>
        /// <internal>
        /// Don't see a ISerializationCallback to use.
        /// </internal>
        [OnSerialized]
        private void OnSerialized(StreamingContext context)
        {
            _success =
            _messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

        }
        
        /// <summary>
        /// Runs when the entire object graph has been deserialized.
        /// </summary>
        [OnDeserializing]
        public void OnDeserializing(StreamingContext streamingContext)
        {
            if (_messages == null)
            {
                _messages = new List<Message>();
            }
            if (_metadata == null)
            {
                _metadata = new List<Metadata>();
            }
        }

        ///// <summary>
        ///// Adds the specified message to the
        ///// Response object's Message array.
        ///// </summary>
        ///// <param name="severity">The severity.</param>
        ///// <param name="messageCode">The message code.</param>
        ///// <param name="arguments">The arguments.</param>
        //public void AddMessage(Severity severity, ulong messageCode, params string[] arguments)
        //{
        //    Message message = new Message(severity, messageCode, arguments);

        //    //Notice how we do not set the Text Property.

        //    //That is set later using ICultureSpecificMessageService.

        //    _messages.Add(message);
        //}

        /// <summary>
        /// Adds the specified message to the
        /// Response object's Message array.
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="arguments">The arguments.</param>
        public bool AddMessage(MessageCode messageCode, params string[] arguments)
        {
            //Notice how we do not set the Text Property.

            //That is set later using ICultureSpecificMessageService.

            Message message = new Message(messageCode, arguments);

            this.Messages.Add(message);

            return this.Success;
        }

    }

}