﻿namespace XAct.Messages
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    public interface IPagedDataRequest<T> : IPagedQuerySpecification
    {
        /// <summary>
        /// Gets or sets the request data/payload.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [DataMember(Name = "Data")]
        T Data { get; set; }


    }
}