﻿namespace XAct.Messages
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Generic payload carrying version of <see cref="IResponse"/>.
    /// </summary>
    /// <typeparam name="TData">The type of the data.</typeparam>
    public interface IResponse<TData> : IResponse
    {

        /// <summary>
        /// Gets a value indicating whether this instance has data.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has data; otherwise, <c>false</c>.
        /// </value>
        bool HasData { get; }

        /// <summary>
        /// Gets or sets the response data/payload.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        TData Data { get; set; }
    }



    /// <summary>
    /// 
    /// <para>
    /// Contract implemented by at least
    /// <see cref="Response"/> 
    /// <see cref="Response{T}"/>
    /// <see cref="PagedResponse{T}"/>,
    /// </para>
    /// </summary>
    public interface IResponse : IHasSuccessReadOnly
    {

        /// <summary>
        /// Gets or sets a collection of optional messages that describe the
        /// reason why <see cref="Success"/> is as it is.
        /// </summary>
        /// <value>
        /// The messages.
        /// </value>
        List<Message> Messages { get; }


        /// <summary>
        /// Gets or sets the time it took to perform the query.
        /// <para>
        /// Note that the property is not marked serializable
        /// as it is only for returning the value to the requestor
        /// in the Application layer, to be transferred to a 
        /// <see cref="PagedResponse{TItem}"/> object.
        /// </para>
        /// <para>
        /// Do not mark with WCF or other serialization directives
        /// as it is only required internally.
        /// </para>
        /// </summary>
        /// <value>The time elapsed.</value>
        TimeSpan TimeElapsed { get; set; }


        /// <summary>
        /// Gets or sets an list of additional metadata that upper tiers can use as they need.
        /// </summary>
        /// <value>
        /// The metadata.
        /// </value>
        List<Metadata> Metadata { get; }
    }
}