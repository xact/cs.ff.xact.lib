namespace XAct.Messages
{
    /// <summary>
    /// Contract for a Message object 
    /// to pass around between 
    /// Presentation Tier and Client Tier.
    /// <para>
    /// Messages are intended to be added
    /// to the <see cref="Response.Messages"/> 
    /// List of the <see cref="Response"/>
    /// object.
    /// </para>
    /// <para>
    /// At the Presentation Tier boundary, 
    /// just before serialization to the Client Tier, 
    /// the Message is passed through a In most cases translated to
    /// <c>ICultureSpecificMessage</c> to set the <c>Text</c> 
    /// 
    /// </para>
    /// </summary>
    public interface IMessagePresentationAttributes 
    {
        
    }
}