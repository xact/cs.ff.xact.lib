﻿
//namespace XAct.Messages
//{
//    /// <summary>
//    /// <para>
//    /// An XAct.Lib Contract, defined in <c>XAct.Core</c>.
//    /// </para>
//    /// Contract for a paged search, given a string search term.
//    /// </summary>
//    public interface IPagedStringQuerySpecification : IPagedQuerySpecification
//    {
//        /// <summary>
//        /// Gets or sets the search term.
//        /// </summary>
//        /// <value>
//        /// The search term.
//        /// </value>
//        string SearchTerm { get; set; }

//        /// <summary>
//        /// Gets or sets additional information to limit the search range.
//        /// </summary>
//        /// <value>
//        /// The search context.
//        /// </value>
//        string SearchContext { get; set; }
//    }
//}
