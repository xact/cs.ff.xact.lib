﻿namespace XAct.Messages
{
    public interface IReferenceDataIOModel :  
        IHasKeyReadOnly,
        IHasTextAndDescriptionReadOnly,
        IHasTagReadOnly, 
        IHasFilterReadOnly,
        IHasMetadataReadOnly
    {
        
    }
}