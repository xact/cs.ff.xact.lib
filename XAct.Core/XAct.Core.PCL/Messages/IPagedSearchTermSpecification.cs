﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace XAct.Messages
//{
//    interface IPagedSearchTermSpecification
//    {
//        /// <summary>
//        /// Gets or sets the time it took to perform the query.
//        /// <para>
//        /// Note that the property is not marked serializable
//        /// as it is only for returning the value to the requestor
//        /// in the Application layer, to be transferred to a 
//        /// <see cref="PagedDataResponse{TItem}"/> object.
//        /// </para>
//        /// <para>
//        /// Do not mark with WCF or other serialization directives
//        /// as it is only required internally.
//        /// </para>
//        /// </summary>
//        /// <value>The time elapsed.</value>
//        TimeSpan TimeElapsed { get; set; }

//        /// <summary>
//        ///   Gets the zero-based index of the page to return.
//        /// </summary>
//        /// <value>The index of the page.</value>
//        int PageIndex { get; }

//        /// <summary>
//        ///   Gets the size of the page to return .
//        /// </summary>
//        /// <value>The size of the page.</value>
//        int PageSize { get; }

//        /// <summary>
//        /// Sets a value indicating whether to return the total number of records.
//        /// <para>
//        /// The default value is <c>True</c>, even if it does take a little longer 
//        /// (a second query).
//        /// </para>
//        /// </summary>
//        /// <value>
//        ///   <c>true</c> if the Total Record count is important; otherwise, <c>false</c>.
//        /// </value>
//        bool RetrieveTotalRecordsInSource { get; }


//        ///// <summary>
//        ///// Gets or sets the number of records found in the single page.
//        ///// <para>
//        ///// In all pages except that last, this will be same value 
//        ///// as <see cref="PageSize"/>
//        ///// </para>
//        ///// </summary>
//        ///// <value>
//        ///// The records found.
//        ///// </value>
//        //[Obsolete("In a Repository, causes Query to happen to early.")]
//        //int PageRecordsReturned { get; set; }

//        /// <summary>
//        ///   Gets or sets the total records found (not just the page of records found).
//        /// <para>
//        /// Do not mark with WCF or other serialization directives
//        /// as it is only required internally.
//        /// </para>
//        /// </summary>
//        /// <value>The total records.</value>
//        int TotalRecordsInSource { get; set; }
//    }
//}
