﻿
namespace XAct.Collections
{
    /// <summary>
    ///   Contract for an argument package.
    /// </summary>
    public interface IQuerySortSpecification
    {
        /// <summary>
        ///   Gets the sort expresion.
        /// </summary>
        /// <value>The sort expresion.</value>
        string Key { get; }

        /// <summary>
        ///   Gets the sort direction.
        /// </summary>
        /// <value>The sort direction.</value>
        SortDirection Direction { get; }

    }
}