﻿namespace XAct.Messages
{
    using System;

    /// <summary>
    ///   Contract for an argument package to 
    /// <c>IRepository{TAggregateRootEntity}.GetByFilter</c>.
    /// <para>
    /// Basis of <see cref="IPagedSearchTermQuerySpecification"/>
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage would be:
    /// <code>
    /// <![CDATA[
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// An implementation example would be (as was done with
    /// <c>PagedQuerySpecification</c>
    /// <code>
    /// <![CDATA[
    /// public class PagedQuerySpecification :IPagedQuerySpecification
    /// {
    ///   private bool _initialized;
    ///   public int PageIndex { get { return _pageIndex; } set { _pageIndex = value; } }
    ///   private int _pageIndex = 0;
    ///   
    ///   public int PageSize { get { return _pageSize; } set { _pageSize = value; } }
    ///   private int _pageSize = 20;
    ///   
    ///   public PagedQuerySpecification(){}
    ///   
    ///   public PagedQuerySpecification(int pageSize, int pageIndex){
    ///     _pageSize = pageSize;
    ///     _pageIndex = pageIndex;
    ///   }
    ///   
    ///   public bool GetTotalRecords { get; private set; }
    ///   
    ///   public int TotalRecords { get { return _totalRecords; } set { _totalRecords = value; } }
    ///   private int _totalRecords=-1;
    ///   
    ///   public void SetPageIndexAndSize(int pageIndex, int pageSize, bool getTotalRecords=true){
    ///     if (_initialized) {
    ///       throw new ArgumentException("PagedQuerySpecs already initialized");
    ///     }
    ///   
    ///     GetTotalRecords = getTotalRecords;
    ///     PageIndex = pageIndex;
    ///     PageSize = pageSize;
    ///     _initialized = true;
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Regarding Repositories and ORMs: although Linq 
    /// already provides Skip and Take, there are still several 
    /// arguments in favour of using an argument package instead.
    /// First of all, with just an IQueryable return value, there
    /// is no strong way of enforcing that the results are paged before returning
    /// the value.
    /// Even if a Skip and Take argument was added to the 
    /// <c>IRepository{TAggregateRootEntity}.GetByFilter</c> method,
    /// in order to enforce paging of some kind, it doesn't provide any 
    /// enforcements of constraints of size.
    /// In addition, with just Linq, there is no way of returning
    /// a value specifying how many records are in the Db.
    /// </para>
    /// </remarks>
    public interface IPagedQuerySpecification
    {
        /// <summary>
        /// Gets or sets the time it took to perform the query.
        /// <para>
        /// Note that the property is not marked serializable
        /// as it is only for returning the value to the requestor
        /// in the Application layer, to be transferred to a 
        /// <see cref="PagedResponse{TItem}"/> object.
        /// </para>
        /// <para>
        /// Do not mark with WCF or other serialization directives
        /// as it is only required internally.
        /// </para>
        /// </summary>
        /// <value>The time elapsed.</value>
        TimeSpan TimeElapsed { get; set; }

        /// <summary>
        ///   Gets the zero-based index of the page to return.
        /// </summary>
        /// <value>The index of the page.</value>
        int PageIndex { get; }

        /// <summary>
        ///   Gets the size of the page to return .
        /// </summary>
        /// <value>The size of the page.</value>
        int PageSize { get; }

        /// <summary>
        /// Sets a value indicating whether to return the total number of records.
        /// <para>
        /// The default value is <c>True</c>, even if it does take a little longer 
        /// (a second query).
        /// </para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if the Total Record count is important; otherwise, <c>false</c>.
        /// </value>
        bool RetrieveTotalRecordsInSource { get; }


        ///// <summary>
        ///// Gets or sets the number of records found in the single page.
        ///// <para>
        ///// In all pages except that last, this will be same value 
        ///// as <see cref="PageSize"/>
        ///// </para>
        ///// </summary>
        ///// <value>
        ///// The records found.
        ///// </value>
        //[Obsolete("In a Repository, causes Query to happen to early.")]
        //int PageRecordsReturned { get; set; }

        /// <summary>
        ///   Gets or sets the total records found (not just the page of records found).
        /// <para>
        /// Do not mark with WCF or other serialization directives
        /// as it is only required internally.
        /// </para>
        /// </summary>
        /// <value>The total records.</value>
        int TotalRecordsInSource { get; set; }



    }
}