﻿namespace XAct.Resources
{
    using System.Globalization;
    using XAct.Messages;
    using XAct.Messages.Services.Configuration;
    using XAct.Resources;

    /// <summary>
    /// Contract for a service to 
    /// process the <c>Id</c>
    /// (and <c>Arguments</c> if any)
    /// of an instance of <see cref="Message"/>,
    /// and set it <c>Text</c> 
    /// value to a culture specific resource string.
    /// </summary>
    public interface ICultureSpecificResponseService : IHasXActLibService
    {

        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        ICultureSpecificMessageServiceConfiguration Configuration { get; }


        ///// <summary>
        ///// Gets or sets the configuration used by this service.
        ///// <para>
        ///// The Configuration object is shared between instances of 
        ///// this service, therefore should only be modified as per the application's needs
        ///// during Bootstrapping, and no later.
        ///// </para>
        ///// </summary>
        //ICultureSpecificMessageServiceConfiguration Configuration { get; }

        /// <summary>
        /// Sets the presentation attributes on each <see cref="Message"/>
        /// in the <see cref="IResponse"/> implementation.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="cultureInfo">The culture information.</param>
        void SetPresentationAttributes(IResponse response, CultureInfo cultureInfo);

        /// <summary>
        /// Uses the <c>Id</c> value given
        /// (and <c>Arguments</c> if any)
        /// to find the related culture specific resource,
        /// and set the <c>IText</c>
        /// of the given <see cref="Message"/>
        /// implementation.
        /// property.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="cultureInfo">The culture information.</param>
        void SetPresentationAttributes(Message message, CultureInfo cultureInfo);
    }
}
