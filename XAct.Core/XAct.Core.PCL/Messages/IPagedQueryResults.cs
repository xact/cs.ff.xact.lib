﻿//using System.Collections.Generic;

//namespace XAct.Messages
//{
//    /// <summary>
//    /// </summary>
//    public interface IPagedQueryResults
//    {
//        /// <summary>
//        ///   Gets the total records in the query.
//        /// </summary>
//        /// <value>The total records.</value>
//        int TotalRecords { get; }

//        /// <summary>
//        ///   Gets the total number of pages required to display TotalRecords.
//        /// </summary>
//        /// <value>The total pages.</value>
//        int TotalPages { get; }

//        /// <summary>
//        ///   Gets the index of the current page.
//        /// </summary>
//        /// <value>The index of the current page.</value>
//        int CurrentPageIndex { get; }

//        /// <summary>
//        ///   Gets whether to enable the Back button.
//        /// </summary>
//        /// <value>A flag.</value>
//        bool EnableBack { get; }

//        /// <summary>
//        ///   Gets whether to enable the Forward button.
//        /// </summary>
//        /// <value>A flag.</value>
//        bool EnabledForward { get; }


//        /// <summary>
//        ///   Initialises the specified specs.
//        /// </summary>
//        /// <param name = "specs">The specs.</param>
//        void Initialise(IPagedQuerySpecification specs);
//    }

//    /// <summary>
//    /// </summary>
//    //[SuppressMessage("Microsoft.Naming", "CA1710", Justification = "It's a list -- not just a collection.")]
//    public interface IPagedQueryResultsCollection<T> : IPagedQueryResults, ICollection<T>
//    {
//    }
//}