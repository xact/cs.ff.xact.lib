﻿namespace XAct.Messages
{
    /// <summary>
    ///   Contract for an argument package to 
    /// <c>IRepository{TAggregateRootEntity}.GetByFilter</c>.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage would be:
    /// <code>
    /// <![CDATA[
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// An implementation example would be (as was done with
    /// <c>PagedQuerySpecification</c>
    /// <code>
    /// <![CDATA[
    /// public class PagedQuerySpecification :IPagedQuerySpecification
    /// {
    ///   private bool _initialized;
    ///   public int PageIndex { get { return _pageIndex; } set { _pageIndex = value; } }
    ///   private int _pageIndex = 0;
    ///   
    ///   public int PageSize { get { return _pageSize; } set { _pageSize = value; } }
    ///   private int _pageSize = 20;
    ///   
    ///   public PagedQuerySpecification(){}
    ///   
    ///   public PagedQuerySpecification(int pageSize, int pageIndex){
    ///     _pageSize = pageSize;
    ///     _pageIndex = pageIndex;
    ///   }
    ///   
    ///   public bool GetTotalRecords { get; private set; }
    ///   
    ///   public int TotalRecords { get { return _totalRecords; } set { _totalRecords = value; } }
    ///   private int _totalRecords=-1;
    ///   
    ///   public void SetPageIndexAndSize(int pageIndex, int pageSize, bool getTotalRecords=true){
    ///     if (_initialized) {
    ///       throw new ArgumentException("PagedQuerySpecs already initialized");
    ///     }
    ///   
    ///     GetTotalRecords = getTotalRecords;
    ///     PageIndex = pageIndex;
    ///     PageSize = pageSize;
    ///     _initialized = true;
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// Regarding Repositories and ORMs: although Linq 
    /// already provides Skip and Take, there are still several 
    /// arguments in favour of using an argument package instead.
    /// First of all, with just an IQueryable return value, there
    /// is no strong way of enforcing that the results are paged before returning
    /// the value.
    /// Even if a Skip and Take argument was added to the 
    /// <c>IRepository{TAggregateRootEntity}.GetByFilter</c> method,
    /// in order to enforce paging of some kind, it doesn't provide any 
    /// enforcements of constraints of size.
    /// In addition, with just Linq, there is no way of returning
    /// a value specifying how many records are in the Db.
    /// </para>
    /// </remarks>
    public interface IPagedSearchTermQuerySpecification : IPagedQuerySpecification
    {


        /// <summary>
        /// Gets or sets the search terms.
        /// </summary>
        /// <value>The search terms.</value>
        QuerySearchTerm[] SearchTerms { get; set; }


        /// <summary>
        /// Gets or sets an optional array of <see cref="QuerySortSpecification"/>.
        /// </summary>
        /// <value>The sort specifications.</value>
        QuerySortSpecification[] SortSpecifications { get; set; }

    }
}