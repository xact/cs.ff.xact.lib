﻿//// ReSharper disable CheckNamespace

//using XAct.Validation;

//namespace XAct.Messages
//// ReSharper restore CheckNamespace
//{
//    using System.Runtime.Serialization;

//    /// <summary>
//    ///Message raised by <see cref="IValidationService{T}"/>
//    /// </summary>
//    /// <internal><para>5/11/2011: Sky</para></internal>
//    public interface IMessage  : IHasIdReadOnly<ulong>
//    {


//        /// <summary>
//        /// Gets an array of optional string arguments to be 
//        /// formatted into the Resource string
//        /// identified using <c>Id</c>
//        /// <para>
//        /// IMPORTANT: The reason for forcing the use of strings
//        /// rather thatn using object[] is in order to serialize
//        /// things over the wire.
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The arguments.
//        /// </value>
//        string[] Arguments { get; }




//        /// <summary>
//        /// Gets or sets the rendering culture specific properties of the Message.
//        /// <para>
//        /// Null until set using a service similar to 
//        /// <c>CultureSpecificMessageService</c>
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The culture specific message.
//        /// </value>
//        [DataMember(Name = "PresentationAttributes")]
//        MessagePresentationAttributes PresentationAttributes { get; set; }
//    }
//}