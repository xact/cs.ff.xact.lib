﻿namespace XAct.Messages
{
    using System;
    using XAct.Messages.Services.Configuration;

    public interface IMessageCodeMetadataService : IHasXActLibService
    {

        IMessageCodeMetadataServiceConfiguration Configuration { get; } 

        void Scan(params Type[] classesContainingStaticPropertiesOfTypeMessageCode);




        bool TryGet(MessageCode messageCode, out MessageCodeMetadataAttribute messageCodeMetadataAttribute);



        /// <summary>
        /// Resolves the specified message code identifier
        /// (commonly just the string name of the static <see cref="MessageCode"/> property)
        /// into it's <see cref="MessageCode"/>.
        /// <para>
        /// For example, given an app having common MessageCodes defined as follows:
        /// <code>
        /// <![CDATA[
        /// public namespace App.Constants {
        ///   public static class MessageCodes {
        ///     public static MessageCode Foo = new MessageCode(123, Severity.Error);
        ///     public static MessageCode Bar = new MessageCode(123, Severity.BlockingWarning);
        ///     public static MessageCode Woo = new MessageCode(123, Severity.Information);
        ///     public static MessageCode Todo = new MessageCode(-1, Severity.Error);
        ///   }
        /// }
        /// ]]>
        /// </code>
        /// and the app's bootstrapping sequence initializing the <see cref="IMessageCodeService"/> as follows
        /// <code>
        /// <![CDATA[
        /// void Main(){
        ///   var serviceConfiguration = 
        ///      XAct.DependencyLocator.Currrent.GetInstance<IMessageCodeResolvingServiceConfiguration>();
        ///   serviceConfiguration.Types.Add(typeof(MessageCodes));
        ///   serviceConfiguration.DefaultType = MessageCodes.Todo;
        /// }
        /// ]]>
        /// </code>
        /// the following will resolve as follows:
        /// <code>
        /// <![CDATA[
        /// MessageCode messageCode = 
        ///   XAct.DependencyLocator.Current.GetInstance<IMessageCodeResolvingService>().Resolve("woo");
        /// 
        /// Debug.Assert(messageCode.Id == "Woo");
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="messageCodeIdentifier">The message code identifier.</param>
        /// <returns></returns>
        bool TryGet(string messageCodeIdentifier, out MessageCode messageCode, out MessageCodeMetadataAttribute messageCodeMetadataAttribute);

    }
}