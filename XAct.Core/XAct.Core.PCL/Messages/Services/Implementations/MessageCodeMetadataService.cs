﻿namespace XAct.Messages.Services.Implementations
{
    using System;
    using XAct.Messages.Services.Configuration;
    using XAct.Services;

    public class MessageCodeMetadataService : IMessageCodeMetadataService
    {
        public IMessageCodeMetadataServiceConfiguration Configuration { get { return _messageCodeServiceConfiguration; } }
        private readonly IMessageCodeMetadataServiceConfiguration _messageCodeServiceConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageCodeMetadataService"/> class.
        /// </summary>
        /// <param name="messageCodeServiceConfiguration">The message code service configuration.</param>
        public MessageCodeMetadataService(IMessageCodeMetadataServiceConfiguration messageCodeServiceConfiguration)
        {
            _messageCodeServiceConfiguration = messageCodeServiceConfiguration;
        }


        public void Scan(params Type[] classesContainingStaticPropertiesOfTypeMessageCode)
        {
            _messageCodeServiceConfiguration.Scan(classesContainingStaticPropertiesOfTypeMessageCode);
        }

        public bool TryGet(MessageCode messageCode, out MessageCodeMetadataAttribute messageCodeMetadataAttribute)
        {
            return _messageCodeServiceConfiguration.TryGetMessageCodeMetadata(messageCode, out messageCodeMetadataAttribute);
        }

        public bool TryGet(string messageCodeIdentifier, out MessageCode messageCode, out MessageCodeMetadataAttribute messageCodeMetadataAttribute)
        {
            return _messageCodeServiceConfiguration.TryGetMessageCode(messageCodeIdentifier, out messageCode, out messageCodeMetadataAttribute);
        }
    }
}