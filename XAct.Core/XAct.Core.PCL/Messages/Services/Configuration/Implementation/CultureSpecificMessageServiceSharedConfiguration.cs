namespace XAct.Messages.Services.Configuration.Implementations
{
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="ICultureSpecificMessageServiceConfiguration"/>
    /// to provide a package of configuration settings
    /// required by an implementation of the
    /// <see cref="ICultureSpecificResponseService"/>
    /// <para>
    /// Its default binding scope is <c>Singleton</c>.
    /// </para>
    /// </summary>
    public class CultureSpecificMessageServiceConfiguration : ICultureSpecificMessageServiceConfiguration, IHasXActLibServiceConfiguration
    {


        
        /// <summary>
        /// Gets or sets name of the resource set (filter)
        /// in which to search for the given Resource key.
        /// </summary>
        /// <value>
        /// The resource set name.
        /// </value>
        public string ResourceFilter { get; set; }
        
        /// <summary>
        /// Gets or sets the resource key format.
        /// <para>
        /// Default value is "{0}".
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        public string ResourceKeyFormat
        { 
            get { return _resourceKeyFormat; }
            set { _resourceKeyFormat = (value.IsNullOrEmpty() == false) ? value : "{0}"; }
        }
        private string _resourceKeyFormat = "{0}";



        /// <summary>
        /// Gets or sets the additional information 
        /// resource key format.
        /// <para>
        /// Default value is "{0}".
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        public string AdditionalInformationResourceKeyFormat {
            get { return _additionalInformationResourceKeyFormat; }
            set { _additionalInformationResourceKeyFormat = value; }
        }
        private string _additionalInformationResourceKeyFormat = string.Empty;

        /// <summary>
        /// Gets or sets the additional information url
        /// resource key format.
        /// <para>
        /// Default value is null.
        /// </para>
        /// </summary>
        /// <value>
        /// The resource key format.
        /// </value>
        public string AdditionalInformationUrlFormat { 
            get { return _additionalInformationUrlFormat; }
            set { _additionalInformationUrlFormat = value; }
        }
        private string _additionalInformationUrlFormat = string.Empty;

    }
}