﻿namespace XAct.Messages.Services.Configuration.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using XAct.Messages.Services.Configuration;
    using XAct.Services;

    public class MessageCodeMetadataServiceConfiguration : IMessageCodeMetadataServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Array of registered Types containing Properties that were decorated with <see cref="MessageCodeMetadataAttribute"/> 
        /// </summary>
        public Type[] ScannedContainerTypes
        {
            get { return _types.ToArray(); }
        }

        private readonly List<Type> _types = new List<Type>();

        
        //Not strictly needed, as the MessageCodeExtensions allows for this:
        private readonly Dictionary<MessageCode, MessageCodeMetadataAttribute> _cachedMessageCodesToMetadata =
            new Dictionary<MessageCode, MessageCodeMetadataAttribute>();



        //Caches of the names of MessageCodes property names, to the MessageCode.
        //Used to back Attribute based ValidatorService:
        private readonly Dictionary<string, MessageCode> _cachedMessageCodeIdentifiersToMessageCode =
            new Dictionary<string, MessageCode>(StringComparer.OrdinalIgnoreCase);
        private readonly List<string> _messageCodeIdentifiersThatWillNotBeFound = new List<string>();


        //private Dictionary<long, MessageCodeMetadataAttribute> _codesToMedata =
        //    new Dictionary<long, MessageCodeMetadataAttribute>();




        /// <summary>
        /// Scan the given type for properties that are decorated with <see cref="MessageCodeMetadataAttribute"/>
        /// <para>
        /// For example, given 
        /// <code>
        /// <![CDATA[
        /// public static class MessageCodes {
        ///   [MessageCodeMetadata(Severity=Severity.Error, ResourceFilter="MessageCodes", ResourceKey="MissingDOB", 0)]
        ///   public static const int Validation_MissingDOB = 103;
        /// }
        /// public static enum MessageCodes2 {
        ///   [MessageCodeMetadata(Severity=Severity.Error, ResourceFilter="MessageCodes", ResourceKey="MissingDOB", 0)]
        ///   public enum Validation_MissingDOB = 105;
        /// }
        /// ]]>
        /// </code>
        /// one has to Scan the class with this method so that the <see cref="IMessageCodeMetadaService"/>
        /// can -- given a value of 105 -- return a matching MessageCodeMetadataAttribute
        /// </para>
        /// </summary>
        /// <param name="containerTypes"></param>
        public void Scan(params Type[] containerTypes)
        {
            foreach (Type containerType in containerTypes)
            {
                if (_types.Contains(containerType))
                {
                    //Don't scan twice:
                    continue;
                }

                lock (this)
                {
                    if (_types.Contains(containerType))
                    {
                        //Don't scan twice:
                        continue;
                    }

                    //Get all static properties:
                    FieldInfo[] fieldInfos =
                        containerType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);

                    MessageCodeMetadataAttribute messageCodeMetadataAttribute;

                    foreach (var fieldInfo in fieldInfos)
                    {
                        //Get Static property value (instance=null).

                        //It's preferred that it's a MessageCode as MessageCode is value + Severity
                        //which makes it easier when Message is trying to determine Result = true|false.
                        if (fieldInfo.FieldType == typeof (MessageCode))
                        {
                            MessageCode messageCode = (MessageCode) fieldInfo.GetValue(null);

                            messageCodeMetadataAttribute =
                                fieldInfo.GetAttribute<MessageCodeMetadataAttribute>();

                            if (messageCodeMetadataAttribute == null)
                            {
                                continue;
                            }

                            _cachedMessageCodesToMetadata[messageCode] = messageCodeMetadataAttribute;

                            //Keep the second cache in case we need to find a property by it's Name 
                            //(we used that approach when we created the Attribute based ValidatorService...
                            //although have to admit we didn't like it at the end (attribute based ValidatorService)
                            _cachedMessageCodeIdentifiersToMessageCode[fieldInfo.Name] = messageCode;

                            
                            continue;
                        }

                        //If it wasn't a MessageCode, we can still work with it, as long as it has Metadata+Severity:
                        //CORRECTION: Not really. It's a nice idea to allow just Ints/Longs rather than force the 
                        //usage of MessageCode struct's, but it becomes an issue for the MessageCode trying to set the
                        //Result flag -- it would not be able to do it without a dependence on this Service. 
                        //We won't go down that route.


                        ////Get Static property value (instance=null):
                        //object tmpValue = fieldInfo.GetValue(null);

                        //long value;
                        //try
                        //{
                        //    value = tmpValue.ConvertTo<long>();
                        //}
                        //catch
                        //{
                        //    //Not an int/long, nor MessageCode - can't do much with it.
                        //    continue;
                        //}

                        //messageCodeMetadataAttribute =
                        //    fieldInfo.GetAttribute<MessageCodeMetadataAttribute>();

                        //_codesToMedata[value] = messageCodeMetadataAttribute;
                    }

                    _types.Add(containerType);
                }
            }
        }



        public bool TryGetMessageCodeMetadata(MessageCode messageCode,
                                              out MessageCodeMetadataAttribute messageCodeMetadataAttribute)
        {

            //We might have been looking at a copy of the struct...?
            if (!_cachedMessageCodesToMetadata.TryGetValue(messageCode, out messageCodeMetadataAttribute))
            {

                //FALLBACK?
                //THE PROBLEM HERE IS THAT WE HAVE THE STRCUT --- BUT NOT ITS PROPERTY NAME, WHICH MEANS
                //LOOKUP BY String doesn't work.
                //But maybe they don't need it (most apps won't be looking for MessageCodes via string?
                messageCodeMetadataAttribute = messageCode.GetType().GetAttribute<MessageCodeMetadataAttribute>();

                if (messageCodeMetadataAttribute != null)
                {
                    _cachedMessageCodesToMetadata[messageCode] = messageCodeMetadataAttribute;
                }
                return (messageCodeMetadataAttribute != null);
            }

            return true;
        }




        private bool TryGetMessageCode(string messageCodeIdentifier, out MessageCode messageCode)
        {
            MessageCode result;

            if (_cachedMessageCodeIdentifiersToMessageCode.TryGetValue(messageCodeIdentifier, out result))
            {
                messageCode = result;
                return true;
            }

            if (_messageCodeIdentifiersThatWillNotBeFound.Contains(messageCodeIdentifier))
            {
                messageCode = default(MessageCode);
                return false;
            }

            //if not found, have to iterate over the collection:
            foreach (string key in _cachedMessageCodeIdentifiersToMessageCode.Keys)
            {
                //The given key should be the whole property name, (eg: "InvalidDOB_603", or "603_InvalidDOB")
                //but should be able to figure out "InvalidDob" or "603"
                if (
                    key.EndsWith("_" + messageCodeIdentifier,StringComparison.OrdinalIgnoreCase) ||
                    key.Contains("_" + messageCodeIdentifier + "_",StringComparison.OrdinalIgnoreCase) ||
                    key.StartsWith(messageCodeIdentifier + "_",StringComparison.OrdinalIgnoreCase)
                    )
                {
                    messageCode = _cachedMessageCodeIdentifiersToMessageCode[key];
                    _cachedMessageCodeIdentifiersToMessageCode[messageCodeIdentifier] = messageCode;
                    return true;
                }
            }

            //To make it slightly faster next time.
            _messageCodeIdentifiersThatWillNotBeFound.Add(messageCodeIdentifier);

            messageCode = default(MessageCode);
            return false;
        }

        public bool TryGetMessageCode(string messageCodeIdentifier,
                                              out MessageCode messageCode,
                                              out MessageCodeMetadataAttribute messageCodeMetadataAttribute)
        {
            if (!TryGetMessageCode(messageCodeIdentifier, out messageCode))
            {
                messageCodeMetadataAttribute = null;
                return false;
            }

            return TryGetMessageCodeMetadata(messageCode, out messageCodeMetadataAttribute);
        }

    }
}