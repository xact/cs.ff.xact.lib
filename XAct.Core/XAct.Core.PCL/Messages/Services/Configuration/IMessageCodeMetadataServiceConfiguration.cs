﻿using System;

namespace XAct.Messages.Services.Configuration
{
    public interface IMessageCodeMetadataServiceConfiguration : IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// An array of the types that have been submitted to <see cref="Scan"/>
        /// </summary>
        Type[] ScannedContainerTypes { get; }

        /// <summary>
        /// Scan the given type for properties that are decorated with <see cref="MessageCodeMetadataAttribute"/>
        /// <para>
        /// Invoke only during the application startup, or when initializing a new Module.
        /// </para>
        /// <para>
        /// For example, given 
        /// <code>
        /// <![CDATA[
        /// public static class MessageCodes {
        ///   [MessageCodeMetadata(ResourceFilter="MessageCodes", ResourceKey="MissingDOB", 0)]
        ///   public MessageCode Validation_MissingDOB = new MessageCode(103,Severity.Error);
        ///   ...
        /// }
        /// ]]>
        /// </code>
        /// one has to Scan the class with this method so that the <see cref="IMessageCodeService"/>
        /// can -- given a value of 105 -- return a matching MessageCodeMetadataAttribute
        /// </para>
        /// </summary>
        /// <param name="containerTypes"></param>
        void Scan(params Type[] containerTypes);



        /// <summary>
        /// Returns the <see cref="MessageCodeMetadataAttribute"/>
        /// registered against the given code.
        /// <para>
        /// Should only be invoked from <see cref="IMessageCodeService"/>
        /// </para>
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="messageCodeMetadataAttribute">The message code metadata attribute.</param>
        /// <returns></returns>
        bool TryGetMessageCodeMetadata(MessageCode messageCode, out MessageCodeMetadataAttribute messageCodeMetadataAttribute);



        bool TryGetMessageCode(string messageCodeIdentifier,
                                       out MessageCode messageCode,
                                       out MessageCodeMetadataAttribute messageCodeMetadataAttribute);

    }
}
