﻿
namespace XAct.Security
{
    using System;

    /// <summary>
    /// Security Attribute that can be attached to 
    /// </summary>
// ReSharper disable InconsistentNaming
    [System.AttributeUsage(
        (System.AttributeTargets.Class | System.AttributeTargets.Interface | System.AttributeTargets.Method | System.AttributeTargets.Property | System.AttributeTargets.Field), 
        AllowMultiple = true, 
        Inherited = true)]
    public class AuthorizationAttribute : Attribute,  IHasEnabled, IHasName,  IHasExcluded
// ReSharper restore InconsistentNaming
    {

        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; set; }


        /// <summary>
        /// Gets the optional Alias name for the method.
        /// <para>
        /// Keeping track of the FQN of a method is difficult when developing -- and namespaces/classnames are
        /// still fluctuating.
        /// </para>
        /// <para>
        /// To allow for this change, use Aliases Names instead.
        /// </para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether the operation protected by this 
        /// <see cref="AuthorizationAttribute" /> 
        /// is excluded if there is an <see cref="AuthorizationAttribute"/>
        /// applied to the class.
        /// </summary>
        /// <value>
        ///   <c>true</c> if excluded; otherwise, <c>false</c>.
        /// </value>
        public bool Excluded { get; set; }

        /// <summary>
        /// IMPORTANT: Prefer to not set Roles in code, but allow the
        /// Interceptor to use alternate solutions to figure out the 
        /// Roles required by this method.
        /// <para>
        /// Gets or sets the Roles too allow or disallow for this method.
        /// </para>
        /// </summary>
        /// <value>
        /// The roles.
        /// </value>
        public string Roles { get; set; }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="AuthorizationAttribute"/> class.
        /// </summary>
        public AuthorizationAttribute()
        {
            Enabled = true;
        }
    }
}