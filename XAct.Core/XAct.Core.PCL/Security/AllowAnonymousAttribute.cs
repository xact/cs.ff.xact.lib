﻿namespace XAct.Security
{
    using System;

    /// <summary>
    /// Specifies that actions are skipped by 
    /// during authorization.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AllowAnonymousAttribute : Attribute
    {
    }
}
