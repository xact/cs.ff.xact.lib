﻿// ReSharper disable CheckNamespace
namespace XAct.Security
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// A contract for objects that need to be checked as to whether
    /// their properties can be read and/or updated.
    /// </summary>
    /// <remarks>
    /// Used by <c>XAct.SettingsSettingsBase</c>
    /// </remarks>
    /// <internal><para>8/15/2011: Sky</para></internal>
    public interface IHasReadWriteAuthorization    {
        /// <summary>
        /// Gets a value indicating whether this instance can be read.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance can be read; otherwise, <c>false</c>.
        /// </value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        bool IsReadAuthorized { get; }


        /// <summary>
        /// Gets a value indicating whether this instance can be written to.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance can be modified; otherwise, <c>false</c>.
        /// </value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        bool IsWriteAuthorized { get; }
    }
}