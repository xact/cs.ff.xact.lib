﻿// ReSharper disable CheckNamespace
namespace XAct.Security
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract for objects that define which roles have access to it.
    /// </summary>
    /// <internal><para>8/15/2011: Sky</para></internal>
    public interface IReadWriteAccessRoleNames
    {
        /// <summary>
        /// Gets a CSV (';|,') separated list of role names that have access to this object.
        /// </summary>
        /// <value>The read access role names.</value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        string ReadAccessRoleNames { get; }

        /// <summary>
        /// Gets a CSV (';|,') separated list of role names that have access to this object.
        /// </summary>
        /// <value>The write access role names.</value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        string WriteAccessRoleNames { get; }
    }
}