﻿namespace XAct.Security
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception when the current user 
    /// is not Authenticated, and therefore 
    /// is not allowed to perform the Operation
    /// </summary>
    public class NotAuthenticatedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotAuthenticatedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public NotAuthenticatedException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="NotAuthenticatedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public NotAuthenticatedException(string message, Exception innerException) : base(message, innerException) { }

        ///// <summary>
        ///// Initializes a new instance of the <see cref="NotAuthenticatedException"/> class.
        ///// </summary>
        ///// <param name="serializationInfo">The serialization info.</param>
        ///// <param name="streamingContext">The streaming context.</param>
        //public NotAuthenticatedException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }
}