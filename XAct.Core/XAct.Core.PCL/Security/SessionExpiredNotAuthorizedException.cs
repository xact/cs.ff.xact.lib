﻿namespace XAct.Security
{
    using System;

    public class SessionExpiredNotAuthorizedException : NotAuthorizedException
    {
        //Other exceptions to sort out.
        //UserNotActive
        //OrgNotActive


        public SessionExpiredNotAuthorizedException(string message)
            : base(message)
        {
        }

        public SessionExpiredNotAuthorizedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}