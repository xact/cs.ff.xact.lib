﻿namespace XAct.Security
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception when the current user (Authenticated or not)
    /// is not allowed to perform the Operation
    /// </summary>
    //[Serializable]
    public class NotAuthorizedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotAuthorizedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public NotAuthorizedException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="NotAuthorizedException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public NotAuthorizedException(string message, Exception innerException) : base(message, innerException) { }
        ///// <summary>
        ///// Initializes a new instance of the <see cref="NotAuthorizedException"/> class.
        ///// </summary>
        ///// <param name="serializationInfo">The serialization info.</param>
        ///// <param name="streamingContext">The streaming context.</param>
        //public NotAuthorizedException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }
}
