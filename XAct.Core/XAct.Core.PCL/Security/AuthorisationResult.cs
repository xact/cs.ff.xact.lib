﻿namespace XAct.Security
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "AuthorizationResult")]
    public enum AuthorizationResult
    {

        /// <summary>
        /// No value has been set.
        /// <para>
        /// This would indicate an error in programming logic.
        /// </para>
        /// <para>
        /// Value = 0
        /// </para>
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// The operation is permitted.
        /// <para>
        /// Value = 1
        /// </para>
        /// </summary>
        [EnumMember]
        AnonymousPermitted = 1,

        /// <summary>
        /// The operation is permitted
        /// (either due to AllowAnonymous,
        /// or the Authenticated Identity
        /// has been Authorized).
        /// <para>
        /// Value = 2
        /// </para>
        /// </summary>
        [EnumMember]
        Authorized = 2,

        /// <summary>
        /// The operation requires the current thread principal's identity to be Authenticated.
        /// <para>
        /// Value = 3
        /// </para>
        /// </summary>
        [EnumMember]
        NotAuthenticated = 3,

        /// <summary>
        /// The operation is not Authorized.
        /// <para>
        /// Value = 4
        /// </para>
        /// </summary>
        [EnumMember]
        NotAuthorized = 4
    }
}
