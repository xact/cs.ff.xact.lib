﻿namespace XAct.Security
{
    using System;
    using System.Linq;

    /// <summary>
    /// Class to generate strong passwords.
    /// <para>
    /// Returns a strong password containing
    /// Uppercase, lowercase, numbers, and optionally specialchars.
    /// </para>
    /// <para>
    /// Always starts with an uppercase.
    /// </para>
    /// <para>
    /// Always contains unique characters.
    /// </para>
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <internal>
    /// Inspired by: http://bit.ly/jbC0mX
    /// </internal>
    public class StrongPasswordGenerator
    {
        //Should be a char[], but too much trouble -- using a string
        //that is converted to char array in constructor.
        private const string PasswordCharsLcase = "abcdefgijkmnopqrstwxyz";
        private const string PasswordCharsUcase = "ABCDEFGHJKLMNPQRSTWXYZ";
        private const string PasswordCharsNumeric = "23456789";
        private const string PasswordCharsSpecial = "$=@!"; //"*$-+?_&=!%{}/";
        private static Random _random;

        private readonly char[] _passwordCharsLcaseArray;
        private readonly char[] _passwordCharsNumericArray;
        private readonly char[] _passwordCharsSpecialArray;
        private readonly char[] _passwordCharsUcaseArray;

        /// <summary>
        /// Initializes a new instance of the <see cref="StrongPasswordGenerator"/> class.
        /// </summary>
        public StrongPasswordGenerator()
        {
            //Keep one alive across multiple reequests.
            //If you generate a new one within a tight loop,
            //you will get the same random numbers.
            _random = new Random();

            //Get this out of the way up front:
            _passwordCharsUcaseArray = PasswordCharsUcase.ToCharArray();
            _passwordCharsLcaseArray = PasswordCharsLcase.ToCharArray();
            _passwordCharsNumericArray = PasswordCharsNumeric.ToCharArray();
            _passwordCharsSpecialArray = PasswordCharsSpecial.ToCharArray();
        }


        /// <summary>
        /// Generates the password.
        /// </summary>
        /// <param name="minLength">Length of the min.</param>
        /// <param name="maxLength">Length of the max.</param>
        /// <param name="requireSpecialCharacters">if set to <c>true</c> [require special characters].</param>
        /// <returns></returns>
        public string GeneratePassword(int minLength, int maxLength, bool requireSpecialCharacters)
        {
            return GeneratePassword(minLength, maxLength, requireSpecialCharacters, _passwordCharsSpecialArray);
        }

        /// <summary>
        /// Generates the password.
        /// </summary>
        /// <param name="minLength">Length of the min.</param>
        /// <param name="maxLength">Length of the max.</param>
        /// <param name="requireSpecialCharacters">if set to <c>true</c> [require special characters].</param>
        /// <param name="specialCharsAllowed">The special chars allowed.</param>
        /// <returns></returns>
        public string GeneratePassword(int minLength, int maxLength, bool requireSpecialCharacters,
                                       char[] specialCharsAllowed)
        {
            if (maxLength < minLength)
            {
                maxLength = Math.Max(minLength, 8);
            }

            int length = _random.Next(minLength, maxLength);

            char[] result = new char[length];
            char[] specialChars = specialCharsAllowed ?? _passwordCharsSpecialArray;

            char[][] passwordCharsAllArray = new[]
                                            {
                                                _passwordCharsUcaseArray,
                                                _passwordCharsLcaseArray,
                                                _passwordCharsNumericArray,
                                                specialChars,
                                            };

            int jMax = (requireSpecialCharacters) ? 4 : 3;
            //Random random = new Random();//GetRandom();

            for (int i = 0; i < length; i += jMax)
            {
                for (int j = 0; j < jMax; j++)
                {
                    char[] charArray = passwordCharsAllArray[j];
                    char randomCharChosen;

                    int safeLoopCounter = 0;
                    while (true)
                    {
                        randomCharChosen = charArray[_random.Next(0, charArray.Length)];
                        //Fix: changed == to >=...not sure how this is possible, but hey...
                        if ((safeLoopCounter >= (charArray.Length*5)) && (!result.Contains(randomCharChosen)))
                        {
                            break;
                        }
                        safeLoopCounter += 1;
                    }
                    result[i + j] = randomCharChosen;
                }
            }
            return new string(result);
        }
    }
}