namespace XAct.Claims
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Identity;

    /// <summary>
    /// An abstract base class for Claims that 
    /// can be persisted in a database.
    /// <para>
    /// See <see cref="IdentityBase{TEntity,TEntityAlias,TClaim}"/>
    /// (used as the basis of <c>XAct.Commerce.Customers.Customer</c>)
    /// which refers back to entities derived from the contract
    /// this base class implements.
    /// </para>
    /// </summary>
    [DataContract]
    public abstract class PersistableClaimBase : IHasPersistableClaim
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// The FK of the entity that this Claim describes.
        /// </summary>
        [DataMember]
        public virtual Guid OwnerFK { get; set; }


        /// <summary>
        /// The claim type (Name, SS, etc.)
        /// </summary>
        [DataMember]
        public virtual string Type { get; set; }


        /// <summary>
        /// The identity claim for the entity.
        /// </summary>
        [DataMember]
        public virtual string Value { get; set; }

        /// <summary>
        /// The guarantor of the claim.
        /// </summary>
        [DataMember]
        public virtual string Authority { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="PersistableClaimBase"/> class.
        /// </summary>
        protected PersistableClaimBase()
        {
            this.GenerateDistributedId();
        }
    }
}