﻿namespace XAct
{
    using System;


    /// <summary>
    /// A means to time an operation, using the 'using' statement.
    /// <para>
    /// <code>
    /// <![CDATA[
    /// using (var timedScope = new TimedScope(){
    /// ...
    ///		var elased = timedScope.Elapsed;
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// WARNING: due to using IDisposable, may be slower. Have to test...
    /// </para>
    /// </summary>
    public class TimedScope : IDisposable
    {
        private readonly DateTime _start = DateTime.UtcNow;

        /// <summary>
        /// Gets the time interval that has elasped since the scope was begun.
        /// </summary>
        /// <value>
        /// The elapsed.
        /// </value>
        public TimeSpan Elapsed { get { return DateTime.UtcNow - _start; } }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            //Nothing to do, just gives us the means to use the using syntax.
        }
    }

}
