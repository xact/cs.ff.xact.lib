﻿namespace XAct.State
{
    using System;
    using System.Collections;

    /// <summary>
    /// Contract for a current State service.
    /// <para>
    /// In a web app, this would be analogous to a Request state.
    /// </para>
    /// </summary>
    public interface IContextStateService : IHasXActLibService
    {
        ///// <summary>
        ///// Gets or sets the <see cref="System.Object"/> with the specified key.
        ///// </summary>
        //object this[string key] { get; set; }

        /// <summary>
        /// Gets the items.
        /// </summary>
        IDictionary Items { get; }




    }
}
