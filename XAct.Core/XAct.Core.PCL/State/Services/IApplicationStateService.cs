﻿namespace XAct.State
{
    using System;
    using System.Collections;

    /// <summary>
    /// Contract to provide an Application State Service that will
    /// work in both an Web Application, as well as a Win application.
    /// </summary>
    /// <remarks>
    /// <para>
    /// In a web application, the servic uses the underlying Application State -- accessing 
    /// it via Reflection -- and in a Win environment, it simply uses
    /// an internal hash.
    /// </para>
    /// </remarks>
    public interface IApplicationStateService : IHasXActLibService
    {


        /// <summary>
        /// Gets or sets the element
        /// with the specified key.
        /// </summary>
        /// <value></value>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        object this[string key] { get; set; }

        /// <summary>
        /// Gets the count of the number of Items in this Collection.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Gets the keys of the Session object.
        /// </summary>
        /// <value>The keys.</value>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        ICollection Keys { get; }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        void Add(string key, object value);

        /// <summary>
        /// Clears the collection.
        /// </summary>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        void Clear();

        /// <summary>
        /// Removes the specified entry from the Session collection.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:08 AM - S
        /// </tracking>
        /// </internal>
        void Remove(string key);

        /// <summary>
        /// Removes all entries in the Session collection.
        /// </summary>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:08 AM - S
        /// </tracking>
        /// </internal>
        void RemoveAll();

        /// <summary>
        /// Copies the values in the collection to the supplied array.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="index">The index.</param>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:08 AM - S
        /// </tracking>
        /// </internal>
        void CopyTo(Array array, int index);
    }
}