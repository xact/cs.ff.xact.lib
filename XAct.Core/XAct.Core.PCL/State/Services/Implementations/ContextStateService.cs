﻿namespace XAct.State.Implementations
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IContextStateService"/>
    /// </summary>
    [DefaultBindingImplementation(typeof (IContextStateService),BindingLifetimeType.SingletonPerWebRequestScope,Priority.Low)]
    public class ContextStateService : IContextStateService
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextStateService"/> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        public ContextStateService(IEnvironmentService environmentService)
        {
            environmentService.ValidateIsNotDefault("environmentService");
            _environmentService = environmentService;
        }

        ///// <summary>
        ///// Gets or sets the <see cref="System.Object"/> with the specified key.
        ///// <para>
        ///// Note that no exception is thrown if the key is not in the Items collection.
        ///// </para>
        ///// </summary>
        //public object this[string key]
        //{
        //    get { return Items[key]; }
        //    set { Items[key] = value; }
        //}

        /// <summary>
        /// Gets the items.
        /// </summary>
        public IDictionary Items
        {
            get
            {
                if (_environmentService.HttpContext != null)
                {
                    IDictionary items = _environmentService.HttpContext.GetMemberValue("Items") as IDictionary;
                    return items;
                }

                //http://blogs.msdn.com/b/atoakley/archive/2010/12/29/unity-lifetime-managers-and-wcf.aspx

                if (XAct.Library.Settings.State
                        .WhenHttpStateIsNotAvailableMakeContextStateThreadBasedRatherThanSingleton)
                {
                    //FIX:
                    //It's per thread:
                    IDictionary items2 = DependencyResolver.Current.GetInstance<IContextPerThread>();
                    return items2;
                }
                //Otherwise, 
                //it's Singleton (as the whole service is singleton) 
                return
                    _items;
            }
        }

        private readonly IDictionary _items = new Dictionary<object, object>();






    }
}