﻿namespace XAct.State.Implementations
{
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IApplicationStateService"/>
    /// allowing a unified way of working with Application State 
    /// in both Win and Web apps.
    /// </summary>
    /// <remarks>
    /// <para>
    /// It's a bit unfortunate that Application wasn't more portable:
    /// if you use it, you need a reference to System.Web, and of 
    /// course it doesn't work when not being hosted by IIS.
    /// </para>
    /// <para>
    /// That means, from a library point of view, when you would 
    /// like to write something that uses Application (not that that's 
    /// always a good decision in a RESTful app)  you can't without
    /// forcing your code to be for a web app only.
    /// </para>
    /// <para>
    /// And, as the Application collection is not available outside
    /// of a WebApp, you can't write unit tests against it, as you 
    /// can't swap it out for another mock service.</para>
    /// <para>
    /// This services solves those issues.
    /// </para>
    /// </remarks>
    public class ApplicationStateService : SessionState, IApplicationStateService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SessionStateService"/> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        public ApplicationStateService(IEnvironmentService environmentService):base(environmentService,ProxyContextCollectionType.Application)
        {
            //System.Web.SessionState
        }
    }
}