﻿namespace XAct.State.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof (IContextPerThread), BindingLifetimeType.SingletonPerThreadScope,Priority.Low)]
    public class ContextPerThread : Dictionary<object, object>, IContextPerThread
    {

    }
}