﻿namespace XAct.State
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using XAct.Environment;

    /// <summary>
    /// A Collection that wrap's a
    /// <c>System.Web.SessionState</c>,
    /// using an internal Hashtable when 
    /// there is no HttpContext (ie, a Win app, rather
    /// than  a WebApp).
    /// </summary>
    public class SessionState
    {
        
        //This is a static WinForm equivalent of 
        //WebForm Session object -- atleast for these vars...
        //private readonly Hashtable _proxyHashTable = new Hashtable();
        private readonly IDictionary<string,object> _proxyHashTable = new Dictionary<string, object>();


        private readonly ProxyContextCollectionType _proxyContextCollectionType;
        private readonly IEnvironmentService _environmentService;

        #region Constructor
        /// <summary>
        /// Initializes a new instance of 
        /// the <see cref="SessionState"/> class.
        /// </summary>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:06 AM - S
        /// </tracking>
        /// </internal>
        public SessionState(IEnvironmentService environmentService, ProxyContextCollectionType proxyContextCollectionType)
        {
            environmentService.ValidateIsNotDefault("environmentService");

            _environmentService = environmentService;
            _proxyContextCollectionType = proxyContextCollectionType;
        }
        #endregion


        #region Private Properties

        /// <summary>
        /// Gets the HTTP session from the HttpContext.
        /// </summary>
        /// <value>The HTTP session.</value>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        private object HttpSession
        {
            get
            {
                //Got strangness on second requests, when cached:
                ////If no longer null/0, return it:
                //if (!_httpSession.Equals(0))
                //{
                //    return _httpSession;
                //}

                //Never been checked before:

                //Get the current Context via Reflection:
                object context = _environmentService.HttpContext;

                //If that didn't work, hum:
                if (context == null)
                {
                    //Don't want to set _httpSession to other than 0 just yet:
                    return null;
                }

                //Right...good...set, and return it:
                _httpSession = context.GetMemberValue(_proxyContextCollectionType.ToString());

                return _httpSession;
            }
        }
        private object _httpSession=0;
        #endregion




        #region Public Properties
        /// <summary>
        /// Gets or sets the HttpContext.Item
        /// with the specified key.
        /// </summary>
        /// <value></value>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        public object this[string key]
        {
            get
            {
                //Note: can't use Reflection on a prop -- need a field:
                object session = this.HttpSession;
                
                if (session == null)
                {
                    //THe HttpContext does not throw an exception if the key is not found:
                    object result;
                    _proxyHashTable.TryGetValue(key, out result);
                    return result;
                }
                return session.InvokeMethod("get_Item", new[] { typeof(string), typeof(object) }, new object[] { key });
            }
            set
            {
                //Note: can't use Reflection on a prop -- need a field:
                object session = HttpSession;
                if (session == null)
                {
                    _proxyHashTable[key] = value;
                    return;
                }
                session.InvokeMethod("set_Item", new[]{typeof(string),typeof(object)}, new[] { key, value });

                ////Invoke the Set method:
                //object[] setArgs = { key, value };
                //    _HttpSessionType.InvokeMember("set_Item", BindingFlags.InvokeMethod, null, _httpSession, setArgs, null);
            }
        }


        /// <summary>
        /// Gets the count of the number of Items in this Collection.
        /// </summary>
        public int Count
        {
            get
            {
                //Note: can't use Reflection on a prop -- need a field:
                object session = HttpSession;
                if (session == null)
                {
                    return _proxyHashTable.Count;
                }
                return (int)session.GetMemberValue("Count");
            }
        }

        /// <summary>
        /// Gets the keys of the Session object.
        /// </summary>
        /// <value>The keys.</value>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        public ICollection Keys
        {
            get
            {
                //Note: can't use Reflection on a prop -- need a field:
                object session = HttpSession;
                if (session == null)
                {
                    //return _proxyHashTable.Keys;
                    return (ICollection) _proxyHashTable.Keys;
                }
                return (ICollection)session.GetMemberValue("Keys"); //get_Keys

            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        public void Add(string key, object value)
        {
            //Note: can't use Reflection on a prop -- need a field:
            object session = HttpSession;
            if (session == null)
            {
                _proxyHashTable.Add(key, value);
                return;
            }
            session.InvokeMethod("Add", new[] {key, value});
        }


        /// <summary>
        /// Clears the collection.
        /// </summary>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:07 AM - S
        /// </tracking>
        /// </internal>
        public void Clear()
        {
            //Note: can't use Reflection on a prop -- need a field:
            object session = HttpSession;
            if (session == null)
            {
                _proxyHashTable.Clear();
                return;
            }
            session.InvokeMethod("Clear", null);
        }

        /// <summary>
        /// Removes the specified entry from the Session collection.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:08 AM - S
        /// </tracking>
        /// </internal>
        public void Remove(string key)
        {
            //Note: can't use Reflection on a prop -- need a field:
            object session = HttpSession;
            if (session == null)
            {
                _proxyHashTable.Remove(key);
                return;
            }
            session.InvokeMethod("Remove", new object[] {key});
        }


        /// <summary>
        /// Removes all entries in the Session collection.
        /// </summary>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:08 AM - S
        /// </tracking>
        /// </internal>
        public void RemoveAll()
        {
            //Note: can't use Reflection on a prop -- need a field:
            object session = HttpSession;
            if (session == null)
            {
                _proxyHashTable.Clear();
                return;
            }
            session.InvokeMethod("RemoveAll", null);

        }
        /*
        public void RemoveAt(int index){
                //If there is no HttpContext we are in a WinForm:
                if (_environmentService.HttpContext == null) {
                    _ProxyStateCollection.RemoveAt(index);
                    return;
                }

            //Note: can't use Reflection on a prop -- need a field:
            object session = HttpSession;
            session.InvokeMethod("RemoveAt", new[] {index});

        }
        */
        /// <summary>
        /// Copies the values in the collection to the supplied array.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="index">The index.</param>
        /// <internal>
        /// 	<tracking>
        /// 4/2/2006 - 5:08 AM - S
        /// </tracking>
        /// </internal>
        public void CopyTo(Array array, int index)
        {
            //Note: can't use Reflection on a prop -- need a field:
            object session = HttpSession;
            //If there is no HttpContext we are in a WinForm:
            if (session == null)
            {
                object[] tmp = new object[array.Length];
                _proxyHashTable.Values.CopyTo(tmp, index);
                tmp.CopyTo(array, index);
                return;
            }
            session.InvokeMethod("CopyTo", new object[] { array, index});

        }

        #endregion



    }
}