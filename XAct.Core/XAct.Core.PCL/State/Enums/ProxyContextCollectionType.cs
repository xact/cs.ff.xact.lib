﻿namespace XAct.State
{
    /// <summary>
    /// Enumeration of types of <see cref="SessionState"/>
    /// collections.
    /// </summary>
    public enum ProxyContextCollectionType
    {
        /// <summary>
        /// The <see cref="SessionState"/>
        /// is wrapping the HttpSession.Application.
        /// </summary>
        Application,

        /// <summary>
        /// The <see cref="SessionState"/>
        /// is wrapping the HttpSession.Session.
        /// </summary>
        Session
    }
}