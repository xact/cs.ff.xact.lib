﻿//namespace XAct.Commands
//{
//    using XAct.Diagnostics;
//    using XAct.Messages;

//    /// <summary>
//    /// A <see cref="ICommandMessageHandler{TCommandMessage}"/>
//    /// demonstrating how to handle/process/execute on the data within
//    /// a <see cref="TraceCommandMessageHandler"/>
//    /// </summary>
//    public class TraceCommandMessageHandler :
//        CommandMessageHandlerBase<TraceCommandMessage>
//    {

//        /// <summary>
//        /// Initializes a new instance of the <see cref="TraceCommandMessageHandler"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        public TraceCommandMessageHandler(ITracingService tracingService)
//            :
//            base(
//            (x)=> tracingService.QuickTrace(x.Message,x.MessageArgs)
//            )
//        {
//        }
//    }
//}