﻿//namespace XAct
//{
//    /// <summary>
//    /// </summary>
//    //[Serializable]
//    public class LateBoundCommandMessage : IHasLateBoundCommandMessage
//    {
//        /// <summary>
//        /// Gets or sets the name of the assembly.
//        /// </summary>
//        /// <value>
//        /// The name of the assembly.
//        /// </value>
//        public string AssemblyName { get; set; }
//        /// <summary>
//        /// Gets or sets the name of the class.
//        /// </summary>
//        /// <value>
//        /// The name of the class.
//        /// </value>
//        public string TypeFullName { get; set; }
//        /// <summary>
//        /// Gets or sets the name of the method.
//        /// </summary>
//        /// <value>
//        /// The name of the method.
//        /// </value>
//        public string MethodName { get; set; }
//        /// <summary>
//        /// Gets or sets the optional parameters required by the method.
//        /// </summary>
//        /// <value>
//        /// The parameters.
//        /// </value>
//        public object[] Parameters { get; set; }
//    }
//}