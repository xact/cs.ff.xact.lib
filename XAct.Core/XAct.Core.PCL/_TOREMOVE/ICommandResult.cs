﻿
//namespace XAct.Commands
//{
//    /// <summary>
//    /// A specialization of the 
//    /// <see cref="ICommandValidationResponse"/>
//    /// in order to return a retun value
//    /// that the Command invoker could use.
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// For background, <see href=""/>
//    /// </para>
//    /// </remarks>
//    /// <typeparam name="TResult">The type of the result.</typeparam>
//    public interface ICommandResult<out TResult> : ICommandValidationResponse, IHasResult<TResult>
//    {
//        ///// <summary>
//        ///// Gets the result value returned by the
//        ///// <see cref="ICommand.Execute"/> 
//        ///// or 
//        ///// <see cref="ICommandMessageHandler{TCommandMessage, TCommandResponse}.Execute"/>
//        ///// </summary>
//        //new TResult Result { get; }
//    }
//}
