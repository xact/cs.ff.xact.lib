﻿//namespace XAct.Messages
//{
//    using System.Runtime.Serialization;

//    /// <summary>
//    /// The metadata to associate to a message code, in order
//    /// to describe where it's resources come from.
//    /// <para>
//    /// Note that although it's easier to use this approach, 
//    /// rather than <see cref="MessageCodeMetadataAttribute"/>,
//    /// that approach has the advantage in that it can't bleed 
//    /// the ResourceSet and ResourceKey to a client 
//    /// when serialized.
//    /// </para>
//    /// </summary>
//    [DataContract]
//    public class MessageCodeMetadata : IHasTextAndResourceFilterAndKeyReadOnly
//    {



//        /// <summary>
//        /// <para>
//        /// Parameter size is 1 byte.
//        /// </para>
//        /// </summary>
//        public byte ArgumentCount { get { return _argumentCount; } }
//        [DataMember(Name = "ArgumentCount")]
//        private byte _argumentCount;




//        /// <summary>
//        /// Gets the ResourceService Group/Set Key
//        /// of the resource containing the associated message.
//        /// </summary>
//        public string ResourceFilter
//        {
//            get { return _resourceFilter; }
//        }
//        [DataMember(Name = "ResourceFilter", EmitDefaultValue = false, IsRequired = false)]
//        private string _resourceFilter;



//        /// <summary>
//        /// Gets or sets the text.
//        /// <para>
//        /// IMPORTANT:
//        /// Prefer using <see cref="ResourceKey"/> and 
//        /// <see cref="ResourceFilter"/>
//        /// for anything but the most trivial apps (POCs, etc.)
//        /// </para>
//        /// </summary>
//        /// <value>
//        /// The text.
//        /// </value>
//        public string Text
//        {
//            get { return _text; }
//        }
//        [DataMember(Name = "Text", EmitDefaultValue = false, IsRequired = false)]
//        private string _text;


//        /// <summary>
//        /// Initializes a new instance of the <see cref="MessageCodeMetadata"/> class.
//        /// </summary>
//        /// <param name="resourceFilter">The resource set.</param>
//        /// <param name="resourceKey">The resource key.</param>
//        /// <param name="argumentCount">The argument count.</param>
//        public MessageCodeMetadata(string resourceFilter,string resourceKey, byte argumentCount)
//        {
//            _useResourceFilterAndKey = true;
//            _resourceFilter = resourceFilter;
//            _resourceKey = resourceKey;
//            _argumentCount = argumentCount;
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="MessageCodeMetadata" /> class.
//        /// </summary>
//        /// <param name="text">The text.</param>
//        /// <param name="argumentCount">The argument count.</param>
//        public MessageCodeMetadata(string text, byte argumentCount)
//        {
//            _useResourceFilterAndKey = false;
//            _text = text;
//            _argumentCount = argumentCount;
//        }


        
//        public override string ToString()
//        {
//            return "[Resource:{0}/{1}, ArgCount:{2}]"
//                .FormatStringInvariantCulture(_resourceKey,_resourceFilter,_argumentCount);
//        }
        
//    }
//}