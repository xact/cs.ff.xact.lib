﻿//namespace XAct.Services.IoC.Initialization
//{
//    using XAct.Commands;

//    /// <summary>
//    /// The custom response object from invoking  
//    /// <c>NinjectBootstrapCommandMessageHandler.Execute"</c>
//    /// <para>
//    /// Note: although strongly recommended that you look at why you
//    /// are doing everything in your power to bind your code to a
//    /// specific DependencyInjectionContainer, you can get a handle on the inner NInject Kernel
//    /// using the <see cref="CommandResultBase{TResult}.Result"/> property.
//    /// </para>
//    /// </summary>
//    /// <internal>
//    /// Created in both <c>XAct.Services.IoC.Ninject</c>
//    /// as well as<c>XAct.Services.IoC.Unity</c>
//    /// </internal>
//    public class BootstrapCommandResponse : CommandResultBase<object>, IBootstrapCommandResponse
//    {
//        /// <summary>
//        /// Gets or sets the results of having invoked
//        /// XAct.Library.Initialization.Bindings.RegisterBindings
//        /// </summary>
//        /// <value>
//        /// The initialize library services results.
//        /// </value>
//        public IInitializeLibraryBindingsResults InitializeLibraryServicesResults { get; set; }
//    }

//}

