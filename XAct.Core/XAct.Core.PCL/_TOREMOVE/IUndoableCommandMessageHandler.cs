﻿
//namespace XAct.Commands
//{
//    /// <summary>
//    /// The contract for a handler that will execute an operation,
//    /// using data within a given <see cref="ICommandMessage"/>
//    /// </summary>
//    /// <typeparam name="TCommandMessage">The type of the command message.</typeparam>
//    /// <typeparam name="TCommandResponse">The type of the command response.</typeparam>
//    /// <remarks>
//    ///   <para>
//    /// Note that the <c>CommandMessageHandler</c> pattern is an improvement
//    /// on the original <c>Command</c> pattern, separating the
//    /// Execution from the Message -- allowing for DI to occur.
//    ///   </para>
//    ///   <para>
//    /// For background, <see href=""/>
//    ///   </para>
//    /// </remarks>
//    public interface IUndoableCommandMessageHandler<in TCommandMessage>:
//        ICommandMessageHandler<TCommandMessage>
//        where TCommandMessage : ICommandMessage

//    {

//        /// <summary>
//        /// Validate the 
//        /// <see cref="ICommandMessage"/>
//        /// properties and required services, 
//        /// before Unexecution.
//        /// </summary>
//        /// <returns></returns>
//        ICommandValidationResponse ValidateForUnexecution(TCommandMessage commandMessage);

//        /// <summary>
//        /// Determines whether this command can be rolled back if 
//        /// <c>Execute</c> is invoked.
//        /// </summary>
//        /// <returns>
//        /// 	<c>true</c> if this instance can unexecute the command; otherwise, <c>false</c>.
//        /// </returns>
//        /// <internal><para>8/10/2011: Sky</para></internal>
//        bool CanUnexecute(TCommandMessage commandMessage);

//        /// <summary>
//        /// Unexecutes the specified Command.
//        /// </summary>
//        /// <param name="commandMessage">The command message.</param>
//        /// <returns></returns>
//        void Unexecute(TCommandMessage commandMessage);

//    }
//}
