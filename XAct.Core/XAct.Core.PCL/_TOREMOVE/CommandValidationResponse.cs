﻿//namespace XAct.Commands
//{
//    using System;
//    using XAct.Messages;

//    /// <summary>
//    /// An default implementation of <see cref="ICommandValidationResponse"/>
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Remember that we are not trying to return a value.
//    /// A command Execute method shouldn't return a message, 
//    /// but can give reasons for not validating it if the Command's
//    /// Validate method is invoked first.
//    /// </para>
//    /// </remarks>
//    public class CommandValidationResponse : Response, ICommandValidationResponse
//    {



//    }
//}
