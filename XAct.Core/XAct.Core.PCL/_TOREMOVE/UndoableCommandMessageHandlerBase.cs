﻿//namespace XAct.Commands
//{
//    /// <summary>
//    /// Abstract base class for CommandHandlers
//    /// that implement the
//    /// <see cref="ICommandMessageHandler{TCommandMessage,TCommandResponse}"/>
//    /// contract.
//    /// </summary>
//    /// <typeparam name="TCommandMessage">The type of the command message.</typeparam>
//    /// <typeparam name="TCommandResponse">The type of the command response.</typeparam>
//    public abstract class UndoableCommandMessageHandlerBase<TCommandMessage> 
//        : CommandMessageHandlerBase<TCommandMessage>,
//        IUndoableCommandMessageHandler<TCommandMessage>
//        where TCommandMessage : ICommandMessage

//    {

//        /// <summary>
//        /// Determines whether this command can be rolled back if <c>Execute</c> is invoked.
//        /// </summary>
//        /// <returns>
//        /// 	<c>true</c> if this instance can unexecute the command; otherwise, <c>false</c>.
//        /// </returns>
//        /// <internal><para>8/10/2011: Sky</para></internal>
//        public bool CanUnexecute(TCommandMessage commandMessage)
//        {
//            return ValidateForUnexecution(commandMessage).Success;
//        }


//        /// <summary>
//        /// Validate the 
//        /// <see cref="ICommandMessage"/>
//        /// properties and required services, 
//        /// before Unexecution.
//        /// </summary>
//        /// <returns></returns>
//        public abstract ICommandValidationResponse ValidateForUnexecution(TCommandMessage commandMessage);


//        /// <summary>
//        /// Rolls back the action performed by the 
//        /// <c>Execute</c> method.
//        /// </summary>
//        /// <param name="commandMessage">The command message.</param>
//        /// <returns></returns>
//        public abstract void Unexecute(TCommandMessage commandMessage);

//    }
//}

