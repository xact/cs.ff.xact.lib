﻿
//#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
//using System.Diagnostics.Contracts;
//#endif

//namespace XAct
//{
//    using System;
//    using System.Reflection;
//    using XAct.Commands;
//    using XAct.Diagnostics;


//    /// <summary>
//    /// A <see cref="ICommandMessageHandler{TCommandMessage}"/>
//    /// to handle/process/execute on the data within
//    /// a 
//    /// <see cref="IHasAssemblyAndTypeAndMethodNamesAndParameters"/>
//    /// </summary>
//    public class LateBoundCommandMessageHandler :
//        CommandMessageHandlerBase<IHasLateBoundCommandMessage>
//    {

//        private Assembly _assembly;
//        private Type _classType;
//        private MethodInfo _targetMethod;
//        private IHasAssemblyAndTypeAndMethodNamesAndParameters _lastCommand;


//        public LateBoundCommandMessageHandler(ITracingService tracingService) : base(
//            tracingService,
//            x =>
//                {
//                    IHasAssemblyAndTypeAndMethodNamesAndParameters lateBoundCommandMessage = x;

//                    var lastCommand = FindElements(lateBoundCommandMessage);
//                    if (!lastCommand)
//                    {
//                        throw new ArgumentException("commandMessage");
//                    }

//                    _targetMethod.Invoke(null, lateBoundCommandMessage.Parameters);
//                }
//            )
//        {
            
//        }
    



//        private static bool FindElements(IHasAssemblyAndTypeAndMethodNamesAndParameters lateBoundCommandMessage)
//        {
//            if (_lastCommand == lateBoundCommandMessage)
//            {
//                try
//                {
//                    _assembly = Assembly.Load(lateBoundCommandMessage.AssemblyName);
//                    _classType = _assembly.GetType(lateBoundCommandMessage.TypeFullName);
//                    _targetMethod = _classType.GetMethod(lateBoundCommandMessage.MethodName);
//                }
//                catch
//                {
//                }
//                _lastCommand = lateBoundCommandMessage;
//            }
//            return (_targetMethod != null);
//        }




//    }
//}