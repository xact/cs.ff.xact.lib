﻿//namespace XAct.Commands
//{
//    using System;

//    /// <summary>
//    /// An implementation of <see cref="ICommandResult{TResult}"/>
//    /// </summary>
//    /// <typeparam name="TResult">The type of the result.</typeparam>
//    public abstract class CommandResultBase<TResult> : CommandResponse, ICommandResult<TResult>
//    {
//        /// <summary>
//        /// Gets or sets the result returned by executing 
//        /// the CommandHandler on the CommandMessage data.
//        /// </summary>
//        /// <value>
//        /// The result.
//        /// </value>
//        public virtual TResult Result { 
//            get { return _result; } 
//            set
//            {
//                if (_hasBeenSet){throw new ArgumentException("Cannot reset Result once set.");}
//                _result = value;
//                _hasBeenSet = true;
//            } 

//        }
//        private TResult _result;
//        private bool _hasBeenSet;
//    }
//}
