﻿using System;

namespace XAct
{
    /// <summary>
    /// Extensions to the EventHandler:
    /// </summary>
    public static class EventHandlerExtensions
    {
        /// <summary>
        /// Raises the specified event handler.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Instead of the usual
        /// <code>
        /// <![CDATA[
        /// if (Click !=null){Click(this,e);}
        /// ]]>
        /// </code>
        /// just do:
        /// <code>
        /// <![CDATA[
        /// Click.Raise(this,e);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="eventHandler">The event handler.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public static void Raise(this EventHandler eventHandler,
            object sender, EventArgs e)
        {
            if (eventHandler != null)
            {
                eventHandler(sender, e);
            }
        }

        /// <summary>
        /// Raises the specified event handler.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Instead of the usual
        /// <code>
        /// <![CDATA[
        /// if (Click !=null){Click(this,e);}
        /// ]]>
        /// </code>
        /// just do:
        /// <code>
        /// <![CDATA[
        /// Click.Raise(this,e);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventHandler">The event handler.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        public static void Raise<T>(this EventHandler<T> eventHandler,
            object sender, T e) where T : EventArgs
        {
            if (eventHandler != null)
            {
                eventHandler(sender, e);
            }
        }
    }
}
