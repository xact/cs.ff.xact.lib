﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
namespace XAct {
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Data;

#endif

/// <summary>
    /// ExtensionMethods for Enumerable lists and collections
    /// to convert them to a CollectionPage.
    /// </summary>
    public static class CollectionPageExtensions
    {
        /// <summary>
        /// Converts a portion (single page's worth) of the source data into a 
        /// <see cref="CollectionPage{TItem}"/>
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="source">The source data.</param>
        /// <param name="pageIndex">The page index.</param>
        /// <param name="pageSize">The page size.</param>
        /// <returns>A <see cref="CollectionPage{TItem}"/></returns>
        public static CollectionPage<TItem> ToCollectionPage<TItem>(this IEnumerable<TItem> source, int pageIndex,
                                                                    int pageSize)
        {
            return new CollectionPage<TItem>(source, pageIndex, pageSize);
        }

        /// <summary>
        /// Converts a portion (single page's worth) of the source data into a 
        /// <see cref="CollectionPage{TItem}"/>
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="source">The source data.</param>
        /// <param name="pageIndex">The page index.</param>
        /// <param name="pageSize">The page size.</param>
        /// <returns>A <see cref="CollectionPage{TItem}"/></returns>
        public static CollectionPage<TItem> ToCollectionPage<TItem>(this IQueryable<TItem> source, int pageIndex,
                                                                    int pageSize)
        {
            return new CollectionPage<TItem>(source, pageIndex, pageSize);
        }
    }

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
