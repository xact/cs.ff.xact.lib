﻿namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    public static class IHasDistributedIdentityExtensions
    {
        /// <summary>
        /// Compares.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool Equals(this IHasDistributedIdentities entity, IHasDistributedIdentities value){
            if (entity.Id != value.Id){
                return false;
            }
            if (entity.MachineId != value.MachineId)
            {
                return false;
            }
            return true;
        }
    }
}
