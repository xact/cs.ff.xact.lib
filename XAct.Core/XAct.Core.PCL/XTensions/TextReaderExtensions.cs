﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    /// Extensions to TextReaders
    /// </summary>
    /// <internal><para>8/15/2011: Sky</para></internal>
    public static class TextReaderExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Detects the CSV separator.
        /// </summary>
        /// <internal>
        /// Src: http://bit.ly/pJ9Cjr
        /// </internal>
        /// <param name="reader">The reader.</param>
        /// <param name="rowCount">The row count.</param>
        /// <param name="separators">The separators.</param>
        /// <returns></returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public static char DetectCSVSeparator(this TextReader reader, int rowCount, IList<char> separators)
        {
            reader.ValidateIsNotDefault("reader");

            IList<int> separatorsCount = new int[separators.Count];

            int row = 0;

            bool quoted = false;
            bool firstChar = true;

            while (row < rowCount)
            {
                int character = reader.Read();

                switch (character)
                {
                    case '"':
                        if (quoted)
                        {
                            if (reader.Peek() != '"')
                                // Value is quoted and current character is " and next character is not ".
                                quoted = false;
                            else
                                reader.Read();
                                    // Value is quoted and current and next characters are "" - read (skip) peeked qoute.
                        }
                        else
                        {
                            if (firstChar) // Set value as quoted only if this quote is the first char in the value.
                                quoted = true;
                        }
                        break;
                    case '\n':
                        if (!quoted)
                        {
                            ++row;
                            firstChar = true;
                            continue;
                        }
                        break;
                    case -1:
                        row = rowCount;
                        break;
                    default:
                        if (!quoted)
                        {
                            int index = separators.IndexOf((char) character);
                            if (index != -1)
                            {
                                ++separatorsCount[index];
                                firstChar = true;
                                continue;
                            }
                        }
                        break;
                }

                if (firstChar)
                    firstChar = false;
            }

            int maxCount = separatorsCount.Max();

            return maxCount == 0 ? '\0' : separators[separatorsCount.IndexOf(maxCount)];
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
