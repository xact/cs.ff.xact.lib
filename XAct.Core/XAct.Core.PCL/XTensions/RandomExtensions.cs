﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Diagnostics.CodeAnalysis;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension methods to the Random Type.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    ///   <para>
    ///     Some light reading on the subject of random numbers: http://bit.ly/dYHPHe and http://bit.ly/fb9KLx
    ///   </para>
    /// </internal>
    public static class RandomExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns Random number between 0 and max
        /// </summary>
        /// <param name = "random">The <see cref = "System.Random" /> object.</param>
        /// <param name = "maximum"></param>
        /// <returns></returns>
        /// <remarks>
        ///   <para>
        ///     Note that range is 0 and Max Inclusive.
        ///   </para>
        /// </remarks>
        [SuppressMessage("Microsoft.Naming", "CA1719", Justification = "")]
        public static int Random(this Random random, int maximum)
        {
            random.ValidateIsNotDefault("random");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            return random.Next(maximum);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Return Random number between min and max.
        /// </summary>
        /// <param name = "random">The <see cref = "System.Random" /> object.</param>
        /// <param name = "minimum">Minimum number.</param>
        /// <param name = "maximum">Maximum number.</param>
        /// <returns>Integer</returns>
        /// <remarks>
        ///   <para>
        ///     Note that range is Min and Max Inclusive.
        ///   </para>
        /// </remarks>
        [SuppressMessage("Microsoft.Naming", "CA1719", Justification = "")]
        public static int Random(this Random random, int minimum, int maximum)
        {
            random.ValidateIsNotDefault("random");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
Contract.EndContractBlock();
#endif
            return random.Next(maximum - minimum) + minimum;
        }

        ////Makes it less portable...for something that is just about fine.
        //private Random GetRandom()
        //{
        //    System.Security.Cryptography.RNGCryptoServiceProvider cryptoServiceProvider =
        //        new System.Security.Cryptography.RNGCryptoServiceProvider();
        //    byte[] randomBytes = new byte[4];
        //    cryptoServiceProvider.GetBytes(randomBytes);
        //    // Convert 4 bytes into a 32-bit integer value.
        //    int seed = (randomBytes[0] & 0x7f) << 24 |
        //               randomBytes[1] << 16 |
        //               randomBytes[2] << 8 |
        //               randomBytes[3];

        //    // Now, this is real randomization.
        //    return new Random(seed);
        //}
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
