﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    /// Extensions to the Stack class definition.
    /// </summary>
    public static class StackExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// </summary>
        /// <param name="stack"></param>
        /// <returns></returns>
        public static string StackToString<T>(this Stack<T> stack)
        {
            IEnumerator enumerator = null;
            int length = ", ".Length;
            StringBuilder builder = new StringBuilder();
            try
            {
                enumerator = stack.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    builder.Append(enumerator.Current.ToString() + ", ");
                }
            }
            finally
            {
                if (enumerator is IDisposable)
                {
                    (enumerator as IDisposable).Dispose();
                }
            }
            builder.Replace("\"", "\"\"");
            if (builder.Length >= length)
            {
                builder.Remove(builder.Length - length, length);
            }
            return ("\"" + builder + "\"");
        }
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
