﻿using System;

namespace XAct
{
    public static class IHasDistributedGuidIdExtensions
    {
        static XAct.IDistributedIdService DistributedIdService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IDistributedIdService>(); }
        }


        /// <summary>
        /// Generates the model's identifier -- but only if model.Id is Guid.Empty.
        /// <para>
        /// Returns true if was set.
        /// </para>
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static bool GenerateDistributedId(this IHasId<Guid> model)
        {
            if (model.Id != Guid.Empty)
            {
                return false;
            }

            model.Id = DistributedIdService.NewGuid();

            return true;
        }

    }
}
