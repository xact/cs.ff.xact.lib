﻿namespace XAct
{
    using System;
    using XAct.Data.Updates;

    /// <summary>
    /// Extensions to the <see cref="DataStoreUpdateLog"/> object.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Usage is as follows:
    /// </para>
    /// <para>
    /// Seeding encounters something like:
    /// <code>
    /// <![CDATA[
    /// var environmentIdentifier = _hostSettingsService.Get<string>("EnvironmentIdentifier"); 
    /// var check = updateLogs.SingleOrDefault(x=>x.Name=="XYZ");
    /// if (check == null || (!check.Applied && check.IsValidEnvironment(environmentIdentifier))){
    ///    if (hostSettings.Get("EnvironmentIndentifier" == "XYZ")){
    ///      context.AddOrUpdate(...);
    ///    }
    ///    ...
    ///    updateLogs.Add(new UpdateLog("XYZ","Updated xyz...",true));
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public static class UpdateLogExtensions
    {
        public static bool IsValidEnvironment(this DataStoreUpdateLog updateLog, string environmentIdentifier)
        {

            if ((updateLog.EnvironmentIdentifier.IsNullOrEmpty()) || (updateLog.EnvironmentIdentifier == "*"))
            {
                //the update is for all environments:
                return true;
            }

            //It's for a specific environment(s). Is the current environment one of them?

            //Loop through all parts of the defined names, and see if one of them 
            //matches the current environment name:
            foreach (
                string environmentIdentifierPart in
                    updateLog.EnvironmentIdentifier.Split(new[] { ',', '|', ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (string.Compare(environmentIdentifierPart, environmentIdentifier, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    //host.EnvironmentIdentifier == C, and updateLog.EnvironmentIdentifier = "B,C,D"
                    return true;
                }
            }
            //either 
            //host.EnvironmentIdentifier == A, and updateLog.EnvironmentIdentifier = "B,C,D"
            //host.EnvironmentIdentifier == null, and updateLog.EnvironmentIdentifier = "B,C,D"
            return false;
        }
    }
}