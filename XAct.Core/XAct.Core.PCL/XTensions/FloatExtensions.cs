﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension Methods to the Float Type.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class FloatExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Test Function - Returns true if Int of Float is Odd number
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        public static bool IsOdd(this float number)
        {
            return !IsEven(number);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Test Function - Returns true if Int or Float is Even number.
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        public static bool IsEven(this float number)
        {
            return (number/2) == Math.Floor(number/2);
        }
    }




#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
