﻿namespace XAct
{
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Messages;

    /// <summary>
    /// Extension methods to <see cref="MessageCode"/> instances.
    /// </summary>
    public static class MessageCodeExtensions
    {

        public static IMessageCodeMetadataService  MessageCodeMetadataService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IMessageCodeMetadataService>(); }
        } 
        


        ///// <summary>
        ///// Gets the resource key for the given MessageCode, 
        ///// extracted from the <see cref="MessageCodeMetadataAttribute"/>
        ///// that was attached to the instance when it was created.
        ///// <para>
        ///// <code>
        ///// <![CDATA[
        ///// public static class MessageCodes {
        /////   [MessageCodeResourceMetadata("MessageCodes","XYZ")]
        /////   public static MessageCode XYZ = new MessageCode(123,Severity.Info,1);
        /////   ...
        ///// }
        ///// ]]>
        ///// </code>
        ///// </para>
        ///// </summary>
        ///// <param name="messageCode">The message code.</param>
        ///// <returns></returns>
        //public static string GetResourceKey(this MessageCode messageCode)
        //{
        //    MessageCodeMetadataAttribute messageCodeMetadataAttribute =
        //        messageCode.GetMetadata();
            
            
        //    return messageCodeMetadataAttribute.ResourceKey;

        //}



        public static bool GetMetadata(this MessageCode messageCode, out string resourceFilter, out string text,
                               out byte argumentCount,
                                IMessageCodeMetadataService messageCodeMetadataService = null)
        {
            if (messageCodeMetadataService == null)
            {
                messageCodeMetadataService = MessageCodeMetadataService;
            }

            MessageCodeMetadataAttribute messageCodeMetadataAttribute = messageCode.GetMetadata(messageCodeMetadataService);

            if (messageCodeMetadataAttribute == null)
            {
                resourceFilter = null;
                text = null;
                argumentCount = 0;

                return false;
            }

            resourceFilter = messageCodeMetadataAttribute.ResourceFilter;
            text = messageCodeMetadataAttribute.Text;
            argumentCount = messageCodeMetadataAttribute.ArgumentCount;

            return true;
        }



        /// <summary>
        /// Gets the resource key for the given MessageCode,
        /// extracted from the <see cref="MessageCodeMetadataAttribute" />
        /// that was attached to the instance when it was created.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// public static class MessageCodes {
        /// [MessageCodeResourceMetadata("MessageCodes","XYZ")]
        /// public static MessageCode XYZ = new MessageCode(123,Severity.Info,1);
        /// ...
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="messageCodeMetadataService">The message code metadata service.</param>
        /// <returns></returns>

        public static MessageCodeMetadataAttribute GetMetadata(this MessageCode messageCode, IMessageCodeMetadataService messageCodeMetadataService=null)
        {

            if (messageCodeMetadataService == null)
            {
                messageCodeMetadataService = MessageCodeMetadataService;
            }

            MessageCodeMetadataAttribute messageCodeMetadataAttribute;
            
            TryGetMetadata(messageCode, out messageCodeMetadataAttribute, messageCodeMetadataService);

            return messageCodeMetadataAttribute;
        }


        public static bool TryGetMetadata(this MessageCode messageCode,
                                          out MessageCodeMetadataAttribute messageCodeMetadataAttribute,
                                          IMessageCodeMetadataService messageCodeMetadataService = null)
        {
            if (messageCodeMetadataService == null)
            {
                messageCodeMetadataService = MessageCodeMetadataService;
            }

            return messageCodeMetadataService.TryGet(messageCode, out messageCodeMetadataAttribute);

        }



        /// <summary>
        /// Checks a collection of messages for a particular message code
        /// </summary>
        /// <param name="messageCodes"></param>
        /// <param name="checkForMessageCode"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public static bool ContainsMessageCode(this IEnumerable<Message> messageCodes, MessageCode checkForMessageCode, bool recursive = true)
        {
            // Check root level
            if (messageCodes.Any(m => m.MessageCode.Id == checkForMessageCode.Id))
            {
                return true;
            }

            if (!recursive)
            {
                return false;
            }

            // Search nested messages, recursively. (Careful when modifying - no infinite loops please)
            foreach (var messageCode in messageCodes)
            {
                // Skip empty collections
                if (messageCode.InnerMessages == null)
                {
                    continue;
                }

                // Search nested messages, recursively. (Careful when modifying - no infinite loops please)
                var result = messageCode.InnerMessages.ContainsMessageCode(checkForMessageCode,recursive);

                if (result)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool ContainsMessageCode(this IEnumerable<MessageCode> messageCodes,
                                               MessageCode checkForMessageCode)
        {
            return (messageCodes.Any(m => m.Id == checkForMessageCode.Id));
        }

        /// <summary>
        /// Checks a collection of message codes if it contains any of a given set of message codes
        /// </summary>
        /// <param name="messageCodes"></param>
        /// <param name="checkForMessageCodes"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public static Message ContainsMessageCode(this IEnumerable<Message> messageCodes, IEnumerable<MessageCode> checkForMessageCodes, bool recursive = true)
        {
            // Check root level
            var foundMessage = messageCodes.FirstOrDefault(m => checkForMessageCodes.Contains(m.MessageCode));

            if (foundMessage != null)
            {
                return foundMessage;
            }

            if (!recursive)
            {
                return null;
            }

            // Search nested messages, recursively. (Careful when modifying - no infinite loops please)




            foreach (var messageCode in messageCodes)
            {
                // Skip empty collections
                if (messageCode.InnerMessages == null)
                {
                    continue;
                }

                // Search nested messages, recursively. (Careful when modifying - no infinite loops please)
                var result = messageCode.InnerMessages.ContainsMessageCode(checkForMessageCodes,recursive);

                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }


    }
}
