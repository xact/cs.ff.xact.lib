﻿//namespace XAct
//{
//    using System;
//    using System.IO;
//    using System.Text;

//    public static class WrappedObjectExtensions
//    {
//        public static void ValidateIsRightTypeOrSubClassThereof(this WrappedObject o, Type expectedType)
//        {
//            XAct.Extensions.ObjectExtensions.ValidateIsRightTypeOrSubClassThereof(o, expectedType);
//        }

//        /// <summary>
//        ///   Serialize the object and return a string of xml.
//        /// </summary>
//        /// <param name = "objectToSerialize">The object to serialize.</param>
//        /// <returns>A string of xml.</returns>
//        //[SuppressMessage("Microsoft.Naming", "CA1720", Justification = "objectToSerialize is more descriptive than 'value'.")]
//        public static string ToXmlString(this WrappedObject objectToSerialize)
//        {
//            return XAct.Extensions.ObjectExtensions.ToXmlString(objectToSerialize.Item);
//        }



















//        /// <summary>
//        /// Validates the object is of the right type or a sub class thereof.
//        /// </summary>
//        /// <param name="o">The o.</param>
//        /// <param name="expectedType">The expected type.</param>
//        public static void ValidateIsRightTypeOrSubClassThereof(this object o, Type expectedType)
//        {
//            //Unwrap if need be:
//            UnWrap(o);

//            if (o.GetType().IsSubClassOfEx(expectedType))
//            {
//                return;
//            }

//            string tIs = o.GetType().AssemblyQualifiedName;
//            string tShould = expectedType.AssemblyQualifiedName;

//            throw new ArgumentNullException(
//                "serverEventContext ('{0}') not correct Type (should be '{1}')".FormatStringCurrentCulture(tIs, tShould));

//        }



//        /// <summary>
//        /// <para>
//        /// An XActLib Extension.
//        /// </para>
//        ///   Determines whether the given type is a subclass of the given superclass type.
//        ///   <para>
//        ///     Example: example.IsSubClassOfEx(typeof(Example));//true
//        ///     Example: example.IsSubClassOfEx(typeof(IExample));//true
//        ///   </para>
//        /// </summary>
//        /// <param name = "childClassInstance">Type of the child sub class.</param>
//        /// <param name = "parentSuperClass">The parent base class.</param>
//        /// <returns>
//        ///   <c>true</c> if [is A subclass of] [the specified child sub class type]; otherwise, <c>false</c>.
//        /// </returns>
//        /// <exception cref = "System.ArgumentNullException" />
//        public static bool IsSubClassOfEx(this object childClassInstance, Type parentSuperClass)
//        {
//            //Unwrap if need be:
//            UnWrap(childClassInstance);

//            childClassInstance.ValidateIsNotDefault("childClassInstance");
//            parentSuperClass.ValidateIsNotDefault("parentSuperClass");
//#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
//            Contract.EndContractBlock();
//#endif
//            return childClassInstance.GetType().IsSubClassOfEx(parentSuperClass);
//        }



//        /// <summary>
//        ///   Serialize the object and return a string of xml.
//        /// </summary>
//        /// <param name = "objectToSerialize">The object to serialize.</param>
//        /// <returns>A string of xml.</returns>
//        //[SuppressMessage("Microsoft.Naming", "CA1720", Justification = "objectToSerialize is more descriptive than 'value'.")]
//        public static string ToXmlString(this object objectToSerialize)
//        {
//            //Unwrap if need be:
//            UnWrap(objectToSerialize);

//            //OK: if (!objectToSerialize.IsNotNull()) { throw new ArgumentNullException("objectToSerialize"); }
//#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
//            Contract.EndContractBlock();
//#endif
//            StringBuilder stringBuilder = new StringBuilder();
//            using (StringWriter writer = new StringWriter(stringBuilder, _invariantCulture))
//            {
//                //Use the extension method
//                writer.Serialize(objectToSerialize);
//            }
//            return stringBuilder.ToString();
//        }

//        /// <summary>
//        /// <para>
//        /// An XActLib Extension.
//        /// </para>
//        /// Provides a way to attach property values dynamically
//        /// to any object.
//        /// <para>
//        /// Wouldn't recommend using it...it's just a proof of concept really.
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// <code>
//        /// <![CDATA[
//        /// object o = new object();
//        /// o.GetDynamicAttributes().Add("stunk", "wow");
//        /// var u = o.GetDynamicAttributes()["stunk"];
//        /// o.GetDynamicAttributes()["stunk"] = "new";
//        /// var x = o.GetDynamicAttributes()["stunk"];
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <internal>
//        /// Not happy yet.
//        /// What it needs is a Timer to cleanup weakreferences when
//        /// objects no longer alive.
//        /// </internal>
//        /// <param name="target">The target.</param>
//        /// <returns></returns>
//        /// <internal><para>8/5/2011: Sky</para></internal>
//        public static Dictionary<string, object> GetDynamicAttributes(this object target)
//        {

//            target.ValidateIsNotDefault("target");

//            //Unwrap if need be:
//            UnWrap(target);

//            WeakReference weakReference;

//            //
//            _dynamicPropertiesCache.TryGetValue(target.GetHashCode(), out weakReference);

//            if (weakReference == null)
//            {
//                //Doesn't exist...create new
//                weakReference = new WeakReference(target);
//                Dictionary<string, object> result = new Dictionary<string, object>();

//                _dynamicPropertiesCache[target.GetHashCode()] = weakReference;
//                _dynamicPropertiesCache2[weakReference] = result;

//                //return newly created dictionary:
//                return result;
//            }

//            //Let's clean up the dictionary...
//            //Note: that this is suboptimal solution
//            KeyValuePair<int, WeakReference>[] deadOnes = _dynamicPropertiesCache.Where(p => p.Value.IsAlive).ToArray();
//            foreach (KeyValuePair<int, WeakReference> deadOne in deadOnes)
//            {
//                _dynamicPropertiesCache.Remove(deadOne.Key);
//                _dynamicPropertiesCache2.Remove(deadOne.Value);
//            }

//            return _dynamicPropertiesCache2[weakReference];
//        }








//        /// <summary>
//        /// <para>
//        /// An XActLib Extension.
//        /// </para>
//        /// Sets the values.
//        /// </summary>
//        /// <typeparam name="TObject">The type of the object.</typeparam>
//        /// <param name="o">The o.</param>
//        /// <param name="values">The values.</param>
//        /// <returns></returns>
//        public static void SetValues<TObject>(this TObject o, IDictionary<string, object> values)
//        {
//            //Unwrap not needed as is Typed by T.

//            foreach (KeyValuePair<string, object> pair in values)
//            {
//                o.SetMemberValue(pair.Key, pair.Value);
//            }
//        }

//        /// <summary>
//        /// <para>
//        /// An XActLib Extension.
//        /// </para>
//        /// Gets the values.
//        /// </summary>
//        /// <typeparam name="TObject">The type of the object.</typeparam>
//        /// <param name="o">The o.</param>
//        /// <param name="values">The values.</param>
//        public static void GetValues<TObject>(TObject o, IDictionary<string, object> values)
//        {
//            //Unwrap not needed as is Typed by T.

//            foreach (KeyValuePair<string, object> pair in values)
//            {
//                values[pair.Key] = o.GetMemberValue(pair.Key);
//            }
//        }







//        /// <summary>
//        /// <para>
//        /// An XActLib Extension.
//        /// </para>
//        /// makes a unique key string from the name of the DataObject <see cref="Type"/> and its <c>HashCode</c>.
//        /// </summary>
//        /// <param name="dataObject"></param>
//        /// <returns></returns>
//        public static int MakeUniqueIdentifier(this object dataObject)
//        {
//            //Unwrap if need be:
//            UnWrap(dataObject);

//            dataObject.ValidateIsNotDefault("dataObject");
//            //Get the type of the object:
//            Type dataObjectType = dataObject.GetType();
//            //Make a Unique string Key for it because we can't use a Strong ref:
//            //(as it would defeat the purpose of a weak-linked TrackingVar...):



//            return (dataObjectType + ":" + dataObject.GetHashCode()).GetHashCode();
//        }





//        /// <summary>
//        /// Tests the WCF serialization.
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <param name="message">The message.</param>
//        /// <returns></returns>
//        public static T TestWCFSerialization<T>(this T message)
//            where T : class
//        {
//            //Unwrap not needed as is Typed by T.

//            return typeof(T).TestWCFSerialization(message) as T;
//        }


//        public static void ToDataContractStream<T>(this T objectToSerialize, Stream streamToSerializeInto)
//        {
//            //Unwrap not needed as is Typed by T.
//            TypeExtensions.ToDataContractStream(typeof(T), objectToSerialize, streamToSerializeInto);
//        }


//        public static string ToDataContractString<T>(this object objectToSerialize)
//        {
//            UnWrap(objectToSerialize);

//            return TypeExtensions.ToDataContractString(typeof(T), objectToSerialize);
//        }


//        /// <summary>
//        /// Serializes an object to a string, in the JSON format,
//        /// using the specified encoding.
//        /// <para>
//        /// Reminder: for an object to be serializable, it must
//        /// be decorated with DataContract/DataMember attributes.
//        /// </para>
//        /// </summary>
//        /// <param name="objToSerialize">The obj to serialize.</param>
//        /// <param name="type">The type.</param>
//        /// <param name="encoding">The encoding.</param>
//        /// <returns></returns>
//        /// <internal>7/16/2011: Sky</internal>
//        public static string ToJSON(this WrappedObject objToSerialize, Type type, Encoding encoding)
//        {
//            return XAct.Extensions.ObjectExtensions.ToJSON(objToSerialize.Item, type, encoding);
//        }














//    }


//}
