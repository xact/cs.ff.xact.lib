
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace

namespace XAct
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using XAct.Collections.Comparers;

    // ReSharper restore CheckNamespace
#endif


    //Try this: http://bit.ly/e47wBG

    /// <summary>
    ///   Class of Static Methods for 
    ///   working with IEnumerable collections.
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <internal>
    ///   See: http://blog.dotnetwiki.org/PermaLink,guid,c773e395-d715-4313-90c2-3c34d1102b2f.aspx
    /// </internal>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    // ReSharper disable InconsistentNaming
    public static class IEnumerableExtensions
        // ReSharper restore InconsistentNaming
    {
        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;

        #region Methods

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the collection contains an item 
        ///   which matches the given <see cref = "Predicate{T}" />.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Array&lt;T&gt; and List&lt;T&gt; has a very useful 
        ///     <c>Exists</c> method, but they don't work on collections 
        ///     or other IEnumerable collections.
        ///     This was intended to solve this.
        ///   </para>
        ///   <para>
        ///     The following simple sample demonstrates its use
        ///     to see if a given list contains numbers within a given range:
        ///     <code>
        ///       <![CDATA[
        /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,40,60,80,100};
        /// bool found = Exists<int>(
        ///                           myList,
        ///                           delegate(int i){return ((i>10)&&(i<100));}
        ///                           );
        /// ]]>
        ///     </code>
        ///   </para>
        ///   <para>
        ///     This returns <c>true</c> of course.
        ///   </para>
        /// </remarks>
        /// <typeparam name = "T">The Type of the collection items.</typeparam>
        /// <param name = "collection">The collection of elements to iterate through.
        /// </param>
        /// <param name = "predicateToApply">The predicateToApply.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified collection]; 
        ///   otherwise, <c>false</c>.
        /// </returns>
        public static bool Exists<T>(this
                                         IEnumerable<T> collection,
                                     Predicate<T> predicateToApply)
        {
            collection.ValidateIsNotDefault("collection");
            predicateToApply.ValidateIsNotDefault("predicateToApply");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            // ReSharper disable ConvertClosureToMethodGroup
            return collection.Any(item => predicateToApply.Invoke(item));
            // ReSharper restore ConvertClosureToMethodGroup
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Filters the collection according to the given predicate.
        ///   return only the items whose match with predicateToApply
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     The following sample demonstrates the filtering of a collection
        ///     for values by a function that checks for the values being 
        ///     in a specified range:
        ///     <code>
        ///       <![CDATA[
        /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,40,60,80,100};
        /// IEnumerable<int> resultList = 
        ///   Collections.Filter<int>(
        ///       myList,
        ///       delegate(int id) {return ((id > 10) && (id <100));}
        ///       );
        /// foreach (int i in resultList){
        ///   //Do Something...
        /// }
        /// ]]>
        ///     </code>
        ///   </para>
        ///   <para>
        ///     This will return:
        ///     <c>{20,40,60,80}</c>
        ///   </para>
        ///   <para>
        ///     Note that as the results are <c>yield</c>ed, 
        ///     one can write the same thing a 
        ///     little more compactly as follows:
        ///     <code>
        ///       <![CDATA[
        /// foreach (int i in Collections.Filter<int>(
        ///           myList,
        ///           delegate(int id) {return ((id > 10) && (id <100));}
        ///         ){
        ///   //Do Something...
        /// }
        /// ]]>
        ///     </code>
        ///   </para>
        ///   <para>
        ///     Note that the result collection size 
        ///     will always be &lt;= size as the input collection
        ///     (the result will depend on the number 
        ///     of input items that pass the predicate).
        ///   </para>
        /// </remarks>
        /// <typeparam name = "T">The Type of the collection items.</typeparam>
        /// <param name = "collection">The collection of elements to iterate through.
        /// </param>
        /// <param name = "predicateToApply">The predicate method to test each 
        ///   collection item with.</param>
        /// <returns>The items that passed the predicate.</returns>
        public static IEnumerable<T> Filter<T>(this
                                                   IEnumerable<T> collection,
                                               Predicate<T> predicateToApply)
        {
            collection.ValidateIsNotDefault("collection");
            predicateToApply.ValidateIsNotDefault("predicateToApply");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            // ReSharper disable ConvertClosureToMethodGroup
            return collection.Where(item => predicateToApply.Invoke(item));
            // ReSharper restore ConvertClosureToMethodGroup
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Finds an item in the collection which matches the specified predicate.
        ///   Returns true if an item is matched, false otherwise.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Array&lt;T&gt; and List&lt;T&gt; has a very useful 
        ///     Find method, but they don't work on collections 
        ///     or other IEnumerable collections.
        ///     This was intended to solve this.
        ///   </para>
        ///   <para>
        ///     <code>
        ///       <![CDATA[
        /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,40,60,80,100};
        /// int foundItem=-1;
        /// bool found = Find<int>(
        ///                           myList,
        ///                           delegate(int i){return ((i>10)&&(i<100));}
        ///                           ref foundItem);
        /// ]]>
        ///     </code>
        ///     The result will be <c>true</c>, and 
        ///     fill <c>matchedValue</c> with <c>20</c>.
        ///   </para>
        /// </remarks>
        /// <typeparam name = "T">The Type of the collection items.</typeparam>
        /// <param name = "collection">The collection of elements to iterate through.
        /// </param>
        /// <param name = "predicateToApply">The predicateToApply.</param>
        /// <param name = "matchedItem">The matchedItem.</param>
        /// <returns></returns>
        public static bool Find<T>(this
                                       IEnumerable<T> collection,
                                   Predicate<T> predicateToApply, ref T matchedItem)
        {
            collection.ValidateIsNotDefault("collection");
            predicateToApply.ValidateIsNotDefault("predicateToApply");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            foreach (T item in collection)
            {
                if (!predicateToApply.Invoke(item))
                {
                    continue;
                }
                matchedItem = item;
                return true;
            }
            return false;
        }


        /*
    public static IEnumerable<T> FindAll(IEnumerable<T> collection,
      Predicate<T> predicateToApply) {
      return Filter(collection, predicateToApply);
    }
    */


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Finds the position/index of the in the collection 
        ///   which matches the specified predicate.
        ///   <para>
        ///     Returns true if an item is matched, false otherwise.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Array&lt;T&gt; and List&lt;T&gt; has a very useful 
        ///     FindIndex method, but its not available for 
        ///     other IEnumerable collections.
        ///     This was intended to solve this.
        ///   </para>
        ///   <para>
        ///     <code>
        ///       <![CDATA[
        /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,40,60,80,100};
        /// int foundItem=-1;
        /// bool found = FindIndex<int>(
        ///                           myList,
        ///                           delegate(int i){return ((i>10)&&(i<100));}
        ///                           ref foundItem);
        /// ]]>
        ///     </code>
        ///     The result will be <c>11</c>, and 
        ///     the <c>matchedValue</c> will be filled with <c>20</c>.
        ///   </para>
        /// </remarks>
        /// <typeparam name = "T">The Type of the collection items.</typeparam>
        /// <param name = "collection">The collection of elements to iterate through.
        /// </param>
        /// <param name = "predicateToApply">The predicateToApply.</param>
        /// <param name = "matchedItem">The matchedItem.</param>
        /// <returns></returns>
        public static int FindIndex<T>(this IEnumerable<T> collection,
                                       Predicate<T> predicateToApply, ref T matchedItem)
        {
            collection.ValidateIsNotDefault("collection");
            predicateToApply.ValidateIsNotDefault("predicateToApply");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            int position = 0;

            foreach (T item in collection)
            {
                if (predicateToApply.Invoke(item))
                {
                    matchedItem = item;
                    return position;
                }
                position++;
            }
            return -1;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Finds an item in the collection which matches the specified predicate.
        ///   Returns true if an item is matched, false otherwise.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Array&lt;T&gt; and List&lt;T&gt; has a very useful 
        ///     Find method, but they don't work on collections 
        ///     or other IEnumerable collections.
        ///     This was intended to solve this.
        ///   </para>
        ///   <para>
        ///     <code>
        ///       <![CDATA[
        /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,40,60,80,100};
        /// int foundItem=-1;
        /// bool found = FindLast<int>(
        ///                           myList,
        ///                           delegate(int i){return ((i>10)&&(i<100));}
        ///                           ref foundItem);
        /// ]]>
        ///     </code>
        ///     The result will be <c>true</c>, and 
        ///     fill <c>matchedValue</c> with <c>80</c>.
        ///   </para>
        ///   <para>
        ///     Note that this method is much slower than <see cref = "Find{T}" />
        ///     if the collection cannot be converted to ICollection{T}, as 
        ///     it then has to enumerate through all elements in the collection, 
        ///     no matter what (since one cannot enumerate backwards).
        ///   </para>
        /// </remarks>
        /// <typeparam name = "T">The Type of the collection items.</typeparam>
        /// <param name = "collection">The collection of elements to iterate through.
        /// </param>
        /// <param name = "predicateToApply">The predicateToApply.</param>
        /// <param name = "matchedItem">The matchedItem.</param>
        /// <returns></returns>
        public static bool FindLast<T>(this
                                           IEnumerable<T> collection,
                                       Predicate<T> predicateToApply, ref T matchedItem)
        {
            collection.ValidateIsNotDefault("collection");
            predicateToApply.ValidateIsNotDefault("predicateToApply");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            bool result = false;

            ICollection<T> collection2 = collection as ICollection<T>;
            if ((collection2 != null) && (collection2.Count > 0))
            {
                //return list[list.Count-1];
                return collection2.FindLast(predicateToApply, ref matchedItem);
                //[collection2.Count-1];
            }


            foreach (T item in collection)
            {
                if (!predicateToApply.Invoke(item))
                {
                    continue;
                }
                matchedItem = item;
                result = true;
            }
            return result;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Apply an <see cref = "Action" /> to each item in a collection.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Array&lt;T&gt; and List&lt;T&gt; both have a very useful 
        ///     ForEach method -- but that doesn't do us much good
        ///     when we want to work with other IEnumerable based classes.
        ///     This was intended to solve this.
        ///   </para>
        ///   <para>
        ///     <code>
        ///       <![CDATA[
        /// List<int> result = new List<int>();
        /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,40,60,80,100};
        /// //invoke ForEach, passing in list, and an anonymous method 
        /// //as the Action&lt;T&gt;:
        /// ForEach<int>( myList, 
        ///               delegate(int id) {
        ///                 //neato: anon methods can refer
        ///                 //to vars in parent context,
        ///                 //which makes it very useful:
        ///                 if (id>10){result.Add(++id);}
        ///               }
        ///             );
        /// foreach(int i in result){
        ///   Trace.WriteLine(i);
        /// }
        /// ]]>
        ///       This results in the following list:
        ///       <c>{21,41,61,81,101}</c>.
        ///     </code>
        ///   </para>
        ///   <para>
        ///     Note that this method is very similar to <c>Filter</c>
        ///     except that it takes only an <see cref = "Action" />, 
        ///     rather than a <c>Predicate</c>.
        ///   </para>
        ///   <para>
        ///     Note that unlike <c>Map</c>, no collection is returned.
        ///   </para>
        /// </remarks>
        /// <typeparam name = "T">The Type of the collection items.</typeparam>
        /// <param name = "collection">The collection to iterate through.</param>
        /// <param name = "actionToApply">
        ///   The function to apply to each element 
        ///   of the collection.</param>
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> actionToApply)
        {
            collection.ValidateIsNotDefault("collection");
            actionToApply.ValidateIsNotDefault("actionToApply");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            foreach (T item in collection)
            {
                actionToApply.Invoke(item);
            }
        }



        /// <summary>
        /// Calls the single param action on each item in the set that is of type T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="action">The action.</param>
        public static void ForEach<T>(this IEnumerable enumerable, Action<T> action) where T : class
        {
            foreach (object untypedObject in enumerable)
            {
                T typedTarget = untypedObject as T;

                if (typedTarget != null)
                {
                    action.Invoke(typedTarget);
                }
            }
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Retuen a new collection corresponding to the result of 
        ///   applying a function (f)
        ///   to each item in the given collection.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     <code>
        ///       <![CDATA[
        /// MemberInfo[] members = this.GetType().GetMembers();
        /// foreach(string name in Map<MemberInfo,string>(
        ///                members,
        ///                delegate(MemberInfo memberInfo){return memberInfo.Name;}
        ///                );
        ///                ){
        ///            Trace.WriteLine(name);
        /// }
        /// ]]>
        ///     </code>
        ///   </para>
        ///   <para>
        ///     Note that the result collection will always 
        ///     be the same size as the input collection.
        ///   </para>
        /// </remarks>
        /// <typeparam name = "TCollectionItem">The Type of the collection items.
        /// </typeparam>
        /// <typeparam name = "TReturn">The Type of the return value.</typeparam>
        /// <param name = "collection">The collection of elements to iterate through.
        /// </param>
        /// <param name = "functionToApply">The function to apply to each 
        ///   item &lt;Type, ReturnType&gt;.</param>
        /// <returns></returns>
        public static IEnumerable<TReturn> Map<TCollectionItem, TReturn>(
            this IEnumerable<TCollectionItem> collection,
            Func<TCollectionItem, TReturn> functionToApply)
        {
            collection.ValidateIsNotDefault("collection");
            functionToApply.ValidateIsNotDefault("functionToApply");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            // ReSharper disable ConvertClosureToMethodGroup
            return collection.Select(item => functionToApply.Invoke(item));
            // ReSharper restore ConvertClosureToMethodGroup
        }


        //        /// <summary>
        //        ///   TODO
        //        /// </summary>
        //        /// <typeparam name = "TItem"></typeparam>
        //        /// <param name = "collection"></param>
        //        /// <param name = "predicate"></param>
        //        /// <returns></returns>
        //        public static IEnumerable<TItem> Where<TItem>(this IEnumerable<TItem> collection, Predicate<TItem> predicate)
        //        {
        //            collection.ValidateIsNotDefault("collection");
        //            predicate.ValidateIsNotDefault("predicate");
        //#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
        //            Contract.EndContractBlock();
        //#endif

        //            return Enumerable.Where(collection, item => predicate(item));
        //        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the items contained in both lists.
        /// <para>
        /// Wraps the <see cref="Intersect{T}"/>
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static IEnumerable<T> GetItemsContainedInBothLists<T>(this IEnumerable<T> first, IEnumerable<T> second,
                                                                     Func<T, object> expression)
        {
            return first.Union(second, expression);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        /// Wraps the Linq Except{T} extension.
        /// </para>
        /// Overload of the System.Linq Except method 
        /// to return a collection that contains all items 
        /// except those that match the given member expression.
        /// <para>
        /// Returns items in <paramref name="first"/> that are not in <paramref name="second"/>
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static IEnumerable<T> GetItemsExceptThoseFoundInBothLists<T>(this IEnumerable<T> first,
                                                                            IEnumerable<T> second,
                                                                            Func<T, object> expression)
        {
            return first.Except(second, expression);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Overload of the <c>System.Linq</c> <c>Except</c> method 
        /// to return a collection that contains all items 
        /// from <paramref name="referenceCollection"/> list
        /// except those that match an item in the given <paramref name="updatedCollection"/> expression.
        /// <para>
        /// An example use would be retrieving <paramref name="referenceCollection"/> from a repository,
        /// round-tripping the collection, retrieving it as <paramref name="updatedCollection"/>,
        /// and using this method to determine which records have been deleted.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="referenceCollection">The first.</param>
        /// <param name="updatedCollection">The second.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static IEnumerable<T> Except<T>(this IEnumerable<T> referenceCollection, IEnumerable<T> updatedCollection,
                                               Func<T, object> expression)
        {
            return referenceCollection.Except(updatedCollection, new FuncComparer<T>(expression));
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Returns the Intersects of the <paramref name="first"/> and <paramref name="second"/> list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static IEnumerable<T> Intersect<T>(this IEnumerable<T> first, IEnumerable<T> second,
                                                  Func<T, object> expression)
        {
            return first.Intersect(second, new FuncComparer<T>(expression));
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Returns the Union of the <paramref name="first"/> and <paramref name="second"/> list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static IEnumerable<T> Union<T>(this IEnumerable<T> first, IEnumerable<T> second,
                                              Func<T, object> expression)
        {
            return first.Union(second, new FuncComparer<T>(expression));
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Implements an action on all items in <paramref name="firstCollection"/>
        /// collection, found in <paramref name="secondCollection"/> collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="firstCollection">The first.</param>
        /// <param name="secondCollection">The second.</param>
        /// <param name="matchExpression">The expression.</param>
        /// <param name="updateSecondCollectionItemOverFirstCollectionItemAction">The action.</param>
        public static void ForEach<T>(
            this IEnumerable<T> firstCollection, 
            IEnumerable<T> secondCollection,
            Func<T, object> matchExpression, 
            Action<T, T> updateSecondCollectionItemOverFirstCollectionItemAction)
        {

            FuncComparer<T> expComparer = new FuncComparer<T>(matchExpression);

            T[] secondList = secondCollection.ToArray();

            //Iterate through first collection 
            foreach (T i in firstCollection)
            {
                //looking for items found also in second collection:
                T t = secondList.FirstOrDefault(i2 => expComparer.Equals(i, i2));

                //If not null, perform the action.
                if (Object.Equals(t, default(T)))
                {
                    continue;
                }

                updateSecondCollectionItemOverFirstCollectionItemAction(t, i);
            }
        }




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convert a collection to a CSV string.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     <code>
        ///       <![CDATA[
        /// int[] myList = new int[] {1,2,3,4,5,6,7,8,9,10,20,40,60,80,100};
        /// ]]>
        ///     </code>
        ///     The result will be: <c>"{1,2,3,4,5,6,7,8,9,10,20,40,60,80,100}"</c>
        ///   </para>
        ///   <para>
        ///     Note that the result is wrapped in '{' and '}'.
        ///   </para>
        /// </remarks>
        /// <param name = "collection">The collection of elements to iterate through.</param>
        /// <param name = "divChar">Optional divChar. 
        ///   If an empty string is provided, "," will be used.
        ///   If null, an empty string will be used.</param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Performance", "CA1800", Justification = "Ok code.")]
        public static string CollectionToString(this IEnumerable collection, string divChar)
        {
            StringBuilder sb = new StringBuilder();
            if (collection == null)
            {
                sb.Append("null");
            }
            else
            {
                //Note: no divchar is acceptable.
                if (divChar == null)
                {
                    divChar = ", ";
                }
                sb.Append("{");
                foreach (object item in collection)
                {
                    if (item is IEnumerable)
                    {
                        sb.Append(((IEnumerable) item).CollectionToString(divChar));
                    }
                    else
                    {
                        sb.Append("{0},".FormatStringInvariantCulture(item));
                    }
                }
                int divCharLength = divChar.Length;
                sb.Remove(sb.Length - divCharLength, divCharLength); // remove ending ','
                sb.Append("}");
            }
            return sb.ToString();
        }

        #endregion

        //private static void Misc()
        //{
        //    List<string> t = new List<string>();
        //    //NOt.Filter
        //    //t.Sort(
        //    //string t.Find(Predicate<T>)
        //    //List<string> = t.FindAll(Predicate<T>)
        //    //int= t.FindIndex(Predicate<T>)
        //    //string =t.FindLast(Predicate<T>)
        //    //t.ForEach(Action<T>)
        //    //bool = t.Exists(Predicate<T>)
        //}


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convenient replacement for a range 'for' loop. e.g. return an array of int from 10 to 20:
        ///   int[] tenToTwenty = 10.to(20).ToArray();
        /// </summary>
        /// <param name = "from"></param>
        /// <param name = "to"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Naming", "CA1719", Justification = "")]
        public static IEnumerable<int> To(this int from, int to)
        {
            for (int i = from; i <= to; i++)
            {
                yield return i;
            }
        }

        //public static void ToConsole<T>(this IEnumerable<T> list)
        //{
        //    list.ForEach(n => Console.Write("{0} ".With(n)));
        //}

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns the odd positioned items in the list.
        /// </summary>
        /// <typeparam name = "T">The type of the items in the list.</typeparam>
        /// <param name = "list">The list.</param>
        /// <returns></returns>
        public static IEnumerable<T> AtOddPositions<T>(this IEnumerable<T> list)
        {
            bool odd = false; // 0th position is even
            foreach (T item in list)
            {
                odd = !odd;
                if (odd)
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns the even positioned items in the list.
        /// </summary>
        /// <typeparam name = "T">The type of the items in the list.</typeparam>
        /// <param name = "list">The list.</param>
        /// <returns></returns>
        public static IEnumerable<T> AtEvenPositions<T>(this IEnumerable<T> list)
        {
            list.ValidateIsNotDefault("list");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            bool even = true; // 0th position is even
            foreach (T item in list)
            {
                even = !even;
                if (even)
                {
                    yield return item;
                }
            }
        }


        //public static string AsCsv<T>(this IEnumerable<T> items)
        //    where T : class
        //{
        //    var csvBuilder = new StringBuilder();
        //    var properties = typeof (T).GetProperties();
        //    foreach (T item in items)
        //    {
        //        string line = properties.Select(p => p.GetValue(item, null).ToCsvValue()).ToArray().Join(",");
        //        csvBuilder.AppendLine(line);
        //    }
        //    return csvBuilder.ToString();
        //}

        //private static string ToCsvValue<T>(this T item)
        //{
        //    if(item == null)
        //    {
        //        return "\"{0}\"".With(item);
        //    }

        //    if (item is string)
        //    {
        //        return "\"{0}\"".With(item.ToString().Replace("\"", "\\\""));
        //    }
        //    double dummy;
        //    if (double.TryParse(item.ToString(), out dummy))
        //    {
        //        return "{0}".With(item);
        //    }
        //    return "\"{0}\"".With(item);
        //}


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts the passed list to a Sql IN clause.
        /// </summary>
        /// <param name = "list">The list to enumerate</param>
        /// <param name = "formatCode">
        ///   The formatting code to use on the 
        ///   value before adding it to the output string.</param>
        /// <param name = "useQuotes">
        ///   Flag to indicate whether values 
        ///   need to be wrapped in quotes first.</param>
        /// <param name = "useLowercase">
        ///   Flag to indicate whether to lowercase the values first.</param>
        /// <param name = "addBrackets">
        ///   Flag to indicate whether to add brackets around 
        ///   the whole fragment before returning.</param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Performance", "CA1800", Justification = "Ok code.")]
        public static string ConvertListToInClause(
            IEnumerable list,
            string formatCode,
            bool useQuotes, bool useLowercase, bool addBrackets)
        {
            if (list == null)
            {
                return string.Empty;
            }

            //bool useFormatting = String.IsNullOrEmpty(formatCode);

            StringBuilder stringBuilder = new StringBuilder();

            //Loop through args:
            foreach (object listItem in list)
            {
                //Stick a comma between args:
                stringBuilder.Append(",");
                //Quote if asked for:
                if (useQuotes)
                {
                    stringBuilder.Append("'");
                }
                //Convert the value to a string:
                string val = (listItem is IFormattable)
                                 ? ((IFormattable) listItem).ToString(
                                     formatCode,
                                     CultureInfo.CurrentCulture)
                                 : listItem.ToString();

                //Lowercase if asked for (sometimes this is expected for Guid's)
                stringBuilder.Append((!useLowercase) ? val : val.ToLower());
                //Quote if asked for:
                if (useQuotes)
                {
                    stringBuilder.Append("'");
                }
            }

            //Remove first comma:
            if (stringBuilder.Length != 0)
            {
                stringBuilder.Remove(0, 1);
            }

            //Bracket the whole thing:
            if ((addBrackets) && (stringBuilder.Length > 0))
            {
                //we have args so bracket them:
                stringBuilder.Insert(0, "(");
                stringBuilder.Append(")");
            }
            //Convert to a string:
            return stringBuilder.ToString();
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Appends the given item to the collection.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "collection">The collection.</param>
        /// <param name = "item">The item.</param>
        /// <returns></returns>
        public static IEnumerable<T> Append<T>(this IEnumerable<T> collection, T item)
        {
            collection.ValidateIsNotDefault("collection");

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            foreach (T colItem in collection)
            {
                yield return colItem;
            }

            yield return item;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Prepends the item to the collection.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "collection">The collection.</param>
        /// <param name = "item">The item.</param>
        /// <returns></returns>
        public static IEnumerable<T> Prepend<T>(this IEnumerable<T> collection, T item)
        {
            collection.ValidateIsNotDefault("collection");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            yield return item;

            foreach (T colItem in collection)
            {
                yield return colItem;
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <typeparam name = "TValue">The type of the value.</typeparam>
        /// <param name = "collection">The collection.</param>
        /// <param name = "function">The function.</param>
        /// <returns></returns>
        public static T ArgMax<T, TValue>(
            this IEnumerable<T> collection,
            Func<T, TValue> function)
            where TValue : IComparable<TValue>
        {
            return ArgComp(collection, function, GreaterThan);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Greaters the than.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "first">The first.</param>
        /// <param name = "second">The second.</param>
        /// <returns></returns>
        private static bool GreaterThan<T>(T first, T second)
            where T : IComparable<T>
        {
            return first.CompareTo(second) > 0;
        }

        /// <summary>
        ///   Args the min.
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <typeparam name = "TValue">The type of the value.</typeparam>
        /// <param name = "collection">The collection.</param>
        /// <param name = "function">The function.</param>
        /// <returns></returns>
        public static T ArgMin<T, TValue>(
            this IEnumerable<T> collection,
            Func<T, TValue> function)
            where TValue : IComparable<TValue>
        {
            return ArgComp(collection, function, LessThan);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Lesses the than.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "first">The first.</param>
        /// <param name = "second">The second.</param>
        /// <returns></returns>
        private static bool LessThan<T>(T first, T second) where T : IComparable<T>
        {
            return first.CompareTo(second) < 0;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Args the comp.
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <typeparam name = "TValue">The type of the value.</typeparam>
        /// <param name = "collection">The collection.</param>
        /// <param name = "function">The function.</param>
        /// <param name = "accept">The accept.</param>
        /// <returns></returns>
        private static T ArgComp<T, TValue>(
            IEnumerable<T> collection, Func<T, TValue> function,
            Func<TValue, TValue, bool> accept)
            where TValue : IComparable<TValue>
        {
            collection.ValidateIsNotDefault("collection");

            function.ValidateIsNotDefault("function");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            bool isSet = false;
            T maxArg = default(T);
            TValue maxValue = default(TValue);

            foreach (T item in collection)
            {
                TValue value = function(item);
                if (isSet && !accept(value, maxValue))
                {
                    continue;
                }
                maxArg = item;
                maxValue = value;
                isSet = true;
            }

            return maxArg;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts an Enumerable list of objects to a list of strings (using ToString()).
        /// <para>
        /// Converts Null items to string.Empty.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="x">The x.</param>
        /// <returns></returns>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public static string[] ToStringArray<T>(this IEnumerable<T> x)
        {
            string[] strings =
                x.Select(y => EqualityComparer<T>.Default.Equals(y) ? string.Empty : y.ToString()).ToArray();

            return strings;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Joins the strings (safely -- doesn't blow up if the list, or any item within the list, is a null).
        /// <para>
        /// Returns string.Empty if the list is null.
        /// </para>
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public static string JoinSafely<T>(this IEnumerable<T> items, string separator)
        {
            return items == null ? string.Empty : items.ToStringArray().JoinSafely(separator);
        }

        /// <summary>
        /// Gets Distinct elements (ie no duplicates) for the collection, based on the value of evaluation of the given property expression on each item.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="keySelector">The key selector.</param>
        /// <returns></returns>
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
            (this IEnumerable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();

            Func<TSource,TKey> compiled = keySelector.Compile();

            foreach (TSource element in source)
            {

                TKey x = compiled.Invoke(element);

                if (!seenKeys.Contains(x))
                {
                    seenKeys.Add(x);
                    yield return element;
                }
            }
        }




        /// <summary>
        /// Creates list of items that are not members of given list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProperty">The type of the property to compare on.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="item">The item.</param>
        /// <param name="keySelector">The key selector.</param>
        /// <returns></returns>
        public static List<T> AddIfNotAlreadyAdded<T, TProperty>(this IEnumerable<T> list, T item,
                                                                 Expression<Func<T, TProperty>> keySelector)
        {
            //Convert enumerable list to List to make it easier to add to:
            List<T> results = list.ToList();

            
            Func<T, TProperty> compiled = keySelector.Compile();



            if (results.Contains(item, x => Object.Equals(compiled.Invoke(x), (compiled.Invoke(item)))))
            {
                results.Add(item);
            }
            return results;

        }

        /// <summary>
        /// Adds if not already added.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="items">The items.</param>
        /// <param name="keySelector">The key selector.</param>
        /// <returns></returns>
        public static List<T> AddIfNotAlreadyAdded<T, TProperty>(this IEnumerable<T> list, IEnumerable<T> items,
                                                                 Expression<Func<T, TProperty>> keySelector)
        {
            //Convert enumerable list to List to make it easier to add to:
            List<T> results = list.ToList();

            if (items == null)
            {
                return results;
            }
            
            Func<T, TProperty> compiled = keySelector.Compile();


            foreach (T item in items)
            {
                if (results.Contains(item, x => Object.Equals(compiled.Invoke(x), (compiled.Invoke(item)))))
                {
                    results.Add(item);
                }
            }
            return results;

        }





        /// <summary>
        /// Creates list of items that are not members of given list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static List<T> AddIfNotAlreadyAdded<T>(this IEnumerable<T> list, T item)
        {

            //Convert enumerable list to List to make it easier to add to:
            List<T> results = list.ToList();

            if (!results.Contains(item))
            {
                results.Add(item);
            }
            return results;

        }

        /// <summary>
        /// Adds items to given list, returning list as a new List, type List{T}
        /// Creates list of items that are not members of given list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public static List<T> AddIfNotAlreadyAdded<T>(this IEnumerable<T> list, IEnumerable<T> items)
        {
            //Convert enumerable list to List to make it easier to add to:
            List<T> results = list.ToList();
            
            if (items == null)
            {
                return results;
            }
            
            foreach (T item in items)
            {
                if (!results.Contains(item))
                {
                    results.Add(item);
                }
            }
            return results;
        }


        /// <summary>
        /// Finds the index of the first item that fits the predicate.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public static int IndexOf<T>(this IEnumerable<T> items, Func<T, bool> predicate)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (predicate == null) throw new ArgumentNullException("predicate");

            int retVal = 0;
            foreach (T item in items)
            {
                if (predicate(item)) return retVal;
                retVal++;
            }
            return -1;
        }



        ///<summary>Finds the index of the first occurence of an item in an enumerable.</summary>
        ///<param name="items">The enumerable to search.</param>
        ///<param name="item">The item to find.</param>
        ///<returns>The index of the first matching item, or -1 if the item was not found.</returns>
        public static int IndexOf<T>(this IEnumerable<T> items, T item) { return items.IndexOf(i => EqualityComparer<T>.Default.Equals(item, i)); }





        /// <summary>
        /// Performs a topological sort over the source list.
        /// <para>
        /// Usage:
        /// <![CDATA[
        /// void Main()
        /// {
        /// 	List<Foo> items = new List<Foo>();
        /// 	items.Add(new Foo{Id="A"});
        /// 	items.Add(new Foo{Id="B",ConditionalOn="D"});
        /// 	items.Add(new Foo{Id="C",ConditionalOn="B"});
        /// 	items.Add(new Foo{Id="D"});
        /// 	items.Add(new Foo{Id="E"});
        /// 	
        /// 	items.TSort(x=> items.Where(y=>y.Id== x.ConditionalOn)).Dump();
        /// }
        ///  public class Foo {
        ///      public string Id { get; set; }
        ///      public string ConditionalOn { get; set; }
        ///  }
        /// ]]>
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="dependencies">The dependencies.</param>
        /// <param name="throwOnCycle">if set to <c>true</c> [throw on cycle].</param>
        /// <returns></returns>
        public static IEnumerable<T> TopologicalSort<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> dependencies, bool throwOnCycle = false)
        {
            var sorted = new List<T>();
            var visited = new HashSet<T>();

            foreach (var item in source)
            {
                Visit(item, visited, sorted, dependencies, throwOnCycle);
            }
            return sorted;
        }

        private static void Visit<T>(T item, HashSet<T> visited, List<T> sorted, Func<T, IEnumerable<T>> dependencies, bool throwOnCycle)
        {
            if (visited.Contains(item))
            {
                if (throwOnCycle)
                {
                    throw new Exception("Cyclic dependency found");
                }
            }
            else
            {
                visited.Add(item);

                foreach (var dep in dependencies(item))
                {
                    Visit(dep, visited, sorted, dependencies, throwOnCycle);
                }
                sorted.Add(item);
            }
        }


        /// <summary>
        /// Converts input list to string delimited by separator. Each item in the list is converted by given function.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="separator">The separator.</param>
        /// <param name="toStringFunction">To sring function.</param>
        /// <returns></returns>
        public static string ToDelimited<T>(this IEnumerable<T> list, string separator, Func<T, string> toStringFunction)
        {
            if (list == null) return string.Empty;
            if (toStringFunction == null)
            {
                toStringFunction = (x) => x.ToString();
            }
            return String.Join(separator, list.Select(toStringFunction));
        }

        /// <summary>
        /// Gets the duplicates.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="selector">The selector.</param>
        /// <returns></returns>
        public static IEnumerable<TSource> GetDuplicates<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector)
        {
            var grouped = source.GroupBy(selector);
            var moreThen1 = grouped.Where(i => i.IsMultiple());

            return moreThen1.SelectMany(i => i);
        }

        public static bool IsMultiple<T>(this IEnumerable<T> source)
        {
            var enumerator = source.GetEnumerator();
            return enumerator.MoveNext() && enumerator.MoveNext();
        }



        /// <summary>
        /// Returns true when collection is empty.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="source">The System.Collections.Generic.IEnumerable to check for emptiness.</param>
        /// <returns></returns>
        public static bool None<TSource>(this IEnumerable<TSource> source)
        {
            return !source.Any();
        }


        /// <summary>
        /// removes the specified Item from the given source list.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static IList<TSource> Except<TSource>(this IEnumerable<TSource> source, TSource item)
        {
            return source.Except(new List<TSource> { item }).ToList();
        }



        public static bool AllValuesUnique<T>(this IEnumerable<T> source)
        {
            var tmp = source.ToArray();
            return tmp.Distinct().Count() == tmp.Count();
        }


    }
#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}

#endif
