﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.IO;
    using System.Text;
    using XAct.Net.Messaging;

// ReSharper restore CheckNamespace
// ReSharper restore CheckNamespace
#endif

    /// <summary>
    ///   Extensions for converting objects to different types.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class ByteArrayExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts the given byte array to a Hex string.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Alternate ways of doing this that you may have seen are:
        ///     <code>
        ///       <![CDATA[
        /// //Method a:
        /// System.Text.StringBuilder sb =
        ///   new System.Text.StringBuilder();
        /// foreach (byte b in hashBuffer) {
        ///   sb.Append(b.ToString("x2"));//'X2' for uppercase
        /// }
        /// return sb.ToString();
        /// ]]>
        ///     </code>
        ///     or
        ///     <code>
        ///       <![CDATA[
        /// //Method B: 
        /// string hashString =
        ///  BitConverter.ToString(hashBuffer);
        /// //The output looks like:
        /// //"19-E2-62-AE-3A-84-0D-72-1F-EF-32-C9-25-D1-A1-89-67-13-5F-58"
        /// //so you want to remove the hashes in most cases:
        /// //return hashString.Replace("-", "");
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "byteArray">The byteArray.</param>
        /// <returns>a string</returns>
        public static string ToHexString(this byte[] byteArray)
        {
            byteArray.ValidateIsNotDefault("byteArray");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
                        Contract.EndContractBlock();
#endif
            char[] result = new char[byteArray.Length*2];


            int i = 0;
            foreach (byte b in byteArray)
            {
                result[i] = GetHexChar(b/0x10);
                result[i + 1] = GetHexChar(b%0x10);
                i += 2;
            }
            return new string(result, 0, result.Length);
        }

        // Used by ToHexString
        private static char GetHexChar(int number)
        {
            return (char) ((number < 10) ? (number + 0x30) : ((number - 10) + 0x41));
        }







        ///// <summary>
        ///// <para>
        ///// An XActLib Extension.
        ///// </para>
        /////   Convert a ByteArray to Str
        ///// </summary>
        ///// <param name = "bytesArray"></param>
        ///// <returns></returns>
        //public static string ByteArrayToString(this byte[] bytesArray)
        //{
        //    if (bytesArray != null)
        //    {
        //        ASCIIEncoding encoding = new ASCIIEncoding();
        //        return encoding.GetString(bytesArray);
        //    }
        //    return string.Empty;
        //}



        /// <summary>
        /// Wraps the byte array to a MemoryStream
        /// </summary>
        /// <param name="bytesArray">The bytes array.</param>
        /// <returns></returns>
        public static Stream ToStream(this byte[] bytesArray)
        {
            return new MemoryStream(bytesArray);
        }





        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts a ByteArray to a String. Default is UTF-8
        /// </summary>
        /// <param name="byteArray">The byte array.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public static string ToString(this byte[] byteArray, Encoding encoding = null)
        {
            if ((byteArray == null) || (byteArray.Length == 0))
            {
                return string.Empty;
            }

            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            //PCL does not support ASCII, so switching to UTF8
            return encoding.GetString(byteArray,0,byteArray.Length);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convert Array of Bytes to a Char array.
        /// </summary>
        /// <param name = "bytesArray"></param>
        /// <returns></returns>
        /// <remarks>
        ///   Demonstrates conversion of a Byte array to a Char array.
        /// </remarks>
        public static char[] ByteArrayToCharArray(this byte[] bytesArray, Encoding encoding=null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            return bytesArray == null ? null : bytesArray.ToString(encoding).ToCharArray();

            //There HAS to be a simpler way?!?
        }

        /// <summary>
        ///   Encode a byte array to Base64 string.
        /// </summary>
        /// <param name = "bytesArray">Byte array of characters to convert to Base64 string.</param>
        /// <returns></returns>
        public static string ByteArrayToBase64(this byte[] bytesArray)
        {
            return (bytesArray != null) ? Convert.ToBase64String(bytesArray) : string.Empty;
        }


        /// <summary>
        /// Changes the encoding of a byte array
        /// <para>
        /// Used to change a <see cref="PersistedFile"/> that is in UTF-8
        /// to another encoding (eg: ISO-8859-1).
        /// </para>
        /// </summary>
        /// <param name="bytesArray">The bytes array.</param>
        /// <param name="srcEncoding">The source encoding.</param>
        /// <param name="targetEncoding">The target encoding.</param>
        /// <returns></returns>
        public static byte[] ChangeEncoding(this byte[] bytesArray, Encoding srcEncoding, Encoding targetEncoding)
        {
            if (object.Equals(srcEncoding, targetEncoding))
            {
                return bytesArray;
            }

            return Encoding.Convert(srcEncoding, targetEncoding, bytesArray);

        }

    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
