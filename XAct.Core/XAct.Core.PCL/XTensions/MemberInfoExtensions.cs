﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Reflection;

    // ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// Extensions to the MemberInfo object.
    /// </summary>
    public static class MemberInfoExtensions
    {
        ///// <summary>
        ///// <para>
        ///// An XActLib Extension.
        ///// </para>
        /////   Gets the specified attribute: null if it doesn't exist.
        ///// </summary>
        ///// <param name = "propertyInfo">The <see cref = "PropertyInfo" />.</param>
        ///// <returns>The found attribute, or null.</returns>
        //public static TAttribute GetAttribute<TAttribute>(this MemberInfo propertyInfo)
        //    where TAttribute : Attribute
        //{
        //    return (TAttribute)GetAttribute(propertyInfo, typeof(TAttribute));
        //}

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the specified <see cref="PropertyInfo"/> has the specified attribute.
        /// </summary>
        /// <param name="propertyInfo">The <see cref="PropertyInfo"/>.</param>
        /// <param name="attributeType">The Attribute Type.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns>
        ///   <c>true</c> if the specified propertyInfo has the attribute; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasAttribute(this MemberInfo propertyInfo, Type attributeType, bool inherit = true)
        {
            propertyInfo.ValidateIsNotDefault("propertyInfo");
#if CONTRACTS_FULL || NET40 || NET45
            // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            object[] attributes = propertyInfo.GetCustomAttributes(attributeType, inherit);
            return attributes.Length > 0;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the specified <see cref="PropertyInfo"/> has the specified attribute.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="memberInfo">The <see cref="PropertyInfo"/>.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns>
        ///   <c>true</c> if the specified propertyInfo has the attribute; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasAttribute<TAttribute>(this MemberInfo memberInfo, bool inherit = true)
            where TAttribute : Attribute
        {
            memberInfo.ValidateIsNotDefault("propertyInfo");
#if CONTRACTS_FULL || NET40 || NET45
            // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            object[] attributes = memberInfo.GetCustomAttributes(typeof (TAttribute), inherit);
            return attributes.Length > 0;
        }



        ///// <summary>
        //// <para>
        //// An XActLib Extension.
        //// </para>
        /////   Gets the attribute if found, null otherwise.
        ///// </summary>
        ///// <typeparam name = "TAttribute">The type of the attribute.</typeparam>
        ///// <param name = "propertyInfo">The property info.</param>
        ///// <returns></returns>
        //public static TAttribute GetAttribute<TAttribute>(this MemberInfo propertyInfo)
        //    where TAttribute : Attribute
        //{
        //    return (TAttribute)GetAttribute(propertyInfo, typeof(TAttribute));
        //}

        /// <summary>
        /// Gets the specified Attribute if there is one attached to the given Type.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="memberInfo">Type of the this entity.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns></returns>
        public static TAttribute GetAttribute<TAttribute>(this MemberInfo memberInfo, bool inherit = true)
            where TAttribute : Attribute
        {
            memberInfo.ValidateIsNotDefault("entityType");
#if CONTRACTS_FULL || NET40 || NET45
            // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            object[] attributes = memberInfo.GetCustomAttributes(typeof (TAttribute), inherit);

            return (attributes.Length == 0) ? null : (TAttribute) attributes[0];
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the attribute if found, null otherwise.
        /// </summary>
        /// <param name="memberInfo">The property info.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns></returns>
        public static Attribute GetAttribute(this MemberInfo memberInfo, Type attributeType, bool inherit = true)
        {
            memberInfo.ValidateIsNotDefault("propertyInfo");
#if CONTRACTS_FULL || NET40 || NET45
            // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            object[] attributes = memberInfo.GetCustomAttributes(attributeType, inherit);

            if (attributes.Length == 0)
            {
                return null;
                //default(attributeType);
            }
            return attributes[0] as Attribute;
        }

        /// <summary>
        /// Gets the FullName ("<c>NamespaceName.ClassName.MemberName</c>")
        /// </summary>
        /// <param name="memberInfo">The member information.</param>
        /// <param name="useReflectedType">if set to <c>true</c> use ReflectedType rather than DeclaringType for the classname.</param>
        /// <returns></returns>
        public static string FullName(this MemberInfo memberInfo,bool useReflectedType=true)
        {
            return (useReflectedType)
                ? memberInfo.ReflectedType.FullName + "." + memberInfo.Name
                : memberInfo.DeclaringType.FullName + "." + memberInfo.Name;
        }

    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
