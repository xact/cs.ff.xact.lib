﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

// ReSharper restore CheckNamespace
#endif

/// <summary>
    ///   Extension Methods for IQueryable interface.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    // ReSharper disable InconsistentNaming
    public static class IQueryableExtensions
// ReSharper restore InconsistentNaming
    {


        private const string C_EXPRESSION_NOT_PROPERTY =
            "propertyExpression must be a property accessor. e.g: 'x => x.MyProperty'";


        /// <summary>
        /// Returns the First or a default instance (non-null)
        /// of the object.
        /// <para>
        /// Note: An exception is raised if the object 
        /// does not have an argumentless constructor.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o">The o.</param>
        /// <returns></returns>
        public static T FirstOrDefaultEx<T>(this IEnumerable<T> o)
        {
            T result = o.FirstOrDefault();
            return (Object.Equals(result, default(T))) ? System.Activator.CreateInstance<T>() : result;
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// </summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "items">The items.</param>
        /// <param name = "propertyExpression">The property expression.</param>
        /// <returns></returns>
        public static IQueryable<T> WithSelectItem<T>(this IQueryable<T> items,
                                                      Expression<Func<T, string>> propertyExpression)
            where T : new()
        {
            propertyExpression.ValidateIsNotDefault("propertyExpression");
            if (propertyExpression.Body.NodeType != ExpressionType.MemberAccess)
            {
                throw new ArgumentException(C_EXPRESSION_NOT_PROPERTY);
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            MemberExpression memberExpression = propertyExpression.Body as MemberExpression;
            if (memberExpression != null)
            {
                PropertyInfo property = memberExpression.Member as PropertyInfo;

                if (property == null)
                {
                    throw new ArgumentException(C_EXPRESSION_NOT_PROPERTY);
                }

                T selectItem = new T();
                property.SetValue(selectItem, "<select>", null);

                List<T> selectItems = new List<T> {selectItem};
                return selectItems.Union(items.ToList()).AsQueryable();
            }
            return null;
        }

        //public static IQueryable<T> NotIncluding<T>(this IQueryable<T> items, int primaryKey)
        //    where T : class
        //{
        //    var itemParameter = Expression.Parameter(typeof(T), "item");

        //    var whereExpression = Expression.Lambda<Func<T, bool>>
        //        (
        //        Expression.NotEqual(
        //            Expression.Property(
        //                itemParameter,
        //                typeof(T).GetPrimaryKey().Name
        //                ),
        //            Expression.Constant(primaryKey)
        //            ),
        //        new ParameterExpression[] { itemParameter }
        //        );
        //    return items.Where(whereExpression);
        //}
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
