﻿namespace XAct {
    /// <summary>
    ///   Extensions to <see cref = "bool" />.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class BoolExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Serializes a boolean value to a string reprentation. 
        ///   <para>
        ///     Normally, the result is 'True', 'False',
        ///     but if the shorthand notation is used, 
        ///     this is '1' or '0'.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Useful for embedding Textos or sending 
        ///     JavaScript arguments for clientside scripts to 
        ///     deserialize.
        ///   </para>
        ///   <para>
        ///     Returns <c>'True'</c> or <c>'False'</c>, 
        ///     unless <c>useShortHand</c> flag is set, in which 
        ///     case <c>'0'</c> or<c>'1'</c> is returned -- 
        ///     which is easier for clientside 
        ///     JavaScript to deserialize.
        ///   </para>
        /// </remarks>
        /// <param name = "booleanValue">Boolean value.</param>
        /// <param name = "useShorthand">Boolean flag.</param>
        /// <returns>A string (True/False) or (0/1).</returns>
        public static string ToString(this bool booleanValue, bool useShorthand)
        {
            if (useShorthand)
            {
                return booleanValue ? "1" : "0";
            }
            return booleanValue.ToString();
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
