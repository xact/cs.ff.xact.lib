﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Xsl;


#if ADD_NAMESPACE_ON_EXTENSIONS 
//See: http://bit.ly/snE0xY
namespace XAct {
#endif

/// <summary>
    /// Extensions to the <see cref="XslCompiledTransform"/>
    /// </summary>
    public static class XslCompiledTransformExtensions
    {
        /// <summary>
        /// Transforms the XML stream using an 
        /// <see cref="XslCompiledTransform"/>.
        /// </summary>
        /// <remarks>
        /// An example might be something like:
        /// <para>
        /// <code>
        /// <![CDATA[
        /// string root = AppDir.Replace("\\", "/") + "/" + "data/";
        /// argList["rootDir"] = @"file://" + root;
        /// argList["cssDirPath"] = argList["rootDir"] + "css/";
        /// argList["imgDirPath"] = argList["rootDir"] + "images/";
        /// inputXmlStream.Transform(xslCompiledTranform,outputStream,argList);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name="inputXmlStream">The input XML stream.</param>
        /// <param name="xslCompiledTransform">The XSL compiled transform.</param>
        /// <param name="outputTransformedStream">The output transformed stream.</param>
        /// <param name="optionalAttributesToAdd">The optional attributes to add.</param>
        /// <returns></returns>
        public static Stream TransformXmlStream(this XslCompiledTransform xslCompiledTransform, Stream inputXmlStream, Stream outputTransformedStream, IDictionary<string, string> optionalAttributesToAdd = null)
        {


#if (CE) || (PocketPC) || (pocketPC) || (WindowsCE)
    //Declare args:

      MSXML2.DOMDocument xslDoc = info.Document;
      foreach (string entry in optionalAttributes.AllKeys) {
       MSXML2.IXMLDOMElement attributeNode = xslDoc.selectSingleNode(string.Format("//xsl:param[@name='{0}']", entry)) as MSXML2.IXMLDOMElement;

       if (attributeNode == null) {
         attributeNode = xslDoc.createElement("xsl:param");
          attributeNode.setAttribute("name", entry);
          //Insert as first child of xsl:stylesheet node:
          //Get first child that is element:
          MSXML2.IXMLDOMNode firstChild = xslDoc.documentElement.firstChild;
          while ((firstChild != null) && (!(firstChild is MSXML2.IXMLDOMElement))) {
            firstChild = firstChild.nextSibling;
          }
          if (firstChild != null) {
            xslDoc.documentElement.insertBefore(attributeNode, xslDoc.documentElement.firstChild);
          }
          else {
            xslDoc.documentElement.appendChild(attributeNode);
          }
        }
        if (attributeNode != null) {
          attributeNode.setAttribute("select", string.Format("'{0}'", optionalAttributes[entry]));
        }
      }

      MSXML2.DOMDocument xmlDoc = new MSXML2.DOMDocumentClass();
      // Load XML document.
      xmlDoc.loadXML(serializedObject);

      return xmlDoc.transformNode(xslDoc);


            /*
       * CANNOT DO THIS ON WM5:
       * 
      // Transform XML data.
      string temp = doc.transformNode(xslDoc);
      MSXML2.XSLTemplate template = new MSXML2.XSLTemplate();
      template.stylesheet = xslDoc;

      MSXML2.IXSLProcessor processor;
      processor = template.createProcessor();

      processor.addParameter("rootDir", rootDir, string.Empty);
 
      processor.input = xmlDoc;
      processor.transform();
      string result = (string) processor.output; 
      */
#else


            XsltArgumentList xsltArgumentList = new XsltArgumentList();
            xsltArgumentList.AddRange(optionalAttributesToAdd);

            #region CE stuff
            /*
    public void CEExample(){
      System
    }
    1. Open Visual Studio 2005 Command prompt, 
navigate to the ..\Include\Armv4i directory of the 
Windows Mobile SDK, and execute the following line:
midl msxml2.idl 
The result of this execution will be creation of the msxml2.tlb library.

2. Copy the msxml2.tlb into your 
project's directory and add the reference to it 
by browsing. 
The Visual Studio will recognize that this is 
a type libabry and will generate the 
MSXML2 interop assembly.
Now you can create an instance of the 
DOMDocumentClass and use its XSL transformation functionality:

MSXML2.DOMDocument doc = new MSXML2.DOMDocumentClass();
// Load XML document.
doc.load(“customer.xml"));

MSXML2.DOMDocument xsl = new MSXML2.DOMDocumentClass();
// Load XSLT document.
xsl.load(“customer.xslt"));
// Transform XML data.
string temp = doc.transformNode(xsl);
    */
            #endregion

            //see: http://www.dotnet247.com/247reference/msgs/17/88816.aspx
            //argList.AddExtensionObject("urn:Xlib-Xslt:iSightFunctions", new LuckyMeClass());

            using (XmlReader xmlReader = XmlReader.Create(inputXmlStream))
            {
                xslCompiledTransform.Transform(xmlReader, xsltArgumentList, outputTransformedStream);
            }
#endif
            return outputTransformedStream;
        }




    }

#if ADD_NAMESPACE_ON_EXTENSIONS 
//See: http://bit.ly/snE0xY
}
#endif
