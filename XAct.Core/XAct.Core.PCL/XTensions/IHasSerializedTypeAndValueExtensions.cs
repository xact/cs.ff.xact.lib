﻿
// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// 
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IHasSerializedTypeValueAndMethodExtensions
// ReSharper restore InconsistentNaming
    {

        /// <summary>
        /// Serializes the given value into the entity's properties
        /// ready for storage.
        /// <para>
        /// Use this method to back a Value property on an entity
        /// that implements <see cref="IHasSerializedTypeValueAndMethod" />
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity">The entity.</param>
        /// <param name="value">The value.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="serializationMethod">The serialization method.</param>
        /// <returns>The serialization method actually used.</returns>
        public static SerializationMethod SerializeValue<T>(this IHasSerializedTypeValueAndMethod entity, T value, SerializationMethod serializationMethod = SerializationMethod.Undefined, Type valueType=null)
        {

            //If not defined,use the one already used last time.
            if (serializationMethod == SerializationMethod.Undefined)
            {
                serializationMethod = entity.SerializationMethod;
            }

            //If it was never set,use one that will work with most objects.
            if (serializationMethod == SerializationMethod.Undefined)
            {
                serializationMethod = SerializationMethod.String;
            }

            if (valueType == null)
            {
                valueType = typeof (T);
            }

            //Try it.
            entity.SerializedValue = valueType.Serialize(value, ref serializationMethod);

            entity.SerializedValueType = valueType.FullName;

            //whether first time, and needs saving, or
            //in case it shifted (due to trying (eg XMl first and falling back to base64))
            //save it again:
            entity.SerializationMethod = serializationMethod;

            return serializationMethod;
        }

        /// <summary>
        /// Gets the entity's value, by deserializing it's
        /// <see cref="IHasSerializedTypeValueAndMethod"/>
        /// properties.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public static object DeserializeValue(this IHasSerializedTypeValueAndMethod entity)
        {
            Type type;
            object result;
            if (entity.SerializedValueType.IsNullOrEmpty())
            {
                result = null;
            }
            else
            {
                //has been serialized at some point in the past, ok to use type
                //to get it back:
                type = Type.GetType(entity.SerializedValueType);
                result = type.DeSerialize(entity.SerializedValue, entity.SerializationMethod);
            }

            return result;
        }

        /// <summary>
        /// Gets the deserialized value from the entities serialization properties.
        /// <para>
        /// Use this method to back a Value property on an entity
        /// that implements <see cref="IHasSerializedTypeValueAndMethod"/>
        /// </para>
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public static T DeserializeValue<T>(this IHasSerializedTypeValueAndMethod entity)
        {
            //But type it was saved in might not be 100% same as Type requested,
            //so convert it:
            return entity.DeserializeValue().ConvertTo<T>();

        }
    }
}
