﻿
namespace XAct
{
    using System.Threading.Tasks;

    public static class TaskExtensions
    {
        public static TResult WaitAndGetResult<TResult>(this Task<TResult> task,int timeOutMilliseconds=2000)
        {
            task.Wait(timeOutMilliseconds);

            return task.Result;
        }
    }
}
