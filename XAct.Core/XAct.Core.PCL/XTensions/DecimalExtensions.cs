﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;

// ReSharper restore CheckNamespace
#endif



/// <summary>
    ///   Extensions for ints, etc.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    [SuppressMessage("Microsoft.Naming", "1704", Justification = "Maths is spelled correctly.")]
    public static class MathsExtensions
    {
        private static readonly CultureInfo _invariantCulture = CultureInfo.CurrentCulture;

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the specified value is 
        ///   between the 
        ///   <paramref name = "lowerBound" />
        ///   and <paramref name = "upperBound" /> values (inclusive).
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Usage Example:
        ///     <code>
        ///       <![CDATA[
        /// Console.WriteLine(((double)1).IsBetween(1,3));//true
        /// Console.WriteLine(((double)3).IsBetween(-1,3));//true
        /// Console.WriteLine(((double)1).IsBetween(-1,-3));//false
        /// Console.WriteLine(((double)-3).IsBetween(-1,-3));//true
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "thisValue">The current decimal value.</param>
        /// <param name = "lowerBound">The lower bound decimal value.</param>
        /// <param name = "upperBound">The upper bound decimal value.</param>
        /// <returns>
        ///   <c>true</c> if the current value is between both bounds; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsBetween(this decimal thisValue, decimal lowerBound, decimal upperBound)
        {
            decimal higherValue = Math.Max(lowerBound, upperBound);
            decimal lowerValue = Math.Min(lowerBound, upperBound);


            return (lowerValue <= thisValue) && (thisValue <= higherValue);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts Currency To String based on Culture of Application.
        /// </summary>
        /// <param name = "number">Double containing number to convert to Currency.</param>
        /// <param name = "cultureInfo">CultureInfo object</param>
        /// <param name = "includeCurrencySymbol">Prepend/append Currency Symbol according to CultureInfo ($ for US, FF for France, etc.)</param>
        /// <returns></returns>
        public static string ToString(decimal number, IFormatProvider cultureInfo, bool includeCurrencySymbol)
        {
            if (includeCurrencySymbol == false)
            {
                //see:
                //ms-help://MS.MSDNVS.1036/cpguide/html/cpconstandardnumericformatstrings.htm
                return number.ToString("F", _invariantCulture); //Can be N:
            }


            cultureInfo.ValidateIsNotDefault("cultureInfo");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //System.Globalization.CultureInfo cultureInfo = Application.CurrentCulture;
            return (number.ToString("c", cultureInfo));
        }
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
