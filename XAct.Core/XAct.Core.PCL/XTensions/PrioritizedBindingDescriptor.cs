﻿namespace XAct
{
    using XAct.Services;

    public class PrioritizedBindingDescriptor
    {
        public Priority Priority { get; private set; }
        public IBindingDescriptor BindingDescriptor { get; private set; }

        public PrioritizedBindingDescriptor(Priority priority, IBindingDescriptor bindingDescriptor)
        {
            Priority = priority;
            BindingDescriptor = bindingDescriptor;
        }
    }
}