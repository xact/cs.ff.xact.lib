// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Library.Settings;
    using XAct.Services;

    /// <summary>
    ///   An XAct.Services.IoC class.
    /// </summary>
    public static class ServiceLocatorServiceExtensions
    {

        /// <summary>
        /// Registers the given service via reflection in the current DependencyInjectionContainer.
        /// <para>
        /// Currently supported are Unity, Ninject, SimpleLocator.
        /// </para>
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface.</typeparam>
        /// <typeparam name="TInstance">The type of the instance.</typeparam>
        /// <param name="dependencyResolver">The dependency resolver.</param>
        /// <param name="serviceLifetimeType">Type of the service lifetime.</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static void RegisterServiceBindingInIoC<TInterface, TInstance>(
            this IDependencyResolver dependencyResolver, 
            BindingLifetimeType serviceLifetimeType = BindingLifetimeType.Undefined
            )
            where TInstance : TInterface
        {
            if (serviceLifetimeType == BindingLifetimeType.Undefined)
            {
// ReSharper disable RedundantNameQualifier
                serviceLifetimeType = Bindings.DefaultServiceLifespan;
// ReSharper restore RedundantNameQualifier
            }

            dependencyResolver.RegisterServiceBindingInIoC(
                new BindingDescriptor(
                    BindingType.Custom, 
                    typeof(TInterface), 
                    typeof(TInstance), 
                    serviceLifetimeType));
        }

        /// <summary>
        /// Registers the given service in the current
        /// DependencyInjectionContainer.
        /// <para>
        /// Currently supported are Unity, Ninject, SimpleLocator.
        /// </para>
        /// </summary>
        /// <param name="dependencyResolver">The dependency resolver.</param>
        /// <param name="serviceRegistrationDescriptor">The service registration descriptor.</param>
        public static void RegisterServiceBindingInIoC(this IDependencyResolver dependencyResolver, 
            IBindingDescriptor serviceRegistrationDescriptor)
        {
            XAct.Library.Settings.IoC.RegisterBindingMethod(serviceRegistrationDescriptor);
        }




        /// <summary>
        /// Determines whether the given service
        /// is registered or not in the current
        /// DependencyInjectionContainer.
        /// </summary>
        /// <param name="dependencyResolver">The dependency resolver.</param>
        /// <param name="serviceInterfaceType">Type of the service interface.</param>
        /// <returns>
        ///   <c>true</c> if [is service registered] [the specified dependencyInjectionContainer]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsServiceRegistered(this IDependencyResolver dependencyResolver, Type serviceInterfaceType)
        {

            return XAct.Library.Settings.IoC.IsBindingRegisteredMethod(serviceInterfaceType,null);
        }
    }
}