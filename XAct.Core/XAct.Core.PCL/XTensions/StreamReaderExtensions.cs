﻿//OK

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Collections.Generic;
    using System.IO;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension Methods for the StreamReader.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class StreamReaderExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Read List from File
        /// </summary>
        /// <param name = "streamReader"></param>
        /// <returns></returns>
        public static IEnumerable<string> ReadLines(this StreamReader streamReader)
        {
            streamReader.ValidateIsNotDefault("streamReader");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            IList<string> results = new List<string>();

            while (true)
            {
                string line = streamReader.ReadLine();
                if (line == null)
                {
                    break;
                }
                results.Add(line);
            }

            return results;
        }

    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
