﻿namespace XAct
{
    using XAct.Commands;

    /// <summary>
    /// Extensions to objects that implement 
    /// the <see cref="ICommandMessage"/> contract.
    /// </summary>
    public static class ICommandMessageExtensions
    {

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Invokes a <see cref="ICommandService"/> to 
        /// executes the specified <see cref="ICommandMessage"/>
        /// (via the correct <see cref="ICommandMessageHandler{TCommandMessage, TCommandResponse}"/>)
        /// <para>
        /// Code is equivalent to:
        /// <code>
        /// <![CDATA[
        /// ICommandMessageDispatcher commandMessageDispatcher =
        ///     DependencyResolver.Current.GetInstance<ICommandMessageDispatcher>();
        /// return commandMessageDispatcher.Execute(commandMessage);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="commandMessage">The command message.</param>
        /// <returns></returns>
        public static void Execute(this ICommandMessage commandMessage)
        {
            ICommandService commandMessageDispatcher =
                DependencyResolver.Current.GetInstance<ICommandService>();

            commandMessageDispatcher.Execute(true, commandMessage);
        }
    }
}
