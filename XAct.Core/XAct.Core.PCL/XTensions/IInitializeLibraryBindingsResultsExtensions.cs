﻿namespace XAct
{
    using System;
    using System.IO;
    using System.Text;
    using XAct.Environment;
    using XAct;
    using XAct.IO;
    using XAct.Services;
    using XAct.Services.IoC.Initialization;

    /// <summary>
    /// Extensions to the <see cref="IInitializeLibraryBindingsResults"/>
    /// </summary>
    public static class IInitializeLibraryBindingsResultsExtensions
    {
        /// <summary>
        /// Generates a more verbose summary of what happened during the Initialization stage.
        /// </summary>
        /// <param name="initializeLibraryBindingsResults">The initialize library bindings results.</param>
        /// <returns></returns>
        public static string ToStringEx(this IInitializeLibraryBindingsResults initializeLibraryBindingsResults)
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            StringBuilder result = new StringBuilder();

            result.Append("TimeElapsed: {0}ms{1}".FormatStringCurrentUICulture(initializeLibraryBindingsResults.TotalTimeElapsed.TotalMilliseconds, environmentService.NewLine));
            result.Append("Had IO Permission: {0}{1}".FormatStringCurrentUICulture(initializeLibraryBindingsResults.BindingScanResults.HadPermissionsToScanDir, environmentService.NewLine));
            result.Append(environmentService.NewLine);

            result.Append("Assemblies Scanned:{0}".FormatStringCurrentUICulture(environmentService.NewLine));
            foreach (string assemblyName in initializeLibraryBindingsResults.BindingScanResults.AssemblyNames)
            {
                result.Append("* {0}{1}".FormatStringCurrentUICulture(assemblyName, environmentService.NewLine));
            }
            result.Append(environmentService.NewLine);

            result.Append("Services Scanned:{0}".FormatStringCurrentUICulture(environmentService.NewLine));
            foreach (IBindingDescriptor bindingDescriptor in initializeLibraryBindingsResults.ServiceBindingsSkipped)
            {
                result.Append("* {0}{1}".FormatStringCurrentUICulture(bindingDescriptor.ToString(), environmentService.NewLine));
            }
            result.Append(environmentService.NewLine);

            result.Append("Services Registered:{0}".FormatStringCurrentUICulture(environmentService.NewLine));
            foreach (IBindingDescriptor bindingDescriptor in initializeLibraryBindingsResults.ServiceBindingsRegistered)
            {
                result.Append("* {0}{1}".FormatStringCurrentUICulture(bindingDescriptor.ToString(), environmentService.NewLine));
            }
            result.Append(environmentService.NewLine);

            


            return result.ToString();
        }

        static object _lock = new object();

        public static void DumpToFile(this IInitializeLibraryBindingsResults initializeLibraryBindingsResults, string fullFilePath =null)
        {
            try
            {
                string[] report = DependencyResolver.BindingResults.CreateBindingReport();
                
                var ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();

                if (fullFilePath.IsNullOrEmpty())
                {
                    var pathService = XAct.DependencyResolver.Current.GetInstance<IPathService>();

                    string baseDirectory =
                        XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().ApplicationBasePath;

                    baseDirectory = ExtractBaseDirectory(baseDirectory);

                    fullFilePath = pathService.Combine(
                        baseDirectory,
                        "XActLib_Initialization_Binding_Report.txt");
                }

                lock (_lock)
                {
                    using (var stream = ioService.FileOpenWriteAsync(fullFilePath, true, true).WaitAndGetResult())
                    {
                        string output =
                            report.JoinSafely(XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().NewLine);

                        using (TextWriter t = new StreamWriter(stream))
                        {
                            t.Write(output);
                            //foreach (string line in report)
                            //{
                            //    t.WriteLine(line);
                            //}
                        }
                    }
                }
            }
// ReSharper disable RedundantCatchClause
#pragma warning disable 168
            catch (System.Exception e)
#pragma warning restore 168
            {
                throw;
            }
// ReSharper restore RedundantCatchClause
        }

        private static string ExtractBaseDirectory(string baseDirectory)
        {
            while (baseDirectory.Contains("\\bin", StringComparison.OrdinalIgnoreCase))
            {
                var index = baseDirectory.LastIndexOf("\\", StringComparison.OrdinalIgnoreCase);
                if (index < 0)
                {
                    break;
                }
                baseDirectory = baseDirectory.Substring(0, index);
            }
            return baseDirectory;
        }

        //private static void WriteReportToHardDrive()
        //{
        //    string[] bindingReport = XAct.DependencyResolver.BindingResults.CreateBindingReport();

        //    ;
        //    string fullName =
        //        System.IO.Path.Combine(XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().ApplicationBasePath,
        //                               "..\\..\\bindingReport.txt");

        //    System.IO.File.WriteAllLines(fullName, bindingReport);
        //}

    }
}
