﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Linq;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// Extension methods to the <see cref="Uri"/> object.
    /// </summary>
    public static class UriExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// This extension method will return all the parameters of an Uri in a Dictionary{string, string}. 
        /// <para>
        /// In case the uri doesn't contain any parameters a empty dictionary will be returned.
        /// </para>
        /// </summary>
        /// <param name="self">The self.</param>
        /// <returns></returns>
        public static Dictionary<string, string> Parameters(this Uri self)
        {
            return String.IsNullOrEmpty(self.Query)
                       ? new Dictionary<string, string>()
                       : self.Query.Substring(1).Split('&').ToDictionary(
                           p => p.Split('=')[0],
                           p => p.Split('=')[1]
                             );
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
