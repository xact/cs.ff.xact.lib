﻿namespace XAct
{
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// Extensions to the <see cref="CultureInfo"/> object.
    /// </summary>
    public static class CultureInfoExtensions
    {
        /// <summary>
        /// Takes the current culture code, and returns ('fr-FR','fr'),
        /// or if the culture is the default code, returns ('en-EN','en','')
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <param name="addInvariant">if set to <c>true</c> [add invariant].</param>
        /// <param name="lowered">if set to <c>true</c> [lowered].</param>
        /// <returns></returns>
        public static string[] BuildCultureCodeList(this CultureInfo culture, bool addInvariant, bool lowered=true)
        {
            List<string> codes = new List<string>();
            string cultureCodeFull = culture.Name;

            string s = lowered? cultureCodeFull.ToLower() : cultureCodeFull;

            //Add ('fr-FR')
            codes.Add(s);

            if (s.IsNullOrEmpty())
            {
                return codes.ToArray();
            }

            if (cultureCodeFull.Length > 0)
            {
                //Is not Invariant
                //so it's something...something we can split I hope (eg: 'en-EN').
                string[] parts = cultureCodeFull.Split('-');
                //It was splittable...
                if (parts.Length > 1)
                {
                    //...so add the short version  ('fr')
                    s = (lowered) ? parts[0].ToLower() : parts[0];
                    codes.Add(s);

                    if (addInvariant)
                    {
                        //And if this is the default language, we can 
                        //finish by adding invariant:
                        //''
                        codes.Add(string.Empty);
                    }
                }
            }
            //Should have something like ('fr-FR', 'fr') or ('en-EN', 'en', '')
            return codes.ToArray();
        }
    }
}
