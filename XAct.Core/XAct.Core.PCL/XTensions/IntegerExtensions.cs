﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Globalization;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension Methods for Integers.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class IntegerExtensions
    {

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the number is larger than the specified number.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// int check = 4;
        /// //Will be true:
        /// Debug.Assert(check.IsLargerThan(3));
        /// //Will be false:
        /// Debug.Assert(check.IsLargerThan(4));
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "minimumNumber">The minimum nmber.</param>
        /// <returns>
        ///   <c>true</c> if [is larger than] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsLargerThan(this int number, int minimumNumber)
        {
            return (number > minimumNumber);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the number is larger than or equal to the specified number.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// int check = 4;
        /// //Will be true:
        /// Debug.Assert(check.IsLargerThanOrEqualTo(3));
        /// //Will be true:
        /// Debug.Assert(check.IsLargerThanOrEqualTo(4));
        /// //Will be false:
        /// Debug.Assert(check.IsLargerThanOrEqualTo(5));
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "minimumNumber">The minimum nmber.</param>
        /// <returns>
        ///   <c>true</c> if [is larger than or equal to] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsLargerThanOrEqualTo(this int number, int minimumNumber)
        {
            return (number >= minimumNumber);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Validates that the number is larger than or equal to the specified number.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// int check = 4;
        /// //No Error raised
        /// Debug.Assert(check.ValidateIsLargerThanOrEqualTo(3));
        /// //Raises an exception:
        /// Debug.Assert(check.ValidateIsLargerThanOrEqualTo(4));
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "minimumNumber">The minimum number.</param>
        [Obsolete]
        public static void ValidateIsLargerThan(this int number, int minimumNumber)
        {
            if (!number.IsLargerThan(minimumNumber))
            {
                throw new ArgumentException(
                    "::Number is too small. Needs to be larger than {0}".FormatStringCurrentUICulture(minimumNumber));
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Validates that the number the is larger than or equal to the specified number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "minimumNumber">The minimum number.</param>
        [Obsolete]
        public static void ValidateIsLargerThanOrEqualTo(this int number, int minimumNumber)
        {
            if (!number.IsLargerThanOrEqualTo(minimumNumber))
            {
                throw new ArgumentException(
                    "::Number is too small. Needs to be larger than {0}".FormatStringCurrentUICulture(minimumNumber));
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the number is smaller than the specified number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "maximumNumber">The maximum nmber.</param>
        /// <returns>
        ///   <c>true</c> if [is smaller than] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSmallerThan(this int number, int maximumNumber)
        {
            return (number > maximumNumber);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the number is smaller than or equal to the specified reference number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "maximumNumber">The maximum nmber.</param>
        /// <returns>
        ///   <c>true</c> if [is smaller than or equal to] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSmallerThanOrEqualTo(this int number, int maximumNumber)
        {
            return (number >= maximumNumber);
        }

        /// <summary>
        ///   Validates that the number argument the is smaller than the specified reference number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "maximumNumber">The maximum number.</param>
        public static void ValidateIsSmallerThan(this int number, int maximumNumber)
        {
            if (!number.IsSmallerThan(maximumNumber))
            {
                throw new ArgumentException(
                    "::Number is too small. Needs to be smaller than {0}".FormatStringCurrentUICulture(maximumNumber));
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Validates that the number argument the is smaller than or equal to the specified reference number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "referenceNumber">The maximum number.</param>
        public static void ValidateIsSmallerThanOrEqualTo(this int number, int referenceNumber)
        {
            if (!number.IsSmallerThanOrEqualTo(referenceNumber))
            {
                throw new ArgumentException(
                    "::Number is too small. Needs to be smaller than {0}".FormatStringCurrentUICulture(referenceNumber));
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts an integer to a Hex string format.
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        public static string ToHexString(this int number)
        {
            try
            {
                return number.ToString("X", CultureInfo.InvariantCulture);
            }
            catch (Exception exception)
            {
                throw new ArgumentException(
                    "Could not Convert Int to Hex notation: {0}".FormatStringCurrentUICulture(number),
                    exception);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convert Integer to Hex Str
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        public static string ToHexString2(int number)
        {
            try
            {
                return "{0:X}".FormatStringInvariantCulture(number);
            }
            catch (Exception exception)
            {
                throw new ArgumentException("Could not convert Int to Hex: " + number, exception);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Pads Integer with "0"'s 
        /// <remarks>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// var result = "3".Pad(3);
        /// //result will be "003";
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// </summary>
        /// <param name = "number"></param>
        /// <param name = "length">The total number of characters needed.</param>
        /// <returns></returns>
        public static string Pad(this int number, int length)
        {
            string tResult = "{0}".FormatStringInvariantCulture(number);
            while (tResult.Length < length)
            {
                tResult = "0" + tResult;
            }
            return tResult;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns the given number, ensuring that it within the specified range.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "min">The min.</param>
        /// <param name = "max">The max.</param>
        /// <returns></returns>
        public static int MinMax(this int number, int min, int max)
        {
            return Math.Max(Math.Min(number, max), min);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Checks that the number fits 
        /// <para>
        /// ie: <c>(number &amp; checkNumber) == checkNumber</c>
        /// </para>
        /// <para>
        /// If you want to set it, try: <c>number | flags</c>
        /// </para>
        /// <para>
        /// If you want to clear a flag, try: <c>number &amp; (~flags)</c>
        /// </para>
        /// </summary>
        /// <param name="number"></param>
        /// <param name="checkNumber"></param>
        /// <returns></returns>
        public static bool BitIsSet(this int number, int checkNumber)
        {
            return (number & checkNumber) == checkNumber;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Bits the is not set.
        /// <para>
        /// ie: <c>(number &amp; (~flags)) == 0</c>
        /// </para>
        /// <para>
        /// If you want to set it, try: <c>number | flags</c>
        /// </para>
        /// <para>
        /// If you want to clear a flag, try: <c>number &amp; (~flags)</c>
        /// </para>
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="flags">The flags.</param>
        /// <returns></returns>
        /// <internal><para>6/11/2011: Sky</para></internal>
        public static bool BitIsNotSet(this int number, int flags)
        {
            return (number & (~flags)) == 0;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Sets the given Flags.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="flags">The flags.</param>
        /// <returns></returns>
        /// <internal><para>6/11/2011: Sky</para></internal>
        public static int BitSet(this int number, int flags)
        {
            return number | flags;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Clears/Removes the Bit.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="flags">The flags.</param>
        /// <returns></returns>
        /// <internal><para>6/11/2011: Sky</para></internal>
        public static int BitClear(this int number, int flags)
        {
            return number & (~flags);
        }

        /// <summary>
        /// Converts an integer to a Guid unique identifier
        /// (obviously not very random).
        /// <para>
        /// Useful for initial seeding scenarios.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public static Guid ToGuid(this int value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }
    
    }

    public static class LongExtensions
    {
        static string[] sizes = { "B", "KB", "MB", "GB" };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public static string ToHumanReadableFileSizeString(this long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
            {
                return "0" + suf[0];
            }
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];


            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            //string result = String.Format(order == 0?"{0:0}{1}":"{0:0.#}{1}", fileSize, sizes[order]);

            //return result;
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
