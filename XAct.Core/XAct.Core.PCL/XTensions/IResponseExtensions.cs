﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using XAct.Messages;
    using XAct.Resources;

    /// <summary>
    /// Extension methods for implementations of
    /// the <see cref="IResponse"/>, such as
    /// <see cref="Response"/>, <see cref="Response{T}"/>,
    /// and <see cref="PagedResponse{T}"/>
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IResponseExtensions
// ReSharper restore InconsistentNaming
    {

        private static ICultureSpecificResponseService CultureSpecificResponseService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<ICultureSpecificResponseService>(); }
        }

        /// <summary>
        /// Method that wraps <see cref="IResponse.Success"/> property,
        /// allowing for more clarity in fluent constructs.
        /// </summary>
        /// <param name="response">The response.</param>
        public static bool IsError(this IResponse response)
        {
            return !response.Success;
        }

        /// <summary>
        /// Method that wraps <see cref="IResponse.Success"/> property,
        /// allowing for more clarity in fluent constructs.
        /// </summary>
        /// <param name="response">The response.</param>
        public static bool IsSuccess(this IResponse response)
        {
            return response.Success;
        }

        /// <summary>
        /// Transfers the values from request.
        /// </summary>
        /// <param name="pagedResponse">The paged response.</param>
        /// <param name="pagedRequest">The paged request.</param>
        public static void TransferValuesFromRequest(this IPageable pagedResponse, IPagedQuerySpecification pagedRequest)
        {
            pagedResponse.PageIndex = pagedRequest.PageIndex;
            pagedResponse.PageSize = pagedRequest.PageSize;
        }


        /// <summary>
        /// Adds a new Message, referencing the given MessageCode (Id and Severity).
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="messageCode">The message code.</param>
        /// <param name="arguments">The arguments.</param>
        public static bool AddMessage(this IResponse response, MessageCode messageCode, params string[] arguments)
        {
            //Notice how we do not set the Text Property.

            //That is set later using ICultureSpecificMessageService.

            Message message = new Message(messageCode, arguments);

            response.Messages.Add(message);

            return response.Success;
        }

        ///// <summary>
        ///// Adds the specified message to the
        ///// Response object's Message array.
        ///// </summary>
        ///// <param name="response">The response.</param>
        ///// <param name="severity">The severity.</param>
        ///// <param name="messageCode">The message code.</param>
        ///// <param name="arguments">The arguments.</param>
        //public static bool AddMessage(this IResponse response,  Severity severity, ulong messageCode, params string[] arguments)
        //{
        //    Message message = new Message(severity, messageCode, arguments);

        //    //Notice how we do not set the Text Property.

        //    //That is set later using ICultureSpecificMessageService.

        //    response.Messages.Add(message);
            // return response.Success;
        //}


        /// <summary>
        /// Transfers the messages, metadata, and TimeElapsed from one <see cref="IResponse"/>
        /// to another <see cref="IResponse"/>
        /// </summary>
        /// <param name="sourceResponse">The source response.</param>
        /// <param name="targetResponse">The target response.</param>
        public static void TransferProperties(this IResponse sourceResponse, IResponse targetResponse)
        {

            if (sourceResponse.Messages != null)
            {
                foreach (Message message in sourceResponse.Messages)
                {
                    targetResponse.Messages.Add(message);
                }
            }

            if (sourceResponse.Metadata!=null)
            {
                foreach (Metadata metadata in sourceResponse.Metadata)
                {
                    targetResponse.Metadata.Add(metadata);
                }
            }

            targetResponse.TimeElapsed = sourceResponse.TimeElapsed;


        
        }

        /// <summary>
        /// Transfers the messages.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceResponse">The source response.</param>
        /// <param name="targetResponse">The target response.</param>
        public static void TransferProperties<T>(this IResponse<T> sourceResponse, IResponse<T> targetResponse)
        {

            ((IResponse)sourceResponse).TransferProperties(targetResponse);

            targetResponse.Data = sourceResponse.Data;

        }



        /// <summary>
        /// Clones the basic Response object, and sets the given data property as the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceResoonse"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Response<T> Clone<T>(this IResponse sourceResource, T data)
        {
            var result = new Response<T>();

            //Copy messages and metadata only:
            sourceResource.TransferProperties(result);

            result.Data = data;
            return result;
        }

        public static Response Clone<T>(this IResponse sourceResource)
        {
            var result = new Response();

            //Copy messages and metadata only:
            sourceResource.TransferProperties(result);

            return result;
        }




        /// <summary>
        /// Retrieves the Resources referenced in the MessageCode
        /// and builds Resource Text which it nests in the Response.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="cultureSpecificResponseService">The culture specific response service.</param>
        public static void SetPresentationAttributes(this IResponse response, CultureInfo cultureInfo=null, ICultureSpecificResponseService cultureSpecificResponseService=null)
        {
            if (cultureSpecificResponseService == null)
            {
                cultureSpecificResponseService = CultureSpecificResponseService;
            }

            cultureSpecificResponseService.SetPresentationAttributes(response, cultureInfo);
        }

        ///// <summary>
        ///// Transfers the properties.
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="sourceResponse">The source response.</param>
        ///// <param name="targetResponse">The target response.</param>
        //public static void TransferProperties<T>(this PagedResponse<T> sourceResponse, PagedResponse<T> targetResponse)
        //{
        //    ((IResponse)sourceResponse).TransferProperties(targetResponse);


        //    targetResponse.Data = sourceResponse.Data;

        //    //targetResponse.HasNextPage = sourceResponse.HasNextPage;
        //    //targetResponse.HasPreviousPage = sourceResponse.HasPreviousPage;
        //    targetResponse.PageIndex = sourceResponse.PageIndex;
        //    targetResponse.PageSize = sourceResponse.PageSize;
        //    targetResponse.TotalCount = sourceResponse.TotalCount;
        //    targetResponse.TotalCountInDataStore = sourceResponse.TotalCountInDataStore;
        //    //targetResponse.TotalPages = sourceResponse.TotalPages;

        //}



        /// <summary>
        /// Determines whether the Response contains a Message with a particular <see cref="MessageCode" />
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="checkForMessageCode">The check for message code.</param>
        /// <param name="recursive">if set to <c>true</c> [recursive].</param>
        /// <returns></returns>
        public static bool ContainsMessageCode(this IResponse response, MessageCode checkForMessageCode, bool recursive = true)
        {
            // Check root level
            return response.Messages.ContainsMessageCode(checkForMessageCode, recursive);
        }

        /// <summary>
        /// Checks a collection of message codes if it contains any of a given set of message codes
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="messageCodes">The message codes.</param>
        /// <param name="checkForMessageCodes">The check for message codes.</param>
        /// <param name="recursive">if set to <c>true</c> [recursive].</param>
        /// <returns></returns>
        public static Message ContainsMessageCode(this IResponse response, IEnumerable<Message> messageCodes, IEnumerable<MessageCode> checkForMessageCodes, bool recursive = true)
        {
            return response.Messages.ContainsMessageCode(checkForMessageCodes, recursive);
        }



        
    }
}
