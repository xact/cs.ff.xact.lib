﻿namespace XAct
{
    using System;

    /// <summary>
    /// Extension methods to arrays
    /// </summary>
    public static class ArrayExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the array is null or empty.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <returns>
        /// 	<c>true</c> if [is null or empty] [the specified array]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty(this Array array)
        {
            return array == null || array.Length == 0;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the array has too many elements.
        /// <para>
        /// Example Usage:
        /// <code>
        /// <![CDATA[
        /// //Result is true, as array has more than 2 elements:
        /// Debug.Assert(string[]{"A","B","C"},2));
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="argMax">The arg max.</param>
        /// <returns>
        /// 	<c>true</c> if [is too many] [the specified array]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsTooMany(this Array array, int argMax)
        {
            return (array.Length > argMax);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the array has too few elements.
        /// <para>
        /// Example Usage:
        /// <code>
        /// <![CDATA[
        /// //Result is true, as array has less than 4 elements:
        /// Debug.Assert(string[]{"A","B","C"},4));
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="argMin">The arg min.</param>
        /// <returns>
        /// 	<c>true</c> if [is too few] [the specified array]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsTooFew(this Array array, int argMin)
        {
            return (array.Length < argMin);
        }


        /// <summary>
        /// Randomly shuffles the items in the array
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="rnd">The random.</param>
        /// <internal>
        /// See: http://stackoverflow.com/a/110570/4140558
        ///   </internal>
        public static void Shuffle<T>(this T[] array, Random rnd = null)
        {
            if (rnd == null)
            {
                rnd = new Random();
            }
            int n = array.Length;
            while (n > 1)
            {
                int k = rnd.Next(n--);
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }

    }

}