namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct.Diagnostics.Performance.Services.Entities;
    using XAct.IO;


    public static class IPerformanceCounterServiceConfigurationExtensions
    {
        public static void ScanForConfigurationFiles(
            this IPerformanceCounterServiceConfiguration performanceCounterServiceConfiguration,
            string currentCurrentDirectory,
            out List<string> messages)
        {
            performanceCounterServiceConfiguration.ScanForConfigurationFiles(
                currentCurrentDirectory,
                (x) =>
                XAct.DependencyResolver.Current.GetInstance<IIOService>()
                    .GetDirectoryFileNamesAsync(x, "*PerformanceCounters.xml", HierarchicalOperationOption.TopOnly)
                    .WaitAndGetResult()
                    .ToList(),
                (x) =>
                XDocument.Load(
                    XAct.DependencyResolver.Current.GetInstance<IIOService>().FileOpenReadAsync(x).WaitAndGetResult()),
                out messages
                );
        }

        public static void ScanForConfigurationFiles(
            this IPerformanceCounterServiceConfiguration performanceCounterServiceConfiguration,
            string currentCurrentDirectory,
            Func<string, List<string>> getXDocumentsInDirectory,
            Func<string, XDocument> getXDocument,
            out List<string> messages)
        {
            messages = new List<string>();

            if (currentCurrentDirectory == null)
            {
                messages.Add("Setting currentDirectory to string.Empty.");

                currentCurrentDirectory = string.Empty;
            }

            currentCurrentDirectory = currentCurrentDirectory.TrimEnd('\\');

            string[] paths = new string[]
                {
                    currentCurrentDirectory,
                    currentCurrentDirectory + "\\Setup"

                };

            List<XDocument> xdocuments = new List<XDocument>();
            foreach (string path in paths)
            {
                try
                {
                    var fileNames = getXDocumentsInDirectory(path);
                    messages.Add("Found {0} documents in scanning dir: {1}".FormatStringInvariantCulture(
                        fileNames.Count, path));

                    foreach (string fileName in fileNames)
                    {
                        try
                        {
                            XDocument xdoc = getXDocument(fileName);
                            xdocuments.Add(xdoc);
                            messages.Add("Sucessfully loaded XDocument: {0}".FormatStringInvariantCulture(fileName));
                        }
                        catch (System.Exception e)
                        {
                            messages.Add(
                                "ERROR: Failed parsing XDocument: {0}. Message: {1}".FormatStringInvariantCulture(
                                    fileName, e.Message));
                            messages.Add(e.Message);
                        }
                    }
                }
#pragma warning disable 168
                catch (System.Exception e)
#pragma warning restore 168
                {
                    messages.Add("ERROR: failed scanning dir: {0}".FormatStringInvariantCulture(path));
                    continue;
                }
            }

            List<string> messsages2;
            performanceCounterServiceConfiguration.ScanForConfigurationFiles(xdocuments.ToArray(), out messsages2);
            messages.AddRange(messsages2);
        }


        public static void ScanForConfigurationFiles(
            this IPerformanceCounterServiceConfiguration performanceCounterServiceConfiguration, XDocument[] xDocuments,
            out List<string> messages)
        {

            messages = new List<string>();

            Dictionary<string, PerformanceCounterCategoryCreationInformation> performanceCounterCategoryCreationInformationCache =
                new Dictionary<string, PerformanceCounterCategoryCreationInformation>();


            foreach (var doc in xDocuments)
            {
                //Log("Done.");

                messages.Add("Parsing doc: Begin...");

                var categoryNodes = doc.Root.Elements("category");

                if (!categoryNodes.Any())
                {
                    categoryNodes = doc.Root.Elements("Category");
                }

                if (!categoryNodes.Any())
                {
                    messages.Add("Parsing: no 'categories' node found.");
                    continue;
                }

                //PerformanceCounterCategoryCreationInformation result = null;


                ParseCategoryNodes(categoryNodes, ref performanceCounterCategoryCreationInformationCache, ref messages);

                performanceCounterServiceConfiguration.CounterCategoryCreationInformation = 
                    performanceCounterCategoryCreationInformationCache.Values.ToArray();

                messages.Add("Parsing: done.");
            }
        }

        private static void ParseCategoryNodes(IEnumerable<XElement> categoryNodes, ref Dictionary<string, PerformanceCounterCategoryCreationInformation> performanceCounterCategoryCreationInformationCache, ref List<string> messages)
        {
            messages.Add("Parsing: have {0} Categories".FormatStringInvariantCulture(categoryNodes.Count()));


            foreach (var categoryNode in categoryNodes)
            {
                ParseCategoryNode(categoryNode, ref performanceCounterCategoryCreationInformationCache, ref messages);


            } //~category
        }

        private static void ParseCategoryNode(XElement categoryNode, ref Dictionary<string, PerformanceCounterCategoryCreationInformation> performanceCounterCategoryCreationInformationCache, ref List<string> messages)
        {
//Category.Name:
            string categoryName = (string) (categoryNode.Attribute("name") ?? categoryNode.Attribute("Name")) ??
                                  string.Empty;

            //Category.Description:
            string categoryDescription =
                (string) (categoryNode.Attribute("description") ?? categoryNode.Attribute("Description")) ??
                string.Empty;



            //Case insensitive searching for Counter elements:
            var counterNodes = categoryNode.Descendants("counter").ToArray();
            if (!counterNodes.Any())
            {
                counterNodes = categoryNode.Descendants("Counter").ToArray();
            }


            messages.Add(
                "Parsing Counter Category [name:{0}, description:{1}, counters:{2}]"
                    .FormatStringInvariantCulture(categoryName, categoryDescription, counterNodes.Count()));

            PerformanceCounterCreationInformation[] performanceCounterCreationInformations =
                ParseCounterNodes(counterNodes, ref messages);

            //Category.Counters:
            PerformanceCounterCategoryCreationInformation performanceCounterCategoryCreationInformation;

            if (!performanceCounterCategoryCreationInformationCache.TryGetValue(categoryName, out performanceCounterCategoryCreationInformation))
            {
                performanceCounterCategoryCreationInformation =
                    new PerformanceCounterCategoryCreationInformation(
                        categoryName,
                        categoryDescription,
                        performanceCounterCreationInformations);

                performanceCounterCategoryCreationInformationCache[categoryName] = performanceCounterCategoryCreationInformation;
            }
            else
            {
                var tmp = performanceCounterCategoryCreationInformation.CounterCreationInformation.ToList();
                foreach (var t in performanceCounterCreationInformations)
                {
                    //We only want one def for a Counter, skip the second:
                    if (tmp.Any(y => y.Name == t.Name))
                    {
                        continue;
                    }
                    tmp.Add(t);
                }
                performanceCounterCategoryCreationInformation.CounterCreationInformation = tmp.ToArray();
            }


        }

        private static PerformanceCounterCreationInformation[] ParseCounterNodes(IEnumerable<XElement> counterNodes, ref List<string> messages)
        {
            List<PerformanceCounterCreationInformation> results = new List<PerformanceCounterCreationInformation>();
            int counterIndex = 0;

            foreach (var counterNode in counterNodes)
            {
                counterIndex++;

                PerformanceCounterCreationInformation performanceCounterCreationInformation = ParseCounter(counterNode,
                                                                                                           counterIndex,
                                                                                                           ref messages);

                results.Add(performanceCounterCreationInformation);

            } //~counter info

            return results.ToArray();
        }

        private static PerformanceCounterCreationInformation ParseCounter(XElement counterNode, int counterIndex, ref List<string> messages)
        {
//Counter.Name:


            string counterName = (string) (counterNode.Attribute("name") ?? counterNode.Attribute("Name"));
            //Counter.Description:
            string counterDescription =
                (string) (counterNode.Attribute("description") ?? counterNode.Attribute("Description")) ??
                string.Empty;


            //Counter.Type:
            string type = (string) (counterNode.Attribute("type") ?? counterNode.Attribute("Type")) ??
                          "NumberOfItems32";
            XAct.Diagnostics.Performance.Services.Entities.PerformanceCounterType performanceCounterType;
            try
            {
                performanceCounterType =
                    (XAct.Diagnostics.Performance.Services.Entities.PerformanceCounterType)
                    Enum.Parse(
                        typeof(XAct.Diagnostics.Performance.Services.Entities.PerformanceCounterType),
                        type, true);
            }
            catch
            {
                messages.Add(
                    "ERROR: Could not parse type: '{0}'. Setting it to '{1}'".FormatStringInvariantCulture(
                        type, PerformanceCounterType.NumberOfItems32));
                performanceCounterType = PerformanceCounterType.CounterDelta32;
            }
            messages.Add(
                "Creating Counter {0} [name:{1}, type:{2}, description:{3}]".FormatStringInvariantCulture(
                    counterIndex, counterName, performanceCounterType, counterDescription));


            return new PerformanceCounterCreationInformation(
                    counterName,
                    counterDescription,
                    performanceCounterType);
        }
    }
}