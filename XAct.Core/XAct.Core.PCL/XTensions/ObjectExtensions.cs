﻿//using System.IO;

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif



namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using XAct.Services;
    using XAct.Services.Implementations;

    public static partial class ObjectExtensions
    {
        private static IConversionService ConversionService
        {
            get { return DependencyResolver.Current.GetInstance<IConversionService>(); }
        }


        
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns true if object is a null.
        /// </summary>
        /// <param name = "value"></param>
        public static bool IsNull([ValidatedNotNull] this object value)
        {
            return (value == null);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the specified value is default (null, or whatever).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns>
        /// 	<c>true</c> if the specified value is default; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public static bool IsDefault<T>(this T value)
        {
            return EqualityComparer<T>.Default.Equals(value);
        }


        /// <summary>
        /// Validates the given value is not default (null, 0, Guid.Empty, etc.).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argName">Name of the arg.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <exception cref="System.ArgumentNullException"></exception>
        public static void ValidateIsNotDefault([ValidatedNotNull] this object value, string argName,
                                                string errorMessage = null)
        {
            if (value.IsDefault())
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    throw new ArgumentNullException(argName);
                }
                throw new ArgumentNullException(argName, errorMessage);
            }
        }

        public static void ValidateIsNotDefaultOrNotInitialized([ValidatedNotNull] this object value, string argName,
                                                                string errorMessage = null)
        {
            if (value.IsDefaultOrNotInitialized())
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    throw new ArgumentNullException(argName);
                }
                throw new ArgumentNullException(argName, errorMessage);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the specified argument is initialized.
        /// </summary>
        /// <param name = "argument">The argument.</param>
        /// <returns>
        ///   <c>true</c> if the specified argument is initialized; otherwise, <c>false</c>.
        /// </returns>
        [DebuggerHidden]
        public static bool IsDefaultOrNotInitialized(this object argument)
        {
            bool isNullOrEmpty;

            if (argument == null)
            {
                return true;
            }


            Type t = argument.GetType();

            if (!t.IsValueType)
            {
                //String:
                isNullOrEmpty = (argument is string) && (((string) argument).Length == 0);
            }
            else
            {
                object defaultValue = Activator.CreateInstance(t);
                isNullOrEmpty = defaultValue.Equals(argument);
            }

            return (isNullOrEmpty);
        }







        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Helper method to convert a type to another type.
        ///   Note how this is slightly better than the tradional
        ///   'ConvertTo' method, which returns all as an object that 
        ///   then needs to be boxed.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        public static TOut ConvertTo<TIn, TOut>(this TIn value, IConversionService conversionService = null)
        {
            if (conversionService == null)
            {
                conversionService = ConversionService;
            }
            return conversionService.Convert<TOut>(value);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Connverts a value from given type to the destination type.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Can accept <c>null</c> and <c>DbNull.Value</c> (returns <c>null</c>).
        ///   </para>
        ///   <para>
        ///     Converts Guid using ToString("D") format, 
        ///     and can recreate Guid from string
        ///     in all Formats.
        ///   </para>
        /// </remarks>
        /// <param name = "value">The value to convert.</param>
        /// <returns>The value converted to the destination type.</returns>
        public static TDestination ConvertTo<TDestination>(this object value, IConversionService conversionService = null)
        {
            if (conversionService == null)
            {
                conversionService = ConversionService;
            }
            return (TDestination)conversionService.Convert(value, typeof(TDestination));
        }




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Connverts a value from given type to the destination type.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Can accept <c>null</c> and <c>DbNull.Value</c> (returns <c>null</c>).
        ///   </para>
        /// </remarks>
        /// <param name = "value">The value to convert.</param>
        /// <param name = "destinationType">The destination Type wanted.</param>
        /// <returns>The value converted to the destination type.</returns>
        [SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1")]
        public static object ConvertTo(this object value, Type destinationType, IConversionService conversionService = null)
        {
            if (conversionService == null)
            {
                conversionService = ConversionService;
            }
            return conversionService.Convert(value, destinationType);
        }


        /// <summary>
        /// Same as <c>Object.Equals()</c>, but doesn't fail if <paramref name="source"/> is null.
        /// </summary>
        /// <param name="source">The first object.</param>
        /// <param name="target">The second object.</param>
        /// <returns></returns>
        public static bool EqualsSafely(this object source, object target)
        {
            if (source == null) {
                return target == null; 
            }
            return source.Equals(target); 
        }

        /// <summary>
        /// Same as <c>Object.ToString()</c>, but doesn't fail if <paramref name="o"/> is null.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <returns></returns>
        public static string ToStringSafely(this object o)
        {
            return o != null ? o.ToString() : string.Empty;
        }

        /// <summary>
        /// Invokes a Generic Method.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// //invoke target.SomeMethod<T>(bool a){....}
        /// //with:
        /// var genericMethodTypes = new []{typeof(int)};
        /// var methodArgs = new []{true};
        /// target.InvokeGenericMethod("SomeMethod",genericMethodTypes, methodArgs);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="target"></param>
        /// <param name="methodName"></param>
        /// <param name="genericTypes"></param>
        /// <param name="methodArguments"></param>
        /// <param name="objecttType"></param>
        /// <returns></returns>
        public static object InvokeGenericMethod(this object target, string methodName, Type[] genericTypes, object[] methodArguments, Type objecttType = null)
        {
            if (objecttType==null){objecttType= target.GetType();}

            MethodInfo genericMethodInfo = objecttType.GetGenericMethod(methodName,genericTypes);

            object result = genericMethodInfo.Invoke(target, methodArguments);
            return result;
        }


        public static IEnumerable<T> ToIEnumarable<T>(this T item)
        {
            yield return item;
        }


    }


}