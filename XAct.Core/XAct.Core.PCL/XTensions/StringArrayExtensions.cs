﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Linq;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension Methods for Strings.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class StringArrayExtensions
    {

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Checks to see that the passed argument
        ///   is not null, or an empty array.
        /// </summary>
        /// <param name = "argumentArray">The argument to check.</param>
        /// <param name = "argumentName">The name of the argument.</param>
        /// <internal>Copied from Normen Code (see web providers).</internal>
        public static void StringArrayNotNullOrEmpty(this string[] argumentArray, string argumentName)
        {
            argumentArray.IsNotNullOrEmpty(argumentName, 1, 0);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Checks to see that the passed argument
        ///   is not null, or an empty array.
        /// </summary>
        /// <param name = "argumentArray">The argument to check.</param>
        /// <param name = "argumentName">The name of the argument.</param>
        /// <param name = "minArrayLength">The minimum number of elements allowed in the array.</param>
        /// <param name = "maxStringLength">The max length of each string element in the array.</param>
        /// <internal>Copied from Normen Code (see web providers).</internal>
        public static void IsNotNullOrEmpty(this string[] argumentArray, string argumentName, int minArrayLength,
                                            int maxStringLength)
        {
            argumentArray.ValidateIsNotDefault(argumentName);

            if (argumentArray.Length < minArrayLength)
            {
                throw new ArgumentException("Argument_can_not_be_empty".FormatStringExceptionCulture(argumentName, Culture.CurrentExceptionCulture));
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            foreach (string t in argumentArray)
            {
                string check = t.Trim();
                if (string.IsNullOrEmpty(check))
                {
                    throw new ArgumentException(
                        "Argument_can_not_have_empty_items".FormatStringExceptionCulture(argumentName));
                }

                if ((check.Length > maxStringLength) && (maxStringLength > 0))
                {
                    throw new ArgumentOutOfRangeException(
                        "One_of_array_item_reach_its_max_length {0}".FormatStringExceptionCulture(maxStringLength),
                        argumentName);
                }
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Joins the strings (safely -- doesn't blow up if it is a null).
        /// </summary>
        /// <param name="stringArray">The string array.</param>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public static string JoinSafely(this string[] stringArray, string separator)
        {
            if (stringArray == null)
            {
                return string.Empty;
            }
            if (separator == null)
            {
                separator = ",";
            }
            return string.Join(separator, stringArray);
        }

        /// <summary>
        /// Joins the strings (safely -- doesn't blow up if it is a null).
        /// </summary>
        /// <param name="strings">The strings.</param>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public static string JoinSafely(this IEnumerable<string> strings, string separator)
        {
            return strings.ToArray().JoinSafely(separator);
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
