﻿// ReSharper disable CheckNamespace

using XAct.IO;

namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;

    /// <summary>
    /// 
    /// </summary>
    public static class XDocumentExtensions
    {


        /// <summary> 
        /// Creates necessary parent nodes using the provided Queue, and assigns the value to the last child node. 
        /// </summary> 
        /// <param name="xElement">XElement to take action on</param> 
        /// <param name="nodeNames">Queue of node names</param> 
        /// <param name="value">Value for last child node</param> 
        public static void UpdateOrCreate(this XElement xElement, Queue<string> nodeNames, string value)
        {
            string previousNodeName = "";

            int queueCount = nodeNames.Count;

            for (int i = 0; i < queueCount; i++)
            {
                string node = nodeNames.Dequeue();

                if (xElement.Descendants(node).FirstOrDefault() == null)
                {
                    if (!string.IsNullOrEmpty(previousNodeName))
                    {
                        xElement.Element(previousNodeName).Add(new XElement(node));
                    }
                    else
                    {
                        // use main parent node if this is the first iteration 
                        xElement.Add(new XElement(node));
                    }
                }
                previousNodeName = node;
            }
            // assign the value of the last child element 
            xElement.Descendants(previousNodeName).First().Value = value;
        }


        /// <summary>
        /// Deserializes the specified <see cref="XDocument"/>
        /// int
        /// </summary>
        /// <typeparam name="T">Type of object to deserialize</typeparam>
        /// <param name="xDocument">The document.</param>
        /// <returns></returns>
        public static T Deserialize<T>(this XDocument xDocument)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof (T));

            if (xDocument.Root != null)
            {
                using (XmlReader reader = xDocument.Root.CreateReader())
                {
                    return (T) xmlSerializer.Deserialize(reader);
                }
            }
            return default(T);
        }

        /// <summary>
        /// Serialize the given object as an <see cref="XDocument"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XDocument SerializeAsXDocument<T>(this T value)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof (T));

            XDocument doc = new XDocument();
            using (XmlWriter writer = doc.CreateWriter())
            {
                xmlSerializer.Serialize(writer, value);
            }

            return doc;
        }


        /// <summary>
        /// This will ensure that elements that are empty
        /// have a string.Empty value -- which effectively
        /// ensures they have both an open and close tag.
        /// <para>
        /// Use only for extremely esoteric legacy-matching scenarios,
        /// as the RFC for XML specifies a preference for self-closing tags,
        /// rather than open/close tags containing no value.
        /// </para>
        /// </summary>
        /// <internal>
        /// Src: http://stackoverflow.com/a/2459207/1052767
        /// </internal>
        /// <param name="document"></param>
        public static void ForceOpenCloseTags(this XDocument document)
        {
            foreach (XElement childElement in
                from xElement in document.DescendantNodes().OfType<XElement>()
                where xElement.IsEmpty
                select xElement)
            {
                childElement.Value = string.Empty;
            }

        }

        public static IEnumerable<XElement> GetDescendentLeafNodes(this XDocument xDocument)
        {
            return xDocument.Descendants().Where(x => !x.Elements().Any());
        }

        /// <summary>
        /// To the stream with declaration.
        /// </summary>
        /// <param name="xDocument">The x document.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="closeStream">if set to <c>true</c> [close stream].</param>
        public static void ToStreamWithDeclaration(this XDocument xDocument, Stream stream, Encoding encoding = null, bool closeStream=false)
        {
            //XmlTextWriter is not PCL?!?!
            //using (var writer = new System.Xml.XmlTextWriter(stream, encoding))
            //{
            //    xDocument.WriteTo(writer);
            //   will close stream too:
            //    writer.Close();
            //}

            XmlWriterSettings settings = new XmlWriterSettings
                { 
                    //CheckCharacters = true
                    //ConformanceLevel = ConformanceLevel.Document
                    //Indent=true,
                    //IndentChars = //
                    //NamespaceHandling=NamespaceHandling.Default,
                    //NewLineChars = ,
                    //NewLineHandling = ,
                    //NewLineOnAttributes = ,
                    //...
                    
                    Encoding = encoding ?? (encoding = Encoding.UTF8), 
                    CloseOutput = closeStream
                };

            xDocument.ToStreamWithDeclaration(stream, settings);
        }

        public static void ToStreamWithDeclaration(this XDocument xDocument, Stream stream, XmlWriterSettings settings)
        {
            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                xDocument.WriteTo(writer);
                //closes writer, and therefore stream:
            }
        }


        public static string ToStringWithDeclaration(this XDocument xDocument, Encoding encoding = null)
        {
            //Not in this App!
            //Contract.Requires<ArgumentNullException>(xDocument!=null);

            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }


            var stringBuilder = new StringBuilder();
            using (TextWriter textWriter = new StringWriterWithEncoding(stringBuilder, encoding))
            {
                xDocument.Save(textWriter, SaveOptions.DisableFormatting);
            }
            return stringBuilder.ToString();
        }

    }
}
