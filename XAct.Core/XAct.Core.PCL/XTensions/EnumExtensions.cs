﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Linq;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension Methods for Enum values.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class EnumExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Maps an enum value to another enum value.
        /// </summary>
        /// <typeparam name = "TSourceEnum"></typeparam>
        /// <typeparam name = "TTargetEnum"></typeparam>
        /// <param name = "source"></param>
        /// <returns></returns>
        public static TTargetEnum MapEnumTo<TSourceEnum, TTargetEnum>(this TSourceEnum source)
        {
            return (TTargetEnum) source.ConvertTo(typeof (TTargetEnum));
            //Enum.GetName(sourceType, source);
        }

    /// <summary>
    /// Gets the Attribute decorating an enum value.
    /// </summary>
    /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
    /// <param name="enumeration">The enumeration.</param>
    /// <returns></returns>
    public static TAttribute GetAttribute<TAttribute>(this Enum enumeration)
        where TAttribute : Attribute
    {
        TAttribute attribute =
            enumeration.GetType().GetMember(enumeration.ToString())[0].GetCustomAttributes(typeof (TAttribute), false)
                                                                      .Cast<TAttribute>()
                                                                      .SingleOrDefault();
        return attribute;
    }


    /// <summary>
        /// Gets the value within an Attribute decorating an Enum value.
        /// <para>
        /// Returns default if not found.
        /// </para>
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <typeparam name="TAttributeValue">The type of the attribute value.</typeparam>
        /// <param name="enumeration">The enumeration.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
    public static TAttributeValue GetAttributeValue<TAttribute, TAttributeValue>(this Enum enumeration,
                                                                                 Func<TAttribute, TAttributeValue>
                                                                                     expression)
        where TAttribute : Attribute
    {
        TAttribute attribute =
            enumeration.GetType().GetMember(enumeration.ToString())[0].GetCustomAttributes(typeof (TAttribute), false)
                                                                      .Cast<TAttribute>()
                                                                      .SingleOrDefault();

        if (attribute == null)
            return default(TAttributeValue);

        return expression(attribute);
    }







    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
