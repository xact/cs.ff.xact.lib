﻿namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using XAct.Environment;

    /// <summary>
    ///   Extension Methods to the DateTime Type.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class DateTimeExtensions
    {

    public static IDateTimeService DateTimeService { get { return XAct.DependencyResolver.Current.GetInstance<IDateTimeService>(); }}
    

    static readonly DateTime SQLMinDate = new DateTime(1753, 1, 1);

        /// <summary>
        /// Determines whether the given nullable date is equal to SQLServer's minimum date (1/1/1753)
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static bool IsStartOfGregorianCalendar(this DateTime? date)
        {
            return date.HasValue && date.Value.IsStartOfGregorianCalendar();
        }

        
        /// <summary>
        /// Determines whether the given date is equal to SQLServer's minimum date (1/1/1753).
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static bool IsStartOfGregorianCalendar(this DateTime date)
        {
            return (object.Equals(date, SQLMinDate));
        }

        /// <summary>
        /// Formats the specified date time (if available) according to the given culture.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="format">The format.</param>
        /// <param name="ifNull">If null.</param>
        /// <returns></returns>
            public static string Format(this DateTime? dateTime, CultureInfo cultureInfo, string format, string ifNull = "N/A")
            {
                return dateTime.HasValue ? dateTime.Value.ToString(format, cultureInfo) : ifNull;
            }

            /// <summary>
            /// Formats the specified date time according to the given culture.
            /// </summary>
            /// <param name="dateTime">The date time.</param>
            /// <param name="cultureInfo">The culture information.</param>
            /// <param name="format">The format.</param>
            /// <returns></returns>
            public static string Format(this DateTime dateTime, CultureInfo cultureInfo, string format)
            {
                return dateTime.ToString(format, cultureInfo);
            }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Formats the dateTime, with milliseconds ('yyyy-MM-dd HH:mm:ss:ffff')
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        /// <internal><para>8/7/2011: Sky</para></internal>
        public static string ToStandardStringWithMilliseconds(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss:ffff");
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns WeekStart
        /// </summary>
        /// <param name = "dateTime"></param>
        /// <returns></returns>
        public static DateTime WeekStart(this DateTime dateTime)
        {
            DateTime tResult = dateTime;
            //DayOfWeek:0=Sunday, 6=Saturday.
            tResult = tResult.AddDays(-((double) tResult.DayOfWeek + 1));
            return new DateTime(tResult.Year, tResult.Month, tResult.Day);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns Last day of week (no time)
        /// </summary>
        /// <param name = "dateTime"></param>
        /// <returns></returns>
        public static DateTime Weekend(this DateTime dateTime)
        {
            DateTime tResult = dateTime;
            int tDelta = (int) tResult.DayOfWeek;
            tResult = tResult.AddDays(7 - tDelta);
            return new DateTime(tResult.Year,tResult.Month, tResult.Day);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns First Day of Month (no time).
        /// </summary>
        /// <param name = "dateTime"></param>
        /// <returns></returns>
        public static DateTime MonthStart(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);

            //This gives OFFSET ...noth same thing:
            //DateTime tResult = dateTime;
            //int offset = -(tResult.Day - 1);
            //tResult = tResult.AddDays(offset);
            //return tResult;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Last Day of Month (no time, so technically, there would be another 23.59 to consider...)
        /// </summary>
        /// <param name = "dateTime"></param>
        /// <returns></returns>
        public static DateTime MonthEnd(this DateTime dateTime)
        {
            return dateTime.MonthStart().AddMonths(1).AddDays(-1);

            //DateTime tResult = dateTime;
            //int offset = -(tResult.Day);
            ////Bring back to first day of this month:
            //tResult = tResult.MonthStart();
            ////Then Add a month
            //tResult = tResult.AddMonths(1).AddDays(-1);
            //return tResult;
        }

        /// <summary>
        /// Gets the Quarter in range 1 to 4
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static int Quarter(this DateTime dateTime)
        {
            //2-1/3 + 1 = 1
            //(3-1)/3 + 1 = 1
            // 5-1/3+1 = 2
            //12-1/3+1 = 3+1=4
            return ((dateTime.Month - 1) / 3) + 1;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns first of Quarter (no time)
        /// </summary>
        /// <param name = "dateTime"></param>
        /// <remarks>
        ///   ToCheck
        /// </remarks>
        public static DateTime QuarterStart(this DateTime dateTime)
        {
            int quarterNumber = dateTime.Quarter();
            return new DateTime(dateTime.Year, (quarterNumber *3)-2, 1);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns last of Quarter (no time, so technically, there would be another 23.59 to consider...)
        /// </summary>
        /// <param name = "dateTime"></param>
        /// <returns></returns>
        /// <remarks>
        ///   ToCheck
        /// </remarks>
        public static DateTime QuarterEnd(this DateTime dateTime)
        {
            int quarterNumber = dateTime.Quarter();
            //quarter is 1 to 4
            return new DateTime(dateTime.Year, quarterNumber*3,1).MonthEnd();

        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Returns start of year
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        /// <internal><para>6/8/2011: Sky</para></internal>
        public static DateTime YearStart(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, 1, 1);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Returns end of year.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        /// <internal><para>6/8/2011: Sky</para></internal>
        public static DateTime YearEnd(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, 12, 31);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns WeekNumber of Year. Default Range:1-52.
        /// </summary>
        /// <param name = "dateTime"></param>
        /// <param name = "firstWeekIsZero"></param>
        /// <returns></returns>
        public static int WeekNumber(this DateTime dateTime, bool firstWeekIsZero)
        {
// ReSharper disable PossibleLossOfFraction
            double t = dateTime.DayOfYear/7;
// ReSharper restore PossibleLossOfFraction

            int weeks = (int) Math.Floor(t);

            if (firstWeekIsZero == false)
            {
                weeks += 1;
            }
            return weeks;
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns Date of nth Week (Range 1-52)
        /// </summary>
        /// <param name = "dateTime"></param>
        /// <param name = "weekNumber"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Usage", "CA2233", Justification = "Shit happens. No biggie.")]
        public static DateTime DateOfWeek(this DateTime dateTime, int weekNumber)
        {
            DateTime tResult = new DateTime(dateTime.Year, 1, 1);

            return tResult.AddDays(7*(weekNumber - 1));
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Return ISO Week number for a given date.
        /// </summary>
        /// <param name = "dt">DateTime to calculate ISO WeekNumber for.</param>
        /// <returns>Integer in range of (1-53).</returns>
        [SuppressMessage("Microsoft.Naming", "1709", Justification = "ISO ok as capitals.")]
        public static int ISOWeekNumber(this DateTime dt)
        {
            // Set Year
            int yyyy = dt.Year;

            // Set Month
            int mm = dt.Month;

            // Set Day
            int dd = dt.Day;

            // Declare other required variables
            int weekNumber = 0;


// ReSharper disable RedundantExplicitArraySize
            int[] mnth = new int[12] {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
// ReSharper restore RedundantExplicitArraySize

            int yearNumber;


            // Set DayofYear Number for yyyy mm dd
            int dayOfYearNumber = dd + mnth[mm - 1];

            // Increase of Dayof Year Number by 1, if year is leapyear and month is february
            if (DateTime.IsLeapYear(yyyy) && (mm == 2))
            {
                dayOfYearNumber += 1;
            }

            // Find the Jan1WeekDay for year 
            int i = (yyyy - 1)%100;
            int j = (yyyy - 1) - i;
            int k = i + i/4;
            int jan1WeekDay = 1 + (((((j/100)%4)*5) + k)%7);

            // Calcuate the WeekDay for the given date
            int l = dayOfYearNumber + (jan1WeekDay - 1);
            int weekDay = 1 + ((l - 1)%7);

            // Find if the date falls in YearNumber set WeekNumber to 52 or 53
            if ((dayOfYearNumber <= (8 - jan1WeekDay)) && (jan1WeekDay > 4))
            {
                yearNumber = yyyy - 1;
                if ((jan1WeekDay == 5) || ((jan1WeekDay == 6) && (jan1WeekDay > 4)))
                {
                    weekNumber = 53;
                }
                else
                {
                    weekNumber = 52;
                }
            }
            else
            {
                yearNumber = yyyy;
            }


            // Set WeekNumber to 1 to 53 if date falls in YearNumber
            if (yearNumber == yyyy)
            {
                int m = DateTime.IsLeapYear(yyyy) ? 366 : 365;
                if ((m - dayOfYearNumber) < (4 - weekDay))
                {
                    yearNumber = yyyy + 1;
                    weekNumber = 1;
                }
            }

            if (yearNumber == yyyy)
            {
                int n = dayOfYearNumber + (7 - weekDay) + (jan1WeekDay - 1);
                weekNumber = n/7;
                if (jan1WeekDay > 4)
                {
                    weekNumber -= 1;
                }
            }

            return (weekNumber);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the actual age of the person (in years).
        /// </summary>
        /// <param name="dateOfBirth">The date of birth.</param>
        /// <returns></returns>
        public static int Age(this DateTime dateOfBirth)
        {
            if (DateTime.Today.Month < dateOfBirth.Month ||
                DateTime.Today.Month == dateOfBirth.Month &&
                DateTime.Today.Day < dateOfBirth.Day)
            {
                return DateTime.Today.Year - dateOfBirth.Year - 1;
            }
            else
            {
                return DateTime.Today.Year - dateOfBirth.Year;
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the specified date is on a weekend.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// 	<c>true</c> if the specified value is on a weekend; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsWeekend(this DateTime value)
        {
            return (value.DayOfWeek == DayOfWeek.Sunday || value.DayOfWeek == DayOfWeek.Saturday);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the last day of the month.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static DateTime GetLastDayOfMonth(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1).AddMonths(1).AddDays(-1);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convert given DateTime to ISO Format (Year - WeekNumber - WeekDay).
        /// </summary>
        /// <remarks>
        ///   Have no idea what this can be useful for.
        /// </remarks>
        /// <param name = "dt">DateTime to Convert</param>
        /// <returns>String</returns>
        [SuppressMessage("Microsoft.Naming", "1709", Justification = "ISO ok as capitals.")]
        public static string DateTimeAsISODate(this DateTime dt)
        {
            int year = dt.Year;
            int weeknumber = ISOWeekNumber(dt);
            int weekday = (int) dt.DayOfWeek;

            string str = year.ToString("d0", CultureInfo.InvariantCulture) + "-" + weeknumber.ToString("d0", CultureInfo.InvariantCulture) +
                         "-" + weekday.ToString("d0", CultureInfo.InvariantCulture);
            return str;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gest the elapsed seconds since the input DateTime
        /// </summary>
        /// <param name="input">Input DateTime</param>
        /// <returns>Returns a Double value with the elapsed seconds since the input DateTime</returns>
        /// <example>
        /// Double elapsed = dtStart.ElapsedSeconds();
        /// </example>
        public static Double ElapsedSeconds(this DateTime input)
        {
            return DateTime.Now.Subtract(input).TotalSeconds;
        }


        public  static DateTime RoundToNearestPreviousInterval(this DateTime dateTime, int intervalMilliseconds)
        {
            int ms = dateTime.Millisecond;
            double r = (double)Math.Floor((double)ms / intervalMilliseconds) * intervalMilliseconds;
            dateTime = new DateTime(dateTime.Ticks - (dateTime.Ticks % TimeSpan.TicksPerSecond)).AddMilliseconds(r);
            return dateTime;
        }


        public static IEnumerable<DateTime> GetIntervalsBetweenDateTimes(this DateTime beginning, DateTime? end = null, int intervalMilliseconds = 1000)
        {
            beginning.ValidateIsUtc("beginning");

            if (end.HasValue)
            {
                end.Value.ValidateIsUtc("beginning");
            }
            else
            {
                end = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC;
            }
            if (intervalMilliseconds < 100)
            {
                intervalMilliseconds = 100;
            }

            List<DateTime> results = new List<DateTime>();

            //Round the now to the nearest *last* interval (approx like Math.Floor):

            end = end.Value.RoundToNearestPreviousInterval(intervalMilliseconds);

            //Round the given dateTime to nearest (lower) interval:

            beginning = beginning.RoundToNearestPreviousInterval(intervalMilliseconds);

            DateTime lastValue = beginning;
            while (lastValue < end)
            {
                lastValue = lastValue.AddMilliseconds(intervalMilliseconds);
                yield return lastValue;
            }

        }


        public static long DateTimeToUnixTimeStamp(this DateTime dateTime)
        {
            dateTime.ValidateIsUtc("dateTime");
            long unixTimestamp = (long)(dateTime.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            return unixTimestamp;
        }


        public static void ValidateIsUtc(this DateTime dateTime, string argumentName)
        {
            if ((dateTime.Kind != DateTimeKind.Utc))
            {
                throw new Exception("'{0}' must be utc".FormatStringCurrentUICulture(argumentName));
            }
        }



        /*
            Now converting Gregorian Date to ISO Date is very easy.

            using System;
            using System.Globalization; 

            public class DateImpl {
    
                public static void Main() {
                    try {
                        DateTime dt=new DateTime(2002,12,1);

                        Trace.WriteLine("Gregorian Date : {0}",
                            dt.ToString("d", DateTimeFormatInfo.InvariantInfo));

                        Trace.WriteLine("ISO Date : {0}",DateEx.DateTimeAsISODate(dt));
                    }
                    catch(Exception e) {
                        Trace.WriteLine("Error \n\n {0}", e.ToString());
                    }
                }
            }
        */
    
    

        // Taken from http://stackoverflow.com/questions/3717791/c-adding-working-days-from-a-cetain-date 
        // (but modified to fix bugs for handling bugs with negative numbers
        /// <summary>
        /// Shifts a given date by a set number of 'working days' (i.e.: Not weekend days)
        /// </summary>
        public static DateTime AddWorkingDays(this DateTime specificDate,
                                              int workingDaysToAdd)
        {
            int completeWeeks = workingDaysToAdd/5;
            DateTime date = specificDate.AddDays(completeWeeks*7);
            workingDaysToAdd = workingDaysToAdd%5;

            var direction = workingDaysToAdd >= 0 ? 1 : -1;

            for (int i = 0; i < Math.Abs(workingDaysToAdd); i++) // Math.Abs used to allow for negative numbers
            {
                date = date.AddDays(direction);
                while (!IsWeekDay(date))
                {
                    date = date.AddDays(direction);
                }
            }
            return date;
        }

        
        public static bool IsWeekDay(this DateTime date, bool mondayIsStartOfWeek=true)
        {
            var day = date.DayOfWeek;
            if (mondayIsStartOfWeek)
            {
                return day != DayOfWeek.Friday
                       && day != DayOfWeek.Saturday;
            }
            else
            {
                return day != DayOfWeek.Saturday
                       && day != DayOfWeek.Sunday;
            }
        }


        public static bool IsFutureDate(this DateTime? date, bool nullDateReturnsTrue=false)
        {
            if (date == null)
            {
                return nullDateReturnsTrue ? true : false;
            }
            return date.Value.IsFutureDate();
        }

        /// <summary>
        /// Determines whether the date component of the date time is in the future (from tomorrow on)
        /// </summary>
        /// <param name="dateOnly">The date.</param>
        /// <returns></returns>
        public static bool IsFutureDate(this DateTime dateOnly)
        {

            return dateOnly.Date > DateTimeService.NowUTC.Date;
        }

        public static bool IsPastDate(this DateTime? date, bool nullDateReturnsTrue=false)
        {
            if (date == null)
            {
                return nullDateReturnsTrue ? true : false;
            }
            return !date.Value.IsFutureDate(); 

        }


        
        /// <summary>
        /// Determines whether the given name is the past.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        public static bool IsPastDate(this DateTime date)
        {
            return !date.IsFutureDate();
        }


        /// <summary>
        /// Determines whether the given time is in the future of at least App.Core.Constants.Common.DateTimeComparisonPrecisionInSec [seconds].	
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public static bool IsFutureDateTime(this DateTime dateTime)
        {
            return dateTime > DateTimeService.NowUTC;
        }

        public static bool IsFutureDateTime(this DateTime? dateTime, bool nullDateReturnsTrue=false)
        {
            if (dateTime == null)
            {
                return nullDateReturnsTrue ? true : false;
            }
           return dateTime > DateTimeService.NowUTC;
        }


        public static bool IsEarlierDateThan(this DateTime? dateTime, DateTime? comparedToDateTime, bool nullDateTimeReturnsTrue = false, bool comparedToDateTimeReturnsTrue = false)
        {
            if (dateTime == null)
            {
                return nullDateTimeReturnsTrue ? true : false;
            }
            if (comparedToDateTime == null)
            {
                return comparedToDateTimeReturnsTrue ? true : false;
            }

            return dateTime.Value.Date < comparedToDateTime.Value.Date;
        }

        public static bool IsLaterDateThan(this DateTime? first, DateTime? second, bool nullDateTimeReturnsTrue = false, bool comparedToDateTimeReturnsTrue = false)
        {
            return !IsEarlierDateThan(first, second, nullDateTimeReturnsTrue, comparedToDateTimeReturnsTrue);
        }

        public static bool IsTodaysDate(this DateTime? date)
        {
            return date.HasValue && date.Value.Date == DateTime.Now.Date;
        }

        /// <summary>
        /// Determines whether the given DateTime instances contains the same Date component.
        /// 
        /// example:
        /// 
        /// 2000.1.1. 3:00pm is the same as 2000.1.1. 2:00pm
        /// 
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns></returns>
        public static bool IsSameDateAs(this DateTime? a, DateTime? b)
        {
            return a.IsSameAs(b, (dt1, dt2) => dt1.Date == dt2.Date);
        }



        /// <summary>
        /// Determines whether the given DateTime instances contains the same Date component.
        /// 
        /// example:
        /// 
        /// 2000.1.1. 3:00pm is the same as 2000.1.1. 2:00pm
        /// 
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="comparedToDate">The b.</param>
        /// <returns></returns>
        public static bool IsSameDateAs(this DateTime date, DateTime comparedToDate)
        {
            return new DateTime?(date).IsSameDateAs(comparedToDate);
        }

        /// <summary>
        /// Determines whether the given datetimes are the same.
        /// 
        /// NOTE: when a comparison operators are used for date time the precision of the data comming from the database and the external 
        /// interfaces and UI differs. Hence the comparison precision currently set to 1 second.
        /// 
        /// </summary>
        /// <param name="dateTime">A.</param>
        /// <param name="comparedToDateTime">The b.</param>
        /// <returns></returns>
        public static bool IsSameDateTimeAs(this DateTime dateTime, DateTime comparedToDateTime)
        {
            return new DateTime?(dateTime).IsSameDateTimeAs(comparedToDateTime);
        }

        /// <summary>
        /// Determines whether the given datetimes are the same.
        /// 
        /// NOTE: when a comparison operators are used for date time the precision of the data comming from the database and the external 
        /// interfaces and UI differs. Hence the comparison precision currently set to 1 second.
        /// 
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns></returns>
        public static bool IsSameDateTimeAs(this DateTime? dateTime, DateTime? comparedToDateTime, TimeSpan? deltaAllowed=null)
        {
            if (!deltaAllowed.HasValue)
            {
                deltaAllowed = XAct.Library.Settings.Comparison.DateTimeComparisonTimeSpan;
            }
            return
                dateTime.IsSameAs
                    (
                        comparedToDateTime,
                        (dt1, dt2) =>
                        dt1.Subtract(dt2).Duration() //duration creates an absolute value of time
                        <
                        deltaAllowed
                    );
        }

        /// <summary>
        /// Determines whether the given datetimes are the same.
        /// NOTE: when a comparison operators are used for date time the precision of the data comming from the database and the external
        /// interfaces and UI differs. Hence the comparison precision currently set to 1 second.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="onlyUseDateComponentForComparison">if set to <c>true</c> only use date component for comparison is used.</param>
        /// <returns></returns>
        public static bool IsSameDateTimeAs(this DateTime? dateTime, DateTime? comparedToDateTime, bool onlyUseDateComponentForComparison)
        {
            return onlyUseDateComponentForComparison ? dateTime.IsSameDateAs(comparedToDateTime) : dateTime.IsSameDateTimeAs(comparedToDateTime);
        }

        /// <summary>
        /// Determines whether the given datetimes are the same.
        /// NOTE: when a comparison operators are used for date time the precision of the data comming from the database and the external
        /// interfaces and UI differs. Hence the comparison precision currently set to 1 second.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="onlyUseDateComponentForComparison">if set to <c>true</c> only use date component for comparison is used.</param>
        /// <returns></returns>
        public static bool IsSameDateAs(this DateTime a, DateTime b, bool onlyUseDateComponentForComparison)
        {
            return new DateTime?(a).IsSameDateTimeAs(b, onlyUseDateComponentForComparison);
        }



    }

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
