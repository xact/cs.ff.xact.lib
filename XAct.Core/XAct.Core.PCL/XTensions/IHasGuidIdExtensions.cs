﻿namespace XAct
{
    using System;
    using XAct.Domain.Repositories;

    /// <summary>
    /// Extensions to entities that
    /// implement <see cref="IHasId{T}"/>
    /// </summary>
    public static class IHasGuidIdExtensions
    {
        /// <summary>
        /// When Persisting entities using Repository, if the Id
        /// is AutoIncrement, it's easy. But that only scales
        /// to 1 machine.
        /// Guids allow scaling, but you then have to choose
        /// between SequentialId -- or a custom service.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="distributedIdService">The distributed identifier service.</param>
        /// <param name="repositoryService">The repository service.</param>
        /// <param name="isNewOverride">The is new override.</param>
        /// <returns></returns>
        public static bool DetermineIfNewAndGenerateGuidIfSo(this IHasId<Guid> entity, 
                                                             IRepositoryService repositoryService,
                                                             IDistributedIdService distributedIdService,
                                                             bool? isNewOverride = null)
        {
            //Note the Ids of both tables are marked as Identity.None
            var hasGuid = (entity.Id == Guid.Empty);

            bool isNew = false;

            if (hasGuid)
            {

                entity.Id = distributedIdService.NewGuid();
                //If no guid, has never been saved before:
                isNew = true;
            }
            else
            {
                //Has a GUid already, but ...is it new?

                if (isNewOverride.HasValue)
                {
                    isNew = isNewOverride.Value;
                }
                else
                {
                    //Since no override provided, guess it by whether
                    //it was ever attached.
                    if (!repositoryService.IsAttached(entity))
                    {
                        isNew = true;
                    }
                }
            }
            return isNew;
        }
    }
}