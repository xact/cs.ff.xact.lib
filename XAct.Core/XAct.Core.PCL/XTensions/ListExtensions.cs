﻿namespace XAct
{
    using System.Collections.Generic;

    public static class ListExtensions
    {
        public static void Truncate<T>(this List<T> list, int newSize)
        {
            //Current list size =8, newSize=5
            if (newSize >= list.Count)
            {
                //0=0
                //1>0
                //1==1
                //if 8 > 5
                //if 8 == 8
                return;
            }
            //8-3:
            int range = list.Count - newSize;

            //01234567
            //.....xxx
            list.RemoveRange(newSize, range);

        }

    }
}