﻿namespace XAct
{
    using System;

    /// <summary>
    /// Extensions to the TimeSpan object.
    /// </summary>
    public static class TimeSpanExtensions
    {

        /// <summary>
        /// Adds the randonmess (an example would be to stagger Worker threads polling a remote service).
        /// </summary>
        /// <param name="timeSpan">The time span.</param>
        /// <param name="fraction">The fraction.</param>
        /// <returns></returns>
        public static TimeSpan AddRandonmess(this TimeSpan timeSpan, int fraction = 4)
        {
            int pollInterval = (int) timeSpan.TotalMilliseconds;


            double interval = timeSpan.TotalMilliseconds;

            Random random = new Random();

            //Make the randomness no more than a quarter of the time
            int maxValue = pollInterval/fraction;

            if (maxValue > 0x3e8)
            {
                maxValue = 0x3e8;
            }

            int num4 = random.Next(2);
            int num5 = random.Next(maxValue);

            interval += (num4 == 0) ? ((double) -num5) : ((double) num5);

            if (interval > (pollInterval + maxValue))
            {
                interval = pollInterval + maxValue;
            }
            if (interval < (pollInterval - maxValue))
            {
                interval = pollInterval - maxValue;
            }

            return new TimeSpan(0,0,0,0,(int)interval);
        }


    }
}
