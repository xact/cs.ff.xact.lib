namespace XAct
{
    using XAct.Environment;

    /// <summary>
    /// Gets or sets the machine id (a hash built using the <see cref="IEnvironmentService.ApplicationNameHash"/>).
    /// </summary>
    public interface IHasMachineId : IHasMachineIdReadOnly
    {
        /// <summary>
        /// Gets or sets the machine id (a hash built using the <see cref="IEnvironmentService.ApplicationNameHash"/>).
        /// </summary>
        /// <value>The machine id.</value>
        new string MachineId { get; set; }
    }
}