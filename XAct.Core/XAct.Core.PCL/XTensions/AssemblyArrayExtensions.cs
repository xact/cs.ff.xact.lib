﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using XAct.Diagnostics;
    using XAct.Initialization;

    /// <summary>
    /// 
    /// </summary>
    public static partial class AssemblyArrayExtensions
    {
        private static ITracingService TracingService
        {
            get { return DependencyResolver.Current.GetInstance<ITracingService>(); }
        }



        public static Type[] GetTypesDecoratedWithAttribute(this Assembly[] assemblies, Type attributeType, bool inherit)
        {
            List<Type> tmp = new List<Type>();
            foreach (Assembly assembly in assemblies)
            {
                Type[] types = assembly.GetTypesDecoratedWithAttributes(attributeType, null, inherit);

                tmp.AddRange(types);
            }

            Type[] results = tmp.ToArray();

            return results;
        }



        public static KeyValuePair<Type, TAttribute>[] GetTypesDecoratedWithAttribute<TAttribute>(
            this Assembly[] assemblies,
            Func<TAttribute, bool> filter = null, bool inherit = false, bool allowMultiples = false)
            where TAttribute : Attribute
        {

            List<KeyValuePair<Type, TAttribute>> tmp = new List<KeyValuePair<Type, TAttribute>>();

            //Dictionary of ClassInstances to their 
            //Dictionary<Type, TAttribute> results = new Dictionary<Type, TAttribute>();
            foreach (Assembly assembly in assemblies)
            {
                tmp.AddRange(assembly.GetTypesDecoratedWithAttibutes(filter, inherit, allowMultiples));
            }

            KeyValuePair<Type, TAttribute>[] results= tmp.ToArray();

            return results;
        }
        public static Type[] GetAllTypesImplementingOpenGenericType<TOpenGenericType>(this Assembly[] assemblies)
        {
            return assemblies.GetAllTypesImplementingOpenGenericType(typeof(TOpenGenericType));
        }


        public static Type[] GetAllTypesImplementingOpenGenericType(this Assembly[] assemblies, Type openGenericType)
        {
            List<Type> results = new List<Type>();

            foreach (Assembly assembly in assemblies)
            {
                results.Add(assembly.GetAllTypesImplementingOpenGenericType(openGenericType));
            }
            return results.ToArray();
        }

        public static Type[] GetTypesImplementingType<TInterface>(this Assembly[] assemblies, bool instantiableOnly=true,Func<Type, bool> filter = null)
        {
            return assemblies.GetTypesImplementingType(typeof (TInterface),instantiableOnly,filter);
        }

        public static Type[] GetTypesImplementingType(this Assembly[] assemblies, Type interfaceType, bool instantiableOnly = true, Func<Type, bool> filter = null)
        { 
            List<Type> results = new List<Type>();
            foreach (Assembly assembly in assemblies)
            {
                results.Add(assembly.GetTypesImplementingType(interfaceType, instantiableOnly, filter));
            }
            return results.ToArray();
        }


        public static KeyValuePair<Type, TAttribute>[] GetTypesDecoratedWithAttribute<TAttribute, TClass>(
    this Assembly[] assemblies,
    Func<TAttribute, bool> filter
    )
            where TAttribute : Attribute
            where TClass : class
        {

            //Get all types in appDomain decorated with given Attribute:
            IQueryable<KeyValuePair<Type, TAttribute>> query =
                assemblies.GetTypesDecoratedWithAttribute<TAttribute>(filter).AsQueryable();


            //IMPORTANT: Note that this can bring in all kinds of classes which are not of right interface
            //So have to filter out:
            query = query.Where(kvp => typeof(TClass).IsAssignableFrom(kvp.Key));

            //Organize initialization by Initialization Stage:
            if (typeof(IHastInitializationStage).IsAssignableFrom(typeof(TAttribute)))
            {
                query = query.OrderBy(kvp => ((IHastInitializationStage)kvp.Value).InitializationStage);
            }

            //Filter by Priority:
            if (typeof(IHasPriorityReadOnly).IsAssignableFrom(typeof(TAttribute)))
            {
                query = query.OrderBy(kvp => ((IHasPriorityReadOnly)kvp.Value).Priority);
            }

            //Then by Order:
            if (typeof(IHasOrderReadOnly).IsAssignableFrom(typeof(TAttribute)))
            {
                query = query.OrderBy(kvp => ((IHasOrderReadOnly)kvp.Value).Order);
            }
            KeyValuePair<Type, TAttribute>[] results = query.ToArray();

            return results;
        }



        /// <summary>
        /// Find all class definitions in the library and current application
        /// that are decorated with the given Attribute, that match the given
        /// Interface, instantiate them, and invoke the given Action on them.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <typeparam name="TClassContract">The type of the class contract.</typeparam>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="action">The action.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="optionalInvokeCallback">The optional invoke callback.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static KeyValuePair<Type, TAttribute>[]
            GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethod<TAttribute, TClassContract>(
            this Assembly[] assemblies,
            Action<TClassContract> action,
            Func<TAttribute, bool> filter,
            Action<bool, Type, TAttribute, TClassContract> optionalInvokeCallback = null
            )
            where TAttribute : Attribute
            where TClassContract : class
        {

            List<KeyValuePair<Type, TAttribute>> typesProcessed = new List<KeyValuePair<Type, TAttribute>>();

            KeyValuePair<Type, TAttribute>[] found =
                assemblies.GetTypesDecoratedWithAttribute<TAttribute, TClassContract>(filter);


            foreach (KeyValuePair<Type, TAttribute> pair in found)
            {

                //IMPORTANT: Remember that original collection
                //can bring in all kinds of classes which are not of right interface


                bool execute = true;

                if (action != null)
                {
                    Type t = pair.Key;
                    //TAttribute attribute = pair.Value;



                    TClassContract initializable;

                    try
                    {

                        initializable = XAct.Shortcuts.ActivateEx<TClassContract>(t);

                        if (initializable == null)
                        {
                            string message = "GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethod: Type:{0} Not of type {1}."
                                .FormatStringCurrentCulture(pair.Key, typeof(TClassContract));
                            TracingService.Trace(TraceLevel.Verbose,
                                                 message);
                            throw new Exception(message);
                        }

                    }
                    //Suppossedly catch(Exception) does not catch unmamanaged.
                    catch
                    {
                        initializable = null;
                        string message = "GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethod: Type:{0} Not convertible to {1}."
                            .FormatStringCurrentCulture(pair.Key, typeof(TClassContract));
                        TracingService.Trace(TraceLevel.Verbose,
                                             message);


                        execute = false;
                    }


                    if (execute)
                    {
                        TracingService.Trace(TraceLevel.Verbose,
                                             "GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethod: Type:{0} ...Begin...."
                                                 .FormatStringCurrentCulture(pair.Key));

                        action.Invoke(initializable);

                        TracingService.Trace(TraceLevel.Verbose,
                                             "GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethod: Type:{0} ...Complete."
                                                 .FormatStringCurrentCulture(pair.Key));


                        if (optionalInvokeCallback != null)
                        {
                            optionalInvokeCallback.Invoke(execute, pair.Key, pair.Value, initializable);
                        }

                    }

                }
                typesProcessed.Add(pair);
            }
            KeyValuePair<Type, TAttribute>[] results =
                typesProcessed.ToArray();

            return results;
        }




    }

}
