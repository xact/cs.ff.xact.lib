﻿//namespace XAct
//{
//    using X = XAct.StringExtensions;

//    public static class WrappedString
//    {
//        public static TValue To<TValue>(this string text)
//        {
//            return X.ConvertTo<TValue>(text);
//        }

//    }
//}

namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Json;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Xml;
    using XSystem.Security.Cryptography;


    public enum Culture
{
        Undefined=0,
    CurrentCulture=1,
    CurrentExceptionCulture = 2,
    CurrentUICulture = 3,
    CurrentCurrencyCulture = 4,
 
}

    /// <summary>
    ///   Extensions for Strings
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static partial class StringExtensions
    {

        /// <summary>
        /// Get the Hash for the given text.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public static string EncodePassword(this string text, string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            byte[] src = Encoding.Unicode.GetBytes(salt);
            byte[] dst = new byte[src.Length + bytes.Length];
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
            HashAlgorithm algorithm =  HashAlgorithm.Create("SHA1");
            byte[] inArray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inArray);
        }

        #region Nested type: Constants

        /// <summary>
        ///   Class of static Resources used by <see cref = "StringExtensions" />
        /// </summary>
        public static class Constants
        {
            /// <summary>
            ///   A Resource.
            /// </summary>
            public static string StringLengthMustBeLargerThanX = Properties.Resources.StringLengthMustBeLargerThanX;

            /// <summary>
            ///   A Resource.
            /// </summary>
            public static string ExceptionLengthMustByLargerThanZero =
                Properties.Resources.Exception_LengthMustByLargerThanZero;
        }

        #endregion


        
        
        
        private static readonly CultureInfo _invariantCulture = CultureInfo.CurrentCulture;


        //#region Example of Static Extension Properties
        //public static int getTest(this string text)
        //{
        //    return 0;
        //}
        //public static void setTest(this string text)
        //{

        //}
        //#endregion


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts the given value to a value of the expected type.
        /// <para>
        /// Useful for parsing Xml attributes into application settings, etc.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>8/16/2011: Sky</para></internal>
        public static TValue ConvertTo<TValue>(this string text)
        {
            return XAct.ObjectExtensions.ConvertTo<TValue>(text);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts the given value to a value of the expected type.
        /// <para>
        /// Useful for parsing Xml attributes into application settings, etc.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="text">The text.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        /// <internal><para>8/16/2011: Sky</para></internal>
        public static TValue ConvertTo<TValue>(this string text, TValue defaultValue)
        {
            return string.IsNullOrEmpty(text) ? defaultValue : text.ConvertTo<TValue>();
        }











        
        ///// <summary>
        ///// <para>
        ///// An XActLib Extension.
        ///// </para>
        /////   Calculate the MD5 for the given string.
        /////   <para>
        /////     Uses <see cref = "Encoding.UTF8" />
        /////     for the character encoding.
        /////   </para>
        ///// </summary>
        ///// <remarks>
        /////   <para></para>
        ///// </remarks>
        ///// <param name = "text">The text.</param>
        ///// <returns>A Guid</returns>
        //public static Guid ToMD5(this string text)
        //{
        //    return new Guid(CalculateHash(text, Encoding.UTF8, "MD5"));
        //}


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Calculates MD5, SHA1-512 hash of a string.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Acceptable Terms are the following 
        ///     (case sensitive):
        ///     <list>
        ///       <item>"MD5"</item>
        ///       <item>"SHA1"</item>
        ///       <item>"SHA256"</item>
        ///       <item>"SHA384"</item>
        ///       <item>"SHA512"</item>
        ///     </list>
        ///   </para>
        /// </remarks>
        /// <param name = "text">input string</param>
        /// <param name = "encoding">Character encoding (default is Encoding.UTF8)</param>
        /// <param name = "hashType">Hash Algorithm Type (case sensitive: MD5|SHA1|SHA256|SHA384|SHA512</param>
        /// <returns>SHA1 hash</returns>
        public static string CalculateHash(this string text, Encoding encoding = null, string hashType = "SHA1")
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            encoding.ValidateIsNotDefault("encoding");

            //Convert string to byte buffer:
            byte[] buffer = encoding.GetBytes(text);

            //HashAlgorithm is IDisposable:
            using (HashAlgorithm hashAlgorithm =
                HashAlgorithm.Create(hashType))
            {
                byte[] hashBuffer =
                    hashAlgorithm.ComputeHash(buffer);

                //Method a:
                //System.Text.StringBuilder sb =
                //  new System.Text.StringBuilder();
                //foreach (byte b in hashBuffer) {
                //  sb.Append(b.ToString("x2"));//'X2' for uppercase
                //}
                //return sb.ToString();

                //Method B: 
                //string hashString =
                //  BitConverter.ToString(hashBuffer);
                //The output looks like:
                //"19-E2-62-AE-3A-84-0D-72-1F-EF-32-C9-25-D1-A1-89-67-13-5F-58"
                //so you want to remove the hashes in most cases:
                //return hashString.Replace("-", "");

                //Method C: Homebrewed speed:
                //Convert 20byte buffer to string...
                return hashBuffer.ToHexString();
            }
        }








        /// <summary>
        /// Formats the specified text in the given culture.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public static string Format(this string text, CultureInfo cultureInfo, params object[] args)
        {
            try
            {
                return string.Format(cultureInfo, text, args);
            }
            catch (System.FormatException e)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(text??string.Empty);
                sb.Append(":");
                sb.Append(args.JoinSafely(","));

                throw new FormatException(e.Message + " (Arguments: " + sb + ")", e);
            }
        }





        /// <summary>
        /// Formats the specified text according to the specified arguments.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="culture">The culture.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public static string Format(this string text, Culture culture, params object[] args)
        {
            CultureInfo cultureInfo;
            switch (culture)
            {
                case Culture.CurrentCulture:
                    cultureInfo = CultureInfo.CurrentCulture;
                    break;
                case Culture.CurrentUICulture:
                    cultureInfo = CultureInfo.CurrentUICulture;
                    break;
                case Culture.CurrentCurrencyCulture:
                    cultureInfo = CultureInfo.CurrentUICulture;
                    break;
                case Culture.CurrentExceptionCulture:
                    cultureInfo = CultureInfo.CurrentUICulture;
                    break;
                default:
                    cultureInfo = CultureInfo.InvariantCulture;
                    break;
            }

            //Use Extension method to have common error throwing:
            return Format(text, cultureInfo, args);
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Formats the string using the CurrentUICulture (Resource strings - not Dates, Numbers, etc).
        /// <para>
        /// This is for error messages that should not be seen by user.
        /// But just in case they are, they should be in culture of end user.
        /// That said, since error messages are rarely translated, will fall back 
        /// to server culture in most cases anyway...
        /// </para>
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        /// <internal><para>7/17/2011: Sky</para></internal>
        public static string FormatStringExceptionCulture(this string text, params object[] args)
        {
            //Use Extension method to have common error throwing:
            return Format(text, Thread.CurrentThread.CurrentUICulture, args);
        }



        public static string FormatStringInvariantCulture(this string text, params object[] args)
        {
            //Use Extension method to have common error throwing:
            return Format(text, CultureInfo.InvariantCulture, args);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        ///   Formats the string using the CurrentCulture (numbers, dates, etc.)
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// var result = "{0}x{1} = {2}".FormatStringCurrentUICulture(
        ///     "{0}".FormatStringCurrentCulture(1),
        ///     "Red Sock",
        ///     "10.00".FormatStringCurrencyCulture(transactionCurrencyCulture)
        /// );
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// WARNING: Are you sure you are wanting to use this, rather than
        /// <see cref="FormatStringCurrentUICulture"/>
        /// </para>
        /// <para>
        /// Note: 
        /// A common solution is to format all numerical/date/currency 
        /// arguments into strings first, using <see cref="FormatStringCurrentCulture"/>
        /// and <see cref="FormatStringCurrencyCulture"/>,
        /// and then pass those strings into this method as arguments.
        /// </para>
        /// </summary>
        /// <param name = "text">The text.</param>
        /// <param name = "args">The args.</param>
        /// <returns></returns>
        public static string FormatStringCurrentCulture(this string text, params object[] args)
        {
            //Use Extension method to have common error throwing:
            return Format(text, Thread.CurrentThread.CurrentCulture, args);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        /// Formats the string using the current UI culture 
        /// (string resources, not dates/numbers, etc.).
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// var result = "{0}x{1} = {2}".FormatStringCurrentUICulture(
        ///     "{0}".FormatStringCurrentCulture(1),
        ///     "Red Sock",
        ///     "10.00".FormatStringCurrencyCulture(transactionCurrencyCulture)
        /// );
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Note: 
        /// A common solution is to format all numerical/date/currency 
        /// arguments into strings first, using <see cref="FormatStringCurrentCulture"/>
        /// and <see cref="FormatStringCurrencyCulture"/>,
        /// and then pass those strings into this method as arguments.
        /// </para>
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        /// <internal><para>7/17/2011: Sky</para></internal>
        public static string FormatStringCurrentUICulture(this string text, params object[] args)
        {
            //Use Extension method to have common error throwing:
            return Format(text, Thread.CurrentThread.CurrentUICulture, args);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        /// Formats the string using the given <see cref="CultureInfo"/>
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// var result = "{0}x{1} = {2}".FormatStringCurrentUICulture(
        ///     "{0}".FormatStringCurrentCulture(1),
        ///     "Red Sock",
        ///     "10.00".FormatStringCurrencyCulture(transactionCurrencyCulture)
        /// );
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Note: 
        /// A common solution is to format all numerical/date/currency 
        /// arguments into strings first, using <see cref="FormatStringCurrentCulture"/>
        /// and <see cref="FormatStringCurrencyCulture"/>,
        /// and then pass those strings into this method as arguments.
        /// </para>
        /// <para>
        /// This is specifically called out so that nobody makes the mistake
        /// thinking that UI culture is the same as currency (you can have a US developed app, 
        /// shown to a french person (ui culture), trading in Swiss frances (currency)...)
        /// </para>
        /// </summary>
        /// <param name="text"></param>
        /// <param name="cultureInfo"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string FormatStringCurrencyCulture(this string text, CultureInfo cultureInfo, params object[] args)
        {
            //Use Extension method to have common error throwing:
            return Format(text, cultureInfo, args);
        }


        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        ///   </para>
        ///   <para>
        /// Returns a string of the same length, with '*' for every character.
        ///   </para>
        ///   <para>
        /// Usage Example:
        ///   <code>
        ///   <![CDATA[
        /// Debug.Assert("Passw0rd".SafeString(0,false) == "********");
        /// ]]>
        ///   </code>
        ///   </para>
        ///   <para>
        /// Note that it does not throw an exception if the string is null or empty.
        ///   </para>
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="leaveVisibleFirstNChars">The leave visible first N chars.</param>
        /// <param name="obfuscateLengthAsWell">if set to <c>true</c> [obfuscate length as well].</param>
        /// <returns></returns>
        /// <internal>5/15/2011: Sky</internal>
        public static string SafeString(this string text, int leaveVisibleFirstNChars=0, bool obfuscateLengthAsWell = true)
        {
            int length = (text == null) ? 0 : text.Length;

            int prefixLength = Math.Min(leaveVisibleFirstNChars, length);

            if (length != 0 && obfuscateLengthAsWell)
            {
                length += new Random().Next(6);
            }

            return (string.IsNullOrEmpty(text)) ? 
                string.Empty :
                text.Left(prefixLength) + new string('*', length - prefixLength);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        ///   Determines whether the argument is null or empty.
        /// </para>
        /// <para>
        /// Usage example:
        /// <code>
        /// <![CDATA[
        /// Debug.Assert(((string)null).IsNullOrEmpty());
        /// Debug.Assert(string.Empty.IsNullOrEmpty());
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name = "argument">The argument.</param>
        /// <returns>
        ///   <c>true</c> if [is null or empty] [the specified argument]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty(this string argument)
        {
            return string.IsNullOrEmpty(argument);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        /// Checks a string argument to ensure it isn't null or empty.
        /// </para>
        /// <para>
        /// Usage example:
        /// <code>
        /// <![CDATA[
        ///  void SomeMethod(string someText){
        ///    someText.ValidateIsNotDefault("someText");
        ///  ...
        ///  }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="argument">The argument value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <param name="errorMessage">The optional error message.</param>
        public static void ValidateIsNotNullOrEmpty([ValidatedNotNull] this string argument, string argumentName, string errorMessage=null)
        {
            if (string.IsNullOrEmpty(argument))
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    throw new ArgumentNullException(argumentName, errorMessage);
                }
                throw new ArgumentNullException(argumentName);
            }
        }

    /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Checks that the string is not null, or empty, or over the maxStringLength.
        /// </summary>
        /// <param name = "argument">The string argument.</param>
        /// <param name = "argumentName">Name of the argument.</param>
        /// <param name = "minStringLength">Min Length of the string.</param>
        /// <param name = "maxStringLength">Max Length of the string.</param>
        /// <param name = "suppressErrorIfOverLength">Flag to suppress error if argument is over maxStringLength.</param>
        public static string ValidateIsNotNullOrEmpty([ValidatedNotNull] this string argument, string argumentName, int minStringLength,
                                                      int maxStringLength, bool suppressErrorIfOverLength)
        {
            if (!argument.IsNullOrEmpty())
            {
                throw new ArgumentNullException("argumentName");
            }
            if (argument.Length < minStringLength)
            {
                throw new ArgumentOutOfRangeException(
                    "Argument '{0}' is under the max length of '{1}'.".FormatStringExceptionCulture(argumentName,
                                                                                                    minStringLength));
            }

            if ((maxStringLength > 0) && (argument.Length > maxStringLength))
            {
                if (!suppressErrorIfOverLength)
                {
                    throw new ArgumentOutOfRangeException(
                        "Argument '{0}' is over the max length of '{1}'.".FormatStringExceptionCulture(argumentName,
                                                                                                       maxStringLength));
                }
                return argument.Substring(0, maxStringLength);
            }

            return argument;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Checks the string to ensure it not or empty, 
        ///   returns the string argument trimmed 
        ///   to max length, if need be,
        ///   without throwing an error 
        ///   (if error raising suppressed).
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Invokes <see cref = "ValidateIsNotNullOrEmpty(string, string,string)" />.
        ///   </para>
        /// </remarks>
        /// <param name = "argument">The argument.</param>
        /// <param name = "argumentName">Name of the argument.</param>
        /// <param name = "maxLength">Max Length of the string.</param>
        /// <param name = "suppressErrorIfOverLength">
        ///   If set to <c>true</c> suppresses error, 
        ///   and simply returns trimmed string.</param>
        [SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public static string ValidateIsNotNullOrEmptyOrTooLong([ValidatedNotNull] this string argument, string argumentName, int maxLength,
                                                               bool suppressErrorIfOverLength)
        {
            argument.ValidateIsNotNullOrEmpty(argumentName);

            if (argument.Length > maxLength)
            {
                if (!suppressErrorIfOverLength)
                {
                    throw new ArgumentException(
                        "String argument ('{0}') too long (longer than {1} chars).".FormatStringExceptionCulture(
                            argumentName,
                            maxLength));
                }
                return argument.Substring(0, maxLength);
            }
            return argument;
        }


        /*
    public static string IsNotNullOrEmptyOrTooLong(string paramValue, string paramName, bool throwIfNull, int lengthToCheck) {
        if (string.IsNullOrEmpty(paramValue)) {
            if (throwIfNull) {
                throw new ArgumentNullException(paramName);
            }
            return null;
        }

        string result = paramValue.Trim();

        if ((lengthToCheck > -1) && (result.Length > lengthToCheck)) {
            throw new ArgumentException("String too long.");
        }
        return result;
    }
      */



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        /// If text is null or empty, return an Empty Guid,
        /// otherwise tries to parse the string into a Guid object.
        /// </para>
        /// <para>
        /// TODO: If it fails, returns Guid.Empty
        /// </para>
        /// </summary>
        /// <param name = "text"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Performance", "CA1820", Justification = "I don't want to use string.IsNullOrEmpty")]
        public static Guid ToGuid(this string text)
        {
            Guid tResult = new Guid();

            if (text == null)
            {
                return tResult;
            }

            text = text.Trim();

            if (text == String.Empty)
            {
                return tResult;
            }
            try
            {
                return new Guid(text);
            }
// ReSharper disable EmptyGeneralCatchClause
            catch
// ReSharper restore EmptyGeneralCatchClause
            {
            }

            if (text.Length == 32)
            {
                //ddddddddDdddDdddDdddDddddddddddd
                //dddddddd-dddd-dddd-dddd-dddddddddddd
                text =
                    "{" +
                    text.Substring(0, 8) + "-" +
                    text.Substring(8, 4) + "-" +
                    text.Substring(12, 4) + "-" +
                    text.Substring(16, 4) + "-" +
                    text.Substring(20, 12) +
                    "}";
            }

            try
            {
                return new Guid(text);
            }
            catch
            {
                return tResult;
            }
        }


        /*
		
		/// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
		/// Convert given string to DateTime based on several standard formats.
		/// </summary>
		/// <remarks>
		/// <para>
		/// You should not use this method in production code -- only use it as an example.
		/// </para>
		/// </remarks>
		/// <param name="qDateTime">DateTime to convert to string.</param>
		/// <returns>String representation of Date.</returns>
		public static System.DateTime StrToDateTime(string qDateTime) {

			System.DateTime tResult;
			string[] tFormats = new string[]{
												"yyyy-M-d H:m:s",
												"yyyy-M-d",
												"d",
												"G",
												"t"
											};

			try {
				tResult = DateTime.ParseExact(qDateTime,tFormats,System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None); 
				return tResult;
			}
			catch {
				tResult = new System.DateTime();
				return tResult;
			}
		}
        */

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convert given DateTime to a format specified by the given CultureCode.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     You should not use this method in production code -- only use it as an example.
        ///   </para>
        /// </remarks>
        /// <param name = "dateTime">DateTime to convert to string.</param>
        /// <param name = "cultureCode">Eg: "en-US"</param>
        /// <returns>String representation of Date.</returns>
        [SuppressMessage("Microsoft.Naming", "CA2204", Justification = "Spelling ('DateTime') is ok.")]
        public static DateTime ToDateTime(this string dateTime, string cultureCode)
        {
            try
            {
                IFormatProvider tFormat = new CultureInfo(cultureCode);
                return Convert.ToDateTime(dateTime, tFormat);
            }
            catch (Exception exception)
            {
                throw new ArgumentException(
                    "Could not Convert String to DateTime: {0}".FormatStringExceptionCulture(dateTime), exception);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Decode a base64 string to byte array.
        /// </summary>
        /// <param name = "base64String">Base64 encoded String to decode.</param>
        /// <returns>Decoded string.</returns>
        /// <remarks>
        ///   <para>
        ///     Demonstrates how to decode a Base64 encoded string.
        ///   </para>
        ///   <para>
        ///     Attention: Base64 is commonly perceived as simply converting the string to base 64. It is not -- Base64
        ///     encryption also contains rules about lengths. Read the RFCs for more info.
        ///   </para>
        /// </remarks>
        [SuppressMessage("Microsoft.Naming", "CA1720",
            Justification = "'base64String' is a perfectly good argument name.")]
        public static string DecodeFromBase64(this string base64String)
        {
            if (string.IsNullOrEmpty(base64String))
            {
                return string.Empty;
            }
            byte[] tBuffer = Convert.FromBase64String(base64String);

            //return Encoding.ASCII.GetString(tBuffer);
            //Encoding.ASCII is not available in PCL:
            return Encoding.UTF8.GetString(tBuffer,0,tBuffer.Length);

            //Alternate description of what is really going on:
            //return System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(qInput));
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts a Base64 encoded string to an array of Bytes (not chars!).
        /// </summary>
        /// <param name = "base64String">Base64 encoded String to decode.</param>
        /// <returns>Array of bytes of decoded string.</returns>
        /// <remarks>
        ///   Demonstrates how to decode a Base64 encoded string to a Byte array. 
        ///   Note that it returns a Byte array -- not an array of Chars.
        /// </remarks>
        public static byte[] Base64ToByteArray(this string base64String)
        {
            try
            {
                return Convert.FromBase64String(base64String);
            }

            catch (Exception exception)
            {
                throw new ArgumentException("Could not Convert Byte Array to Base64.", exception);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Encode a string to Base64 string.
        /// </summary>
        /// <param name = "stringToEncode">String to Encode in Base64</param>
        /// <returns>Base64 Encoded string</returns>
        /// <remarks>
        ///   Demonstrates how to encode a string to Base64, which is used in all Email programs, etc.
        /// </remarks>
        [SuppressMessage("Microsoft.Naming", "CA1720",
            Justification = "'stringToEncode' is a perfectly good argument name.")]
        public static string ToBase64(this string stringToEncode)
        {
            byte[] tBuffer = stringToEncode.ToByteArray();
            return (tBuffer != null) ? Convert.ToBase64String(tBuffer) : string.Empty;

            //return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(qInput));
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convert any possible string-Value of a given enumeration
        ///   type to its internal representation.
        /// </summary>
        /// <param name = "type"></param>
        /// <param name = "text"></param>
        /// <returns></returns>
        public static object StringToEnum(this string text, Type type)
        {
            type.ValidateIsNotDefault("type");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
Contract.EndContractBlock();
#endif

            foreach (FieldInfo fieldInfo in type.GetFields())
            {
                if (fieldInfo.Name == text)
                {
// ReSharper disable AssignNullToNotNullAttribute
                    return fieldInfo.GetValue(null); // We use null because
// ReSharper restore AssignNullToNotNullAttribute
                    // enumeration values
                    // are static
                }
            }

            throw new ArgumentException("Can't convert {0} to  {1}".FormatStringExceptionCulture(text, type));
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        ///   Determines whether the specified string 
        ///   contains numeric data 
        ///   (ie: parsable to an 32bit signed integer).
        /// </para>
        /// <para>
        /// Example:
        /// <code>
        /// <![CDATA[
        /// Debug.Assert("123".IsNumeric());
        /// Debug.Assert(!"1A23".IsNumeric());
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name = "text">The current string.</param>
        /// <returns>
        ///   <c>true</c> if the specified s is numeric; otherwise, <c>false</c>.
        /// </returns>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static bool IsNumeric(this string text)
        {
            int integerNumber;

            return int.TryParse(text, out integerNumber);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Deserializes a String into a boolean value.
        /// </summary>
        /// <remarks>
        /// <para>
        ///   The NET Framework only understands the words,
        ///   <c>True</c>, and <c>False</c>, which 
        ///   is fine in many cases -- but can cause 
        ///   problems parsing XML files that end users
        ///   have edited by hand, and used 1, 0, 
        ///   Yes, No, Ok, Oui, One, Nein, Zero, True, true... 
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// Debug.Assert("Yes".ToBool());
        /// Debug.Assert("Y".ToBool());
        /// Debug.Assert("No".ToBool()==false);
        /// Debug.Assert("no".ToBool()==false);
        /// Debug.Assert("n".ToBool()==false);
        /// Debug.Assert("Oui".ToBool());
        /// Debug.Assert("Ok".ToBool());
        /// Debug.Assert("Non".ToBool()==false);
        /// Debug.Assert("1".ToBool());
        /// Debug.Assert("0".ToBool()==false);
        /// Debug.Assert("True".ToBool());
        /// Debug.Assert("true".ToBool());
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name = "text"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Naming", "CA1720", Justification = "text is more descriptive than 'value'.")]
        public static bool ToBool(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }

            string testChar = text.Substring(0, 1).ToUpper(/*_invariantCulture*/);
            string secondChar = (text.Length > 1) ? (text.Substring(1, 1).ToUpper()):null;

            switch (testChar)
            {
                case "S": //Can't remember why I added S and V...hum..
                case "V":
                case "O":
                    return secondChar != "F";//OFF
                    //versus:
                    //ON
                    //OUI
                    //OK
                case "Y":
                case "1":
                case "T":
                    return true;
            }
            return false;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts a String to an char[] array.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// char[] buffer = "...some xml...".ToCharArray();
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <param name = "text"></param>
        /// <returns></returns>
        public static char[] ToCharArray(this string text)
        {
            return string.IsNullOrEmpty(text) ? new char[0] : text.ToCharArray();
        }


        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts a String to Byte Array
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns></returns>
        /// <remarks>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// byte[] buffer = "...some xml...".ToByteArray();
        /// ]]>
        /// </code>
        /// </remarks>
        public static byte[] ToByteArray(this string text, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            return string.IsNullOrEmpty(text) 
                ? null 
                //Note thate ASCII was not available on PCL so changed to UTF8.
                : encoding.GetBytes(text);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts a Hex string into an Integer
        /// </summary>
        /// <param name = "hexNumberString"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Naming", "CA1720",
            Justification = "'hexNumberString' is a perfectly good argument name.")]
        public static int ToIntegerFromHexString(this string hexNumberString)
        {
            try
            {
                return int.Parse(hexNumberString, NumberStyles.HexNumber, _invariantCulture);
            }
            catch (Exception exception)
            {
                throw new ArgumentException(
                    "Could not convert Hex ('{0}') to Integer.".FormatStringCurrentUICulture(hexNumberString)
                    ,
                    exception);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns the left portion of the string, up to the specified length.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Examples of use are:
        ///     <code>
        ///       <![CDATA[
        /// string test = "A test.";
        /// Console.WriteLine(test.Left(4)); //"A te"
        /// Console.WriteLine(test.Left(20)); //"A test."
        /// Console.WriteLine(test.Left(0)); //""
        /// Console.WriteLine(test.Left(-20)); //Causes exception.
        /// ]]>
        ///     </code>
        ///   </para>
        ///   <para>
        ///     See also <see cref = "Right(string,int)" />.
        ///   </para>
        /// </remarks>
        /// <param name = "text">The current string.</param>
        /// <param name = "length">The length.</param>
        /// <returns></returns>
        /// <exception cref = "ArgumentOutOfRangeException">
        ///   An exception is raised if 
        ///   <paramref name = "length" />
        ///   is less than <c>0</c>.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string Left(this string text, int length)
        {
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(
                    Constants.StringLengthMustBeLargerThanX.FormatStringCurrentUICulture(length));
            }

            if (length == 0 || text.IsNullOrEmpty()) //Interesting options...
            {
                return string.Empty;
            }
            return text.Length <= length ? text : text.Substring(0, length);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns the right portion of the string, up to the specified length.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Examples of use are:
        ///     <code>
        ///       <![CDATA[
        /// string test = "A test.";
        /// Console.WriteLine(test.Right(4)); //"est."
        /// Console.WriteLine(test.Right(20)); //"A test."
        /// Console.WriteLine(test.Right(0)); //""
        /// Console.WriteLine(test.Right(-20)); //Causes exception.
        /// ]]>
        ///     </code>
        ///   </para>
        ///   <para>
        ///     See also <see cref = "Left(string, int)" />.
        ///   </para>
        /// </remarks>
        /// <param name = "text">The current string.</param>
        /// <param name = "length">The length.</param>
        /// <returns></returns>
        /// <exception cref = "ArgumentOutOfRangeException">
        ///   An exception is raised if 
        ///   <paramref name = "length" />
        ///   is less than <c>0</c>.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string Right(this string text, int length)
        {
            if (text.IsNullOrEmpty())
            {
                return text;
            }

            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(
                    Constants.ExceptionLengthMustByLargerThanZero.FormatStringCurrentCulture(length));
            }
            if (length == 0 || text.Length == 0)
            {
                return "";
            }
// ReSharper disable ConvertIfStatementToReturnStatement
            if (text.Length <= length)
// ReSharper restore ConvertIfStatementToReturnStatement
            {
                return text;
            }
            return text.Substring(text.Length - length, length);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns true if Text ends with given text.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "substituteText"></param>
        /// <param name = "caseSensitive"></param>
        /// <returns></returns>
        public static bool EndsWith(this string text, string substituteText, bool caseSensitive)
        {
            if (text.IsNullOrEmpty())
            {
                return false;
            }
            if (substituteText.IsNullOrEmpty())
            {
                return false;
            }

            /*
             * ASP.NET 2.0
            if (caseSensitive)
            {
                return text.EndsWith(substituteText);
            }
            else
            {
                return text.EndsWith(substituteText, true, StringComparison.CurrentCulture);
            }
             */

            int tLen = substituteText.Length;
            //CONTAINS
            //	  AINS
            //01234567
            //4-8=4

            if (caseSensitive)
            {
                if (text.Substring(text.Length - tLen, tLen) == substituteText)
                {
                    return true;
                }
            }
            else
            {
                if (text.Substring(text.Length - tLen, tLen).ToUpper() == substituteText.ToUpper(/*_invariantCulture*/))
                {
                    return true;
                }
            }

            return false;
        }

//Method End:

        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        ///   </para>
        ///   <para>
        /// Checks to see that start of strings matches given text.
        ///   </para>
        ///   <para>
        /// Usage Example:
        ///   <code>
        ///   <![CDATA[
        /// Debug.Assert("Balloon".StartsWith("Ball"));
        /// Debug.Assert("Balloon".StartsWith("ball")==false);
        /// Debug.Assert("Balloon".StartsWith("ball",true));
        /// ]]>
        ///   </code>
        ///   </para>
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="pattern">The pattern.</param>
        /// <param name="caseSensitive">if set to <c>true</c> [case sensitive].</param>
        /// <returns></returns>
        public static bool StartsWith(this string text, string pattern, bool caseSensitive=true)
        {
            if (text.IsNullOrEmpty())
            {
                throw new ArgumentNullException("text");
            }
            if (pattern.IsNullOrEmpty())
            {
                throw new ArgumentNullException("pattern");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            if (text.Length < pattern.Length)
            {
                return false;
            }
            try
            {
                if (caseSensitive)
                {
                    if (text.Substring(0, pattern.Length) == pattern)
                    {
                        return true;
                    }
                }
                else
                {
                    if (text.Substring(0, pattern.Length).ToUpper() == pattern.ToUpper(/*_invariantCulture*/))
                    {
                        return true;
                    }
                }
            }
// ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
// ReSharper restore EmptyGeneralCatchClause
            {
            }
            return false;
        }

        /// <summary>
        /// Compares the given string with the given wildcard pattern.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="wildcardPattern">The wildcard pattern.</param>
        /// <param name="emptyWildvardIsSameAsMatchAll">if set to <c>true</c> [empty wildvard is everything].</param>
        /// <param name="regexOptions">The regex options.</param>
        /// <returns></returns>
        public static bool CompareWildcard(this string text, string wildcardPattern, bool emptyWildvardIsSameAsMatchAll, RegexOptions regexOptions = RegexOptions.CultureInvariant| RegexOptions.IgnoreCase)
        {
            if (wildcardPattern == "*")
            {
                return true;
            }
            if (wildcardPattern.IsNullOrEmpty())
            {
                return (emptyWildvardIsSameAsMatchAll);
            }
            
            string wildCardPattern = wildcardPattern.WildcardPatternToRegexPattern();

            return text.RegexMatch(wildCardPattern, regexOptions);
        }

        /// <summary>
        /// Determines whether the string contains the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="pattern">To check.</param>
        /// <param name="comp">The comp.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified source]; otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains(this string source, string pattern, StringComparison comp = StringComparison.OrdinalIgnoreCase)
        {
            return source.IndexOf(pattern, comp) >= 0;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines if that char at [n]pos is control character.
        /// </summary>
        /// <param name = "text">string to analyse.</param>
        /// <param name = "index">Index of Character</param>
        /// <returns>Returns true if Charater is a Control Character.</returns>
        public static bool IsControlChar(this string text, int index)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            return index < text.Length && Char.IsControl(text, index);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines if char at [n]pos is numerical.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "index"></param>
        /// <returns></returns>
        public static bool IsDigitChar(this string text, int index)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            return index < text.Length && Char.IsDigit(text, index);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines if char at [n]pos is alpha.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "index"></param>
        /// <returns></returns>
        public static bool IsLetterChar(this string text, int index)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            return index < text.Length && Char.IsLetter(text, index);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines if char at [n]pos is punctuation.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "index"></param>
        /// <returns></returns>
        public static bool IsPunctuationChar(this string text, int index)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            return index < text.Length && Char.IsPunctuation(text, index);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determins if char at [n]pos is lowercase.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "index"></param>
        /// <returns></returns>
        public static bool IsLowerCaseChar(this string text, int index)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            return index < text.Length && Char.IsLower(text, index);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines if char at [n]pos is uppercase.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "index"></param>
        /// <returns></returns>
        public static bool IsUpperCaseChar(this string text, int index)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            return index < text.Length && Char.IsUpper(text, index);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines if char at [n]pos is Symbol.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "index"></param>
        /// <returns></returns>
        public static bool IsSymbolChar(this string text, int index)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            return index < text.Length && Char.IsSymbol(text, index);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines if char at [n]pos is whitespace.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "index"></param>
        /// <returns></returns>
        public static bool IsWhiteSpaceChar(this string text, int index)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            return index < text.Length && Char.IsWhiteSpace(text, index);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the specified text is same as another text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="textToComareAgainst">The text to comare against.</param>
        /// <param name="stringComparison">The string comparison.</param>
        /// <returns>
        ///   <c>true</c> if the specified text is same; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSame(this string text, string textToComareAgainst, StringComparison stringComparison = StringComparison.Ordinal)
        {
            return (String.Compare(text, textToComareAgainst, stringComparison)==0);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts the given string list (or other enumerable source)
        ///   to a single string, with each element separated from the next
        ///   by the given <paramref name = "separator" /> string.
        ///   <para>
        ///     Always returns a string, never null.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     An example of usage would be:
        ///     <code>
        ///       <![CDATA[
        /// string[] input = {"john", "paul", "mary"};
        /// string result = input.ToSingleString(";"); 
        /// //result will be "john;paul;mary"
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "textEnumerable">The current enumerable string collection/list.</param>
        /// <param name = "separator">The separator string.</param>
        /// <returns>A string (never null).</returns>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string ToSingleString(this IEnumerable<string> textEnumerable, string separator)
        {
            return textEnumerable.ToSingleString(separator, false);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts the given string list (or other enumerable source)
        ///   to a single string, with each element separated from the next
        ///   by the given <paramref name = "separator" /> string.
        ///   <para>
        ///     Always returns a string, never null.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     An example of usage would be:
        ///     <code>
        ///       <![CDATA[
        /// string[] input = {"john", "paul", "mary"};
        /// string result = input.ToSingleString(";"); 
        /// //result will be "john;paul;mary"
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "textEnumerable">The current enumerable string collection/list.</param>
        /// <param name = "separator">The separator string.</param>
        /// <param name = "ignoreNullOrEmpty">Flag determine whether to ignore null or empty strings in the list.</param>
        /// <returns>A string (never null).</returns>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string ToSingleString(this IEnumerable<string> textEnumerable, string separator,
                                            bool ignoreNullOrEmpty)
        {
            //SKY: 2009-08-17: Added validation.
            separator.ValidateIsNotDefault("separator");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            string result;

            if (textEnumerable == null)
            {
                result = string.Empty;
            }
            else
            {
                //SKY: 2009-08-17: redid in order to use StringBuilder, and drop reliance on Count() method.
                StringBuilder stringBuilder = new StringBuilder();

                foreach (string element in textEnumerable)
                {
                    if (ignoreNullOrEmpty && string.IsNullOrEmpty(element))
                    {
                        continue;
                    }

                    stringBuilder.Append(element);
                    stringBuilder.Append(separator);
                }
                //Remove final separator, but only if something was actually added:
                if (stringBuilder.Length > 0)
                {
                    stringBuilder.Remove(stringBuilder.Length - separator.Length, separator.Length);
                }
                result = stringBuilder.ToString();
            }
            return result;
        }

        /// <summary>
        /// Replaces multiple tokens.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="replacement">The replacement.</param>
        /// <param name="findMe">The find me.</param>
        /// <returns></returns>
        public static string ReplaceMultipleTokens(this string text, string replacement, params string[] findMe)
        {
            foreach (string token in findMe)
            {
                text = text.Replace(token, replacement);
            }
            return text;
        }

        /// <summary>
        /// Replaces the specified string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        /// <param name="comparison">The comparison.</param>
        /// <returns></returns>
        public static string Replace(this string str, string oldValue, string newValue, StringComparison comparison)
        {
            StringBuilder sb = new StringBuilder();

            int previousIndex = 0;
            int index = str.IndexOf(oldValue, comparison);
            while (index != -1)
            {
                sb.Append(str.Substring(previousIndex, index - previousIndex));
                sb.Append(newValue);
                index += oldValue.Length;

                previousIndex = index;
                index = str.IndexOf(oldValue, index, comparison);
            }
            sb.Append(str.Substring(previousIndex));

            return sb.ToString();
        }

    /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Replaces all <paramrefe name = "pattern" /> matches in the current string, with 
        ///   the <paramref name = "replacementText" /> string.
        /// </summary>
        /// <param name = "text">The current string.</param>
        /// <param name = "pattern">The regular expression pattern to match.</param>
        /// <param name = "replacementText">The replacement string.</param>
        /// <returns>
        ///   A new string that is identical to this string, 
        ///   except that a replacement string takes the place 
        ///   of each matched string.
        /// </returns>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "text" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "pattern" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "replacementText" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentException">
        ///   Regular expression parsing error.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string RegexReplace(this string text, string pattern, string replacementText)
        {
            return Regex.Replace(text, pattern, replacementText);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Replaces all <paramref name = "pattern" /> matches in the current string, 
        ///   using the <paramref name = "matchEvaluator" />.
        /// </summary>
        /// <param name = "text">The current string.</param>
        /// <param name = "pattern">The regular expression pattern to match.</param>
        /// <param name = "matchEvaluator">
        ///   A custom method that examines each match and returns either 
        ///   the original matched string or a replacement string.
        /// </param>
        /// <returns>
        ///   A new string that is identical to this string, 
        ///   except that a replacement string takes the place 
        ///   of each matched string.
        /// </returns>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "text" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "pattern" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "matchEvaluator" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentException">
        ///   Regular expression parsing error.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string RegexReplace(this string text, string pattern, MatchEvaluator matchEvaluator)
        {
            return Regex.Replace(text, pattern, matchEvaluator);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Replaces all <paramref name = "pattern" /> matches in the current string, with 
        ///   the <paramref name = "replacementText" /> string.
        ///   <para>
        ///     Specified options modify the matching operation.
        ///   </para>
        /// </summary>
        /// <param name = "text">The current string.</param>
        /// <param name = "pattern">The regular expression pattern to match.</param>
        /// <param name = "replacementText">The replacement string.</param>
        /// <param name = "options">
        ///   A bitwise <c>OR</c> combination of 
        ///   <see cref = "System.Text.RegularExpressions.RegexOptions" />
        ///   enumeration values.
        /// </param>
        /// <returns>
        ///   A new string that is identical to this string, 
        ///   except that a replacement string takes the place 
        ///   of each matched string.
        /// </returns>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "text" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "pattern" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "replacementText" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentException">
        ///   Regular expression parsing error.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string RegexReplace(this string text, string pattern, string replacementText,
                                          RegexOptions options)
        {
            return Regex.Replace(text, pattern, replacementText, options);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Replaces all <paramref name = "pattern" /> matches in the current string, with 
        ///   the value specified within <paramref name = "matchEvaluator" />.
        /// </summary>
        /// <param name = "text">The current string.</param>
        /// <param name = "pattern">The regular expression pattern to match.</param>
        /// <param name = "matchEvaluator">
        ///   A custom method that examines each match and returns either 
        ///   the original matched string or a replacement string.
        /// </param>
        /// <param name = "options">
        ///   A bitwise <c>OR</c> combination of 
        ///   <see cref = "System.Text.RegularExpressions.RegexOptions" />
        ///   enumeration values.
        /// </param>
        /// <returns>
        ///   A new string that is identical to this string, 
        ///   except that a replacement string takes the place 
        ///   of each matched string.
        /// </returns>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "text" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "pattern" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentNullException">
        ///   An exception is raised if <paramref name = "matchEvaluator" /> is null.
        /// </exception>
        /// <exception cref = "ArgumentException">
        ///   Regular expression parsing error.
        /// </exception>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string RegexReplace(this string text, string pattern, MatchEvaluator matchEvaluator,
                                          RegexOptions options)
        {
            return Regex.Replace(text, pattern, matchEvaluator, options);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the specified input is a valid URI.
        /// </summary>
        /// <param name = "input">The input.</param>
        /// <returns>
        ///   <c>true</c> if the specified input is URI; otherwise, <c>false</c>.
        /// </returns>
        /// <internal>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static bool IsUri(this string input)
        {
            Uri uri;
            return Uri.TryCreate(input, UriKind.Absolute, out uri);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Fasts the replace.
        /// </summary>
        /// <param name="original">The original.</param>
        /// <param name="pattern">The pattern.</param>
        /// <param name="replacement">The replacement.</param>
        /// <param name="comparisonType">Type of the comparison.</param>
        /// <returns></returns>
        /// <internal>Src: Xero.Common</internal>
        public static string FastReplace(this string original, string pattern, string replacement,
                                         StringComparison comparisonType)
        {
            if (original == null)
            {
                return null;
            }

            if (string.IsNullOrEmpty(pattern))
            {
                return original;
            }

            int lenPattern = pattern.Length;
            int idxPattern = -1;
            int idxLast = 0;

            StringBuilder result = new StringBuilder();

            while (true)
            {
                idxPattern = original.IndexOf(pattern, idxPattern + 1, comparisonType);

                if (idxPattern < 0)
                {
                    result.Append(original, idxLast, original.Length - idxLast);
                    break;
                }

                result.Append(original, idxLast, idxPattern - idxLast);
                result.Append(replacement);

                idxLast = idxPattern + lenPattern;
            }

            return result.ToString();
        }

        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Trims the current string,
        /// without raising an exception if the value is <c>null</c>.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="returnNullIfSourceIsNull">if set to <c>true</c> [return null if source is null].</param>
        /// <returns>
        /// A trimmed string, or <c>null</c>.
        /// </returns>
        /// <internal>Src: Xero.Common</internal>
        public static string SafeTrim(this string value, bool returnNullIfSourceIsNull = true)
        {
            return value == null ? ((returnNullIfSourceIsNull) ? null : string.Empty) : value.Trim();
        }


        public static string SafeReplace(this string value, string oldValue, string newValue, bool returnNullIfSourceIsNull = true)
        {
            return value == null ? ((returnNullIfSourceIsNull) ? null : string.Empty) : value.Replace(oldValue, newValue);
        }


        /// <summary>
        /// Splits a string without Exception - returns string[0] if source is null or empty.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="stringSplitOptions">The string split options.</param>
        /// <param name="dividers">The dividers.</param>
        /// <returns></returns>
        public static string[] SafeSplit(this string value, StringSplitOptions stringSplitOptions= StringSplitOptions.None, params char[] dividers)
        {
            return (value.IsNullOrEmpty()) ? new string[0] : value.Split(dividers,stringSplitOptions);
        }


        /// <summary>
        /// Splits a string without Exception - returns string[0] if source is null or empty.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="stringSplitOptions">The string split options.</param>
        /// <param name="dividers">The dividers.</param>
        /// <returns></returns>
        public static string[] SafeSplit(this string value, StringSplitOptions stringSplitOptions= StringSplitOptions.None, params string[] dividers)
        {
            return (value.IsNullOrEmpty()) ? new string[0] : value.Split(dividers,stringSplitOptions);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Formats the given string (originally intended for an UserGroup Display name)
        ///   so that if more than <paramref name = "maxLength" /> characters long, 
        ///   it displays <paramref name = "maxLength" /> characters and 
        ///   '...', rather than the full text.
        /// </summary>
        /// <param name = "text">The text to format.</param>
        /// ///
        /// <param name = "maxLength">Length to concatenate the text to.</param>
        /// <param name = "useSingleChar"></param>
        /// <returns>A truncated string.</returns>
        /// <internal>
        ///   <para>
        ///     TODO: Consider refactoring method out to a common infrastructure library...
        ///   </para>
        ///   <para>
        ///     TODO: The logic is flawed: it needs to Remove(maxLength - 3)
        ///     after checking that maxLength>=3.
        ///   </para>
        ///   <para>
        ///     Src: Xero.Common
        ///   </para>
        /// </internal>
        public static string Truncate(this string text, int maxLength, bool useSingleChar)
        {
            if (text == null)
            {
                return string.Empty;
            }

            int removeChars = (useSingleChar) ? 1 : 3;

            maxLength = maxLength - removeChars;

            if (text.Length > (maxLength))
            {
                maxLength = (Math.Max(0,maxLength));

                text = text.Remove(maxLength) + ((useSingleChar) ? "…" : "...");
            }
            return text;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Joins the specified values.
        /// </summary>
        /// <param name = "values">The values.</param>
        /// <param name = "joinText">The join text.</param>
        /// <returns></returns>
        public static string Join(this string[] values, string joinText)
        {
            values.ValidateIsNotDefault("values");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
Contract.EndContractBlock();
#endif


            if (values.Length == 0)
            {
                return string.Empty;
            }

            return string.Join(joinText, values);

            //StringBuilder result = new StringBuilder();
            //result.Append(values[0]);

            //for (int i = 1; i < values.Length; i++)
            //{
            //    result.Append(joinText);
            //    result.Append(values[i]);
            //}

            //return result.ToString();
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   replacement for String.Format
        /// </summary>
        public static string With(this string format, params object[] args)
        {
            return format.FormatStringInvariantCulture(args);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// <para>
        /// Turns "Home_Var" to "Home Var"
        /// </para>
        /// </para>
        ///   prettily renders property names
        /// </summary>
        /// <param name = "text"></param>
        /// <returns></returns>
        public static string PrettyCase(this string text)
        {
            return DeCamelCase(text).Replace("_", " ");
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   turns "HelloWorld" into "Hello World"
        /// </summary>
        /// <param name = "text"></param>
        /// <returns></returns>
        public static string DeCamelCase(this string text)
        {
            return Regex.Replace(text, @"([A-Z])", @" $&").Trim();
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts a string to Pretty/Title Case, but more effectively (for western languages atleast) then
        ///   the one provided by the Framework.
        /// </summary>
        /// <param name = "text">String to PrettyCase</param>
        /// <param name = "separationChars">Chars between words (space, commas, dashes, etc.)</param>
        /// <returns>PrettyCased result string</returns>
        /// <remarks>
        ///   TODO: Although much more flexible and complete than the built in Framework method, there is still
        ///   one case scenario that needs to be addressed at some point:
        ///   stuff like 'xact' -> 'XAct', instead of 'Xact'...
        /// </remarks>
        public static string PCase(string text, string separationChars)
        {
            if (text.IsNullOrEmpty())
            {
                return text;
            }
            if (separationChars.IsNullOrEmpty())
            {
                return separationChars;
            }

            string tResult = string.Empty;
            separationChars = separationChars.ToUpper(/*_invariantCulture*/);
            if (separationChars.IsNullOrEmpty())
            {
                separationChars =
                    //" _,;:.\\/"
                    " -_@+{}[]'\"\\/`~\t";
            }

            int iMax = text.Length;
            bool tFlag = true;
            string singleChar /*= string.Empty*/;

            for (int i = 0; i < iMax; i++)
            {
                singleChar = text.Substring(i, 1);
                if (tFlag)
                {
                    singleChar = singleChar.ToUpper(/*_invariantCulture*/);
                    tFlag = false;
                }
                else
                {
                    singleChar = singleChar.ToLower(/*_invariantCulture*/);
                }
                if (separationChars.IndexOf(singleChar, StringComparison.OrdinalIgnoreCase) > -1)
                {
                    tFlag = true;
                }
                tResult += singleChar;
            }
            return tResult;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns the position of the Xth Occurance of a char in a string.
        /// </summary>
        /// <param name = "text">String to analyse.</param>
        /// <param name = "occurence">Find the [n]th occurance of the character.</param>
        /// <param name = "singleChar">Character to find.</param>
        /// <returns>Position of character, or -1 if not found.</returns>
        public static int FindPositionOfNthChar(this string text, char singleChar, int occurence)
        {
            if (string.IsNullOrEmpty(text))
            {
                return -1;
            }

            //initialize

            //find the xth occurance
            int r = 0;
            int count = 0;
            foreach (char c in text)
            {
                if (c == singleChar)
                {
                    r++;
                    //if this is the xth character occurance, then exit now:
                    if (r == occurence)
                    {
                        return count;
                    }
                }
                count++;
            }
            //Didn't find enough counts -- which is same as 'not found':
            return -1;
        }


//        /// <summary>
        ///// <para>
        ///// An XActLib Extension.
        ///// </para>
        ////        ///   Similar to string.Format, but works with Dictionary Keys instead of numerical positions of arguments.
//        /// </summary>
//        /// <param name = "source">The source.</param>
//        /// <param name = "dictionary">The dictionary.</param>
//        /// <param name = "throwErrorIfRemainingBrackets">Raise an error if not all param brackets in given string were proccessed.</param>
//        /// <param name = "args">Optional args to string.Format the *processed* string (ie, a second step).</param>
//        /// <returns>The formatted string.</returns>
//        public static string FormatStringEx(this string source, StringDictionary dictionary,
//                                            bool throwErrorIfRemainingBrackets, params object[] args)
//        {
//            string result = source;

//            /*
//      string result = 
//      (paramPlaceHolder == "?")
//?
//        //Replace all '@myVar' with '?':
//        //NB: only looks for vars that look like '@camelCase':
//System.Text.RegularExpressions.Regex.Replace(source, "([^@])@([a-z]+[A-Z]([a-zA-Z_]){1,})", "$1" + paramPlaceHolder)
//:
//        //Replace all '@myVar' with '%myVar':
//        //NB: only looks for vars that look like '@camelCase':
//System.Text.RegularExpressions.Regex.Replace(source, "([^@])@([a-z]+[A-Z]([a-zA-Z_]){1,})", "$1" + paramPlaceHolder + "$2");
//      */

//            foreach (string key in dictionary.Keys)
//            {
//                string val = Convert.ToString(dictionary[key]);
//                //Case insensitive scan:
//                result = Regex.Replace(result, "{" + key + "}", val,
//                                       RegexOptions.
//                                           IgnoreCase);
//            }

//            //Proccess additional args:
//            if (args.Length > 0)
//            {
//                result = string.Format(result, args);
//            }


//            if (throwErrorIfRemainingBrackets)
//            {
//                //Check to see that there are no, or only brackets that work for string.Format(...):
//                if (Regex.Match(result, "{[^0-9]+}").Success)
//                {
//                    throw new ArgumentException(":;Check your Sql Keywords: " + result);
//                }
//            }

//            return result;
//        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Finds keywords (in the format of "{xxx}", similar to
        /// the format used by string.Format, but text based, rather than number based)
        /// within the given string.
        /// <para>
        /// Returns the keyword, without the enclosing brackets.
        /// </para>
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="extended">if set to <c>true</c> [extended].</param>
        /// <returns>
        /// An enumerable list of keywords found in the given string.
        /// </returns>
        public static IEnumerable<string> FindLabeledPlaceHolders(this string source, bool extended=false)
        {
            string pattern = (extended) ? "[{#]([A-Za-z_][A-Za-z0-9_]+)[}#]" : "{([A-Za-z][A-Za-z0-9_]+)}";

            return from Match match in (Regex.Matches(
                source,
                pattern,
                RegexOptions.Singleline))
                   select match.Groups[1].Value;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether [has key words] [the specified source].
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns>
        ///   <c>true</c> if [has key words] [the specified source]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasLabeledPlaceHolders(this string source)
        {
            //Check to see that there are no {text}, 
            //Does not trigger if {number} (as needed by 
            //normal string.Format(...)):
            return Regex.Match(source, "{[^0-9]+}").Success;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Similar to string.Format, but works with 
        ///   Dictionary Keys instead of numerical positions of arguments.
        ///   <para>
        ///     See <see cref = "FindLabeledPlaceHolders" />.
        ///   </para>
        /// </summary>
        /// <param name = "source">The source string to replace.</param>
        /// <param name = "dictionary">
        ///   The dictionary of string key/values 
        ///   to search for and replace.</param>
        /// <param name = "args">
        ///   Optional arguments to be replaced 
        ///   *after* doing the first replacements.</param>
        /// <param name = "paramPlaceHolder">
        ///   The SqlParameter prefix (eg: '@')</param>
        /// <returns>The formatted string.</returns>
        public static string FormatStringEx(
            this string source,
            Dictionary<string,string> dictionary,
            string paramPlaceHolder,
            params object[] args)
        {
            if (dictionary == null)
            {
                return source;
            }
            if (dictionary.Count == 0)
            {
                return source;
            }

            //string paramPlaceHolder = 
            // "@";//GenericDbProviderSchema.dbParamPlaceHolderChar;

            if (string.IsNullOrEmpty(paramPlaceHolder))
            {
                paramPlaceHolder = "?"; //Default to ODBC format.
            }

            //Version 3:
            string result = (paramPlaceHolder == "?")
                                ? //Replace all '@myVar' with '?':
                            //NB: only looks for vars that look like '@camelCase':
                            Regex.Replace(
                                source,
                                "([^@])@([a-zA-Z][a-zA-Z0-9_]+)", "$1" + paramPlaceHolder)
                                : //Replace all '@myVar' with '$myVar' (assuming char='$':

                            //NB: only looks for vars that look like '@camelCase'
                            //and will miss var if it is not (ie, @UserName):
                            //*Doesn't start with two @ ('@@')
                            //*Doesn't start with _
                            //Note that you have to double up (escape) '$' characters 
                            //(http://msdn2.microsoft.com/en-us/library/ewy2t5e0.aspx)
                            Regex.Replace(
                                source,
                                "([^@])@([a-zA-Z][a-zA-Z0-9_]+)",
                                "$1" +
                                (paramPlaceHolder == "$" ? "$$" : paramPlaceHolder) +
                                "$2");


            foreach (string key in dictionary.Keys)
            {
                string val = dictionary[key];
                //Case insensitive scan:
                result = Regex.Replace(
                    result,
                    "{" + key + "}",
                    val,
                    RegexOptions.IgnoreCase);
            }


            if (args.Length > 0)
            {
                result = result.FormatStringInvariantCulture(args);
            }

            //Check to see that there are no, or
            //only brackets that work for string.Format(...):
            Match m =
                Regex.Match(result, "{[^0-9]+}");

            if (m.Success)
            {
                throw new ArgumentException(
                    "::Check your Sql Keywords...(hint: '{0}')." +
                    " Not everything was parsed correctly: '{1}'".FormatStringExceptionCulture(
                        m.Value, result));
            }


            return result;
        }


        /*
    static private Regex rx = new Regex("{([a-zA-Z]+)}", RegexOptions.Compiled);


    /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
    /// as String.Format but use {identifier} + dictionary<id, value>
    /// instead of positional parameters
    /// </summary>
    /// <param name="source"></param>
    /// <param name="dic"></param>
    /// <returns></returns>
    public static string FormatStringEx(string source, System.Collections.Specialized.StringDictionary dic, params object[] args) {

#warning todo: use the try catch around the method and try to get the Key not found from the exception
        
      return rx.Replace(source,
                        delegate(Match m) {
                          try {
                            return dic[m.Groups[1].Value];
                          } catch (KeyNotFoundException e) {
                            throw new FormatStringExException(m.Groups[1].Value, e);
                          }
                        });
    }
     */


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Reverses order of characters back to front.
        /// </summary>
        /// <param name = "text">String to Reverse</param>
        /// <param name = "chunkSize">Size of character sections. Default is 1.</param>
        /// <returns>string</returns>
        /// <remarks>
        ///   Never figured out what to use this function for. But voila.
        /// </remarks>
        public static string Reverse(this string text, int chunkSize)
        {
            if (text == null)
            {
                return string.Empty;
            }
            if (chunkSize == 0)
            {
                chunkSize = 1;
            }

            string tResult = "";
            int i;
            int iMax = text.Length;
            for (i = iMax - chunkSize; i > -1; i = i - chunkSize)
            {
                //int tBite = (i - chunkSize) > 0 ? chunkSize : i;
                tResult += text.Substring(i, 1);
            }
            return tResult;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Split function that only splits if not within brackets or quotes.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "divChar"></param>
        /// <returns></returns>
        /// <remarks>
        ///   <para>
        ///     Currently only handles ({['" as start chars and "']}) as end chars for non-splits.
        ///   </para>
        ///   <para>
        ///     It is not confused by escaped charaters -- but it cannot handle """" (VB Style) quoting -- only Linux \" style escaping.
        ///   </para>
        /// </remarks>
        /// <example>
        ///   <code>
        ///     string[] tResult = SplitPlus ("This is a \"very dumb\" cat", " ");
        ///     //tResult.Length will be 5, not 6...
        ///   </code>
        /// </example>
        public static string[] SplitEx(this string text, string divChar)
        {
            if (text.IsNullOrEmpty())
            {
                return new[] {text};
            }

            if (divChar.IsNullOrEmpty())
            {
                divChar = ",";
            }


            List<string> results = new List<string>();
            string tWord = "";
            bool escaped = false;
// ReSharper disable TooWideLocalVariableScope
// ReSharper disable RedundantAssignment
            string lastChar = "";
// ReSharper restore RedundantAssignment
// ReSharper restore TooWideLocalVariableScope
            Stack<string> quotes = new Stack<string>();

            for (int i = 0; i < text.Length; i++)
            {
                string singleChar = text[i].ToString();

                if (quotes.Count == 0)
                {
                    //We are outside of quotes, so look for quote beginnings...
                    if ((singleChar == "(") ||
                        (singleChar == "{") ||
                        (singleChar == "[") ||
                        (singleChar == "'") ||
                        (singleChar == "\""))
                    {
                        quotes.Push(singleChar);
// ReSharper disable RedundantAssignment
                        lastChar = singleChar;
// ReSharper restore RedundantAssignment
                    }
                    if ((singleChar == divChar))
                    {
                        results.Add(tWord);
                        tWord = "";
                        singleChar = "";
                    }
                }
                else
                {
                    //We are within quotes...need to look for close chars:
                    if (escaped == false)
                    {
                        if (singleChar == "\\")
                        {
                            escaped = true;
                        }
                        else
                        {
                            lastChar = (string) quotes.Peek();

                            if ((singleChar == "\"") && (singleChar == lastChar))
                            {
                                quotes.Pop();
                            }
                            else if ((singleChar == "\'") && (singleChar == lastChar))
                            {
                                quotes.Pop();
                            }
                            else if ((singleChar == "]") && (lastChar == "["))
                            {
                                quotes.Pop();
                            }
                            else if ((singleChar == "}") && (lastChar == "{"))
                            {
                                quotes.Pop();
                            }

                            if ((singleChar == ")") && (lastChar == "("))
                            {
                                quotes.Pop();
                            }
                        }
                    }
                    else
                    {
                        escaped = false;
                    }
                }
                tWord = tWord + singleChar;
            }
            if (!tWord.IsNullOrEmpty())
            {
                results.Add(tWord);
            }
            return results.ToArray();
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Strips unwanted characters from a string.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "charactersToStrip"></param>
        /// <param name = "caseSensitive"></param>
        /// <returns></returns>
        public static string Strip(string text, string charactersToStrip, bool caseSensitive)
        {
            if (text.IsNullOrEmpty())
            {
                return text;
            }
            if (charactersToStrip.IsNullOrEmpty())
            {
                return text;
            }

            try
            {
                int i;
                int iMax = text.Length;
                int j;
                int jMax = charactersToStrip.Length;
                string singleChar;
                string tResult = "";

                if (caseSensitive)
                {
                    for (i = 0; i < iMax; i++)
                    {
                        singleChar = text.Substring(i, 1);

                        for (j = 0; j < jMax; j++)
                        {
// ReSharper disable InvertIf
                            if (singleChar == charactersToStrip.Substring(j, 1))
// ReSharper restore InvertIf
                            {
                                singleChar = "";
                                break;
                            }
                        }
                        tResult += singleChar;
                    }
                }
                else
                {
                    for (i = 0; i < iMax; i++)
                    {
                        singleChar = text.Substring(i, 1);

                        for (j = 0; j < jMax; j++)
                        {
// ReSharper disable InvertIf
                            if (singleChar.ToUpper(/*_invariantCulture*/) ==
                                charactersToStrip.Substring(j, 1).ToUpper(/*_invariantCulture*/))
// ReSharper restore InvertIf
                            {
                                singleChar = "";
                                break;
                            }
                        }
                        tResult += singleChar;
                    }
                }
                return tResult;
            }
            catch (Exception exception)
            {
                throw new ArgumentException("Could not Strip string.", exception);
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Splits the string and returns the last part.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="splitChar">The split char.</param>
        /// <returns></returns>
        public static string LastPart(this string value, char splitChar)
        {
            value.ValidateIsNotDefault("value");

            string[] strParts = value.Split(new[] {splitChar});
            return (strParts.Length == 0) ? value : strParts[strParts.Length - 1];
        }


        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// A SubString method that doesn't throw an exception if the string is too small for the given params.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="start">The start.</param>
        /// <param name="length">The length.</param>
        /// <param name="returnNullIfSourceIsNull">if set to <c>true</c> [return null if source is null].</param>
        /// <returns></returns>
        /// <internal>6/14/2011: Sky</internal>
        public static string SafeSubstring(this string text, int start, int length, bool returnNullIfSourceIsNull=false)
        {
            if (string.IsNullOrEmpty(text))
            {
                return ((returnNullIfSourceIsNull) ? null : string.Empty);
            }

            if (start > text.Length)
            {
                return ((returnNullIfSourceIsNull) ? null : string.Empty);
            }

            int maxLength = (text.Length - start);


            return text.Substring(start, Math.Min(maxLength, length));
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Convert an integer to Int32;
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/11/2011: Sky</para></internal>
        public static int ToInt32(this string text)
        {
            return Convert.ToInt32(text);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Toes the int16.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/11/2011: Sky</para></internal>
        public static int ToInt16(this string text)
        {
            return Convert.ToInt16(text);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Returns the length of a string, without faililng if the string was null (returns 0 in that case).
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/17/2011: Sky</para></internal>
        public static int LengthSafe(this string text)
        {
            return (text == null) ? 0 : text.Length;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Get the parts of the path.
        /// <para>
        /// For example, given 'abc/sub/sub' returns
        /// 'abc/sub/sub', 'abc/sub' and 'abc'
        /// </para>
        /// </summary>
        /// <param name="text">Name of the role.</param>
        /// <param name="divChar">The div char.</param>
        /// <returns></returns>
        /// <internal><para>8/14/2011: Sky</para></internal>
        public static string[] SplitPath(this string text, char divChar)
        {
            //abc/sub/s
            //0123456789
            List<string> results = new List<string>();
            int pos = text.Length;
            string part = text + divChar;
            while (pos != -1)
            {
                part = part.Substring(0, pos);
                results.Add(part);
                pos = part.LastIndexOf(divChar);
            }
            return results.ToArray();
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Checks that the given name does not end with a slash...which would escape the next char, which is the closing quote.
        /// </summary>
        /// <remarks>
        /// <para>
        /// If a final slash is found, counts back the number of slashes. If the count is odd, it has to add a slash
        /// before returning it so that closing quotes will not be escaped.
        /// </para>
        /// </remarks>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static string EnsureLastSlashIsNotEscapeChar(this string fileName)
        {
            fileName = fileName.Trim();
            if (fileName.EndsWith("\\"))
            {
                int i = fileName.Length - 1;
                int iFound = 0;
                while (fileName.Substring(i) == "\\")
                {
                    iFound++;
                    i--;
                }

                if ((iFound & 1) == 1)
                {
                    //Is Odd:
                    fileName += "\\";
                }
            }

            //Now ok to quote:
            //return "\"" + fileName + "\"";
            return fileName;
        }



        /// <summary>
        /// Returns the first part of the string.
        /// <para>
        /// <example>
        /// <![CDATA[
        /// var domain = @"JQDEV\SkyS".First("\\");
        /// ]]>
        /// </example>
        /// </para>
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="divChar">The div char.</param>
        /// <returns></returns>
        public static string First(this string text, string divChar)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            string[] parts = text.Split(new string[] { divChar }, StringSplitOptions.None);

            return parts[0];
        }


        /// <summary>
        /// Returns the last part of the string.
        /// <para>
        /// <example>
        /// <![CDATA[
        /// var userName = @"JQDEV\SkyS".Last("\\");
        /// ]]>
        /// </example>
        /// </para>
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="divChar">The div char.</param>
        /// <returns></returns>
        public static string Last(this string text, string divChar)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            string[] parts = text.Split(new[] { divChar }, StringSplitOptions.RemoveEmptyEntries);

            return parts[parts.Length - 1];
        }




        /// <summary>
        /// Makes the string safe enough for a filename (without extension).
        /// <para>
        /// Takes "John's letter! Really..." and returns "John_s_letter__Really"
        /// </para>
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="replaceChar">The replace char.</param>
        /// <param name="spaceReplace">The space replace.</param>
        /// <param name="dotReplace">The dot replace.</param>
        /// <returns></returns>
        public static string MakeSafeForForFilename(this string text, string replaceChar="_", string spaceReplace="_", string dotReplace="")
        {
            //Strip out 
            const string pattern = @"[^A-Za-z0-9._\- ]";
            text = Regex.Replace(text, pattern, replaceChar);

            //Has spaces and dots
            if (spaceReplace != null)
            {
                text = text.Replace(" ", spaceReplace);
            }
            if (dotReplace !=null)
            {
                text = text.Replace(".", dotReplace);
            }

            return text;
        }


        /// <summary>
        /// Splits a PrettyCase ('WildGreen') string to separate words ('Wild Green').
        /// </summary>
        /// <param name="pcaseText">The string value.</param>
        /// <returns></returns>
        public static string SplitPCase(this string pcaseText)
        {

            StringBuilder buf = new StringBuilder(pcaseText);

            // assume first letter is upper!

            bool lastWasUpper = true;

            int lastSpaceIndex = -1;

            for (int i = 1; i < buf.Length; i++)
            {

                bool isUpper = char.IsUpper(buf[i]);

                if (isUpper & !lastWasUpper)
                {

                    buf.Insert(i, new char[]{' '});

                    lastSpaceIndex = i;

                }

                if (!isUpper && lastWasUpper)
                {

                    if (lastSpaceIndex != i - 2)
                    {

                        buf.Insert(i - 1,new char[]{' '});

                        lastSpaceIndex = i - 1;

                    }

                }

                lastWasUpper = isUpper;

            }

            return buf.ToString();

        }



        /// <summary>
        /// Splits the CSS delineated string into a dictionary.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caseInSensitive">if set to <c>true</c> the dictionary is insensitive.</param>
        /// <returns></returns>
    public static Dictionary<string, string> SplitIntoKeyValues(this string text, bool caseInSensitive=false)
    {
        Dictionary<string, string> results = 
            (caseInSensitive)?new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase) : new Dictionary<string, string>();
        
        foreach (string set in text.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries))
        {
            string[] parts = set.Split(new[] {':'});

            results[parts[0]] = parts[1];
        }

        return results;
    }



    /// <summary>
    /// Repeats the given text a specific number of times.
    /// <para>
    /// eg:, given '\t', can be used to get back '\t\t\t'
    /// </para>
    /// </summary>
    /// <param name="textToRepeat">The text to repeat.</param>
    /// <param name="numberOfTimesToRepeat">The number of times to repeat.</param>
    /// <returns></returns>
        public static string Repeat(this string textToRepeat, int numberOfTimesToRepeat)
        {
            return string.Concat(Enumerable.Repeat(textToRepeat, numberOfTimesToRepeat));
        }

        //public static string ToTitleCase(this string text, CultureInfo cultureInfo)
        //{
        //    string language = "war and peace";
        //    TextInfo textInfo = new CultureInfo("en-US").TextInfo;
        //    language = textInfo.ToTitleCase(language);
        //    //War And Peace
        //}



        /// <summary>
        /// Converts "A silly example. Really it is." to "ASillyExample.ReallyItIs.";
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string ToCamelCase(this string text)
        {
            return text.ToTitleCase(false);
        }

        /// <summary>
        /// Converts "A silly example. Really it is." to "A Silly Example. Really It Is.";
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="keepSpaces">if set to <c>true</c> [keep spaces].</param>
        /// <returns></returns>
        public static string ToTitleCase(this string text , bool keepSpaces=true)
        {
            StringBuilder result = new StringBuilder();
            bool newWord = true;
            foreach (char c in text)
            {
                if (newWord)
                {
                    result.Append(Char.ToUpper(c));
                    newWord = false;
                    continue;
                }
                if (c == ' ')
                {
                    newWord = true;
                    if (!keepSpaces)
                    {
                        continue;
                    }
                }
                result.Append(Char.ToLower(c));
            }
            return result.ToString();
        }



        public static T DeserializeFromDataContractSerializedString<T>(this string xml)
        {
            return (T)xml.DeserializeFromDataContractSerializedString(typeof(T));
        }

        public static object DeserializeFromDataContractSerializedString(this string xml, Type toType)
        {
            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                //XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(memoryStream, Encoding.UTF8, new XmlDictionaryReaderQuotas(), null);

                using (XmlReader xmlReader = XmlReader.Create(memoryStream))
                {

                    DataContractSerializer serializer = new DataContractSerializer(toType);
                    return serializer.ReadObject(xmlReader);
                }
            }
        }



        public static T DeserialiseFromJSON<T>(this string json, Encoding encoding)
        {
            return (T)json.DeserialiseFromJSON(typeof(T), encoding);
        }


        public static object DeserialiseFromJSON(this string json, Type type, Encoding encoding)
        {
            object obj;
            try
            {
                obj = XAct.DependencyResolver.Current.GetInstance(type);
            }
            catch
            {
                obj = System.Activator.CreateInstance(type);
            }

            using (var memoryStream = new MemoryStream(encoding.GetBytes(json)))
            {
                var serializer = new DataContractJsonSerializer(obj.GetType());

                obj = serializer.ReadObject(memoryStream);

                return obj;
            }

        }


        public static string ToSentenceCase(this string text)
        {
            if (text.IsNullOrEmpty())
            {
                return text;
            }

            // start by converting entire string to lower case
            string lowerCase = text.ToLower().Trim();
            // matches the first sentence of a string, as well as subsequent sentences
            Regex r = new Regex(@"(^[a-z])|\.\s+(.)", RegexOptions.ExplicitCapture);
            // MatchEvaluator delegate defines replacement of setence starts to uppercase
            string result = r.Replace(lowerCase, s => s.Value.ToUpper());

            return result;
        }
    }






#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
