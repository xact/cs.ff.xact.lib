﻿namespace XAct
{
    using XAct.Messages;

    /// <summary>
    /// 
    /// </summary>
    public static class QuerySearchTermExtensions
    {
        /// <summary>
        /// Parses the given text into the <see cref="QuerySearchTerm"/>'s Value property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="querySearchTerm">The query search term.</param>
        /// <param name="defaultValue">The default value.</param>
        public static void TryParse<T>(this IQuerySearchTerm querySearchTerm, T defaultValue)
        {
            querySearchTerm.Value = querySearchTerm.Text.TryParse<T>(defaultValue);
        }

        /// <summary>
        /// Converts the Text property into the <see cref="QuerySearchTerm"/>'s Value property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="querySearchTerm">The query search term.</param>
        public static void ConvertTo<T>(this IQuerySearchTerm querySearchTerm)
        {
            querySearchTerm.Value = querySearchTerm.Text.ConvertTo<T>();
        }
    }
}
