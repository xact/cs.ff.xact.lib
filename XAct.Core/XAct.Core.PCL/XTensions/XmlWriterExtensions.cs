﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Xml;
    using System.Xml.Serialization;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension Methods to the Xml object.
    /// </summary>
    public static class XmlWriterExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Serialize the object to the given XmlWriter.
        /// </summary>
        /// <param name = "writer">The textWriter to write to.</param>
        /// <param name = "objectToSerialize">The object to serialize.</param>
        //[SuppressMessage("Microsoft.Naming", "CA1720", Justification = "objectToSerialize is more descriptive than 'value'.")]
        public static void Serialize(this XmlWriter writer, object objectToSerialize)
        {
            objectToSerialize.ValidateIsNotDefault("objectToSerialize");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            XmlSerializer xmlSerializer = new XmlSerializer(objectToSerialize.GetType());

            xmlSerializer.Serialize(writer, objectToSerialize);
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
