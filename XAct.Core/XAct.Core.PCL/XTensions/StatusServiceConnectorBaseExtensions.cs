﻿

namespace XAct
{
    using XAct.Diagnostics.Status.Connectors.Implementations;

    public static class StatusServiceConnectorBaseExtensions
    {
        public static void ConfigureBasicInformation(this StatusServiceConnectorBase statusServiceConnector, string name, string title, string description)
        {
            statusServiceConnector.Name = name;
            statusServiceConnector.Title = title;
            statusServiceConnector.Description = description;
        }

    }
}
