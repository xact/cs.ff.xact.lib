﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
using TraceLevel = XAct.Diagnostics.TraceLevel;

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Text;
    using XAct.Diagnostics;

// ReSharper restore CheckNamespace
#endif


    /// <summary>
    ///   Extension Methods to the IDictionary Type.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    // ReSharper disable InconsistentNaming
    public static class IDictionaryExtensions
// ReSharper restore InconsistentNaming
    {
        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// </summary>
        /// <param name = "dictionary">The dictionary.</param>
        /// <param name = "tag">The tag to search for.</param>
        /// <param name = "caseInsensitive">if set to <c>true</c> is case insensitive.</param>
        /// <param name = "oneAlternateTag">The alternate tag.</param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Performance", "CA1800", Justification = "Ok code.")]
        public static object GetValue(this IDictionary dictionary, object tag, bool caseInsensitive,
                                      params object[] oneAlternateTag)
        {
            dictionary.ValidateIsNotDefault("dictionary");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            object tResult = null;
            object qAlternateTag = (oneAlternateTag.Length != 0) ? oneAlternateTag[0] : null;
            if (caseInsensitive)
            {
                tag = ((string) tag).ToUpper();
                qAlternateTag = (((string) qAlternateTag) != String.Empty)
// ReSharper disable PossibleNullReferenceException
                                    ? ((string) qAlternateTag).ToUpper()
// ReSharper restore PossibleNullReferenceException
                                    : "<not_set>";

                foreach (DictionaryEntry tEntry in dictionary)
                {
                    string tTest = ((string) tEntry.Key).ToUpper();
                    if ((tTest != tag.ToString()) && (tTest != qAlternateTag.ToString()))
                    {
                        continue;
                    }
                    //Found
                    tResult = tEntry.Value;
                    break;
                }
            }
            else
            {
                tResult = dictionary[tag];
                if ((tResult == null) && (qAlternateTag != null))
                {
                    tResult = dictionary[qAlternateTag];
                }
            }
            return tResult;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Clones values within source Dictionary to the target dictionary.
        /// </summary>
        /// <typeparam name = "TKey">The type of the Dictionary key.</typeparam>
        /// <typeparam name = "TValue">The Dictionary key value.</typeparam>
        /// <param name = "source">The source dirctionary.</param>
        /// <param name = "target">The target dictiontary.</param>
        public static void Clone<TKey, TValue>(this IDictionary<TKey, TValue> source, IDictionary<TKey, TValue> target)
        {
            source.ValidateIsNotDefault("source");
            target.ValidateIsNotDefault("target");

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            target.Clear();

            foreach (KeyValuePair<TKey, TValue> pair in source)
            {
                target[pair.Key] = pair.Value;
            }
        }

        /// <summary>
        /// Traces the specified dictionary key/values to the current <see cref="ITracingService"/>.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="traceLevel">The trace level.</param>
        public static void Trace(this IDictionary dictionary, TraceLevel traceLevel =TraceLevel.Verbose)
        {
            
            ITracingService tracingService = DependencyResolver.Current.GetInstance<ITracingService>();

            IDictionaryEnumerator dictionaryEnumerator = dictionary.GetEnumerator();
            while (dictionaryEnumerator.MoveNext())
            {
                tracingService.Trace(traceLevel, "{0}='{1}'",dictionaryEnumerator.Key,dictionaryEnumerator.Value);
            }
        }

        /// <summary>
        /// Joins the key Values of the dictionary into a string that can be rendered.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="separator">The separator.</param>
        /// <param name="separator2">The separator2.</param>
        /// <returns></returns>
        public static string JoinSafely(this IDictionary dictionary, string separator=";", string separator2=":")
        {
            if (separator == null)
            {
                separator = ":";
            }
            if (separator2 == null)
            {
                separator2 = ";";
            }

            System.Text.StringBuilder stringBuilder = new StringBuilder();

            IDictionaryEnumerator dictionaryEnumerator = dictionary.GetEnumerator();
            while (dictionaryEnumerator.MoveNext())
            {
                stringBuilder.Append(separator);
                stringBuilder.Append(dictionaryEnumerator.Key);
                stringBuilder.Append(separator2);
                stringBuilder.Append(dictionaryEnumerator.Value);
            }
            if (stringBuilder.Length>0)
            {
                stringBuilder.Remove(0, 1);
            }
            return stringBuilder.ToString();
        }


        public static bool TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, out TValue result,
                                                     params TKey[] keys)
        {

            foreach (TKey key in keys)
            {
                if (dictionary.TryGetValue(key, out result))
                {
                    return true;
                }
            }

            result = default(TValue);
            return false;
        }

    }


//using System;
//using System.Collections.Generic;
//using System.Text;



//    /// <summary>
//    /// Extension Methods for
//    /// generic dictionaries that implement
//    /// the 
//    /// <see cref="IDictionary{TKey,TValue}"/>
//    /// interface.
//    /// </summary>
//    public static class DictionaryExtensions
//    {
//        /// <summary>
//        /// Gets the value in the dictionary, 
//        /// or if the key does not exist, returns
//        /// the generic default value (eg: a null, Guid.Empty, etc.)
//        /// rather than raising an exception 
//        /// (as is the default behavior 
//        /// when using a dictionary's indexer).
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Usage example:
//        /// <code>
//        /// <![CDATA[
//        /// Dictionary<string,string> data = new Dictionary<string,string>();
//        /// data.Add("first","John");
//        /// Console.WriteLine(data.GetValue("first"));//"John"
//        /// Console.WriteLine(data.GetValue("last"));//null.
//        /// ...
//        /// Dictionary<string,string> data = new Dictionary<string,Guid>();
//        /// data.Add("id",id);
//        /// Console.WriteLine(data.GetValue("id"));//id
//        /// Console.WriteLine(data.GetValue("fk"));//Guid.Empty.
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <typeparam name="TKey">The type of the key.</typeparam>
//        /// <typeparam name="TValue">The type of the value.</typeparam>
//        /// <param name="thisDictionary">The current dictionary.</param>
//        /// <param name="key">The item key.</param>
//        /// <returns>The item value found, or a generic default value (eg: null).</returns>
//        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> thisDictionary, TKey key)
//        {
//#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
//            Contract.EndContractBlock();
//#endif
//            return thisDictionary.ContainsKey(key) ? thisDictionary[key] : default(TValue);
//        }


//        /// <summary>
//        /// Gets the value in the dictionary, 
//        /// or if the key does not exist, returns
//        /// the <param name="defaultValue"/>.
//        /// rather than raising an exception 
//        /// (as is the default behavior 
//        /// when using a dictionary's indexer).
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Usage example:
//        /// <code>
//        /// <![CDATA[
//        /// Dictionary<string,string> data = new Dictionary<string,string>();
//        /// data.Add("first","John");
//        /// Console.WriteLine(data.GetValue("first",""));//"John"
//        /// Console.WriteLine(data.GetValue("last",""));//""
//        /// ...
//        /// Dictionary<string,string> data = new Dictionary<string,Guid>();
//        /// data.Add("id",id);
//        /// Console.WriteLine(data.GetValue("id",Guid.Empty));//id
//        /// Console.WriteLine(data.GetValue("fk",Guid.Empty));//Guid.Empty.
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <typeparam name="TKey">The type of the key.</typeparam>
//        /// <typeparam name="TValue">The type of the value.</typeparam>
//        /// <param name="thisDictionary">The current dictionary.</param>
//        /// <param name="key">The item key.</param>
//        /// <returns>The item value found, or the <paramref name="defaultValue"/>.</returns>
//        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> thisDictionary, TKey key, TValue defaultValue)
//        {
//#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
//            Contract.EndContractBlock();
//#endif
//            return thisDictionary.ContainsKey(key) ? thisDictionary[key] : defaultValue;
//        }


//        /// <summary>
//        /// Gets the value in the dictionary, 
//        /// or if the key does not exist, returns
//        /// a value generated by the 
//        /// <paramref name="createAndAddValue"/>,
//        /// rather than raising an exception 
//        /// (as is the default behavior 
//        /// when using a dictionary's indexer).
//        /// </summary>
//        /// <typeparam name="TKey">The type of the key.</typeparam>
//        /// <typeparam name="TValue">The type of the value.</typeparam>
//        /// <param name="thisDictionary">The current dictionary.</param>
//        /// <param name="key">The item key.</param>
//        /// <param name="createAndAddValue">The create and add value.</param>
//        /// <returns>The item value found, or the generated by the <paramref name="createAndAddValue"/> method.</returns>
//        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> thisDictionary, TKey key, Func<TValue> createAndAddValue)
//        {
//#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
//            Contract.EndContractBlock();
//#endif
//            if (thisDictionary.ContainsKey(key))
//            {
//                return thisDictionary[key];
//            }
//            else
//            {
//                //Instantiate the new value:
//                TValue newValue = createAndAddValue();
//                //Add key:newValue to dictionary:
//                thisDictionary.Add(key, newValue);

//                return newValue;
//            }
//        }
//    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
