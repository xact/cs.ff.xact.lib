﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct
{
    public static class GenericExtensions
    {
        /// <summary>
        /// Determines whether two nullable instances differ in value using comparer function and checking for nullablity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns></returns>
        public static bool IsSameAs<T>(this T? a, T? b, Func<T, T, bool> comparer) where T : struct
        {
            if (a.HasValue != b.HasValue) return false; //one of them is null but the other one not
            if (!a.HasValue) return true; //both are null -> they are the same
            return comparer.Invoke(a.Value, b.Value); //both are not null invoke the comparison
        }

        /// <summary>
        /// Determines whether two instances differ in value using comparer function and checking for nullablity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns></returns>
        public static bool IsSameAs<T>(this T a, T b, Func<T, T, bool> comparer) where T : class
        {
            if ((a == null) != (b == null)) return false; //one of them is null but the other one not
            if (a == null) return true; //both are null -> they are the same
            return comparer.Invoke(a, b); //both are not null invoke the comparison
        }
    }
}
