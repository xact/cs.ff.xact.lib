﻿
namespace XAct
{
    using XAct.Environment;

    /// <summary>
    /// 
    /// </summary>
    public interface IHasMachineIdReadOnly
    {
        /// <summary>
        /// Gets the machine id (a hash built using the <see cref="IEnvironmentService.ApplicationNameHash"/>).
        /// </summary>
        /// <value>The machine id.</value>
        string MachineId { get; }
    }
}
