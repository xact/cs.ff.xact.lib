﻿using System.Collections.Generic;
using System.Linq;
using XAct;

namespace XAct
{
    public static class HasIdListExtensions
    {
        public static IEnumerable<TId> GetIds<TEntity, TId>(this IEnumerable<TEntity> entities)
            where TEntity : IHasId<TId>
        {
            return entities.Select(s => s.Id);
        }

    }
}
