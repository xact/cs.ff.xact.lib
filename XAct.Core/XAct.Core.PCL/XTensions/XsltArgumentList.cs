﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Xml.Xsl;


#if ADD_NAMESPACE_ON_EXTENSIONS 
//See: http://bit.ly/snE0xY
namespace XAct {
#endif



/// <summary>
    /// Extensions to the XsltArgumentList,
    /// which is used to transform Xml streams 
    /// using the XslCompiledTransform
    /// </summary>
    public static class XsltArgumentListExtensions
    {
        /// <summary>
        /// Adds the range of values to the XsltArgumentList.
        /// </summary>
        /// <param name="xsltArgumentList"></param>
        /// <param name="attributesToAdd"></param>
        /// <returns></returns>
        public static void AddRange (this XsltArgumentList xsltArgumentList, IEnumerable<KeyValuePair<string, string>> attributesToAdd)
        {
            foreach (KeyValuePair<string,string> keyValuePair in attributesToAdd)
            {

                xsltArgumentList.AddParam(keyValuePair.Key, string.Empty, keyValuePair.Value);
            }
        }

        /// <summary>
        /// Adds the range of values to the XsltArgumentList.
        /// </summary>
        /// <param name="xsltArgumentList">The XSLT argument list.</param>
        /// <param name="attributesToAdd">The attributes to add.</param>
        /// <returns></returns>
        public static void AddRange(this XsltArgumentList xsltArgumentList, NameValueCollection attributesToAdd)
        {
            foreach(var key in attributesToAdd.AllKeys)
            {
                xsltArgumentList.AddParam(key, string.Empty, attributesToAdd[key]);
            }
        }

    }


#if ADD_NAMESPACE_ON_EXTENSIONS 
//See: http://bit.ly/snE0xY
}
#endif
