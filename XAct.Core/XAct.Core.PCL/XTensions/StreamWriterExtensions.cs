﻿//OK

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Collections;
    using System.IO;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension Methods for the StreamWriter.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class StreamWriterExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Append List to File
        /// </summary>
        /// <param name = "streamOrTextWriter"></param>
        /// <param name = "enumerableList"></param>
        /// <returns></returns>
        public static void WriteLines(TextWriter streamOrTextWriter, IEnumerable enumerableList)
        {
            streamOrTextWriter.ValidateIsNotDefault("streamOrTextWriter");
            enumerableList.ValidateIsNotDefault("enumerableList");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            foreach (object item in enumerableList)
            {
                streamOrTextWriter.WriteLine(item.ToString());
            }
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
