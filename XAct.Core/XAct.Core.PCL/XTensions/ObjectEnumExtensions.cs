﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections;
    //using XAct.Extensions;

// ReSharper restore CheckNamespace
#endif


//For extension methods, which are compiler syntatic sugar
//use the root namespace (see notes in Core explaining why).
// ReSharper disable CheckNamespace

    /// <summary>
    ///   Extensions to <see cref = "IEnumerable" /> objects.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class ObjectEnumEnumerableExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Checks an Enum argument to ensure that its value is defined by the specified Enum type.
        /// </summary>
        /// <param name = "enumType">The Enum type the value should correspond to.</param>
        /// <param name = "value">The value to check for.</param>
        /// <param name = "argumentName">The name of the argument holding the value.</param>
        public static void EnumDefined(this object value, Type enumType, string argumentName)
        {
            if (Enum.IsDefined(enumType, value) == false)
            {
                throw new ArgumentException(argumentName);
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Gets the numerical value of the Enum value.
        /// </summary>
        /// <typeparam name = "TEnum">The type of the enum.</typeparam>
        /// <param name = "name">The name.</param>
        /// <param name = "throwArgumentIfNotFound">Throw an argument if not found.</param>
        /// <returns></returns>
        public static int GetEnumValue<TEnum>(this TEnum name, bool throwArgumentIfNotFound)
        {
            if (name.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("name");
            }
            name.ValidateIsEnum("name");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            foreach (int i in Enum.GetValues(typeof (TEnum)))
            {
                TEnum e = (TEnum) Enum.ToObject(typeof (TEnum), i);

                if (e.Equals(name))
                {
                    return i;
                }
            }
            if (throwArgumentIfNotFound)
            {
                throw new ArgumentNullException("name", Constants.EnumValueNotFound);
            }

            return 0;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the specified value is of Type Enum.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <returns>
        ///   <c>true</c> if the specified value is an enum; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEnum(this object value)
        {
            if (value.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("value");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            return value.GetType().IsEnum;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Validates that the argument is of Type Enum.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <param name = "argumentName">Name of the argument to validate.</param>
        public static void ValidateIsEnum(this object value, string argumentName)
        {
            if (!value.IsEnum())
            {
                throw new ArgumentException(argumentName);
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
        }

        #region Nested type: Constants

        /// <summary>
        ///   Class of static Resources used
        ///   by <see cref = "ObjectEnumEnumerableExtensions" />
        /// </summary>
        public class Constants
        {
            /// <summary>
            ///   Resource.
            /// </summary>
            public static string EnumValueNotFound = Properties.Resources.EnumValueNotFound;
        }

        #endregion
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
