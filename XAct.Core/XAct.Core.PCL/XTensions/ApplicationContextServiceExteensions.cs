﻿namespace XAct
{
    using System.Collections.Generic;
    using XAct.State;

    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationContextServiceExtensions
    {

        /// <summary>
        /// Gets the X act lib state dictionary.
        /// </summary>
        /// <param name="applicationStateService">The application state service.</param>
        public static Dictionary<string, object> GetXActLibStateDictionary(this IApplicationStateService applicationStateService)
        {
            const string key = "XActLib";
            Dictionary<string, object> xactLibState = applicationStateService[key] as Dictionary<string, object>;
            
            if (xactLibState == null)
            {
                applicationStateService[key] =  xactLibState = new Dictionary<string, object>();
            }
            return xactLibState;
        }

        /// <summary>
        /// Gets the X act lib state dictionary.
        /// </summary>
        /// <param name="sessionStateService">The session state service.</param>
        public static Dictionary<string, object> GetXActLibStateDictionary(this ISessionStateService sessionStateService)
        {
            const string key = "XActLib";
            Dictionary<string, object> xactLibState = sessionStateService[key]  as Dictionary<string, object>;
            

            if (xactLibState == null)
            {
                sessionStateService[key] = xactLibState = new Dictionary<string, object>();
            }
            return xactLibState;
        }
        /// <summary>
        /// Gets the X act lib context dictionary.
        /// </summary>
        /// <param name="contextStateService">The context state service.</param>
        public static Dictionary<string, object> GetXActLibContextDictionary(this IContextStateService contextStateService)
        {
            const string key = "XActLib";
            Dictionary<string, object> xactLibState = contextStateService.Items[key] as Dictionary<string, object>;


            if (xactLibState == null)
            {
                contextStateService.Items[key] = xactLibState = new Dictionary<string, object>();
            }
            return xactLibState;
        }

    }
}
