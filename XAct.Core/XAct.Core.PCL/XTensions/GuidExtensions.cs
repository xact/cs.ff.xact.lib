
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Diagnostics.CodeAnalysis;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extension Methods to the Guid Type.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class GuidExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts System.Guid to 32 continuous character representation ("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx").
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     This is just an example of how to convert a System.Guid. 
        ///   </para>
        ///   <para>
        ///     Its main use is for persistence in DB/XML --
        ///     note that MySql users are used to this format, whereas
        ///     SqlServer users are used to the "B" format.
        ///   </para>
        /// </remarks>
        /// <param name = "guid">System.Guid to convert to string.</param>
        /// <returns>32 char string: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720", Justification = "'guid' is a perfectly good argument name.")]
        public static String ToString(this Guid guid)
        {
            /*
			
            N 32 chiffres :	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            D 36 chiffres : xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
            B 38 chiffres :	{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}
            P 38 chiffres :	(xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)
	
            */

            string tResult = guid.ToString("N");
            return tResult;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Create a COMB Guid from the given Guid (usually will be some kind of Machine specific Guid).
        /// <para>
        /// Can maybe be useful for creating Guids in a distributed environment.
        /// </para>
        /// <para>
        /// Not good enough. The following code produces several guids in a row that are the same.
        /// Maybe there is a better solution out there (finer grain).
        /// </para>
        /// </summary>
        /// <param name="machineGuid"></param>
        /// <internal>
        /// See: http://stackoverflow.com/questions/665417/sequential-guid-in-linq-to-sql
        /// </internal>
        /// <returns></returns>
        /// <internal>
        /// </internal>
        [Obsolete]
        public static Guid GenerateCombGuid(this Guid machineGuid)
        {
            byte[] destinationArray = machineGuid.ToByteArray();
            DateTime time = new DateTime(0x76c, 1, 1);
            DateTime now = DateTime.Now;
            TimeSpan span = new TimeSpan(now.Ticks - time.Ticks);
            TimeSpan timeOfDay = now.TimeOfDay;
            byte[] bytes = BitConverter.GetBytes(span.Days);
            byte[] array = BitConverter.GetBytes((long) (timeOfDay.TotalMilliseconds/3.333333));
            Array.Reverse(bytes);
            Array.Reverse(array);
            Array.Copy(bytes, bytes.Length - 2, destinationArray, destinationArray.Length - 6, 2);
            Array.Copy(array, array.Length - 4, destinationArray, destinationArray.Length - 4, 4);
            return new Guid(destinationArray);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts the Guid to the a short string
        /// <para>
        /// Used for roundtripping Guids across the net.
        /// </para>
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string ToShortGuidString(this Guid guid)
        {
            //Convert to Byte array,
            //get rid of '/' and '+'
            //strip off closing '=='
            return Convert.ToBase64String(guid.ToByteArray()).Replace("/", "_").Replace("+", "-").Substring(0, 0x16);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts the Guid to the a short string
        /// <para>
        /// Used for roundtripping Guids across the net.
        /// </para>
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="embedTimeStamp">Add TimeStamp</param>
        /// <returns></returns>
        public static string ToShortGuidString(this Guid guid, bool embedTimeStamp)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts a ShortGuid string back into a Guid.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static Guid ToShortGuid(string value)
        {
            if (value.IsNullOrEmpty())
            {
                throw new ArgumentNullException("value");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            value = value.Replace("_", "/").Replace("-", "+") + "==";
            return new Guid(Convert.FromBase64String(value));
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts a ShortGuid string back into a Guid.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="timeToLive">The amount the time allowed since the ShortGuid was generated.</param>
        /// <returns></returns>
        public static Guid ToShortGuid(string value, TimeSpan timeToLive)
        {
            throw new NotImplementedException();
        }
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
