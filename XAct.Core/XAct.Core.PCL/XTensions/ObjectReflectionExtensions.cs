﻿namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    ///   Extension Methods for Objects, specifically to do with Reflection.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
// ReSharper disable CheckNamespace
    public static class ObjectReflectionExtensions
        // ReSharper restore CheckNamespace
    {
        private static readonly CultureInfo _invariantCulture = CultureInfo.CurrentCulture;

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Sets the object's public property or field's value.
        ///   <para>
        ///     Note: Does not throw an error if object is null.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Usage Example:
        ///     <code>
        ///       <![CDATA[
        /// ListItem item = new ListItem();
        /// ...
        /// Reflection.SetMemberValue(item, "text", "Some Visible Text");
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "objectInstance">The objectInstance.</param>
        /// <param name = "name">Name of the property or field(Ignores Case).</param>
        /// <param name = "value">The value.</param>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        [SuppressMessage("Microsoft.Naming", "CA1720",
            Justification = "'objectInstance' is a perfectly good argument name.")]
        public static void SetMemberValue(
            this object objectInstance,
            string name,
            object value)
        {
            const BindingFlags bindings = BindingFlags.IgnoreCase |
                                          BindingFlags.Instance |
                                          BindingFlags.Public;

            objectInstance.SetMemberValue(name, value, bindings);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Sets the object's public property or field's value.
        ///   <para>
        ///     Note: Does not throw an error if object is null.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Usage Example:
        ///     <code>
        ///       <![CDATA[
        /// ListItem item = new ListItem();
        /// ...
        /// Reflection.SetMemberValue(item, "text", "Some Visible Text");
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "objectInstance">The objectInstance.</param>
        /// <param name = "name">Name of the property or field(Ignores Case).</param>
        /// <param name = "bindingFlags">The BindingFlags to use.</param>
        /// <param name = "value">The value.</param>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        [SuppressMessage("Microsoft.Naming", "CA1726", Justification = "bindingFlags is perfectly ok.")]
        public static void SetMemberValue(
            this object objectInstance,
            string name, object value,
            BindingFlags bindingFlags)
        {
            if (objectInstance == null)
            {
                return;
            }
            Type instanceType = objectInstance.GetType();


            PropertyInfo propertyInfo =
                instanceType.GetProperty(name,
                                         bindingFlags);
            if (propertyInfo != null)
            {
                if (!propertyInfo.CanWrite)
                {
                    throw new ArgumentException(
                        "{0} Property found -- but it is read only or cannot be set.".FormatStringExceptionCulture(name));
                }
                Type t1 = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;

                if ((value != null) && (value.GetType() != propertyInfo.PropertyType))
                {
                    value = propertyInfo.PropertyType.IsEnum
                                ? Enum.Parse(propertyInfo.PropertyType, value.ToString(), true)
                                : Convert.ChangeType(value, t1, null);
                }
                propertyInfo.SetValue(objectInstance, value, null);
                return;
            }

            //Wasn'instanceType a Property...try a field:
            FieldInfo fieldInfo =
                GetFieldInfo(instanceType, name, bindingFlags);

            if (fieldInfo != null)
            {
                if ((value != null) && (value.GetType() != fieldInfo.FieldType))
                {
                    value = fieldInfo.FieldType.IsEnum
                                ? Enum.Parse(fieldInfo.FieldType, value.ToString(), true)
                                : Convert.ChangeType(value, fieldInfo.FieldType, _invariantCulture);
                }
                fieldInfo.SetValue(objectInstance, value);
                return;
            }

            throw new ArgumentException(
                "Could not find a Property or Field with given name:'{0}'.".FormatStringExceptionCulture(name)
                );
        }





        ///<summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///  Gets the object's public property value.
        ///  <para>
        ///    Note: Does not throw an error if object is null.
        ///  </para>
        ///</summary>
        ///<remarks>
        ///  <para>
        ///    Usage Example:
        ///    <code>
        ///      <![CDATA[
        /// ListItem item = new ListItem("Some Value");
        /// ...
        /// string s = 
        ///		(string)
        ///		Reflection.SetPropertyValue(item, "text");
        /// ]]>
        ///    </code>
        ///  </para>
        ///</remarks>
        ///<param name = "objectInstance">The objectInstance.</param>
        ///<param name = "name">Name of the property or field.</param>
        ///<returns></returns>
        ///<internal>
        ///  <para>
        ///    UnitTested.
        ///  </para>
        ///</internal>
        [SuppressMessage("Microsoft.Naming", "CA1720",
            Justification = "'objectInstance' is a perfectly good argument name.")]
        public static object GetMemberValue(this object objectInstance, string name)
        {
            const BindingFlags bindings = BindingFlags.IgnoreCase |
                                          BindingFlags.Instance |
                                          BindingFlags.Public;

            var result = GetMemberValue(objectInstance, name, bindings);
            return result;
        }

        ///<summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///  Gets the object's public property value.
        ///  <para>
        ///    Note: Does not throw an error if object is null.
        ///  </para>
        ///</summary>
        ///<remarks>
        ///  <para>
        ///    Usage Example:
        ///    <code>
        ///      <![CDATA[
        /// ListItem item = new ListItem("Some Value");
        /// ...
        /// string s = 
        ///		(string)
        ///		Reflection.SetPropertyValue(item, "text");
        /// ]]>
        ///    </code>
        ///  </para>
        ///</remarks>
        ///<param name = "objectInstance">The objectInstance.</param>
        ///<param name = "name">Name of the property or field.</param>
        ///<param name = "bindingFlags"> The Binding Flags to use</param>
        ///<returns></returns>
        ///<internal>
        ///  <para>
        ///    UnitTested.
        ///  </para>
        ///</internal>
        [SuppressMessage("Microsoft.Naming", "CA1726", Justification = "bindingFlags is perfectly ok.")]
        public static object GetMemberValue(this object objectInstance, string name,
                                            BindingFlags bindingFlags)
        {
            if (objectInstance == null)
            {
                return null;
            }

            Type instanceType = objectInstance.GetType();


            PropertyInfo propertyInfo =
                instanceType.GetProperty(
                    name,
                    bindingFlags);

            if (propertyInfo != null)
            {
                return propertyInfo.GetValue(objectInstance, null);
            }

            FieldInfo fieldInfo = GetFieldInfo(instanceType, name, bindingFlags);
            if (fieldInfo != null)
            {
                return fieldInfo.GetValue(objectInstance);
            }

            throw new ArgumentException(
                "Could not find a Property or Field with given name:'{0}'.".FormatStringExceptionCulture(name)
                );
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Recursively look for fieldInfo's in type or anscestors.
        /// </summary>
        /// <param name = "type"></param>
        /// <param name = "fieldName"></param>
        /// <param name = "bindingFlags"></param>
        /// <returns></returns>
        /// <internal>
        ///   <para>
        ///     UnitTested.
        ///   </para>
        /// </internal>
        private static FieldInfo GetFieldInfo(
            this Type type, string fieldName,
            BindingFlags bindingFlags)
        {
            //See: http://skysigal.xact-solutions.com/Blog/tabid/427/EntryID/470/Default.aspx

            FieldInfo field = type.GetField(fieldName, bindingFlags);
            if (field != null)
            {
                return field;
            }
            return type.BaseType == null ? null : type.BaseType.GetFieldInfo(fieldName, bindingFlags);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns an array of the classnames in the given Namespace.
        /// </summary>
        /// <param name = "classWithinNamespace">The q A class within namespace.</param>
        /// <returns></returns>
        public static string[] EnumerateClassesInNamespace(this Type classWithinNamespace)
        {
            classWithinNamespace.ValidateIsNotDefault("classWithinNamespace");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            List<string> result = new List<string>();
            Type type;
            string classNamespace = classWithinNamespace.Namespace;

            Type[] tTypes = classWithinNamespace.Assembly.GetTypes();
            foreach (Type t in tTypes)
            {
                type = t;
                if (
                    //(type.MemberType == MemberTypes.TypeInfo) &&
                    (type.Namespace == classNamespace))
                {
                    result.Add(type.ToString());
                }
            }
            return result.ToArray();
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Invokes a Method of a instantiated Class Object.
        /// </summary>
        /// <param name = "classInstance"></param>
        /// <param name = "methodName"></param>
        /// <param name = "arguments"></param>
        /// <returns></returns>
        public static object InvokeMethod(this object classInstance, string methodName, object[] arguments)
        {
            classInstance.ValidateIsNotDefault("classInstance");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            MethodInfo tMethod = classInstance.GetType().GetMethod(methodName);
            //*** get the method that is to be called

            object tResult = null;
            if (tMethod != null)
            {
                tResult = tMethod.Invoke(classInstance, arguments); //*** invoke the method call
            }
            return tResult;
        }



        /// <summary>
        ///   Invokes a Method of a instantiated Class Object.
        /// </summary>
        /// <param name="classInstance">The class instance.</param>
        /// <param name="argumentTypes">The argument types.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns></returns>
        public static object InvokeMethod(this object classInstance, string methodName, Type[] argumentTypes,
                                          object[] arguments)
        {
            classInstance.ValidateIsNotDefault("classInstance");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            MethodInfo tMethod = classInstance.GetType().GetMethod(methodName, argumentTypes);

            //*** get the method that is to be called

            if (tMethod == null)
            {
                return null;
            }
            return tMethod.Invoke(classInstance, arguments); //*** invoke the method call

        }






        /// <summary>
        /// Gets the object proeprties as a dictionary.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static Dictionary<string, object> GetObjectPropertyValues(this object context)
        {
            Dictionary<string, object> result = context.GetType()
                                                       .GetProperties()
                                                       .ToDictionary(p => p.Name, p => p.GetValue(context, null));

            return result;
        }


        /// <summary>
        /// Sets the object property values fom the given dictionary.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="propertyValues">The property values.</param>
        public static void SetObjectPropertyValues(this object context, Dictionary<string, object> propertyValues)
        {
            foreach (KeyValuePair<string, object> kvp in propertyValues)
            {
                context.SetMemberValue(kvp.Key, kvp.Value);
            }
        }

        /// <summary>
        /// Poor maps mapper.
        /// <para>
        /// WARNING: This is only a very rudimentary mapper (in so much that it will copy
        /// scalar variables fine -- but if the variable is reference to an object, it won't
        /// clone it, so nothing gets copied over.
        /// at all.
        /// </para>
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="onlyScalarProperties">if set to <c>true</c> [only scalar properties].</param>
        public static void MapPropertyValues(this object source, object target, bool onlyScalarProperties = true)
        {
            foreach (PropertyInfo propertyInfo in source.GetType().GetProperties())
            {
                if (onlyScalarProperties &&
                    ((!propertyInfo.PropertyType.IsValueType) && (propertyInfo.PropertyType != typeof (string))))
                {
                    continue;
                }
                target.SetMemberValue(propertyInfo.Name, source.GetMemberValue(propertyInfo.Name));
            }
        }

        /// <summary>
        /// Poor maps mapper.
        /// <para>
        /// WARNING: This is only a very rudimentary mapper (in so much that it will copy
        /// scalar variables fine -- but if the variable is reference to an object, it won't
        /// clone it, so nothing gets copied over.
        /// at all.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="onlyScalarProperties">if set to <c>true</c> [only scalar properties].</param>
        public static void MapPropertyValues<T>(this T source, T target,  bool ignoreNull=true, bool updateOnlyIfNonEqual = true,
                                                bool onlyScalarProperties = true)
        {
            foreach (PropertyInfo propertyInfo in source.GetType().GetProperties())
            {
                if (onlyScalarProperties &&
                    ((!propertyInfo.PropertyType.IsValueType) && (propertyInfo.PropertyType != typeof (string))))
                {
                    continue;
                }
                var sourceValue = source.GetMemberValue(propertyInfo.Name);
                if  (ignoreNull && sourceValue.IsNull())
                {
                    continue;
                }
                if (updateOnlyIfNonEqual)
                {
                    var targetValue = target.GetMemberValue(propertyInfo.Name);

                    if (object.Equals(sourceValue, targetValue))
                    {
                        continue;
                    }
                }
                target.SetMemberValue(propertyInfo.Name, sourceValue);
            }
        }
    }
}
