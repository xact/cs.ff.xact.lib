﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
using TraceLevel = XAct.Diagnostics.TraceLevel;

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using XAct.Collections.Comparers;
    using XAct.Diagnostics;
    using XAct.Environment;

// ReSharper restore CheckNamespace
#endif

/// <summary>
    /// Extensions to IList collections
    /// </summary>
    public static class IListExtensions
    {

        /// <summary>
        /// Gets the environment service.
        /// </summary>
        private static IEnvironmentService EnvironmentService
        {
            get { return DependencyResolver.Current.GetInstance<IEnvironmentService>(); }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Adds the item if is not already added.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage:
        /// <example>
        /// <code>
        /// <![CDATA[
        /// IList<string> stuff = new List<string>();
        /// stuff.AddIfNotAlreadyAdded("new");
        /// stuff.AddIfNotAlreadyAdded("new");
        /// Debug.Assert(stuff.Count==1);
        /// ]]>
        /// </code>
        /// </example>
        /// </para>
        /// <para>
        /// In essense, all it does is check first, using the <c>Contain</c> method.
        /// </para>
        /// </remarks>
        /// <param name="list">The string list.</param>
        /// <param name="item">The item.</param>
        public static void AddIfNotAlreadyAdded<T>(this IList<T> list, T item)
        {
            if (!list.Contains(item))
            {
                list.Add(item);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Adds the items if they are not already added.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="items">The items.</param>
        public static void AddIfNotAlreadyAdded<T>(this IList<T> list, IEnumerable<T> items)
        {
            foreach (T item in items.Where(item => !list.Contains(item)))
            {
                list.Add(item);
            }
        }




        /// <summary>
        /// Determines whether the list contains the item, by using the expression to compare properties of the item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="item">The item.</param>
        /// <param name="expression">The expression.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified list]; otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains<T>(this IList<T> list, T item, Func<T, object> expression)
        {
            if (expression == null)
            {
                expression = (x) => x;
            }

            FuncComparer<T> expComparer = new FuncComparer<T>(expression);
            
            foreach(T i in list)
            {
                if (expComparer.Equals(i,item))
                {
                    return true;
                }
            }
            return false;
        }

    /// <summary>
        /// Converts the array to a string paragraph, each item on its own line.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stringList">The string list.</param>
        /// <returns></returns>
        public static string ToText<T>(this IList<T> stringList)
        {
            string newLine = EnvironmentService.NewLine;

            StringBuilder stringBuilder = new StringBuilder();
            
            foreach(T item in stringList)
            {
                stringBuilder.Append(item);
                stringBuilder.Append(newLine);
            }
            return stringBuilder.ToString();
        }

    /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Chunks the list into groups of certain size.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="chunkMaxSize">Size of the chunk max.</param>
        /// <returns></returns>
        /// <internal><para>5/12/2011: Sky</para></internal>
        public static IEnumerable<IGrouping<int, TData>> Chunk<TData>(this IList<TData> list, int chunkMaxSize)
        {
            //IEnumerable<IGrouping<int, Guid>> chunkedCorrelationIds =
            //    from index in Enumerable.Range(0, correlationIdsToSend.Length)
            //    group correlationIdsToSend[index] by index / maxCorrelationIdsPerRequest;

            //IEnumerable<IGrouping<int, Guid>> chunkedCorrelationIds =
            //    Enumerable.Range(0, correlationIdsToSend.Length).GroupBy(index => index/maxCorrelationIdsPerRequest,
            //                                                             index => correlationIdsToSend[index]);

            IEnumerable<IGrouping<int, TData>> chunkedResults =
                Enumerable
                    .Range(0, list.Count)
                    .GroupBy(index => index/chunkMaxSize, index => list[index]);

            return chunkedResults;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the page of items from the from the list.
        /// <para>
        /// Note that the pageNumber is zero-indexed.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// <code>
        /// <![CDATA[
        /// List<Elements> pageItems = list.GetPage(10,3);
        /// Debug.Assert(pageItems.Count <=10);
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <returns></returns>
        /// <internal><para>5/15/2011: Sky</para></internal>
        public static IEnumerable<TData> GetPage<TData>(this IList<TData> list, int pageSize, int pageNumber)
        {
            int start = pageSize*pageNumber;
            int end = Math.Min(start + pageSize, list.Count);

            for (int i = start; i < end; i++)
            {
                yield return list[i];
            }
        }



        ///// <summary>
        ///// <para>
        ///// An XActLib Extension.
        ///// </para>
        ///// Removes all elements in list one found in list two.
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="first">The first.</param>
        ///// <param name="second">The second.</param>
        ///// <param name="expression">The expression.</param>
        //public static void RemoveAll<T>(this IList<T> first, IList<T> second, Func<T, object> expression)
        //{
        //    FuncComparer<T> expComparer = new FuncComparer<T>(expression);

        //    //From back to front:
        //    for (int i = first.Count - 1; i > -1; i--)
        //    {
        //        T item = first[i];

        //        if (Object.Equals(second.FirstOrDefault(i2 => expComparer.Equals(item, i2)), default(T)) == false)
        //        {
        //            first.RemoveAt(i);
        //        }

        //    }
        //}



        /// <summary>
        /// Remove all from list1 items found in list2 (returning the removed items)
        /// <para>
        /// <code>
        /// <![CDATA[
        //Will remove from List1 elements found in list2:
        /// var results = list1.RemoveAll<X>(list2,x=>x.Name);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listToModify">The list.</param>
        /// <param name="itemsToUseAsFilters">The items2.</param>
        /// <param name="comparisionExpression">The expression.</param>
        /// <param name="removedItems">The items removed from <paramref name="listToModify"/>.</param>
        public static void RemoveAll<T>(this IList<T> listToModify, ICollection<T> itemsToUseAsFilters, Func<T, object> comparisionExpression, out T[] removedItems)
        {

            //DELETE from originalCollection any items not found in newCollection:
            //var removed = originalCollection.Except(secondArray, comparisonExpression).ToList();
            //originalCollection.RemoveAll(removed, comparisonExpression);

            FuncComparer<T> expComparer = new FuncComparer<T>(comparisionExpression);

            Collection<T> results = new Collection<T>();

            int imax = listToModify.Count;

            //From end to beginning (or else throws off indexer count):
            for (int i = imax - 1; i > -1; i--)
            {
                T originalListItem = listToModify[i];
                T filterListItem = itemsToUseAsFilters.FirstOrDefault(o => expComparer.Equals(o, originalListItem));

                //If default, it's because it was not found in filter list, so get out:
                if (Object.Equals(filterListItem, default(T)))
                {
                    continue;
                }
                //Found in filter list, so remove from from source list:
                listToModify.Remove(originalListItem);
                //And save in results:
                results.Insert(0, filterListItem);

            }

            removedItems = results.ToArray();
        }


        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <summary>
        /// Removes all items from the collectionthat match the predicate.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first">The first.</param>
        /// <param name="predicate">The predicate.</param>
        public static void RemoveAll<T>(this IList<T> first, Predicate<T> predicate)
        {
            //From back to front:
            for (int i = first.Count - 1; i > -1; i--)
            {
                T item = first[i];

                if (predicate.Invoke(item))
                {
                    first.RemoveAt(i);
                }

            }
        }


        /// <summary>
        /// Traces the contents of the specified collection to the current <see cref="ITracingService"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        public static string[] Trace<T>(this IList<T> list)
        {

            List<string> results = new List<string>();

            IEnumerator<T> enumerator = list.GetEnumerator();
            //int position = 0;
            while (enumerator.MoveNext())
            {
                results.Add(enumerator.Current.ToString());
                //position++;
            }
            return results.ToArray();
        }


        /// <summary>
        /// Gets the last occurance in the list of an item that fits the given predicate.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public static int LastIndexOf<T>(this IList<T> items, Func<T, bool> predicate)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (predicate == null) throw new ArgumentNullException("predicate");

            int iMax = items.Count - 1;
            for (int i = iMax; i > -1; i--)
            {
                //Find first non-blank:
                if (!predicate(items[i]))
                {
                    //return the last one:
                    return (i == iMax) ? -1 : i + 1;
                }
            }
            return -1;
        }


        public static void Truncate<T>(this IList<T> list, int newSize)
        {
            //Current list size =8, newSize=5
            if (newSize >= list.Count)
            {
                //0=0
                //1>0
                //1==1
                //if 8 > 5
                //if 8 == 8
                return;
            }
            //8-3:
            int range = list.Count - newSize;

            //01234567
            //.....xxx
            //8...7
            //1...0
            while (list.Count > newSize)
            {
                list.RemoveAt(list.Count - 1);
            }

        }
    
    }

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
