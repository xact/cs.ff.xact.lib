﻿//OK

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    ///   Extension Methods to 
    ///   <see cref = "Stream" />
    ///   object
    /// </summary>
    public static class StreamExtensions
    {

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Copies the input stream into the target output stream.
        /// <para>
        /// For a much faster solution: http://stackoverflow.com/a/4139427
        /// </para>
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="input">The input.</param>
        public static long CopyStreamFrom(this Stream output, Stream input)
        {
            long result=0;
            const int bufferSize = 32768;
            byte[] buffer = new byte[bufferSize];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
                result += read;
            }
            return result;
        }

        //private static long CopyStream(FileStream inputStream, Stream outputStream)
        //{
        //    long bufferSize = inputStream.Length < BUFFER_SIZE ? inputStream.Length : BUFFER_SIZE;
        //    byte[] buffer = new byte[bufferSize];
        //    int bytesRead = 0;
        //    long bytesWritten = 0;
        //    while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) != 0)
        //    {
        //        outputStream.Write(buffer, 0, bytesRead);
        //        bytesWritten += bufferSize;
        //    }

        //    return bytesWritten;
        //}


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Creates a new <see cref="StreamReader"/> (ie, a Text reader) for the Stream.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static StreamReader CreateStreamReader(this Stream stream)
        {
            return new StreamReader(stream);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Creates the stream writer.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns></returns>
        public static StreamWriter CreateStreamWriter(this Stream stream, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            return new StreamWriter(stream, encoding);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Using a StreamReader, reads the stream into an array of lines.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static IEnumerable<string> ReadLines(this Stream stream)
        {
            using (StreamReader streamReader = stream.CreateStreamReader())
            {
                return streamReader.ReadLines();
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Using a StreamReader, Reads the stream into a string.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static String ReadToEnd(this Stream stream)
        {
            using (StreamReader streamReader = stream.CreateStreamReader())
            {
                return streamReader.ReadToEnd();
            }
        }

        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Using a streamWriter, writes/appends (depends on how stream was opened) the string contents to the given stream.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="contents">The contents.</param>
        /// <param name="leaveOpen">if set to <c>true</c> [leave open].</param>
        public static void AppendAllText(this Stream stream, string contents, bool leaveOpen=false)
        {
            if (leaveOpen)
            {
                StreamWriter streamWriter = stream.CreateStreamWriter();
                streamWriter.Write(contents);
                return;
            }
            //Disposing the StreamWriter will Close it, which closes the underlying Stream:
            using (StreamWriter streamWriter = stream.CreateStreamWriter())
            {
                streamWriter.Write(contents);
            }

        }

        public static void AppendAllText(this Stream stream, string[] contents, bool leaveOpen = false)
        {
            if (leaveOpen)
            {
                StreamWriter streamWriter = stream.CreateStreamWriter();
                //Checked: this *is* the slow way that the framework is using.
                foreach (string line in contents)
                {
                    streamWriter.WriteLine(line);
                }
                return;
            }
            //Disposing the StreamWriter will Close it, which closes the underlying Stream:
            using (StreamWriter streamWriter = stream.CreateStreamWriter())
            {
                foreach (string line in contents)
                {
                    streamWriter.WriteLine(line);
                }
            }

        }

        /*
/// <summary>
/// 
/// </summary>
/// <remarks>
/// <para>
/// Solution for CE:
/// </para>
/// </remarks>
/// <param name="xsltFilePath"></param>
/// <param name="xmlReader"></param>
/// <param name="stringWriter"></param>
protected static void Transform(string xsltFilePath,  XmlReader xmlReader, StringWriter stringWriter) {
  System.Xml.Xsl.XslTransform tra = new XslTransform();

  System.Xml.XPath.XPathDocument doc = new 
    System.Xml.XPath.XPathDocument(xmlReader);

  tra.Load(xsltFilePath);

  XsltArgumentList args = new XsltArgumentList();
  tra.Transform(doc,args,stringWriter);
}
*/

        /*
        public static string Transform(string qXMLPath, string qXSLPath) {
          System.Xml.XmlDocument oXML = new System.Xml.XmlDocument();
          oXML.Load(qXMLPath);
          return Transform(oXML, qXSLPath);
        }
        public static string Transform(System.Xml.XmlDocument oXML, string qXSLPath) {
          System.Xml.Xsl.XslTransform oXSL = new System.Xml.Xsl.XslTransform();
          oXSL.Load(qXSLPath);

          return Transform(oXML, oXSL);
        }
        */


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Serialize the object to the given stream.
        /// </summary>
        /// <param name = "stream">The stream to write to.</param>
        /// <param name = "objectToSerialize">The object to serialize.</param>
        //[SuppressMessage("Microsoft.Naming", "CA1720", Justification = "objectToSerialize is more descriptive than 'value'.")]
        public static void Serialize(this Stream stream, object objectToSerialize)
        {
            stream.ValidateIsNotDefault("stream");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            XmlSerializer xmlSerializer = new XmlSerializer(objectToSerialize.GetType());

            xmlSerializer.Serialize(stream, objectToSerialize);
        }




        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Reads all bytes.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        //public static byte[] ReadAllBytes(this Stream stream)
        //{
        //    byte[] bytes;
        //        // Do a blocking read
        //        int index = 0;
        //        long fileLength = stream.Length;
        //        int count = (int)fileLength;
        //        bytes = new byte[count];

        //        while (count > 0)
        //        {
        //            count = Math.Min(count, 64*1024);

        //            int n = stream.Read(bytes, index, count);
                    
        //            if (n == 0)
        //            {
        //                break;
        //            }
        //            index += n;
        //            count -= n;
        //        }
        //    return bytes;
        //}

        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Reads all bytes.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static byte[] ReadAllBytes(this Stream stream)
        {
            var buffer = new byte[64 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


            /// <summary>
            /// Determines whether stream 
            /// (consider whether you need to set Position)
            /// contains valid Xml.
            /// </summary>
            /// <param name="stream">The stream.</param>
            /// <returns></returns>
            public static bool ContainsXmlElements(this Stream stream)
            {
                long currentPos = stream.Position;
                try
                {
                    using (XmlReader xmlTextReader = XmlReader.Create(stream))
                    {
                        xmlTextReader.MoveToContent();
                        if (xmlTextReader.NodeType == XmlNodeType.Element)
                        {
                            return true;
                        }
                    }
                }
                finally
                {
                    stream.Position = currentPos;
                }
                return false;
            }



    }

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
