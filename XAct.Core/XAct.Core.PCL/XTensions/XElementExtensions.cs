﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;

    /// <summary>
    /// Extensions to the XElement Linq object.
    /// </summary>
    public static class XElementExtensions
    {
        /// <summary>
        /// Gets the value of an XElement under the given element.
        /// </summary>
        /// <param name="xElement">The XML element.</param>
        /// <param name="childName">Name of the child.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        public static bool GetDescendentValue(this XElement xElement, string childName, out string result)
        {
            XElement tmpElement = xElement.Descendants(childName).FirstOrDefault();

            if (tmpElement == null)
            {
                result = null;
                return false;
            }
            result = tmpElement.Value;
            return true;
        }
        /// <summary>
        /// Gets the first XElement under the given one.
        /// </summary>
        /// <param name="xElement">The x element.</param>
        /// <param name="childName">Name of the child.</param>
        /// <param name="childElement">The child element.</param>
        /// <returns></returns>
        public static bool GetDescendent(this XElement xElement, string childName, out XElement childElement)
        {
            childElement = xElement.Descendants(childName).FirstOrDefault();

            return (childElement != null);
        }


        /// <summary>
        /// Deserializes the specified <see cref="XDocument"/>
        /// int
        /// </summary>
        /// <typeparam name="T">Type of object to deserialize</typeparam>
        /// <param name="xElement">The XElement.</param>
        /// <returns></returns>
        public static T Deserialize<T>(this XElement xElement)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (XmlReader reader = xElement.CreateReader())
            {
                return (T)xmlSerializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// Serialize the given object as an <see cref="XElement" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <returns></returns>
        public static XElement SerializeAsXElement<T>(this T value, string elementName)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            XElement xElement = new XElement(elementName);

            using (XmlWriter writer = xElement.CreateWriter())
            {
                xmlSerializer.Serialize(writer, value);
            }

            return xElement;
        }




        /// <summary>
        /// Serialize the given object as an <see cref="XElement" />.
        /// <para>
        /// Works. But is expensive to use. And possible flaky in regards to namespaces.
        /// </para>
        /// <para>
        /// See: http://stackoverflow.com/questions/760262/xmlserializer-remove-unnecessary-xsi-and-xsd-namespaces
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <returns></returns>
        public static XElement SerializeAsXElement_InProgress<T>(this T value, string elementName)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T), null, null, null, "");

            XElement xElement = new XElement(elementName);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                StringBuilder stringBuilder = new StringBuilder();

                using (XmlWriter writer = XmlWriter.Create(stringBuilder, new XmlWriterSettings
                {
                    ConformanceLevel = ConformanceLevel.Document,
                    OmitXmlDeclaration = false,
                    NamespaceHandling = NamespaceHandling.OmitDuplicates,
                }))
                {

                    var x = new XmlSerializerNamespaces(new XmlQualifiedName[]
                        {
                            // Don't do this!! Microsoft's documentation explicitly says it's not supported.
                            // It doesn't throw any exceptions, but in my testing, it didn't always work.

                            // new XmlQualifiedName(string.Empty, string.Empty),  // And don't do this:
                            // new XmlQualifiedName("", "")

                            // DO THIS:
                            new XmlQualifiedName(string.Empty, string.Empty) // Default Namespace
                            // Add any other namespaces, with prefixes, here.
                        });

                    xmlSerializer.Serialize(writer, value, x);
                }
                XDocument xDocument = XDocument.Parse(stringBuilder.ToString());

                XElement root = xDocument.Root;
                return root;
            }
        }



        public static IEnumerable<XElement> GetDescendentLeafNodes(this XElement xElement)
        {
            return xElement.Descendants().Where(x => !x.Elements().Any());
        }

        /// <summary>
        /// Get the absolute XPath to a given XElement, including the namespace.
        /// (e.g. "/a:people/b:person[6]/c:name[1]/d:last[1]").
        /// </summary>
        public static string GetAbsoluteXPath(this XElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            Func<XElement, string> relativeXPath = e =>
            {
                int index = e.IndexPosition();

                XNamespace currentNamespace = e.Name.Namespace;

                string name;
                if (currentNamespace == null)
                {
                    name = e.Name.LocalName;
                }
                else
                {
                    string namespacePrefix =
                        e.GetPrefixOfNamespace(currentNamespace);
                    name = (namespacePrefix.IsNullOrEmpty())
                               ? e.Name.LocalName
                               : namespacePrefix + ":" + e.Name.LocalName;
                }

                // If the element is the root, no index is required
                return (index == -1)
                           ? "/" + name
                           : string.Format
                                 (
                                     "/{0}[{1}]",
                                     name,
                                     index.ToString()
                                 );
            };

            var ancestors = from e in element.Ancestors()
                            select relativeXPath(e);

            return string.Concat(ancestors.Reverse().ToArray()) +
                   relativeXPath(element);
        }

        /// <summary>
        /// Get the index of the given XElement relative to its
        /// siblings with identical names. If the given element is
        /// the root, -1 is returned.
        /// </summary>
        /// <param name="element">
        /// The element to get the index of.
        /// </param>
        public static int IndexPosition(this XElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            if (element.Parent == null)
            {
                return -1;
            }

            int i = 1; // Indexes for nodes start at 1, not 0

            foreach (var sibling in element.Parent.Elements(element.Name))
            {
                if (sibling == element)
                {
                    return i;
                }

                i++;
            }

            throw new InvalidOperationException
                ("element has been removed from its parent.");
        }





        ///// <summary>
        ///// This will ensure that elements that are empty
        ///// have a string.Empty value -- which effectively
        ///// ensures they have both an open and close tag.
        ///// <para>
        ///// Use only for extremely esoteric legacy-matching scenarios,
        ///// as the RFC for XML specifies a preference for self-closing tags,
        ///// rather than open/close tags containing no value.
        ///// </para>
        ///// <para>There is also an equivalent XDocument extension
        ///// </para>
        ///// </summary>
        ///// <param name="element">The element.</param>
        ///// <internal>
        ///// Src: http://stackoverflow.com/a/2459207/1052767
        /////   </internal>
        //public static void ForceOpenCloseTags(this XElement element)
        //{
        //    foreach (XElement childElement in
        //        from xElement in element.DescendantNodes().OfType<XElement>()
        //        where xElement.IsEmpty
        //        select xElement)
        //    {
        //        childElement.Value = string.Empty;
        //    }
        //}

    }
}
