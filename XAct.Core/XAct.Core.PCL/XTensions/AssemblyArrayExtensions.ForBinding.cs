﻿
namespace XAct
{
using System;
using System.Collections.Generic;
using System.Linq;
    using System.Reflection;
    using XAct.Services;
    using XAct.Services.IoC.Initialization;


    /// <summary>
    /// 
    /// </summary>
    public static partial class AssemblyArrayExtensions
    {

        // ReSharper disable InconsistentNaming
        private static readonly object _lock = new object();
        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Scan given assemblies for
        /// <see cref="IBindingDescriptor"/> information,
        /// and register them in the
        /// current <see cref="DependencyResolver"/>.
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="bindingsResults">The bindings results.</param>
        /// <param name="performPreScan">if set to <c>true</c> [perform pre scan].</param>
        /// <param name="optionalOnErrorBindingCallback">The optional on error binding callback.</param>
        /// <param name="optionalPostBindingCallback">The optional post binding callback.</param>
        /// <param name="servicesToRegisterBeforeXActLibServices">The services to register before x act library services.</param>
        /// <param name="?">The ?.</param>
        public static void ScanForAndRegisterBindingDescriptors(
            this Assembly[] assemblies,
            ref InitializeLibraryBindingsResults bindingsResults,
            bool performPreScan = true,
            Action<IBindingDescriptor> optionalOnErrorBindingCallback = null,
            Action<IBindingDescriptorResult> optionalPostBindingCallback = null,
            IEnumerable<IBindingDescriptorBase> servicesToRegisterBeforeXActLibServices = null
            )
        {


            //Record what Assemblies were scanned:
            bindingsResults.BindingScanResults.AssemblyNames = 
                assemblies.Select(a=>a.FullName.ToString()).ToArray();


            lock (_lock)
            {
                using (TimedScope totalElapsed = new TimedScope())
                {

                    //Keep a list of Services already registered, and services skipped:
                    IDictionary<string, IBindingDescriptor>
                        servicesAlreadyRegistered = new Dictionary<string, IBindingDescriptor>();

                    List<IBindingDescriptor> servicesSkipped = new List<IBindingDescriptor>();


                    IBindingDescriptor[] preRegisterlist =
                        FlattenPreRegisterList(servicesToRegisterBeforeXActLibServices);

                    //Add this flattened list at the front of any services it then finds.

                    //----------------------------------------------------------------------
                    //----------------------------------------------------------------------
                    //This is the main work -- scan the assemblies for bindings
                    //returning the info in the bindingScanResults -- which is a subset
                    //of the BindingResults:
                    BindingScanResults bindingScanResults = bindingsResults.BindingScanResults;

                    if (performPreScan)
                    {
                        // ReSharper disable RedundantArgumentDefaultValue
                        assemblies.ScanForDefaultBindingDescriptors(preRegisterlist, true, ref bindingScanResults);
                        // ReSharper restore RedundantArgumentDefaultValue
                    }
                    else
                    {
                        bindingsResults.BindingScanResults.BindingDescriptors = preRegisterlist;

                    }
                    //----------------------------------------------------------------------
                    //----------------------------------------------------------------------

                    //FIX: Register the service so that it doesn't register itself again
                    //(it's done the first time by the get_Current() method...registering it again
                    //confuses the DependencyInjectionContainer)
                    servicesAlreadyRegistered.Add(
                        typeof (IDependencyResolver).AssemblyQualifiedName,
                        new BindingDescriptor(BindingType.Custom,  typeof (IDependencyResolver), typeof (DependencyResolver)));

                    //----------------------------------------------------------------------
                    using (TimedScope timedScope = new TimedScope())
                    {
                        //We need the service to use it's 
                        //extension method in order to register services:
                        IDependencyResolver serviceLocatorService = DependencyResolver.Current;

                        //int counter = 0;
                        //Register them all
                        foreach (
                            IBindingDescriptor serviceRegistrationDescriptor in bindingScanResults.BindingDescriptors)
                        {

                            IBindingDescriptorResult result;

                            string alreadyRegisteredKey = AlreadyRegisteredKey(serviceRegistrationDescriptor);

                            if ((alreadyRegisteredKey ==null) || servicesAlreadyRegistered.ContainsKey(alreadyRegisteredKey))
                            {
                                //If we have already registered this interface (not implementation)
                                //we don't do it again:
                                servicesSkipped.Add(serviceRegistrationDescriptor);
                                //new BindingDescriptor(
                                //	interfaceType,
                                //	implementationType,
                                //	pair.Value.LifeSpan));

                                result = new BindingDescriptorResult(serviceRegistrationDescriptor, true);
                            }
                            else
                            {
                                try
                                {
                                    serviceLocatorService.RegisterServiceBindingInIoC(serviceRegistrationDescriptor);

                                    result = new BindingDescriptorResult(serviceRegistrationDescriptor, false);
                                }
                                catch (Exception exception)
                                {
                                    result = new BindingDescriptorResult(serviceRegistrationDescriptor, false, exception);

                                    if (optionalOnErrorBindingCallback != null)
                                    {
                                        optionalOnErrorBindingCallback(serviceRegistrationDescriptor);
                                    }


                                    if (result.ThrowException)
                                    {
                                        throw;
                                    }

                                }
#pragma warning disable 1058
            catch
#pragma warning restore 1058
                                {
                                    //Suppossedly catch(Exception) does not catch unmamanaged.
                                    result = new BindingDescriptorResult(serviceRegistrationDescriptor, false, null);

                                    if (optionalOnErrorBindingCallback != null)
                                    {
                                        optionalOnErrorBindingCallback(serviceRegistrationDescriptor);
                                    }

                                    if (result.ThrowException)
                                    {
                                        throw;
                                    }

                                }


                                //Save a ref to the services already registered:
                                alreadyRegisteredKey = AlreadyRegisteredKey(serviceRegistrationDescriptor);

                                if (alreadyRegisteredKey != null)
                                {
                                    servicesAlreadyRegistered[alreadyRegisteredKey]
                                        = serviceRegistrationDescriptor;
                                }
                            }

                            if (optionalPostBindingCallback != null)
                            {
                                optionalPostBindingCallback(result);
                            }


                        } //~loop

                        bindingsResults.TimeToRegisterElapsed = timedScope.Elapsed;
                    }



                    bindingsResults.ServiceBindingsRegistered = servicesAlreadyRegistered.Values.ToArray();
                    bindingsResults.ServiceBindingsSkipped = servicesSkipped.ToArray();

                    
                    //bindingsResults.IsInitialized 
                    bindingsResults.TotalTimeElapsed = totalElapsed.Elapsed;

                    //And save a copy in a WellKnown location:
                    bindingsResults.IsInitialized = true;






                }

            } //~lock

        }
        /// <summary>
        /// Scans for default binding descriptors.
        /// </summary>
        /// <param name="assemblies">The assemblies.</param>
        /// <param name="servicesToRegisterFirst">The services to register first.</param>
        /// <param name="skipDuplicates">if set to <c>true</c> [skip duplicates].</param>
        /// <param name="bindingScanResults">The binding scan results.</param>
        public static void ScanForDefaultBindingDescriptors(this Assembly[] assemblies,
            IBindingDescriptor[] servicesToRegisterFirst, bool skipDuplicates,
            ref BindingScanResults bindingScanResults)
        {

            if (servicesToRegisterFirst == null)
            {
                servicesToRegisterFirst = new IBindingDescriptor[0];
            }

            using (TimedScope timedScope = new TimedScope())
            {
                //Now scan all assemblies for Class Def's that are marked with the 
                //right attribute:
                //Type attributeType = typeof(DefaultServiceImplementationAttribute);

                //Start off with what we received:

                List<string> shit = new List<string>();

                List<PrioritizedBindingDescriptor> foundBindingDescriptors = new List<PrioritizedBindingDescriptor>();

                //Scan by attributes:
                //Note that Attributes are of higher priority than anything else
                //as they are not guessed in any way -- there are specifically chosen 
                //by the developer to be the binding they want:
                List<PrioritizedBindingDescriptor> descriptorsFoundByAttribute = 
                    BuildListFromAttributes(assemblies, servicesToRegisterFirst, skipDuplicates);


                if (XAct.Library.Settings.Bindings.ScanByInterfaces)
                {
                    //Scan for interfaces:
                    List<PrioritizedBindingDescriptor> descriptorsFoundByInterface = BuildListFromInterfaces(
                        assemblies, servicesToRegisterFirst,
                        skipDuplicates, ref shit);



                    //remove from t1 those already in t2:
                    //List<PrioritizedBindingDescriptor> t4 =
                    //    t1.Intersect(t2,
                    //                 x =>
                    //                 x.BindingDescriptor.InterfaceType.FullName + ":" +
                    //                 x.BindingDescriptor.ImplementationType.FullName + ":" +
                    //                 x.BindingDescriptor.ServiceLifeType + ":" + x.BindingDescriptor.Tag + ":" +
                    //                 x.Priority).ToList();

                    //NO: remove from t1 those already in t2:
                    //YES: remove from t2 what's in t1 - the reason being that T1 is more descriptive (has Tag)

                    /*
                    List<PrioritizedBindingDescriptor> trimmedDescriptorsFoundByAttribute =
                        descriptorsFoundByAttribute.Except(descriptorsFoundByInterface,
                                  x =>
                                  x.BindingDescriptor.InterfaceType.FullName + ":" +
                                  x.BindingDescriptor.ImplementationType.FullName + ":" +
                                  x.BindingDescriptor.ServiceLifeType + ":" + x.BindingDescriptor.Tag + ":" + x.Priority)
                          .ToList();


                    prioritizedBindingDescriptors.AddRange(descriptorsFoundByInterface);
                    prioritizedBindingDescriptors.AddRange(trimmedDescriptorsFoundByAttribute);
                     */


                    List<PrioritizedBindingDescriptor> trimmedDescriptorsFoundByInterface =
                        descriptorsFoundByInterface.Except(descriptorsFoundByAttribute,
                                                           x =>
                                                           x.BindingDescriptor.InterfaceType.FullName + ":" +
                                                           x.BindingDescriptor.ImplementationType.FullName + ":" +
                                                           x.BindingDescriptor.ServiceLifeType + ":" +
                                                           //NO:(x.BindingDescriptor.Tag??"") + ":" + 
                                                           x.Priority)
                                                   .ToList();

                    //Doesn't matter which way they are added, as they will be sorted in a sec:
                    foundBindingDescriptors.AddRange(descriptorsFoundByAttribute);
                    foundBindingDescriptors.AddRange(trimmedDescriptorsFoundByInterface);


                }
                else
                {
                    //Just add the Attribute list
                    foundBindingDescriptors.AddRange(descriptorsFoundByAttribute);
                }



                //Start the results with the given arguments, first:
                List<IBindingDescriptor> results = new List<IBindingDescriptor>();
                
                //Add the ones specifically mentioned, first:
                results.AddRange(servicesToRegisterFirst.ToList());

                //Then add the rest, prioritized:
                var prioritizedBindingDescriptors =
                    foundBindingDescriptors
                    .OrderByDescending(x => x.Priority)
                    //.ThenBy(x=>x.BindingDescriptor.InterfaceType)
                    //.ThenBy(x=>x.BindingDescriptor.ImplementationType)
                    .ToArray();
                results.AddRange(prioritizedBindingDescriptors.Select(x => x.BindingDescriptor));

                //That's a wrap:
                bindingScanResults.InstanceTypesNotBound =  shit.ToArray();
                bindingScanResults.BindingDescriptors = results.ToArray();

                bindingScanResults.TimeToScanElapsed = timedScope.Elapsed;
            }
        }




        private static List<PrioritizedBindingDescriptor> BuildListFromAttributes(Assembly[] assemblies,
                                                                                         IBindingDescriptor[]
                                                                                             servicesToRegisterFirst,
                                                                                         bool skipDuplicates)
        {
            List<PrioritizedBindingDescriptor> results = new List<PrioritizedBindingDescriptor>();

            //Scan appDomain for Types decorated with Attribute, ordered:
            foreach (KeyValuePair<Type, DefaultBindingImplementationAttribute> pair in
                assemblies
                    .GetTypesDecoratedWithAttribute<DefaultBindingImplementationAttribute>(null, false, true)
                    .OrderByDescending(kvp => kvp.Value.Priority))
            {
                //Iterate through each found one
                //IHasNameAndDescription rebuild

                Type interfaceType = pair.Value.ServiceInterface;
                Type implementationType = pair.Key;

                //Convert unkonwn Binding lifespans to 
                //default value:
                BindingLifetimeType bindingLifetimeType = (pair.Value.LifeSpan == BindingLifetimeType.Undefined)
                                                              ? XAct.Library.Settings.Bindings
                                                                    .DefaultServiceLifespan
                                                              : pair.Value.LifeSpan;


                //Convert the info in the type and Attribute to a new Descriptor:
                IBindingDescriptor serviceRegistrationDescriptor =
                    new BindingDescriptor(
                        BindingType.Attribute,
                        interfaceType,
                        implementationType,
                        bindingLifetimeType,
                        pair.Value.Tag);



                if ((skipDuplicates) && (servicesToRegisterFirst!=null) && (servicesToRegisterFirst.Exists(s => s.InterfaceType == interfaceType)))
                {
                    continue;
                }

                results.Add(new PrioritizedBindingDescriptor(pair.Value.Priority, serviceRegistrationDescriptor));


            }

            return results;
        }




        private static List<PrioritizedBindingDescriptor> BuildListFromInterfaces(Assembly[] assemblies, IBindingDescriptor[] servicesToRegisterFirst,
                                                    bool skipDuplicates, ref List<string> implementationsUndeterminedDueToNotFindingInterfaceToMapTo)
        {

            List<PrioritizedBindingDescriptor> results = new List<PrioritizedBindingDescriptor>();


            //Find all types (classes, -- not abstracts, interfaces) that derive from IHasScope:
            Type[] implementationTypes = assemblies.GetTypesImplementingType<IHasBindingScope>(true);

            //Iterate through instantiable class types:
            foreach (Type implementationType in implementationTypes)
            {
                var interfaceType = GetBestMatchForInterfaceType(
                    implementationType,
                    true,
                    implementationType.GetInterfaces(false).ToArray()
                    );
                if (interfaceType == null)
                {
                    //Fix
                    //In some cases -- such as XAct.Diagnostics.Implementations.SystemDiagnosticsTracingService
                    //the interfaces are not on the class itself, but on an abstract super-class.
                    //So we get all interfaces this time (direct and all inherited):
                    interfaceType =
                        GetBestMatchForInterfaceType(
                            implementationType,
                            false,
                            implementationType.GetInterfaces()
                            );
                }



                if (interfaceType == null)
                {
                    //Damn. Not found by name:

                    if (!typeof (IHasNoBindingRequiredScope).IsAssignableFrom(implementationType))
                    {
                        implementationsUndeterminedDueToNotFindingInterfaceToMapTo.Add(implementationType.FullName);
                    }


                    continue;
                }


                //----------------------------------------
                //We know that this interface 
                BindingLifetimeType bindingLifetimeType;

                //We search on the Implementation type (ie, SomeService),
                //not the Interface, as the Scope can be associated, surplanted, on the side:
                //eg SomeService: ISomeService, IHasTransientScope:

                //Most costly to less costly:
                if (implementationType.IsSubClassOfEx(typeof(IHasTransientBindingScope)))
                {
                    bindingLifetimeType = BindingLifetimeType.TransientScope;
                }
                else if (implementationType.IsSubClassOfEx(typeof(IHasWebThreadBindingScope)))
                {
                    bindingLifetimeType = BindingLifetimeType.SingletonPerWebRequestScope;
                }
                else if (implementationType.IsSubClassOfEx(typeof(IHasSingletonBindingScope)))
                {
                    bindingLifetimeType = BindingLifetimeType.SingletonScope;
                }
                else
                {
                    bindingLifetimeType = XAct.Library.Settings.Bindings.DefaultServiceLifespan;
                }

                //----------------------------------------
                if ((skipDuplicates) && (servicesToRegisterFirst != null) && (servicesToRegisterFirst.Exists(s => s.InterfaceType == interfaceType)))
                {
                    continue;
                }
                //----------------------------------------
                //We now have enough information to build a complete binding descriptor
                IBindingDescriptor serviceRegistrationDescriptor =
                    new BindingDescriptor(
                        BindingType.Interface,
                        interfaceType,
                        implementationType,
                        bindingLifetimeType,
                        null //NO Tag
                        );
                //----------------------------------------
                //Define the Priority:
                //Again, as we did for scope lifetime above, we search on the implementatino type,
                Priority priority = Priority.VeryLow;
                if (implementationType.IsSubClassOfEx(typeof(IHasLowBindingPriority)))
                {
                    priority = Priority.Low;
                }
                if (implementationType.IsSubClassOfEx(typeof(IHasMediumBindingPriority)))
                {
                    priority = Priority.Normal;
                }
                if (implementationType.IsSubClassOfEx(typeof(IHasHighBindingPriority)))
                {
                    priority = Priority.High;
                }

                //----------------------------------------
                results.Add(new PrioritizedBindingDescriptor(priority, serviceRegistrationDescriptor));
            }

            return results;
        }

        private static Type GetBestMatchForInterfaceType(Type implementationType, bool areDirectInterfaces, Type[] interfaceTypes)
        {
//From that mess of interfaces, we want the ISomeService interface
            //So we first whittle down the list to interfaces that implement IHasScope:
            Type[] allBindingScopedInterfaceTypes = interfaceTypes
// ReSharper disable UseMethodIsInstanceOfType
                .Where(x => typeof (IHasBindingScope).IsAssignableFrom(x))
// ReSharper restore UseMethodIsInstanceOfType
//IMPORTANT: Notice the ordering by length....
                .OrderByDescending(x => x.Name.Length)
                .ToArray();


            Type interfaceType = null;

            //----------
            //Normally best guess is one that matches naming by conventions (ie, SomeService = ISomeService)
            //But it can be something like
            //SomeValidator : ValidatorBase<IHasInterface>
            //where ValidatorBase: IHasScope
            if ((allBindingScopedInterfaceTypes.Length == 1) && (areDirectInterfaces))
            {
                interfaceType = allBindingScopedInterfaceTypes.FirstOrDefault();
            }
            //----------
            if (interfaceType == null)
            {
                //Best guess: SomeService = ISomeService 
                interfaceType = allBindingScopedInterfaceTypes.FirstOrDefault(
                    i => i.Name.Equals("I" + implementationType.Name, StringComparison.OrdinalIgnoreCase));
            }
            //----------
            //Next best guess: ends with SomeService
            //MyGreatSomeService matches ISomeService, 
            //if there was not a IGreatSomeService (which would have been the longer, and therefore preferred choice)
            if (interfaceType == null)
            {
            interfaceType =
                allBindingScopedInterfaceTypes.FirstOrDefault(
                    i => implementationType.Name.EndsWith(i.Name.Substring(1),
                        StringComparison.OrdinalIgnoreCase));
            }
            //----------
            //Next best guess:
            if (interfaceType == null)
            {
                //Here's a tricky part. Generic types, unless they are exactly alike, won't match
                //ie SomeService won't match ISomeService<int>, or SomeService<>
                interfaceType =
                    allBindingScopedInterfaceTypes.FirstOrDefault(
                        i => implementationType.Name.EndsWith(NameWithNoGenericSuffix(i).Substring(1),
                            StringComparison.OrdinalIgnoreCase));
            }
            if (interfaceType == null)
            {
                interfaceType =
                    allBindingScopedInterfaceTypes.FirstOrDefault(
                        i => NameWithNoGenericSuffix(implementationType).EndsWith(NameWithNoGenericSuffix(i).Substring(1),
                            StringComparison.OrdinalIgnoreCase));
            }

            return interfaceType;
        }

        private static string NameWithNoGenericSuffix(Type implementationType)
        {
            string result = implementationType.Name;
            int pos = result.IndexOf('`');

            if (pos < 0)
            {
                return result;
            }
            result = result.Substring(0,pos);
            return result;
        }


        private static string AlreadyRegisteredKey(IBindingDescriptor serviceRegistrationDescriptor)
        {
            string alreadyRegisteredKey =
                serviceRegistrationDescriptor.InterfaceType.AssemblyQualifiedName;

            if (!serviceRegistrationDescriptor.Tag.IsNullOrEmpty())
            {
                alreadyRegisteredKey += ":" + serviceRegistrationDescriptor.Tag;
            }
            return alreadyRegisteredKey;
        }


        private static IBindingDescriptor[] FlattenPreRegisterList(
            IEnumerable<IBindingDescriptorBase> servicesToRegisterBeforeXActLibServices)
        {

            List<IBindingDescriptor> results = new List<IBindingDescriptor>();
            if (servicesToRegisterBeforeXActLibServices == null)
            {
                //Getting out early helps with recursive registration:
                return results.ToArray();
            }

            foreach (
                IBindingDescriptorBase serviceRegistrationDescriptorBase in
                    servicesToRegisterBeforeXActLibServices)
            {

                if (serviceRegistrationDescriptorBase is IBindingDescriptorGroup)
                {

                    IBindingDescriptorGroup serviceBindingDescriptorGroup =
                        serviceRegistrationDescriptorBase as IBindingDescriptorGroup;

                    //Register children -- if any -- first:
                    results.AddRange(FlattenPreRegisterList(serviceBindingDescriptorGroup.ServiceBindings));

                    continue;
                }


                IBindingDescriptor serviceRegistrationDescriptor =
                    serviceRegistrationDescriptorBase as IBindingDescriptor;

                results.Add(serviceRegistrationDescriptor);

            }
            return results.ToArray();

        }


    }
}
