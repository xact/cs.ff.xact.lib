﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

// ReSharper restore CheckNamespace
#endif

/// <summary>
    /// Extensions to the KeyedCollection class.
    /// </summary>
    /// <internal><para>8/17/2011: Sky</para></internal>
    public static class KeyedCollectionExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Tries to get the specified Item in the collection.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="keyedCollection">The keyed collection.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <internal><para>8/17/2011: Sky</para></internal>
        public static bool TryGet<TKey, TValue>(this KeyedCollection<TKey, TValue> keyedCollection, TKey key,
                                                out TValue value)
        {
            keyedCollection.ValidateIsNotDefault("keyedCollection");
            if (EqualityComparer<TKey>.Default.Equals(key))
            {
                throw new ArgumentNullException("key");
            }

            if (keyedCollection.Contains(key))
            {
                value = keyedCollection[key];
                return true;
            }

            value = default(TValue);

            return false;
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
