﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using XAct.Collections;
    using XAct.Diagnostics;

    // ReSharper restore CheckNamespace


    /// <summary>
    /// Extensions to the <see cref="Assembly"/> class.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Example:
    /// <code>
    /// <![CDATA[
    /// Bitmap bitmap = 
    ///   Assembly
    ///     .GetExecutingAssembly()
    ///     .LoadBitmapFromResource("Resources.Progress.gif");
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public static class AssemblyExtensions
    {


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the Assembly's CompanyName value.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static string GetProductCompanyName(this Assembly assembly)
        {
            object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            return attributes.Length == 0 ? "" : ((AssemblyCompanyAttribute)attributes[0]).Company;
        }

        /// <summary> 
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// The copyright holder of the calling assembly. 
        /// </summary> 
        public static string GetProductCopyright(this Assembly assembly)
        {
            object[] attributes =
                assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            return attributes.Length == 0 ? "" : ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
        }

        /// <summary> 
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// The product name of the calling assembly. 
        /// </summary> 
        public static string GetProductName(this Assembly assembly)
        {
            object[] attributes =
                assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            return attributes.Length == 0 ? "" : ((AssemblyProductAttribute)attributes[0]).Product;
        }


        /// <summary> 
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// The title of the calling assembly. 
        /// </summary> 
        public static string GetProductTitle(this Assembly assembly)
        {
            object[] attributes = assembly.GetCustomAttributes(
                typeof(AssemblyTitleAttribute), false);

            if (attributes.Length > 0)
            {
                AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                if (titleAttribute.Title.Length > 0) return titleAttribute.Title;
            }

            //NOT PORTABLE:
            ////Get name from Exe.
            //return Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            return null;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Get's the product description
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static string GetProductDescription(this Assembly assembly)
        {
            //now get the customattribute for the AssemblyDescriptionAttribute
            object[] customAttributes = assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);

            return customAttributes.Length == 0 ? "" : ((AssemblyDescriptionAttribute)customAttributes[0]).Description;

        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the types decorated with attribute.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// Assembly assembly = Assembly.GetEntryAssembly();
        /// 
        /// Type[] results = 
        ///    assembly.GetTypesDecoratedWithAttribute
        ///      <DefaultServiceImplementationAttribute>(false);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assembly">The assembly.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns></returns>
        public static IDictionary<Type, T> GetTypesDecoratedWithAttribute<T>(this Assembly assembly,
                                                                             bool inherit = false)
            where T : Attribute
        {
            ReadOnlyDictionary<Type, T> results = new ReadOnlyDictionary<Type, T>();
            foreach (Type t in assembly.GetTypesDecoratedWithAttributes<T>(null, inherit))
            {
                T defaultServiceAttribute = t.GetFirstAttribute<T>(inherit);
                results.Add(t, defaultServiceAttribute);
            }
            results.IsReadOnly = true;

            return results;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets a list of the assemblies referenced by the given assembly.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// Assembly assembly = Assembly.GetEntryAssembly();
        /// 
        /// Asssembly[] results = 
        ///    assembly.GetReferencedAssemblies();
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static AssemblyName[] GetReferencedAssemblies(this Assembly assembly)
        {
            //AssemblyName[] assemblyNames = 
            AssemblyName[] results = assembly.GetReferencedAssemblies();
            //foreach (AssemblyName assemblyName in assemblyNames)
            //{
            //    Console.WriteLine(assemblyName.FullName);
            //}

            return results;
        }

        




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the first Type in the Assembly that ends with the given Type Name
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="typeName">Name of the type.</param>
        /// <returns></returns>
        /// <internal><para>7/26/2011: Sky</para></internal>
        public static Type GetTypeEndingWith(this Assembly assembly, string typeName)
        {
            Type t2;
            t2 = assembly.GetType(typeName, false);
            return t2 ??
                   assembly.GetTypes().FirstOrDefault(t => t.FullName.EndsWith("." + typeName, StringComparison.OrdinalIgnoreCase));
        }

        

        /// <summary>
        /// Gets the types decorated with attibutes.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="assembly">The assembly.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <param name="allowMultiples">if set to <c>true</c> [allow multiples].</param>
        /// <returns></returns>
        public static List<KeyValuePair<Type, TAttribute>> GetTypesDecoratedWithAttibutes<TAttribute>(this Assembly assembly, Func<TAttribute, bool> filter, bool inherit, bool allowMultiples)
    where TAttribute : Attribute
        {

            List<KeyValuePair<Type, TAttribute>> results = new List<KeyValuePair<Type, TAttribute>>();

            foreach (Type type in assembly.GetTypesDecoratedWithAttributes<TAttribute>(filter, inherit))
            {
                foreach (TAttribute attribute in type.GetCustomAttributes(typeof(TAttribute), inherit))
                {
                    results.Add(new KeyValuePair<Type, TAttribute>(type, attribute));

                    //We have each Type...but the Type could have several copies of the Attribute
                    if (!allowMultiples)
                    {
                        break;
                    }
                }
            }
            return results;
        }






        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets all public types in the assembly that are decorated with the specified attribute.
        /// <para>
        /// See also: <c>GetTypesImplementingType</c>
        /// </para>
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="assembly">The assembly.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns></returns>
        /// <internal>8/15/2011: Sky</internal>
        public static Type[] GetTypesDecoratedWithAttributes<TAttribute>(this Assembly assembly,
            Func<TAttribute, bool> filter = null,
            bool inherit = true)
            where TAttribute :class
        {

            Type attributeType = typeof (TAttribute);

            //FIX: See: http://bit.ly/tL84yI
#if NET35
            if (assembly.ManifestModule.GetType().Namespace == "System.Reflection.Emit")
#else
            if (assembly.IsDynamic)
#endif
            {
                return new Type[0];
            }

            List<Type> tmp = new List<Type>();
            try
            {
                Type[] types;

                try
                {
                    types = assembly.GetExportedTypesSafely();
                }
                catch
                {
                    types = new Type[0];
                    return types;
                }

                try
                {
                    Type atttributeType = typeof(TAttribute);

                    foreach (Type type in types)
                    {
                        //If any, the objects will all be of type 'attributeType'
                        object[] foundAttributes = type.GetCustomAttributes(atttributeType, inherit);
                        
                        if (foundAttributes.Length == 0)
                        {
                            //Doesn't have a single one, so this is not what we are looking for:
                            continue;
                        }

                        //Does, as long as filter doesn't knock it out:
                        bool ok = true;

                        if (filter != null)
                        {
                            foreach (object foundAttributeObject in foundAttributes)
                            {
                                //Type the Attribute:
                                TAttribute foundAttribute = foundAttributeObject as TAttribute;
                                
                                if (foundAttribute == null)
                                {
                                    //Not sure this is possible, but just in case,
                                    //we don't want it:
                                    ok = false;
                                    continue;
                                }
                                else
                                {
                                    if (!filter.Invoke(foundAttribute))
                                    {
                                        //Not passing filter:
                                        ok=false;
                                        //Break out of checking attributes 
                                        //as we don't want this object
                                        break;
                                    }
                                }
                            }
                        }

                        //Was correct type,
                        //has right attributes
                        //none of the attributes caused the filter to exclude it:
                        if (!ok)
                        {
                            continue;
                        }
                        tmp.Add(type);
                    }
                }
                catch
                {
                    //System.Diagnostics.Debug.WriteLine(e.Message);
                }
            }
#pragma warning disable 168
            catch (System.TypeLoadException e)
#pragma warning restore 168
            {
                //Error is expected from some assemblies.
                //eg: Could not load type 'Microsoft.AspNet.SignalR.Hosting.IWebSocketRequest' from 
                //assembly 'Microsoft.AspNet.SignalR.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'.
                //TraceExceptionIfTracingServiceAvailable(TraceLevel.Verbose, e, "Eating error.");
            }
#pragma warning disable 168
            catch
#pragma warning restore 168
            {
                //TraceExceptionIfTracingServiceAvailable(TraceLevel.Verbose, e, "Eating error.");
            }
            Type[] results =
                tmp.ToArray();

            return results;
        }


        public static Type[] GetExportedTypesSafely(this Assembly assembly)
        {

            Type[] types;
            if (_errorAssemblies.Contains(assembly.FullName))
            {
                types = new Type[0];
                return types;
            }

            try
            {
                types = assembly.GetExportedTypes();
            }
#pragma warning disable 168
            catch (Exception e)
#pragma warning restore 168
            { 
                //Often will be FileNotFoundException.
                types = new Type[0];
                _errorAssemblies.Add(assembly.FullName);
            }
            return types;
        }

        static List<string> _errorAssemblies = new List<string>();

        public static Type[] GetAllTypesImplementingOpenGenericType<TOpenGenericType>(this Assembly assembly)
        {
            return assembly.GetAllTypesImplementingOpenGenericType(typeof(TOpenGenericType));
        }


        public static Type[] GetAllTypesImplementingOpenGenericType(this Assembly assembly, Type openGenericType)
        {
            var results =
            assembly.GetTypes().Where(x =>
                //As Assembly open returns closed types, and we are looking for open, must have parent to qualify...
            (x.BaseType != null)
            &&
                //The basetype must be Generic. (what about parent, parent?)
            (x.BaseType.IsGenericType)
            && 
            (!x.IsAbstract)
            &&
            //
            (
            (openGenericType.IsAssignableFrom(x.BaseType.GetGenericTypeDefinition()))
            ||
            (x.GetInterfaces().Any(y => openGenericType.IsAssignableFrom(y)))
            )
            ).Distinct().ToArray();

            return results;
        }


        /// <summary>
        /// Gets an array of Types in the assembly that inherit from the specified type.
        /// 
        /// <para>
        /// <code>
        /// <![CDATA[
        /// var results = assembly.GetTypesImplementingType<IHasScope>(true);
        /// //result contains a list of instantiable classes that all implement IHasScope.
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TInterface"></typeparam>
        /// <param name="assembly"></param>
        /// <param name="instantiableOnly"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static Type[] GetTypesImplementingType<TInterface>(this Assembly assembly, bool instantiableOnly = true,
                                                                   Func<Type, bool> filter = null)
        {

            return assembly.GetTypesImplementingType(typeof (TInterface), instantiableOnly, filter);
        }


        /// <summary>
        /// Gets an array of Types in the assembly that inherit from the specified type.
        /// <para><code><![CDATA[
        /// var results = assembly.GetTypesImplementingType<IHasScope>(true);
        /// //result contains a list of instantiable classes that all implement IHasScope.
        /// ]]></code></para>
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="instantiableOnly">if set to <c>true</c> [instantiable only].</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static Type[] GetTypesImplementingType(this Assembly assembly, Type interfaceType, bool instantiableOnly = true,
                                                                   Func<Type, bool> filter = null)
        {
            List<Type> tmp = new List<Type>();

            Type[] types;


#if NET35
            if (assembly.ManifestModule.GetType().Namespace == "System.Reflection.Emit")
#else
            if (assembly.IsDynamic)
#endif
            {
                return tmp.ToArray();
            }
            try
            {
                //Get all public (and private) types:
                types = assembly.GetExportedTypesSafely();
            }
#pragma warning disable 168
            catch (System.Exception e)
#pragma warning restore 168
            {
                //throw;
                types = new Type[0];
                return tmp.ToArray();
            }

            //Iterate through types:
            foreach (Type type in types)
            {
                if (!type.IsSubClassOfEx(interfaceType))
                {
                    continue;
                }
                if (instantiableOnly && type.IsAbstract)
                {
                    //Generally looking for a class instance, not the interface or abstract:
                    continue;
                }
                //Filter it: 
                if (filter != null)
                {
                    if (!filter.Invoke(type))
                    {
                        continue;
                    }
                }
                tmp.Add(type);

            }
            //Done:
            return tmp.ToArray();
        }







        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets all public types in the assembly that are decorated with the specified attribute.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns></returns>
        /// <internal>8/15/2011: Sky</internal>
        public static Type[] GetTypesDecoratedWithAttributes(this Assembly assembly, Type attributeType,
                                                             Func<Attribute, bool> filter = null,
                                                             bool inherit = true)
        {

            List<Exception> exceptions = new List<Exception>();
            //FIX: See: http://bit.ly/tL84yI
#if NET35
            if (assembly.ManifestModule.GetType().Namespace == "System.Reflection.Emit")
#else
            if (assembly.IsDynamic)
#endif
            {
                return new Type[0];
            }

            List<Type> tmp = new List<Type>();

            Type[] types;
            try
                {
                    types = assembly.GetExportedTypesSafely();
                }
                catch
                {
                    types = new Type[0];
                }

            try {
                    foreach (Type type in types)
                    {
                        object[] foundAttributes = type.GetCustomAttributes(attributeType, inherit);
                        if (foundAttributes.Length == 0)
                        {
                            continue;
                        }

                        //Potentially filter out the attribute:
                        //Heavy...but can't see way of passing Attribute directly
                        if (filter != null)
                        {
                            foreach (object foundAttributeObject in foundAttributes)
                            {
                                Attribute foundAttribute = foundAttributeObject as Attribute;
                                if (!filter.Invoke(foundAttribute))
                                {
                                    continue;
                                }
                            }
                        }


                        tmp.Add(type);
                    }
            }
            catch (System.TypeLoadException e)
            {
                exceptions.Add(e);
                //Error is expected from some assemblies.
                //eg: Could not load type 'Microsoft.AspNet.SignalR.Hosting.IWebSocketRequest' from 
                //assembly 'Microsoft.AspNet.SignalR.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'.
                //TraceExceptionIfTracingServiceAvailable(TraceLevel.Verbose, e, "Eating error.");
            }
                //Suppossedly catch(Exception) does not catch unmamanaged.
            catch //(System.Exception e)
            {
                //exceptions.Add(e);
                //TraceExceptionIfTracingServiceAvailable(TraceLevel.Verbose, e, "Eating error.");
            }

            //out Exception[] exceptionsRaisedIfAny
            //exceptionsRaisedIfAny = exceptions.ToArray();

            Type[] results = tmp.ToArray();

            return results;
        }












        //static void TraceExceptionIfTracingServiceAvailable(TraceLevel traceLevel, System.Exception e, string message, params object[] args)
        //{
        //    //Seems to still blow up.
        //    ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>(false); ;
        //    if (tracingService==null){return;}
        //    tracingService.TraceException(TraceLevel.Warning, e, message, args);
        //}


    }

}
