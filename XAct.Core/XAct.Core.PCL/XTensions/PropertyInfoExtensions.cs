
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

// ReSharper restore CheckNamespace
#endif

//using System.Data.Linq.Mapping;

    /// <summary>
    ///   Extensions to the <see cref = "PropertyInfo" /> object.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class PropertyInfoExtensions
    {
        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the specified attribute or default (null).
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="propertyInfo">The property info.</param>
        /// <param name="defaultAttribute">The default attribute.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns></returns>
        public static TAttribute GetAttributeOrDefault<TAttribute>(this PropertyInfo propertyInfo, TAttribute defaultAttribute, bool inherit=false)
            where TAttribute :Attribute
        {
            return propertyInfo.GetCustomAttributes(typeof(TAttribute), inherit).FirstOrDefault() as TAttribute ?? defaultAttribute;
        }


        /// <summary>
        /// Gets Attributes on the given <see cref="PropertyInfo" />.
        /// <para>
        /// The Default System has a recurse <see cref="bool" /> flag,
        /// but it doesn't do anything: no recursion takes place on
        /// <see cref="MethodInfo" /> or <see cref="PropertyInfo" /> objects.
        /// </para>
        /// <para>
        /// This is a known issue. See: http://hyperthink.net/blog/getcustomattributes-gotcha/
        /// </para>
        /// <para>
        /// This method does does.
        /// </para>
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="propertyInfo">The property information.</param>
        /// <param name="recurse">if set to <c>true</c> [recurse].</param>
        /// <returns></returns>
        public static TAttribute GetAttributeRecursively<TAttribute>(this PropertyInfo propertyInfo, bool recurse = true)
            where TAttribute : Attribute
        {
            return (TAttribute) propertyInfo.GetAttributeRecursively(typeof (TAttribute), recurse);
        }

        public static Attribute GetCustomAttribute(this PropertyInfo propertyInfo, Type attributeType, bool inherit = true)
        {
            Attribute[] resultAttributes = propertyInfo.GetCustomAttributes(attributeType, inherit) as Attribute[];

            if ((resultAttributes == null) || (resultAttributes.Length == 0))
            {
                return null;
            }

            //Attribute resultAttribute = propertyInfo.GetCustomAttribute(attributeType, recurse);
            Attribute resultAttribute = resultAttributes.First();

            return resultAttribute;

        }




        /// <summary>
        /// Gets Attributes on the given <see cref="PropertyInfo" />.
        /// <para>
        /// The Default System has a recurse <see cref="bool" /> flag,
        /// but it doesn't do anything: no recursion takes place on
        /// <see cref="MethodInfo" /> or <see cref="PropertyInfo" /> objects.
        /// </para>
        /// <para>
        /// This is a known issue. See: http://hyperthink.net/blog/getcustomattributes-gotcha/
        /// </para>
        /// <para>
        /// This method does does.
        /// </para>
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <param name="recurse">if set to <c>true</c> [recurse].</param>
        /// <returns></returns>
        public static Attribute GetAttributeRecursively(this PropertyInfo propertyInfo, Type attributeType, bool recurse = true)
        {
            //Get the attributes on this Method:

            Attribute resultAttribute = propertyInfo.GetCustomAttribute(attributeType, recurse);

            //If there are any attibutes on this object, certainly no need to recurse higher
            //as it's expensive:
            if ((!recurse) || (resultAttribute != null))
            {
                return resultAttribute;
            }

            //But if we must...
            foreach (Type parentClassType in propertyInfo.DeclaringType.GetParentTypes())
            {

                //The Source property we are trying to match upon parent Methods, 
                //has parameters:

                //Which we use as filters for properties on the parent element.
                PropertyInfo parentPropertyInfo =
                    parentClassType.GetProperty(propertyInfo.Name, propertyInfo.PropertyType);


                if (parentPropertyInfo == null)
                {
                    //It's possible that this parent/anscestor 
                    //type doesn't sport this property:
                    continue;
                }


                //But if it does, get its attributes:
                resultAttribute = parentPropertyInfo.GetCustomAttribute(attributeType);

                if (resultAttribute != null)
                {
                    //Found one (and even if there ones on higher ones, this overrides them):
                    return resultAttribute;
                }
            }
            return null;
        }








        /// <summary>
        /// Gets Attributes on the given <see cref="PropertyInfo" />.
        /// <para>
        /// The Default System has a recurse <see cref="bool" /> flag,
        /// but it doesn't do anything: no recursion takes place on
        /// <see cref="MethodInfo" /> or <see cref="PropertyInfo" /> objects.
        /// </para>
        /// <para>
        /// This is a known issue. See: http://hyperthink.net/blog/getcustomattributes-gotcha/
        /// </para>
        /// <para>
        /// This method does does.
        /// </para>
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <param name="recurse">if set to <c>true</c> [recurse].</param>
        /// <returns></returns>
        public static Attribute[] GetAttributesRecursively(this PropertyInfo propertyInfo, Type attributeType,
                                                           bool recurse = true, bool allowDescendents = true)
        {
            //Get the attributes on this Method:


            List<Attribute> resultAttributes = allowDescendents
                                                   ? propertyInfo.GetCustomAttributes(true).Where(x => attributeType.IsAssignableFrom(x.GetType())).Cast<Attribute>().ToList()
                                                   : propertyInfo.GetCustomAttributes(attributeType, recurse).Cast<Attribute>().ToList();


            //                                                   ? propertyInfo.GetCustomAttributes().Where(x=>x.GetType().IsSubclassOf(attributeType)).ToList()

            //If there are any attibutes on this object, certainly no need to recurse higher
            //as it's expensive:
            if (!recurse)
            {
                return resultAttributes.ToArray();
            }

            //But if we must...
            foreach (Type parentClassType in propertyInfo.DeclaringType.GetParentTypes())
            {

                //The Source property we are trying to match upon parent Methods, 
                //has parameters:

                //Which we use as filters for properties on the parent element.
                PropertyInfo parentPropertyInfo =
                    parentClassType.GetProperty(propertyInfo.Name, propertyInfo.PropertyType);


                if (parentPropertyInfo == null)
                {
                    //It's possible that this parent/anscestor 
                    //type doesn't sport this property:
                    continue;
                }


                //But if it does, get its attributes:
                // ReSharper disable UseMethodIsInstanceOfType
                //IsAssignableFrom seems to produce better results:
                IEnumerable<Attribute> found = parentPropertyInfo.GetCustomAttributes(true).Where(x => attributeType.IsAssignableFrom(x.GetType())).Cast<Attribute>();
                // ReSharper restore UseMethodIsInstanceOfType

                if (found != null)
                {
                    resultAttributes.AddRange(found);
                    //Found one (and even if there ones on higher ones, this overrides them):
                    return resultAttributes.ToArray();
                }
            }

            var results = resultAttributes.ToArray();
            return results;
        }





        public static bool IsGetterPublic(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetGetMethod() ?? propertyInfo.GetGetMethod(true);
            if (pi == null) { return false; }
            return pi.IsPublic();
        }
        public static bool IsGetterInternal(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetGetMethod() ?? propertyInfo.GetGetMethod(true);
            if (pi == null) { return false; }
            return pi.IsInternal();
        }
        public static bool IsGetterProtected(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetGetMethod() ?? propertyInfo.GetGetMethod(true);
            if (pi == null) { return false; }
            return pi.IsProtected();
        }
        public static bool IsGetterPrivate(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetGetMethod() ?? propertyInfo.GetGetMethod(true);
            if (pi == null) { return false; }
            return pi.IsPrivate();
        }
        public static bool IsGetterAbstract(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetGetMethod() ?? propertyInfo.GetGetMethod(true);
            if (pi == null) { return false; }
            return pi.IsAbstract();
        }
        public static bool IsGetterPublicAbstract(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetGetMethod() ?? propertyInfo.GetGetMethod(true);
            if (pi == null) { return false; }
            return pi.IsAbstract() && pi.IsPublic();
        }





        public static bool IsSetterPublic(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetSetMethod() ?? propertyInfo.GetSetMethod(true);
            if (pi == null) { return false; }
            return pi.IsPublic();
        }
        public static bool IsSetterInternal(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetSetMethod() ?? propertyInfo.GetSetMethod(true);
            if (pi == null) { return false; }
            return pi.IsInternal();
        }
        public static bool IsSetterProtected(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetSetMethod() ?? propertyInfo.GetSetMethod(true);
            if (pi == null) { return false; }
            return pi.IsProtected();
        }
        public static bool IsSetterPrivate(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetSetMethod() ?? propertyInfo.GetSetMethod(true);
            if (pi == null) { return false; }
            return pi.IsPrivate();
        }
        public static bool IsSetterAbstract(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetSetMethod() ?? propertyInfo.GetSetMethod(true);
            if (pi == null) { return false; }
            return pi.IsAbstract();
        }
        public static bool IsSetterPublicAbstract(this PropertyInfo propertyInfo)
        {
            var pi = propertyInfo.GetSetMethod() ?? propertyInfo.GetSetMethod(true);
            if (pi == null) { return false; }
            return pi.IsAbstract() && pi.IsPublic();
        }















        public static bool IsPublicAbstract(this PropertyInfo propertyInfo)
        {
            return propertyInfo.IsGetterPublicAbstract() || propertyInfo.IsSetterPublicAbstract();
        }

        public static bool IsPublic(this PropertyInfo propertyInfo)
        {
            return propertyInfo.IsGetterPublic() || propertyInfo.IsSetterPublic();
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
