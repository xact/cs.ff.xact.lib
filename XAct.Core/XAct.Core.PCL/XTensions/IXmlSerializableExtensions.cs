﻿//using System.IO;

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    ///   Extension methods to objects that 
    ///   implement the <see cref = "IXmlSerializable" />
    ///   contract.
    /// </summary>
// ReSharper disable InconsistentNaming
    public static class IXmlSerializableExtensions
// ReSharper restore InconsistentNaming
    {
        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;



        /// <summary>
        ///   Serialize the object and return an string of xml.
        /// </summary>
        /// <param name = "objectToSerialize">The object to serialize.</param>
        /// <returns>A string of xml.</returns>
        //[SuppressMessage("Microsoft.Naming", "CA1720", Justification = "objectToSerialize is more descriptive than 'value'.")]
        public static string ToXmlString(this IXmlSerializable objectToSerialize)
        {
            objectToSerialize.ValidateIsNotDefault("objectToSerialize");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            StringBuilder stringBuilder = new StringBuilder();

            using (StringWriter stringWriter = new StringWriter(stringBuilder, _invariantCulture))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter))
                {
                    objectToSerialize.WriteXml(xmlWriter);
                }
            }
            return stringBuilder.ToString();
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
