﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;
    using XAct.Text.RegularExpressions;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   String Extensions using Regex.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class StringRegexExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Uses RegularExpression Pattern to search/replace.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "pattern"></param>
        /// <param name = "replacementText"></param>
        /// <returns></returns>
        public static string Replace(this string text, string pattern, string replacementText)
        {
            try
            {
                Regex tR = new Regex(pattern);
                return tR.Replace(text, replacementText);
            }
            catch
            {
                return text;
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Example of Regex function to find several occurances in a string.
        /// </summary>
        /// <param name = "text"></param>
        /// <param name = "pattern"></param>
        /// <param name = "matchGroupNumber"></param>
        /// <param name = "results"></param>
        /// <returns></returns>
        public static bool MatchMulti(this string text, string pattern, int matchGroupNumber, out string[] results)
        {
            results = null;
            try
            {
                bool tResult = false;
                if (text.IsNullOrEmpty())
                {
                    return false;
                }

                //Example pattern: "(?<number>\\d+)"
                Regex digitregex = new Regex(pattern);
                //String s = "abc 123 def 456 ghi 789";


                MatchCollection mc = digitregex.Matches(text);

                if (mc.Count > 0)
                {
                    tResult = true;
                    results = new string[mc.Count];

                    for (int i = 0; i < mc.Count; i++)
                    {
                        results[i] = mc[i].Groups[matchGroupNumber].Value;
                        //tResults.Add(oMatch.Value );
                    }
                }
                return tResult;
            }
            catch (Exception e)
            {
                throw new ArgumentException(
                    "Could not Regex the string with string and pattern given: {0}".FormatStringCurrentUICulture(pattern),
                    e);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Search given string for one or more pattern matches.
        /// </summary>
        /// <param name = "text">String to Analyse.</param>
        /// <param name = "pattern">Regex Pattern to use.</param>
        /// <returns>Match collection.</returns>
        public static Match FindMatches(this string text, string pattern)
        {
            try
            {
                Regex regex = new Regex(pattern, RegexOptions.IgnoreCase & RegexOptions.Multiline);
                return regex.Match(text);
            }
            catch (Exception e)
            {
                throw new ArgumentException(
                    "Could not Regex the string with string and pattern given: {0}".FormatStringCurrentUICulture(pattern),
                    e);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts a wildcard to a regex.
        /// </summary>
        /// <param name = "pattern">The wildcard pattern to convert.</param>
        /// <returns>A regex equivalent of the given wildcard.</returns>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if <paramref name = "pattern" />
        ///   is null.
        /// </exception>
        public static string WildcardPatternToRegexPattern(this string pattern)
        {
            if (pattern.IsNullOrEmpty())
            {
                throw new ArgumentNullException("pattern");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            return
                "^" + Regex.Escape(pattern).
                          Replace("\\*", ".*").
                          Replace("\\?", ".") + "$";
        }


        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether [is in regex list] [the specified source].
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="patterns">The patterns.</param>
        /// <param name="regexOptions">The regex options.</param>
        /// <returns>
        ///   <c>true</c> if [is in regex list]
        /// [the specified source]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// An exception is raised if <paramref name="source"/>
        /// is null.
        ///   </exception>
        public static bool IsInRegexList(this string source, string[] patterns, RegexOptions regexOptions = RegexOptions.IgnoreCase)
        {
            if (source.IsNullOrEmpty())
            {
                throw new ArgumentNullException("source");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            return patterns != null && patterns.Any(p => Regex.Match(source, p, regexOptions).Success);
        }


        /// <summary>
        /// Does a regular expression match on the given string.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="pattern">The pattern.</param>
        /// <param name="regexOptions">The regex options.</param>
        /// <returns></returns>
        public static bool RegexMatch(this string source, string pattern, RegexOptions regexOptions = RegexOptions.CultureInvariant | RegexOptions.IgnoreCase)
        {
            if (source.IsNullOrEmpty())
            {
                throw new ArgumentNullException("source");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            if (pattern.IsNullOrEmpty())
            {
                return false;
            }

            return Regex.Match(source, pattern, regexOptions).Success;
            
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether [is A match] [the specified source].
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <param name = "pattern">The pattern.</param>
        /// <returns>
        ///   <c>true</c> if [is A match] [the specified source]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAMatch(this string source, string pattern)
        {
            return Regex.IsMatch(source, pattern);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the given string is in the valie yyyy-mm-dd format,
        ///   within the range of 1999-01-01 and 2099-12-12.
        ///   <para>
        ///     Important: there are better ways of validating a date string.
        ///   </para>
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns>
        ///   <c>true</c> if the source matches the expected pattern; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidDate(string source)
        {
            return Regex.Match(source, CommonRegularExpressionPatterns.Validation.DateValidation).Success;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the source text is in the format of a valid, unsigned, integer.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns>
        ///   <c>true</c> if the source matches the expected pattern; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidInteger(string source)
        {
            return
                Regex.Match(source, CommonRegularExpressionPatterns.CodeAnalysis.IntFragment).Success;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the source text is in the format of a valid, unsigned, integer.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns>
        ///   <c>true</c> if the source matches the expected pattern; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidUnsignedInteger(string source)
        {
            return
                Regex.Match(source, CommonRegularExpressionPatterns.CodeAnalysis.UIntFragment).Success;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the source text is in the format of a valid variable name.
        /// </summary>
        /// <param name = "source">The source.</param>
        /// <returns>
        ///   <c>true</c> if the source matches the expected pattern; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidVariableName(string source)
        {
            return
                Regex.Match(source, CommonRegularExpressionPatterns.CodeAnalysis.VariableName).Success;
        }
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
