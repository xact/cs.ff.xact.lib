﻿
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
using XAct.Services;

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Serialization;
    using XAct.Collections;
    using XAct.Collections.Comparers;
    using XAct.Reflection;
    using XAct.Services;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// Extensions to the <see cref="Type" /> object.
    /// </summary>
    /// <internal>
    /// FAQ: Why Extensions?
    /// See Tips Folder for a Readme.txt on the subject.
    ///   </internal>
    public static class TypeExtensions
    {
        private static IConversionService ConversionService
        {
            get { return DependencyResolver.Current.GetInstance<IConversionService>(); }
        }



        /// <summary>
        /// Ensures all public methods are marked virtual.
        /// <para>
        /// Used by Unity Interception, to ensure AOP by proxies can be done.</para>
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static bool EnsureAllPublicMethodsAreMarkedVirtual(this Type type)
        {
            MethodInfo[] methodsNotMarkedVirtual = type.GetMethods()
                                                       .Where(mi => (mi.IsPublic) && (mi.IsVirtual) && (mi.IsFinal))
                                                       .ToArray();

            return methodsNotMarkedVirtual.Length <= 0;
        }




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the specified entity type is enumerable.
        /// </summary>
        /// <param name = "entityType">The entity type.</param>
        /// <returns>
        ///   <c>true</c> if the specified entity type is enumerable; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEnumerable(this Type entityType)
        {
            return typeof (IEnumerable).IsAssignableFrom(entityType);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the type without nullability.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Type GetTypeWithoutNullability(this Type type)
        {
            return type.IsNullable() ? Nullable.GetUnderlyingType(type) : null;
        }





        //private static readonly CultureInfo _invariantCulture =
        //    CultureInfo.CurrentCulture;

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Creates the specified type.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="type">The type.</param>
        /// <param name="args">The optional constructor args.</param>
        /// <returns></returns>
        public static TObject Create<TObject>(this Type type, params object[] args)
        {
            return args == null
                       ? (TObject) Activator.CreateInstance(type)
                       : (TObject) Activator.CreateInstance(type, args);
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Verifies that an argument type is assignable from the provided type (meaning
        ///   interfaces are implemented, or classes exist in the base class hierarchy).
        /// <para>
        /// Example:
        /// <code>
        /// <![CDATA[
        /// class MyClass : IMyClass {}
        /// var x = myClass;
        /// var b = x.AssignableFrom(typeof(IMyClass));
        /// //true
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name = "assignee">The argument type.</param>
        /// <param name = "providedType">The type it must be assignable from.</param>
        [SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1")]
        public static void AssignableFrom(this Type assignee, Type providedType)
        {
            assignee.ValidateIsNotDefault("assignee");
            providedType.ValidateIsNotDefault("providedType");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
        Contract.EndContractBlock();
#endif

            if (!providedType.IsAssignableFrom(assignee))
            {
                throw new ArgumentException(
                    "Type not assignable: {0} ".FormatStringExceptionCulture(assignee));

                //throw new ArgumentException(Constants.TypeNotCompatible.FormatStringExceptionCulture(assignee, providedType), argumentName);
            }
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the property is unitialized.
        /// <para>
        /// Eg, null, or 0, or Enum value of 0.
        /// </para>
        /// </summary>
        /// <param name = "argument"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Performance", "CA1800", Justification = "Ok code.")]
        public static bool IsUnitialized(this object argument)
        {
            if (argument == null)
            {
                return true;
            }

            bool isNullOrEmpty;
            Type type = argument.GetType();

            if (!type.IsValueType)
            {
                //String:
                if ((argument is string) && (((string) argument).Length == 0))
                {
                    isNullOrEmpty = true;
                }
                else
                {
                    isNullOrEmpty = false;
                }
            }
            else
            {
                object defaultValue = Activator.CreateInstance(type);
                isNullOrEmpty = defaultValue.Equals(argument);
            }
            return isNullOrEmpty;
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the specified type can be considered numeric.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// 	<c>true</c> if the specified typet is numeric; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNumeric(this Type type)
        {
            Type type2 = type.GetTypeWithoutNullability();

            return
                type2 == typeof (Int16) ||
                type2 == typeof (Int32) ||
                type2 == typeof (Int64) ||
                type2 == typeof (UInt16) ||
                type2 == typeof (UInt32) ||
                type2 == typeof (UInt64) ||
                type2 == typeof (decimal) ||
                type2 == typeof (float) ||
                type2 == typeof (double);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Gets the properties of the type.
        /// </summary>
        /// <param name = "thisEntityType">Type of the entity.</param>
        /// <param name = "attributeType">Type of the attribute to filter by.</param>
        /// <returns>The <see cref = "PropertyInfo" /> instances.</returns>
        public static IEnumerable<PropertyInfo> GetProperties2(this Type thisEntityType, BindingFlags bindingFlags = BindingFlags.Instance|BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.DeclaredOnly)
        {
            return thisEntityType.GetProperties(bindingFlags);
        }

        public static IEnumerable<MethodInfo> GetMethods2(this Type thisEntityType, BindingFlags bindingFlags = BindingFlags.Instance|BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.DeclaredOnly)
        {
            return thisEntityType.GetMethods(bindingFlags);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Gets the properties of the type, that have the specified attribute.
        /// </summary>
        /// <param name = "thisEntityType">Type of the entity.</param>
        /// <param name = "attributeType">Type of the attribute to filter by.</param>
        /// <returns>The <see cref = "PropertyInfo" /> instances.</returns>
        public static IEnumerable<PropertyInfo> GetPropertiesWithAttribute(this Type thisEntityType, Type attributeType)
        {
            return thisEntityType.GetProperties().Where(propertyInfo => propertyInfo.HasAttribute(attributeType));
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Search for a resource value.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     The format is PropertyName {FQTN}: 
        ///     <code>
        ///       <![CDATA[
        /// <myvar label="MyPropertyName {MyClass, MyAssembly}"/>
        /// <myvar label=""/>
        /// ]]>
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "fullPropertyName"></param>
        /// <param name = "result"></param>
        /// <returns></returns>
        public static bool GetReferencedPropertyValue<T>(
            string fullPropertyName, out T result)
        {
            if (String.IsNullOrEmpty(fullPropertyName))
            {
                result = default(T);
                return false;
            }


            const string pattern = @"\s*([^\s{]+)\s*{\s*([^}]+)\s*}\s*$";
            Match match =
                Regex.Match(fullPropertyName, pattern);
            if (!match.Success)
            {
                result = default(T);
                return false;
            }

            string fieldName = match.Groups[1].Value;
            string typeAndAssemblyName = match.Groups[2].Value;


            Type type = Type.GetType(typeAndAssemblyName, false);
            if (type == null)
            {
                result = default(T);
                return false;
            }


            //Find the static property within the class:
            PropertyInfo pInfo =
                type.GetProperty(fieldName,
                                 BindingFlags.Public |
                                 BindingFlags.Static |
                                 BindingFlags.IgnoreCase);

            if ((pInfo != null) && (pInfo.CanRead))
            {
                result = (T) pInfo.GetValue(null, null);
                return true;
            }


            //Find the static field within the class:
            FieldInfo fInfo =
                type.GetField(fieldName,
                              BindingFlags.Public |
                              BindingFlags.Static |
                              BindingFlags.IgnoreCase);

            if (fInfo != null)
            {
                // ReSharper disable AssignNullToNotNullAttribute
                result = (T) fInfo.GetValue(null);
                // ReSharper restore AssignNullToNotNullAttribute
                return true;
            }

            result = default(T);
            return false;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the given type is a subclass of the given superclass type.
        ///   <para>
        ///     Example: typeof(Example).IsSubClassOf(typeof(Example));//true
        ///     Example: typeof(Example).IsSubClassOf(typeof(IExample));//true
        ///     Example: typeof(IExample).IsSubClassOf(typeof(Example));//false
        ///   </para>
        /// <para>
        /// Note that this is just a wrapper around IsAssignableFrom.
        /// </para>
        /// </summary>
        /// <param name = "childSubClassType">Type of the child sub class.</param>
        /// <param name = "parentBaseClass">The parent base class.</param>
        /// <returns>
        ///   <c>true</c> if [is A subclass of] [the specified child sub class type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSubClassOfEx(this Type childSubClassType, Type parentBaseClass)
        {
            return parentBaseClass.IsAssignableFrom(childSubClassType);
        }




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the default value for a type
        /// (equivalent of default(T).
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public static object GetDefault(this Type type)
        {
            return (type.IsValueType) ? Activator.CreateInstance(type) : null;
        }




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the specified type is nullable.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// 	<c>true</c> if the specified type is nullable; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullable(this Type type)
        {
            return type.IsGenericType &&
                   type.GetGenericTypeDefinition() == typeof (Nullable<>);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the specified type is static.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// 	<c>true</c> if the specified type is static; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsStatic(this Type type)
        {
            ConstructorInfo[] constructors = type.GetConstructors();
            return (type.IsAbstract && type.IsSealed && constructors.Length == 0);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the attribute if any.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="type">The type.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns></returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public static TAttribute GetFirstAttribute<TAttribute>(this Type type, bool inherit = true)
            where TAttribute : Attribute
        {
            Type attributeType = typeof (TAttribute);

            object[] objects = type.GetCustomAttributes(attributeType, inherit);
            if (objects.Length == 0)
            {
                return null;
            }
            return objects[0] as TAttribute;
        }




        /// <summary>
        /// Gets the type's fullname in a way that is usable by 
        /// MonoCSharpHost.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static string FullNameHack(this Type type)
        {

            string fullName = type.FullName;

            //TODO: This is an awful hack:
            if (type.IsGenericType)
            {
                fullName = fullName.Split(',')[0];
                fullName = fullName.Replace("`1[[", "<");
                fullName += ">";
            }


            return fullName;
        }



        /// <summary>
        /// Determines whether [is anonymous type] [the specified type].
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if [is anonymous type] [the specified type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAnonymous(this Type type)
        {
            bool hasCompilerGeneratedAttribute =
                type.GetCustomAttributes(typeof (CompilerGeneratedAttribute), false).Count() > 0;
            bool nameContainsAnonymousType = type.FullName.Contains("AnonymousType");
            bool isAnonymousType = hasCompilerGeneratedAttribute && nameContainsAnonymousType;

            return isAnonymousType;
        }

        /// <summary>
        /// Tries to parse the string into the desired format.
        /// <para>
        /// Note that this is not the same using ConvertTo (which doesn't parse).
        /// </para>
        /// <para>
        /// Can handle String, Int, Double, DateTime.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stringVal">The string val.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static T TryParse<T>(this string stringVal, T defaultValue)
        {

            if (typeof (DateTime).IsAssignableFrom(typeof (T)))
            {
                DateTime tmpInt;
                return (DateTime.TryParse(stringVal, out tmpInt)) ? (T) (object) tmpInt : defaultValue;
            }
            if (typeof (double).IsAssignableFrom(typeof (T)))
            {
                double tmpInt;
                return (Double.TryParse(stringVal, out tmpInt)) ? (T) (object) tmpInt : defaultValue;
            }
            if (typeof (int).IsAssignableFrom(typeof (T)))
            {
                int tmpInt;
                return (Int32.TryParse(stringVal, out tmpInt)) ? (T) (object) tmpInt : defaultValue;
            }
            if (typeof (string).IsAssignableFrom(typeof (T)))
            {
                return (String.IsNullOrEmpty(stringVal) == false) ? (T) (object) stringVal : defaultValue;
            }
            return defaultValue;
        }



        /// <summary>
        /// Tries to parse the string as an decimal.
        /// If not possible, returns the <paramref name="defaultValue"/>.
        /// </summary>
        /// <param name="decimalString">The decimal string.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static decimal? TryParseAsdecimal(this string decimalString, decimal? defaultValue = null)
        {
            decimal tmpdecimal;
            return (Decimal.TryParse(decimalString, out tmpdecimal)) ? tmpdecimal : defaultValue;
        }



        /// <summary>
        /// Tries to parse the string as an int.
        /// If not possible, returns the <paramref name="defaultValue"/>.
        /// </summary>
        /// <param name="intString">The int string.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static int? TryParseAsInt(this string intString, int? defaultValue = null)
        {
            int tmpInt;
            return (Int32.TryParse(intString, out tmpInt)) ? tmpInt : defaultValue;
        }


        /// <summary>
        /// Tries to parse the given string as a date. If not parsable, returns the <paramref name="defaultValue"/>.
        /// </summary>
        /// <param name="dateTimeString">The date time string.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static DateTime? TryParseAsDateTime(this string dateTimeString, DateTime? defaultValue = null)
        {
            DateTime tmpDateTime;
            return (DateTime.TryParse(dateTimeString, out tmpDateTime)) ? tmpDateTime : defaultValue;
        }


        /// <summary>
        /// Determines whether the specified value is the type's Default value.
        /// <para>
        /// This is the equivalent of default(T)
        /// </para>
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if the specified type is default; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDefault(this Type type, object value)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type).Equals(value);
            }
            return value == null;
        }



        ///// <summary>
        ///// Gets all parent types (parent abstract *and* interfaces).
        ///// <para>
        ///// Used by <see cref="MethodInfoExtensions.GetAttributeRecursively{T}"/>
        ///// </para>
        ///// </summary>
        ///// <param name="type">The type.</param>
        ///// <returns></returns>
        //public static IEnumerable<Type> GetParentTypes(this Type type)
        //{
        //    // is there any base type?
        //    if ((type == null) || (type.BaseType == null))
        //    {
        //        yield break;
        //    }

        //    // return all implemented or inherited interfaces
        //    foreach (Type i in type.GetInterfaces())
        //    {
        //        yield return i;
        //    }

        //    // return all inherited types
        //    Type currentBaseType = type.BaseType;

        //    while (currentBaseType != null)
        //    {
        //        yield return currentBaseType;
        //        currentBaseType = currentBaseType.BaseType;
        //    }



        //}

        /// <summary>
        /// Gets the parent types.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="sort">if set to <c>true</c> [sort].</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetParentTypes(this Type type, bool sort = false)
        {
            List<Type> results = new List<Type>();

            if (type != null && type.BaseType != null)
            {
                //Do Instances First:
                for (Type currentBaseType = type.BaseType;
                     currentBaseType != null;
                     currentBaseType = currentBaseType.BaseType)
                {
                    results.Add(currentBaseType);
                }
                //Then interfaces:
                results.AddRange(type.GetInterfaces());
            }
            return sort ? results.TopologicalSort((x) => x.GetParentTypes()) : results;
            //return results;
        }



        public static TAttribute GetCustomAttribute<TAttribute>(this Type type, bool inherit = true)
            where TAttribute : class
        {
            return type.GetCustomAttribute(typeof (TAttribute), inherit) as TAttribute;
        }


        public static Attribute GetCustomAttribute(this Type type, Type attributeType, bool inherit = true)
        {
            Attribute[] resultAttributes = type.GetCustomAttributes(attributeType, inherit) as Attribute[];

            if ((resultAttributes == null) || (resultAttributes.Length == 0))
            {
                return null;
            }

            //Attribute resultAttribute = type.GetCustomAttribute(attributeType, recurse);
            Attribute resultAttribute = resultAttributes.First();

            return resultAttribute;

        }




        /// <summary>
        /// Gets Attributes on the given <see cref="Type" />.
        /// <para>
        /// The Default System has a recurse <see cref="bool" /> flag,
        /// but it works only up among classes. Not interfaces.
        /// </para>
        /// <para>
        /// This method does.
        /// </para>
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="type">The type.</param>
        /// <param name="recurse">if set to <c>true</c> [recurse].</param>
        /// <returns></returns>
        public static TAttribute GetAttributeRecursively<TAttribute>(this Type type, bool recurse = true)
            where TAttribute : Attribute
        {
            TAttribute attribute = type.GetCustomAttribute<TAttribute>();

            //If there are any attibutes on this object, certainly no need to recurse higher
            //as it's expensive:
            if ((!recurse) || (attribute != null))
            {
                return attribute;
            }

            //But if we must...
            foreach (Type parentType in GetParentTypes(type.DeclaringType))
            {
                //But if it does, get its attributes:
                attribute = parentType.GetCustomAttribute<TAttribute>();

                if (attribute != null)
                {
                    //Found one (and even if there ones on higher ones, this overrides them):
                    return attribute;
                }
            }
            return null;
        }




        /// <summary>
        /// Gets Attributes on the given <see cref="Type" />.
        /// <para>
        /// The Default System has a recurse <see cref="bool" /> flag,
        /// but it works only up among classes. Not interfaces.
        /// </para>
        /// <para>
        /// This method does.
        /// </para>
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="type">The type.</param>
        /// <param name="recurse">if set to <c>true</c> [recurse].</param>
        /// <returns></returns>
        public static TAttribute[] GetAttributesRecursively<TAttribute>(this Type type, bool recurse = true)
            where TAttribute : Attribute
        {
            TAttribute[] attributes = type.GetCustomAttributes(typeof (TAttribute), true) as TAttribute[];


            //If there are any attibutes on this object, certainly no need to recurse higher
            //as it's expensive:
            if ((!recurse) || (attributes != null) && (attributes.Length > 0))
            {
                return attributes;
            }

            //But if we must...
            foreach (Type parentType in GetParentTypes(type.DeclaringType))
            {
                //But if it does, get its attributes:
                attributes = type.GetCustomAttributes(typeof (TAttribute), true) as TAttribute[];

                if (attributes != null)
                {
                    //Found one (and even if there ones on higher ones, this overrides them):
                    return attributes;
                }
            }
            return null;
        }



        /// <summary>
        /// Finds in the given assembly all classes definitions that derive 
        /// from the given type.
        /// </summary>
        /// <param name="baseType">Type of the base.</param>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static IEnumerable<Type> FindSubClassesOf(this Type baseType, Assembly assembly = null)
        {
            if (assembly == null)
            {
                assembly = baseType.Assembly;
            }

            return assembly.GetTypes().Where(t => t.IsSubclassOf(baseType));
        }



        /// <summary>
        /// Activates the specified Type 
        /// using the <see cref="IServiceLocatorService"/>
        /// if it is an interface, or Activator if it is a Type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        public static T ActivateEx<T>(this Type t) where T : class
        {
            //return XAct.Shortcuts.ActivateEx(t);

            try
            {
                //object o = t.IsInterface ? DependencyResolver.Current.GetInstance(t) : Activator.CreateInstance(t);

                object o = DependencyResolver.Current.GetInstance(t);

                return o as T; // as TClassContract
            }
            catch
            {
                return default(T);
            }

        }


        /// <summary>
        /// Gets the interfaces (either specific to the class, or inherited ones as well).
        /// <para>
        /// Note that in both cases, the interfaces can inherited, and order is not 
        /// based on inheritence (bummer).
        /// </para>
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="includeInherited">if set to <c>true</c> [include inherited].</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetInterfaces(this Type type, bool includeInherited)
        {
            //See: http://stackoverflow.com/a/5318886/4140558

            var results = includeInherited || type.BaseType == null
                              ? type.GetInterfaces()
                              // ReSharper disable PossibleNullReferenceException
                              : type.GetInterfaces().Except(type.BaseType.GetInterfaces());
            // ReSharper restore PossibleNullReferenceException

            return results;
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns whether the passed value is the same as the default value for the property of the Type.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Last time I used this was in XMenu, where I wanted to know if I needed to embed a texto attribute...
        ///     Since there were many, I checked first, in order to decrease what I had to transmit:
        ///     <code>
        ///       if (IsDefaultString(typeof(ControlImages), "ArrowRight",this.Images.ArrowRight)) { WC_Root.Attributes["imgUrlArrowRight"] = this.Images.ArrowRight; }
        ///     </code>
        ///   </para>
        /// </remarks>
        /// <param name = "type"></param>
        /// <param name = "propertyName"></param>
        /// <param name = "value"></param>
        /// <returns></returns>
        public static bool IsNotDefaultString(this Type type, string propertyName, string value)
        {
            type.ValidateIsNotDefault("type");

            if (propertyName.IsNullOrEmpty())
            {
                throw new ArgumentNullException("propertyName");
            }

            //Return false because we don't want to write it as an attribute:
            if (String.IsNullOrEmpty(value))
            {
                return false;
            }

            //Get property by that name:
            PropertyInfo propertyInfo = type.GetProperty(propertyName);

            propertyInfo.ValidateIsNotDefault("named property not found:'{0}'".FormatStringCurrentUICulture(propertyName));
            //If I have a property, get its attributes:
            DefaultValueAttribute[] attributes =
                (DefaultValueAttribute[]) propertyInfo.GetCustomAttributes(typeof (DefaultValueAttribute), true);
            //
            if ((attributes == null) || (attributes.Length > 0))
            {
                throw new ArgumentNullException(
                    "No DefaultProperty attributes found for '{0}'".FormatStringCurrentUICulture(propertyName));
            }
            string val = (string) attributes[0].Value;

            return String.Compare(value, 0, val, 0, val.Length, StringComparison.OrdinalIgnoreCase) != 0;

            //Not the same...we need to write it out as attribute...
        }


        public static void ToDataContractStream(this Type type, object objectToSerialize, Stream streamToSerializeInto)
        {
            DataContractSerializer dataContractSerializer =
                new DataContractSerializer(type);

            dataContractSerializer.WriteObject(streamToSerializeInto, objectToSerialize);


        }


        public static string ToDataContractString(this Type type, object objectToSerialize)
        {

            using (MemoryStream memoryStream = new MemoryStream())
            {
                type.ToDataContractStream(objectToSerialize, memoryStream);
                memoryStream.Position = 0;
                string results = memoryStream.ReadToEnd();

                return results;
            }
        }

        /// <summary>
        /// Helper method to test whether an object is correctly
        /// decorated with DataContract and DataMember, enough
        /// to make it across a tier seapration.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public static object TestWCFSerialization(this Type type, object message)
        {
            object roundTrippedMessage;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                DataContractSerializer dataContractSerializer =
                    new DataContractSerializer(type);

                dataContractSerializer.WriteObject(memoryStream, message);

                //rewind:
                memoryStream.Position = 0;

                DataContractSerializer dataContractSerializer2 =
                    new DataContractSerializer(type);
                roundTrippedMessage = dataContractSerializer2.ReadObject(memoryStream);
            }

            return roundTrippedMessage;
        }




        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Serializes the specified value into a string representation.
        /// </summary>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializationMethod">The serialization method (if it can't do it, may automatically switch to another option).</param>
        /// <returns></returns>
        /// <internal><para>8/16/2011: Sky</para></internal>
        public static string Serialize(this Type valueType, object value, ref SerializationMethod serializationMethod)
        {


            if (serializationMethod == SerializationMethod.String)
            {

                if (typeof(IConvertible).IsAssignableFrom(valueType))
                {
                    if (typeof (DateTime).IsAssignableFrom(valueType))
                    {
                        //Prefer round-trip format:
                        return ((DateTime) value).ToString("O");
                    }
                    return ConvertType(value, typeof(string)) as string;
                }
                else
                {
                    serializationMethod = SerializationMethod.Json;
                }

            }

            if (serializationMethod == SerializationMethod.Json)
            {
                string r1= value.ToJSON(valueType, Encoding.UTF8);
                string r2;
                ////throw new NotImplementedException();
                ////Was Only available for 3.5 Full -- not Client! 
                ////But available in 4.5
                using (MemoryStream serializationStream = new MemoryStream())
                {
                    new System.Runtime.Serialization.Json.DataContractJsonSerializer(valueType).WriteObject(
                        serializationStream, value);
                    r2 = new StreamReader(serializationStream).ReadToEnd();
                }
                return r1;
            }
            if (serializationMethod == SerializationMethod.Xml)
            {

                try
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    using (StringWriter writer = new StringWriter(stringBuilder))
                    {
                        //Use the extension method
                        XmlSerializer xmlSerializer = new XmlSerializer(valueType);
                        xmlSerializer.Serialize(writer, value);
                    }
                    return stringBuilder.ToString();
                }
                catch
                {
                    serializationMethod = SerializationMethod.Base64Binary;
                }
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Deerialize the string back into an object.
        /// </summary>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="serializedValue">The serialized value.</param>
        /// <param name="serializationMethod">The serialization method.</param>
        /// <returns></returns>
        /// <internal><para>8/16/2011: Sky</para></internal>
        public static object DeSerialize(this Type valueType, string serializedValue,
                                         SerializationMethod serializationMethod)
        {


            if (serializationMethod == SerializationMethod.Undefined)
            {
                throw new ArgumentException("serializationMethod");
            }
            object result;

            if (serializationMethod == SerializationMethod.String)
            {
                return ConvertType(serializedValue, valueType);
            }
            if (serializationMethod == SerializationMethod.Xml)
            {
                using (StringReader reader = new StringReader(serializedValue))
                {
                    XmlSerializer serializer = new XmlSerializer(valueType);
                    result = serializer.Deserialize(reader);
                    return result;
                }
            }
            if (serializationMethod == SerializationMethod.Json)
            {
                return serializedValue.DeserialiseFromJSON(valueType, Encoding.UTF8);

                //using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(serializedValue)))
                //{
                //    return new System.Runtime.Serialization.Json.DataContractJsonSerializer(valueType).ReadObject(stream);
                //}
            }
            throw new NotImplementedException();
        }




        //https://github.com/structuremap/structuremap/commit/ae573d02e616816c22f4b4e5f81e2d1e761b0ff5
        public static object ConvertType(object value, Type dependencyType)
        {
            if (value == null)
            {
                return dependencyType.GetDefault();
            }
            if (value.GetType() == dependencyType) return value;

            if (dependencyType.IsEnum) return Enum.Parse(dependencyType, value.ToString(), true);

            return Convert.ChangeType(value, dependencyType, null);
        }






        public static MethodInfo GetGenericMethod(this Type objectType, string methodName, Type[] genericTypes)
        {
            MethodInfo methodInfo = objectType.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public);
            MethodInfo genericMethodInfo = methodInfo.MakeGenericMethod(genericTypes);

            return genericMethodInfo;
        }


        public static IEnumerable<Type> GetImmediateInterfaces(this Type type)
        {
            var interfaces = type.GetInterfaces();
            var result = new HashSet<Type>(interfaces);
            foreach (Type i in interfaces)
            {
                result.ExceptWith(i.GetInterfaces());
            }
            return result;
        }




        public static Type[] GetSubclasses(this Type type, Type[] exclude = null)
        {
            List<Type> results = new List<Type>();
            while (type != null)
            {
                type = type.BaseType;

                if ((exclude != null) && (exclude.Contains(type)))
                {
                    break;
                }
                results.Add(type);
            }
            return results.ToArray();
        }


        public static TreeNode<NodeTypeWrapper> GetSubclassesAndInterfaces(this Type type, bool trimInterfaces = false,
                                                                Type[] exclude = null)
        {

            if (exclude == null)
            {
                exclude = new[] {typeof (object)};
            }
            List<Type> excludes = new List<Type>();
            excludes.Add(exclude);


            TreeNode<NodeTypeWrapper> treeNode = new TreeNode<NodeTypeWrapper>(new NodeTypeWrapper(type));


            var alreadyAdded = new List<Type>();

            GetSubclassesAndInterfaces(treeNode, treeNode, ref excludes, ref alreadyAdded, trimInterfaces);

            return treeNode;
        }

        private static bool GetSubclassesAndInterfaces(this TreeNode<NodeTypeWrapper> sourceNode, TreeNode<NodeTypeWrapper> rootNode, ref List<Type> exclude, ref List<Type> alreadyAdded, bool trimInterfaces = false)
        {


            Type type = sourceNode.Value.Type;

            if (exclude.Contains(type))
            {
                //Probably a System.Object, which we don't need:
                return false;
            }

            //We have current Type -- we want to work upwards:
            Type baseType = type.BaseType;

            if (baseType != null)
            {
                var baseTypeNode = new TreeNode<NodeTypeWrapper>(new NodeTypeWrapper(baseType));


                //Depth First:
                if (baseTypeNode.GetSubclassesAndInterfaces(rootNode, ref exclude,ref alreadyAdded, trimInterfaces))
                {
                    if (trimInterfaces)
                    {
                        if (alreadyAdded.Contains(baseTypeNode.Value.Type))
                        {
                            baseTypeNode.Value.Display = false;
                        }
                    }

                    alreadyAdded.Add(baseTypeNode.Value.Type);

                    sourceNode.Add(baseTypeNode);
                }
            }


            //We want recursive interfaces:
            foreach (var tn in type.GetImmediateInterfaces())
            {
                var interfaceTypeNode = new TreeNode<NodeTypeWrapper>(new NodeTypeWrapper(tn));


                if (interfaceTypeNode.GetSubclassesAndInterfaces(rootNode, ref exclude, ref alreadyAdded, trimInterfaces))
                {
                    if (trimInterfaces)
                    {
                        if (alreadyAdded.Contains(interfaceTypeNode.Value.Type))
                        {
                            interfaceTypeNode.Value.Display = false;
                        }
                    }
                    alreadyAdded.Add(interfaceTypeNode.Value.Type);

                    sourceNode.Add(interfaceTypeNode);
                }
            }



            return true;
        }

        public static TreeNode<Type>[] GetInterfaceInheritence(this Type type)
        {
            List<TreeNode<Type>> results = new List<TreeNode<Type>>();

            //Given Foo, get IFoo
            foreach (Type interfaceType in type.GetImmediateInterfaces())
            {

                TreeNode<Type> subInterfaceNode = new TreeNode<Type>(interfaceType);
                //Get IBoo
                foreach (var t in interfaceType.GetInterfaceInheritence())
                {
                    subInterfaceNode.Add(t);
                }
                //[IFoo]:
                results.Add(subInterfaceNode);
            }
            return results.ToArray();
        }


        /// <summary>
        /// Gets the name of a Generic Type such that instead of returning
        /// <c>Dictionary`2 stuff)</c> or <c>List`1[]</c>
        /// it returns:
        /// <code>
        /// <![CDATA[
        /// ]]></code>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetName(this Type type, bool keepFullNames=false)
        {
            StringBuilder retType = new StringBuilder();

            if (type.IsArray)
            {
                var result = GetName(type.GetElementType()) + "[]";
                return result;
            }

            if (!type.IsGenericType)
            {
                return keepFullNames ? type.FullName : type.Name;
            }
            string rootType = (keepFullNames ? type.FullName : type.Name).Split('`')[0];

            StringBuilder argList = new StringBuilder();

            // We will build the type here.
            Type[] genericArgumentTypes = type.GetGenericArguments();


            foreach (Type argumentType in genericArgumentTypes)
            {
                // Let's make sure we get the argument list.
                string arg = argumentType.GetName();

                if (argList.Length <= 0)
                {
                    argList.Append(arg);
                }
                else
                {
                    argList.AppendFormat(", {0}", arg);
                }
            }

            if (argList.Length > 0)
            {
                retType.AppendFormat("{0}<{1}>", rootType, argList.ToString());
            }
            else
            {
                retType.Append(type.ToString());
            }

            return retType.ToString();
        }

        public static ConstructorInfo GetConstructorWithMostArguments(this Type type,
                                                                      BindingFlags bindingFlags =
                                                                          BindingFlags.Instance | BindingFlags.Public |
                                                                          BindingFlags.NonPublic)
        {
            var constructors =
                type.GetConstructors(bindingFlags);

            ConstructorInfo constructorInfo =
                constructors.Aggregate((i1, i2) => i1.GetParameters().Count() > i2.GetParameters().Count() ? i1 : i2);

            return constructorInfo;
        }
    }
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
