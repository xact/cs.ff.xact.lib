﻿//The ONLY Extensions that are in a namespace different than XAct.
//It's because end users find that it adds too many methods to all objects
//even when irrelevant.
namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Json;
    using System.Text;




    /// <summary>
    ///   Extensions to the <see cref = "object" />.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    // ReSharper disable CheckNamespace
    public static partial class ObjectExtensions
        // ReSharper restore CheckNamespace
    {


        #region Nested type: Constants

        /// <summary>
        ///   Class of static Resources used by <see cref = "ObjectExtensions" />
        /// </summary>
        public static class Constants
        {
            /// <summary>
            ///   A Resource.
            /// </summary>
            public static string ErrMsgArgumentEnumValueCannotBeUndefined =
                Properties.Resources.ErrMsgArgumentEnumValueCannotBeUndefined;

            /// <summary>
            ///   A Resource.
            /// </summary>
            public static string ErrMsgArgumentCannotBeNullOrEmpty =
                Properties.Resources.ErrMsgArgumentCannotBeNullOrEmpty;

            /// <summary>
            ///   A Resource.
            /// </summary>
            public static string ErrMsgArgumentCannotBeNull =
                Properties.Resources.ErrMsgArgumentCannotBeNull;
        }

        #endregion

        
        // ReSharper disable InconsistentNaming
        //Used by DynamicProperties
        private static readonly Dictionary<int, WeakReference>
            _dynamicPropertiesCache = new Dictionary<int, WeakReference>();

   
        //Used by DynamicProperties
        private static readonly Dictionary<WeakReference, Dictionary<string, object>>
            _dynamicPropertiesCache2 = new Dictionary<WeakReference, Dictionary<string, object>>();

        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;

        private static void ValidateIsNotWrappedObject(object o)
        {
            //Safe Guard against being passed a WrappedObject:
            if (o is WrappedObject)
            {
                throw new ArgumentException("Don't pass WrappedObject to Object Extensions");
            }
        }

        
        // ReSharper restore InconsistentNaming

        /*
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets a static object representing the XActLib,
        /// giving access to common lib methods from anywhere.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <returns></returns>
        public static Lib XActLib(this object o)
        {
            return _xactLib;
        }

        private readonly static Lib _xactLib = new Lib();
        */


        ///// <summary>
        ///// As the test.
        ///// </summary>
        ///// <param name="o">The o.</param>
        //public static void ATest(this object o)
        //{

        //}


        ///// <summary>
        ///// <para>
        ///// An XActLib Extension.
        ///// </para>
        ///// Validates the argument is not null.
        ///// </summary>
        ///// <param name="value">The value.</param>
        ///// <param name="argName">Name of the arg.</param>
        ///// <param name="errorMessage">The error message.</param>
        //public static void ValidateIsNotNull([ValidatedNotNull] this object value, string argName,
        //                                     string errorMessage = null)
        //{
        //    if (value == null)
        //    {
        //        if (string.IsNullOrEmpty(errorMessage))
        //        {
        //            throw new ArgumentNullException(argName);
        //        }
        //        throw new ArgumentNullException(argName, errorMessage);
        //    }
        //}



        /// <summary>
        /// Additional XActLib Extension methods.
        /// <para>
        /// Extension Methods on Objects of Type Object are problematic: making Intellisense on all objects almost too long to navigate easily.
        /// </para>
        /// <para>
        /// Either add the XAct.Extensions namespace, or use this Extension method to get to the extensions. 
        /// </para>
        /// </summary>
        public static WrappedObject Xtensions(this object o)
        {
            return new WrappedObject(o);
        }


        /// <summary>
        /// Validates the object is of the right type or a sub class thereof.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <param name="expectedType">The expected type.</param>
        public static void ValidateIsRightTypeOrSubClassThereof(this object o, Type expectedType)
        {
            //Unwrap if need be:
            ValidateIsNotWrappedObject(o);

            if (o.GetType().IsSubClassOfEx(expectedType))
            {
                return;
            }

            string tIs = o.GetType().AssemblyQualifiedName;
            string tShould = expectedType.AssemblyQualifiedName;

            throw new ArgumentNullException(
                "serverEventContext ('{0}') not correct Type (should be '{1}')".FormatStringCurrentCulture(tIs, tShould));

        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Determines whether the given type is a subclass of the given superclass type.
        ///   <para>
        ///     Example: example.IsSubClassOfEx(typeof(Example));//true
        ///     Example: example.IsSubClassOfEx(typeof(IExample));//true
        ///   </para>
        /// </summary>
        /// <param name = "childClassInstance">Type of the child sub class.</param>
        /// <param name = "parentSuperClass">The parent base class.</param>
        /// <returns>
        ///   <c>true</c> if [is A subclass of] [the specified child sub class type]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref = "System.ArgumentNullException" />
        public static bool IsSubClassOfEx(this object childClassInstance, Type parentSuperClass)
        {
            //Unwrap if need be:
            ValidateIsNotWrappedObject(childClassInstance);

            childClassInstance.ValidateIsNotDefault("childClassInstance");
            parentSuperClass.ValidateIsNotDefault("parentSuperClass");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            return childClassInstance.GetType().IsSubClassOfEx(parentSuperClass);
        }



        /// <summary>
        ///   Serialize the object and return a string of xml.
        /// </summary>
        /// <param name = "objectToSerialize">The object to serialize.</param>
        /// <returns>A string of xml.</returns>
        //[SuppressMessage("Microsoft.Naming", "CA1720", Justification = "objectToSerialize is more descriptive than 'value'.")]
        public static string ToXmlString(this object objectToSerialize)
        {
            //Unwrap if need be:
            ValidateIsNotWrappedObject(objectToSerialize);

            //OK: if (!objectToSerialize.IsNotNull()) { throw new ArgumentNullException("objectToSerialize"); }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            StringBuilder stringBuilder = new StringBuilder();
            using (StringWriter writer = new StringWriter(stringBuilder, _invariantCulture))
            {
                //Use the extension method
                writer.Serialize(objectToSerialize);
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Provides a way to attach property values dynamically
        /// to any object.
        /// <para>
        /// Wouldn't recommend using it...it's just a proof of concept really.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <para>
        /// <code>
        /// <![CDATA[
        /// object o = new object();
        /// o.GetDynamicAttributes().Add("stunk", "wow");
        /// var u = o.GetDynamicAttributes()["stunk"];
        /// o.GetDynamicAttributes()["stunk"] = "new";
        /// var x = o.GetDynamicAttributes()["stunk"];
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        /// <internal>
        /// Not happy yet.
        /// What it needs is a Timer to cleanup weakreferences when
        /// objects no longer alive.
        /// </internal>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        /// <internal><para>8/5/2011: Sky</para></internal>
        public static Dictionary<string, object> GetDynamicAttributes(this object target)
        {

            target.ValidateIsNotDefault("target");

            //Unwrap if need be:
            ValidateIsNotWrappedObject(target);

            WeakReference weakReference;

            //
            _dynamicPropertiesCache.TryGetValue(target.GetHashCode(), out weakReference);

            if (weakReference == null)
            {
                //Doesn't exist...create new
                weakReference = new WeakReference(target);
                Dictionary<string, object> result = new Dictionary<string, object>();

                _dynamicPropertiesCache[target.GetHashCode()] = weakReference;
                _dynamicPropertiesCache2[weakReference] = result;

                //return newly created dictionary:
                return result;
            }

            //Let's clean up the dictionary...
            //Note: that this is suboptimal solution
            KeyValuePair<int, WeakReference>[] deadOnes = _dynamicPropertiesCache.Where(p => p.Value.IsAlive).ToArray();
            foreach (KeyValuePair<int, WeakReference> deadOne in deadOnes)
            {
                _dynamicPropertiesCache.Remove(deadOne.Key);
                _dynamicPropertiesCache2.Remove(deadOne.Value);
            }

            return _dynamicPropertiesCache2[weakReference];
        }


        





        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Sets the values.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="o">The o.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static void SetValues<TObject>(this TObject o, IDictionary<string, object> values)
        {
            //Unwrap not needed as is Typed by T.

            foreach (KeyValuePair<string, object> pair in values)
            {
                o.SetMemberValue(pair.Key, pair.Value);
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the values.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="o">The o.</param>
        /// <param name="values">The values.</param>
        public static void GetValues<TObject>(TObject o, IDictionary<string, object> values)
        {
            //Unwrap not needed as is Typed by T.

            foreach (KeyValuePair<string, object> pair in values)
            {
                values[pair.Key] = o.GetMemberValue(pair.Key);
            }
        }







        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// makes a unique key string from the name of the DataObject <see cref="Type"/> and its <c>HashCode</c>.
        /// </summary>
        /// <param name="dataObject"></param>
        /// <returns></returns>
        public static int MakeUniqueIdentifier(this object dataObject)
        {
            //Unwrap if need be:
            ValidateIsNotWrappedObject(dataObject);

            dataObject.ValidateIsNotDefault("dataObject");
            //Get the type of the object:
            Type dataObjectType = dataObject.GetType();
            //Make a Unique string Key for it because we can't use a Strong ref:
            //(as it would defeat the purpose of a weak-linked TrackingVar...):



            return (dataObjectType + ":" + dataObject.GetHashCode()).GetHashCode();
        }





        /// <summary>
        /// Tests the WCF serialization.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public static T TestWCFSerialization<T>(this T message)
            where T : class
        {
            //Unwrap not needed as is Typed by T.

            return typeof(T).TestWCFSerialization(message) as T;
        }


        public static void ToDataContractStream<T>(this T objectToSerialize, Stream streamToSerializeInto)
        {
            //Unwrap not needed as is Typed by T.
            TypeExtensions.ToDataContractStream(typeof(T), objectToSerialize, streamToSerializeInto);
        }


        public static string ToDataContractString<T>(this object objectToSerialize)
        {
            ValidateIsNotWrappedObject(objectToSerialize);

            return TypeExtensions.ToDataContractString(typeof(T), objectToSerialize);
        }





        /// <summary>
        /// Serializes an object to a string, in the JSON format,
        /// using UTF-8 encoding.
        /// <para>
        /// Reminder: for an object to be serializable, it must
        /// be decorated with DataContract/DataMember attributes.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objToSerialize">The obj to serialize.</param>
        /// <returns></returns>
        /// <internal><para>7/2/2011: Sky</para></internal>
        public static string ToJSON<T>(this T objToSerialize, Encoding encoding) where T : class
        {
            //Unwrap not needed as is Typed by T.

            return objToSerialize.ToJSON(typeof (T), encoding);
        }

        /// <summary>
        /// Serializes an object to a string, in the JSON format,
        /// using the specified encoding.
        /// <para>
        /// Reminder: for an object to be serializable, it must
        /// be decorated with DataContract/DataMember attributes.
        /// </para>
        /// </summary>
        /// <param name="objToSerialize">The obj to serialize.</param>
        /// <param name="type">The type.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns></returns>
        /// <internal>7/16/2011: Sky</internal>
        public static string ToJSON(this object objToSerialize, Type type, Encoding encoding)
        {
            ValidateIsNotWrappedObject(objToSerialize);


            using (MemoryStream stream = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(type);

                serializer.WriteObject(stream, objToSerialize);

                byte[] b = stream.ToArray();

                return encoding.GetString(b, 0, b.Length);
            }
        }






    }
}