﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XAct
{
    using XAct.Collections.Comparers;
    using XAct.Diagnostics;

    public static class ICollectionsExtensions
    {
        private static ITracingService TracingService
        {
            get { return DependencyResolver.Current.GetInstance<ITracingService>(); }
        }


        /// <summary>
        /// Traces the contents of the <paramref name="collection"/> 
        /// to the current <see cref="ITracingService"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="traceLevel">The trace level.</param>
        public static void Trace<T>(this ICollection<T> collection, TraceLevel traceLevel = TraceLevel.Verbose)
        {


            IEnumerator<T> enumerator = collection.GetEnumerator();
            int position = 0;
            while (enumerator.MoveNext())
            {
                TracingService.Trace(traceLevel, "{0}= '{1}'", position, enumerator.Current);
                
                position++;
            }
        }


        ///// <summary>
        ///// Removes items from the given collection that match the given filter.
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="collection">The collection.</param>
        ///// <param name="filter">The filter.</param>
        //public static void Remove<T>(this ICollection<T> collection, Func<T, bool> filter)
        //{
        //    T[] tmp = collection.Where(filter).ToArray();

        //    foreach (T t in tmp)
        //    {
        //        collection.Remove(t);
        //    }
        //}

        /// <summary>
        /// An XActLib Extension.
        /// <para>
        /// Removes all elements from the <paramref name="first"/> collection 
        /// found in the <paramref name="second"/> collection
        /// (using <paramref name="identitExpression"/> to compare)
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <param name="identitExpression">The expression.</param>
        public static void RemoveAll<T>(this ICollection<T> first, IEnumerable<T> second, Func<T, object> identitExpression)
        {
            FuncComparer<T> expComparer = new FuncComparer<T>(identitExpression);

            T[] secondArray = second.ToArray();

            List<T> toRemove = new List<T>();
            foreach (T item in first)
            {
                if (Object.Equals(secondArray.FirstOrDefault(i2 => expComparer.Equals(item, i2)), default(T)) == false)
                {
                    toRemove.Add(item);
                }

            }
            foreach (T item in toRemove)
            {
                first.Remove(item);
            }
        }



        /// <summary>
        /// An XActLib Extension.
        /// <para>
        /// Removes all elements from the <paramref name="collection"/> that match the given
        /// <paramref name="predicate"/> condition.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The first.</param>
        /// <param name="predicate">The predicate.</param>
        /// <remarks>An array of the items removed.</remarks>
        public static T[] RemoveAll<T>(this ICollection<T> collection, Predicate<T> predicate)
        {
            //List<T> toRemove = new List<T>();
            //foreach (T item in first)
            //{
            //    if (predicate.Invoke(item))
            //    {
            //        toRemove.Add(item);
            //    }
            //}

            // ReSharper disable ConvertClosureToMethodGroup
            List<T> toRemove = collection.Where(item => predicate.Invoke(item)).ToList();
            // ReSharper restore ConvertClosureToMethodGroup



            foreach (T item in toRemove)
            {
                collection.Remove(item);
            }
            return toRemove.ToArray();
        }



        /// <summary>
        /// Determines whether the <paramref name="collection"/> 
        /// contains any item that matchs the 
        /// given <paramref name="predicate"/> condition.
        /// <para>
        /// Note that this is just a convenient wrapper around (<c>Any</c>)
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The list.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public static bool Contains<T>(this ICollection<T> collection, Func<T,bool> predicate)
        {
            return collection.Any(predicate);
        }

        


        /// <summary>
        /// Returns items missing <paramref name="collection1"/> found in <paramref name="collection2"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection1">The list1.</param>
        /// <param name="collection2">The list2.</param>
        /// <param name="comparisionExpression">The comparision expression.</param>
        /// <returns></returns>
        public static List<T> GetItemsMissingFromList1<T>(this ICollection<T> collection1, ICollection<T> collection2, Func<T, object> comparisionExpression)
        {
            return Missing(collection1, collection2, comparisionExpression);
        }




        /// <summary>
        /// Returns items missing from <paramref name="collection1"/> found in <paramref name="collection2"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection1">The list1.</param>
        /// <param name="collection2">The list2.</param>
        /// <param name="comparisionExpression">The comparision expression.</param>
        /// <returns></returns>
        public static List<T> Missing<T>(this ICollection<T> collection1, ICollection<T> collection2, Func<T, object> comparisionExpression)
        {
            List<T> itemsInCollection2NotInCollection1 =
                collection2.Where(i2 => !(collection1.Select(comparisionExpression).Contains(comparisionExpression(i2)))).ToList();

            return itemsInCollection2NotInCollection1;
        }






        /// <summary>
        /// Updates <paramref name="originalItemCollection"/> items
        /// by
        /// adding to <paramref name="originalItemCollection"/> any new items found in <paramref name="updatedItemCollection"/>.
        /// removing from <paramref name="originalItemCollection"/> any items not found in <paramref name="updatedItemCollection"/>,
        /// and updating <paramref name="originalItemCollection"/> with any items found in <paramref name="updatedItemCollection"/>.
        /// <para>
        /// For mapping one object over another another (as when updating properties of item in
        /// <paramref name="originalItemCollection"/> with new property values), use the provided
        /// <paramref name="updateOriginalCollectionItemFromUpdatedCollectionItemAction"/> to map over original values
        /// (eg, using AutoMapper of ValueInjecter).
        /// </para>
        /// 	<para>
        /// If you have to update properties when you add new items (eg, set an FK property), use the optional <paramref name="preAddItemAction"/>
        /// to do that.
        /// </para>
        /// 	<para>
        /// 		<example>
        /// 			<code>
        /// 				<![CDATA[
        /// .AfterMap((s,d)=>
        /// {
        ///   //map returning ViewModel back to equivalent Domain models. Note that these Model objects 
        ///   //are probably incomplete as ViewModel usually are partial Model projections in the first place.
        ///   //They are are also not attached to context in any way.
        ///   //Use Domain Model representations of returned ViewModels to update the original (dbcontext attached) Model collection:
        ///   d.Attachments.UpdateFrom<Attachment>(
        ///     attachmentsReturned,
        ///     i=>i.Id,
        ///     (i1,i2)=> AutoMapper.Mapper.Map<BankCustomer,BankCustomer>(i1,i2),
        ///     null /* SoftDelete Predicate eg: (x)=>x.Deleted=true; /*
        ///     null /* PreCreate Callback:  eg: (x)=>{x.Created=DateTime.Now;x.BankCustomerWorkInstructionFK =d.Id} */
        ///     null /* PreUpdate Callback   eg: (x)=>{x.Modified = DateTime.Now;} */
        ///     null /* PreDelete Callback   eg: (x)=>{} */
        ///     );
        /// }
        /// ]]>
        /// 			</code>
        /// 		</example>
        /// 	</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="originalItemCollection">The reference data (e.g.: the original collection).</param>
        /// <param name="updatedItemCollection">The updated (e.g.: roundtripped as a View Model) version of the original <paramref name="originalItemCollection"/>.</param>
        /// <param name="referenceItemIdentityExpression">The comparison expression used to compare the items (i.e.: a expression like <c>m=&gt;m.Id</c> would be a common example).</param>
        /// <param name="updateOriginalCollectionItemFromUpdatedCollectionItemAction">The Action to perform on each original item (something like  (i1,i2)=&gt; AutoMapper.Mapper.Map{BankCustomer,BankCustomer}(i1,i2) would be indicative).</param>
        /// <param name="softDeleteItemPredicate">The soft delete predicate.</param>
        /// <param name="preAddItemAction">An optional Action to perform on every new item before adding it to the <paramref name="originalItemCollection"/>. Null is most common.</param>
        /// <param name="preUpdateItemAction">The pre update item action.</param>
        /// <param name="preDeleteItemAction">The (optional) action to perform prior to removing an item from <paramref name="originalItemCollection"/>.</param>
        public static void UpdateFrom<T>(
            this ICollection<T> originalItemCollection, 
            IEnumerable<T> updatedItemCollection,
            Func<T, object> referenceItemIdentityExpression,
            Func<T, T, bool> updateOriginalCollectionItemFromUpdatedCollectionItemAction=null,
            Predicate<T> softDeleteItemPredicate = null,
            Action<T> preAddItemAction=null,
            Action<T> preUpdateItemAction = null,
            Action<T> preDeleteItemAction = null
            )
        {

            //--------------------------------------------------
            //Hopefully what's coming in is a collection, but who knows.
            List<T> updatedCollectionItemList = updatedItemCollection.ToList();
            //--------------------------------------------------
            //FIX: Convert Soft Deletes in incoming collection
            //as Hard Deletes of incoming collection, 
            //by actually removing them early, making it easier later.
            //Note that this does not contravene practice of following CRUD sequence (
            //the CRUD sequencing applies to the target/original collection).
            if (softDeleteItemPredicate != null)
            {
                updatedCollectionItemList.RemoveAll(softDeleteItemPredicate);
            }
            //--------------------------------------------------
            //PROCESS IN CRUD SEQUENCE (Create before Update, before Delete)
            //There's a bit of waste when a Update repeats Updating New Items, but it
            //ensures that CreatedDateTime is done, and UpdatedDateTime is also addressed.
            //--------------------------------------------------
            //CREATE new items in originalCollection found in newCollection
            //BETTER, but not swithcing tonight due to delivery risk: var newlyCreatedItems = originalCollection.Missing(secondArray, comparisonExpression);
            List<T> newlyCreatedItems =
                updatedCollectionItemList
                .Where(i2 => !(originalItemCollection.Select(referenceItemIdentityExpression).Contains(referenceItemIdentityExpression(i2)))).ToList();

            //These two do not provide same answer as Except and Union are comparing against result, not list1
            //Misses out multiple 0 ids: var newItems2 = list2.Except(list1,i=>i.Id).ToList();
            //Misses out multiple 0 ids: var newItems3 = list1.Union(list2,i=>i.Id);

            //With EF collections, we have to ensure the new items are added to the collection, 
            //so that their FK's are updated to the collection owner.

            if (preAddItemAction != null)
            {
                //Perform operations such as 
                //* Update DateTimeCreated properties, 
                //* Maybe _repository.AddOnCommit<T>(...)
                newlyCreatedItems.ForEach(preAddItemAction);
            }
            foreach (T newlyCreatedItem in newlyCreatedItems)
            {
                originalItemCollection.Add(newlyCreatedItem);
            }
            //--------------------------------------------------
            //UPDATE:
            //Update items in originalCollection found in both collections
            IEnumerable<T> updatedUpdatedCollectionItems = updatedCollectionItemList.Intersect(originalItemCollection, referenceItemIdentityExpression);
            //Iterate through each element in updatedUpdatedCollectionItems (eg 3 items), 
            //trying to find  matching item 
            //updating referenceCollection item that matches.


            if (updateOriginalCollectionItemFromUpdatedCollectionItemAction == null)
            {
                updateOriginalCollectionItemFromUpdatedCollectionItemAction = 
                    (o1, o2) =>
                        {
                            o1.MapPropertyValues(o2);

                            return true;
                        };
            }

            originalItemCollection.ForEach(
                updatedUpdatedCollectionItems,
                referenceItemIdentityExpression, 
                (o1, o2) =>
                    {
                        if ( (updateOriginalCollectionItemFromUpdatedCollectionItemAction(o1, o2)) && (preUpdateItemAction != null))
                        {
                            preUpdateItemAction(o2);
                        }
                    }
            );
            //--------------------------------------------------
            //DELETE:

            //Now that the list is truncated:
            //delete from originalCollection any items not found in newCollection:
            List<T> removed = originalItemCollection.Except(updatedCollectionItemList, referenceItemIdentityExpression).ToList();


            if (preDeleteItemAction != null)
            {
                //Perform operations such as 
                //* Update DateTimeDeleted properties, 
                //* Maybe _repository.Delete<T>(...)
                removed.ForEach(preDeleteItemAction);
            }
            originalItemCollection.RemoveAll(removed, referenceItemIdentityExpression);
            //Done.
        }


        /// <summary>
        /// Updates <paramref name="originalItemCollection"/> items
        /// by
        /// adding to <paramref name="originalItemCollection"/> any new items found in <paramref name="updatedItemCollection"/>.
        /// removing from <paramref name="originalItemCollection"/> any items not found in <paramref name="updatedItemCollection"/>,
        /// and updating <paramref name="originalItemCollection"/> with any items found in <paramref name="updatedItemCollection"/>.
        /// <para>
        /// For mapping one object over another another (as when updating properties of item in
        /// <paramref name="originalItemCollection"/> with new property values), use the provided
        /// <paramref name="updateOriginalCollectionItemFromUpdatedCollectionItemAction"/> to map over original values
        /// (eg, using AutoMapper of ValueInjecter).
        /// </para>
        /// 	<para>
        /// If you have to update properties when you add new items (eg, set an FK property), use the optional <paramref name="preAddItemAction"/>
        /// to do that.
        /// </para>
        /// 	<para>
        /// 		<example>
        /// 			<code>
        /// 				<![CDATA[
        /// .AfterMap((s,d)=>
        /// {
        ///   //Use Domain Model representations of returned ViewModels to update the original (dbcontext attached) Model collection:
        ///   d.Attachments.UpdateFrom<Attachment,AttachmentViewModel>(
        ///     returningViewModels,
        ///     i=>i.Id,
        ///     (i1,i2)=> AutoMapper.Mapper.Map<BankCustomer,BankCustomer>(i1,i2),
        ///     null /* SoftDelete Predicate eg: (x)=>x.Deleted=true; /*
        ///     null /* PreCreate Callback:  eg: (x)=>{x.Created=DateTime.Now;x.BankCustomerWorkInstructionFK =d.Id} */
        ///     null /* PreUpdate Callback   eg: (x)=>{x.Modified = DateTime.Now;} */
        ///     null /* PreDelete Callback   eg: (x)=>{} */
        ///     );
        /// }
        /// ]]>
        /// 			</code>
        /// 		</example>
        /// 	</para>
        /// </summary>
        /// <typeparam name="T1">The type of the 1.</typeparam>
        /// <typeparam name="TProxy">The type of the proxy.</typeparam>
        /// <param name="originalItemCollection">The original item collection.</param>
        /// <param name="updatedProxyItemCollection">The updated proxy item collection.</param>
        /// <param name="referenceItemIdentityExpression">The reference item identity expression.</param>
        /// <param name="mapProxyItemToReferenceItem">The map proxy item to reference item.</param>
        /// <param name="updateOriginalCollectionItemFromUpdatedCollectionItemAction">The update original collection item from updated collection item action.</param>
        /// <param name="softDeleteItemPredicate">The soft delete item predicate.</param>
        /// <param name="preAddItemAction">The pre add item action.</param>
        /// <param name="preUpdateItemAction">The pre update item action.</param>
        /// <param name="preDeleteItemAction">The pre delete item action.</param>
        public static void UpdateFrom<T1, TProxy>(
            this ICollection<T1> originalItemCollection,
            IEnumerable<TProxy> updatedProxyItemCollection,
            Func<T1, object> referenceItemIdentityExpression,
            Func<TProxy, T1> mapProxyItemToReferenceItem,
            Func<T1, T1, bool> updateOriginalCollectionItemFromUpdatedCollectionItemAction,
            Predicate<TProxy> softDeleteItemPredicate = null,
            Action<T1> preAddItemAction = null,
            Action<T1> preUpdateItemAction = null,
            Action<T1> preDeleteItemAction = null
            )
        {
            //--------------------------------------------------
            //FIX: Convert Soft Deletes in incoming collection
            //as hard deletes of incoming collection, by actually removing them
            //early, which makes it easier later.
            //Note that this does not contravene practice of following CRUD sequence (
            //the CRUD sequencing applies to the target/original collection).
            List<TProxy> updatedProxyItemCollectionArray = updatedProxyItemCollection.ToList();
            if (softDeleteItemPredicate != null)
            {
                updatedProxyItemCollectionArray.RemoveAll(softDeleteItemPredicate);
            }
            //--------------------------------------------------
            //Convert the returning ViewModel to a Model collection:
            List<T1> updatedCollectionArray = new List<T1>();

            //Note that these items are probably sparse in that they don't have all properties 
            //filled in. Makes senses as ViewModels are often thinner/incomplete.
            updatedCollectionArray.AddRange(updatedProxyItemCollectionArray.Select(mapProxyItemToReferenceItem));


            //Now that both collections are of same type,
            //use overload:
            originalItemCollection.UpdateFrom<T1>(updatedCollectionArray,
                                                  referenceItemIdentityExpression,
                                                  updateOriginalCollectionItemFromUpdatedCollectionItemAction,
                                                  null /*As PRoxies have already been stripped out, can pass null */,
                                                  preAddItemAction,
                                                  preUpdateItemAction,
                                                  preDeleteItemAction
                );
        }




        /// <summary>
        /// Adds the items in the source to the destination collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="destination">The destination.</param>
        /// <param name="source">The source.</param>
        public static void Add<T>(this ICollection<T> destination,
                           IEnumerable<T> source)
        {
            if (source == null) { return; }

            foreach (T item in source)
            {
                destination.Add(item);
            }
        }       



        /*
         *         public static void ForEach<T>(this IEnumerable<T> collection, Action<T> actionToApply)
        {
            collection.ValidateIsNotDefault("collection");
            actionToApply.ValidateIsNotDefault("actionToApply");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            foreach (T item in collection)
            {
                actionToApply.Invoke(item);
            }
        }

                 public static IEnumerable<T> Intersect<T>(this IEnumerable<T> first, IEnumerable<T> second,
                                                  Func<T, object> expression)
        {
            return first.Intersect(second, new FuncComparer<T>(expression));
        }

        public static IEnumerable<T> Except<T>(this IEnumerable<T> referenceCollection, IEnumerable<T> updatedCollection,
                                               Func<T, object> expression)
        {
            return referenceCollection.Except(updatedCollection, new FuncComparer<T>(expression));
        }
         */

    }
}
