﻿namespace XAct {
/// <summary>
    /// Extensions to char arrays.
    /// </summary>
    /// <internal><para>5/14/2011: Sky</para></internal>
    public static class CharArrayExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts the char array to a <see cref="System.String"/>.
        /// </summary>
        /// <param name="charArray">The char array.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        /// <internal><para>5/14/2011: Sky</para></internal>
        public static string ToString(this char[] charArray)
        {
            return new string(charArray);
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
