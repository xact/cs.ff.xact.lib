﻿//// ReSharper disable CheckNamespace

//namespace XAct
//// ReSharper restore CheckNamespace
//{
//    using System;

//    /// <summary>
//    /// A contract for an Command that can be Done/Undone.
//    /// <para>
//    /// IMPORTANT: although not exactly depracated, 
//    /// the command pattern is an old pattern, that is not appropriate
//    /// for less coupled/Dependency Injection scenarios. 
//    /// In such cases, consider usering 
//    /// ICommandMessage and ICommandMessageHandler.
//    /// </para>
//    /// </summary>
//    /// <remarks>
//    ///   <para>
//    ///     According to Design Patters, the Command design pattern
//    ///     "encapsulates a request as an object, thereby letting you parameterize 
//    ///     clients with different requests, queue or log requests, 
//    ///     and support undoable operations."
//    ///   </para>
//    ///   <para>
//    ///     Command: abstact base class 
//    ///     Client: the creator of the Command
//    ///     Invoker: Executes the command
//    ///     Receiver: Any class that is acted on by a Command
//    ///   </para>
//    ///   <para>
//    ///     ICommand is defined in the FF, but not as elegantly 
//    ///     as I would like.
//    ///   </para>
//    ///   <para>
//    ///     First of all, it is a UI specific implementation, defined
//    ///     in <c>System.Windows.Input</c> (available since .NET 3.0).
//    ///   </para>
//    ///   <para>
//    ///     Secondly, it doesn't define an Undo/UnExecute.
//    ///   </para>
//    /// </remarks>
//    /// <internal>
//    ///   References: http://bit.ly/fq5LLf
//    /// </internal>
//    //[Obsolete("ICommand was a pattern developed prior to SOLID/Dependency Injection.")]
//    public interface ICommand : IHasExecutableAction
//    {
//        /// <summary>
//        ///   Event raised when the command wishes to notify 
//        ///   an <see cref = "ICommandSource{TTarget}" /> that it should check
//        ///   this command (using <see cref = "CanExecute" />).
//        /// </summary>
//        event EventHandler CanExecuteChanged;

//        /// <summary>
//        ///   Defines the method that determines whether the 
//        ///   command can execute in its current state
//        ///   <para>
//        ///     Usually invoked by the <see cref = "ICommandSource{TTarget}" />
//        ///     after it has received a <c>CanExecuteChanged</c> event notification.
//        ///   </para>
//        /// </summary>
//        /// <remarks>
//        ///   Typically, an <see cref = "ICommand" /> based Command will raise the 
//        ///   <see cref = "CanExecuteChanged" /> event when something changes 
//        ///   (eg: it knows that Network is no longer available), so that the 
//        ///   UI layer can query the Command's <see cref = "CanExecute" /> method, 
//        ///   in order to update the UI to reflect whether the Command
//        ///   is currently available.
//        /// </remarks>
//        /// <returns>
//        ///   <c>true</c> if this instance can execute the specified parameter; otherwise, <c>false</c>.
//        /// </returns>
//        bool CanExecute();


//        /// <summary>
//        /// Determines whether this command can be rolled back if Execute is invoked.
//        /// </summary>
//        /// <returns>
//        /// 	<c>true</c> if this instance can unexecute the command; otherwise, <c>false</c>.
//        /// </returns>
//        /// <internal><para>8/10/2011: Sky</para></internal>
//        bool CanUnExecute();


//        /// <summary>
//        ///   Defines the method to be called when the command is invoked.
//        /// </summary>
//        /// <internal>
//        /// The method is still accessible as IExecutableFunction
//        /// </internal>
//        new void Execute();

//        /// <summary>
//        /// Rolls back the work performed by <see cref="Execute"/>
//        /// </summary>
//        void UnExecute();
//    }
//}