﻿namespace XAct.Commands
{
    /// <summary>
    /// An interface that can be applied to a <see cref="ICommandMessage"/>
    /// to provide a common way of retrieving a result from the execution
    /// of a message.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICommandMessageResult<T>:ICommandMessage
    {
        /// <summary>
        /// The result of the Execution of
        /// this <see cref="ICommandMessage"/>
        /// </summary>
        T Result { get; set; }
    }
}