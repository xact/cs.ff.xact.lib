﻿namespace XAct.Commands
{
    using XAct.Commands.Configuration;
    using XAct.State;


    public interface ISimpleContextCommandService
    {
        void Execute();
    }


    public class SimpleContextCommandService : ISimpleContextCommandService
    {
        /*
         * The service is for ASP.NET and ASP.MVC scenarios.
         * See: http://www.pluralsight.com/courses/aspdotnet-undo
         * 
         * The service adds to the basic scenario in that Executing an action
         * Adds a cookie to the outoing response, cointaining a guid.
         * The incoming cookie is then removed from the response (expired)
         * while its contents -- the guid -- is transferred to the Context.
         * If the Context has an Items of known name -- eg "Undo", 
         * the UI can build itself with a message banner, with an action link
         * to undo the delete.
         * The Guid is used to retrieve from the Datastore, the record that
         * contains the id of the record that was deleted.
         * The datastore record is deleted after a set time (in case it was not used)
         * The datastore record is deleted if the record is used to rehydrate an undeleted.
         * RegisterUndoCommand -> Execute
         * BeginRequestHandler -> Put into Global.Begin_Request to clear out any cookies/set Context
         * IsUndoAvailable -> a property
         * GetUndoFromContextItem ->
         * UndoCommandDescription -> a Property (...hum)
         * Undo () -> the actual Undo operation is invoked.
         * 
         * RegisterUndoCommand(Command command){
         *   Gudi id= undoRepo.Save(command);
         *   setUndoCookie(id);
         * }
         * 
         */

        private readonly ICommandService _commandService;
        private readonly IContextStateService _contextStateService;

        SimpleContextCommandService(ICommandService commandService, IContextStateService contextStateService)
        {
            _commandService = commandService;
            _contextStateService = contextStateService;
        }

        

        public void Execute()
        {
            
        }
    }

    /// <summary>
    /// A contract for a dispatcher of a 
    /// <see cref="ICommandMessageHandler{TComandMessage}"/>
    /// </summary>
    public interface ICommandService : IHasXActLibService, IHasWebThreadBindingScope
    {

        ICommandServiceConfiguration Configuration { get; }

        int HistoryIndex { get; }
        int HistoryLength { get; }

        /// <summary>
        /// Finds a <see cref="ICommandMessageHandler{TCommandMessage}" />
        /// appropriate to the given
        /// <paramref name="commandMessages" />
        /// and executes it.
        /// </summary>
        /// <param name="commandMessages">The command messages.</param>
        void Execute(params ICommandMessage[] commandMessages);

        /// <summary>
        /// Finds a <see cref="ICommandMessageHandler{TCommandMessage}" />
        /// appropriate to the given
        /// <paramref name="commandMessages" />
        /// and executes it.
        /// </summary>
        /// <param name="executeImmetiately">if set to <c>true</c> [execute immetiately].</param>
        /// <param name="commandMessages">The command messages.</param>
        void Execute(bool executeImmetiately, params ICommandMessage[] commandMessages);


        /// <summary>
        /// Undo a number of already executed steps.
        /// </summary>
        /// <param name="steps"></param>
        void Undo(int steps = 1);

        /// <summary>
        /// Redo steps (as long as new Commands have not been executed, wiping out undone steps).
        /// </summary>
        /// <param name="steps"></param>
        void Redo(int steps = 1);

    }
}
