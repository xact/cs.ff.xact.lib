﻿namespace XAct.Commands.Configuration
{
    /// <summary>
    /// Contract for a configuration object for the <see cref="ICommandService"/>
    /// </summary>
    public interface ICommandServiceConfiguration: IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// The default implementation of <see cref="ICommandService"/>
        /// only persists information about Commands that implement
        /// <see cref="ICommandRecordable"/>, unless this parameter 
        /// is set to <c>True</c>.
        /// </summary>
        bool RecordAllCommands { get; set; }

    }
}