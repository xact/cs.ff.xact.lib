﻿namespace XAct.Commands
{

    /// <summary>
    /// A service to manage the records of Commands executed 
    /// by <see cref="ICommandService"/>
    /// </summary>
    public interface ICommandRecordService : IHasXActLibService
    {
        void RecordCommand(ICommandMessage commandMessage, bool commandIsUndoable, bool unexecute = false);
    }
}