﻿
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 
using System.Diagnostics.Contracts;
#endif

namespace XAct.Commands.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using XAct.Commands.Configuration;
    using XAct.Commands.Services.State;
    using XAct.Commands.Services.State.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="ICommandService"/>
    /// to process data by one or more <see cref="ICommandService"/>s.
    /// </summary>
    /// <remarks>
    /// Note that the <c>DefaultBindingImplementation</c> uses
    /// <see cref="BindingLifetimeType.SingletonPerWebRequestScope"/>
    /// as this object is Stateful (it contains a Queue).
    /// </remarks>
    public class  CommandService : ICommandService
    {
        //private readonly ITracingService _tracingService;
        //private readonly IDateTimeService _dateTimeService;
        private readonly ICommandRecordService _commandRecordService;
        //private readonly IRepositoryService _repositoryService;
        private readonly ICommandServiceState _commandMessageDispatcherServiceState;

        public ICommandServiceConfiguration Configuration { get; private set; }

        public CommandService(
            //ITracingService tracingService, 
            //IDateTimeService dateTimeService,
            ICommandRecordService commandRecordService,
            ICommandServiceConfiguration commandServiceConfiguration,
            ICommandServiceState commandMessageDispatcherServiceState)
        {
            //_tracingService = tracingService;
            //_dateTimeService = dateTimeService;
            _commandRecordService = commandRecordService;
            Configuration = commandServiceConfiguration;
            _commandMessageDispatcherServiceState = commandMessageDispatcherServiceState;
        }

        public int HistoryLength { get { return _commandMessageDispatcherServiceState.History.Count; } }

        public int HistoryIndex
        {
            get { return _commandMessageDispatcherServiceState.HistoryIndex; }
            private set { _commandMessageDispatcherServiceState.HistoryIndex = value; }
        }


        /// <summary>
        /// Executes the <see cref="ICommandMessage" /> using its
        /// associated <see cref="ICommandMessageHandler{ICommandMessage}" />
        /// </summary>
        /// <param name="commandMessages">The command messages.</param>
        public void Execute(ICommandMessage[] commandMessages)
        {
            //Call overload:
            Execute(true, commandMessages);

        }


        /// <summary>
        /// Finds a <see cref="ICommandMessageHandler{TCommandMessage}" />
        /// appropriate to the given
        /// <paramref name="commandMessages" />
        /// and executes it.
        /// </summary>
        /// <param name="executeImmetiately">if set to <c>true</c> [execute immetiately].</param>
        /// <param name="commandMessages">The command messages.</param>
        public void Execute(bool executeImmetiately, params ICommandMessage[] commandMessages)
        {
            EnqueueCommands(commandMessages);

            if (executeImmetiately)
            {
                ExecuteCommands();
            }

            //No. We won't be serializing the Result State,
            //as a) it could often be large resultsets... b) not sure if we want the later state.


        }


        public void Undo(int steps=1)
        {
            for (int i = 0; i < steps; i++)
            {
                if (HistoryIndex <= 0)
                {
                    //All undone:
                    break;
                }

                //Since Index is the position of the *NEXT* insert, 
                //when going backwards, we Decrement the counter before:
                HistoryIndex--;

                var kvp =
                    _commandMessageDispatcherServiceState.History[HistoryIndex];

                try
                {

                    InvokeOperationAndUpdateCommandRecord(kvp, true);
                }
                catch
                {
                    //If unexecute fails, you throw away all older commands (undo history). 
                    throw;
                }
            }
        }


        public void Redo(int steps=1)
        {
            for (int i = 0; i < steps; i++)
            {
                
                if (HistoryIndex >= _commandMessageDispatcherServiceState.History.Count)
                {
                    //Index is back to being same as Size.
                    //ie, position of next record.
                    break;
                }

                //Get current position:
                var kvp =
                    _commandMessageDispatcherServiceState.History[HistoryIndex];

                //Incrementing afterwards:
                HistoryIndex++;
                try
                {
                    InvokeOperationAndUpdateCommandRecord(kvp);
                }
                catch
                {
                    //If execute fails during redo, you throw away all newer commands (redo history). 
                    throw;
                }
            }
        }

        public void ClearHistory()
        {
            _commandMessageDispatcherServiceState.History.Clear();
            _commandMessageDispatcherServiceState.HistoryIndex = 0;
        }




        public ICommandMessageHandler<TCommandMessage> GetHandler<TCommandMessage>(TCommandMessage commandMessage)
            where TCommandMessage : ICommandMessage
        {
            ICommandMessageHandler<TCommandMessage> result = GetHandlerInternal(commandMessage, typeof(TCommandMessage)) as ICommandMessageHandler<TCommandMessage>;

            return result;
        }

        public object GetHandlerInternal<TCommandMessage>(TCommandMessage commandMessage, Type commandMessageType)
            where TCommandMessage : ICommandMessage
        {
            if (commandMessageType == null)
            {
                commandMessageType = commandMessage.GetType();
            }


            Type genericCommandMessageHandlerType = typeof(ICommandMessageHandler<>);
            Type commandMessageHandlerType = genericCommandMessageHandlerType.MakeGenericType(new[] { commandMessageType });

            //If it's a hybrid Command/CommandHandler:
            if (commandMessageHandlerType.IsAssignableFrom(commandMessageType))
            {
                return commandMessage;
            }

            var result = XAct.DependencyResolver.Current.GetInstance(commandMessageHandlerType);
            return result;
        }



        private void EnqueueCommands(IEnumerable<ICommandMessage> commandMessages)
        {

            var queue = _commandMessageDispatcherServiceState.Queue;
            foreach (ICommandMessage commandMessage in commandMessages)
            {
                var commandMessageHandler = GetHandlerInternal(commandMessage, commandMessage.GetType());

                queue.Enqueue(
                    new CommandHistoryItem
                        {
                            Key = null, 
                            Value = commandMessage,
                            Handler = commandMessageHandler
                        });
            }
        }

        private void ExecuteCommands()
        {
            var queue = _commandMessageDispatcherServiceState.Queue;
            while (queue.Count > 0)
            {
                CommandHistoryItem kvp = queue.Dequeue();

                InvokeOperationAndUpdateCommandRecord(kvp);

                //So that it can be undone:
                AddToHistory(kvp);
            }
        }

        // Note that this method is invoked by ExecuteCommand
        // as well as Undo/Redo
        private void InvokeOperationAndUpdateCommandRecord(CommandHistoryItem kvp, bool unexecute=false)
        {
            ICommandMessage commandMessage = kvp.Value;
            object handler = kvp.Handler;


            //Issue:
            //It would be easier to create record only after,
            //but if I serialize it then, it has the Result embedded.
            //But if i do it before, it doesn't have State...

            handler.InvokeMethod(unexecute ? "Unexecute" : "Execute", new[] { commandMessage });
            //Record after success (no record created if unsuccessful).

            _commandRecordService.RecordCommand(commandMessage, kvp.Handler.Undoable,unexecute);

        }


        //invoked by Execute -- not Redo.
        private void AddToHistory(CommandHistoryItem kvp)
        {
            //If we are adding, whatever history we had, if we rewound, we truncate the old branch:
            _commandMessageDispatcherServiceState.History.Truncate(HistoryIndex);

            //Add at the current End 
            _commandMessageDispatcherServiceState.History.Add(kvp);

            //Important:increment counter at same time:
            HistoryIndex++;
            Debug.Assert(HistoryIndex == _commandMessageDispatcherServiceState.History.Count,"HistoryCount should be same as History Size.");
        }
    }
}