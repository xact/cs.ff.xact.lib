﻿namespace XAct.Commands.Services.Implementations
{
    using XAct.Commands.Models;
    using XAct.Domain.Repositories;
    using XAct.Environment;

    public class CommandRecordService : ICommandRecordService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IApplicationTennantService _applicationTennantService;
        private readonly IPrincipalService _principalService;
        private readonly IRepositoryService _repositoryService;

        public CommandRecordService(IDateTimeService dateTimeService, IApplicationTennantService applicationTennantService, IPrincipalService principalService, IRepositoryService repositoryService)
        {
            _dateTimeService = dateTimeService;
            _applicationTennantService = applicationTennantService;
            _principalService = principalService;
            _repositoryService = repositoryService;
        }

        public void RecordCommand(ICommandMessage commandMessage, bool commandIsUndoable, bool unexecute = false)
        {
            CommandRecord commandRecord = new CommandRecord();

            commandRecord.ApplicationTennantId = _applicationTennantService.Get();
            commandRecord.UserIdentifier = _principalService.CurrentIdentityIdentifier;

            //Mark whether the operation is undoable or not:
            commandRecord.Undoable = commandIsUndoable;
            //And whether this is a Do or Undo operation:
            commandRecord.Operation = unexecute ? true : false;


            commandRecord.CreatedOnUtc = _dateTimeService.NowUTC;
            commandRecord.LastModifiedOnUtc = commandRecord.CreatedOnUtc;

            //Embed the CommandMessage and its arguments and possibly its State
            commandRecord.SerializeValue(commandMessage, SerializationMethod.Json, commandMessage.GetType());

            _repositoryService.AddOnCommit(commandRecord);
        }
    }
}