namespace XAct.Commands.Services.State
{
    using System.Collections.Generic;
    using XAct.Commands.Services.State.Implementations;

    public interface ICommandServiceState : 
        IHasXActLibServiceState
    {

        Queue<CommandHistoryItem> Queue { get; }
        List<CommandHistoryItem> History { get; }

        /// <summary>
        /// Number of Executed Oeprations.
        /// <para>
        /// And the Position of next operation.
        /// </para>
        /// </summary>
        int HistoryIndex { get; set; }
    }
}