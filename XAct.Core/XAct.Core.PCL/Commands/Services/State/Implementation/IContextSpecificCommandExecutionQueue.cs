namespace XAct.Commands.Services.State
{
    using System.Collections.Generic;
    using XAct.Commands.Services.State.Implementations;

    /// <summary>
    /// Contract for a service to return a Command Queue, isolated by Context
    /// so that it is usable in web application as well as a desktop application.
    /// </summary>
    public interface IContextSpecificCommandExecutionQueue :
        IWebThreadSpecificContextManagementServiceBase<Queue<CommandHistoryItem>>
    {

    }
}