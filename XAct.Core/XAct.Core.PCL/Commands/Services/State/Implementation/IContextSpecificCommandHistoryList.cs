namespace XAct.Commands.Services.State
{
    using System.Collections.Generic;
    using XAct.Commands.Services.State.Implementations;

    /// <summary>
    /// A contract for a context specific list of commands already executed, that can later be undone.
    /// </summary>
    public interface IContextSpecificCommandHistoryList :
        IWebThreadSpecificContextManagementServiceBase<List<CommandHistoryItem>>
    {
        
    }
}