namespace XAct.Commands.Services.State.Implementations
{
    using System.Collections.Generic;
    using XAct.Diagnostics;
    using XAct.State;

    /// <summary>
    /// An implementation of <see cref="IContextSpecificCommandExecutionQueue"/>
    /// to provide a <see cref="ICommandMessage"/> processing queue, isolated by Context.
    /// </summary>
    public class ContextSpecificCommandExecutionQueue :
        WebThreadSpecificContextManagementServiceBase<Queue<CommandHistoryItem>>,
        IContextSpecificCommandExecutionQueue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContextSpecificCommandExecutionQueue"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="contextStateService">The context state service.</param>
        public ContextSpecificCommandExecutionQueue(ITracingService tracingService,
                                                IContextStateService contextStateService)
            : base(tracingService, contextStateService)
        {
        }


        protected override Queue<CommandHistoryItem> CreateNewSourceInstance
            ()
        {
            //return base.CreateNewSourceInstance();
            //Queue has several constructors, and Unity got confused:
            return new Queue<CommandHistoryItem>();
        }

    }
}
