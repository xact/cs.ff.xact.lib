namespace XAct.Commands.Services.State.Implementations
{
    using System.Runtime.Serialization;
    using XAct.Commands.Models;

    [DataContract]
    public class CommandHistoryItem : XAct.KeyValue<CommandRecord, ICommandMessage>
    {
        //[DataMember]
        //public CommandRecord Key { get; set; }
        //[DataMember]
        //public ICommandMessage Value { get; set; }
        [DataMember]
        public dynamic Handler { get; set; }
    }
}