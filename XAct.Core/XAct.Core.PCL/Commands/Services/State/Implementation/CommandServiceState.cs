namespace XAct.Commands.Services.State.Implementations
{
    using System.Collections.Generic;
    using XAct.Commands.Services.State;
    using XAct.State;

    /// <summary>
    /// An implementation of the <see cref="ICommandServiceState"/>
    /// to manage state for the <see cref="ICommandService"/>
    /// </summary>
    public class CommandServiceState :
        ICommandServiceState
    {
        private readonly string _counterKey;

        private readonly IContextStateService _contextStateService;
        private readonly IContextSpecificCommandExecutionQueue _contextSpecificCommandExecutionQueue;
        private readonly IContextSpecificCommandHistoryList _contextSpecificCommandHistoryList;

        public Queue<CommandHistoryItem> Queue { get { return _contextSpecificCommandExecutionQueue.Current; } }
        public List<CommandHistoryItem> History { get { return _contextSpecificCommandHistoryList.Current; } }

        public int HistoryIndex
        {
            get
            {
                object o = _contextStateService.Items[_counterKey];
                if (o == null)
                {
                    _contextStateService.Items[_counterKey] = o = 0;
                }
                return (int) o;
            }set
            {
                _contextStateService.Items[_counterKey] = value;
            }
        }

        public CommandServiceState(
            IContextStateService contextStateService,
            IContextSpecificCommandExecutionQueue contextSpecificCommandExecutionQueue,
            IContextSpecificCommandHistoryList contextSpecificCommandHistoryList)
        {
            _counterKey = this.GetType().Name + ":" + "HistoryCounter";

            _contextStateService = contextStateService;
            _contextSpecificCommandExecutionQueue = contextSpecificCommandExecutionQueue;
            _contextSpecificCommandHistoryList = contextSpecificCommandHistoryList;
        }

        
    }
}
