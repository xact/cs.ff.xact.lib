namespace XAct.Commands.Services.State.Implementations
{
    using System.Collections.Generic;
    using XAct.Diagnostics;
    using XAct.State;

    /// <summary>
    /// An implementation of <see cref="IContextSpecificCommandHistoryList"/>
    /// to provide a context specific list of commands already executed, that can later be undone.
    /// </summary>
    public class ContextSpecificCommandHistoryList:
        WebThreadSpecificContextManagementServiceBase<List<CommandHistoryItem>>,
        IContextSpecificCommandHistoryList
    {
        public ContextSpecificCommandHistoryList(ITracingService tracingService, IContextStateService contextStateService) : base(tracingService, contextStateService)
        {
        }

        protected override List<CommandHistoryItem> CreateNewSourceInstance()
        {
            //return base.CreateNewSourceInstance();
            //Queue has several constructors, and Unity got confused:
            return new List<CommandHistoryItem>();
        }

    }
}