﻿namespace XAct.Commands
{
    /// <summary>
    /// Provides a common Interface for 
    /// saving State before the operation is 
    /// Executed.
    /// <para>
    /// The State will be serialized and persisted,
    /// if the <see cref="ICommandMessage"/> implementation
    /// also implements <see cref="ICommandMessageRecordable"/>
    /// </para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICommandMessageState<T> :ICommandMessage
    {
        /// <summary>
        /// The State recorded just prior to Execution.
        /// <para>
        /// It's filled by <see cref="ICommandMessageHandler{T}.Execute"/>
        /// </para>
        /// </summary>
        T State { get; set; }

    }
}
