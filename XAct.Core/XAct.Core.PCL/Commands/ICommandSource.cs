﻿//namespace XAct
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    ///   Defines an object that knows how to invoke a command.
//    /// </summary>
//    /// <remarks>
//    ///   <para>
//    ///     The FF defines 
//    ///     <c>ICommandSource.CommandTarget</c> in
//    ///     System.Windows.Input.
//    ///   </para>
//    ///   <para>
//    ///     Note that this implementation differs to the 
//    ///     FF version in that the CommandTarget
//    ///     is not limited to <c>System.Windows.IInputElement</c>
//    ///   </para>
//    /// </remarks>
//    public interface ICommandSource<TTarget>
//    {
//        /// <summary>
//        ///   Gets or sets the <see cref = "ICommand" />
//        ///   based Command that will be executed when the command source is invoked.
//        /// </summary>
//        /// <value>The command.</value>
//        ICommand Command { get; }


//        /// <summary>
//        ///   Gets the command specific data.
//        /// </summary>
//        /// <value>The command parameter.</value>
//        object CommandParameter { get; }


//        /// <summary>
//        ///   The object that the command is being executed on.
//        /// </summary>
//        TTarget CommandTarget { get; }
//    }
//}