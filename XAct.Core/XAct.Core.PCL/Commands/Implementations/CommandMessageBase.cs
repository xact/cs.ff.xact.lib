﻿namespace XAct.Commands
{

    public abstract class CommandMessageBase : ICommandMessage
    {
        /// <summary>
        /// Gets the Filter for the Resource in the <c>XAct.Resources.IResourceFilter, XAct.Resources</c>.
        /// </summary>
        public string ResourceFilter { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// <para>
        /// Member defined in the <see cref="IHasTitle" /> contract.
        /// </para>
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The Text to display in an Undo panel on the UI.
        /// <para>
        /// Example: "Undo of Delete Selected Student(s)?"
        /// </para>
        /// <para>
        /// If <see cref="IHasResourceFilterReadOnly.ResourceFilter" /> is set,
        /// the Property Text is that of a <c>ResourceKey</c>.
        /// </para>
        /// </summary>
        public string UndoTitle { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMessageBase"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="undoTitle">The undo title.</param>
        /// <param name="resourceFilter">The resource filter.</param>
        protected CommandMessageBase(string title, string undoTitle, string resourceFilter = null)
        {
            Title = title;
            UndoTitle = undoTitle;
        }

        protected CommandMessageBase()
        {
            Title = this.GetType().Name;
            UndoTitle = "Undo " + Title;
        }
    }
}
