﻿namespace XAct.Commands
{
    using System;
    using System.Runtime.Serialization;
    using XAct;
    using XAct.Messages;

    /// <summary>
    /// Abstract base class for CommandHandlers
    /// that implement the
    /// <see cref="ICommandMessageHandler{TCommandMessage}"/>
    /// contract.
    /// <para>
    /// It's purpose is to simplify the creation of new CommandMessageHandlers.
    /// </para>
    /// <code>
    /// <![CDATA[
    /// public class MyCommandMessageHandler : CommandMessageHandlerBase<MyCommand,MyCommandResponse> {
    /// 
    /// }
    /// ]]>
    /// </code>
    /// </summary>
    /// <internal>
    ///   See: http://bit.ly/h62amx
    /// </internal>
    /// <typeparam name="TCommandMessage">The type of the command message.</typeparam>
    [DataContract]
    public abstract class CommandMessageHandlerBase<TCommandMessage> :
        ICommandMessageHandler<TCommandMessage>
        where TCommandMessage : ICommandMessage
    {
        /// <summary>
        /// Occurs when the conditions within which the 
        /// Command can be executed or not, change.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        public bool Undoable { get { return _canUndo; } }
        [IgnoreDataMember]
        protected bool _canUndo;

        
        // ReSharper disable InconsistentNaming
// ReSharper restore InconsistentNaming

        private Func<TCommandMessage, IResponse> _validateForExecution;
        private Func<TCommandMessage, IResponse> _validateForUnexecute;

        private Action<TCommandMessage> _execute;
        private Action<TCommandMessage> _unexecute;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMessageHandlerBase{TCommandMessage}"/> class.
        /// </summary>
        /// <param name="execute">The execute.</param>
        protected CommandMessageHandlerBase(
            Action<TCommandMessage> execute
// ReSharper disable RedundantArgumentDefaultValue
            ):this(execute, validateForExecution: null, canUndo: false, validateForUnexecute: null, unExecute: null){}
// ReSharper restore RedundantArgumentDefaultValue


        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMessageHandlerBase{TCommandMessage}"/> class.
        /// </summary>
        /// <param name="execute">The execute.</param>
        /// <param name="validateForExecution">The validate for execution.</param>
        /// <param name="canUndo">if set to <c>true</c> [can undo].</param>
        /// <param name="validateForUnexecute">The validate for unexecute.</param>
        /// <param name="unExecute">The un execute.</param>
        /// <exception cref="System.ArgumentException">Need an Execute Action, at the very least.</exception>
        protected CommandMessageHandlerBase(
            Action<TCommandMessage> execute,
            Func<TCommandMessage, IResponse> validateForExecution = null,
            bool canUndo = false,
            Func<TCommandMessage, IResponse> validateForUnexecute = null,
            Action<TCommandMessage> unExecute = null
            )
        {

            if (execute == null){throw new ArgumentException("Need an Execute Action, at the very least.");}

            _execute = execute;



            //Execute:
            if (validateForExecution == null)
            {
                validateForExecution = x => new Response();
            }
            _validateForExecution = validateForExecution;

            _canUndo = canUndo;

            //Unexecute:
            if (validateForUnexecute == null)
            {
                validateForUnexecute = x =>
                                           {
                                               var r = new Response();
                                               r.AddMessage(new MessageCode(XAct.Library.Constants.MessageCodes.OperationValidationFailed, Severity.BlockingWarning));
                                               return r;
                                           };
            }
            _validateForUnexecute = validateForUnexecute;

            if (unExecute == null)
            {
                //No nothing:
                unExecute = x => { };
            }

            _unexecute = unExecute;
        }

        /// <summary>
        ///   Determines whether the Command can execute in its current state.
        ///   <para>
        ///   Internally, it's simply invoking <see cref="ValidateForExecution"/>  
        ///   and returning just the <see cref="IResponse.Success"/> flag.
        /// </para>
        /// </summary>
        public bool CanExecute (TCommandMessage commandMessage)
        {
            return ValidateForExecution(commandMessage).Success;
        }



        /// <summary>
        /// Validate the 
        /// <see cref="ICommandMessage"/>
        /// properties and required services, 
        /// before Execution.
        /// <para>
        /// The default implementation has no default validation logic, always returning a successful CommandResponse.
        /// As it's not actually validating is occuring, no error messages need returning either,
        /// hence why a simple CommandResponse suffices.
        /// </para>
        /// <para>
        /// <code>
        /// <![CDATA[
        /// public virtual ICommandResponse ValidateForExecution(TCommandMessage commandMessage) 
        /// {
        ///    return new CommandResponse { Success = true };
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// Override to provide useful validation appropriate to the command being implemented,
        /// and return a custom CommandResponse that contains error messages explaining why it failed.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public virtual IResponse ValidateForExecution(TCommandMessage commandMessage)
        {
            return _validateForExecution(commandMessage);
        }



        /// <summary>
        /// Execute an operation, using the arguments contained
        /// in the given <paramref name="commandMessage"/>
        /// that
        /// </summary>
        public virtual void Execute (TCommandMessage commandMessage)
        {
            _execute(commandMessage);
        }



        /// <summary>
        /// Raises the <see cref="CanExecuteChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void OnCanExecuteChanged(EventArgs e)
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, e);
            }
        }


        /// <summary>
        /// Determines whether this command can be rolled back if <c>Execute</c> is invoked.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance can unexecute the command; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>8/10/2011: Sky</para></internal>
        public bool CanUnexecute(TCommandMessage commandMessage)
        {
            return ValidateForUnexecution(commandMessage).Success;
        }


        /// <summary>
        /// Validate the 
        /// <see cref="ICommandMessage"/>
        /// properties and required services, 
        /// before Unexecution.
        /// </summary>
        /// <returns></returns>
        public virtual IResponse ValidateForUnexecution(TCommandMessage commandMessage)
        {
            if (!_canUndo)
            {
                return cantUndoError();
            }
            return _validateForUnexecute(commandMessage);
        }


        /// <summary>
        /// Rolls back the action performed by the 
        /// <c>Execute</c> method.
        /// </summary>
        /// <param name="commandMessage">The command message.</param>
        /// <returns></returns>
        public virtual void Unexecute(TCommandMessage commandMessage)
        {
            if (!_canUndo)
            {

                throw new Exception("Do not call Unexecute on Commands that can't be Unexecuted. Check first with ValdiateForUnexecution.");
            }
            _unexecute(commandMessage);
        }

        protected Response cantUndoError()
        {
            var r = new Response();

            r.AddMessage(new MessageCode(XAct.Library.Constants.MessageCodes.OperationNotPossible, Severity.Error));
            
            return r;
        }
    }
}