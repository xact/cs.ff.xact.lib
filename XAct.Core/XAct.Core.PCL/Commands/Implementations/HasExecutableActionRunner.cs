﻿// ReSharper disable CheckNamespace
namespace XAct.Commands
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics;

    /// <summary>
    /// A means to execute a series of classes that implement
    /// <see cref="IHasExecutableAction" />
    /// </summary>
    public class HasExecutableActionRunner : IHasExecutableAction, IHasInitializedReadOnly
    {
        /// <summary>
        /// Gets or sets the indent level
        /// used by <see cref="ExecutableRunnerActionResult.ToString"/>
        /// to make more readable reports.
        /// </summary>
        /// <value>
        /// The indent level
        /// </value>
        public static int IndentLevel { get { return _indentLevel; } }
        private static int _indentLevel = -1;

        /// <summary>
        /// Gets or sets the global *static* summary
        /// to which results are added if <see cref="AddToGlobalResults"/>
        /// is <c>True</c>
        /// </summary>
        /// <value>
        /// The global summary.
        /// </value>
        public static ExecutableRunnerActionResult[] GlobalSummary { get; protected set; }




        /// <summary>
        /// Gets or sets a value indicating whether [throw on exception].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [throw on exception]; otherwise, <c>false</c>.
        /// </value>
        public readonly bool ThrowOnException;


        /// <summary>
        /// Optional Tag to indicated context of run
        /// <para>
        /// eg: Name of the class that is invoking the runner.
        /// </para>
        /// </summary>
        public readonly string ContextTag;


        /// <summary>
        /// Whether the runner should add the results to the global results.
        /// </summary>
        public readonly bool AddToGlobalResults;


        private readonly Type[] _stepTypes = new Type[0];

        /// <summary>
        /// Gets a value indicating whether the object is initialized
        /// using <see cref="T:XAct.IHasInitialize" />.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool Initialized { get; protected set; }


        /// <summary>
        /// Gets the summary information of the results.
        /// <para>
        /// Zero size till  
        /// </para>
        /// </summary>
        /// <value>
        /// The summary.
        /// </value>
        public ExecutableRunnerActionResult[] Summary { get { return _summary.ToArray(); } }
        readonly List<ExecutableRunnerActionResult> _summary = new List<ExecutableRunnerActionResult>();



        /// <summary>
        /// Gets or sets the optional callback that will be executed after
        /// each given step has been executed.
        /// </summary>
        /// <value>
        /// The callback.
        /// </value>
        public Action<ExecutableRunnerActionResult> Callback { get; set; }




        /// <summary>
        /// Initializes a new instance of the <see cref="HasExecutableActionRunner" /> class.
        /// </summary>
        /// <param name="stepTypes">The step types.</param>
        /// <param name="contextTag"></param>
        public HasExecutableActionRunner(string contextTag = null, params Type[] stepTypes)
            : this(contextTag, true, false, null, stepTypes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HasExecutableActionRunner" /> class.
        /// </summary>
        /// <param name="contextTag">The context tag.</param>
        /// <param name="addToGlobalResults">if set to <c>true</c> [add to global results].</param>
        /// <param name="callback">The optional callback.</param>
        /// <param name="throwOnException">if set to <c>true</c> throws the exception, aborting the chain of operations, rather than absorb it.</param>
        /// <param name="stepTypes">The step types.</param>
        public HasExecutableActionRunner(
            string contextTag = null,
            bool throwOnException = true,
            bool addToGlobalResults = false,
            Action<ExecutableRunnerActionResult> callback = null,
            params Type[] stepTypes)
        {
            this.ContextTag = contextTag;

            this._stepTypes = stepTypes;


            this.AddToGlobalResults = addToGlobalResults;

            this.ThrowOnException = throwOnException;

            Callback = callback;
        }


        /// <summary>
        /// Begins creation and invocation of the
        /// <see cref="IHasExecutableAction"/>
        /// types in the <c>StepTypes</c> array.
        /// <para>Member defined in<see cref="IHasExecutableAction" /></para>
        /// </summary>
        public void Execute()
        {

            if (Initialized)
            {
                return;
            }
            lock (this)
            {
                InternalExecute();
            } //~lock
        }

        private void InternalExecute()
        {
            if (Initialized)
            {
                return;
            }
            using (TimedScope timedScope = new TimedScope())
            {
                //Execute each Initialization Step
                //...remember that one of the steps is going to invoke the SmokeTests in sequence as well.
                if ((_stepTypes == null) || (_stepTypes.Length == 0))
                {
                    return;
                }


                if (AddToGlobalResults)
                {
                    _indentLevel++;
                }

                //Loop through each step:
                foreach (Type initializationStepType in this._stepTypes)
                {
                    //Prepare results:
                    ExecutableRunnerActionResult runState =
                        new ExecutableRunnerActionResult
                        {
                            IndentLevel = HasExecutableActionRunner.IndentLevel,
                            StepType = initializationStepType
                        };


                    _summary.Add(runState);

                    //Probably rarely needed (see: Bootstrapping/Init/Integration steps):
                    if (AddToGlobalResults)
                    {
                        AddToGlobalSummary(runState);
                    }


                    InvokeStep(runState);
                }

                if (AddToGlobalResults)
                {
                    _indentLevel--;

                    //Correction:
                    if (_indentLevel < -1)
                    {
                        _indentLevel = -1;
                    }
                }

                //Log completion:
                if (XAct.DependencyResolver.BindingResults.IsInitialized)
                {
                    ITracingService tracingService =
                        DependencyResolver.Current.GetInstance<ITracingService>(false);

                    if (tracingService != null)
                    {
                        DependencyResolver.Current.GetInstance<ITracingService>()
                            .Trace(
                                TraceLevel.Verbose,
                                "{0}: Elapsed: {1}ms".FormatStringInvariantCulture(
                                    ContextTag,
                                    timedScope.Elapsed.TotalMilliseconds));
                    }
                }

                //Mark run as complete:
                Initialized = true;
            } //~timedScope
        }

        //Run just one step:
        private void InvokeStep(ExecutableRunnerActionResult runState)
        {
            using (TimedScope timedScope = new TimedScope())
            {
                try
                {
                    Type initializationStepType = runState.StepType;

                    IHasExecutableAction stepInstance;

                    //Depending if given an interface or not, 
                    //use ServiceLocator:
                    if (!initializationStepType.IsInterface)
                    {
                        stepInstance =
                            (IHasExecutableAction)
                            System.Activator.CreateInstance(initializationStepType);
                    }
                    else
                    {
                        //If we got here, it's presumably because we've crossed the point where
                        //we have registered all services, therefore ok to Trace:

                        //BACK:
                        DependencyResolver.Current.GetInstance<ITracingService>()
                            .Trace(TraceLevel.Verbose,
                                   "*** Running '{0}'".FormatStringInvariantCulture(initializationStepType.Name));

                        stepInstance =
                            (IHasExecutableAction)
                            DependencyResolver.Current.GetInstance(initializationStepType);
                    }

                    //Save ref to Step:
                    runState.Step = stepInstance;

                    //Run:
                    stepInstance.Execute();

                    //Hey...wow...no exception:
                    runState.Success = true;
                }
                catch (System.Exception e)
                {
                    //Bummer:
                    runState.Exception = e;

                    //Trace exception service locator is up and running:
                    ITracingService tracingService =
                        DependencyResolver.Current.GetInstance<ITracingService>(false);

                    if (tracingService != null)
                    {
                        tracingService.TraceException(
                            TraceLevel.Error,
                            e,
                            "'{0}' step ('{1}') Failed: ".FormatStringInvariantCulture(
                                this.ContextTag,
                                runState.StepType.Name));
                    }

                    //By Default throw, but maybe not in some scenarios:
                    if (ThrowOnException)
                    {
                        throw;
                    }


                }
                finally
                {
                    runState.Duration = timedScope.Elapsed;

                    //Report back to callback how the step executed/failed:
                    if (Callback != null)
                    {
                        Callback(runState);
                    }

                }
            }
        }

        /// <summary>
        /// Adds the given summary object to the <see cref="HasExecutableActionRunner.GlobalSummary"/>.
        /// </summary>
        /// <param name="executableRunnerActionResult">The executable runner action result.</param>
        public static void AddToGlobalSummary(ExecutableRunnerActionResult executableRunnerActionResult)
        {
            List<ExecutableRunnerActionResult> tmp = new List<ExecutableRunnerActionResult>();
            if (GlobalSummary != null)
            {
                tmp.AddRange(GlobalSummary);
            }
            tmp.Add(executableRunnerActionResult);

            GlobalSummary = tmp.ToArray();

        }


    }




    /// <summary>
    /// The summary of what happened when the 
    /// <see cref="HasExecutableActionRunner"/> tried to execute a specific
    /// <see cref="XAct.IHasExecutableAction"/>
    /// </summary>
    public class ExecutableRunnerActionResult
    {

        /// <summary>
        /// Gets or sets the optional Indent level of the result
        /// (used by <see cref="ToString"/>)
        /// </summary>
        /// <value>
        /// The indent level.
        /// </value>
        public int IndentLevel { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Type"/> of the step.
        /// <para>
        /// Will be a Interface type if the Service Locator is up and running -- otherwise 
        /// an argument-less constructor Class Type that <see cref="System.Activator"/>
        /// can create.
        /// </para>
        /// </summary>
        /// <value>
        /// The type of the step.
        /// </value>
        public Type StepType { get; set; }
        /// <summary>
        /// Reference to Step the <see cref="ExecutableRunnerActionResult"/>
        /// tried to run.
        /// </summary>
        public IHasExecutableAction Step { get; set; }
        /// <summary>
        /// Whether or not executing the step caused an Exception.
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// The exception raised by the Step if it failed.
        /// </summary>
        public Exception Exception { get; set; }
        /// <summary>
        /// The total duration of the Step and calling the 
        /// optional Callback.
        /// </summary>
        public TimeSpan Duration { get; set; }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {

            return "{0}[Success:{1}] [Duration:{2}ms] [StepType:{3}]"
            .FormatStringInvariantCulture(
            new String(' ', Math.Min(10, Math.Max(0, IndentLevel * 2))),
            Success,
            (long)Duration.TotalMilliseconds,
            StepType);
        }

    }

}