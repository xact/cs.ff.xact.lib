﻿
namespace XAct.Commands
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Namespace for the contracts and base classes for a coherent set of commands. 
    /// </summary>
    /// <remarks>
    /// <para>
    /// The command pattern has to be one of the most poorly understood useful
    /// patterns in software.
    /// </para>
    /// <para>
    /// Because of that, each dev shop seems to have their own implementation,
    /// some accepting arguments (wtf?!) others trying to figure out how to mix
    /// Commands with DI patterns, etc.
    /// </para>
    /// <para>
    /// In an attempt to stop 'command-rot' here, I've defined the original ICommand
    /// contract, fully loaded with the most options (prechecking, etc.) and then
    /// recommended that it be avoided, in favour of the DI compliant ICommandMessage
    /// pattern.
    /// </para>
    /// </remarks>
    /// <internal>
    /// This class is required by Sandcastle to document Namespaces: http://bit.ly/vqzRiK
    /// </internal>
    [CompilerGenerated]
    class NamespaceDoc { }
}
