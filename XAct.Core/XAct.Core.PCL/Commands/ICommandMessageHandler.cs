﻿
namespace XAct.Commands
{
    using System;
    using XAct.Messages;


    /// <summary>
    /// The contract for a handler that will execute an operation,
    /// using data within a given <see cref="ICommandMessage" />
    /// </summary>
    /// <typeparam name="TCommandMessage">The type of the command message.</typeparam>
    /// <remarks>
    ///   <para>
    /// Note that the <c>CommandMessageHandler</c> pattern is an improvement
    /// on the original <c>Command</c> pattern, separating the
    /// Execution from the Message -- allowing for DI to occur.
    ///   </para>
    ///   <para>
    /// For background, <see href="" />
    ///   </para>
    /// </remarks>
    public interface ICommandMessageHandler<in TCommandMessage> :
        //ICommandMessageHandler,    
        IHasSingletonBindingScope,
        IHasExecutableAction<TCommandMessage>,
        IHasUndoableReadOnly
        where TCommandMessage : ICommandMessage
    {


        /// <summary>
        /// Occurs when the conditions within which the 
        /// Command can be executed or not, change.
        /// </summary>
        event EventHandler CanExecuteChanged;

        /// <summary>
        ///   Defines the method that determines whether the 
        ///   command can execute in its current state
        /// <para>
        /// Internally invokes
        /// <see cref="ValidateForExecution"/> and returns 
        /// just the <see cref="IResponse.Success"/> flag.
        /// </para>
        /// </summary>
        bool CanExecute(TCommandMessage commandMessage);


        /// <summary>
        /// Determines whether this command can be rolled back if <c>Execute</c> is invoked.
        /// <para>
        /// Internally invokes
        /// <see cref="ValidateForUnexecution"/> and returns 
        /// just the <see cref="IResponse.Success"/> flag.
        /// </para>
        /// </summary>
        bool CanUnexecute(TCommandMessage commandMessage);


        /// <summary>
        /// Validate the 
        /// <see cref="ICommandMessage"/>
        /// properties and required services, 
        /// before Execution.
        /// </summary>
        /// <returns></returns>
        IResponse ValidateForExecution(TCommandMessage commandMessage);



        /// <summary>
        /// Validate the 
        /// <see cref="ICommandMessage"/>
        /// properties and required services, 
        /// before Unexecution.
        /// </summary>
        IResponse ValidateForUnexecution(TCommandMessage commandMessage);


        /// <summary>
        /// Rolls back the action performed by the 
        /// <c>Execute</c> method.
        /// </summary>
        /// <param name="commandMessage">The command message.</param>
        /// <returns></returns>
        void Unexecute(TCommandMessage commandMessage);

    }
}
