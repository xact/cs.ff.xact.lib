﻿using System;

namespace XAct.Commands.Models
{
    using System.Runtime.Serialization;
    using XAct.Environment;

    /// <summary>
    /// An datastore Model to store the serialized
    /// <see cref="ICommandMessage"/>
    /// </summary>
    [DataContract]
    public class CommandRecord : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasApplicationTennantId, IHasUserIdentifier, IHasEnabled, IHasSerializedTypeValueAndMethod, IHasTag, IHasDateTimeCreatedOnUtc, XAct.IHasDateTimeModifiedOnUtc
    {
        private readonly IDateTimeService _dateTimeService;
        private bool _enabled;
        private DateTime? _createdOnUtc;
        private DateTime? _lastModifiedOnUtc;

        [DataMember]
        public virtual Guid Id { get; set; }

        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        [DataMember]
        public virtual bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// <para>
        /// Member defined in the <see cref="IHasUserIdentifier" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        [DataMember]
        public virtual string UserIdentifier { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the 
        /// <see cref="ICommandMessageHandler{ICOmmandMessage}"/> 
        /// of the <see cref="ICommandMessage"/> is actually a 
        /// <see cref="IUndoableCommandMessageHandler{ICOmmandMessage}"/>
        /// </summary>
        /// <value>
        ///   <c>true</c> if [undoable]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Undoable { get; set; }

        /// <summary>
        /// 0=Execute
        /// 1=Unexecute
        /// </summary>
        [DataMember]
        public virtual bool Operation { get; set; }

        /// <summary>
        /// The method by which the <see cref="ICommandMessage"/> was serialized.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual SerializationMethod SerializationMethod { get; set; }

        /// <summary>
        /// Gets or sets the Assembly qualified name of the <see cref="ICommandMessage"/>.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string SerializedValueType { get; set; }

        /// <summary>
        /// Gets or sets the serialization of the <see cref="ICommandMessage"/>.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string SerializedValue { get; set; }

        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        [DataMember]
        public virtual string Tag { get; set; }


        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        [DataMember]
        public virtual DateTime? CreatedOnUtc
        {
            get { return _createdOnUtc; }
            set { _createdOnUtc = value; }
        }

        [DataMember]
        public virtual DateTime? LastModifiedOnUtc
        {
            get { return _lastModifiedOnUtc; }
            set { _lastModifiedOnUtc = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRecord" /> class.
        /// </summary>
        public CommandRecord()
        {
            this.GenerateDistributedId();

            _enabled = true;

            if (_dateTimeService == null)
            {
                _dateTimeService = XAct.DependencyResolver.Current.GetInstance<IDateTimeService>();
            }

            _createdOnUtc = _dateTimeService.NowUTC;

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRecord"/> class.
        /// </summary>
        /// <param name="dateTimeService">The date time service.</param>
        public CommandRecord(IDateTimeService dateTimeService)
        {
            _dateTimeService = dateTimeService;
        }
    }
}
