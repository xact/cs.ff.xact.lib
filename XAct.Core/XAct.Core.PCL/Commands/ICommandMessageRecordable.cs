﻿namespace XAct.Commands
{
    /// <summary>
    /// A contract to mark a <see cref="ICommandMessage"/>
    /// as recorded when executied.
    /// </summary>
    public interface ICommandMessageRecordable
    {
    }
}