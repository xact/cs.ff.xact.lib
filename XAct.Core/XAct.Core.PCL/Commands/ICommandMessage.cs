﻿namespace XAct.Commands
{
    /// <summary>
    /// A contract for an object containing 
    /// arguments that will be processed by an <see cref="ICommandService"/>
    /// that knows nothing about it, but knows how to find
    /// an <see cref="ICommandMessageHandler{TCommandMessage}"/>
    /// that knows how to Execute it.
    /// <para>
    /// See <see cref="ICommandMessageResult{T}"/>
    /// if the invoker is expecting to retrieve a response.
    /// </para>
    /// <para>
    /// See <see cref="ICommandMessageState{T}"/>
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// The original <c>ICommand</c> pattern was designed
    /// prior to the general availability of <c>DependencyInjectionContainer</c>'s such as Unity and Ninject,
    /// as conflicts with SOLID principles (specifically Dependency Injection).
    /// </para>
    /// <para>
    /// The CommandMessage/CommandMessageHandler/CommandDispatcher patterns
    /// elegantly bring the Command pattern up to date, separating arguments (ComandMessage)
    /// from Execution (CommandMessageHandler) that probably needs its constructor
    /// for injection of needed services.
    /// </para>
    /// <para>
    /// It's important to note that the end developer does not concern itself
    /// with finding the right handler -- just the creation of the CommandMessage,
    /// and then passes it to the CommandMessageDispatcher to work out what is the required
    /// Handler that needs loading, and execution.
    /// </para>
    /// <para>
    /// Defines no properties or methods -- it's just a contract that <see cref="ICommandService"/>
    /// uses to find the appropriate <see cref="ICommandMessageHandler{ICommandMessage}"/>
    /// </para>
    /// </remarks>
    public interface ICommandMessage : IHasResourceFilterReadOnly, IHasTitle
    { 

        
        /// <summary>
        /// The Text to display in an Undo panel on the UI.
        /// <para>
        /// Example: "Undo of Delete Selected Student(s)?"
        /// </para>
        /// <para>
        /// If <see cref="IHasResourceFilterReadOnly.ResourceFilter"/> is set, 
        /// the Property Text is that of a <c>ResourceKey</c>.
        /// </para>
        /// </summary>
        string UndoTitle { get; set; }

    }
}
