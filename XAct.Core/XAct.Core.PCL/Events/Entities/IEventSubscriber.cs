namespace XAct.Events
{
    /// <summary>
    /// 
    /// <para>
    /// NOTE: Internal use only.
    /// </para>
    /// </summary>
    public interface IEventSubscriber
    {
    }



    /// <summary>
    /// Contract for a message subscriber, defining the type of message 
    /// they are interested in.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEventSubscriber<T> : IEventSubscriber
        where T : IEvent
    {
        /// <summary>
        /// Handles the <see cref="IEvent"/> sent by the <see cref="IEventAggregatorService"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        void Handle(T message);
    }
}