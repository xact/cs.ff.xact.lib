﻿// ReSharper disable CheckNamespace

namespace XAct
{
    using System;
    using System.Collections.Generic;

// ReSharper restore CheckNamespace

    /// <summary>
    /// Generic Cancellable EventArgs package.
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public abstract class CancelEventArgsBase<TItem> : EventArgs
    {
        private bool _cancel;

        /// <summary>
        /// The generic item this argument package is about.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Enherit from this class and do something like the following:
        /// <code>
        /// <![CDATA[
        /// public Vertex Vertex {
        ///		get {
        ///		return _Item;
        ///		}
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </remarks>
        protected TItem _item;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CancelEventArgsBase&lt;TItem&gt;"/> class.
        /// </summary>
        /// <param name="item">The item.</param>
        protected CancelEventArgsBase(TItem item) : this(item, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CancelEventArgsBase&lt;TItem&gt;"/> class.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="allowDefaultValue">if set to <c>true</c> [allow default value].</param>
        protected CancelEventArgsBase(TItem item, bool allowDefaultValue)
        {
            if ((!allowDefaultValue) && (Comparer<TItem>.Default.Compare(item, default(TItem)) == 0))
            {
                throw new ArgumentNullException("item");
            }
            _item = item;
        }

        #endregion

        /// <summary>
        /// Gets a value indicating if this event should be cancelled.
        /// </summary>
        public bool Cancel
        {
            get { return _cancel; }
            set
            {
                if (_cancel)
                {
                    //Already cancelled.
                    return;
                }
                _cancel = value;
            }
        }
    }
}