﻿namespace XAct.Events
{
    using System;

    /// <summary>
    /// A generic EventArgs package containing an Exception and an Item.
    /// </summary>
    /// <internal>
    /// Used by the GenericThreadDispatcher.
    /// </internal>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class GenericExceptionEventArgs<TItem> : ExceptionEventArgs<TItem>
        {
            /// <summary>
            /// Gets the item.
            /// </summary>
            public TItem Item { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="GenericExceptionEventArgs&lt;TItem&gt;"/> class.
            /// </summary>
            /// <param name="exception">The exception.</param>
            /// <param name="item">The item.</param>
            public GenericExceptionEventArgs(Exception exception,TItem item):base(exception)
            {

                item.ValidateIsNotDefault("item");
                Item = item;
            }
        }

}
