﻿namespace XAct.Events
{
    using System;

    /// <summary>
    /// A EventArgs package containing an exception
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class ExceptionEventArgs<TItem> :EventArgs 
    {
        /// <summary>
        /// Gets the exception.
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="ExceptionEventArgs&lt;TItem&gt;"/> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public ExceptionEventArgs(Exception exception)
        {
            exception.ValidateIsNotDefault("exception");

            Exception = exception;
        }
    }
}