﻿
namespace XAct.Events
{
    /// <summary>
    /// A generic implementation of
    /// <see cref="CancelEventArgsBase{TItem}"/>
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class GenericCancelEventArgs<TItem>:CancelEventArgsBase<TItem>
    {
        /// <summary>
        /// Gets the payload item.
        /// </summary>
        public TItem Item { get { return _item; } }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="GenericCancelEventArgs&lt;TItem&gt;"/> class.
        /// </summary>
        /// <param name="item">The item.</param>
        public GenericCancelEventArgs(TItem item)
            : base(item)
        {

        }
    }
}
