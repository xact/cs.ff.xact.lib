﻿namespace XAct.Events
{
    /// <summary>
    /// The contract for a message sent from publishers 
    /// to <see cref="IEventSubscriber"/>s,
    /// using <see cref="IEventAggregatorService"/>.
    /// </summary>
    public interface IEvent
    {
    }
}