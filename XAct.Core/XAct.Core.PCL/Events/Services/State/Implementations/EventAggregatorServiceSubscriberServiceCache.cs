namespace XAct.Events.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IEventAggregatorServiceSubscriberServiceState"/>
    /// to provide a per thread EventAggregatorSubscriberCache
    /// to the <see cref="IEventAggregatorService"/> implementation.
    /// <para>
    /// In a web environment, have to remap entities.
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IEventAggregatorServiceSubscriberServiceState),BindingLifetimeType.SingletonPerWebRequestScope,Priority.Low)]
    public class EventAggregatorServiceSubscriberServiceState  : List<IEventSubscriber>, IEventAggregatorServiceSubscriberServiceState
    {

    }
}