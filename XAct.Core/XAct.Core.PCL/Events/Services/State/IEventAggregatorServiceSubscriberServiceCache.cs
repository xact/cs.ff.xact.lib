namespace XAct.Events
{
    using System.Collections.Generic;

    /// <summary>
    /// An contract for a cache mechanism to provide thread based caching
    /// of the subscribers that the 
    /// <see cref="IEventAggregatorService"/>
    /// implementation manages. 
    /// <para>
    /// In a web environment, have to remap entities.
    /// </para>
    /// </summary>
    public interface IEventAggregatorServiceSubscriberServiceState : IList<IEventSubscriber>, IHasXActLibServiceState
    {

    }
}