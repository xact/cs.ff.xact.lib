﻿namespace XAct.Events
{
    /// <summary>
    /// A contract for a service to 
    /// provide a means to pass messages among loosely coupled elements.
    /// </summary>
    /// <internal>
    /// See: http://www.codeproject.com/Articles/52810/Event-Aggregator-with-Specialized-Listeners
    /// </internal>
    public interface IEventAggregatorService : IHasXActLibService
    {

        /// <summary>
        /// Publishes the specified message
        /// to registered event
        /// <see cref="IEventSubscriber{T}"/>s.
        /// </summary>
        /// <typeparam name="T">The <see cref="IEvent"/></typeparam>
        /// <param name="message">The message.</param>
        void Publish<T>(T message) where T : IEvent;


        /// <summary>
        /// Adds the given subscriber.
        /// </summary>
        /// <param name="subscriber">The subscriber.</param>
        void AddSubscriber(IEventSubscriber subscriber);


        /// <summary>
        /// Removes the given subscriber.
        /// </summary>
        /// <param name="subscriber">The subscriber.</param>
        void RemoveSubscriber(IEventSubscriber subscriber);
    }


}