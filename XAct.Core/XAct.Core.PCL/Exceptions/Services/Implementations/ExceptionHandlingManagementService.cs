﻿namespace XAct.Exceptions.Implementations
{
    using System;
    using XAct.Exceptions;
    using XAct.Exceptions.Configuration;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IExceptionHandlingManagementService"/>
    /// used to register 
    /// </summary>
    public class ExceptionHandlingManagementService : IExceptionHandlingManagementService
    {

        private readonly IExceptionHandlingServiceConfigurations _exceptionHandlingDefinitions ;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlingManagementService"/> class.
        /// </summary>
        public ExceptionHandlingManagementService(IExceptionHandlingServiceConfigurations exceptionHandlingDefinitions)
        {
            exceptionHandlingDefinitions.ValidateIsNotDefault("exceptionHandlingDefinitions");

            _exceptionHandlingDefinitions = exceptionHandlingDefinitions;

        }

        /// <summary>
        /// Registers the specified exception handling description.
        /// </summary>
        /// <param name="exceptionHandlingDescription">The exception handling description.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public void Register(IExceptionHandlingConfiguration exceptionHandlingDescription, string category = "default")
        {
            _exceptionHandlingDefinitions.Add(exceptionHandlingDescription, category);
        }


        /// <summary>
        /// Retrieves the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public IExceptionHandlingConfiguration Retrieve(Exception exception, string category = "default")
        {
            return _exceptionHandlingDefinitions.Get(exception, category);
        }
    }
}