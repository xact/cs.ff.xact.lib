﻿namespace XAct.Exceptions.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Exceptions;
    using XAct.Services;


    /// <summary>
    /// An implementation of the <see cref="IExceptionHandlingService"/>
    /// to handle unexpected exceptions, log them,
    /// and throw up to higher layers a cleansed replacement message.
    /// </summary>
    public class ExceptionHandlingService : IExceptionHandlingService
    {
        private readonly ITracingService _tracingService;
        private readonly IExceptionHandlingManagementService _exceptionManagementService;


        /// <summary>
        /// Gets or sets the default exception handling configuration.
        /// <para>
        /// Default is Null.
        /// </para>
        /// </summary>
        /// <value>
        /// The default exception handling configuration.
        /// </value>
        public IExceptionHandlingConfiguration Configuration { get; set; }

        /// <summary>
        /// Gets the default type of Exception to create
        /// when the <see cref="ExceptionHandlingBehaviour"/>
        /// is set to <see cref="ExceptionHandlingBehaviour.ReplaceException"/>
        /// </summary>
        /// <value>
        /// The default exception type to raise.
        /// </value>
        public Type DefaultExceptionType
        {
            get { return _defaultExceptionType; }
            set { _defaultExceptionType = value; }
        }

        private Type _defaultExceptionType = typeof (Exception);

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ExceptionHandlingService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="exceptionManagementService">The exception management service that was used to register Exception handling information.</param>
        /// <param name="exceptionHandlingServiceConfiguration">The exception handling service configuration.</param>
        public ExceptionHandlingService(ITracingService tracingService,
                                        IExceptionHandlingManagementService exceptionManagementService,
            IExceptionHandlingServiceConfiguration exceptionHandlingServiceConfiguration)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            exceptionManagementService.ValidateIsNotDefault("exceptionManagementService");

            _tracingService = tracingService;
            _exceptionManagementService = exceptionManagementService;


            _defaultExceptionType = exceptionHandlingServiceConfiguration.DefaultExceptionType;
            Configuration =
                exceptionHandlingServiceConfiguration.DefaultExceptionHandlingConfiguration;
        }


        /// <summary>
        /// Handle the exception, and based on any rules
        /// defined in the ExceptionService, 
        /// log the given Exception,
        /// and raise a new one (cleansed) exception
        /// suitable to passed up to higher layers.
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="category"></param>
        public bool HandleException(ref Exception exception, string category = null)
        {

            //See if we have any Exception handling schema/configuration registered.
            IExceptionHandlingConfiguration exceptionHandlingDescription =
                _exceptionManagementService.Retrieve(exception, category);

            // -----------------------
            if (exceptionHandlingDescription == null)
            {
                exceptionHandlingDescription = Configuration;
            }
            // -----------------------
            //If not, then there's nothing this service can do
            //and pass it up to the top to do something with it.
            if (exceptionHandlingDescription == null)
            {
                return true;
            }
            // -----------------------
            //We've been told to ignore this error. No logging, no nothing.
            if (
                ((int) exceptionHandlingDescription.ExceptionHandlingBehaviour).BitIsSet(
                    (int) ExceptionHandlingBehaviour.Ignore))
            {
                return false;
            }
            // -----------------------
            // Should we log?
            // Log, LogAndLeaveExceptionIntact, LogAndReplaceException
            if (
                ((int) exceptionHandlingDescription.ExceptionHandlingBehaviour).BitIsSet(
                    (int) ExceptionHandlingBehaviour.Log))
            {
                _tracingService.TraceException(TraceLevel.Error, exception, "Exception handled by ExceptioService");

                if (exceptionHandlingDescription.ExceptionHandlingBehaviour == ExceptionHandlingBehaviour.Log)
                {
                    //If it's *JUST* Log, might as well get out now.
                    return false;
                }
            }
            // -----------------------
            if (
                ((int) exceptionHandlingDescription.ExceptionHandlingBehaviour).BitIsSet(
                    (int) ExceptionHandlingBehaviour.ReplaceException))
            {

                if (exceptionHandlingDescription.ReplacementExceptionType != null)
                {
                    exception =
                        (Exception) Activator.CreateInstance(exceptionHandlingDescription.ReplacementExceptionType);

                }
                else
                {
                    exception = (Exception) Activator.CreateInstance(this.DefaultExceptionType);
                }

                //breaks trace:
                return true;
            }

            // -----------------------

            // LeaveExceptionIntact, LogAndLeaveExceptionIntact
            //if (
            //    ((int) exceptionHandlingDescription.ExceptionHandlingBehaviour).BitIsSet(
            //        (int) ExceptionHandlingBehaviour.LeaveExceptionIntact))
            //{
            //    //Return true so that the stack is undamaged,
            //    //and the outer one is handling it.
            //    return true;
            //}
            // -----------------------
            // default:
            return true;
        }
    }
}
