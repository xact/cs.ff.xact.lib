﻿namespace XAct.Exceptions
{
    using System;

    /// <summary>
    /// The contract for a service to register ExceptionHandling descriptors.
    /// </summary>
    public interface IExceptionHandlingManagementService : IHasXActLibService
    {
        /// <summary>
        /// Registers the specified exception handling description.
        /// </summary>
        /// <param name="exceptionHandlingDescription">The exception handling description.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        void Register(IExceptionHandlingConfiguration exceptionHandlingDescription, string category = "default");

        /// <summary>
        /// Retrieves the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="category">The category.</param>
        IExceptionHandlingConfiguration Retrieve(Exception exception, string category = "default");
    }
}