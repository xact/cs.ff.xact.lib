﻿namespace XAct.Exceptions
{
    using System;

    /// <summary>
    /// See XAct.EnterpriseLibrary 
    /// and see if you can think of a way of generalizing it...
    /// </summary>
    public interface IExceptionHandlingService : IHasXActLibService
    {

        /// <summary>
        /// Gets or sets the default exception handling configuration.
        /// <para>
        /// Default is Null.
        /// </para>
        /// </summary>
        /// <value>
        /// The default exception handling configuration.
        /// </value>
        IExceptionHandlingConfiguration Configuration { get; set; }

        /// <summary>
        /// Gets the default type of Exception to create
        /// when the <see cref="ExceptionHandlingBehaviour"/>
        /// is set to <see cref="ExceptionHandlingBehaviour.ReplaceException"/>
        /// </summary>
        /// <value>
        /// The default exception type to raise.
        /// </value>
        Type DefaultExceptionType { get; set; }


        /// <summary>
        /// Handle the exception, and based on any rules
        /// defined in the ExceptionService, 
        /// log the given Exception,
        /// and raise a new one (cleansed) exception
        /// suitable to passed up to higher layers.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// try {
        ///   var a=1;var b=0;
        ///   var r = a/b;
        /// }
        /// catch (ref System.Exception e){
        ///   //Only have to throw exception if result is true:
        ///   if (exceptionService.Handle(ref e)){throw;}
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="category"></param>
        /// <param name="exception"></param>
        bool HandleException(ref Exception exception,string category =null);



    }
}