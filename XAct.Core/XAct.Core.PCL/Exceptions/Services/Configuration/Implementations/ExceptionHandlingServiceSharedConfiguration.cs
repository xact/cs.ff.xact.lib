﻿namespace XAct.Exceptions.Implementations
{
    using System;
    using XAct.Exceptions;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class ExceptionHandlingServiceConfiguration : IExceptionHandlingServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the default exception handling configuration.
        /// <para>
        /// Default is Null.
        /// </para>
        /// </summary>
        /// <value>
        /// The default exception handling configuration.
        /// </value>
        public IExceptionHandlingConfiguration DefaultExceptionHandlingConfiguration { get; set; }

        /// <summary>
        /// Gets the default type of Exception to create
        /// when the <see cref="ExceptionHandlingBehaviour"/>
        /// is set to <see cref="ExceptionHandlingBehaviour.ReplaceException"/>
        /// </summary>
        /// <value>
        /// The default exception type to raise.
        /// </value>
        public Type DefaultExceptionType
        {
            get { return _defaultExceptionType; }
            set { _defaultExceptionType = value; }
        }

        private Type _defaultExceptionType = typeof(Exception);


        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlingServiceConfiguration" /> class.
        /// </summary>
        public ExceptionHandlingServiceConfiguration()
        {

            DefaultExceptionHandlingConfiguration = null;
            _defaultExceptionType = typeof (Exception);
        }

    }
}