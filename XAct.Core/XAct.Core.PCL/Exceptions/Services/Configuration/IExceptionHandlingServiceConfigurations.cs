﻿namespace XAct.Exceptions.Configuration
{
    using System;

    /// <summary>
    /// Contract for a configuration Object used by an
    /// implementation of the <see cref="IExceptionHandlingManagementService"/>
    /// </summary>
    public interface IExceptionHandlingServiceConfigurations : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Adds the specified exception handling description to the inner cache.
        /// </summary>
        /// <param name="exceptionHandlingDescription">The exception handling description.</param>
        /// <param name="category">The category.</param>
        void Add(IExceptionHandlingConfiguration exceptionHandlingDescription, string category="default");


        /// <summary>
        /// Gets from the inner cache an <see cref="ExceptionHandlingConfiguration"/>
        /// that matches the given <paramref name="exception"/>
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        IExceptionHandlingConfiguration Get(Exception exception, string category = "default");
    }
}