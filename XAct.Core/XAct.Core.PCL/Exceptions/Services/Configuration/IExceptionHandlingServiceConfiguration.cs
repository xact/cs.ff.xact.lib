﻿using XAct.Exceptions;

namespace XAct.Exceptions
{
    using System;

    /// <summary>
    /// c
    /// </summary>
    public interface IExceptionHandlingServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the default exception handling configuration.
        /// <para>
        /// Default is Null.
        /// </para>
        /// </summary>
        /// <value>
        /// The default exception handling configuration.
        /// </value>
        IExceptionHandlingConfiguration DefaultExceptionHandlingConfiguration { get; set; }

        /// <summary>
        /// Gets the default type of Exception to create
        /// when the <see cref="ExceptionHandlingBehaviour"/>
        /// is set to <see cref="ExceptionHandlingBehaviour.ReplaceException"/>
        /// </summary>
        /// <value>
        /// The default exception type to raise.
        /// </value>
        Type DefaultExceptionType { get; set; }
    }
}
