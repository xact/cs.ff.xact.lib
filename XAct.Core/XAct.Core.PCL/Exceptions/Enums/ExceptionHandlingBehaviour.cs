﻿namespace XAct.Exceptions
{
    using System;

    /// <summary>
    /// The behaviour to use when an exception is handled.
    /// </summary>
    [Flags]
    public enum ExceptionHandlingBehaviour
    {
        /// <summary>
        /// Unstated - an error condition.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Does not log.
        /// <para>
        /// Returns False for invoker to ignore exception (it's been logged)
        /// </para>
        /// </summary>
        Ignore = 1,

        /// <summary>
        /// Log the exception before deciding to throw or rethrow.
        /// Note: Usually not chosen (prefer using any of the others.
        /// <para>
        /// Returns False for invoker to ignore exception (it's been logged)
        /// </para>
        /// </summary>
        Log=2,

        /// <summary>
        /// Throw the original Exception, keeping Stack intact.
        /// <para>
        /// Returns True for invoker to handle returned original Exception.
        /// </para>
        /// <para>
        /// No Logging occurs unless combines with <see cref="Log"/>
        /// (by choosing <see cref="LogAndLeaveExceptionIntact"/>)
        /// </para>
        /// </summary>
        LeaveExceptionIntact=4,

        /// <summary>
        /// Rethrowing a replacement Exception.
        /// <para>
        /// Returns True for invoker to handle returned original Exception.
        /// </para>
        /// <para>
        /// No Logging occurs unless combines with <see cref="Log"/>
        /// (by choosing <see cref="LogAndReplaceException"/>)
        /// </para>
        /// </summary>
        ReplaceException=8,

        /// <summary>
        /// Exceptionhandler logs error, and returns true for invoker to throw (not rethrow the exception).
        /// <para>
        /// Returns True for invoker to handle returned original Exception.
        /// </para>
        /// </summary>
        LogAndLeaveExceptionIntact = Log | LeaveExceptionIntact,

        /// <summary>
        /// Exceptionhandler logs error,  replaces exception (breaking the stack trace)
        /// <para>
        /// Returns True for invoker to handle returned original Exception.
        /// </para>
        /// </summary>
        LogAndReplaceException = Log | ReplaceException
    }
}