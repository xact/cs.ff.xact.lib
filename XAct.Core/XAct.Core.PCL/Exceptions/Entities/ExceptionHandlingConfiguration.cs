﻿namespace XAct.Exceptions
{
    using System;
    using XAct.Diagnostics;

    /// <summary>
    /// A description of what to do with an Exception handled
    /// by an implementation of the <see cref="IExceptionHandlingService"/>
    /// </summary>
    /// <typeparam name="TException">The type of the exception.</typeparam>
    public class ExceptionHandlingConfiguration<TException> : ExceptionHandlingConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlingConfiguration&lt;TException&gt;"/> class.
        /// </summary>
        /// <param name="exceptionHandlingBehaviour">The exception handling behaviour.</param>
        public ExceptionHandlingConfiguration(ExceptionHandlingBehaviour exceptionHandlingBehaviour): base(typeof(TException),exceptionHandlingBehaviour){}
    }

    /// <summary>
    /// A description of what to do with an Exception handled
    /// by an implementation of the <see cref="IExceptionHandlingService"/>
    /// </summary>
    /// <typeparam name="TException">The type of the exception.</typeparam>
    /// <typeparam name="TReplacementException">The type of the replacement exception.</typeparam>
    public class ExceptionHandlingConfiguration<TException,TReplacementException> : ExceptionHandlingConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlingConfiguration&lt;TException, TReplacementException&gt;"/> class.
        /// </summary>
        /// <param name="exceptionHandlingBehaviour">The exception handling behaviour.</param>
        public ExceptionHandlingConfiguration(ExceptionHandlingBehaviour exceptionHandlingBehaviour): base(typeof(TException),exceptionHandlingBehaviour, typeof(TReplacementException)){}
    }

    /// <summary>
    /// A description of what to do with an Exception handled
    /// by an implementation of the <see cref="IExceptionHandlingService"/>
    /// </summary>
    public class ExceptionHandlingConfiguration : IExceptionHandlingConfiguration
    {
        /// <summary>
        /// The type of exception to handle.
        /// </summary>
        public Type ExceptionType {get;private set;}
        
        /// <summary>
        /// What to do with the exception
        /// </summary>
        public ExceptionHandlingBehaviour ExceptionHandlingBehaviour { get; private set; }


        /// <summary>
        /// Gets or sets the trace level to use when logging the Exception.
        /// <para>
        /// Default is <c>Exception</c>.
        /// </para>
        /// </summary>
        /// <value>
        /// The trace level.
        /// </value>
        public TraceLevel TraceLevel { get; set; }

        /// <summary>
        /// If Behaviour includes <see cref="ExceptionHandlingBehaviour">Throw</see>
        /// an exception will be thrown: it will be the original one, 
        /// unless a replacement (a cleansed one) is defined here.
        /// </summary>
        public Type ReplacementExceptionType { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlingConfiguration"/> class.
        /// </summary>
        /// <param name="exceptionToHandle">The exception to trap.</param>
        /// <param name="exceptionHandlingBehaviour">The exception handling behaviour.</param>
        /// <param name="replacementException">The replacement exception.</param>
        public ExceptionHandlingConfiguration(Type exceptionToHandle, ExceptionHandlingBehaviour exceptionHandlingBehaviour, Type replacementException =null)
        {
            exceptionToHandle.ValidateIsNotDefault("exceptionToHandle");

            if (exceptionHandlingBehaviour== ExceptionHandlingBehaviour.Unknown)
            {
                throw new ArgumentException("Exception handling behaviour not defined.","exceptionHandlingBehaviour");
            }
            TraceLevel = TraceLevel.Error;
            ExceptionType = exceptionToHandle;
            ExceptionHandlingBehaviour = exceptionHandlingBehaviour;
            ReplacementExceptionType = replacementException;
        }
    }
}
