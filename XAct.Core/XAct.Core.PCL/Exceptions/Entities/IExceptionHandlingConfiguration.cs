﻿namespace XAct.Exceptions
{
    using System;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public interface IExceptionHandlingConfiguration
    {
        /// <summary>
        /// The type of exception to handle.
        /// </summary>
        Type ExceptionType { get; }

        /// <summary>
        /// What to do with the exception
        /// </summary>
        ExceptionHandlingBehaviour ExceptionHandlingBehaviour { get; }

        /// <summary>
        /// Gets or sets the trace level to use when logging the Exception.
        /// <para>
        /// Default is <c>Exception</c>.
        /// </para>
        /// </summary>
        /// <value>
        /// The trace level.
        /// </value>
        TraceLevel TraceLevel { get; set; }

        /// <summary>
        /// If Behaviour includes <see cref="ExceptionHandlingBehaviour">Throw</see>
        /// an exception will be thrown: it will be the original one, 
        /// unless a replacement (a cleansed one) is defined here.
        /// </summary>
        Type ReplacementExceptionType { get; }
    }
}