﻿namespace XAct.Resources
{
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// Contract for a service that returns localized resources.
    /// </summary>
    public interface IResourceService : IHasXActLibService
    {
        /// <summary>
        /// Gets the localized string resource, localised to
        /// the CurrentUICulture, unless specified otherwise.new
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        string GetString(string name, CultureInfo cultureInfo=null);


        /// <summary>
        /// Gets the localized string resource, localised to
        /// the CurrentUICulture, unless specified otherwise.new
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="name">The name.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        string GetString(string filter, string name, CultureInfo cultureInfo = null);

        /// <summary>
        /// Gets all string resources for the given filter/page.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        Dictionary<string, string> GetAllStrings(string filter, CultureInfo cultureInfo = null);



        /// <summary>
        /// Gets all string resources for the given filters/pages.
        /// </summary>
        /// <param name="filters">The filters.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        Dictionary<string, Dictionary<string, string>> GetAllStrings(CultureInfo cultureInfo, params string[] filters);

    }
}
