﻿

// ReSharper disable CheckNamespace

namespace XAct.Resources
// ReSharper restore CheckNamespace
{
    using System;
    using System.Globalization;
    using System.Runtime.Serialization;
    using XAct.Environment;

    /// <summary>
    /// An Entity to init the Db with.
    /// <para>
    /// Note: 
    /// at present, only used by the Db initializer in
    /// XAct.Resources.Persistence.EF, but if CodeFirst becomes more
    /// standard in Enterprise development, then that might change.
    /// </para>
    /// </summary>
    /// <internal>
    /// Virtualized ready for Proxy Generation using CodeFirst.
    /// <para>
    /// Entity will never implement <see cref="IHasDistributedIdentities"/>
    /// </para>
    /// </internal>
    public class Resource : IHasXActLibEntity, IResource
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Resource"/> class.
        /// </summary>
        public Resource()
        {
            this.GenerateDistributedId();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Resource" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="cultureInfo">The culture information.</param>
        public Resource(Guid id, string filter, string key, string value, CultureInfo cultureInfo = null)
            : this(id, filter, key, value, ((cultureInfo == null) ? (string)null : cultureInfo.ToString()))
        {
            
        }




        /// <summary>
        /// Initializes a new instance of the <see cref="Resource" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="cultureCode">The culture code.</param>
    public Resource (Guid id, string filter, string key, string value, string cultureCode = null)
        {
            this.Id = id;

            if (filter == null)
            {
                filter = string.Empty;
            }
            if (cultureCode == null)
            {
                cultureCode = string.Empty;
            }


            Key = key;
            Value = value;
            Filter = filter;
            CultureCode = cultureCode;
        }

        /// <summary>
        /// Gets or sets the distributed identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
    public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
    public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
        public virtual string Filter { get { return _filter; } 
            set { if (value == null) {value = string.Empty;} _filter = value;  } } 
        private string _filter;

        /// <summary>
        /// 
        /// </summary>
        public virtual string Key { get { return _key; } set { _key = value; 
            //KeyLowered = (Key == null) ? null : Key.ToLower(); 
        } }
        private string _key;

        /// <summary>
        /// 
        /// </summary>
        public virtual string CultureCode
        {
            get { return _cultureCode; } 
            set {
                if (value == null) { value = string.Empty; }
                _cultureCode = value; 
                //CultureCodeLowered = (CultureCode == null) ? null : CultureCode.ToLower(); 
            }
        }
        private string _cultureCode;

        ///// <summary>
        ///// Internal. Prefer the use of the <see cref="Filter"/> property.
        ///// </summary>
        //public virtual string FilterLowered { get; protected set; }

        ///// <summary>
        ///// Internal. Prefer the use of the <see cref="Key"/> property. 
        ///// </summary>
        //public virtual string KeyLowered { get; protected set; }

        ///// <summary>
        ///// Internal.  Prefer the use of the <see cref="CultureCode"/> property.
        ///// </summary>
        //public virtual string CultureCodeLowered { get; protected set; }

        /// <summary>
        /// Gets or sets the serialized value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public virtual string Value { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
    }
}