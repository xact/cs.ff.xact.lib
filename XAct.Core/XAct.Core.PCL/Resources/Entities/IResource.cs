﻿namespace XAct.Resources
{
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    public interface IResource : IHasDistributedGuidIdAndTimestamp, IHasFilter, IHasDescription
    {


        /// <summary>
        /// 
        /// </summary>
        string Key { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string CultureCode { get; set; }

        ///// <summary>
        ///// Internal. Prefer the use of the <c>Filter</c> property.
        ///// </summary>
        //string FilterLowered { get; }

        ///// <summary>
        ///// Internal. Prefer the use of the <see cref="Key"/> property. 
        ///// </summary>
        //string KeyLowered { get; }

        ///// <summary>
        ///// Internal.  Prefer the use of the <see cref="CultureCode"/> property.
        ///// </summary>
        //string CultureCodeLowered { get; }

        /// <summary>
        /// Gets or sets the serialized value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        string Value { get; set; }

    }
}