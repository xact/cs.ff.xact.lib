﻿namespace XAct.Settings.Tests
{
}



namespace XAct.Settings.Tests
{
    using XAct;
    using System;
    using NUnit.Framework;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SettingTests
    {
        private const string EnvironmentIdentifier = "";
        private const string ZoneOrTierIdentifier = "Y";
        private const string HostIdentifier = "Z";
        private readonly Guid TierIdentifier = Guid.Empty;


        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void SetAndGetSettingValueAsTimeSpan()
        {
            Setting setting = new Setting("A", typeof(TimeSpan), TimeSpan.FromDays(3), null, null,null,
                new SettingEditingMetadata(
                    //new ApplicationSettingScope(EnvironmentIdentifier, ZoneOrTierIdentifier, HostIdentifier, TierIdentifier),
                TimeSpan.FromSeconds(123)));

            Assert.AreEqual(TimeSpan.FromDays(3),setting.Value);
        }

        [Test]
        public void SetAndGetSettingValueAsInt()
        {
            Setting setting = new Setting("A", typeof(int), 3);

            Assert.AreEqual(3, setting.Value);
        }

        [Test]
        public void SetAndGetSettingValueResetWorks()
        {
            Setting setting = new Setting("A", typeof(int), 3, null, null,
                null,
                new SettingEditingMetadata(
                    //new ApplicationSettingScope(EnvironmentIdentifier, ZoneOrTierIdentifier, HostIdentifier, TierIdentifier), 
                123));

            
            //Reset value to default:
            setting.ResetValue();

            Assert.AreEqual(123, setting.Value);
        }

    }
}
