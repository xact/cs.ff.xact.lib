﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XAct;
namespace Tests.Objects
{
    using NUnit.Framework;
    using XAct.Extensions;
    using XAct.Security;

    [TestFixture]
    public class NotAuthorizedExceptionTests
    {

        [Test]
        [Ignore("A PCL based Exception can't be serialized (FF can).")]
        public void IsDeSerializable()
        {
            NotAuthorizedException e = new NotAuthorizedException("Some Message");

            NotAuthorizedException e2 = e.TestWCFSerialization<NotAuthorizedException>();

            Assert.IsNotNull(e2);
        }
    }
}
