﻿
namespace Tests.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using NUnit.Framework;
    using XAct;
    using XAct.Environment;
    using XAct.Extensions;
    using XAct.Messages;
    using XAct.Messages.Services.Implementations;
    using XAct.Tests;

    [TestFixture]
    internal class ResponseTests
    {
        //Properties HAVE to be public for MessageCodeServiceConfiguration to be able to scan...

        [MessageCodeMetadata(null, 2)]
        public static MessageCode _error = new MessageCode(123, Severity.Error);

        [MessageCodeMetadata(null, 2)]
        public static MessageCode _blockingWarning = new MessageCode(123, Severity.BlockingWarning);

        [MessageCodeMetadata(null, 2)]
        public static MessageCode _nonBlockingWarning = new MessageCode(123, Severity.NonBlockingWarning);

        [MessageCodeMetadata(null,2)]
        public static MessageCode _info = new MessageCode(123, Severity.Info);


        [MessageCodeMetadata("Blah{0},{1},{2}",2)]
        public static MessageCode _info2 = new MessageCode(123, Severity.Info);

        [MessageCodeMetadata("Blah{0},{1},{2}",2)]
        public static MessageCode _nonBlocking2 = new MessageCode(123, Severity.NonBlockingWarning);




        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanAddMessages()
        {
            Response response = new Response();
            response.AddMessage(_blockingWarning,"argA","argB","argC");
            Assert.AreEqual(1, response.Messages.Count);
            Assert.AreEqual(Severity.BlockingWarning, response.Messages.First().MessageCode.Severity);
            Assert.AreEqual(3, response.Messages.First().Arguments.Length);
        }

        [Test]
        public void NoMessagesMeansSuccessIsTrue()
        {
            Response response = new Response();


            Assert.AreEqual(true, response.Success);

        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void AddingAnInfoMessageMakesSuccessTrue()
        {
            Response response = new Response();
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }


            Assert.AreEqual(true, response.Success);

        }
        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void AddingANonBlockingWarningMessageMakesSuccessTrue()
        {
            Response response = new Response();
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }

            Assert.AreEqual(true, response.Success);

        }


        [Test]
        public void AddingABlockingWarningMessageMakesSuccessFalse()
        {
            Response response = new Response();
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }
            response.AddMessage(_blockingWarning, "argA", "argB", "argC");


            Assert.AreEqual(false, response.Success);

        }



        [Test]
        public void AddingAnErrorMessageMakesSuccessFalse()
        {
            Response response = new Response();
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }
            response.AddMessage(_error, "argA", "argB", "argC");


            Assert.AreEqual(false, response.Success);

        }



        [Test]
        public void SerializingOfTrueSuccessWorks()
        {
            Response response = new Response();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 2; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }

            Assert.AreEqual(true, response.Success);

            string serializedText = response.GetType().ToDataContractString(response);

            Trace.WriteLine(serializedText);
            Response response2 = serializedText.DeserializeFromDataContractSerializedString<Response>();

            Assert.AreEqual(true, response2.Success);

        }



        [Test]
        public void SerializingOfTrueSuccessWorksWithMetadata()
        {
            Response response = new Response();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info2, "argA", "argB", "argC");
            }

            for (int i = 0; i < 2; i++)
            {
                response.AddMessage(_nonBlocking2, "argA", "argB", "argC");
            }

            Assert.AreEqual(true, response.Success);

            string serializedText = response.GetType().ToDataContractString(response);

            Trace.WriteLine(serializedText);
            Response response2 = serializedText.DeserializeFromDataContractSerializedString<Response>();

            Assert.AreEqual(true, response2.Success);


            object[] x = _info.GetType().GetCustomAttributes(false);


            var check = _info.GetMetadata();


            foreach (Message message in response2.Messages)
            {
                Assert.AreEqual(2, message.MessageCode.GetMetadata().ArgumentCount);
                Assert.IsTrue(message.MessageCode.GetMetadata().Text.StartsWith("Blah"));
            }

        }


        [Test]
        public void SerializingOfFalseSuccessWorks()
        {
            Response response = new Response();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_blockingWarning, "argA", "argB", "argC");
            }
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_error, "argA", "argB", "argC");
            }

            Assert.AreEqual(false, response.Success);

            string serializedText = response.GetType().ToDataContractString(response);

            Trace.WriteLine(serializedText);

            Response response2 = serializedText.DeserializeFromDataContractSerializedString<Response>();
            //As Has NonBlocking and iNfo should be OK.
            Assert.AreEqual(false, response2.Success);

        }



        [Test]
        public void MassiveCheckForRandomWierdNess()
        {
            Random random = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random;
            for (int x = 0; x < 100000; x++)
            {
                int randomMaxPerCategory = 2;

                Response response = new Response();

                int iMaxInfo = random.Next(randomMaxPerCategory);
                for (int i = 0; i < iMaxInfo; i++)
                {
                    response.AddMessage(_info, "argA", "argB", "argC");
                }

                int iMaxNonBlockingWarning = random.Next(randomMaxPerCategory);
                for (int i = 0; i < iMaxNonBlockingWarning; i++)
                {
                    response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
                }

                int iMaxBlockingWarning = random.Next(randomMaxPerCategory);
                for (int i = 0; i < iMaxBlockingWarning; i++)
                {
                    response.AddMessage(_blockingWarning, "argA", "argB", "argC");
                }
                int iMaxError = random.Next(randomMaxPerCategory); 
                for (int i = 0; i < iMaxError; i++)
                {
                    response.AddMessage(_error, "argA", "argB", "argC");
                }

                int originalCount = response.Messages.Count;

                Severity[] _noSuccess = new[] {Severity.Error, Severity.BlockingWarning};
                bool directCheck = response.Messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

                bool origSUccess = response.Success;

                Assert.AreEqual(directCheck, origSUccess, "Failed #1");


                string serializedText = response.GetType().ToDataContractString(response);

                
                Response response2 = serializedText.DeserializeFromDataContractSerializedString<Response>();

                Assert.AreEqual(originalCount, response2.Messages.Count);
                Assert.AreEqual(iMaxInfo, response2.Messages.Count(m => m.MessageCode.Severity == Severity.Info));
                Assert.AreEqual(iMaxNonBlockingWarning,
                                response2.Messages.Count(m => m.MessageCode.Severity == Severity.NonBlockingWarning));
                Assert.AreEqual(iMaxBlockingWarning,
                                response2.Messages.Count(m => m.MessageCode.Severity == Severity.BlockingWarning));
                Assert.AreEqual(iMaxError, response2.Messages.Count(m => m.MessageCode.Severity == Severity.Error));
                Assert.AreEqual(origSUccess, response2.Success);

            }
        }


        [Test]
        public void MassiveCheckForRandomWierdNessWhenNoMessages()
        {
            Random random = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random;
            for (int x = 0; x < 100000; x++)
            {
                //int randomMaxPerCategory = 2;

                Response response = new Response();

                int originalCount = response.Messages.Count;

                Severity[] _noSuccess = new[] { Severity.Error, Severity.BlockingWarning };
                bool directCheck = response.Messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

                bool origSUccess = response.Success;

                Assert.AreEqual(directCheck, origSUccess, "Failed #1");


                string serializedText = response.GetType().ToDataContractString(response);


                Response response2 = serializedText.DeserializeFromDataContractSerializedString<Response>();

                Assert.AreEqual(originalCount, response2.Messages.Count);
                Assert.AreEqual(origSUccess, response2.Success);

            }
        }




        [Test]
        public void MassiveCheckForRandomWierdNessWhenNoMessagesUsingJSONSerializer()
        {
            Random random = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random;
            for (int x = 0; x < 100000; x++)
            {
                //int randomMaxPerCategory = 2;

                Response response = new Response();

                int originalCount = response.Messages.Count;

                Severity[] _noSuccess = new[] { Severity.Error, Severity.BlockingWarning };
                bool directCheck = response.Messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

                bool origSUccess = response.Success;



                Assert.AreEqual(directCheck, origSUccess, "Failed #1");


                Assert.AreEqual(true, origSUccess, "Failed #2");

                string serializedText = response.ToJSON(Encoding.UTF8);


                Response response2 = serializedText.DeserialiseFromJSON<Response>(Encoding.UTF8);




                Assert.AreEqual(originalCount, response2.Messages.Count);
                Assert.AreEqual(origSUccess, response2.Success);

            }
        }








        [Test]
        public void JSONSerializingOfTrueSuccessWorks()
        {
            Response response = new Response();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 2; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }

            Assert.AreEqual(true, response.Success);

            string serializedText = response.ToJSON(response.GetType(), Encoding.UTF8);

            Trace.WriteLine(serializedText);
            Response response2 = serializedText.DeserialiseFromJSON<Response>(Encoding.UTF8);

            Assert.AreEqual(true, response2.Success);

        }

        [Test]
        public void JSONSerializingOfFalseSuccessWorks()
        {
            Response response = new Response();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_blockingWarning, "argA", "argB", "argC");
            }
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_error, "argA", "argB", "argC");
            }

            Assert.AreEqual(false, response.Success);

            string serializedText = response.ToJSON<Response>(Encoding.UTF8);

            Trace.WriteLine(serializedText);

            Response response2 = serializedText.DeserialiseFromJSON<Response>(Encoding.UTF8);
            //As Has NonBlocking and iNfo should be OK.
            Assert.AreEqual(false, response2.Success);

        }





        //...Tests go here...
        [Test]
        public void voidMessaegCodeEqualityChecks_AreNotEqual()
        {

            MessageCode messageCode2 = new MessageCode(13, Severity.Info);

            Assert.AreNotEqual(_info, messageCode2);
        }
        //...Tests go here...
        [Test]
        public void MessaegCodeEqualityChecks_AreEqual()
        {

            MessageCode messageCode2 = new MessageCode(123, Severity.Info);

            Assert.AreEqual(_info, messageCode2);
        }



        [Test]
        public void MessaegCodeService()
        {

            IMessageCodeMetadataService messageCodeMetadataService =
                XAct.DependencyResolver.Current.GetInstance<IMessageCodeMetadataService>();

            messageCodeMetadataService.Scan(typeof(ResponseTests));


           var metadata= _info2.GetMetadata();


            Assert.IsNotNull(metadata);
            Assert.AreEqual(metadata.ArgumentCount,2);
            Assert.IsTrue(metadata.Text.StartsWith("Blah"));
        }




    }
}
