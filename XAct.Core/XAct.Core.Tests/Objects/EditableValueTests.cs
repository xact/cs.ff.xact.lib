namespace XAct.Tests
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;
    using NUnit.Framework;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class EditableValueTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void EditableValueBase()
        {
            EditableValue<string> setting = new XAct.EditableValue<string>();
            setting.Value = "CheckMe";

            EditableValue<string> roundTrippedSetting = RoundTripSerialization(setting);
            Assert.AreEqual("CheckMe", roundTrippedSetting.Value);

        }


        private static T RoundTripSerialization<T>(T profile)
            where T : class
        {
            DataContractSerializer dataContractSerializer =
                new DataContractSerializer(typeof(T));
            DataContractSerializer dataContractSerializer2 =
                new DataContractSerializer(typeof(T));

            object resultObk;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                dataContractSerializer.WriteObject(memoryStream, profile);


                memoryStream.Position = 0;
                resultObk = dataContractSerializer2.ReadObject(memoryStream);
            }

            T profile2 = (T)resultObk as T;

            return profile2;
        }


    }
}