namespace XAct.Settings.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Extensions;
    using XAct.Tests;


    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SettingsTests
    {
        private const string EnvironmentIdentifier = "";
        private const string ZoneOrTierIdentifier = "Y";
        private const string HostIdentifier = "Z";
        private readonly Guid TennantIdentifier = Guid.Empty;

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void AddAnIntSettingToSettingsAndRetrieveSingleSetting()
        {
            XAct.Settings.Settings profile = new Settings();

            profile.AddSetting("SettingA",typeof(int),3);

            Setting profileSetting = profile.RetrieveSetting("SettingA");

            Assert.IsNotNull(profileSetting);

        }

        [Test]
        public void AddAnIntSettingToSettingsAndGetValue()
        {
            XAct.Settings.Settings profile = new Settings();

            profile.AddSetting("SettingA", typeof(int), 3);

            int result  = profile.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3,result);

        }

        [Test]
        public void AddAnIntSettingObjectToSettingsAndGetValue()
        {
            XAct.Settings.Settings profile = new Settings();


            Setting setting = new Setting("SettingA", typeof(int), 3);

            profile.AddSetting(setting);

            int result = profile.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result);

        }

        [Test]
        public void AddAnIntSettingObjectToSettingsGroupAndGetValue()
        {
            XAct.Settings.Settings profile = new Settings();


            Setting setting = new Setting("A/B", typeof(int), 3);

            profile.AddSetting(setting);

            int result = profile.GetSettingValue<int>("A/B");

            Assert.AreEqual(3, result);

        }


        [Test]
        public void AddAnStringSettingObjectToSettingsGroupAndGetValue()
        {
            XAct.Settings.Settings profile = new Settings();


            Setting setting = new Setting("A/B", typeof(string),"Test");

            profile.AddSetting(setting);

            string result = profile.GetSettingValue<string>("A/B");

            Assert.AreEqual("Test", result);

        }



        [Test]
        public void TryGetFailsCorrectly()
        {
            Settings settings = new Settings();

            Setting setting;
            bool found = settings.TryGetSetting("C", out setting);

            Assert.AreEqual(false, found);

        }
        [Test]
        public void TryGetSucceedsCorrectly()
        {
            Settings settings = new Settings();

            settings.AddSetting("C", typeof(int), 3);

            Setting setting;
            bool found = settings.TryGetSetting("C", out setting);

            Assert.AreEqual(true, found);

        }



        [Test]
        public void TryGetSucceedsCorrectlyWhenNested()
        {
            Settings settings = new Settings();

            settings.AddSetting("A/C", typeof(int), 3);

            Setting setting;
            bool found = settings.TryGetSetting("A/C", out setting);

            Assert.AreEqual(true, found);

        }

        [Test]
        public void TryGetSucceedsCorrectlyWhenDoubleNested()
        {
            Settings settings = new Settings();

            settings.AddSetting("A/B/C", typeof(int), 3);

            Setting setting;
            bool found = settings.TryGetSetting("A/B/C", out setting);

            Assert.AreEqual(true, found);

        }
        [Test]
        public void TryGetSucceedsCorrectlyWhenDoubleNested2()
        {
            Settings settings = new Settings();

            settings.AddSetting("A/B/C", typeof(int), 3);

            Setting setting;
            bool found = settings["A"].TryGetSetting("B/C", out setting);

            Assert.AreEqual(true, found);

        }

        [Test]
        public void TryGetSucceedsCorrectlyWhenDoubleNested3()
        {
            Settings settings = new Settings();

            settings.AddSetting("A/B/C", typeof(int), 3);

            Setting setting;
            bool found = settings["A"]["B"].TryGetSetting("C", out setting);

            Assert.AreEqual(true, found);

        }


        [Test]
        public void DisAllowAdditionOfTwoSettingsWithSameName()
        {
            Settings settings = new Settings();

            settings.AddSetting("C", typeof(int), 3);
            try
            {
                settings.AddSetting("C", typeof (int), 5);
                Assert.IsTrue(false);
            }
            catch
            {
                Assert.IsTrue(true);
            }


        }



        [Test]
        public void SettingInProfileSubGroup()
        {
            XAct.Settings.Settings profile = new Settings();

            SettingGroup p1 = profile["P.1"];


            p1.AddSetting("SettingA", typeof(int), 3);

            int result = p1.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result);

        }

        [Test]
        public void SettingInProfileSubSubGroup()
        {
            XAct.Settings.Settings profile = new Settings();

            SettingGroup p1 = profile["P.1"];

            SettingGroup p2 = p1["P.2"];

            p2.AddSetting("SettingA", typeof(int), 3);

            int result = p2.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result);

        }




        [Test]
        public void CreateAndAddNewSettingToBaseProfilesSubGroup_RetrieveFromProfile()
        {
            //Add a setting in a sub group of base Profile (not a Module)
            XAct.Settings.Settings profile = new Settings();

            SettingGroup p1=  profile["P.1"];


            p1.AddSetting("SettingA", typeof(int), 3);

            int result = p1.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result);

        }

        [Test]
        public void CreateAndAddNewSettingToBaseProfilesSubSubGroup_RetrieveFromSettingGroup()
        {
            //Add a setting in a sub group of base Profile (not a Module)
            XAct.Settings.Settings profile = new Settings();

            SettingGroup p1 = profile["P.1"];

            SettingGroup p2 = p1["P.2"];

            p2.AddSetting("SettingA", typeof(int), 3);

            int result = p2.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result);

        }



        [Test]
        public void CreateNewSettingInBaseProfileSubGroupSubGroup_RetrieveFromProfile_UsingPathSyntax()
        {
            //Add a setting in a sub group of base Profile (not a Module)
            XAct.Settings.Settings profile = new Settings();

            profile["P.1"]["P.2"].AddSetting("SettingA", typeof(int), 3);

            int result = profile.GetSettingValue<int>("P.1/P.2/SettingA");

            Assert.AreEqual(3, result);
        }



        [Test]
        public void AddSetting_Int_RetrieveFromProfileByPathSyntax()
        {
            Settings settings = new Settings();
            settings["A"]["B"].AddSetting("C", typeof(int), 3);

            int check = settings.GetSettingValue<int>("A/B/C");

            Assert.AreEqual(3, check);
        }


        [Test]
        public void AddSetting_Int_ApplyReset_RetrieveFromProfilePathSyntax()
        {
            Settings settings = new Settings();
            settings["A"]["B"].AddSetting("C", typeof(int), 3, 
                new SettingEditingMetadata(
                    //new ApplicationSettingScope(
                    //    EnvironmentIdentifier,
                    //    ZoneOrTierIdentifier,
                    //    HostIdentifier,
                    //    TennantIdentifier), 
                    -1));

            settings["A"]["B"].RetrieveSetting("C").ResetValue();
            int check = settings.GetSettingValue<int>("A/B/C");

            Assert.AreEqual(-1, check);
        }

        [Test]
        public void AddSetting_Int_RetrieveFromSubGroupPathSyntax()
        {
            Settings settings = new Settings();
            settings["A"]["B"].AddSetting("C", typeof(int), 3);

            int check = settings["A"]["B"].GetSettingValue<int>("C");

            Assert.AreEqual(3, check);
        }


        [Test]
        public void CorrectlyDistinguishesBetweenSubGroupAndValueWithSameName()
        {
            Settings settings = new Settings();
            settings["A"]["B"].AddSetting("C", typeof(int), 3);
            settings["A"]["B"].RetrieveSetting("C").ResetValue();

            settings["A"]["B"]["C"].AddSetting("D", typeof(int), 5);
            int check = settings.GetSettingValue<int>("A/B/C/D");
            
            Assert.AreEqual(5, check);
        }




        //[ExpectedException]
        [Test]
        //[ExpectedException(typeof(ArgumentException), "SettingsGroup completed initialization. Key 'C' not found.")]
        public void AddRetrieveSettingsFromSubSubGroup()
        {
            Settings settings = new Settings();
            settings["A"]["B"].AddSetting("C", typeof(int), 3);
            Setting setting = settings["A"]["B"].RetrieveSetting("C");
            Assert.IsNotNull(setting);
        }

        public void AddRetrieveSettingsFromSubSubGroup2()
        {
            Settings settings = new Settings();
            settings["A"]["B"].AddSetting("C", typeof(int), 3);
            Setting setting = settings["A"]["B"].RetrieveSetting("C");
            setting.ResetValue();
            Assert.IsNotNull(setting);
            Assert.AreEqual(0,setting.Value);
        }

        //[ExpectedException]
        [Test]
        //[ExpectedException(typeof(ArgumentException), "SettingsGroup completed initialization. Key 'C' not found.")]
        public void UnitTestA6()
        {
            Settings settings = new Settings();
            settings["A"]["B"].AddSetting("C", typeof(int), 3);
            settings["A"]["B"].RetrieveSetting("C").ResetValue();
            try
            {
                //this will throw an exception:
                settings["A"]["B"]["C"].AddSetting("D", typeof(int), 5);
                int check = settings.GetSettingValue<int>("A/B/C/D");
                Assert.AreEqual(0, check);
            }
            catch
            {
                Assert.IsTrue(true);
            }
        }



         //---------------------------------------------------------------------------------
        //Dealing with not Profile, but Module:
        //---------------------------------------------------------------------------------















        [Test]
        public void CreateAndAddNewSettingToAModule_RetriveFromSubGroup()
        {
            //Add a setting in a sub group of Module within Base Profile
            XAct.Settings.Settings profile = new Settings();

            ModuleSettingGroup moduleSettingsGroup  = profile.GetModule("ModA");

            moduleSettingsGroup.AddSetting("SettingA", typeof(int), 3);

            int result = moduleSettingsGroup.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result);

        }

        [Test]
        public void CreateAndAddNewSettingToAModulesSubGroup_RetrieveFromSubGroup()
        {
            XAct.Settings.Settings profile = new Settings();

            ModuleSettingGroup moduleSettingsGroup = profile.GetModule("ModA");

            SettingGroup subA = moduleSettingsGroup["SubA"];

            subA.AddSetting("SettingA", typeof (int), 3);

            int result = subA.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result);

        }



        [Test]
        public void CreateAndAddNewSettingToAModulesSubGroup_RetriveFromModule_UsingPathSyntax()
        {
            XAct.Settings.Settings profile = new Settings();
            ModuleSettingGroup moduleSettingsGroup = profile.GetModule("ModA");
            SettingGroup subA = moduleSettingsGroup["SubA"];
            subA.AddSetting("SettingA", typeof(int), 3);
            int result = moduleSettingsGroup.GetSettingValue<int>("SubA/SettingA");

            Assert.AreEqual(3, result);

        }

        [Test]
        public void CreateAndAddNewSettingToAModulesSubSubGroup_RetrievedFromModule_UsingPathSyntax()
        {
            XAct.Settings.Settings profile = new Settings();
            ModuleSettingGroup moduleSettingsGroup = profile.GetModule("ModA");
            //Two nested groups under the mod:
            SettingGroup subA = moduleSettingsGroup["A.1"];
            SettingGroup subB = subA["A.1.1"];
            subB.AddSetting("SettingA", typeof(int), 3);
            int result = moduleSettingsGroup.GetSettingValue<int>("A.1/A.1.1/SettingA");
            Assert.AreEqual(3, result);
        }


        [Test]
        public void CreateAndAddNewSettingToAModulesSubSubGroup_RetrievedFromProfile_ThenModule_UsingPathSyntax()
        {
            XAct.Settings.Settings profile = new Settings();
            ModuleSettingGroup moduleSettingsGroup = profile.GetModule("ModA");
            //Two nested groups under the mod:
            SettingGroup subA = moduleSettingsGroup["A.1"];
            SettingGroup subB = subA["A.1.1"];
            subB.AddSetting("SettingA", typeof(int), 3);
            int result = profile.GetModule("ModA").GetSettingValue<int>("A.1/A.1.1/SettingA");
            Assert.AreEqual(3, result);
        }



        [Test]
        public void CreateAndAddNewSettingToAModulesSubSubGroup_RetrievedFromProfile_UsingPathSyntax()
        {
            XAct.Settings.Settings profile = new Settings();
            profile.AddSetting<int>("ModA:A.1/A.1.1/SettingA", 3);

            int result = profile.GetModule("ModA").GetSettingValue<int>("A.1/A.1.1/SettingA");
            Assert.AreEqual(3, result);
        }



        [Test]
        public void CreateAndAddNewSettingToAModulesSubSubGroup_RetrievedFromProfile_UsingPathSyntax2()
        {
            XAct.Settings.Settings profile = new Settings();
            profile.AddSetting<int>("ModA:A.1/A.1.1/SettingA", 3);

            int result = profile.GetSettingValue<int>("ModA:A.1/A.1.1/SettingA");

            //YAY!

            Assert.AreEqual(3, result);
        }














        //---------------------------------------------------------------------------------
        //Dictionary Setting:
        //---------------------------------------------------------------------------------

        //[Test]
        //public void SetUsingDictionary()
        //{
        //    Dictionary<string,string> fromAppHostOrDb = new Dictionary<string,string>();
            
        //    fromAppHostOrDb.Add("A","Value A");
        //    fromAppHostOrDb.Add("A/A1", "Value A.A1");
        //    fromAppHostOrDb.Add("A/B/A1", "Value A.B.A1");

        //    fromAppHostOrDb.Add("Test", "ok");
        //    fromAppHostOrDb.Add("Sub1/Sub2/Sub3", "ok");
        //    fromAppHostOrDb.Add("module2:SubA/SubB/SubC", "ok");
        //    fromAppHostOrDb.Add("[System.Boolean]module3:SubA/SubB/SubC", "True");
        //    fromAppHostOrDb.Add("[System.Int32]module3:SubA/SubB/SubD", "-123");
        //    fromAppHostOrDb.Add("[System.Int32]counter", "0");


        //    Settings settings = new Settings();

        //    settings.SetValues(fromAppHostOrDb,null);


        //    Assert.AreEqual("Value A",settings.GetSettingValue<string>("A"));
        //    Assert.AreEqual("Value A.A1", settings.GetSettingValue<string>("A/A1"));
        //    Assert.AreEqual("Value A.B.A1", settings.GetSettingValue<string>("A/B/A1"));

        //}



        //[Test]
        //public void SetUsingDictionaryForModuleValues()
        //{
        //    Dictionary<string, string> fromAppHostOrDb = new Dictionary<string, string>();

        //    fromAppHostOrDb.Add("A", "Value A");
        //    fromAppHostOrDb.Add("A/A1", "Value A.A1");
        //    fromAppHostOrDb.Add("A/B/A1", "Value A.B.A1");

        //    fromAppHostOrDb.Add("Test", "ok");
        //    fromAppHostOrDb.Add("Sub1/Sub2/Sub3", "ok");
        //    fromAppHostOrDb.Add("module2:SubA/SubB/SubC", "ok");
        //    fromAppHostOrDb.Add("[System.Boolean]module3:SubA/SubB/SubC", "True");
        //    fromAppHostOrDb.Add("[System.Int32]module3:SubA/SubB/SubD", "-123");
        //    fromAppHostOrDb.Add("[System.Int32]counter", "0");


        //    Settings settings = new Settings();

        //    settings.SetValues(fromAppHostOrDb, null);


        //    Assert.AreEqual("Value A", settings.GetSettingValue<string>("A"));
        //    Assert.AreEqual("Value A.A1", settings.GetSettingValue<string>("A/A1"));
        //    Assert.AreEqual("Value A.B.A1", settings.GetSettingValue<string>("A/B/A1"));

        //    Assert.AreEqual("ok", settings.GetSettingValue<string>("module2:SubA/SubB/SubC"));
        //    Assert.AreEqual("ok", settings.GetModule("module2").GetSettingValue<string>("SubA/SubB/SubC"));
        //}



        //[Test]
        //public void SetUsingDictionaryOfTypedValues()
        //{
        //    Dictionary<string, string> fromAppHostOrDb = new Dictionary<string, string>();

        //    fromAppHostOrDb.Add("A", "Value A");
        //    fromAppHostOrDb.Add("A/A1", "Value A.A1");
        //    fromAppHostOrDb.Add("A/B/A1", "Value A.B.A1");

        //    fromAppHostOrDb.Add("Test", "ok");
        //    fromAppHostOrDb.Add("Sub1/Sub2/Sub3", "ok");
        //    fromAppHostOrDb.Add("module2:SubA/SubB/SubC", "ok");
        //    fromAppHostOrDb.Add("[System.Boolean]module3:SubA/SubB/SubC", "True");
        //    fromAppHostOrDb.Add("[System.Int32]module3:SubA/SubB/SubD", "-123");
        //    fromAppHostOrDb.Add("[System.Int32]counter", "0");


        //    Settings settings = new Settings();

        //    settings.SetValues(fromAppHostOrDb, null);


        //    Assert.AreEqual("Value A", settings.GetSettingValue<string>("A"));
        //    Assert.AreEqual("Value A.A1", settings.GetSettingValue<string>("A/A1"));
        //    Assert.AreEqual("Value A.B.A1", settings.GetSettingValue<string>("A/B/A1"));

        //    Assert.AreEqual("ok", settings.GetSettingValue<string>("module2:SubA/SubB/SubC"));
        //    Assert.AreEqual("ok", settings.GetModule("module2").GetSettingValue<string>("SubA/SubB/SubC"));

        //    Assert.AreEqual(true, settings.GetSettingValue<bool>("module3:SubA/SubB/SubC"));
        //    Assert.AreEqual(true, settings.GetModule("module3").GetSettingValue<bool>("SubA/SubB/SubC"));

        //    Assert.AreEqual(-123, settings.GetSettingValue<int>("module3:SubA/SubB/SubD"));
        //    Assert.AreEqual(-123, settings.GetModule("module3").GetSettingValue<int>("SubA/SubB/SubD"));

        //}

        //[Test]
        //public void SetUsingDictionaryOfTypedValues_caseInsensitive()
        //{
        //    Dictionary<string, string> fromAppHostOrDb = new Dictionary<string, string>();

        //    fromAppHostOrDb.Add("A", "Value A");
        //    fromAppHostOrDb.Add("A/A1", "Value A.A1");
        //    fromAppHostOrDb.Add("A/B/A1", "Value A.B.A1");

        //    fromAppHostOrDb.Add("Test", "ok");
        //    fromAppHostOrDb.Add("Sub1/Sub2/Sub3", "ok");
        //    fromAppHostOrDb.Add("module2:SubA/SubB/SubC", "ok");
        //    fromAppHostOrDb.Add("[System.Boolean]module3:SubA/SubB/SubC", "True");
        //    fromAppHostOrDb.Add("[System.Int32]module3:SubA/SubB/SubD", "-123");
        //    fromAppHostOrDb.Add("[System.Int32]counter", "0");


        //    Settings settings = new Settings();

        //    settings.SetValues(fromAppHostOrDb, null);


        //    Assert.AreEqual("Value A", settings.GetSettingValue<string>("A"));
        //    Assert.AreEqual("Value A.A1", settings.GetSettingValue<string>("A/A1"));
        //    Assert.AreEqual("Value A.B.A1", settings.GetSettingValue<string>("A/B/A1"));

        //    Assert.AreEqual("ok", settings.GetSettingValue<string>("module2:SubA/SubB/SubC"));
        //    Assert.AreEqual("ok", settings.GetModule("Module2").GetSettingValue<string>("Suba/SubB/subC"));

        //    Assert.AreEqual(true, settings.GetSettingValue<bool>("module3:SubA/SubB/SubC"));
        //    Assert.AreEqual(true, settings.GetModule("Module3").GetSettingValue<bool>("SubA/SubB/subc"));

        //    Assert.AreEqual(-123, settings.GetSettingValue<int>("Module3:SubA/SubB/SubD"));
        //    Assert.AreEqual(-123, settings.GetModule("Module3").GetSettingValue<int>("Suba/Subb/Subd"));

        //}



        //---------------------------------------------------------------------------------
        //Dealing with Serialization:
        //---------------------------------------------------------------------------------


        [Test]
        public void IterateASettingCollection()
        {
            XAct.Settings.Settings profile = new Settings();
            //Others:
            profile.AddSetting<int>("ModA:A.1/A.1.2/SettingA", 1);
            profile.AddSetting<int>("ModA:A.1/A.1.2/SettingB", 2);
            profile.AddSetting<int>("ModA:A.1/A.1.2/SettingC", 3);

            //The ones we want:
            profile.AddSetting<int>("ModA:A.1/A.1.1/SettingA", 3);
            profile.AddSetting<int>("ModA:A.1/A.1.1/SettingB", 4);
            profile.AddSetting<int>("ModA:A.1/A.1.1/SettingC", 5);
            profile.AddSetting<int>("ModA:A.1/A.1.1/SettingD", 6);

            //But will also pick up child items, unless filtered out:
            profile.AddSetting<int>("ModA:A.1/A.1.1/SubA/SettingA", 1);
            profile.AddSetting<int>("ModA:A.1/A.1.1/SubA/SettingB", 2);
            profile.AddSetting<int>("ModA:A.1/A.1.1/SubA/SettingC", 3);

            List<Setting> settings = new List<Setting>();
            var module = profile.Modules["ModA"];
            IEnumerator<Setting> enumerator = profile.Modules["ModA"]["A.1"]["A.1.1"].GetEnumerator();
            while (enumerator.MoveNext())
            {
                settings.Add(enumerator.Current);
            }
            Assert.AreEqual(7, settings.Count);
        }


        //---------------------------------------------------------------------------------
        //Dealing with Serialization:
        //---------------------------------------------------------------------------------



        [Test]
        public void ProfileIsWCFSerializable()
        {
            XAct.Settings.Settings profile = new Settings();
            ModuleSettingGroup moduleSettingsGroup = profile.GetModule("ModA");
            string check1 = moduleSettingsGroup.NamePrefix;
            SettingGroup subA = moduleSettingsGroup["SubA"];
            subA.AddSetting("SettingA", typeof(int), 3);
            string check2 = subA.NamePrefix;

            //int result1 = profile["ModA"]["SubA"].GetValue<int>("SettingA");

            int result0 = subA.GetSettingValue<int>("SettingA");
            int result1 = moduleSettingsGroup.GetSettingValue<int>("SubA/SettingA");
            int result11 = moduleSettingsGroup["SubA"].GetSettingValue<int>("SettingA");
            int result12 = profile.GetModule("ModA")["SubA"].GetSettingValue<int>("SettingA");


            System.Runtime.Serialization.DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(Settings));
            System.Runtime.Serialization.DataContractSerializer dataContractSerializer2 = new DataContractSerializer(typeof(Settings));

            object resultObk;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                dataContractSerializer.WriteObject(memoryStream, profile);


                memoryStream.Position = 0;
                resultObk = dataContractSerializer2.ReadObject(memoryStream);
            }

            Settings profile2 = resultObk as Settings;

            //ProfileModuleSettingsGroup moduleSettingsGroup2 = profile2.GetModule("ModA");

            ModuleSettingGroup moduleSettingsGroup2 = profile2.GetModule("ModA");
            SettingGroup subA2 = moduleSettingsGroup2["SubA"];
            int result2 = subA2.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result2);

        }

        [Test]
        public void ProfileIsWCFSerializable2()
        {
            XAct.Settings.Settings profile = new Settings();
            ModuleSettingGroup moduleSettingsGroup = profile.GetModule("ModA");
            string check1 = moduleSettingsGroup.NamePrefix;
            SettingGroup subA = moduleSettingsGroup["SubA"];
            subA.AddSetting("SettingA", typeof(int), 3);
            string check2 = subA.NamePrefix;

            //int result1 = profile["ModA"]["SubA"].GetValue<int>("SettingA");

            int result0 = subA.GetSettingValue<int>("SettingA");
            int result1 = moduleSettingsGroup.GetSettingValue<int>("SubA/SettingA");
            int result11 = moduleSettingsGroup["SubA"].GetSettingValue<int>("SettingA");
            int result12 = profile.GetModule("ModA")["SubA"].GetSettingValue<int>("SettingA");


            System.Runtime.Serialization.DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(Settings));
            
            System.Runtime.Serialization.DataContractSerializer dataContractSerializer2 = new DataContractSerializer(typeof(Settings));


            Settings profile2 = profile.TestWCFSerialization();


            ModuleSettingGroup moduleSettingsGroup2 = profile2.GetModule("ModA");
            SettingGroup subA2 = moduleSettingsGroup2["SubA"];
            int result2 = subA2.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result2);

        }

        [Test]
        public void SerializableObject()
        {
            SerializedObject obj = new XAct.SerializedObject(SerializationMethod.String, typeof(int), 3);

            SerializedObject roundTrippedObj = RoundTripSerialization(obj);
            Assert.AreEqual("3", roundTrippedObj.SerializedValue);
        }

        [Test]
        public void SerializableObject2()
        {
            SerializedObject obj = new XAct.SerializedObject(SerializationMethod.String, typeof(double), 3.12d);

            SerializedObject roundTrippedObj = RoundTripSerialization(obj);
            Assert.AreEqual("3.12", roundTrippedObj.SerializedValue);
        }


        [Test]
        public void SerializableObject3()
        {
            SerializedObject obj = new XAct.SerializedObject(SerializationMethod.String, typeof(string), "Boobly");

            SerializedObject roundTrippedObj = RoundTripSerialization(obj);
            Assert.AreEqual("Boobly", roundTrippedObj.SerializedValue);

        }

        [Test]
        public void SerializableObject4()
        {
            DateTime x = DateTime.Now;
            SerializedObject obj = new XAct.SerializedObject(SerializationMethod.String, typeof(DateTime), x);

            SerializedObject roundTrippedObj = RoundTripSerialization(obj);
            Assert.AreEqual(x.ToString("O"), roundTrippedObj.SerializedValue);

        }


        [Test]
        public void SerializableObject_DateTime()
        {
            DateTime x = DateTime.Now;
            SerializedObject obj = new XAct.SerializedObject(SerializationMethod.String, typeof(DateTime), x);

            SerializedObject roundTrippedObj = RoundTripSerialization(obj);
            Assert.AreEqual(x.ToString("O"), roundTrippedObj.SerializedValue);

            DateTime check = roundTrippedObj.DeserializeValue<DateTime>();

            Assert.AreEqual(x, check);

        }



        [Test]
        public void SerializeSetting()
        {
            Setting setting = new Setting("SettingA", typeof(int), 45678);
            Setting roundTrippedSetting = RoundTripSerialization(setting);
            Assert.AreEqual(45678, roundTrippedSetting.Value);

        }
        [Test]
        public void SerializeSettings()
        {
            Setting setting = new Setting("SettingA", typeof(int), 45678);
            Setting roundTrippedSetting = RoundTripSerialization(setting);
            Assert.AreEqual(45678, roundTrippedSetting.Value);


        }
        [Test]
        public void SerializeSettings2()
        {
            Setting setting = new Setting("SettingA", typeof(bool), true);
            Setting roundTrippedSetting = RoundTripSerialization(setting);
            Assert.AreEqual(true, roundTrippedSetting.Value);

        }


        [Test]
        public void SerializeSettings3()
        {
            Setting setting = new Setting("SettingA", typeof(double), 3.45);
            Setting roundTrippedSetting = RoundTripSerialization(setting);
            Assert.AreEqual(3.45, roundTrippedSetting.Value);
        }

        [Test]
        public void SerializeSettingsCollection()
        {
#pragma warning disable 168
            SettingCollection settingCollection = new SettingCollection();

            Setting setting = new Setting("SettingA", typeof(double), 3.45);

            settingCollection.Add(setting);

            SettingCollection roundTrippedObj = RoundTripSerialization(settingCollection);

            Assert.AreEqual(1, roundTrippedObj.Count);
            Assert.AreEqual(3.45, roundTrippedObj[0].Value);
        }

        [Test]
        public void SerializeModule()
        {
#pragma warning disable 168
            ModuleSettingGroup moduleSettingsGroup = new ModuleSettingGroup("GroupO", null);

            Setting setting = new Setting("SettingA", typeof(double), 3.45);

            moduleSettingsGroup.AddSetting(setting);

            ModuleSettingGroup roundTrippedObj = RoundTripSerialization(moduleSettingsGroup);

            SettingCollection settingCollection =
                ((IProfileSettingCollectionAccessor)roundTrippedObj).Settings;
            Assert.AreEqual(1, settingCollection.Count);
            Assert.AreEqual(3.45, settingCollection[0].Value);
        }


        [Test]
        public void UnitTestCheckSerialization()
        {
#pragma warning disable 168
            XAct.Settings.Settings profile = new Settings();

            ModuleSettingGroup moduleSettingsGroup = profile.GetModule("ModA");
            string check1 = moduleSettingsGroup.NamePrefix;

            SettingGroup subA = moduleSettingsGroup["SubA"];

            subA.AddSetting("SettingA", typeof(int), 3);



            string check2 = subA.NamePrefix;

            //int result1 = profile["ModA"]["SubA"].GetValue<int>("SettingA");

            int result0 = subA.GetSettingValue<int>("SettingA");
            int result1 = moduleSettingsGroup.GetSettingValue<int>("SubA/SettingA");
            int result11 = moduleSettingsGroup["SubA"].GetSettingValue<int>("SettingA");


            int result12 = profile.GetModule("ModA")["SubA"].GetSettingValue<int>("SettingA");
#pragma warning restore 168


            Settings profile2 = RoundTripSerialization(profile);

            //ProfileModuleSettingsGroup moduleSettingsGroup2 = profile2.GetModule("ModA");

            ModuleSettingGroup moduleSettingsGroup2 = profile2.GetModule("ModA");
            SettingGroup subA2 = moduleSettingsGroup2["SubA"];
            int result2 = subA2.GetSettingValue<int>("SettingA");

            Assert.AreEqual(3, result2);

        }

        private static T RoundTripSerialization<T>(T profile)
            where T : class
        {
            DataContractSerializer dataContractSerializer =
                new DataContractSerializer(typeof(T));
            DataContractSerializer dataContractSerializer2 =
                new DataContractSerializer(typeof(T));

            object resultObk;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                dataContractSerializer.WriteObject(memoryStream, profile);


                memoryStream.Position = 0;
                resultObk = dataContractSerializer2.ReadObject(memoryStream);
            }

            T profile2 = (T)resultObk as T;

            return profile2;
        }




    }


}


