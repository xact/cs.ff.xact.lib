﻿namespace Tests.Objects
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Extensions;
    using XAct.Messages;
    using XAct.Tests;

    public class UnitTestReferenceData :ReferenceDataBase<int>
    {
        
    }


    [TestFixture]
    public class ReferenceDataTests
    {


        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanRoundTripAReferenceDataEntity()
        {

            UnitTestReferenceData referenceData = new UnitTestReferenceData();
            referenceData.Description = "Foo";
            referenceData.Enabled = true;
            referenceData.Filter = "...";
            referenceData.Id = 3;
            referenceData.Order = 3;
            referenceData.ResourceFilter = "R_Filter";
            referenceData.Tag = "Tag";
            referenceData.Text = "...";

            string wcfString = referenceData.ToDataContractString<UnitTestReferenceData>();

            var referenceData2 = wcfString.DeserializeFromDataContractSerializedString<UnitTestReferenceData>();

            Assert.AreEqual(referenceData.Description, referenceData2.Description,"DESC");
            Assert.AreEqual(referenceData.Enabled, referenceData2.Enabled, "DESC");

            Assert.AreEqual(referenceData.Filter, referenceData2.Filter, "DESC");
            Assert.AreEqual(referenceData.Id, referenceData2.Id, "DESC");
            Assert.AreEqual(referenceData.Order, referenceData2.Order, "DESC");
            Assert.AreEqual(referenceData.ResourceFilter, referenceData2.ResourceFilter, "DESC");
            Assert.AreEqual(referenceData.Tag, referenceData2.Tag, "DESC");
            Assert.AreEqual(referenceData.Text, referenceData2.Text, "DESC");
        }

    }
}
