﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Extensions;
    using XAct.Messages;

    [TestFixture]
    public class ReferenceDataIOModelTests
    {


        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanRoundTrip()
        {
            ReferenceDataIOModel referenceDataIOModel = new ReferenceDataIOModel();
            referenceDataIOModel.Description = "foo";
            referenceDataIOModel.Filter = "...";
            referenceDataIOModel.Key = "Bar";
            referenceDataIOModel.Metadata.Add(new Metadata{Key="K",Value = "V"});
            referenceDataIOModel.Metadata.Add(new Metadata { Key = "K2", Value = "V2" });

            string wcfStream = referenceDataIOModel.ToDataContractString<ReferenceDataIOModel>();
            ReferenceDataIOModel referenceDataIOModel2 =
                wcfStream.DeserializeFromDataContractSerializedString<ReferenceDataIOModel>();

            Assert.AreEqual(referenceDataIOModel.Description,referenceDataIOModel2.Description);
            Assert.AreEqual(referenceDataIOModel.Filter, referenceDataIOModel2.Filter);
            Assert.AreEqual(referenceDataIOModel.Key, referenceDataIOModel2.Key);
            Assert.AreEqual(referenceDataIOModel.Metadata.Count, referenceDataIOModel2.Metadata.Count);

        }
    }
}