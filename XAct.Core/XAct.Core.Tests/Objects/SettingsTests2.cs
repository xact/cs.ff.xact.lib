﻿namespace XAct.Settings.Tests
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SettingTests2
    {
        const string ApplicationName = "X";
        private const string ZoneOrTierName = "Y";
        private const string HostName = "Z";
        private const string UserName = null;

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void SetAndGetSettingValue()
        {
            Settings settings = new Settings();

            List<SerializedApplicationSetting> fromRepository = new List<SerializedApplicationSetting>();

            GetEntries(fromRepository);

            settings.AddSetting(new Setting("A/B",typeof(string),"123","-123"));

            Setting se;
            settings["A"].TryGetSetting("B", out se);

            Setting se2;
            settings.TryGetSetting("A/B", out se2);

            int result = settings["A"].GetSettingValue<int>("B");

            Assert.AreEqual(123, result);
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void SetAndGetSettingValueThatIsAuthorized()
        {
            Settings settings = new Settings();

            List<SerializedApplicationSetting> fromRepository = new List<SerializedApplicationSetting>();


            GetEntries(fromRepository);

            var serializedSettings = fromRepository.ToArray();

            settings.Load(
                serializedSettings,
                true,
                true,
                DelegateIsLockedReturnsTrue,
                DelegateRead,
                DelegateWrite
                );

            Setting se;
            settings["A"].TryGetSetting("B", out se);

            Setting se2;
            settings.TryGetSetting("A/B", out se2);

            int result = settings["A"].GetSettingValue<int>("B");

            Assert.AreEqual(123, result);
        }



        [Test]
        public void SetAndGetSettingValueThatNotAuthorizedCorrectlyRaisesException()
        {
            Settings settings = new Settings();

            List<SerializedApplicationSetting> fromRepository = new List<SerializedApplicationSetting>();


            GetEntries(fromRepository);

            var serializedSettings = fromRepository.ToArray();

            settings.Load(
                serializedSettings,
                true,
                true,
                DelegateIsLockedReturnsTrue,
                DelegateRead2,
                DelegateWrite2
                );

            Setting se;
            settings["A"].TryGetSetting("B", out se);

            Setting se2;
            settings.TryGetSetting("A/B", out se2);

            try
            {
                int result = settings["A"].GetSettingValue<int>("B");
                Assert.IsTrue(false);
            }
            catch
            {
                Assert.IsTrue(true);
            }

        }




        [Test]
        public void SetAndGetSettingValueWithWritePermissionTrue()
        {
            Settings settings = new Settings();

            List<SerializedApplicationSetting> fromRepository = new List<SerializedApplicationSetting>();


            GetEntries(fromRepository);

            var serializedSettings = fromRepository.ToArray();

            settings.Load(
                serializedSettings,
                true,
                true,
                DelegateIsLockedReturnsTrue,
                DelegateRead,
                DelegateWrite
                );

            Setting se;
            settings["A"].TryGetSetting("B", out se);

            Setting se2;
            settings.TryGetSetting("A/B", out se2);

            try
            {
                int result = settings["A"].GetSettingValue<int>("B");
                Assert.AreEqual(result, 123);

                settings.SetSettingValue("A/B", 10);
                int result2 = settings["A"].GetSettingValue<int>("B");
                Assert.AreEqual(result2,10);
            }
            catch
            {
                Assert.IsTrue(true);
            }

        }

        [Test]
        public void SetAndGetSettingValueWithWritePermissionFalse()
        {
            Settings settings = new Settings();

            List<SerializedApplicationSetting> fromRepository = new List<SerializedApplicationSetting>();


            GetEntries(fromRepository);

            var serializedSettings = fromRepository.ToArray();

            settings.Load(
                serializedSettings,
                true,
                true,
                DelegateIsLockedReturnsTrue,
                DelegateRead,
                DelegateWrite2
                );

            Setting se;
            settings["A"].TryGetSetting("B", out se);

            Setting se2;
            settings.TryGetSetting("A/B", out se2);

            int result = settings["A"].GetSettingValue<int>("B");
            Assert.AreEqual(result, 123);

            try
            {

                settings.SetSettingValue("A/B", 10);
                int result2 = settings.GetSettingValue<int>("A/B");
                Assert.IsTrue(false);
            }
            catch
            {
                Assert.IsTrue(true);
            }

        }


        [Test]
        public void SetAndGetSettingValueWithWritePermissionTrueButLocked()
        {
            Settings settings = new Settings();

            List<SerializedApplicationSetting> fromRepository = new List<SerializedApplicationSetting>();


            GetEntries(fromRepository);

            var serializedSettings = fromRepository.ToArray();

            settings.Load(
                serializedSettings,
                true,
                true,
                DelegateIsLockedReturnsFalse,
                DelegateRead,
                DelegateWrite
                );

            Setting se;
            settings["A"].TryGetSetting("B", out se);

            Setting se2;
            settings.TryGetSetting("A/B", out se2);

            int result = settings["A"].GetSettingValue<int>("B");
            Assert.AreEqual(result, 123);

            try
            {

                settings.SetSettingValue("A/B", 10);
                int result2 = settings.GetSettingValue<int>("A/B");
                Assert.IsTrue(false);
            }
            catch
            {
                Assert.IsTrue(true);
            }

        }





        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void LoadFromDataStoreThenAddMoreAndDistinguishBetweenNewAndExisting()
        {
            Settings settings = new Settings();

            List<SerializedApplicationSetting> fromRepository = new List<SerializedApplicationSetting>();


            GetEntries(fromRepository);

            var serializedSettings = fromRepository.ToArray();

            settings.Load(
                serializedSettings,
                true,
                true,
                DelegateIsLockedReturnsTrue,
                DelegateRead,
                DelegateWrite
                );


            //Setting se;
            //settings.TryApplicationIdentifier", out se);
            //Assert.IsNotNull(se);


            

        }














        public static bool DelegateRead(string s)
        {
            return s.Contains("USER");
        }
        public static bool DelegateRead2(string s)
        {
            return s.Contains("NOT_USER");
        }
        public static bool DelegateWrite(string s)
        {
            return s.Contains("ACCOUNTANT");
        }
        public static bool DelegateWrite2(string s)
        {
            return s.Contains("NOT_ACCOUNTANT");
        }
        public static bool DelegateIsLockedReturnsTrue(string s)
        {
            return true;
        }
        public static bool DelegateIsLockedReturnsFalse(string s)
        {
            return false;
        }

        private static void GetEntries(List<SerializedApplicationSetting> fromRepository)
        {
#pragma warning disable 168
            //string applicationName = "/";
#pragma warning restore 168

            fromRepository.Add(
                new SerializedApplicationSetting
                    {
                        Description = "...description...",
                        Enabled = true,
                        ZoneOrTierIdentifier = string.Empty,
                        HostIdentifier = "MYCOMPUTER01",
                        IsReadableAuthorisationInformation = "USER",
                        IsWritableAuthorisationInformation = "ACCOUNTANT",
                        IsUnlockedInformation = null,
                        Key = "A/B",
                        SerializationMethod = SerializationMethod.String,
                        SerializedValueType = typeof (int).FullName,
                        SerializedValue = "123"
                        
                    });
        }
    }
}
