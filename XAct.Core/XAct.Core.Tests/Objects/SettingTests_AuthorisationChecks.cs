﻿namespace XAct.Settings.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class SettingTests_AuthorisationChecks
    {
        private const string EnvironmentIdentifier = "";
        private const string ZoneOrTierIdentifier = "Y";
        private const string HostIdentifier = "Z";
        private readonly Guid TierIdentifier = Guid.Empty;

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void SetAndGetSettingCanWriteIfEditableIsTrue()
        {
            Setting setting = new Setting("A", typeof(int), 3, null, null, null,
                                          new SettingEditingMetadata(
                                              //new ApplicationSettingScope(EnvironmentIdentifier, ZoneOrTierIdentifier, HostIdentifier, TierIdentifier),
                                              123,
                                              null, null, () => true));

            //Reset value to default:
            setting.Value = 5;

            Assert.AreEqual(5, setting.Value);
        }

        [Test]
        public void SetAndGetSettingCannotWriteIfEditableIsTrue()
        {
            Setting setting = new Setting("A", typeof(int), 3, null, null, null,
                                          new SettingEditingMetadata(
                                              //new ApplicationSettingScope(EnvironmentIdentifier, ZoneOrTierIdentifier, HostIdentifier, TierIdentifier),
                                              123,
                                              null, null, () => false));

            //Reset value to default:
            try
            {
                setting.Value = 5;
                Assert.IsTrue(false);
            }
            catch
            {
                Assert.IsTrue(true);
            }
        }

    
    }
}