﻿
namespace Tests.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using NUnit.Framework;
    using XAct;
    using XAct.Environment;
    using XAct.Extensions;
    using XAct.Messages;
    using XAct.Tests;

    [TestFixture]
    internal class ResponseGenericTests
    {

        [MessageCodeMetadata(null, 2)]
        private MessageCode _error = new MessageCode(123, Severity.Error);

        [MessageCodeMetadata(null, 2)]
        private MessageCode _blockingWarning = new MessageCode(123, Severity.BlockingWarning);

        [MessageCodeMetadata(null, 2)]
        private MessageCode _nonBlockingWarning = new MessageCode(123, Severity.NonBlockingWarning);

        [MessageCodeMetadata(null, 2)]
        private MessageCode _info = new MessageCode(123, Severity.Info);


        [MessageCodeMetadata("Blah{0},{1},{2}", 2)]
        private MessageCode _info2 = new MessageCode(123, Severity.Info);


        [MessageCodeMetadata("Blah{0},{1},{2}", 2)]
        private MessageCode _nonBlocking2 = new MessageCode(123, Severity.NonBlockingWarning);




        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            Singleton<IocContext>.Instance.ResetIoC();

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void CanAddMessages()
        {
            Response<string> response = new Response<string>();
            response.AddMessage(_blockingWarning,"argA","argB","argC");
            Assert.AreEqual(1, response.Messages.Count);
            Assert.AreEqual(Severity.BlockingWarning, response.Messages.First().MessageCode.Severity);
            Assert.AreEqual(3, response.Messages.First().Arguments.Length);
        }

        [Test]
        public void NoMessagesMeansSuccessIsTrue()
        {
            Response<string> response = new Response<string>();


            Assert.AreEqual(true, response.Success);

        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void AddingAnInfoMessageMakesSuccessTrue()
        {
            Response<string> response = new Response<string>();
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }


            Assert.AreEqual(true, response.Success);

        }
        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void AddingANonBlockingWarningMessageMakesSuccessTrue()
        {
            Response<string> response = new Response<string>();
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }

            Assert.AreEqual(true, response.Success);

        }


        [Test]
        public void AddingABlockingWarningMessageMakesSuccessFalse()
        {
            Response<string> response = new Response<string>();
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }
                response.AddMessage(_blockingWarning, "argA", "argB", "argC");


            Assert.AreEqual(false, response.Success);

        }



        [Test]
        public void AddingAnErrorMessageMakesSuccessFalse()
        {
            Response<string> response = new Response<string>();
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 10; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }
            response.AddMessage(_error, "argA", "argB", "argC");


            Assert.AreEqual(false, response.Success);

        }



        [Test]
        public void SerializingOfTrueSuccessWorks()
        {
            Response<string> response = new Response<string>();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 2; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }

            Assert.AreEqual(true, response.Success);

            string serializedText = response.GetType().ToDataContractString(response);

            Trace.WriteLine(serializedText);
            Response<string> response2 = serializedText.DeserializeFromDataContractSerializedString<Response<string>>();

            Assert.AreEqual(true, response2.Success);

        }

        [Test]
        public void SerializingOfFalseSuccessWorks()
        {
            Response<string> response = new Response<string>();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_blockingWarning, "argA", "argB", "argC");
            }
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_error, "argA", "argB", "argC");
            }

            Assert.AreEqual(false, response.Success);

            string serializedText = response.GetType().ToDataContractString(response);

            Trace.WriteLine(serializedText);

            Response<string> response2 = serializedText.DeserializeFromDataContractSerializedString<Response<string>>();
            //As Has NonBlocking and iNfo should be OK.
            Assert.AreEqual(false, response2.Success);

        }



        [Test]
        public void MassiveCheckForRandomWierdNess()
        {
            Random random = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random;
            for (int x = 0; x < 50000; x++)
            {
                int randomMaxPerCategory = 2;

                Response<string> response = new Response<string>();

                int iMaxInfo = random.Next(randomMaxPerCategory);
                for (int i = 0; i < iMaxInfo; i++)
                {
                    response.AddMessage(_info, "argA", "argB", "argC");
                }

                int iMaxNonBlockingWarning = random.Next(randomMaxPerCategory);
                for (int i = 0; i < iMaxNonBlockingWarning; i++)
                {
                    response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
                }

                int iMaxBlockingWarning = random.Next(randomMaxPerCategory);
                for (int i = 0; i < iMaxBlockingWarning; i++)
                {
                    response.AddMessage(_blockingWarning, "argA", "argB", "argC");
                }
                int iMaxError = random.Next(randomMaxPerCategory); 
                for (int i = 0; i < iMaxError; i++)
                {
                    response.AddMessage(_error, "argA", "argB", "argC");
                }

                int originalCount = response.Messages.Count;

                Severity[] _noSuccess = new[] {Severity.Error, Severity.BlockingWarning};
                bool directCheck = response.Messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

                bool origSUccess = response.Success;

                Assert.AreEqual(directCheck, origSUccess, "Failed #1");


                string serializedText = response.GetType().ToDataContractString(response);

                
                Response<string> response2 = serializedText.DeserializeFromDataContractSerializedString<Response<string>>();

                Assert.AreEqual(originalCount, response2.Messages.Count, "Not all messages were carried over!");
                Assert.AreEqual(origSUccess, response2.Success, "OrigSuccess!=response.Success");

                Assert.AreEqual(iMaxInfo, response2.Messages.Count(m => m.MessageCode.Severity == Severity.Info), "Info count are != not the same.");
                Assert.AreEqual(iMaxNonBlockingWarning,
                                response2.Messages.Count(m => m.MessageCode.Severity == Severity.NonBlockingWarning),"NonBlockingWarning != not the same count.");
                Assert.AreEqual(iMaxBlockingWarning,
                                response2.Messages.Count(m => m.MessageCode.Severity == Severity.BlockingWarning), "BlockingWarning != not the same count.");
                Assert.AreEqual(iMaxError, response2.Messages.Count(m => m.MessageCode.Severity == Severity.Error), "Error != not the same count.");


            }
        }


        [Test]
        public void MassiveCheckForRandomWierdNessWhenNoMessages()
        {
            Random random = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random;
            for (int x = 0; x < 50000; x++)
            {
                //int randomMaxPerCategory = 2;

                Response<string> response = new Response<string>();

                int originalCount = response.Messages.Count;

                Severity[] _noSuccess = new[] { Severity.Error, Severity.BlockingWarning };
                bool directCheck = response.Messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

                bool origSUccess = response.Success;

                Assert.AreEqual(directCheck, origSUccess, "Failed #1");


                string serializedText = response.GetType().ToDataContractString(response);


                Response<string> response2 = serializedText.DeserializeFromDataContractSerializedString<Response<string>>();

                Assert.AreEqual(originalCount, response2.Messages.Count);
                Assert.AreEqual(origSUccess, response2.Success);

            }
        }




        [Test]
        public void MassiveCheckForRandomWierdNessWhenNoMessagesUsingJSONSerializer()
        {
            Random random = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().Random;
            for (int x = 0; x < 50000; x++)
            {
                //int randomMaxPerCategory = 2;

                Response<string> response = new Response<string>();

                int originalCount = response.Messages.Count;

                Severity[] _noSuccess = new[] { Severity.Error, Severity.BlockingWarning };
                bool directCheck = response.Messages.All(m => !_noSuccess.Contains(m.MessageCode.Severity));

                bool origSUccess = response.Success;



                Assert.AreEqual(directCheck, origSUccess, "Failed #1");


                Assert.AreEqual(true, origSUccess, "Failed #2");

                string serializedText = response.ToJSON(Encoding.UTF8);


                Response<string> response2 = serializedText.DeserialiseFromJSON<Response<string>>(Encoding.UTF8);




                Assert.AreEqual(originalCount, response2.Messages.Count);
                Assert.AreEqual(origSUccess, response2.Success);

            }
        }








        [Test]
        public void JSONSerializingOfTrueSuccessWorks()
        {
            Response<string> response = new Response<string>();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 2; i++)
            {
                response.AddMessage(_nonBlockingWarning, "argA", "argB", "argC");
            }

            Assert.AreEqual(true, response.Success);

            string serializedText = response.ToJSON(response.GetType(), Encoding.UTF8);

            Trace.WriteLine(serializedText);
            Response<string> response2 = serializedText.DeserialiseFromJSON<Response<string>>(Encoding.UTF8);

            Assert.AreEqual(true, response2.Success);

        }

        [Test]
        public void JSONSerializingOfFalseSuccessWorks()
        {
            Response<string> response = new Response<string>();
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_info, "argA", "argB", "argC");
            }
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_blockingWarning, "argA", "argB", "argC");
            }
            for (int i = 0; i < 1; i++)
            {
                response.AddMessage(_error, "argA", "argB", "argC");
            }

            Assert.AreEqual(false, response.Success);

            string serializedText = response.ToJSON<Response<string>>(Encoding.UTF8);

            Trace.WriteLine(serializedText);

            Response<string> response2 = serializedText.DeserialiseFromJSON<Response<string>>(Encoding.UTF8);
            //As Has NonBlocking and iNfo should be OK.
            Assert.AreEqual(false, response2.Success);

        }





        //...Tests go here...
        [Test]
        public void voidMessaegCodeEqualityChecks_AreNotEqual()
        {

            MessageCode messageCode2 = new MessageCode(13, Severity.Info);

            Assert.AreNotEqual(_info, messageCode2);
        }
        //...Tests go here...
        [Test]
        public void MessaegCodeEqualityChecks_AreEqual()
        {

            MessageCode messageCode2 = new MessageCode(123, Severity.Info);

            Assert.AreEqual(_info, messageCode2);
        }

    }
}
