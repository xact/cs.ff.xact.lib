﻿namespace XAct.Events.Tests
{
    using System.Collections.Generic;

    public class StringMessage:IEvent
    {
        public List<string> Results { get { return _results ?? (_results = new List<string>()); } }
        private List<string> _results ;
        public string Message { get; set; }
    }
}