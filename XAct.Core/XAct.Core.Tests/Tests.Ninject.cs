﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Diagnostics;
    using XAct.Services;

    [TestFixture]
    public class TestsNinject
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                                    Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        public interface ISomeService: IHasXActLibService
        {
            int Score { get; }
            int Do();
        }
        public class SomeService : XActLibServiceBase, ISomeService
        {
            public SomeService(ITracingService tracingService):base(tracingService){}
            public int Score { get; set; }
            public int Do() { return ++Score; }
        }

        [Test]
        public void RegisterService()
        {
            //IKernel kernel = new StandardKernel();

            ////Register services one at a time:
            //kernel
            //    .Bind<ISomeService>()
            //    .To<SomeService>()
            //    .InThreadScope();

            //int r = kernel.Get<ISomeService>().Do();
            //Assert.AreEqual(1,r);
            //r = kernel.Get<ISomeService>().Do();
            //Assert.AreEqual(2, r);
            //r = kernel.Get<ISomeService>().Do();
            //Assert.AreEqual(3, r);
        }

        //[Test]
        //[ExpectedException(typeof(ArgumentException))]
        //public void DoubleRegisterService()
        //{
        //    //IKernel kernel = new StandardKernel();

        //    ////Register services one at a time:
        //    //kernel
        //    //    .Bind<ISomeService>()
        //    //    .To<SomeService>()
        //    //    .InThreadScope();

        //    ////Double Register:
        //    //kernel
        //    //    .Bind<ISomeService>()
        //    //    .To<SomeService>()
        //    //    .InThreadScope();


        //    //int r = kernel.Get<ISomeService>().Do();
        //    //Assert.AreEqual(1, r);
        //    //r = kernel.Get<ISomeService>().Do();
        //    //Assert.AreEqual(2, r);
        //    //r = kernel.Get<ISomeService>().Do();
        //    //Assert.AreEqual(3, r);
        //}


        [Test]
        public void RegisterByReflection()
        {


            //IKernel dependencyInjectionContainer = new StandardKernel();
            
            //RegisterServiceByReflection<ISomeService, SomeService>(dependencyInjectionContainer, BindingLifetimeType.SingletonPerWebRequestScope);

            //int r = dependencyInjectionContainer.Get<ISomeService>().Do();
            //Assert.AreEqual(1, r);
            //r = dependencyInjectionContainer.Get<ISomeService>().Do();
            //Assert.AreEqual(2, r);
            //r = dependencyInjectionContainer.Get<ISomeService>().Do();
            //Assert.AreEqual(3, r);
        }

        [Test]
        public void RegisterByReflectionAsSingleton()
        {


            //IKernel dependencyInjectionContainer = new StandardKernel();

            //RegisterServiceByReflection<ISomeService, SomeService>(dependencyInjectionContainer, BindingLifetimeType.SingletonScope);

            //int r = dependencyInjectionContainer.Get<ISomeService>().Do();
            //Assert.AreEqual(0, r);
            //r = dependencyInjectionContainer.Get<ISomeService>().Do();
            //Assert.AreEqual(1, r);
            //r = dependencyInjectionContainer.Get<ISomeService>().Do();
            //Assert.AreEqual(2, r);
        }

        private void RegisterServiceByReflection<TInterface, TInstance>(object dependencyInjectionContainer, BindingLifetimeType serviceLifetimeType)
        {
            string s = dependencyInjectionContainer.
            GetType().FullName;

    //        kernel
    //.Bind<ISomeService>()
    //.To<SomeService>()
    //.InSingletonScope();
            

            //IKernel k;
            //k.Bind();
            object o = dependencyInjectionContainer.InvokeMethod("Bind", new[] { typeof(Type) }, new[] { typeof(TInterface) });
            object o2 = o.InvokeMethod("To", new[] { typeof(Type) }, new[] { typeof(TInstance) });

            switch (serviceLifetimeType)
            {
                case BindingLifetimeType.SingletonPerWebRequestScope:
                    try
                    {
                        o2.InvokeMethod("InRequestScope", null);
                    }catch
                    {
                        o2.InvokeMethod("InTransientScope", null);
                    }
                    break;
                case BindingLifetimeType.TransientScope:
                    o2.InvokeMethod("InTransientScope", null);
                    break;
                case BindingLifetimeType.SingletonScope:
                    o2.InvokeMethod("InSingletonScope", null);
                    break;
                case BindingLifetimeType.SingletonPerThreadScope:
                    o2.InvokeMethod("InThreadScope", null);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("serviceLifetimeType");
            }


        }



    }

}

