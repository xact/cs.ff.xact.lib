﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    using NUnit.Framework;
    using XAct;
    using XAct.Net;
    using XAct.Tests;

    [TestFixture]
    public class TimeoutWebClientTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void MakeRequest()
        {
            var webClient = new TimeoutWebClient();
            var result = webClient.DownloadString("http://google.com");

            Assert.That(result,Is.Not.Null);
        }
        [Test]
        [ExpectedException(typeof(System.Net.WebException))]
        public void MakeTimeoutRequest_TooShort()
        {
            var webClient = new TimeoutWebClient(TimeSpan.FromMilliseconds(10));
            var result = webClient.DownloadString("http://google.com");

        }
        [Test]
        public void MakeTimeoutRequest_Long_Enough()
        {
            var webClient = new TimeoutWebClient(TimeSpan.FromMilliseconds(1000));
            var result = webClient.DownloadString("http://google.com");

            Assert.That(result, Is.Not.Null);
        }
    }
}
