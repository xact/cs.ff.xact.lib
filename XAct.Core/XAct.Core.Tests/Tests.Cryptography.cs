﻿namespace XAct.Tests
{
    using System;
    using System.Text;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Security.Cryptography;

    [TestFixture]
    public class MyTestFixture2
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                                    Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void EncryptTDES()
        {
            SymmetricAlgorithms symmetricAlgorithms = new SymmetricAlgorithms();
            symmetricAlgorithms.Initialize("test");

            string originalText;
            originalText =
                @"L'Espagnol Carlos Sastre (Team CSC) s'est imposé mercredi dans la 17e étape du Tour de France entre Embrun et L'Alpe d'Huez (210,5 km) et a endossé le maillot jaune. 
            Samuel Sanchez (Euskaltel) finit deuxième de l'étape avec 2'03"" de retard et Andy Schleck (Team CSC) termine troisième 
            dans le même temps. 
            Au classement général, Carlos Sastre possède 1'24"" d'avance sur son équipier Frank Schleck, 1'33"" sur Bernhard Kohl 
            (Gerolsteiner) et 1'34"" sur Cadel Evans (Silence-Lotto), sixième de l'étape.";

            //originalText = @"A very simple string.";

            string encryptedText = symmetricAlgorithms.Encrypt(originalText);
            Console.WriteLine(encryptedText);

            string decryptedText =
                symmetricAlgorithms.Decrypt(encryptedText);


            Assert.AreEqual(decryptedText, originalText);
        }

        [Test]
        public void Test_MD5_All_Possibilities()
        {
            Encoding[] encoders =
                new[]
                    {
                        Encoding.ASCII, Encoding.UTF7, Encoding.UTF8, Encoding.UTF32, Encoding.Unicode,
                        Encoding.BigEndianUnicode, Encoding.Default
                    };
            string[] texts = new[] {"hello", "La Defense", "La Défense"};

            foreach (Encoding encoding in encoders)
            {
                Console.WriteLine("---");
                foreach (string text in texts)
                {
                    string result = text.CalculateHash(encoding, "MD5");
                    string s = "{0} [{1}, {2}]".FormatStringInvariantCulture(result, text, encoding);
                    Console.WriteLine(s);
                }
            }

/*
---
5D41402ABC4B2A76B9719D911017C592 [hello, System.Text.ASCIIEncoding]
5FA39A733B79D60627E00A62AEEBE8A3 [La Defense, System.Text.ASCIIEncoding]
ACBA3AD79835A2ED875F7BDA6746FD5C [La Défense, System.Text.ASCIIEncoding]
---
5D41402ABC4B2A76B9719D911017C592 [hello, System.Text.UTF7Encoding]
5FA39A733B79D60627E00A62AEEBE8A3 [La Defense, System.Text.UTF7Encoding]
7AA3E135D5F8CA0EA7D2D8B895F0A186 [La Défense, System.Text.UTF7Encoding]
---
5D41402ABC4B2A76B9719D911017C592 [hello, System.Text.UTF8Encoding]
5FA39A733B79D60627E00A62AEEBE8A3 [La Defense, System.Text.UTF8Encoding]
FDC1F64520302DD82236C68DFC0362DA [La Défense, System.Text.UTF8Encoding]
---
A6F145A01AD0127E555C051D15806EB5 [hello, System.Text.UTF32Encoding]
A36FD23797FB8D648CD2B2125064D13D [La Defense, System.Text.UTF32Encoding]
C94618A06189EB82E53C05A5D3604FA8 [La Défense, System.Text.UTF32Encoding]
---
FD186DD49A16B1BF2BD2F44E495E14C9 [hello, System.Text.UnicodeEncoding]
A08B28403340D4383A4646EB5D8AF519 [La Defense, System.Text.UnicodeEncoding]
8E9D8AF595C80B5E2907CD4D3005BCEB [La Défense, System.Text.UnicodeEncoding]
---
A009BCCF13CA2631D3982CD37FBDCD8B [hello, System.Text.UnicodeEncoding]
03C19AC0D976518CD4ECCA6F1F7F6B38 [La Defense, System.Text.UnicodeEncoding]
C966F7AC3B780D6F19021EF62A11D167 [La Défense, System.Text.UnicodeEncoding]
---
5D41402ABC4B2A76B9719D911017C592 [hello, System.Text.SBCSCodePageEncoding]
5FA39A733B79D60627E00A62AEEBE8A3 [La Defense, System.Text.SBCSCodePageEncoding]
2D445DB41DA3DC087B35CA16F6FF8A45 [La Défense, System.Text.SBCSCodePageEncoding]
             */
        }

        //...Tests go here...
        [Test]
        public void Test_Sha1()
        {
            string originalText;
            originalText =
                @"L'Espagnol Carlos Sastre (Team CSC) s'est imposé mercredi dans la 17e étape du Tour de France entre Embrun et L'Alpe d'Huez (210,5 km) et a endossé le maillot jaune. 
            Samuel Sanchez (Euskaltel) finit deuxième de l'étape avec 2'03"" de retard et Andy Schleck (Team CSC) termine troisième 
            dans le même temps. 
            Au classement général, Carlos Sastre possède 1'24"" d'avance sur son équipier Frank Schleck, 1'33"" sur Bernhard Kohl 
            (Gerolsteiner) et 1'34"" sur Cadel Evans (Silence-Lotto), sixième de l'étape.";

            originalText =
                @"A very simple string.";

            string hash1 = originalText.CalculateHash(Encoding.Default, "SHA1");

            Console.WriteLine(hash1);

            string hash2 = originalText.CalculateHash(Encoding.Default, "SHA1");


            Assert.AreEqual(hash1, hash2);
        }


        //...Tests go here...


        ////...Tests go here...
        //[Test]
        //public void DecryptTDES() {
        //    string s = Env.GetMotherboardSN();
        //    Console.WriteLine(string.Format("Motherboard SN: {0}", s));
        //    //I got '.DR4FQ1J.CN3652156R013F.'

        //    //The first part I recoginze as a the dell service tag:

        //    Assert.IsTrue(s.Length > 0); //whatever test you want...
        //}
    }


}


