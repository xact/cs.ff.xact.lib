﻿
namespace Tests.Tests.ExtensionMethods
{
    using System.Collections.Generic;
    using System.Reflection;
    using NUnit.Framework;
    using System;
    using System.Linq;
    using XAct;
    using XAct.Collections;
    using XAct.Reflection;
    using XAct.Tests;

    [TestFixture]
    public class TypeExtensionTests
    {
        #region Test Data
        public interface IBar
        {
        }
        public interface IFoo : IBar
        {
        }
        public abstract class AbstractFoo : IFoo
        {
        }
        public class Foo : AbstractFoo
        {
        }
        #endregion


        #region Test Data 
        public interface IA1
        {
            
        }
        public interface IA2
        {
            
        }
        public interface IB1 :IA1
        {
            
        }
        public interface IB2 :IA1, IA2
        {
            
        }
        public abstract class AbsA : IB2 {}

        public abstract class AbsB :AbsA, IB1, IA1{}

        public class Yay :AbsB, IA2, IB1{}
        #endregion

        #region TestData


        #region
        public interface IDemoClass
        {
            int PublicProp { get; set; }
            void PublicMethod();
            List<int>[] PublicMethod(Dictionary<int, string> stuff);
        }

        public abstract class DemoClassBase : IDemoClass
        {
            public abstract int PublicProp { get; set; }
            public abstract void PublicMethod();

            public abstract List<int>[] PublicMethod(Dictionary<int, string> stuff);
        }


        public class DemoClass : DemoClassBase
        {
            public DemoClass() { }
            protected DemoClass(int i) { }
            internal DemoClass(int i, string s, char c) { }



            override public int PublicProp { get; set; }
            internal string InternalProp { get; private set; }
            protected char ProtectedProp { private get; set; }
            private char PrivateProp { get; set; }

            override public void PublicMethod() { }
            override public List<int>[] PublicMethod(Dictionary<int, string> stuff) { return null; }
            internal int InternalMethod<T>(IList<Dictionary<int, string>> x) { return 0; }
            protected string ProtectedMethod<T>(string first, int age) { return "."; }
            private string[] PrivateMethod<T>(string first, int age) { return new[] { "A", "B" }; }
        }

        #endregion
        #endregion


        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Get_Interface_Inheritence_Chain_2()
        {
            var t = typeof (IBar);
            //Use Extension Methods:
            //IBar has no anscesotrs, so count=0
            var results = t.GetImmediateInterfaces();
            Assert.IsTrue(results.Count() == 0);

            t = typeof (IFoo);
            //Use Extension Methods:
            //IFoo comes frmo IBar, so count = 1
            results = t.GetImmediateInterfaces();
            Assert.IsTrue(results.Count()>0);
        }


        [Test]
        public void Get_Immediate_Interfaces()
        {
            var t = typeof (Foo);
            //Use Extension Methods:
            var results = t.GetImmediateInterfaces();
            Assert.IsTrue(results.Count() == 1);
            Assert.IsTrue(results.First()  == typeof(IFoo));

        }

        [Test]
        public void Check_BaseClass_Inheritence_Chain()
        {
            var t = typeof (Foo);
            int counter = 0;
            while (t != null)
            {
                Console.WriteLine(t);
                t = t.BaseType;
                counter++;
            }
            Assert.IsTrue(counter>0);
        }

        [Test]
        public void Get_Interface_Inheritence_TreeChain()
        {
            //Interesting:
            //Even though Foo does not implement IFoo directly (only AbstractFoo does)
            //it still picks it up (which is not  really good):
            var t = typeof (Foo);

            TreeNode<Type>[] results = t.GetInterfaceInheritence();


            var counter = 0;
            foreach (TreeNode<Type> r in results)
            {
                foreach (var r2 in r.SelfAndDescendants)
                {
                    counter++; Console.WriteLine(r2.Value); }
            }
            Assert.AreEqual(1, results.Count());//Only one base class (IFoo)
            Assert.IsTrue(counter > 0);
        }

        [Test]
        public void Get_ClassAndInterfaceInheritence_TreeChain()
        {
            var t = typeof (Foo);

            TreeNode<NodeTypeWrapper> result = t.GetSubclassesAndInterfaces();

            foreach (var n in result.SelfAndDescendants)
            {
                Console.WriteLine("-".Repeat(n.Level) + n.Value);
            }
            Assert.AreEqual(6,result.SelfAndDescendants.Count());
            Assert.AreEqual(typeof(Foo), result.SelfAndDescendants.First().Value.Type);
            Assert.AreEqual(typeof(IBar), result.SelfAndDescendants.Last().Value.Type);



            foreach (var r2 in result.SelfAndDescendants)
            {
                
                Console.WriteLine("-".Repeat(r2.Level) +  r2.Value);
            }
        }

        [Test]
        public void Get_Complex_ClassAndInterfaceInheritence_TreeChain()
        {
            var t = typeof(Yay);

            TreeNode<NodeTypeWrapper> result = t.GetSubclassesAndInterfaces();

            foreach (var n in result.SelfAndDescendants)
            {

                Console.WriteLine("-".Repeat(n.Level) + n.Value.Type);
            }

            //Assert.AreEqual(10,result.SelfAndDescendants.Count());
            Assert.AreEqual(typeof(Yay), result.SelfAndDescendants.First().Value.Type);
            Assert.AreEqual(typeof(AbsB), result.SelfAndDescendants.ElementAt(1).Value.Type);
            Assert.AreEqual(typeof(IA1), result.SelfAndDescendants.Last().Value.Type);



            foreach (var r2 in result.SelfAndDescendants)
            {
                
                Console.WriteLine("-".Repeat(r2.Level) +  r2.Value);
            }
        }

        [Test]
        public void Get_Complex_ClassAndInterfaceInheritence_TreeChain_Trimming_Excess()
        {
            var t = typeof(Yay);

            TreeNode<NodeTypeWrapper> result = t.GetSubclassesAndInterfaces(true);

            foreach (var n in result.SelfAndDescendants)
            {

                Console.WriteLine("-".Repeat(n.Level) + n.Value);
            }

            //Assert.AreEqual(10,result.SelfAndDescendants.Count());
            Assert.AreEqual(typeof(Yay), result.SelfAndDescendants.First().Value.Type);
            Assert.AreEqual(typeof(AbsB), result.SelfAndDescendants.ElementAt(1).Value.Type);
            Assert.AreEqual(typeof(IA1), result.SelfAndDescendants.Last().Value.Type);



            foreach (var r2 in result.SelfAndDescendants)
            {

                Console.WriteLine("-".Repeat(r2.Level) + r2.Value.Type.Name);
            }
        }

        [Test]
        public void GetConstructorWithMostArguements()
        {

            var t = typeof(DemoClass);

            var constructors =
                t.GetConstructors(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            ConstructorInfo constructorInfo =
                constructors.Aggregate((i1, i2) => i1.GetParameters().Count() > i2.GetParameters().Count() ? i1 : i2);


            Assert.AreEqual(3, constructorInfo.GetParameters().Count());
        }

    }

}
