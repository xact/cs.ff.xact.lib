namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    /// NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class ObjectExtensions_ConvertTo_Tests
    {
        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void UnitTest01()
        {
            //XAct.Data.AClassName ctrl = new XAct.Data.AClassName();

            Assert.AreEqual(3.ConvertTo<string>(), "3");
        }


    //conversionService.Convert("a9e20bcd-c291-42f0-bd67-4305a05614db",typeof(Guid)).Dump();
    //conversionService.Convert("a9e20bcdc29142f0bd674305a05614db",typeof(Guid)).Dump();
    //conversionService.Convert("3",typeof(int)).Dump();
    //conversionService.Convert("-3",typeof(int)).Dump();
    //conversionService.Convert("3.2",typeof(float)).Dump();
    //conversionService.Convert("true",typeof(bool)).Dump();
    //conversionService.Convert("true",typeof(bool)).Dump();
    //    conversionService.Convert("True",typeof(bool)).Dump();
    //conversionService.Convert("false",typeof(bool)).Dump();
    //    conversionService.Convert("false",typeof(bool)).Dump();
    //conversionService.Convert((int)3,typeof(float)).Dump();



        [Test]
        public void ATest_BoolToString1()
        {
            string result = true.ToString();
            Assert.AreEqual(result, "True");
        }

        [Test]
        public void ATest_StringToBool()
        {
            string[] tests = new[]
                                 {
                                     "true",
                                     "True",
                                     "vrai",
                                     "1",
                                     "One",
                                     "Vero",
                                     "si",
                                     "Vrai",
                                     "One",
                                     "OK",
                                     "Yes"
                                 };
            bool result = true;
            foreach (string test in tests)
            {
                result &= test.ToBool();
            }
            Assert.IsTrue(result);
        }

        [Test]
        public void ATest_StringToBool2()
        {
            string[] tests = new[]
                                 {
                                     "false",
                                     "faux",
                                     "0",
                                     "Non",
                                     "No",
                                     "Nein",
                                     "niche",
                                 };
            bool result = true;
            foreach (string test in tests)
            {
                result &= !test.ToBool();
            }
            Assert.IsTrue(result);
        }


        [Test]
        public void ConvertDoubleToInt()
        {
            Assert.AreEqual(3, 3.2.ConvertTo<int>());
        }

        [Test]
        public void ConvertDoubleToIntRoundingUp()
        {
            Assert.AreEqual(4, 3.7.ConvertTo<int>());
        }

        [Test]
        public void ConvertDoubleToIntRoundingDownFromPointFive()
        {
            Assert.AreEqual(4, 3.5.ConvertTo<int>());
        }

        [Test]
        public void ConvertDoubleToIntRoundingDownFromPointFour()
        {
            Assert.AreEqual(3, 3.4.ConvertTo<int>());
        }

        [Test]
        public void ConvertDoubleToString()
        {
            Assert.AreEqual("3.4", 3.4.ConvertTo<string>());
        }

        [Test]
        public void ConvertGuidToString()
        {
            Guid guid = Guid.NewGuid();
            string s = guid.ToString();

            Assert.AreEqual(s, guid.ConvertTo<string>());
        }

        [Test]
        public void ConvertStringToGuid()
        {
            Guid guid = Guid.NewGuid();
            string s = guid.ToString();

            Assert.AreEqual(guid, s.ConvertTo<Guid>());
        }

        [Test]
        public void ConvertStringToDouble()
        {

            Assert.AreEqual(3.31, "3.31".ConvertTo<double>());
        }

        [Test]
        public void ConvertStringToFloat()
        {

            Assert.AreEqual(3.31f, "3.31".ConvertTo<float>());
        }

	 
    }


}


