﻿namespace Tests.Tests.ExtensionMethods
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Diagnostics.Performance;
    using XAct.Tests;



    public class MethodInfoExtensionsTests
    {
        public interface IX
        {
            [PerformanceCounter("Group1", "NameA", Type = PerformanceCounterUpdateType.Increment)]
            [PerformanceCounter("Group1", "NameB", Type = PerformanceCounterUpdateType.Increment)]
            void Foo();
        }

        public class X : IX
        {
            public void Foo() { }    
        }

        public class Y : IX
        {
            [PerformanceCounter("Group1", "NameA", Type = PerformanceCounterUpdateType.Increment)]
            [PerformanceCounter("Group1", "NameB", Type = PerformanceCounterUpdateType.Increment)]
            public void Foo() { }
        }


        [TestFixture]
        public class MethodInfoExtensions_Tests
        {
            [SetUp]
            public void MyTestSetup()
            {
                //Run before every test:
                //Get hold of the IoCContext Singleton and reset it before the next test.
                Singleton<IocContext>.Instance.ResetIoC();
            }
            [TearDown]
            public void MyTestTearDown()
            {
                GC.Collect();
            }


            [Test]
            public void GetAttributes()
            {
                X x = new X();
                var attributes =
                    x.GetType().GetMethod("Foo").GetAttributesRecursively(typeof (PerformanceCounterAttribute), true);

                Assert.IsTrue(attributes.Length > 0);

            }
            [Test]
            public void GetAttributes2()
            {
                Y x = new Y();
                var attributes =
                    x.GetType().GetMethod("Foo").GetAttributesRecursively(typeof(PerformanceCounterAttribute), true);

                Assert.IsTrue(attributes.Length > 0);

            }
        }
    }
}