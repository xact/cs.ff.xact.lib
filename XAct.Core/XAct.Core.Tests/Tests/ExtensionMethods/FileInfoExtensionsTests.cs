﻿namespace XAct.Tests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;

    [TestFixture]
    public class FileInfoExtensionsTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                                    Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        //...Tests go here...
        [Test]
        public void PrefixFileName()
        {

            FileInfo fi = new FileInfo(@"C:\SomeDir\SomeFile.htm");
            string r = fi.PrefixFileName("_locked_");

            Assert.AreEqual(@"C:\SomeDir\_locked_SomeFile.htm",r);

        }

        [Test]
        public void SuffixFileName()
        {
            FileInfo fi = new FileInfo(@"C:\SomeDir\SomeFile.htm");
            string r = fi.SuffixFileName("_locked_");

            Assert.AreEqual(@"C:\SomeDir\SomeFile_locked_.htm",r);
        }


        [Test]
        public void PrefixFileName2()
        {

            FileInfo fi = new FileInfo(@"SomeFile.htm");
            string r = fi.PrefixFileName("_locked_");

            //Note that FileInfo tacks on File if missing:
            Assert.AreEqual(Path.Combine(XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().ApplicationBasePath,"_locked_SomeFile.htm"), r);

        }

        [Test]
        public void SuffixFileName2()
        {
            FileInfo fi = new FileInfo(@"SomeFile.htm");
            string r = fi.SuffixFileName("_locked_");

            //Note that FileInfo tacks on File if missing:
            Assert.AreEqual(Path.Combine(XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>().ApplicationBasePath, "SomeFile_locked_.htm"), r);
        }
    }


}


