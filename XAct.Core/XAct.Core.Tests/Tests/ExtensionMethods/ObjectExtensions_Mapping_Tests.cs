﻿namespace XAct.Data.Tests.ForNUnit
{
    using System;
    using NUnit.Framework;
    using XAct.Tests;

    /// <summary>
    /// NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class ObjectExtensions_Mapping_Tests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void MappingOfScalarPropertiesWithNoNullables()
        {
            X1 x1 = new X1 {Id = 123, FirstName = "John", DOB = DateTime.Now, Y = new Y1 {Text = "Yeah"}};

            X1 x2 = new X1();

            x1.MapPropertyValues(x2);
            Assert.AreEqual(123, x2.Id);
            Assert.AreEqual("John", x2.FirstName);
            Assert.AreEqual(DateTime.Today, x2.DOB.Date);

        }

        [Test]
        public void MappingOfScalarPropertiesWithNullables()
        {
            X2 x1 = new X2 { Id = 123, FirstName = "John", DOB = DateTime.Now, DOD=DateTime.Now, Y = new Y1 { Text = "Yeah" } };

            X2 x2 = new X2();

            x1.MapPropertyValues(x2);
            Assert.AreEqual(123, x2.Id);
            Assert.AreEqual("John", x2.FirstName);
            Assert.AreEqual(DateTime.Today, x2.DOD.Value.Date);
            Assert.AreEqual(DateTime.Today, x2.DOB.Date);

        }


        public class X2 :X1
        {
            public DateTime? DOD { get; set; } 
        }

        public class X1
        {
            public int Id { get; set; }
            public string FirstName { get; set; }
            public DateTime DOB { get; set; }
            public Y1 Y { get; set; }
        }
        public class Y1
        {
            public string Text { get; set; }
        }
    }

}