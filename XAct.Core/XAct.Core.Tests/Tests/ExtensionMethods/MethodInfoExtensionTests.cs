﻿namespace Tests.Objects
{
    using System;
    using System.Reflection;
    using NUnit.Framework;
    using XAct;
    using XAct.Tests;

    [TestFixture]
    public class MethodInfoExtensionTests
    {
#region Test Data 
public abstract class Foo {

    public bool BarPublicProperty {get;set;}
    internal bool BarInternalProperty { get; set; }
    protected bool BarProtectedProperty { get; set; }
    private bool BarPrivateProperty { get; set; }


  public bool BarPublicMethod() { return true; }
  internal bool BarInternalMethod() { return true; }
  protected bool BarProtectedMethod() { return true; }
  private bool BarPrivateMethod() { return true; }
  public abstract bool BarAbstractMethod();
  internal abstract bool BarInternalAbstractMethod();
}
#endregion
        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void Is_MethodInfo_Public()
        {
            var t = typeof(Foo);
            var mi = t.GetMethod("BarPublicMethod", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(true, mi.IsPublic());
            Assert.AreEqual(false, mi.IsInternal());
            Assert.AreEqual(false, mi.IsProtected());
            Assert.AreEqual(false, mi.IsPrivate());
            Assert.AreEqual(false, mi.IsAbstract());
        }


        [Test]
        public void Is_MethodInfo_Internal()
        {
            var t = typeof(Foo);
            var mi = t.GetMethod("BarInternalMethod", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(false, mi.IsPublic());
            Assert.AreEqual(true, mi.IsInternal());
            Assert.AreEqual(false, mi.IsProtected());
            Assert.AreEqual(false, mi.IsPrivate());
            Assert.AreEqual(false, mi.IsAbstract());
        }


        [Test]
        public void Is_MethodInfo_Protected()
        {
            var t = typeof(Foo);
            var mi = t.GetMethod("BarProtectedMethod", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(false, mi.IsPublic());
            Assert.AreEqual(false, mi.IsInternal());
            Assert.AreEqual(true, mi.IsProtected());
            Assert.AreEqual(false, mi.IsPrivate());
            Assert.AreEqual(false, mi.IsAbstract());
        }


        [Test]
        public void Is_MethodInfo_Private()
        {
            var t = typeof(Foo);
            var mi = t.GetMethod("BarPrivateMethod", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(false, mi.IsPublic());
            Assert.AreEqual(false, mi.IsInternal());
            Assert.AreEqual(false, mi.IsProtected());
            Assert.AreEqual(true, mi.IsPrivate());
            Assert.AreEqual(false, mi.IsAbstract());
        }


        [Test]
        public void Is_MethodInfo_Abstract()
        {
            var t = typeof(Foo);
            var mi = t.GetMethod("BarAbstractMethod", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(true, mi.IsPublic());
            Assert.AreEqual(false, mi.IsInternal());
            Assert.AreEqual(false, mi.IsProtected());
            Assert.AreEqual(false, mi.IsPrivate());
            Assert.AreEqual(true, mi.IsAbstract());
        }

        [Test]
        public void Is_MethodInfo_InternalAbstract()
        {
            var t = typeof(Foo);
            var mi = t.GetMethod("BarInternalAbstractMethod", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(false, mi.IsPublic());
            Assert.AreEqual(true, mi.IsInternal());
            Assert.AreEqual(false, mi.IsProtected());
            Assert.AreEqual(false, mi.IsPrivate());
            Assert.AreEqual(true, mi.IsAbstract());
        }


    }
}
