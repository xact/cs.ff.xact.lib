﻿namespace Tests.Objects
{
    using System;
    using System.Reflection;
    using NUnit.Framework;
    using XAct;
    using XAct.Tests;

    [TestFixture]
    public class PropertyInfoExtensionTests
    {
#region Test Data 
public abstract class Foo {

    public bool BarPublicProperty {get;set;}
    internal bool BarInternalProperty { get; set; }
    protected bool BarProtectedProperty { get; set; }
    private bool BarPrivateProperty { get; set; }
    public abstract bool BarAbstractProperty { get; set; }
    internal abstract bool BarInternalAbstractProperty { get; set; }
}
#endregion
        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void Is_PropertyInfo_Public()
        {
            var t = typeof(Foo);
            var pi = t.GetProperty("BarPublicProperty", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(true, pi.IsGetterPublic());
            Assert.AreEqual(false, pi.IsGetterInternal());
            Assert.AreEqual(false, pi.IsGetterProtected());
            Assert.AreEqual(false, pi.IsGetterPrivate());
            Assert.AreEqual(false, pi.IsGetterAbstract());
        }


        [Test]
        public void Is_PropertyInfo_Internal()
        {
            var t = typeof(Foo);
            var pi = t.GetProperty("BarInternalProperty", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(false, pi.IsGetterPublic());
            Assert.AreEqual(true, pi.IsGetterInternal());
            Assert.AreEqual(false, pi.IsGetterProtected());
            Assert.AreEqual(false, pi.IsGetterPrivate());
            Assert.AreEqual(false, pi.IsGetterAbstract());
        }


        [Test]
        public void Is_PropertyInfo_Protected()
        {
            var t = typeof(Foo);
            var pi = t.GetProperty("BarProtectedProperty", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(false, pi.IsGetterPublic());
            Assert.AreEqual(false, pi.IsGetterInternal());
            Assert.AreEqual(true, pi.IsGetterProtected());
            Assert.AreEqual(false, pi.IsGetterPrivate());
            Assert.AreEqual(false, pi.IsGetterAbstract());
        }


        [Test]
        public void Is_PropertyInfo_Private()
        {
            var t = typeof(Foo);
            var pi = t.GetProperty("BarPrivateProperty", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(false, pi.IsGetterPublic());
            Assert.AreEqual(false, pi.IsGetterInternal());
            Assert.AreEqual(false, pi.IsGetterProtected());
            Assert.AreEqual(true, pi.IsGetterPrivate());
            Assert.AreEqual(false, pi.IsGetterAbstract());
        }


        [Test]
        public void Is_PropertyInfo_Abstract()
        {
            var t = typeof(Foo);
            var pi = t.GetProperty("BarAbstractProperty", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(true, pi.IsGetterPublic());
            Assert.AreEqual(false, pi.IsGetterInternal());
            Assert.AreEqual(false, pi.IsGetterProtected());
            Assert.AreEqual(false, pi.IsGetterPrivate());
            Assert.AreEqual(true, pi.IsGetterAbstract());
        }

        [Test]
        public void Is_PropertyInfo_InternalAbstract()
        {
            var t = typeof(Foo);
            var pi = t.GetProperty("BarInternalAbstractProperty", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.AreEqual(false, pi.IsGetterPublic());
            Assert.AreEqual(true, pi.IsGetterInternal());
            Assert.AreEqual(false, pi.IsGetterProtected());
            Assert.AreEqual(false, pi.IsGetterPrivate());
            Assert.AreEqual(true, pi.IsGetterAbstract());
        }


    }
}
