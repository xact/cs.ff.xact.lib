﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Tests.ExtensionMethods
{
    using NUnit.Framework;
    using XAct;
    using XAct.Tests;

    [TestFixture]
    public class StringExtensionTests
    {
        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void JoinSafely()
        {
            var result = new[] {"A", "bB'", "C"}.JoinSafely("|");

            Assert.AreEqual("A|bB'|C",result);
        }
        [Test]
        public void JoinSafely_With_Null()
        {
            var result = new[] { "A", null, "C" }.JoinSafely("|");

            Assert.AreEqual("A||C", result);
        }
    }
}
