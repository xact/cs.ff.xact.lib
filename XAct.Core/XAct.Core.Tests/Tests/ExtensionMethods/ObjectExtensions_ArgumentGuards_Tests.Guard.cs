﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;

    [TestFixture]
    public class ObjectExtensions_ArgumentGuards_Tests
    {
        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void TestArgumentNotNull()
        {
            object arg = 1;
            if (arg.IsNull())
            {
                throw new ArgumentNullException("arg");
            }
        }

        [Test]
        public void TestCheckAndTrimStringArgument()
        {
            string arg = "Some very long arg that is longer than 12 ";
            string result = arg.Truncate(12, true);
            Assert.IsTrue(result.Length == 12);
        }
    }


}


