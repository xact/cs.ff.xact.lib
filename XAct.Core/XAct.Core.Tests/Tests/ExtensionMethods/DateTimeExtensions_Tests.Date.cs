﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class DateTimeExtensions_Tests
    {
        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void DateMonthStart()
        {

            DateTime dateTime = new DateTime(2000, 3, 3);
            DateTime result = dateTime.MonthStart();
            DateTime expected = new DateTime(2000, 3, 1);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void DateMonthEnd()
        {

            DateTime dateTime = new DateTime(2000, 3, 3);
            DateTime result = dateTime.MonthEnd();
            DateTime expected = new DateTime(2000, 3, 31);
            Assert.AreEqual(expected,result);
        }

        [Test]
        public void DateQuarterStart()
        {
            DateTime dateTime = new DateTime(2000, 3, 3);
            DateTime result = dateTime.QuarterStart();
            DateTime expected = new DateTime(2000, 1, 1);
            Assert.AreEqual(expected,result);
        }

        [Test]
        public void DateQuarterStart2()
        {
            DateTime dateTime = new DateTime(2000, 6, 3);
            DateTime result = dateTime.QuarterStart();
            DateTime expected = new DateTime(2000, 4, 1);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void DateQuarterStart3()
        {
            DateTime dateTime = new DateTime(2000, 11, 3);
            DateTime result = dateTime.QuarterStart();
            DateTime expected = new DateTime(2000, 10, 1);
            Assert.AreEqual(expected, result);
        }




        [Test]
        public void DateQuarterEnd()
        {
            DateTime dateTime = new DateTime(2000, 1, 3);
            DateTime result = dateTime.QuarterEnd();
            DateTime expected = new DateTime(2000, 3, 31);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void DateQuarterEnd2()
        {
            DateTime dateTime = new DateTime(2000, 6, 3);
            DateTime result = dateTime.QuarterEnd();
            DateTime expected = new DateTime(2000, 6, 30);
            Assert.AreEqual(expected, result);
        }
        
        [Test]
        public void DateQuarterEnd3()
        {
            DateTime dateTime = new DateTime(2000, 11, 3);
            DateTime result = dateTime.QuarterEnd();
            DateTime expected = new DateTime(2000, 12, 31);
            Assert.AreEqual(expected, result);
        }





        [Test]
        public void DateYearStart()
        {
            DateTime dateTime = new DateTime(2000, 3, 3);
            DateTime result = dateTime.YearStart();
            DateTime expected = new DateTime(2000, 1, 1);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void DateYearEnd()
        {
            DateTime dateTime = new DateTime(2000, 3, 3);
            DateTime result = dateTime.YearEnd();
            DateTime expected = new DateTime(2001, 1, 1).AddDays(-1);
            Assert.AreEqual(expected, result);
        }

    }
}
