﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;

    [TestFixture]
    public class ILIstExtensionsTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //                        Singleton<IocContext>.Instance.ResetIoC();
        }


        [Test]
        public void ATest_RemoveAll()
        {
            List<X> list1 = new List<X> { new X { Name = "A" }, new X { Name = "B" }, new X { Name = "C" }, new X { Name = "D" } };

            List<X> list2 = new List<X> { new X { Name = "A" }, new X { Name = "D" } };


            X[] removed;

            list1.RemoveAll(list2, o => o.Name, out removed);

            Assert.AreEqual(2, removed.Length);
            Assert.AreEqual("A", removed[0].Name);
            Assert.AreEqual("D", removed[1].Name);
        }

        public class X
        {
            public string Name { get; set; }
        }


        [Test]
        public void ListIndexOf()
        {
            List<string> list = new List<string> {"A", "B", "", "","C", "", ""};

            int index = list.IndexOf(s => s.IsNullOrEmpty());

            Assert.AreEqual(2,index);

        }

        [Test]
        public void ListLastIndexOf()
        {
            List<string> list = new List<string> { "A", "B", "", "", "C", "", "" };

            int index = list.LastIndexOf(s => s.IsNullOrEmpty());

            Assert.AreEqual(5, index);

        }

    }


}


