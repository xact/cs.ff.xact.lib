﻿
namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using XAct;
    using System.Collections.ObjectModel;
    using NUnit.Framework;

    [TestFixture]
    public class CollectionExtentionsTests
    {


        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        [Test]
        public void RemoveAllFromCollectionThatMatchPredicate()
        {
            Collection<DbRecord> dbRecords = FakeLoadOriginalRecordsFromRepository();

            //Remove from collection all items that have an empty host name:
           
            dbRecords.RemoveAll(s => s.HostName.IsNullOrEmpty());

            //Should have removed 3 or so, leaving only entries that have a hostname:

            //If any remaining that have a hostname, that's an error:
            Assert.IsFalse(dbRecords.Any(s => s.HostName.IsNullOrEmpty()));
        }



        [Test]
        public void RemoveAllFromCollectionThatMatchItemsInFilterList()
        {
            Collection<DbRecord> dbRecords = FakeLoadOriginalRecordsFromRepository();

            Collection<DbRecord> toRemove = new Collection<DbRecord>();
            toRemove.Add(new DbRecord {ApplicationName = "App1", HostName = string.Empty, Key = "KeyA", Value = "123"});
            toRemove.Add(new DbRecord {ApplicationName = "App1", HostName = string.Empty, Key = "KeyB", Value = "323"});
            toRemove.Add(new DbRecord {ApplicationName = "App1", HostName = string.Empty, Key = "KeyC", Value = "523"});

            int origCount = dbRecords.Count;
            //Compare on HostName:
            //Remove from dbRecords all 
            DbRecord[] removed;
            dbRecords.RemoveAll(toRemove,
                //s => s.HostName
                (o) => string.Join(":", o.ApplicationName, o.HostName, o.Key) //We compare on Keys
                ,
                out removed);

            //If any remaining that have a hostname, that's an error:
            Assert.IsFalse(dbRecords.Any(s => s.HostName.IsNullOrEmpty()));
        }



        [Test]
        public void RemoveAllFromCollectionThatMatchItemsInFilterList2()
        {
            List<ILIstExtensionsTests.X> list1 = new List<ILIstExtensionsTests.X> { new ILIstExtensionsTests.X { Name = "A" }, new ILIstExtensionsTests.X { Name = "B" }, new ILIstExtensionsTests.X { Name = "C" }, new ILIstExtensionsTests.X { Name = "D" } };
            List<ILIstExtensionsTests.X> list2 = new List<ILIstExtensionsTests.X> { new ILIstExtensionsTests.X { Name = "A" }, new ILIstExtensionsTests.X { Name = "D" } };

            ILIstExtensionsTests.X[] removed;

            list1.RemoveAll(
                list2,
                //s => s.HostName
                (o) => string.Join(":", o.Name) //We compare on Keys
                ,
                out removed);

            Assert.AreEqual(2, removed.Length);
            Assert.AreEqual("A", removed[0].Name);
            Assert.AreEqual("D", removed[1].Name);
        }






        [Test]
        public void GetListOfItemsMissingFromSourceCollectionThatAreSecondList()
        {
            Collection<DbRecord> dbRecords = FakeLoadOriginalRecordsFromRepository();

            Collection<DbRecord> toRemove = new Collection<DbRecord>();

            toRemove.Add(new DbRecord {ApplicationName = "App1", HostName = string.Empty, Key = "KeyA", Value = "123"});
            toRemove.Add(new DbRecord {ApplicationName = "App1", HostName = string.Empty, Key = "KeyB", Value = "323"});
            toRemove.Add(new DbRecord {ApplicationName = "App1", HostName = "H3", Key = "KeyC", Value = "523"});


            List<DbRecord> results= dbRecords.Missing(toRemove, o => o.HostName);

            

            Assert.IsTrue(results.Count>0 && results.Count< 3);
            Assert.IsTrue(results.Any(x=>x.HostName == "H3"));
            
        }

        [Test]
        public void IntersectExtensionMethodWorks()
        {
            List<ILIstExtensionsTests.X> referenceCollection = new List<ILIstExtensionsTests.X>();
            
            referenceCollection.AddRange (
            new[]{
                new ILIstExtensionsTests.X {Name="A"},
                new ILIstExtensionsTests.X {Name="B"},
                new ILIstExtensionsTests.X {Name="C"},
                new ILIstExtensionsTests.X {Name="D"},
                new ILIstExtensionsTests.X {Name="E"},
            });

            List<ILIstExtensionsTests.X> updatedCollection = new List<ILIstExtensionsTests.X>();

            updatedCollection.AddRange(
            new[]{
                new ILIstExtensionsTests.X {Name="A"},
                new ILIstExtensionsTests.X {Name="C"},
                new ILIstExtensionsTests.X {Name="E"},
            });

            IEnumerable<ILIstExtensionsTests.X> results 
                = updatedCollection.Intersect(referenceCollection, (o) => o.Name);

            Assert.AreEqual(3, results.Count());

            Assert.IsTrue(results.All(x=>updatedCollection.Contains(x)));
        }


        [Test]
        public void UpdateFromCollectionOfSameType()
        {
            //This one is rather a pain...
            //Hard to determine if really really useful as such, 

            //In the db we had a list of Items.
            //We sent up to the UI a serialization of the items...but with not all fields entered
            //

            Collection<DbRecord> originalDbRecords = FakeLoadOriginalRecordsFromRepository();

            Collection<DbRecord> roundTrippedRecords = CreateViewModelsOfSameTypeAsDbRecord();

            Debug.Assert(originalDbRecords.Count == roundTrippedRecords.Count);

            //Send up to UI, where some Attributes get modified:
            roundTrippedRecords[0].Value = "Modified A";
            roundTrippedRecords[2].Value = "Modified B";
            roundTrippedRecords[4].Value = "Modified C";

            originalDbRecords.UpdateFrom<DbRecord>(
                roundTrippedRecords,
                (o) => string.Join(":", o.ApplicationName, o.HostName, o.Key), //We compare on Keys:
                (o, o2) =>
                    {
                        if (o.Value != o2.Value)
                        {
                            o2.Value = o.Value; //Carry across the only value that will have changed.
                            return true;
                        }
                        return false;
                    } /*Map Action*/,
                null /*SoftDeletePredicate*/,
                (o) => o.Created = DateTime.Now ,/*Create Action*/
                (o) => o.LastModified = DateTime.Now,
                null /*PreDelete Action*/
                );

            //Original records have been updated.
            //Assert.IsTrue(originalDbRecords.Any(x => x.Value.StartsWith("Modified")));
            Assert.IsTrue(originalDbRecords.Count(x=>x.Value.StartsWith("Modified"))==3);
            Assert.IsTrue(originalDbRecords[0].Value.StartsWith("Modified"));
            Assert.IsTrue(originalDbRecords[2].Value.StartsWith("Modified"));
            Assert.IsTrue(originalDbRecords[4].Value.StartsWith("Modified"));

            Assert.IsTrue(originalDbRecords.Any(x => x.LastModified.HasValue));

        }


        [Test]
        public void UpdateFromCollectionOfSameTypeAndAddingItems()
        {
            //This one is rather a pain...
            //Hard to determine if really really useful as such, 

            //In the db we had a list of Items.
            //We sent up to the UI a serialization of the items...but with not all fields entered
            //

            Collection<DbRecord> originalDbRecords = FakeLoadOriginalRecordsFromRepository();

            Collection<DbRecord> roundTrippedRecords = CreateViewModelsOfSameTypeAsDbRecord();



            //Send up to UI, where some Attributes get modified:
            roundTrippedRecords[0].Value = "Modified A";
            roundTrippedRecords[2].Value = "Modified B";
            roundTrippedRecords[4].Value = "Modified C";

            //Add a new record or two:
            roundTrippedRecords.Add(new DbRecord
            {
                ApplicationName = "App2",
                HostName = "H5",
                Key = "Aba",
                Value = "New Value"
            });


            originalDbRecords.UpdateFrom<DbRecord>(
                roundTrippedRecords,
                (o) => string.Join(":", o.ApplicationName, o.HostName, o.Key), //We compare on Keys:
                (o, o2) =>
                    {
                        if (o.Value != o2.Value)
                        {
                            o2.Value = o.Value; //Carry across the only value that will have changed.
                            return true;
                        }
                        return false;
                    } /*Map Action*/,
                null/*SoftDeletePredicate*/,
                (o) => o.Created = DateTime.Now /*Create Action*/,
                (o) => o.LastModified = DateTime.Now,
                null /*PreDelete Action*/
                );

            //Original records have been updated.
            //Assert.IsTrue(originalDbRecords.Any(x => x.Value.StartsWith("Modified")));
            Assert.IsTrue(originalDbRecords.Count(x => x.Value.StartsWith("Modified")) == 3);
            Assert.IsTrue(originalDbRecords[0].Value.StartsWith("Modified"));
            Assert.IsTrue(originalDbRecords[2].Value.StartsWith("Modified"));
            Assert.IsTrue(originalDbRecords[4].Value.StartsWith("Modified"));

            Assert.IsTrue(originalDbRecords.Any(x => x.LastModified.HasValue));


            //New Items added:
            Assert.IsTrue(originalDbRecords.Any(x => x.Value == "New Value"));
            Assert.IsTrue(originalDbRecords.Any(x => x.Created.HasValue));

        }



        [Test]
        public void UpdateFromCollectionOfSameTypeAndAddingItemsAndSoftDeletingAsWell()
        {
            //This one is rather a pain...
            //Hard to determine if really really useful as such, 

            //In the db we had a list of Items.
            //We sent up to the UI a serialization of the items...but with not all fields entered
            //

            Collection<DbRecord> originalDbRecords = FakeLoadOriginalRecordsFromRepository();
            int originalDbRecordsCount = originalDbRecords.Count;

            Collection<DbRecord> roundTrippedRecords = CreateViewModelsOfSameTypeAsDbRecord();

            //Send up to UI, where some Attributes get modified:
            roundTrippedRecords[0].Value = "Modified A";
            roundTrippedRecords[2].Value = "Modified B";

            //Remove 4 and 5 by soft and hard delete:
            roundTrippedRecords.RemoveAt(4);
            //Soft delete one of them (ie [4], which is: ( "App1", HostName = "H1", Key = "KeyC", Value = "523")
            roundTrippedRecords.Last().Deleted = true;


            Debug.Assert(roundTrippedRecords.Count == originalDbRecords.Count);

            //Add a new record or two:
            roundTrippedRecords.Add(new DbRecord
            {
                ApplicationName = "App2",
                HostName = "H5",
                Key = "Aba",
                Value = "New Value"
            });

            Debug.Assert(roundTrippedRecords.Count == originalDbRecords.Count+1);

            originalDbRecords.UpdateFrom<DbRecord>(
                roundTrippedRecords,
                (o) => string.Join(":", o.ApplicationName, o.HostName, o.Key), //We compare on Keys:
                (o, o2) =>
                {
                    if (o2.Value != o.Value)
                    {
                        o2.Value = o.Value; //Carry across the only value that will have changed.
                        return true;
                    }
                    return false;
                } /*Map Action*/,
                o=>o.Deleted==true/*SoftDeletePredicate*/,
                (o) => o.Created = DateTime.Now /*PreCreate Action*/,
                (o)=> o.LastModified = DateTime.Now,
                null /*PreDelete Action*/
                );

            //Original records have been updated.
            //Assert.IsTrue(originalDbRecords.Any(x => x.Value.StartsWith("Modified")));
            Assert.IsTrue(originalDbRecords.Count(x => x.Value.StartsWith("Modified")) == 2);
            Assert.IsTrue(originalDbRecords[0].Value.StartsWith("Modified"));
            Assert.IsTrue(originalDbRecords[2].Value.StartsWith("Modified"));

            Assert.IsTrue(originalDbRecords[4].Value.StartsWith("New Value"));
            Assert.IsTrue(originalDbRecords[4].Created.HasValue);


            //New Items added:
            //Assert.IsTrue(originalDbRecords.Any(x => x.Value == "New Value"));
            //Assert.IsTrue(originalDbRecords.Any(x => x.Created.HasValue));


            Assert.IsTrue(!originalDbRecords.Any(x => x.ApplicationName == "App1" && x.HostName == "H1" && x.Key=="KeyC"));

            //We deleted 2, added 1, so:
            Assert.IsTrue(originalDbRecords.Count == originalDbRecordsCount - 1);

        }


















        [Test]
        public void UpdateFromCollectionOViewModelAndAddingItemsAndSoftDeletingAsWell3_UsingPoorMansMapperMethod()
        {
            //This one is rather a pain...
            //Hard to determine if really really useful as such, 

            //In the db we had a list of Items.
            //We sent up to the UI a serialization of the items...but with not all fields entered
            //

            Collection<DbRecord> originalDbRecords = FakeLoadOriginalRecordsFromRepository();
            int originalDbRecordsCount = originalDbRecords.Count;

            Collection<ViewModel> roundTrippedViewModels = CreateViewModelsOfDifferentType();

            //Send up to UI, where some Attributes get modified:
            roundTrippedViewModels[0].Value = "Modified A";
            roundTrippedViewModels[2].Value = "Modified B";

            roundTrippedViewModels.RemoveAt(4);
            //roundTrippedRecords[4].Value = "Modified C";

            //Soft delete one of them (ie [4], which is: ( "App1", HostName = "H1", Key = "KeyC", Value = "523")
            roundTrippedViewModels.Last().Deleted = true;


            Debug.Assert(roundTrippedViewModels.Count == originalDbRecords.Count);

            //Add a new record or two:
            roundTrippedViewModels.Add(new ViewModel()
            {
                AppName = "App2",
                HostName = "H5",
                Name = "Aba",
                Value = "New Value"
            });

            Debug.Assert(roundTrippedViewModels.Count == originalDbRecords.Count + 1);

            originalDbRecords.UpdateFrom<DbRecord, ViewModel>(
                roundTrippedViewModels,
                (o) => string.Join(":", o.ApplicationName, o.HostName, o.Key), //We compare on Keys:
                (o) =>
                new DbRecord()
                    {
                        ApplicationName = o.AppName,
                        HostName = o.HostName,
                        Key = o.Name,
                        Value = o.Value
                    },
                (o1, o2) =>
                    {
                        o1.MapPropertyValues(o2);
                        return true;
                    },
                o => o.Deleted == true /*SoftDeletePredicate*/,
                (o) => o.Created = DateTime.Now /*PreCreate Action*/,
                (o) => o.LastModified = DateTime.Now,
                null /*PreDelete Action*/
                );


            //Original records have been updated.
            //Assert.IsTrue(originalDbRecords.Any(x => x.Value.StartsWith("Modified")));
            //Assert.IsTrue(originalDbRecords.Any(x => x.LastModified.HasValue));
            
            Assert.IsTrue(originalDbRecords.Count(x => x.Value.StartsWith("Modified")) == 2);
            Assert.IsTrue(originalDbRecords[0].Value.StartsWith("Modified"));
            Assert.IsTrue(originalDbRecords[2].Value.StartsWith("Modified"));


            //New Items added:
            //Assert.IsTrue(originalDbRecords.Any(x => x.Value == "New Value"));
            //Assert.IsTrue(originalDbRecords.Any(x => x.Created.HasValue));
            Assert.IsTrue(originalDbRecords[4].Value.StartsWith("New Value"));
            Assert.IsTrue(originalDbRecords[4].Created.HasValue);


            Assert.IsTrue(!originalDbRecords.Any(x => x.ApplicationName == "App1" && x.HostName == "H1" && x.Key == "KeyC"));

            //We deleted 2, added 1, so:
            Assert.IsTrue(originalDbRecords.Count == originalDbRecordsCount - 1);

        }






        [Test]
        public void UpdateFromCollectionOfViewModelAndAddingItemsAndSoftDeletingAsWell3_UsingNullAsMapperMethod()
        {
            //This one is rather a pain...
            //Hard to determine if really really useful as such, 

            //In the db we had a list of Items.
            //We sent up to the UI a serialization of the items...but with not all fields entered
            //

            Collection<DbRecord> originalDbRecords = FakeLoadOriginalRecordsFromRepository();
            int originalDbRecordsCount = originalDbRecords.Count;

            Collection<ViewModel> roundTrippedViewModels = CreateViewModelsOfDifferentType();

            //Send up to UI, where some Attributes get modified:
            roundTrippedViewModels[0].Value = "Modified A";
            roundTrippedViewModels[2].Value = "Modified B";

            roundTrippedViewModels.RemoveAt(4);
            //roundTrippedRecords[4].Value = "Modified C";

            //Soft delete one of them (ie [4], which is: ( "App1", HostName = "H1", Key = "KeyC", Value = "523")
            roundTrippedViewModels.Last().Deleted = true;


            Debug.Assert(roundTrippedViewModels.Count == originalDbRecords.Count);

            //Add a new record or two:
            roundTrippedViewModels.Add(new ViewModel()
            {
                AppName = "App2",
                HostName = "H5",
                Name = "Aba",
                Value = "New Value"
            });

            Debug.Assert(roundTrippedViewModels.Count == originalDbRecords.Count + 1);

            originalDbRecords.UpdateFrom<DbRecord, ViewModel>(
                roundTrippedViewModels,
                (o) => string.Join(":", o.ApplicationName, o.HostName, o.Key), //We compare on Keys:
                (o) =>
                new DbRecord()
                {
                    ApplicationName = o.AppName,
                    HostName = o.HostName,
                    Key = o.Name,
                    Value = o.Value
                },
                null,
                o => o.Deleted == true /*SoftDeletePredicate*/,
                (o) => o.Created = DateTime.Now /*PreCreate Action*/,
                (o) => o.LastModified = DateTime.Now,
                null /*PreDelete Action*/
                );


            //Original records have been updated.
            Assert.IsTrue(originalDbRecords.Any(x => x.Value.StartsWith("Modified")));
            Assert.IsTrue(originalDbRecords.Any(x => x.LastModified.HasValue));
            //New Items added:
            Assert.IsTrue(originalDbRecords.Any(x => x.Value == "New Value"));
            Assert.IsTrue(originalDbRecords.Any(x => x.Created.HasValue));

            Assert.IsTrue(originalDbRecords.Count(x => x.Value.StartsWith("Modified")) == 2);
            Assert.IsTrue(originalDbRecords[0].Value.StartsWith("Modified"));
            Assert.IsTrue(originalDbRecords[2].Value.StartsWith("Modified"));


            //New Items added:
            //Assert.IsTrue(originalDbRecords.Any(x => x.Value == "New Value"));
            //Assert.IsTrue(originalDbRecords.Any(x => x.Created.HasValue));
            Assert.IsTrue(originalDbRecords[4].Value.StartsWith("New Value"));
            Assert.IsTrue(originalDbRecords[4].Created.HasValue);


            Assert.IsTrue(!originalDbRecords.Any(x => x.ApplicationName == "App1" && x.HostName == "H1" && x.Key == "KeyC"));

            //We deleted 2, added 1, so:
            Assert.IsTrue(originalDbRecords.Count == originalDbRecordsCount - 1);

        }











        [Test]
        public void UpdateFromCollectionOfViewModelAndAddingItemsAndSoftDeletingAsWell2()
        {
            //This one is rather a pain...
            //Hard to determine if really really useful as such, 

            //In the db we had a list of Items.
            //We sent up to the UI a serialization of the items...but with not all fields entered
            //

            Collection<DbRecord> originalDbRecords = FakeLoadOriginalRecordsFromRepository();
            int originalDbRecordsCount = originalDbRecords.Count;

            Collection<ViewModel> roundTrippedViewModels = CreateViewModelsOfDifferentType();

            //Send up to UI, where some Attributes get modified:
            roundTrippedViewModels[0].Value = "Modified A";
            roundTrippedViewModels[2].Value = "Modified B";

            roundTrippedViewModels.RemoveAt(4);
            //roundTrippedRecords[4].Value = "Modified C";

            //Soft delete one of them (ie [4], which is: ( "App1", HostName = "H1", Key = "KeyC", Value = "523")
            roundTrippedViewModels.Last().Deleted = true;


            Debug.Assert(roundTrippedViewModels.Count == originalDbRecords.Count);

            //Add a new record or two:
            roundTrippedViewModels.Add(new ViewModel()
            {
                AppName = "App2",
                HostName = "H5",
                Name = "Aba",
                Value = "New Value"
            });

            Debug.Assert(roundTrippedViewModels.Count == originalDbRecords.Count + 1);

            originalDbRecords.UpdateFrom<DbRecord, ViewModel>(
                roundTrippedViewModels,
                (o) => string.Join(":", o.ApplicationName, o.HostName, o.Key), //We compare on Keys:
                (o) =>
                new DbRecord()
                    {
                        ApplicationName = o.AppName,
                        HostName = o.HostName,
                        Key = o.Name,
                        Value = o.Value
                    },
                (o1, o2) =>
                    {
                    if (o2.Value != o1.Value)
                    {
                        o2.Value = o1.Value; //Carry across the only value that will have changed.
                        return true;
                    }
                    return false;
                    },
                o => o.Deleted == true /*SoftDeletePredicate*/,
                (o) => o.Created = DateTime.Now /*PreCreate Action*/,
                (o) => o.LastModified = DateTime.Now,
                null /*PreDelete Action*/
                );


            //Original records have been updated.
            Assert.IsTrue(originalDbRecords.Any(x => x.Value.StartsWith("Modified")));
            Assert.IsTrue(originalDbRecords.Any(x => x.LastModified.HasValue));


            //New Items added:
            Assert.IsTrue(originalDbRecords.Any(x => x.Value == "New Value"));
            Assert.IsTrue(originalDbRecords.Any(x => x.Created.HasValue));


            Assert.IsTrue(originalDbRecords.Count(x => x.Value.StartsWith("Modified")) == 2);
            Assert.IsTrue(originalDbRecords[0].Value.StartsWith("Modified"));
            Assert.IsTrue(originalDbRecords[2].Value.StartsWith("Modified"));


            //New Items added:
            //Assert.IsTrue(originalDbRecords.Any(x => x.Value == "New Value"));
            //Assert.IsTrue(originalDbRecords.Any(x => x.Created.HasValue));
            Assert.IsTrue(originalDbRecords[4].Value.StartsWith("New Value"));
            Assert.IsTrue(originalDbRecords[4].Created.HasValue);



            Assert.IsTrue(!originalDbRecords.Any(x => x.ApplicationName == "App1" && x.HostName == "H1" && x.Key == "KeyC"));

            //We deleted 2, added 1, so:
            Assert.IsTrue(originalDbRecords.Count == originalDbRecordsCount - 1);

        }



        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        private Collection<DbRecord> FakeLoadOriginalRecordsFromRepository()
                {
                    Collection<DbRecord> collection = new Collection<DbRecord>();

                    collection.Add(new DbRecord { ApplicationName = "App1", HostName = string.Empty, Key = "KeyA", Value = "123" });
                    collection.Add(new DbRecord { ApplicationName = "App1", HostName = string.Empty, Key = "KeyB", Value = "323" });
                    collection.Add(new DbRecord { ApplicationName = "App1", HostName = string.Empty, Key = "KeyC", Value = "523" });
                    collection.Add(new DbRecord { ApplicationName = "App1", HostName = "H1", Key = "KeyA", Value = "123" });
                    collection.Add(new DbRecord { ApplicationName = "App1", HostName = "H1", Key = "KeyB", Value = "323" });
                    collection.Add(new DbRecord { ApplicationName = "App1", HostName = "H1", Key = "KeyC", Value = "523" });

                    return collection;
                }

                private Collection<DbRecord> CreateViewModelsOfSameTypeAsDbRecord()
                {
                    Collection<DbRecord> roundTrippedRecords = FakeLoadOriginalRecordsFromRepository();
                    //It's commone with 
                    //View Models (if using same Entity object) to be a smaller projection of original datastore entity.
                    //i.e.: don't always have full complement of attributes
                    StripDownAndRemoveSomeAttributes(roundTrippedRecords);

                    return roundTrippedRecords;
                }

                private Collection<ViewModel> CreateViewModelsOfDifferentType()
                {
                    Collection<ViewModel> c = new Collection<ViewModel>();

                    CreateViewModelsOfSameTypeAsDbRecord()
                                    .Select(
                                        x=>new ViewModel{AppName=x.ApplicationName,HostName=x.HostName,Name=x.Key,Value=x.Value})
                                        .ForEach(c.Add);

                    return c;
                }


                private static void StripDownAndRemoveSomeAttributes(Collection<DbRecord> roundTrippedRecords)
                {
                    foreach (DbRecord x in roundTrippedRecords)
                    {
                        //Simulate that the request was anemic some Attributes are not sent up to ui...
                        //a minimal projection
                        x.LastModified = null;
                    }
                }

    
    }

    internal class ViewModel
    {
        public string AppName { get; set; }
        public string HostName { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public bool Deleted { get; set; }


    }

    internal class DbRecord
    {
        public string ApplicationName { get; set; }
        public string HostName { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? LastModified { get; set; }

        //[Ignore]
        public bool Deleted { get; set; }

    }

}
