namespace XAct.Data.Tests.ForNUnit
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Tests;

    /// <summary>
    /// NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IntegerExtensionsTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }
        
        #region Tests

        [Test]
        public void UnitTest01()
        {
            //XAct.Data.AClassName ctrl = new XAct.Data.AClassName();

            Assert.AreEqual(10.MinMax(0, 3), 3);
        }


        [Test]
        public void UnitTest02()
        {
            //XAct.Data.AClassName ctrl = new XAct.Data.AClassName();
            int a = -7;
            Assert.AreEqual(a.MinMax(1, 5), 1);
        }

        [Test]
        public void UnitTest02c()
        {
            //Interesting...
            Assert.AreEqual(-7.MinMax(1, 5), -5);
        }

        [Test]
        public void UnitTest02b()
        {
            //XAct.Data.AClassName ctrl = new XAct.Data.AClassName();

            Assert.AreEqual(0.MinMax(1, 5), 1);
        }


        [Test]
        public void UnitTest03()
        {
            //XAct.Data.AClassName ctrl = new XAct.Data.AClassName();

            Assert.AreEqual(2.MinMax(1, 5), 2);
        }

        #endregion

        #region TearDown

        /// <summary>
        /// Tear down after each and every test.
        /// </summary>
        [TearDown]
        public void TearDown()
        {
        }

        /// <summary>
        /// Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }

        #endregion
    }


}


