﻿namespace XAct.Caching.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.State;
    using XAct.State.Implementations;
    using XAct.Tests;

    [TestFixture]
    public class ISessionStateServiceTests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetISessionStateService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ISessionStateService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }


        [Test]
        public void CanGetISessionStateServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ISessionStateService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(SessionStateService), service.GetType());
        }

        [Test]
        public void CanPersistAndRetrieveValue_CaseSensitive()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ISessionStateService>();

            //ACT
            service.Add("KeyA","foo");
            System.Threading.Thread.Sleep(100);
            object tmp = service["KeyA"];
            string result = tmp as string;

            //ASSERT
            Assert.IsNotNullOrEmpty(result);
        }
        [Test]
        public void CanPersistAndRetrieveValue_ButIsNotCapableOfBeingCaseInsensitive()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ISessionStateService>();

            //ACT
            service.Add("KeyA", "foo");
            System.Threading.Thread.Sleep(100);
            object tmp = service["KEYA"];
            string result = tmp as string;

            //ASSERT
            Assert.IsNullOrEmpty(result);
        }

        [Test]
        public void CanRetrieveNonExistentKeysWithoutExpcetion()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ISessionStateService>();

            //ACT
            object tmp = service["KeyNonExistent"];
            
            //ASSERT
            Assert.IsNull(tmp);
        }

    }
}