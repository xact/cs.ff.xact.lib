﻿namespace XAct.Environment.Tests
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using NUnit.Framework;
    using XAct.Environment.Implementations;
    using XAct.Tests;

    [TestFixture]
    public class IEnvironmentServiceTests
    {


        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIEnvironmentService()
        {
            IEnvironmentService environmentService
                = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            Assert.IsNotNull(environmentService);
        }

        [Test]
        public void CanGetIEnvironmentServiceOfExpectedType()
        {
            //ARRANGE
            IEnvironmentService environmentService
                = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ACT:

            //ASSERT
            Assert.AreEqual(typeof(FFEnvironmentService), environmentService.GetType());
        }



        [Test]
        public void GetTotalMemoryAllocated()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            
            //ACT:
            long result = service.GetTotalMemoryAllocated();



            //ASSERT
            Assert.IsTrue(result>0);
        }
        [Test]
        public void IsUserInteractive()
        {
            //ARRANGE:
            var service = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ACT:
            var a = XAct.DependencyResolver.BindingResults.BindingScanResults.AssemblyNames;
            bool result = service.IsUserInteractive;

            //ASSERT:
            Assert.IsTrue(result);
        }

        [Test]
        public void NewLine()
        {
            //ARRANGE:
            var service = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ACT:
            string result = service.NewLine;

            //ASSERT:
            Assert.AreEqual(System.Environment.NewLine, result );
        }
        [Test]
        public void ApplicationBasePath()
        {
            //ARRANGE:
            var service = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ACT:
            string result = service.ApplicationBasePath;

            //ASSERT:
            Assert.IsNotNullOrEmpty(result);
        }
        [Test]
        public void ApplicationName()
        {
            //ARRANGE:
            var service = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ACT:
            string result = service.ApplicationName;

            //ASSERT:
            Assert.IsNotNullOrEmpty(result);
        }
        [Test]
        public void ApplicationNameHash()
        {
            //ARRANGE:
            var service = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ACT:
            string result = service.ApplicationNameHash;

            //ASSERT:
            Assert.IsNotNullOrEmpty(result);
        }
        [Test]
        public void MapPath()
        {
            //ARRANGE:
            var service = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ACT:
            string result = service.MapPath("~/Test/Sub/Dir/AFile.htm");
            string r = "\\Test\\Sub\\Dir\\AFile.htm";


            //ASSERT:
            Assert.IsTrue(result.EndsWith(r));
            //at least 5 chars more...
            Assert.IsTrue(result.Length > (r.Length+5));
        }

        //TEST:FAILING
        [Test]
        public void TestMapPath1()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();
            string x = environmentService.MapPath("~\\TestData\\Files\\CopyMe\\");
            DirectoryInfo directoryInfo = new DirectoryInfo(x);
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));
            Assert.IsTrue(directoryInfo.Exists);
        }

        //TEST:FAILING
        [Test]
        public void TestMapPath2()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            DirectoryInfo directoryInfo = new DirectoryInfo(environmentService.MapPath("TestData\\Files\\CopyMe\\"));
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));

            Assert.IsTrue(directoryInfo.Exists);
        }

        //TEST:FAILING
        [Test]
        public void TestMapPath3()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            DirectoryInfo directoryInfo = new DirectoryInfo(environmentService.MapPath("\\TestData\\Files\\CopyMe\\"));
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));

            Assert.IsTrue(directoryInfo.Exists);
        }

        //TEST:FAILING
        [Test]
        public void TestMapPath4()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            DirectoryInfo directoryInfo = new DirectoryInfo(environmentService.MapPath("~\\TestData\\Files\\CopyMe"));
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));

            Assert.IsTrue(directoryInfo.Exists);
        }

        //TEST:FAILING
        [Test]
        public void TestMapPath5()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            DirectoryInfo directoryInfo = new DirectoryInfo(environmentService.MapPath("TestData\\Files\\CopyMe"));
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));

            Assert.IsTrue(directoryInfo.Exists);


        }

        //TEST:FAILING
        [Test]
        public void TestMapPath6()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            DirectoryInfo directoryInfo = new DirectoryInfo(environmentService.MapPath("\\TestData\\Files\\CopyMe"));
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));

            Assert.IsTrue(directoryInfo.Exists);
        }


        //TEST:FAILING
        [Test]
        public void TestMapPath7()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            DirectoryInfo directoryInfo = new DirectoryInfo(environmentService.MapPath("~/TestData/Files/CopyMe"));
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));

            Assert.IsTrue(directoryInfo.Exists);
        }

        //TEST:FAILING
        [Test]
        public void TestMapPath8()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            DirectoryInfo directoryInfo = new DirectoryInfo(environmentService.MapPath("TestData\\Files\\CopyMe"));
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));

            Assert.IsTrue(directoryInfo.Exists);
        }

        //TEST:FAILING
        [Test]
        public void TestMapPath9()
        {
            IEnvironmentService environmentService = DependencyResolver.Current.GetInstance<IEnvironmentService>();

            DirectoryInfo directoryInfo = new DirectoryInfo(environmentService.MapPath("/TestData/Files/CopyMe"));
            Trace.WriteLine("Path:{0}".FormatStringInvariantCulture(directoryInfo.FullName));

            Assert.IsTrue(directoryInfo.Exists);
        }

    }
}
