﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Environment;
    using XAct.Environment.Services.Implementations;

    [TestFixture]
    public class IDateTimeServiceTests
    {
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void CanGetIDateTimeService()
        {
            //ARRANGE:
            IDateTimeService dateTimeService
                = DependencyResolver.Current.GetInstance<IDateTimeService>();

            //ACT:

            //ASSERT:
            Assert.IsNotNull(dateTimeService);
        }

        [Test]
        public void CanGetIDateTimeServiceOfExpectedType()
        {
            //ARRANGE
            IDateTimeService dateTimeService
                = DependencyResolver.Current.GetInstance<IDateTimeService>();

            //ASSERT
            Assert.AreEqual(typeof(DateTimeService), dateTimeService.GetType());
        }

        [Test]
        public void NowUtc()
        {

            //ARRANGE
            IDateTimeService dateTimeService
                = DependencyResolver.Current.GetInstance<IDateTimeService>();

            //ACT:
            DateTime nowLocal = DateTime.Now;
            DateTime result = dateTimeService.NowUTC;
            //ASSERT
            Assert.IsTrue(result.Kind == DateTimeKind.Utc);
        }


        [Test]
        public void NowSame()
        {
            //ARRANGE:
            IDateTimeService dateTimeService
                = DependencyResolver.Current.GetInstance<IDateTimeService>();

            //ACT
            //XAct.ApplicationEnvironment.IgnoreNowAppSetings = true;
            DateTime result = dateTimeService.NowUTC.ToLocalTime();
            DateTime nowLocal = DateTime.Now;
            double dif = result.Subtract(nowLocal).TotalMilliseconds;

            //ASSERT
            Assert.IsTrue(result.Subtract(nowLocal).TotalMilliseconds < 100);
        }
        [Test]
        public void ReasonableSpeedAfterFirstHit()
        {
            //ARRANGE
            IDateTimeService dateTimeService
                = DependencyResolver.Current.GetInstance<IDateTimeService>();

            //ACT
            //XAct.ApplicationEnvironment.IgnoreNowAppSetings = true;
            DateTime result = dateTimeService.NowUTC;
            DateTime start = DateTime.Now;
            for (int i = 0; i < 10000; i++)
            {
                DateTime result2 = dateTimeService.NowUTC.ToLocalTime();
            }

            //ASSERT
            Assert.IsTrue(DateTime.Now.Subtract(start).TotalMilliseconds < 100);
        }

        [Test]
        public void OffsetToUtcByUsingConfigFile()
        {

            //ARRANGE
            IDateTimeService dateTimeService
                = DependencyResolver.Current.GetInstance<IDateTimeService>();

            
            //ACT
            //The time will be utc of local time of 8/1, which, being +12, will now be 7/31, noon
            dateTimeService.SetUTCOffset(new DateTime(2011, 08, 01, 0, 0, 0).ToUniversalTime());
            DateTime result = dateTimeService.NowUTC;



            //So entering 
            DateTime shouldBe = new DateTime(2011, 08, 01, 0, 0, 0, 0, DateTimeKind.Local);
            DateTime shouldBeUtc = shouldBe.ToUniversalTime();




            //double dif = result.Subtract(shouldBe).TotalMilliseconds;
            double dif2 = result.Subtract(shouldBeUtc).TotalMilliseconds;


            dateTimeService.ResetUTCOffset();

            //ASSERT
            Assert.IsTrue(dif2 < 100);
        }

    }
}
