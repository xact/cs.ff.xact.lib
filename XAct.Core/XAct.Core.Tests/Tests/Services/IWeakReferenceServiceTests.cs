//namespace XAct.Caching.Tests
//{
//    using NUnit.Framework;
//    using XAct.Entities.Implementations;
//    using XAct.Environment;
//    using XAct.Environment.Implementations;
//    using XAct.Tests;

//    [TestFixture]
//    public class IWeakReferenceServiceTests
//    {

//        [SetUp]
//        public void MyTestSetup()
//        {
//            //Run before every test:
//            Singleton<IocContext>.Instance.ResetIoC();
//        }

//        [Test]
//        public void CanGetService()
//        {
//            //ARRANGE
//            var service = DependencyResolver.Current.GetInstance<IWeakReferenceService>();

//            //ACT

//            //ASSERT
//            Assert.IsNotNull(service);

//        }



//        [Test]
//        public void CanGetServiceOfExpectedType()
//        {
//            //ARRANGE
//            var service = DependencyResolver.Current.GetInstance<IWeakReferenceService<object>>();

//            //ACT

//            //ASSERT
//            Assert.AreEqual(typeof (WeakReferenceService<,>), service.GetType());
//        }
//    }
//}