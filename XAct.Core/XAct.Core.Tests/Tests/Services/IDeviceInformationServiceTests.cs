﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Environment;
    using XAct.Environment.Implementations;
    using XAct.Environment.Services.Implementations;

    [TestFixture]
    public class IDeviceInformationServiceTests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIDeviceInformationService()
        {
            //ARRANGE

            IDeviceInformationService deviceInformationService =
                XAct.DependencyResolver.Current.GetInstance<IDeviceInformationService>();

            //ASSERT
            Assert.IsNotNull(deviceInformationService);
        }

        [Test]
        public void CanGetIDeviceInformationServiceOfExpectedType()
        {
            //ARRANGE

            IDeviceInformationService deviceInformationService =
                XAct.DependencyResolver.Current.GetInstance<IDeviceInformationService>();

            //ASSERT
            Assert.AreEqual(typeof(DeviceInformationService),deviceInformationService.GetType());
        }

        [Test]
        public void CanGetHash()
        {
            //ARRANGE
            IDeviceInformationService deviceInformationService =
                XAct.DependencyResolver.Current.GetInstance<IDeviceInformationService>();

            //ACT
            string hash = deviceInformationService.GetUniqueDeviceID();

            //ASSERT
            Assert.IsNotNullOrEmpty(hash);
        }

        [Test]
        public void CanGetHashMultipleTimes()
        {
            //ARRANGE
            IDeviceInformationService deviceInformationService =
                XAct.DependencyResolver.Current.GetInstance<IDeviceInformationService>();

            //ACT
            string hash = deviceInformationService.GetUniqueDeviceID();

            for (int i = 0; i < 1000; i++)
            {
                var hash2= deviceInformationService.GetUniqueDeviceID();
                //ASSERT
                Assert.AreEqual(hash,hash2);
            }

        }


        [Test]
        public void HashIsDifferentIfMachineNameChanged()
        {

            //ARRANGE
            IDeviceInformationService deviceInformationService =
                XAct.DependencyResolver.Current.GetInstance<IDeviceInformationService>();

            //ACT
            string hash = deviceInformationService.GetUniqueDeviceID();

            //Reset everything:.
            Singleton<IocContext>.Instance.ResetIoC();

            //Change the machine name:
            IEnvironmentServiceConfiguration environmentServiceConfiguration =
    XAct.DependencyResolver.Current.GetInstance<IEnvironmentServiceConfiguration>();

            environmentServiceConfiguration.MachineName = "SOMEOTHERMACHINE";

            IEnvironmentService environmentService =
    XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>();

            //ASSERT
            Assert.AreEqual("SOMEOTHERMACHINE", environmentService.MachineName, "Env machine name should be the new fake name");

            deviceInformationService =
                XAct.DependencyResolver.Current.GetInstance<IDeviceInformationService>();

            //CLEAR this first:
            DeviceInformationService.CachedDeviceUniqueId = null;

            //If the name has successfully been changed, should be able to gen a new hash.
            string hash2 = deviceInformationService.GetUniqueDeviceID();


            string check  =
                ("Default".ToLower() + ":" + environmentService.DomainName + "\\" + environmentService.MachineName).CalculateHash();

            Assert.AreEqual(check, hash2, "With name changed, should be same as our hand-crafted hash");


            //Reset everything:.
            Singleton<IocContext>.Instance.ResetIoC();

            Assert.AreNotEqual(hash,hash2, "should not be the same if we changed the name...");
        }




        [Test]
        public void GetDeviceUniqueId()
        {


            //ARRANGE
            IDeviceInformationService deviceInformationService = DependencyResolver.Current.GetInstance<IDeviceInformationService>();


            //ASSERT
            string haskKey = "EXAMPLE".CalculateHash();
            string result = deviceInformationService.GetUniqueDeviceID(haskKey);
            string result2 = deviceInformationService.GetUniqueDeviceID(haskKey);

            //ACT
            Assert.IsNotNullOrEmpty(result);
            Assert.AreEqual(result, result2);
        }

    }
}
