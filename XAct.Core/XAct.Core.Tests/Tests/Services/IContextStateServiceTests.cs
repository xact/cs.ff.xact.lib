﻿namespace XAct.Caching.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.State;
    using XAct.State.Implementations;
    using XAct.Tests;

    [TestFixture]
    public class IContextStateServiceTests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetContextStateService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IContextStateService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }


        [Test]
        public void CanGetContextStateServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IContextStateService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(ContextStateService), service.GetType());
        }

        [Test]
        public void CanPersistAndRetrieveValue_CaseSensitive()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IContextStateService>();

            //ACT
            service.Items["KeyA"] = "foo";
            System.Threading.Thread.Sleep(100);
            object tmp = service.Items["KeyA"];
            string result = tmp as string;

            //ASSERT
            Assert.IsNotNullOrEmpty(result);
        }
        [Test]
        public void CanPersistAndRetrieveValue_ButIsNotCapableOfBeingCaseInsensitive()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IContextStateService>();

            //ACT
            service.Items["KeyA"] = "foo";
            System.Threading.Thread.Sleep(100);
            object tmp = service.Items["KEYA"];
            string result = tmp as string;

            //ASSERT
            Assert.IsNullOrEmpty(result);
        }

        [Test]
        public void CanRetrieveNonExistentKeysWithoutExpcetion()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IContextStateService>();

            //ACT
            object tmp = service.Items["KeyNonExistent"];
            
            //ASSERT
            Assert.IsNull(tmp);
        }

    }
}