namespace XAct.Caching.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Environment;
    using XAct.Environment.Implementations;
    using XAct.Environment.Services.Implementations;
    using XAct.Tests;


    [TestFixture]
    public class IClientEnvironmentServiceTests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIClientEnvironmentService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IClientEnvironmentService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }



        [Test]
        public void CanGetIClientEnvironmentServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IClientEnvironmentService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(ClientEnvironmentService), service.GetType());
        }

        [Test]
        public void CanGetClientUICulture()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IClientEnvironmentService>();

            //ACT
            var clientCulture = service.ClientUICulture;

            //ASSERT
            Assert.AreEqual(System.Globalization.CultureInfo.CurrentUICulture, clientCulture);
        }


        [Test]
        [Ignore]
        [ExpectedException]
        public void CanGetClientIP()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IClientEnvironmentService>();

            //ACT
            var ip = service.ClientIP;

            //ASSERT
            //Raises Not Implemented.
            Assert.IsNullOrEmpty(ip);
        }




    }
}