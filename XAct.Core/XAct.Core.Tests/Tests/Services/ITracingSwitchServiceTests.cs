﻿namespace Tests
{
    using System;
    using System.Diagnostics;
    using NUnit.Framework;
    using XAct;
    using XAct.Diagnostics;
    using XAct.Tests;

    [TestFixture]
    public class ITracingSwitchServiceTests
    {


        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetITraceSwitchService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ITraceSwitchService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }

        [Test]
        public void CanGetITraceSwitchServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ITraceSwitchService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(XAct.Diagnostics.Implementations.TraceSwitchService), service.GetType());
        }

        [Test]
        public void ResolveTraceSwitchService()
        {
            ITraceSwitchService traceSwitchService;
            TraceSwitch appSwitch;
            XAct.Diagnostics.TraceLevel appTraceLevel;

            traceSwitchService =
                XAct.DependencyResolver.Current.GetInstance<ITraceSwitchService>();

            //Get App Switch:
            appSwitch = traceSwitchService.GetApplicationTraceSwitch();

            //Save:
            appTraceLevel = (XAct.Diagnostics.TraceLevel)appSwitch.Level;

            //Set and check:
            appSwitch.Level = (System.Diagnostics.TraceLevel)XAct.Diagnostics.TraceLevel.Verbose;
            Assert.IsTrue(appSwitch.Level == (System.Diagnostics.TraceLevel)XAct.Diagnostics.TraceLevel.Verbose);

            //Switch back:
            appSwitch.Level = (System.Diagnostics.TraceLevel)appTraceLevel;
            Assert.IsTrue(appSwitch.Level == (System.Diagnostics.TraceLevel)appTraceLevel);

        }

        [Test]
        public void Test02()
        {
            ITraceSwitchService traceSwitchService;
            TraceSwitch appSwitch;
            XAct.Diagnostics.TraceLevel appTraceLevel;
            bool shouldTrace;
                
            traceSwitchService =
                XAct.DependencyResolver.Current.GetInstance<ITraceSwitchService>();

            //Get App Switch:
            appSwitch = traceSwitchService.GetApplicationTraceSwitch();

            //Save:
            appTraceLevel = (XAct.Diagnostics.TraceLevel)appSwitch.Level;

            //Set:
            appSwitch.Level = (System.Diagnostics.TraceLevel)XAct.Diagnostics.TraceLevel.Verbose;
            Assert.IsTrue(appSwitch.Level == (System.Diagnostics.TraceLevel)XAct.Diagnostics.TraceLevel.Verbose);

            shouldTrace = traceSwitchService.ShouldTrace(XAct.Diagnostics.TraceLevel.Verbose,this.GetType());
            Assert.IsTrue(shouldTrace == true);

            //Set:
            appSwitch.Level = (System.Diagnostics.TraceLevel)XAct.Diagnostics.TraceLevel.Warning;

            shouldTrace = traceSwitchService.ShouldTrace(XAct.Diagnostics.TraceLevel.Verbose, this.GetType());
            Assert.IsTrue(shouldTrace == false);


            //Switch back:
            appSwitch.Level = (System.Diagnostics.TraceLevel)appTraceLevel;
            Assert.IsTrue(appSwitch.Level == (System.Diagnostics.TraceLevel)appTraceLevel);

        }





        [Test]
        public void Tests3()
        {

            ITraceSwitchService traceSwitchService=null;
            //TraceSwitch appSwitch;
            //TraceLevel appTraceLevel;
            bool shouldTrace;
            //ITracingService tracingService=null;


            ITraceSwitchServiceConfiguration traceSwitchServiceConfiguration =
    XAct.DependencyResolver.Current.GetInstance<ITraceSwitchServiceConfiguration>();



            traceSwitchServiceConfiguration.CacheTraceSwitches = false;

            traceSwitchService= XAct.DependencyResolver.Current.GetInstance<ITraceSwitchService>();


            DateTime start = DateTime.Now;
            for(int i=0;i<100000;i++)
            {
                shouldTrace = traceSwitchService.ShouldTrace(XAct.Diagnostics.TraceLevel.Verbose, this.GetType());
            }
            TimeSpan elapsed = DateTime.Now.Subtract(start);



                traceSwitchServiceConfiguration.CacheTraceSwitches = true;

            DateTime start2 = DateTime.Now;
            for (int i = 0; i < 100000; i++)
            {
                shouldTrace = traceSwitchService.ShouldTrace(XAct.Diagnostics.TraceLevel.Verbose, this.GetType());
            }
            TimeSpan elapsed2 = DateTime.Now.Subtract(start);

            Debug.WriteLine("Dif:" + (elapsed.TotalMilliseconds - elapsed2.TotalMilliseconds).ToString());
        }



        [Test]
        public void Tests5()
        {

            ITraceSwitchService traceSwitchService = null;
            //TraceSwitch appSwitch;
            //TraceLevel appTraceLevel;
            //bool shouldTrace;
            ITracingService tracingService = null;


            ITraceSwitchServiceConfiguration traceSwitchServiceConfiguration =
    XAct.DependencyResolver.Current.GetInstance<ITraceSwitchServiceConfiguration>();

            tracingService =
    XAct.DependencyResolver.Current.GetInstance<ITracingService>();



            traceSwitchServiceConfiguration.CacheTraceSwitches = false;

            traceSwitchService = XAct.DependencyResolver.Current.GetInstance<ITraceSwitchService>();


            DateTime start = DateTime.Now;
            for (int i = 0; i < 1000; i++)
            {
                tracingService.Trace(XAct.Diagnostics.TraceLevel.Verbose, "XXXX", this.GetType());
            }
            TimeSpan elapsed = DateTime.Now.Subtract(start);



            traceSwitchServiceConfiguration.CacheTraceSwitches = true;

            DateTime start2 = DateTime.Now;
            for (int i = 0; i < 1000; i++)
            {
                tracingService.Trace(XAct.Diagnostics.TraceLevel.Verbose, "XXXX", this.GetType());
            }
            TimeSpan elapsed2 = DateTime.Now.Subtract(start);

            Debug.WriteLine("Dif:" + (elapsed.TotalMilliseconds - elapsed2.TotalMilliseconds).ToString());
        }


    }
}
