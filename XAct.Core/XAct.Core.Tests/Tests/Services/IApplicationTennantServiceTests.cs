namespace XAct.Caching.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Environment;
    using XAct.Environment.Services.Implementations;
    using XAct.Tests;


    [TestFixture]
    public class IApplicationTennantServiceTests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIApplicationTennantService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IApplicationTennantService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }


        [Test]
        public void CanGetIApplicationTennantServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IApplicationTennantService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(ApplicationTennantService), service.GetType());
        }

        [Test]
        public void CanGetEmptyApplicationTennantId()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IApplicationTennantService>();

            //ACT
            Guid id = service.Get();
            //ASSERT
            Assert.AreEqual(Guid.Empty, id);
        }
    }
}