namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Exceptions;
    using XAct.Exceptions.Implementations;


    [TestFixture]
    public class IExceptionHandlingManagementServiceTests
    {

        public class TestCustomException : Exception
        {
            public TestCustomException()
                : base("Good!")
            {
            }
        }



        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void CanGetIExceptionHandlingService()
        {
            //ARRANGE
            IExceptionHandlingManagementService service = XAct.DependencyResolver.Current.GetInstance<IExceptionHandlingManagementService>();

            //ASSERT
            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetIExceptionHandlingServiceOfExpectedType()
        {
            //ARRANGE
            IExceptionHandlingManagementService service = XAct.DependencyResolver.Current.GetInstance<IExceptionHandlingManagementService>();

            //ASSERT
            Assert.AreEqual(typeof(ExceptionHandlingManagementService), service.GetType());
        }




        [Test]
        public void RegisterAnExceptionHandlingConfiguration()
        {

            IExceptionHandlingManagementService exceptionHandlingManagementService =
                XAct.DependencyResolver.Current.GetInstance<IExceptionHandlingManagementService>();

            ExceptionHandlingConfiguration configuration = 
                new ExceptionHandlingConfiguration(typeof(ArgumentNullException), ExceptionHandlingBehaviour.Log);

            exceptionHandlingManagementService.Register(configuration);

            try
            {
                throw new ArgumentNullException();
            }
            catch (System.Exception e)
            {
                IExceptionHandlingService service = XAct.DependencyResolver.Current.GetInstance<IExceptionHandlingService>();

                if (!service.HandleException(ref e))
                {
                    //This is where we should be with just Logging....not having to do anything with the exception
                    Assert.IsTrue(true);
                }

            }
        }



        [Test]
        public void RegisterAnExceptionHandlingConfigurationWithReplacement()
        {

            IExceptionHandlingManagementService exceptionHandlingManagementService =
                XAct.DependencyResolver.Current.GetInstance<IExceptionHandlingManagementService>();

            ExceptionHandlingConfiguration configuration =
                new ExceptionHandlingConfiguration(typeof(ArgumentNullException), ExceptionHandlingBehaviour.LogAndReplaceException,typeof(TestCustomException));

            exceptionHandlingManagementService.Register(configuration);

            try
            {
                throw new ArgumentNullException();
            }
            catch (System.Exception e)
            {
                IExceptionHandlingService service = XAct.DependencyResolver.Current.GetInstance<IExceptionHandlingService>();

                if (service.HandleException(ref e))
                {
                    //THe service is reporting that I have to handle it.
                    Assert.IsTrue(true);

                    Assert.AreEqual("Good!",e.Message);

                }

            }
        }



    }


}


