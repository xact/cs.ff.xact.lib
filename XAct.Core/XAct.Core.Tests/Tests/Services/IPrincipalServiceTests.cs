﻿namespace XAct.Caching.Tests
{
    using System;
    using System.Security.Principal;
    using NUnit.Framework;
    using XAct.Environment;
    using XAct.Environment.Implementations;
    using XAct.Tests;

    [TestFixture]
    public class IPrincipalServiceTests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIPrincipalService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IPrincipalService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }


        [Test]
        public void CanGetIPrincipalServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IPrincipalService>();

            //ACT

            //ASSERT
            //While running in desktop FF, will be FF, and not PCL, implementation:
            Assert.AreEqual(typeof(FFPrincipalService), service.GetType());
        }

        [Test]
        public void CanGetPrincipal()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IPrincipalService>();

            //ACT
            IPrincipal principal = service.Principal;

            //ASSERT
            Assert.IsNotNull(principal);
        }
        [Test]
        public void CanGetCurrentIdentityIdentifier()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IPrincipalService>();

            //ACT
            string sid = service.CurrentIdentityIdentifier;

            //ASSERT
            
            //Interesting: 
            // When running under Resharper's Unit Test runner, it's null.
            // When running under Visual Studio's Test Explorer, it's got a sid..."S-1-5-21-17..."

            if (sid.IsNullOrEmpty())
            {
                Assert.IsNullOrEmpty(sid);
            }
            else
            {
                Assert.IsTrue(sid.StartsWith("S-1-"));
            }
        }


    }
}