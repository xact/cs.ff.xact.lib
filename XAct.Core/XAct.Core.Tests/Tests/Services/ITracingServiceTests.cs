﻿namespace XAct.Tests
{
    using System;
    using System.Diagnostics;
    using NUnit.Framework;
    using XAct;
    using XAct.Diagnostics;

    [TestFixture]
    public class ITracingServiceTests
    {


        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetITracingService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ITracingService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }

        [Test]
        public void CanGetITracingServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ITracingService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(XAct.Diagnostics.Implementations.SystemDiagnosticsTracingService), service.GetType());
        }



        [Test]
        public void Tests2()
        {

            ITraceSwitchService traceSwitchService;
            TraceSwitch appSwitch;
            XAct.Diagnostics.TraceLevel appTraceLevel;
            bool shouldTrace;
            ITracingService tracingService;


            tracingService =
                XAct.DependencyResolver.Current.GetInstance<ITracingService>();

            traceSwitchService =
                XAct.DependencyResolver.Current.GetInstance<ITraceSwitchService>();

            //Get App Switch:
            appSwitch = traceSwitchService.GetApplicationTraceSwitch();

            //Save:
            appTraceLevel = (XAct.Diagnostics.TraceLevel)appSwitch.Level;

            //Set:
            appSwitch.Level = (System.Diagnostics.TraceLevel)XAct.Diagnostics.TraceLevel.Verbose;
            Assert.IsTrue(appSwitch.Level == (System.Diagnostics.TraceLevel)XAct.Diagnostics.TraceLevel.Verbose);

            shouldTrace = traceSwitchService.ShouldTrace(XAct.Diagnostics.TraceLevel.Verbose, this.GetType());
            Assert.IsTrue(shouldTrace == true);

            tracingService.Trace(XAct.Diagnostics.TraceLevel.Warning, "Should Trace");

            //Set:
            appSwitch.Level = (System.Diagnostics.TraceLevel)XAct.Diagnostics.TraceLevel.Warning;

            shouldTrace = traceSwitchService.ShouldTrace(XAct.Diagnostics.TraceLevel.Verbose, this.GetType());
            Assert.IsTrue(shouldTrace == false);

            tracingService.Trace(XAct.Diagnostics.TraceLevel.Warning, "Should not Trace");

            //Switch back:
            appSwitch.Level = (System.Diagnostics.TraceLevel)appTraceLevel;
            Assert.IsTrue(appSwitch.Level == (System.Diagnostics.TraceLevel)appTraceLevel);



            tracingService.Trace(XAct.Diagnostics.TraceLevel.Warning, "Should Trace");

        }

    }
}