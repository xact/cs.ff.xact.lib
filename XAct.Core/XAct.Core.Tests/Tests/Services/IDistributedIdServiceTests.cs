﻿namespace XAct.Caching.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using NUnit.Framework;
    using XAct.Implementations;
    using XAct.Tests;


    [TestFixture]
    public class IDistributedIdServiceTests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetDistributedIdService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IDistributedIdService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }


        [Test]
        public void CanGetDistributedIdServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IDistributedIdService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(DistributedIdService), service.GetType());
        }

        [Test]
        public void CanGenerateGuid()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IDistributedIdService>();

            //ACT
            Guid result = service.NewGuid();

            //ASSERT
            Assert.AreNotEqual(Guid.Empty, result);
        }

        [Test]
        public void CanGenerateTenGuid()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IDistributedIdService>();

            //ACT
            Guid result =default(Guid);
            for (int i = 0; i < 10; i++)
            {
                result = service.NewGuid();
                Trace.WriteLine(result);
            }

            //ASSERT
            Assert.AreNotEqual(Guid.Empty, result);
        }


        [Test]
        public void CanGenerateUniqueGuids()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IDistributedIdService>();

            //ACT
            List<Guid> results = new List<Guid>();
            int max = 10000;
            for(int i=0;i<max;i++)
            {
                Guid result = service.NewGuid();
                if (results.Contains(result))
                {
                    break;
                }
                results.Add(result);
            }
            //ASSERT
            Assert.AreEqual(max,results.Count);
            
        }
        [Test]
        public void CanGenerateSequentialGuids()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<IDistributedIdService>();

            //ACT
            int max = 10000;
            List<Guid> results = new List<Guid>();
            string lastGuidAsText = null;
            for (int i = 0; i < max; i++)
            {
                Guid result = service.NewGuid();
                if (results.Contains(result))
                {
                    break;
                }
                string tmp = result.ToString();
                if (string.Compare(tmp, lastGuidAsText)<0)
                {
                    break;
                }
                results.Add(result);
            }
            
            //ASSERT
            Assert.AreEqual(max, results.Count);

        }
    }
}