namespace XAct.Exceptions.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Exceptions.Implementations;
    using XAct.Tests;

    [TestFixture]
    public class IExceptionManagementServiceTests
    {

        public class TestCustomException : Exception
        {
            public TestCustomException()
                : base("Good!")
            {
            }
        }



        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void CanGetIExceptionHandlingService()
        {
            //ARRANGE
            IExceptionHandlingService service =
                XAct.DependencyResolver.Current.GetInstance<IExceptionHandlingService>();

            //ASSERT
            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetIExceptionHandlingServiceOfExpectedType()
        {
            //ARRANGE
            IExceptionHandlingService service =
                XAct.DependencyResolver.Current.GetInstance<IExceptionHandlingService>();

            //ASSERT
            Assert.AreEqual(typeof (ExceptionHandlingService), service.GetType());
        }


    }
}