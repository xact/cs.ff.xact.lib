﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Events;
    using XAct.Events.Implementations;
    using XAct.Events.Tests;
    using XAct.Tests;

    [TestFixture]
    public class IEventAggregatorServiceTests
    {


        public class SubA : IEventSubscriber<StringMessage>
        {
            private readonly string _name;

            public SubA(string name)
            {
                _name = name;
            }

            /// <summary>
            /// Handles the <see cref="IEvent"/> sent by the <see cref="IEventAggregatorService"/>.
            /// </summary>
            /// <param name="message">The message.</param>
            public void Handle(StringMessage message)
            {
                Console.WriteLine("'{0}' Got Message: {1}", _name, message.Message);
                message.Results.Add("woo!");
            }
        }


        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIEventAggregatorService()
        {
            //ARRANGE
            IEventAggregatorService service = XAct.DependencyResolver.Current.GetInstance<IEventAggregatorService>();

            //ASSERT
            Assert.IsNotNull(service);
        }

        [Test]
        public void CanGetIEventAggregatorServiceOfExpectedType()
        {
            //ARRANGE
            IEventAggregatorService service = XAct.DependencyResolver.Current.GetInstance<IEventAggregatorService>();

            //ASSERT
            Assert.AreEqual(typeof(EventAggregatorService), service.GetType());
        }

        [Test]
        public void CacheWorksForATimeSpan()
        {
            //ARRANGE
            IEventAggregatorService service = XAct.DependencyResolver.Current.GetInstance<IEventAggregatorService>();

            //ACT
            service.AddSubscriber(new SubA("ABA"));
            service.AddSubscriber(new SubA("BABA"));

            StringMessage sm = new StringMessage {Message = "Piece of Cake"};
            service.Publish(sm);

            //ASSERT
            Assert.AreEqual(2,sm.Results.Count);
        }
    }
}