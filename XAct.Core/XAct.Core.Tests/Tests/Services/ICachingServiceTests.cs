﻿namespace XAct.Caching.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Caching.Implementations;
    using XAct.Tests;


    [TestFixture]
    public class ICachingServiceTests
    {

        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            Singleton<IocContext>.Instance.ResetIoC();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetICachingService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ICachingService>();

            //ACT

            //ASSERT
            Assert.IsNotNull(service);

        }

        [Test]
        public void CanGetICachingServiceOfExpectedType()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ICachingService>();

            //ACT

            //ASSERT
            Assert.AreEqual(typeof(CachingService),service.GetType());
        }


        [Test]
        public void CacheWorksForATimeSpan()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ICachingService>();

            //ACT
            //Save for 3 secs:
            service.Set("TestA", "SomeData", new TimeSpan(0, 0, 3));

            System.Threading.Thread.Sleep(1);
            string result;

            //Should still be there:
            service.TryGet("TestA", out result);

            //ASSERT
            Assert.IsNotNullOrEmpty(result);
        }


        [Test]
        public void ValueClearsAfterExpiration()
        {
            //ACT
            var service = DependencyResolver.Current.GetInstance<ICachingService>();

            //ACT
            service.Set("TestA", "SomeData", new TimeSpan(0, 0, 2));
            string result;
            System.Threading.Thread.Sleep(1000);
            service.TryGet("TestA", out result);

            //ACT
            Assert.IsNotNullOrEmpty(result);
            
            System.Threading.Thread.Sleep(2000);
            service.TryGet("TestA", out result);

            //ACT2
            Assert.IsNull(result);
        }


        [Test]
        public void SelfUpdates()
        {

            //ARRANGE
            ICachingService CachingService = DependencyResolver.Current.GetInstance<ICachingService>();


            //ACT
            string result;
            bool found;

            found = CachingService.TryGet("TestC1", out result, RunMeToGetCacheTestResult, TimeSpan.FromSeconds(1), true);
            Assert.IsTrue(found);
            Assert.IsNotNull(result);
            result = null;

            System.Threading.Thread.Sleep(500);
            found = CachingService.TryGet("TestC1", out result, RunMeToGetCacheTestResult, TimeSpan.FromSeconds(1), true);

            //ASSERT
            Assert.IsTrue(found);
            Assert.IsNotNull(result);
            result = null;

        }

        [Test]
        public void SelfUpdates2()
        {
            //ARRANGE
            ICachingService CachingService = DependencyResolver.Current.GetInstance<ICachingService>();

            string result;
            bool found;

            //ACT
            found = CachingService.TryGet("TestC2", out result, RunMeToGetCacheTestResult, TimeSpan.FromSeconds(1), true);
            Assert.IsTrue(found);
            Assert.IsNotNull(result);
            result = null;

            System.Threading.Thread.Sleep(750);
            found = CachingService.TryGet("TestC2", out result, RunMeToGetCacheTestResult, TimeSpan.FromSeconds(1), true);
            Assert.IsTrue(found);
            Assert.IsNotNull(result);
            result = null;

            System.Threading.Thread.Sleep(750);
            found = CachingService.TryGet("TestC2", out result, RunMeToGetCacheTestResult, TimeSpan.FromSeconds(1), true);
            Assert.IsTrue(found);
            Assert.IsNotNull(result);
            result = null;

            //ASSERT...
        }




        [Test]
        public void SelfUpdates3()
        {

            //ARRANGE
            ICachingService cachingService = DependencyResolver.Current.GetInstance<ICachingService>();

            string result;
            bool found;

            //ACT
            found = cachingService.TryGet("TestC3", out result, RunMeToGetCacheTestResult, TimeSpan.FromSeconds(1), true);
            Assert.IsTrue(found);
            Assert.IsNotNull(result);
            result = null;


            System.Threading.Thread.Sleep(1500);
            found = cachingService.TryGet("TestC3", out result, RunMeToGetCacheTestResult, TimeSpan.FromSeconds(1), true);
            Assert.IsTrue(found);
            Assert.IsNotNull(result);
            result = null;

            //ASSERT...
        }




        private string RunMeToGetCacheTestResult()
        {
            return DateTime.Now.ToString();
        }
    }
}
