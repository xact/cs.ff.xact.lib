﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Environment;
    using XAct.Environment.Implementations;

    [TestFixture]
    public class IProductInformationServiceTests
    {


        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();

            ProductInformationServiceConfiguration productInformationServiceConfiguration =
                DependencyResolver.Current.GetInstance<ProductInformationServiceConfiguration>();


            productInformationServiceConfiguration.DeveloperInformation = new ProductInformation
                {
                    CompanyName = "XAct",
                    ProductName = "SomeApp",

                    //The foolowing should all be resource keys, but for testing
                    //we'll use direct strings:

                    CompanyDisplayNameResourceKey = "XAct Software Solutions, Inc.",
                    CompanyDisplayAddressResourceKey = "123 Sherlock Way, Arlington, Virginia, 20123, USA",
                    CompanyDisplayContactInformationResourceKey = "(800) 123 4567",

                    ProductDisplayTitleResourceKey = "Some App",
                    ProductDisplaySubTitleResourceKey = "Best Little App for doing nothing",
                    ProductDisplayDescriptionResourceKey = "An app to waste your time, everytime.",
                    ProductDisplayProductUrlResourceKey = "http://ourcompany.com/products/xact",
                    ProductDisplayCopyrightResourceKey = "Copyright 3013",

                    ProductAPIBaseEndpointResourceKey = "http://ourcompany.com/api/",
                    ProductAPIErrorReportingEndpointResourceKey = "http://ourcompany.com/api/errors/",
                    ProductAPISupportEndpointResourceKey = "http://ourcompany.com/api/support/",
                    ProductAPILicensingEndpointResourceKey = "http://ourcompany.com/api/licenses",
                    ProductAPIUpdatesUvailableEndpointResourceKey = "http://ourcompany.com/api/verion",
                };
            //.Assembly = this.GetType().Assembly;

            productInformationServiceConfiguration.ResellerInformation = new ProductInformation
                {
                    CompanyName = "SomeReseller",

                    CompanyDisplayNameResourceKey = "Some Reseller, Inc.",
                    CompanyDisplayAddressResourceKey = "123 Watson Lane, San Fernando, CA 9000, USA",
                    CompanyDisplayContactInformationResourceKey = "(800) 765 4321"
                };
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }




        [Test]
        public void TestAttributes()
        {

            //ARRANGE:
            IProductInformationService productInformationService =
                DependencyResolver.Current.GetInstance<IProductInformationService>();
            
            //ACT:
            //NOtice that it was returning NULL in test mode. Google for it.
            //Solution is to set it to something you trust.

            string CompanyName = productInformationService.CompanyName();

            //ASSERT:
            Assert.AreEqual("SomeReseller",CompanyName);
        }




        [Test]
        public void GetApplicationCompany()
        {
            //ARRANGE:
            IProductInformationService productInformationService =
                DependencyResolver.Current.GetInstance<IProductInformationService>();
            
            //ACT:
            string result = productInformationService.CompanyDisplayNameResourceKey();

            //ASSERT:
            Assert.AreEqual("Some Reseller, Inc.", result);
        }

        [Test]
        public void GetApplicationDescription()
        {
            //ARRANGE:
            IProductInformationService productInformationService =
                DependencyResolver.Current.GetInstance<IProductInformationService>();

            //ACT:
            string result = productInformationService.ProductDisplayDescriptionResourceKey();

            //ASSERT:
            Assert.AreEqual("An app to waste your time, everytime.",result);
        }

        [Test]
        public void GetApplicationTitle()
        {

            //ARRANGE:
            IProductInformationService productInformationService =
    DependencyResolver.Current.GetInstance<IProductInformationService>();

            //ACT:
            string result = productInformationService.ProductDisplayTitleResourceKey();

            //ASSERT:
            Assert.AreEqual("Some App", result);
        }




    }


}


