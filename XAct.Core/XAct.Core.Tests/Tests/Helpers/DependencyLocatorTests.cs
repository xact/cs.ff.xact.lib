﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Diagnostics;
    using XAct.Services;

    [TestFixture]
    public class DependencyLocatorTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        //...Tests go here...
        [Test]
        public void FindAServiceThatWillBeThere()
        {
            //ARRANGE

            //ACT
            ISomeTestServiceA someTestServiceA = XAct.DependencyResolver.Current.GetInstance<ISomeTestServiceA>();

            //ASSERT:
            Assert.IsNotNull(someTestServiceA);
        }



        [Test]
        public void FindAServiceThatWillBeThereAndHasADependency()
        {
            //ARRANGE

            //ACT
            ISomeTestServiceB someTestServiceB = XAct.DependencyResolver.Current.GetInstance<ISomeTestServiceB>();
            
            //ASSERT:
            Assert.IsNotNull(someTestServiceB);
        }

        [Test]
        public void InstantiateServiceDirectlyFromInstance()
        {
            //ARRANGE

            //ACT
            ISomeTestServiceA someTestServiceA = XAct.DependencyResolver.Current.GetInstance<SomeTestServiceA>();

            //ASSERT
            Assert.IsNotNull(someTestServiceA);
        }

        [Test]
        public void InstantiateServiceThatHasDependencyDirectlyFromInstance()
        {
            //ARRANGE

            //ACT
            ISomeTestServiceB someTestServiceB = XAct.DependencyResolver.Current.GetInstance<SomeTestServiceB>();

            //ASSERT
            Assert.IsNotNull(someTestServiceB);
        }
        [Test]
        public void InstantiateServiceDirectlyFromInstanceByType()
        {
            //ARRANGE

            //ACT
            ISomeTestServiceA someTestServiceA = XAct.DependencyResolver.Current.GetInstance(typeof(SomeTestServiceA)) as ISomeTestServiceA;

            //ASSERT
            Assert.IsNotNull(someTestServiceA);
        }

        [Test]
        public void InstantiateServiceThatHasDependencyDirectlyFromInstanceByType()
        {
            //ARRANGE

            //ACT
            ISomeTestServiceB someTestServiceB = XAct.DependencyResolver.Current.GetInstance(typeof(SomeTestServiceB)) as ISomeTestServiceB;

            //ASSERT
            Assert.IsNotNull(someTestServiceB);
        }
        [Test]
        public void InstantiateServiceDirectlyFromInstanceByType2()
        {
            //ARRANGE

            //ACT
            ISomeTestServiceA someTestServiceA = (ISomeTestServiceA)XAct.DependencyResolver.Current.GetInstance(typeof(SomeTestServiceA));

            //ASSERT
            Assert.IsNotNull(someTestServiceA);
        }

        [Test]
        public void InstantiateServiceThatHasDependencyDirectlyFromInstanceByType2()
        {
            //ARRANGE

            //ACT
            ISomeTestServiceB someTestServiceB = (ISomeTestServiceB)XAct.DependencyResolver.Current.GetInstance(typeof(SomeTestServiceB));

            //ASSERT
            Assert.IsNotNull(someTestServiceB);
        }

        [Test]
        public void FindAService()
        {
            //ARRANGE

            //ACT
            ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<ITracingService>();

            //ASSERT
            Assert.IsNotNull(tracingService);
        }


        [Test]
        public void FindAServiceByInstance()
        {
            //ARRANGE:

            //ACT:
            ITracingService tracingService = XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Implementations.SystemDiagnosticsTracingService>();

            //ASSERT:
            Assert.IsNotNull(tracingService);
        }



    }


    public interface ISomeTestServiceA :IHasXActLibService
    {
        string Foo();
    }

    public class SomeTestServiceA:ISomeTestServiceA
    {
        public string Foo()
        {
            return "Bar";
        }
    }


    public interface ISomeTestServiceB: IHasXActLibService
    {
        string Foo();
    }

    public class SomeTestServiceB : ISomeTestServiceB
    {
        private readonly ISomeTestServiceA _somedependency;

        public SomeTestServiceB(ISomeTestServiceA somedependency)
        {
            _somedependency = somedependency;
        }

        public string Foo()
        {
            return "Bar";
        }
    }





}