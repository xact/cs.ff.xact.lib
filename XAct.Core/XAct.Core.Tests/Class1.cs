﻿/*
After scrounging around the net for tips, got the bits and pieces I needed, 
and am posting in case anybody else needs the info at some point. 

Basically, its a combination of several techniques: 
a) Need a P/Invoke to get the entry assembly 
b) Version is gotten from GetName() of Assembly returned 
c) For the xtra little touch, use reflection to look for Attributes that 
were defined in AssemblyInfo.cs 

Voila. 

Now I just need some figuring out how MS converted the assembly Evidence and 
Evidence hash into the string fragment they use to create a folder entryAssemblyName for 
userSettings, and bob's your uncle. Anybody want to help out on that piece? 

 
namespace XAct.Core {

    public static class X {
        public static System.Reflection.Assembly EntryAssembly;
        public static string ProgramName = string.Empty;
        public static string Company = string.Empty;
        public static string copyright = string.Empty;
        public static string Description = string.Empty;
        public static string Product = string.Empty;
        public static string TradeMark = string.Empty;
        public static string Configuration = string.Empty;

        public void FindOut() {

            EntryAssembly = GetEntryAssembly();
            string entryAssemblyPath = a.ManifestModule.FullyQualifiedName;
            string entryAssemblyName = a.ManifestModule.Name;

            string shortName = System.IO.Path.GetFileNameWithoutExtension(entryAssemblyPath);
            //Took me a second to notice this little detail for making 
            //userSettings storage: 
            if (shortName.Length > 25) {
                shortName = shortName.Substring(0, 25);
            }
            //Get Version of Attribute:
            string version = a.GetName().Version.ToString();

            //Get all attributes of Assembly:
            object[] attrs = a.GetCustomAttributes(true);

            //Loop through them:
            foreach (object attr in attrs) {
                Type attrType = attr.GetType();
                if (attr is System.Reflection.AssemblyTitleAttribute) {
                    ProgramName =
                    ((System.Reflection.AssemblyTitleAttribute)attr).Title;
                    //System.Reflection.AssemblyVersionAttribute; 
                    //System.Reflection.AssemblyFileVersionAttribute; 
                }

                if (attr is System.Reflection.AssemblyCompanyAttribute) {
                    Company =
                    ((System.Reflection.AssemblyCompanyAttribute)attr).Company;
                }
                if (attr is System.Reflection.AssemblyProductAttribute) {
                    Product =
                    ((System.Reflection.AssemblyProductAttribute)attr).Product;
                }
                if (attr is System.Reflection.AssemblyDescriptionAttribute) {
                    Description =
                    ((System.Reflection.AssemblyDescriptionAttribute)attr).Description;
                }
                if (attr is System.Reflection.AssemblyCopyrightAttribute) {
                    copyright =
                    ((System.Reflection.AssemblyCopyrightAttribute)attr).Copyright;
                }
                if (attr is System.Reflection.AssemblyTrademarkAttribute) {
                    TradeMark =
                    ((System.Reflection.AssemblyTrademarkAttribute)attr).Trademark;
                }
                if (attr is System.Reflection.AssemblyConfigurationAttribute) {
                    Configuration =
                    ((System.Reflection.AssemblyConfigurationAttribute)attr).Configuration;
                }
            }//foreach attributes

            //string evType = System.AppDomain.CurrentDomain.Evidence.ToString(); 
            //Application.Run(new Form1()); 
        }

        public static System.Reflection.Assembly GetEntryAssembly() {

#if (!PocketPC) && (!pocketPC) && (!WindowsCE)
            return System.Reflection.Assembly.GetEntryAssembly();
#else 
            System.Text.StringBuilder sb = new System.Text.StringBuilder(0x100 * 
            System.Runtime.InteropServices.Marshal.SystemDefaultCharSize); 
            int returnedSize = GetModuleFileName(IntPtr.Zero, sb, 0xff); 

            if (returnedSize <= 0) { 
            return null; 
            } 
            if (returnedSize > 0xff) { 
            throw new System.Exception("Returned Assembly entryAssemblyName longer than 
            MAX_PATH chars."); 
            } 
            return System.Reflection.Assembly.LoadFrom(sb.ToString()); 
#endif
        }

        #region API

        private class API {
#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
            //PC:
            [System.Runtime.InteropServices.DllImport("kernel32.dll",
               SetLastError = true)]
#else 
                //CE:
                [System.Runtime.InteropServices.DllImport("coredll.dll", 
                  SetLastError = true)] 
#endif
            private static extern int GetModuleFileName(IntPtr hModule,
              System.Text.StringBuilder lpFilename, int nSize);
        }
        #endregion API 

    }//Class:End
}//Namespace:End

*/