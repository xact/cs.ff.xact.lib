﻿namespace XAct.Tests
{
    using System;
    using System.Text;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;

    [TestFixture]
    public class TestCryptographicHashes
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                                    Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Test_MD5_All_Possibilities()
        {
            Encoding[] encoders =
                new[]
                    {
                        Encoding.ASCII, Encoding.UTF7, Encoding.UTF8, Encoding.UTF32, Encoding.Unicode,
                        Encoding.BigEndianUnicode, Encoding.Default
                    };
            string[] texts = new[] {"hello", "La Defense", "La Défense"};

            foreach (Encoding encoder in encoders)
            {
                Console.WriteLine("---");
                foreach (string text in texts)
                {
                    string result = text.CalculateHash(encoder, "MD5");
                    string s = "{0} [{1}, {2}]".FormatStringInvariantCulture(result, text, encoder);
                    Console.WriteLine(s);
                }
            }

/*
---
5D41402ABC4B2A76B9719D911017C592 [hello, System.Text.ASCIIEncoding]
5FA39A733B79D60627E00A62AEEBE8A3 [La Defense, System.Text.ASCIIEncoding]
ACBA3AD79835A2ED875F7BDA6746FD5C [La Défense, System.Text.ASCIIEncoding]
---
5D41402ABC4B2A76B9719D911017C592 [hello, System.Text.UTF7Encoding]
5FA39A733B79D60627E00A62AEEBE8A3 [La Defense, System.Text.UTF7Encoding]
7AA3E135D5F8CA0EA7D2D8B895F0A186 [La Défense, System.Text.UTF7Encoding]
---
5D41402ABC4B2A76B9719D911017C592 [hello, System.Text.UTF8Encoding]
5FA39A733B79D60627E00A62AEEBE8A3 [La Defense, System.Text.UTF8Encoding]
FDC1F64520302DD82236C68DFC0362DA [La Défense, System.Text.UTF8Encoding]
---
A6F145A01AD0127E555C051D15806EB5 [hello, System.Text.UTF32Encoding]
A36FD23797FB8D648CD2B2125064D13D [La Defense, System.Text.UTF32Encoding]
C94618A06189EB82E53C05A5D3604FA8 [La Défense, System.Text.UTF32Encoding]
---
FD186DD49A16B1BF2BD2F44E495E14C9 [hello, System.Text.UnicodeEncoding]
A08B28403340D4383A4646EB5D8AF519 [La Defense, System.Text.UnicodeEncoding]
8E9D8AF595C80B5E2907CD4D3005BCEB [La Défense, System.Text.UnicodeEncoding]
---
A009BCCF13CA2631D3982CD37FBDCD8B [hello, System.Text.UnicodeEncoding]
03C19AC0D976518CD4ECCA6F1F7F6B38 [La Defense, System.Text.UnicodeEncoding]
C966F7AC3B780D6F19021EF62A11D167 [La Défense, System.Text.UnicodeEncoding]
---
5D41402ABC4B2A76B9719D911017C592 [hello, System.Text.SBCSCodePageEncoding]
5FA39A733B79D60627E00A62AEEBE8A3 [La Defense, System.Text.SBCSCodePageEncoding]
2D445DB41DA3DC087B35CA16F6FF8A45 [La Défense, System.Text.SBCSCodePageEncoding]
             */
        }

        //...Tests go here...
        [Test]
        public void Test_Sha1()
        {
            string plainText = "La Défense";
            string hash1 = plainText.CalculateHash(Encoding.Default, "SHA1");

            Console.WriteLine(hash1);

            string hash2 = plainText.CalculateHash(Encoding.Default, "SHA1");


            Assert.AreEqual(hash1, hash2);
        }
    }


}


