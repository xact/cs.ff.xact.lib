﻿namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Messages;

    [TestFixture]
    public class TestsMessages
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                                    Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        //...Tests go here...
        [Test]
        public void MessageCodesAreNotEqual()
        {
            MessageCode messageCode =new MessageCode(12,Severity.Info);

            MessageCode messageCode2 = new MessageCode(13, Severity.Info);

            Assert.AreNotEqual(messageCode,messageCode2);
        }
        //...Tests go here...
        [Test]
        public void MessageCodesAreEqual()
        {
            MessageCode messageCode = new MessageCode(12, Severity.Info);

            MessageCode messageCode2 = new MessageCode(12, Severity.Info);

            Assert.AreEqual(messageCode, messageCode2);
        }
    }


}


