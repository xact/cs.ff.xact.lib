﻿namespace XAct.Tests
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Security.Cryptography;
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;
    using XAct.Security.Cryptography;

    [TestFixture]
    public class TestsSymmetricAlgorithms
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                                    Singleton<IocContext>.Instance.ResetIoC();

            CreateTestFile();

        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        private const string plainText1 =
            @"L'Espagnol Carlos Sastre (Team CSC) s'est imposé mercredi dans 
            la 17e étape du Tour de France entre Embrun et L'Alpe 
            d'Huez (210,5 km) et a endossé le maillot jaune. 
            Samuel Sanchez (Euskaltel) finit deuxième de l'étape 
            avec 2'03"" de retard et Andy Schleck (Team CSC) 
            termine troisième 
            dans le même temps. 
            Au classement général, Carlos Sastre 
            possède 1'24"" d'avance sur son équipier 
            Frank Schleck, 1'33"" sur Bernhard Kohl 
            (Gerolsteiner) et 1'34"" sur Cadel 
            Evans (Silence-Lotto), sixième de l'étape.";

        private const string plainText3 =
            @"· 胡同深处的非著名烤鸭店(图) 
            · 老北京十大最难以忘怀的零嘴 
            · 皇城根儿的美味烤鸭(图) 
            · 姜丝排叉：";

        private const string plainText2 = @"A very simple string.";


        //method

        private void TestEncryption(string algorithmName,
                                    CipherMode cipherMode,
                                    PaddingMode paddingMode)
        {
            Trace.WriteLine(
                "==================================================");
            Trace.WriteLine(
                "{0} [{1}/{2}]".FormatStringInvariantCulture(algorithmName, cipherMode, paddingMode));

            /*
      System.Diagnostics.Trace.WriteLine(
        string.Format("Plain Text [{0}]:", plainText.Length));
      System.Diagnostics.Trace.WriteLine(plainText);
      */

            foreach (string plainText in
                new[] {plainText1, plainText2, plainText3})
            {
                SymmetricAlgorithms sa =
                    new SymmetricAlgorithms(algorithmName, cipherMode, paddingMode);
                sa.Initialize("MY_PWD", null, null);


                string cipherText = sa.Encrypt(plainText);
                Trace.WriteLine(cipherText);


                sa = new SymmetricAlgorithms(algorithmName, cipherMode, paddingMode);
                sa.Initialize("MY_PWD", null, null);

                string decryptedText = sa.Decrypt(cipherText);


                Trace.WriteLine("Decrypted Text [{0}]:".FormatStringInvariantCulture(decryptedText.Length));
                Trace.WriteLine(decryptedText);

                Assert.AreNotEqual(plainText, cipherText);
                Assert.AreEqual(decryptedText, plainText);
            }
        }


        //...Tests go here...
        //method


        private void TestEncryption2(string algorithmName,
                                     CipherMode cipherMode,
                                     PaddingMode paddingMode)
        {
            Trace.WriteLine(
                "==================================================");
            Trace.WriteLine("{0} [{1}/{2}]".FormatStringInvariantCulture(algorithmName, cipherMode, paddingMode));

            /*
      System.Diagnostics.Trace.WriteLine(
        "Plain Text [{0}]:".FormatStringInvariantCulture(plainText.Length));
      System.Diagnostics.Trace.WriteLine(plainText);
      */


            SymmetricAlgorithms sa =
                new SymmetricAlgorithms(algorithmName, cipherMode, paddingMode);
            sa.Initialize("MY_PWD", null, null);


            sa.Encrypt(SymAlgoTestFile1, SymAlgoTestFile2);


            sa = new SymmetricAlgorithms(algorithmName, cipherMode, paddingMode);
            sa.Initialize("MY_PWD", null, null);

            sa.Decrypt(SymAlgoTestFile2, SymAlgoTestFile3);


            FileInfo fi1 = new FileInfo(SymAlgoTestFile1);
            FileInfo fi3 = new FileInfo(SymAlgoTestFile3);


            Assert.AreEqual(fi1.Length, fi3.Length);

            Trace.WriteLine("Src Size: {0}".FormatStringInvariantCulture(fi1.Length));
            Trace.WriteLine("Dest Size: {0}".FormatStringInvariantCulture(fi3.Length));
        }

        private string SymAlgoTestFile1 = @"SymAlgoTextTest.txt";
        private string SymAlgoTestFile2 = @"SymAlgoTextTest.enc";
        private string SymAlgoTestFile3 = @"SymAlgoTextTest.dec";

        /*
      string SymAlgoTestFile1 = @"c:\worldmap.png";
      string SymAlgoTestFile2 = @"c:\worldmap.enc";
      string SymAlgoTestFile3 = @"c:\worldmap.dec.png";
    */

        public void CreateTestFile()
        {
            if (!File.Exists(SymAlgoTestFile1))
            {
                using (StreamWriter tw =
                    File.CreateText(SymAlgoTestFile1))
                {
                    for (int i = 0; i < 5000; i++)
                    {
                        foreach (string plainText in 
                            new[] {plainText1, plainText2, plainText3})
                        {
                            tw.WriteLine(plainText);
                        }
                    }
                }
            }
        }

        [Test]
        public void TestEncryption()
        {
            //XAct.Core.Utils.Diagnostics.SourceSwitch.Level =
            //  System.Diagnostics.SourceLevels.Verbose;

            //System.Diagnostics.Trace.Listeners.Clear();
            //System.Diagnostics.Trace.Listeners.Add(new System.Diagnostics.DefaultTraceListener());
            ////System.Diagnostics.Trace.Listeners.Add(new System.Diagnostics.ConsoleTraceListener());

            string[] algorithmNames = new[] {"DES", "TripleDES", "RC2", "Rijndael"};
            foreach (string algorithmName in algorithmNames)
            {
                foreach (CipherMode cipherMode in Enum.GetValues(typeof (CipherMode)))
                {
                    if ((algorithmName == "DES") && (cipherMode == CipherMode.OFB))
                    {
                        continue;
                    }
                    if ((algorithmName == "DES") && (cipherMode == CipherMode.CTS))
                    {
                        continue;
                    }
                    if ((algorithmName == "TripleDES") && (cipherMode == CipherMode.OFB))
                    {
                        continue;
                    }
                    if ((algorithmName == "TripleDES") && (cipherMode == CipherMode.CTS))
                    {
                        continue;
                    }
                    if ((algorithmName == "RC2") && (cipherMode == CipherMode.OFB))
                    {
                        continue;
                    }
                    if ((algorithmName == "RC2") && (cipherMode == CipherMode.CTS))
                    {
                        continue;
                    }
                    if ((algorithmName == "Rijndael") && (cipherMode == CipherMode.OFB))
                    {
                        continue;
                    }
                    if ((algorithmName == "Rijndael") && (cipherMode == CipherMode.CTS))
                    {
                        continue;
                    }

                    //CipherMode cipherMode = CipherMode.CBC;
                    //PaddingMode paddingMode = PaddingMode.ANSIX923; //[Works:PKCS7
                    foreach (PaddingMode paddingMode in Enum.GetValues(typeof (PaddingMode)))
                    {
                        if (paddingMode == PaddingMode.None)
                        {
                            continue;
                        }
                        if (paddingMode == PaddingMode.Zeros)
                        {
                            continue;
                        }
                        TestEncryption(algorithmName, cipherMode, paddingMode);
                    } //paddingMode
                } //cipherMode
            } //algorithmName
        }

        [Test]
        public void TestEncryption2()
        {
            string[] algorithmNames = new[] {"DES", "TripleDES", "RC2", "Rijndael"};
            foreach (string algorithmName in algorithmNames)
            {
                foreach (CipherMode cipherMode in
                    Enum.GetValues(typeof (CipherMode)))
                {
                    if ((algorithmName == "DES") && (cipherMode == CipherMode.OFB))
                    {
                        continue;
                    }
                    if ((algorithmName == "DES") && (cipherMode == CipherMode.CTS))
                    {
                        continue;
                    }
                    if ((algorithmName == "TripleDES") && (cipherMode == CipherMode.OFB))
                    {
                        continue;
                    }
                    if ((algorithmName == "TripleDES") && (cipherMode == CipherMode.CTS))
                    {
                        continue;
                    }
                    if ((algorithmName == "RC2") && (cipherMode == CipherMode.OFB))
                    {
                        continue;
                    }
                    if ((algorithmName == "RC2") && (cipherMode == CipherMode.CTS))
                    {
                        continue;
                    }
                    if ((algorithmName == "Rijndael") && (cipherMode == CipherMode.OFB))
                    {
                        continue;
                    }
                    if ((algorithmName == "Rijndael") && (cipherMode == CipherMode.CTS))
                    {
                        continue;
                    }

                    //CipherMode cipherMode = CipherMode.CBC;
                    //PaddingMode paddingMode = PaddingMode.ANSIX923; //[Works:PKCS7
                    foreach (PaddingMode paddingMode in Enum.GetValues(typeof (PaddingMode)))
                    {
                        if (paddingMode == PaddingMode.None)
                        {
                            continue;
                        }
                        if (paddingMode == PaddingMode.Zeros)
                        {
                            continue;
                        }
                        TestEncryption2(algorithmName, cipherMode, paddingMode);
                    } //paddingMode
                } //cipherMode
            } //algorithmName
        }
    }


}


