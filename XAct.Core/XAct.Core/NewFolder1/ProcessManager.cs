using System.Diagnostics;
using System.Text;
using System.Threading;

namespace XAct
{
    /// <summary>
    /// A class to run Windows processes.
    /// </summary>
    public class ProcessManager
    {
        /// <summary>
        /// Invoke a Windows process and get the output.
        /// </summary>
        /// <param name="exePath"></param>
        /// <param name="arguments"></param>
        /// <param name="workingDirectory"></param>
        /// <param name="timeoutSeconds"></param>
        /// <returns></returns>
        public static ProcessManagerResult InvokeProcess(string exePath, string arguments=null, string workingDirectory = null,
            int timeoutSeconds = 30)
        {

            var result = new ProcessManagerResult();

            ProcessStartInfo processStartInfo = new ProcessStartInfo();
            processStartInfo.LoadUserProfile = true;
            processStartInfo.UseShellExecute = false;
            processStartInfo.CreateNoWindow = true;
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.FileName = exePath;
            //process.StartInfo.LoadUserProfile = true;
            
            if (!arguments.IsNull())
            {
                processStartInfo.Arguments = arguments;
            }
            if (!workingDirectory.IsNull())
            {
                processStartInfo.WorkingDirectory = workingDirectory;
            }

            Process process = new Process();
            process.StartInfo = processStartInfo;

            process.Start();
            result.StandardOut = process.StandardOutput.ReadToEnd(); // pick up STDOUT
            result.StandardError = process.StandardError.ReadToEnd();  // pick up STDERR
            process.WaitForExit(timeoutSeconds * 1000);
            process.WaitForExit();




            //StringBuilder output = new StringBuilder();
            //StringBuilder error = new StringBuilder();

            //using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
            //using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            //{
            //    process.OutputDataReceived += (sender, e) =>
            //    {
            //        if (e.Data == null)
            //        {
            //            outputWaitHandle.Set();
            //        }
            //        else
            //        {
            //            output.AppendLine(e.Data);
            //        }
            //    };
            //    process.ErrorDataReceived += (sender, e) =>
            //    {
            //        if (e.Data == null)
            //        {
            //            errorWaitHandle.Set();
            //        }
            //        else
            //        {
            //            error.AppendLine(e.Data);
            //        }
            //    };

            //    process.Start();

            //    process.BeginOutputReadLine();
            //    process.BeginErrorReadLine();

            //    if (process.WaitForExit(timeoutSeconds*1000) &&
            //        outputWaitHandle.WaitOne(timeoutSeconds*1000) &&
            //        errorWaitHandle.WaitOne(timeoutSeconds*1000))
            //    {
            //        // Process completed. Check process.ExitCode here.
            //    }
            //    else
            //    {
            //        // Timed out.
            //    }
            //}
            result.ExitCode = process.ExitCode;
            result.ProcessExited = process.HasExited;
            process.Close();
            return result;
        }


    }
}