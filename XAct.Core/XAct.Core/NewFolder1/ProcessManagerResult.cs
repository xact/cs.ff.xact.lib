﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct
{
    /// <summary>
    /// A result package created when invoking a process.
    /// </summary>
    public class ProcessManagerResult
    {
        /// <summary>
        /// The exit code returned by the process.
        /// <para>
        /// By Convention (only) a successful run is associated to the value zero.
        /// </para>
        /// </summary>
        public int ExitCode { get; set; }
        /// <summary>
        /// If the process was waited for, return true if the process completed before timeout.
        /// </summary>
        public bool? ProcessExited { get; set; }
        /// <summary>
        /// Gets the standard output text.
        /// </summary>
        public string StandardOut { get; set; }
        /// <summary>
        /// Gets the standard error text if the process failed.
        /// </summary>
        public string StandardError { get; set; }
    }
}
