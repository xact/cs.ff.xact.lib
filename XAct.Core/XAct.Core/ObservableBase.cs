﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using System.ComponentModel.DataAnnotations;//Only FF4.0 & Silverlight 

namespace XAct
{
    public class ObservableBase : INotifyPropertyChanged, IEditableObject
    {

        #region INotifyPropertyChanged Members
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Fields

        protected IDictionary<string, object> _backupValues =
            new Dictionary<string, object>();

        protected IDictionary<string, object> _currentValues =
            new Dictionary<string, object>();
        #endregion


        
        #region Raise Events
        /// 
        /// Called when an obserable Property value is changed.
        /// 
        /// 
        /// 
        /// Example of usage:
        /// 
        /// 
        /// public string FirstName {
        ///  get {
        ///    return _FirstName;
        ///  }
        ///  set { 
        ///    if (value == _FirstName){return;}
        ///    _FirstName = value;
        ///    //Call with the name of this Property:
        ///    OnPropertyChanged("FirstName");
        ///    }
        /// }
        /// ]]>
        /// 
        /// 
        /// 
        /// Name of the property.
        protected virtual void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        
        /// 
        /// Raises the  event.
        /// 
        /// 
        /// The 
        ///  
        /// instance containing the event data.
        /// 
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
        #endregion





        #region IEditableObject Members
        /// 
        /// Causes the object to enter editing mode.
        /// 
        public virtual void BeginEdit()
        {
            CloneDictionary(_currentValues, _backupValues);
        }
        /// 
        /// Causes the object to leave editing mode and revert to the previous, unedited value.
        /// 
        public virtual void CancelEdit()
        {
            CloneDictionary(_backupValues, _currentValues);
        }
        /// 
        /// Causes the object to leave editing mode and commit the edited value.
        /// 
        public virtual void EndEdit()
        {
            CloneDictionary(_currentValues, _backupValues);
        }
        #endregion
        #region Protected Helper Methods
        protected virtual T GetPropertyValue<T>(string propertyName)
        {
            object result;
            _currentValues.TryGetValue(propertyName, out result);
            return (
                    (result != null)
                    &&
                    (Comparer.Default.Compare((T)result, default(T)) != 0))
                    ?
                    (T)result
                    :
                    default(T);
        }
        protected virtual void SetPropertyValue<T>(string propertyName, object value)
        {
            if (!Object.Equals(value, GetPropertyValue<T>(propertyName)))
            {
                ValidatePropertyValue(propertyName, value);
                _currentValues[propertyName] = value;
                OnPropertyChanged(propertyName);
            }
        }
        /// 
        /// Validates the value against any
        /// System.ComponentModel.DataAnnotations rules
        /// applied to the property.
        /// 
        /// Name of the property.
        /// The value.
        protected virtual void ValidatePropertyValue(string propertyName, object value)
        {
            ValidationContext validationContext =
              new ValidationContext(this, null, null);

            //point back to Property, and therefore to its Attributes.
            validationContext.MemberName = propertyName;
            //Kick in validation
            Validator.ValidateProperty(
              value, //The value to validate before setting field
              validationContext);
        }



        /// 
        /// Clones the dictionary.
        /// 
        /// 
        /// Invoked by 
        /// ,
        /// , 
        /// and
        /// 
        /// 
        /// The dictionary whose values are to be copied from.
        /// The dictionary the values are to be copied to.
        protected virtual void CloneDictionary(Dictionary<string, object> from, Dictionary<string, object> to)
        {
            to.Clear();
            foreach (KeyValuePair<string, object> pair in from)
            {
                to[pair.Key] = pair.Value;
            }
        }
        #endregion
    }
}
