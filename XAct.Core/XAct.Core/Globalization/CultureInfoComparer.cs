﻿using System;
using System.Collections;
using System.Globalization;

namespace XAct.Globalization
{
    /// <summary>
    /// A comparer of <see cref="CultureInfo"/>
    /// objects.
    /// </summary>
    public class CultureInfoComparer : IComparer
    {
        // Methods
        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// Value
        /// Condition
        /// Less than zero
        /// <paramref name="x"/> is less than <paramref name="y"/>.
        /// Zero
        /// <paramref name="x"/> equals <paramref name="y"/>.
        /// Greater than zero
        /// <paramref name="x"/> is greater than <paramref name="y"/>.
        /// </returns>
        /// <exception cref="T:System.ArgumentException">
        /// Neither <paramref name="x"/> nor <paramref name="y"/> implements the <see cref="T:System.IComparable"/> interface.
        /// -or-
        ///   <paramref name="x"/> and <paramref name="y"/> are of different types and neither one can handle comparisons with the other.
        ///   </exception>
        public int Compare(object x, object y)
        {
            if (((x == null) && (y == null)) || x.Equals(y))
            {
                return 0;
            }
            if (x.Equals(CultureInfo.InvariantCulture) || (y == null))
            {
                return -1;
            }
            if (y.Equals(CultureInfo.InvariantCulture) || (x == null))
            {
                return 1;
            }
            if (!(x is CultureInfo))
            {
                throw new ArgumentException("Can only compare CultureInfo objects.", "x");
            }
            string displayName = ((CultureInfo)x).DisplayName;
            if (!(y is CultureInfo))
            {
                throw new ArgumentException("Can only compare CultureInfo objects.", "y");
            }
            string strB = ((CultureInfo)y).DisplayName;
            return displayName.CompareTo(strB);
        }
    }
}

