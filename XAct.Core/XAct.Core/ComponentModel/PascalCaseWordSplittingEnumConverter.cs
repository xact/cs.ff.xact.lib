﻿
using System;
using System.ComponentModel;
using System.Text;

namespace XAct
{

    /// <summary>
    /// 
    /// </summary>
    public class PascalCaseWordSplittingEnumConverter : EnumConverter
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="PascalCaseWordSplittingEnumConverter" /> class.
        /// </summary>
        /// <param name="type">The type.</param>
        public PascalCaseWordSplittingEnumConverter(Type type)

            : base(type)
        {

        }

        /// <summary>
        /// Converts the given value object to the specified destination type.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="culture">An optional <see cref="T:System.Globalization.CultureInfo" />. If not supplied, the current culture is assumed.</param>
        /// <param name="value">The <see cref="T:System.Object" /> to convert.</param>
        /// <param name="destinationType">The <see cref="T:System.Type" /> to convert the value to.</param>
        /// <returns>
        /// An <see cref="T:System.Object" /> that represents the converted <paramref name="value" />.
        /// </returns>
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {

            if (destinationType == typeof(string))
            {

                string stringValue = (string)base.ConvertTo(context, culture, value, destinationType);

                stringValue = stringValue.SplitPCase();

                return stringValue;

            }

            return base.ConvertTo(context, culture, value, destinationType);

        }


    }
}
