﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using XAct.Services;

namespace XAct
{
    /// <summary>
    /// 
    /// </summary>
    public class Lib
    {
        /// <summary>
        /// The XAct Solutions, Inc. Signed Key
        /// </summary>
        public const string AssemblyKey = "9f1417dfe4ed073e";
        /// <summary>
        /// Gets or sets the default service lifespan.
        /// </summary>
        /// <value>
        /// The default service lifespan for a Stateless service is <see cref="ServiceLifetimeType.SingletonScope"/>.
        /// </value>
        public static ServiceLifetimeType DefaultServiceLifespan
        {
            get { return _defaultServiceLifespan; }
            set { _defaultServiceLifespan = value; }
        }
        private static ServiceLifetimeType _defaultServiceLifespan = ServiceLifetimeType.SingletonScope;


        /// <summary>
        /// Gets the service instance.
        /// <para>
        /// Note that this is just an expedient wrapper around:
        /// <code>
        /// XAct.Services.ServiceLocatorService.Current.GetInstance{IServiceInterface}();
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TServiceInterface">The type of the service interface.</typeparam>
        /// <returns></returns>
        public static TServiceInterface GetServiceInstance<TServiceInterface>()
        {
            return XAct.Services.ServiceLocatorService.Current.GetInstance<TServiceInterface>();
        }

        /// <summary>
        /// Initializes the library services.
        /// </summary>
        public static void InitializeLibraryServices()
        {
            //
            var check = ServiceLocatorService.GetMSPPServiceLocator<IServiceLocator>();

            if ((_iocInitializedLastTime == check))
            {
                //Same as last time, do not reinitialize:
                return;
            }

            //Save it:
            _iocInitializedLastTime = check;


            DateTime start = DateTime.Now;

            //Ensure all non-system assemblies are in memory, or else
            //they can't be scanned... controversial...I know...
            //Force load all assemblies:
            AppDomain.CurrentDomain.LoadAllReferencedAssemblies(true);
            //OBSOLETE:AppDomain.CurrentDomain.LoadAllAssembliesInDomain();

            //Now scan all assemblies for Class Def's that are marked with the 
            //right attribute:
            Type attributeType = typeof(DefaultServiceImplementationAttribute);

            IDictionary<Type, DefaultServiceImplementationAttribute> results =
                AppDomain.CurrentDomain.GetTypesDecoratedWithAttribute<DefaultServiceImplementationAttribute>(false);

            int servicesFound = results.Count;

            IServiceLocator serviceLocator = ServiceLocator.Current;

            //Register them all
            foreach (KeyValuePair<Type, DefaultServiceImplementationAttribute> pair in results)
            {
                Type interfaceType = pair.Value.ServiceInterface;
                Type instanceType = pair.Key;

                //NO: NOT POSSIBLE, As the class we are checking might
                //need constructor args not yet avaible...red herrring
                //object check = null;
                //try
                //{
                //    check = serviceLocator.GetInstance(interfaceType);
                //} catch
                //{
                //    check = null;
                //}
                //if (check != null)
                //{
                //    //it appears that developer has already registered a replacement for this service.
                //    //don't repeat it...
                //}else{
                //Good...it's still null, so nothing has been registered,
                    //Ok to proceed
                    ServiceLocatorService.RegisterServiceByReflection(
                        serviceLocator,
                        new ServiceRegistrationDescriptor(interfaceType, instanceType, pair.Value.LifeSpan));
                //}
            }

            TimeSpan elapsed = DateTime.Now.Subtract(start);

            ServiceLocatorService.Current.GetInstance<XAct.Diagnostics.ITracingService>().Trace("XAct.Lib.InitializeLibraryServices() (initialization of all Services) took {0}ms".FormatStringCurrentCulture(elapsed.TotalMilliseconds));
        }

        private static object _iocInitializedLastTime = -1;




    }
}