﻿namespace XAct.Diagnostics.Status.Connectors
{
    using XAct.Diagnostics.Status.Connectors.Implementations;

    /// <summary>
    /// 
    /// </summary>
    public class XmlDocumentStatusServiceConnectorFactory
    {
        /// <summary>
        /// Creates an instance of a <see cref="XmlDocumentStatusServiceConnector"/>
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="relativeFilePath"></param>
        /// <param name="xpathDefinitions"></param>
        /// <returns></returns>
        public static XmlDocumentStatusServiceConnector Create(string name, string title, string description, string relativeFilePath, params XmlDocumentStatusServiceConnectorConfigurationItem[] xpathDefinitions)
        {
            var statusServiceConnector =
                XAct.DependencyResolver.Current.GetInstance<XmlDocumentStatusServiceConnector>();

            statusServiceConnector.ConfigureBasicInformation(name, title, description);

            statusServiceConnector.Configuration.FilePath = relativeFilePath;

            statusServiceConnector.Configuration.XPathDefinitions.Add(xpathDefinitions);

            return statusServiceConnector;
        }

    }

}
