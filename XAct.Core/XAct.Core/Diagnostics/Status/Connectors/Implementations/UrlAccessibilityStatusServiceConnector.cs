﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Net;

    /// <summary>
    /// A StatusService Connector that reports on the ability to access remote Urls.
    /// </summary>
    public class UrlAccessibilityStatusServiceConnector : XActLibStatusServiceConnectorBase
    {
        /// <summary>
        /// The configuration package for this Connector.
        /// </summary>
        public UrlAccessibilityStatusServiceConnectorConfiguration Configuration { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlAccessibilityStatusServiceConnector"/> class.
        /// </summary>
        /// <param name="uriAccessibilityFeedServiceConnectorConfiguration">The URI accessibility feed service connector configuration.</param>
        public UrlAccessibilityStatusServiceConnector(
            UrlAccessibilityStatusServiceConnectorConfiguration uriAccessibilityFeedServiceConnectorConfiguration)
            : base()
        {
            Name = "UrlAccessibility";

            Configuration = uriAccessibilityFeedServiceConnectorConfiguration;
        }


        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments = null, DateTime? startTimeUtc = null,
                                           DateTime? endTimeUtc = null)
        {

            //Using the base helper ensures it gets the right Title/Description
            //in the user's culture:
            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);


            return result;
        }

        private void MakeTable(StatusResponseTable table, object arguments)
        {
            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:
            List<UrlAccessibilityStatusServiceConnectorConfigurationItem> urlsToQuery = Configuration.Urls;



            //Make headers:
            table.Headers.Add("Url");
            table.Headers.Add("Accessible");

            //Make rows:
            if (urlsToQuery == null)
            {
                return;
            }

            foreach (UrlAccessibilityStatusServiceConnectorConfigurationItem urlSetting in urlsToQuery)
            {
                var row = MakeTableRow(urlSetting);
                table.Rows.Add(row);
            }
        }

        private StatusResponseTableRow MakeTableRow(UrlAccessibilityStatusServiceConnectorConfigurationItem urlSetting)
        {

            StatusResponseTableRow row = new StatusResponseTableRow();

            row.Cells.Add(urlSetting.Url.ToString());

            //if (!Uri.TryCreate(value, UriKind.RelativeOrAbsolute, out uri))
            //{
            //    continue;
            //}


            // Got an URI, try hitting
            using (
                var webClient =
                    new TimeoutWebClient(
                        TimeSpan.FromMilliseconds(Math.Min(this.Configuration.Timeout.TotalMilliseconds,
                                                           urlSetting.Timeout.TotalMilliseconds))))
            {
                try
                {
                    webClient.DownloadString(urlSetting.Url);
                    row.Cells.Add("OK");
                    row.Status = ResultStatus.Success;
                }
#pragma warning disable 168
                catch (Exception e)
#pragma warning restore 168
                {
                    row.Status = ResultStatus.Fail;
                    row.Cells.Add("Err");
                }
            }

            return row;
        }
    }
}