﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System.Collections.Generic;

    /// <summary>
    /// Configuration for <see cref="XmlDocumentStatusServiceConnector"/>
    /// </summary>
    public class XmlDocumentStatusServiceConnectorConfiguration : IHasTransientBindingScope
    {
        /// <summary>
        /// Gets or sets the absolute or relative file path to the XmlDocument.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets the list of XPaths to investigate within the document.
        /// </summary>
        public List<XmlDocumentStatusServiceConnectorConfigurationItem> XPathDefinitions
        {
            get
            {
                var result = _xPaths ?? (_xPaths = new List<XmlDocumentStatusServiceConnectorConfigurationItem>());
                return result;
            }
        }
        private List<XmlDocumentStatusServiceConnectorConfigurationItem> _xPaths;


    }
}