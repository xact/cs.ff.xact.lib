﻿
namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    
    /// <summary>
    /// 
    /// </summary>
    public class XmlDocumentStatusServiceConnectorConfigurationItem :IHasTitle
    {
        /// <summary>
        /// Gets or sets the displayable Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The XPath to the setting.
        /// </summary>
        public string XPath { get; set; }



        /// <summary>
        /// XMLs the document status service connector x path definition.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="xpath">The xpath.</param>
        /// <returns></returns>
        public XmlDocumentStatusServiceConnectorConfigurationItem(string title, string xpath)
        {
            Title = title;
            XPath = xpath;
        }
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var result = "[Title:{0}, Url:{1}]"
                .FormatStringInvariantCulture(this.Title, XPath);
            return result;
        }

    }
}
