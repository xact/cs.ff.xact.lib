﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;


    /// <summary>
    /// The configuration package for an instance of
    /// <see cref="UrlAccessibilityStatusServiceConnector"/>
    /// </summary>
    public class UrlAccessibilityStatusServiceConnectorConfiguration : IHasTransientBindingScope
    {
        /// <summary>
        /// Gets or sets the max time to wait for a response.
        /// </summary>
        /// <value>
        /// The timeout.
        /// </value>
        public TimeSpan Timeout { get; set; }

        /// <summary>
        /// Gets the list of urls to check.
        /// </summary>
        /// <value>
        /// The urls.
        /// </value>
        public List<UrlAccessibilityStatusServiceConnectorConfigurationItem> Urls
        {
            get
            {
                var result = _urls ?? (_urls = new List<UrlAccessibilityStatusServiceConnectorConfigurationItem>());
                return result;
            }
        }
        private List<UrlAccessibilityStatusServiceConnectorConfigurationItem> _urls;

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlAccessibilityStatusServiceConnectorConfiguration"/> class.
        /// </summary>
        public UrlAccessibilityStatusServiceConnectorConfiguration()
        {
            Timeout = TimeSpan.FromSeconds(30);
        }
    }
}