﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Diagnostics.Status.Connectors;
    using XAct.Environment;
    using XAct.IO;
    using XAct.Resources;

    /// <summary>
    /// An implementation of <see cref="IStatusServiceConnector"/>
    /// that returns the values of AppSettings on the Server.
    /// </summary>
    public class XmlDocumentStatusServiceConnector : XActLibStatusServiceConnectorBase
    {
        private readonly IEnvironmentService _environmentService;
        private readonly IIOService _ioService;
        private XmlDocument _xmlDocument;

        /// <summary>
        /// Gets or sets the configuration package for this Connector.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public XmlDocumentStatusServiceConnectorConfiguration Configuration { get; private set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="Connectors.Implementations.XmlDocumentStatusServiceConnector" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="ioService">The io service.</param>
        /// <param name="connectorConfiguration">The connector configuration.</param>
        public XmlDocumentStatusServiceConnector(IEnvironmentService environmentService, IIOService ioService,
                                                 XmlDocumentStatusServiceConnectorConfiguration connectorConfiguration)
            : base()
        {
            _environmentService = environmentService;
            _ioService = ioService;

            Name = "XmlDocument";
            Configuration = connectorConfiguration;
        }

        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments = null, DateTime? startTimeUtc = null,
                                           DateTime? endTimeUtc = null)
        {
            _xmlDocument = new XmlDocument();

            string fileName = _environmentService.MapPath(this.Configuration.FilePath);


            //NOT UNTIL I FIGURE OUT WHY ASYNC IS NOT WORKING WITHIN MVC CONTEXT (HANGS)
            //if (_ioService.FileExistsAsync(fileName).WaitAndGetResult())
            if (System.IO.File.Exists(fileName))
            {
                _xmlDocument.Load(fileName);
            }


            StatusResponse result = base.BuildReponseObject();

            MakeTable(result.Data, arguments);

            return result;
        }


        private void MakeTable(StatusResponseTable table, object arguments)
        {

            _xmlDocument = new XmlDocument();

            //The list of files this connector checks
            //was probably created when the Connector
            //was initialized and registered, but can be coming in as arguments:
            List<XmlDocumentStatusServiceConnectorConfigurationItem> xpathDefinitions = Configuration.XPathDefinitions;

            //Create Headers whether there are rows or not:
            table.Headers.Add("Key");
            table.Headers.Add("Value");

            //Make rows:
            if (xpathDefinitions == null)
            {
                return;
            }

            Dictionary<string, string> xpaths = new Dictionary<string, string>();
            Configuration.XPathDefinitions.ForEach(x => xpaths.Add(x.Title, x.XPath));

            var xpathValues = _xmlDocument.ReadXPathValues(Configuration.FilePath, xpaths);


            foreach (var xpath in xpathDefinitions)
            {
                table.Rows.Add(MakeTableRow(xpath.Title, xpathValues[xpath.Title]));
            }
        }

        private StatusResponseTableRow MakeTableRow(string title, string value)
        {
            StatusResponseTableRow row = new StatusResponseTableRow();


            //Cell #1:

            row.Cells.Add(title);

            //Cell #2:
            row.Cells.Add(value??"[NULL]");

            //Either way, it's ok
            row.Status = ResultStatus.Success;

            return row;
        }
    }
}