﻿namespace XAct.Diagnostics.Status.Connectors.Implementations
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class UrlAccessibilityStatusServiceConnectorConfigurationItem :IHasTitle
    {

        /// <summary>
        /// The displayable title (often just set to the Url).
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the URL to test.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public Uri Url { get; set; }

        /// <summary>
        /// Expected Response. Default is 200 (OK)
        /// </summary>
        public int ExpectedHttpResponseCode { get; set; }


        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        /// <value>
        /// The timeout.
        /// </value>
        public TimeSpan Timeout { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlAccessibilityStatusServiceConnectorConfigurationItem"/> class.
        /// </summary>
        public UrlAccessibilityStatusServiceConnectorConfigurationItem()
        {
            ExpectedHttpResponseCode = 200;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlAccessibilityStatusServiceConnectorConfigurationItem" /> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="expectedResponse">The expected response.</param>
        /// <param name="timeOut">The time span.</param>
        public UrlAccessibilityStatusServiceConnectorConfigurationItem(string url, int expectedResponse=200, TimeSpan timeOut=default(TimeSpan))
            : this(new Uri(url), expectedResponse,timeOut)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="UrlAccessibilityStatusServiceConnectorConfigurationItem" /> class.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="expectedResponse">The expected response.</param>
        /// <param name="timeOut">The time span.</param>
        public UrlAccessibilityStatusServiceConnectorConfigurationItem(Uri uri, int expectedResponse = 200, TimeSpan timeOut = default(TimeSpan))
        {
            Url = uri;
            ExpectedHttpResponseCode = 200;
            if (timeOut == TimeSpan.Zero)
            {
                timeOut = TimeSpan.FromSeconds(30);
            }
            Timeout = timeOut;
            ExpectedHttpResponseCode = expectedResponse;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var result = "[Title:{0}, Url:{1}, Timeout:{2}ms, ExpectedHttpResponseCode:{3}]"
                .FormatStringInvariantCulture(this.Title, Url, Timeout.TotalMilliseconds, ExpectedHttpResponseCode);
            return result;
        }
    }
}