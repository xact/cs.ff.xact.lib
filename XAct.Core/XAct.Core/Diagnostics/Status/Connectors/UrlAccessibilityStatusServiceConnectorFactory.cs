﻿
namespace XAct.Diagnostics.Status.Connectors
{
    using System;
    using XAct.Diagnostics.Status.Connectors.Implementations;

    /// <summary>
    /// A Factory for creating <see cref="UrlAccessibilityStatusServiceConnector"/>
    /// instances.
    /// </summary>
    public class UrlAccessibilityStatusServiceConnectorFactory
    {
        

        /// <summary>
        /// Creates a <see cref="UrlAccessibilityStatusServiceConnector"/>
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        /// <param name="urlInfos">The urls.</param>
        /// <returns></returns>
        public static UrlAccessibilityStatusServiceConnector Create(string name, string title, string description,
                                                                    params UrlAccessibilityStatusServiceConnectorConfigurationItem[] urlInfos)
        {
            var statusServiceConnector =
               XAct.DependencyResolver.Current.GetInstance<UrlAccessibilityStatusServiceConnector>();

            statusServiceConnector.ConfigureBasicInformation(name, title, description);

            statusServiceConnector.Configuration.Urls.Add(urlInfos);

            return statusServiceConnector;
        }
    }

}
