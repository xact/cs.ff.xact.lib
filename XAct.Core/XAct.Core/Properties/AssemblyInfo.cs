using System.Runtime.InteropServices;
using System.Reflection;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.


[assembly: AssemblyTitle("XAct.Core")]
[assembly: AssemblyDescription("An XActLib assembly: a base assembly that almost all other XActLib assemblies have a reference. Contains mostly ExtensionMethods, essential common interfaces, enums, and collections, but contains a couple of essential Services used by all other assemblies.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("XAct Software Solutions, Inc.")]
[assembly: AssemblyProduct("XActLib")]
[assembly: AssemblyCopyright("Copyright © XAct Software Solutions, Inc. 2007")]
[assembly: AssemblyTrademark("XAct Software Solutions, Inc. 2007")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

//PLC: [assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

//PLC: [assembly: Guid("46051122-c2e3-4156-8541-250317e214ab")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.0.0.0")]

#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//PC:

[assembly: AssemblyFileVersion("1.0.0.0")]
#endif


//Locked down CAS rights:
//[assembly: ReflectionPermission(SecurityAction.RequestOptional, Flags=ReflectionPermissionFlag.NoFlags,MemberAccess=false,Unrestricted=false, RestrictedMemberAccess=false)]



[assembly: NeutralResourcesLanguage("en")]
[assembly: ComVisible(false)]

//[assembly:
//    InternalsVisibleTo(
//        "XAct.Core.Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100932be739f8653b856ef99aab6fe0d3cc40dd4029d7f2de80660ff2db162d7deb9805ff2bb2e4cf3b071fb1f9ff645de186a8027a55df30662a95cbde6294743f65813b808dfce516ebb673b0dc60220d9d75f0ee59d8fb78a981543a5a62d2ae17a196be7697482500d948748850b06a22509123f66c5f9ae720a249668045d0"
//        )]
//[assembly:
//    InternalsVisibleTo(
//        "XAct.Core.Explorables, PublicKey=0024000004800000940000000602000000240000525341310004000001000100932be739f8653b856ef99aab6fe0d3cc40dd4029d7f2de80660ff2db162d7deb9805ff2bb2e4cf3b071fb1f9ff645de186a8027a55df30662a95cbde6294743f65813b808dfce516ebb673b0dc60220d9d75f0ee59d8fb78a981543a5a62d2ae17a196be7697482500d948748850b06a22509123f66c5f9ae720a249668045d0"
//        )]



//Begin:SIGNED
#if SIGNED
//Allow this signed assembly access from Partially trusted: http://bit.ly/vyFFTu
[assembly: System.Security.AllowPartiallyTrustedCallers] //Only If Signed
#if NET40
//When Ninject3 running in a .NET40 environment starts causing errors due to demanding
//more precise security. Can't do it, so added this:
[assembly: System.Security.SecurityRules(System.Security.SecurityRuleSet.Level1)] //Only If Signed
//See note in NinjectMvcBootstrapper regarding Security.Critical use of IKernel

Example error:
...is marked with the AllowPartiallyTrustedCallersAttribute, and 
uses the level 2 security transparency model.  
Level 2 transparency causes all methods in AllowPartiallyTrustedCallers 
assemblies to become security transparent by default, which may be the cause of this exception.

#endif
#endif
//End:SIGNED



