﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18051
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XAct.Properties {
    using System;
    using System.Reflection;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("XAct.Properties.Resources", typeof(Resources).GetTypeInfo().Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enum value not found..
        /// </summary>
        public static string EnumValueNotFound {
            get {
                return ResourceManager.GetString("EnumValueNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A value is required..
        /// </summary>
        public static string ErrMsg_AValueIsRequired {
            get {
                return ResourceManager.GetString("ErrMsg_AValueIsRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Argument &apos;{0}&apos; cannot be not initialized (&apos;{1}&apos;)..
        /// </summary>
        public static string ErrMsgArgumentCannotBeNotInitialized {
            get {
                return ResourceManager.GetString("ErrMsgArgumentCannotBeNotInitialized", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Argument &apos;{0}&apos; cannot be null..
        /// </summary>
        public static string ErrMsgArgumentCannotBeNull {
            get {
                return ResourceManager.GetString("ErrMsgArgumentCannotBeNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Argument &apos;{0}&apos; cannot be null or Empty..
        /// </summary>
        public static string ErrMsgArgumentCannotBeNullOrEmpty {
            get {
                return ResourceManager.GetString("ErrMsgArgumentCannotBeNullOrEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Argument &apos;{0}&apos; Enum Value cannot be Undefined (0)..
        /// </summary>
        public static string ErrMsgArgumentEnumValueCannotBeUndefined {
            get {
                return ResourceManager.GetString("ErrMsgArgumentEnumValueCannotBeUndefined", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Argument &apos;{0}&apos; Length cannot be 0..
        /// </summary>
        public static string ErrMsgArgumentLengthCannotBeZero {
            get {
                return ResourceManager.GetString("ErrMsgArgumentLengthCannotBeZero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Argument (of type &apos;{0}&apos;) is not of right Type (&apos;{1}&apos;)..
        /// </summary>
        public static string ErrMsgItemIsNotOfRightType {
            get {
                return ResourceManager.GetString("ErrMsgItemIsNotOfRightType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value cannot be null or Empty..
        /// </summary>
        public static string ErrMsgValueCannotBeNull {
            get {
                return ResourceManager.GetString("ErrMsgValueCannotBeNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to length must be &gt; 0.
        /// </summary>
        public static string Exception_LengthMustByLargerThanZero {
            get {
                return ResourceManager.GetString("Exception_LengthMustByLargerThanZero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot generate UniqueKey for Null objects..
        /// </summary>
        public static string ObjectExtensions_MakeUniqueIdentifier_Cannot_generate_UniqueKey_for_Null_objects_ {
            get {
                return ResourceManager.GetString("ObjectExtensions_MakeUniqueIdentifier_Cannot_generate_UniqueKey_for_Null_objects_" +
                        "", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DependencyInjectionContainer container not yet handled via Reflection..
        /// </summary>
        public static string ObjectExtensions_RegisterService_IoC_container_not_yet_handled_via_Reflection_ {
            get {
                return ResourceManager.GetString("ObjectExtensions_RegisterService_IoC_container_not_yet_handled_via_Reflection_", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        public static string String1 {
            get {
                return ResourceManager.GetString("String1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to length must be &gt; {0}.
        /// </summary>
        public static string StringLengthMustBeLargerThanX {
            get {
                return ResourceManager.GetString("StringLengthMustBeLargerThanX", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to *.
        /// </summary>
        public static string ValidatorWarningSymbol {
            get {
                return ResourceManager.GetString("ValidatorWarningSymbol", resourceCulture);
            }
        }
    }
}
