﻿Requirements are:
CONTRACTS_FULL as a precondition.
Namespace:
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment System.Diagnostics.Contracts.Contract

Read: http://bit.ly/foSNag

Cut/paste:

#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
using System.Diagnostics.Contracts;
#endif


#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
