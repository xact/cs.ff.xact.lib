﻿Having an Assembly analysed by FxCop (by turning on CodeAnalysis)
is great. But it does bring to attention stuff that sometimes you want to ignore.

Use annotation like this:
[SuppressMessage("Microsoft.Naming", "CA1710", Justification = "It's a list -- not just a collection.")]

or better yet, Right Click the error message so that it writes the exception to a
GlobalSuppressions.cs file in the root of the application -- that's tidier.
