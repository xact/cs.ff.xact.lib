﻿Regarding Business Entity Enums

The whole concept of keeping separate Externals versus Internals Entities 
is to have entities that are safe to expose to 3rd parties.
Wrapping them is a the only possible solution...which is not trivial btw (I'm thinking about 
when entities have collection properties of sub elements, that also have to be wrapped, 
on the fly), but doable.

But...once that solved...there's one more issue...property Types. 
Specifically...enums.

Classes, whether internal or external, are going to need Enums...so the LCD place to put them
is in the Externals assembly rather than the Internals assembly.

So that's where they should go.


And, btw, Enums...bug me.

I love their ability to add type checking to integer flags, but for a long while I wasn't sure sure of where to put them.

If put in the Repository layer, they're too low (UI shouldn't be referencing Repository layer).
If put in the Services layer, they're too high (Repositories shouldn't be looking up).
If put in a cross cutting assembly (such as this one), I don't like that they all end up grouped by Type, and not by Concern
(it really makes little sense to whip a new folder/namespace for just one Enum...even if I could add a a couple Extensions and Exceptions to 
the folder...

The answer is that to group them by Type (makes for easier navigation in Visual Studio) by 
their namespace is certainly by Concern. There is no such concern as Enums -- they just exist to describe Business Entities.
So hence the namespace being the same as the business Entities.


