﻿FAQ: Why Extensions? 

Answer: OO principles state that the an object should have 
high cohesion, interacting on only its own properties.
A helper class (eg: StringHelpers) manipulating strings (target object)
is more of an manager class than Object specific methods
(hence why so easily abused and break SRP).


What Namespace to use?
This took me a long long time to understand.
I kept on thinking old-school, wanting to put Extension methods in the 
same namespace as the entity they are decorating.
For example, I woudl think an Extension method to StringWriter should be in maybe something like XAct.IO.
I was wrong.
The Class is organised in the namespace it should be. 
ExtensionMethods are PART of the class. 
Hence, it doesn't matter where they are defined -- their namespace is not the defining factor - their object's namespace is the namespace of the object.
The namespace of the Extension method is not relevant. Nor is the wrapping syntatic static class in this case, as the Method is not part of the static class, but rewired to the target object.
So if it's not relevant to the concept of OO, might as well make it the easiest place to work with.
That means they should be in the root (XAct) directory so that VS can find them in any context that the 
target object is being used.
Yeah. A head trip. But before you respond...think. Think again. And think again.
Methods are not classes. Namespaces are Class concepts. Not method concepts.
