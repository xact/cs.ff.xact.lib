﻿# XAct.Core #

## Description ##

## Summary ##
* What it is:
	* Base assembly
	* Referenced by most other assemblies in XActLib.
* What it is not:
	* It's *not* a dump all!
	* As it is used on every layer of an app, it must have not include any code that is more than .NET35Client, or require elevated rights by access IO of any kind.

## Design Considerations ##
* Compiled against .NET35 Client for portability reasons.
* References have been removed to the bare minimum for portability *and* layering reasons.

## References ##
* The list below is imcomplete, but of note are:
	* None
	
Last edited: now.
	

