namespace XAct.Diagnostics
{
    using System;
    using System.Diagnostics;
    using System.Reflection;

    /// <summary>
    /// 
    /// </summary>
    /// <internal><para>7/31/2011: Sky</para></internal>
    public interface ITraceSwitchService: IHasXActLibService
    {


        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        ITraceSwitchServiceConfiguration Configuration { get; }

        /// <summary>
        /// Gets or sets a value indicating whether Tracing is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ITraceSwitchService"/> is enabled.
        /// <para>
        /// Note: TraceSwitching via Reflection is very expensive.
        /// </para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        bool ReflectionEnabled { get; set; }

        /// <summary>
        /// Gets the <see cref="System.Diagnostics.TraceSwitch"/> 
        /// with the specified trace switch name.
        /// </summary>
        /// <value></value>
        TraceSwitch this[string traceSwitchName] { get; }

        /// <summary>
        /// Builds a traceSwitch from info found in configuration.
        /// If no configuration found, returns a TraceSwitch, with default
        /// TraceLevel set to what was given in 
        /// <paramref name="defaultTraceLevel"/>.
        /// </summary>
        /// <param name="traceSwitchName">Name of the switch.</param>
        /// <param name="defaultTraceLevel">Level of trace if not defined in configuration.</param>
        /// <returns>A TraceSwitch</returns>
        TraceSwitch GetNamedTraceSwitch(string traceSwitchName, XAct.Diagnostics.TraceLevel defaultTraceLevel);

        /// <summary>
        /// Builds a traceSwitch from info found in configuration.
        /// If no configuration is found for a TraceSwitch with the name "Application"
        /// returns a TraceSwitch, with default TraceLevel set to Off.
        /// </summary>
        /// <returns>A TraceSwitch</returns>
        TraceSwitch GetApplicationTraceSwitch();

        /// <summary>
        /// Verify message should be traced, based on the given class Type.
        /// <para>
        /// In order to trace, the switch's trace level has 
        /// to be equal to or higher than the given
        /// trace level.
        /// </para>
        /// <para>
        /// Note that this works (seemingly backwards) before Error=1, Info=3, Verbose=4...
        /// </para>
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="classInstanceType">Type of the class instance.</param>
        /// <returns></returns>
        bool ShouldTrace(XAct.Diagnostics.TraceLevel traceLevel, Type classInstanceType);

        /// <summary>
        /// Builds a traceSwitch from info found in configuration.
        /// If no configuration is found for a TraceSwitch with the name 
        /// of the Assembly returns a TraceSwitch, 
        /// with default TraceLevel set to Off.
        /// </summary>
        /// <returns>A TraceSwitch</returns>
        TraceSwitch GetAssemblyTraceSwitch(Assembly assembly);

        /// <summary>
        ///   Gets the <see cref = "TraceSwitch" /> for the given class Type.
        /// <para>
        /// Use offset = 1 for invoking class.
        /// </para>
        /// </summary>
        /// <returns>
        ///   A <see cref = "TraceSwitch" />.
        /// </returns>
        /// <param name="stackTraceFrameOffset">StackTrace Frame Offset.</param>
        TraceSwitch GetTraceSwitch(int stackTraceFrameOffset);

        /// <summary>
        ///   Gets the TraceSwitch for the given class Type.
        /// </summary>
        /// <param name = "instanceType">Type of the instance.</param>
        /// <returns></returns>
        TraceSwitch GetTraceSwitchByType(Type instanceType);
    }
}