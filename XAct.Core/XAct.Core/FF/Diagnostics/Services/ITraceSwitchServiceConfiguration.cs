
namespace XAct.Diagnostics
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITraceSwitchServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether Tracing is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to enable the <see cref="ITraceSwitchService"/>'s fine grain (but expensive) Tracing by Reflection.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        bool ReflectionEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [cache trace switches].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [cache trace switches]; otherwise, <c>false</c>.
        /// </value>
        bool CacheTraceSwitches { get; set; }

        /// <summary>
        /// Gets or sets the default trace level for new TraceSwitchs created per Type/Assebly.
        /// </summary>
        /// <value>
        /// The default trace level.
        /// </value>
        TraceLevel DefaultTraceLevel { get; set; }


        /// <summary>
        /// Gets or sets the default Application TraceLevel.
        /// </summary>
        /// <value>
        /// The default application trace level.
        /// </value>
        TraceLevel DefaultApplicationTraceLevel { get; set; }
    }
}