namespace XAct.Diagnostics.SharedState
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public interface ITraceSwitchServiceState : IDictionary<string,TraceSwitch>,IHasXActLibServiceState 
    {}
}