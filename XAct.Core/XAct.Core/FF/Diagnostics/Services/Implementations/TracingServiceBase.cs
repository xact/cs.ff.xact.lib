﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

namespace XAct.Diagnostics.Implementations
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Timers;

    /// <summary>
    /// Abstract base implementation of 
    /// <see cref="ITracingService"/>
    /// <para>
    /// Provides a timed flush mechanism
    /// and a fine grain TraceSwitch mechanism to 
    /// any subclass.
    /// </para>
    /// <para>
    /// See:
    /// <see cref="SystemDiagnosticsTracingService"/>
    /// as well as 
    /// <c>Log4NetLoggingService</c>, and 
    /// <c>EnterpriseLoggingServce</c>
    /// </para>
    /// </summary>
    public abstract class TracingServiceBase : ITracingService, IDisposable
    {
        #region Services

        private readonly ITraceSwitchService _traceSwitchService;

        #endregion

        //Number of times Flush was offset:
        private static DateTime _lastDateTimeReseted;

        #region Static Constructors

        /// <summary>
        ///   Initializes the 
        ///   TracingService implementation.
        /// </summary>
        /// <remarks>
        ///   A common static constructor is used to initialize
        ///   the internal flush timer mechanism.
        /// </remarks>
        protected TracingServiceBase(ITraceSwitchService traceSwitchService)
        {
            traceSwitchService.ValidateIsNotDefault("traceSwitchService");
            _traceSwitchService = traceSwitchService;

            _timer.Elapsed += TimerElapsedInternal;

            _timer.Interval = 1000;
            
            _timer.AutoReset = false;
            _timer.Enabled = false;
        }

        #endregion

        #region Public Implementations of ILoggingServiceLogger

        /// <summary>
        /// Logs the given message to the current Trace output.
        /// <para>
        /// Important: this method is only available in debug mode, 
        /// as it is decorated with ConditionalAttribute("DEBUG")
        /// </para>
        /// </summary>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public virtual void DebugTrace(Diagnostics.TraceLevel traceLevel, string message, params object[] arguments)
        {
            //Have to move it to an internal method as one cannot associate 
            //a ConditionalAttribute to a method declared in an interface.
            DebugTraceInternal(0, traceLevel, message, arguments);
        }


        /// <summary>
        /// Logs the given message to the current Trace output.
        /// <para>
        /// Important: this method is only available in debug mode, 
        /// as it is decorated with ConditionalAttribute("DEBUG")
        /// </para>
        /// <para>
        /// If called directly, the stackTraceOffset should be 0.
        /// But if this infrastructure service is called indirectly
        /// (eg wrapped by a business service) the stackTraceOffset should be 1)
        /// </para>
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public virtual void DebugTrace(int stackTraceFrameOffset, Diagnostics.TraceLevel traceLevel, string message,
                               params object[] arguments)
        {
            //Have to move it to an internal method as one cannot associate 
            //a ConditionalAttribute to a method declared in an interface.
            DebugTraceInternal(stackTraceFrameOffset, traceLevel, message, arguments);
        }


        /// <summary>
        /// Logs the exception.
        /// <para>
        /// Important: this method is only available in debug mode, 
        /// as it is decorated with ConditionalAttribute("DEBUG")
        /// </para>
        /// <para>
        /// In most cases, use the other overload as it does not require specifying the ErrorLevel.
        /// </para>
        /// 	<para>
        /// This overload can be useful to log exceptions as Warnings only.
        /// </para>
        /// </summary>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public virtual void DebugTraceException(Diagnostics.TraceLevel traceLevel, Exception exception, string message,
                                        params object[] arguments)
        {
            //Have to move it to an internal method as one cannot associate 
            //a ConditionalAttribute to a method declared in an interface.
            DebugTraceExceptionInternal(0, traceLevel, exception, message, arguments);
        }


        /// <summary>
        /// Logs the exception.
        /// <para>
        /// Important: this method is only available in debug mode, 
        /// as it is decorated with ConditionalAttribute("DEBUG")
        /// </para>
        /// <para>
        /// In most cases, use the other overload as it does not require specifying the ErrorLevel.
        /// </para>
        /// 	<para>
        /// This overload can be useful to log exceptions as Warnings only.
        /// </para>
        /// <para>
        /// If called directly, the stackTraceOffset should be 0.
        /// But if this infrastructure service is called indirectly
        /// (eg wrapped by a business service) the stackTraceOffset should be 1)
        /// </para>
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public virtual void DebugTraceException(int stackTraceFrameOffset, Diagnostics.TraceLevel traceLevel, Exception exception,
                                        string message, params object[] arguments)
        {
            //Have to move it to an internal method as one cannot associate 
            //a ConditionalAttribute to a method declared in an interface.
            DebugTraceExceptionInternal(stackTraceFrameOffset, traceLevel, exception, message, arguments);
        }


//        /// <summary>
//        /// Writes the given message to the current Trace output.
//        /// <para>
//        /// IMPORTANT: Use sparingly! In fact, avoid using this method except in the rarest of cases,
//        /// as it bypasses the logic of the TraceLevel arguments in the other
//        /// message signature.
//        /// </para>
//        /// 	<para>
//        /// TIP: Implement Implicitly.
//        /// </para>
//        /// </summary>
//        /// <param name="message">The message.</param>
//        /// <param name="arguments">Any optional arguments to format into the message.</param>
//        /// <internal><para>5/29/2011: Sky</para></internal>
//        void ITracingService.TraceLine(string message, params object[] arguments)
//        {
//            if (message.IsNullOrEmpty())
//            {
//                throw new ArgumentNullException("message");
//            }
//#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
//            Contract.EndContractBlock();
//#endif
//            //Offset by +1 for this method
//            WriteLine(TraceLevel.Verbose, message.FormatStringCurrentCulture(arguments));
//            ResetFlushTimer();
//        }

        /// <summary>
        /// QuickTraces the specified message, prefixing it with the name of the method.
        /// <para>
        /// IMPORTANT: Use sparingly! In fact, avoid using this method except in the rarest of cases,
        /// as it bypasses the logic of the TraceLevel arguments in the other
        /// message signature.
        /// </para>
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="arguments">The arguments.</param>
        public virtual void QuickTrace(string message, params object[] arguments)
        {
            message = PrefixMessageWithMethodName(message, arguments);
            Trace(Diagnostics.TraceLevel.Verbose, message);
        }


        /// <summary>
        ///   Logs the given message to the current Trace output.
        /// </summary>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        public virtual void Trace(Diagnostics.TraceLevel traceLevel, string message, params object[] arguments)
        {
            //if (traceLevel.IsNotInitialized())
            //{
            //    throw new ArgumentNullException("traceLevel");
            //}
            if (message.IsNullOrEmpty())
            {
                message = string.Empty;
                //FIX: Remove as sometimes error messages are empty string.
                //throw new ArgumentNullException("message");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Offset by +1 for this method
            WriteLineIf(baseTraceOffsetAmount, traceLevel, message, arguments);
            ResetFlushTimer();
        }


        /// <summary>
        ///   Logs the given message to the current Trace output.
        /// <para>
        /// If called directly, the stackTraceOffset should be 0.
        /// But if this infrastructure service is called indirectly
        /// (eg wrapped by a business service) the stackTraceOffset should be 1)
        /// </para>
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "arguments">Any optional arguments to format into the message.</param>
        public virtual void Trace(int stackTraceFrameOffset, Diagnostics.TraceLevel traceLevel, string message, params object[] arguments)
        {
            //if (traceLevel.IsNotInitialized())
            //{
            //    throw new ArgumentNullException("traceLevel");
            //}
            if (message.IsNullOrEmpty())
            {
                message = string.Empty;
                //throw new ArgumentNullException("message");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Offset by +1 for this method
            WriteLineIf(stackTraceFrameOffset + baseTraceOffsetAmount, traceLevel, message, arguments);
            ResetFlushTimer();
        }


        /// <summary>
        ///   Logs the exception.
        ///   <para>
        ///     In most cases, use the other overload as it does not require specifying the ErrorLevel.
        ///   </para>
        ///   <para>
        ///     This overload can be useful to log exceptions as Warnings only.
        ///   </para>
        /// </summary>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "exception">The exception.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "messageArguments">Any optional arguments to format into the message.</param>
        public virtual void TraceException(Diagnostics.TraceLevel traceLevel, Exception exception, string message,
                                   params object[] messageArguments)
        {
            exception.ValidateIsNotDefault("exception");
            if (message.IsNullOrEmpty())
            {
                //FIX: Remove as sometimes error messages are empty string.
                //throw new ArgumentNullException("message");
                message = string.Empty;
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Offset by +1 for this method
            WriteLineIf(baseTraceOffsetAmount, traceLevel, message, messageArguments);
            TraceException(baseTraceOffsetAmount, traceLevel, exception);
            ResetFlushTimer();
        }


        /// <summary>
        ///   Logs the exception.
        ///   <para>
        ///     In most cases, use the other overload as it does not require specifying the ErrorLevel.
        ///   </para>
        ///   <para>
        ///     This overload can be useful to log exceptions as Warnings only.
        ///   </para>
        /// <para>
        /// If called directly, the stackTraceOffset should be 0.
        /// But if this infrastructure service is called indirectly
        /// (eg wrapped by a business service) the stackTraceOffset should be 1)
        /// </para>
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name = "traceLevel">The level of this message.</param>
        /// <param name = "exception">The exception.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "messageArguments">Any optional arguments to format into the message.</param>
        public virtual void TraceException(int stackTraceFrameOffset, Diagnostics.TraceLevel traceLevel, Exception exception, string message,
                                   params object[] messageArguments)
        {
            exception.ValidateIsNotDefault("exception");
            if (message.IsNullOrEmpty())
            {
                //FIX: Remove as sometimes error messages are empty string.
                //throw new ArgumentNullException("message");
                message = string.Empty;
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Offset by +1 for this method
            WriteLineIf(stackTraceFrameOffset + baseTraceOffsetAmount, traceLevel, message, messageArguments);

            TraceException(baseTraceOffsetAmount, traceLevel, exception);
            
            ResetFlushTimer();
        }

        #endregion

        #region Private Methods

        //Had to move this down to another internal method as 
        //Conditional Attribute interfered with ability of class
        //to implement contract...(contract gets broken if code is conditionally stripped out...)
        [Conditional("DEBUG")]
        private void DebugTraceInternal(int stackTraceFrameOffset, Diagnostics.TraceLevel traceLevel, string message,
                                        params object[] arguments)
        {
            if (message == null)
            {
                message = string.Empty;
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            //Offset = base(+0) + thismethod(+1) + public.DebugTrace(+1) + invokingMethod (+1) 
            WriteLineIf(stackTraceFrameOffset + (baseTraceOffsetAmount + 1), traceLevel, message, arguments);
            ResetFlushTimer();
        }

        //Had to move this down to another internal method as 
        //Conditional Attribute interfered with ability of class
        //to implement contract...(contract gets broken if code is conditionally stripped out...)
        [Conditional("DEBUG")]
        private void DebugTraceExceptionInternal(int stackTraceFrameOffset, Diagnostics.TraceLevel traceLevel, Exception exception,
                                                 string message, params object[] messageArguments)
        {
            exception.ValidateIsNotDefault("exception");
            if (message == null)
            {
                message = string.Empty;
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            //Offset = base(+0) + thismethod(+1) + public.DebugTrace(+1) + invokingMethod (+1) 
            WriteLineIf(stackTraceFrameOffset + (baseTraceOffsetAmount + 1), traceLevel, message, messageArguments);
            TraceException(3, traceLevel, exception);
            ResetFlushTimer();
        }


        /// <summary>
        ///   Resets the flush timer.
        /// </summary>
        private void ResetFlushTimer()
        {
            //First time checked it will be at DateTime.Min, 
            //so will trigger.
            if (DateTime.UtcNow.Subtract(_lastDateTimeReseted).TotalMilliseconds > 1000)
            {
                //Been a while...time to flush
                //so don't reset the timer.
                return;
            }

            _timer.Stop(); //Kill last iteration:
            _timer.Start(); //Start again
        }

        private void TimerElapsedInternal(object sender, ElapsedEventArgs e)
        {
            //It's done this ways so that no subclass forgets to reset the timer.
            TimerElapsed(sender, e);

            //Reset:
            _lastDateTimeReseted = DateTime.UtcNow;
        }

        /// <summary>
        ///   Event handler for when the flush timer goes off.
        /// <para>
        /// Flushes the Logging mechanism.
        /// </para>
        /// </summary>
        /// <internal>
        /// <para>
        /// An example of what to put in the override would be:
        /// <code>
        /// <![CDATA[
        /// System.Diagnostics.Trace.Flush();
        /// ]]>
        /// </code>
        /// </para>
        /// </internal>
        /// <param name = "sender">The sender.</param>
        /// <param name = "e">The <see cref = "System.Timers.ElapsedEventArgs" /> instance containing the event data.</param>
        protected abstract void TimerElapsed(object sender, ElapsedEventArgs e);


        /// <summary>
        ///   Traces the given 
        ///   <paramref name = "message" /> 
        ///   if the relevent TraceSwitch's Level is 
        ///   equal or higher than the given 
        ///   <paramref name = "traceLevel" /> 
        ///   argument.
        /// </summary>
        /// <param name="stackTraceFrameOffset">1 for invoker, 2 for invoker's invoker, etc.</param>
        /// <param name = "traceLevel">
        ///   The level that the relevant TraceSwitch has to be at to log this message.
        /// </param>
        /// <param name = "message">The text message to trace.</param>
        /// <param name = "messageArguments">
        ///   The optional arguments to string format into the message.
        /// </param>
        private void WriteLineIf(
            int stackTraceFrameOffset,
            Diagnostics.TraceLevel traceLevel,
            string message,
            params object[] messageArguments)
        {
            if ((_traceSwitchService == null) ||(!_traceSwitchService.Enabled))
            {
                return;
            }

            //If TraceSwitchService is Enabled get the current Method's Type:
            Type classInstanceType = _traceSwitchService.ReflectionEnabled 
                                         ? 
                                             new StackTrace().CurrentMethodDeclaringType(stackTraceFrameOffset) : null;

            //The Type will be null if we are not using reflection:

            WriteLineIf(classInstanceType, traceLevel, message, messageArguments);
        }


        /// <summary>
        ///   Trace the given 
        ///   <paramref name = "message" /> 
        ///   if the relevent TraceSwitch's Level is 
        ///   equal or higher than the given 
        ///   <paramref name = "traceLevel" /> 
        ///   argument.
        /// </summary>
        /// <remarks>
        /// <para>
        /// In other words, if the config file switch is Info (3)
        /// and the message's switch level is Verbose (4), then it
        /// it is not logged. 
        /// </para>
        /// </remarks>
        /// <param name = "classInstanceType">Type of the class instance.</param>
        /// <param name = "traceLevel">
        ///   The level that the relevant TraceSwitch has to be at to log this message.
        /// </param>
        /// <param name = "message">The text message to trace.</param>
        /// <param name = "args">
        ///   The optional arguments to string format into the message.
        /// </param>
        private void WriteLineIf(
            Type classInstanceType,
            Diagnostics.TraceLevel traceLevel,
            string message,
            params object[] args)
        {
            try
            {

                if ((_traceSwitchService == null) || (!_traceSwitchService.Enabled))
                {
                    return;
                }


                //Debug.Assert(classInstanceType != typeof (SystemDiagnosticsLoggingService));

                //Note that Type can be null (in which case, it will take Application TraceSwitch):
                if (!_traceSwitchService.ShouldTrace(traceLevel, classInstanceType))
                {
                    return;
                }

                //Appears that we have passed -- so trace:
                WriteLine(traceLevel, StringFormatMessage(message, args));

                //And reset the timer so that we flush not just yet...
                ResetFlushTimer();
            }
            catch (Exception exception2)
            {
                Debug.WriteLine(exception2.Message);
            }
        }


        /// <summary>
        /// When implemented by subclasses, 
        /// writes the given message to the 
        /// specific implementation's logging system.
        /// </summary>
        /// <internal>
        /// Invoked only after passing WriteLineIf.
        /// </internal>
        /// <param name="traceLevel">The message's tracelevel</param>
        /// <param name="message">The already string formatted message.</param>
        protected abstract void WriteLine(
            Diagnostics.TraceLevel traceLevel,
            string message);


        /// <summary>
        /// Helper method to string format the message using
        /// an <see cref="CultureInfo.InvariantCulture"/>
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageArguments">The message arguments.</param>
        /// <returns></returns>
        private string StringFormatMessage(string message, params object[] messageArguments)
        {
            string msg;
            try
            {
                msg = message.FormatStringExceptionCulture(messageArguments);
            }
            catch (System.Exception e)
            {
                msg =
                    "[ERR: string.Format() failed while building Message to Log] Msg:{0} MessageArguments:{1}, Exception:{2}".FormatStringExceptionCulture(
                        message,messageArguments, e.Message);
            }
            return msg;
        }


        /// <summary>
        ///   Prefixes a message intended for Debug output with 
        ///   same stuff that Trace prepends.
        ///   <para>
        ///     Invoked only if going to Debug listeners (ie Verbose).
        ///   </para>
        /// </summary>
        /// <param name = "traceLevel">
        ///   The level that the relevant TraceSwitch has to be at to log this message.
        /// </param>
        /// <param name = "message">The message.</param>
        /// <returns></returns>
        private string PrefixMessage(Diagnostics.TraceLevel traceLevel, string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return string.Empty;
            }

            try
            {
                //string traceLevelText = traceLevel.ToString();

                return
                    "{0} {1}: {2}: [{3}] {4}".FormatStringInvariantCulture(
                        Path.GetFileName(
                            Process.GetCurrentProcess()
                                .MainModule
                                .FileName),
                        traceLevel.ToString().PadRight(7, '.'),
                        "0",
                        DateTime.Now.ToString("MM-dd HH:mm:ss"),
                        message);
            }
                // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
                // ReSharper restore EmptyGeneralCatchClause
            {
            }
            return message;
        }

        private string PrefixMessageWithMethodName(string message, params object[] messageArgs)
        {
            //Get name of current Class is relatively easy:
            StackTrace stackTrace = new StackTrace();
            MethodInfo methodInfo = stackTrace.GetFrame(1).GetMethod() as MethodInfo;

            var result = methodInfo.ToStringSignature(true) + ":" + message.FormatStringCurrentCulture(messageArgs);
            return result;
        }


        private void TraceException(int stackTraceFrameOffset, Diagnostics.TraceLevel traceLevel, Exception exception)
        {
            Type classInstanceType = new StackTrace().CurrentMethodDeclaringType(stackTraceFrameOffset);

            TraceException(classInstanceType, traceLevel, exception);
        }

        /// <summary>
        /// Print out error message, and 5 deeper messages, and finally, the stack trace
        /// </summary>
        /// <param name="invokingClassInstance"></param>
        /// <param name="traceLevel"></param>
        /// <param name="exception"></param>
        private void TraceException(Type invokingClassInstance, Diagnostics.TraceLevel traceLevel, Exception exception)
        {
            int counter = 0;

            Exception e = exception;
            while (e != null)
            {
                if (counter == 5)
                {
                    break;
                }
                string prefix = "InnerException (level:{0}): ".FormatStringInvariantCulture(counter + 1);

                WriteLineIf(invokingClassInstance, traceLevel, prefix + e.Message);

                e = e.InnerException;
                counter++;
            }

            WriteLineIf(invokingClassInstance, traceLevel, exception.StackTrace);
        }

        #endregion

        private readonly Timer _timer = new Timer();
        private int baseTraceOffsetAmount = 2;
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}