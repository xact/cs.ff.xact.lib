using XAct.Services;

namespace XAct.Diagnostics.Implementations
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using XAct.Diagnostics.SharedState;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [ComVisible(false)]
    public class TraceSwitchServiceState : Dictionary<string, TraceSwitch>, ITraceSwitchServiceState
    {
        
    }
}