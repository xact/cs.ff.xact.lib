namespace XAct.Diagnostics
{
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    public class TraceSwitchServiceConfiguration : ITraceSwitchServiceConfiguration, IHasEnabled, IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets or sets a value indicating whether Tracing is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to enable the <see cref="ITraceSwitchService"/>'s fine grain (but expensive) Tracing by Reflection.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool ReflectionEnabled { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [cache trace switches].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [cache trace switches]; otherwise, <c>false</c>.
        /// </value>
        public bool CacheTraceSwitches { get; set; }

        /// <summary>
        /// Gets or sets the default trace level for new TraceSwitchs created per Type/Assebly.
        /// </summary>
        /// <value>
        /// The default trace level.
        /// </value>
        public TraceLevel DefaultTraceLevel { get; set; }

        /// <summary>
        /// Gets or sets the default Application TraceLevel.
        /// </summary>
        /// <value>
        /// The default application trace level.
        /// </value>
        public TraceLevel DefaultApplicationTraceLevel { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceSwitchServiceConfiguration"/> class.
        /// </summary>
        public TraceSwitchServiceConfiguration()
        {
            Enabled = true;
            ReflectionEnabled = false;
            CacheTraceSwitches = true;
            DefaultTraceLevel = TraceLevel.Off;
            DefaultApplicationTraceLevel = TraceLevel.Warning;
        }
    }
}