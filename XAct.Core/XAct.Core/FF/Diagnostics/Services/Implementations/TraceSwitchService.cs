namespace XAct.Diagnostics.Implementations
{
    using System;
    using System.Diagnostics;
    using System.Reflection;
    using XAct.Diagnostics.SharedState;
    using XAct.Services;

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment

#endif


    /// <summary>
    /// Service that returns TraceSwitches based on context.
    /// </summary>
    /// <intenral>
    /// Used within DefaultLoggingService.
    /// </intenral>
    /// 
    public class TraceSwitchService : /*XActLibServiceBase,*/ ITraceSwitchService, IHasEnabled
    {
        /// <summary>
        /// Name of application wide TraceSwitch.
        /// <para>
        /// Value: 'Application'
        /// </para>
        /// </summary>
        public const string ApplicationTraceSwitchName = "Application";

        //Make it static so that the cache can be manipulated from anywhere in the app
        private readonly ITraceSwitchServiceState _traceSwitchCache;
        //= new Dictionary<string, TraceSwitch>();



        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public ITraceSwitchServiceConfiguration Configuration { get { return _traceSwitchServiceConfiguration; } }
        private readonly ITraceSwitchServiceConfiguration _traceSwitchServiceConfiguration;


        /// <summary>
        /// Gets or sets a value indicating whether Tracing is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get { return _traceSwitchServiceConfiguration.Enabled; } set { _traceSwitchServiceConfiguration.Enabled = value; } }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ITraceSwitchService"/>'s fine grain TraceSwitch by Reflection on Type/Assembly is enabled.
        /// <para>
        /// Note: TraceSwitching for Logging is very expensive.
        /// </para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool ReflectionEnabled { get { return _traceSwitchServiceConfiguration.ReflectionEnabled; } set { _traceSwitchServiceConfiguration.ReflectionEnabled = value; }}


        /// <summary>
        /// Gets or sets the default trace level for new TraceSwitchs created per Type/Assebly.
        /// </summary>
        /// <value>
        /// The default trace level.
        /// </value>
        public XAct.Diagnostics.TraceLevel DefaultTraceLevel { get { return _traceSwitchServiceConfiguration.DefaultTraceLevel; } set { _traceSwitchServiceConfiguration.DefaultTraceLevel = value; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceSwitchService"/> class.
        /// </summary>
        public TraceSwitchService(ITraceSwitchServiceState traceSwitchCache, ITraceSwitchServiceConfiguration traceSwitchServiceConfiguration)
        {
            _traceSwitchCache = traceSwitchCache;
            _traceSwitchServiceConfiguration = traceSwitchServiceConfiguration;
        }



        /// <summary>
        /// Gets the <see cref="System.Diagnostics.TraceSwitch"/> 
        /// with the specified trace switch name.
        /// </summary>
        /// <value></value>
        public TraceSwitch this[string traceSwitchName]
        {
            get
            {
                var result = GetNamedTraceSwitch(traceSwitchName,_traceSwitchServiceConfiguration.DefaultTraceLevel);
                return result;
            }
        }



        /// <summary>
        /// Builds a traceSwitch from info found in configuration.
        /// If no configuration found, returns a TraceSwitch, with default
        /// TraceLevel set to what was given in 
        /// <paramref name="defaultTraceLevel"/>.
        /// </summary>
        /// <param name="traceSwitchName">Name of the switch.</param>
        /// <param name="defaultTraceLevel">Level of trace if not defined in configuration.</param>
        /// <returns>A TraceSwitch</returns>
        public TraceSwitch GetNamedTraceSwitch(string traceSwitchName, XAct.Diagnostics.TraceLevel defaultTraceLevel)
        {
            TraceSwitch result;
            _traceSwitchCache.TryGetValue(traceSwitchName, out result);

            if (result == null)
            {
                result = new TraceSwitch(
                    traceSwitchName,
                    string.Empty,
                    defaultTraceLevel.ToString()
                    );
                if (_traceSwitchServiceConfiguration.CacheTraceSwitches)
                {
                    _traceSwitchCache[traceSwitchName] = result;
                }
            }

            return result;
        }


        /// <summary>
        /// Builds a traceSwitch from info found in configuration.
        /// If no configuration is found for a TraceSwitch with the name "Application"
        /// returns a TraceSwitch, with default TraceLevel set to Off.
        /// </summary>
        /// <returns>A TraceSwitch</returns>
        public TraceSwitch GetApplicationTraceSwitch()
        {
            var result = GetNamedTraceSwitch(
                ApplicationTraceSwitchName, 
                _traceSwitchServiceConfiguration.DefaultApplicationTraceLevel );
            return result;
        }


        /// <summary>
        /// Verify message should be traced, based on the given class Type.
        /// <para>
        /// In order to trace, the switch's trace level has 
        /// to be equal to or higher than the given
        /// trace level.
        /// </para>
        /// <para>
        /// Note that this works (seemingly backwards) before Error=1, Info=3, Verbose=4...
        /// </para>
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="classInstanceType">Type of the class instance.</param>
        /// <returns></returns>
        public bool ShouldTrace(XAct.Diagnostics.TraceLevel traceLevel, Type classInstanceType)
        {
            //Don't bother tracing if requested trace level makes no sense:
            if (traceLevel == XAct.Diagnostics.TraceLevel.Off)
            {
                return false;
            }

            //Don't bother tracing if service not enabled
            if (!this.Enabled)
            {
                return false;
            }

            System.Diagnostics.TraceSwitch applicationSwitch = GetApplicationTraceSwitch();

            if (classInstanceType == null)
            {
                var result = ((int)applicationSwitch.Level) >= ((int)traceLevel);
                return result;
            }

            System.Diagnostics.TraceSwitch traceSwitch = GetTraceSwitchByType(classInstanceType);


            //Set it to highest level of either application, assembly, or this class:

            TraceSwitch assemblySwitch = GetAssemblyTraceSwitch(classInstanceType.Assembly);

            //TraceLevel applicationLevel = (TraceLevel) applicationSwitch.Level;
            //TraceLevel assemblyLevel = (TraceLevel) assemblySwitch.Level;

            XAct.Diagnostics.TraceLevel result2 = (XAct.Diagnostics.TraceLevel)traceSwitch.Max(
                applicationSwitch,
                assemblySwitch
                );



            var result3 = (result2 >= traceLevel);
            return result3;
        }


        /// <summary>
        /// Builds a traceSwitch from info found in configuration.
        /// If no configuration is found for a TraceSwitch with the name 
        /// of the Assembly returns a TraceSwitch, 
        /// with default TraceLevel set to Off.
        /// </summary>
        /// <returns>A TraceSwitch</returns>
        public TraceSwitch GetAssemblyTraceSwitch(Assembly assembly)
        {
            //Get switch for application
            string key = assembly.GetName().Name;
            var result = GetNamedTraceSwitch(key , this.DefaultTraceLevel);
            return result;
        }


        /// <summary>
        ///   Gets the cached <see cref = "TraceSwitch" /> for the given class Type.
        /// <para>
        /// Use offset = 1 for invoking class.
        /// </para>
        /// </summary>
        /// <returns>
        ///   A <see cref = "TraceSwitch" />.
        /// </returns>
        /// <param name="stackTraceFrameOffset">StackTrace Frame Offset.</param>
        public TraceSwitch GetTraceSwitch(int stackTraceFrameOffset)
        {
            try
            {
                //Get Type of *calling* class:
                Type classInstanceType =
                    new StackTrace().GetFrame(stackTraceFrameOffset).GetMethod().DeclaringType;

                var result = GetTraceSwitchByType(classInstanceType);
                return result;
            }
            catch (Exception exception)
            {
                //Funny...
                Debug.WriteLine(exception.Message);
            }
            return null;
        }


        /// <summary>
        ///   Gets the cached <see cref = "TraceSwitch" /> for the given class Type.
        /// </summary>
        /// <param name = "instanceType">Type of the instance.</param>
        /// <returns></returns>
        public TraceSwitch GetTraceSwitchByType(Type instanceType)
        {
            //Important: Type can be null:
            //NO:instanceType.ValidateIsNotDefault("instanceType");

            TraceSwitch result;

            if (instanceType == null)
            {
                //As we don't have type, we can't determine either type level or Assembly level,
                //only Application:
                result = GetApplicationTraceSwitch();
                return result;
            }


            string instanceTypeFullName = instanceType.FullName;

            Debug.Assert(instanceTypeFullName != null);


            //Find existing, or create a new one and add it to collection:
            result = GetNamedTraceSwitch(instanceTypeFullName,_traceSwitchServiceConfiguration.DefaultTraceLevel);


            return result;
        }



        /*
        public TraceSwitch GetTraceSwitch(object classInstance)
        {
            if (classInstance == null){
                throw new System.ArgumentNullException("classInstance");
            }


            List<WeakReference> cleanup = new List<WeakReference>();

             foreach (WeakReference weakReference in _Cache.Keys)
             {
                 if (!weakReference.IsAlive) { cleanup.Add(weakReference); }
                 else if (weakReference.Target == classInstance)
                 {
                     return _Cache[weakReference];
                 }
             }
             foreach (WeakReference weakReference in cleanup)
             {
                 _Cache.Remove(weakReference);
             }

             Type instanceType = classInstance.GetType();

             TraceSwitch traceSwitch = 
         * new TraceSwitch(
         * instanceType.FullName, 
         * string.Empty, 
         * TraceLevel.Off.ToString());

             //Set it to highest level of either application, assembly, or this class:
             traceSwitch.Level =
                 (TraceLevel)Math.Max(
                     (sbyte)traceSwitch.Level,
                     Math.Max(
                     (sbyte)new TraceSwitch(
         * "Application", string.Empty, TraceLevel.Off.ToString()).Level,
                     (sbyte)new TraceSwitch(
         * instanceType.Assembly.FullName, string.Empty, TraceLevel.Off.ToString()).Level));


            _Cache[new WeakReference(classInstance)] = traceSwitch;

            return traceSwitch;
        }
        */

    }
}