﻿ namespace XAct.Diagnostics.Implementations
{
    using System;

    /// <summary>
    /// A trace service that does nothing (ie fast).
    /// </summary>
    public class NullTracingService : TracingServiceBase, IHasLowBindingPriority
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NullTracingService"/> class.
        /// </summary>
        /// <param name="traceSwitchService">The trace switch service.</param>
        public NullTracingService(ITraceSwitchService traceSwitchService)
            : base(traceSwitchService)
    {
        
    }
        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="traceLevel">The message's tracelevel</param>
        /// <param name="message">The already string formatted message.</param>
        /// <internal>
        /// Invoked only after passing WriteLineIf.
        ///   </internal>
        protected override void WriteLine(
            TraceLevel traceLevel,
            string message)
        {
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public override void DebugTrace(int stackTraceFrameOffset, TraceLevel traceLevel, string message, params object[] arguments)
        {
        }
        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public override void DebugTrace(TraceLevel traceLevel, string message, params object[] arguments)
        {
        }
        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public override void DebugTraceException(int stackTraceFrameOffset, TraceLevel traceLevel, Exception exception, string message, params object[] arguments)
        {
        }
        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public override void DebugTraceException(TraceLevel traceLevel, Exception exception, string message, params object[] arguments)
        {
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="arguments">The arguments.</param>
        public override void QuickTrace(string message, params object[] arguments)
        {
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        /// <internal>
        /// An example of what to put in the override would be:
        ///   <code>
        ///   <![CDATA[
        /// System.Diagnostics.Trace.Flush();
        /// ]]>
        ///   </code>
        ///   </internal>
        protected override void TimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
        }
        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public override void Trace(int stackTraceFrameOffset, TraceLevel traceLevel, string message, params object[] arguments)
        {
        }
        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="message">The message.</param>
        /// <param name="arguments">Any optional arguments to format into the message.</param>
        public override void Trace(TraceLevel traceLevel, string message, params object[] arguments)
        {
        }
        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="stackTraceFrameOffset"></param>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="messageArguments">Any optional arguments to format into the message.</param>
        public override void TraceException(int stackTraceFrameOffset, TraceLevel traceLevel, Exception exception, string message, params object[] messageArguments)
        {
        }
        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="traceLevel">The level of this message.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="messageArguments">Any optional arguments to format into the message.</param>
        public override void TraceException(TraceLevel traceLevel, Exception exception, string message, params object[] messageArguments)
        {
        }

    }
}
