﻿namespace XAct.Diagnostics.Implementations
{
    using System.Timers;

#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment

#endif

    /// <summary>
    ///   An implementation of <see cref = "ITracingService" />, 
    ///   using <see cref = "System.Diagnostics.Trace" />.
    /// </summary>
    
    public class SystemDiagnosticsTracingService : TracingServiceBase, IHasMediumBindingPriority
    {
        #region COnstructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemDiagnosticsTracingService"/> class.
        /// </summary>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public SystemDiagnosticsTracingService(ITraceSwitchService traceSwitchService) : base(traceSwitchService)
        {
        }

        #endregion

        /// <summary>
        /// Writes the given message to the 
        /// specific implementation's logging system.
        /// </summary>
        /// <internal>
        /// Invoked only after passing WriteLineIf.
        /// </internal>
        /// <param name="traceLevel">The message's tracelevel</param>
        /// <param name="message">The already string formatted message.</param>
        protected override void WriteLine(
            TraceLevel traceLevel,
            string message)
        {
            switch (traceLevel)
            {
                case TraceLevel.Verbose:
                    //Only when verbose is Debug stream used...
                    //message = PrefixMessage(traceLevel, message);
                    System.Diagnostics.Trace.WriteLine(message);
                    break;
                    //The rest goes to Trace stream...
                    //with full information as specified in config files...
                case TraceLevel.Info:
                    System.Diagnostics.Trace.TraceInformation(message);
                    break;
                case TraceLevel.Warning:
                    System.Diagnostics.Trace.TraceWarning(message);
                    break;
                case TraceLevel.Error:
                    System.Diagnostics.Trace.TraceError(message);
                    break;
            }
        }


        /// <summary>
        /// Event handler for when the flush timer goes off.
        /// <para>
        /// Flushes the Logging mechanism.
        /// </para>
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            System.Diagnostics.Trace.Flush();
            //Not needed as goes to same place: System.Diagnostics.Debug.Flush();
        }
    }
}