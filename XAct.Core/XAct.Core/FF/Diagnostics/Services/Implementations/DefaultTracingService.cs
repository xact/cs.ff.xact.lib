﻿//namespace XAct.Diagnostics.Implementations
//{
//#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
//    using XAct.Services;

//#endif

//    /// <summary>
//    ///   Default implementation of <see cref = "ITracingService" />, 
//    ///   using <see cref = "System.Diagnostics.Trace" />.
//    ///   <para>
//    ///     For an Log4Net based implementation of 
//    ///     <see cref = "ITracingService" />
//    ///     add a reference to the 
//    ///     <c>XAct.Diagnostics.Log4NetLoggingService</c>
//    ///     asssembly.
//    ///   </para>
//    /// </summary>
//    [DefaultBindingImplementation(typeof(ITracingService),BindingLifetimeType.Undefined,Priority.VeryLow)]
//    public class DefaultTracingService : SystemDiagnosticsTracingService
//    {

//        ///// <summary>
//        ///// Gets the current trace listener.
//        ///// </summary>
//        //public static ITracingService RegisterInServiceLocator()
//        //{
//        //    ITracingService result = DependencyResolver.Current.GetInstance<ITracingService>(false);

//        //    if (result == null)
//        //    {
//        //        TraceSwitchService.RegisterInServiceLocator();
//        //        IServiceLocatorService serviceLocatorService = ServiceLocatorService.Current;

//        //         ServiceLocatorService.RegisterServiceByReflection(serviceLocatorService, new ServiceBindingDescriptor().Initialize<ITracingService, DefaultTracingService>());

//        //        result = DependencyResolver.Current.GetInstance<ITracingService>();
//        //    }

//        //    return result;
//        //}

//        /// <summary>
//        /// Initializes a new instance of the <see cref="DefaultTracingService"/> class.
//        /// </summary>
//        /// <param name="traceSwitchService"></param>
//        /// <internal>7/31/2011: Sky</internal>
//        /// <internal><para>8/7/2011: Sky</para></internal>
//        public DefaultTracingService(ITraceSwitchService traceSwitchService) : base(traceSwitchService)
//        {



//        }
//    }
//}