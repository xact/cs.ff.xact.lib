﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// </summary>
    /// <internal>
    ///   Src: http://blogs.msdn.com/rido/articles/FileRollerTraceListener.aspx
    /// </internal>
    internal class RollingFileNameHelper
    {
        /// <summary>
        /// Returns a date resolved name -- eg 'c:\\Logs\\MyLog{yyyyMMdd}.log' gets 
        /// converted to 'c:\\Logs\\MyLogFile250509.log'
        /// </summary>
        /// <param name="templateFileName">'c:\\Logs\\MyLog{yyyyMMdd}.log'</param>
        /// <param name="currentDateTime">Now</param>
        /// <param name="deleteOlderFiles">true</param>
        /// <param name="numberOfDaysToLeaveOldFiles">14</param>
        /// <returns></returns>
        public static FileInfo ResolveNameOfNewRollingFile(DateTime currentDateTime,
                                                           string templateFileName = "Log{yyyyMMdd}.log",
                                                           bool deleteOlderFiles = false,
                                                           int numberOfDaysToLeaveOldFiles = 14)
        {
            string datePattern;

            //If not rooted, prepends it withApplication root path:
            if (!Path.IsPathRooted(templateFileName))
            {
                //so filePath is now "c:\\Logs\\MyLogFile{yyyyMMdd}.log"
                templateFileName =
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, templateFileName);
            }

            //So...given a path such as "c:\\Logs\\MyLogFile{yyyyMMdd}.log" replaces it with "c:\\Logs\\MyLogFile250509.log"
            string filePath = ReplaceDateTimePatternInTextWithCurrentDateTime(currentDateTime, templateFileName,
                                                                              out datePattern);

            //should be enough to move on:
            FileInfo fileInfo = new FileInfo(filePath);

            DirectoryInfo directoryInfo = new DirectoryInfo(fileInfo.DirectoryName);

            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            //If we found a pattern in the filename, it stands
            //to reason there might be older files too:
            //Let's cleanup:
            if ((deleteOlderFiles) && (!string.IsNullOrEmpty(datePattern)))
            {
                IEnumerable<FileInfo> oldFilesFoundThatAreCandidatesToDelete =
                    GetListOfOlderFiles(currentDateTime, templateFileName, datePattern, numberOfDaysToLeaveOldFiles);

                foreach (FileInfo fileInfo2 in oldFilesFoundThatAreCandidatesToDelete)
                {
                    fileInfo2.Delete();
                }
            }

            //Return the full name (dir+name+ext):
            return fileInfo;
        }


        /// <summary>
        /// Replaces the date pattern in the filename, returning something like
        /// </summary>
        /// <param name="filePath">'c:\\Logs\\MyLogFile{yyyyMMdd}.log'</param>
        /// <param name="now">Current DateTime</param>
        /// <param name="datePattern">'yyyyMMdd'</param>
        /// <returns>c:\\Logs\\MyLogFile20090525.log</returns>
        private static string ReplaceDateTimePatternInTextWithCurrentDateTime(DateTime now, string filePath,
                                                                              out string datePattern)
        {
            datePattern = FindDateTimePatternInText(filePath);


            //So if today's date is (using the above pattern):
            string dateTime = now.ToString(datePattern);

            //The the filename is:
            filePath =
                filePath.Replace("{" + datePattern + "}", dateTime);

            return filePath;
        }

        /// <summary>
        /// Looks for the a pattern in the filename.
        /// <para>
        /// Eg: Extracts "yyyyMMdd" from "MyLogFile{yyyyMMdd}.log"
        /// </para>
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static string FindDateTimePatternInText(string filePath)
        {
            //Find the first curly:
            int start = filePath.LastIndexOf("{");

            if (start == -1)
            {
                //Not found:
                return null;
            }
            //Find the last curly:
            int end = filePath.LastIndexOf("}");

            return end == -1 ? null : filePath.Substring(start + 1, end - start - 1);

            //Have both curly's so the patter is in between:
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentDateTime">Now</param>
        /// <param name="templateFileName">A template path -- such as 'c:\Logs\MyLogFile{yyyyMMdd}.log'.  Has to have directory info at this point)</param>
        /// <param name="datePattern">'yyyyMMdd'</param>
        /// <param name="numberOfDaysToLeaveOldFiles">numberOfDaysToLeaveOldFiles</param>
        /// <returns>The count of files deleted.</returns>
        private static IEnumerable<FileInfo> GetListOfOlderFiles(DateTime currentDateTime, string templateFileName,
                                                          string datePattern = "yyyyMMdd",
                                                          int numberOfDaysToLeaveOldFiles = 14)
        {
            List<FileInfo> oldFilesFoundThatAreCandidatesToDelete = new List<FileInfo>();
            //Count of files deleted:


            if (string.IsNullOrEmpty(datePattern))
            {
                return oldFilesFoundThatAreCandidatesToDelete;
            }

            string wrappedDatePattern = "{" + datePattern + "}";
            //Correct as necessary:

            //Look back at most 356 days, deleting any older than 14 days,

            //but stop looking if nothing found for 30 days (obviously been 

            //deleted before or never created)...

            int numberOfIterationsWhereNoFilesFound = 0;

            //Note that yes... 30 hits to the hard drive is a bit heavy...
            //But i don't see an easy way to cache the info:
            //Nor do I want to delete anything less than 14 days...
            for (int i = numberOfDaysToLeaveOldFiles; i < 356; i++)
            {
                if (numberOfIterationsWhereNoFilesFound > 30)
                {
                    //We've been looking back for now 30 file hits 
                    //(one per day...and havn't found anything)
                    //time to think that there are no more files to delete:
                    return oldFilesFoundThatAreCandidatesToDelete;
                }

                //Go back n days, and turn that date into the pattern we are looking for:
                //eg '210509'
                string formattedDateTimeString
                    = currentDateTime.AddDays(-i).ToString(datePattern);

                //So we take our original filepattern 
                //MyLogFile{yyyyMMdd}.log
                //and replace it with our dateTime:
                string olderFilePath =
                    templateFileName.Replace(
                        wrappedDatePattern,
                        formattedDateTimeString);

                //giving us the filename we are looking for 'MyLogFile210509.log'
                FileInfo olderFileInfo = new FileInfo(olderFilePath);

                //If it exists, we want to delete it:
                if (!olderFileInfo.Exists)
                {
                    numberOfIterationsWhereNoFilesFound++;
                    continue;
                }

                oldFilesFoundThatAreCandidatesToDelete.Add(olderFileInfo);
                //Reset the Counter so we look back atleast another 30 days:
                numberOfIterationsWhereNoFilesFound = 0;
            }
            return oldFilesFoundThatAreCandidatesToDelete;
        }
    }
}

//~class