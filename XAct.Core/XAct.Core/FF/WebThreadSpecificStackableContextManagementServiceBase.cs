﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics;
    using XAct.Extensions;
    using XAct.State;

    /// <summary>
    /// Base class for ManagementServices to return a current context per thread,
    /// that is cloned from a default base one. Useful for having base settings, 
    /// but allowing customization as needed for a request.
    /// </summary>
    /// <internal>
    /// NOT Portable due to invocation of CloneTheUncloneable.
    /// </internal>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public abstract class WebThreadSpecificStackableContextManagementServiceBase<TContext> :
        WebThreadSpecificContextManagementServiceBase<TContext>,
        IWebThreadSpecificStackableContextManagementServiceBase<TContext>, 
        IHasXActLibService
        where TContext:class
        //NO: where TContext : ICloneable
    {
        /// <summary>
        /// Flag determining whether values should be cloned to a new object.
        /// </summary>
        protected  bool IsMutable = true;

        


        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="WebThreadSpecificStackableContextManagementServiceBase&lt;TContext&gt;"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="stateService">The state service.</param>
        protected WebThreadSpecificStackableContextManagementServiceBase(ITracingService tracingService, IContextStateService stateService)
            :base(tracingService,stateService)
        {
        }


        /// <summary>
        /// Pushes a new context on to the Context stack, for <see cref="IHasCurrent{TContext}.Current"/> to return.
        /// </summary>
        /// <param name="context">The context.</param>
        public void Push(TContext context)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Pushing new Thread specific Context: {0}", context);

            //Get the stack that is specific to this thread, and push a new instance onto it.
            GetOrCreateThreadSpecificStack().Push(context);
        }

        /// <summary>
        /// Pops the specified context off the Context stack.
        /// </summary>
        public TContext Pop()
        {

            //Get the stack that is specific to this thread, and pop off.
            TContext context = GetOrCreateThreadSpecificStack().Pop();

            _tracingService.Trace(TraceLevel.Verbose, "Popping Thread specific Context: {0}", context);

            return context;
        }

        /// <summary>
        /// Counts the number of items pushed onto the internal stack.
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            var result = GetOrCreateThreadSpecificStack().Count;
            return result;
        }

        /// <summary>
        /// Returns existing or newly Factoried Context object.
        /// </summary>
        /// <returns></returns>
        protected override TContext EnsureThreadSpecificInstance()
        {
            //The base one just gets it out of the Context.

            //This override uses instead a stack:

            Stack<TContext> stack = GetOrCreateThreadSpecificStack();

            if (stack.Count == 0)
            {
                TContext result = Create();

                stack.Push(result);
            }

            return stack.Peek();
        }


        /// <summary>
        /// Gets the thread specific stack in which contexts are kept.
        /// </summary>
        /// <returns></returns>
        private Stack<TContext> GetOrCreateThreadSpecificStack()
        {
            //Get a web thread specific object:
            Stack<TContext> result = _stateService.Items[_cacheKey].ConvertTo<Stack<TContext>>();

            //If it didn't exist, make it:
            if (result == null)
            {
                result = new Stack<TContext>();
                _stateService.Items[_cacheKey] = result;
            }
            return result;
        }



        ///// <summary>
        ///// Gets the thread specific stack in which contexts are kept.
        ///// </summary>
        ///// <returns></returns>
        //private Stack<TContext> GetOrCreateThreadSpecificStack<TContextSpecialization>() where TContextSpecialization : TContext
        //{
        //    //Get or create Dictionary of Stacks:
        //    Dictionary<object, Stack<TContext>> dictionary =
        //        _stateService.Items[_cacheKey] as Dictionary<object, Stack<TContext>>;
        //    if (dictionary == null)
        //    {
        //        _stateService.Items[_cacheKey] = dictionary = new Dictionary<object, Stack<TContext>>();
        //    }

        //    //Within dictionary, try to find a stack by it's key, which is the Type of the Context we are looking for.
        //    Stack<TContext> stack;

        //    Type specializedContextType = typeof(TContextSpecialization);

        //    if (!dictionary.TryGetValue(specializedContextType, out stack))
        //    {
        //        stack = new Stack<TContext>();

        //        dictionary.Add(specializedContextType, stack);
        //    }

        //    return stack;
        //}


        /// <summary>
        /// Creates a new Context.
        /// <para>
        /// Invoked by <see cref="IHasCurrent{TContext}.Current"/>
        /// to create a thread specific instance if one doesn't exist.
        /// </para>
        /// <para>
        /// If <see cref="IsMutable"/>, clones the object before returning it.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public override TContext Create()
        {

            //Invoke protected method to 
            //get hands on default singleton (using ServiceLocator).
            TContext srcContext = CreateNewSourceInstance();

            _tracingService.Trace(TraceLevel.Verbose, "Retrieved source Context: {0}", srcContext);


            if (!IsMutable)
            {
                return srcContext;
            }


            return CloneSourceInstanceValuesToNewThreadInstance(srcContext);
        }




        /// <summary>
        /// Clones the source instance values to the new thread specific instance.
        /// </summary>
        /// <param name="srcContext">The SRC context.</param>
        /// <returns></returns>
        protected virtual TContext CloneSourceInstanceValuesToNewThreadInstance(TContext srcContext)
        {
            //Clone to another copy to leave the original untouched:
            if (srcContext is ICloneable)
            {
                TContext result = (TContext)((ICloneable)srcContext).Clone();

                _tracingService.Trace(TraceLevel.Verbose, "Clonned Context to: {0}", result);
                return result;
            }

            return srcContext.CloneTheUncloneable();
        }









    }
}
