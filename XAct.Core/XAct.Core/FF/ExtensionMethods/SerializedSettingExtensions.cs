﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using XAct.Settings;

    /// <summary>
    /// Extensions to the <see cref="SerializedApplicationSetting"/>
    /// </summary>
    public static class SerializedApplicationSettingExtensions
    {

        /// <summary>
        /// Serializes the ApplicationIdentifier,ZoneOrTierIdentifier,HostIdentifier,UserIdentifier,Key as a string.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        public static string CompositeKey(this SerializedApplicationSetting setting)
        {
            return new[]
                {
                    setting.ContextIdentity(),
                    setting.Key
                }.Join(":");
        }

    }
}