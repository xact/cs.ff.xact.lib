﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Diagnostics.Contracts;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;

// ReSharper restore CheckNamespace
#endif

/// <summary>
    /// Extension to the <see cref="Image"/> class.
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts given Image to Byte Array
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        public static byte[] ToBytes(this Image image, ImageFormat format)
        {
            image.ValidateIsNotDefault("image");

            format.ValidateIsNotDefault("format");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, format);
                return stream.ToArray();
            }
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
