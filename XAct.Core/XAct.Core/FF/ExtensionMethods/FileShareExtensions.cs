﻿namespace XAct
{
    using System;
    using XAct.IO;

    /// <summary>
    /// Extensions to the <see cref="XAct.IO.FileShare"/> enum
    /// </summary>
    public static class FileShareExtensions
    {
        /// <summary>
        /// Maps the given portable enum value to a <see cref="System.IO.FileShare" /> enum value.
        /// </summary>
        /// <param name="fileShare">The file share.</param>
        /// <returns></returns>
        /// <exception cref="System.IndexOutOfRangeException"></exception>
        public static System.IO.FileShare MapTo(this XAct.IO.FileShare fileShare)
        {
            switch (fileShare)
            {
                case FileShare.Delete:
                    return System.IO.FileShare.Delete;
                case FileShare.Inheritable:
                    return System.IO.FileShare.Inheritable;
                case FileShare.None:
                    return System.IO.FileShare.None;
                case FileShare.Read:
                    return System.IO.FileShare.Read;
                case FileShare.ReadWrite:
                    return System.IO.FileShare.ReadWrite;
                case FileShare.Write:
                    return System.IO.FileShare.Write;
                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}