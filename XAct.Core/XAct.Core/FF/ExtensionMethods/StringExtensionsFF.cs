﻿namespace XAct
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// 
    /// </summary>
    public static class StringExtensionsFF
    {


     

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Encrypts the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="key">The key.</param>
        /// <param name="iv">The iv.</param>
        /// <returns></returns>
        /// <internal>
        ///   <para>
        /// An XActLib Extension.
        ///   </para>
        ///   <para>
        /// Src: Xero.Common
        ///   </para>
        ///   </internal>
        public static string Encrypt(this string source, string key, byte[] iv)
        {
            if (string.IsNullOrEmpty(source))
            {
                return null;
            }

            using (TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider())
            {
                Byte[] byteSource = Encoding.UTF8.GetBytes(source);
                Byte[] byteKey = Encoding.UTF8.GetBytes(key);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(byteKey, iv),
                                                                        CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(byteSource, 0, byteSource.Length);
                        cryptoStream.FlushFinalBlock();


                        return Convert.ToBase64String(memoryStream.ToArray());
                    }
                }
            }
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Decrypts the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="key">The key.</param>
        /// <param name="iv">The iv.</param>
        /// <returns></returns>
        /// <internal>Src: Xero.Common</internal>
        public static string Decrypt(this string source, string key, byte[] iv)
        {
            if (string.IsNullOrEmpty(source))
            {
                return null;
            }

            using (TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider())
            {
                Byte[] byteSource = Convert.FromBase64String(source);
                Byte[] byteKey = Encoding.UTF8.GetBytes(key);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(byteKey, iv),
                                                                 CryptoStreamMode.Write);


                    cryptoStream.Write(byteSource, 0, byteSource.Length);
                    cryptoStream.FlushFinalBlock();

                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
        }



 
        
    }
}