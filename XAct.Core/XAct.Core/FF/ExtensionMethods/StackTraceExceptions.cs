﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Diagnostics;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    /// Extension methods for the StackTrace
    /// </summary>
    /// <internal><para>7/31/2011: Sky</para></internal>
    public static class StackTraceExceptions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the type of the class containing the method at the specified offset.
        /// </summary>
        /// <param name="stackTrace">The stack trace.</param>
        /// <param name="stackTraceFrameOffset">The stack trace frame offset.</param>
        /// <returns></returns>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public static Type CurrentMethodDeclaringType(this StackTrace stackTrace, int stackTraceFrameOffset)
        {
            return stackTrace.GetFrame(stackTraceFrameOffset).GetDeclaringType();
        }

        /// <summary>
        /// Gets the string representation of the method at the specified offset.
        /// </summary>
        /// <param name="stackTrace">The stack trace.</param>
        /// <param name="stackTraceFrameOffset">The stack trace frame offset.</param>
        /// <returns></returns>
        /// <internal><para>8/10/2011: Sky</para></internal>
        public static string GetMethodSignature(this StackTrace stackTrace, int stackTraceFrameOffset)
        {
            return stackTrace.GetFrame(stackTraceFrameOffset).GetMethodSignature();
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
