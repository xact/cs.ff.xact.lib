﻿//namespace XAct
//{
//    using System;
//    using System.ComponentModel;
//    using System.IO;
//    using System.Runtime.Serialization.Formatters.Binary;
//    using System.Text;
//    using System.Xml.Serialization;

//    /// <summary>
//    /// 
//    /// </summary>
//    public static class TypeExtensionsFF
//    {




//        /// <summary>
//        /// <para>
//        /// An XActLib Extension.
//        /// </para>
//        /// Serializes the specified value into a string representation.
//        /// </summary>
//        /// <param name="valueType">Type of the value.</param>
//        /// <param name="value">The value.</param>
//        /// <param name="serializationMethod">The serialization method (if it can't do it, may automatically switch to another option).</param>
//        /// <returns></returns>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        public static string Serialize(this Type valueType, object value, ref SerializationMethod serializationMethod)
//        {
//            string result;


//            if (serializationMethod == SerializationMethod.String)
//            {
//                TypeConverter typeConverter = TypeDescriptor.GetConverter(valueType);

//                if (((typeConverter != null) && typeConverter.CanConvertTo(typeof(string))) &&
//                    typeConverter.CanConvertFrom(typeof(string)))
//                {
//                    result = typeConverter.ConvertToInvariantString(value);
//                    return result;
//                }
//                else
//                {
//                    serializationMethod = SerializationMethod.Xml;
//                }
//            }

//            if (serializationMethod == SerializationMethod.Xml)
//            {
//                try
//                {
//                    StringBuilder stringBuilder = new StringBuilder();
//                    using (StringWriter writer = new StringWriter(stringBuilder))
//                    {
//                        //Use the extension method
//                        XmlSerializer xmlSerializer = new XmlSerializer(valueType);
//                        xmlSerializer.Serialize(writer, value);
//                    }
//                    return stringBuilder.ToString();
//                }
//                catch
//                {
//                    serializationMethod = SerializationMethod.Base64Binary;
//                }
//            }
//            if (serializationMethod == SerializationMethod.Base64Binary)
//            {
//                using (MemoryStream serializationStream = new MemoryStream())
//                {
//                    new BinaryFormatter().Serialize(serializationStream, value);
//                    return Convert.ToBase64String(serializationStream.ToArray());
//                }
//            }
//            if (serializationMethod == SerializationMethod.Json)
//            {
//                //throw new NotImplementedException();
//                //Only available for 3.5 Full -- not Client! 
//                using (MemoryStream serializationStream = new MemoryStream())
//                {
//                    new System.Runtime.Serialization.Json.DataContractJsonSerializer(valueType).WriteObject(serializationStream,value);
//                    return new StreamReader(serializationStream).ReadToEnd();
//                }
//            }

//            throw new NotImplementedException();
//        }

//        /// <summary>
//        /// <para>
//        /// An XActLib Extension.
//        /// </para>
//        /// Deerialize the string back into an object.
//        /// </summary>
//        /// <param name="valueType">Type of the value.</param>
//        /// <param name="serializedValue">The serialized value.</param>
//        /// <param name="serializationMethod">The serialization method.</param>
//        /// <returns></returns>
//        /// <internal><para>8/16/2011: Sky</para></internal>
//        public static object DeSerialize(this Type valueType, string serializedValue,
//                                         SerializationMethod serializationMethod)
//        {
//            if (serializationMethod == SerializationMethod.Undefined)
//            {
//                throw new ArgumentException("serializationMethod");
//            }
//            object result;

//            if (serializationMethod == SerializationMethod.String)
//            {
//                TypeConverter typeConverter = TypeDescriptor.GetConverter(valueType);
//                result = typeConverter.ConvertFromInvariantString(serializedValue);
//                return result;
//            }
//            if (serializationMethod == SerializationMethod.Xml)
//            {
//                using (StringReader reader = new StringReader(serializedValue))
//                {
//                    XmlSerializer serializer = new XmlSerializer(valueType);
//                    result = serializer.Deserialize(reader);
//                    reader.Close();
//                    return result;
//                }
//            }
//            if (serializationMethod == SerializationMethod.Base64Binary)
//            {
//                byte[] buffer = Convert.FromBase64String(serializedValue);

//                using (MemoryStream memoryStream = new MemoryStream(buffer))
//                {
//                    result = new BinaryFormatter().Deserialize(memoryStream);
//                    memoryStream.Close();
//                    return result;
//                }
//            }
//            if (serializationMethod == SerializationMethod.Json)
//            {
//                using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(serializedValue)))
//                {
//                    return new System.Runtime.Serialization.Json.DataContractJsonSerializer(valueType).ReadObject(stream);
//                }
//            }
//            throw new NotImplementedException();
//        }
//    }
//}