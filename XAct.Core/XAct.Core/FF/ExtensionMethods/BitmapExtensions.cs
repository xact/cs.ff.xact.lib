﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Drawing;
    using System.Drawing.Imaging;

// ReSharper restore CheckNamespace
#endif

/// <summary>
    /// Extensions to Bitmaps
    /// </summary>
    public static class BitmapExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Makes the grayscale3.
        /// </summary>
        /// <remarks>
        /// Src: http://www.switchonthecode.com/tutorials/csharp-tutorial-convert-a-color-image-to-grayscale
        /// </remarks>
        /// <param name="original">The original.</param>
        /// <returns></returns>
        public static Bitmap ConvertToGrayscale(this Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
                new[]
                    {
                        new[] {.3f, .3f, .3f, 0, 0},
                        new[] {.59f, .59f, .59f, 0, 0},
                        new[] {.11f, .11f, .11f, 0, 0},
                        new float[] {0, 0, 0, 1, 0},
                        new float[] {0, 0, 0, 0, 1}
                    });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
                        0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }


        //Can do it -- unsafe.
//        Bitmap bmp = new Bitmap(1, 1, PixelFormat.Format8bppIndexed);
// ColorPalette monoPalette = bmp.Palette;
// for (int i = 0; i < 256; i++) {
// monoPalette.Entries[i] = Color.FromArgb(i, i, i);
// }

//b8.Palette = monoPalette;

        //static Bitmap Make8bitGrayscale(Bitmap original)
        //{
        //    unsafe
        //    {
        //        //create an empty bitmap the same size as original
        //        Bitmap newBitmap = new Bitmap(original.Width, original.Height, PixelFormat.Format8bppIndexed);

        //        //lock the original bitmap in memory
        //        BitmapData originalData = original.LockBits(
        //            new Rectangle(0, 0, original.Width, original.Height),
        //            ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

        //        //lock the new bitmap in memory
        //        BitmapData newData = newBitmap.LockBits(
        //            new Rectangle(0, 0, original.Width, original.Height),
        //            ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

        //        //number of bytes per pixel in original image
        //        int pixelSize = 3;

        //        for (int y = 0; y < original.Height; y++)
        //        {
        //            //get the data from the original image
        //            byte* oRow = (byte*) originalData.Scan0 + (y*originalData.Stride);

        //            //get the data from the new image
        //            byte* nRow = (byte*) newData.Scan0 + (y*newData.Stride);

        //            for (int x = 0; x < original.Width; x++)
        //            {
        //                //create the grayscale version
        //                /*byte grayScale = // Color image as source
        //                (byte)((oRow[x * pixelSize] * .11) + //B
        //                (oRow[x * pixelSize + 1] * .59) + //G
        //                (oRow[x * pixelSize + 2] * .3)); //R*/
        //                byte grayScale = oRow[x*pixelSize]; // Greyscale

        //                //set the new image's pixel to the grayscale version
        //                nRow[x] = grayScale; // only one byte per pixel in greyscale format
        //                //nRow[x * pixelSize] = grayScale; //B
        //                //nRow[x * pixelSize + 1] = grayScale; //G
        //                //nRow[x * pixelSize + 2] = grayScale; //R
        //            }
        //        }

        //        //unlock the bitmaps
        //        newBitmap.UnlockBits(newData);
        //        original.UnlockBits(originalData);

        //        return newBitmap;
        //    }
        //}
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
