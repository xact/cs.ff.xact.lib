﻿namespace XAct
{
    using System;
    using System.Timers;

    /// <summary>
    /// Extensions to the <see cref="Timer"/> object
    /// </summary>
    public static class TimerExtensions
    {
        private static readonly Random random = new Random();

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Adds randonmess to timer interval.
        /// </summary>
        /// <param name="timer">The timer.</param>
        /// <param name="pollInterval">The poll interval.</param>
        public static void AddRandonmessToTimerInterval(this Timer timer, int pollInterval)
        {

            double interval = timer.Interval;

            int maxValue = pollInterval/4;

            if (maxValue > 1000) 
            {
                maxValue = 1000;
            }

            int num4 = random.Next(2);
            int num5 = random.Next(maxValue);
            interval += (num4 == 0) ? (-num5) : (num5);
            if (interval > (pollInterval + maxValue))
            {
                interval = pollInterval + maxValue;
            }
            if (interval < (pollInterval - maxValue))
            {
                interval = pollInterval - maxValue;
            }

            timer.Interval = interval;
        }
    }
}
