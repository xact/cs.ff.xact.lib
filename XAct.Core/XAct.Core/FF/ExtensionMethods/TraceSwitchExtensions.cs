﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Diagnostics;
    using System.Linq;

#endif

    /// <summary>
    /// Extension Methods to the 
    /// <see cref="TraceSwitch"/>
    /// class.
    /// <para>
    /// Warning: TraceSwitch is not available in CF or Ag Frameworks.
    /// </para>
    /// </summary>
    public static class TraceSwitchExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Returns the max Level of all the TraceSwitches.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// //As Values are Off=0,Error=1,Warning=2,Info=3,Verbose=4: 
        /// Debug.Assert(TraceSwitch.Verbose.Max(TraceSwitch.Error,TraceSwitch.Information)==TraceSwitch.Verbose)
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="traceSwitch">The trace switch.</param>
        /// <param name="traceSwitchs">The trace switchs.</param>
        public static TraceLevel Max(this TraceSwitch traceSwitch, params TraceSwitch[] traceSwitchs)
        {
            byte result = Math.Max((byte) traceSwitch.Level, (byte) traceSwitchs.Max(ts => ts.Level));
            return (TraceLevel) result;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Returns the min Level of all the given TraceSwitches.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// //As Values are Off=0,Error=1,Warning=2,Info=3,Verbose=4: 
        /// Debug.Assert(TraceSwitch.Verbose.Min(TraceSwitch.Error,TraceSwitch.Information)==TraceSwitch.Error)
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="traceSwitch">The trace switch.</param>
        /// <param name="traceSwitchs">The trace switchs.</param>
        public static TraceLevel Min(this TraceSwitch traceSwitch, params TraceSwitch[] traceSwitchs)
        {
            byte result = Math.Min((byte) traceSwitch.Level, (byte) traceSwitchs.Min(ts => ts.Level));
            return (TraceLevel) result;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determine whether the messageLevel is within the 
        /// TraceSwitch's Level.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// //As Values are Off=0,Error=1,Warning=2,Info=3,Verbose=4: 
        /// traceSwitch.Level = TraceLevel.Info;
        /// //As 2 <= 3 therefore:
        /// Debug.Assert(traceSwitch.ShouldTrace(TraceLevel.Warning))
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="traceSwitch">The trace switch.</param>
        /// <param name="messsageTraceLevel">The messsage trace level.</param>
        /// <returns></returns>
        public static bool ShouldTrace(this TraceSwitch traceSwitch, TraceLevel messsageTraceLevel)
        {
            //If the The message level is 
            //error = 1
            //Warning = 2
            //Verbose = 4
            
            return (messsageTraceLevel <= traceSwitch.Level);
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
