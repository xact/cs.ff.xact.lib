﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.IO;
    using XAct.Enums;

    /// <summary>
    /// Extensions to the DirectoryInfo object
    /// </summary>
    public static class DirectoryInfoExtensions
    {

        /// <summary>
        /// Sets the directory's dates.
        /// </summary>
        /// <param name="directoryInfo">The directory information.</param>
        /// <param name="type">The type.</param>
        /// <param name="dateTimeUtc">The date time UTC.</param>
        /// <exception cref="System.NotSupportedException">Only Type=Created, Accessed, Modified are supported.</exception>
        public static void SetDateTime(this DirectoryInfo directoryInfo, AuditableEvent type, DateTime dateTimeUtc)
        {
            switch (type)
            {
                case AuditableEvent.Created:
            Directory.SetCreationTimeUtc(directoryInfo.FullName, dateTimeUtc);
                    break;
                case AuditableEvent.Accessed:
            Directory.SetLastAccessTimeUtc(directoryInfo.FullName, dateTimeUtc);
                    break;
                case AuditableEvent.Modified:
            Directory.SetLastWriteTimeUtc(directoryInfo.FullName, dateTimeUtc);
                    break;
                default:
                    throw new NotSupportedException("Only Type=Created, Accessed, Modified are supported.");

            }
        }

        /// <summary>
        /// Ensures Directory exists before use.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <returns></returns>
        public static bool ExistsOrCreate(this DirectoryInfo info)
        {
            try
            {
                //and does not fail if it already exists:
                //Will create all in between paths as well:
                info.Create();
                if (info.Exists == false)
                {
                    //Can be stale. have to recheck:
                    info.Refresh();
                }
                return info.Exists  ;
            }
            catch
            {
                return false;
            }

        }


        /// <summary>
        /// Tries the find parent file.
        /// <para>
        /// An example use case would be when given a path to a folder containing a *.cs file, and trying to find
        /// the parent *.csproj file.
        /// </para>
        /// </summary>
        /// <param name="directoryInfo">The directory information.</param>
        /// <param name="searchPattern">The search pattern.</param>
        /// <param name="resultFileInfo">The result file information.</param>
        /// <returns></returns>
        public static bool TryFindParentFile(this DirectoryInfo directoryInfo, string searchPattern, out FileInfo resultFileInfo)
        {
            if (directoryInfo == null)
            {
                resultFileInfo = null;
                return false;
            }

            FileInfo[] fileInfos = directoryInfo.GetFiles(searchPattern, SearchOption.TopDirectoryOnly);

            if (fileInfos.Length > 0)
            {
                resultFileInfo = fileInfos[0];
                return true;
            }

            return TryFindParentFile(directoryInfo.Parent, searchPattern, out resultFileInfo);
        }

    }
}
