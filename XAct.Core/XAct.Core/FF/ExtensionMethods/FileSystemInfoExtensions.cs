﻿namespace XAct
{
    using System;
    using System.IO;

    /// <summary>
    /// Extensions to the <see cref="FileSystemInfo"/>
    /// object.
    /// </summary>
    public static class FileSystemInfoExtensions
    {
        /// <summary>
        /// Determines whether the file is a directory or not.
        /// </summary>
        /// <param name="fileSystemInfo">The file system info.</param>
        /// <returns>
        ///   <c>true</c> if [is A directory] [the specified file system info]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsADirectory(this FileSystemInfo fileSystemInfo)
        {
            FileAttributes attr = fileSystemInfo.Attributes;
            return ((attr & FileAttributes.Directory) == FileAttributes.Directory);
        }

        /// <summary>
        /// Sets the create time (UTC).
        /// </summary>
        /// <param name="fileSystemInfo">The file system info.</param>
        /// <param name="creationTimeUtc">The creation time UTC.</param>
        public static void SetCreateTime(this FileSystemInfo fileSystemInfo, DateTime creationTimeUtc)
        {
            if (fileSystemInfo.IsADirectory())
            {
                Directory.SetCreationTimeUtc(fileSystemInfo.FullName,creationTimeUtc);
            }else
            {
                File.SetCreationTimeUtc(fileSystemInfo.FullName, creationTimeUtc);
            }
        }
        /// <summary>
        /// Sets the last write time (UTC).
        /// </summary>
        /// <param name="fileSystemInfo">The file system info.</param>
        /// <param name="lastWriteTimeUtc">The creation time UTC.</param>
        public static void SetLastWriteTime(this FileSystemInfo fileSystemInfo, DateTime lastWriteTimeUtc)
        {
            if (fileSystemInfo.IsADirectory())
            {
                Directory.SetLastWriteTimeUtc(fileSystemInfo.FullName, lastWriteTimeUtc);
            }
            else
            {
                File.SetLastWriteTimeUtc(fileSystemInfo.FullName, lastWriteTimeUtc);
            }
        }

        /// <summary>
        /// Sets the last access time (UTC).
        /// </summary>
        /// <param name="fileSystemInfo">The file system info.</param>
        /// <param name="lastAccessTimeUtc">The creation time UTC.</param>
        public static void SetLastAccessTime(this FileSystemInfo fileSystemInfo, DateTime lastAccessTimeUtc)
        {
            if (fileSystemInfo.IsADirectory())
            {
                Directory.SetLastAccessTimeUtc(fileSystemInfo.FullName, lastAccessTimeUtc);
            }
            else
            {
                File.SetLastAccessTimeUtc(fileSystemInfo.FullName, lastAccessTimeUtc);
            }
        }

    }
}
