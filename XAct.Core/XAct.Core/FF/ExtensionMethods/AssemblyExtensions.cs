﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;

// ReSharper restore CheckNamespace
#endif


    /// <summary>
    /// Extensions to the <see cref="Assembly"/> class.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Example:
    /// <code>
    /// <![CDATA[
    /// Bitmap bitmap = 
    ///   Assembly
    ///     .GetExecutingAssembly()
    ///     .LoadBitmapFromResource("Resources.Progress.gif");
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public static class AssemblyExtensions
    {

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the assembly version.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// Assembly assembly = Assembly.GetEntryAssembly();
        /// Version version = assembly.GetAssemblyVersion();
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static Version GetAssemblyVersion(this Assembly assembly)
        {
            return assembly.GetName().Version;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the assembly file version,
        /// correctly handling any '*' characters.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// Assembly assembly = Assembly.GetEntryAssembly();
        /// Version version = assembly.GetAssemblyFileVersion();
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static Version GetAssemblyFileVersion(this Assembly assembly)
        {

            //This makes "1.0.*" into "1.0"
            //I don't think one is aloud to do "1.0.*.*" but it would also make it "1.0"
            return new Version(System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location).FileVersion.Replace(".*", string.Empty));
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the assembly product version,
        /// correctly handling any '*' characters.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// Assembly assembly = Assembly.GetEntryAssembly();
        /// Version version = assembly.GetAssemblyProductVersion();
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static Version GetAssemblyProductVersion(this Assembly assembly)
        {
            //This makes "1.0.*" into "1.0"
            //I don't think one is aloud to do "1.0.*.*" but it would also make it "1.0"
            return
                new Version(System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion.Replace(".*", string.Empty));
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the path/location of referenced assemblies.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// Assembly assembly = Assembly.GetEntryAssembly();
        /// 
        /// string[] results = 
        ///    assembly.GetReferencedAssemblyLocations();
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static IEnumerable<string> GetReferencedAssemblyLocations(this Assembly assembly)
        {
            return
                assembly.GetReferencedAssemblies().Select(
                    assemblyName => Assembly.ReflectionOnlyLoad(assemblyName.FullName).Location);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Loads the bitmap from resource.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="imageResourcePath">The image resource path.</param>
        /// <returns></returns>
        public static Bitmap LoadBitmapFromResource(this Assembly assembly, string imageResourcePath)
        {
            assembly.ValidateIsNotDefault("assembly");
            if (imageResourcePath.IsNullOrEmpty())
            {
                throw new ArgumentNullException("imageResourcePath");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            Stream stream = assembly.GetManifestResourceStream(imageResourcePath);
            return stream != null ? new Bitmap(stream) : null;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Determines whether the assembly was built in debug mode or not.
        /// <para>
        /// Could be used for post deployment checks.
        /// </para>
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        /// 	<c>true</c> if [is assembly debug build] [the specified assembly]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>6/11/2011: Sky</para></internal>
        /// <internal>
        /// Source: http://www.eggheadcafe.com/tutorials/aspnet/cb350027-3869-433d-90c4-d0af320409c7/aspnet-create-a-debug-build-checker-page-for-your-site.aspx    
        /// </internal>
        public static bool IsAssemblyDebugBuild(this Assembly assembly)
        {
            foreach (object attribute in assembly.GetCustomAttributes(false))
            {
                System.Diagnostics.DebuggableAttribute debuggableAttribute = attribute as System.Diagnostics.DebuggableAttribute;
                // if IgnoreSymbolStoreSequencePoints and DisableOptimizations flags are both present, the assembly was built in debug mode
                if (debuggableAttribute == null)
                {
                    continue;
                }

                return
                    ((int) debuggableAttribute.DebuggingFlags).BitIsSet(
                        (int)System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)
                    &&
                    ((int) debuggableAttribute.DebuggingFlags).BitIsSet(
                        (int)System.Diagnostics.DebuggableAttribute.DebuggingModes.DisableOptimizations);
            }
            return false;
        }


        /// <summary>
        /// Determines if the assembly is a 64 bit assembly (at this point, all should really).
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        /// <internal>If you need to know from a command line, consider looking into DUMPBIN command line</internal>
        public static bool Is64Bit(this Assembly assembly)
        {
            PortableExecutableKinds peKind;
            ImageFileMachine machine;
            assembly.ManifestModule.GetPEKind(out peKind, out machine);
            return (machine != ImageFileMachine.I386);
        }



    }

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
