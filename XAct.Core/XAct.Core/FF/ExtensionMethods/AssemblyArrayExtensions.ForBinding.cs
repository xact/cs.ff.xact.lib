﻿//namespace XAct
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Reflection;
//    using XAct.Services;
//    using XAct.Services.IoC.Initialization;

//    /// <summary>
//    /// 
//    /// </summary>
//    public static partial class AssemblyArrayExtensions
//    {

//        // ReSharper disable InconsistentNaming
//        private static readonly object _lock = new object();
//        // ReSharper restore InconsistentNaming

//        /// <summary>
//        /// Scan given assemblies for 
//        /// <see cref="IBindingDescriptor"/> information, 
//        /// and register them in the 
//        /// current <see cref="DependencyResolver"/>.
//        /// </summary>
//        /// <param name="assemblies">The assemblies.</param>
//        /// <param name="performPreScan">if set to <c>true</c> [perform pre scan].</param>
//        /// <param name="optionalOnErrorBindingCallback">The optional on error binding callback.</param>
//        /// <param name="optionalPostBindingCallback">The optional post binding callback.</param>
//        /// <param name="servicesToRegisterBeforeXActLibServices">The services to register before x act library services.</param>
//        /// <returns></returns>
//        public static InitializeLibraryBindingsResults ScanForDefaultBindingDescriptorsAndRegisterThem(
//            this Assembly[] assemblies,
//            bool performPreScan = true,
//            Action<IBindingDescriptor> optionalOnErrorBindingCallback = null,
//            Action<IBindingDescriptorResult> optionalPostBindingCallback = null,
//            IEnumerable<IBindingDescriptorBase> servicesToRegisterBeforeXActLibServices = null
//            )
//        {
//            lock (_lock)
//            {
//                using (TimedScope totalElapsed = new TimedScope())
//                {

//                    InitializeLibraryBindingsResults bindingsResults =
//                        new InitializeLibraryBindingsResults();


//                    //Keep a list of Services already registered, and services skipped:
//                    IDictionary<string, IBindingDescriptor>
//                        servicesAlreadyRegistered = new Dictionary<string, IBindingDescriptor>();

//                    List<IBindingDescriptor> servicesSkipped = new List<IBindingDescriptor>();


//                    IBindingDescriptor[] preRegisterlist =
//                        FlattenPreRegisterList(servicesToRegisterBeforeXActLibServices);

//                    //Add this flattened list at the front of any services it then finds.
//                    BindingScanResults bindingScanResults = bindingsResults.BindingScanResults;

//                    if (performPreScan)
//                    {
//                        // ReSharper disable RedundantArgumentDefaultValue
//                        assemblies.ScanForDefaultBindingDescriptors(preRegisterlist, true, ref bindingScanResults);
//                        // ReSharper restore RedundantArgumentDefaultValue
//                    }
//                    else
//                    {
//                        bindingsResults.BindingScanResults.BindingDescriptors = preRegisterlist;

//                    }


//                    //----------------------------------------------------------------------
//                    //FIX: Register the service so that it doesn't register itself again
//                    //(it's done the first time by the get_Current() method...registering it again
//                    //confuses the DependencyInjectionContainer)
//                    servicesAlreadyRegistered.Add(
//                        typeof (IDependencyResolver).AssemblyQualifiedName,
//                        new BindingDescriptor(typeof (IDependencyResolver), typeof (DependencyResolver)));

//                    //----------------------------------------------------------------------
//                    using (TimedScope timedScope = new TimedScope())
//                    {
//                        //We need the service to use it's 
//                        //extension method in order to register services:
//                        IDependencyResolver serviceLocatorService = DependencyResolver.Current;

//                        //int counter = 0;
//                        //Register them all
//                        foreach (
//                            IBindingDescriptor serviceRegistrationDescriptor in bindingScanResults.BindingDescriptors)
//                        {

//                            IBindingDescriptorResult result;

//                            string alreadyRegisteredKey = AlreadyRegisteredKey(serviceRegistrationDescriptor);

//                            if (servicesAlreadyRegistered.ContainsKey(alreadyRegisteredKey))
//                            {
//                                //If we have already registered this interface (not implementation)
//                                //we don't do it again:
//                                servicesSkipped.Add(serviceRegistrationDescriptor);
//                                //new BindingDescriptor(
//                                //	interfaceType,
//                                //	implementationType,
//                                //	pair.Value.LifeSpan));

//                                result = new BindingDescriptorResult(serviceRegistrationDescriptor, true);
//                            }
//                            else
//                            {
//                                try
//                                {
//                                    serviceLocatorService.RegisterServiceBindingInIoC(serviceRegistrationDescriptor);

//                                    result = new BindingDescriptorResult(serviceRegistrationDescriptor, false);
//                                }
//                                catch (Exception exception)
//                                {
//                                    result = new BindingDescriptorResult(serviceRegistrationDescriptor, false, exception);

//                                    if (optionalOnErrorBindingCallback != null)
//                                    {
//                                        optionalOnErrorBindingCallback(serviceRegistrationDescriptor);
//                                    }


//                                    if (result.ThrowException)
//                                    {
//                                        throw;
//                                    }

//                                }


//                                //Save a ref to the services already registered:
//                                alreadyRegisteredKey = AlreadyRegisteredKey(serviceRegistrationDescriptor);

//                                servicesAlreadyRegistered[alreadyRegisteredKey]
//                                    = serviceRegistrationDescriptor;
//                            }

//                            if (optionalPostBindingCallback != null)
//                            {
//                                optionalPostBindingCallback(result);
//                            }


//                        } //~loop

//                        bindingsResults.TimeToRegisterElapsed = timedScope.Elapsed;
//                    }



//                    bindingsResults.ServiceBindingsRegistered = servicesAlreadyRegistered.Values.ToArray();
//                    bindingsResults.ServiceBindingsSkipped = servicesSkipped.ToArray();
//                    //bindingsResults.IsInitialized 
//                    bindingsResults.TotalTimeElapsed = totalElapsed.Elapsed;

//                    //And save a copy in a WellKnown location:
//                    bindingsResults.IsInitialized = true;

//                    XAct.Library.Status.Initialization.BindingResults = bindingsResults;





//                    return bindingsResults;
//                }

//            } //~lock

//        }

//        private static string AlreadyRegisteredKey(IBindingDescriptor serviceRegistrationDescriptor)
//        {
//            string alreadyRegisteredKey =
//                serviceRegistrationDescriptor.InterfaceType.AssemblyQualifiedName;

//            if (!serviceRegistrationDescriptor.Tag.IsNullOrEmpty())
//            {
//                alreadyRegisteredKey += ":" + serviceRegistrationDescriptor.Tag;
//            }
//            return alreadyRegisteredKey;
//        }


//        private static IBindingDescriptor[] FlattenPreRegisterList(
//            IEnumerable<IBindingDescriptorBase> servicesToRegisterBeforeXActLibServices)
//        {

//            List<IBindingDescriptor> results = new List<IBindingDescriptor>();
//            if (servicesToRegisterBeforeXActLibServices == null)
//            {
//                //Getting out early helps with recursive registration:
//                return results.ToArray();
//            }

//            foreach (
//                IBindingDescriptorBase serviceRegistrationDescriptorBase in
//                    servicesToRegisterBeforeXActLibServices)
//            {

//                if (serviceRegistrationDescriptorBase is IBindingDescriptorGroup)
//                {

//                    IBindingDescriptorGroup serviceBindingDescriptorGroup =
//                        serviceRegistrationDescriptorBase as IBindingDescriptorGroup;

//                    //Register children -- if any -- first:
//                    results.AddRange(FlattenPreRegisterList(serviceBindingDescriptorGroup.ServiceBindings));

//                    continue;
//                }


//                IBindingDescriptor serviceRegistrationDescriptor =
//                    serviceRegistrationDescriptorBase as IBindingDescriptor;

//                results.Add(serviceRegistrationDescriptor);

//            }
//            return results.ToArray();

//        }
//    }
//}
