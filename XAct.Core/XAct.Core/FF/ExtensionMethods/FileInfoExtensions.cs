﻿//OK

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.IO;
    using XAct.Environment;
    using XAct.IO;

// ReSharper restore CheckNamespace
#endif

/// <summary>
    /// Extensions to the <see cref="FileInfo"/> class
    /// </summary>
    /// <internal><para>8/16/2011: Sky</para></internal>
    public static class FileInfoExtensions
    {
    private static IDateTimeService DateTimeService
    {
        get { return XAct.DependencyResolver.Current.GetInstance<IDateTimeService>(); }

    }

    private static IEnvironmentService EnvironmentService
    {
        get { return XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>(); }
    }


    /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Ensures paths are absolute or not
        /// </summary>
        /// <remarks>
        /// <para>
        /// If the path starts with "D:\" or "\\" or "d:/" or "//", it's absolute
        /// </para>
        /// </remarks>
        /// <param name = "fileInfo"></param>
        public static bool IsPathAbsolute(this FileInfo fileInfo)
        {
            fileInfo.ValidateIsNotDefault("fileInfo");
            string path = fileInfo.FullName;

            if (path == string.Empty)
            {
                return false;
            }

            //If not long enough to ever be Physical path, return false:
            if (path.Length < 3)
            {
                return false;
            }

            //if Path is either format "D:\" or "\\" or "d:/" or "//":
            if ((path[1] == ':') && IsDirectorySeparatorChar(path[2], true))
            {
                return true;
            }
            //If Path starts with '\\' it's a network absolute
            if (IsDirectorySeparatorChar(path[0], false) && IsDirectorySeparatorChar(path[1], false))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Returns true if the given char is '\', and/or '/' character.
        /// </summary>
        /// <param name = "sepChar">The char to test.</param>
        /// <param name = "eitherDirection">Flag to test for forward slash as well.</param>
        /// <returns>True if char is '\\' and if eitherDirection flag is set, '/'.</returns>
        private static bool IsDirectorySeparatorChar(char sepChar, bool eitherDirection)
        {
            if (sepChar == '\\')
            {
                return true;
            }
            return (eitherDirection) && (sepChar == '/');
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Returns the part of the fileInfo's path, relative to the given directory.
        /// <para>
        /// An example would be:
        /// <code>
        /// <![CDATA[
        ///  new FileInfo("c:\\A\\b\\c\\d\\e.txt").RelativeTo(new DirectoryInfo("c:\\a\\b\\c\\"),false);
        ///  //returns:
        ///  "\\d\\e.txt" (note the leading slash).
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="fileInfo">The file info.</param>
        /// <param name="directoryInfo">The directory info.</param>
        /// <param name="throwExceptionIfNotCorrelation">if set to <c>true</c> [throw exception if not correlation].</param>
        /// <returns></returns>
        public static FileInfo RelativeTo(this FileInfo fileInfo, DirectoryInfo directoryInfo, bool throwExceptionIfNotCorrelation)
        {
            string file = fileInfo.FullName;

            if (directoryInfo == null)
            {
                return fileInfo;
            }

            string dir = directoryInfo.FullName;
            if (dir.EndsWith("\\")) { dir = dir.Substring(0, dir.Length - 1); }

            if (file.ToLower().StartsWith(dir.ToLower()))
            {
                return new FileInfo(file.Substring(dir.Length));
            }
            if (throwExceptionIfNotCorrelation)
            {
                throw new ArgumentOutOfRangeException("fileInfo");
            }

            return new FileInfo(file);
        }




        /// <summary>
        /// Sets the create time (UTC).
        /// </summary>
        /// <param name="fileInfo">The file system info.</param>
        /// <param name="creationTimeUtc">The creation time UTC.</param>
        public static void SetCreateTime(this FileInfo fileInfo, DateTime creationTimeUtc)
        {
                File.SetCreationTimeUtc(fileInfo.FullName, creationTimeUtc);
        }
        /// <summary>
        /// Sets the last write time (UTC).
        /// </summary>
        /// <param name="fileInfo">The file system info.</param>
        /// <param name="lastWriteTimeUtc">The creation time UTC.</param>
        public static void SetLastWriteTime(this FileInfo fileInfo, DateTime lastWriteTimeUtc)
        {
                File.SetLastWriteTimeUtc(fileInfo.FullName, lastWriteTimeUtc);
        }
        /// <summary>
        /// Sets the last access time (UTC).
        /// </summary>
        /// <param name="fileInfo">The file system info.</param>
        /// <param name="lastAccessTimeUtc">The creation time UTC.</param>
        public static void SetLastAccessTime(this FileInfo fileInfo, DateTime lastAccessTimeUtc)
        {
                File.SetLastAccessTimeUtc(fileInfo.FullName, lastAccessTimeUtc);
        }

        /// <summary>
        /// Gets the age of the file.
        /// <para>
        /// Default is to use <c>LastWriteTimeUtc</c>
        /// as <c>LastCreateTimeUtc</c> can get sticky, and cached.
        /// </para>
        /// </summary>
        /// <param name="fileInfo">The file info.</param>
        /// <param name="fileDateType">Type of the file date.</param>
        /// <param name="nowUtc">The now UTC.</param>
        /// <returns></returns>
        public static TimeSpan GetFileAge(this FileInfo fileInfo, DateTime? nowUtc=null, FileDateType fileDateType= FileDateType.LastModified )
        {
            DateTime checkDateUTC = GetDateFromFileInfo(fileInfo, fileDateType);

            if (!nowUtc.HasValue)
            {
                nowUtc = DateTimeService.NowUTC;
            }

            TimeSpan fileAge = nowUtc.Value - checkDateUTC;

            return fileAge;
        }

        /// <summary>
        /// Gets the date from file info.
        /// <para>
        /// Default is to use <c>LastWriteTimeUtc</c>
        /// as <c>LastCreateTimeUtc</c> can get sticky, and cached.
        /// </para>
        /// </summary>
        /// <param name="fileInfo">The file info.</param>
        /// <param name="fileDateType">Type of the file date.</param>
        /// <param name="forceRefresh">if set to <c>true</c> force refresh of dates, to get around stale dates being handed back.</param>
        /// <returns></returns>
        public static DateTime GetDateFromFileInfo(this FileInfo fileInfo, FileDateType fileDateType=FileDateType.LastModified, bool forceRefresh=true)
        {
            if (forceRefresh)
            {
                fileInfo.Refresh();
            }

            DateTime checkDate;

            switch (fileDateType)
            {
                case FileDateType.LastModified:
                    checkDate = fileInfo.LastWriteTimeUtc;
                    break;
                case FileDateType.LastAccessed:
                    checkDate = fileInfo.LastAccessTimeUtc;
                    break;
                default:
                    checkDate = fileInfo.CreationTimeUtc;
                    break;
            }
            return checkDate;
        }






        /// <summary>
        /// Prefixes the name of the file with something.
        /// <para>
        /// Give <c>'C:\SomeDir\SomeFile.txt' and '_20121001'</c>
        /// returns
        /// <c>'C:\SomeDir\SomeFile_20121001.txt' </c>
        /// </para>
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="fileNameSuffix">The file name prefix.</param>
        /// <returns></returns>
        public static string SuffixFileName(this FileInfo filePath, string fileNameSuffix)
        {
            string toPath = filePath.FullName;

            
            string altFileName =
                "{0}{1}{2}".FormatStringInvariantCulture(
                    Path.GetFileNameWithoutExtension(toPath),
                    fileNameSuffix,
                    Path.GetExtension(toPath)
                    );

            string dir = Path.GetDirectoryName(toPath);
            if (!dir.IsNullOrEmpty())
            {
                altFileName = dir + Path.DirectorySeparatorChar + altFileName;
            }   

            return altFileName;
        }

        /// <summary>
        /// Prefixes the name of the file.
        /// <para>
        /// Give <c>'C:\SomeDir\SomeFile.txt' and '_locked_'</c>
        /// returns
        /// <c>'C:\SomeDir\_locked_SomeFile.txt' </c>
        /// </para>
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="fileNamePrefix">The file name prefix.</param>
        /// <returns></returns>
        public static string PrefixFileName(this FileInfo filePath, string fileNamePrefix)
        {
            string toPath = filePath.FullName;

            string altFileName =
    "{0}\\{1}{2}".FormatStringInvariantCulture(
        Path.GetDirectoryName(toPath),
        fileNamePrefix,
        Path.GetFileName(toPath)
        );
            return altFileName;

        }





        /// <summary>
        /// Given a FileInfo, tries to find the specified file in a parent directory.
        /// <para>
        /// An example use case would be when given a path to a *.cs file, and trying to find
        /// the parent *.csproj file.
        /// </para>
        /// </summary>
        /// <param name="fileInfo">The file information.</param>
        /// <param name="searchPattern">The search pattern.</param>
        /// <param name="resultFileInfo">The result file information.</param>
        /// <returns></returns>
        public static bool TryFindParentFile(this FileInfo fileInfo, string searchPattern,
                                     out FileInfo resultFileInfo)
        {
            if (fileInfo == null)
            {
                resultFileInfo = null;
                return false;
            }
            return fileInfo.Directory.TryFindParentFile(searchPattern, out resultFileInfo);
        }


    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
