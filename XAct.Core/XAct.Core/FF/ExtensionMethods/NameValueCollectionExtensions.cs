﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Linq;

// ReSharper restore CheckNamespace
#endif


    /// <summary>
    ///   Extension Methods for
    ///   <see cref = "NameValueCollection" />s.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class NameValueCollectionExtensions
    {
        private static readonly CultureInfo _invariantCulture
            = CultureInfo.InvariantCulture;

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Converts the current <see cref = "NameValueCollection" />
        ///   to a more flexible dictionary.
        ///   <para>
        ///     Note that even though the source collection is string/string collection, 
        ///     the result is string/object dictionary -- not a string/string dictionary.
        ///   </para>
        /// </summary>
        /// <param name = "thisNameValueCollection">The current collection.</param>
        /// <returns></returns>
        public static Dictionary<string, string> ToDictionary(this NameValueCollection thisNameValueCollection)
        {
            return thisNameValueCollection.Cast<KeyValuePair<string, string>>().ToDictionary(
                pair => pair.Key,
                pair => pair.Value);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Helper method used by the Init sequence of a provider 
        ///   to extract a value from the passed NameValueCollection. 
        ///   Reverts to Default value if no value found.
        /// </summary>
        /// <param name = "nameValueCollection">The config.</param>
        /// <param name = "attributeTag">The attribute tag.</param>
        /// <param name = "defaultValue">The default value.</param>
        /// <param name = "sqlKeywords">The SQL key words.</param>
        [SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "sqlKeywords")]
        public static TValue InitializeParam<TValue>(
            this NameValueCollection nameValueCollection,
            string attributeTag,
            TValue defaultValue,
            StringDictionary sqlKeywords = null)
        {
            nameValueCollection.ValidateIsNotDefault("nameValueCollection");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
Contract.EndContractBlock();
#endif

            TValue result = nameValueCollection[attributeTag].ConvertTo(defaultValue);

            if ((sqlKeywords != null) && (typeof (TValue).IsAssignableFrom(typeof (string))))
            {
                if (attributeTag == "dbStringMatchCriteria")
                {
                    attributeTag = "LIKE";
                }
                if (attributeTag == "dbParamPlaceHolderChar")
                {
                    attributeTag = "DBPARAM";
                }
                if (!sqlKeywords.ContainsKey(attributeTag))
                {
                    sqlKeywords.Add(attributeTag, result as string);
                }
            }

            return result;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Extracts from the NameValueCollection the Typed version of the Key's value.
        /// </summary>
        /// <param name = "formOrQueryStringNameValueCollection">The form or query string name value collection.</param>
        /// <param name = "key">A QueryString key -- eg 'pageIndex'</param>
        /// <returns>A typed value.</returns>
        public static T PageNumber<T>(this NameValueCollection formOrQueryStringNameValueCollection, string key)
        {
            return formOrQueryStringNameValueCollection == null
                       ? default(T)
                       : formOrQueryStringNameValueCollection[key].ConvertTo<T>();
        }
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
