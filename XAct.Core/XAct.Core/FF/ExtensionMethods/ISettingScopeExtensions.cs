﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct
{
    using XAct.Settings;

    /// <summary>
    /// Extension methods to entities that 
    /// implement <see cref="IApplicationSettingScope"/> 
    /// (such as <see cref="Setting"/>)
    /// </summary>
    public static class IApplicationSettingScopeExtensions
    {
        /// <summary>
        /// Contexts the identity.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        public static string ContextIdentity(this IApplicationSettingScope setting)
        {
            return new[]
                {
                    setting.EnvironmentIdentifier,
                    setting.ZoneOrTierIdentifier,
                    setting.HostIdentifier
                    //setting.TennantIdentifier.ToString("N"), //As 32character without dashes.
                    //setting.UserIdentifier
                }.Join(":");
        }
    }
}
