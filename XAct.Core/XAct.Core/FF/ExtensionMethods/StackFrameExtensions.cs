﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Diagnostics;
    using System.Reflection;

// ReSharper restore CheckNamespace
#endif

/// <summary>
    /// Extensions to the StackFrame object.
    /// </summary>
    /// <internal><para>8/10/2011: Sky</para></internal>
    public static class StackFrameExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the type of the class containing the currently executing Method.
        /// </summary>
        /// <param name="stackFrame">The stack frame.</param>
        /// <returns></returns>
        /// <internal><para>8/10/2011: Sky</para></internal>
        public static Type GetDeclaringType(this StackFrame stackFrame)
        {
            return stackFrame.GetMethod().DeclaringType;
        }

        /// <summary>
        /// Gets a string representation of the method (useful for logging purposes).
        /// </summary>
        /// <param name="stackFrame">The stack frame.</param>
        /// <param name="includeClassName">if set to <c>true</c> [include class name].</param>
        /// <returns></returns>
        /// <internal><para>8/10/2011: Sky</para></internal>
        public static string GetMethodSignature(this StackFrame stackFrame, bool includeClassName = false)
        {
            MethodInfo methodInfo = stackFrame.GetMethod() as MethodInfo;
            return methodInfo.ToStringSignature(includeClassName);
        }
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
