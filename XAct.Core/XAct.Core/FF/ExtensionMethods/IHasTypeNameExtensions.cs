﻿using System;

namespace XAct
{
    /// <summary>
    /// Extensions to entities implementing 
    /// the 
    /// <see cref="IHasTypeName"/>
    /// contract
    /// </summary>
    public static class IHasTypeNameExtensions
    {

        /// <summary>
        /// Gets the type from the given type name.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public static Type GetTypeFromTypeName(this IHasTypeName entity)
        {
            return AppDomain.CurrentDomain.GetTypeFromTypeFullName(entity.TypeName);
        }

        /// <summary>
        /// Gets the type from the given type name.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public static Type GetTypeFromTypeName(this IHasAssemblyAndTypeAndMethodNames entity)
        {
            return AppDomain.CurrentDomain.GetTypeFromTypeFullName(entity.TypeFullName, entity.AssemblyName);
            
        }
    }
}
