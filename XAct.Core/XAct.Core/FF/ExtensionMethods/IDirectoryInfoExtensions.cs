﻿namespace XAct.XTensions
{
    using System.IO;
    using XAct.Enums;
    using XAct.IO;

    /// <summary>
    /// Extensions to <see cref="DirectoryInfo"/>
    /// </summary>
    public static class DirectoryInfoExtensions
    {

        ///// <summary>
        ///// Sets the directory's dates.
        ///// </summary>
        ///// <param name="directoryInfo">The directory information.</param>
        ///// <param name="type">The type.</param>
        ///// <param name="dateTimeUtc">The date time UTC.</param>
        ///// <exception cref="System.NotSupportedException">Only Type=Created, Accessed, Modified are supported.</exception>
        //public static void SetDateTime(this DirectoryInfo directoryInfo, AuditableEvent type, DateTime dateTimeUtc)
        //{
        //    XAct.DependencyResolver.Current.GetInstance<IFSIOService>().FileSetDateTimeAsync(directoryInfo.FullName,type,dateTimeUtc);
        //}

    }
}
