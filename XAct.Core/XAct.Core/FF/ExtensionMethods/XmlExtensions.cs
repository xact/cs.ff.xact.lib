﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Diagnostics.Contracts;
    using System.Xml;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    ///   Extensions for Xml entities.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    public static class XmlExtensions
    {

//#if (CE) || (PocketPC) || (pocketPC) || (WindowsCE)
//    //CE:
//      public MSXML2.DOMDocument Document {
//        get {
//          if ((_Document == null) && (string.IsNullOrEmpty(_Path)) && (ioService.Exists(_Path))) {
//            _Document = new MSXML2.DOMDocument();
//            _Document.load(_Path);
//          }
//          return _Document;
//        }
//      }
//      private MSXML2.DOMDocument _Document;
//#endif

        
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the given element's attribute value. If none found,
        /// can optionally look for a nested tag with the given name.
        /// <para>
        /// Note: Returns found string, or null if not found.
        /// </para>
        /// </summary>
        /// <param name="xmlElement">The XmlElement.</param>
        /// <param name="tag">The Attribute tag.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="checkChildTagsAsWell">if set to <c>true</c> look for value within child elements as well.</param>
        /// <returns>Returns found string, or null.</returns>
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public static TValue GetAttributeValue<TValue>(this XmlElement xmlElement, string tag,
                                                       TValue defaultValue,
                                                       bool checkChildTagsAsWell = false)
        {
            xmlElement.ValidateIsNotDefault("xmlElement");
            if (tag.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("tag");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            //See if there is an attribute first:
            XmlAttribute attr = xmlElement.GetAttributeNode(tag);
            if (attr != null)
            {
                string value = attr.Value;
                return string.IsNullOrEmpty(value) ? defaultValue : value.ConvertTo<TValue>();
            }

            if (checkChildTagsAsWell)
            {
                //If not,fish around for a child xmlElement that has the given tag:
                XmlElement childElement = xmlElement.SelectSingleNode(tag) as XmlElement;

                if (childElement != null)
                {
                    string value = childElement.InnerText;
                    return string.IsNullOrEmpty(value) ? defaultValue : value.ConvertTo<TValue>();
                }
            }

            //Neither:
            return defaultValue;
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Sets the attribute value of a given XmlElement.
        ///   <para>
        ///     Note: If given null, corrects it to String.Empty before saving.
        ///   </para>
        /// </summary>
        /// <param name = "xmlElement">The XmlElement to be parsed.</param>
        /// <param name = "tag">The Attribute tag.</param>
        /// <param name = "value">The value.</param>
        /// <returns>The updated existing Attribute, or new one that was created.</returns>
        /// <exception cref = "System.ArgumentNullException">An exception is raised if the xmlElement is null.</exception>
        /// <exception cref = "System.ArgumentNullException">An exception is raised if the tag is null.</exception>
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public static XmlAttribute SetAttributeValue<TValue>(this XmlElement xmlElement, string tag, TValue value)
        {
            xmlElement.ValidateIsNotDefault("xmlElement");
            if (tag.IsDefaultOrNotInitialized())
            {
                throw new ArgumentNullException("tag");
            }
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            XmlAttribute xmlAttribute = xmlElement.Attributes[tag];

            if (xmlAttribute == null)
            {
                if (xmlElement.OwnerDocument == null)
                {
                    throw new ArgumentException("xmlElement.OwnerDocument is null");
                }
                // ReSharper disable PossibleNullReferenceException
                xmlAttribute = xmlElement.OwnerDocument.CreateAttribute(tag);
                // ReSharper restore PossibleNullReferenceException
                xmlElement.Attributes.Append(xmlAttribute);
            }

            

            xmlAttribute.Value = value.ConvertTo<string>() ?? string.Empty;

            return xmlAttribute;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the child element.
        /// </summary>
        /// <param name="xmlElement">The XML element.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="createIfNotFound">if set to <c>true</c> [create if not found].</param>
        /// <returns></returns>
        /// <internal><para>8/16/2011: Sky</para></internal>
        public static XmlElement GetChildElement(this XmlElement xmlElement, string elementName, bool createIfNotFound)
        {
            xmlElement.ValidateIsNotDefault("xmlElement");
            xmlElement.OwnerDocument.ValidateIsNotDefault("xmlElement." + "OwnerDocument");
            XmlElement childElement =
                xmlElement.SelectSingleNode(elementName) as XmlElement;

            if ((childElement == null) && (createIfNotFound))
            {
                //If it didn't exist, create:
                if (xmlElement.OwnerDocument != null)
                {
                    childElement = xmlElement.OwnerDocument.CreateElement(elementName);
                    xmlElement.AppendChild(childElement);
                }
            }
            return childElement;
        }
    
    
    
    
    
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}



#endif




