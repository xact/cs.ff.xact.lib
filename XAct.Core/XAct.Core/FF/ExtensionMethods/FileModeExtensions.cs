﻿namespace XAct
{
    using System;
    using XAct.IO;

    /// <summary>
    /// Extensions to the <see cref="XAct.IO.FileMode"/>
    /// enumeration.
    /// </summary>
    public static class FileModeExtensions
    {
        /// <summary>
        /// Maps the given portable enum value to a <see cref="System.IO.FileMode"/> enum value.
        /// </summary>
        /// <param name="fileMode">The file mode.</param>
        /// <returns></returns>
        /// <exception cref="System.IndexOutOfRangeException"></exception>
        public static System.IO.FileMode MapTo(this XAct.IO.FileMode fileMode)
        {
            switch (fileMode)
            {
                case FileMode.Append:
                    return System.IO.FileMode.Append;
                case FileMode.Create:
                    return System.IO.FileMode.Create;
                case FileMode.CreateNew:
                    return System.IO.FileMode.CreateNew;
                case FileMode.Open:
                    return System.IO.FileMode.Open;
                case FileMode.OpenOrCreate:
                    return System.IO.FileMode.OpenOrCreate;
                case FileMode.Truncate:
                    return System.IO.FileMode.Truncate;
                default:
                    throw new IndexOutOfRangeException();

            }
        }
    }
}