﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using XAct.Settings;

    /// <summary>
    /// Extensions to the <see cref="XAct.Settings.Setting"/>.
    /// </summary>
    public static class SettingExtensions
    {

        /// <summary>
        /// Serializes the specified <see cref="Setting" />
        /// as a <see cref="SerializedApplicationSetting" />
        /// <para>
        /// Invoked only when *new* Settings is added to existing
        /// collection of <see cref="SerializedApplicationSetting"/>
        /// </para>
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <param name="applicationSettings">The owner settings.</param>
        /// <returns></returns>
        public static SerializedApplicationSetting SerializeAsSerializedSetting(this Setting setting, ApplicationSettings applicationSettings)
        {
            SerializedApplicationSetting serializedSetting = new SerializedApplicationSetting();

            serializedSetting.Id = setting.Id;

            ApplicationSettingScope applicationSettingScope = applicationSettings.ContextIdentifier;

            //Can't Set this...
            
            //serializedSetting.EnvironmentIdentifier = applicationSettingScope.EnvironmentIdentifier;
            //serializedSetting.ZoneOrTierIdentifier = applicationSettingScope.ZoneOrTierIdentifier;
            //serializedSetting.HostIdentifier = applicationSettingScope.HostIdentifier;
            //serializedSetting.TennantIdentifier = applicationSettingScope.TennantIdentifier;

            serializedSetting.Key = setting.Name;

            //Non persisted value (only used to round-trip when persisting):
            serializedSetting.Scope = setting.EditingMetadata.Scope;

            //Convert the Value to serialization values:
            SerializedObject serializedObject = setting.Serialize();

            serializedSetting.SerializationMethod = serializedObject.SerializationMethod;
            serializedSetting.SerializedValueType = serializedObject.SerializedValueType;
            serializedSetting.SerializedValue = serializedObject.SerializedValue;

            //SerializationMethod serializationMethod = serializedObject.SerializationMethod;
            //serializedSetting.SerializedDefaultValue = setting.ValueType.Serialize(settingEditingMetadata.DefaultValue, ref serializationMethod);


            return serializedSetting;
        }


    }
}