﻿namespace XAct
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text;

    /// <summary>
    /// 
    /// </summary>
    public static class IListExtensionsFF
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts the IList the CSV
        /// <para>
        /// first line is the names of the Properties in the List Item).
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data.</param>
        /// <param name="includePropertyNamesOnFistLine">if set to <c>true</c> [include property names on fist line].</param>
        /// <returns></returns>
        public static string ToCsv<T>(this IList<T> data, bool includePropertyNamesOnFistLine=true)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            StringBuilder sb = new StringBuilder();

            if (includePropertyNamesOnFistLine)
            {
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    sb.Append(prop.Name);
                    if (i < props.Count - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append(System.Environment.NewLine);
            }

            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                    sb.Append(values[i].ToString());
                    if (i < values.Length - 1)
                    {
                        sb.Append(",");
                    }
                }
                sb.Append(System.Environment.NewLine);
            }
            return sb.ToString();
        }
    }
}