﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace XAct
//{
//    using System.IO;
//    using System.Xml;
//    using System.Xml.Linq;

//    /// <summary>
//    /// Non PCL Etensions to the XDocument object.
//    /// </summary>
//    public static class XDocumentExtensions
//    {
//        /// <summary>
//        /// To the stream with declaration.
//        /// </summary>
//        /// <param name="xDocument">The x document.</param>
//        /// <param name="stream">The stream.</param>
//        /// <param name="encoding">The encoding.</param>
//        public static void ToStreamWithDeclaration(this XDocument xDocument, Stream stream, Encoding encoding = null)
//        {
//            //XmlTextWriter is not PCL?!?!
//            //using (var writer = new System.Xml.XmlTextWriter(stream, encoding))
//            //{
//            //    xDocument.WriteTo(writer);
//            //    writer.Close();
//            //}

//            XmlWriterSettings settings = new XmlWriterSettings {Encoding = encoding};

//            using (XmlWriter writer = XmlWriter.Create(stream, settings))
//            {
//                xDocument.WriteTo(writer);
//            }
//        }
//    }
//}
