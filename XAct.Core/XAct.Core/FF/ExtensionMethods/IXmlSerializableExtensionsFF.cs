﻿namespace XAct
{
    using System.Diagnostics.Contracts;
    using System.Xml;
    using System.Xml.Serialization;
    using XAct.Xml;

    /// <summary>
    /// 
    /// </summary>
    public static class IXmlSerializableExtensionsFF
    {
        //AWESOME:http://www.codeproject.com/Tips/285472/Get-rid-of-XmlInclude-when-using-XmlSerializer#alternative4
        //See:
        //        // use reflection to get all derived types
        //var knownTypes = Assembly.GetExecutingAssembly().GetTypes().Where(
        //    t => typeof(Car).IsAssignableFrom(t) || typeof(
        //    Wheel).IsAssignableFrom(t) || typeof(Door).IsAssignableFrom(t)).ToArray();

        //// prepare to serialize a car object
        //XmlSerializer serializer = new XmlSerializer(typeof(Car), knownTypes);

        //// serialize!
        //TextWriter textWriter = new StreamWriter(@"car.xml", false, Encoding.UTF8);
        //serializer.Serialize(textWriter, car);
        //textWriter.Close();

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Serialize the object and return an XmlNode.
        /// </summary>
        /// <param name = "objectToSerialize">The object to serialize.</param>
        //[SuppressMessage("Microsoft.Naming", "CA1720", Justification = "objectToSerialize is more descriptive than 'value'.")]
        public static XmlNode ToXmlNode(this IXmlSerializable objectToSerialize)
        {
            objectToSerialize.ValidateIsNotDefault("objectToSerialize");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif


            XmlDocument xmlDocument = new XmlDocument();
            using (XmlNodeWriter xmlNodeWriter = new XmlNodeWriter(xmlDocument))
            {
                objectToSerialize.WriteXml(xmlNodeWriter);
            }
            return xmlDocument.DocumentElement;
        }


    }
}