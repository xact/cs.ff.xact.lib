﻿namespace XAct
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// 
    /// </summary>
    public static class DateTimeFFExtensions
    {

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convert Date to Integer format.
        /// </summary>
        /// <param name = "dateTime">DateTime to convert.</param>
        /// <returns>Integer representation fo Date.</returns>
        /// <remarks>
        ///   <para>
        ///     The Integer will represent days since 30 december 1899 at midnight.
        ///   </para>
        /// </remarks>
        [SuppressMessage("Microsoft.Naming", "CA1720", Justification = "dateTime is more descriptive than 'value'.")]
        public static int ToInt(this DateTime dateTime)
        {
            return (Int32)dateTime.ToOADate();
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Convert DateTime to Double format.
        /// </summary>
        /// <param name = "dateTime">DateTime to convert</param>
        /// <returns>Double representation of Date.</returns>
        /// <remarks>
        ///   The Integer will represent days since 30 december 1899 at midnight, the float, the number of ticks.
        /// </remarks>
        public static double ToDouble(this DateTime dateTime)
        {
            return dateTime.ToOADate();
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Adds the specified interval amount (interval amount can be negative as well).
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <param name="timeInterval">The time interval.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        /// <internal><para>6/8/2011: Sky</para></internal>
        public static DateTime AddInterval(this DateTime dateTime, TimeInterval timeInterval, int amount)
        {
            switch (timeInterval)
            {
                case TimeInterval.Second:
                    return dateTime.AddSeconds(amount);
                case TimeInterval.Minute:
                    return dateTime.AddMinutes(amount);
                case TimeInterval.Hour:
                    return dateTime.AddHours(amount);
                case TimeInterval.ThreeHours:
                    return dateTime.AddHours(amount * 3);
                case TimeInterval.SixHours:
                    return dateTime.AddHours(amount * 6);
                case TimeInterval.TwelveHours:
                    return dateTime.AddHours(amount * 12);
                case TimeInterval.Day:
                    return dateTime.AddDays(amount);
                case TimeInterval.Week:
                    return dateTime.WeekStart().AddDays(amount * 7);
                case TimeInterval.Month:
                    return dateTime.MonthStart().AddMonths(amount);
                case TimeInterval.Quarter:
                    return dateTime.QuarterStart().AddMonths(amount * 3);
                case TimeInterval.Year:
                    return dateTime.YearStart().AddYears(amount);
                default:
                    return dateTime;
            }
        }

    }
}