﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.IO.IsolatedStorage;
    using System.Text;
    using System.Xml;

// ReSharper restore CheckNamespace
#endif

/// <summary>
    /// Extensions to the IsolatatedStorageFileStream class.
    /// </summary>
    /// <internal><para>8/16/2011: Sky</para></internal>
    public static class IsolatedStorageFileStreamExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   (OverLoad) Loads an <see cref = "XmlDocument" /> 
        ///   from the specified <see cref = "IsolatedStorageFileStream" />.
        /// </summary>
        /// <param name = "isolatedStorageFileStream">The <see cref = "IsolatedStorageFileStream" />.</param>
        /// <param name = "xmlDocument">The <see cref = "XmlDocument" /> to save.</param>
        /// <returns>Returns <c>true</c> if the document was found and parsed correctly.</returns>
        public static void Load(this IsolatedStorageFileStream isolatedStorageFileStream, ref XmlDocument xmlDocument)
        {
            isolatedStorageFileStream.ValidateIsNotDefault("isolatedStorageFileStream");
            xmlDocument.ValidateIsNotDefault("xmlDocument");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            xmlDocument.Load(isolatedStorageFileStream);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Saves the given <see cref="XmlDocument"/> to the given <see cref="IsolatedStorageFileStream"/>.
        /// </summary>
        /// <param name="isolatedStorageFileStream">The <see cref="IsolatedStorageFileStream"/>.</param>
        /// <param name="xmlDocument">The <see cref="XmlDocument"/>.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns>
        /// Returns <c>true</c> if the document was saved correctly.
        /// </returns>
        public static bool Save(this IsolatedStorageFileStream isolatedStorageFileStream, XmlDocument xmlDocument,
                                Encoding encoding = null)
        {
            isolatedStorageFileStream.ValidateIsNotDefault("isolatedStorageFileStream");
            xmlDocument.ValidateIsNotDefault("xmlDocument");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            if (encoding == null)
            {
                encoding = Encoding.Default;
            }
            using (XmlTextWriter xmlWriter = new XmlTextWriter(isolatedStorageFileStream, encoding))
            {
                xmlDocument.WriteTo(xmlWriter);
                xmlWriter.Close();

                return true;
            }
        }
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
