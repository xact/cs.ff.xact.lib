﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System.Collections.Specialized;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    /// Extensions to the StringDictionary.
    /// </summary>
    /// <internal><para>6/6/2011: Sky</para></internal>
    public static class StringDictionaryExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the Typed value -- or the given default value.
        /// <para>
        /// Useful for Xml Attributes.
        /// </para>
        /// </summary>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        /// <internal><para>6/6/2011: Sky</para></internal>
        public static TProperty GetValue<TProperty>(this StringDictionary dictionary, string key, TProperty defaultValue)
        {
            if (!dictionary.ContainsKey(key))
            {
                return defaultValue;
            }

            string textValue = dictionary[key];
            if (string.IsNullOrEmpty(textValue))
            {
                return defaultValue;
            }

            TProperty result = defaultValue.ConvertTo<TProperty>();

            return result.IsUnitialized() ? defaultValue : result;
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
