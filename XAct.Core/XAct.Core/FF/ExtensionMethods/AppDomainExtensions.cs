﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security;
    using System.Security.Permissions;
    using XAct.Diagnostics;
    using XAct.Initialization;
    using XAct.Security.Permissions;
    using XAct.Services.IoC.Initialization;

    // ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// Extensions to the AppDomain object
    /// </summary>
    /// <internal>8/15/2011: Sky</internal>
// ReSharper disable CheckNamespace
    public static partial class AppDomainExtensions
// ReSharper restore CheckNamespace
    {

        private static ITracingService TracingService
        {
            get { return DependencyResolver.Current.GetInstance<ITracingService>(); }
        }




        /// <summary>
        /// Gets the base dir in which to search for assemblies.
        /// <para>
        /// Determined from AppDomain.CurrentDomain.BaseDirectory
        /// </para>
        /// <para>
        /// Note that will be foo/bar/bin/debug in NUnit and Console apps.
        /// In an web app, this will be the base dir - not the bin\debug or bin\release
        /// directory.
        /// </para>
        /// <para>
        /// Returned without final slash in both cases.
        /// </para>
        /// </summary>
        /// <internal>
        /// Note that XAct.Services.IoC has a class that is also
        /// called <see cref="AppDomainExtensions"/>
        /// </internal>
        public static string BaseDir
        {
            get
            {
                if (_appDir.IsNullOrEmpty())
                {
                    _appDir = AppDomain.CurrentDomain.BaseDirectory;
                    if (_appDir.EndsWith("\\"))
                    {
                        _appDir = _appDir.Substring(0, _appDir.Length - 1);
                    }
                }
                return _appDir;
            }
        }

        private static string _appDir;




        /// <summary>
        /// Loads all local and referenced assemblies.
        /// <para>
        /// Invoked by <c>AppDomainExtensions.RegisterBindings</c>
        /// </para>
        /// </summary>
        /// <param name="appDomain">The application domain.</param>
        /// <param name="bindingScanResults">The binding scan results.</param>
        public static Assembly[] LoadAllLocalAndReferencedAssemblies(this AppDomain appDomain, ref BindingScanResults bindingScanResults)
        {


            using (TimedScope timedScope = new TimedScope())
            {

                FileIOPermission permission =
                    // ReSharper disable RedundantNameQualifier
                    new FileIOPermission(FileIOPermissionAccess.PathDiscovery |
                    // ReSharper restore RedundantNameQualifier
                                         FileIOPermissionAccess.Read,
                                         BaseDir);

                if (appDomain.IsPermitted(permission))
                {
                    bindingScanResults.HadPermissionsToScanDir = true;

                    //Loads all Assemblies that are:
                    //* available without being automatically loaded.
                    //* In BaseDir (ie AppDomain.CurrentDomain.BaseDirectory) 
                    //and then in Bin/Debug (ie AppDomain.CurrentDomain.SetupInformation.PrivateBinPath)
                    bindingScanResults.AssemblySearchPaths =
                        new[]
                            {
                                appDomain.LoadAllAssembliesInBaseDirectory(),
                                appDomain.LoadAllAssembliesInPrivateBinDirectory()
                            };
                }

                //Ensure all non-system assemblies are in memory, or else
                //they can't be scanned... controversial...I know...
                //Force load all assemblies:
                bindingScanResults.AssemblyNames =
                    appDomain.LoadAllReferencedAssemblies(true).Select(assembly => assembly.FullName).
                              ToArray();


                bindingScanResults.TimeToLoadFilesElapsed = timedScope.Elapsed;

            }

            Assembly[] results =
                appDomain.GetAssemblies();

            return results;
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Forces the Loading of all assemblies in domain.
        /// <para>
        /// WARNING: Unfortunately, it uses Directory.GetFiles
        /// which requires priveleges beyond what is available in a sandboxed environment.
        /// </para>
        /// </summary>
        /// <param name="appDomain">The app domain.</param>
        [DebuggerHidden]
        public static Assembly[] LoadAllAssembliesInDomain(this AppDomain appDomain)
        {
            //Get All loaded assemblies:
            List<Assembly> loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();

            //Get their paths:
            string[] loadedPaths = loadedAssemblies.Select(a => a.Location).ToArray();

            IEnumerable<string> toLoad;

            //IEnumerable<string> toLoadCheck = loadedPaths.Where(p => ((!p.ToLower().Contains("microsoft.net")) && (!p.ToLower().Contains("gac_msil")))).ToArray();

            try
            {
                //Try to find all paths in the app's directory:
                string[] referencedPaths = Directory.GetFiles(AppDomainExtensions.BaseDir, "*.dll");

                //Of those, remove those already loaded:
                toLoad =
                    referencedPaths.Where(r => !loadedPaths.Contains(r, StringComparer.InvariantCultureIgnoreCase)).
                        ToList();
            }
            //Suppossedly catch(Exception) does not catch unmamanaged.
            catch
            {

                //Probably don't have enough rights to load 
                toLoad =
                    loadedPaths.Where(
                        loadedPath =>
                        ((!loadedPath.ToLower().Contains("microsoft.net")) &&
                         (!loadedPath.ToLower().Contains("gac_msil")))).ToArray();
            }

            //G
            toLoad.ForEach(
                path => loadedAssemblies.Add(AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(path))));

            return loadedAssemblies.ToArray();
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Forces the Loading of all assemblies in domain.
        /// <para>
        /// Note that it does not use System.Directory, so should
        /// work in a Sandboxed environment.
        /// </para>
        /// </summary>
        /// <param name="appDomain">The app domain.</param>
        /// <param name="avoidLoadingSystemAssemblies">if set to <c>true</c> [remove system assemblies].</param>
        /// <returns></returns>
        [DebuggerHidden]
        public static Assembly[] LoadAllReferencedAssemblies(this AppDomain appDomain,
                                                                        bool avoidLoadingSystemAssemblies)
        {
            Dictionary<string, Assembly> results = new Dictionary<string, Assembly>();
            Stack<Assembly> stack = new Stack<Assembly>(); // stack of Assembly Info objects

            // store root assembly (level 0) directly into results list

            Assembly[] start = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly outerAssembly in start)
            {
                //Each known assembly is a start again:
                stack.Clear();
                stack.Push(outerAssembly);

                // do a preorder, non-recursive traversal
                while (stack.Count > 0)
                {

                    Assembly assembly = stack.Pop(); // get next assembly info


                    if (avoidLoadingSystemAssemblies)
                    {
                        string assemblyName = assembly.ToString();
                        if (assemblyName.Contains("b77a5c561934e089"))
                        {
                            //"SKIP".Dump();
                            continue;
                        }
                        if (assemblyName.Contains("b03f5f7f11d50a3a"))
                        {
                            //"SKIP".Dump();
                            continue;
                        }
                        if (assemblyName.Contains("31bf3856ad364e35"))
                        {
                            //"SKIP".Dump();
                            continue;
                        }
                        if (assemblyName.Contains("d127efab8a9c114f"))
                        {
                            //"SKIP".Dump();
                            continue;
                        }
                        if (assemblyName.Contains("System."))
                        {
                            //"SKIP".Dump();
                            continue;
                        }

                        if (results.ContainsKey(assemblyName.ToLower()))
                        {
                            continue;
                        }
                    }

                    results.Add(assembly.ToString().ToLower(), assembly);

                    AssemblyName[] subchild = assembly.GetReferencedAssemblies();

                    for (int i = subchild.Length - 1; i >= 0; --i)
                    {
                        AssemblyName assemblyName = subchild[i];
                        if (avoidLoadingSystemAssemblies)
                        {

                            if (assemblyName.ToString().Contains("b77a5c561934e089"))
                            {
                                //"SKIP".Dump();
                                continue;
                            }
                            if (assemblyName.ToString().Contains("b03f5f7f11d50a3a"))
                            {
                                //"SKIP".Dump();
                                continue;
                            }
                            if (assemblyName.ToString().Contains("31bf3856ad364e35"))
                            {
                                //"SKIP".Dump();
                                continue;
                            }
                            if (assemblyName.ToString().Contains("d127efab8a9c114f"))
                            {
                                //"SKIP".Dump();
                                continue;
                            }
                            if (assemblyName.ToString().Contains("System."))
                            {
                                //"SKIP".Dump();
                                continue;
                            }
                        }

                        try
                        {
                            try
                            {
                                Assembly childAssembly = Assembly.Load(assemblyName);
                                stack.Push(childAssembly);
                            }
                            catch (FileLoadException)
                            {

                            }
                            catch (System.BadImageFormatException)
                            {

                            }
                            catch (System.IO.FileNotFoundException)
                            {

                            }
                            //Suppossedly catch(Exception) does not catch unmamanaged.
                            catch
                            {
                            }
                        }
                        catch
                        {
                        }
                    }

                }

                //break;
            }
            return results.Values.ToArray();
        }

//        /// <summary>
//        /// Loads all assemblies in application's base directory.
//        /// <para>
//        /// Warning - requires File Read permissions (making your
//        /// code less portable in the long run), so prefer instead
//        /// <see cref="LoadAllReferencedAssemblies"/> if is enough to get
//        /// the job done.
//        /// </para>
//        /// </summary>
//        /// <param name="appDomain">The app domain.</param>
//        /// <param name="discoveryPath">The discovery path.</param>
//        public static string LoadAllAssembliesInBaseAndGivenPrivateBinDirectory(this AppDomain appDomain, string discoveryPath = null)
//        {
//            //note: baseDirectory is same as AppDir property (but with slash)
//#pragma warning disable 168
//            string baseDirectory = appDomain.LoadAllAssembliesInBaseDirectory();
//#pragma warning restore 168

//            return appDomain.LoadAllAssembliesInPrivateBinDirectory(discoveryPath);
//        }

        /// <summary>
        /// Loads all assemblies in base directory.
        /// </summary>
        /// <param name="appDomain">The application domain.</param>
        /// <returns></returns>
        public static string LoadAllAssembliesInBaseDirectory(this AppDomain appDomain)
        {
            //note: baseDirectory is same as AppDir property (but with slash)
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            LoadAssembliesInDirectory(baseDirectory);

            return baseDirectory;
        }

        /// <summary>
        /// Loads all assemblies in privte bin directory (ie AppDomain.CurrentDomain.SetupInformation.PrivateBinPath)
        /// </summary>
        /// <param name="appDomain">The application domain.</param>
        /// <param name="discoveryPath">The discovery path.</param>
        /// <returns></returns>
        public static string LoadAllAssembliesInPrivateBinDirectory(this AppDomain appDomain, string discoveryPath = null)
        {
            //note: baseDirectory is same as AppDir property (but with slash)
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

            //Why are we doing this?
            //LoadAssembliesInDirectory(baseDirectory);


            //But in a web app, that's the base dir -- not the bin/Debug or bin/Release dir
            //which is retrieved from the privateBinPath
            string privateBinPath = discoveryPath ??
                                    Library.Settings.Bindings.AssemblySearchPath ??
                                    AppDomain.CurrentDomain.SetupInformation.PrivateBinPath;

            if (!privateBinPath.IsNullOrEmpty())
            {
                //TODO: May need to handle quotes (ie SplitEx)
                foreach (string path in privateBinPath.Split(';'))
                {
                    if (Directory.Exists(privateBinPath) && (path != baseDirectory) )
                    {
                        LoadAssembliesInDirectory(privateBinPath);
                    }
                }
            }
            return privateBinPath;
        }


        private static void LoadAssembliesInDirectory(string path)
        {
            //The risk is if it is finding base path of a webapp:
            //eg: D:\\CODE\\NSI10\\Dev\\WA\\MOE.NSI.WA.Core.Back.AppHost.Web
            //and picking up bin/debug AND bin/release
            //so we can't just use 'SearchOption.AllDirectories'

            IEnumerable<string> assemblyFiles = Directory.GetFiles(path, "*.dll");

            //IEnumerable<string> assemblyFiles = Directory.GetFiles(path)
            //    .Where(file => file.ToLower().EndsWith(".dll"));

            foreach (string assemblyFile in assemblyFiles)
            {
                // TODO: check it isnt already loaded in the app domain
                try
                {
                    Assembly.LoadFrom(assemblyFile);
                }
                catch (System.BadImageFormatException)
                {

                }
                //catch (System.IO.FileNotFoundException)
                //{

                //}
                //Suppossedly catch(Exception) does not catch unmamanaged.
                catch
                {
                    
                }
            }
        }



        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets all the the types in all loaded assemblies, 
        /// that are decorated with attributes.
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// Type[] results = appDomain.GetTypesDecoratedWithAttributes(
        ///   typeof(DefaultServiceImplementationAttribute),
        ///   false);
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="appDomain">The app domain.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <returns></returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public static Type[] GetTypesDecoratedWithAttribute(this AppDomain appDomain, Type attributeType,
                                                             bool inherit = true)
        {

            Type[] results =
                appDomain.GetAssemblies().GetTypesDecoratedWithAttribute(attributeType, inherit);

            return results;
        }



        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        ///   </para>
        /// Gets the types decorated with attribute.
        ///   <para>
        /// Usage Example:
        ///   <code>
        ///   <![CDATA[
        /// Type[] results = appDomain.GetTypesDecoratedWithAttributes
        ///   <DefaultServiceImplementationAttribute>(null, false,true);
        /// ]]>
        ///   </code>
        ///   </para>
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <param name="appDomain">The app domain.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="inherit">if set to <c>true</c> [inherit].</param>
        /// <param name="allowMultiples">if set to <c>true</c> [allow multiples].</param>
        /// <returns></returns>
        public static KeyValuePair<Type, TAttribute>[] GetTypesDecoratedWithAttribute<TAttribute>(
            this AppDomain appDomain,  Func<TAttribute,bool> filter=null,   bool inherit = false, bool allowMultiples = false)
            where TAttribute : Attribute
        {

            KeyValuePair<Type, TAttribute>[] results =
                appDomain.GetAssemblies().GetTypesDecoratedWithAttribute(filter,inherit,allowMultiples);

            return results;
        }

        /// <summary>
        /// Find all class definitions in the library and current application
        /// that are decorated with the given Attribute, that match the given
        /// Interface, instantiate them, and invoke the given Action on them.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <typeparam name="TClass">The type of the class contract.</typeparam>
        /// <param name="appDomain">The app domain.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static KeyValuePair<Type, TAttribute>[] GetTypesDecoratedWithAttribute<TAttribute, TClass>(
            this AppDomain appDomain,
            Func<TAttribute, bool> filter
            )
            where TAttribute : Attribute
            where TClass : class
        {

            KeyValuePair<Type, TAttribute>[] results =
                appDomain.GetAssemblies().GetTypesDecoratedWithAttribute<TAttribute, TClass>(filter);

            return results;
        }

        /// <summary>
        /// Gets the type of all types implementing open generic.
        /// </summary>
        /// <typeparam name="TOpenGenericType">The type of the open generic type.</typeparam>
        /// <param name="assemblies">The assemblies.</param>
        /// <returns></returns>
        public static Type[] GetAllTypesImplementingOpenGenericType<TOpenGenericType>(this Assembly[] assemblies)
        {
            return assemblies.GetAllTypesImplementingOpenGenericType(typeof (TOpenGenericType));
        }

        /// <summary>
        /// Gets all types implementing the given open generic contract.
        /// </summary>
        /// <typeparam name="TOpenGenericType">The type of the open generic type.</typeparam>
        /// <param name="appDomain">The application domain.</param>
        /// <returns></returns>
        public static Type[] GetAllTypesImplementingOpenGenericType<TOpenGenericType>(this AppDomain appDomain)
        {
            return appDomain.GetAssemblies().GetAllTypesImplementingOpenGenericType(typeof(TOpenGenericType));
        }

        /// <summary>
        /// Gets the type of the types inmplementing.
        /// </summary>
        /// <typeparam name="IDbModelBuilder">The type of the database model builder.</typeparam>
        /// <param name="appDomain">The application domain.</param>
        /// <param name="instantiableOnly">if set to <c>true</c> [instantiable only].</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static Type[] GetTypesImplementingType<IDbModelBuilder>(this AppDomain appDomain, bool instantiableOnly = true, Func<Type, bool> filter = null)
        {
            var results = appDomain.GetAssemblies().GetTypesImplementingType<IDbModelBuilder>(instantiableOnly, filter);
            return results;
        }

        /// <summary>
        /// Gets the type of the types inmplementing.
        /// </summary>
        /// <param name="appDomain">The application domain.</param>
        /// <param name="modelBuilderType">Type of the model builder.</param>
        /// <param name="instantiableOnly">if set to <c>true</c> [instantiable only].</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static Type[] GetTypesImplementingType(this AppDomain appDomain, Type modelBuilderType, bool instantiableOnly = true, Func<Type, bool> filter = null)
        {
            var results = appDomain.GetAssemblies().GetTypesImplementingType(modelBuilderType, instantiableOnly, filter);
            return results;
        }
















        /// <summary>
        /// Find all class definitions in the library and current application
        /// that are decorated with the given Attribute, that match the given
        /// Interface, instantiate them, and invoke the given Action on them.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attribute.</typeparam>
        /// <typeparam name="TClassContract">The type of the class contract.</typeparam>
        /// <param name="appDomain">The app domain.</param>
        /// <param name="action">The action.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="optionalInvokeCallback">The optional invoke callback.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static KeyValuePair<Type, TAttribute>[]
            GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethod<TAttribute, TClassContract>(
            this AppDomain appDomain,
            Action<TClassContract> action,
            Func<TAttribute, bool> filter,
            Action<bool, Type, TAttribute, TClassContract> optionalInvokeCallback = null
            )
            where TAttribute : Attribute
            where TClassContract : class
        {

            KeyValuePair<Type, TAttribute>[] results =
                appDomain.GetAssemblies()
                         .GetTypesDecoratedWithAttributeAndInstantiateAndInvokeInterfaceMethod
                    <TAttribute, TClassContract>(action, filter, optionalInvokeCallback);

            return results;
        }







        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        /// An extension method to check whether the user
        /// has the required permission.
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// var requestedPermission = new FileIOPermission(FileIOPermissionAccess.Write,folder);
        /// 
        /// if (AppDomain.IsPermitted(requestedPermission))
        /// {
        /// //go ahead...
        /// }else{
        ///   //...hum...
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="appDomain"></param>
        /// <param name="requestedPermission"></param>
        public static bool IsPermitted(this AppDomain appDomain, IPermission requestedPermission)
        {
            //var permission = new FileIOPermission(FileIOPermissionAccess.Write, folder);

            //Application.CurrentDomain came in .NET40 and makes SecurityManager.IsGranted obsolete:

            //Use our ExtensionMethod:
            var result = AppDomain.CurrentDomain.PermissionSet.IsPermitted(requestedPermission);
            return result;
        }




        /// <summary>
        /// Sets the thread to current windows thread.
        /// <para>
        /// See: http://support.microsoft.com/kb/301256
        /// </para>
        /// </summary>
        /// <param name="appDomain">The app domain.</param>
        public static void SetThreadToCurrentWindowsThread(this AppDomain appDomain)
        {
            //AppDomain.CurrentDomain
            appDomain.SetPrincipalPolicy(System.Security.Principal.PrincipalPolicy.WindowsPrincipal);
        }


        /// <summary>
        /// Gets the Type from just the Type's fullname (which may be FQN or partial).
        /// </summary>
        /// <param name="appDomain">The application domain.</param>
        /// <param name="fullTypeName">Full name of the type.</param>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <returns></returns>
        public static Type GetTypeFromTypeFullName(this AppDomain appDomain, string fullTypeName, string assemblyName=null)
        {
            Dictionary<string, Type> cached = new Dictionary<string, Type>();
            Type result;

            if (cached.TryGetValue(fullTypeName, out result))
            {
                return result;
            }

            if (assemblyName != null)
            {
                Assembly assembly;
                try
                {
                    assembly = appDomain.Load(assemblyName);
                    result = assembly.GetType(fullTypeName);
                }
                catch
                {
                    int pos = assemblyName.IndexOf(",");
                    if (pos > -1)
                    {
                        assemblyName = assemblyName.Substring(0, pos).Trim();
                    }
                    result = appDomain.GetAssemblies().Where(a => a.FullName == assemblyName)
                                      .Select(a => a.GetType(fullTypeName))
                                      .FirstOrDefault(t => t != null);
                }
                //May be null:
            }
            else
            {

                result = Type.GetType(fullTypeName) ??
                         appDomain.GetAssemblies()
                                  .Select(a => a.GetType(fullTypeName))
                                  .FirstOrDefault(t => t != null);
            }

            cached[fullTypeName] = result;

            return result;

        }


        /// <summary>
        /// Gets the full name of the type from type.
        /// </summary>
        /// <param name="appDomain">The application domain.</param>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public static Type GetTypeFromTypeFullName(this AppDomain appDomain, IHasAssemblyAndTypeNames entity)
        {
            return appDomain.GetTypeFromTypeFullName(entity.TypeFullName, entity.AssemblyName);
        }

    }

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
