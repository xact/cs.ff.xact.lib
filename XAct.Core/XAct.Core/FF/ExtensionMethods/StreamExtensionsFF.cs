﻿namespace XAct
{
    using System.IO;
    using System.Xml;
    using System.Xml.Xsl;

    /// <summary>
    /// 
    /// </summary>
    public static class StreamExtensionsFF
    {

        /// <summary>
        ///   <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets a <see cref="XslCompiledTransform"/>
        /// for the given stream (of an Xsl file).
        /// </summary>
        /// <param name="xslStream">The XSL stream.</param>
        /// <param name="enabledScript">if set to <c>true</c> [enabled script].</param>
        /// <returns></returns>
        /// <value>The compiled.</value>
        public static XslCompiledTransform CreateXslCompiledTransform(this Stream xslStream, bool enabledScript = true)
        {
            using (XmlReader xmlReader = XmlReader.Create(xslStream))
            {
                XslCompiledTransform result = new XslCompiledTransform();

                XsltSettings xmlSettings = new XsltSettings();
                xmlSettings.EnableScript = enabledScript;

                result.Load(xmlReader, xmlSettings, null);
                return result;
            }

        }

            ///// <summary>
            ///// Determines whether stream 
            ///// (consider whether you need to set Position)
            ///// contains valid Xml.
            ///// </summary>
            ///// <param name="stream">The stream.</param>
            ///// <returns></returns>
            //public static bool ContainsXmlElements(Stream stream)
            //{
            //    try
            //    {
            //        using (XmlTextReader xmlTextReader = new XmlTextReader(stream))
            //        {
            //            xmlTextReader.MoveToContent();
            //            if (xmlTextReader.NodeType == XmlNodeType.Element)
            //            {
            //                return true;
            //            }
            //        }
            //    }
            //    finally
            //    {
            //    }
            //    return false;
            //}
 


    }
}