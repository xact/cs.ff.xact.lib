﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Domain;
    using XAct.Extensions;
    using XAct.Settings;

    /// <summary>
    /// Extensions to the <see cref="XAct.Settings"/> (Profile) object).
    /// </summary>
    public static class SettingsExtensions
    {
//        /// <summary>
//        /// Adds new <see cref="Setting"/> objects
//        /// based on the values in the given Dictionary.
//        /// </summary>
//        /// <param name="settings">The settings.</param>
//        /// <param name="dict">The dictionary.</param>
//        /// <param name="settingSourceProviderTag">The setting source provider tag.</param>
//        public static void SetValues(this Settings.Settings settings, Dictionary<string, string> dict, string settingSourceProviderTag=null)
//        {
//            //Iterate through all appsettings in web.config:
//            foreach (KeyValuePair<string, string> kvp in dict)
//            {
//                //The format of the app settings keys is:
//                /*
//                   <add key="Test" value="ok"/>
//                    <add key="Sub1/Sub2/Sub3" value="ok"/>
//                    <add key="module2:SubA/SubB/SubC" value="ok"/>
//                    <add key="[System.Boolean]module3:SubA/SubB/SubC" value="True"/>
//                    <add key="[System.Int32]module3:SubA/SubB/SubD" value="-123"/>
//                */
//                //ABC:EFG
//                //0123456
//                string moduleName;
//                string variablePath;
//                Type type;

//                //Use Helper method to parse key:
//                settings.ExtractModuleAndPath(kvp.Key, out moduleName, out variablePath, out type);

//                //Get appsetting
//                string value = kvp.Value;
//                object typedValue = value.ConvertTo(type);

//                // Note:
//                // AppSettings are always loaded first -- as without local *.config file
//                // not going to get to remote Db (and therefore Db Settings):
//                // So there is no need to think about removing previous values
//                // (later, EFAppSettings will have to consider this).

//                if (moduleName.IsNullOrEmpty())
//                {
//                    // ReSharper disable RedundantArgumentDefaultValue
//                    settings.AddSetting(variablePath, type, typedValue, null, null, null, null,settingSourceProviderTag, null,
//                                        false);
//                    // ReSharper restore RedundantArgumentDefaultValue
//                }
//                else
//                {
//                    // ReSharper disable RedundantArgumentDefaultValue
//                    settings.GetModule(moduleName)
//                            .AddSetting(variablePath, type, typedValue, null, null, null, null,settingSourceProviderTag, null,
//                                        false);
//                    // ReSharper restore RedundantArgumentDefaultValue
//                }
//            }
//        }

//        /// <summary>
//        /// Gets the values as a dictionary of strings.
//        /// <para>
//        /// Used byy IAppHostSettingsService to persist values.
//        /// </para>
//        /// </summary>
//        /// <param name="settings">The settings.</param>
//        /// <param name="moduleSettingGroup">The module setting group.</param>
//        /// <param name="tag">The tag.</param>
//        /// <returns></returns>
//        /// <exception cref="System.ArgumentOutOfRangeException">Could not find {0} in appSettings under any expected key format..FormatStringInvariantCulture(settingName)</exception>
//        public static Dictionary<string,string> GetValuesAsDictionaryOfStrings(this XAct.Settings.Settings settings, ModuleSettingGroup moduleSettingGroup , string tag)
//        {
//            Dictionary<string,string> results = new Dictionary<string, string>();

//            foreach (Setting setting in ((IProfileSettingCollectionAccessor)moduleSettingGroup).SettingsCollection)
//            {
//                if (setting.Src != tag)
//                {
//                    //Persist only the Settings marked with "APP" or whatever this setting is:
//                    continue;
//                }

//                if (setting.ModelState == OfflineModelState.Unchanged)
//                {
//                    //If it hasn't changed, don't bother saving it:
//                    continue;
//                }

//                if (!setting.IsReadable)
//                {
//                    //If it's not readable, don't see how I'm going to get the new value:
//                    continue;
//                }

//                ////It would be good if we could Type it more precisely,
//                ////but it's not needed, as all typed AppSettings are just
//                ////subclassed wrappers to the underlying internals of the core Settings object:
//                //XAct.Settings.Settings settings = this.Retrieve<XAct.Settings.Settings>();
                
//                //setting name will be of format 'mykey' or 'mygroup/mykey'.
//                //name will not include modulename
//                string settingName = setting.Name;

//                //note that it doesn't have module name on it, so if we are in a module
//                //prepend it to look like 'mymodule:mykey' or 'mymodule:mygroup/mykey'
//// ReSharper disable RedundantThisQualifier
//                if (moduleSettingGroup != settings)
//// ReSharper restore RedundantThisQualifier
//                {
//                    settingName = moduleSettingGroup.Name + XAct.Settings.Settings.ModuleSeparatorCharacter + settingName;
//                }

//                string typeFullName = setting.ValueType.FullName;
//                string keyNameWithType = "[{0}]{1}".FormatStringInvariantCulture(typeFullName, settingName);
//                string keyNameWithTypeAlt = "[{0}]{1}".FormatStringInvariantCulture(setting.ValueType.Name, settingName);

//                //We hope to serialize it as String, but may run into issues.
//                SerializationMethod serializationMethod = SerializationMethod.String;
//                string str = setting.ValueType.Serialize(setting.Value, ref serializationMethod);
//                //...note that it may now be marked as SerializionMethod.Xml or binary...whatever worked first.

//                //Look for it in the existing collection...should be there, under one of the formats:
//                if (results.ContainsKey(keyNameWithType))
//                {
//                    results[keyNameWithType] = str;
                    
//                }
//                else if (results.ContainsKey(keyNameWithTypeAlt))
//                {
//                    results[keyNameWithTypeAlt] = str;
//                }
//                else if (results.ContainsKey(settingName))
//                {
//                    results[settingName] = str;
//                }
//                else
//                {
//                    throw new ArgumentOutOfRangeException("Could not find {0} in appSettings under any expected key format.".FormatStringInvariantCulture(settingName));
//                }
//            }

//            return results;
//        }



        /// <summary>
        /// Enumerates through an array of <see cref="SerializedApplicationSetting" />s
        /// in order to load up the given <see cref="XAct.Settings" /> object
        /// with <see cref="SerializedApplicationSetting" />s.
        /// <para>
        /// Used by an implementation of
        /// IRepositorySettingService.
        /// </para>
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="serializedSettings">The serialized settings.</param>
        /// <param name="serializedSettingsAreOrdered">if set to <c>true</c> [serialized settings are ordered].</param>
        /// <param name="includeEditingInformation">if set to <c>true</c> [include editing information].</param>
        /// <param name="isUnlockedCallback">The is unlocked callback.</param>
        /// <param name="isReadableCallback">The is readable callback.</param>
        /// <param name="isWritableCallback">The is writable callback.</param>
        /// <param name="offlineModelState">State of the offline model.</param>
        public static void Load(this Settings.Settings settings,
                                SerializedApplicationSetting[] serializedSettings,
                                bool serializedSettingsAreOrdered,
                                bool includeEditingInformation,
                                Func<string, bool> isUnlockedCallback = null,
                                Func<string, bool> isReadableCallback = null,
                                Func<string, bool> isWritableCallback = null,
                                OfflineModelState offlineModelState = OfflineModelState.UnchangedExisting
                

            )
        {


            //Loop through each serialized setting in order:
            SerializedApplicationSetting[] orderedSerializedSettings = (serializedSettingsAreOrdered)
                                                                ? serializedSettings
                                                                : serializedSettings.OrderBy(r => r.Key)
                                                                                    .ThenByDescending(r => r.ZoneOrTierIdentifier)
                                                                                    .ThenByDescending(r => r.HostIdentifier)
                                                                                    .ToArray();

            //Now that SerializedSettings are in order, 
            //(especially the fact that they all variables in the same Module
            //are grouped together AND that the as the variable is from the most
            //specific to the least specific (ie for the same Key, any Host specific
            //entry is before any generic-for-the-tier entry, and so on)
            //one can safely go through each one:
            foreach (SerializedApplicationSetting serializedApplicationSetting in orderedSerializedSettings)
            {
                string name = serializedApplicationSetting.Key;
                string moduleName;
                string variablePath;
                Type type;

                //From the serialized setting's name, extract the Module name, variable name, and Type:
                settings.ExtractModuleAndPath(name, out moduleName, out variablePath, out type);



                //using IHasSerializedTypeValueAndMethodExtensions extension method
                //to convert the serializedValue, valueType into a correctly typed object:
                object value = serializedApplicationSetting.DeserializeValue();

                //If we are Host -- what happens if the underlying Setting was already set
                //by an ISettingsService? Seems one would need to remove it, and reset it
                //in order to mark the Src Tag that can only be set by a Setting constructor.

                SettingEditingMetadata settingEditingInformation;

                if (includeEditingInformation)
                {
                    settingEditingInformation
                        =
                        //In the case of User settings, all the following will be ...null.
                        //So doesn't take up much memory space.
                        new SettingEditingMetadata(
                            type.GetDefault(), /*defaultValue*/
                            () => isUnlockedCallback(serializedApplicationSetting.IsUnlockedInformation) /*isEditable*/,
                            () => isReadableCallback(serializedApplicationSetting.IsReadableAuthorisationInformation)
                            /*hasReadAccess*/,
                            () => isWritableCallback(serializedApplicationSetting.IsWritableAuthorisationInformation)
                            /*hasWriteAccess*/
                            );
                }
                else
                {
                    settingEditingInformation = null;
                }

                //Are we talking about the base Setting (whihc is a moduleSettingGroup, or a ModuleSettingGroup within it?
                ModuleSettingGroup moduleSettingGroup;
                if (moduleName.IsNullOrEmpty())
                {
                    moduleSettingGroup = settings;
                }
                else
                {
                    moduleSettingGroup = settings.GetModule(moduleName);
                }

                //Add the setting -- unless setting was already set:
                if (moduleSettingGroup.ContainsSetting(variablePath))
                {
                    continue;
                }
                //// ReSharper disable RedundantArgumentDefaultValue
                //moduleSettingGroup.AddSetting(
                //    variablePath,
                //    type,
                //    value,
                //    settingEditingInformation,
                //    serializedApplicationSetting.Tag,
                //    serializedApplicationSetting.Description,
                //    serializedApplicationSetting.Metadata,
                //    true,
                //    offlineModelState //Mark that it's coming from a Db (default is OfflineModelState.UnchangedExisting)
                //    );
                //// ReSharper restore RedundantArgumentDefaultValue

                var setting = new Setting(serializedApplicationSetting.Id,
                                        variablePath,
                                          type,
                                          value,
                                          serializedApplicationSetting.Tag,
                                          serializedApplicationSetting.Description,
                                          serializedApplicationSetting.Metadata,
                                          settingEditingInformation,
                                          offlineModelState
                    //Mark that it's coming from a Db (default is OfflineModelState.UnchangedExisting)
                    );

                moduleSettingGroup.AddSetting(setting,true);

            } //~loop
        }





        //private static void GetModuleSerializedSettings(this ModuleSettingGroup moduleSettingsGroup, ref List<Setting> results)
        //{
        //    //Iterate through each Module, getting the variables from the Modules' Collection:

        //    foreach (Setting setting in 
        //        )
        //    {
        //        SettingEditingMetadata settingEditingInformation =
        //            setting.EditingMetadata;


        //        if (settingEditingInformation == null)
        //        {
        //            //can't save something that has no Editing information 
        //            //that would express where to save it to.
        //            continue;
        //        }

        //        SerializedSetting serializedSetting = settingEditingInformation.SourceSerializedSetting;

        //        if (serializedSetting == null)
        //        {
        //            if (templateSerializedSettingForNewSettings == null)
        //            {
        //                throw new Exception("Cannot have added Settings dynamically, if no TemplateSettings is defined.");
        //            }

        //            //Setting was added on the fly.
        //            //THerefore has no db record to match it:
        //            Debug.Assert(setting.ModelState == OfflineModelState.New);

        //            serializedSetting = new SerializedSetting();
        //            //Attach:
        //            //settingEditingInformation.SourceSerializedSetting = templateSerializedSettingForNewSettings;

                    
        //            //The key entries:
        //            serializedSetting.ApplicationName = templateSerializedSettingForNewSettings.ApplicationName;
        //            serializedSetting.ZoneOrTier = templateSerializedSettingForNewSettings.ZoneOrTier;
        //            serializedSetting.Host = templateSerializedSettingForNewSettings.Host;
        //            serializedSetting.User = templateSerializedSettingForNewSettings.User;
        //            serializedSetting.Key = setting.Name;
        //        }

        //        //Serialize the Value (based on ValueType & preferred Serialization technique):
        //        serializedSetting.SerializeValue(setting.Value, setting.ValueType, SerializationMethod.Undefined);

        //        //But what's the state?

        //        serializedSetting.SetState(setting.ModelState);

        //        results.Add(serializedSetting);
        //    }
        //}


        /// <summary>
        /// Gets all <see cref="Setting"/> objects in <see cref="Settings"/> object.
        /// <para>
        /// INTERNAL: For now, don't want to make this an extension method and widely known,
        /// but don't know where to park it. Shitty. I know...
        /// </para>
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public static List<Setting> GetAllSettings(this XAct.Settings.Settings settings)
        {
            List<Setting> results = new List<Setting>();

            foreach (ModuleSettingGroup settingModule in settings.Modules)
            {
                results.AddRange(((IProfileSettingCollectionAccessor)settingModule).Settings);
            }
            //Add Settings of this settings object:
            results.AddRange(((IProfileSettingCollectionAccessor)settings).Settings);

            return results;
        }


        /// <summary>
        /// Clears all settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public static void ClearAllSettings(this XAct.Settings.Settings settings)
        {
            foreach (ModuleSettingGroup settingModule in settings.Modules)
            {
               ((IProfileSettingCollectionAccessor)settingModule).Settings.Clear();
            }
            //Add Settings of this settings object:
            ((IProfileSettingCollectionAccessor)settings).Settings.Clear();

        }


        /// <summary>
        /// Clones the settings.
        /// </summary>
        /// <typeparam name="TSettings"></typeparam>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        public static TSettings CloneSettings<TSettings>(this TSettings settings)
            where TSettings : XAct.Settings.Settings
        {
            //Serialize the whole thing, and deserialize again
            //as an secondary entity.
            //Note that at this point, Func<bool> properties will be invoked, and embedded as ()=>true.
            return settings.TestWCFSerialization();
        }

    }
}
