﻿namespace XAct.Extensions
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Extension methods to allow reflection on all <see cref="object"/>s.
    /// </summary>
    public static class ObjectReflectionExtensions2FF
    {
        
        /// <summary>
        /// Clones the uncloneable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static T CloneTheUncloneable<T>(this T source)
        {
            if (source is ISerializable)
            {
                using (Stream objectStream = new MemoryStream())
                {
                    IFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(objectStream, source);
                    objectStream.Seek(0, SeekOrigin.Begin);
                    return (T) formatter.Deserialize(objectStream);
                }
            }

            Type sourceType = typeof (T);
            FieldInfo[] fis = sourceType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            //Not 
            T result = (T)Activator.CreateInstance(sourceType);
            
            foreach (FieldInfo fi in fis)
            {
                if (fi.FieldType.Namespace != sourceType.Namespace)
                {
                    fi.SetValue(result, fi.GetValue(source));
                }
                else
                {
                    object obj = fi.GetValue(source);
                    fi.SetValue(result, obj.CloneTheUncloneable());
                }
            }
            return (T)result; 

        }
    }
}