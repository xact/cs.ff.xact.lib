﻿
#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.IO;
    using System.IO.IsolatedStorage;
    using System.Xml;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    /// 
    /// </summary>
    /// <internal>
    /// <internal>
    ///   READ THIS ABOUT SAVING SETTINGS:
    ///   http://blogs.msdn.com/rprabhu/articles/433979.aspx
    /// </internal>
    /// <internal>
    ///   Introduction to IsolatedStorage:
    ///   http://winfx.msdn.microsoft.com/library/default.asp?url=/library/en-us/dv_fxfund/html/aff939d7-9e49-46f2-a8cd-938d3020e94e.asp
    ///   Creating Files and Directories in IsolatedStorage:
    ///   http://winfx.msdn.microsoft.com/library/default.asp?url=/library/en-us/dv_fxfund/html/9d08328b-9b7b-4753-b20c-e556c312cf91.asp
    /// </internal>
    /// </internal>
    public static class IsolatedStorageExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Gets the User in Application Scoped <see cref = "IsolatedStorageFile" />.
        /// </summary>
        /// <returns></returns>
        public static IsolatedStorageFile GetApplicationIsolatedStorageFile()
        {
            // //http://stackoverflow.com/questions/7294461/unable-to-determine-application-identity-of-the-caller
            // //GOOD Read: http://blogs.msdn.com/b/shawnfa/archive/2006/01/20/514411.aspx
            //Then again...maybe not best, as 
            //"Requires full trust for the immediate caller. This member cannot be used by partially trusted or transparent code."

            //But this needs System.Deployment
            //if (System.Deployment.Application.ApplicationDeployment.CurrentDeployment.IsNetworkDeployed)

            if (AppDomain.CurrentDomain.ActivationContext != null)
            {
                //We are running within ClickOnce
                //Note that this will error if not within a ClickOnce:
                return IsolatedStorageFile.GetUserStoreForApplication();
                //So will this:
                //return IsolatedStorageFile.GetMachineStoreForApplication();
            }
            else
            {
                //Only Domain ones will work outside of ClickOnce:
                //This should only be happening in a VS environment 
                //-- or NUnit tests within CI environment.
                return IsolatedStorageFile.GetUserStoreForDomain();
                //return IsolatedStorageFile.GetMachineStoreForDomain();
            }


        //    if (AppDomain.CurrentDomain.ActivationContext != null)
        //    {
        //        //We are running within ClickOnce
        //        return IsolatedStorageFile.GetUserStoreForApplication();
        //    }
        //    try
        //    {
        //        //Fails in UnitTests:
        //        //Fails in UnitTests:

                
        //    }
        //    catch
        //    {

        //        try
        //        {
        //            return IsolatedStorageFile.GetMachineStoreForApplication();
        //        }
        //        catch
        //        {
        //            //This should only be happening in a VS environment -- or NUnit tests within CI environment.
        //            return IsolatedStorageFile.GetUserStoreForDomain();
        //        }
        //    }

        //    //return IsolatedStorageFile.GetUserStoreForDomain();
        //    //IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Verifies that a file that matches the specified search pattern exists. 
        ///   <para>
        ///     Returns the first filename that matches the given pattern.
        ///   </para>
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     returned fileName will be <c>null</c> if not found.
        ///   </para>
        /// </remarks>
        /// <param name = "isolatedStorageArea">The <see cref = "IsolatedStorageFile" /> store.</param>
        /// <param name = "searchPattern">The search pattern.</param>
        /// <param name = "foundFileName">The first found FileName that matches the search pattern.</param>
        /// <returns>True if a file name is found to match the pattern.</returns>
        public static bool FileExists(this IsolatedStorageFile isolatedStorageArea, string searchPattern,
                                      out string foundFileName)
        {
            //Check Vars:
            if (isolatedStorageArea.IsNull())
            {
                throw new ArgumentNullException("isolatedStorageArea");
            }

            if (searchPattern.IsNullOrEmpty())
            {
                throw new ArgumentNullException("searchPattern");
            }

            //Get files that match the given pattern:
            string[] matchingFileNames = isolatedStorageArea.GetFileNames(searchPattern);

            foreach (string testFileName in matchingFileNames)
            {
                if (string.Compare(testFileName, searchPattern, true) == 0)
                {
                    foundFileName = testFileName;
                    return true;
                }
            }
            foundFileName = null;
            //Never found:
            return false;
        }


        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   (OverLoad) Loads an <see cref = "XmlDocument" /> from the specified <see cref = "IsolatedStorageFile" />.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     The resulting <c>xmlDocument</c> will be <c>null</c> if no file found.
        ///   </para>
        /// </remarks>
        /// <param name = "relativeFileNamePath">The relative file name path.</param>
        /// <param name = "isolatedStorageFile">The <see cref = "IsolatedStorageFile" /> within which to look for the <see cref = "XmlDocument" />.</param>
        /// <returns>Returns <c>true</c> if the document was found and parsed correctly.</returns>
        public static IsolatedStorageFileStream OpenReadStream(this IsolatedStorageFile isolatedStorageFile,
                                                               string relativeFileNamePath)
        {
            //Check vars:
            if (isolatedStorageFile.IsNull())
            {
                throw new ArgumentNullException("isolatedStorageFile");
            }
            if (relativeFileNamePath.IsNullOrEmpty())
            {
                throw new ArgumentNullException("relativeFileNamePath");
            }
            if (IsPathAbsolute(relativeFileNamePath))
            {
                throw new ArgumentException("paths in IsolatedStorage cannot be Absolute.");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            // Open or create a readable file.


            IsolatedStorageFileStream isolatedStorageFileStream =
                new IsolatedStorageFileStream(relativeFileNamePath,
                                              FileMode.Open,
                                              FileAccess.Read,
                                              isolatedStorageFile);

            return isolatedStorageFileStream;

        }


    /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Opens the write stream.
        /// </summary>
        /// <param name="isolatedStorageFile">The isolated storage file.</param>
        /// <param name="relativeFileNamePath">The relative file name path.</param>
        /// <param name="append">if set to <c>true</c> [append].</param>
        /// <returns></returns>
        /// <internal>8/16/2011: Sky</internal>
        public static IsolatedStorageFileStream OpenWriteStream(this IsolatedStorageFile isolatedStorageFile,
                                                                string relativeFileNamePath, bool append=false)
        {
            //Check vars:
            if (isolatedStorageFile.IsNull())
            {
                throw new ArgumentNullException("isolatedStorageFile");
            }
            if (relativeFileNamePath.IsNullOrEmpty())
            {
                throw new ArgumentNullException("relativeFileNamePath");
            }
            if (IsPathAbsolute(relativeFileNamePath))
            {
                throw new ArgumentException("paths in IsolatedStorage cannot be Absolute.");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            // Open or create a readable file.
            FileMode fileMode = (append) ? FileMode.Append : FileMode.OpenOrCreate;

            IsolatedStorageFileStream isolatedStorageFileStream =
                new IsolatedStorageFileStream(relativeFileNamePath, fileMode, FileAccess.Write,
                                              isolatedStorageFile);

            return isolatedStorageFileStream;

        }




    private static bool IsPathAbsolute(string path)
        {
            if (path == string.Empty)
            {
                return false;
            }

            //If not long enough to ever be Physical path, return false:
            if (path.Length < 3)
            {
                return false;
            }

            //if Path is either format "D:\" or "\\" or "d:/" or "//":
            if ((path[1] == ':') && IsDirectorySeparatorChar(path[2], true))
            {
                return true;
            }

            //If Path starts with '\\' it's a network absolute
            if (IsDirectorySeparatorChar(path[0], false) && IsDirectorySeparatorChar(path[1], false))
            {
                return true;
            }
            return false;
        }


        /// <summary>
        ///   Returns true if the given char is '\', and/or '/' character.
        /// </summary>
        /// <param name = "sepChar">The char to test.</param>
        /// <param name = "eitherDirection">Flag to test for forward slash as well.</param>
        /// <returns>True if char is '\\' and if eitherDirection flag is set, '/'.</returns>
        private static bool IsDirectorySeparatorChar(char sepChar, bool eitherDirection)
        {
            if (sepChar == '\\')
            {
                return true;
            }
            return (eitherDirection) && (sepChar == '/');
        }

    
    
    }





#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
