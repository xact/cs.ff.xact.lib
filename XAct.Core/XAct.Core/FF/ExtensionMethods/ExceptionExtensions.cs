﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Text;
    using XAct.Diagnostics;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    /// Extension methods to the <see cref="Exception"/>
    /// object.
    /// </summary>
    public static class ExceptionExtensions
    {

        /// <summary>
        /// Trace the Exception to the <see cref="ITracingService" /> implementation.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The arguments.</param>
        public static void Trace(this Exception e, TraceLevel traceLevel, string message, params object[] args)
        {
            XAct.DependencyResolver.Current.GetInstance<ITracingService>().TraceException(traceLevel, e,message, args);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>Creates a log-string from the Exception.</para>
        /// <para>The result includes the stacktrace, innerexception et cetera, separated by <seealso cref="System.Environment.NewLine"/>.</para>
        /// </summary>
        /// <param name="ex">The exception to create the string from.</param>
        /// <param name="additionalMessage">Additional message to place at the top of the string, maybe be empty or null.</param>
        /// <returns></returns>
        /// <internal>
        /// Src: http://bit.ly/eoOpqq 
        /// </internal>
        public static string ToLogString(this Exception ex, string additionalMessage)
        {
            StringBuilder msg = new StringBuilder();

            if (!string.IsNullOrEmpty(additionalMessage))
            {
                msg.Append(additionalMessage);
                msg.Append(System.Environment.NewLine);
            }

            if (ex != null)
            {
                try
                {
                    Exception orgEx = ex;

                    msg.Append("Exception:");
                    msg.Append(System.Environment.NewLine);
                    while (orgEx != null)
                    {
                        msg.Append(orgEx.Message);
                        msg.Append(System.Environment.NewLine);
                        orgEx = orgEx.InnerException;
                    }

                    foreach (object i in ex.Data)
                    {
                        msg.Append("Data :");
                        msg.Append(i.ToString());
                        msg.Append(System.Environment.NewLine);
                    }


                    if (ex.StackTrace != null)
                    {
                        msg.Append("StackTrace:");
                        msg.Append(System.Environment.NewLine);
                        msg.Append(ex.StackTrace);
                        msg.Append(System.Environment.NewLine);
                    }

                    if (ex.Source != null)
                    {
                        msg.Append("Source:");
                        msg.Append(System.Environment.NewLine);
                        msg.Append(ex.Source);
                        msg.Append(System.Environment.NewLine);
                    }

                    if (ex.TargetSite != null)
                    {
                        msg.Append("TargetSite:");
                        msg.Append(System.Environment.NewLine);
                        msg.Append(ex.TargetSite.ToString());
                        msg.Append(System.Environment.NewLine);
                    }

                    Exception baseException = ex.GetBaseException();

                    msg.Append("BaseException:");
                    msg.Append(System.Environment.NewLine);
                    msg.Append(ex.GetBaseException());
                }
                finally
                {
                }
            }
            return msg.ToString();
        }


        //    private static Action<Exception> _preserveInternalException;

        //    static ExceptionExtensions()
        //    {
        //        MethodInfo preserveStackTrace = 
        //            typeof(Exception).GetMethod("InternalPreserveStackTrace", BindingFlags.Instance | BindingFlags.NonPublic);

        //        _preserveInternalException = (Action<Exception>)Delegate.CreateDelegate(typeof(Action<Exception>), preserveStackTrace);
        //    }

        ///// <summary>
        ///// Fluent way to preserve the stack before rethrowing the same exception. 
        ///// Not at all sure why one would use it.
        ///// </summary>
        ///// <param name="ex"></param>
        ///// <returns></returns>
        //    public static Exception PreserveStackTrace(this Exception ex)
        //    {
        //        _preserveInternalException(ex);
        //        return ex;
        //    }
    }




#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
