﻿// ReSharper disable CheckNamespace
namespace XAct.Extensions
// ReSharper restore CheckNamespace
{
    using System.Diagnostics.Contracts;
    using System.Xml;
    using XAct.Xml;

    /// <summary>
    /// Extensions Methods to all <see cref="object"/>s.
    /// </summary>
    public static class ObjectExtensions2
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        ///   Serialize the object and return an XmlNode.
        /// </summary>
        /// <param name = "objectToSerialize">The object to serialize.</param>
        //[SuppressMessage("Microsoft.Naming", "CA1720", Justification = "objectToSerialize is more descriptive than 'value'.")]
        public static XmlNode ToXmlNode(this object objectToSerialize)
        {
            objectToSerialize.ValidateIsNotDefault("objectToSerialize");
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            XmlNodeSerializer xmlNodeSerializer = new XmlNodeSerializer(objectToSerialize.GetType());

            return xmlNodeSerializer.Serialize(objectToSerialize);
        }


    }
}