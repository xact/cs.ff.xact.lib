﻿#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.Diagnostics;

// ReSharper restore CheckNamespace
#endif


/// <summary>
    /// Extension Methods to the 
    /// <see cref="TraceLevel"/> Enumeration type.
    /// </summary>
    public static class TraceLevelExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Converts a generic all purpose <see cref="TraceLevel"/>
        /// to the more 
        /// specific 
        /// <see cref="TraceEventType"/> 
        /// lthe type of the trace event.
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <returns></returns>
        public static TraceEventType ToTraceEventType(this TraceLevel traceLevel)
        {
            switch (traceLevel)
            {
                case TraceLevel.Error:
                    return TraceEventType.Error;
                case TraceLevel.Warning:
                    return TraceEventType.Warning;
                case TraceLevel.Info:
                    return TraceEventType.Information;
                case TraceLevel.Verbose:
                    return TraceEventType.Verbose;
                case TraceLevel.Off:
                    throw new ArgumentOutOfRangeException("traceLevel");
            }
            return TraceEventType.Verbose;
        }
    }


#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
