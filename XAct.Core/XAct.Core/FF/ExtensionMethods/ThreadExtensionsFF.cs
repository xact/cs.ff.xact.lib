namespace XAct
{
    using System;
    using System.Threading;

    /// <summary>
    /// Extensions to the <see cref="Thread"/> object
    /// </summary>
    public static class ThreadExtensionsFF
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the TLS object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="thread">The thread.</param>
        /// <param name="slotName">Name of the slot.</param>
        /// <returns></returns>
        [Obsolete("As per http://bit.ly/wScpJd, ThreadStatic is more performant.")]
        public static T GetTLSObject<T>(this Thread thread, string slotName)
        {
            slotName.ValidateIsNotNullOrEmpty("slotName", "A slot name is required.");

            LocalDataStoreSlot dslSlot = Thread.GetNamedDataSlot(slotName);

            return Thread.GetData(dslSlot).ConvertTo<T>();
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Sets the TLS object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="thread">The thread.</param>
        /// <param name="slotName">The slot.</param>
        /// <param name="obj">The obj.</param>
        [Obsolete("As per http://bit.ly/wScpJd, ThreadStatic is more performant.")]
        public static void SetTLSObject<T>(this Thread thread, string slotName, T obj)
        {
            slotName.ValidateIsNotNullOrEmpty("slotName", "A slot is required.");
            LocalDataStoreSlot dslSlot = Thread.GetNamedDataSlot(slotName);
            dslSlot = Thread.AllocateNamedDataSlot(slotName);
            Thread.SetData(dslSlot, obj);
        }
    }
}