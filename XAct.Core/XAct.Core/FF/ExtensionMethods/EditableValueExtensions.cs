﻿

#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment

#endif

#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
// ReSharper restore CheckNamespace
#endif

/// <summary>
    /// Extensions to the <see cref="EditableValue{TValue}"/>
    /// </summary>
    /// <internal><para>8/15/2011: Sky</para></internal>
    public static class EditableValueExtensions
    {
        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// Gets the typed value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <returns></returns>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public static TValue GetValue<TValue>(this EditableValue<TValue> editableValue)
        {
            return editableValue.Value.ConvertTo<TValue>();
        }

        /// <summary>
        /// Sets the typed value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="editableValue">The editable value.</param>
        /// <param name="value">The value.</param>
        /// <internal>8/13/2011: Sky</internal>
        public static void SetValue<TValue>(this EditableValue<TValue> editableValue, TValue value)
        {
            //editableValue.Value.SetValue(value.ConvertTo(editableValue.ValueType)));
        }
    }



#if (!REMOVENS4EXTENSIONS) 
//See: http://bit.ly/snE0xY
}
#endif
