﻿namespace XAct
{
    using System;
    using XAct.IO;

    /// <summary>
    /// Extensions to the <see cref="XAct.IO.FileAccess"/>
    /// enumeration
    /// </summary>
    public static class FileAccessExtensions
    {
        /// <summary>
        /// Maps the given portable enum value to a <see cref="System.IO.FileAccess"/> enum value.
        /// </summary>
        /// <param name="fileAccess">The file access.</param>
        /// <returns></returns>
        /// <exception cref="System.IndexOutOfRangeException"></exception>
        public static System.IO.FileAccess MapTo(this XAct.IO.FileAccess fileAccess)
        {
            switch (fileAccess)
            {
                case FileAccess.Read:
                    return System.IO.FileAccess.Read;
                case FileAccess.ReadWrite:
                    return System.IO.FileAccess.ReadWrite;
                case FileAccess.Write:
                    return System.IO.FileAccess.Write;
                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}