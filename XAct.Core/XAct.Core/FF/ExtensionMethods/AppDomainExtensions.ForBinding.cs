﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using XAct.Services;
    using XAct.Services.IoC.Initialization;

    /// <summary>
    /// Extensions to the AppDomain object.
    /// </summary>
    public static partial class AppDomainExtensions
    {

        /// <summary>
        /// Initializes the library services.
        /// <para>
        /// Invoked by XAct.Services.IoC.Ninject's NinjectBootstrapper and
        /// XAct.Services.IoC.Unity's UnityBootstrapper, etc.
        /// </para>
        /// </summary>
        /// <param name="optionalOnErrorBindingCallback">The optional pre binding callback.</param>
        /// <param name="optionalPostBindingCallback">The optional post binding callback.</param>
        /// <param name="servicesToRegisterBeforeXActLibServices">The services to register before x act library services.</param>
        /// <param name="performPreScan">if set to <c>true</c> perform an (expensive) pre scan of assemblies for Binding information to do after processing the <paramref name="servicesToRegisterBeforeXActLibServices"/> bindings.</param>
        /// <returns></returns>
        public static InitializeLibraryBindingsResults RegisterBindings(
            bool performPreScan = true,
            Action<IBindingDescriptor> optionalOnErrorBindingCallback = null,
            Action<IBindingDescriptorResult> optionalPostBindingCallback = null,
            IEnumerable<IBindingDescriptorBase> servicesToRegisterBeforeXActLibServices = null
            )
        {


            AppDomain appDomain = (XAct.Library.Settings.IoC.UseTempAppDomainWhenScanningForServices)
                                      ? AppDomain.CreateDomain("XActLibScan")
                                      : AppDomain.CurrentDomain;

            InitializeLibraryBindingsResults bindingResults = 
                new InitializeLibraryBindingsResults();

                appDomain.RegisterBindings(
                ref bindingResults,
                    performPreScan,
                    optionalOnErrorBindingCallback,
                    optionalPostBindingCallback,
                    servicesToRegisterBeforeXActLibServices);


            DependencyResolver.BindingResults = bindingResults;

            return bindingResults;
        }




        /// <summary>
        /// Initializes the library services.
        /// <para>
        /// Invoked by XAct.Services.IoC.Ninject's NinjectBootstrapper and
        /// XAct.Services.IoC.Unity's UnityBootstrapper, etc.
        /// </para>
        /// </summary>
        /// <param name="appDomain">The application domain.</param>
        /// <param name="bindingResults">The binding results.</param>
        /// <param name="performPreScan">if set to <c>true</c> perform an (expensive) pre scan of assemblies for Binding information to do after processing the <paramref name="servicesToRegisterBeforeXActLibServices" /> bindings.</param>
        /// <param name="optionalOnErrorBindingCallback">The optional pre binding callback.</param>
        /// <param name="optionalPostBindingCallback">The optional post binding callback.</param>
        /// <param name="servicesToRegisterBeforeXActLibServices">The services to register before x act library services.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Aha! LoadAllLocalAndReferencedAssemblies *does* work.</exception>
        public static void RegisterBindings(
            this AppDomain appDomain,
           ref InitializeLibraryBindingsResults bindingResults,
            bool performPreScan = true,
            Action<IBindingDescriptor> optionalOnErrorBindingCallback = null,
            Action<IBindingDescriptorResult> optionalPostBindingCallback = null,
            IEnumerable<IBindingDescriptorBase> servicesToRegisterBeforeXActLibServices = null
            )
        {



            BindingScanResults bindingScanResults = bindingResults.BindingScanResults;
#if DEBUG
            int count1 = appDomain.GetAssemblies().Count();
#endif
            //Pay close attention to this sequence:
            appDomain.LoadAllLocalAndReferencedAssemblies(ref bindingScanResults);
#if DEBUG

            Assembly[] assemblies = appDomain.GetAssemblies();

            int count2 = assemblies.Count();
            if (count2 > count1)
            {
                //Yes...it does work.
                //throw new Exception("Aha! LoadAllLocalAndReferencedAssemblies *does* work.");
            }
#endif



                assemblies.ScanForAndRegisterBindingDescriptors(
                ref bindingResults,    
                performPreScan,
                optionalOnErrorBindingCallback,
                optionalPostBindingCallback,
                servicesToRegisterBeforeXActLibServices);


            XAct.DependencyResolver.BindingResults  = bindingResults;

        }







        /// <summary>
        /// Scans discoverable assemblies for Class instances
        /// decorated with <see cref="DefaultBindingImplementationAttribute"/>.
        /// </summary>
        /// <param name="appDomain">The application domain.</param>
        /// <param name="bindingScanResults">The results.</param>
        /// <param name="dependenciesToRegisterFirst">The services to register first.</param>
        /// <param name="skipDuplicates">if set to <c>true</c> [skip duplicates].</param>
        /// <returns></returns>
        public static IBindingDescriptor[] ScanForDefaultBindingDescriptors(
            this AppDomain appDomain,
            ref BindingScanResults bindingScanResults,
            IBindingDescriptor[] dependenciesToRegisterFirst = null,
            bool skipDuplicates = true)
        {

            if (dependenciesToRegisterFirst == null)
            {
                dependenciesToRegisterFirst = new IBindingDescriptor[0];
            }
            Assembly[] assemblies = appDomain.LoadAllLocalAndReferencedAssemblies(ref bindingScanResults);

            assemblies.ScanForDefaultBindingDescriptors(dependenciesToRegisterFirst, skipDuplicates,
                                                        ref bindingScanResults);

            return bindingScanResults.BindingDescriptors;
        }







    }
}
