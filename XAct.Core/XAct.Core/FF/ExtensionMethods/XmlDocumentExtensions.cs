﻿using System.IO;
using System.Text;
using System.Xml.Linq;

namespace XAct
{
    using System.Collections.Generic;
    using System.Xml;
    using XAct.Environment;
    using XAct.IO;

    /// <summary>
    /// 
    /// </summary>
    public static class XmlDocumentExtensions
    {
        /// <summary>
        /// Helper tool to read from an xml file a certain set of XPaths.
        /// </summary>
        /// <param name="emptyXmlDocument">The empty XML document.</param>
        /// <param name="relativeOrfileFullName">Full name of the relative orfile.</param>
        /// <param name="xPaths">The x paths.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="ioService">The io service.</param>
        /// <returns></returns>
        public static Dictionary<string, string> ReadXPathValues(this XmlDocument emptyXmlDocument, string relativeOrfileFullName, Dictionary<string, string> xPaths, IEnvironmentService environmentService = null, IIOService ioService = null)
        {
            if (environmentService == null)
            {
                environmentService = XAct.DependencyResolver.Current.GetInstance<IEnvironmentService>();
            }
            if (ioService == null)
            {
                ioService = XAct.DependencyResolver.Current.GetInstance<IIOService>();
            }


            string fileName = environmentService.MapPath(relativeOrfileFullName);


            //NOT UNTIL 
            //return await Task.Run(() => File.Exists(fileFullName));
            //IS FIXED!
            //if (ioService.FileExistsAsync(fileName).WaitAndGetResult())
            //{
            if (System.IO.File.Exists(fileName))
            {
                emptyXmlDocument.Load(fileName);
            }
            //}

            return emptyXmlDocument.ReadXPathValues(xPaths);
        }
        /// <summary>
        /// Reads the x path values.
        /// </summary>
        /// <param name="emptyXmlDocument">The empty XML document.</param>
        /// <param name="xPaths">The x paths.</param>
        /// <returns></returns>
        public static Dictionary<string, string> ReadXPathValues(this XmlDocument emptyXmlDocument,
                                                          Dictionary<string, string> xPaths)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> kvp in xPaths)
            {
                var nodes = emptyXmlDocument.SelectNodes(kvp.Value);
                var node = nodes.Count > 0 ? nodes[0] : null;
                string value;
                if (node == null) { value = null; }
                else
                {
                    if (node.NodeType == XmlNodeType.Attribute)
                    {
                        value = node.Value;
                    }
                    else
                    {
                        value = node.InnerText;
                    }
                }

                results[kvp.Key] = value;
            }

            return results;

        }

        /// <summary>
        /// Converts the given xDcoument To a stream, with declaration.
        /// </summary>
        /// <param name="xDocument">The x document.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="encoding">The encoding.</param>
        public static void ToStreamWithDeclaration(this XDocument xDocument, Stream stream, Encoding encoding = null)
        {
            using (var writer = new XmlTextWriter(stream, encoding))
            {
                xDocument.WriteTo(writer);
                writer.Close();
            }
        }

    }
}