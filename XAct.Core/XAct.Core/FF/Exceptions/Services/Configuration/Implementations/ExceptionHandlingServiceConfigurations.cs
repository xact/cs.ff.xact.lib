﻿namespace XAct.Exceptions.Configuration.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="IExceptionHandlingServiceConfigurations"/>
    /// contract.
    /// <para>
    /// This configuration object is used by an
    /// implementation of the <see cref="IExceptionHandlingManagementService"/>
    /// </para>
    /// <para>
    /// Note that the default behaviour is to register it into the DependencyInjectionContainer as a Singleton.
    /// </para>
    /// </summary>
    public class ExceptionHandlingServiceConfigurations : IExceptionHandlingServiceConfigurations, IHasXActLibServiceConfiguration
    {
        private readonly Dictionary<string, List<IExceptionHandlingConfiguration>> _cache =
            new Dictionary<string, List<IExceptionHandlingConfiguration>>();

        /// <summary>
        /// Adds the specified exception handling description to the inner cache.
        /// </summary>
        /// <param name="exceptionHandlingDescription">The exception handling description.</param>
        /// <param name="category">The category.</param>
        public void Add(IExceptionHandlingConfiguration exceptionHandlingDescription, string category="default")
        {
            exceptionHandlingDescription.ValidateIsNotDefault("exceptionHandlingDescription");

            if (category.IsNullOrEmpty())
            {
                category = "default";
            }

            if (!_cache.ContainsKey(category))
            {
                _cache.Add(category,new List<IExceptionHandlingConfiguration>());
            }
            
            _cache[category].Add(exceptionHandlingDescription);
        }

        /// <summary>
        /// Gets from the inner cache an <see cref="ExceptionHandlingConfiguration"/>
        /// that matches the given <paramref name="exception"/>
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public IExceptionHandlingConfiguration Get(Exception exception, string category = "default")
        {
            exception.ValidateIsNotDefault("exception");

            if (category.IsNullOrEmpty())
            {
                category = "default";
            }

            if (!_cache.ContainsKey(category))
            {
                if (category== "default")
                {
                    return null; //we won't find it.
                }
                category = "default";
                if (!_cache.ContainsKey(category))
                {
                    return null; //we won't find it.
                }
            }


            IExceptionHandlingConfiguration result = null;

            result = _cache[category].Find(d => exception.IsSubClassOfEx(d.ExceptionType));

            if ((result == null) && (category != "default"))
            {
                category = "default";

                if (!_cache.ContainsKey(category))
                {
                    _cache.Add(category, new List<IExceptionHandlingConfiguration>());
                }
                result = _cache[category].Find(d => exception.IsSubClassOfEx(d.ExceptionType));
            }

            return result;

        }
    }
}