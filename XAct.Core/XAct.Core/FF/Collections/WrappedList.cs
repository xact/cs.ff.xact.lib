﻿#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 

#endif

namespace XAct.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Diagnostics.Contracts;
    using System.Runtime.Serialization;

    /// <summary>
    /// </summary>
    /// <typeparam name = "TItem"></typeparam>
    /// <remarks>
    ///   <para>
    ///     Collection to wrap the EntityCollection
    ///     allowing end users to not have to reference
    ///     the LLBL Generated assemblies where the 
    ///     EntityCollection was defined.
    ///   </para>
    /// </remarks>
    [CollectionDataContract]
    [Serializable]
    [SuppressMessage("Microsoft.Naming", "CA1710", Justification = "It's a list -- not just a collection.")]
    public class WrappedList<TItem> : IList, IHasInnerItemReadOnly, IHasIsReadOnly
        //DAMN!REMOVED: IList<TItem>,
        //SLS:, INotifyCollectionChanged
    {
        private readonly IList _innerList; // = new List<TItem>();

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "WrappedList&lt;TItem&gt;" /> class.
        /// </summary>
        public WrappedList()
        {
            if (_innerList == null)
            {
                _innerList = new List<TItem>();
            }
        }

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "WrappedList&lt;TItem&gt;" /> class.
        /// </summary>
        /// <param name = "innerList">The innerlist.</param>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if the 
        ///   <paramref name = "innerList" /> 
        ///   argument is null.
        /// </exception>
        public WrappedList(IList innerList)
        {
            innerList.ValidateIsNotDefault("innerList");
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            _innerList = innerList;
        }

        #endregion

        //REMOVED DUE TO ERROR IN 
        //UnitTests_SMTPDeliveryServices iterating over Headers...
        /*
        /// <summary>
        /// Returns an enumerator that 
        /// iterates through the collection.
        /// </summary>
        /// <returns>
        /// A 
        /// <see cref="T:System.Collections.Generic.IEnumerator`1"/> 
        /// that can be used to iterate through the collection.
        /// </returns>
        IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
        {
            return (_InnerList != null)
                ?
                (IEnumerator<TItem>)_InnerList.GetEnumerator()
                :
                null;
        }
        */

        /// <summary>
        ///   Gets or sets the 
        ///   item 
        ///   at the specified index.
        /// </summary>
        /// <value></value>
        public TItem this[int index]
        {
            get
            {
                var result = (TItem) _innerList[index];
                return result;
            }

            set
            {
                //FIX:CA1804:TItem old = (TItem) _InnerList[index];

                _innerList[index] = value;
            }
        }

        #region IList Members

        /// <summary>
        ///   Removes the list item 
        ///   at the specified index.
        /// </summary>
        /// <param name = "index">
        ///   The zero-based index of the item to remove.
        /// </param>
        /// <exception cref = "T:System.ArgumentOutOfRangeException">
        ///   <paramref name = "index" /> 
        ///   is not a valid index in the 
        ///   list.
        /// </exception>
        /// <exception cref = "T:System.NotSupportedException">
        ///   The list is read-only.
        ///   -or-
        ///   The list has a fixed size.
        /// </exception>
        public void RemoveAt(int index)
        {
            //FIX:CA1804:TItem item = (TItem) _InnerList[index];

            _innerList.RemoveAt(index);
        }

        /// <summary>
        ///   Removes all items from the 
        ///   list.
        /// </summary>
        /// <exception cref = "T:System.NotSupportedException">
        ///   The list is read-only.
        /// </exception>
        public void Clear()
        {
            _innerList.Clear();
        }

        /// <summary>
        ///   Gets the number of elements contained in the 
        ///   list.
        /// </summary>
        /// <value></value>
        /// <returns>
        ///   The number of elements contained in the 
        ///   list.
        /// </returns>
        public int Count
        {
            get
            {
                var result = (_innerList != null) ? _innerList.Count : 0;
                return result;
            }
        }

        /// <summary>
        ///   Gets a value indicating whether the 
        ///   list is read-only.
        /// </summary>
        /// <value></value>
        /// <returns>true if the list is read-only; otherwise, false.
        /// </returns>
        public bool IsReadOnly
        {
            get
            {
                return _innerList.IsReadOnly;
            }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        ///   Returns an enumerator that iterates 
        ///   through a collection.
        /// </summary>
        /// <returns>
        ///   An <see cref = "T:System.Collections.IEnumerator" /> 
        ///   object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        ///   Adds an item to the 
        ///   <see cref = "T:System.Collections.IList" />.
        /// </summary>
        /// <param name = "value">The <see cref = "T:System.Object" />
        ///   to add to the 
        ///   <see cref = "T:System.Collections.IList" />.
        /// </param>
        /// <returns>
        ///   The position into which the new element was inserted.
        /// </returns>
        /// <exception cref = "T:System.NotSupportedException">
        ///   The <see cref = "T:System.Collections.IList" /> is read-only.
        ///   -or-
        ///   The <see cref = "T:System.Collections.IList" /> has a fixed size.
        /// </exception>
        public int Add(object value)
        {
            _innerList.Add((TItem) value);
            return -1;
        }

        /// <summary>
        ///   Determines whether the 
        ///   <see cref = "T:System.Collections.IList" /> 
        ///   contains a specific value.
        /// </summary>
        /// <param name = "value">
        ///   The <see cref = "T:System.Object" /> to locate in the 
        ///   <see cref = "T:System.Collections.IList" />.</param>
        /// <returns>
        ///   true if the 
        ///   <see cref = "T:System.Object" /> is found in the 
        ///   <see cref = "T:System.Collections.IList" />; otherwise, false.
        /// </returns>
        public bool Contains(object value)
        {
            var result = _innerList.Contains((TItem) value);
            return result;
        }

        /// <summary>
        ///   Determines the index of a specific item in the 
        ///   <see cref = "T:System.Collections.IList" />.
        /// </summary>
        /// <param name = "value">
        ///   The <see cref = "T:System.Object" /> 
        ///   to locate in the <see cref = "T:System.Collections.IList" />.
        /// </param>
        /// <returns>
        ///   The index of <paramref name = "value" /> if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(object value)
        {
            var result = _innerList.IndexOf((TItem) value);
            return result;
        }

        /// <summary>
        ///   Inserts an item to the 
        ///   <see cref = "T:System.Collections.IList" /> 
        ///   at the specified index.
        /// </summary>
        /// <param name = "index">
        ///   The zero-based index at which 
        ///   <paramref name = "value" /> should be inserted.
        /// </param>
        /// <param name = "value">
        ///   The <see cref = "T:System.Object" /> 
        ///   to insert into the 
        ///   <see cref = "T:System.Collections.IList" />.</param>
        /// <exception cref = "T:System.ArgumentOutOfRangeException">
        ///   <paramref name = "index" /> is not a valid index in the 
        ///   <see cref = "T:System.Collections.IList" />.
        /// </exception>
        /// <exception cref = "T:System.NotSupportedException">
        ///   The <see cref = "T:System.Collections.IList" /> is read-only.
        ///   -or-
        ///   The <see cref = "T:System.Collections.IList" /> has a fixed size.
        /// </exception>
        /// <exception cref = "T:System.NullReferenceException">
        ///   <paramref name = "value" /> is null reference in the 
        ///   <see cref = "T:System.Collections.IList" />.
        /// </exception>
        public void Insert(int index, object value)
        {
            _innerList.Insert(index, (TItem) value);
        }

        /// <summary>
        ///   Gets a value indicating whether the 
        ///   <see cref = "T:System.Collections.IList" /> 
        ///   has a fixed size.
        /// </summary>
        /// <value></value>
        /// <returns>
        ///   true if the <see cref = "T:System.Collections.IList" />
        ///   has a fixed size; otherwise, false.
        /// </returns>
        public bool IsFixedSize
        {
            get { return false; }
        }

        /// <summary>
        ///   Removes the first occurrence of a specific 
        ///   object from the <see cref = "T:System.Collections.IList" />.
        /// </summary>
        /// <param name = "value">The <see cref = "T:System.Object" /> 
        ///   to remove from the <see cref = "T:System.Collections.IList" />.</param>
        /// <exception cref = "T:System.NotSupportedException">
        ///   The <see cref = "T:System.Collections.IList" /> is read-only.
        ///   -or-
        ///   The <see cref = "T:System.Collections.IList" /> has a fixed size.
        /// </exception>
        public void Remove(object value)
        {
            //FIX:CA1804:int index = _InnerList.IndexOf((TItem) value);
            _innerList.Remove((TItem) value);
        }

        /// <summary>
        ///   Gets or sets the <see cref = "System.Object" /> at the specified index.
        /// </summary>
        /// <value></value>
        object IList.this[int index]
        {
            get
            {
                var result = _innerList[index];
                return result;
            }
            set
            {
                //FIX:CA1804:TItem old = (TItem) _InnerList[index];
                _innerList[index] = (TItem) value;
            }
        }

        /// <summary>
        ///   Copies the elements of the 
        ///   <see cref = "T:System.Collections.ICollection" />
        ///   to an 
        ///   <see cref = "T:System.Array" />, 
        ///   starting at a particular 
        ///   <see cref = "T:System.Array" /> index.
        /// </summary>
        /// <param name = "array">
        ///   The one-dimensional 
        ///   <see cref = "T:System.Array" /> 
        ///   that is the destination of the elements copied from 
        ///   <see cref = "T:System.Collections.ICollection" />. 
        ///   The <see cref = "T:System.Array" /> must have zero-based indexing.</param>
        /// <param name = "index">
        ///   The zero-based index in 
        ///   <paramref name = "array" /> 
        ///   at which copying begins.</param>
        /// <exception cref = "T:System.ArgumentNullException">
        ///   <paramref name = "array" /> is null.
        /// </exception>
        /// <exception cref = "T:System.ArgumentOutOfRangeException">
        ///   <paramref name = "index" /> is less than zero.
        /// </exception>
        /// <exception cref = "T:System.ArgumentException">
        ///   <paramref name = "array" /> is multidimensional.
        ///   -or-
        ///   <paramref name = "index" /> 
        ///   is equal to or greater than the length of 
        ///   <paramref name = "array" />.
        ///   -or-
        ///   The number of elements in the source 
        ///   <see cref = "T:System.Collections.ICollection" /> 
        ///   is greater than the available space from 
        ///   <paramref name = "index" /> 
        ///   to the end of the destination 
        ///   <paramref name = "array" />.
        /// </exception>
        /// <exception cref = "T:System.ArgumentException">
        ///   The type of the source 
        ///   <see cref = "T:System.Collections.ICollection" /> 
        ///   cannot be cast automatically to the type of the destination 
        ///   <paramref name = "array" />.
        /// </exception>
        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///   Gets a value indicating whether access to the 
        ///   <see cref = "T:System.Collections.ICollection" /> 
        ///   is synchronized (thread safe).
        /// </summary>
        /// <value></value>
        /// <returns>
        ///   true if access to the 
        ///   <see cref = "T:System.Collections.ICollection" /> 
        ///   is synchronized (thread safe); 
        ///   otherwise, false.
        /// </returns>
        public bool IsSynchronized
        {
            get { return false; }
        }

        /// <summary>
        ///   Gets an object that can be used to synchronize access to the
        ///   <see cref = "T:System.Collections.ICollection" />.
        /// </summary>
        /// <value></value>
        /// <returns>
        ///   An object that can be used to synchronize access to the 
        ///   <see cref = "T:System.Collections.ICollection" />.
        /// </returns>
        public object SyncRoot
        {
            get { return this; }
        }

        #endregion

        #region IObjectWrapper<IList> Members

        /// <summary>
        ///   The inner <see cref = "IList" /> that this List wraps.
        /// </summary>
        /// <remarks>
        ///   This property is an explicit implementation 
        ///   of the <see cref = "IWrappedList" /> interface
        ///   as it should not be readily accessible
        /// </remarks>
        T IHasInnerItemReadOnly.GetInnerItem<T>()
        {
            T result = _innerList.ConvertTo<T>();
            return result;
        }

        #endregion

        /// <summary>
        ///   Returns the index of the item within the collection.
        /// </summary>
        /// <param name = "item">The item.</param>
        /// <returns></returns>
        public int IndexOf(TItem item)
        {
#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            var result = _innerList.IndexOf(item);
            return result;
        }

        /// <summary>
        ///   Inserts the item at the specified index.
        /// </summary>
        /// <param name = "index">The index.</param>
        /// <param name = "item">The item.</param>
        public void Insert(int index, TItem item)
        {
            _innerList.Insert(index, item);
        }

        /// <summary>
        ///   Adds the specified item to the collection.
        /// </summary>
        /// <param name = "item">The item.</param>
        public void Add(TItem item)
        {
            _innerList.Add(item);
        }

        /// <summary>
        ///   Determines whether The list contains the specified item.
        /// </summary>
        /// <param name = "item">The item.</param>
        /// <returns>
        ///   <c>true</c> if the list contains the specified item; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(TItem item)
        {
            var result = _innerList.Contains(item);
            return result;
        }

        ///<summary>
        ///  Copies the elements of the System.Collections.ICollection to an System.Array,
        ///  starting at a particular System.Array index.
        ///</summary>
        ///<param name = "array">
        ///  The one-dimensional System.Array that is the destination of the elements
        ///  copied from System.Collections.ICollection. The System.Array must have zero-based
        ///  indexing.
        ///</param>
        ///<param name = "arrayIndex">
        ///  The zero-based index in array at which copying begins.
        ///</param>
        ///<exception cref = "System.ArgumentNullException">
        ///  array is null.
        ///</exception>
        ///<exception cref = "System.ArgumentOutOfRangeException">
        ///  index is less than zero.
        ///</exception>
        ///<exception cref = "System.ArgumentException">
        ///  array is multidimensional.
        ///</exception>
        ///<exception cref = "System.ArgumentException">
        ///  index is equal to or greater than the length of array.
        ///</exception>
        ///<exception cref = "System.ArgumentException">
        ///  The number of elements in the source System.Collections.ICollection
        ///  is greater than the available space from 
        ///  index to the end of the destination
        ///  array.
        ///</exception>
        ///<exception cref = "System.ArgumentException">
        ///  The type of the source System.Collections.ICollection 
        ///  cannot be cast automatically to the 
        ///  type of the destination array.
        ///</exception>
        public void CopyTo(TItem[] array, int arrayIndex)
        {
            _innerList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        ///   Removes the specified item from the collection.
        /// </summary>
        /// <param name = "item">The item.</param>
        /// <returns></returns>
        public bool Remove(TItem item)
        {
            int index = _innerList.IndexOf(item);
            if (index < 0)
            {
                //TODO: 
                return false;
            }
            _innerList.Remove(item);
            return true;
        }

        /// <summary>
        ///   Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        ///   An <see cref = "T:System.Collections.IEnumerator" /> 
        ///   object that can be used to iterate through the collection.
        /// </returns>
        protected IEnumerator GetEnumerator()
        {
            if (_innerList != null)
            {
                return _innerList.GetEnumerator();
            }
            return null;
        }
    }
}