﻿namespace XAct.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class PriorityQueue<TItem> : IEnumerable<TItem>, ICollection, IEnumerable where TItem : new()
    {
        // Fields
        private readonly SortedDictionary<int, Queue<TItem>> _innerDictionaryOfQueuesByPriority;

        /// <summary>
        /// Class of Reosources
        /// </summary>
        public class Resources
        {
            /// <summary>
            /// Local Resource string.
            /// </summary>
            public const string ArgumentCannotBeNull = "Argument {0} cannot be null";
        }

        #region Properties

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.ICollection"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The number of elements contained in the <see cref="T:System.Collections.ICollection"/>.
        /// </returns>
        public int Count
        {
            get
            {
                var result = this._innerDictionaryOfQueuesByPriority.Values.Sum(queue => queue.Count);
                return result;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this queue is empty.
        /// </summary>
        /// <value><c>true</c> if this instance is empty; otherwise, <c>false</c>.</value>
        public bool IsEmpty
        {
            get
            {
                var result = (this._innerDictionaryOfQueuesByPriority.Count == 0);
                return result;

            }
        }

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection"/> is synchronized (thread safe).
        /// </summary>
        /// <value></value>
        /// <returns>true if access to the <see cref="T:System.Collections.ICollection"/> is synchronized (thread safe); otherwise, false.
        /// </returns>
        bool ICollection.IsSynchronized
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// An object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection"/>.
        /// </returns>
        object ICollection.SyncRoot
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PriorityQueue&lt;TItem&gt;"/> class.
        /// </summary>
        public PriorityQueue()
        {
            this._innerDictionaryOfQueuesByPriority = new SortedDictionary<int, Queue<TItem>>();
        }

        /// <summary>
        /// Clears the Queue.
        /// </summary>
        public void Clear()
        {
            foreach (Queue<TItem> queue in this._innerDictionaryOfQueuesByPriority.Values)
            {
                queue.Clear();
            }
        }

        /// <summary>
        /// Determines whether the queue contains the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(TItem item)
        {
            foreach (Queue<TItem> queue in this._innerDictionaryOfQueuesByPriority.Values)
            {
                IEnumerator enumerator = queue.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    TItem current = (TItem) enumerator.Current;
                    if (Equals(current, item))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection"/> to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection"/>. The <see cref="T:System.Array"/> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in <paramref name="array"/> at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="array"/> is null.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is less than zero.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="array"/> is multidimensional.
        /// -or-
        /// <paramref name="index"/> is equal to or greater than the length of <paramref name="array"/>.
        /// -or-
        /// The number of elements in the source <see cref="T:System.Collections.ICollection"/> is greater than the available space from <paramref name="index"/> to the end of the destination <paramref name="array"/>.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// The type of the source <see cref="T:System.Collections.ICollection"/> cannot be cast automatically to the type of the destination <paramref name="array"/>.
        /// </exception>
        public void CopyTo(Array array, int index)
        {
            array.ValidateIsNotDefault(Resources.ArgumentCannotBeNull.FormatStringCurrentUICulture("array"));
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException("index");
            }
            if (index >= array.Length)
            {
                throw new ArgumentException("index");
            }
            int num = 0;
            int length = array.Length;
            int num3 = index;
            if (length != 0)
            {
                foreach (KeyValuePair<int, Queue<TItem>> pair in this._innerDictionaryOfQueuesByPriority)
                {
                    IEnumerator enumerator = pair.Value.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        if (num >= index)
                        {
                            if (num3 == length)
                            {
                                break;
                            }
                            array.SetValue((TItem) enumerator.Current, num);
                            num3++;
                        }
                        num++;
                    }
                }
            }
        }

        /// <summary>
        /// Dequeues an element from the queue, and returns it.
        /// </summary>
        /// <returns></returns>
        public TItem Dequeue()
        {
            if (this._innerDictionaryOfQueuesByPriority.Count == 0)
            {
                throw new InvalidOperationException();
            }
            SortedDictionary<int, Queue<TItem>>.Enumerator enumerator = this._innerDictionaryOfQueuesByPriority.GetEnumerator();
            enumerator.MoveNext();
            KeyValuePair<int, Queue<TItem>> current = enumerator.Current;
            Queue<TItem> queue = current.Value;
            TItem local = queue.Dequeue();
            if (queue.Count == 0)
            {
                this._innerDictionaryOfQueuesByPriority.Remove(current.Key);
            }
            return local;
        }

        /// <summary>
        /// Dequeues an element from the queue witht the specified priority, and returns it.
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <returns></returns>
        public TItem Dequeue(int priority)
        {
            TItem local = default(TItem);
            if (this._innerDictionaryOfQueuesByPriority.Count == 0)
            {
                throw new InvalidOperationException();
            }
            this._innerDictionaryOfQueuesByPriority.GetEnumerator();
            foreach (KeyValuePair<int, Queue<TItem>> pair in this._innerDictionaryOfQueuesByPriority)
            {
                if (pair.Key >= priority)
                {
                    Queue<TItem> queue = pair.Value;
                    local = queue.Dequeue();
                    if (queue.Count == 0)
                    {
                        this._innerDictionaryOfQueuesByPriority.Remove(pair.Key);
                    }
                    return local;
                }
            }
            throw new Exception();
        }

        /// <summary>
        /// Enqueues the specified item, at the given priority.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="priority">The priority.</param>
        public void Enqueue(TItem value, int priority)
        {
            Queue<TItem> queue;
            if (!this._innerDictionaryOfQueuesByPriority.TryGetValue(priority, out queue))
            {
                queue = new Queue<TItem>();
                this._innerDictionaryOfQueuesByPriority.Add(priority, queue);
            }
            queue.Enqueue(value);
        }

        /// <summary>
        /// Gets the next Item, returning it, without removing it from the queue.
        /// </summary>
        /// <returns></returns>
        public TItem Peek()
        {
            if (this._innerDictionaryOfQueuesByPriority.Count == 0)
            {
                throw new InvalidOperationException();
            }
            SortedDictionary<int, Queue<TItem>>.Enumerator enumerator = this._innerDictionaryOfQueuesByPriority.GetEnumerator();
            enumerator.MoveNext();
            KeyValuePair<int, Queue<TItem>> current = enumerator.Current;
            var result = current.Value.Peek();
            return result;
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
        {
            var result = (IEnumerator<TItem>) new Enumerator<TItem>(this);
            return result;
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator<TItem>(this);
        }

        /// <summary>
        /// Converts the queue to an array.
        /// </summary>
        /// <returns></returns>
        public TItem[] ToArray()
        {
            List<TItem> list = new List<TItem>();
            foreach (KeyValuePair<int, Queue<TItem>> pair in this._innerDictionaryOfQueuesByPriority)
            {
                IEnumerator enumerator = pair.Value.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    list.Add((TItem) enumerator.Current);
                }
            }
            var result = list.ToArray();
            return result;
        }

        // Nested Types
        /// <summary>
        /// 
        /// </summary>
        public class Enumerator : Enumerator<TItem>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PriorityQueue&lt;TItem&gt;.Enumerator"/> class.
            /// </summary>
            /// <param name="priorityQueue">The priority queue.</param>
            public Enumerator(PriorityQueue<TItem> priorityQueue)
                : base(priorityQueue)
            {
            }
        }


        /// <summary>
        /// The Enumerator for the <see cref="PriorityQueue{TItem}"/>
        /// </summary>
        /// <typeparam name="TTItem">The type of the T item.</typeparam>
        public class Enumerator<TTItem> : IEnumerator where TTItem : new()
        {
            // Fields
            private TTItem _current;
            private int _index;
            private readonly TTItem[] _items;

            // Methods
            /// <summary>
            /// Initializes a new instance of the <see cref="PriorityQueue&lt;TItem&gt;.Enumerator&lt;TTItem&gt;"/> class.
            /// </summary>
            public Enumerator()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="PriorityQueue&lt;TItem&gt;.Enumerator&lt;TTItem&gt;"/> class.
            /// </summary>
            /// <param name="priorityQueue">The priority queue.</param>
            public Enumerator(PriorityQueue<TTItem> priorityQueue)
            {
                this._items = priorityQueue.ToArray();
                this.Reset();
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>
            /// true if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.
            /// </returns>
            /// <exception cref="T:System.InvalidOperationException">
            /// The collection was modified after the enumerator was created.
            /// </exception>
            public virtual bool MoveNext()
            {
                this._index++;
                if (this._index >= this._items.Length)
                {
                    return false;
                }
                this._current = this._items[this._index];
                return true;
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            /// <exception cref="T:System.InvalidOperationException">
            /// The collection was modified after the enumerator was created.
            /// </exception>
            public virtual void Reset()
            {
                this._index = -1;
                this._current = default(TTItem);
            }

            // Properties
            /// <summary>
            /// Gets the current element in the collection.
            /// </summary>
            /// <value></value>
            /// <returns>
            /// The current element in the collection.
            /// </returns>
            /// <exception cref="T:System.InvalidOperationException">
            /// The enumerator is positioned before the first element of the collection or after the last element.
            /// </exception>
            public object Current
            {
                get
                {
                    return this._current;
                }
            }
        }
    }
}