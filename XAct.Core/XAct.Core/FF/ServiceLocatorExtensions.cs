//using System;
//using System.Diagnostics;
//using Microsoft.Practices.ServiceLocation;

//// ReSharper disable CheckNamespace
//namespace XAct
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public static class ServiceLocatorExtensions
//    {
//        /// <summary>
//        /// Tries to get the specified service type, returning a null if not found.
//        /// </summary>
//        /// <param name="serviceLocator">The service locator.</param>
//        /// <param name="serviceType">Type of the service.</param>
//        /// <returns></returns>
//        [DebuggerHidden]
//        public static object TryGetInstance(this IServiceLocator serviceLocator, Type serviceType)
//        {
//            try
//            {
//                return serviceLocator.GetInstance(serviceType);
//            }
//            catch
//            {
//                return null;
//            }
//        }

//        /// <summary>
//        /// Tries to get the specified service type, returning a null if not found.
//        /// </summary>
//        /// <param name="serviceLocator">The service locator.</param>
//        /// <param name="serviceType">Type of the service.</param>
//        /// <param name="key">The key.</param>
//        /// <returns></returns>
//        [DebuggerHidden]
//        public static object TryGetInstance(this IServiceLocator serviceLocator, Type serviceType, string key)
//        {
//            try
//            {
//                return serviceLocator.GetInstance(serviceType, key);
//            }
//            catch
//            {
//                return null;
//            }
//        }

//        /// <summary>
//        /// Tries to get the specified service type, returning a null if not found.
//        /// </summary>
//        /// <typeparam name="TService">The type of the service.</typeparam>
//        /// <param name="serviceLocator">The service locator.</param>
//        /// <returns></returns>
//        [DebuggerHidden]
//        public static object TryGetInstance<TService>(this IServiceLocator serviceLocator)
//        {
//            try
//            {
//                return serviceLocator.GetInstance<TService>();
//            }
//            catch
//            {
//                return null;
//            }
//        }
//        /// <summary>
//        /// Tries to get the specified service type, returning a null if not found.
//        /// </summary>
//        /// <typeparam name="TService">The type of the service.</typeparam>
//        /// <param name="serviceLocator">The service locator.</param>
//        /// <param name="key">The key.</param>
//        /// <returns></returns>
//        [DebuggerHidden]
//        public static object TryGetInstance<TService>(this IServiceLocator serviceLocator, string key)
//        {
//            try
//            {
//                return serviceLocator.GetInstance<TService>(key);
//            }
//            catch
//            {
//                return null;
//            }
//        }

//        /// <summary>
//        /// Gets the Microsoft Practices ServiceLocator, or null if non registered.
//        /// <para>
//        /// The return Argument is Generic in order to not cause a dependency on 
//        /// <see cref="ServiceLocator"/> for all assemblies that reference this class.
//        /// </para>
//        /// <para>
//        /// Just cast to IServiceLocator before using:
//        /// <code>
//        /// <![CDATA[
//        /// var sl = ServiceLocatorService.GetMSPPServiceLocator<IServiceLocator>();
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </summary>
//        /// <returns></returns>
//        //NO:[DebuggerStepThrough]
//        [DebuggerHidden]
//// ReSharper disable InconsistentNaming
//        public static TServiceInterface TryGetMSPPServiceLocator<TServiceInterface>()
//// ReSharper restore InconsistentNaming
//        {
//            try
//            {
//                //The MSPP property throws an exception
//                //if Current is not registered yet,
//                //or the service is not registered yet:
//                IServiceLocator o = ServiceLocator.Current;

//                return (TServiceInterface)o;
//            }
//            catch
//            {
//                return default(TServiceInterface);
//            }
//        }

        
//    }
//}