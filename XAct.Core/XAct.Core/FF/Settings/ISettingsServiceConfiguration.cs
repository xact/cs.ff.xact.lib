namespace XAct.Settings
{
    using System;
    
    /// <summary>
    /// Contract for a configuration package 
    /// for the <see cref="ISettingsService"/>
    /// </summary>
    public interface ISettingsServiceConfiguration : IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets or sets a value indicating whether to audit changes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [audit changes]; otherwise, <c>false</c>.
        /// </value>
        bool AuditChanges { get; set; }


        /// <summary>
        /// Function to process 
        /// a <see cref="SerializedApplicationSetting.IsUnlockedInformation"/>
        /// and return a <c>bool</c> result.
        /// </summary>
        Func<string, bool> IsUnlockedCallback { get; }
        /// <summary>
        /// Function to process 
        /// a <see cref="SerializedApplicationSetting.IsReadableAuthorisationInformation"/>
        /// and return a <c>bool</c> result.
        /// </summary>
        Func<string, bool> IsReadAuthorizedCallback { get; }
        /// <summary>
        /// Function to process 
        /// a <see cref="SerializedApplicationSetting.IsWritableAuthorisationInformation"/>
        /// and return a <c>bool</c> result.
        /// </summary>
        Func<string, bool> IsWriteAuthorizedCallback { get; }

        /// <summary>
        /// 
        /// </summary>
        Type SettingsType { get; }


        /// <summary>
        /// Instantiates a <see cref="Settings"/> object 
        /// (or subclass of <see cref="Settings"/> 
        /// if <c>SetAppSettingType{TSettings}</c> was invoked prior).
        /// </summary>
        /// <returns></returns>
        TSettings Build<TSettings>()
            where TSettings : Settings;


        /// <summary>
        /// Initializes the type of the <see cref="Settings" />
        /// object to be Instantiated when <c>Build{TSettings}</c>
        /// is invoked.
        /// <para>
        /// If not set, the default action is to return
        /// a <see cref="Settings" /> (not a subclass).
        /// </para>
        /// <para>
        /// If invoked, this method should be invoked
        /// early, during the Application Bootstrap
        /// sequence, then left as is.
        /// </para>
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <param name="appSetting">The application setting.</param>
        /// <param name="zoneOrTier">The zone or tier.</param>
        /// <param name="isUnlockedCallback">The is unlocked callback.</param>
        /// <param name="isReadAuthorizedCallback">The is read authorized callback.</param>
        /// <param name="isWriteAuthorizedCallback">The is write authorized callback.</param>
        /// <param name="auditChanges">if set to <c>true</c> [audit changes].</param>
        void Initialize<TSettings>(
            TSettings appSetting, 
            string zoneOrTier = null, 
            Func<string, bool> isUnlockedCallback = null, 
            Func<string, bool> isReadAuthorizedCallback = null,
            Func<string, bool> isWriteAuthorizedCallback = null,
              bool auditChanges = true)
            where TSettings : Settings;

    }
}