﻿

namespace XAct.Settings
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface ISettings :IModuleSettingGroup
    {

        /// <summary>
        /// Gets the specified module (usually a Vendor or similar set of variables) .
        /// <para>
        /// If found, returns it.
        /// </para>
        /// <para>
        /// If not found, 
        /// dynamically adds the required Module before return it.
        /// </para>
        /// </summary>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="hasAccess">The otional hasAccess func/logic (only required during initialization stage).</param>
        /// <returns></returns>
        /// <internal><para>8/17/2011: Sky</para></internal>
        ModuleSettingGroup GetModule(string moduleName, Func<bool> hasAccess = null);

    }
}
