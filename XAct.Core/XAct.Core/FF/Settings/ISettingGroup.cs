﻿
namespace XAct.Settings
{
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public interface ISettingGroup : ISettingValueAccessor, IHasNameReadOnly, IProfileSettingCollectionAccessor,IEnumerable<Setting>
    {
    }


    
}
