﻿namespace XAct.Settings
{
    using System.ComponentModel;
    using XAct.Security;

    /// <summary>
    /// 
    /// </summary>
    public interface ISetting
    {
        
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ISetting<TValue> :
        IHasDistributedGuidIdReadOnly,
        ISetting,

        IHasNameReadOnly, 
        IEditableObject, 
        INotifyPropertyChanging, 
        INotifyPropertyChanged, 
        IHasModelState, 
        IHasValue<TValue>,
        IHasTagReadOnly
    {




        /// <summary>
        /// Resets the Current Value to the <see cref="SettingEditingMetadata.DefaultValue"/>.
        /// </summary>
        /// <returns></returns>
        /// <internal><para>8/14/2011: Sky</para></internal>
        void ResetValue();


    }
}
