// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;

    /// <summary>
    /// Contract for a service that returns
    /// manages a single copy of a 
    /// <see cref="Settings"/> 
    /// (or a descendent of <see cref="Settings"/>).
    /// <para>
    /// The contract is the basis of:
    /// <c>IApplicationSettingsService</c>
    /// and 
    /// <c>IUserSettingsService</c>.
    /// </para>
    /// </summary>
    /// <internal>8/16/2011: Sky</internal>
    public interface ISettingsService : IHasXActLibService
    {
        /// <summary>
        /// Retrieves the <see cref="Settings"/>
        /// of the specified <see cref="Type"/>.
        /// <para>
        /// If the current <see cref="Settings"/>
        /// is a descendent of <see cref="Settings"/>,
        /// and you don't want to have to cast it
        /// to the right type, consider using <see cref="GetCurrentSettings{TSettings}"/>
        /// instead.
        /// </para>
        /// </summary>
        /// <returns></returns>
        Settings Current { get; }

        /// <summary>
        /// Retrieves the <see cref="Settings"/> (or a subclass of it)
        /// of the specified <see cref="Type"/>.
        /// </summary>
        /// <typeparam name="TSettings">The type of the <see cref="Settings"/>.</typeparam>
        /// <returns></returns>
        TSettings GetCurrentSettings<TSettings>()
            where TSettings : Settings;

        /// <summary>
        /// Persists the <see cref="Current"/>
        ///  <see cref="Settings"/>.
        /// <para>
        /// Any new <see cref="Setting"/> elements in the 
        /// <see cref="Current"/>
        /// <see cref="Settings"/> will be persisted using
        /// ZoneIdentifier, and HostIdentifier
        /// information retrieved from 
        /// <c>ApplicationSettings.ContextIdentifier</c>
        /// </para>
        /// </summary>
        void Persist();
    }
}