﻿namespace XAct.Settings
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The contract for a Service that returns 
    /// readonly Application Settings, common to 
    /// all instances of the application, irrespective
    /// of the load balanced Host it is running on.
    /// <para>
    /// Very often, the service is implemented using
    /// <c>XAct.Settings.ApplicationSettings.AppSettings</c>
    /// </para>
    /// <para>
    /// Important: although the notion of ApplicationSettings
    /// are umbiqutious, there are several disadvantages
    /// to using ApplicationSettings versus 
    /// <c>ProfileSettings</c> (that *can* includ readonly
    /// settings, which is the same as ApplicationSettings).
    /// One of these is that Application Settings
    /// are not serializeable (a separeate mechanism
    /// has to be created to push them to a RIA client)
    /// and are more an old school web server, or WinForm
    /// concept.
    /// </para>
    /// </summary>
    public interface IApplicationSettingsService : IHasXActLibService //: ISettingsService
    {

        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        XAct.Settings.IApplicationSettingsServiceConfiguration Configuration { get; }



        /// <summary>
        /// Retrieves the <see cref="Settings"/>
        /// of the specified <see cref="Type"/>.
        /// <para>
        /// If the current <see cref="Settings"/>
        /// is a descendent of <see cref="Settings"/>,
        /// and you don't want to have to cast it
        /// to the right type, consider using <see cref="GetCurrentSettings{TSettings}"/>
        /// instead.
        /// </para>
        /// </summary>
        /// <returns></returns>
        ApplicationSettings Current { get; }

        /// <summary>
        /// Retrieves the <see cref="Settings"/> (or a subclass of it)
        /// of the specified <see cref="Type"/>.
        /// </summary>
        /// <typeparam name="TSettings">The type of the <see cref="Settings"/>.</typeparam>
        /// <returns></returns>
        TSettings GetCurrentSettings<TSettings>()
            where TSettings : ApplicationSettings;


        
        /// <summary>
        /// Initializes the specified settings.
        /// <para>
        /// Note: settings serializationContext information must be set first.
        /// </para>
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        TSettings Initialize<TSettings>(TSettings settings)
            where TSettings : ApplicationSettings;


        /// <summary>
        /// Persists the specified <see cref="Settings"/> object.
        /// <para>
        /// Any new <see cref="Setting"/> elements in 
        /// <paramref name="settings"/> will be persisted using
        /// ApplicationIdentifier, ZoneIdentifier, and HostIdentifier
        /// information retrieved from 
        /// <see cref="ApplicationSettings.ContextIdentifier"/>
        /// </para>
        /// </summary>
        void Persist(ApplicationSettings settings, IDictionary<string, object> grabBag = null);

    }

}
