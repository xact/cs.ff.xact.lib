﻿namespace XAct.Settings
{
    internal interface IProfileSettingsInternalService: IHasXActLibService
    {
        string[] GetRoleOwner();
    }
}