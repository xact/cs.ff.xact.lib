namespace XAct.Settings.Implementations
{
    /// <summary>
    /// Contract for a service to edit Settings on screen.
    /// </summary>
    public interface ISettingsEditingService: IHasXActLibService
    {
        /// <summary>
        /// Gets a <see cref="Settings"/> collection object
        /// with all the trimming required to render the settings
        /// on an UX View to edit Settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        Settings GetSettingsForEditing(Settings settings);
    }
}