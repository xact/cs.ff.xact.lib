// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using XAct.Environment;

    /// <summary>
    /// Contract for a configuration package to be injected 
    /// into an implementation of <see cref="IApplicationSettingsService"/>.
    /// </summary>
    public interface IApplicationSettingsServiceConfiguration : ISettingsServiceConfiguration, IApplicationSettingScope, IHasXActLibServiceConfiguration
    {

        
        /// <summary>
        /// By default auditing of settings changes
        /// is done with Utc time.
        /// <para>
        /// Override if you really must (never recommended).
        /// </para>
        /// </summary>
        /// <value>
        /// <c>true</c> if [persist using local time]; otherwise, <c>false</c>.
        /// </value>
        bool PersistUsingLocalTime { get; set; }
    }
}