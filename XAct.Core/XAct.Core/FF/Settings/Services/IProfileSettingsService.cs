﻿namespace XAct.Settings
{
    /// <summary>
    /// Contract for a service that returns user settings.
    /// </summary>
    /// <internal>8/16/2011: Sky</internal>
    public interface IProfileSettingsService : ISettingsService, IHasXActLibService
    {
    }
}