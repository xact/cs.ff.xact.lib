﻿namespace XAct.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public interface IModuleSettingGroup
    {
        /// <summary>
        /// Gets a value indicating whether this instance has access.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has access; otherwise, <c>false</c>.
        /// </value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        bool HasAccess { get; }


    }
}
