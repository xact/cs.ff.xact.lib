﻿namespace XAct.Settings
{
    /// <summary>
    /// Contract that  
    /// <see cref="SettingGroup"/>
    /// implements.
    /// </summary>
    public interface ISettingValueAccessor
    {
        /// <summary>
        /// Gets the Value property of the specified *existing* (already Added) <see cref="Setting"/> value.
        /// <para>
        /// Raises an Exception if the Setting is not found.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the property.</param>
        /// <returns></returns>
        /// <internal><para>8/13/2011: Sky</para></internal>
        TValue GetSettingValue<TValue>(string settingName);

        /// <summary>
        /// Gets the Value property of the specified *existing* (already Added) <see cref="Setting" /> value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the setting.</param>
        /// <param name="defaultValueIfNotFound">The default value if not found.</param>
        /// <returns></returns>
        /// <internal>8/13/2011: Sky</internal>
        TValue TryGetSettingValue<TValue>(string settingName, TValue defaultValueIfNotFound);


        /// <summary>
        /// Gets the Value property of the specified *existing* (already Added) <see cref="Setting" /> value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the setting.</param>
        /// <param name="value">The value.</param>
        /// <param name="defaultValueIfNotFound">The default value if not found.</param>
        /// <returns></returns>
        /// <internal>8/13/2011: Sky</internal>
        bool TryGetSettingValue<TValue>(string settingName, out TValue value, TValue defaultValueIfNotFound);

        /// <summary>
        /// Sets the Value property of the specified *existing* (already Added) <see cref="Setting"/> value.
        /// <para>
        /// Raises an Exception if the Setting is not found.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        void SetSettingValue<TValue>(string propertyName, TValue propertyValue);
    }


}