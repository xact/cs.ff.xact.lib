﻿

namespace XAct.FF.Settings.Implementations
{
    using System.Runtime.Serialization;
    using XAct.Domain;
    using XAct.Settings;

    /// <summary>
    /// 
    /// </summary>
        [DataContract(Name = "SettingRecordMetadata", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/settings")]
    public class SettingRecordMetadata
    {


        /// <summary>
        /// Gets or sets the state.
        /// <para>
        /// This flag is not persisted to database (hence why converted to Methods, 
        /// so that ORMs don't think its a datastore table attribute (without
        /// having to decorate it with Ignore, or other ORM specific solution).
        /// </para>
        /// </summary>
        public OfflineModelState GetState() { return _state; }

            /// <summary>
        /// Sets the state.
        /// <para>
        /// This flag is not persisted to database (hence why converted to Methods, 
        /// so that ORMs don't think its a datastore table attribute (without
        /// having to decorate it with Ignore, or other ORM specific solution).
        /// </para>
        /// </summary>
        /// <param name="modelState">State of the offline model.</param>
         public void SetState(OfflineModelState modelState) { _state=modelState; }

        [DataMember]
// ReSharper disable InconsistentNaming
        private OfflineModelState _state { get; set; }
// ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets or sets the optional name of the application for which this setting applices.
        /// </summary>
        /// <value>
        /// The name of the application.
        /// </value>
        [DataMember]
        public virtual string ApplicationName
        {
            get { return _applicationName ?? string.Empty; }
            set { _applicationName = value; }
        }
        private string _applicationName;

        /// <summary>
        /// Gets or sets the optional Application Tier for which this setting applies.
        /// <para>
        /// If Empty, applies to all Tiers -- ie, it is an Application setting.
        /// </para>
        /// </summary>
        /// <value>
        /// The zone.
        /// </value>
        [DataMember]
        public virtual string ZoneOrTier
        {
            get { return _zoneOrTier ?? string.Empty; }
            set { _zoneOrTier = value; }
        }
        private string _zoneOrTier;


        /// <summary>
        /// Gets or sets the optional name of the Host computer this setting is intended for.
        /// <para>
        /// Note that <see cref="ZoneOrTier"/> is generally enough.
        /// </para>
        /// <para>
        /// If Empty, applies to all Hosts -- ie, it is a Tier setting.
        /// </para>
        /// </summary>
        /// <value>
        /// The host.
        /// </value>
        [DataMember]
        public virtual string Host
        {
            get { return _host ?? string.Empty; }
            set { _host = value; }
        }
        private string _host;




        /// <summary>
        /// Gets or sets the (optional) User this setting is relevant to
        /// (only used by <c>IUserSettingsService</c>)
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        [DataMember]
        public string User
        {
            get { return _user ?? string.Empty; }
            set { _user = value; }
        }
        private string _user;


        /// <summary>
        /// Gets or sets the <see cref="Setting"/>'s Fully Qualified Name (Module + Groups + Key).
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }


    
    }
}
