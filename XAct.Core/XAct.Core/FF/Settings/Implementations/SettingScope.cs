﻿namespace XAct.Settings
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Information as to the query specs used to assemble
    /// this <see cref="Settings"/>' <see cref="Setting"/>s.
    /// </summary>
    [DataContract(Name = "ApplicationSettingScope", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/settings")]
    public class ApplicationSettingScope : IApplicationSettingScope
    {
         
        /// <summary>
        /// Gets or sets the identifier/name of the
        /// environment (DEV, ST, SIT, PROD, etc.)
        ///  the Settings are for.
        /// </summary>
        /// <value>
        /// The application identifier.
        /// </value>
        public string EnvironmentIdentifier
        {
            //Note that the db constraint is NotNull:
            get { return _environmentIdentifier ?? string.Empty; }
            set { _environmentIdentifier = value; }
        }
        [DataMember]
        private string _environmentIdentifier;

        /// <summary>
        /// Gets or sets the optional identifier/name of the zone/tier the Settings are for.
        /// </summary>
        /// <value>
        /// The zone or tier identifier.
        /// </value>
        public string ZoneOrTierIdentifier
        {
            //Note that the db constraint is NotNull:
            get { return _zoneOrTierIdentifier ?? string.Empty; }
            set { _zoneOrTierIdentifier = value; }
        }
        [DataMember]
        private string _zoneOrTierIdentifier;

        /// <summary>
        /// Gets or sets the optional identifier/name of the 
        /// host server the Settings are for.
        /// </summary>
        /// <value>
        /// The host identifier.
        /// </value>
        public string HostIdentifier
        {
            //Note that the db constraint is NotNull:
            get { return _hostIdentifier ?? string.Empty; }
            set { _hostIdentifier = value; }
        }
        [DataMember]
        private string _hostIdentifier;


        /// <summary>
        /// Gets or sets the unique Identifier
        /// for the current application Tennant
        /// (in most cases, this is the Organisation).
        /// </summary>
        /// <value>
        /// The tennant identifier.
        /// </value>
        [DataMember]
        public Guid TennantIdentifier { get; set; }



        private ApplicationSettingScope()
        {
            //private constructor required for WCF
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettingScope" /> class.
        /// </summary>
        /// <param name="environmentIdentifier">The application identifier.</param>
        /// <param name="zoneOrTierIdentifier">The zone or tier identifier.</param>
        /// <param name="hostIdentifier">The host identifier.</param>
        /// <param name="tennantIdentifier">The tennant identifier.</param>
        public ApplicationSettingScope(
            string environmentIdentifier, 
            string zoneOrTierIdentifier,
            string hostIdentifier,
            Guid tennantIdentifier)
        {
            EnvironmentIdentifier = environmentIdentifier;
            ZoneOrTierIdentifier = zoneOrTierIdentifier;
            HostIdentifier = hostIdentifier;
            TennantIdentifier = tennantIdentifier;
        }
    }
}