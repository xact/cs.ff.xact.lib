﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;
    using System.Security;
    using XAct;
    using global::XAct.Domain;


    /// <summary>
    /// The base class for a <see cref="Settings"/>.
    /// </summary>
    /// <remarks>
    /// <para>
    /// SettingBase extends <see cref="EditableValue{TValue}"/>
    /// parameter, adding
    /// <see cref="EditingMetadata"/>.<c>DefaultValue</c>
    /// and 
    /// <see cref="EditingMetadata"/>.<c>IsUnlocked</c>
    /// parameters
    /// </para>
    /// <para>
    /// See also <see cref="SettingCollection"/>
    /// </para>
    /// </remarks>
    /// <internal>
    /// NOTE: The reason for the separation between Setting and SettingBase
    /// is that SettingBase can be generic -- and potentially reused somewhere
    /// else in the future.
    /// </internal>
    /// <internal><para>8/13/2011: Sky</para></internal>
    [DataContract(Name = "Setting", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/profiles")]
    [ComVisible(false)]
    public class Setting : EditableValue<object>, ISetting<object>, IHasIdReadOnly<Guid>
    {

        /// <summary>
        /// Gets the settings unique identifier, if already set.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public virtual Guid Id { get { return _id; } }
        [DataMember(Name = "Id", IsRequired = true)]
        private Guid _id;




        /// <summary>
        /// Gets the setting's Name.
        /// </summary>
        /// <value>The name.</value>
        /// <internal><para>8/14/2011: Sky</para></internal>
        public virtual string Name
        {
            get { return _name; }
        }

        [DataMember(Name = "Name", IsRequired = true)] private string _name;



        /// <summary>
        /// Gets or sets the value.
        /// depending on values of 
        /// <see cref="EditingMetadata"/>.<c>IsReadAuthorized</c>, 
        /// <see cref="EditingMetadata"/>.<c>IsWriteAuthorized</c>, 
        /// and/or <see cref="EditingMetadata"/>.<c>IsUnlocked</c>.
        /// </summary>
        /// <internal>
        /// Override forces it to use this GetValue/SetValue so that it goes through checks.
        /// </internal>
        /// <value>The value.</value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public override object Value
        {
            get
            {
                var result = GetValue();
                return result;
            }
            set { SetValue(value); }
        }



        /// <summary>
        /// Gets an optional tag to help processors.
        /// </summary>
        /// <value>The Tag.</value>
        public virtual string Tag
        {
            get { return _tag; }
        }

        [DataMember(Name = "Tag", IsRequired = false)] private readonly string _tag;


        /// <summary>
        /// Gets an optional tag to help processors.
        /// </summary>
        /// <value>The Tag.</value>
        public virtual string Description
        {
            get { return _description; }
        }

        [DataMember(Name = "Description", IsRequired = false)]
        private readonly string _description;


        
        /// <summary>
        /// Gets an optional metadata to help processors.
        /// </summary>
        /// <value>The Tag.</value>
        public virtual string Metadata
        {
            get { return _metadata; }
        }

        [DataMember(Name = "Metadata", IsRequired = false)]
        private readonly string _metadata;

        /// <summary>
        /// Gets the setting's (optional) information 
        /// required to *update* the setting (IsLocked, IsWriteAuthorized, SerializationMethod, SerializedValue, etc).
        /// </summary>
        /// <value>
        /// The setting editing information.
        /// </value>
        public virtual SettingEditingMetadata EditingMetadata
        {
            get { return _editingMetadata; }
        }

        [DataMember(Name = "EditingInformation", IsRequired = false)] private readonly SettingEditingMetadata
            _editingMetadata;



        /// <summary>
        /// Initializes a new instance of the <see cref="Setting"/> class.
        /// </summary>
        /// <internal><para>8/16/2011: Sky</para></internal>
        // ReSharper disable UnusedMember.Local
        private Setting()
            : this(null, null, null)
            // ReSharper restore UnusedMember.Local
        {
            //A private constructionless constructor is required for WCF serialization   
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Setting"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="value">The value.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="description">The description.</param>
        /// <param name="metadata">The metadata.</param>
        /// <param name="editingMetadata">The editing metadata.</param>
        /// <param name="offlineModelState">State of the offline model.</param>
        public Setting(Guid id,
                       string name,
                       Type valueType,
                       object value,
                       string tag = null,
                       string description = null,
                       string metadata = null,
                       SettingEditingMetadata editingMetadata = null,
                       OfflineModelState offlineModelState = OfflineModelState.New
            ) : this(name, valueType, value, tag, description, metadata, editingMetadata, offlineModelState)
        {
            _id = id;
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="Setting" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="value">The value.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="description">The description.</param>
        /// <param name="metadata">The metadata.</param>
        /// <param name="editingMetadata">The setting editing information.</param>
        /// <param name="offlineModelState">State of the offline model.</param>
        public Setting( string name,
                       Type valueType,
                       object value,
                       string tag = null,
                       string description = null,
                        string metadata=null,
                       SettingEditingMetadata editingMetadata = null,
                       OfflineModelState offlineModelState = OfflineModelState.New
            )
        {
            //Can only be set if IHasId<Guid>, not IHasIdReadOnly<Guid>
            //this.GenerateDistributedId();
            _id = XAct.DependencyResolver.Current.GetInstance<IDistributedIdService>().NewGuid();


            name.ValidateIsNotNullOrEmpty("name");

            _name = name;


            // ReSharper disable RedundantBaseQualifier
            base.ValueType = valueType;
            // ReSharper restore RedundantBaseQualifier

            //Call base, to skip the check whether it is editable or not:
            //And it will set event...but nobody is listening yet...
            //And keep Modified state as UnChanged.
            base.SetValue(value);

            //Set it afterwards:
            this.ModelState = offlineModelState;

            _description = description;
            _tag = tag;
            _metadata = metadata;

            //Only after setting variable do we set up permission checking on variable:
            _editingMetadata = editingMetadata;

        }

        /// <summary>
        /// Sets the value if different to current value.
        /// <para>
        /// If <see cref="EditingMetadata" />.<c>IsUnlocked</c> = <c>False</c>,
        /// a <see cref="SecurityException" /> is raised.
        /// </para>
        /// <para>
        /// Invokes <see cref="EditableValue{TValue}.OnPropertyChanging(PropertyChangingEventArgs)" />
        /// and
        /// <see cref="EditableValue{TValue}.OnPropertyChanged(PropertyChangedEventArgs)" />
        /// </para>
        /// </summary>
        /// <param name="value">The value.</param>
        /// <internal>8/13/2011: Sky</internal>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.Security.SecurityException">Write Access is currently denied to {0}.FormatStringCurrentCulture(Name)</exception>
        protected override void SetValue(object value)
        {

            if (ModelState == OfflineModelState.Deleted)
            {
                throw new ArgumentException("Setting Value cannot be updated. Setting has been marked for deletion.");
            }


            if (_editingMetadata != null)
            {
                if (!_editingMetadata.IsUnlocked)
                {
                    throw new ArgumentException("Cannot set the Value: the LockState is not Unlocked.");
                }

                //Use property to reevaluate function:
                if (!_editingMetadata.IsWriteAuthorized)
                {
                    throw new SecurityException(
                        "Write Access is currently denied to {0}".FormatStringCurrentCulture(Name));
                }
            }

            base.SetValue(value);
        }




        /// <summary>
        /// Gets the Setting's Typed value.
        /// </summary>
        /// <returns></returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        protected override object GetValue()
        {
            //Use property to reevaluate function:
            if (_editingMetadata != null)
            {
                if (!_editingMetadata.IsReadAuthorized)
                {
                    throw new SecurityException(
                        "Read Access is currently denied to {0}".FormatStringCurrentCulture(Name));
                }
            }
            return base.GetValue();
        }



        /// <summary>
        /// Resets the Current Value to the <see cref="Setting"/>'s <see cref="EditingMetadata"/>.<c>DefaultValue</c>.
        /// </summary>
        /// <returns></returns>
        /// <internal><para>8/14/2011: Sky</para></internal>
        public virtual void ResetValue()
        {

            //Ensure we are not editing (reset Editing flag, and sets Prev = Current)
            EndEdit();

            if (_editingMetadata == null)
            {
                return;
            }
            //Then Set Current Value.
            //Although should not be able to roll back to 
            //default value if locked,
            //so pass it through this instance's override.
            SetValue(_editingMetadata.DefaultValue);
            //The base layer will raise events that the UI
            //can respond to and uTpdate itself from.

            //Can't think of a good reason anybody needs to know
            //before or after what the default value is.
            //return _defaultValue;
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            ModelState = OfflineModelState.Deleted;
        }




        /// <summary>
        /// Internals Helper.
        /// If the Setting name contains a Module name, removes Module name from Setting Name.
        /// </summary>
        public void InternalRemoveModuleFromName()
        {
            int pos = _name.IndexOf(":", 0, System.StringComparison.Ordinal);

            if (pos == -1)
            {
                return;
            }

            _name = _name.Substring(pos + 1);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{0}='{1}' [{2}]".FormatStringInvariantCulture(this.Name, this.Value, this.ValueType.Name);
            //return base.ToString();
        }



    }
}