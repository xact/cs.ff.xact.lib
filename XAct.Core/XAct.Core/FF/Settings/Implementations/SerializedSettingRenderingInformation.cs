﻿namespace XAct.Settings
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    public class SerializedSettingRenderingInformation : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasKey, IHasSettingRenderingHints 
    {
        /*
         * IHasRenderingHintsIdentifier, 
         */


        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }

        /// <summary>
        /// Gets the Identifier to a Record describing how to render the
        /// <c>Setting</c> or other similar entity.
        /// <para>
        /// IMPORTANT: Usually one sets *either* the RenderHintsIdentifier 
        /// *OR* the Group/Order/Label/ViewControl/EditControl/EditValidation
        /// values -- not both.
        /// </para>
        /// </summary>
        /// <value>
        /// The render hints identifier.
        /// </value>
        [DataMember]
        public virtual string RenderHintsIdentifier { get; set; }



        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to group Settings together.
        /// </summary>
        [DataMember]
        public virtual string RenderGroupingHints { get; set; }




        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to order the Settings within a Group.
        /// </summary>
        [DataMember]
        public virtual int RenderOrderHint { get; set; }




        /// <summary>
        /// Gets or sets the rendering image hints.
        /// <para>
        /// Images could be for controls tucked away, or simply rendered beside/instead of labels.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string RenderingImageHints { get; set; }



        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to retrieve a Resource
        /// for a Label.
        /// <para>
        /// In many cases it will be just the Resource Id.
        /// </para>
        /// <para>
        /// But it could be a richer system such as
        /// <code>
        /// label:RES_L123;Hint:RES_H123
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// The render label hints.
        /// </value>
        [DataMember]
        public virtual string RenderLabelHints { get; set; }



        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to choose an appropriate platform
        /// specific control.
        /// <para>
        /// Examples are:
        /// <code>
        /// <![CDATA[
        /// * Bool
        /// * Int
        /// * Text (or String)
        /// * Date
        /// * Time
        /// * DateTime
        /// * CustomXYZ
        /// ]]>
        /// </code>
        /// or more verbosely:
        /// <code>
        /// <![CDATA[
        /// * Type:Bool
        /// * Type:Int
        /// * Type:Text;MaxLength:40;
        /// * Type:Date
        /// * Type:Time
        /// * Type:DateTime
        /// * Type:CustomXYZ
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// A CSS formatted string.
        /// </value>
        [DataMember]
        public virtual string RenderViewControlHints { get; set; }


        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to choose an appropriate platform
        /// specific control.
        /// <para>
        /// Examples are:
        /// <code>
        /// <![CDATA[
        /// * Bool
        /// * Int
        /// * Text (or String)
        /// * Date
        /// * Time
        /// * DateTime
        /// * CustomXYZ
        /// ]]>
        /// </code>
        /// or more verbosely:
        /// <code>
        /// <![CDATA[
        /// * Type:Bool
        /// * Type:Int
        /// * Type:Text;MaxLength:40;
        /// * Type:Date
        /// * Type:Time
        /// * Type:DateTime
        /// * Type:CustomXYZ
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <value>
        /// A CSS formatted string.
        /// </value>
        [DataMember]
        public virtual string RenderEditControlHints { get; set; }


        /// <summary>
        /// Gets or sets hints that can be used by a 
        /// View rendering mechanism to choose an appropriate platform
        /// specific validation strategy.
        /// </summary>
        /// <value>
        /// A CSS formatted string.
        /// </value>
        [DataMember]
        public virtual string RenderEditValidationHints { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="SerializedSettingRenderingInformation"/> class.
        /// </summary>
        public SerializedSettingRenderingInformation()
        {
            this.GenerateDistributedId();
        }
    }
}