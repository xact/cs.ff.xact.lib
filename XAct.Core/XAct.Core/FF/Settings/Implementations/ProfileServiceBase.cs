﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;
    using XAct.Diagnostics;
    using XAct.State;


    /// <summary>
    /// An abstract implementation of <see cref="IProfileSettingsService"/>.
    /// </summary>
    /// <internal><para>8/16/2011: Sky</para></internal>
    public abstract class ProfileServiceBase : IProfileSettingsService
    {
        private readonly string _cacheKey;
        /// <summary>
        /// The name of the current Type (used for logging purposes).
        /// </summary>
// ReSharper disable InconsistentNaming
        protected readonly string _typeName;
// ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets the tracing service used by this service.
        /// </summary>
        /// <value>The tracing service.</value>
        protected ITracingService TracingService { get { return _tracingService; } }
        private readonly ITracingService _tracingService;


        /// <summary>
        /// Gets the context state service.
        /// </summary>
        /// <value>
        /// The context state service.
        /// </value>
        protected IContextStateService ContextStateService { get { return _contextStateService; } }
        private readonly IContextStateService _contextStateService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileServiceBase"/> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <param name="contextStateService"> </param>
        /// <internal><para>8/16/2011: Sky</para></internal>
        protected ProfileServiceBase(ITracingService tracingService, IContextStateService contextStateService)
        {
            _typeName = this.GetType().Name;
            _cacheKey = _typeName;
            _tracingService = tracingService;
            _contextStateService = contextStateService;
            _cacheKey = _typeName;
        }


        /// <summary>
        /// Saves the current IIdentity's settings.
        /// </summary>
        /// <internal><para>8/16/2011: Sky</para></internal>
        public abstract void Persist();



        /// <summary>
        /// Retrieves the <see cref="Settings" /> (or a subclass of it)
        /// of the specified <see cref="Type" />.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public Settings Current
        {
            get
            {
                Settings result = _contextStateService.Items[_cacheKey] as Settings;
                if (result != null)
                {
                    return result;
                }
                throw new Exception();
            }
        }


        /// <summary>
        /// Gets the current IIdentity's settings.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// // Retrieve default settings:
        /// Settings profile = _profileSettings.Current();
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <returns></returns>
        /// <internal>8/16/2011: Sky</internal>
        public TSettings GetCurrentSettings<TSettings>()
            where TSettings : Settings
        {
            TSettings result = _contextStateService.Items[_cacheKey] as TSettings;

            if (result != null)
            {
                return result;
            }

            _contextStateService.Items[_cacheKey] = result = this.InternalRetrieve<TSettings>();

            return result;
  
        }

        /// <summary>
        /// Internals the retrieve.
        /// </summary>
        /// <typeparam name="TSettings">The type of the settings.</typeparam>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        protected abstract TSettings InternalRetrieve<TSettings>(string context = null);




        /// <summary>
        /// Gets the ids of modules in app that user is allowed to use.
        /// </summary>
        /// <typeparam name="TIdType">The type of the id type.</typeparam>
        /// <returns></returns>
        /// <internal><para>8/16/2011: Sky</para></internal>
        /// <internal>
        /// In a multi-enterprise portal system, there could be hundreds 
        /// of 3rdPartyModules - hopefully thousands.
        /// <para>
        /// In a single enterprise, there might even by many tens of modules
        /// that have been purchased for the app. 
        /// </para>
        /// <para>
        ///  Hence, we dont' want to recurse through settings for each and 
        /// every module - we just want the Modules loaded into 
        /// this application instance, that the user has access rights to
        /// with the current Roles the user is a member of.
        /// </para>
        /// </internal>
        protected abstract TIdType[] GetIdsOfOnlyTheModulesRegisteredForThisApp<TIdType>();


        /// <summary>
        /// Gets the default settings.
        /// </summary>
        /// <param name="moduleId">The module id.</param>
        /// <returns></returns>
        /// <internal><para>8/16/2011: Sky</para></internal>
        protected abstract ModuleSettingGroup GetDefaultSettings<TIdType>(TIdType moduleId);



    }
}