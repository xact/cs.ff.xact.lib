﻿namespace XAct.Settings
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ApplicationSettingAudit:
        IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp,
        IHasApplicationTennantId,
        IHasKey,
        IHasSerializedTypeValueAndMethod,
        IHasDateTimeCreatedBy,
        IHasTag
        
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the organisation id of the record
        /// within a multi-tenant application.
        /// <para>
        /// Design tip: it is preferable to allow users to register only online,
        /// but if the app has to allow the creation of new users/tenants offline,
        /// investigate <see cref="IDistributedIdService" />
        /// </para>
        /// </summary>
        [DataMember]
        public virtual Guid ApplicationTennantId { get; set; }





        /// <summary>
        /// Gets or sets the key.
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }

        /// <summary>
        /// Gets or sets the FK to the <see cref="SerializedApplicationSetting"/>
        /// that this Audit is tracking.
        /// </summary>
        /// <value>
        /// The setting fk.
        /// </value>
        [DataMember]
        public virtual Guid SettingFK { get; set; }

        /// <summary>
        /// Gets or sets the serialization method.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The serialization method.
        /// </value>
        [DataMember]
        public virtual SerializationMethod SerializationMethod { get; set; }

        /// <summary>
        /// Gets or sets the Assembly qualified name of the Value that is serialized.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValueType { get; set; }

        /// <summary>
        /// Gets or sets the serialized value.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValue { get; set; }

        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        ///   <internal>
        /// As to why its Nullable: sometimes the contract is applied to items
        /// that are not Entities themselves, but pointers to objects that are not known
        /// if they are
        ///   </internal>
        ///   <internal>
        /// The value is Nullable due to SQL Server.
        /// There are times where one needs to create an Entity, before knowing the Create
        /// date. In such cases, it is *NOT* appropriate to set it to UtcNow, nor DateTime.Empty,
        /// as SQL Server cannot store dates prior to Gregorian calendar.
        ///   </internal>
        [DataMember]
        public virtual DateTime? CreatedOnUtc { get; set; }



        /// <summary>
        /// Gets or sets the who created the record.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy" /></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public virtual string CreatedBy { get; set; }





        /// <summary>
        /// Gets or sets the who created the record.
        /// <para>Member defined in<see cref="IHasDateTimeCreatedBy" /></para>
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        [DataMember]
        public virtual string CreatedByOrganisation { get; set; }


        /// <summary>
        /// Gets or sets the serialized previous value.
        /// </summary>
        [DataMember]
        public virtual string SerializedPreviousValue { get; set; }


        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Tag { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettingAudit"/> class.
        /// </summary>
        public ApplicationSettingAudit()
        {
            this.GenerateDistributedId();
        }
    }
}