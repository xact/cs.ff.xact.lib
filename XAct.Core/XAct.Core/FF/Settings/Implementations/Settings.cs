﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;
    using System.Text.RegularExpressions;
    using XAct.Domain;

//http://odetocode.com/Articles/440.aspx


    /// <summary>
    /// A serializeable package of partitioned Settings, 
    /// appropriate for an enterprise application that allows 
    /// pluggable 3rd parties to employ settings as well.
    /// </summary>
    /// <remarks>
    /// <para>
    /// A <see cref="Settings"/> can have Settings directly embedded in it.
    /// <code>
    /// <![CDATA[
    /// Settings profile = new Settings();
    /// profile.AddSetting("KeyA",typeof(int),3);
    /// ...
    /// int result1 = profile.GetSetting("KeyA").GetValue<int>();
    /// int result2 = profile.GetValue<int>("KeyA");
    /// ]]>
    /// </code>
    /// In addition, to allow tidy compartimentalization, 
    /// a <see cref="Settings"/>, can have one or more nested 
    /// <see cref="SettingGroup"/>, that in turn can have more settings:
    /// <code>
    /// <![CDATA[
    /// Settings profile = new Settings();
    /// profile["SubGroupA"]["SubGroupB"].AddSetting("KeyA",typeof(int),3);
    /// ...
    /// int result1 = profile["SubGroupA"]["SubGroupA1"].GetSetting("KeyA").GetValue<int>();
    /// int result2 = profile["SubGroupA"]["SubGroupA1"].GetValue<int>("KeyA");
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// In addition, a Settings can reference a collection of Module Settings.
    /// Note that each Module manages it properties separately.
    /// <code>
    /// <![CDATA[
    /// Settings profile = new Settings();
    /// profile.GetModule("A").["SubGroupA"].AddSetting("KeyA",typeof(int),3);
    /// ...
    /// int result1 = profile.GetModule("A").["SubGroupA"].GetSetting("KeyA").GetValue<int>();
    /// int result2 = profile.GetModule("A").["SubGroupA"].GetValue<int>("KeyA");
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// A Settings is serializable.
    /// </para>
    /// </remarks>
    /// <internal>
    /// <para>
    /// A Settings is a ModuleSettingsGroup, which is a SettingsGroup.
    /// </para>
    /// </internal>
    /// <internal><para>8/15/2011: Sky</para></internal>
    [DataContract(Name = "Settings", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/settings")]
    public class Settings :  ModuleSettingGroup,  ISettings
    {


        /// <summary>
        /// Gets the module settings group collection.
        /// </summary>
        /// <value>
        /// The module settings group collection.
        /// </value>
        public ModuleSettingGroupCollection Modules
        {
            get { return _moduleSettingsGroupCollection; }
        }
        [DataMember(Name = "Modules", IsRequired = true)] 
        private ModuleSettingGroupCollection _moduleSettingsGroupCollection
                = new ModuleSettingGroupCollection();



        /// <summary>
        /// Gets the specified module (usually a Vendor or similar set of variables) .
        /// <para>
        /// If found, returns it.
        /// </para>
        /// <para>
        /// If not found, 
        /// dynamically adds the required Module before return it.
        /// </para>
        /// </summary>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="hasAccess">The otional hasAccess func/logic (only required during initialization stage).</param>
        /// <returns></returns>
        /// <internal><para>8/17/2011: Sky</para></internal>
        public ModuleSettingGroup GetModule(string moduleName, Func<bool> hasAccess = null)
        {
            ModuleSettingGroup moduleSettingsGroup;
            if (_moduleSettingsGroupCollection.TryGet(moduleName, out moduleSettingsGroup))
            {
                return moduleSettingsGroup;
            }

            moduleSettingsGroup = new ModuleSettingGroup(moduleName, hasAccess);

            _moduleSettingsGroupCollection.Add(moduleSettingsGroup);

            return moduleSettingsGroup;
        }


        /// <summary>
        /// Gets the Value property of the specified *existing* (already Added) <see cref="Setting" /> value.
        /// <para>
        /// Raises an Exception if the Setting is not found.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the setting.</param>
        /// <param name="defaultValueIfNotFound">The default value if not found.</param>
        /// <returns></returns>
        /// <internal>8/13/2011: Sky</internal>
        public override TValue TryGetSettingValue<TValue>(string settingName, TValue defaultValueIfNotFound)
        {
            TValue result;
            TryGetSettingValue(settingName, out result, defaultValueIfNotFound);
            return result;
        }



        /// <summary>
        /// Gets the Value property of the specified *existing* (already Added) <see cref="Setting" /> value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the setting.</param>
        /// <param name="value">The value.</param>
        /// <param name="defaultValueIfNotFound">The default value if not found.</param>
        /// <returns></returns>
        /// <internal>8/13/2011: Sky</internal>
        public override bool TryGetSettingValue<TValue>(string settingName, out TValue value, TValue defaultValueIfNotFound)
        {
            string moduleName;
            string variablePath;
            Type valueType2;

            this.ExtractModuleAndPath(settingName, out moduleName, out variablePath, out valueType2);

            //ModuleSettingGroup moduleSettingGroup = (moduleName.IsNullOrEmpty()) ? base : GetModule(moduleName);
            //return moduleSettingGroup.TryGetSettingValue<TValue>(settingName, out value, defaultValueIfNotFound);

            if (moduleName.IsNullOrEmpty())
            {
                // ReSharper disable RedundantArgumentDefaultValue
                //FIX: Calling BASE (or get stack overflow)
// ReSharper disable RedundantTypeArgumentsOfMethod
                return base.TryGetSettingValue<TValue>(settingName, out value, defaultValueIfNotFound);
// ReSharper restore RedundantTypeArgumentsOfMethod
                // ReSharper restore RedundantArgumentDefaultValue
            }

            //name = RemoveModuleFromName(name);
// ReSharper disable RedundantTypeArgumentsOfMethod
            var result = this.GetModule(moduleName).TryGetSettingValue<TValue>(variablePath, out value, defaultValueIfNotFound);
// ReSharper restore RedundantTypeArgumentsOfMethod
            return result;
        }



        /// <summary>
        /// Tries to get the specified Setting.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        public override bool TryGetSetting(string key, out Setting setting)
        {
            string moduleName;
            string variablePath;
            Type valueType2;

            this.ExtractModuleAndPath(key, out moduleName, out variablePath, out valueType2);

            if (moduleName.IsNullOrEmpty())
            {
                // ReSharper disable RedundantArgumentDefaultValue
                //FIX: Calling BASE (or get stack overflow)
                return base.TryGetSetting(key, out setting);
                // ReSharper restore RedundantArgumentDefaultValue
            }

            var result = this.GetModule(moduleName).TryGetSetting(variablePath, out setting);
            return result;
        }


        /// <summary>
        /// Adds the setting.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="settingEditingInformation">The setting editing information.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="description">The description.</param>
        /// <param name="metadata">The metadata.</param>
        /// <param name="throwExceptionIfAlreadyAdded">if set to <c>true</c> [throw exception if already added].</param>
        /// <param name="offlineModelState">State of the offline model.</param>
        public override void AddSetting<TValue>(
            string name, 
            object value, 
            SettingEditingMetadata settingEditingInformation = null, 
            string tag = null, 
            string description=null, 
            string metadata=null,
            bool throwExceptionIfAlreadyAdded = true,
            OfflineModelState offlineModelState = OfflineModelState.New)
        {
            string moduleName;
            string variablePath;
            //Discovered value (not used in this context)
            Type valueType2;

            this.ExtractModuleAndPath(name, out moduleName, out variablePath, out valueType2);

            if (moduleName.IsNullOrEmpty())
            {
                // ReSharper disable RedundantArgumentDefaultValue
                //FIX: Calling BASE (or get stack overflow)

                base.AddSetting<TValue>(name,value,settingEditingInformation,tag,description, metadata, throwExceptionIfAlreadyAdded,offlineModelState);
                // ReSharper restore RedundantArgumentDefaultValue
                return;
            }


            this.GetModule(moduleName)
                .AddSetting<TValue>(variablePath, value, settingEditingInformation, tag, description, metadata, throwExceptionIfAlreadyAdded, offlineModelState);
        }

        /// <summary>
        /// Adds the setting.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="value">The value.</param>
        /// <param name="settingEditingInformation">The setting editing information.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="description">The description.</param>
        /// <param name="metadata">The metadata.</param>
        /// <param name="throwExceptionIfAlreadyAdded">if set to <c>true</c> [throw exception if already added].</param>
        /// <param name="offlineModelState">State of the offline model.</param>
        public override void AddSetting(string name, Type valueType, object value, 
            SettingEditingMetadata settingEditingInformation = null, 
            string tag = null, 
            string description=null,
            string metadata=null,
            bool throwExceptionIfAlreadyAdded = true,
            OfflineModelState offlineModelState = OfflineModelState.New)
        {
            string moduleName;
            string variablePath;

            //Discovered value (not used in this context)
            Type valueType2;

            this.ExtractModuleAndPath(name, out moduleName, out variablePath, out valueType2);

            if (moduleName.IsNullOrEmpty())
            {
                // ReSharper disable RedundantArgumentDefaultValue
                //FIX: Calling BASE (or get stack overflow)

                base.AddSetting(name, valueType, value, settingEditingInformation, tag,description, metadata, throwExceptionIfAlreadyAdded,offlineModelState);
                // ReSharper restore RedundantArgumentDefaultValue
                return;
            }


            // ReSharper disable RedundantArgumentDefaultValue
            this.GetModule(moduleName)
                .AddSetting(variablePath, valueType, value, settingEditingInformation, tag,description, metadata, throwExceptionIfAlreadyAdded,offlineModelState);

        }




        /// <summary>
        /// INTERNAL USE ONLY: Adds the setting.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <param name="throwExceptionIfAlreadyAdded">if set to <c>true</c> [throw exception if already added].</param>
        public override void AddSetting(Setting setting, bool throwExceptionIfAlreadyAdded = true)
        {
            string moduleName;
            string variablePath;

            //Discovered value (not used in this context)
            Type valueType2;

            this.ExtractModuleAndPath(setting.Name, out moduleName, out variablePath, out valueType2);

            if (moduleName.IsNullOrEmpty())
            {
                // ReSharper disable RedundantArgumentDefaultValue
                //FIX: Calling BASE (or get stack overflow)

                base.AddSetting(setting, throwExceptionIfAlreadyAdded);
                // ReSharper restore RedundantArgumentDefaultValue
                return;
            }

            //can't use 'variablePath' as already set:
            setting.InternalRemoveModuleFromName();

            // ReSharper disable RedundantArgumentDefaultValue
            this.GetModule(moduleName)
                .AddSetting(setting, throwExceptionIfAlreadyAdded);
            
        }


        /// <summary>
        /// Gets the value of the specified property value.
        /// <para>
        /// Throws an exception if the property is not available.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the property.</param>
        /// <returns></returns>
        /// <internal>8/13/2011: Sky</internal>
        ///   <internal>8/13/2011: Sky</internal>
        public override TValue GetSettingValue<TValue>(string settingName)
        {
            string moduleName;
            string variablePath;

            //Discovered value (not used in this context)
            Type valueType2;

            this.ExtractModuleAndPath(settingName, out moduleName, out variablePath, out valueType2);

            if (moduleName.IsNullOrEmpty())
            {
                // ReSharper disable RedundantArgumentDefaultValue
                //FIX: Calling BASE (or get stack overflow)
               return base
                .GetSettingValue<TValue>(variablePath);
                // ReSharper restore RedundantArgumentDefaultValue
            }

            // ReSharper disable RedundantArgumentDefaultValue
            var result = this.GetModule(moduleName)
                .GetSettingValue<TValue>(variablePath);
            return result;
        }







        [OnSerializing]
        private void OnSerializing(StreamingContext streamingContext)
        {
        }


        [OnDeserializing]
        private void OnDeserializing(StreamingContext streamingContext)
        {
            //Since we are not passing through constructor, 
            //we create now:
            if (_moduleSettingsGroupCollection == null)
            {
                _moduleSettingsGroupCollection = new ModuleSettingGroupCollection();
            }
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext streamingContext)
        {
        }






        /// <summary>
        /// Helper method to parse a given Setting Path into a module name if it exists, and the subpath in the module.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="variablePath">The variable path.</param>
        /// <param name="type">The type.</param>
        public void ExtractModuleAndPath(string key, out string moduleName, out string variablePath, out Type type)
        {
            //int pos = key.IndexOf(']');
            //if (pos > -1)
            //{
            //    string typeName = System.Text.RegularExpressions.Regex.Match(key, "\\[(.*)\\]").Groups[1].Captures[0].Value.Trim();

            //    type = Type.GetType(typeName);

            //    key = key.Replace("["+typeName+"]",string.Empty).Trim();//.Substring(pos + 1);
            //}

            Match match = System.Text.RegularExpressions.Regex.Match(key, "\\[(.*)\\]");

            if (match.Success)
            {
                string typeName = match.Groups[1].Captures[0].Value.Trim();
                type = Type.GetType(typeName);
            }
            else
            {
                type = typeof(string);
            }

            int pos = key.IndexOf(ModuleSeparatorCharacter, System.StringComparison.Ordinal);

            if (pos > -1)
            {
                moduleName = key.Substring(0, pos);
                variablePath = key.Substring(pos + 1);
            }
            else
            {
                moduleName = null;
                variablePath = key;
            }

        }



        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{0} settings, {1} Modules.".FormatStringInvariantCulture(this._settingsCollection.Count, this._moduleSettingsGroupCollection.Count);
            //return base.ToString();
        }



    }
}