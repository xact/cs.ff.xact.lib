﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Security;


    /// <summary>
    /// Information beyond a <see cref="Setting"/>'s Value,
    /// required for Editing the setting 
    /// (IsLocked, IsWriteAuthorized, Source Record (the <see cref="SerializedApplicationSetting"/>)
    /// </summary>
    [DataContract(Name = "SettingEditingMetadata", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/profiles")]
    public class SettingEditingMetadata : 
        IHasReadWriteAuthorization
    {

        /// <summary>
        /// Gets or sets the scope.
        /// </summary>
        /// <value>
        /// The scope.
        /// </value>
        public int Scope
        {
            get { return _scope; }
            set { _scope = value; }
        }
        [DataMember(Name = "Scope", IsRequired = false)]
        private int _scope;



        ///// <summary>
        ///// Gets or sets the setting serialization context.
        ///// </summary>
        ///// <value>
        ///// The setting serialization context.
        ///// </value>
        //FUBAR:SKY
        //public ApplicationSettingScope SettingContextIdentifier
        //{ 
        //    get { return _settingSerializationContext1; }
        //    set { _settingSerializationContext1 = value; }
        //}
        //private ApplicationSettingScope _settingSerializationContext1;

        //[DataMember] private ApplicationSettingScope _settingContextIdentifier;
        

        ///// <summary>
        ///// Gets or sets the <see cref="Setting"/>'s Fully Qualified Name (Module + Groups + Key).
        ///// <para>Member defined in<see cref="IHasKey" /></para>
        ///// </summary>
        ///// <value>
        ///// The key.
        ///// </value>
        //[DataMember]
        //public virtual string Key { get; set; }

        
        

        /// <summary>
        /// Gets a value indicating whether this setting is readable with the current Role membership.
        /// <para>Implementation of IHasReadWriteAccess</para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is readable; otherwise, <c>false</c>.
        /// </value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public bool IsReadAuthorized
        {
            get
            {
                _isReadAuthorized = _isReadAuthorizedFunc();
                return _isReadAuthorized;
            }
        }
        [DataMember(Name = "IsReadAuthorized", IsRequired = false, EmitDefaultValue = false)]
        private bool _isReadAuthorized;
        private Func<bool> _isReadAuthorizedFunc;

        /// <summary>
        /// Gets a value indicating whether this setting is writable with the current Role membership.
        /// <para>Implementation of IHasReadWriteAccess</para>
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is writable; otherwise, <c>false</c>.
        /// </value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public bool IsWriteAuthorized
        {
            get
            {
                _isWriteAuthorized = _isWriteAuthorizedFunc();
                return _isWriteAuthorized;
            }
        }
        [DataMember(Name = "IsWriteAuthorized", IsRequired = false, EmitDefaultValue = false)]
        private bool _isWriteAuthorized;
        private Func<bool> _isWriteAuthorizedFunc;

        /// <summary>
        /// Gets or sets whether the value can be set or not (or has been locked).
        /// <para>
        /// A <see cref="Setting"/> can be <see cref="IHasReadWriteAuthorization.IsWriteAuthorized"/>  -- but in a Hierarchical scenario, 
        /// a prent <see cref="Setting"/> can have Locked Descendents from editing
        /// the value.
        /// </para>.
        /// </summary>
        /// <value>The state of the lock.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public bool IsUnlocked
        {
            get
            {
                _isUnlocked = _isUnlockedFunc();
                return _isUnlocked;
            }
        }
        [DataMember(Name = "IsUnlocked", IsRequired = false, EmitDefaultValue = false)]
        private bool _isUnlocked;
        private Func<bool> _isUnlockedFunc;


        /// <summary>
        /// Gets the default value to which to set the value if <see cref="Setting"/>.<c>ResetValue()</c> is invoked.
        /// </summary>
        /// <value>The default value.</value>
        /// <internal><para>8/14/2011: Sky</para></internal>
        public object DefaultValue
        {
            get { return _defaultValue; }
        }
        /// <summary>
        /// The default value. 
        /// </summary>
        /// <internal>
        /// The Generic value will be serialized as an Object.
        /// </internal>
        [DataMember(Name = "DefaultValue", IsRequired = false, EmitDefaultValue = false)] // ReSharper disable InconsistentNaming
        protected object _defaultValue;



        /// <summary>
        /// Initializes a new instance of the <see cref="SettingEditingMetadata"/> class.
        /// </summary>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="isUnlockedFunc">The is unlocked function.</param>
        /// <param name="isReadAuthorizedFunc">The is read authorized function.</param>
        /// <param name="isWriteAuthorizedFunc">The is write authorized function.</param>
        public SettingEditingMetadata(object defaultValue,
                                         Func<bool> isUnlockedFunc = null,
                                         Func<bool> isReadAuthorizedFunc = null,
                                         Func<bool> isWriteAuthorizedFunc = null
            )
        {

            _defaultValue = defaultValue;
            

            //IMPORTANT: AFTER Setting values...

            //Now set functions as to whether editable or not:
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InitializeAuthorizationFuncs(isUnlockedFunc, isReadAuthorizedFunc, isWriteAuthorizedFunc);
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }




        /// <summary>
        /// Setups the funcs.
        /// </summary>
        /// <param name="isUnlocked">The is editable.</param>
        /// <param name="isReadAuthorized">The has read role access.</param>
        /// <param name="isWriteAuthorized">The has write role access.</param>
        /// <internal><para>8/16/2011: Sky</para></internal>
        protected virtual void InitializeAuthorizationFuncs(Func<bool> isUnlocked, Func<bool> isReadAuthorized,
                                          Func<bool> isWriteAuthorized)
        {
            _isUnlockedFunc = isUnlocked ?? (() => true);
            _isReadAuthorizedFunc = isReadAuthorized ?? (() => true);
            _isWriteAuthorizedFunc = isWriteAuthorized ?? (() => true);
        }


        /// <summary>
        /// Initializes the funcs.
        /// </summary>
        /// <param name="streamingContext">The streaming context.</param>
        /// <internal>
        /// Can't do much about serializing the methods across
        /// the wire, but as it is being serialized,
        /// can freeze the setting as is:
        /// </internal>
        [OnSerializing]
        // ReSharper disable UnusedMember.Local
        // ReSharper disable UnusedParameter.Local
        private void SerializeAuthenticationFuncMethods(StreamingContext streamingContext)
            // ReSharper restore UnusedParameter.Local
            // ReSharper restore UnusedMember.Local
        {
            //We can't serialize the methods....
            //but we can serialize/freeze the result
            //of the method calls:

            //Evoke so that the values are filled when invoke by Serializer:

            // ReSharper disable RedundantAssignment
#pragma warning disable 219
            bool invoked = IsUnlocked;
#pragma warning restore 219
            invoked = IsReadAuthorized;
            invoked = IsWriteAuthorized;
            // ReSharper restore RedundantAssignment
        }



        [OnDeserializing]
        private void OnDeserializing(StreamingContext streamingContext)
        {

            //FUBAR:SKY
            //if (_settingContextIdentifier == null)
            //{
            //    _settingContextIdentifier = 
            //        new ApplicationSettingScope(null,null,null,Guid.Empty);
            //}

            //and on the client side, we can fill the function
            //with a very simple logic of a method that wraps the frozen
            //value.
            //It's not much of a method, but that could be supplied in other
            //ways by partial class, with another method marked OnDeserializing...

            //The null checks are in order to check that another partial class
            //applied clientside hasn't already filled the methods in.
            if (_isUnlockedFunc == null)
            {
                _isUnlockedFunc = () => _isUnlocked;
            }
            if (_isReadAuthorizedFunc == null)
            {
                _isReadAuthorizedFunc = () => _isReadAuthorized;
            }
            if (_isWriteAuthorizedFunc == null)
            {
                _isWriteAuthorizedFunc = () => _isWriteAuthorized;
            }
        }


    }
}