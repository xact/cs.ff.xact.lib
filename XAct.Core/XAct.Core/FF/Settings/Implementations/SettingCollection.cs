﻿namespace XAct.Settings
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;
    using XAct.Domain;

    /// <summary>
    /// A dictionary of <see cref="Setting"/> items.
    /// </summary>
    /// <internal>
    /// This is used to back the root <see cref="Settings"/> Settings dictionary,
    /// which optional nested <see cref="SettingGroup"/>s obfuscate.
    /// </internal>
    /// <internal><para>8/13/2011: Sky</para></internal>
    [CollectionDataContract(Name = "SettingCollection", ItemName = "Setting",
        Namespace = "http://xact-solutions.com/contracts/v0.1/xact/profiles")]
    [ComVisible(false)]
    public class SettingCollection : KeyedCollection<string, Setting>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingCollection"/> class.
        /// </summary>
        public SettingCollection()
            : base(StringComparer.OrdinalIgnoreCase)
        {
        }

        /// <summary>
        /// Gets the items in the collection that match the given <paramref name="modelState"/>.
        /// <para>
        /// Used to find the items in the collection that need persisting.
        /// </para>
        /// </summary>
        /// <param name="modelState">State of the model.</param>
        /// <returns></returns>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public IEnumerable<Setting> GetFilteredItems(OfflineModelState modelState)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i].ModelState == modelState)
                {
                    yield return this[i];
                }
            }
        }


        /// <summary>
        /// As the item is added to the collection, gets the key for item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <para>Member defined in<see cref="System.Collections.ObjectModel.KeyedCollection{TKey,TValue}"/></para>
        /// <returns></returns>
        /// <internal><para>8/14/2011: Sky</para></internal>
        protected override string GetKeyForItem(Setting item)
        {
            return item.Name;
        }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{0} Setting items.".FormatStringInvariantCulture(this.Items.Count);
            //return base.ToString();
        }
    }
}