﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.Serialization;
    using System.Text;
    using XAct.Domain;

    /// <summary>
    /// A Group of <see cref="Settings"/>
    /// nested under a <see cref="Settings"/>
    /// </summary>
    /// <internal>
    /// <para>
    /// SettingsGroup is an interesting piece of architecture because
    /// SettingsGroup is actually a fake. It doesn't really have a child 
    /// collection of Settings. It's just a mechanism to point back
    /// to the parent ModuleSettingsGroup (which is a SettingsGroup in its own right), 
    /// which is where the true Collection of objects
    /// is located.
    /// </para>
    /// <para>
    /// The reason for this architecture is that it is less iterative
    /// when loading/saving, so it can be used to pick up and save the variables
    /// with only one hit to the data persistence mechanism, rather than 
    /// several, one for each variable group.
    /// </para>
    /// </internal>
    [DataContract(Name = "SettingGroup", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/profiles")]
    public class SettingGroup : ISettingGroup, ISettingAccessor 
    {

        /// <summary>
        /// Occurs when a property value changes.
        /// <para>Defined in INotifyPropertyChanged</para>
        /// </summary>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Occurs when a property value is changing.
        /// <para>Defined in INotifyPropertyChanging</para>
        /// </summary>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public event PropertyChangingEventHandler PropertyChanging;


        /// <summary>
        /// The char used to separate parts of a Setting Name.
        /// <para>
        /// Value is '\\'
        /// </para>
        /// </summary>
        public const char SettingPathSeparator = '/';





        /// <summary>
        /// Direct parent 
        /// <see cref="SettingGroup"/>,
        /// <see cref="ModuleSettingGroup"/>, 
        /// or <see cref="Settings"/>.
        /// </summary>
        //[DataMember(Name = "ParentGroup")]
        protected SettingGroup ParentGroup;



        [DataMember(Name = "Name")] private string _name;


        [DataMember(Name = "NamePrefix")] private string _namePrefix;
        
        #region Properties 

        /// <summary>
        /// Gets the nested 
        /// <see cref="XAct.Settings.SettingGroup"/> 
        /// with the specified setting name.
        /// </summary>
        /// <value></value>
        /// <internal><para>8/17/2011: Sky</para></internal>
        public SettingGroup this[string settingGroupName]
        {
            get
            {
                SettingGroup settingsGroup;

                //Not happy about this -- ExtensionMethod is not as optimized
                //as I would have liked:
                if (_settingsGroupCollection.TryGet(settingGroupName, out settingsGroup))
                {
                    return settingsGroup;
                }

 
                //Ok to add:
                settingsGroup = new SettingGroup(this, settingGroupName);
                _settingsGroupCollection.Add(settingsGroup);

                //return:
                return settingsGroup;
            }
        }



        #endregion


        /// <summary>
        /// Initializes a new instance of the <see cref="SettingGroup"/> class.
        /// </summary>
        /// <internal><para>8/15/2011: Sky</para></internal>
        protected SettingGroup()
        {
            //A protected argumentless constructor is required for enheritence
            //by private argumentless constructor for WCF serialization


            _name = string.Empty;
            _namePrefix = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingGroup"/> class.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public SettingGroup(string groupName)
        {
            groupName.ValidateIsNotNullOrEmpty("groupName");

            if (groupName.IndexOf(SettingPathSeparator) > -1)
            {
                throw new ArgumentException("{0} cannot contain a '{1}'".FormatStringCurrentCulture("groupName",
                                                                                                    SettingPathSeparator));
            }

            //Save the name first so that it is available in next step:
            _name = groupName;

            //Build up a 
            BuildupNamePrefix(null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingGroup"/> class.
        /// </summary>
        /// <param name="parentProfileSettingsGroup">The parent profile settings group.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <internal><para>8/13/2011: Sky</para></internal>
        internal SettingGroup(SettingGroup parentProfileSettingsGroup, string groupName)
        {
            groupName.ValidateIsNotNullOrEmpty("groupName");
            if (groupName.IndexOf(SettingPathSeparator) > -1)
            {
                throw new ArgumentException("{0} cannot contain a '{1}'".FormatStringCurrentCulture("groupName",
                                                                                                    SettingPathSeparator));
            }

            //Save the name first so that it is available in next step:
            _name = groupName;

            _settingsCollection = ((IProfileSettingCollectionAccessor)parentProfileSettingsGroup).Settings;

            //Build up a 
            BuildupNamePrefix(parentProfileSettingsGroup);
        }


        #region Fields

        /// <summary>
        /// Gets the dictionary of registered core properties.
        /// </summary>
        /// <value>The properties.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        [DataMember(Name = "Settings", IsRequired = true)] 
// ReSharper disable InconsistentNaming
        protected SettingCollection _settingsCollection = new SettingCollection();
// ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets the dictionary of registered core properties.
        /// </summary>
        /// <value>The properties.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        SettingCollection IProfileSettingCollectionAccessor.Settings
        {
            get { return _settingsCollection; }    
        }

        [DataMember] 
        private SettingGroupCollection _settingsGroupCollection = new SettingGroupCollection();

        #endregion

        /// <summary>
        /// Gets or sets the name prefix.
        /// <para>
        /// Unlike <see cref="Name"/>, this does contain the 
        /// name of any parent groups.
        /// </para>
        /// <para>
        /// Note: ends with a FieldNameSeparator.
        /// </para>
        /// </summary>
        /// <value>The name prefix.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public string NamePrefix
        {
            get { return _namePrefix; }
        }


        #region INamed Members

        /// <summary>
        /// Gets the name of the <see cref="SettingGroup"/>.
        /// <para>
        /// The name is not suffixed with the names of any parent groups.
        /// </para>
        /// </summary>
        /// <value>The name.</value>
        /// <internal>8/13/2011: Sky</internal>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public string Name
        {
            get { return _name; }
        }

        #endregion


        #region ISettingAccessor Members

        /// <summary>
        /// Gets the specified <see cref="Setting" /> (from which one can get its <c>Value</c>).
        /// </summary>
        /// <param name="settingName">Name of the setting.</param>
        /// <returns>
        ///   <c>true</c> if the specified setting name contains setting; otherwise, <c>false</c>.
        /// </returns>
        /// <internal>8/17/2011: Sky</internal>
        ///   <internal>8/17/2011: Sky</internal>
        public bool ContainsSetting(string settingName)
        {
            string fullSettingName = BuildFullPropertyName(settingName,true);

            var result = this._settingsCollection.Contains(fullSettingName);
            return result;
        }


        /// <summary>
        /// Retrieves the specified *existing* (already Added) setting.
        /// </summary>
        /// <param name="settingName">Name of the setting.</param>
        /// <returns></returns>
        /// <internal>8/17/2011: Sky</internal>
        [DebuggerHidden]
        [DebuggerStepThrough]
        [DebuggerNonUserCode]
        public Setting RetrieveSetting(string settingName)
        {
            settingName.ValidateIsNotNullOrEmpty("settingName");
            string fullSettingName = BuildFullPropertyName(settingName,true);

            //In this case the name will remain the same:
            //same as just saying _settingsCollection
            return ((IProfileSettingCollectionAccessor) this).Settings[fullSettingName];
        }

        #endregion

        #region ISettingValueAccessor Members

        /// <summary>
        /// Gets the value of the specified property value.
        /// <para>
        /// Throws an exception if the property is not available.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the property.</param>
        /// <returns></returns>
        /// <internal>8/13/2011: Sky</internal>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public virtual TValue GetSettingValue<TValue>(string settingName)
        {
            Setting setting;
            try
            {
                setting = RetrieveSetting(settingName);
            }
            catch
            {
                setting = null;
            }
            if (setting != null)
            {
                return setting.Value.ConvertTo<TValue>();
            }
            throw new ArgumentException(
                "SettingsGroup (name:'{0}') does not contain a Setting named  '{1}'.".FormatStringCurrentCulture(
                    _namePrefix,
                    settingName));
        }


        /// <summary>
        /// Gets the Value property of the specified *existing* (already Added) <see cref="Setting" /> value.
        /// <para>
        /// Raises an Exception if the Setting is not found.
        /// </para>
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the setting.</param>
        /// <param name="defaultValueIfNotFound">The default value if not found.</param>
        /// <returns></returns>
        /// <internal>8/13/2011: Sky</internal>
        public virtual TValue TryGetSettingValue<TValue>(string settingName, TValue defaultValueIfNotFound)
        {
            TValue result;
            TryGetSettingValue(settingName, out result, defaultValueIfNotFound);
            return result;
        }



        /// <summary>
        /// Gets the Value property of the specified *existing* (already Added) <see cref="Setting" /> value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="settingName">Name of the setting.</param>
        /// <param name="value">The value.</param>
        /// <param name="defaultValueIfNotFound">The default value if not found.</param>
        /// <returns></returns>
        /// <internal>8/13/2011: Sky</internal>
        public virtual bool TryGetSettingValue<TValue>(string settingName, out TValue value, TValue defaultValueIfNotFound)
        {
            if (!this.ContainsSetting(settingName))
            {
                value = defaultValueIfNotFound;
                return false;
            }

            value = GetSettingValue<TValue>(settingName);
            return true;
        }

        /// <summary>
        /// Sets the Value property of the specified <see cref="Setting"/> value.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="settingName"></param>
        /// <param name="settingValue"></param>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public virtual void SetSettingValue<TValue>(string settingName, TValue settingValue)
        {
            Setting setting = RetrieveSetting(settingName);

            if (setting != null)
            {
                setting.Value = settingValue;
                return;
            }
            throw new ArgumentException(
                        "SettingsGroup (name:'{0}') does not contain a Setting named  '{1}'.".FormatStringCurrentCulture(
                        _namePrefix,
                            settingName));
        }

        #endregion

        #region Helpers


        /// <summary>
        /// Adds the setting.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="settingEditingMetadata">The setting editing information.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="description">The description.</param>
        /// <param name="metadata">The metadata.</param>
        /// <param name="throwExceptionIfAlreadyAdded">if set to <c>true</c> [throw exception if already added].</param>
        /// <param name="offlineModelState">State of the offline model.</param>
        /// <exception cref="System.ArgumentException">This SettingsGroup has been initialized: cannot add additional Settings.</exception>
                public virtual void AddSetting<TValue>(string name,
                                                       object value,
                                                       SettingEditingMetadata settingEditingMetadata = null,
                                                       string tag = null,
                                                        string description=null,
                                                        string metadata = null,
                                                       bool throwExceptionIfAlreadyAdded = true,
                                                       OfflineModelState offlineModelState = OfflineModelState.New
                                                       
                    )
                {
                    this.AddSetting(name,typeof(TValue),value,settingEditingMetadata,tag,description, metadata=null, throwExceptionIfAlreadyAdded, offlineModelState);
                }



                /// <summary>
                /// Adds the setting.
                /// </summary>
                /// <param name="name">The name.</param>
                /// <param name="valueType">Type of the value.</param>
                /// <param name="value">The value.</param>
                /// <param name="settingEditingMetadata">The setting editing information.</param>
                /// <param name="tag">The tag.</param>
                /// <param name="description">The description.</param>
                /// <param name="metadata">The metadata.</param>
                /// <param name="throwExceptionIfAlreadyAdded">if set to <c>true</c> [throw exception if already added].</param>
                /// <param name="offlineModelState">State of the offline model.</param>
                /// <exception cref="System.ArgumentException">This SettingsGroup has been initialized: cannot add additional Settings.</exception>
        public virtual void AddSetting(string name,
                                       Type valueType,
                                       object value,
                                       SettingEditingMetadata settingEditingMetadata = null,
                                       string tag = null,
                                        string description=null,
                                        string metadata=null,
                                       bool throwExceptionIfAlreadyAdded = true,
                                       OfflineModelState offlineModelState = OfflineModelState.New
            )
        {


            string fullSettignName = BuildFullPropertyName(name, true);

            //In this case the name will remain the same:
            Setting setting = new Setting(
                fullSettignName,
                valueType,
                value,
                tag,
                description,
                metadata,
                settingEditingMetadata,
                offlineModelState
                );


            AddSetting(setting, throwExceptionIfAlreadyAdded);
        }




        /// <summary>
        /// INTERNAL USE ONLY: Adds the setting.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <param name="throwExceptionIfAlreadyAdded">if set to <c>true</c> [throw exception if already added].</param>
        /// <exception cref="System.ArgumentException">Setting '{0}' already a member of collection..FormatStringCurrentUICulture(setting.Name)</exception>
        public virtual void AddSetting(Setting setting, bool throwExceptionIfAlreadyAdded = true)
        {
//Problem...we don't know if the nested SettingsGroups exist yet...
            //so have to check...

            SettingGroup settingsGroup = this;

            string[] nameParts = setting.Name.Split('/');

            for (int i = 0; i < nameParts.Length - 1; i++)
            {
                string settingGroupName = nameParts[i];
                settingsGroup = settingsGroup[settingGroupName];

                //That should have created all the nested elements...
            }

            //We also don't know if there is already a setting by this name.
            //If there is, we have to remove it, in order for the setting.Src
            //to be set to "HOST" rather than "APP" or whatever is the current scenario.
            Setting existingSetting;

            if (this._settingsCollection.TryGet(setting.Name, out existingSetting))
            {
                if (throwExceptionIfAlreadyAdded)
                {
                    throw new ArgumentException(
                        "Setting '{0}' already a member of collection.".FormatStringCurrentUICulture(setting.Name));
                }

                //Replace:
                existingSetting.PropertyChanging -= setting_PropertyChanging;
                existingSetting.PropertyChanged -= setting_PropertyChanged;

                _settingsCollection.Remove(existingSetting);
            }

            //Wire up the setting's events in order to know when it's value has changed.
            setting.PropertyChanging += setting_PropertyChanging;
            setting.PropertyChanged += setting_PropertyChanged;


            //Finally add it:
            ((IProfileSettingCollectionAccessor) this).Settings.Add(setting);
        }


        /// <summary>
        /// Tries to get the specified Setting.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        public virtual bool TryGetSetting(string key, out Setting setting)
        {
            //SettingGroup settingsGroup = this;
            
            //string[] nameParts = key.Split('/');
            //string name = nameParts[nameParts.Length-1];

            //for (int i = 0; i < nameParts.Length - 1; i++)
            //{
            //    string settingGroupName = nameParts[i];
            //    settingsGroup = settingsGroup[settingGroupName];

            //    //That should have created all the nested elements...
            //}
            string fullSettingName = BuildFullPropertyName(key, true);

            var result = this._settingsCollection.TryGet(fullSettingName, out setting);
            return result;
        }


        void setting_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            Setting setting = sender as Setting;
            OnPropertyChanging("{0}/{2}.{2}".FormatStringInvariantCulture( _namePrefix , setting.Name , e.PropertyName));
        }
        void setting_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Setting setting = sender as Setting;
            OnPropertyChanged("{0}/{2}.{2}".FormatStringInvariantCulture(_namePrefix, setting.Name, e.PropertyName));
        }

        private void OnPropertyChanging(string propertyName)
        {
            OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        private void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Raises the <see cref="PropertyChanging"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangingEventArgs"/> instance containing the event data.</param>
        /// <internal><para>8/13/2011: Sky</para></internal>
        protected virtual void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="PropertyChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="PropertyChangingEventArgs"/> instance containing the event data.</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }



        /// <summary>
        /// Buildups the name prefix by iterating through all parent 
        /// <see cref="SettingGroup"/>s.
        /// <para>
        /// End up with something like 'GroupA\SubGroup\SettingName'
        /// </para>
        /// </summary>
        /// <param name="parentProfileSettingsGroup">The parent profile settings group.</param>
        /// <internal><para>8/14/2011: Sky</para></internal>
        private void BuildupNamePrefix(SettingGroup parentProfileSettingsGroup)
        {
            StringBuilder stringBuilder = new StringBuilder();


            if (parentProfileSettingsGroup != null) // .ParentGroup != null
            {
                //If we are a ModuleSettingGroup - or the Profile subclass
                //then we don't want to tack on this name -- that's only 
                //there for Profile to find it in the Module Catalog.

                //As long as the parent is not the root:
                stringBuilder.Append(parentProfileSettingsGroup.NamePrefix);
                //Note: will already have a SettingsPathSeparator tacked on.

                stringBuilder.Append(Name);
                stringBuilder.Append(SettingPathSeparator);
            }


            _namePrefix = stringBuilder.ToString();
        }


        /// <summary>
        /// Takes the given name (without path separator)
        /// and prefixes it with the <see cref="NamePrefix"/>
        /// so that i can then be passed back to the Root Profile
        /// where the SettingsCollection is...
        /// <para>
        /// Builds the full name of the property.
        /// </para>
        /// </summary>
        /// <param name="settingName">Name of the property.</param>
        /// <param name="allowSeparator">if set to <c>true</c> [allow separator].</param>
        /// <returns></returns>
        /// <internal>8/14/2011: Sky</internal>
        private string BuildFullPropertyName(string settingName,bool allowSeparator=false)
        {
            settingName.ValidateIsNotNullOrEmpty("settingName");

            //allowSeparator = false;
            if (!allowSeparator&& (settingName.IndexOf(SettingPathSeparator) > -1))
            {
                //return settingName;
                throw new ArgumentException(
                    "PropertyName ('{0}') cannot contain a '{1}')".FormatStringCurrentCulture(settingName,
                                                                                              SettingPathSeparator));
            }
            return NamePrefix + settingName;
        }

        #endregion


/// <summary>
/// Returns an enumerator that iterates through the collection.
/// </summary>
/// <returns>
/// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
/// </returns>
        public IEnumerator<Setting> GetEnumerator()
        {
            string prefix = _namePrefix;

                foreach (Setting setting in  ((IProfileSettingCollectionAccessor) this).Settings)
                {
                    if (!setting.Name.StartsWith(prefix))
                    {
                        continue;
                    }
                    //if (setting.Name.IndexOf(SettingPathSeparator, prefix.Length + 1) > -1)
                    //{
                    //    //could filter out...
                    //}
                    yield return setting;
                }
        }

/// <summary>
/// Returns an enumerator that iterates through a collection.
/// </summary>
/// <returns>
/// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
/// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            string prefix = _namePrefix;

            foreach (Setting setting in ((IProfileSettingCollectionAccessor) this).Settings)
            {
                if (!setting.Name.StartsWith(prefix))
                {
                    continue;
                }
                //if (setting.Name.IndexOf(SettingPathSeparator, prefix.Length + 1) > -1)
                //{
                //    //could filter out...
                //}
                yield return setting;
            }
        }




        //[OnSerializing]
        //void OnSerializing(StreamingContext streamingContext)
        //{
        //}


        /// <summary>
        /// Called when deserializing.
        /// </summary>
        /// <param name="streamingContext">The streaming context.</param>
        /// <internal><para>8/17/2011: Sky</para></internal>
        [OnDeserializing]
        private void OnDeserializing(StreamingContext streamingContext)
        {
            //Check first -- don't want to crush it if it is back.
            if (_settingsCollection == null)
            {
                _settingsCollection = new SettingCollection();
            }
        }


        //[OnDeserialized]
        //void OnDeserialized(StreamingContext streamingContext)
        //{
        //}


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{0} items in top group.".FormatStringInvariantCulture(this._settingsCollection.Count);

            //return base.ToString();
        }

    }
}