﻿namespace XAct.Settings
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;

    /// <summary>
    /// A collection of <see cref="SettingGroup"/>
    /// </summary>
    /// <remarks>
    /// Each <see cref="SettingGroup"/>
    /// contains an array of optional nested <see cref="SettingGroup"/>
    /// </remarks>
    /// <internal><para>8/17/2011: Sky</para></internal>
    [ComVisible(false)]
    [CollectionDataContract(Name = "SettingGroupCollection",ItemName = "SettingGroup", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/profiles")]
    public class SettingGroupCollection : KeyedCollection<string, SettingGroup>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingGroupCollection"/> class.
        /// </summary>
        public SettingGroupCollection():base(StringComparer.OrdinalIgnoreCase)
        {
        }

        /// <summary>
        /// Gets the key for item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        protected override string GetKeyForItem(SettingGroup item)
        {
            return item.Name;
        }
    }
}