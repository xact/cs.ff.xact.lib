﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;


    /// <summary>
    /// Entity for storage of a <see cref="Setting" /> in a datastore (Db, AppSettings, etc).
    /// </summary>
    [DataContract(Name = "SerializedApplicationSetting", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/settings")]
    public class SerializedApplicationSetting :
        IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp,
        IHasKey, 
        IHasEnabled, 
        IHasSerializedTypeValueAndMethod,
        IApplicationSettingScope,
        IHasTag, 
        IHasDescription, 
        IHasDateTimeCreatedOnUtc, 
        IHasDateTimeModifiedOnUtc
    {

        /// <summary>
        /// Gets or sets the unique identifier of the setting.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the environment identifier
        /// (eg: DEV, ST, SIT, UAT, PP, PROD, etc.)
        /// <para>
        /// If Empty, applies to all Environments.
        /// </para>
        /// <para>
        /// Part of a unique Index applied to datastorage.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string EnvironmentIdentifier
        {
            get { return _environmentIdentifier ?? string.Empty; }
            set { _environmentIdentifier = value; }
        }
        private string _environmentIdentifier;

        


        /// <summary>
        /// Gets or sets the optional Application Tier for which this setting applies.
        /// <para>
        /// If Empty, applies to all Tiers -- ie, it is an Application setting.
        /// </para>
        /// <para>
        /// Part of a unique Index applied to datastorage.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string ZoneOrTierIdentifier
        {
            get { return _zoneOrTierIdentifier ?? string.Empty; }
            set { _zoneOrTierIdentifier = value; }
        }
        private string _zoneOrTierIdentifier;




        /// <summary>
        /// Gets or sets the optional name of the Host computer this setting is intended for.
        /// <para>
        /// Note that <see cref="ZoneOrTierIdentifier"/> is generally enough.
        /// </para>
        /// <para>
        /// If Empty, applies to all Hosts -- ie, it is a Tier setting.
        /// </para>
        /// <para>
        /// Part of a unique Index applied to datastorage.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string HostIdentifier 
        {
            get { return _hostIdentifier ?? string.Empty; }
            set { _hostIdentifier = value; }
        }
        private string _hostIdentifier;



        /// <summary>
        /// Gets or sets the optional name of the Host computer this setting is intended for.
        /// <para>
        /// Note that <see cref="ZoneOrTierIdentifier"/> is generally enough.
        /// </para>
        /// <para>
        /// If Empty, applies to all Hosts -- ie, it is a Tier setting.
        /// </para>
        /// <para>
        /// Part of a unique Index applied to datastorage.
        /// </para>
        /// </summary>
        [DataMember]
// ReSharper disable ConvertToAutoProperty
        public virtual Guid TennantIdentifier
// ReSharper restore ConvertToAutoProperty
        {
            get { return _tennantIdentifier; }
            set { _tennantIdentifier = value; }
        }
        private Guid _tennantIdentifier;



        /// <summary>
        /// Gets or sets the <see cref="Setting"/>'s Fully Qualified Name (Module + Groups + Key).
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="Setting" /> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets the serialization method.
        /// </summary>
        /// <value>
        /// The serialization method.
        /// </value>
        public virtual SerializationMethod SerializationMethod
        {
            get { return (SerializationMethod) SerializationMethodRaw; }
            set { SerializationMethodRaw = (int) value; }
        }

        /// <summary>
        /// Gets or sets the serialization method raw.
        /// </summary>
        /// <value>
        /// The serialization method raw.
        /// </value>
        [DataMember]
        public virtual int SerializationMethodRaw { get; set; }



        /// <summary>
        /// Gets or sets the serialized value.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValue { get; set; }


        /// <summary>
        /// Gets or sets the serialized default value.
        /// </summary>
        /// <value>
        /// The serialized default value.
        /// </value>
        [DataMember]
        public virtual string SerializedDefaultValue { get; set; }


        /// <summary>
        /// Information to help the managing Service determine whether
        /// the information is accessible by the current user.
        /// <para>
        /// Usually a CSV string of RoleNames, that the IAuthorisationService
        /// injected into the <see cref="ISettingsService"/>
        /// uses to create new <see cref="Setting"/>'s IsReadable.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string IsReadableAuthorisationInformation { get; set; }


        /// <summary>
        /// Information to help the managing Service determine whether
        /// the information is accessible by the current user.
        /// <para>
        /// Usually a CSV string of RoleNames, that the IAuthorisationService
        /// injected into the <see cref="ISettingsService"/>
        /// uses to create new <see cref="Setting"/>'s IsReadable.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string IsWritableAuthorisationInformation { get; set; }
         
        /// <summary>
        /// Information to help the managing Service determine whether
        /// the information is accessible by the current user,
        /// or was locked off higher up (note that a Setting
        /// can be Writable due to user having rights to his own 
        /// settings, but enherited a Lock, which would disable
        /// editing).
        /// </summary>
        [DataMember]
        public virtual string IsUnlockedInformation { get; set; }

        /// <summary>
        /// Gets or sets the Assembly qualified name of the Value that is serialized.
        /// <para>
        /// Member defined in the <see cref="IHasSerializedTypeValueAndMethod" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// <internal>8/16/2011: Sky</internal>
        [DataMember]
        public virtual string SerializedValueType { get; set; }




        /// <summary>
        /// Gets the tag of the object.
        /// <para>Member defined in<see cref="XAct.IHasTag" /></para>
        /// <para>Can be used to associate information -- such as an image ref -- to a SelectableItem.</para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember]
        public virtual string Tag { get; set; }


        /// <summary>
        /// Gets or sets optional metadata
        /// that other concerns may find useful
        /// (eg: the UI layer, until a better solution is developed).
        /// </summary>
        [DataMember]
        public virtual string Metadata { get; set; }


        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }



        /// <summary>
        /// Sets the specified value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        public virtual SerializedApplicationSetting Set<T>(T value)
        {
            SerializationMethod serializationMethod = SerializationMethod.String;

            this.SerializedValue = typeof (T).Serialize(value, ref serializationMethod);
            this.SerializedValueType = typeof (T).FullName;
            this.SerializationMethod = serializationMethod;

            return this;
        }


        /// <summary>
        /// Gets the date this entity was created, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual System.DateTime? CreatedOnUtc { get; set; }


        /// <summary>
        /// Gets the date this entity was last modified, expressed in UTC.
        /// <para>Member defined in <see cref="XAct.IHasDateTimeTrackabilityUtc" />.</para>
        /// <para>
        /// See also <see cref="IHasAuditability" />.
        /// </para>
        /// <para>
        /// Required: Must be set prior to being saved.
        /// </para>
        /// </summary>
        /// <internal>
        /// There are many arguments for wanting variables of a certain
        /// type to be grouped together by Type (DateCreated, DateUpdated,DateDeleted)
        /// and it's very very tempting...but in the long run, I can't see
        /// it being right decision to go against the whole nomenclature of C#
        /// just to give better Intellisense in VS...
        ///   </internal>
        [DataMember]
        public virtual System.DateTime? LastModifiedOnUtc { get; set; }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{0}={1}".FormatStringInvariantCulture(this.CompositeKey(), this.SerializedValue);
        }

        //We do *not* persist this:
        /// <summary>
        /// Gets or sets the scope.
        /// </summary>
        /// <value>
        /// The scope.
        /// </value>
        [IgnoreDataMember]
        public virtual int Scope { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="SerializedApplicationSetting"/> class.
        /// </summary>
        public SerializedApplicationSetting()
        {
            this.GenerateDistributedId();
        }
    }
}
