﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The base <see cref="SettingGroup"/>,
    /// which is the true location of the <see cref="SettingCollection"/>
    /// to which nested <see cref="SettingGroup"/>s point back to.
    /// <para>
    /// Profile is a special case of <see cref="ModuleSettingGroup"/>
    /// </para>
    /// </summary>
    [DataContract(Name = "ModuleSettingGroup", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/profiles")]
    public class ModuleSettingGroup : SettingGroup, IModuleSettingGroup
    {
        /// <summary>
        /// Character used to separate name of Module (if it exists)
        /// from Var path.
        /// </summary>
        public const string ModuleSeparatorCharacter = ":";

        /// <summary>
        /// Gets a value indicating whether this instance has access.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has access; otherwise, <c>false</c>.
        /// </value>
        /// <internal><para>8/15/2011: Sky</para></internal>
        public bool HasAccess
        {
            get
            {
                _hasAccess = _hasAccessFunc();
                return _hasAccess;
            }
        }
        private bool _hasAccess;
        private Func<bool> _hasAccessFunc;


        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSettingGroup"/> class.
        /// </summary>
        /// <internal>8/15/2011: Sky</internal>
        /// <internal><para>8/16/2011: Sky</para></internal>
// ReSharper disable RedundantBaseConstructorCall
        protected ModuleSettingGroup() // ReSharper restore RedundantBaseConstructorCall
        {
            //This overload is only used by Profile
            //(all other ModuleSettingsGroups are descendents of Profile).

            //As it is the root Profile, we can assume everybody always has
            //access to the core settings.
            _hasAccessFunc = () => true;


        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSettingGroup"/> class.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="hasAccess">The has access.</param>
        /// <internal>8/13/2011: Sky</internal>
        /// <internal>8/15/2011: Sky</internal>
        public ModuleSettingGroup(string groupName, Func<bool> hasAccess = null)
            :
                base(groupName)
        {
            _hasAccessFunc = hasAccess ?? (() => true);

            OnDeserializing(default(StreamingContext));
        }







        /*
        //Do *not* provide an iterator -- Properties cannot be made generic.
        public SettingBase<object> this[string settingName] {get...set....}
        */

        /// <summary>
        /// Called when serializing.
        /// </summary>
        /// <internal>
        /// We can't serialize the method...but we 
        /// can freeze the result of the method call:
        /// </internal>
        /// <param name="streamingContext">The streaming context.</param>
        /// <internal><para>8/17/2011: Sky</para></internal>
        [OnSerializing]
// ReSharper disable UnusedMember.Local
// ReSharper disable UnusedParameter.Local
        private void OnSerializing(StreamingContext streamingContext)
// ReSharper restore UnusedParameter.Local
// ReSharper restore UnusedMember.Local
        {
            _hasAccess = HasAccess;
        }


        /// <summary>
        /// Called when [deserializing].
        /// </summary>
        /// <internal>
        /// On the client side, we didn't get a fully serialized method
        /// we only got a serialized bool result of that server side method.
        /// Best we can put back a method
        /// that wraps the frozen value. It's not great logic,
        /// but someone can write a partial class on the receiving
        /// side that overrides it
        /// </internal>
        /// <param name="streamingContext">The streaming context.</param>
        /// <internal><para>8/17/2011: Sky</para></internal>
        [OnDeserializing]
        private void OnDeserializing(StreamingContext streamingContext)
        {
            if (_hasAccessFunc == null)
            {
                //The null check was to make sure a clientside partial 
                //hadn't already done the job of putting a method here:
                _hasAccessFunc = () => { return _hasAccess; };
            }
        }

        [OnDeserialized]
        void OnDeserialized(StreamingContext streamingContext)
        {

        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{0} settings.".FormatStringInvariantCulture(this._settingsCollection.Count);
            //return base.ToString();
        }

    
    
    }


}