﻿namespace XAct.Settings
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Settings specific for Application scope 
    /// (cached for long durations, 
    /// rather than retrieved on every request).
    /// </summary>
    [DataContract(Name = "ApplicationSettings", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/settings")]
    public class ApplicationSettings : Settings
    {

        /// <summary>
        /// Gets or sets 
        /// (optional)
        /// information as to the query specs used to assemble
        /// this <see cref="Settings"/>' <see cref="Setting"/>s.
        /// </summary>
        [DataMember]
        public ApplicationSettingScope ContextIdentifier { get; set; }

    }
}