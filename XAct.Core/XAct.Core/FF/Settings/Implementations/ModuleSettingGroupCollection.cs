﻿namespace XAct.Settings
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;

    /// <summary>
    /// A key'ed collection of <see cref="ModuleSettingGroup"/> items.
    /// <para>
    /// Used to back <see cref="Settings"/> indexer.
    /// </para>
    /// </summary>
    /// <internal><para>8/15/2011: Sky</para></internal>
    [CollectionDataContract(Name = "ModuleSettingGroupCollection", ItemName = "ModuleSetting",
        Namespace = "http://xact-solutions.com/contracts/v0.1/xact/profiles")]
    [ComVisible(false)]
    public class ModuleSettingGroupCollection : KeyedCollection<string, ModuleSettingGroup>
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleSettingGroupCollection"/> class.
        /// </summary>
        public ModuleSettingGroupCollection()
            : base(StringComparer.OrdinalIgnoreCase)
        {
        }

        /// <summary>
        /// Gets the key for item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        /// <internal><para>8/15/2011: Sky</para></internal>
        protected override string GetKeyForItem(ModuleSettingGroup item)
        {
            return item.Name;
        }
    }
}