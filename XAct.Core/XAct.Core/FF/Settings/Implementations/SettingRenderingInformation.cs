﻿namespace XAct.Settings
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Rendering Information for a <see cref="Setting"/>
    /// </summary>
    [DataContract(Name = "SettingRenderingHints", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/profiles")]
    public class SettingRenderingHints : IHasSettingRenderingHintsReadOnly , IHasRenderingHintsIdentifierReadOnly
    {

        /// <summary>
        /// Gets the Identifier to an Entity Record 
        /// (that implements a contract similar to <see cref="IHasSettingRenderingHints"/>)
        /// describing how to render the 
        /// <c>Setting</c> or other similar entity.
        /// <para>
        /// IMPORTANT: 
        /// If this entity implements both <see cref="IHasRenderingHintsIdentifierReadOnly"/>
        /// and <see cref="IHasSettingRenderingHintsReadOnly"/> (probably for convenvience/faster developement)  
        /// one sets *either* the <see cref="RenderHintsIdentifier"/>
        /// *OR* the Group/Order/Label/ViewControl/EditControl/EditValidation
        /// values -- not both.
        /// </para>
        /// </summary>
        /// <value>
        /// The render hints identifier.
        /// </value>
        public string RenderHintsIdentifier
        {
            get { return _renderHintsIdentifier; }
        }
        [DataMember(Name = "RenderHintsIdentifier", IsRequired = false)]
        private readonly string _renderHintsIdentifier;


        /// <summary>
        /// Gets optional hints to indicate to the UI how to layout settings.
        /// </summary>
        /// <value>
        /// The render grouping optional hints.
        /// </value>
        public string RenderGroupingHints
        {
            get { return _renderGroupingHints; }
        }
        [DataMember(Name = "RenderGroupingHints", IsRequired = false)]
        private readonly string _renderGroupingHints;



        /// <summary>
        /// Gets the hint as to which order to render the Setting's control in the Group.
        /// </summary>
        /// <value>
        /// The render order hint.
        /// </value>
        public int RenderOrderHint
        {
            get { return _renderOrderHint; }
        }
        [DataMember(Name = "RenderOrderHint", IsRequired = false)]
        private readonly int _renderOrderHint;

        /// <summary>
        /// Gets a hint as to the icons to use if the full item cannot be rendered.
        /// <para>
        /// Images could be for controls tucked away, or simply rendered beside/instead of labels.
        /// </para>
        /// </summary>
        public string RenderingImageHints
        {
            get { return _renderingImageHints; }
            private set { _renderingImageHints = value; }
        }
        [DataMember(Name = "RenderingImageHints", IsRequired = false)]
        private string _renderingImageHints;



        /// <summary>
        /// Gets optional hints to indicate to the UI how to layout settings.
        /// </summary>
        /// <value>
        /// The render grouping optional hints.
        /// </value>
        public string RenderLabelHints
        {
            get { return _renderLabelHints; }
        }
        [DataMember(Name = "RenderLabelHints", IsRequired = false)]
        private readonly string _renderLabelHints;

        /// <summary>
        /// Gets optional hints to indicate to the UI how to layout settings.
        /// </summary>
        /// <value>
        /// The render grouping optional hints.
        /// </value>
        public string RenderViewControlHints
        {
            get { return _renderViewControlHints; }
        }
        [DataMember(Name = "RenderViewControlHints", IsRequired = false)]
        private readonly string _renderViewControlHints;

        /// <summary>
        /// Gets optional hints to indicate to the UI how to layout settings.
        /// </summary>
        /// <value>
        /// The render grouping optional hints.
        /// </value>
        public string RenderEditControlHints
        {
            get { return _renderEditControlHints; }
        }
        [DataMember(Name = "RenderEditControlHints", IsRequired = false)]
        private readonly string _renderEditControlHints;


        /// <summary>
        /// Gets optional hints to indicate to the UI how to layout settings.
        /// </summary>
        /// <value>
        /// The render grouping optional hints.
        /// </value>
        public string RenderEditValidationHints
        {
            get { return _renderEditValidationHints; }
        }
        [DataMember(Name = "RenderValidationHints", IsRequired = false)]
        private readonly string _renderEditValidationHints;



        private SettingRenderingHints()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingRenderingHints"/> class.
        /// </summary>
        /// <param name="renderingHintsIdentifier">The rendering hints identifier.</param>
        /// <param name="renderingGroupHints">The rendering group hints.</param>
        /// <param name="renderingOrderHint">The rendering order hint.</param>
        /// <param name="renderingLabelsHints">The rendering labels hints.</param>
        /// <param name="renderingViewControlHints">The rendering view control hints.</param>
        /// <param name="renderingEditControlHints">The rendering edit control hints.</param>
        /// <param name="renderingEditValidationControlHints">The rendering edit validation control hints.</param>
        public SettingRenderingHints(string renderingHintsIdentifier=null,  
                                     string renderingGroupHints = null, int renderingOrderHint = 0,
                                     string renderingLabelsHints = null, string renderingViewControlHints = null,
                                     string renderingEditControlHints = null,
                                     string renderingEditValidationControlHints = null)
        {
            //ID of record if any:
            _renderHintsIdentifier = renderingHintsIdentifier;

            //or do it verbosely/heavily:
            _renderGroupingHints = renderingGroupHints;
            _renderOrderHint = renderingOrderHint;
            _renderLabelHints = renderingLabelsHints;
            _renderViewControlHints = renderingViewControlHints;
            _renderEditControlHints = renderingEditControlHints;
            _renderEditValidationHints = renderingEditValidationControlHints;
        }

    }
}