﻿//namespace XAct.Settings
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public interface IModuleSettingsGroupAccessor : IProfileSettingCollectionAccessor 
//    {
//        /// <summary>
//        /// Gets the module settings group collection.
//        /// </summary>
//        /// <value>
//        /// The module settings group collection.
//        /// </value>
//        ModuleSettingGroupCollection ModuleSettingsGroupCollection { get; }
//    }
//}