﻿
// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Contract used by the Persistors to access the internal property.
    /// </summary>
    /// <internal>
    /// The reason for this contract is that the internal
    /// collection -- whether it be Module or Profile -- should
    /// only be used to Hydrate and persist the collection.
    /// All other operations should go through the accessor methods.
    /// </internal>
    public interface IProfileSettingCollectionAccessor
    {
        /// <summary>
        /// Gets the settings collection.
        /// </summary>
        /// <value>The settings collection.</value>
        SettingCollection Settings { get; }

    }
}
