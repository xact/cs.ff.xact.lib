﻿namespace XAct.Settings
{
    internal interface ISettingAccessor
    {

        /// <summary>
        /// Retrieves the specified *existing* (already Added) setting.
        /// </summary>
        /// <param name="settingName">Name of the setting.</param>
        /// <returns></returns>
        /// <internal><para>8/17/2011: Sky</para></internal>
        Setting RetrieveSetting(string settingName);
    }
}