﻿// ReSharper disable CheckNamespace
namespace XAct.Settings
// ReSharper restore CheckNamespace
{
    using System;



    /// <summary>
    /// 
    /// </summary>
    public interface IApplicationSettingScope
    {
        /// <summary>
        /// Gets or sets the environment identifier
        /// (eg: DEV, ST, SIT, UAT, PP, PROD, etc.)
        /// <para>
        /// If Empty, applies to all Environments.
        /// </para>
        /// <para>
        /// Part of a unique Index applied to datastorage.
        /// </para>
        /// </summary>
        string EnvironmentIdentifier { get; set; }



        /// <summary>
        /// Gets or sets the optional identifier/name of the zone/tier the Settings are for.
        /// </summary>
        /// <value>
        /// The zone or tier identifier.
        /// </value>
        string ZoneOrTierIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the optional identifier/name of the host server the Settings are for.
        /// </summary>
        /// <value>
        /// The host identifier.
        /// </value>
        string HostIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the unique Identifier
        /// for the current application Tennant
        /// (in most cases, this is the Organisation).
        /// </summary>
        /// <value>
        /// The tennant identifier.
        /// </value>
        Guid TennantIdentifier { get; set; }


    }

    ///// <summary>
    ///// 
    ///// </summary>
    //public interface ISettingScope :IApplicationSettingScope
    //{


    //    /// <summary>
    //    /// Gets or sets the unique Identifier
    //    /// for the current application Tennant
    //    /// (in most cases, this is the Organisation).
    //    /// </summary>
    //    /// <value>
    //    /// The tennant identifier.
    //    /// </value>
    //    Guid TennantIdentifier { get; set; }

    //    /// <summary>
    //    /// Gets or sets the identifier/name of the user the Settings are for.
    //    /// </summary>
    //    /// <value>
    //    /// The user identifier.
    //    /// </value>
    //    string UserIdentifier { get; set; }



    //}
}