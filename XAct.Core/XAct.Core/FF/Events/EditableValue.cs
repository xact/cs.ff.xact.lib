﻿#if CONTRACTS_FULL || NET40 || NET45 || NET45 // Requires .Net 4, so hold for the moment 

#endif


// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using XAct.Domain;

    /// <summary>
    /// An abstract base class for holding settings that can be set Transacted (committed, or rolled back).
    /// </summary>
    /// <remarks>
    /// <para>
    /// Note that due to implicit Operators one
    /// can interact with an Editable object
    /// as follows:
    /// <code>
    /// <![CDATA[
    /// EditableValueBase<string> demo = new EditableValueBase<string>();
    /// demo.Value = "Works";
    /// string s = demo; //no need to do the full verbose 's = demo.Value'
    /// //although this is not allowed: 
    /// //demo = "as does this";
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    /// <typeparam name="TValue">The type of the value.</typeparam>
    /// <internal><para>8/13/2011: Sky</para></internal>
    [DataContract(Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public class EditableValue<TValue> : IEditableObject, INotifyPropertyChanging, INotifyPropertyChanged, IHasModelState, IHasValue<TValue>
    {


        /// <summary>
        /// Occurs when a property value changes.
        /// <para>
        /// See <see cref="INotifyPropertyChanged"/>
        /// </para>
        /// </summary>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public event PropertyChangedEventHandler PropertyChanged;

 

        /// <summary>
        /// Occurs when a property value is changing.
        /// <para>
        /// See <see cref="INotifyPropertyChanging"/>
        /// </para>
        /// </summary>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public event PropertyChangingEventHandler PropertyChanging;




        /// <summary>
        /// The cached previous value that was backing <see cref="Value"/>
        /// <para>
        /// Set when <see cref="BeginEdit"/> is invoked.
        /// </para>
        /// </summary>
// ReSharper disable InconsistentNaming
        protected TValue _previousValue;

// ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets or sets a value indicating whether 
        /// <see cref="BeginEdit"/> has been invoked.
        /// <para>
        /// Reset by <see cref="CancelEdit"/> or 
        /// <see cref="EndEdit"/>
        /// </para>
        /// </summary>
        /// <value><c>true</c> if editing; otherwise, <c>false</c>.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        protected bool Editing { get; set; }










        /// <summary>
        /// THe current value backing <see cref="Value"/>
        /// </summary>
        /// <internal>
        /// The Generic value will be serialized as an Object.
        /// </internal>
        [DataMember(Name = "Value", IsRequired = false)] // ReSharper disable InconsistentNaming
            protected TValue _currentValue;

// ReSharper restore InconsistentNaming



        /// <summary>
        /// 
        /// </summary>
        /// <internal>
        /// Because Type is not serializable via WCF, we have to pass the Type
        /// and Value as strings.
        /// </internal>
        [DataMember(Name = "SerializedValue", IsRequired = true)] private SerializedObject _serializedValue;

        /// <summary>
        /// Gets or sets the Type of <see cref="Value"/>.
        /// </summary>
        /// <value>The type of the value.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        /// <internal>
        /// Do NOT mark as DataMember: Type is !serializable
        /// </internal>
        public Type ValueType
        {
            get { return _valueType; }
            protected set { _valueType = value; }
        }
        private Type _valueType = typeof(TValue);

        /// <summary>
        /// Gets or sets the Typed value.
        /// </summary>
        /// <value>The value.</value>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public virtual TValue Value
        {
            get
            {
                var result = GetValue();
                return result;
            }
            set { SetValue(value); }
        }

        /// <summary>
        /// Gets the <see cref="ModelState"/> of the object.
        /// <para>
        /// Model states will be:
        /// <code>
        /// <![CDATA[
        /// * New (Initial State)
        /// * UnchangedExisting
        /// * Modified (when SetValue() is invoked).
        /// * Deleted
        /// ]]>
        /// </code>
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasModelState"/></para>
        /// </summary>
        public OfflineModelState ModelState
        {
            get { return _modelState; }
            set { _modelState = value; }
        }
        [DataMember(Name = "ModelSate", IsRequired = true)]
        private OfflineModelState _modelState = OfflineModelState.New;




        /// <summary>
        /// Begins an edit on an object.
        /// <para>Member defined in<see cref="System.ComponentModel.IEditableObject"/></para>
        /// </summary>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public void BeginEdit()
        {
            //Backup:
            _previousValue = _currentValue;
            Editing = true;
        }

        /// <summary>
        /// Discards changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> call.
        /// <para>Member defined in<see cref="System.ComponentModel.IEditableObject"/></para>
        /// </summary>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public void CancelEdit()
        {
            //Rollback:
            _currentValue = _previousValue;
            Editing = false;
        }

        /// <summary>
        /// Pushes changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> or 
        /// <see cref="M:System.ComponentModel.IBindingList.AddNew"/> call into the underlying object.
        /// <para>Member defined in<see cref="System.ComponentModel.IEditableObject"/></para>
        /// </summary>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public void EndEdit()
        {
            //Save:
            _previousValue = _currentValue;
            Editing = false;
        }


        #region Overrideable Helper Methods

        /// <summary>
        /// Gets the value.
        /// <para>Member defined in<see cref="XAct.EditableValue{T}"/></para>
        /// </summary>
        /// <returns></returns>
        /// <internal><para>8/13/2011: Sky</para></internal>
        protected virtual TValue GetValue()
        {
            return _currentValue;
        }

        /// <summary>
        /// Sets the value if different to current value.
        /// <para>
        /// Note: Modifies <see cref="ModelState"/> only if value is different than current value.
        /// </para>
        /// <para>
        /// Invokes <see cref="EditableValue{TValue}.OnPropertyChanging(PropertyChangingEventArgs)" />
        /// and
        /// <see cref="EditableValue{TValue}.OnPropertyChanged(PropertyChangedEventArgs)" />
        /// </para>
        /// <para>Member defined in<see cref="XAct.EditableValue{T}"/></para>
        /// </summary>
        /// <param name="value">The value.</param>
        /// <internal>8/13/2011: Sky</internal>
        protected virtual void SetValue(TValue value)
        {
            if (Comparer<TValue>.Default.Compare(value, _currentValue) == 0)
            {
                //If same, don't do anything
                return;
            }

            //If validation did not raise exception, we are going to update value:
            OnPropertyChanging("Value");

            _currentValue = value;

            if (!Editing)
            {
                //As long as Begin Edit was not yet invoked,
                //save to previous as well.
                _previousValue = _currentValue;
            }
            //else
            //{
            //    //we are in Caching mode until EndEdit or CancelEdit is invoked
            //    //so we don't update previousValue.
            //}

            if (ModelState == OfflineModelState.New)
            {
                //Leave alone, as we don't want to mess up 
                //trail that this is a new Entry, that needs to be
                //saved as New, rather than Update.
            }
            else
            {
                //Whether it's currently OfflineModelState.UnchangedExisting 
                //or OfflineModelState.Updated set to OfflineModelState.Updated:
                ModelState = OfflineModelState.Updated;
            }

            OnPropertyChanged("Value");
        }

        /// <summary>
        /// Serializes <see cref="Value"/> as a <see cref="SerializedObject"/>.
        /// </summary>
        /// <returns></returns>
        public SerializedObject Serialize()
        {
            _serializedValue = new SerializedObject(SerializationMethod.String, ValueType, Value);

            return _serializedValue;
        }
        #endregion

        [OnSerializing]
        // ReSharper disable UnusedMember.Local
        // ReSharper disable UnusedParameter.Local
        private void InitializeFuncs(StreamingContext streamingContext)
            // ReSharper restore UnusedParameter.Local
            // ReSharper restore UnusedMember.Local
        {
            _serializedValue = new SerializedObject(SerializationMethod.String, ValueType, Value);
        }

        #region Raise Events

        /// <summary>
        /// Invoked when <see cref="Value"/> is changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <internal><para>8/13/2011: Sky</para></internal>
        private void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }


        /// <summary>
        /// Raises the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        /// <internal><para>8/10/2011: Sky</para></internal>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }


        /// <summary>
        /// Invoked when <see cref="Value"/> is changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <internal><para>8/13/2011: Sky</para></internal>
        private void OnPropertyChanging(string propertyName)
        {
            OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        /// <summary>
        /// Raises the <see cref="PropertyChanging"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangingEventArgs"/> instance containing the event data.</param>
        /// <internal><para>8/13/2011: Sky</para></internal>
        protected virtual void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, e);
            }
        }

        #endregion

        #region Operators

        /// <summary>
        /// Performs an implicit conversion from 
        /// <see cref="XAct.EditableValue&lt;TValue&gt;"/> to 
        /// the expected Type.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The result of the conversion.</returns>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public static implicit operator TValue(EditableValue<TValue> value)
        {
            return value.Value;
        }

        ///// <summary>
        ///// Performs an implicit conversion from <see cref="TValue"/> to <see cref="XAct.EditableValueBase&lt;TValue&gt;"/>.
        ///// </summary>
        ///// <param name="value">The value.</param>
        ///// <returns>The result of the conversion.</returns>
        ///// <internal><para>8/13/2011: Sky</para></internal>
        //public static implicit operator EditableValueBase<TValue>(TValue value)
        //{
        //    //NOT GOOD:
        //    //WILL FAIL EQUALITY CHECKE AS IT IS A NEW INSTANCE:
        //    //return new EditableValueBase<TValue> { Value = value };
        //}

        #endregion
    }
}