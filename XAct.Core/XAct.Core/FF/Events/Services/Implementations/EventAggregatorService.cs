﻿namespace XAct.Events.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="IEventAggregatorService"/>
    /// to provide a means to pass messages among loosely coupled elements.
    /// </summary>
    /// <internal>
    /// See: http://www.codeproject.com/Articles/52810/Event-Aggregator-with-Specialized-Listeners
    /// <para>
    /// Going forward: http://msdn.microsoft.com/en-us/library/ff921122(v=pandp.20).aspx
    /// </para>
    /// </internal>
    public class EventAggregatorService : XActLibServiceBase, IEventAggregatorService
    {
        private readonly object _lock = new object();

        private IEventAggregatorServiceSubscriberServiceState Subcribers
        {
            get { return XAct.DependencyResolver.Current.GetInstance<IEventAggregatorServiceSubscriberServiceState>(); }
        }

        private SynchronizationContext Context
        {
            get
            {
                if (SynchronizationContext.Current == null)
                {
                    SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
                }
                return SynchronizationContext.Current;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventAggregatorService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="subscriberCache">The subscriber cache.</param>
        public EventAggregatorService(ITracingService tracingService, IEventAggregatorServiceSubscriberServiceState subscriberCache) : base(tracingService)
        {
            //Different than usual. We can't have a Singleton cache -- only a Singleton per thread.
            //therefore, we don't need/want the cache...just want to ensure that it's fresh per
            //Just is helpful to know ServiceLocator can find it...
            //Subcribers == null;

        }

        /// <summary>
        /// Publishes the specified message
        /// to registered
        /// <see cref="IEventSubscriber"/>s.
        /// </summary>
        /// <typeparam name="T">The <see cref="IEvent"/></typeparam>
        /// <param name="message">The message.</param>
        public void Publish<T>(T message) where T : IEvent
        {



            //Publish the messag to all Subscribers of type T
            SendAction(() => All().ForEach<IEventSubscriber<T>>(x =>
            {
                x.Handle(message);
            }));


            //SendAction(() => All().CallOnEach<IPredicateSubscriber<T>>(x =>
            //{
            //    if (x.SatisfiedBy(message))
            //    {
            //        x.Handle(message);
            //    }
            //}));

        }



        //public void Publish<T>() where T : IMessage, new()
        //{
        //    SendMessage(new T());
        //}

        /// <summary>
        /// Adds the given subscriber.
        /// </summary>
        /// <param name="listener">The listener.</param>
        public void AddSubscriber(IEventSubscriber listener)
        {
            lock (_lock)
            {
                if (Subcribers.Contains(listener))
                {
                    return;
                }

                Subcribers.Add(listener);
            }
        }

        /// <summary>
        /// Removes the subscriber.
        /// </summary>
        /// <param name="listener">The listener.</param>
        public void RemoveSubscriber(IEventSubscriber listener)
        {
            lock (_lock)
            {
                Subcribers.Remove(listener);
            }
        }


        private IEnumerable<IEventSubscriber> All()
        {
            lock (_lock)
            {
                return Subcribers;
            }
        }



        private void SendAction(Action action)
        {
            this.Context.Send(state => action(), null);
        }
    }
}
