﻿// ReSharper disable CheckNamespace
namespace XAct
// ReSharper restore CheckNamespace
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// An object to pass serialized objects across the wire in a controlled way
    /// (some serializers will fail).
    /// </summary>
    /// <internal>
    /// Used internally by <c>XAct.Settings.Setting</c>
    /// to pass values across the wire to the Client.
    /// </internal>
    /// <internal><para>8/16/2011: Sky</para></internal>
    [DataContract(Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public class SerializedObject : IHasId<string>, IHasSerializedTypeValueAndMethod
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializedObject"/> class.
        /// </summary>
        public SerializedObject()
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="SerializedObject"/> class.
        /// </summary>
        /// <param name="serializationMethod">The serialization method.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="value">The value.</param>
        /// <internal><para>8/16/2011: Sky</para></internal>
        public SerializedObject(SerializationMethod serializationMethod, Type valueType, object value)
        {
            //SerializedValueType = valueType.AssemblyQualifiedName;

            if (serializationMethod == SerializationMethod.Undefined)
            {
                throw new ArgumentException("serializationMethod");
            }

              
            SerializedValue = valueType.Serialize(value, ref serializationMethod);

            SerializedValueType = valueType.FullName;

            SerializationMethod = serializationMethod;
        }


        /// <summary>
        /// Gets or sets an (optional) Id.
        /// Useful for sending a couple of Settings across the wire 
        /// and know which one we're talking about.
        /// </summary>
        /// <value>The id.</value>
        /// <internal><para>8/16/2011: Sky</para></internal>
        [DataMember(Name = "Id", EmitDefaultValue = false, IsRequired = false)]
        public string Id { get; set; }


        /// <summary>
        /// Gets or sets the serialization method employed.
        /// </summary>
        /// <value>The serialization method.</value>
        /// <internal><para>8/16/2011: Sky</para></internal>
        [DataMember(Name = "SerializationMethod", EmitDefaultValue = true, IsRequired = true)]
        public SerializationMethod SerializationMethod { get; set; }
        /// <summary>
        /// Gets or sets the serialized <see cref="SerializedValue"/> <see cref="SerializedValueType"/>.
        /// </summary>
        /// <value>The type.</value>
        /// <internal><para>8/16/2011: Sky</para></internal>
        [DataMember(Name = "Type", EmitDefaultValue = false, IsRequired = true)]
        public string SerializedValueType { get; set; }

        /// <summary>
        /// Gets or sets the serialized value.
        /// </summary>
        /// <value>The value.</value>
        /// <internal><para>8/16/2011: Sky</para></internal>
        [DataMember(Name = "Value", EmitDefaultValue = true, IsRequired = true)]
        public string SerializedValue { get; set; }


    }
}