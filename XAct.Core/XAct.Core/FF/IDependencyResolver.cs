﻿//namespace XAct
//{
//    using System;
//    using System.Collections.Generic;

//    /// <summary>
//    /// Defines the methods that simplify service location and dependency resolution.
//    /// </summary>
//    public interface IDependencyResolver
//    {
//        /// <summary>
//        /// Resolves singly registered services that support arbitrary object creation.
//        /// </summary>
//        /// 
//        /// <returns>
//        /// The requested service or object.
//        /// </returns>
//        /// <param name="serviceType">The type of the requested service or object.</param>
//        object GetService(Type serviceType);

//        /// <summary>
//        /// Resolves multiply registered services.
//        /// </summary>
//        /// 
//        /// <returns>
//        /// The requested services.
//        /// </returns>
//        /// <param name="serviceType">The type of the requested services.</param>
//        IEnumerable<object> GetServices(Type serviceType);
//    }
//}