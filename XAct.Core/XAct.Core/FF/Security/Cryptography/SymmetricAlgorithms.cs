﻿namespace XAct.Security.Cryptography
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using XAct.Diagnostics;

    /// <summary>
    ///   Instantiable class for managing encryption 
    ///   and decryption using Symmetric Algorithms.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     See also: 
    ///     http://www.codeproject.com/KB/security/Cryptor.aspx
    ///     and
    ///     http://www.codeproject.com/KB/security/SymmetricAlgorithmHelper.aspx
    ///     Definately read:
    ///     http://www.grimes.demon.co.uk/workshops/secWSEleven.htm
    ///     and
    ///     <![CDATA[
    /// http://books.google.com/books
    /// ?id=IqvXsWfzN8wC&pg=PA82&lpg=PA82&dq=symmetricalgorithm+padding&
    /// source=bl&ots=XeRYx2zAp0&sig=EFD6ekEYF9cHydS4veGwJ5T-JZc&hl=en&sa=X&
    /// oi=book_result&resnum=6&ct=result#PPR6,M1
    /// ]]>
    ///   </para>
    /// </remarks>
    public class SymmetricAlgorithms
    {
        #region Constants

        /// <summary>
        ///   Default Symmetric Algorithm to create if no name is supplied.
        ///   <para>
        ///     Default value is "Rijndael".
        ///   </para>
        /// </summary>
        public const string DefaultSymmetricalAlgorithm = "Rijndael";

        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;

        #endregion

        #region Properties

        private const int C_KB = 1024;

        /// <summary>
        ///   The size of the chunk (in KBytes) to read from the incoming stream,
        ///   when processing an encryption.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Note that 50* 1Kb = 50*1024=51200
        ///   </para>
        /// </remarks>
        public const long MaxChunkSize = 1024*C_KB;

        /// <summary>
        ///   Represents 1 KByte of data. Vary only for testing purposes.
        /// </summary>
        public const int ChunkSize = 128*1024; //1kb * 1kb = 1Mb

        private static ITracingService _tracingService;

        /// <summary>
        ///   Name of the Symmetric Algorithm to use.
        ///   <para>
        ///     If none given in constructor, this will be set to the value
        ///     of <see cref = "DefaultSymmetricalAlgorithm" />.
        ///   </para>
        ///   <para>
        ///     The property is read only.
        ///   </para>
        /// </summary>
        private readonly string _algorithmName;

        /// <summary>
        ///   Flag indicating whether Initialization Vector (IV) buffer 
        ///   is embedded at head of stream, before cipher text.
        ///   <para>
        ///     Note that the default value is <c>True</c>.
        ///   </para>
        /// </summary>
        private readonly bool _embedIVAtHeadOfOutgoingStream = true;

        /// <summary>
        ///   Flag indicating whether obfuscated Key buffer is embedded 
        ///   at head of stream, before cipher text.
        ///   <para>
        ///     The default value is <c>False</c>.
        ///   </para>
        /// </summary>
        private readonly bool _embedKeyAtHeadOfOutgoingStream;

        /// <summary>
        ///   Flag indicating whether Salt buffer is embedded 
        ///   at head of stream, before cipher text.
        ///   <para>
        ///     The default value is <c>False</c>.
        ///   </para>
        /// </summary>
        private readonly bool _embedSaltAtHeadOfOutgoingStream = true;


        private bool _initialized;

        /// <summary>
        ///   The string Key used to set up the Key Bytes.
        /// </summary>
        /// <internal>
        ///   Architecturally, this stinks. 
        ///   At the very least, it should be a SecureString -- but they're such a 
        ///   pain to work with that I havn't implemented it here...maybe one day.
        /// </internal>
        private string _keyPhrase;

        /// <summary>
        ///   Protected: Bytes used as Salt for converting the KeyPhrase 
        ///   into a Key byte buffer (<see cref = "KeyBytes" />).
        ///   <para>
        ///     If none supplied, the IV byte buffer will be used.
        ///   </para>
        /// </summary>
        // ReSharper disable InconsistentNaming
        private byte[] _saltBytes;

        private SymmetricAlgorithm _symmetricAlgorithm;

        /// <summary>
        ///   The cipher mode used by the Symmetric Algorithm.
        ///   <para>
        ///     The default value is 
        ///     <see cref = "System.Security.Cryptography.CipherMode.CBC" />.
        ///   </para>
        /// </summary>
        public CipherMode CipherMode
        {
            get { return _symmetricAlgorithm.Mode; }
        }

        /// <summary>
        ///   The padding mode used by the Symmetric Algorithm.
        ///   <para>
        ///     The default value is 
        ///     <see cref = "System.Security.Cryptography.PaddingMode.PKCS7" />
        ///   </para>
        /// </summary>
        public PaddingMode PaddingMode
        {
            get { return _symmetricAlgorithm.Padding; }
        }


        /// <summary>
        ///   Bytes used as Initialization Vector (IV) for the Symmetric Algorithm.
        ///   <para>
        ///     If none supplied, a randomly generated one will be supplied, 
        ///     which needs to be recorded
        ///     in order to be used for the decryption stage.
        ///   </para>
        /// </summary>
        public byte[] IVBytes
        {
            get { return _symmetricAlgorithm.IV; }
        }

        /// <summary>
        ///   Bytes used as Salt for converting the <c>KeyPhrase</c> 
        ///   into a Key byte buffer (<see cref = "KeyBytes" />).
        ///   <para>
        ///     If none supplied, the IV byte buffer will be used.
        ///   </para>
        /// </summary>
        public byte[] SaltBytes
        {
            get { return _saltBytes; }
            protected set { _saltBytes = value; }
        }

// ReSharper restore InconsistentNaming


        /// <summary>
        ///   Bytes used for Key of Symmetrical Algorithm.
        ///   <para>
        ///     Note that this is normally not as simple as making
        ///     a fixed size byte buffer of the <c>KeyPhrase</c>.
        ///   </para>
        /// </summary>
        public byte[] KeyBytes
        {
            get { return _symmetricAlgorithm.Key; }
        }

        /// <summary>
        ///   Flag indicating whether Salt buffer is embedded 
        ///   at head of stream, before cipher text.
        ///   <para>
        ///     The default value is <c>False</c>.
        ///   </para>
        /// </summary>
        public bool EmbedSaltAtHeadOfOutgoingStream
        {
            get { return _embedSaltAtHeadOfOutgoingStream; }
        }

        /// <summary>
        ///   Flag indicating whether obfuscated Key buffer is embedded 
        ///   at head of stream, before cipher text.
        ///   <para>
        ///     The default value is <c>False</c>.
        ///   </para>
        /// </summary>
        public bool EmbedKeyAtHeadOfOutgoingStream
        {
            get { return _embedKeyAtHeadOfOutgoingStream; }
        }

        /// <summary>
        ///   Flag indicating whether Initialization Vector (IV) buffer 
        ///   is embedded at head of stream, before cipher text.
        ///   <para>
        ///     Note that the default value is <c>True</c>.
        ///   </para>
        /// </summary>
        public bool EmbedIVAtHeadOfOutgoingStream
        {
            get { return _embedIVAtHeadOfOutgoingStream; }
        }

        /// <summary>
        ///   Name of the Symmetric Algorithm to use.
        ///   <para>
        ///     If none given in constructor, this will be set to the value
        ///     of <see cref = "DefaultSymmetricalAlgorithm" />.
        ///   </para>
        ///   <para>
        ///     The property is read only.
        ///   </para>
        /// </summary>
        public string AlgorithmName
        {
            get { return _algorithmName; }
        }

        /// <summary>
        ///   Sets the loggging service .
        /// </summary>
        /// <value>The loggging service.</value>
        public static void SetLoggingService(ITracingService tracingService)
        {
            if (_tracingService != null)
            {
                throw new ArgumentException("Cannot reset LoggingService once set.");
            }
            _tracingService = tracingService;
        }

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of 
        ///   the <see cref = "SymmetricAlgorithms" /> class.
        /// </summary>
        public SymmetricAlgorithms()
            : this(DefaultSymmetricalAlgorithm, CipherMode.CBC, PaddingMode.PKCS7)
        {
        }

        /// <summary>
        ///   Initializes a new instance of 
        ///   the <see cref = "SymmetricAlgorithms" /> class.
        ///   <para>
        ///     Valid algorithm names are: "DES","TripleDES","RC2","Rijndael".
        ///   </para>
        /// </summary>
        /// <param name = "algorithmName">
        ///   Name of the symmetric algorithm to use.
        /// </param>
        public SymmetricAlgorithms(string algorithmName)
            : this(algorithmName, CipherMode.CBC, PaddingMode.PKCS7)
        {
        }

        /// <summary>
        ///   Initializes a new instance of 
        ///   the <see cref = "SymmetricAlgorithms" /> class.
        ///   <para>
        ///     Valid algorithm names are: "DES","TripleDES","RC2","Rijndael".
        ///   </para>
        /// </summary>
        /// <param name = "algorithmName">
        ///   Name of the symmetric algorithm to use.
        /// </param>
        /// <param name = "cipherMode">The cipher mode to use.</param>
        /// <param name = "paddingMode">The padding mode to use.</param>
        public SymmetricAlgorithms(
            string algorithmName, CipherMode cipherMode, PaddingMode paddingMode) :
                this(algorithmName, cipherMode, paddingMode, true, true, false)
        {
        }

        /// <summary>
        ///   Initializes a new instance of 
        ///   the <see cref = "SymmetricAlgorithms" /> class.
        ///   <para>
        ///     Valid algorithm names are: "DES","TripleDES","RC2","Rijndael".
        ///   </para>
        /// </summary>
        /// <param name = "algorithmName">
        ///   Name of the symmetric algorithm to use.
        /// </param>
        /// <param name = "cipherMode">The cipher mode to use.</param>
        /// <param name = "paddingMode">The padding mode to use.</param>
        /// <param name = "embedIVAtHeadOfOutgoingStream">
        ///   if set to <c>true</c>, embed the IV bytes at head of outgoing stream.
        /// </param>
        /// <param name = "embedSaltAtHeadOfOutgoingStream">
        ///   if set to <c>true</c> embed salt bytes at head of outgoing stream.
        /// </param>
        /// <param name = "embedKeyAtHeadOfOutgoingStream">
        ///   if set to <c>true</c> embed key bytes at head of outgoing stream.
        /// </param>
        public SymmetricAlgorithms(string algorithmName,
                                   CipherMode cipherMode, PaddingMode paddingMode,
                                   bool embedIVAtHeadOfOutgoingStream,
                                   bool embedSaltAtHeadOfOutgoingStream,
                                   bool embedKeyAtHeadOfOutgoingStream)
        {
            //The default symmetric algorithm is "Rijndael".
            if (string.IsNullOrEmpty(algorithmName))
            {
                algorithmName = DefaultSymmetricalAlgorithm;
            }

            //Save the settings:
            _algorithmName = algorithmName;
            _embedIVAtHeadOfOutgoingStream = embedIVAtHeadOfOutgoingStream;
            _embedSaltAtHeadOfOutgoingStream = embedSaltAtHeadOfOutgoingStream;
            _embedKeyAtHeadOfOutgoingStream = embedKeyAtHeadOfOutgoingStream;

            InitializeAlgorithm(cipherMode, paddingMode);
        }

        #endregion

        #region Static Methods

        /// <summary>
        ///   Static method to encrypt the contents of the incoming stream, 
        ///   writting it out to the outgoing stream.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Invoked by <see cref = "Encrypt(Stream,Stream)" />.
        ///   </para>
        /// </remarks>
        /// <param name = "plainTextStream">The stream in.</param>
        /// <param name = "cipherTextStream">The stream out.</param>
        /// <param name = "cryptoTransform">The crypto transform.</param>
        public static void Encrypt(Stream plainTextStream, Stream cipherTextStream, ICryptoTransform cryptoTransform)
        {
            //Check args:
            plainTextStream.ValidateIsNotDefault("plainTextStream");
            cipherTextStream.ValidateIsNotDefault("cipherTextStream");
            cryptoTransform.ValidateIsNotDefault("cryptoTransform");

            //Step 1: Deal with reading vars first:
            // Read the unencrypted file into fileData 
            //Create variables to help with read and write.
            long totalInputByteLength = plainTextStream.Length;
            int bufferLength = (totalInputByteLength < MaxChunkSize)
                                   ? (int) totalInputByteLength
                                   : cryptoTransform.InputBlockSize;
            byte[] buffer = new byte[bufferLength];
            long bytesRead = 0; //Total read so far.

            // Step 4: Create the CryptoStream object wrapping cipherTextStream
            using (
                CryptoStream cryptocipherTextStream = new CryptoStream(cipherTextStream, cryptoTransform,
                                                              CryptoStreamMode.Write))
            {
                int bytesActuallyRead; //Total read this pass.
                //Read from the input file, then encrypt and write to the output file.
                do
                {
                    bytesActuallyRead = plainTextStream.Read(buffer, 0, bufferLength);
                    cryptocipherTextStream.Write(buffer, 0, bytesActuallyRead);
                    bytesRead = bytesRead + bytesActuallyRead;
                } while (bytesActuallyRead != 0);
                if (bytesRead >= totalInputByteLength)
                {
                    //int remaining = bytesActuallyRead%cryptoTransform.InputBlockSize;
                }
                cryptocipherTextStream.FlushFinalBlock();
            }
        }

//~method


        /// <summary>
        ///   Static method to decrypt the contents of the incoming cipherText stream, 
        ///   writting it out to the outgoing plainText stream.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Invoked by <see cref = "Decrypt(Stream, Stream)" />.
        ///   </para>
        /// </remarks>
        /// <param name = "cipherTextStream">The stream in.</param>
        /// <param name = "plainTextStream">The stream out.</param>
        /// <param name = "cryptoTransform">The crypto transform.</param>
        public static void Decrypt(
            Stream cipherTextStream, Stream plainTextStream, ICryptoTransform cryptoTransform)
        {
            //Check args:
            cipherTextStream.ValidateIsNotDefault("cipherTextStream");
            plainTextStream.ValidateIsNotDefault("plainTextStream");
            cryptoTransform.ValidateIsNotDefault("cryptoTransform");

            //Step 1: Deal with reading vars first:
            // Read the unencrypted file into fileData 
            //Create variables to help with read and write.
            //long streamLength = cipherTextStream.Length;

            //If any info was prepended (eg IV and/or Salt), 
            //then the startPos won't be zero:
            //Total length of input left is:
            long totalInputByteLength = cipherTextStream.Length - cipherTextStream.Position;

            //If small, we'll do it all in one go:
            //1Kb = 1024;
            //1Mb = 1024 * 1024 = 1048576;
            int bufferLength = (totalInputByteLength < MaxChunkSize) ? (int) totalInputByteLength : ChunkSize;
            byte[] buffer = new byte[bufferLength];
            long bytesRead = 0; //Total read so far.

            // Step 4: Create the CryptoStream object wrapping cipherTextStream
            using (CryptoStream cryptocipherTextStream =
                new CryptoStream(cipherTextStream, cryptoTransform, CryptoStreamMode.Read))
            {
                /*
        //Skip the embedded Salt:
        cryptocipherTextStream.Seek(8, 0);
        //Skip the embedded IV:
        cryptocipherTextStream.Seek(8, 0);
        */

                int bytesActuallyRead; //Total read this pass.

                //Read from the input file, then encrypt and write to the output file.
                //don't do it this way as the encrypted length will *always* be longer
                //than the bytes read...
                do
                {
                    bytesActuallyRead = cryptocipherTextStream.Read(buffer, 0, bufferLength);

                    plainTextStream.Write(buffer, 0, bytesActuallyRead);
                    bytesRead = bytesRead + bytesActuallyRead;

                    //Update user:
                    LogEntry(Diagnostics.TraceLevel.Verbose,
                             "{0}/{1}[{2}] bytes processed. ",
                             bytesRead, totalInputByteLength,
                             "{0:P}".FormatStringInvariantCulture(bytesRead/totalInputByteLength));


                    LogEntry(Diagnostics.TraceLevel.Verbose, bytesRead < totalInputByteLength ? "Processing..." : "Finished.");
                } while (bytesActuallyRead != 0);
            } //Close cryptoStream
        }

//Method in

        #endregion

        #region Public Methods - Initialization

        /// <summary>
        ///   Initializes the specified key phrase.
        ///   <para>
        ///     A valid <paramref name = "keyPhrase" /> must be provided.
        ///   </para>
        /// </summary>
        /// <param name = "keyPhrase">
        ///   The key phrase. Cannot be null or empty.
        /// </param>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if keyPhrase is null or empty.
        /// </exception>
        public void Initialize(string keyPhrase)
        {
            Initialize(keyPhrase, null, null);
        }

        /// <summary>
        ///   Instantiates and Initializes the Symmetric Algorithm.
        ///   <para>
        ///     A valid <paramref name = "keyPhrase" /> must be provided.
        ///   </para>
        ///   <para>
        ///     <paramref name = "ivBytes" /> and <paramref name = "saltBytes" /> can be null:
        ///     random values will be provided as needed.
        ///   </para>
        /// </summary>
        /// <param name = "keyPhrase">The key phrase. Cannot be null or empty.</param>
        /// <param name = "ivBytes">The iv bytes. If null, a random IV will be generated.</param>
        /// <param name = "saltBytes">The salt bytes. If null, a Salt (derived from the IV) will be generated.</param>
        /// <exception cref = "System.ArgumentNullException">
        ///   An exception is raised if keyPhrase is null or empty.
        /// </exception>
        public void Initialize(string keyPhrase, byte[] ivBytes, byte[] saltBytes)
        {
            if (_initialized)
            {
                throw new NotSupportedException("Class has already been initialized.");
            }

            keyPhrase.ValidateIsNotNullOrEmpty("keyPhrase");

            if (ivBytes != null)
            {
                if (ivBytes.Length != _symmetricAlgorithm.BlockSize/8)
                {
                    throw new ArgumentException(
                        "An 'ivByte' array was provided, but its size doesn't correspond to the SymmetricAlgorithm's BlockSize."
                            .FormatStringExceptionCulture());
                }

                _symmetricAlgorithm.IV = ivBytes;
            }


            if (saltBytes == null)
            {
                _saltBytes = _symmetricAlgorithm.IV;
            }
            else if (saltBytes.Length != _symmetricAlgorithm.BlockSize/8)
            {
                string msg =
                    "An '_SaltBytes' array was provided, but its size doesn't correspond to the SymmetricAlgorithm's BlockSize."
                        .FormatStringExceptionCulture();


                throw new ArgumentException(msg);
            }

            _keyPhrase = keyPhrase;

            GenerateKey(keyPhrase, saltBytes);

            _initialized = true;
        }

        /// <summary>
        ///   Instantiates and Initializes the Symmetric Algorithm.
        ///   <para>
        ///     This override is intended when the KeyBytes have already
        ///     been determined. 
        ///   </para>
        ///   <para>
        ///     <paramref name = "ivBytes" /> and <paramref name = "keyBytes" /> can be null:
        ///     random values will be provided as needed.
        ///   </para>
        /// </summary>
        /// <param name = "keyBytes">The key bytes.</param>
        /// <param name = "ivBytes">The iv bytes. If null, a random IV will be generated.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720", Justification = "Bytes is perfectly good spelling.")]
        public void Initialize(byte[] ivBytes, byte[] keyBytes)
        {
            if (ivBytes != null)
            {
                _symmetricAlgorithm.IV = ivBytes;
            }


            //Create SALT from password:
            //byte[] _SaltBytes = new byte[_SymmetricAlgorithm.BlockSize / 8];
            //RandomNumberGenerator.Create().GetBytes(_SaltBytes);
            //Simpler! Since IV is random, why not use it? Exactly... :-)
            if (keyBytes != null)
            {
                if (keyBytes.Length != _symmetricAlgorithm.KeySize/8)
                {
                    throw new ArgumentException(
                        "Key size '{0}' is not the right dimension.".FormatStringCurrentUICulture(keyBytes.Length));
                }
                _symmetricAlgorithm.Key = keyBytes;
            }


            _initialized = true;
        }

        #endregion

        #region Public Methods - Encryption

        /// <summary>
        ///   Returns an encrypted version (the cipherText) of the given plainText.
        /// </summary>
        /// <param name = "plainText">The plain text to encrypt.</param>
        /// <returns>A string containting the encrypted cipherText.</returns>
        public string Encrypt(string plainText)
        {
            if (!_initialized)
            {
                throw new NotSupportedException("Class in not Initialized.");
            }
            //Convert the data to a byte array:
            byte[] bytes =
                Encoding.Unicode.GetBytes(plainText);
            //System.Convert.FromBase64String(plainText);

            using (MemoryStream plainTextStream = new MemoryStream())
            {
                plainTextStream.Write(bytes, 0, bytes.Length);
                plainTextStream.Position = 0;
                //Build a memory stream:
                using (MemoryStream cipherTextStream = new MemoryStream())
                {
                    Encrypt(plainTextStream, cipherTextStream);

                    string result = Convert.ToBase64String(cipherTextStream.ToArray());
                    return result;
                }
            }
        }

        /// <summary>
        ///   Encrypts the specified source file, writting the output to the
        ///   output cipherText file.
        /// </summary>
        /// <param name = "inputPlainTextFile">The in plain text file.</param>
        /// <param name = "outputCipherTextFileName">Name of the out cypher text file.</param>
        public void Encrypt(string inputPlainTextFile, string outputCipherTextFileName)
        {
            using (FileStream fileStreamIn = new FileStream(inputPlainTextFile, FileMode.Open, FileAccess.Read))
            {
                using (
                    FileStream fileStreamOut = new FileStream(outputCipherTextFileName, FileMode.OpenOrCreate,
                                                       FileAccess.Write))
                {
                    fileStreamOut.SetLength(0);
                    Encrypt(fileStreamIn, fileStreamOut);
                } //~fileOut
            } //~fileIn
        }


        /// <summary>
        ///   Reads the contents of the given plainTextStream, and outputs
        ///   an encrypted version to the given cipherTextStream.
        /// </summary>
        /// <param name = "inputPlainTextStream">The plaintext stream in.</param>
        /// <param name = "outputCipherTextStream">The ciphered stream out.</param>
        public void Encrypt(Stream inputPlainTextStream, Stream outputCipherTextStream)
        {
            //Check Args:
            inputPlainTextStream.ValidateIsNotDefault("inputPlainTextStream");
            outputCipherTextStream.ValidateIsNotDefault("outputCypherTextStream");

            //And optionally embed IV to outstream (before encryption starts):
            if (_embedIVAtHeadOfOutgoingStream)
            {
                /*
        if (XSourceSwitch.ShouldTrace(TraceLevel.Verbose)) {
          Debug.WriteLine("Embedding: {0}".FormatStringCulture(DumpBuffer("IVBytes", _SymmetricAlgorithm.IV)));
        }
         */
                outputCipherTextStream.Write(_symmetricAlgorithm.IV, 0, _symmetricAlgorithm.IV.Length);
            }
            //And optionally embed salt to outstream (before encryption starts):
            if (_embedSaltAtHeadOfOutgoingStream)
            {
                /*
        if (XSourceSwitch.ShouldTrace(TraceLevel.Verbose)) {
          Debug.WriteLine("Embedding: {0}".FormatString(DumpBuffer("SaltBytes", _SaltBytes)));
        }
         */
                outputCipherTextStream.Write(_saltBytes, 0, _saltBytes.Length);
            }
            //And optionally embed Key to outstream (before encryption starts):
            if (_embedKeyAtHeadOfOutgoingStream)
            {
                /*
        if (XSourceSwitch.ShouldTrace(TraceLevel.Verbose)) {
          Debug.WriteLine("Embedding: {0}".FormatString(DumpBuffer("KeyBytes", _SymmetricAlgorithm.Key)));
        }
         */
                outputCipherTextStream.Write(_symmetricAlgorithm.Key, 0, _symmetricAlgorithm.Key.Length);
            }

            LogEntry(Diagnostics.TraceLevel.Verbose, "Encrypting: {0}", DumpBuffer("Key", _symmetricAlgorithm.Key));

            //Now encrypt the rest:
            using (ICryptoTransform cryptoTransform = _symmetricAlgorithm.CreateEncryptor())
            {
                Encrypt(inputPlainTextStream, outputCipherTextStream, cryptoTransform);
            }
        }

        #endregion

        #region Public Methods - Decryption

        /// <summary>
        ///   Returns the decrypted plainText from the given cipherText.
        /// </summary>
        /// <param name = "cipherText">The encrypted cipherText.</param>
        /// <returns>A string containing the decrypted plainText.</returns>
        public string Decrypt(string cipherText)
        {
            //Convert the data to a byte array:
            byte[] bytes = Convert.FromBase64String(cipherText);


            //System.Convert.FromBase64String(textToEncrypt);

            using (MemoryStream cipherTextStream = new MemoryStream())
            {
                cipherTextStream.Write(bytes, 0, bytes.Length);
                cipherTextStream.Position = 0;
                //Build a memory stream:
                using (MemoryStream plainTextStream = new MemoryStream())
                {
                    Decrypt(cipherTextStream, plainTextStream);
                    string result = Encoding.Unicode.GetString(plainTextStream.ToArray());

                    return result;
                }
            }
        }

        /// <summary>
        ///   Encrypts the specified source file, writting the output to the
        ///   output cipherText file.
        /// </summary>
        /// <param name = "inputCipherTextFile">The in plain text file.</param>
        /// <param name = "outputPlainTextFileName">Name of the out cypher text file.</param>
        public void Decrypt(string inputCipherTextFile, string outputPlainTextFileName)
        {
            using (FileStream fileStreamIn = new FileStream(inputCipherTextFile, FileMode.Open, FileAccess.Read))
            {
                using (
                    FileStream fileStreamOut = new FileStream(outputPlainTextFileName, FileMode.OpenOrCreate,
                                                       FileAccess.Write))
                {
                    fileStreamOut.SetLength(0);
                    Decrypt(fileStreamIn, fileStreamOut);
                } //~fileOut
            } //~fileIn
        }


        /// <summary>
        ///   Reads the contents of the given cipherTextStream, and outputs
        ///   the decrypted version to the given plainTextStream.
        /// </summary>
        /// <param name = "inputCipherTextStream">The stream in.</param>
        /// <param name = "outputPlainTextStream">The stream out.</param>
        public void Decrypt(Stream inputCipherTextStream, Stream outputPlainTextStream)
        {
            inputCipherTextStream.ValidateIsNotDefault("inputCypherTextStream");
            outputPlainTextStream.ValidateIsNotDefault("outputPlainTextStream");

            bool requiresNewKey = false;

            //Use given IV, or extract it from head of stream:
            if (_embedIVAtHeadOfOutgoingStream)
            {
                //Read in IV from start of Stream:
                byte[] tmpBuffer = new byte[_symmetricAlgorithm.BlockSize/8];
                inputCipherTextStream.Read(tmpBuffer, 0, tmpBuffer.Length);

                /*
       if (XSourceSwitch.ShouldTrace(TraceLevel.Verbose)) {
          Debug.WriteLine("Extracting: {0}".FormatString(DumpBuffer("IVBytes", tmpBuffer)));
        }
        */

                _symmetricAlgorithm.IV = tmpBuffer;
            }

            //Get the Salt from the IV, or get it from the head of the stream:
            if (_embedSaltAtHeadOfOutgoingStream)
            {
                //Read in Salt from start of Stream:
                _saltBytes = new byte[_symmetricAlgorithm.BlockSize/8];
                inputCipherTextStream.Read(_saltBytes, 0, _saltBytes.Length);
                /*
        if (XSourceSwitch.ShouldTrace(TraceLevel.Verbose)) {
          Debug.WriteLine("Extracting: {0}".FormatString(DumpBuffer("_SaltBytes", _SaltBytes)));
        }
         */
                requiresNewKey = true;
            }


            //Get the KeyBytes from the head of the stream if that's where it was parked:
            if (_embedKeyAtHeadOfOutgoingStream)
            {
                //Read in KeyBytes from start of Stream:
                byte[] tmpBuffer = new byte[_symmetricAlgorithm.BlockSize/8];
                inputCipherTextStream.Read(tmpBuffer, 0, tmpBuffer.Length);

                /*
        if (XSourceSwitch.ShouldTrace(TraceLevel.Verbose)) {
          Debug.WriteLine("Extracting: {0}".FormatString(DumpBuffer("_KeyBytes", tmpBuffer)));
        }
         */
                _symmetricAlgorithm.Key = tmpBuffer;
                requiresNewKey = false;
            }
            else
            {
                Debug.Print("...");

                if (requiresNewKey)
                {
                    GenerateKey(_keyPhrase, _saltBytes);
                }
            }

            LogEntry(Diagnostics.TraceLevel.Verbose, "Decrypting: {0}", DumpBuffer("Key", _symmetricAlgorithm.Key));

            using (ICryptoTransform cryptoTransform = _symmetricAlgorithm.CreateDecryptor())
            {
                Decrypt(inputCipherTextStream, outputPlainTextStream, cryptoTransform);
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        ///   Initializes the internal <see cref = "SymmetricAlgorithm" /> 
        ///   with the Initialization vector (IV) and key string or byte buffer as specified when
        ///   <c>Initialize</c> was invoked.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Invoked only by Constructors.
        ///   </para>
        /// </remarks>
        protected void InitializeAlgorithm(CipherMode cipherMode, PaddingMode paddingMode)
        {
            _symmetricAlgorithm = SymmetricAlgorithm.Create(_algorithmName);

            if (_symmetricAlgorithm == null)
            {
                string msg =
                    "Could not create specified SymmetricAlgorithm ('{0}')".FormatStringCurrentCulture(_algorithmName);


                throw new ArgumentException(msg);
            }

            _symmetricAlgorithm.Mode = cipherMode;
            _symmetricAlgorithm.Padding = paddingMode;
        }


        /// <summary>
        ///   Generates the key.
        /// </summary>
        protected void GenerateKey(string keyPhrase, byte[] saltBytes)
        {
            if (keyPhrase.IsNullOrEmpty())
            {
                throw new ArgumentNullException("keyPhrase");
            }
            //Get DerivedBytes helper from key+salt:
            //Note:PasswordDeriveBytes is obsolete since .NET 2.0

            if (saltBytes == null)
            {
                saltBytes = _symmetricAlgorithm.IV;
            }

            DeriveBytes derivedBytes = new Rfc2898DeriveBytes(keyPhrase, saltBytes, keyPhrase.Length);

            //Use DerivedBytes to obtain Obfuscated key bytes:
            _symmetricAlgorithm.Key = derivedBytes.GetBytes(_symmetricAlgorithm.KeySize/8);
        }

        /// <summary>
        ///   Helper method to dump to string in a 
        ///   readable format the content of the various byte buffers.
        /// </summary>
        /// <param name = "label">The label.</param>
        /// <param name = "buffer">The buffer.</param>
        /// <returns></returns>
        protected static string DumpBuffer(string label, byte[] buffer)
        {
            buffer.ValidateIsNotDefault("buffer");

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("[{0}] [Len:{1}] ", label, buffer.Length);

            //sb.Append(Collections.CollectionToString(buffer, ","));

            sb.Append("{");
            int iMax = Math.Min(16, buffer.Length);
            for (int i = 0; i < iMax; i++)
            {
                sb.Append("{0:X2}".FormatStringInvariantCulture(buffer[i]));
                //sb.Append(",");
            }
            //if (iMax > 0) { sb.Remove(sb.Length - 1, 1); }
            sb.Append("}");

            return sb.ToString();
        }

        #endregion

        /// <summary>
        ///   Logs the entry.
        /// </summary>
        /// <param name = "traceLevel">The trace level.</param>
        /// <param name = "message">The message.</param>
        /// <param name = "args">The args.</param>
        protected static void LogEntry(Diagnostics.TraceLevel traceLevel, string message, params object[] args)
        {
            if (_tracingService == null)
            {
                return;
            }
            _tracingService.Trace(
                traceLevel,
                message,
                args);
        }
    }


}


